/*
 * Copyright (C) 2015 Angel Zaprianov <me@fire1.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

(function(a) {
    a.fn.duplicateElement = function(b) {
        b = a.extend(a.fn.duplicateElement.defaults, b);
        return this.each(function() {
            var e = a(this);
            var c = document.createElement(b.tag_name);
            c.setAttribute("id", b.tag_id);
            e[0].parentNode.appendChild(c);
            var d = 0;
            a(e, a("#" + b.tag_id)).on("click", b.class_create, function(f) {
                // e.clone().addClass("dinamic-field").appendTo("#" + b.tag_id);
                var new_one = e.clone().addClass("dinamic-field");
                new_one.find("input[type='text']").val("");
                new_one.find("input[type='hidden']").val("");
                new_one.find(".informational_messages_area").html("");
                $("#" + b.tag_id).append(new_one);
                a(b.class_remove).show();
                a(b.class_remove).first().hide();
                a(b.class_create).hide();
                a(b.class_create).first().show();
                f.preventDefault();
                if (typeof b.onCreate === "function") b.onCreate();                
                d++;
                return false;
            });
            e.find(b.class_remove).first().hide();
            a("#" + b.tag_id).on("click", b.class_remove, function(f) {
                a(this).parents(".dinamic-field").remove();
                f.preventDefault();
                if (typeof b.onRemove === "function") b.onRemove();                
                return false;
            })
        })
    };
    a.fn.duplicateElement.defaults = {
        tag_name: "div",
        tag_id: "dinamic-fields",
        clone_model: "#clone-field-model",
        class_remove: ".remove-this-fields",
        class_create: ".create-new-fields",
        onCreate: "",
        onRemove: ""
    }
})(jQuery);
