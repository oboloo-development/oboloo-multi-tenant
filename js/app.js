function checkRegistration()
{
	var errors = "";

	if ($.trim($('#new_fullname').val()) == '')
	{
		if (errors == "") errors = 'Full name is a required field';
		else errors = errors + '<br />Full name is a required field';
	}

	if ($.trim($('#new_username').val()) == '')
	{
		if (errors == "") errors = 'User name (login id) is a required field';
		else errors = errors + '<br />User name (login id) is a required field';
	}

	if ($.trim($('#new_password').val()) == '')
	{
		if (errors == "") errors = 'Password is a required field';
		else errors = errors + '<br />Password is a required field';
	}

	if ($.trim($('#new_email').val()) == '')
	{
		if (errors == "") errors = 'Email address is a required field';
		else errors = errors + '<br />Email address is a required field';
	}

	if (errors == "")
	{
	    $.ajax({
			url: BICES.Options.baseurl + '/app/validateRegistration',
	        type: 'POST', async: false, dataType: 'json',
	        data: {
	                username: $.trim($('#new_username').val()),
	                email: $.trim($('#new_email').val())
	            },
			success: function(existing_data) {
	            if (existing_data.username == 1)
	            {
					if (errors == "") errors = 'Username already exists';
					else errors = errors + '<br />Username already exists';
	            }
	            if (existing_data.email == 1)
	            {
					if (errors == "") errors = 'Email address already exists';
					else errors = errors + '<br />Email address already exists';
	            }
			},
			error: function() {
				if (errors == "") errors = 'Cannot check for duplicate username and/or email address ... Please try later';
				else errors = errors + '<br />Cannot check for duplicate username and/or email address ... Please try later';
			}
		});

	    $.ajax({
			url: BICES.Options.baseurl + '/app/validatePassword', type: 'POST',
			async: false, data: { password: $.trim($('#new_password').val()) },
			success: function(return_data) {
	            if (return_data.length != 0)
	            {
					if (errors == "") errors = return_data.replace(/"/g, '');
					else errors = errors + '<br />' + return_data.replace(/"/g, '');
	            }
			},
			error: function() {
				if (errors == "") errors = 'Cannot check for password restrictions ... Please try later';
				else errors = errors + '<br />Cannot check for password restrictions ... Please try later';
			}
		});
	}

    if (errors == "") {
		$('#register_form').submit();
	}

    else
    {
    	$('#registration_errors').html(errors);
    	$('#delete_confirm_modal').modal('show');
		return false;
    }
}

function checkLogin()
{
    /*
	var submit = true;
	if (!validator.checkAll($('#login_form'))) submit = false;
	if (submit) $('#login_form').submit();
	return false;
    */
    $('#login_form').submit();
}

$(document).ready(function() {

    $('.to_register').click(function() {
        $('#message_area').hide();
        $('#status_area').css('color', '#73879C');
        $('#status_area').html('');
    });

});
