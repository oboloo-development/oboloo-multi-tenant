if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

// function to move to a certain area on the page with scrolling effect
function goToWithScroll(id)
{
	$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
}

// function to copy field value one to the other
function copyField(src, dest)
{
	if ($.trim($('#' + src).val()) != '') $('#' + dest).val($.trim($('#' + src).val()));
}

// append error message to existing set of errors
function addToError(error, message)
{
	if ($.trim(error) == '') error = message;
	else error = error + '<br />' + message;
	return error;
}

// check if the input field has empty value or not
function isFieldEmpty(fieldid)
{
	return $.trim($('#' + fieldid).val()) == '';
}

// check if at least one field from the list of inputs is populated or not
function checkAtLeastOneField(all_fields)
{
	var found_filled = false;
	for (var i=0; i<all_fields.length; i++)
		if (!isFieldEmpty(all_fields[i])) found_filled = true;
	return found_filled;
}

// reset/blank out values populated for fields passed in as input
function clearFieldValues(all_fields)
{
	for (var i=0; i<all_fields.length; i++) $('#' + all_fields[i]).val('');
}

// check if ALL of the fields from the list of inputs is populated or not
function checkRequiredFields(required_fields)
{
	var error = '';
	var field_label = '';

	for (var i=0; i<required_fields.length; i++)
	{
		if (isFieldEmpty(required_fields[i]))
		{
			field_label = '';
			$('label').each(function() {
				if ($(this).attr('for') == required_fields[i])
					field_label = $(this).text();
			});

			if (error == '') error = field_label + ' is a required field.';
			else error = error + '<br />' + field_label + ' is a required field.';
		}
	}

	return error;
}

// toggle values of checkboxes ie check boxes if they
// are unchecked and uncheck the already checked ones
function toggleCheckboxes(class_name)
{
	if ($('#allbox').is(':checked'))
		$('.' + class_name).each(function() { $(this).attr('checked', 'checked'); });
	else
		$('.' + class_name).each(function() { $(this).removeAttr('checked'); });
}


function loadDepartments(department_id)
{
	var location_id = $('#location_id').val();
	
	if (location_id == 0)
		$('#department_id').html('<option value="0">All Departments</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { location_id: location_id }, dataType: "json",
	        url: BICES.Options.baseurl + '/locations/getDepartments',
	        success: function(options) {
	        	var options_html = '<option value="0">All Departments</option>';
	        	for (var i=0; i<options.length; i++)
	        		options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
	        	$('#department_id').html(options_html);
						$("#department_id").trigger('change');
	        	if (department_id != 0) $('#department_id').val(department_id); 
	        },
	        error: function() { $('#department_id').html('<option value="0">All Departments</option>'); }
	    });
	}
}


function loadDepartmentsForEdit(department_id)
{
	var location_id = $('#location_id').val();
	
	if (location_id == 0)
		$('#department_id').html('<option value="">Select Department</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { location_id: location_id }, dataType: "json",
	        url: BICES.Options.baseurl + '/locations/getDepartments',
	        success: function(options) {
	        	var options_html = '<option value="">Select Department</option>';
	        	for (var i=0; i<options.length; i++)
	        		options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
	        	$('#department_id').html(options_html);
	        	if (department_id != 0) $('#department_id').val(department_id); 
	        },
	        error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
	    });
	}
}

