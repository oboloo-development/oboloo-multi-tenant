$(document).ready(function(){
	 autoLoadDocumentTable();
	 $("#file_upload_alert").hide();
	$('#saving_document_form').on('submit', function(){
		var saving_id = $('.saving_id').val();
		var doc_title = $('.doc_title').val();
		var doc_file  = $('.doc_file').prop("files")[0];
		
		var form_data = new FormData();
		form_data.append("saving_id", saving_id);
    	form_data.append("doc_title", doc_title);
    	form_data.append("doc_file", doc_file);
    
		if(doc_title == '' || doc_file == ''){
			return false;
		}

		$.ajax({
	        url:  BICES.Options.baseurl + '/savingsUsg/saveDocument',
	         dataType: 'json', // what to expect back from the PHP script, if anything
		     cache: false,
		     contentType: false,
		     processData: false,
		     data: form_data,
		     type: 'post',
	         beforeSend: function() {
	          $(".processing_oboloo").show();
	          $("#file_upload_alert").html('');
	          $("#file_upload_alert").hide();
	        },
	        success: function(data) {
	         if (data.uploaded == 1) {
	          $("#file_upload_alert").show();
	          $("#file_upload_alert").html('File uploaded successfully');
	          $(".processing_oboloo").hide();
              autoLoadDocumentTable();
			  $('.doc_title').val('');
			  $('.doc_file').val('');
          	 } else {
              $("#file_upload_alert").html('Problem occured while uploading file, try again');
              $("#file_upload_alert").show();
          	 }
          }
      });
   
	});

 /* download Document */
$("#file_upload_alert").hide();
$(".processing_oboloo").hide();

 function autoLoadDocumentTable(){
    var saving_id = $('.saving_id').val();
    if(saving_id !== undefined){
    $.ajax({
          type: 'POST', async: false, dataType: 'html',
          data: { saving_id: saving_id},
          url: BICES.Options.baseurl + '/savingsUsg/getSavingDocumentList',
          success: function(data){
          data = JSON.parse(data);
          $("#saving_document_list").html(data);
         }
      });
    }
 }

}); 

/* delete Document */
function deleteSavingDocument(id){
  var savingDocID = id;
    $.confirm({
      title: false,
      content: '<spam style="font-size:11px">Are you sure want to delete this file/document</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              dataType: 'html',
              type:'POST',
              url: BICES.Options.baseurl + '/savingsUsg/savingDocumentsDelete',
              data:{savingDocID:savingDocID},
              success: function(data){
                  $("#file_upload_alert").show();
                  $("#file_upload_alert").html('File deleted successfully');
                  $('.tr_'+id).remove();
              }
            });
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });

    $("#file_upload_alert").delay(1000 * 5).fadeOut();
}

$(document).ready(function(){
	$(':checkbox[readonly]').click(function(){ return false; });
  $(':checkbox[readonly]').parent('label').addClass('saving_usg_checkbox');
  $(':checkbox[readonly]').css('opacity','0.5');

  // $('#saving_document').dataTable({});

  
});