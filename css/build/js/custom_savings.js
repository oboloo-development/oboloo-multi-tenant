
function loadUpdateSavingChart2(){
/* $.ajax({
    type: "post",
    url: savingChartUrl2,
    data: {saving_id:saving_id},
    dataType: "json",
    success: function (response) {
      console.log(response.reduction);
      console.log(response.avoidance);
 
    ApexCharts.exec('id_saving_bar_chart', 'updateSeries', [{
          name: 'Cost Reduction',
          data: response.reduction
        }, {
          name: 'Cost Avoidance',
          data: response.avoidance
        }], true);

     //  ApexCharts.exec("id_saving_bar_chart", "updateOptions", {
      //  xaxis: {
      //    categories: ['Projected Savings', 'Realised Savings']
      //  },
       
      //});
    }
  });*/ 
}
$(document).ready(function(){
  $(".ajax_success").hide();
  $(".processing_oboloo").hide();
   $('#due_date').datetimepicker({
    format: dateFormat,});
   // $('.generaldate').datetimepicker({
   //  format: dateFormat,});

  $(".select_search").select2({
       containerCssClass: "notranslate",
       dropdownCssClass: "notranslate"
    });


// Start: Saving Milestone
  $("#add_milestone_form").submit(function(event){
    var mileStone = $("#milestone_title").val();
    var dueDate = $("#due_date").val();
    //var savingStartDate = $("#start_date").val();
    var savingDuration=$("#field_value").val();
    var savingDueDate = $("#milestone_due_date").val();
    var baseLine = $("#base_line_spend").val();
    var costReduction = $("#cost_reduction").val();
    var costAvoidance = $("#cost_avoidance").val();
    var notes = $("#milestone_notes").val();

    var durationMontly= new Array();
    $('.milestone-plann-duration').each(function(){
        mothly  = {
          'fieldName' : $(this).attr("name"),
          'fieldValue' : $(this).val()
        };
        durationMontly.push(mothly);
    });

      $.ajax({
          url: addMilestoneUrl, // point to server-side PHP script 
          dataType: 'json',  // what to expect back from the PHP script, if anything
          data: {title:mileStone,due_date:dueDate,/*saving_start_date:savingStartDate,*/saving_duration:savingDuration,savings_due_date:savingDueDate,base_line_spend:baseLine,cost_reduction:costReduction,cost_avoidance:costAvoidance,notes:notes,saving_id:saving_id,duration_monthly: durationMontly},
          type: 'post',
           beforeSend: function() {
            $(".processing_oboloo").show();
            $(".ajax_success").html('');
            $(".ajax_success").hide();
            $(".btn-functionality").attr('disabled','disabled');
            $(".btn-functionality").prop('disabled',true);
            $("#add_milestone_btn").prop('disabled',true);
            $("#nextBtn").prop('disabled',true);
           },
          success: function(uploaded_response){

            if(uploaded_response.mesg==1){
              //$(".ajax_success").show();
              $(".ajax_success").html('Milestone added successfully');
              window.location.reload();
            }else{

             // $(".ajax_success").html('Problem occured while saving Milestone, try again');
             // $(".ajax_success").show();
            }
           
            $(".processing_oboloo").hide();
            $(".btn-functionality").prop('disabled',false);
            $("#add_milestone_btn").prop('disabled',false);
            $("#nextBtn").prop('disabled',false);
            // $('#milestone_table').DataTable().ajax.reload();
            $("#milestone_title").val('');
            $("#due_date").val('');
            //$("#start_date").val('');
            $("#base_line_spend").val('');
            $("#cost_reduction").val('');
            $("#cost_avoidance").val('');
            $("#milestone_notes").val('');
            $("#amountFields").hide();
            $("#add_milestone").modal('hide');

          }
       });

      //$(".ajax_success").delay(1000*5).fadeOut();
      return false;
  });
 // END: Saving Milestone

 //Start: if saving summary is clicked
 $('#quote-tab').click(function () {

     $.ajax({
        dataType: 'json',  
        type: "post",
        url: savingCalculationFieldsUrl,
        data: {saving_id:saving_id },
        success: function(response){
          $("#total_project_saving").val(response.total_project_saving);
          $("#total_project_saving_perc").val(response.total_project_saving_perc);
          $("#realised_saving").val(response.realised_saving);
          $("#realised_saving_perc").val(response.realised_saving_perc);
          $("#saving_due_date").val(response.due_date);

        } 
           
    });
});
 //End: if saving summary is clcked

 // Start: line and area chart

    /*var options = {
          series: [{
          name: 'Projected Savings',
          type: 'area',
          data: [44, 55, 31, 47]
        }, {
          name: 'Realised Savings',
          type: 'line',
          data: [55, 69, 45, 61]
        }],
          chart: {
          height: 350,
          type: 'line',
        },
        stroke: {
          curve: 'smooth'
        },
        fill: {
          type:'solid',
          opacity: [0.35, 1],
        },
        labels: ['M-1', 'M-2','M-3','M-4'],
        markers: {
          size: 0
        },
        yaxis: [
          {
            title: {
              text: 'Projected Savings',
            },
          },
          {
            opposite: true,
            title: {
              text: 'Realised Savings',
            },
          },
        ],
        tooltip: {
          shared: true,
          intersect: false,
          y: {
            formatter: function (y) {
              if(typeof y !== "undefined") {
                return  y.toFixed(0) + " points";
              }
              return y;
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#saving_area_chart"), options);
        chart.render();*/
    
    });


function loadSubcategories(subcategoryUrl,category_id,subcategory_id){
  var category_id = category_id;
  var subcategory_id = subcategory_id;
  if (category_id == 0)
    $('#subcategory_id').html('<option value="0">Select Sub Category</option>');
  else
  {
      $.ajax({
          type: "POST", data: { category_id: category_id,subcategory_id:subcategory_id}, dataType: "json",
          url: subcategoryUrl,
          success: function(options) {
            var options_html = '<option value="0">Select  Sub Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#subcategory_id').html(options_html);
            if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id); 
          },
          error: function() { $('#subcategory_id').html('<option value="0">Select Sub Category</option>'); }
      });
  }}
function loadDepartmentsForSingleLocation(sel){
    var location_id = sel.value;
    var single = 1;
    var department_id  = document.getElementById("department_id");
    
    loadDepartments(departmentUrl,location_id,department_id);}
function loadDepartments(url,location_id,department_id){
    var location_id = location_id;
    var single = 1;
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: url,
            success: function(options) {
                var options_html = '<option value="">Select</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                $('#department_id').html(options_html);
               if (department_id != 0) $('#department_id').val(department_id); 
            },
            error: function() { $('#department_id').html('<option value="">Select</option>'); }
        });}
function deleteMilestone(recordKey)
{
  var recordKey = recordKey;
  $(".ajax_success").hide();

   $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Are you sure want to delete milestone</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                $.ajax({
                                    dataType: 'json',  
                                    type: "post",
                                    url: deleteMilestoneUrl,
                                    data: { recordKey: recordKey },
                                    success: function(uploaded_response){
                                      if(uploaded_response.mesg==1){
                                        $(".ajax_success").show();
                                        $(".ajax_success").html('Milestone deleted successfully');
                                      }else{
                                        $(".ajax_success").show();
                                        $(".ajax_success").html('Problem occured while deleting milestone, try again');
                                      }
                                      $('#milestone_table').DataTable().ajax.reload();
                                    }
                                });
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },
                
                
                
            }});
 
      $(".ajax_success").delay(1000*5).fadeOut();
}


function deletePlanning(recordKey)
{
  var recordKey = recordKey;
  $(".ajax_success").hide();

   $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Are you sure want to delete this plan</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                $.ajax({
                                    dataType: 'json',  
                                    type: "post",
                                    url: deletePlaneUrl,
                                    data: { recordKey: recordKey },
                                    success: function(uploaded_response){
                                      if(uploaded_response.mesg==1){
                                        $(".ajax_success").show();
                                        $(".ajax_success").html('Milestone deleted successfully');
                                      }else{
                                        $(".ajax_success").show();
                                        $(".ajax_success").html('Problem occured while deleting milestone, try again');
                                      }
                                      $('#milestone_table').DataTable().ajax.reload();
                                    }
                                });
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },
                
                
                
            }});
 
      $(".ajax_success").delay(1000*5).fadeOut();
}

function milestoneRealsedCalculation(milestoneID,spendLine)
{  
  reduction = parseInt($("#complete_edit_realised_cost_reduction"+milestoneID).val());
  avoidance = parseInt($("#complete_edit_realised_cost_avoidance"+milestoneID).val());
  $("#complete_edit_realised_saving"+milestoneID).val(reduction+avoidance);
  $("#complete_edit_realised_saving_perc"+milestoneID).val(((reduction+avoidance)/spendLine).toFixed(2));
}

function milestonePlanningCalculation(milestoneID)
{  
  reduction = parseInt($("#complete_edit_realised_cost_reduction"+milestoneID).val());
  avoidance = parseInt($("#complete_edit_realised_cost_avoidance"+milestoneID).val());
  $("#complete_edit_realised_saving"+milestoneID).val(reduction+avoidance);
}




