var selectedUsers = [];
  function contributorsPopup() {
    $.ajax({
      type: "POST",
      dataType: "json",
      url: BICES.Options.baseurl + '/quotes/quoteInternalUser',
      success: function(options) {
         options.forEach(function(user) {
                let checkbox = $('<input>', {
                    type: 'checkbox',
                    value: user.userID,
                    id: 'user_' + user.userID,
                    class: 'userCheckbox'
                });
                let label = $('<label>', {
                    for: 'user_' + user.userID,
                    text: user.full_name,
                    style: 'margin-left: 10px; font-weight:100'
                });
                $('#quote_internal_user').append(checkbox).append(label).append('<br>');
                $('.required').hide();
            });
      },error: function(error) {
            console.error("Error fetching users:", error);
        }
    });

    $.confirm({
      title: false,
      content: `<div class="row m-0">
                  <h5 class="heading">Invite Internal Contributors</h5><br/>
                  <h5 class="heading text-danger required">Please select at least one checkbox</h5><br/>
                    <div class="col-lg-12 col-xs-12">
                    <label class="form-label">Select Users<span class="text-danger">*</span></label>
                    </div>
                  </div>
                  <div class="row m-0 ">
                    <div class="clone_for_more_users row m-0">
                    <div class="col-lg-12 col-xs-12">
                      <div class="mt11" id="quote_internal_user" ></div>
                    </div>
                    </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-xs-12 form-group px-3 py-2 text-primary mt-15">
                    <h4 class="subheading text-black" style="font-size: 14px">Please select a deadline (UTC) for contributions below <span class="text-danger">*</span></h4><br/>
                    <div class="form-group">
                    <input type="datetime-local" id="datetime_contribute_deadline" onchange="contributeDeadline()" class="form-control" name="datetime">
                    <p id="my-message" class="text-danger">Please select a deadline (UTC)</p>
                    </div>
                  </div>`,
          buttons: {
            text: {
              text: "Send",
              btnClass: 'btn-blue send-user-internal disabled',
              action: function() {
                  var isChecked = false;
                  $('.userCheckbox').each(function(){
                      if($(this).is(':checked')){
                          isChecked = true;
                      }
                  });
                  if(isChecked){
                      $('#quote_form').submit();
                      $('.quote_internal_user_class').val(selectedUsers);
                      $('#quote_status').val('Draft');
                      $('.required').hide();
                  } else {
                      $('.required').show();
                      return false;
                  }
              }
            },
            No: {
              text: "Cancel",
              btnClass: 'btn-red',
              action: function() {
                $('.scoring_supplier_tab').show();
                show_hide_score = 0;
                $('.quote_internal_user_class').val("");
                // $("#prevBtn").trigger('click');
              }
            },

          }
        });
  }

function contributeDeadline() {
  var date = $("#datetime_contribute_deadline").val();
  $(".contributions_datetime").val(date);
  if (date !== '') {
    $(".send-user-internal").removeClass('disabled');
    $('#my-message').addClass('hidden');
  } else {
    $('#my-message').removeClass('hidden');
  }
  $(".contributions_datetime").attr("value", date);
};

// Event listener for checkbox clicks
$(document).on('click', '#quote_internal_user .userCheckbox', function() {
  let clickedValue = $(this).val();
  if ($(this).prop('checked')) {
    // If checkbox is checked, add value to array
    selectedUsers.push(clickedValue);
  } else {
    // If checkbox is unchecked, remove value from array
    selectedUsers = selectedUsers.filter(userID => userID !== clickedValue);
  }
});


  $(document).on('click', '.send-user-internal', function(){
    var isChecked = false;
    $('.userCheckbox').each(function(){
        if($(this).is(':checked')){
          isChecked = true;
          $('#quote_form').submit();
        }
    });
    if(!isChecked){
      $('.required').text('Please select at least one checkbox');
      return false;
    }
  });



