$(document).ready(function(){
  $(".select_search").select2({
     containerCssClass: "notranslate",
     dropdownCssClass: "notranslate"
    });


    // ---------------------------------------
    //  Start: information help popup side bar
    // ---------------------------------------

    $(".get_start").click(function(){
      $("#module_modal").modal('hide'); 
      $('#getting_start_module_modal').modal('show'); 
    });

    $('.help_centre').click(function(){
      $("#module_modal").modal('hide'); 
      $('#getting_start_module_modal').modal('hide'); 
      $('#help_centre_module_modal').modal('show');  
    });
    
    $('.home_redirect').click(function(){
      $("#module_modal").modal('show'); 
      $('#getting_start_module_modal').modal('hide'); 
      $('#help_centre_module_modal').modal('hide');  
    });

     $('.product_tour_visiable_sandbox_doamin').click(function(){
      $("#sandbox_help_sidebar_open_module_modal").modal('show'); 
      $('#getting_start_module_modal').modal('hide'); 
      $('#help_centre_module_modal').modal('hide'); 
      $("#module_modal").modal('hide'); 
    });

    // ---------------------------------------
    //  End: information help popup side bar
    // ---------------------------------------

    $('#venodr_contact_email, #email').keypress(function( e ) {
    if(e.which === 32) 
        return false;
    });
    
});

    function omit_special_char(e) {
      var k;
      document.all ? k = e.keyCode : k = e.which;
      return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    }

