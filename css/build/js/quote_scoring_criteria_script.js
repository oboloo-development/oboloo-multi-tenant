
//--------------- Docuement Sections ------------//
$("#file_upload_alert").hide();
$(".processing_oboloo").hide();
// save docuemnt, 
function uploadDocumentDraft(uploadBtnIDX, quote_id, e) {
  e.preventDefault();
  var uploadedObj = $('#order_file_' + uploadBtnIDX);
  var uploadedObjDesc = $('#file_desc_' + uploadBtnIDX);
  var quoteId = quote_id;


  var file_data = $('#order_file_' + uploadBtnIDX).prop('files')[0];
  var field_data = $('#file_desc_' + uploadBtnIDX).val();


  if (typeof file_data !== 'undefined') {

    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('file_description', field_data);
    form_data.append('quoteId', quoteId);
    $.ajax({
      url: BICES.Options.baseurl + '/quotes/uploadDocumentDraft', // point to server-side PHP script 
      dataType: 'json', // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      beforeSend: function () {
        $(".processing_oboloo").show();
        $("#file_upload_alert").html('');
        $("#file_upload_alert").hide();
        $(".btn-functionality").attr('disabled', 'disabled');
        $(".btn-functionality").prop('disabled', true);
        $("#btn_upload_1").prop('disabled', true);
      },
      success: function (uploaded_response) {
        if (uploaded_response.uploaded == 1) {
          $("#file_upload_alert").show();
          $("#file_upload_alert").html('File uploaded successfully');
          manageData();
          location.reload();
        } else {

          $("#file_upload_alert").html('Problem occured while uploading file, try again');
          $("#file_upload_alert").show();
        }

        $(".processing_oboloo").hide();
        $(".btn-functionality").prop('disabled', false);
        $("#btn_upload_1").prop('disabled', false);
        uploadedObj.val(null);
        uploadedObjDesc.val(null);
      }
    });
  } else {
    $("#file_upload_alert").show();
    $("#file_upload_alert").html('Document is required');
  }

  $("#file_upload_alert").delay(1000 * 5).fadeOut();
}
//List, Delete, list docuement ajax JS
manageData();
/* manage data list */
function manageData() {
  $.ajax({
    dataType: 'html',
    url: BICES.Options.baseurl + '/quotes/quoteDocumentsDraft',
    success: function (data) {
      data = JSON.parse(data)
      manageRow(data);
      $('#myQuoteDocumentEditModal').hide();
      $('.modal-backdrop.fade.in').remove();
      $('body').css('overflow', 'auto');
    }
  });

}

/* List Document table */
function manageRow(data) {
  var rows = '';
  $.each(data, function (key, value) {
    rows += '<tr>';
    rows += '<td>' + value.file_name + '</td>';
    rows += '<td>' + value.description + '</td>';
    rows += '<td data-id="' + value.file_name + '">';
    rows += '<button class="btn btn-danger remove-document_draft"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>';
    rows += '</td>';
    rows += '</tr>';
  });

  $("#tbody").html(rows);
}

/* delete Document */
$("body").on("click", ".remove-document_draft", function () {
  var filename = $(this).parent("td").data('id');
  var c_obj = $(this).parents("tr");

  $.confirm({
    title: false,
    content: '<spam style="font-size:11px">Are you sure want to delete this file/document</span>',
    buttons: {
      Yes: {
        text: "Yes",
        btnClass: 'btn-blue',
        action: function () {
          $.ajax({
            dataType: 'html',
            type: 'POST',
            url: BICES.Options.baseurl + '/quotes/quoteDocumentsDraftDelete',
            data: { filename: filename },
            success: function (done) {
              if (done == 1) {
                $("#file_upload_alert").show();
                $("#file_upload_alert").html('File deleted successfully');
                c_obj.remove();
                manageData();
                location.reload();
              } else {
                $("#file_upload_alert").show();
                $("#file_upload_alert").html('Problem occured while deleting file, try again');
              }
            }
          });
        }
      },
      No: {
        text: "No",
        btnClass: 'btn-red',
        action: function () {

        }
      },
    }
  });

  $("#file_upload_alert").delay(1000 * 5).fadeOut();
});

//-------------------------------------------------//
// Common function 
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
function validate(email) {
  //alert(email);
  var result = $("#emails_error");
  result.text("");

  if (validateEmail(email)) {
    return true;
  } else {
    return false;
  }

}
function isNumberKey(evt) {
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 &&
    (charCode < 48 || charCode > 57))
    return false;

  return true;
}

maxScoring(currentFieldValue = '');
function maxScoring(currentFieldValue = '') {
  var totalScore = 0;
  var scoreExist = 0;
  $(".scoring_alert").hide();
  $('.score-criteria').each(function (index, currentElement) {
    totalScore = totalScore + parseFloat($(this).val());
    scoreExist = 1;
  });

  if (currentFieldValue == '') {
    totalScore = 0;
    $('.score-criteria').each(function (index, currentElement) {
      totalScore = totalScore + parseFloat($(this).val());
    });
  }
  $('.save_as_draft').prop("disabled", false);
  $('.save_as_publish').prop("disabled", false);
  if (scoreExist && totalScore < 100 || totalScore > 100) {
    var msg = "Sourcing criteria must not exceed 100%";
    var alert = totalScore > 100 ? msg : msg = "Scoring Criteria must add up to 100%";
    $('.scoring_alert').show();
    $('.scoring_alert').html(alert);
    $('.save_as_draft').prop("disabled", true);
    $('.save_as_publish').prop("disabled", true);
    return false;
  }
  return true;
}

$(document).ready(function () {
  $('#quote_items_table').DataTable({
    "columnDefs": [{
      "targets": 0,
    }],

    "createdRow": function (row, data, index) {
      if (data[0].indexOf('glyphicon glyphicon-ok') >= 0) {
        for (var i = 1; i <= 6; i++)
          $('td', row).eq(i).css('text-decoration', 'line-through');
      }

    },
    "order": [[1, "desc"]],
    "pageLength": 5,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": BICES.Options.baseurl + "/quotes/draftProductItemslistAjax",
      "type": "POST",
      data: function (input) {
        input.quote_id = $('#quote_id').val();
      }
    },
    "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      if (aData[1].search("<span class='deactivated_vendor'></span>") > 0) {
        $(nRow).addClass('deactivated_record');
      }
    }
  })

  // quote questionnaire 
  $(document).on('keyup', '.score-criteria', function () {
    var totalEnteredScore = 0;

    $('.score-criteria').each(function (index, currentElement) {
      score = parseFloat($(this).val());
      if (!isNaN(score)) {
        totalEnteredScore = totalEnteredScore + score;
      }
    });

    $(".scoring_percentage_alert").html(totalEnteredScore + "/100%");
    if (totalEnteredScore > 100) {
      $('.queestionnaire_submit').attr('disabled', 'disabled')
      $('.queestionnaire_submit').attr('disabled', true);
    } else if (totalEnteredScore < 100) {
      $('.queestionnaire_submit').attr('disabled', 'disabled')
      $('.queestionnaire_submit').attr('disabled', true);
    } else {
      $('.queestionnaire_submit').attr('disabled', '')
      $('.queestionnaire_submit').attr('disabled', false);
    }
  });

  $(".scoring_alert").hide();
  $(".scoring_alert2").hide();


});
maxScoring(currentFieldValue);
function maxScoring(currentFieldValue = '') {
  var totalScore = 0;
  $('.scoring_alert').hide();
  $('.score-criteria').each(function (index, currentElement) {
    totalScore = totalScore + parseFloat($(this).val());

    if (totalScore > 100) {
      $(this).val('');
      $('.scoring_alert').show();
      $('.scoring_alert').html("By entering this value " + currentFieldValue + ", the scoring criteria exceeds 100. Please try another value.");
      $(".queestionnaire_submit").attr('disabled', 'disabled');
      $(".queestionnaire_submit").prop('disabled', true);
      return false;
    }
  });

  if (currentFieldValue == '') {
    totalScore = 0;
    $('.score-criteria').each(function (index, currentElement) {
      totalScore = totalScore + parseFloat($(this).val());
    });

    if (totalScore < 100) {
      $('.scoring_alert').show();
      $('.scoring_alert').html("Scoring Criteria must add up to 100%");
      $(".queestionnaire_submit").attr('disabled', 'disabled');
      return false;
    }

    if (totalScore == 100) {
      $(".queestionnaire_submit").attr('disabled', '');
      $(".queestionnaire_submit").prop('disabled', false);
    }
  }

  return true;
}

function showAllContributorsAjax(){
    $.ajax({
      dataType: 'html',
      data : {quote_id : $('#quote_id').val()},
      url: BICES.Options.baseurl + "/quotes/getAllcontributors",
      success: function (data) {
        dataArr = JSON.parse(data)
        showAllContributors(dataArr);
      }
    });
}

$(document).ready(function(){  
  showAllContributorsAjax();
  
  $("#updateContributionDeadlineOpen").hide();
  $('#addContributionOpen').hide();
  $("#removeContribution").hide();

  $('#addContribution').click(() => {
    $("#removeContribution").show();
    $("#addContribution").hide();
    $('#addContributionOpen').show();
  });

  $('#removeContribution').click(() => {
    $("#addContribution").show();
    $("#removeContribution").hide();
    $('#addContributionOpen').hide();
  })

  $('#updateContributionDeadlineClose').hide();
  $('#updateContributionDeadlineshow').click(() => {
    $("#updateContributionDeadlineOpen").show();
    $('#updateContributionDeadlineClose').show()
    $('#updateContributionDeadlineshow').hide();
  });
  $('#updateContributionDeadlineClose').click(() => {
    $("#updateContributionDeadlineOpen").hide();
    $('#updateContributionDeadlineshow').show();
      $('#updateContributionDeadlineClose').hide();
  });

  $('.userCheckbox').change(function() {
    var isChecked = false;
    $('.userCheckbox').each(function() {
        if ($(this).is(':checked')) {
          isChecked = true;
        }
    });
    if (isChecked) {
        $('.contributor_submit').removeClass('disabled');
    } else {
        $('.contributor_submit').addClass('disabled');
    }
  });
});

const userIDs = [];
const selectedUserIds = [];
function showAllContributors(data) {

    for (let i = 0; i < data.length; i++) {
      userIDs.push(data[i].user_id);
    }

  const rows = data.map(value => `
  <tr>
    <td>${ value.full_name }</td>
    <td></td>
    <td class="text-center">
      ${value.contribute_status == '1' ? 
        `<button class="btn submit-btn1 btn-sm text-white" style="font-size: 9px;">Completed</button>` : 
        `<button class="btn sunshine btn-sm" style="font-size: 9px;">Not Completed</button>`
      }
    </td>
   </tr>`
  ).join('');
  $("#contributor_table").html(rows);
}
//   Add new users in select option 
function addMoreInternalUsers() {
  $('.add-more-users').show();
}









