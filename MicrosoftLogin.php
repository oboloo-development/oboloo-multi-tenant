<?php
// Define Azure AD application credentials
$appId = "67fdd7fb-c82d-40dd-96c5-497e0b9da824";
$tenantId = "739bfa56-7473-4edd-a605-3b632dfd403f";
$secret = "gGP8Q~jPbQrjp-Bh.j6qd1vjd56m1XDyeVJ6Ub0R";

// Define Azure AD endpoints
$authorizationEndpoint = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize";
$graphApiEndpoint = "https://graph.microsoft.com/v1.0/me/";

// Start session if not already started
if (session_status() == PHP_SESSION_NONE) { session_start(); }

// Check if 'logintese' action is triggered
if (isset($_GET['action']) && $_GET['action'] == 'logintese') {
    // Get the current domain from the request, default to empty string if not provided
    $currentDomain = $_GET['currentDomain'] ?? '';
	$_SESSION['currentDomain'] = $currentDomain;
    // Construct the redirect URL with current domain parameter
    $redirectUrl = "https://oboloo.software/microsoftLogin.php";

    // Define OAuth 2.0 parameters
    $params = [
        'client_id' => $appId,
        'redirect_uri' => $redirectUrl,
        'response_type' => 'token',
        'response_mode' => 'form_post',
        'client_secret' => $secret,
        'scope' => 'https://graph.microsoft.com/User.Read',
        'state' => json_encode([session_id(), $currentDomain]),
    ];

    // Redirect user to the authorization endpoint with OAuth parameters
    header('Location: ' . $authorizationEndpoint . '?' . http_build_query($params));
    exit;
}

// Check if access token is received via POST request
if (isset($_POST['access_token'])) {
    // Store the access token in session
    $_SESSION['access_token'] = $_POST['access_token'];

    // Get the serialized state from the request, default to null if not provided
    $serializedState = $_POST['state'] ?? null;

    // Decode the serialized state to retrieve session ID and current domain
    [$sessionId, $currentDomain] = json_decode($serializedState, true);

    // Retrieve user information from Microsoft Graph API using the access token
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $_SESSION['access_token'], 'Content-type: application/json']);
    curl_setopt($ch, CURLOPT_URL, $graphApiEndpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);

    // Decode the response JSON
    $userData = json_decode($response, true);
    // // Redirect user to the appropriate page based on the retrieved data
    if (!empty($currentDomain)) {
        $email = base64_encode($userData["mail"]);
        $redirectUrl = "https://$currentDomain.oboloo.software/app/microsoftAutoLogin?action=$email";
        header('Location: ' . $redirectUrl);
        exit;
    } else {
        $redirectUrl = "https://".$_SESSION['currentDomain'].".oboloo.software/login";
        header('Location: ' . $redirectUrl);
        exit;
    }
}

// If no access token is received, redirect user to the login page
$redirectUrl = "https://".$_SESSION['currentDomain'].".oboloo.software/login";
header('Location: ' . $redirectUrl);
exit;
?>
