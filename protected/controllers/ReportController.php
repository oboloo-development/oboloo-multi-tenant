<?php

require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');

class ReportController extends CommonController
{
	public $layout = 'main';

	public function actionQuoteSupplier()
	{  
		 $data = array('vendorFields'=>0,'vendorFieldsCompleted'=>0,'supplierFieldsPercentage'=>0,'vendordocumentApproved'=>0,'vendorDocuments'=>0,'vendorDocApprovedPercentage'=>0);

		/*$sql = "SELECT q.quote_name, q.quote_currency, q.quote_id as q_id,v.vendor_name, sum((qd.unit_price*qd.quantity)+qd.shipping+qd.other_charges) as total_amount FROM (select * from quotes order by quote_id desc) as q 

		inner join (select quote_vendor_details.*,sum((unit_price*quantity)+shipping+other_charges) as min_total_amount,vendor_id,quote_id from quote_vendor_details group by vendor_id,quote_id order by min_total_amount asc) as qd on q.quote_id = qd.quote_id

		inner join vendors as v on v.vendor_id = qd.vendor_id 

		group by q_id,qd.vendor_id HAVING total_amount = (select min(min_total_amount) as min_total_amount from quote_vendor_details where quote_id=q_id group by vendor_id order by min_total_amount asc limit 1)  limit 20";*/

		$sql = "SELECT q.quote_name, q.quote_currency, q.quote_id as q_id,v.vendor_id,v.vendor_name, min(min_total_amount) as total_amount,qd.min_total_amount FROM (select * from quotes order by quote_id desc) as q 

		  inner join (select quote_vendor_details.*,sum((unit_price*quantity)+shipping+other_charges) as min_total_amount from quote_vendor_details group by vendor_id, quote_id order by min_total_amount asc) as qd on q.quote_id = qd.quote_id 

		  inner join vendors as v on v.vendor_id = qd.vendor_id group by qd.quote_id HAVING min_total_amount = min(min_total_amount) order by total_amount asc limit 10 ";



		$quoteReader = Yii::app()->db->createCommand($sql)->query()->readAll();

		$qoute_series = array();
		$qoute_label = array();
		$qoute_vendor = array();
		$quote_currency = array();
		foreach($quoteReader as $value){
			$qoute_series[]   = $value['total_amount'];
			$qoute_label[]    = $value['quote_name'];
			$qoute_vendor[]   =  $value['vendor_name'];
			$quote_currency[] = $value['quote_currency'];
			
		}
		
		$data['qoute_series']   = $qoute_series;
		$data['qoute_label']    = $qoute_label;
		$data['qoute_vendor']   = $qoute_vendor;
		$data['quote_currency'] = $quote_currency;

		$days30         = strtotime('+ 30 days');
	    $months3        = strtotime('+ 3 months');
	    $dateDays30     = date('Y-m-d', $days30);
	    $months3        = date('Y-m-d', $months3);
	    $currentDate    = date("Y-m-d");

	    $report = new Report;
	    $contractCount = $report->contractByStatus();

	    $data['contract_count']   = $contractCount['contract_count'];
    	$data['contract_label']   = $contractCount['contract_label'];
    	$data['contract_color']   = $contractCount['contract_color'];


		// Start: Contract approved document
		$sql = "select sum(CASE WHEN status = 'Approved' THEN 1 ELSE 0 END) as total_approved,sum(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END) as total_pending from contract_documents";
        $contractDoc = Yii::app()->db->createCommand($sql)->queryRow();
         if(!empty($contractDoc['total_approved']) || !empty($contractDoc['total_pending'])){
            $data['documentApproved'] = $contractDoc['total_approved'];
            $data['totalDocuments']   = $contractDoc['total_approved']+$contractDoc['total_pending'];
            $data['documentApprovedPercentage'] =  number_format($data['documentApproved']/ $data['totalDocuments']*100,0);
        }else{
        	$data['documentApprovedPercentage'] = 0;
        }

		// End: Contract approved document

		// Start: Suppliers fields completed

		

          $sql = "select ((vendor_name !='')+(contact_name !='')+(phone_1 !='')+(phone_2 !='')++(external_id !='')+(profile_status !='')+(industry_id !=0)+(subindustry_id !=0)+(number !='')+(tax_number !='')+(preferred_flag !=0)+(address_1 !='')+(address_1 !='')+(address_2 !='')+(city !='')+(state !='')+(country !='')+(zip !='')+(comments !='')+(payment_term_id !=0)+(shipping_term_id !=0)+(shipping_method_id !=0))  as total_completed_fields from vendors";
         $vendorFields = Yii::app()->db->createCommand($sql)->queryRow();
          if(!empty($vendorFields['total_completed_fields'])){
            $data['vendorFieldsCompleted'] = $vendorFields['total_completed_fields'];
            $data['vendorFields']     = 22;
            $data['supplierFieldsPercentage'] = number_format($data['vendorFieldsCompleted']/$data['vendorFields']*100,0);
        }
		// End: Suppliers fields completed

        // Start: Suppliers document approved

		$sql = "select sum(CASE WHEN status = 'Approved' THEN 1 ELSE 0 END) as total_approved,sum(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END) as total_pending from vendor_documents";
        $vendorDocument = Yii::app()->db->createCommand($sql)->queryRow();
         if(!empty($vendorDocument['total_approved']) || !empty($vendorDocument['total_pending'])){
            $data['vendordocumentApproved'] = $vendorDocument['total_approved'];
            $data['vendorDocuments'] = $vendorDocument['total_approved']+$vendorDocument['total_pending'];
            $data['vendorDocApprovedPercentage'] =  number_format($data['vendordocumentApproved']/ $data['vendorDocuments']*100,0);
        }

        // End: Suppliers document approved

		echo json_encode($data);
    	exit;
 
		
		//INNER JOIN quote_vendor_details ON vendors.vendor_id=quote_vendor_details.vendor_id
		
	}

	public function actionDashboardContracts()
	{  

	    $days30         = strtotime('+ 30 days');
	    $months3         = strtotime('+ 3 months');
	    $dateDays30     = date('Y-m-d', $days30);
	    $months3     = date('Y-m-d', $months3);
	    $currentDate    = date("Y-m-d");

		$sql = "select 
			SUM(CASE 
			 WHEN contract_end_date<'".$currentDate."' THEN 1
             ELSE 0
            END) AS 'Expired',
            SUM(CASE 
             WHEN contract_end_date>'".$months3."' THEN 1
             ELSE 0
            END) AS 'Active',
            SUM(CASE 
             WHEN contract_end_date>='".$currentDate."' and contract_end_date<='".$dateDays30."' THEN 1
             ELSE 0
            END) AS 'Expire in 30 days',
             SUM(CASE 
             WHEN contract_end_date>'".$dateDays30."' and contract_end_date<='".$months3."' THEN 1
             ELSE 0
            END) AS 'Expire in 3 months'
            from contracts ";

		$dataReader = Yii::app()->db->createCommand($sql)->queryRow();

		$data = array();
		$data['contract_count']   = $dataReader;
		$data['contract_label']   = array_keys($dataReader);
		echo json_encode($data);
    	
		
	}
}

