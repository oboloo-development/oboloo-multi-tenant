<?php

class BundlesController extends CommonController
{

    public $layout = 'main';

    public function actionIndex()
    {
        $this->redirect(AppUrl::bicesUrl('bundles/list'));
    }

    public function actionList()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'bundles_list';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);
        
        if (! isset(Yii::app()->session['user_type']) || Yii::app()->session['user_type'] != 1)
            $this->redirect(AppUrl::bicesUrl('orders/list'));
        
        // get list of all bundles (with pagination of course)
        $bundle = new Bundle();
        
        // and display
        $view_data = array();
        $view_data['bundles'] = $bundle->getBundles();
        $this->render('list', $view_data);
    }

    public function actionEdit($bundle_id = 0)
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'bundles_edit';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);
        
        if (! isset(Yii::app()->session['user_type']) || Yii::app()->session['user_type'] != 1)
            $this->redirect(AppUrl::bicesUrl('orders/list'));
        
        // if form was submitted ... first save the data to the database
        if (Yii::app()->input->post('form_submitted')) {
            $bundle = new Bundle();
            if (Yii::app()->input->post('active')) {
                $bundle->fields['active'] = 'N';
                $override_data['active'] = 1;
                $bundle->saveData($override_data);
            } else
                $bundle->saveData();
            
            $bundle_product = new BundleProduct();
            $bundle_product->delete(array(
                'bundle_id' => $bundle->rs['bundle_id']
            ));
            
            $product_id = Yii::app()->input->post('product_id');
            $quantity = Yii::app()->input->post('quantity');
            
            for ($idx = 0; $idx <= count($product_id); $idx ++) {
                $bundle_detail = array(
                    'id' => 0,
                    'bundle_id' => $bundle->rs['bundle_id']
                );
                $bundle_detail['product_id'] = isset($product_id[$idx]) ? $product_id[$idx] : 0;
                $bundle_detail['quantity'] = isset($quantity[$idx]) ? $quantity[$idx] : 1;
                if (! empty($bundle_detail['product_id']))
                    $bundle_product->saveData($bundle_detail);
            }
            
            $this->redirect(AppUrl::bicesUrl('bundles/edit/' . $bundle->rs['bundle_id']));
        }
        
        // get data for the selected bundle
        $view_data = array(
            'bundle_id' => $bundle_id
        );
        $bundle = new Bundle();
        $bundle_data = $bundle->getBundles($bundle_id);
        if (isset($bundle_data[$bundle_id]) && is_array($bundle_data[$bundle_id]))
            $view_data['bundle'] = $bundle_data[$bundle_id];
        
        // and display
        $this->render('edit', $view_data);
    }

    public function actionGetProducts()
    {
        $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
        $products = array();
        
        $product = new Product();
        $search_results = $product->getAll(array(
            'product_name LIKE' => '%' . $search_string . '%',
            'ORDER' => 'product_name'
        ));
        
        foreach ($search_results as $product_data)
            $products[] = array(
                "value" => $product_data['product_name'],
                "data" => $product_data['product_id']
            );
        
        echo json_encode(array(
            'suggestions' => $products
        ));
    }

    public function actionUpdateItemStatus()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        
        // only admin can delete item
        if (Yii::app()->session['user_type'] == 1) {
            $delete_item_id = isset($_REQUEST['delete_item_id']) ? $_REQUEST['delete_item_id'] : "";
            $active_flag = isset($_REQUEST['active_flag']) ? $_REQUEST['active_flag'] : 0;
            
            if ($delete_item_id) {
                $bundle = new Bundle();
                $bundle->rs = array();
                $bundle->rs['bundle_id'] = $delete_item_id;
                $bundle->rs['active'] = $active_flag;
                $bundle->write();
            }
        }
    }

    public function actionExport()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $bundle = new Bundle();
        $bundle->export();
    }

    public function actionDeleteItemStatus()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        
        $delete_item_id = isset($_REQUEST['delete_item_id']) ? $_REQUEST['delete_item_id'] : "";
        
        if ($delete_item_id) {
            $product = new Bundle();
            $product->rs = array();
            $product->rs['bundle_id'] = $delete_item_id;
            $product->rs['status'] = '0';
            $product->write();
            return 'OK';
        }
        
        
//         if ((new Bundle())->delete([
//             'bundle_id' => $_POST['delete_item_id']
//         ])) {
//             return 'OK';
//         }
        // return $data;
    }
}
