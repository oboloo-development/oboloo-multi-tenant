<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');
class ExpensesController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('expenses/list'));
	}

	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'expenses_list';

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		// get list of all expenses (with pagination of course)
		$expense = new Expense();
		// get expenses as per filters applied
		$view_data = array();
		$location_id = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$expense_status = Yii::app()->input->post('expense_status');
		
		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');

		$db_from_date = "";
		if (!empty($from_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $from_date);
	       	$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['from_date'] = $db_from_date;
		}

		$db_to_date = "";
		if (!empty($to_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $to_date);
	       	$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['to_date'] = $db_to_date;
		}

		if(!empty($department_id)){
			$location = new Location();
			$department_info = $location->getDepartments($location_id);
		}else
		$department_info='';
		
		// need filters too
		$location = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;
		$view_data['department_info'] = $department_info;
		// and display
		$view_data['department_id'] = $department_id;
		$view_data['expense_status'] = $expense_status;
		$view_data['expenses'] = $expense->getExpenses(0, $location_id, $department_id, $expense_status, $db_from_date, $db_to_date);
		$this->render('list', $view_data);
	}

	public function actionEdit($expense_id = 0)
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'expenses_edit';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);


		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted'))
		{
			//echo "<pre>"; print_r($_POST);exit;
			$files_only_upload = Yii::app()->input->post('files_only_upload');

			if(!empty( Yii::app()->input->post('total_expenses'))){
				$override_data['calc_expenses'] = Yii::app()->input->post('total_expenses')/Yii::app()->input->post('currency_rate');
			}


			// save actual expense data
			$expense = new Expense();
			if (!$files_only_upload) 
			{  
				$expense->saveData($override_data);
				$expense_id = $expense->rs['expense_id'];
			}
			else {$expense_id = Yii::app()->input->post('expense_id'); }
			
			// receipts if any were uploaded
//			if (isset($_FILES['file']) && is_array($_FILES['file']) && count($_FILES['file']))
//			{
//				if (!is_dir('uploads/expenses')) mkdir('uploads/expenses');
//				if (!is_dir('uploads/expenses/' . $expense_id))
//					mkdir('uploads/expenses/' . $expense_id);
//
//				for ($idx=0; $idx<count($_FILES['file']['name']); $idx++)
//				{
//					$receipt_file = array();
//					$receipt_file['name'] = $_FILES['file']['name'][$idx];
//					$receipt_file['type'] = $_FILES['file']['type'][$idx];
//					$receipt_file['tmp_name'] = $_FILES['file']['tmp_name'][$idx];
//					$receipt_file['error'] = $_FILES['file']['error'][$idx];
//					$receipt_file['size'] = $_FILES['file']['size'][$idx];
//
//					if (!isset($receipt_file['error']) || empty($receipt_file['error']))
//					{
//			            $file_name = $receipt_file['name'];
//			            move_uploaded_file($receipt_file['tmp_name'], 'uploads/expenses/' . $expense_id . '/' . $file_name);
//					}
//				}
//			}
			$uploadedFiles = Yii::app()->session['uploaded_files'];
			$total_documents = Yii::app()->input->post('total_documents');
			if(!empty($uploadedFiles))
			foreach ($uploadedFiles as $fileValue){
				/*$delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idx);
				if ($delete_document_flag) continue;*/
				if (empty($fileValue['expense_id'])){
					$file_name = $fileValue['file_name'];
					$file_description = $fileValue['file_description'];
					if (!is_dir('uploads/expenses')) mkdir('uploads/expenses');
					if (!is_dir('uploads/expenses/' . $expense_id))
						mkdir('uploads/expenses/' . $expense_id);

					if(@rename('uploads/expenses/' . $file_name, 'uploads/expenses/' . $expense_id . '/' . $file_name)){
						$expense->saveExpenseFile($expense_id, $file_name, $file_description);
					}
				}
			}

			if (!$files_only_upload)
			{ 
				// expense types in the request
				$expense_detail = new ExpenseDetail();
				$expense_detail->delete(array('expense_id' => $expense->rs['expense_id']));
	
				$expense_type_ids = Yii::app()->input->post('expense_type_id');
				$expense_dates = Yii::app()->input->post('expense_date');
				$expense_notes = Yii::app()->input->post('expense_notes');
				$expense_prices = Yii::app()->input->post('expense_price');
				$vendor_ids = Yii::app()->input->post('vendor_id');
				$vendor_names = Yii::app()->input->post('vendor_name');
				$tax_rates = Yii::app()->input->post('tax_rate');
	
				for ($idx=0; $idx<=count($expense_type_ids); $idx++)
				{
					$expense_data = array('id' => 0, 'expense_id' => $expense->rs['expense_id']);
					$expense_data['expense_type_id'] = isset($expense_type_ids[$idx]) ? $expense_type_ids[$idx] : 0;
					$expense_data['expense_date'] = isset($expense_dates[$idx]) ? $expense_dates[$idx] : "";
					$expense_data['expense_notes'] = isset($expense_notes[$idx]) ? $expense_notes[$idx] : "";
					$expense_data['expense_price'] = isset($expense_prices[$idx]) ? $expense_prices[$idx] : 0;
					$expense_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
					$expense_data['tax_rate'] = isset($tax_rates[$idx]) ? $tax_rates[$idx] : 0;
					
					if (empty($expense_data['vendor_id']))
					{
						$vendor_name = isset($vendor_names[$idx]) ? $vendor_names[$idx] : "";
						$vendor = new Vendor();
						$expense_data['vendor_id'] = $vendor->getIdForName($vendor_name);
					}
	
					if (!empty($expense_data['expense_type_id']) && !empty($expense_data['expense_price']))
						$expense_detail->saveData($expense_data);
				}
	
				// and then routing information for approvals too
				if ($expense->rs['expense_status'] == 'Submitted')
				{
					$approval_route_created = $expense->createRouteForApproval($expense->rs['expense_id']);
					if (!$approval_route_created)
					{
						// do not submit the expense if no approval route found
						$non_submitted_expense = new Expense();
						$non_submitted_expense->rs = array();
						$non_submitted_expense->rs['expense_id'] = $expense->rs['expense_id'];
						$non_submitted_expense->rs['expense_status'] = 'Pending';
						$non_submitted_expense->write();
						
						// let the user know what happened
						Yii::app()->session['expense_status_changed_id'] = $expense->rs['expense_id'];
					}
				}
			}

			if(Yii::app()->request->isAjaxRequest){
				echo "Expense submitted";
				exit;
			}else{
				if (!$files_only_upload)
					$this->redirect(AppUrl::bicesUrl('expenses/edit/' . $expense->rs['expense_id']));
				else
					$this->redirect(AppUrl::bicesUrl('expenses/edit/' . $expense_id));
			}
		}

		// get data for the selected vendor
		$view_data = array('expense_id' => $expense_id);
		$expense = new Expense();
		$view_data['expense'] = $expense->getExpenses($expense_id);
		if (!$view_data['expense'] || !is_array($view_data['expense'])) $expense_id = 0;

		if ($expense_id && !isset(Yii::app()->session['user_type']) || in_array(Yii::app()->session['user_type'],array(1,2,3,4)))
		{
			//
		}else
		if ($expense_id && (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4))))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if ($view_data['expense']['user_id'] != $my_user_id
					&& $view_data['expense']['created_by_user_id'] != $my_user_id)
				$this->redirect(AppUrl::bicesUrl('expenses/list/'));
		}

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));
	
		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));
	
		$expense_type = new ExpenseType();
		$view_data['expense_types'] = $expense_type->getData(array('order' => 'value'));

		// other drop downs may be needed for vendor quick add modal window
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));
			
		// and also detail information for the expense
		$expense_detail = new ExpenseDetail();
		$view_data['expense_details'] = $expense_detail->getExpenseDetails($expense_id);
		

		// and finally approvals if any
		if ($expense_id)
		{
			$route = new OrderRoute();
			/*$view_data['approvals'] = $route->getApprovalRoute('E', $expense_id);*/
			$view_data['approvals'] = $route->getApprovalRouteE($view_data['expense']);
		}
		else $view_data['approvals'] = array();
		unset(Yii::app()->session['uploaded_files']);
		// and display
		$this->render('edit', $view_data);
		$this->renderPartial('/vendors/modal_add', $view_data);
	}

	public function actionCreateByModal(){

		// is someone already logged in?
		error_reporting(0);
		// is someone already logged in?
		$this->checklogin();
		$this->current_option = 'expenses_edit';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		$expense_id = 0;
		// get data for the selected vendor
		$view_data = array('expense_id' => $expense_id);
		$expense = new Expense();
		$view_data['expense'] = $expense->getExpenses($expense_id);
		if (!$view_data['expense'] || !is_array($view_data['expense'])) $expense_id = 0;

		if ($expense_id && !isset(Yii::app()->session['user_type']) || in_array(Yii::app()->session['user_type'],array(1,2,3,4)))
		{
			//
		}else
		if ($expense_id && (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4))))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if ($view_data['expense']['user_id'] != $my_user_id
					&& $view_data['expense']['created_by_user_id'] != $my_user_id)
				$this->redirect(AppUrl::bicesUrl('expenses/list/'));
		}

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));
	
		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));
	
		$expense_type = new ExpenseType();
		$view_data['expense_types'] = $expense_type->getData(array('order' => 'value'));

		// other drop downs may be needed for vendor quick add modal window
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));
			
		// and also detail information for the expense
		$expense_detail = new ExpenseDetail();
		$view_data['expense_details'] = $expense_detail->getExpenseDetails($expense_id);
		

		// and finally approvals if any
		if ($expense_id)
		{
			$route = new OrderRoute();
			/*$view_data['approvals'] = $route->getApprovalRoute('E', $expense_id);*/
			$view_data['approvals'] = $route->getApprovalRouteE($view_data['expense']);
		}
		else $view_data['approvals'] = array();
		unset(Yii::app()->session['uploaded_files']);
		$this->renderPartial('create_modal_expense', $view_data);
	
		$this->renderPartial('/vendors/modal_add', $view_data);	exit;
		
	

	}

	public function actionUploadDocument()
	{  error_reporting(0);
		if (!empty($_FILES)){
			$expense = new Expense;
			$total_documents = Yii::app()->input->post('total_documents');
			if (isset($_FILES['file']) && is_array($_FILES['file'])){
				$expense_id = Yii::app()->input->post('expense_id');
				if(empty($expense_id)){
					$expense_id ='';
				}
				if (!is_dir('uploads/expenses')) mkdir('uploads/expenses');
				if (!is_dir('uploads/expenses/' . $expense_id))
					mkdir('uploads/expenses/' . $expense_id);
				if (!is_dir('uploads/expenses/'))
					mkdir('uploads/expenses/');

				if (!isset($_FILES['file']['error']) || empty($_FILES['file']['error']))
				{
					$file_name = $_FILES['file']['name'];
					$file_description = Yii::app()->input->post('file_description');
					$uploadedFiles = Yii::app()->session['uploaded_files'];

					$uploadedFiles[] = array('file_name'=>$file_name,'file_description'=>$file_description,'expense_id'=>$expense_id);

					Yii::app()->session['uploaded_files'] = $uploadedFiles;
					//move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/expenses/' . $file_name);
					if(move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/expenses/' . (!empty($expense_id)?$expense_id. '/'. $file_name: $file_name)) && !empty($expense_id)){
						$expense->saveExpenseFile($expense_id, $file_name, $file_description);
					}
					$display = '<table class="table"><tbody>';
					foreach(Yii::app()->session['uploaded_files'] as $key=>$value){
						$display .= '<tr><td>'.$value['file_name'].'</td><td>'.$value['file_description'].'</td><td><span class="glyphicon glyphicon-trash" aria-hidden="true"  style="cursor:grab"  onclick="(confirm(\'Are you sure?\')?deleteSessionDocument('.$key.'):\'\')"></span></td></tr>';	
					}
					$display .= '</tbody></table>';
				}
				echo $display;exit;
			}
		}
	}

	public function actionDeleteSessionDocument()
	{  error_reporting(0);
		if (isset($_POST['IDX'])){
			$expense = new Expense;
				
			$uploadedFiles = Yii::app()->session['uploaded_files'];

			$delFile = $uploadedFiles[$_POST['IDX']];
			// which expense record the file relates to? 
			$_POST['file_name'] = $delFile['file_name'];
			$_POST['expense_id'] = $delFile['expense_id'];
			$file_name = Yii::app()->input->post('file_name');
			$this->actionDeleteFile();
			unset($uploadedFiles[$_POST['IDX']]);


			//$uploadedFiles[] = array('file_name'=>$file_name,'file_description'=>$file_description,'expense_id'=>$expense_id);

			Yii::app()->session['uploaded_files'] = $uploadedFiles;
			
			$display = '<table class="table"><tbody>';
			foreach(Yii::app()->session['uploaded_files'] as $key=>$value){
				$display .= '<tr><td>'.$value['file_name'].'</td><td>'.$value['file_description'].'</td><td><span class="glyphicon glyphicon-trash" aria-hidden="true" style="cursor:grab" onclick="(confirm(\'Are you sure?\')?deleteSessionDocument('.$key.'):\'\')"></span></td></tr>';	
			}
			$display .= '</tbody></table>';
			echo $display;exit;
		}
	}

	public function actionClone($expense_id = 0)
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'expenses_edit';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);


		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted'))
		{
			//echo "<pre>"; print_r($_POST);exit;
			$files_only_upload = Yii::app()->input->post('files_only_upload');

			if(!empty( Yii::app()->input->post('total_expenses'))){
				$override_data['calc_expenses'] = Yii::app()->input->post('total_expenses')/Yii::app()->input->post('currency_rate');
			}


			// save actual expense data
			$expense = new Expense();
			if (!$files_only_upload) 
			{   
				$expense->saveData($override_data);
				$expense_id = $expense->rs['expense_id'];
			}
			else {$expense_id = Yii::app()->input->post('expense_id'); }
			$total_documents = Yii::app()->input->post('total_documents');
			for ($idx=1; $idx<=$total_documents; $idx++)
			{
				$delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idx);
				if ($delete_document_flag) continue;

				if (isset($_FILES['expense_file_' . $idx]) && is_array($_FILES['expense_file_' . $idx]))
				{
					if (!is_dir('uploads/expenses')) mkdir('uploads/expenses');
					if (!is_dir('uploads/expenses/' . $expense_id))
						mkdir('uploads/expenses/' . $expense_id);

					if (!isset($_FILES['expense_file_' . $idx]['error']) || empty($_FILES['expense_file_' . $idx]['error']))
					{
						$file_name = $_FILES['expense_file_' . $idx]['name'];
						move_uploaded_file($_FILES['expense_file_' . $idx]['tmp_name'], 'uploads/expenses/' . $expense_id . '/' . $file_name);

						$file_description = Yii::app()->input->post('file_desc_' . $idx);
						$expense->saveExpenseFile($expense_id, $file_name, $file_description);
					}
				}
			}

			if (!$files_only_upload)
			{ 
				// expense types in the request
				$expense_detail = new ExpenseDetail();
				//$expense_detail->delete(array('expense_id' => $expense->rs['expense_id']));
	
				$expense_type_ids = Yii::app()->input->post('expense_type_id');
				$expense_dates = Yii::app()->input->post('expense_date');
				$expense_notes = Yii::app()->input->post('expense_notes');
				$expense_prices = Yii::app()->input->post('expense_price');
				$vendor_ids = Yii::app()->input->post('vendor_id');
				$vendor_names = Yii::app()->input->post('vendor_name');
				$tax_rates = Yii::app()->input->post('tax_rate');
	
				for ($idx=0; $idx<=count($expense_type_ids); $idx++)
				{
					$expense_data = array('id' => 0, 'expense_id' => $expense->rs['expense_id']);
					$expense_data['expense_type_id'] = isset($expense_type_ids[$idx]) ? $expense_type_ids[$idx] : 0;
					$expense_data['expense_date'] = isset($expense_dates[$idx]) ? $expense_dates[$idx] : "";
					$expense_data['expense_notes'] = isset($expense_notes[$idx]) ? $expense_notes[$idx] : "";
					$expense_data['expense_price'] = isset($expense_prices[$idx]) ? $expense_prices[$idx] : 0;
					$expense_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
					$expense_data['tax_rate'] = isset($tax_rates[$idx]) ? $tax_rates[$idx] : 0;
					
					if (empty($expense_data['vendor_id']))
					{
						$vendor_name = isset($vendor_names[$idx]) ? $vendor_names[$idx] : "";
						$vendor = new Vendor();
						$expense_data['vendor_id'] = $vendor->getIdForName($vendor_name);
					}
	
					if (!empty($expense_data['expense_type_id']) && !empty($expense_data['expense_price']))
						$expense_detail->saveData($expense_data);
				}
	
				// and then routing information for approvals too
				if ($expense->rs['expense_status'] == 'Submitted')
				{
					$approval_route_created = $expense->createRouteForApproval($expense->rs['expense_id']);
					if (!$approval_route_created)
					{
						// do not submit the expense if no approval route found
						$non_submitted_expense = new Expense();
						$non_submitted_expense->rs = array();
						$non_submitted_expense->rs['expense_id'] = $expense->rs['expense_id'];
						$non_submitted_expense->rs['expense_status'] = 'Pending';
						$non_submitted_expense->write();
						
						// let the user know what happened
						Yii::app()->session['expense_status_changed_id'] = $expense->rs['expense_id'];
					}
				}
			}
			if (!$files_only_upload)
				$this->redirect(AppUrl::bicesUrl('expenses/edit/' . $expense->rs['expense_id']));
			else
				$this->redirect(AppUrl::bicesUrl('expenses/edit/' . $expense_id));
		}

		// get data for the selected vendor
		$view_data = array('expense_id' => $expense_id);
		$expense = new Expense();
		$view_data['expense'] = $expense->getExpenses($expense_id);
		if (!$view_data['expense'] || !is_array($view_data['expense'])) $expense_id = 0;

		if ($expense_id && !isset(Yii::app()->session['user_type']) || in_array(Yii::app()->session['user_type'],array(1,2,3,4)))
		{
			//
		}else
		if ($expense_id && (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4))))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if ($view_data['expense']['user_id'] != $my_user_id
					&& $view_data['expense']['created_by_user_id'] != $my_user_id)
				$this->redirect(AppUrl::bicesUrl('expenses/list/'));
		}

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));
	
		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));
	
		$expense_type = new ExpenseType();
		$view_data['expense_types'] = $expense_type->getData(array('order' => 'value'));

		// other drop downs may be needed for vendor quick add modal window
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));
			
		// and also detail information for the expense
		$expense_detail = new ExpenseDetail();
		$view_data['expense_details'] = $expense_detail->getExpenseDetails($expense_id);
		

		// and finally approvals if any
		if ($expense_id)
		{
			$route = new OrderRoute();
			/*$view_data['approvals'] = $route->getApprovalRoute('E', $expense_id);*/
			$view_data['approvals'] = $route->getApprovalRouteE($view_data['expense']);
		}
		else $view_data['approvals'] = array();

		// and display
		$this->render('clone', $view_data);
		$this->renderPartial('/vendors/modal_add', $view_data);
	}


	public function actionChangeStatus()
	{
		
		if(!empty($_POST['expense_id'])){
		 	$status = $_POST['expense_status'];
		 	$expense_id = $_POST['expense_id'];
            $sql = "UPDATE expenses SET expense_status = '".$status."' WHERE expense_id =".$expense_id;
            if(Yii::app()->db->createCommand($sql)->execute()){
            	Yii::app()->user->setFlash('success', "Cancelled Successfully");
            	$this->redirect(AppUrl::bicesUrl('expenses/list'));
            }else {
            	Yii::app()->user->setFlash('error', "There is a problem, try again or contact to administrator");
            	$this->redirect(AppUrl::bicesUrl('expenses/edit/' . $expense_id));
            }
            	
        }
	}

	public function actionDeleteFile()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$expense = new Expense();

		// which expense record the file relates to?
		$expense_id = Yii::app()->input->post('expense_id');
		$file_name = Yii::app()->input->post('file_name');
		
		// directory where this file resides
		if(!empty($expense_id)){
			$complete_file_name = 'uploads/expenses/' . $expense_id . '/' . $file_name;
			if (is_file($complete_file_name)) @unlink($complete_file_name);
			$expense->deleteFile($expense_id, $file_name);
		}else{
			$complete_file_name = 'uploads/expenses/' . $file_name;
			if (is_file($complete_file_name)) @unlink($complete_file_name);
		}
	}

	public function actionExport()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$expense = new Expense();
		$expense->export();
	}


}
