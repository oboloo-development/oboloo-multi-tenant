<?php

require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');

class QuotesController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('quotes/list'));
	}
	public function actionList()
	{
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		unset(Yii::app()->session['quick_vendor']);
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_list';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);

		// get list of all orders (with pagination of course)
		$view_data = array();
		$user = new User();
		$userData = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

		$view_data['quote_visit'] = $userData['quote_visit'];
		if ($view_data['quote_visit'] != 1) {
			$user->rs = array();
			$user->rs['user_id'] = $userData['user_id'];
			$user->rs['quote_visit'] = 1;
			$user->write();
		}
		$quote = new Quote();

		$location_id = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$quote_status = Yii::app()->input->post('quote_status');
		$category_id = Yii::app()->input->post('category_id');
		$subcategory_id = Yii::app()->input->post('subcategory_id');

		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');

		$db_from_date = "";
		if (!empty($from_date)) {
			if (FunctionManager::dateFormat() == "d/m/Y") {
				list($input_date, $input_month, $input_year) = explode("/", $from_date);
				$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
			} else {
				$db_from_date = date("Y-m-d", strtotime($from_date));
			}
			$view_data['from_date'] = $db_from_date;
		}

		$db_to_date = "";
		if (!empty($to_date)) {
			if (FunctionManager::dateFormat() == "d/m/Y") {
				list($input_date, $input_month, $input_year) = explode("/", $to_date);
				$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
			} else {
				$db_to_date = date("Y-m-d", strtotime($to_date));
			}
			$view_data['to_date'] = $db_to_date;
		}

		if (!empty($department_id)) {
			$location = new Location();
			//$department_selected = implode(',',$department_id);

			//$sqlCond = " department_id IN ($department_selected)";
			//$sql = "SELECT * FROM departments WHERE ".$sqlCond." ORDER BY department_name";
			//$department_selected = Yii::app()->db->createCommand($sql)->query()->readAll();
			$department_info = $location->getDepartments($location_id);
		} else
			$department_info = '';

		// need filters too
		$location = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;

		// and display
		$view_data['department_id'] = $department_id;
		$view_data['department_info'] = $department_info;
		$view_data['quote_status'] = $quote_status;
		$view_data['category_id'] = $category_id;
		$view_data['subcategory_id'] = $subcategory_id;

		$view_data['quotes'] = $quote->getQuotes($location_id, $department_id, $category_id, $subcategory_id, $quote_status, $db_from_date, $db_to_date);

		$notification = new Notification();
		$view_data['notifications'] = $notification->getLastestForQuotes(10);
		$category = new Category();
		$view_data['categories'] = $category->getAll();
		if (!empty($category_id)) {
			$subcategory = new Subcategory();
			$view_data['subcategory'] = $subcategory->getAll(array('code' => $category_id));
		} else {
			$view_data['subcategory'] = '';
		}

		$view_data['location_quote_count'] =  $quote->getLocationQuote();
		$view_data['inDraftMetric'] = $quote->getQuoteMetric('in_draft');
		$view_data['inProgressMetric'] = $quote->getQuoteMetric('in_progress');
		$view_data['completedMetric']  = $quote->getQuoteMetric('completed');
		$view_data['awardedMetric'] = $quote->getQuoteMetric('awarded');
		$view_data['baseLinetotalByYear'] = $quote->baseLinetotalByYear();
		

		$this->render('list', $view_data);
	}


	public function actionListAjax()
	{
		$quote = new Quote();
		// get orders as per filters applied
		$location_id = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$quote_status = '';
		$category_id = Yii::app()->input->post('category_id');
		$subcategory_id = Yii::app()->input->post('subcategory_id');
		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');
		$search_for = "";
		if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value'])) {
			$search_for = $_REQUEST['search']['value'];
		}
		$order_by = array();
		if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])) {
			$order_by = $_REQUEST['order'];
		} else {
			//$order_by[1] = 'vendor_name';
		}

		$quotes = $quote->getQuotesAjax($_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $quote_status, $from_date, $to_date);
		$quoteArr = array();
		$i = 1;
		$subdomain = FunctionManager::subdomainByUrl();

		foreach ($quotes as $value) {
			$closeingDate = '';
			$style = "";
			$notQuoteCancelled = strtolower($value['quote_status']) != 'cancelled';
			$quoteStatusNotDraft = $value['quote_status'];
			$closing_date = date("Y-m-d H:i:s", strtotime($value['closing_date']));
			$currentDate = date("Y-m-d H:i:s");
			if ($notQuoteCancelled && $quoteStatusNotDraft !== 'Draft' && $closing_date >= $currentDate && $value['awarded_vendor_id'] == 0) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
        	 ' . $this->BtnColor(['#f0ad4e', '#f0ad4e']) . '>In Progress</button>';
			} else if ($quoteStatusNotDraft !== 'Draft' && !empty($value['awarded_vendor_id'])) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
        	 ' . $this->BtnColor(['#1abb9c', '#1abb9c']) . '>Awarded</button>';
			} else if ($quoteStatusNotDraft !== 'Draft' && $closing_date < $currentDate) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
        	 ' . $this->BtnColor(['#6cce47', '#6cce47']) . '>Complete</button>';
			} else if (strtolower($value['quote_status']) == 'cancelled') {
				$closeingDate = '<button type="submit" class="btn btn-success" 
        	 ' . $this->BtnColor(['#fb5a5a !important', '#fb5a5a !important']) . '>Cancelled</button>';
			} else if ($quoteStatusNotDraft == 'Draft') {
				$closeingDate = '<button type="submit" class="btn btn-danger" 
        	 ' . $this->BtnColor(['#fb5a5a', '#fb5a5a']) . '>Draft</button>';
			} else {
				$closeingDate = '';
			}

			$imgFullPath = '';
			if (!empty($value['profile_img'])) {
				$imgFullPath = Yii::app()->baseUrl . "/../images/" . $subdomain . "/users/" . $value['user_id'] . "/" . $value['profile_img'];
			} else if (!empty($value['user_id'])) {
				$imgFullPath = CommonFunction::userTextAvatar($value['full_name']);
			}
			if (!empty($imgFullPath)) {
				$imgFullPath = CHtml::image($imgFullPath);
			}

			$profileImg = '<div class="image-container">'.$imgFullPath.' </i><div class="profile-username"> '. $value['full_name'].' </div> </div> ';

			$sql = 'select user_id from quote_user';
			$quoteUser = Yii::app()->db->createCommand($sql)->queryAll();


			$current_data = array();

			$edit_link  = '';
			$edit_link .= '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $value['quote_id']) . '">
        				<button class="btn btn-sm btn-success view-btn">View/Edit</button></a>';

			$current_data[] = $edit_link;
			$current_data[] = $value['quote_id'];
			$current_data[] = $value['quote_name'];
			$current_data[] = $value['quotes_by_category'];
			$current_data[] = date(FunctionManager::dateFormat() . " H:i A", strtotime($value['opening_date']));
			$current_data[] = date(FunctionManager::dateFormat() . " H:i A", strtotime($value['closing_date']));
			$current_data[] = $profileImg;
			$current_data[] = !empty($value['awarded_vendor_name']) ? $value['awarded_vendor_name'] : '';
			$current_data[] = $value['quote_currency'];
			$current_data[] = "<span style='display:block;text-align:center'>" . round($value['cost_savings']) . "</span>";
			$current_data[] = $closeingDate;
			$quoteArr[]   = $current_data;
			
		}
		
        $database_values_total = $quote->getQuotesAjax('none', 'none', $_REQUEST['search']['value'], $order_by,$location_id, $department_id,$category_id, $subcategory_id, $quote_status,$from_date,$to_date);
        $total_quotes=count($database_values_total);
        $json_data = array(
	            "draw"            => intval( $_REQUEST['draw'] ),
	            "recordsTotal"    => $total_quotes,
	            "recordsFiltered" => $total_quotes,
	            "data"            => $quoteArr
            );
            echo json_encode($json_data);exit;  
	}

	public function actionAwardSupplier()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		if (!empty($_POST['award_vendor'])) {
			$vendorID     = $_POST['award_vendor'];
			$quoteID      = $_POST['quote_id'];
			$userID       = Yii::app()->session['user_id'];
			$userName     = Yii::app()->session['full_name'];
			$dateTime     = date("Y-m-d H:i:s");
			$vendor = new Vendor;
			$vendorRecord = $vendor->getOne(array('vendor_id' => $vendorID));
			$quote = new Quote;

			$costSaving = $quote->getCostSaving($quoteID, $vendorID);

			$quote->rs = array();
			$quote->rs['quote_id'] = $quoteID;
			$quote->rs['awarded_vendor_id'] = $vendorID;
			$quote->rs['awarded_vendor_name'] = $vendorRecord['vendor_name'];
			$quote->rs['awarded_by_user_id'] = $userID;
			$quote->rs['awarded_datetime'] = $dateTime;
			$quote->rs['awarded_by_username'] = $userName;
			$quote->rs['cost_savings']  = $costSaving;
			if ($quote->write()) {
				Yii::app()->user->setFlash('success', '<strong>Problem!</strong>try again.');
			} else {
				$quote = $quote->getOne(array('quote_id' => $quoteID));
				//$clientName = !empty(Yii::app()->session['company_name'])?Yii::app()->session['company_name']:$quote['created_by_name'];
				$subject = "Quote Award";
				$notificationText = "A Quote <b> " . $quoteID . "-" . $quote['quote_name'] . "</b> has been awarded";
				$url = AppUrl::bicesUrl('quotes/edit/' . $quoteID);
				$systemText = '<a href="' . $url . '">' . addslashes($notificationText) . '</a>';

				if (!empty($quote['created_by_user_id'])) $user_ids[$quote['created_by_user_id']] = $quote['created_by_user_id'];
				if (!empty($quote['commercial_lead_user_id'])) $user_ids[$quote['commercial_lead_user_id']] = $quote['commercial_lead_user_id'];

				$sql = "select * from users where user_id in(" . implode(",", $user_ids) . ")";
				$users = Yii::app()->db->createCommand($sql)->query()->readAll();
				foreach ($users as $value) {
					$name = $value['full_name'];
					$email = $value['email'];
					$userID = $value['user_id'];

					// $notification = new Notification();
					//   $notification->rs = array();
					//   $notification->rs['id'] = 0;
					//   $notification->rs['user_type'] = 'Client';
					//   $notification->rs['notification_flag'] = '';
					//   $notification->rs['user_id'] = $userID;
					//   $notification->rs['notification_text'] = $systemText;
					//   $notification->rs['notification_date'] = date("Y-m-d H:i");
					//   $notification->rs['read_flag'] = 0;
					//   $notification->rs['notification_type'] = 'Quote and not home';
					//   $notification->write();
					//  EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
				}

				$notificationText = "Congratulations, you have been awarded <b> " . $quote['quote_name'] . "</b>";

				$url = Yii::app()->createAbsoluteUrl('supplier/default/login');
				$systemText = '<a href="' . $url . '">' . addslashes($notificationText) . '</a>';

				$sql = "SELECT v.vendor_id,vendor_name,emails from vendors v left join vendor_teammember vt on v.vendor_id=vt.teammember_id WHERE v.vendor_id=" . $vendorID . " or vt.vendor_id =" . $vendorID;
				$vendors = Yii::app()->db->createCommand($sql)->query()->readAll();
				foreach ($vendors as $quote_vendor) {
					$name = $quote_vendor['vendor_name'];
					$email = $quote_vendor['emails'];
					$vendorID = $quote_vendor['vendor_id'];
					$notification = new Notification();
					$notification->rs = array();
					$notification->rs['id'] = 0;
					$notification->rs['user_type'] = 'Vendor';
					$notification->rs['notification_flag'] = '';
					$notification->rs['user_id'] = $vendorID;
					$notification->rs['notification_text'] = $systemText;
					$notification->rs['notification_date'] = date("Y-m-d H:i");
					$notification->rs['read_flag'] = 0;
					$notification->rs['notification_type'] = 'Quote and not home';
					$notification->write();
					//EmailManager::userNotification($email, $name, $subject, $notificationText, $url);
				}

				//EmailManager::userQuoteAwarded($quoteRecord);
				//EmailManager::vendorQuoteAwarded($quoteRecord,$vendorID);
				Yii::app()->user->setFlash('success', '<strong>Success! </strong>Quote awarded successfully.');
			}
			$this->redirect(AppUrl::bicesUrl('quotes/edit/' . $quoteID));
		}
	}

	public function actionReopenQuote()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		if (!empty($_POST['reopen_vendor'])) {
			$openDate  = $_POST['quote_start_date'];
			$closeDate = $_POST['qoute_end_date'];

			if (FunctionManager::dateFormat() == "d/m/Y") {
				$openDate = date("Y-m-d H:i:s", strtotime(strtr($openDate, '/', '-')));
				$closeDate = date("Y-m-d H:i:s", strtotime(strtr($closeDate, '/', '-')));
			} else {
				$openDate = date("Y-m-d H:i:s", strtotime($openDate));
				$closeDate = date("Y-m-d H:i:s", strtotime($closeDate));
			}

			$notes     = $_POST['reopen_notes'];
			$vendorIDs  = $_POST['reopen_vendor'];
			foreach ($vendorIDs as $vendorID) {
				$quoteID      = $_POST['quote_id'];
				$userID       = Yii::app()->session['user_id'];
				$userName     = Yii::app()->session['full_name'];
				$dateTime     = date("Y-m-d H:i:s");

				$vendor = new Vendor;
				$vendorRecord = $vendor->getOne(array('vendor_id' => $vendorID));
				$quote = new Quote;
				$quoteRecord = $quote->getOne(array('quote_id' => $quoteID));
				$openDateOld = $quoteRecord['opening_date'];
				$closeDateOld = $quoteRecord['closing_date'];

				$quote->rs = array();
				$quote->rs['quote_id'] = $quoteID;
				$quote->rs['opening_date']      = $openDate;
				$quote->rs['closing_date']      = $closeDate;

				$quote->rs['quote_status']      = 'Pending';

				$quote->write();

				$sql = 'update quote_vendors set submit_status=2 where quote_id=' . $quoteID . ' and vendor_id=' . $vendorID;
				$updateVendor = Yii::app()->db->createCommand($sql)->execute();

				if (!empty($updateVendor) || $openDateOld != strtotime($openDate) || $closeDateOld != strtotime($closeDate)) {
					$reopenHistory = new QuoteReopenHistory;
					$reopenHistory->rs = array();
					$reopenHistory->rs['quote_id']  = $quoteID;
					$reopenHistory->rs['vendor_id'] = $vendorID;
					$reopenHistory->rs['quote_start_date'] = $openDate;
					$reopenHistory->rs['qoute_end_date'] = $closeDate;
					$reopenHistory->rs['quote_old_start_date'] = $openDateOld;
					$reopenHistory->rs['quote_old_end_date'] = $closeDateOld;
					$reopenHistory->rs['notes'] = $notes;
					$reopenHistory->rs['created_by_id'] = $userID;
					$reopenHistory->rs['created_by_name'] = $userName;
					$reopenHistory->rs['created_at'] = $dateTime;
					$reopenHistory->write();

					$cronEmail = new CronEmail;
					$cronEmail->rs = array();
					$cronEmail->rs['record_id'] = $quoteID;
					$cronEmail->rs['send_to'] = 'Vendor';
					$cronEmail->rs['user_id'] = $vendorID;
					$cronEmail->rs['user_type'] = 'Re-Open Quote';
					$cronEmail->rs['status'] = 'Pending';
					$cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
					$cronEmail->write();
					// Start: interal users notification
					$userCommnets = "A quote <b>" . $quoteRecord['quote_id'] . "</b> - <b>" . $quoteRecord['quote_name'] . "</b> has been re-opened";
					// $quote->setInternalUsersNotification($quoteRecord,array($vendorID),$userCommnets);
					// End: interal users notification
					// Start: interal users notification
					//$vendorCommnets = "Re-open Quote Submission Invite";
					//$quote->setVendorNotification($quoteRecord, $vendorID,$vendorCommnets,array($vendorID));
					// End: interal users notification
					Yii::app()->user->setFlash('success', '<strong>Success! </strong>Quote re-opened successfully.');
				} else {
					Yii::app()->user->setFlash('success', '<strong>Success! </strong>Quote re-opened successfully.');
				}
			}
			$this->redirect(AppUrl::bicesUrl('quotes/edit/' . $quoteID));
		}
	}


	public function actionCreateContract()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		if (!empty($_POST['quote_id']) && !empty($_POST['vendor_id'])) {
			$contract = new Contract;
			$override_data = array();
			$override_data['user_id']   = Yii::app()->session['user_id'];
			$override_data['user_name'] = Yii::app()->session['full_name'];
			$contract->saveData($override_data);

			$contractID = $contract->rs['contract_id'];
			$vendorID   = $_POST['vendor_id'];
			$quoteID    = $_POST['quote_id'];
			$userID     = Yii::app()->session['user_id'];
			$userName   = Yii::app()->session['full_name'];
			$dateTime   = date("Y-m-d H:i:s");
			$quote = new Quote;
			$quote->rs = array();
			$quote->rs['quote_id'] = $quoteID;
			$quote->rs['awarded_contract_id'] = $contractID;
			if ($quote->write()) {
				Yii::app()->user->setFlash('success', '<strong>Problem!</strong>try again.');
			} else {
				Yii::app()->user->setFlash('success', '<strong>Success! </strong>Contract created successfully.');
			}
			$this->redirect(AppUrl::bicesUrl('quotes/edit/' . $quoteID));
		}
	}


	public function actionSubmitQuote()
	{
		// is someone already logged in?
		$this->clearSession();
		$this->pageTitle = "Submit Quote";
		$this->layout = 'main';

		$error = 0;
		$invitation_reference = "";
		$form_submitted = Yii::app()->input->post('form_submitted');
		if ($form_submitted) {
			$reference = Yii::app()->input->post('reference');
			$invitation_reference = $reference;

			$quote = new Quote();
			$quote_data = $quote->getQuoteByReference($reference);
			if ($quote_data && is_array($quote_data) && isset($quote_data['quote_id']) && isset($quote_data['vendor_id'])) {
				$reference_key = $quote_data['id'] . '_' . $quote_data['quote_id'] . '_' . $quote_data['vendor_id'];
				$quote_id = base64_encode($reference_key);
				$this->redirect(AppUrl::bicesUrl('quotes/submitQuoteDetails/?submit_quote_id=' . $quote_id));
			} else $error = 1;
		}

		$this->render('submit_quote', array('error' => $error, 'invitation_reference' => $invitation_reference));
	}

	public function actionSubmitQuoteDetails()
	{
		// is someone already logged in?
		$this->clearSession();
		$this->pageTitle = "Submit Quote Details";
		$this->layout = 'main';
		$view_data = array();

		$quote_found = false;
		$reference_key = isset($_REQUEST['submit_quote_id']) ? $_REQUEST['submit_quote_id'] : "";
		if (!empty($reference_key)) {
			$quote = new Quote();
			if (Yii::app()->input->post('form_submitted')) {
				$quote_submission_data = array();
				$quote_submission_data['quote_vendor_id'] = Yii::app()->input->post('quote_vendor_id');
				$quote_id = $quote_submission_data['quote_id'] = Yii::app()->input->post('quote_id');
				$vendor_id = $quote_submission_data['vendor_id'] = Yii::app()->input->post('vendor_id');
				$quote_submission_data['submit_quote'] = isset($_REQUEST['submit_quote']) ? $_REQUEST['submit_quote'] : 0;

				$quote_submission_data['product_names'] = Yii::app()->input->post('product_name');
				$quote_submission_data['quantities'] = Yii::app()->input->post('quantity');
				$quote_submission_data['prices'] = Yii::app()->input->post('price');
				$quote_submission_data['unit_price'] = Yii::app()->input->post('unit_price');
				$quote_submission_data['product_notes'] = Yii::app()->input->post('vendor_product_notes');
				$quote_submission_data['product_tax_rate'] = Yii::app()->input->post('product_tax_rate');
				$quote_submission_data['product_shipping_charges'] = Yii::app()->input->post('product_shipping_charges');
				$quote_submission_data['product_other_charges'] = Yii::app()->input->post('product_other_charges');

				$quote_submission_data['notes'] = Yii::app()->input->post('notes');
				$quote_submission_data['tax_rate'] = Yii::app()->input->post('tax_rate');
				$quote_submission_data['shipping'] = Yii::app()->input->post('shipping');
				$quote_submission_data['other_charges'] = Yii::app()->input->post('other_charges');
				$quote_submission_data['discount'] = Yii::app()->input->post('discount');
				$quote_submission_data['total_price'] = Yii::app()->input->post('total_price');

				$quote->saveQuoteSubmissionData($quote_submission_data);

				$quote->deleteQuoteDocumentandAnswers($quote_id, $vendor_id);


				// files uploaded if any
				$total_documents = Yii::app()->input->post('total_documents');
				for ($idx = 1; $idx <= $total_documents; $idx++) {
					$delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idx);
					if ($delete_document_flag) continue;

					if (isset($_FILES['quote_file_' . $idx]) && is_array($_FILES['quote_file_' . $idx])) {
						if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
						if (!is_dir('uploads/quotes/' . $quote_id))
							mkdir('uploads/quotes/' . $quote_id);
						if (!is_dir('uploads/quotes/' . $quote_id . '/' . $vendor_id))
							mkdir('uploads/quotes/' . $quote_id . '/' . $vendor_id);

						if (!isset($_FILES['quote_file_' . $idx]['error']) || empty($_FILES['quote_file_' . $idx]['error'])) {
							$file_name = $_FILES['quote_file_' . $idx]['name'];
							move_uploaded_file($_FILES['quote_file_' . $idx]['tmp_name'], 'uploads/quotes/' . $quote_id . '/' . $vendor_id . '/' . $file_name);
							//Save documents
							$qouteVendorFile = new QuoteVendorFile();
							$qouteVendorFile->rs = array();
							$qouteVendorFile->rs['quote_id'] = $quote_id;
							$qouteVendorFile->rs['vendor_id'] = $vendor_id;
							$qouteVendorFile->rs['file_name'] = $file_name;
							$qouteVendorFile->write();
						}
					}
				}
				$total_questions = Yii::app()->input->post('total_questions');
				for ($i = 1; $i <= $total_questions; $i++) {
					$qouteVendorAnswer = new QuoteVendorAnswer();
					$qouteVendorAnswer->rs = array();
					$qouteVendorAnswer->rs['quote_id'] = $quote_id;
					$qouteVendorAnswer->rs['vendor_id'] = $vendor_id;
					$qouteVendorAnswer->rs['question_id'] = Yii::app()->input->post('question_id_' . $i);
					$qouteVendorAnswer->rs['question_type'] = Yii::app()->input->post('question_type_' . $i);
					$qouteVendorAnswer->rs['free_text'] = Yii::app()->input->post('free_text_answer_' . $i);
					$qouteVendorAnswer->rs['yes_no'] = Yii::app()->input->post('yes_or_no_' . $i);
					$qouteVendorAnswer->rs['quote_answer'] = Yii::app()->input->post('quote_answer_' . $i);
					$qouteVendorAnswer->write();
				}
				if (!empty($quote_submission_data['submit_quote'])) {
					$quote->submitQuote($quote_id, $vendor_id);
				}
			}

			$quote_vendor_id = $quote_id = $vendor_id = 0;
			$complete_quote_id = base64_decode($reference_key);
			if (count(explode("_", $complete_quote_id)) >= 3)
				list($quote_vendor_id, $quote_id, $vendor_id) = explode("_", $complete_quote_id);

			$view_data['quote_vendor_id'] = $quote_vendor_id;
			$view_data['quote_id'] = $quote_id;
			$view_data['vendor_id'] = $vendor_id;
			$view_data['submit_quote_id'] = $reference_key;

			$view_data['quote'] = $quote->getQuote($quote_id);
			$view_data['quote_questions'] = $quote->getQuoteQuestions($quote_id);
			if ($view_data['quote'] && is_array($view_data['quote'])) {
				$quote_found = true;
				$view_data['quote_details'] = $quote->getQuoteDetails($quote_id, $vendor_id);
				$view_data['quote'] = array_merge($view_data['quote'], $quote->getVendorQuote($quote_id, $vendor_id));

				$vendor = new Vendor();
				$view_data['vendor'] = $vendor->getOne(array('vendor_id' => $vendor_id));
			}
		}

		if (!$quote_found) $this->redirect(AppUrl::bicesUrl('quotes/submitQuote'));
		else {
			// Yii::app()->user->setFlash('success', "Success! Sourcing Activity Created Successfully");
			$this->render('submit_quote_details', $view_data);
		}
	}
	public function actionEdit($quote_id = 0)
	{
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_edit';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		error_reporting(0);

		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted')) {
			// save actual quote data
			$quote = new Quote();
			if ($quote_id == 0) {
				$override_data = array();
				$override_data['send_to'] = 'Vendor';
				$override_data['spend_type'] = !empty($_POST['spend_type']) ? $_POST['spend_type'] : '';
				$override_data['commercial_lead_user_id'] = !empty($_POST['commercial_lead_user_id']) ? $_POST['commercial_lead_user_id'] : 0;

				$quote->saveData($override_data);
				Yii::app()->user->setFlash('success', "Success! Sourcing Activity Created Successfully");
			} else {
				$quote->saveData();
				Yii::app()->user->setFlash('success', "Success! Sourcing Activity Created Successfully");
			}
			$quote_id = $quote->rs['quote_id'];

			// IN update case this function call for quote logs 
			if (!empty($quote_id)) $this->quoteLogsInsert($quote_id);
			// Quote Comments Inserted Function
			if (!empty($_POST['quote_comments'])) $this->quoteCommentInsert($quote_id);
			// Quote Questionnaire Inserted Function
			if (count($_POST['question_form_id']) > 0) $this->quoteQuestionnaireInsert($quote_id, 'edit');

			// files uploaded if any
			$total_documents = Yii::app()->input->post('total_documents');

			$uploadedFile = Yii::app()->session['uploaded_files'];

			if (!empty($uploadedFile)) {
			  $file = Yii::getPathOfAlias('webroot') . "/uploads/quotes/temp/" . $deleted['file_name'];
			  foreach ($uploadedFile as $key => $value) {
				$fileName = $value['file_name'];

				$fileDescription = $value['file_description'];
				$fileSource = Yii::getPathOfAlias('webroot') . "/uploads/quotes/temp/" . $fileName;
				if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
				if (!is_dir('uploads/quotes/' . $quote_id))
					mkdir('uploads/quotes/' . $quote_id);
				$fileTarget = Yii::getPathOfAlias('webroot') . "/uploads/quotes/$quote_id/" . $fileName;
				copy($fileSource, $fileTarget);
				$quote->saveQuoteFile($quote_id, $fileName, $fileDescription);
				unlink($fileSource);
			  }
			}
			
			//Start:  products in the quote
			$product_names = Yii::app()->input->post('product_name');
			$quantities = Yii::app()->input->post('quantity');
			$uoms = Yii::app()->input->post('uom');
			$current_prices = Yii::app()->input->post('current_price');
			$product_code  = Yii::app()->input->post('product_code');
			$quote->saveProducts($quote_id, $product_names, $quantities, $uoms, $current_prices, $product_code);
			//END:  products in the quote

			// Suppliers invited to send bid
			$vendor_ids = array();
			$total_vendors = Yii::app()->input->post('vendor_count');
			$selectedVendors = !empty($_POST['vendor_name_new']) ? $_POST['vendor_name_new'] : '';
			//$selectedVendorContact = !empty($_POST['vendor_contact_new']) ? $_POST['vendor_contact_new'] : '';
			
			// Start: vendor notification
			$quote_data = $quote->getOne(array('quote_id' => $quote_id));
			if ($_POST['quote_send_to'] == 'choosen_supplier' || $_POST['quote_send_to'] == 'consultant_and_choosen_supplier') {
				if (!empty($selectedVendors)) {
					foreach ($selectedVendors as $selectedID) {
						$vendor_ids[] = $selectedID;
					}
				}
				$quick_vendor_ids = array();
				if (!empty(Yii::app()->session['quick_vendor'])) {
					$quickaddedVendorArr = Yii::app()->session['quick_vendor'];
				}
				
				// $vendor_ids = Yii::app()->input->post('vendor_id');
				$already_invited = $quote->saveVendors($quote_id, $selectedVendors);
				// may be supplier details were updated?
				$vendor = new Vendor();

				if ((isset($_POST['quote_status']) && strtolower($_POST['quote_status']) == 'pending') || Yii::app()->input->post('invite_suppliers')) {
					$quote->setVendorNotification($quote_data, $vendor_ids);
				}
			}

			// Start :Internal quote users & send email each contributor for this quote 
			if (!empty($_POST['quote_status']) && $_POST['quote_status'] == 'Draft') {
				// create quote modal hidden fields
				if (!empty($_POST['quote_internal_users']) || !empty($_POST['contribute_deadline'])) {
					$quote_internal_user  = !empty($_POST['quote_internal_users']) ? $_POST['quote_internal_users'] : Yii::app()->session['user_id'];
					$quoteInternalArray = explode(",", $quote_internal_user);
					$contributions_datetime = !empty($_POST['contribute_deadline']) ? date("Y-m-d H:i:s", strtotime($_POST['contribute_deadline'])) : '';
					$userID  = Yii::app()->session['user_id'];
					$userName = Yii::app()->session['full_name'];
					$contribute = 1;
					foreach ($quoteInternalArray as $key => $quote_user_id) {
						$sql = 'INSERT INTO quote_user (quote_id,user_id,create_user_id,user_name,quote_internal_user_role,contribute_deadline) 
						VALUES ("' . $quote_id . '",
						"' . $quote_user_id . '",
						"' . $userID . '",
						"' . $userName . '",
						"' . $contribute . '",
						"' . $contributions_datetime . '") ';
						Yii::app()->db->createCommand($sql)->execute();

						$notificationComments  = $userName." has invited you to contribute to <b> ".$quote_data['quote_name']."</b>";
						$notification = new Notification();
						$notification->rs = array();
						$notification->rs['id'] = 0;
						$notification->rs['user_id'] = $quote_user_id;
						$notification->rs['user_type'] = 'User';
						$notification->rs['notification_text'] = addslashes('<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_data['quote_id']). '">'.$notificationComments.'</a>');
						$notification->rs['notification_date'] = date("Y-m-d H:i");
						$notification->rs['read_flag'] = 0;
						$notification->rs['notification_type'] = 'Quote';
						$notification->write();

						$cronEmail = new CronEmail;
						$cronEmail->rs = array();
						$cronEmail->rs['id'] = 0;
						$cronEmail->rs['record_id'] = $quote_data['quote_id'];
						$cronEmail->rs['send_to'] = 'User';
						$cronEmail->rs['user_id'] = $quote_user_id;
						$cronEmail->rs['user_type'] = 'Quote Contributor';
						$cronEmail->rs['status'] = 'Pending';
						$cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
						$cronEmail->write();
					
					}
				}
			}else{
				// assign questions to supplers of this quotes
				$this->questionAssignToSupplier($quote_id);
			}
			// END :Internal quote users & send email each contributor for this quote 

			// when status is draft ignore e=event trggier and crone 
			if ($_POST['quote_send_to'] == 'consultant' || $_POST['quote_send_to'] == 'consultant_and_choosen_supplier') {
				$userArray = array('hello@oboloo.com');
				// EmailManager::independentUserQuote($quote_data,$userArray);
			}
			// End: vendor notification

			$this->redirect(AppUrl::bicesUrl('quotes/list'));
		}
		// get data for the selected vendor
		$view_data = array('quote_id' => $quote_id);
		$quote = new Quote();
		$view_data['quote'] = $quote->getOne(array('quote_id' => $quote_id));
		$view_data['costSaving'] = $quote->getCostSaving($quote_id, $view_data['quote']['awarded_vendor_id']);

		if (!$view_data['quote'] || !is_array($view_data['quote'])) $quote_id = 0;
		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));
		// and also product and supplier information for the quote
		$view_data['quote_details'] = $quote->getQuoteDetails($quote_id);
		$view_data['quote_details_history'] = $quote->getQuoteDetailsHistory($quote_id);
		$view_data['quote_details_history_new'] = $quote->getQuoteDetailsHistoryNew($quote_id);

		$view_data['quote_vendors'] = $quote->getQuoteVendors($quote_id);
		if (isset($view_data['quote_vendors'][0]['vendor_id']) && !empty($view_data['quote_vendors'][0]['vendor_id'])) {
			$vendor_id = $view_data['quote_vendors'][0]['vendor_id'];
		} else {
			$vendor_id = 0;
		}
		$view_data['quote_files'] = $quote->getQuoteFiles($quote_id);
		$view_data['quote_questions'] = $quote->getQuoteQuestions($quote_id);
		$view_data['quote_stacked_bar_graph'] = $quote->getQuoteGraph($quote_id, '');
		// echo "<pre>"; print_r($view_data['quote_stacked_bar_graph']); echo "</pre>"; exit;
		$view_data['quote_answer_line_graph'] = $quote->getQuoteAnswer($quote_id, '');
		$view_data['quote_overall_stats'] = $quote->getQuoteOverAllStats($quote_id);
		$view_data['scoringCriteria'] = $quote->scoringCriteria($quote_id);
		$view_data['quoteVendors'] = $quote->getQuoteVendors($quote_id);
		$view_data['quoteAnswerVendor'] = $quote->getAnwserVendors($quote_id);
		$category = new Category();
		$view_data['categories'] = $category->getAll();
		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();
		$view_data['documentList'] = $quote->getQuoteDocuments($quote_id);
		$reopenReader =  Yii::app()->db->createCommand("SELECT created_at,id FROM quote_reopen_history where quote_id=" . $quote_id)->query()->readAll();
		$total_reopen = 0;
		$previousCreatedAt = '';
		$reopenIds = array();
		foreach ($reopenReader as $value) {
			$total_reopen = $total_reopen + 1;
			$reopenIds[] = $value['id'];
			$previousCreatedAt = $value['created_at'];
		}
		$view_data['total_reopen'] = $total_reopen;
		$view_data['reopenIds'] = $reopenIds;

		$quoteLogs = new QuoteLog();
		$quoteCommHistory = new QuoteCommentsHistory();

		$view_data['scoringLog'] = $quoteLogs->getScoringLogs($quote_id);
		$view_data['quoteCommHistory'] = $quoteCommHistory->getQuoteCommentsHistory($quote_id);

		$quoteVendorsQuestionnaire = new QuoteVendorsQuestionnaire();
		$view_data['quote_vendors_questionnaire'] = $quoteVendorsQuestionnaire->getOne(array('quote_id' => $quote_id));
		$view_data['checkQuestionnireOfQuote'] = $quoteVendorsQuestionnaire->totalQuestionnainre($quote_id); 

		// if status comming draft this redirect to this page edit_draft_eSourcing_activity
		if (!empty($view_data['quote']) && $view_data['quote']['quote_status'] == 'Draft') {
			$view_data['quote_products'] = $quote->getQuoteProducts($quote_id);
			Yii::app()->session['quote_id'] = $quote_id;
			$view_data['maxQuestion'] = $quote->getQuoteQuestionires($quote_id);
			$view_data['defaultScoringCriteria'] = $quote->defaultScoringCriteria($quote_id);
			$view_data['defaultScoring'] = $quote->defaultScoring($quote_id);
			$this->render('edit_draft_eSourcing_activity', $view_data);
		} else {
			$this->render('edit', $view_data);
		}

		unset(Yii::app()->session['quick_vendor']);
	}


	public function actionSendToindependent()
	{

		/*	$cronEmail = new cronEmail();
		$cronEmail->rs = array();
		$cronEmail->rs['record_id'] = ''; 
		$cronEmail->rs['user_id'] = '';
		$cronEmail->rs['user_type'] = 'Independent Consultant';
		$cronEmail->rs['status'] = 'Pending';
		$cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
		$cronEmail->write(); */

		$userArray = array('hello@oboloo.com');
		$userLoggedinID = Yii::app()->session['user_id'];
		EmailManager::independentConsultantQuote($userArray, $userLoggedinID);
	}

	public function scoringQuestionnair($quote_id, $scoringID)
	{

		for ($i = 1; $i <= $_POST['total_questions_' . $scoringID]; $i++) {

			$_POST['quote_question_' . $i . $scoringID] = addslashes($_POST['quote_question_' . $i . $scoringID]);
			$_POST['question_type_' . $i . $scoringID] = addslashes($_POST['question_type_' . $i . $scoringID]);

			if (!empty($_POST['quote_question_' . $i . $scoringID])) {
				$qouteQuestion = new QuoteQuestion();
				$qouteQuestion->rs = array();
				$qouteQuestion->rs['quote_id'] = $quote_id;
				$qouteQuestion->rs['vendor_scoring_id'] = $scoringID;
				$qouteQuestion->rs['question'] = $_POST['quote_question_' . $i . $scoringID];
				$qouteQuestion->rs['question_type'] = $_POST['question_type_' . $i . $scoringID];
				$qouteQuestion->write();
				$question_id = $qouteQuestion->rs['id'];
				if ($_POST['question_type_' . $i . $scoringID] == 'yes_or_no') {
					$qouteAnswer = new QuoteAnswer();
					$qouteAnswer->rs = array();
					$qouteAnswer->rs['quote_id'] = $quote_id;
					$qouteAnswer->rs['question_id'] = $question_id;
					$qouteAnswer->rs['yes'] = addslashes($_POST['yes_' . $i . $scoringID]);
					$qouteAnswer->rs['no'] = addslashes($_POST['no_' . $i . $scoringID]);
					$qouteAnswer->write();
				} elseif ($_POST['question_type_' . $i . $scoringID] == 'multiple_choice') {


					for ($j = 1; $j <= 10; $j++) {
						$postedQuestionAns = addslashes($_POST['quote_answer_' . $i . $scoringID . '_' . $j]);
						if (!empty($postedQuestionAns)) {
							$qouteAnswer = new QuoteAnswer();
							$qouteAnswer->rs = array();
							$qouteAnswer->rs['quote_id'] = $quote_id;
							$qouteAnswer->rs['question_id'] = $question_id;
							$qouteAnswer->rs['answer'] = $postedQuestionAns;

							$qouteAnswer->rs['yes'] = addslashes($_POST['answer_yes_' . $i . $scoringID . '_' . $j]);
							$qouteAnswer->rs['no'] = addslashes($_POST['answer_no_' . $i . $scoringID . '_' . $j]);
							$qouteAnswer->write();
						}
					}
				} else {
					$qouteAnswer = new QuoteAnswer();
					$qouteAnswer->rs = array();
					$qouteAnswer->rs['quote_id'] = $quote_id;
					$qouteAnswer->rs['question_id'] = $question_id;
					$qouteAnswer->rs['free_text'] = "Free Text";
					$qouteAnswer->write();
				}
			}
		}
	}


	public function actionSaveEditScoring()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$scoreCriteria = $_POST['score_criteria'];
		$quote_id      = $_POST['quote_id'];

		foreach ($scoreCriteria as $scoreID => $value) {

			$VendorScoring =  new VendorScoring();
			$scoreRecord = $VendorScoring->getOne(array('id' => $scoreID));


			foreach ($value as $vendorID => $vendValue) {
				$vendorQuoteScoring =  new VendorQuoteScoring();
				$vendorQuoteScoring = $vendorQuoteScoring->getOne(array('score_id' => $scoreID, 'vendor_id' => $vendorID, 'quote_id' => $quote_id));

				$scorePercentage = $vendorQuoteScoring['score_percentage'];

				$vendorID = $vendorID;
				$selectedScore = $vendValue;
				//$scoring = $selectedScore/count($value)*$scorePercentage/100;
				$scoring = $selectedScore / count($value) * $scorePercentage;

				$quoteScoring = new VendorQuoteScoring();
				$rs = array();
				$rs['id'] = $vendorQuoteScoring['id'];
				$rs['vendor_id'] = $vendorQuoteScoring['vendor_id'];
				$rs['quote_id'] = $vendorQuoteScoring['quote_id'];
				$rs['created_by_id'] = $vendorQuoteScoring['created_by_id'];
				$rs['created_by_name'] = $vendorQuoteScoring['created_by_name'];
				$rs['score_id'] = $vendorQuoteScoring['score_id'];
				$rs['score_percentage'] = $vendorQuoteScoring['score_percentage'];
				$rs['total_vendors'] = $vendorQuoteScoring['total_vendors'];
				//$rs['created_at'] = $vendorQuoteScoring['created_at'];

				$rs['score_value'] = $selectedScore;
				$rs['calculated_value'] = $scoring;

				if ($quoteScoring->saveData($rs)) {
					$msg = 'Scoring edited successfully';
				}
				$msg = 'Scoring edited successfully';
				//}
			}
		}
		echo json_encode(array('msg' => $msg));
	}

	public function actionCheckScoringSubmission()
	{

		$quoteID = $_POST['quote_id'];
		$vendorQuoteScoring =  new VendorQuoteScoring();
		$scoreRecord = $vendorQuoteScoring->getOne(array('quote_id' => $quoteID));
		if (!empty($scoreRecord)) {
			echo "0";
			exit;
		} else {
			echo "1";
			exit;
		}
	}


	/* public function actionListScoring()
    {
    	
        $body = '';
        $bodyEdit = '';
    	$quoteID = $_POST['quote_id'];
    	$quote = new Quote();
		$quoteVendors = $quote->getQuoteVendors($quoteID);
        $createBy = Yii::app()->session['user_id'];
        $user = new User();
        $userRecord = $user->getOne(array('user_id' => $createBy));
        $sql = "select ven.vendor_name,ven.vendor_id from vendor_quote_scoring sco 
        		inner join vendors ven on sco.vendor_id = ven.vendor_id
        where quote_id=".$quoteID.' group by sco.vendor_id order by sco.score_id asc';
        $vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $sql = "select sco.id,score_percentage,vsco.value as score_name,GROUP_CONCAT(score_value) as score,GROUP_CONCAT(calculated_value) as calculated_score,GROUP_CONCAT(vendor_id) as vendor_ids from vendor_quote_scoring sco 
        		inner join vendor_scoring vsco on sco.score_id = vsco.id
        where quote_id=".$quoteID.' group by sco.score_id order by sco.score_id asc';
        $scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
        $totalVendorScore = array();
        if(!empty($vendoReader)){
        $body = ' <h3>Scoring Criteria List</h3><br /><br /><table class="table table-striped table-bordered"><thead><tr><th>Scoring Criteria</th>';

        $bodyEditHeading = '<table class="table table-striped table-bordered"><thead><tr><th>Scoring Criteria</th>';

        foreach($vendoReader as $vendValue){
        	$totalVendorScore[$vendValue['vendor_id']] = 0;
        	$body .= '<th>'.$vendValue['vendor_name'].'</th>';
        	$bodyEditHeading .= '<th>'.$vendValue['vendor_name'].'</th>';
    	}
    	$body .= '<th>Action</th></tr>';
    	$body .= '</thead><tbody>';

    	$bodyEditHeading .= '</tr>';
    	$bodyEditHeading .= '</thead><tbody>';


    	$i=1;
    	foreach($scoreReader as $value){
    		$scoreArr = explode(",", $value['score']);
    		$calculatedScoreArr = explode(",", $value['calculated_score']);
    		$calculatedScoreArr = explode(",", $value['calculated_score']);
    		$vendorIDsArr = explode(",", $value['vendor_ids']);
    		
        	$body .= '<tr>
        				<th>'.$value['score_name'].'('.$value['score_percentage'].'%)</th>';

        	$bodyEdit = $bodyEditHeading.'<tr>
        				<th>'.$value['score_name'].'('.$value['score_percentage'].'%)</th>';

        	foreach($scoreArr as $key=>$scoreInfo){
        		$totalVendorScore[$vendorIDsArr[$key]] = $totalVendorScore[$vendorIDsArr[$key]]+$scoreInfo;
        		$body .= '
        					<td>'.$scoreInfo.'</td>';
        		$scoreID  = $value['id'];
        		$vendorID = $vendorIDsArr[$key];
        		$bodyEdit .= '<td><select required name="score_criteria['.$scoreID.']['.$vendorID.']"  class="scoreedit_'.$i.' vendor_'.$vendorIDsArr[$key].'  form-control" onChange="checkUniqScoreEdit(this,'.$i.');">
                  <option value="">Select</option>';

                for($j=1; $j<=count($quoteVendors); $j++) { 
                    $bodyEdit .= '<option value="'.$j.'" '. ($scoreInfo==$j?'selected="SELECTED"':'').'>'.$j.'</option>';
                } 
                $bodyEdit .= '</select> <span></span></td>';

        	}

        	$bodyEdit .= '</tr></tbody></table>';

        

        	$popUpID=$value['id'];
        	$formID = "edit".$popUpID;

        	$actionLink='<a data-toggle="modal" data-target="#'.$popUpID.'" href="#"><i class="fa fa-edit" style="font-size:18px"></i></a>';

        	$body .= '<td>'.$actionLink;

        	$infoModal = '<div id="'.$popUpID.'" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Edit Scoring Criteria</h4></div>
                	<div class="modal-body"><form id="'.$formID.'" action="" method="post">'.$bodyEdit.'
                	 </form>
                    <div class="edit_msg_area"></div>
                	<div class="modal-footer"><button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button> <button  type="button" class="btn btn-primary btn sm" onclick="editProject('.$formID.');">Edit</button></div></div</div></div>style
                	<style>
                	.control-label {min-width: 22% !important;}
                	.form-control {width: 50%  !important;}
                	.form-group {margin-bottom: 14px !important; width: 100%;}
                	</style>

                	';
                $body .=$infoModal.'</td>';

        	$body .= '</tr>';
    	}
    	 $body .= '<tr><td><strong>Total</strong></td>';
    	 foreach($vendoReader as $vendValue){
        	$body .= '<td>'.number_format($totalVendorScore[$vendValue['vendor_id']],2).'</td>';
    	}
    	$body .= '</tr>';

    	$body .= '</tbody></table>';
    	}
    	echo $body;
    	exit;
        
    }*/



	public function actionListScoring()
	{
		error_reporting(0);
		$body = '';
		$quoteID = $_POST['quote_id'];

		$quote = new Quote();
		$quoteRecord = $quote->getOne(array('quote_id' => $quoteID));

		$createBy = Yii::app()->session['user_id'];
		$user = new User();
		$userRecord = $user->getOne(array('user_id' => $createBy));
		$sql = "select ven.vendor_name,ven.vendor_id from vendor_quote_scoring sco 
        		inner join vendors ven on sco.vendor_id = ven.vendor_id
        where quote_id=" . $quoteID;
		$vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = "select sco.id,sco.score_id,score_percentage,vsco.scoring_title as score_name,sco.vendor_id from vendor_quote_scoring sco inner join quote_scoring vsco on sco.score_id = vsco.vendor_scoring_id and vsco.quote_id=sco.quote_id where sco.quote_id=" . $quoteID . " group by sco.score_id";

		$scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
		$totalVendorScore = array();
		if (!empty($vendoReader)) {
			$body = '<div class="x_title  pull-left" style="border-bottom:none"><h4 class="heading" style="padding: 10px 0px 10px 0px">Score Suppliers</h4>
        <h4 class="subheading" style="padding: 10px 0px 10px 0px">Highest Number = Best Scoring Supplier, Lowest Number = Lowest Scoring Supplier  (Suppliers can have the same score)</h4></div><button type="button" class="btn btn-info pull-right" onclick="$(\'#anwser_supplier_modal\').modal(\'show\');" style="right: 0;"><span class="fa fa-list-alt"></span> Supplier Questionnaire Answers</button><br /><table class="table table-striped table-bordered"><thead><tr><th>Scoring Criteria</th>';

			//Start: Vendor Name heading
			$vendor_ids = array();
			foreach ($vendoReader as $vendValue) {
				//$totalVendorScore[$vendValue['vendor_id']] = 0;
				if (!in_array($vendValue['vendor_id'], $vendor_ids)) {
					$vendor_ids[] = $vendValue['vendor_id'];
					$body .= '<th>' . $vendValue['vendor_name'] . '</th>';
				}
			}

			if (empty($quoteRecord['awarded_vendor_id'])) {
				$body .= '<th>Action</th>';
			}
			$body .= '</tr></thead><tbody>';
			//End: Vendor Name heading

			//Start: Vendor scoring
			$i = 0;
			foreach ($scoreReader as $value) {
				/* $scoreID = $value['score_id'];
    		$sql = "SELECT * FROM quote_scoring where vendor_scoring_id=".$scoreID." and quote_id=".$quoteID;
        	$scoringForQuoteReader = Yii::app()->db->createCommand($sql)->queryRow();*/

				$body .= '<tr><th>' . $value['score_name'] . ' (' . number_format($value['score_percentage'], 0) . '%)</th>';

				$sql = "select id,score_value,vendor_id from vendor_quote_scoring  where quote_id=" . $quoteID . " and score_id=" . $value['score_id'] . " and vendor_id in(" . implode(",", $vendor_ids) . ")";
				$i++;

				$vendorScoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
				foreach ($vendorScoreReader as $scoreValue) {
					//$scoreInfo = $scoreValue['calculated_score'];
					$scoreInfo = $scoreValue['score_value'];

					$totalVendorScore[$scoreValue['vendor_id']] = $totalVendorScore[$scoreValue['vendor_id']] + $scoreInfo;

					$body .= '<td>' . number_format($scoreInfo, 0) . '</td>';
				}

				if (empty($quoteRecord['awarded_vendor_id'])) {
					$actionLink = '<a href="#" onClick="loadEditForm(event,' . $value['score_id'] . ')"><i class="fa fa-edit" style="font-size:18px"></i></a>';
					$body .= '<td>' . $actionLink . '</td>';
				}

				$body .= '</tr>';
			}
			$body .= '</tbody></table>';
		}
		echo $body;
		exit;
	}


	public function actionListScoreDataChart()
	{
		if (!empty($_POST['quote_id'])) {
			$quote_id = $_POST['quote_id'];
			$sql = "select sum(sco.calculated_value) as s_valu,vend.vendor_name from vendor_quote_scoring as sco
	         inner join vendors as vend on sco.vendor_id=vend.vendor_id where sco.quote_id=" . $quote_id . "  group by sco.vendor_id order by s_valu DESC";
			$scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

			$location_series = array();
			$location_label = array();
			foreach ($scoreReader as $value) {
				if ($value['s_valu'] > 0) {
					$location_series[] = $value['s_valu'];
					$location_label[] = $value['vendor_name'];
				}
			}

			$sql = "select score as s_valu,scoring_title from quote_scoring where quote_id=" . $quote_id . " order by s_valu DESC";
			$scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

			$pie_score_series = array();
			$pie_score_label = array();
			foreach ($scoreReader as $value) {
				/*$scoreID = $value['score_id'];
				$sql = "SELECT * FROM quote_scoring where vendor_scoring_id=".$scoreID." and quote_id=".$quote_id;
	        	$scoringForQuoteReader = Yii::app()->db->createCommand($sql)->queryRow();*/
				if ($value['s_valu'] > 0) {
					$pie_score_series[] = $value['s_valu'];
					$pie_score_label[] = $value['scoring_title'];
				}
			}
		} else {
			$sql = "select AVG(sco.calculated_value) as s_valu,vend.vendor_name from 
	        	(
	        		select sco_inner.* from vendor_quote_scoring as sco_inner order by calculated_value DESC 
	        	)  as sco
	         inner join vendors as vend on sco.vendor_id=vend.vendor_id  group by sco.vendor_id order by s_valu DESC limit 10";
			$scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

			$location_series = array();
			$location_label = array();
			foreach ($scoreReader as $value) {
				$location_series[] = intval($value['s_valu']);
				$location_label[] = $value['vendor_name'];
			}

			//
			/*$sql = "select AVG(score) as s_valu,scoring_title from 
			( select * from quote_scoring order by score DESC ) as qs */
			$sql = " select score as s_valu, value as scoring_title from vendor_scoring order by s_valu DESC ";
			$scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

			$pie_score_series = array();
			$pie_score_label = array();
			foreach ($scoreReader as $value) {
				/*$scoreID = $value['score_id'];
				$sql = "SELECT * FROM quote_scoring where vendor_scoring_id=".$scoreID." and quote_id=".$quote_id;
	        	$scoringForQuoteReader = Yii::app()->db->createCommand($sql)->queryRow();*/
				if ($value['s_valu'] > 0) {
					$pie_score_series[] = intval($value['s_valu']);
					$pie_score_label[] = $value['scoring_title'];
				}
			}
		}

		$data = array();
		$data['score_values'] = $location_series;
		$data['score_labels'] = $location_label;
		$data['pie_score_series'] = $pie_score_series;
		$data['pie_score_label'] = $pie_score_label;
		echo json_encode($data);
		exit;
	}
	public function actionEditScoring()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		error_reporting(0);
		$body = '';
		$bodyEdit = '';
		$scoreID = $_POST['score_id'];
		$quoteID = $_POST['quote_id'];
		$quote = new Quote();
		$quoteVendors = $quote->getQuoteVendors($quoteID);
		$createBy = Yii::app()->session['user_id'];
		$user = new User();
		$userRecord = $user->getOne(array('user_id' => $createBy));
		$sql = "select ven.vendor_name,ven.vendor_id from vendor_quote_scoring sco 
        		inner join vendors ven on sco.vendor_id = ven.vendor_id
        where sco.score_id=" . $scoreID . " and sco.quote_id=" . $quoteID . " group by sco.vendor_id order by sco.score_id asc";
		$vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

		$sql = "select sco.id,score_percentage,vsco.scoring_title as score_name,sco.score_id,GROUP_CONCAT(score_value) as score,GROUP_CONCAT(calculated_value) as calculated_score,GROUP_CONCAT(vendor_id) as vendor_ids from vendor_quote_scoring sco 
        		inner join quote_scoring vsco on sco.score_id = vsco.vendor_scoring_id and vsco.quote_id=sco.quote_id
        where sco.score_id=" . $scoreID . " and sco.quote_id=" . $quoteID . " group by sco.score_id order by sco.score_id asc";
		$scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
		$totalVendorScore = array();
		if (!empty($vendoReader)) {

			$bodyEditHeading = '<input type="hidden" class="notranslate"  name="quote_id" value="' . $quoteID . '" /> <table class="table table-striped table-bordered"><thead><tr><th class="notranslate">Scoring Criteria</th>';

			foreach ($vendoReader as $vendValue) {
				$totalVendorScore[$vendValue['vendor_id']] = 0;

				$bodyEditHeading .= '<th class="notranslate">' . $vendValue['vendor_name'] . '</th>';
			}

			$bodyEditHeading .= '</tr>';
			$bodyEditHeading .= '</thead><tbody>';


			$i = 1;
			foreach ($scoreReader as $value) {

				$scoreID = $value['score_id'];
				$sql = "SELECT * FROM quote_scoring where vendor_scoring_id=" . $scoreID . " and quote_id=" . $quoteID;
				$scoringForQuoteReader = Yii::app()->db->createCommand($sql)->queryRow();

				$scoreArr = explode(",", $value['score']);
				$calculatedScoreArr = explode(",", $value['calculated_score']);
				$calculatedScoreArr = explode(",", $value['calculated_score']);
				$vendorIDsArr = explode(",", $value['vendor_ids']);

				$bodyEdit = $bodyEditHeading . '<tr>
        				<th class="notranslate">' . $scoringForQuoteReader['scoring_title'] . '(' . number_format($value['score_percentage'], 0) . '%)</th>';

				foreach ($scoreArr as $key => $scoreInfo) {
					$totalVendorScore[$vendorIDsArr[$key]] = $totalVendorScore[$vendorIDsArr[$key]] + $scoreInfo;

					$vendorID = $vendorIDsArr[$key];
					$bodyEdit .= '<td><select required  name="score_criteria[' . $scoreID . '][' . $vendorID . ']"  class="scoreedit_' . $i . ' vendor_' . $vendorIDsArr[$key] . '  form-control notranslate">
                  <option value="">Select</option>';
					//onChange="checkUniqScoreEdit(this,'.$i.');"

					for ($j = 1; $j <= count($quoteVendors); $j++) {
						$bodyEdit .= '<option value="' . $j . '" ' . ($scoreInfo == $j ? 'selected="SELECTED"' : '') . ' class="score_criteria">' . $j . '</option>';
					}
					$bodyEdit .= '</select> <span></span></td>';
				}

				$bodyEdit .= '</tr></tbody></table>';
			}
		}
		echo $bodyEdit;
		exit;
	}

	public function actionCreateByModal()
	{
		// get data for the selected vendor

		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_edit';

		unset(Yii::app()->session['max_score_id']);
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$quote_id = 0;
		$this->getCurrency($user_currency);
		$view_data = array('quote_id' => $quote_id);
		$quote = new Quote();
		$view_data['quote'] = $quote->getOne(array('quote_id' => $quote_id));
		if (!$view_data['quote'] || !is_array($view_data['quote'])) $quote_id = 0;

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

		$user = new User();
		$view_data['users'] = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

		// and also product and supplier information for the quote
		$view_data['quote_details'] = $quote->getQuoteDetails($quote_id);
		$view_data['quote_vendors'] = $quote->getQuoteVendors($quote_id);
		if (isset($view_data['quote_vendors'][0]['vendor_id']) && !empty($view_data['quote_vendors'][0]['vendor_id'])) {
			$vendor_id = $view_data['quote_vendors'][0]['vendor_id'];
		} else {
			$vendor_id = 0;
		}
		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();
		$view_data['quote_files'] = $quote->getQuoteFiles($quote_id);
		$view_data['quote_questions'] = $quote->getQuoteQuestions($quote_id);
		$view_data['quote_stacked_bar_graph'] = $quote->getQuoteGraph($quote_id, '');
		$view_data['quote_answer_line_graph'] = $quote->getQuoteAnswer($quote_id, '');
		$view_data['quote_overall_stats'] = $quote->getQuoteOverAllStats($quote_id);
		$view_data['scoringCriteria'] = $quote->scoringCriteria($quote_id);

		$category = new Category();
		$view_data['categories'] = $category->getAll(array('soft_deleted' => "0"));
		//$view_data['quote_vendor_files'] = $quote->getVendorFiles($quote_id);
		unset(Yii::app()->session['uploaded_files']);
		$this->renderPartial('create_quote_modal', $view_data);
	}

	public function actionCustomSocreAndQuestion()
	{

		$sql = "SELECT max(vendor_scoring_id) as max_score_id FROM quote_scoring";
		$maxSocreReader = Yii::app()->db->createCommand($sql)->queryRow();
		if (!isset(Yii::app()->session['max_score_id'])) {
			Yii::app()->session['max_score_id'] = $maxSocreReader['max_score_id'] + 1;
			$maxScoreID = Yii::app()->session['max_score_id'];
		} else {
			$maxScoreID = Yii::app()->session['max_score_id'] + 1;
			Yii::app()->session['max_score_id'] = $maxScoreID;
		}
		$view_data = array();
		$view_data['rowNumber'] = $maxScoreID;
		$this->renderPartial('custom_score', $view_data);
	}

	public function actionCustomSocreAndQuestionDraft()
	{

		$sql = "SELECT max(vendor_scoring_id) as max_score_id FROM quote_scoring";
		$maxSocreReader = Yii::app()->db->createCommand($sql)->queryRow();
		if (!isset(Yii::app()->session['max_score_id'])) {
			Yii::app()->session['max_score_id'] = $maxSocreReader['max_score_id'] + 1;
			$maxScoreID = Yii::app()->session['max_score_id'];
		} else {
			$maxScoreID = Yii::app()->session['max_score_id'] + 1;
			Yii::app()->session['max_score_id'] = $maxScoreID;
		}
		$view_data = array();
		$view_data['rowNumber'] = $maxScoreID;
		$view_data['scoreId'] = $_POST['scoreId'];
		$this->renderPartial('draft_edit_quote/custom_score', $view_data);
	}


	public function actionCreateByModalQ()
	{
		// get data for the selected vendor

		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_edit';

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$quote_id = 0;
		$this->getCurrency($user_currency);
		$view_data = array('quote_id' => $quote_id);
		$quote = new Quote();
		$view_data['quote'] = $quote->getOne(array('quote_id' => $quote_id));
		if (!$view_data['quote'] || !is_array($view_data['quote'])) $quote_id = 0;

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));

		// and also product and supplier information for the quote
		$view_data['quote_details'] = $quote->getQuoteDetails($quote_id);
		$view_data['quote_vendors'] = $quote->getQuoteVendors($quote_id);
		if (isset($view_data['quote_vendors'][0]['vendor_id']) && !empty($view_data['quote_vendors'][0]['vendor_id'])) {
			$vendor_id = $view_data['quote_vendors'][0]['vendor_id'];
		} else {
			$vendor_id = 0;
		}
		$view_data['quote_files'] = $quote->getQuoteFiles($quote_id);
		$view_data['quote_questions'] = $quote->getQuoteQuestions($quote_id);
		$view_data['quote_stacked_bar_graph'] = $quote->getQuoteGraph($quote_id, '');
		$view_data['quote_answer_line_graph'] = $quote->getQuoteAnswer($quote_id, '');
		$view_data['quote_overall_stats'] = $quote->getQuoteOverAllStats($quote_id);
		$view_data['scoringCriteria'] = $quote->scoringCriteria($quote_id);
		//$view_data['quote_vendor_files'] = $quote->getVendorFiles($quote_id);
		unset(Yii::app()->session['uploaded_files']);
		$this->render('create_quote_modal_q', $view_data);
	}

	public function actionClone($quote_id = 0)
	{
		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_edit';

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);

		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted')) {
			// save actual quote data
			$quote = new Quote();
			$quote->saveData();
			$quote_id = $quote->rs['quote_id'];

			// files uploaded if any
			$total_documents = Yii::app()->input->post('total_documents');
			for ($idx = 1; $idx <= $total_documents; $idx++) {
				$delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idx);
				if ($delete_document_flag) continue;
				if (isset($_FILES['quote_file_' . $idx]) && is_array($_FILES['quote_file_' . $idx])) {
					if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
					if (!is_dir('uploads/quotes/' . $quote_id))
						mkdir('uploads/quotes/' . $quote_id);

					if (!isset($_FILES['quote_file_' . $idx]['error']) || empty($_FILES['quote_file_' . $idx]['error'])) {
						$file_name = $_FILES['quote_file_' . $idx]['name'];
						move_uploaded_file($_FILES['quote_file_' . $idx]['tmp_name'], 'uploads/quotes/' . $quote_id . '/' . $file_name);

						$file_description = Yii::app()->input->post('file_desc_' . $idx);
						$quote->saveQuoteFile($quote_id, $file_name, $file_description);
					}
				}
			}
			// products in the quote
			$product_names = Yii::app()->input->post('product_name');
			$quantities = Yii::app()->input->post('quantity');
			$uoms = Yii::app()->input->post('uom');
			$current_prices = Yii::app()->input->post('current_price');
			$product_code = Yii::app()->input->post('product_code');
			$quote->saveProducts($quote_id, $product_names, $quantities, $uoms, $current_prices, $product_code);

			// and suppliers invited to send bid
			$vendor_ids = array();
			$total_vendors = Yii::app()->input->post('vendor_count');
			for ($vendor_idx = 1; $vendor_idx <= $total_vendors; $vendor_idx++) {
				$delete_vendor_flag = Yii::app()->input->post('delete_vendor_flag_' . $vendor_idx);
				if (!$delete_vendor_flag) {
					$selected_vendor_id = Yii::app()->input->post('vendor_name_' . $vendor_idx);
					if ($selected_vendor_id) $vendor_ids[] = $selected_vendor_id;
				}
			}

			// $vendor_ids = Yii::app()->input->post('vendor_id');
			$already_invited = $quote->saveVendors($quote_id, $vendor_ids);

			// may be supplier details were updated?
			$vendor = new Vendor();
			foreach ($vendor_ids as $a_vendor_id) {
				$vendor->rs = array();

				$vendor_name = Yii::app()->input->post('vendor_name_' . $a_vendor_id);
				if (!empty($vendor_name)) $vendor->rs['vendor_name'] = $vendor_name;

				$contact_name = Yii::app()->input->post('vendor_contact_' . $a_vendor_id);
				if (!empty($contact_name)) $vendor->rs['contact_name'] = $contact_name;

				$address_1 = Yii::app()->input->post('vendor_address_1_' . $a_vendor_id);
				if (!empty($address_1)) $vendor->rs['address_1'] = $address_1;

				$address_2 = Yii::app()->input->post('vendor_address_2_' . $a_vendor_id);
				if (!empty($address_2)) $vendor->rs['address_2'] = $address_2;

				$city = Yii::app()->input->post('vendor_city_' . $a_vendor_id);
				if (!empty($city)) $vendor->rs['city'] = $city;

				$state = Yii::app()->input->post('vendor_state_' . $a_vendor_id);
				if (!empty($state)) $vendor->rs['state'] = $state;

				$zip = Yii::app()->input->post('vendor_zip_' . $a_vendor_id);
				if (!empty($zip)) $vendor->rs['zip'] = $zip;

				$emails = Yii::app()->input->post('vendor_email_' . $a_vendor_id);
				if (!empty($emails)) $vendor->rs['emails'] = $emails;

				$phone_1 = Yii::app()->input->post('vendor_phone_' . $a_vendor_id);
				if (!empty($phone_1)) $vendor->rs['phone_1'] = $phone_1;

				if (count($vendor->rs)) {
					$vendor->rs['vendor_id'] = $a_vendor_id;
					$vendor->write();
				}
			}

			// questions if sent for suppliers

			//$quote->deleteQuestions($quote_id);
			for ($i = 1; $i <= Yii::app()->input->post('total_questions'); $i++) {

				if (!empty(Yii::app()->input->post('quote_question_' . $i))) {
					$qouteQuestion = new QuoteQuestion();
					$qouteQuestion->rs = array();
					$qouteQuestion->rs['quote_id'] = $quote_id;
					$qouteQuestion->rs['question'] = Yii::app()->input->post('quote_question_' . $i);
					$qouteQuestion->rs['question_type'] = Yii::app()->input->post('question_type_' . $i);
					$qouteQuestion->write();

					$question_id = $qouteQuestion->rs['id'];

					if (Yii::app()->input->post('question_type_' . $i) == 'yes_or_no') {
						$qouteAnswer = new QuoteAnswer();
						$qouteAnswer->rs = array();
						$qouteAnswer->rs['quote_id'] = $quote_id;
						$qouteAnswer->rs['question_id'] = $question_id;
						$qouteAnswer->rs['yes'] = Yii::app()->input->post('yes_' . $i);
						$qouteAnswer->rs['no'] = Yii::app()->input->post('no_' . $i);
						$qouteAnswer->write();
					} elseif (Yii::app()->input->post('question_type_' . $i) == 'multiple_choice') {

						for ($j = 1; $j <= 10; $j++) {
							if (!empty(Yii::app()->input->post('quote_answer_' . $i . '_' . $j))) {

								$qouteAnswer = new QuoteAnswer();
								$qouteAnswer->rs = array();
								$qouteAnswer->rs['quote_id'] = $quote_id;
								$qouteAnswer->rs['question_id'] = $question_id;
								$qouteAnswer->rs['answer'] = Yii::app()->input->post('quote_answer_' . $i . '_' . $j);
								if (Yii::app()->input->post('answer_' . $i . '_' . $j) == 'Excellent') {
									$xcellent = "Excellent";
								} else {
									$xcellent = "";
								}
								if (Yii::app()->input->post('answer_' . $i . '_' . $j) == 'Good') {
									$good = "Good";
								} else {
									$good = "";
								}
								if (Yii::app()->input->post('answer_' . $i . '_' . $j) == 'Okay') {
									$ok = "Okay";
								} else {
									$ok = "";
								}
								if (Yii::app()->input->post('answer_' . $i . '_' . $j) == 'Bad') {
									$bad = "Bad";
								} else {
									$bad = "";
								}
								if (Yii::app()->input->post('answer_' . $i . '_' . $j) == 'Very Bad') {
									$very_bad = "Very Bad";
								} else {
									$very_bad = "";
								}
								$qouteAnswer->rs['excellent'] = $xcellent;
								$qouteAnswer->rs['good'] = $good;
								$qouteAnswer->rs['okay'] = $ok;
								$qouteAnswer->rs['bad'] = $bad;
								$qouteAnswer->rs['very_bad'] = $very_bad;
								$qouteAnswer->write();
							}
						}
					} else {

						$qouteAnswer = new QuoteAnswer();
						$qouteAnswer->rs = array();
						$qouteAnswer->rs['quote_id'] = $quote_id;
						$qouteAnswer->rs['question_id'] = $question_id;
						$qouteAnswer->rs['free_text'] = "Free Text";
						$qouteAnswer->write();
					}
				}
			}

			$quote_data = $quote->getOne(array('quote_id' => $quote_id));
			$quote->setInternalUsersNotification($quote_data, $vendor_ids);
			$quote->sendUserNotification($quote_data);
			//$quote_question = Yii::app()->input->post('quote_question_' . $idx);
			//if (!empty($quote_question)) $quote->saveQuoteQuestion($quote_id, $quote_question);

			// finally if the quote is submitted, then send emails to suppliers

			if ((isset($_POST['quote_status']) && strtolower($_POST['quote_status']) == 'pending') || Yii::app()->input->post('invite_suppliers')) {
				//echo 'Here....';die;
				$quote->sendInvites($quote_id, $already_invited);
				$this->redirect(AppUrl::bicesUrl('quotes/edit/' . $quote_id));
			}
		}

		// get data for the selected vendor
		$view_data = array('quote_id' => $quote_id);
		$quote = new Quote();
		$view_data['quote'] = $quote->getOne(array('quote_id' => $quote_id));
		if (!$view_data['quote'] || !is_array($view_data['quote'])) $quote_id = 0;

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));

		// and also product and supplier information for the quote
		$view_data['quote_details'] = $quote->getQuoteDetails($quote_id);
		$view_data['quote_vendors'] = $quote->getQuoteVendors($quote_id);
		if (isset($view_data['quote_vendors'][0]['vendor_id']) && !empty($view_data['quote_vendors'][0]['vendor_id'])) {
			$vendor_id = $view_data['quote_vendors'][0]['vendor_id'];
		} else {
			$vendor_id = 0;
		}
		$view_data['quote_files'] = $quote->getQuoteFiles($quote_id);
		$view_data['quote_questions'] = $quote->getQuoteQuestions($quote_id);
		$view_data['quote_stacked_bar_graph'] = $quote->getQuoteGraph($quote_id, '');
		$view_data['quote_answer_line_graph'] = $quote->getQuoteAnswer($quote_id, '');
		$view_data['quote_overall_stats'] = $quote->getQuoteOverAllStats($quote_id);
		//$view_data['quote_vendor_files'] = $quote->getVendorFiles($quote_id);

		// and display
		$this->render('clone', $view_data);
	}

	public function actionQuestionnaire()
	{

		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_questionnaire';
		$quote = new Quote();
		$view_data['quote_questions'] = $quote->getQuoteQuestions($_GET['id']);

		// and display
		$this->render('questionnaire', $view_data);
	}


	public function actionGetVendorQuote()
	{
		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_get_vendor_quote';
		// get list of all orders (with pagination of course)
		$quote = new Quote();
		// get orders as per filters applied
		$view_data = array();
		$vendor_id = Yii::app()->input->post('vendor_id');
		$quote_id = Yii::app()->input->post('quote_id');
		$quote_found = false;
		$view_data['quote'] = $quote->getOne(array('quote_id' => $quote_id));
		if ($view_data['quote'] && is_array($view_data['quote'])) {
			$view_data['quote_id'] = $quote_id;
			$view_data['vendor_id'] = $vendor_id;
			$view_data['quote_details'] = $quote->getQuoteDetails($quote_id, $vendor_id);
			$view_data['quote_questions'] = $quote->getQuoteQuestions($quote_id);
			if (is_array($view_data['quote_details']) && count($view_data['quote_details'])) $quote_found = true;

			$view_data['quote'] = array_merge($view_data['quote'], $quote->getVendorQuote($quote_id, $vendor_id));
			if (!isset($view_data['quote']['submit_status']) || !$view_data['quote']['submit_status']) $quote_found = false;
		}

		if ($quote_found) $this->renderPartial('vendor_quote_details', $view_data);
		else echo '<br /><div style="margin-left: 15px;">This supplier has not yet replied for this quote request as yet</div><br />';
	}

	public function actionGetVendorEvaluation()
	{
		
		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_get_vendor_quote';

		// get list of all orders (with pagination of course)
		$quote = new Quote();

		// get orders as per filters applied
		$view_data = array();
		$vendor_id = Yii::app()->input->post('vendor_id');
		$quote_id = Yii::app()->input->post('quote_id');
		
		$quote_found = false;
		$view_data['quote'] = $quote->getOne(array('quote_id' => $quote_id));
		if ($view_data['quote'] && is_array($view_data['quote'])) {
			$view_data['quote_id'] = $quote_id;
			$view_data['vendor_id'] = $vendor_id;
			$view_data['quote_stacked_bar_graph'] = $quote->getQuoteGraph($quote_id, $vendor_id);
			$view_data['quote_answer_line_graph'] = $quote->getQuoteAnswer($quote_id, $vendor_id);
			$view_data['quote_overall_stats'] = $quote->getQuoteOverAllStats($quote_id);
			$quote_found = true;
		}

		if ($quote_found) $this->renderPartial('vendor_quote_evaluation', $view_data);
		else echo '<br /><div style="margin-left: 15px;">This supplier has not yet replied for this quote request as yet</div><br />';
	}

	public function actionDeleteFile()
	{
		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';

		// which order record the file relates to?
		$quote_id = Yii::app()->input->post('quote_id');
		$file_name = Yii::app()->input->post('file_name');

		// directory where this file resides
		$complete_file_name = 'uploads/quotes/' . $quote_id . '/' . $file_name;
		if (is_file($complete_file_name)) @unlink($complete_file_name);

		// and also description from the database
		$quote = new Quote();
		$quote->deleteFile($quote_id, $file_name);
	}


	public function actionDeleteVendorFile()
	{
		// which order record the file relates to?
		$quote_id = Yii::app()->input->post('quote_id');
		$vendor_id = Yii::app()->input->post('vendor_id');
		$file_name = Yii::app()->input->post('file_name');

		// directory where this file resides
		$complete_file_name = 'uploads/quotes/' . $quote_id . '/' . $vendor_id . '/' . $file_name;
		if (is_file($complete_file_name)) @unlink($complete_file_name);
		echo $complete_file_name;
	}

	public function actionGetVendorDetails()
	{
		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';
		// which vendor details are needed?
		$vendor_ids = Yii::app()->input->post('vendor_ids');
		if (!empty($vendor_ids) && !empty(Yii::app()->input->post('quote_id'))) {
			$quote_id = Yii::app()->input->post('quote_id');
			$quote = new Quote();
			$quoteVendor = $quote->getQuoteVendorInfo($vendor_ids, $quote_id);
			if (!empty($quoteVendor)) {
				echo json_encode($quoteVendor);
			} else {
				$vendor = new Vendor();
				echo json_encode($vendor->getData(array('vendor_id IN' => explode(",", $vendor_ids))));
			}
		} else if (!empty($vendor_ids)) {
			$vendor = new Vendor();
			echo json_encode($vendor->getData(array('vendor_id IN' => explode(",", $vendor_ids))));
		} else echo json_encode(array());
	}


	public function actionProductImport()
	{
		$file_name = $_FILES['file']['tmp_name'];
		$fileReader = fopen($file_name, "r");
		$view_data = array();
		$view_data['fileReader'] = $fileReader;
		$this->renderPartial('_product', $view_data);
		exit;
	}

	public function actionProductSample()
	{

		$file = Yii::app()->basePath . "/../uploads/samples/product_sample.csv";

		if ($file) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
			header('Content-Disposition: attachment; filename="' . basename($file) . '"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}

	public function actionExportChart($quote_id, $type_id)
	{

		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'export_chart';


		$quote = new Quote();
		$view_data['quote_vendors'] = $quote->getQuoteVendors($quote_id);
		if (isset($view_data['quote_vendors'][0]['vendor_id']) && !empty($view_data['quote_vendors'][0]['vendor_id'])) {
			$vendor_id = $view_data['quote_vendors'][0]['vendor_id'];
		} else {
			$vendor_id = 0;
		}

		if ($type_id == 1) {
			$dimension_name = 'Products';
			$export_data = $quote->getVendorProducts($quote_id, $vendor_id);

			$quote_stacked_bar_graph = $quote->getQuoteGraph($quote_id, '');
			$heatMapSeries = '';
			$vendor = array_unique($quote_stacked_bar_graph['vendor_name']);
			$idx = 0;
			$tool_currency = Yii::app()->session['user_currency'];
			if (!empty($quote_stacked_bar_graph)) {
				$vendorProduct = $quote_stacked_bar_graph['vendorProduct'];
				asort($vendorProduct);
				$exportable_data_row = array();
				$exportable_data_row[] = "Product Title";
				foreach ($vendor as $vendor_name)
					$exportable_data_row[] = $vendor_name;
				$exportable_data[] = $exportable_data_row;

				foreach ($vendorProduct as $product_name => $vendorInfo) {
					$exportable_data_row = array();
					$exportable_data_row[] = $product_name;

					foreach ($vendor as $vendor_name) {
						$productPrice = $vendorProduct[$product_name][$vendor_name];
						$productPrice = number_format($productPrice * FunctionManager::currencyRate($tool_currency), 0, ".", "");
						$exportable_data_row[] = $productPrice;
					}
					$exportable_data[] = $exportable_data_row;
				}
			}
		} else {
			$dimension_name = 'Answers';
			$export_data = $quote->getQuoteAnswer($quote_id, $vendor_id);
			if (count($export_data) > 0) {
				foreach ($export_data['data'] as $export_row) {
					$exportable_data_row = array();

					$exportable_data_row['Question'] = $export_row['question'];
					if ($export_row['question_type'] == 'free_text') {
						$exportable_data_row['Answer'] = $export_row['free_text'];
					} elseif ($export_row['question_type'] == 'yes_or_no') {
						$exportable_data_row['Answer'] = $export_row['yes_no'];
					} else {
						$answer = $quote->getAnswer($export_row['quote_answer']);
						$exportable_data_row['Answer'] = $answer['answer'];
					}


					$exportable_data[] = $exportable_data_row;
				}
			}
		}


		$export = new Common();
		$export->export($exportable_data, $dimension_name);
	}

	public function actionChangeQuote($id)
	{
		if (empty($id)) {
			Yii::app()->user->setFlash('success', "Cancelled Successfully");
			$this->redirect(AppUrl::bicesUrl('quotes/edit'));
		} else {
			$update_sql = "UPDATE  quotes
							  SET quote_status = 'Cancelled',
							  	  cancelled_date = '" . date("Y-m-d H:i:s") . "' 
							WHERE quote_id = $id AND quote_status != 'Cancelled'";
			$updatedReader = Yii::app()->db->createCommand($update_sql)->execute();

			if (!empty($updatedReader)) {

				Yii::app()->user->setFlash('success', "Quote #" . $id . " Cancelled Successfully");

				$sql  = 'select created_by_user_id,commercial_lead_user_id,quote_id,quote_name,closing_date from quotes where  quote_id="' . $id . '"';
				$quoteReader = Yii::app()->db->createCommand($sql)->query()->readAll();

				if (!empty($quoteReader)) {
					foreach ($quoteReader as $quoteInfo) {
						$expiryDate           = $quoteInfo['closing_date'];
						$quoteName            = $quoteInfo['quote_name'];
						$notificationQuoteID  = $quoteInfo['quote_id'];
						$user_id              = $quoteInfo['created_by_user_id'];
						$secondary_id         = $quoteInfo['commercial_lead_user_id'];
						$userArr              = array($user_id, $secondary_id);

						$notificationComments  = "<b>" . $notificationQuoteID . "- " . $quoteName . "</b> has been cancelled";

						foreach ($userArr as $value) {
							if (!empty($value)) {
								$notification = new Notification();
								$notification->rs = array();
								$notification->rs['id'] = 0;
								$notification->rs['user_type'] = 'Client';
								$notification->rs['notification_flag'] = '';
								$notification->rs['user_id'] = $value;
								$notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $notificationQuoteID) . '">' . addslashes($notificationComments) . '</a>';
								$notification->rs['notification_date'] = date("Y-m-d H:i");
								$notification->rs['read_flag'] = 0;
								$notification->write();
							}
						}
					}
					//
					$sql = "SELECT * FROM quote_vendors where quote_id=" . $id;
					$vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
					foreach ($vendorReader as $value) {
						$cronEmail = new CronEmail;
						$cronEmail->rs = array();
						$cronEmail->rs['record_id'] = $id;
						$cronEmail->rs['send_to'] = 'Vendor';
						$cronEmail->rs['user_id'] = $value['vendor_id'];
						$cronEmail->rs['user_type'] = 'Cancellation Quote';
						$cronEmail->rs['status'] = 'Pending';
						$cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
						$cronEmail->write();
					}
				}
			} else {
				Yii::app()->user->setFlash('error', "Quote #" . $id . ", found some problem, please, contact to system admin.");
			}
			$this->redirect(AppUrl::bicesUrl('quotes/edit/' . $id));
		}
	}

	public function actionScoreDefaultValue()
	{
		$data = array();
		$quote = new Quote();
		$scoringCriteria = $quote->scoringCriteria();
		$scoreValueArr = array();
		foreach ($scoringCriteria as $value) {
			$scoreValueArr[$value['id']] = number_format($value['score'], 0);
		}
		$data['score'] = $scoreValueArr;
		echo json_encode($data);
	}

	public function actionUploadDocument()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		if (!empty($_FILES)) {
			$quote = new Quote;
			if (isset($_FILES['file']) && is_array($_FILES['file'])) {
				$quoteID = $_POST['quote_id'];
				if (empty($quoteID)) {
					$quoteID = '';
				}
				$path = Yii::getPathOfAlias('webroot') . '/uploads/quote_comm_documents';
				if (!is_dir($path)) mkdir($path);
				if (!is_dir($path . '/' . $quoteID))
					mkdir($path . '/' . $quoteID);
				if (!is_dir($path . '/'))
					mkdir($path . '/');

				if (!empty($_FILES['file']['name'])) {
					$file_name = time() . '_' . $_FILES['file']['name'];
					$file_name = str_replace("'", "", $file_name);
					$file_name = str_replace('"', '', $file_name);
					$file_name = str_replace(' ', '_', $file_name);
					$file_description = addslashes($_POST['file_description']);
					$document_type = $_POST['document_type'];
					$document_status = !empty($_POST['document_status']) ? $_POST['document_status'] : 0;
					if (move_uploaded_file($_FILES['file']['tmp_name'], $path . '/' . ($quoteID . '/' . $file_name))) {
						if (!empty($quoteID)) {
							$quote->saveQuoteDocument($quoteID, $file_description, $file_name, $document_type, $document_status);
							$documentList = $quote->getQuoteDocuments($quoteID);
						}
					}
					$quoteDetail = $quote->getOne(array('quote_id' => $quoteID));
					$location_id = $quoteDetail['location_id'];
					$department_id = $quoteDetail['department_id'];
					$this->renderPartial('_documents', array('documentList' => $documentList, 'quote_id' => $quoteID));
				}
			}
		}
	}

	public function actionApproveDocument()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		error_reporting(0);
		$quote = new Quote;
		if (null != Yii::app()->input->post('document_ids')) {
			$documentIDs = Yii::app()->input->post('document_ids');
			$quote->approveDocument($documentIDs);
			$quote_id = Yii::app()->input->post('quote_id');
			$quoteDetail = $quote->getOne(array('quote_id' => $quote_id));
			$location_id = $quoteDetail['location_id'];
			$department_id = $quoteDetail['department_id'];
			$this->renderPartial('_documents', array('documentList' => $quote->getQuoteDocuments($quote_id), 'quote_id' => $quote_id));
		}
	}


	public function actionDocumentDownload()
	{
		$id = $_GET['id'];
		$sql = "select document_file,quote_id from quote_communication WHERE id =" . $id;
		$fileReader = Yii::app()->db->createCommand($sql)->queryRow();
		$quoteID = $fileReader['quote_id'];

		$file = Yii::app()->basePath . "/../uploads/quote_comm_documents/" . $quoteID . "/" . $fileReader['document_file'];
		if ($fileReader) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
			header('Content-Disposition: attachment; filename="' . basename($file) . '"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}

	public function actionDeleteDocument()
	{
		$id = $_POST['documentID'];
		$quoteID = 0;
		//$sql = "select document_file,vendor_id from vendor_documents WHERE status='Pending' and id =".$id;
		$sql = "select document_file,quote_id from quote_communication WHERE id =" . $id;
		$fileReader = Yii::app()->db->createCommand($sql)->queryRow();
		if (!empty($fileReader)) {
			$quoteID = $fileReader['quote_id'];
			$fileName = $fileReader['document_file'];
			$file = Yii::app()->basePath . "/../uploads/quote_comm_documents/" . $quoteID . "/" . $fileName;
			if (unlink($file)) {
				//$sql = "delete from vendor_documents WHERE status='Pending' and id =".$id;
				$sql = "delete from quote_communication WHERE  id =" . $id;
				$fileReader = Yii::app()->db->createCommand($sql)->execute();
			}
		}
		$quote = new Quote;

		$quoteDetail = $quote->getOne(array('quote_id' => $quoteID));
		$location_id = $quoteDetail['location_id'];
		$department_id = $quoteDetail['department_id'];
		$this->renderPartial('_documents', array('documentList' => $quote->getQuoteDocuments($quoteID), 'quote_id' => $quoteID, 'location_id' => $location_id, 'department_id' => $department_id));
	}

	public function actionUploadDocumentModal()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$uploaded = 0;
		if (!empty($_FILES)) {
			$quote = new Quote;
			if (isset($_FILES['file']) && is_array($_FILES['file'])) {

				$path = Yii::getPathOfAlias('webroot') . '/uploads/quotes';
				if (!is_dir($path)) mkdir($path);
				if (!is_dir($path . '/temp'))
					mkdir($path . '/temp');
				$path .= '/temp';
				if (!is_dir($path . '/'))
					mkdir($path . '/');

				if (!empty($_FILES['file']['name'])) {
					/*$file_name = time().'_'.$_FILES['file']['name'];
                    $file_description = $_POST['file_description'];*/

					$file_name = time() . '_' . $_FILES['file']['name'];
					$file_name = str_replace("'", "", $file_name);
					$file_name = str_replace('"', '', $file_name);
					$file_name = str_replace(' ', '_', $file_name);
					$file_description = addslashes($_POST['file_description']);

					if (move_uploaded_file($_FILES['file']['tmp_name'], $path . '/' . $file_name)) {
						$uploadedFiles = Yii::app()->session['uploaded_files'];
						$uploadedFiles[] = array('file_name' => $file_name, 'file_description' => $file_description);
						Yii::app()->session['uploaded_files'] = $uploadedFiles;
						$uploaded = 1;
					}
				}
			}
		}
		$data = array();
		$quoteFile = Yii::app()->session['uploaded_files'];

		$exp = "<table class='table table-striped table-bordered' style='width: 100%;'><tr><th>File</th><th>Description</th><th>Action</th></tr>";
		foreach ($quoteFile as $key => $value) {
			$deleteFunc = 'deleteDocumentModal("' . $key . '")';
			$exp .= "<tr><td>" . $value['file_name'] . "</td><td class='notranslate'>" . $value['file_description'] . "</td><td><a style='cursor: pointer; padding: 5px;' onclick='" . $deleteFunc . "'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a></td></tr>";
		}
		$exp .= "</table>";
		$data['uploaded'] = $uploaded;
		$data['uploaded_list'] = $exp;
		echo json_encode($data);
		exit;
	}
	public function actionQuoteDocumentsDraft()
	{
		$quote_id = Yii::app()->session['quote_id'];
		$sql = "select * from quote_files where quote_id=" . $quote_id;
		$data = Yii::app()->db->createCommand($sql)->queryAll();
		echo json_encode($data);
		exit;
	}
	public function actionDeleteDocumentModal()
	{
		$key = $_POST['documentKey'];
		$uploaded = '';
		$uploadedFiles = Yii::app()->session['uploaded_files'];
		$deleted = $uploadedFiles[$key];
		unset($uploadedFiles[$key]);
		Yii::app()->session['uploaded_files'] = $uploadedFiles;



		$file = Yii::getPathOfAlias('webroot') . "/uploads/quotes/temp/" . $deleted['file_name'];
		if (unlink($file)) {
			$uploaded = 1;
		}

		$exp = "<table class='table table-striped table-bordered' style='width: 100%;'><tr><th>File</th><th>Description</th><th>Action</th></tr>";
		foreach ($uploadedFiles as $key => $value) {
			$deleteFunc = 'deleteDocumentModal("' . $key . '")';
			$deleteID = "doc_modal_$key";
			$exp .= "<tr><td>" . $value['file_name'] . "</td><td>" . $value['file_description'] . "</td><td><a id='" . $deleteID . "' style='cursor: pointer; padding: 5px;' onclick='" . $deleteFunc . "'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a></td></tr>";
		}
		$exp .= "</table>";
		$data['uploaded'] = $uploaded;
		$data['uploaded_list'] = $exp;
		echo json_encode($data);
		exit;
	}

	public function actionSaveDetailDesc()
	{
		$this->checklogin();
		$this->layout = 'main';

		$quoteId = $_POST['quote_id'];
		$detail_description = !empty($_POST['detail_dsc']) ? addslashes($_POST['detail_dsc']) : '';

		$sql = 'update quotes set detail_description="' . $detail_description . '" where quote_id=' . $quoteId;
		$quotesDetailDesc = Yii::app()->db->createCommand($sql)->execute();

		if (!empty($quotesDetailDesc)) {
			$msg = 1;
		}
		echo json_encode(array('msg' => $msg));
	}

	// @basheer alam 13/12/2022
	private function BtnColor($style = [])
	{
		return "style='background:" . $style[0] . ";border-color:" . $style[1] . "' ";
	}

	public function actionChangeQuoteStatusAndUpdate()
	{
		$status = 'draft';
		$quoteID = $_POST['quote_id'];
		if (empty($quoteID)) {
			$this->redirect(AppUrl::bicesUrl('quotes/list'));
		} else {
			$update_sql = "UPDATE  quotes
			  SET quote_status = 'Pending' 
			  WHERE quote_id = $quoteID AND quote_status = 'Draft'";
			$updatedReader = Yii::app()->db->createCommand($update_sql)->execute();

			if (!empty($updatedReader)) {
				$quote = new Quote();
				$quote_data = $quote->getOne(array('quote_id' => $quoteID));
				$quote->setVendorNotification($quote_data, $vendor_ids = '');
				Yii::app()->user->setFlash('success', "Quote #" . $quoteID . " Publish Successfully");
				//$this->redirect(AppUrl::bicesUrl('quotes/edit/' . $quoteID));
				$status = 'pushlished';
			}
			$this->questionAssignToSupplier($quoteID);
		}


		echo json_encode(array('status' => $status));
	}

	public function actionUploadDocumentDraft()
	{
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$uploaded = 0;
		$quote_id = $_POST['quoteId'];

		if (!empty($_FILES)) {
			$quote = new Quote;
			if (isset($_FILES['file']) && is_array($_FILES['file'])) {
				$path = Yii::getPathOfAlias('webroot') . '/uploads/quotes';

				if (!is_dir($path)) mkdir($path);
				if (!is_dir($path . '/temp'))
					mkdir($path . '/temp');
				$path .= '/temp';
				if (!is_dir($path . '/'))
					mkdir($path . '/');
				if (!empty($_FILES['file']['name'])) {
					$file_name = time() . '_' . $_FILES['file']['name'];
					$file_name = str_replace("'", "", $file_name);
					$file_name = str_replace('"', '', $file_name);
					$file_name = str_replace(' ', '_', $file_name);
					$file_description = addslashes($_POST['file_description']);

					if (move_uploaded_file($_FILES['file']['tmp_name'], $path . '/' . $file_name)) {
						$fileSource = Yii::getPathOfAlias('webroot') . "/uploads/quotes/temp/" . $file_name;
						if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
						if (!is_dir('uploads/quotes/' . $quote_id))
							mkdir('uploads/quotes/' . $quote_id);
						$fileTarget = Yii::getPathOfAlias('webroot') . "/uploads/quotes/$quote_id/" . $file_name;
						copy($fileSource, $fileTarget);
						$quote->saveQuoteFile($quote_id, $file_name, $file_description);
						unlink($fileSource);

						$comment = "";
						$comment .= '<b>File:</b> <span class="title-text">' . $file_name . '</b></span><br/>';
						$comment .= '<b>File Description:</b> <span class="title-text">' . $file_description . '</b></span><br/>';
						if (!empty($comment)) {
							$log = new QuoteLog();
							$log->rs = array();
							$log->rs['quote_id'] = $quote_id;
							$log->rs['user_id'] = Yii::app()->session['user_id'];
							$log->rs['comment'] = !empty($comment) ? $comment : "";
							$log->rs['created_datetime'] = date('Y-m-d H:i:s');
							$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
							$log->write();
						}
					}
				}
			}
		}
		echo json_encode(array('uploaded' => 1));
	}

	public function actionQuoteSaveAsDraft()
	{
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_edit';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		error_reporting(0);

		// if form was submitted ... first save the data to the database
		$quote_id = Yii::app()->input->post('quote_id');
		// IN update case this function call for quote logs 
		if (!empty($quote_id)) $this->quoteLogsInsert($quote_id);
		// Quote Comments Inserted Function
		if (!empty($_POST['quote_comments'])) $this->quoteCommentInsert($quote_id);
		// Quote Questionnaire Inserted Function
		if (count($_POST['question_form_id']) > 0) $this->quoteQuestionnaireInsert($quote_id, 'draft');

		if (!empty($quote_id)) {
			if (FunctionManager::dateFormat() == "d/m/Y") {
				$openingDate = date("Y-m-d H:i:s", strtotime(strtr($_POST['opening_date'], '/', '-')));
				$closingDate = date("Y-m-d H:i:s", strtotime(strtr($_POST['closing_date'], '/', '-')));
			} else {
				$openingDate = date("Y-m-d H:i:s", strtotime($_POST['opening_date']));
				$closingDate = date("Y-m-d H:i:s", strtotime($_POST['closing_date']));
			}
			
			//update actual quote data 
			$quote = new Quote();
			$oldRecord = $quote->getOne(['quote_id' => $quote_id]);
			$quoteUpdate = new Quote;
			$quoteUpdate->rs = array();
			$quoteUpdate->rs['quote_id']   = $quote_id;
			$quoteUpdate->rs['quote_name'] = !empty($_POST['quote_name']) ? $_POST['quote_name'] : $oldRecord['quote_name'];
			$quoteUpdate->rs['quote_desc'] = !empty($_POST['quote_desc']) ? $_POST['quote_desc'] : $oldRecord['quote_desc'];
			$quoteUpdate->rs['quote_currency'] = !empty($_POST['quote_currency']) ? $_POST['quote_currency'] : $oldRecord['quote_currency'];
			$quoteUpdate->rs['location_id']    = !empty($_POST['location_id'])   ? $_POST['location_id'] : $oldRecord['location_id'];
			$quoteUpdate->rs['department_id']  = !empty($_POST['department_id']) ? $_POST['department_id'] : $oldRecord['department_id'];
			$quoteUpdate->rs['category_id']    = !empty($_POST['category_id'])   ? $_POST['category_id'] : $oldRecord['category_id'];
			$quoteUpdate->rs['subcategory_id'] = !empty($_POST['subcategory_id']) ? $_POST['subcategory_id'] : $oldRecord['subcategory_id'];
			$quoteUpdate->rs['spend_type']    = !empty($_POST['spend_type']) 	? $_POST['spend_type'] : $oldRecord['spend_type'];
			$quoteUpdate->rs['opening_date']  = !empty($_POST['opening_date'])  ? $openingDate : date("Y-m-d H:i:s", strtotime(strtr($oldRecord['opening_date'], '/', '-')));
			$quoteUpdate->rs['closing_date']  = !empty($_POST['closing_date'])  ? $closingDate : date("Y-m-d H:i:s", strtotime(strtr($oldRecord['closing_date'], '/', '-')));
			$quoteUpdate->write();

			// Start: products in the quote In create and update case
			$product_names = Yii::app()->input->post('product_name');
			if (!empty($product_names)) {
				$quote->saveQuoteProducts($quote_id, $product_names);
			}

			// and suppliers invited to send bid
			$newVendors = !empty($_POST['vendor_name_new']) ? $_POST['vendor_name_new'] : '';
			$updatedVendors = !empty($_POST['vendor_name_update']) ? $_POST['vendor_name_update'] : '';

			if (!empty($updatedVendors)) {
				$quote->saveQuoteVendors($quote_id, $newVendors, $updatedVendors);
			}

			Yii::app()->user->setFlash('success', "Sourcing Activity Edited Successfully");
			$this->redirect(AppUrl::bicesUrl('quotes/edit/' . $quote_id));
		}
	}

	public function actionQuoteDocumentsDraftDelete()
	{
		$quote = new Quote();
		$filename = $_POST['filename'];
		$quote_id = Yii::app()->session['quote_id'];
		$sql = "SELECT file_name,description FROM quote_files WHERE quote_id=" . $quote_id;
		$fileName = Yii::app()->db->createCommand($sql)->queryRow();

		$comment = "";
		$comment .= '<b>File:</b> <span class="title-text">' . $fileName['file_name'] . '</b></span><br/>';
		$comment .= '<b>File Description:</b> <span class="title-text">' . $fileName['description'] . '</b></span><br/>';
		$comment .= '<span class="title-text"> Deleted By <b>' . Yii::app()->session['full_name'] . '</b></span><br/>';

		if (!empty($comment)) {
			$log = new QuoteLog();
			$log->rs = array();
			$log->rs['quote_id'] = $quote_id;
			$log->rs['user_id'] = Yii::app()->session['user_id'];
			$log->rs['comment'] = !empty($comment) ? $comment : "";
			$log->rs['created_datetime'] = date('Y-m-d H:i:s');
			$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
			$log->write();
		}

		
		$file = Yii::getPathOfAlias('webroot') . "/uploads/quotes/temp/" . $fileName['file_name'];
		$quote->deleteFile($quote_id, $filename);
		echo json_encode($done = 1);
		exit;
	}

	public function actionQuoteInternalUser()
	{	
		$output = '';
		if(!empty($_POST['quoteID'])){
			// START : This code is for quote Scorer, when we Add new scorer in Scoring tab
			$quoteScorerUser = [];
			$quoteQuestionnaire = new QuoteScorerUser();
  			$quoteQuestionnaire = $quoteQuestionnaire->getQuoteScorerQuestions($_POST['quoteID']); 
			foreach($quoteQuestionnaire as $quoteScorer){
				if(!in_array($quoteScorer['user_id'] ,$quoteScorerUser)){  $quoteScorerUser[] = $quoteScorer['user_id'];	}
			}

			if(!empty($quoteScorerUser)){
				$arrUser = implode(', ', $quoteScorerUser);
				$arrQuoteScorerUser = " and user_id not in(2,3," . $arrUser . ") ";
			}else{ $arrQuoteScorerUser =" and user_id not in(2,3) "; }

			if (!empty($_POST['selectedUserIds'])) {
				$arr = implode(', ', $_POST['selectedUserIds']);
				$userCondition = " and user_id not in(2,3, " . $arr . ") ".$arrQuoteScorerUser;
			} else { $userCondition = $arrQuoteScorerUser; }
			
			$sql = "select user_id,full_name from users  where 1 " . $userCondition . " order by full_name asc ";
			$users = Yii::app()->db->createCommand($sql)->query()->readAll();

			$output .= '<option>Select</option>';
			foreach ($users as $row) {
				$output .= '<option value="' . $row["user_id"] . '">' . $row["full_name"] . '</option>';
			}
			// END : This code is for quote Scorer, when we Add new scorer in Scoring tab
			// userID 2,3 is for oboloo owner. it's hidden for all domain 
		}else{
			
			if (!empty($_POST['selectedUserIds']) && 
				!in_array('Select', $_POST['selectedUserIds'])) {
				$result = array_filter($_POST['selectedUserIds'], function($value) {return $value !== ""; });
				$arr = implode(', ', $result);
				$userCondition = " and user_id not in(2,3, " . $arr . ") ";
			} else { $userCondition = " and user_id not in(2,3) "; }

			$sql = "select user_id,full_name from users  where 1 " . $userCondition . " order by full_name asc ";
			$users = Yii::app()->db->createCommand($sql)->query()->readAll();

			$output = [];
			foreach ($users as $row) {
				$output[] = [
					"userID"     => $row["user_id"],
					"full_name"  => $row["full_name"]
				];
			}
			// $output = '<option>Select</option>';
			// foreach ($users as $row) {
			// 	$output .= '<option value="' . $row["user_id"] . '">' . $row["full_name"] . '</option>';
			// }
		}

		echo json_encode($output);
		exit;
	}


	public function actionQuestionnaires($quote_id = 0)
	{
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'quotes_edit';
		error_reporting(0);
		$data = [];

		$quoteUser = new QuoteUser();
		$data['questionnaireuser']    = $quoteUser->getQuoteUser($quote_id, Yii::app()->session['user_id']);
		$data['questionnaireuserList'] = $quoteUser->getQuoteUserAll($quote_id);

		$this->render('questionnaire_user_section', $data);
	}

	public function actionMyScoring()
	{
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'myscoring';

		$this->render('scoring/myscoring');
	}

	public function actionMySourcingActivities()
	{
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
		$this->layout = 'main';
		$this->current_option = 'mysourcingactivities';

		$this->render('scoring/mysourcingactivities');
	}

	public function actionSourcingActivitiesListAjax()
	{
		$quote = new Quote();
		// get orders as per filters applied
		$location_id  = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$quote_status = '';
		$category_id  = Yii::app()->input->post('category_id');
		$subcategory_id = Yii::app()->input->post('subcategory_id');
		$from_date = Yii::app()->input->post('from_date');
		$to_date   = Yii::app()->input->post('to_date');
		$search_for = "";

		if (
			isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value'])
			&& !empty($_REQUEST['search']['value'])
		) {
			$search_for = $_REQUEST['search']['value'];
		}

		$order_by = array();
		if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])) {
			$order_by = $_REQUEST['order'];
		} else {
		}

		$quotes = $quote->getMySourcingActivities($_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $quote_status, $from_date, $to_date, $userID = Yii::app()->session['user_id']);
		$quoteArr = array();
		$i = 1;
		$subdomain = FunctionManager::subdomainByUrl();
		
		foreach ($quotes as $value) {
			$closeingDate = '';
			$notQuoteCancelled = strtolower($value['quote_status']) != 'cancelled';
			$quoteStatusNotDraft = $value['quote_status'] != 'Draft';
			$closing_date = date("Y-m-d H:i:s", strtotime($value['closing_date']));
			$contribute_deadline = date("Y-m-d H:i:s", strtotime($value['contribute_deadline']));
			$currentDate = date("Y-m-d H:i:s");

			if ($notQuoteCancelled && $quoteStatusNotDraft && $closing_date >= $currentDate && $value['awarded_vendor_id'] == 0) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
          		' . $this->BtnColor(['#f0ad4e', '#f0ad4e']) . '>In Progress</button>';
			} else if ($quoteStatusNotDraft && !empty($value['awarded_vendor_id'])) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
          		' . $this->BtnColor(['#1abb9c', '#1abb9c']) . '>Awarded</button>';
			} else if ($quoteStatusNotDraft && $closing_date < $currentDate) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
          		' . $this->BtnColor(['#6cce47', '#6cce47']) . '>Complete</button>';
			} else if (strtolower($value['quote_status']) == 'cancelled') {
				$closeingDate = '<button type="submit" class="btn btn-success" 
          		' . $this->BtnColor(['#fb5a5a !important', '#fb5a5a !important']) . '>Cancelled</button>';
			} else if (!$quoteStatusNotDraft) {
				$closeingDate = '<button type="submit" class="btn btn-danger" 
          	    ' . $this->BtnColor(['#fb5a5a', '#fb5a5a']) . '>Draft</button>';
			} else {
				$closeingDate = '';
			}

			$imgFullPath = '';
			if (!empty($value['profile_img'])) {
				$imgFullPath = Yii::app()->baseUrl . "/../images/" . $subdomain . "/users/" . $value['user_id'] . "/" . $value['profile_img'];
			} else if (!empty($value['user_id'])) {
				$imgFullPath = CommonFunction::userTextAvatar($value['full_name']);
			}
			if (!empty($imgFullPath)) {
				$imgFullPath = CHtml::image($imgFullPath);
			}
			
			$profileImg = '<div class="image-container">'.$imgFullPath.' </i><div class="profile-username"> '. $value['full_name'].' </div> </div> ';

			$current_data = array();
			$edit_link = '';
			if($value['contribute_status'] === '1' && $value['qu_userID'] === Yii::app()->session['user_id']){
			 $edit_link = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $value['quote_id']) . '">
			 				<button class="btn btn-sm btn-success view-btn text-white">Complete</button></a>';
			}
			else if (($value['quote_status'] == 'Draft' && $currentDate < $contribute_deadline) || ($value['quote_status'] == 'Draft' && $value['user_id'] === Yii::app()->session['user_id'])) {
				$edit_link = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $value['quote_id']) . '">
    				 	   <button class="btn btn-sm btn-success btn-green">Contribute</button></a>';
			} 
			else {
				$edit_link = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $value['quote_id']) . '" >
    				 	   <button class="btn btn-sm btn-pink">Closed</button></a>';
			}

			$getCrtburelineDate = "";
			if (date(FunctionManager::dateFormat() . " H:i A", strtotime($value['contribute_deadline'])) != '01/01/1970 00:00 AM') {
                if (isset($value['quote_id']) && !empty($value['quote_id'])) $getCrtburelineDate = date(FunctionManager::dateFormat() . " H:i A", strtotime($value['contribute_deadline']));
            } 

			$current_data[] = $edit_link;
			$current_data[] = $value['quote_id'];
			$current_data[] = $value['quote_name'];
			$current_data[] = $getCrtburelineDate;
			$current_data[] = date(FunctionManager::dateFormat() . " H:i A", strtotime($value['opening_date']));
			$current_data[] = date(FunctionManager::dateFormat() . " H:i A", strtotime($value['closing_date']));
			$current_data[] = $profileImg;
			$current_data[] = !empty($value['awarded_vendor_name']) ? $value['awarded_vendor_name'] : '';
			$current_data[] = $value['quote_currency'];
			$current_data[] = "<span style='display:block;text-align:center'>" . round($value['cost_savings']) . "</span>";
			$current_data[] = $closeingDate;
			$quoteArr[]   = $current_data;
			$i += 1;
		}

		$database_values_total = $quote->getMySourcingActivities('none', 'none', $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $quote_status, $from_date, $to_date, $userID = Yii::app()->session['user_id']);
		$total_quotes = count($database_values_total);
		$json_data = array(
			"draw"            => intval($_REQUEST['draw']),
			"recordsTotal"    => $total_quotes,
			"recordsFiltered" => $total_quotes,
			"data"            => $quoteArr
		);

		echo json_encode($json_data);
		exit;
	}


	public function actionAddQuoteUserForScoring()
	{	
	  //echo "<pre>"; print_r($_POST); echo "</pre>"; exit;
	  if(!empty($_POST['quote_id']) || !empty($_POST['quote_internal_scorer_user'])){
	   foreach($_POST['quote_internal_scorer_user'] as $scorerID){
		 $quote = new Quote();
		 $user = new User();
		 $quote = $quote->getOne(array('quote_id' => $_POST['quote_id']));
		 $user = $user->getOne(array('user_id' => $scorerID));
		 
		 $quoteScorer = new QuoteScorerUser();
		 $quoteScorer->rs = array();
		 $quoteScorer->rs['quote_id']  = $quote['quote_id'];
		 $quoteScorer->rs['user_id']   = $user['user_id'];
		 $quoteScorer->rs['user_name'] = $user['full_name'];
		 $quoteScorer->rs['creator_id']= $quote['created_by_user_id'];
		 $quoteScorer->rs['creator_user_name'] = $quote['created_by_name'];
		 $quoteScorer->rs['status'] = '';
		 $quoteScorer->rs['created_date'] = date("Y-m-d H:i:s");
		 $quoteScorer->write();

		 $notificationComments  = $quote['created_by_name']." has invited you to score the supplier responses for sourcing activity - <b> ".$quote['quote_name']."</b> ";
		 $notification = new Notification();
		 $notification->rs = array();
		 $notification->rs['id'] = 0;
		 $notification->rs['user_id'] = $scorerID;
		 $notification->rs['user_type'] = 'Client';
	 	 $notification->rs['notification_text'] = addslashes('<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote['quote_id']). '">'.$notificationComments.'</a>');
		 $notification->rs['notification_date'] = date("Y-m-d H:i");
		 $notification->rs['read_flag'] = 0;
		 $notification->rs['notification_type'] = 'Quote';
		 $notification->write();
	   }
	  } 

	 $url = AppUrl::bicesUrl('quotes/edit/' . $_POST['quote_id'].'?tab=scoring');
	 echo json_encode(['msg' => 1, 'redirectURL' => $url]);exit;
	}

	public function actionMyScoringListAjax()
	{
		$quote = new Quote();
		// get orders as per filters applied
		$location_id  = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$quote_status = '';
		$category_id  = Yii::app()->input->post('category_id');
		$subcategory_id = Yii::app()->input->post('subcategory_id');
		$from_date = Yii::app()->input->post('from_date');
		$to_date   = Yii::app()->input->post('to_date');
		$search_for = "";

		if (
			isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value'])
			&& !empty($_REQUEST['search']['value'])
		) {
			$search_for = $_REQUEST['search']['value'];
		}


		$order_by = array();
		if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])) {
			$order_by = $_REQUEST['order'];
		} else {
		}

		$quotes = $quote->getMyScoring($_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $quote_status, $from_date, $to_date, $userID = Yii::app()->session['user_id']);
		$quoteArr = array();
		$i = 1;
		$subdomain = FunctionManager::subdomainByUrl();

		foreach ($quotes as $value) {
			$closeingDate = '';
			$style = "";
			$notQuoteCancelled = strtolower($value['quote_status']) != 'cancelled';
			$quoteStatusNotDraft = $value['quote_status'] != 'Draft';
			$closing_date = date("Y-m-d H:i:s", strtotime($value['closing_date']));
			$currentDate  = date("Y-m-d H:i:s");

			if ($notQuoteCancelled && $quoteStatusNotDraft && $closing_date >= $currentDate && $value['awarded_vendor_id'] == 0) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
          ' . $this->BtnColor(['#f0ad4e', '#f0ad4e']) . '>In Progress</button>';
			} else if ($quoteStatusNotDraft && !empty($value['awarded_vendor_id'])) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
          ' . $this->BtnColor(['#1abb9c', '#1abb9c']) . '>Awarded</button>';
			} else if ($quoteStatusNotDraft && $closing_date < $currentDate) {
				$closeingDate = '<button type="submit" class="btn btn-success" 
          ' . $this->BtnColor(['#6cce47', '#6cce47']) . '>Complete</button>';
			} else if (strtolower($value['quote_status']) == 'cancelled') {
				$closeingDate = '<button type="submit" class="btn btn-success" 
          ' . $this->BtnColor(['#fb5a5a !important', '#fb5a5a !important']) . '>Cancelled</button>';
			} else if (!$quoteStatusNotDraft) {
				$closeingDate = '<button type="submit" class="btn btn-danger" 
          ' . $this->BtnColor(['#fb5a5a', '#fb5a5a']) . '>Draft</button>';
			} else {
				$closeingDate = '';
			}

			$imgFullPath = '';
			if (!empty($value['profile_img'])) {
				$imgFullPath = Yii::app()->baseUrl . "/../images/" . $subdomain . "/users/" . $value['user_id'] . "/" . $value['profile_img'];
			} else if (!empty($value['user_id'])) {
				$imgFullPath = CommonFunction::userTextAvatar($value['full_name']);
			}
			if (!empty($imgFullPath)) {
				$imgFullPath = CHtml::image($imgFullPath); 
			}

			$profileImg = '<div class="image-container">'.$imgFullPath.' </i><div class="profile-username"> '. $value['full_name'].' </div> </div> ';
			$current_data = array();
			$edit_link = '';
		
    		$scorerStatus = CommonFunction::ScorerStatus($value['quote_id']);
			if(!empty($scorerStatus) && $scorerStatus['status'] == 2){ 
				$edit_link .= '<a href="' . AppUrl::bicesUrl('quotes/myscoringdetail/' . $value['quote_id']) . '">
						<button class="btn btn-sm btn-success btn-green"  
						style="background:#1ABB9C !important;border-color:#1ABB9C !important">Completed</button></a>';
			}else if(!empty($scorerStatus) && $scorerStatus['status'] == 3){ 
				$edit_link .= '<a href="' . AppUrl::bicesUrl('quotes/myscoringdetail/' . $value['quote_id']) . '">
    				 	<button class="btn btn-sm btn-danger">Scoring Closed</button></a>';
			}else{
			$edit_link .= '<a href="' . AppUrl::bicesUrl('quotes/myscoringdetail/' . $value['quote_id']) . '">
    				 	<button class="btn btn-sm btn-success btn-green">Score Suppliers</button></a>';
			}
			$current_data[] = $edit_link;
			$current_data[] = $value['quote_id'];
			$current_data[] = $value['quote_name'];
			$current_data[] = $value['quotes_by_category'];
			$current_data[] = date(FunctionManager::dateFormat() . " H:i A", strtotime($value['opening_date']));
			$current_data[] = date(FunctionManager::dateFormat() . " H:i A", strtotime($value['closing_date']));
			$current_data[] = $profileImg;
			$current_data[] = !empty($value['awarded_vendor_name']) ? $value['awarded_vendor_name'] : '';
			$current_data[] = $value['quote_currency'];
			$current_data[] = "<span style='display:block;text-align:center'>" . round($value['cost_savings']) . "</span>";
			$current_data[] = $closeingDate;
			$quoteArr[]   = $current_data;
			$i += 1;
		}

		$database_values_total = $quote->getMyScoring('none', 'none', $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $quote_status, $from_date, $to_date, $userID = Yii::app()->session['user_id']);
		$total_quotes = count($database_values_total);
		$json_data = array(
			"draw"            => intval($_REQUEST['draw']),
			"recordsTotal"    => $total_quotes,
			"recordsFiltered" => $total_quotes,
			"data"            => $quoteArr
		);

		echo json_encode($json_data);
		exit;
	}

	public function actionQuoteVendors()
	{
		$vendor_ids = array();
		$total_vendors = Yii::app()->input->post('vendor_count');
		$selectedVendors = !empty($_POST['vendor_name_new']) ? $_POST['vendor_name_new'] : '';
		if (!empty($_POST['quote_id'])) {
			$sql = " delete from quote_vendors where quote_id=" . $_POST['quote_id'];
			Yii::app()->db->createCommand($sql)->execute();

			$sql = " delete from quote_vendor_contact where quote_id=" . $_POST['quote_id'];
			Yii::app()->db->createCommand($sql)->execute();
		}
		if (!empty($selectedVendors)) {
			foreach ($selectedVendors as $selectedID) {
				$vendor_ids[] = $selectedID;
			}

			$quote = new Quote();
			$quote->saveVendors($_POST['quote_id'], $selectedVendors);
		}
		Yii::app()->user->setFlash('success', '<strong>Supplier added Successfully.</strong>');
		$this->redirect(AppUrl::bicesUrl('quotes/edit/' . $_POST['quote_id']));
	}

	private function quoteLogsInsert($quote_id)
	{

		// @Coder: basheer alam 
		// date: 07/04/2023
		// Start: Quote log

		$oldRecord = '';
		if (!empty($quote_id)) {
			// echo $quote_id; exit;
			$quoteCommentExit = new Quote;
			$oldRecord = $quoteCommentExit->getOne(array('quote_id' => $quote_id));
		}
		
		$comment = '';
		if (!empty($oldRecord['quote_id'])) { 
			$quoteName = !empty($_POST['quote_name']) ? $_POST['quote_name'] : $oldRecord['quote_name'];
			if (strcasecmp($oldRecord['quote_name'], $quoteName) != 0) {
				$comment .= '<strong>Quote Title:</strong> <span class="title-text"><b>' . $oldRecord['quote_name'] . ' </b>changed to <b>' . $_POST['quote_name'] . '</b></span><br/>';
			}

			$quoteDesc = !empty($_POST['quote_desc']) ? $_POST['quote_desc'] : $oldRecord['quote_desc'];
			if (strcasecmp($oldRecord['quote_desc'], $quoteDesc) != 0) {
				$comment .= '<strong>Quote Notes:</strong> <span class="title-text"><b>' .
				$oldRecord['quote_desc'] . ' </b>changed to <b>' . $_POST['quote_desc'] . '</b></span><br/>';
			}

			$quoteCurrency = !empty($_POST['quote_currency']) ? $_POST['quote_currency'] : $oldRecord['quote_currency'];
			if (strcasecmp($oldRecord['quote_currency'], $quoteCurrency) != 0) {
				$comment .= '<b>Currency:</b> <span class="title-text"><b>' .
				$oldRecord['quote_currency'] . '</b> changed to <b>' . $_POST['quote_currency'] . '</b></span><br/>';
			}

			$locationID = !empty($_POST['location_id']) ? $_POST['location_id'] : $oldRecord['location_id'];
			if (strcasecmp($oldRecord['location_id'], $locationID) != 0) {
				$location = new Location();
				$locationPosted = $location->getOne(array('location_id' => Yii::app()->input->post('location_id')));
				$locationExisted = $location->getOne(array('location_id' => $oldRecord['location_id']));
				$comment .= '<b>Location:</b> <span class="title-text"><b>' . $locationExisted['location_name'] . ' </b>changed to <b>' .
				$locationPosted['location_name'] . '</b></span><br/>';
			}

			$departmentID = !empty($_POST['department_id']) ? $_POST['department_id'] : $oldRecord['department_id'];
			if (strcasecmp($oldRecord['department_id'], $departmentID) != 0) {
				$department = new Department();
				$departmentPosted = $department->getOne(array('department_id' => Yii::app()->input->post('department_id')));
				$departmentExisted = $department->getOne(array('department_id' => $oldRecord['department_id']));
				$comment .= '<b>Department:</b> <span class="title-text"><b>' . $departmentExisted['department_name'] . ' </b>changed to <b>' .
				$departmentPosted['department_name'] . '</b></span><br/>';
			}

			$categoryID = !empty($_POST['category_id']) ? $_POST['category_id'] : $oldRecord['category_id'];
			if (strcasecmp($oldRecord['category_id'], $categoryID) != 0) {
				$category = new Category();
				$categoryPosted  = $category->getOne(array('id' => Yii::app()->input->post('category_id')));
				$categoryExisted = $category->getOne(array('id' => $oldRecord['category_id']));
				$comment .= '<b>Category:</b> <span class="title-text"><b>' .
				$categoryExisted['value'] . '</b> changed to <b>' . $categoryPosted['value'] . '</b></span><br/>';
			}

			$subcategoryID = !empty($_POST['subcategory_id']) ? $_POST['subcategory_id'] : $oldRecord['subcategory_id'];
			if (strcasecmp($oldRecord['subcategory_id'], $subcategoryID) != 0) {
				$subcategory = new Subcategory();
				$subcategoryPosted = $subcategory->getOne(array('id' => Yii::app()->input->post('subcategory_id')));
				$subcategoryExisted = $subcategory->getOne(array('id' => $oldRecord['subcategory_id']));
				$comment .= '<b>Sub Category:</b> <span class="title-text"><b>' . $subcategoryExisted['value'] . '</b> changed to <b>' . $subcategoryPosted['value'] . '</b></span><br/>';
			}

			$spendType = !empty($_POST['spend_type']) ? $_POST['spend_type'] : $oldRecord['spend_type'];
			if (strcasecmp($oldRecord['spend_type'], $spendType) != 0) {
				$comment .= '<b>Spend Type:</b> <span class="title-text"><b>' . $oldRecord['spend_type'] . '</b> changed to <b>' . $_POST['spend_type'] . '</b></span><br/>';
			}

			if (FunctionManager::dateFormat() == "d/m/Y") {
				$openingDate = date("Y-m-d", strtotime(strtr($_POST['opening_date'], '/', '-')));
				$closingDate = date("Y-m-d", strtotime(strtr($_POST['closing_date'], '/', '-')));
				$oldOpeningDate = date("Y-m-d", strtotime(strtr($oldRecord['opening_date'], '/', '-')));
				$oldClosingDate = date("Y-m-d", strtotime(strtr($oldRecord['closing_date'], '/', '-')));
			} else {
				$openingDate = date("Y-m-d", strtotime($_POST['opening_date']));
				$closingDate = date("Y-m-d", strtotime($_POST['closing_date']));

				$oldOpeningDate = date("Y-m-d", strtotime($oldRecord['opening_date']));
				$oldClosingDate = date("Y-m-d", strtotime($oldRecord['closing_date']));
			}
			
			$opening_date = !empty($openingDate) ? $openingDate : $oldOpeningDate;
			if ($oldOpeningDate != $opening_date) {
				$comment .= '<b>Opening Date & Time (UTC):</b> <span class="title-text"><b>' . $oldOpeningDate . '</b> changed to <b>' . $_POST['opening_date'] . '</b></span><br/>';
			}
			
			$closing_date = !empty($closingDate) ? $closingDate : $oldClosingDate;
			if ($oldClosingDate != $closing_date) {
				$comment .= '<b>Closing Date & Time (UTC):</b> <span class="title-text"><b>' . $oldClosingDate . '</b> changed to <b>' . $_POST['closing_date'] . '</b></span><br/>';
			}

			if (!empty($comment)) {
				$log = new QuoteLog();
				$log->rs = array();
				$log->rs['quote_id'] = $oldRecord['quote_id'];
				$log->rs['user_id'] = Yii::app()->session['user_id'];
				$log->rs['comment'] = !empty($comment) ? $comment : "";
				$log->rs['created_datetime'] = date('Y-m-d H:i:s');
				$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
				$log->write();
			}
		}
		//End: saving log	
	}

	private function quoteCommentInsert($quoteID)
	{ 
		if ($quoteID) {
			$quoteCommentHistory     =  new QuoteCommentsHistory();
			$quoteCommentHistory->rs =  array();
			$quoteCommentHistory->rs['quote_id']= Yii::app()->input->post('quote_id');
			$quoteCommentHistory->rs['user_id'] = Yii::app()->session['user_id'];
			$quoteCommentHistory->rs['comment'] = addslashes($_POST['quote_comments']);
			$quoteCommentHistory->rs['datetime']= date('Y-m-d H:i:s');
			$quoteCommentHistory->write();
		}
	}

	private function quoteQuestionnaireInsert($quoteID, $actionStatus)
	{
		$quoteQuestionFormID = $_POST['question_form_id'];
		$sort = 1;
		if (count($quoteQuestionFormID) > 0) {
			if(!empty($actionStatus) && $actionStatus == 'draft'){
				$comment = "";
				$questionnaire = [];
				foreach ($quoteQuestionFormID as $key => $questionFormID) {
				  $questionnaire[] = $questionFormID;
				  $question_form_name = $_POST['question_form_name'][$key];
				  $question_scorer 	  = $_POST['question_scorer'][$key];
				  $sql = " select * from quote_questionnaire where id=".$questionFormID." and  quote_id=" . $quoteID;
				  $oldRecord = Yii::app()->db->createCommand($sql)->queryRow();
				  if (trim($oldRecord['question_form_name']) !== trim($question_form_name))
					$comment .= '<b>Questionnaire Name: </b> <span class="title-text"><b>' . $oldRecord['question_form_name'] . '</b> changed to <b>' . $question_form_name . '</b></span><br/>';
				  if (trim($oldRecord['question_scorer']) !== trim($question_scorer))
					$comment .= '<b>Score (%): </b> <span class="title-text"><b>' . $oldRecord['question_scorer'] . '</b> changed to <b>' . $question_scorer . '</b></span><br/>';
				}
				if (!empty($comment)) {
					$log = new QuoteLog();
					$log->rs = array();
					$log->rs['quote_id'] = $quoteID;
					$log->rs['user_id'] = Yii::app()->session['user_id'];
					$log->rs['comment'] = !empty($comment) ? $comment : "";
					$log->rs['created_datetime'] = date('Y-m-d H:i:s');
					$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
					$log->write();
				}
				
				if(!empty($questionnaire)){
					$sql = " select * from quote_questionnaire where id not in(". implode(',', $questionnaire).") and  quote_id=" . $quoteID;
					$deletedRecord = Yii::app()->db->createCommand($sql)->queryAll();
					$deleteComment = "";
					foreach($deletedRecord as $vValue){
						if (trim($vValue['question_form_name']))
							$deleteComment .= '<b style="color:red">Removed Questionnaire Name</b> <span class="title-text"><b>: ' . $vValue['question_form_name'] . '</b></span><br/>';
						if (trim($vValue['question_scorer']))
							$deleteComment .= '<b style="color:red">Removed Score (%)</b> <span class="title-text"><b>: ' . $vValue['question_scorer'] . '</b></span><br/>';
					}

					if (!empty($deletedRecord)) {
						$log = new QuoteLog();
						$log->rs = array();
						$log->rs['quote_id'] = $quoteID;
						$log->rs['user_id'] = Yii::app()->session['user_id'];
						$log->rs['comment'] = !empty($deleteComment) ? $deleteComment : "";
						$log->rs['created_datetime'] = date('Y-m-d H:i:s');
						$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
						$log->write();
					}
				}
			}
			
			$sql = " delete from quote_questionnaire where quote_id=" . $quoteID;
			Yii::app()->db->createCommand($sql)->execute();
			foreach ($quoteQuestionFormID as $key => $questionForm) {
				$question = new QuoteQuestionnaire();
				$question->rs = array();
				$question->rs['id'] = 0;
				$question->rs['quote_id'] = $quoteID;
				$question->rs['question_form_id']  = $questionForm;
				$question->rs['question_form_name']= addslashes($_POST['question_form_name'][$key]);
				$question->rs['question_scorer']   = $_POST['question_scorer'][$key];
				$question->rs['status'] 		   = 'To Be Completed';
				$question->rs['question_order']    = $sort;
				$question->rs['updated_at']  	   = date("Y-m-d");
				$question->write();
				$sort++;
			}
		}
	}

	public function actionCompleteContribution(){
		 $quoteUser = new QuoteUser();
		 $getUser = $quoteUser->getOne(['quote_id' => $_POST['quote_id'], 'user_id' => Yii::app()->session['user_id']]);
		if(!empty($_POST['quote_id']) && !empty($_POST['user_id'])){
		 $quoteUser = new QuoteUser();
		 $quoteUser->rs = array();
		 $quoteUser->rs['id'] = $getUser['id'];
		 $quoteUser->rs['contribute_status']   = 1;
		 $quoteUser->rs['contribute_date_time']= date("Y-m-d H:i:s");
		 $quoteUser->write();

		return $this->redirect(AppUrl::bicesUrl('quotes/edit/' . $_POST['quote_id']));		 
		}

	}

	private function questionAssignToSupplier($quoteID) {
		$sql = 'select * from quote_questionnaire where quote_id ="'.$quoteID.'" group by question_form_id order by  question_order asc ';
		$quoteQuestionnaire = Yii::app()->db->createCommand($sql)->queryAll();
		
		$sql = 'select vendor_id from quote_vendors where quote_id='.$quoteID;
		$quoteVendors = Yii::app()->db->createCommand($sql)->queryAll();
		
		foreach($quoteVendors as $getVendors){
		 $sql = 'select * from quote_vendor_questionnaire where quote_id ="'.$quoteID.'" and vendor_id ="'.$getVendors['vendor_id'].'"  group by question_form_id order by  question_order asc ';
		 $quoteQuestionnaireRender = Yii::app()->db->createCommand($sql)->queryAll();
		 if (count($quoteQuestionnaire) > 0 && empty($quoteQuestionnaireRender)) {
			foreach ($quoteQuestionnaire as $key => $questionForm) {
				Yii::app()->db->createCommand()
				->insert('quote_vendor_questionnaire', array(
					'quote_id'  => $questionForm['quote_id'],
					'question_form_id'	=> $questionForm['question_form_id'],
					'vendor_id' 	  	=> $getVendors['vendor_id'],
					'question_form_name'=> addslashes($questionForm['question_form_name']),
					'status' 			=> $questionForm['status'],
					'question_order' 	=> $questionForm['question_order'],
					'updated_at'=> $questionForm['updated_at'],
			  ));
			}
		 }
		}
	}

	public function actionUserOpenForScoring(){
		
		$quoteRes = new Quote();
		if (!empty($_POST['quote_id'])) {
			$sql = 'update quote_scorer_user set status=1 where quote_id='. $_POST['quote_id']. " and user_id=".$_POST['user_id'] ;
			Yii::app()->db->createCommand($sql)->execute();
			
		  if(!empty($_POST['quote_questionId'])){
		   foreach($_POST['quote_questionId'] as $socingQuestion){
			$crateScoreAndQuestion = New QuoteScorerForm();
			$crateScoreAndQuestion->rs = array();
			$crateScoreAndQuestion->rs['scoring_row_id']= $_POST['scoring_row_id'];
			$crateScoreAndQuestion->rs['quote_id']  	= $_POST['quote_id'];
			$crateScoreAndQuestion->rs['user_id']   	= $_POST['user_id'];
			$crateScoreAndQuestion->rs['user_name'] 	= $_POST['user_name'];
			$crateScoreAndQuestion->rs['creator_id']	= Yii::app()->session['user_id'];
			$crateScoreAndQuestion->rs['creator_user_name'] = Yii::app()->session['full_name'];
			$crateScoreAndQuestion->rs['question_id']   = $socingQuestion;
			$crateScoreAndQuestion->rs['status']  		= 1;
			$crateScoreAndQuestion->rs['created_date'] = date("Y-m-d H:i:s");
			$crateScoreAndQuestion->write();
		  }
		 
		 }
		
		 $quote = $quoteRes->getOne(['quote_id' => $_POST['quote_id']]);
		 EmailManager::quoteOpenForScoring($quote, $_POST['user_id']);
		}	
		$msg = 0;
		!empty($_POST['quote_id']) ? $msg=1 : $msg=0; 
		$creatorURL = AppUrl::bicesUrl('quotes/edit/' . $_POST['quote_id'].'?tab=scoring');
		echo json_encode(['username' => $_POST['user_name'], 'creatorURL' => $creatorURL]);exit;
	}

	public function actionMyscoringdetail($quote_id){
	   $reslt = '';
	   if(!empty($quote_id)){
		$sql =" select qsq.id, qsq.quote_id,qsq.user_id,qsq.question_id as question_form_id, qsq.creator_id,qq.question_form_name, qq.question_scorer from 
		quote_scorer_questions_form qsq
		inner join quote_questionnaire qq on qq.question_form_id = qsq.question_id and qsq.quote_id = qq.quote_id
		where qsq.quote_id=".$quote_id." and qsq.user_id=".Yii::app()->session['user_id']." group by question_form_name "; 
		$reslt = Yii::app()->db->createCommand($sql)->queryAll();
	   }
	   
	   $reStatus = CommonFunction::ScorerStatus($quote_id);
	   $this->render('scoring/myscoring-detail', ['data' => $reslt, 'scorerStatus' => $reStatus, 'quote_id'=>$quote_id]);
	}

	public function actionMyscoringdetailSave(){
	  if(!empty($_POST['scorer_score'])){
		$status = !empty($_POST['submit_score']) && $_POST['submit_score'] == 'Submit Scoring' ? 2 : 1;
		foreach($_POST['scorer_score'] as $key => $selectedScore){
		  
		$quoteAnsScore = new UserQuoteAnsScore();
		$quoteAnsScoreData = $quoteAnsScore->getOne(array('vendor_question_answer_id' => $key));
		if(!empty($quoteAnsScoreData['vendor_question_answer_id'])){
		 $sql="delete from user_quote_ans_score where quote_id=".$_POST['quote_id']." and (user_id=".Yii::app()->session['user_id'].") and vendor_question_answer_id=".$key;
		 $deleteOldData = Yii::app()->db->createCommand($sql)->execute();
		}
		$userID = Yii::app()->session['user_id'];
		$quoteAnsScore->rs = array();
		$quoteAnsScore->rs['quote_id'] = $_POST['quote_id'];
		$quoteAnsScore->rs['form_id'] = $_POST['form_id'];
		$quoteAnsScore->rs['question_form_id'] = $_POST['question_form_id'];
		$quoteAnsScore->rs['vendor_question_answer_id'] = $key;
		$quoteAnsScore->rs['user_id'] = $userID;
		$quoteAnsScore->rs['created_by_id'] = $_POST['created_by_id'];
		$quoteAnsScore->rs['score']   = $selectedScore;
		$quoteAnsScore->rs['questionnaire_per']= $_POST['questionniare_score'];
		$quoteAnsScore->rs['calculated_score']= (($selectedScore * 20) * $_POST['questionniare_score']) / 100;
		$quoteAnsScore->rs['status']  = $status;
		$quoteAnsScore->rs['created_at'] = date("Y-m-d H:i:s");
		$quoteAnsScore->write();
		}

		$sql = "select count(distinct qf.question_form_id) as total_record from user_quote_ans_score qf 
		   where qf.user_id=".$userID." and qf.quote_id=".$_POST['quote_id']." and qf.status=2";
		$asReader = Yii::app()->db->createCommand($sql)->queryRow();

		$sql = " select count(distinct su.question_id) as total_record from quote_scorer_questions_form su 
		   where su.user_id=".$userID." and su.quote_id=".$_POST['quote_id'];
		$suReader = Yii::app()->db->createCommand($sql)->queryRow();
	
		if(isset($asReader) && isset($suReader) && $asReader['total_record']==$suReader['total_record']){
			$sql = "update quote_scorer_questions_form set status=2 where user_id=".$userID." and quote_id=".$_POST['quote_id'];
			Yii::app()->db->createCommand($sql)->execute();
			$quote = new Quote();
			$quote = $quote->getOne(['quote_id' => $_POST['quote_id']]);
			$userID = Yii::app()->session['user_id'];
			EmailManager::ScorerHasCompletedScoring($quote, $userID);
	  	}
		
	}
	 
	  $this->redirect(AppUrl::bicesUrl('quotes/myscoringdetail/'. $_POST['quote_id']));
	}

	public function actionReopenScoring(){
		$quoteID = base64_decode($_GET['quoteID']);
		$id 	 = base64_decode($_GET['id']);
		$userID  = base64_decode($_GET['userID']);
		
		if(!empty($quoteID) && !empty($id) && !empty($userID)){
		 $sql = "update quote_scorer_questions_form set status=1 where scoring_row_id=".$id." and user_id=".$userID." and quote_id=".$quoteID;
		 Yii::app()->db->createCommand($sql)->execute();

		 $sql = "update user_quote_ans_score set status=1 where user_id=".$userID." and quote_id=".$quoteID;
		 Yii::app()->db->createCommand($sql)->execute();
		}

		$user = new User();
		$quote = new Quote();
		$quote = $quote->getOne(['quote_id' => $quoteID]);
		$user = $user->getOne(['user_id' => $userID]);
		$notificationComments  = $quote['created_by_name']." has reopened the supplier scoring for sourcing activity <b> ".$quote['quote_name']."</b> You can now amend your scores for this sourcing activity ";
		$notification = new Notification();
		$notification->rs = array();
		$notification->rs['id'] = 0;
		$notification->rs['user_id'] = $userID;
		$notification->rs['user_type'] = 'Client';
	 	$notification->rs['notification_text'] = addslashes('<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quoteID). '">'.$notificationComments.'</a>');
		$notification->rs['notification_date'] = date("Y-m-d H:i");
		$notification->rs['read_flag'] = 0;
		$notification->rs['notification_type'] = 'Quote';
		$notification->write();
		
		EmailManager::reOpenForScoring($quote, $userID);

		
		Yii::app()->user->setFlash('success', '<strong>'.$user['full_name'].'</strong> Reopen for Scoring successfully.');
		$this->redirect(AppUrl::bicesUrl('quotes/edit/'.$quoteID));
	}

	public function actionCloseScoring(){
		$quoteID = base64_decode($_GET['quoteID']);
		$id 	 = base64_decode($_GET['id']);
		$userID  = base64_decode($_GET['userID']);
		
		if(!empty($quoteID) && !empty($id) && !empty($userID)){
			$sql = "update quote_scorer_questions_form set status=3 where quote_id=".$quoteID." and scoring_row_id=".$id." and user_id=".$userID;
			Yii::app()->db->createCommand($sql)->execute();

		 $quote = new Quote();
		 $quote = $quote->getOne(['quote_id' => $quoteID]);
	
		$sql = "select * from quote_scorer_questions_form where status=3 and quote_id=".$quoteID." and scoring_row_id=".$id." and user_id=".$userID." group by user_id ";
		$quoteScorerQuestions = Yii::app()->db->createCommand($sql)->queryRow(); 
		if(!empty($quoteScorerQuestions)) {
		  $notificationComments  = $quote['created_by_name']." has closed the supplier scoring for sourcing activity <b> ".$quote['quote_name']."</b> You can no longer submit supplier scoring for this sourcing activity ";
		  $notification = new Notification();
		  $notification->rs = array();
		  $notification->rs['id'] = 0;
		  $notification->rs['user_id'] = $quoteScorerQuestions['user_id'];
		  $notification->rs['user_type'] = 'Client';
		  $notification->rs['notification_text'] = addslashes('<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quoteID). '">'.$notificationComments.'</a>');
		  $notification->rs['notification_date'] = date("Y-m-d H:i");
		  $notification->rs['read_flag'] = 0;
		  $notification->rs['notification_type'] = 'Quote';
		  $notification->write();
		
		   EmailManager::closedQuoteScorer($quote, $quoteScorerQuestions['user_id']);
		   $user = new User();
		   $user = $user->getOne(['user_id' => $userID]);
		   Yii::app()->user->setFlash('success', '<strong>Close</strong> for Scorer successfully.');
		  }else{
			Yii::app()->user->setFlash('success', '<strong>Scorer</strong> are not open tyr again!');
		  }
		}

		$this->redirect(AppUrl::bicesUrl('quotes/edit/'.$quoteID));
	}

	public function actionCloseScoringForAll(){
		$quoteID = base64_decode($_GET['quoteID']);
		$id 	 = base64_decode($_GET['id']);
		$userID  = base64_decode($_GET['userID']);
		
		if(!empty($quoteID) && !empty($id) && !empty($userID)){
			$sql = "update quote_scorer_questions_form set status=3 where quote_id=".$quoteID;
			Yii::app()->db->createCommand($sql)->execute();
		}

		$quote = new Quote();
		$quote = $quote->getOne(['quote_id' => $quoteID]);

		$sql = "select * from quote_scorer_questions_form where status=3 and quote_id=".$quoteID;
		$quoteScorerQuestions = Yii::app()->db->createCommand($sql)->queryAll();
		foreach($quoteScorerQuestions as $vValue) {
		
		 $notificationComments  = $quote['created_by_name']." has closed the supplier scoring for sourcing activity <b> ".$quote['quote_name']."</b> You can no longer submit supplier scoring for this sourcing activity ";
		 $notification = new Notification();
		 $notification->rs = array();
		 $notification->rs['id'] = 0;
		 $notification->rs['user_id'] = $vValue['user_id'];
		 $notification->rs['user_type'] = 'Client';
		 $notification->rs['notification_text'] = addslashes('<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quoteID). '">'.$notificationComments.'</a>');
		 $notification->rs['notification_date'] = date("Y-m-d H:i");
		 $notification->rs['read_flag'] = 0;
		 $notification->rs['notification_type'] = 'Quote';
		 $notification->write();
		
		 EmailManager::closedQuoteScorer($quote, $vValue['user_id']);
		}

		$user = new User();
		$user = $user->getOne(['user_id' => $userID]);
		Yii::app()->user->setFlash('success', '<strong>Close</strong> for Scorers successfully.');
		$this->redirect(AppUrl::bicesUrl('quotes/edit/'.$quoteID));
	}

	public function actionDraftProductItemslistAjax(){
	
		$quoteId = $_POST['quote_id'];
		$quote = new Quote();
		$start = intval($_REQUEST['start']);
		$length = intval($_REQUEST['length']);
		$searchValue = $_REQUEST['search']['value'];
		$sortColumnIndex = $_REQUEST['order'][0]['column']; 
		$sortDirection = $_REQUEST['order'][0]['dir'];
		$columns = array('product_name', 'product_code', 'uom', 'quantity'); 

		$sql = "SELECT * FROM quote_details WHERE quote_id = ".$quoteId;
		if (!empty($searchValue)) {
			$sql .= " AND (product_name LIKE '%".$searchValue."%' OR product_code LIKE '%".$searchValue."%' OR
			 		 quantity LIKE '%".$searchValue."%' OR uom LIKE '%".$searchValue."%')";
		}

		// Add sorting condition if valid column index and direction are provided
		if (isset($sortColumnIndex) && isset($sortDirection) && in_array($sortColumnIndex, array_keys($columns))) {
			$sortColumn = $columns[$sortColumnIndex];
			$sql .= " ORDER BY ".$sortColumn." ".$sortDirection;
		}

		$sql .= " LIMIT ".$start.", ".$length;
		$quotePproducts = Yii::app()->db->createCommand($sql)->queryAll();
		$quoteArr = array();
		foreach ($quotePproducts as $value) {
			$current_data = array();
			$current_data[] = $value['product_name'];
			$current_data[] = $value['product_code'];
			$current_data[] = $value['uom'];
			$current_data[] = $value['quantity'];
			$quoteArr[]   = $current_data;
			
		}
		
        $sql = " SELECT * FROM quote_details where quote_id=".$quoteId;
		$quotePproductsCount = Yii::app()->db->createCommand($sql)->queryAll();
        $TotalquotePproducts=count($quotePproductsCount);
        $json_data = array(
	            "draw"            => intval( $_REQUEST['draw'] ),
	            "recordsTotal"    => $TotalquotePproducts,
	            "recordsFiltered" => $TotalquotePproducts,
	            "data"            => $quoteArr
            );
            echo json_encode($json_data);exit; 
	}

	public function actionGetAllcontributors(){
		$quoteId = $_GET['quote_id'];
		
		$sql = " SELECT qu.*, u.full_name as full_name FROM quote_user qu
				 inner join users u on u.user_id = qu.user_id WHERE quote_id = ".$quoteId;
		$quoteContributors = Yii::app()->db->createCommand($sql)->queryAll();
		$responseData = array();
		foreach ($quoteContributors as $value) {
			$responseData[] = [
				'user_id' => $value['user_id'],
				'full_name' => $value['full_name'],
				'contribute_status' => $value['contribute_status'],
			];
		}
		
        echo json_encode($responseData);exit; 
	}

	public function actionQuoteContributorUser()
	{	
		
		$output = '';
		$userCondition = "";
		if (!empty($_POST['selectedUserIds']) && empty($_POST['userIds'])) {
			 $arr = implode(', ', $_POST['selectedUserIds']);
			 $userCondition = " and user_id not in(2,3," . $arr . ") ";
		} else { 
			if(!empty($_POST['userIds'])){
			 $arr = implode(', ', $_POST['userIds']); 
			 $userCondition = " and user_id not in(2,3," . $arr . ") ";
			}
		}
		
		if(!empty($_POST['selectedUserIds']) && !empty($_POST['userIds'])){
			$mergedArray = array_unique(array_merge($_POST['selectedUserIds'], $_POST['userIds']));
			$arr = implode(', ', $mergedArray);
			$userCondition = " and user_id not in(2,3," . $arr . ") ";
		}

		
		$sql = "select user_id,full_name from users  where 1 " . $userCondition . " order by full_name asc ";
		$users = Yii::app()->db->createCommand($sql)->query()->readAll();

		$output = [];
		foreach ($users as $row) {
			$output[] = [
				'user_id' => $row['user_id'],
				'full_name' => $row['full_name'],
			];
		}
		echo json_encode($output); exit;
	}

	public function actionAddMoreContributors(){
		$quote_id = $_POST['quote_id'];
		if (!empty($quote_id) && !empty($_POST['quote_internal_user'])) {
			$quoteInternalArray  = $_POST['quote_internal_user'];
			$quote_data = new Quote();
			$quote_data = $quote_data->getOne(['quote_id' => $quote_id]);
			
			if(empty($_POST['contribute_deadline'])){
			 $quote_contributiors =  new QuoteUser();
			 $quote_contributiors = $quote_contributiors->getOne(['quote_id' => $quote_id]);
			 $contributiors_datetime = !empty($quote_contributiors['contribute_deadline']) ? date("Y-m-d H:i:s", strtotime($quote_contributiors['contribute_deadline'])) : '';
			}else{
			 $contributiors_datetime = date("Y-m-d H:i:s", strtotime($_POST['contribute_deadline']));	
			}
			$userID  = Yii::app()->session['user_id'];
			$userName = Yii::app()->session['full_name'];
			$contribute = 1;

			foreach ($quoteInternalArray as $key => $quote_contributor_id) {
				$sql = 'INSERT INTO quote_user (quote_id,user_id,create_user_id,user_name,quote_internal_user_role,contribute_deadline) 
				VALUES ("' . $quote_id . '",
				"' . $quote_contributor_id . '",
				"' . $userID . '",
				"' . $userName . '",
				"' . $contribute . '",
				"' . $contributiors_datetime . '") ';
				Yii::app()->db->createCommand($sql)->execute();

				$notificationComments  = $userName." has invited you to contribute to <b> ".$quote_data['quote_name']."</b>";
				$notification = new Notification();
				$notification->rs = array();
				$notification->rs['id'] = 0;
				$notification->rs['user_id'] = $quote_contributor_id;
				$notification->rs['user_type'] = 'Client';
				$notification->rs['notification_text'] = addslashes('<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_data['quote_id']). '">'.$notificationComments.'</a>');
				$notification->rs['notification_date'] = date("Y-m-d H:i");
				$notification->rs['read_flag'] = 0;
				$notification->rs['notification_type'] = 'Quote';
				$notification->write();

				$cronEmail = new CronEmail;
				$cronEmail->rs = array();
				$cronEmail->rs['id'] = 0;
				$cronEmail->rs['record_id'] = $quote_data['quote_id'];
				$cronEmail->rs['send_to'] = 'User';
				$cronEmail->rs['user_id'] = $quote_contributor_id;
				$cronEmail->rs['user_type'] = 'Quote Contributor';
				$cronEmail->rs['status'] = 'Pending';
				$cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
				$cronEmail->write();
			}

			Yii::app()->user->setFlash('success', '<strong>Contributors Invited</strong> successfully.');
			Yii::app()->user->setFlash('updatedContributeDeadline', 1);
		 	$this->redirect(AppUrl::bicesUrl('quotes/edit/'. $_POST['quote_id']));
		}
	}

	public function actionUpdateContributionDeadline(){
		if(!empty($_POST['quote_id'])){
		 $deadline = date("Y-m-d H:i:s", strtotime($_POST['contribute_deadline']));
		 $sql = 'update quote_user set contribute_deadline="'.$deadline.'" where quote_id=' . $_POST['quote_id'];
		 $updateVendor = Yii::app()->db->createCommand($sql)->execute();

		 Yii::app()->user->setFlash('success', '<strong>Contributors Deadline</strong> Updated successfully.');
		 $updatedContributeDeadline = 1;
		 Yii::app()->user->setFlash('updatedContributeDeadline', $updatedContributeDeadline);
		 $this->redirect(AppUrl::bicesUrl('quotes/edit/'. $_POST['quote_id']));
		}

	}

	public function actionQuestionaireDropdown(){
		$condition='';
		$option='<option value="">Select Questionnaire</option>';
		if(!empty($_POST['questionaire'])){
			$arr = implode(', ', $_POST['questionaire']);
			$condition = ' where id not in('.$arr.') ';
		} 
		$questionReader = Yii::app()->db->createCommand('SELECT id, name FROM questionnaires_forms '.$condition.' order by name ')->queryAll();
		foreach ($questionReader as $questions) {
		 $option .= "<option value=".$questions['id'].">".$questions['name']."</option>";
		}
		echo json_encode(['option' => $option]);
	}

	public function actionQuoteComment(){
		$this->quoteCommentInsert($_POST['quote_id']);
		 Yii::app()->user->setFlash('success', '<strong>Comment</strong> Added successfully.');
		$this->redirect(AppUrl::bicesUrl('quotes/edit/'. $_POST['quote_id']));
	}

	public function ActionExportCSV(){
		$saving = new Quote();
        $saving->export();
	}

}
