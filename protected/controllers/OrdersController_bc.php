<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');

class OrdersController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('orders/list'));
	}

	public function actionList()
	{
		//CVarDumper::dump($_POST,10,1);die;
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'orders_list';

		// get list of all orders (with pagination of course)
		$order = new Order();

		// get orders as per filters applied
		$view_data = array();
		$location_id = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$order_status = Yii::app()->input->post('order_status');
		$spend_type = Yii::app()->input->post('spend_type');

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		
		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');

		$db_from_date = "";
		if (!empty($from_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $from_date);
	       	$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['from_date'] = $db_from_date;
		}

		$db_to_date = "";
		if (!empty($to_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $to_date);
	       	$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['to_date'] = $db_to_date;
		}

		if(!empty($department_id)){
			$location = new Location();
			//$department_selected = implode(',',$department_id);

			//$sqlCond = " department_id IN ($department_selected)";
			//$sql = "SELECT * FROM departments WHERE ".$sqlCond." ORDER BY department_name";
			//$department_selected = Yii::app()->db->createCommand($sql)->query()->readAll();
			$department_info = $location->getDepartments($location_id);
		}else
		$department_info='';

		// need filters too
		$location = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;
		$view_data['department_info'] = $department_info;
		// and display
		$view_data['department_id'] = $department_id;
		$view_data['order_status'] = $order_status;
		$view_data['spend_type'] = $spend_type;
		$view_data['orders'] = $order->getOrders(0,$spend_type,$location_id, $department_id, $order_status, $db_from_date, $db_to_date);
		$this->render('list', $view_data);
	}

	public function actionEditInvoices()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		$order_id = Yii::app()->input->post('order_id_for_invoices');
		$invoice_number = Yii::app()->input->post('invoice_number');

		$order = new Order();
		$order->rs = array();
		$order->rs['order_id'] = $order_id;
		if(!empty($invoice_number))
		$order->rs['invoice_number'] = $invoice_number;
		$order->write();
		
		// invoices if any were uploaded
		if (Yii::app()->input->post('form_submitted_for_invoices'))
		{
			if (isset($_FILES['invoice_file']) && is_array($_FILES['invoice_file']) && count($_FILES['invoice_file']))
			{
				if (!is_dir('uploads/orders/invoices')) mkdir('uploads/orders/invoices');
				if (!is_dir('uploads/orders/invoices/' . $order_id)) 
					mkdir('uploads/orders/invoices/' . $order_id);
				
				for ($idx=0; $idx<count($_FILES['invoice_file']['name']); $idx++)
				{
					$invoice_file = array();
					$invoice_file['name'] = $_FILES['invoice_file']['name'][$idx];
					$invoice_file['type'] = $_FILES['invoice_file']['type'][$idx];
					$invoice_file['tmp_name'] = $_FILES['invoice_file']['tmp_name'][$idx];
					$invoice_file['error'] = $_FILES['invoice_file']['error'][$idx];
					$invoice_file['size'] = $_FILES['invoice_file']['size'][$idx];
					
					if (!isset($invoice_file['error']) || empty($invoice_file['error']))
					{
			            $file_name = $invoice_file['name'];
			            move_uploaded_file($invoice_file['tmp_name'], 'uploads/orders/invoices/' . $order_id . '/' . $file_name);
					}
				}
			}
		}

		$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
	}

	public function actionEdit($order_id = 0)
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'orders_edit';

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		$vendorOrderArr = array();
		$vendorIDSArr = array();// 
		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted'))
		{ 
			$rand = rand ( 10000 , 99999 );
			$files_only_upload = Yii::app()->input->post('files_only_upload');
			$order_id = Yii::app()->input->post('order_id');
			if (!empty($order_id))
			{
				// what if vendor id is blank?
				$override_data = array();
				$vendor_id = Yii::app()->input->post('vendor_id');
				$override_data['recurrent_number'] = Yii::app()->input->post('recurrent_number');
				if (empty($vendor_id))
				{
					$vendor_name = Yii::app()->input->post('vendor_name');
					$vendor = new Vendor();
					$override_data['vendor_id'] = $vendor->getIdForName($vendor_name);
				}

				// and also by default tax is being applied at 20% :(
				//$tax_flag = Yii::app()->input->post('tax_flag');
				//if (!isset($tax_flag) || !$tax_flag) $override_data['tax_rate'] = 0;

				if(!empty( Yii::app()->input->post('total_price'))){
					$override_data['calc_price'] = Yii::app()->input->post('total_price')*Yii::app()->input->post('currency_rate');
				}

				// save actual order data
				$order = new Order();
				$order->saveData($override_data);
				$order_id = $order->rs['order_id'];
				// products in the order
				$order_detail = new OrderDetail();
				$order_detail->delete(array('order_id' => $order->rs['order_id']));
				$product_types = Yii::app()->input->post('product_type');
				$product_names = Yii::app()->input->post('product_name');
				$product_ids = Yii::app()->input->post('product_id');
				$vendor_ids = Yii::app()->input->post('vendor_id');
				$quantities = Yii::app()->input->post('quantity');
				$unit_prices = Yii::app()->input->post('unit_price');
				$taxs = Yii::app()->input->post('tax');
				$shipping_costs = Yii::app()->input->post('shipping_cost');

				$total_records = count($product_types);
              
				for ($idx=0; $idx<=$total_records; $idx++)
				{
					$order_data = array('order_id' => $order->rs['order_id']);
					//$order_data = $order_id;
					$order_data['product_type'] = isset($product_types[$idx]) ? $product_types[$idx] : "";
					$product_name = isset($product_names[$idx]) ? $product_names[$idx] : "";
					$order_data['product_id'] = isset($product_ids[$idx]) ? $product_ids[$idx] : 0;
					$order_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
					$order_data['quantity'] = isset($quantities[$idx]) ? $quantities[$idx] : 1;
					$order_data['unit_price'] = isset($unit_prices[$idx]) ? $unit_prices[$idx] : 0;
					$order_data['tax'] = isset($taxs[$idx]) ? $taxs[$idx] : 0;
					$order_data['shipping_cost'] = isset($shipping_costs[$idx]) ? $shipping_costs[$idx] : 0;

					// new products can be added I guess like the vendor above :(
					if (!empty($order_data['product_type']) && !empty($order_data['unit_price']))
					{
						$order_product = new Product();
						if (empty($order_data['product_id']))
						{
							$order_data['product_id'] = $order_product->getIdForName($product_name);

							$order_product->rs = array();
							$order_product->rs['product_id'] = $order_data['product_id'];
							$order_product->rs['price'] = round($order_data['unit_price'] / $order_data['quantity'], 2);
							$order_product->write();
						}

						if (!empty($order_data['product_id'])) $order_detail->saveData($order_data);
					}

					if ($order->rs['order_status'] == 'Submitted')
					{
						$approval_route_created = $order->createRouteForApproval($order->rs['order_id']);
						//CVarDumper::dump($approval_route_created,10,1);die;
						if (!$approval_route_created)
						{
							// do not submit the order if no approval route found
							$non_submitted_order = new Order();
							$non_submitted_order->rs = array();
							$non_submitted_order->rs['order_id'] = $order->rs['order_id'];
							$non_submitted_order->rs['order_status'] = 'Pending';
							$non_submitted_order->write();

							// let the user know what happened
							Yii::app()->session['order_status_changed_id'] = $order->rs['order_id'];
						}
					}

					$total_documents = Yii::app()->input->post('total_documents');
					
					for ($idxInner=1; $idxInner<=$total_documents; $idxInner++)
					{
						$delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idxInner);

						if ($delete_document_flag) continue;

						if (isset($_FILES['order_file_' . $idxInner]['name']) && is_array($_FILES['order_file_' . $idxInner]))
						{
							if (!is_dir('uploads/orders')) mkdir('uploads/orders');
							if (!is_dir('uploads/orders/' . $order->rs['order_id']))
								mkdir('uploads/orders/' . $order->rs['order_id']);

							if (!isset($_FILES['quote_file_' . $idxInner]['error']) || empty($_FILES['quote_file_' . $idxInner]['error']))
							{
								$file_name = $_FILES['order_file_' . $idxInner]['name'];
								move_uploaded_file($_FILES['order_file_' . $idxInner]['tmp_name'], 'uploads/orders/' . $order->rs['order_id'] . '/' . $file_name);

								$file_description = Yii::app()->input->post('file_desc_' . $idxInner);
								$order->saveOrderFile($order->rs['order_id'], $file_name, $file_description);
							}
						}
					}
				}
			} else {



			// receipts if any were uploaded
//			if (isset($_FILES['file']) && is_array($_FILES['file']) && count($_FILES['file']))
//			{
//				if (!is_dir('uploads/orders')) mkdir('uploads/orders');
//				if (!is_dir('uploads/orders/' . $order_id))
//					mkdir('uploads/orders/' . $order_id);
//
//				for ($idx=0; $idx<count($_FILES['file']['name']); $idx++)
//				{
//					$receipt_file = array();
//					$receipt_file['name'] = $_FILES['file']['name'][$idx];
//					$receipt_file['type'] = $_FILES['file']['type'][$idx];
//					$receipt_file['tmp_name'] = $_FILES['file']['tmp_name'][$idx];
//					$receipt_file['error'] = $_FILES['file']['error'][$idx];
//					$receipt_file['size'] = $_FILES['file']['size'][$idx];
//
//					if (!isset($receipt_file['error']) || empty($receipt_file['error']))
//					{
//			            $file_name = $receipt_file['name'];
//			            move_uploaded_file($receipt_file['tmp_name'], 'uploads/orders/' . $order_id . '/' . $file_name);
//					}
//				}
//			}
				// products in the order
				$order_detail = new OrderDetail();
				//$order_detail->delete(array('order_id' => $order->rs['order_id']));
	
				$product_types = Yii::app()->input->post('product_type');
				$product_names = Yii::app()->input->post('product_name');
				$product_ids = Yii::app()->input->post('product_id');
				$vendor_ids = Yii::app()->input->post('vendor_id');
				$quantities = Yii::app()->input->post('quantity');
				$unit_prices = Yii::app()->input->post('unit_price');
				$taxs = Yii::app()->input->post('tax');
				$shipping_costs = Yii::app()->input->post('shipping_cost');

				$total_records = count($product_types)-1;
				

				for ($idx=0; $idx<=$total_records; $idx++)
				{
					// what if vendor id is blank?
					$override_data = array();
					$override_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
					$override_data['recurrent_number'] = $rand;
					// and also by default tax is being applied at 20% :(
					//$tax_flag = Yii::app()->input->post('tax_flag');
					//if (!isset($tax_flag) || !$tax_flag) $override_data['tax_rate'] = 0;

					if(!empty( Yii::app()->input->post('total_price'))){
						$override_data['calc_price'] = Yii::app()->input->post('total_price')/Yii::app()->input->post('currency_rate');
					}

					// save actual order data
					if(!in_array($override_data['vendor_id'],$vendorIDSArr)){
						$order = new Order();
						$order->saveData($override_data);
						$order_id = $order->rs['order_id'];
						$vendorIDSArr[] = $override_data['vendor_id'];
						$vendorOrderArr[$override_data['vendor_id']] = $order_id;
					}else{
						$order_id = $vendorOrderArr[$override_data['vendor_id']];
					}
				   
					// and then routing information for approvals too
					if ($order->rs['order_status'] == 'Submitted')
					{
						$approval_route_created = $order->createRouteForApproval($order->rs['order_id']);
						//CVarDumper::dump($approval_route_created,10,1);die;
						if (!$approval_route_created)
						{
							// do not submit the order if no approval route found
							$non_submitted_order = new Order();
							$non_submitted_order->rs = array();
							$non_submitted_order->rs['order_id'] = $order->rs['order_id'];
							$non_submitted_order->rs['order_status'] = 'Pending';
							$non_submitted_order->write();

							// let the user know what happened
							Yii::app()->session['order_status_changed_id'] = $order->rs['order_id'];
						}
					}

					$order_data = array('order_id' => $order->rs['order_id']);
					//$order_data = $order_id;
					$order_data['product_type'] = isset($product_types[$idx]) ? $product_types[$idx] : "";
					$product_name = isset($product_names[$idx]) ? $product_names[$idx] : "";
					$order_data['product_id'] = isset($product_ids[$idx]) ? $product_ids[$idx] : 0;
					$order_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
					$order_data['quantity'] = isset($quantities[$idx]) ? $quantities[$idx] : 1;
					$order_data['unit_price'] = isset($unit_prices[$idx]) ? $unit_prices[$idx] : 0;
					$order_data['tax'] = isset($taxs[$idx]) ? $taxs[$idx] : 0;
					$order_data['shipping_cost'] = isset($shipping_costs[$idx]) ? $shipping_costs[$idx] : 0;

					// new products can be added I guess like the vendor above :(
					if (!empty($order_data['product_type']) && !empty($order_data['unit_price']))
					{
						$order_product = new Product();
						if (empty($order_data['product_id']))
						{
							$order_data['product_id'] = $order_product->getIdForName($product_name);
							
							$order_product->rs = array();
							$order_product->rs['product_id'] = $order_data['product_id'];
							$order_product->rs['price'] = round($order_data['unit_price'] / $order_data['quantity'], 2);
							$order_product->write();
						}
						if (!empty($order_data['product_id'])){ $order_detail->saveData($order_data); }
					}

					$total_documents = Yii::app()->input->post('total_documents');
					for ($idxInner=1; $idxInner<=$total_documents; $idxInner++)
					{
						$delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idxInner);
						if ($delete_document_flag) continue;

						if (isset($_FILES['order_file_' . $idxInner]) && is_array($_FILES['order_file_' . $idxInner]))
						{
							if (!is_dir('uploads/orders')) mkdir('uploads/orders');
							if (!is_dir('uploads/orders/' . $order->rs['order_id']))
								mkdir('uploads/orders/' . $order->rs['order_id']);

							if (!isset($_FILES['quote_file_' . $idxInner]['error']) || empty($_FILES['quote_file_' . $idxInner]['error']))
							{
								$file_name = $_FILES['order_file_' . $idxInner]['name'];
								move_uploaded_file($_FILES['order_file_' . $idxInner]['tmp_name'], 'uploads/orders/' . $order->rs['order_id'] . '/' . $file_name);

								$file_description = Yii::app()->input->post('file_desc_' . $idxInner);
								$order->saveOrderFile($order->rs['order_id'], $file_name, $file_description);
							}
						}
					}

				}

			}

			if (!$files_only_upload)
				$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order->rs['order_id']));
			else
				$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
		}
 
		// get data for the selected vendor
		$view_data = array('order_id' => $order_id);
		$order = new Order();
		$view_data['order'] = $order->getOrders($order_id);
		if (!$view_data['order'] || !is_array($view_data['order'])) $order_id = 0;

		if ($order_id && (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4))))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if ($view_data['order']['user_id'] != $my_user_id
					&& $view_data['order']['created_by_user_id'] != $my_user_id)
				$this->redirect(AppUrl::bicesUrl('orders/list/'));
		}

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));

		// other drop downs may be needed for vendor quick add modal window
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));
			
		// and also product information for the order
		$order_detail = new OrderDetail();
		$view_data['order_details'] = $order_detail->getOrderDetails($order_id);

		// and also product information for the order
		$order_vendor_detail = new OrderVendorDetail();
		$view_data['order_vendor_detail'] = $order_vendor_detail->getOrderVendorDetails($order_id);

		// and also product information for the order
		$view_data['related_orders'] = $order->getRelatedOrders($order_id);

		// and finally approvals if any
		if ($order_id)
		{
			$route = new OrderRoute();
			$view_data['approvals'] = $route->getApprovalRoute('O', $order_id);
		}
		else $view_data['approvals'] = array();

		// if approved, is purchase order already created?
		if ($order_id)
		{
			$purchase_order = new PurchaseOrder();
			$view_data['purchase_order'] = $purchase_order->getOne(array('order_id' => $order_id));
		}
		else $view_data['purchase_order'] = false;

		// and display
		$this->render('edit', $view_data);
		$this->renderPartial('/vendors/modal_add', $view_data);
	}

	public function actionUpdateOnlyStatus()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		// if form was submitted ... first save the data to the database
		$order_id = Yii::app()->input->post('order_id_from_modal');
		$order_status = Yii::app()->input->post('order_status_from_modal');
		if (Yii::app()->input->post('modal_form_submitted'))
		{			
			if ($order_id)
			{
				$order = new Order();
				$order->rs = array();
				$order->rs['order_id'] = $order_id;
				$order->rs['order_status'] = $order_status;
				$order->write();
				
				$not_applicable_statuses_for_po = array('Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier', 'Approved');
				if (!in_array($order_status, $not_applicable_statuses_for_po))
				{
					$purchase_order = new PurchaseOrder();
					$po_id = Yii::app()->input->post('po_id_from_modal');

					if (!$po_id)
					{
						$po_data = $purchase_order->getOne(array('order_id' => $order_id));
						if ($po_data && is_array($po_data) && isset($po_data['po_id']))
							$po_id = $po_data['po_id'];
					}
					
					if ($po_id)
					{
						$purchase_order->rs = array();
						$purchase_order->rs['po_id'] = $po_id;
						$purchase_order->rs['po_status'] = $order_status;
						$purchase_order->write();
					}
				}
			}
		}

		$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
	}

	public function actionMoveSupplierInvoices()
	{
		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';

		// if form was submitted ... first save the data to the database
		$order_id = Yii::app()->input->post('order_id_from_modal');
		$order_status = Yii::app()->input->post('order_status_from_modal');
		$order = new Order();
		$docs = $order->getSupplierDocs($order_id);
		foreach($docs as $doc):
			copy('uploads/orders/' . $order_id . '/'. $doc['vendor_id'] . '/' . $doc['file_name'] , 'uploads/orders/invoices/' . $order_id . '/' . $doc['file_name']);
		endforeach;




		$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
	}

	public function actionGetProducts()
	{
		$search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";

		if (isset($_REQUEST['product_type']) && $_REQUEST['product_type'] == 'Product')
		{
			$products = array();

			$product = new Product();
			$search_results = $product->getAll(array('active' => 1, 'product_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'product_name', 'limit' => 100));

			foreach ($search_results as $product_data)
				$products[] = array("value" => $product_data['product_name'],
									"data" => array('id' => $product_data['product_id'],
													 'price' => $product_data['price']));

			echo json_encode(array('suggestions' => $products));
		}
		else
		{
			$bundles = array();

			$bundle = new Bundle();
			$search_results = $bundle->getAll(array('active' => 1, 'bundle_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'bundle_name'));

			foreach ($search_results as $bundle_data)
				$bundles[] = array("value" => $bundle_data['bundle_name'],
									"data" => array('id' => $bundle_data['bundle_id'],
													 'price' => $bundle_data['price']));

			echo json_encode(array('suggestions' => $bundles));
		}

	}

	public function actionGetCurrencyRates()
	{
		$currencyRate = new CurrencyRate();
		$currency = $currencyRate->getCurrencyRates($_POST['currency_id']);

		echo json_encode($currency['rate']);

	}

	public function actionGetVendors()
	{
		$search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
		
		$text_field_name = "value";
		$id_field_name = "data";
		
		$select2_call = false;
		if (is_array($search_string)) 
		{
			$search_string = $search_string['term'];
			$select2_call = true;
			$text_field_name = "text";
			$id_field_name = "id";
		}

		$vendors = array();

		$vendor = new Vendor();
		$search_results = $vendor->getAll(array('active' => 1, 'vendor_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'vendor_name', 'limit' => 100));

		foreach ($search_results as $vendor_data)
			$vendors[] = array($text_field_name => $vendor_data['vendor_name'], $id_field_name => $vendor_data['vendor_id']);

		if ($select2_call) echo json_encode($vendors);
		else echo json_encode(array('suggestions' => $vendors));
	}

	public function actionDeleteFile()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$order = new Order();

		// which order record the file relates to?
		$order_id = Yii::app()->input->post('order_id');
		$file_name = Yii::app()->input->post('file_name');
		
		// directory where this file resides
		$complete_file_name = 'uploads/orders/' . $order_id . '/' . $file_name;
		if (is_file($complete_file_name)) @unlink($complete_file_name);
		$order->deleteFile($order_id, $file_name);
	}

	public function actionDeleteInvoice()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		// which order record the file relates to?
		$order_id = Yii::app()->input->post('order_id');
		$file_name = Yii::app()->input->post('file_name');
		
		// directory where this file resides
		$complete_file_name = 'uploads/orders/invoices/' . $order_id . '/' . $file_name;
		if (is_file($complete_file_name)) @unlink($complete_file_name);		
	}

	public function actionExport()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$order = new Order();
		$order->export();
	}

}
