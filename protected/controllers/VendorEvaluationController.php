<?php

require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');

class VendorEvaluationController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('vendorEvaluation/list'));
	}
	public function actionList()
	{   error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'quotes_quick_evaluation';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		// get list of all orders (with pagination of course)
		$quote = new Quote();

		// get orders as per filters applied
		$view_data = array();
		
		if(!empty($_POST['score_criteria']) || !empty($_POST['score_custom_title'])){
           
			$scoreCriteria = isset($_POST['score_criteria'])?$_POST['score_criteria']:'';
			$scorePercentageArr = isset($_POST['score_percentage'])?$_POST['score_percentage']:'';
            $quoteName = $_POST['quote_name'];
            $quoteDesc = $_POST['quote_desc'];
            $categoryID = !empty($_POST['category_id'])?$_POST['category_id']:'0';
            $subcategoryID = !empty($_POST['subcategory_id'])?$_POST['subcategory_id']:'0';

			$createBy = Yii::app()->session['user_id'];
        	$user = new User();
        	$userRecord = $user->getOne(array('user_id' => $createBy));
            // $view_data['modelMain']->getCategorySubCategories($category_id, $subcategory_id);
        	$modelMain = new MainQuickScoring;
			$modelMain->rs = array();
			$modelMain->rs['best_supplier_id'] = 0;
            $modelMain->rs['quote_name']    = $quoteName;
            $modelMain->rs['quote_desc']  = $quoteDesc;
			$modelMain->rs['created_by_id']    = $createBy;
			$modelMain->rs['created_by_name']  = $userRecord['full_name'];
			$modelMain->rs['total_vendors']    = 0;
            $modelMain->rs['category_id']    = $categoryID;
            $modelMain->rs['subcategory_id'] = $subcategoryID;
           
	        $modelMain->rs['created_at'] = date("Y-m-d H:i:s");
	        $modelMainID=0;
	        $modelMain->write();
	        /*if($modelMain->write()){*/
	        	$modelMainID = $modelMain->rs['id'];
	       /* }*/
	       
	       $vendorCount = 0;
	       $max=array();
            if(!empty($scoreCriteria))
			foreach($scoreCriteria as $scoreID=>$vendorArr){
				$scorePercentage = $scorePercentageArr[$scoreID];
				$sql = "SELECT * FROM vendor_scoring where id=".$scoreID;
        		$scoringReader = Yii::app()->db->createCommand($sql)->queryRow();
        		$scoringTitle  = $scoringReader['value'];
        		$vendorCount = $vendorCount+count($vendorArr);

				foreach($vendorArr as $vendorID=>$vendorScore){
					$calculatedscore = $vendorScore/count($vendorArr)*$scorePercentage;
					$model = new VendorQuickScoring;
					$model->rs = array();
					$model->rs['main_quick_scoring_id'] = $modelMainID;
					$model->rs['vendor_id'] = $vendorID;
					$model->rs['created_by_id']    = $createBy;
					$model->rs['created_by_name']  = $userRecord['full_name'];
					$model->rs['score_id']         = $scoreID;
					$model->rs['score_title']      = $scoringTitle ;
					$model->rs['score_percentage'] = $scorePercentage;
					$model->rs['score_value']      = $vendorScore;
					$model->rs['total_vendors']    = count($vendorArr);
			        $model->rs['calculated_value'] = $calculatedscore;
                    $model->rs['socre_type'] = 'Default';
			        $model->rs['created_at'] = date("Y-m-d H:i:s");
			        if($model->write()){
			        	$msg = 'Scoring Submitted Successfully';
			        }else{
			        	$msg = 'Scoring Submitted Successfully';
			        }
			        $max[$vendorID] =  $max[$vendorID]+$vendorScore;

			        
				}
			}
            if(!empty($_POST['score_custom_title'])){
                $scoreCriteria = $_POST['score_custom_title'];

                $scorePercentageArr = $_POST['score_custom_percentage'];
            foreach($scoreCriteria as $key=>$scoringTitle){
 
                $scorePercentage = $scorePercentageArr[$key];
                $vendorArr =  $_POST['score_custom_criteria'][$key];
                foreach($vendorArr as $vendorID=>$vendorScore){
 
                    $calculatedscore = $vendorScore/count($vendorArr)*$scorePercentage;
                    $model = new VendorQuickScoring;
                    $model->rs = array();
                    $model->rs['main_quick_scoring_id'] = $modelMainID;
                    $model->rs['vendor_id'] = $vendorID;
                    $model->rs['created_by_id']    = $createBy;
                    $model->rs['created_by_name']  = $userRecord['full_name'];
                    $model->rs['score_id']         = $key;
                    $model->rs['score_title']      = $scoringTitle;
                    $model->rs['score_percentage'] = $scorePercentage;
                    $model->rs['score_value']      = $vendorScore;
                    $model->rs['total_vendors']    = count($vendorArr);
                    $model->rs['calculated_value'] = $calculatedscore;
                    $model->rs['socre_type'] = 'Custom';
                    $model->rs['created_at'] = date("Y-m-d H:i:s");
                    if($model->write()){
                        $msg = 'Scoring Submitted Successfully';
                    }else{
                        $msg = 'Scoring Submitted Successfully';
                    }
                    $max[$vendorID] =  $max[$vendorID]+$vendorScore;

            // $category = new Category();
            // $modelMain->rs['categories'] = $category->getAll();
            // $subcategory = new Subcategory();
            // $modelMain->rs['subcategory'] =$subcategory->getAll();
                }
            }}
           
			$bestSupplierIDs = array_keys($max, max($max));

			$redirectID  = $modelMainID;
			$modelMain = new MainQuickScoring;
			$modelMain->rs = array();
			$modelMain->rs['id'] = $modelMainID;
			$modelMain->rs['best_supplier_id']    = implode(",", $bestSupplierIDs);
			$modelMain->rs['total_vendors']    = $vendorCount;
	        $modelMainID=0;
	        $modelMain->write();
             Yii::app()->user->setFlash('success', "Quick Evaluation Saved");
            $this->redirect(array('quick-sourcing-evaluation/edit/'.$redirectID));
		}

        // Start: main list query
		if (Yii::app()->request->isAjaxRequest){

		 $quickEvaluationList = [];
         $search_for = "";
         if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value']))
         {
            $search_for = $_REQUEST['search']['value'];       
         }
			
		 $order_by = array();
		 if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])){
			$order_by = $_REQUEST['order'];
		 }

		 $vendor_id      = $_POST['vendor_id'];
		 $category_id    = $_POST['category_id'];
		 $subcategory_id = $_POST['subcategory_id'];

		 $vendorQuickList = new VendorQuickScoring;
         $subdomain = FunctionManager::subdomainByUrl();
		 $database_values = $vendorQuickList->getQuickEveluationList($_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $vendor_id ,$category_id ,$subcategory_id);
			 foreach ($database_values as $value) { 
	              $modelMainID = $value['id'];

	          $imgFullPath = '';
	          if(!empty($value['profile_img'])){
	              $imgFullPath = Yii::app()->baseUrl . "/../images/".$subdomain."/users/".$value['created_by_id']."/".$value['profile_img'];

	          }else if(!empty($value['created_by_id'])){
	            $imgFullPath = CommonFunction::userTextAvatar($value['full_name']);;
	          }
	          
	          if(!empty($imgFullPath)){
	              $imgFullPath = '<img src="'. $imgFullPath .'" />';
	          }

             $profileImg = '<div class="image-container">'.$imgFullPath.' </i><div class="quick-profile-username"> '. $value['created_by_name'].' </div> </div> ';

	         $current_data = [];
	         $editBtn  = '';
	         $editBtn .= '<a href="'.$this->createUrl('vendorEvaluation/edit', array('score_record'=>$modelMainID)).'">';
	         $editBtn .= '<button class="btn btn-sm btn-success view-btn">View/Edit</button></a>';
	         
	         $current_data[] = $editBtn;
	         $current_data[] = $value['quote_name'];
	         $current_data[] = $value['quote_desc'];
	         $current_data[] = $value['category_name'];
	         $current_data[] = '<span class="notranslate text-center" >'.$value['vendor_name'].'</span>';
	         $current_data[] = '<span class="notranslate text-center" >'.$value['best_vendor_name'].'</span>';
	         $current_data[] = $profileImg;
	         $current_data[] = date(FunctionManager::dateFormat(), strtotime($value['created_at']));
	        	
	         $quickEvaluationList[] = $current_data;
	        }

            $total_quicks = $vendorQuickList->getQuickEveluationList('none', 'none', $search_for, $order_by, $vendor_id ,$category_id ,$subcategory_id);
	        $total_quicks = count($total_quicks);
            $json_data = array(
	            "draw"            => intval( $_REQUEST['draw'] ),
	            "recordsTotal"    => $total_quicks,
                "recordsFiltered" => $total_quicks,
	            "data"            => $quickEvaluationList
	         );
			echo json_encode($json_data);exit;	
		}


		//End : Quick Evaluation List 

        $report = new Report();
        $view_data['evaluationCateReader']= $report->getEvaluationTopTenCategory();

  
        $user = new User();
        $userData = $user->getOne(array('user_id'=>Yii::app()->session['user_id']));
         
        $view_data['quick_evaluations_visit'] = $userData['quick_evaluations_visit'];
        if($userData['quick_evaluations_visit'] !=1){
            $user->rs = array();
            $user->rs['user_id'] = $userData['user_id'];
            $user->rs['quick_evaluations_visit'] = 1;
            $user->write();
        }
		$this->render('/vendor_evaluation/list', $view_data);
		unset(Yii::app()->session['quick_vendor']);
	}

	public function actionEdit()
	{  
        date_default_timezone_set(Yii::app()->params['timeZone']);
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'quotes_quick_evaluation';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		
		 $mainScoreID = $_GET['score_record'];
		if(empty($mainScoreID)){
            Yii::app()->user->setFlash('success', "Quick Evaluation Saved");
			$this->redirect(array('list'));
		}

        $mainQuickScoring = new MainQuickScoring();
        $view_data['mainQuickScoring'] = $mainQuickScoring->getOne(array('id' => $mainScoreID));
        
        $sql ="select c.value as category_name, sc.value as subcategory_name from main_quick_scoring mqs 
         inner join categories as c on mqs.category_id = c.id
         inner join sub_categories as sc on mqs.subcategory_id = sc.id
         where mqs.id='".$mainScoreID."'";
         $categorySubcateReader = Yii::app()->db->createCommand($sql)->queryRow();
         $view_data['categorySubcateReader'] = $categorySubcateReader;


		$sql = "select * from vendor_quick_scoring where main_quick_scoring_id='".$mainScoreID."'";
		$scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $view_data['scoreReader'] = $scoreReader;
		$view_data['mainScoreID'] = $mainScoreID;

		$this->render('/vendor_evaluation/view', $view_data);
		unset(Yii::app()->session['quick_vendor']);
	}
	public function actionCreateBySourcingEvaluationModal(){
      // get data for the selected vendor
       
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'quotes_edit';

        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $quote_id = 0;
        $this->getCurrency($user_currency);
        $view_data = array('quote_id' => $quote_id);
        $quote = new Quote();
        $view_data['quote'] = $quote->getOne(array('quote_id' => $quote_id));
        if (!$view_data['quote'] || !is_array($view_data['quote'])) $quote_id = 0;

        // need some drop down values
        $location = new Location();
        $view_data['locations'] = $location->getData(array('order' => 'location_name'));

        $project = new Project();
        $view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

        $user = new User();
        $view_data['users'] = $user->getData(array('order' => 'full_name'));

        // and also product and supplier information for the quote
        $view_data['quote_details'] = $quote->getQuoteDetails($quote_id);
        $view_data['quote_vendors'] = $quote->getQuoteVendors($quote_id);
        if(isset($view_data['quote_vendors'][0]['vendor_id']) && !empty($view_data['quote_vendors'][0]['vendor_id'])){
            $vendor_id = $view_data['quote_vendors'][0]['vendor_id'];
        }else {
            $vendor_id = 0;
        }
        $category = new Category();
        $view_data['categories'] = $category->getAll(array('soft_deleted' => "0"));
 

        $view_data['quote_files'] = $quote->getQuoteFiles($quote_id);
        $view_data['quote_questions'] = $quote->getQuoteQuestions($quote_id);
        $view_data['quote_stacked_bar_graph'] = $quote->getQuoteGraph($quote_id,'');
        $view_data['quote_answer_line_graph'] = $quote->getQuoteAnswer($quote_id,'');
        $view_data['quote_overall_stats'] = $quote->getQuoteOverAllStats($quote_id);
        $view_data['scoringCriteria'] = $quote->scoringCriteria($quote_id);
        //$view_data['quote_vendor_files'] = $quote->getVendorFiles($quote_id);
        unset(Yii::app()->session['uploaded_files']);
        $this->renderPartial('/vendor_evaluation/create_sourcing_evaluation_modal', $view_data);
    }

     public function actionListScoreDataChart()
    {
        $main_score_id = $_POST['main_score_id'];
        $sql = "select sum(sco.calculated_value) as s_valu,vend.vendor_name from vendor_quick_scoring as sco
         inner join vendors as vend on sco.vendor_id=vend.vendor_id where sco.main_quick_scoring_id='".$main_score_id."'  group by sco.vendor_id order by vendor_name";
        $scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $location_series = array();
		$location_label = array();
		foreach($scoreReader as $value){
			$location_series[] = $value['s_valu'];
			$location_label[] = $value['vendor_name'];
		}

		$sql = "select sco.score_percentage as s_valu,sco.score_title as scoring_name,sco.score_id from vendor_quick_scoring as sco  where sco.main_quick_scoring_id='".$main_score_id."' group by sco.score_id order by scoring_name";
        $scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $pie_score_series = array();
		$pie_score_label = array();
		foreach($scoreReader as $value){
			$scoreID = $value['score_id'];
			$pie_score_series[] = $value['s_valu'];
			$pie_score_label[] = $value['scoring_name'];
		}
		$data = array();
		$data['score_values'] = $location_series;
		$data['score_labels'] = $location_label;
		$data['pie_score_series'] = $pie_score_series;
		$data['pie_score_label'] = $pie_score_label;
		echo json_encode($data);
    	exit;
    }

    public function actionListScoring()
    {
        error_reporting(0);
        $body = '';
        $main_score_id = $_POST['main_score_id'];
        $createBy = Yii::app()->session['user_id'];
        $user = new User();
        $userRecord = $user->getOne(array('user_id' => $createBy));
        $sql = "select ven.vendor_name,ven.vendor_id from vendor_quick_scoring sco 
        		inner join vendors ven on sco.vendor_id = ven.vendor_id where sco.main_quick_scoring_id='".$main_score_id."'";
        $vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $sql = "select sco.id,sco.score_id,score_percentage,sco.score_title as score_name,sco.vendor_id from vendor_quick_scoring sco where sco.main_quick_scoring_id='".$main_score_id."' group by sco.score_id";

        $scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
        $totalVendorScore = array();
        if(!empty($vendoReader)){
        $body = '<div class="x_title" style="border-bottom:none"><h4 class="heading" style="padding: 10px 0px 10px 0px">Score Suppliers</h4>
        <h4 class="subheading"> Highest Number = Best Scoring Supplier, Lowest Number = Lowest Scoring Supplier (Suppliers can have the same score)</h4></div><br /><table class="table table-striped table-bordered"><thead><tr><th class="notranslate">Scoring Criteria</th>';

        //Start: Vendor Name heading
        $vendor_ids=array();
        foreach($vendoReader as $vendValue){
        	//$totalVendorScore[$vendValue['vendor_id']] = 0;
        	if(!in_array($vendValue['vendor_id'],$vendor_ids)){
        		$vendor_ids[] = $vendValue['vendor_id'];
        		$body .= '<th>'.$vendValue['vendor_name'].'</th>';
        	}
    	}
    	//echo "<pre>";print_r($totalVendorScore)
    	//$body .= '<th>Action</th></tr>';
    	$body .= '<th></th></tr>';
        $body .= '</tr>';
        $body .= '</thead><tbody>';
    	//End: Vendor Name heading

    	//Start: Vendor scoring
    	$i=0;
    	foreach($scoreReader as $value){
           /* $scoreID = $value['score_id'];
    		$sql = "SELECT * FROM quote_scoring where vendor_scoring_id=".$scoreID." and quote_id=".$quoteID;
        	$scoringForQuoteReader = Yii::app()->db->createCommand($sql)->queryRow();*/

        	$body .= '<tr><th>'.$value['score_name'].' ('.number_format($value['score_percentage'],0).'%)</th>';

        	$sql = "select id,score_value,vendor_id,main_quick_scoring_id from vendor_quick_scoring  where main_quick_scoring_id='".$main_score_id."' and score_id=".$value['score_id']." and vendor_id in(".implode(",",$vendor_ids).")";
        	$i++;
        	
           $vendorScoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
        	foreach($vendorScoreReader as $scoreValue){
        		//$scoreInfo = $scoreValue['calculated_score'];
        		$scoreInfo = $scoreValue['score_value'];
        		
        		$totalVendorScore[$scoreValue['vendor_id']] = $totalVendorScore[$scoreValue['vendor_id']]+$scoreInfo;

        		$body .= '<td>'.number_format($scoreInfo,0).'</td>';
        	}

        	$actionLink='<a href="#" onClick="loadEditForm(event,'.$value['score_id'].','.$main_score_id.')"><button class="btn btn-sm btn-success view-btn">Edit</button></i></a>';
        	$body .= '<td>'.$actionLink.'</td>';
        	$body .= '</tr>';
    	}
    	//END: Vendor scoring
    	/* $body .= '<tr><td><strong>Total</strong></td>';
    	 foreach($totalVendorScore as $vendValue){
        	$body .= '<td>'.number_format($vendValue,0).'</td>';
    	}
    	$body .= '</tr>';*/

    	$body .= '</tbody></table>';
    	}
    	echo $body;
    	exit;
    }

    public function actionEditScoring()
    { error_reporting(0);
    	
        $body = '';
        $bodyEdit = '';
    	$scoreID = $_POST['score_id'];
    	$mainID = $_POST['main_scoring_id'];   	
    	 
       

      $sql = "select ven.vendor_name,ven.vendor_id from vendor_quick_scoring sco 
        		inner join vendors ven on sco.vendor_id = ven.vendor_id where sco.main_quick_scoring_id='".$mainID."'";
        $vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $sql = "select sco.id,sco.score_id,score_percentage,sco.score_title as score_name,sco.vendor_id from vendor_quick_scoring sco where sco.main_quick_scoring_id='".$mainID."'  and sco.score_id=".$scoreID." group by sco.score_id";

        $scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
        $totalVendorScore = array();
        if(!empty($vendoReader)){
        $body = '<table class="table table-striped table-bordered"><thead><tr><th>Scoring Criteria</th>';

        //Start: Vendor Name heading
        $vendor_ids=array();
        foreach($vendoReader as $vendValue){
        	//$totalVendorScore[$vendValue['vendor_id']] = 0;
        	if(!in_array($vendValue['vendor_id'],$vendor_ids)){
        		$vendor_ids[] = $vendValue['vendor_id'];
        		$body .= '<th class="notranslate">'.$vendValue['vendor_name'].'</th>';
        	}
    	}
    	//echo "<pre>";print_r($totalVendorScore)
    	//$body .= '<th>Action</th></tr>';
    	$body .= '</tr>';
    	$body .= '</thead><tbody>';
    	//End: Vendor Name heading


    	$i=0;
    	foreach($scoreReader as $value){
           /* $scoreID = $value['score_id'];
    		$sql = "SELECT * FROM quote_scoring where vendor_scoring_id=".$scoreID." and quote_id=".$quoteID;
        	$scoringForQuoteReader = Yii::app()->db->createCommand($sql)->queryRow();*/
        	$scoreID = $value['score_id'];
        	$body .= '<tr><th class="notranslate">'.$value['score_name'].' ('.number_format($value['score_percentage'],0).'%)</th>';

        	$sql = "select id,score_value,vendor_id,main_quick_scoring_id from vendor_quick_scoring  where main_quick_scoring_id='".$mainID."' and score_id=".$scoreID." and vendor_id in(".implode(",",$vendor_ids).")";
        	$i++;
        	
           $vendorScoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
        	foreach($vendorScoreReader as $scoreValue){

        		//$scoreInfo = $scoreValue['calculated_score'];
        		$scoreInfo = $scoreValue['score_value'];
        		$vendorID = $scoreValue['vendor_id'];
        		
        		$totalVendorScore[$vendorID] = $totalVendorScore[$vendorID]+$scoreInfo;
        		  $scoreInfo = number_format($scoreInfo,0,".","");

        		$body .= '<td><select class="form-control notranslate" name="score_criteria['.$mainID.']['.$scoreID.']['.$vendorID.']">';
        		for($j=1;$j<=count($vendor_ids);$j++){
        			if($j==$scoreInfo){
        				$selected = 'selected="selected"';
        			}else{ $selected = '';}
        			$body .= '<option value="'.$j.'" '.$selected.'>'.$j.'</option>';

        		}
        		$body .= '</select></td>';
        	}

        	$actionLink='';
        	$body .= '<td>'.$actionLink.'</td>';
        	$body .= '</tr>';
    	}
    	//END: Vendor scoring
    	/* $body .= '<tr><td><strong>Total</strong></td>';
    	 foreach($totalVendorScore as $vendValue){
        	$body .= '<td>'.number_format($vendValue,0).'</td>';
    	}*/
    	$body .= '</tr>';

    	$body .= '</tbody></table>';
    	}
    	echo $body;
    	exit;
        
    }

    public function actionSaveEditScoring()
    {
        $scoreCriteria = $_POST['score_criteria'];
        foreach ($scoreCriteria as $mainScoreID => $value) {
        	foreach ($value as $socreID => $vendValue) {

        		foreach($vendValue as $vendorID=>$scoreValue){

	        	 	$vendorID = $vendorID;
	        	 	$quoteScoring = new VendorQuoteScoring();
	        	 	$sql = 'update vendor_quick_scoring set score_value='.$scoreValue.' , calculated_value='.$scoreValue/count($vendValue).'*score_percentage where main_quick_scoring_id='.$mainScoreID.' and score_id='.$socreID.' and vendor_id='.$vendorID;
	        	 	$scoringReader = Yii::app()->db->createCommand($sql)->execute();
			        if($scoringReader){
			        	$msg = 'Scoring Submitted Successfully';
			        }else{
			        	$msg = 'Scoring Submitted Successfully';
			        }
        		}       	
        	}    
    	}
    	echo json_encode(array('msg' => $msg));exit;
	}

    public function actionCustomTableHeader(){
        //$_POST['vendorIDs'] = ",0,7"
        $vendorIDs = ltrim($_POST['vendorIDs'],",");
        $vendorIDs = ltrim($_POST['vendorIDs'],",");
        $vendorIDs = rtrim($vendorIDs,",");
        
        $table = '';
        
        $table = '<table class="table" id="scoring-custom-table"><tbody><tr class="custom-heading-row"><th class="notranslate">Scoring Criteria</th><th class="notranslate">Scoring Percentage</th>';
        $sql = "select vendor_name,vendor_id from vendors where vendor_id in(".$vendorIDs.")";
        $vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();
        foreach($vendoReader as $value){
            $table .="<th>".$value['vendor_name']."</th>";
        } 
        $table .='<td style="width:3%"></td>';
         
        $table .="</tr>";
         
        echo $table;exit;
    } 

    

    public function actionCustomValuesUpdate(){
       error_reporting(0);
       $_POST['vendorIDs']=" 0,5";
        $alreadyVendorSelected = Yii::app()->session['vendor_ids_custom'];
        $newVendorSelected = array();
        $vendorIDs = ltrim($_POST['vendorIDs'],",");
        $vendorIDs = ltrim($_POST['vendorIDs'],",");
        $vendorIDs = rtrim($vendorIDs,",");
        $_POST['scoring_criteria_custom_index']=8;
        $customIndex = $_POST['scoring_criteria_custom_index'];

        $sql = "SELECT max(score_id) as max_score_id FROM vendor_quick_scoring";
        $maxSocreReader = Yii::app()->db->createCommand($sql)->queryRow();
        $maxScoreID = $maxSocreReader['max_score_id'];

        $classNum = intval($maxScoreID)+intval($customIndex);
        $className='custom-row-'.$classNum;
         
        $quote = new Quote();
        $scoringCriteria = $quote->scoringCriteria($quote_id=0);
        $_POST['custom_clready_title'] = '<tr class="custom-heading-row"><th class="notranslate">Scoring Criteria</th><th class="notranslate">Scoring Percentage</th><th>Corporate Cabs</th><td style="width:3%"></td></tr><tr class="custom-row-44"><td class="score-custom-title-already"><input type="text" name="score_custom_title[44]" class="form-control notranslate" placeholder="Enter Scoring Title" required="" style="padding:5px;"></td><td class="score-custom-value-already"><input type="text" name="score_custom_percentage[44]" class="form-control score-criteria notranslate" placeholder="Enter Scoring Percentage" onkeypress="return isNumberKey(event)" onkeyup="scoringCriterShow()" required="" style="padding:5px;"></td><td class="custom-value-vendor"><select class="form-control notranslate" name="score_custom_criteria[44][5]"><option value="1">1</option></select></td><td style="width:3%"><a href="#" onclick="removeCustomTableRow(\'custom-row-44\')" class="fa fa-minus fa-2x text-danger remove pull-right"></a></td></tr>';
        

 
         
        $rowExp = $_POST['custom_clready_title'];
         //echo "<pre>";print_r($_POST);exit;
        $rowExp = explode('</tr>', $rowExp);
       
    $table = '';
    $sql = "select vendor_name,vendor_id from vendors where vendor_id in(".$vendorIDs.")";
       $vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $headingRow = '<tr class="custom-heading-row"><th class="notranslate">Scoring Criteria</th><th class="notranslate">Scoring Percentage</th>';
        foreach($vendoReader as $value){
            if(!in_array($value['vendor_id'],$alreadyVendorSelected)){
                $newVendorSelected[] = $value['vendor_id'];
            };
            $headingRow .="<th>".$value['vendor_name']."</th>";
        } 
        $headingRow .='<td style="width:3%"></td></tr>';
        
        $ctr = 1;
        unset($rowExp[0]);
        
        

        foreach($rowExp as $expValue){
            $data =  $expValue;
            if( $ctr == 1){
               // $data = substr_replace($data,'',$headingLastPostion,$headingLastPostion);
               // continue;
            }
            $ctr++;
            $deleteExp = '';
 if(isset($expVendor)){
            preg_match('/<td class="custom-value-vendor">(.*)/', $data, $expVendor);
           
            $vendExp = explode('</td>', $expVendor[0]);
 
 
            $vtd='';
            
            foreach($vendExp as $expVendorTdData){
                unset($expVendorTd);
                
              
                if(strpos($expVendorTdData, 'class="custom-value-vendor"') !== false) {
                    $testing =$expVendorTdData."</td>";
                     
                preg_match('/<select class="form-control notranslate"(.*)/', $testing, $expSelectValue);
                preg_match('/<option value="1">(.*)/', $expSelectValue[0], $expOptionValue);
                //preg_match('/<\/select><\/td>(.*)/', $expOptionValue[0], $lastTd);

                echo "<pre>";print_r($expOptionValue[0]); 

                //$lastTd = str_replace('</select></td>','',$expOptionValue[0]);
                $toReplace = str_replace('</select></td>','',$expOptionValue[0]);
              

             $optionExp = '';
            for($m = 1; $m <=count($vendoReader); $m++) {
               $optionExp =$optionExp .'<option value="'.$m.'">'.$m.'</option>';
            }

             echo "<pre>";print_r($toReplace); 
         
            $vtd  = str_replace($toReplace,$optionExp,$testing);
       
            $data = str_replace($testing,$vtd,$data);
 
          /*  print_r($testing);*/;
            // Remove Delete dt
            preg_match('/<\/option><\/select><\/td>(.*)/', $data, $deleteTd);
            $deleteExp = $deleteTd[1];
            $data = str_replace($deleteExp,'',$data);
            echo "<pre>";print_r($data); 
           
            //End: Remove Delete dt
                
            //
              //echo "<pre>";print_r($testing);print_r($vtd);print_r($data);
 }} }

/* echo "<pre>";print_r($newVendorSelected);*/
             
            if(!empty($newVendorSelected)){
                 $optionExp = '';
                for($m = 1; $m <=count($vendoReader); $m++) {
                   $optionExp =$optionExp .'<option value="'.$m.'">'.$m.'</option>';
                }

                foreach($newVendorSelected as $newVendor){
                $data .='<td class="custom-value-vendor"><select class="form-control notranslate" name="score_custom_criteria['.$classNum.']['.$newVendor.']">'.$optionExp.'</select></td>';
                }

           }
       
            $table .=  $data.$deleteExp."</tr>";
        }
 
       echo $headingRow.$table;exit;
    }

     public function actionGetMaximSocreID(){
        $vendorIDs = ltrim($_POST['vendorIDs'],",");
        $vendorIDs = ltrim($_POST['vendorIDs'],",");
        $vendorIDs = rtrim($vendorIDs,",");
        $customIndex = $_POST['scoring_criteria_custom_index'];
        $dropdownSelected = isset($_POST['dropdown_selected'])?$_POST['dropdown_selected']:array();
        $scoreSelected = isset($_POST['score_selected'])?$_POST['score_selected']:array();
        //echo "<pre>";print_r($dropdownSelected);exit;
       
        $quote = new Quote();
        $scoringCriteria = $quote->scoringCriteria($quote_id=0);

       /* foreach($scoringCriteria as $value){
      $scoringCriteriaArr[$value['id']] = $value['value'];*/

       $sql = "select vendor_name,vendor_id from vendors where vendor_id in(".$vendorIDs.")";
       $vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $sql = "SELECT max(score_id) as max_score_id FROM vendor_quick_scoring";
        $maxSocreReader = Yii::app()->db->createCommand($sql)->queryRow();
        $maxScoreID = $maxSocreReader['max_score_id'];
        $table = '';
        //if($customIndex==1){
        if($customIndex ==1 || !empty($_POST['vendorChangeOnly']) ){
        $table = '<tr class="custom-heading-row"><th class="notranslate">Scoring Criteria</th><th class="notranslate">Scoring Percentage</th>';
        $vendorIDs = array();
        foreach($vendoReader as $value){
            $table .="<th>".$value['vendor_name']."</th>";
            $vendorIDs[] = $value['vendor_id']; 

        } 

        Yii::app()->session['vendor_ids_custom'] = $vendorIDs;
        $table .='<td style="width:3%"></td></tr>';
        }
       
        $optionExp = '';
        
        $classNum = intval($maxScoreID)+intval($customIndex);
        $className='custom-row-'.$classNum;

        $scoringCriteria = !empty($_POST['score_custom_value'])?$_POST['score_custom_value']:'';
        $scoringTitle =  !empty($_POST['score_custom_title'])?$_POST['score_custom_title']:'';

        if(!empty($scoringTitle) && !empty($_POST['vendorChangeOnly']) ){
        foreach($scoringTitle as $key=>$value){
         
         $table .='<tr class="'.$className.'"><td class="score-custom-title-already"><input type="text" name="score_custom_title['.$classNum.']" class="form-control score_custom_title notranslate" placeholder="Enter Scoring Title" required="" value="'.$value.'" style="padding:5px;"></td>';
         $table .='<td class="score-custom-value-already"><input type="text" name="score_custom_percentage['.$classNum.']" class="form-control score-criteria text-center score_custom_value notranslate" placeholder="Enter Scoring Percentage"  value="'.($scoringCriteria[$key]).'" onkeypress="return isNumberKey(event)" onkeyup="scoringCriterShow()" required="" style="padding:5px;"></td>';

        foreach($vendoReader as $vendorValue){
            $selectedValue='';
            $optionExp = '';
            $selected  = '';
            if(!empty($dropdownSelected[$vendorValue['vendor_id']+$classNum])){
                 $selectedValue=$dropdownSelected[$vendorValue['vendor_id']+$classNum];
            } 

            for($m = 1; $m <=count($vendoReader); $m++) {
                $selected  = '';
                if(!empty($selectedValue) && $selectedValue==$m){
                    $selected  = 'selected="selected"';
                }
                $optionExp =$optionExp .'<option value="'.$m.'" '. $selected.'>'.$m.'</option>';
            }

            $table .='<td class="custom-value-vendor"><select  id="'.$vendorValue['vendor_id'].'" data="'.$classNum.'" class="form-control score_custom_vendor_dropdown notranslate" name="score_custom_criteria['.$classNum.']['.$vendorValue['vendor_id'].']">'.$optionExp.'</td>';
        
        } 

         $onclick="removeCustomTableRow('".$className."')";

         $table .='<td style="width:3%"><a href="#" onclick="'.$onclick.'" class="fa fa-minus fa-2x text-danger remove pull-right"></a></td>';
         
        $table .='</tr>';

        $classNum++;
          $className='custom-row-'.$classNum;
        } }

         
        if(empty($_POST['vendorChangeOnly'])){
         $table .='<tr class="'.$className.'"><td class="score-custom-title-already"><input type="text" name="score_custom_title['.$classNum.']" class="form-control score_custom_title notranslate" placeholder="Enter Scoring Title" required="" style="padding:5px;"></td>';
         $table .='<td class="score-custom-value-already"><input type="text" name="score_custom_percentage['.$classNum.']" class="form-control score-criteria text-center score_custom_value notranslate" placeholder="Enter Scoring Percentage"  onkeypress="return isNumberKey(event)" onkeyup="scoringCriterShow()" required="" style="padding:5px;"></td>';

         for($m = 1; $m <=count($vendoReader); $m++) {
                $optionExp =$optionExp .'<option value="'.$m.'">'.$m.'</option>';
            }



        foreach($vendoReader as $vendorValue){
            $table .='<td class="custom-value-vendor"><select data="'.$classNum.'" id="'.$vendorValue['vendor_id'].'" class="form-control score_custom_vendor_dropdown notranslate" name="score_custom_criteria['.$classNum.']['.$vendorValue['vendor_id'].']">'.$optionExp.'</td>';
        
        }
        
        $onclick="removeCustomTableRow('".$className."')";

         $table .='<td style="width:3%"><a href="#" onclick="'.$onclick.'" class="fa fa-minus fa-2x text-danger remove pull-right"></a></td>';
         
        $table .='</tr>';
        }

        //if($customIndex==1){
            //$table .="</tbody></table>";
        //}
        echo $table;exit;
    }  

    public function actionSaveDetailDesc(){
        $this->checklogin();
        $this->layout = 'main';

        $main_score_id = $_POST['main_score_id'];
        $detail_description = !empty($_POST['detail_dsc'])?addslashes($_POST['detail_dsc']):'';
         $sql = 'update main_quick_scoring set detail_description="'.$detail_description.'" where id='.$main_score_id;
         $detailDescription = Yii::app()->db->createCommand($sql)->execute();

        if(!empty($detailDescription)){
            $msg = 1;
        }
        echo json_encode(array('msg'=>$msg));
     
        
    }


     public function actionReplaceExp(){
        
        $htmlExpressOthere = $_POST['htmlExpressOthere'];
        $eleClass = $_POST['eleClass'];
        $eleCount = $_POST['eleCount'];
        $eleCountReplacement = $_POST['eleCountReplacement'];
 
        $htmlExpressOthere =str_replace("deleteScoringVendorField('".$eleClass."',".$eleCount.")","deleteScoringVendorField('".$eleClass."',".$eleCountReplacement.")",$htmlExpressOthere);

        $htmlExpressOthere =str_replace('data="'.$eleCount.'"','data="'.$eleCountReplacement.'"',$htmlExpressOthere);
   
        echo $htmlExpressOthere;exit;
     
        
    }

      public function actionSetVendorScoreTable(){
        error_reporting(0);
        $vendorIDs = ltrim($_POST['vendorIDs'],",");
        $vendorIDs = rtrim($vendorIDs,",");
        $loadDefaultValue = $_POST['loadDefaultValue'];
         
        $quote = new Quote();
        $scoringCriteria = $quote->scoringCriteria($quote_id=0);

       /* foreach($scoringCriteria as $value){
      $scoringCriteriaArr[$value['id']] = $value['value'];*/

       $sql = "select vendor_name,vendor_id from vendors where vendor_id in(".$vendorIDs.")";
       $vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $table = '<table class="table"><tbody><tr><th class="notranslate">Scoring Criteria</th><th class="notranslate">Scoring Percentage</th>';
        $scoreVendorIDs = isset(Yii::app()->session['score_vendor_ids'])?Yii::app()->session['score_vendor_ids']:array();
        foreach($vendoReader as $value){
            $scoreVendorIDs[$value['vendor_id']] = $value['vendor_id'];
            $table .="<th class='notranslate'>".$value['vendor_name']."</th>";
        }
        $table .="</tr>";
        $optionExp = '';
        for($m = 1; $m <=count($vendoReader); $m++) {
           $optionExp =$optionExp .'<option value="'.$m.'">'.$m.'</option>';
        }

        foreach($scoringCriteria as $value){
         $table .="<tr><td>".$value['value']."</td>";
         $table .='<td><input type="text" name="score_percentage['.$value['id'].']" class="form-control text-center score-criteria notranslate" placeholder="Enter Scoring Percentage" onkeypress="return isNumberKey(event)" value="'.(!empty($loadDefaultValue)?$value['score']:'').'" onkeyup="scoringCriterShow()" required="" style="padding:5px;"></td>';

        foreach($vendoReader as $vendorValue){
            $table .='<td><select class="form-control notranslate" name="score_criteria['.$value['id'].']['.$vendorValue['vendor_id'].']">'.$optionExp.'</td>';
        }
        }
        $table .="</tr>";

        $table .="</tr>";
        echo $table;exit;

    }


    public function actionLoadDefaultValue(){
        
        $vendorIDs = ltrim($_POST['vendorIDs'],",");
        $vendorIDs = rtrim($vendorIDs,",");
         
        $quote = new Quote();
        $scoringCriteria = $quote->scoringCriteria($quote_id=0);

       /* foreach($scoringCriteria as $value){
      $scoringCriteriaArr[$value['id']] = $value['value'];*/

       $sql = "select vendor_name,vendor_id from vendors where vendor_id in(".$vendorIDs.")";
       $vendoReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $table = '<table class="table"><tbody><tr><th class="notranslate">Scoring Criteria</th><th class="notranslate">Scoring Percentage</th>';
        foreach($vendoReader as $value){
            $table .="<th class='notranslate'>".$value['vendor_name']."</th>";
        }
        $table .="</tr>";
        $optionExp = '';
        for($m = 1; $m <=count($vendoReader); $m++) {
           $optionExp =$optionExp .'<option value="'.$m.'">'.$m.'</option>';
        }

        foreach($scoringCriteria as $value){
         $table .="<tr><td>".$value['value']."</td>";
         $table .='<td><input type="text" name="score_percentage['.$value['id'].']" class="form-control score-criteria notranslate" placeholder="Enter Scoring Percentage" onkeypress="return isNumberKey(event)" onkeyup="scoringCriterShow()" required="" style="padding:5px;"></td>';

        foreach($vendoReader as $vendorValue){
            $table .='<td><select class="form-control notranslate" name="score_criteria['.$value['id'].']['.$vendorValue['vendor_id'].']">'.$optionExp.'</td>';
        }
        }
        $table .="</tr>";

        $table .="</tr>";
        echo $table;exit;

    }
}
