<?php

class ApprovalsController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('approvals/list'));
	}

	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'approvals';
		
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	

		// we store this by approval type, location and department
		$form_submitted = Yii::app()->input->post('form_submitted');
		$location_id = Yii::app()->input->post('location_id');
		$approval_type = Yii::app()->input->post('approval_type');
		$data_type = Yii::app()->input->post('data_type');
		$condition_type = Yii::app()->input->post('condition_type');
		$department_id = Yii::app()->input->post('department_id');

		// create a data filter useful for delete as well as select operations
		$data_filter = array();
		$data_filter['approval_type'] = $approval_type;
		$data_filter['location_id'] = $location_id;
		$data_filter['department_id'] = $department_id;
		
		// if form was submitted ... first save the data to the database
		$approval = new Approval();
		if ($form_submitted == 2)
		{
			$approval->delete($data_filter);

			$user_id = Yii::app()->input->post('user_id');
			$amount = Yii::app()->input->post('amount');

			for ($idx=0; $idx<=count($user_id); $idx++)
			{
				$approval_data = array('id' => 0);
				$approval_data['approval_type'] = $approval_type;
				$approval_data['location_id'] = $location_id;
				$approval_data['department_id'] = $department_id;
				$approval_data['data_type'] = $data_type;
				$approval_data['condition_type'] = $condition_type;
				$approval_data['user_id'] = isset($user_id[$idx]) ? $user_id[$idx] : 0;
				$approval_data['amount'] = isset($amount[$idx]) ? $amount[$idx] : 0;

				if (!empty($approval_data['amount']) && !empty($approval_data['user_id']))
					$approval->saveData($approval_data);
			}
		}

		// show values selected by the user back to them
		$view_data = array();
		$view_data['approval_type'] = $approval_type;
		$view_data['data_type'] = $data_type;
		$view_data['location_id'] = $location_id;
		$view_data['department_id'] = $department_id;
		$view_data['data_type'] = $data_type;
		$view_data['condition_type'] = $condition_type;
		$view_data['form_submitted'] = $form_submitted;
	
		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$department = new Department();
		$view_data['departments'] = $department->getAll(array('order' => 'department_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name', 'user_type_id' => 1));

		// and display various approvals to the user
		$data_filter['data_type'] = $data_type;
		$data_filter['condition_type'] = $condition_type;
		$data_filter['order'] = 'amount';
		$view_data['approvals'] = $approval->getData($data_filter);
		$this->render('list', $view_data);
	}

}
