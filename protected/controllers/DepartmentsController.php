<?php

class DepartmentsController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('departments/list'));
	}

	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'departments';

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	

		// get list of all departments (with related locations too)
		$department = new Department();
		$departments = $department->getDepartmentsWithLocations();

		// which location has which department active? 
		$view_data = array();
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));
		
		// and display
		$view_data['departments'] = $departments;
		$this->render('list', $view_data);
	}

	public function actionSave()
	{
		$this->checklogin();
        if (!isset(Yii::app()->session['user_type']) ||  !in_array(Yii::app()->session['user_type'],array(4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	

		$department = new Department();
		if(!empty($department)){	
		$department->saveData();
		Yii::app()->user->setFlash('success', "Department added successfully.");
		}
		$locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";
		$department->saveLocations($department->rs['department_id'], $locations);
		
		$location = new Location();
		$location_names = array();
		foreach ($location->getData() as $location_data)
			$location_names[$location_data['location_id']] = $location_data['location_name'];
		
		$new_locations_text = "";
		if (!empty($locations))
		{
			foreach (explode(",", $locations) as $location_id)
			{
				if (isset($location_names[$location_id]))
				{
					if ($new_locations_text == "") $new_locations_text = $location_names[$location_id];
					else $new_locations_text = $new_locations_text . ", " . $location_names[$location_id];
				}
			}
		}
		
		echo json_encode(array('new_department_id' => $department->rs['department_id'], 'new_locations_text' => $new_locations_text));
	}
	
	public function actionGetDepartmentChartData()
	{
		$this->checklogin();

		// by default, show reports for year to date time frame
		$user_selected_year = Yii::app()->input->post('user_selected_year');
		if (empty($user_selected_year)) $user_selected_year = date("Y");		
		$year_start_date = $user_selected_year . "-01-01";
		$current_date = $user_selected_year . "-12-31";

		// everything is by location now
		$department = new Department();
		$location_id = Yii::app()->input->post('location_id');
		if (empty($location_id)) $location_id = 0;

		// budget and spend for this year
		$return_data['budget_data'] = $department->getBudgetSpendData($location_id, $year_start_date, $current_date);
		
		// top departments for this year
		$return_data['top_departments'] = $department->getYearlySortedData($location_id, $year_start_date, $current_date);
				
		// departments by category
		$return_data['departments_by_category'] = $department->getSpendByCategory($location_id, $year_start_date, $current_date);
				
		// vendors by departments
		$return_data['department_vendors_combo'] = $department->getSpendByVendor($location_id, $year_start_date, $current_date);
				
		// vendors by departments
		$return_data['department_vendors_type'] = $department->getVendorTypeCount($location_id, $year_start_date, $current_date);
				
		// metrics on the dashboard
		$return_data['top_metrics'] = $department->getDashboardMetrics($location_id, $year_start_date, $current_date);
		
		// return all data together
		echo json_encode($return_data);
	}

	public function actionGetDepartmentModalData()
	{
		$this->checklogin();

		// by default, show reports for year to date time frame
		$user_selected_year = Yii::app()->input->post('user_selected_year');
		if (empty($user_selected_year)) $user_selected_year = date("Y");
		$year_start_date = $user_selected_year . "-01-01";
		$current_date = $user_selected_year . "-12-31";

		// everything is by location now
		$department = new Department();
		$location_id = Yii::app()->input->post('location_id');
		if (empty($location_id)) $location_id = 0;

		// budget and spend for this year
		$return_data['budget_data'] = $department->getBudgetSpendData($location_id, $year_start_date, $current_date);
		$return_data['avg_suppliers'] = $department->getDepartmentAvgSuppliers($location_id, $year_start_date, $current_date);
		$return_data['avg_categories'] = $department->getSupplierAvgCategories($location_id, $year_start_date, $current_date);

		// return all data together
		echo json_encode($return_data);
	}

}
