<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');
require_once('vendor/autoload.php');
use HubSpot\Factory;
use HubSpot\Client\Crm\Contacts\ApiException;
use HubSpot\Client\Crm\Contacts\Model\SimplePublicObjectInput;
class EmailCroneJobController extends CommonController
{
  public function actionSystemNotification()
  { 
    set_time_limit(0);
		error_reporting(0);
    date_default_timezone_set(Yii::app()->params['timeZone']);
    $days7          = strtotime('+ 7 days');
    $days14         = strtotime('+ 14 days');
    $days30         = strtotime('+ 30 days');
    $days60         = strtotime('+ 60 days');
    $days90         = strtotime('+ 90 days');
    // $days100        = strtotime('+ 100 days');
    $dateDays7      = date('Y-m-d', $days7);
    $dateDays14     = date('Y-m-d', $days14);
    $dateDays30     = date('Y-m-d', $days30);
    $dateDays60     = date('Y-m-d', $days60);
    $dateDays90     = date('Y-m-d', $days90);
    // $dateDays100    = date('Y-m-d', $days100);
    $currentDate    = date("Y-m-d");
    
    $this->actionContractEndDate();

    $this->actionVendorEmails();
    $this->actionUserEmails();
    $this->actionContractAndVendorRequestDocumentEmails();
    $this->actionContributorInvaite();

    $sql  = 'select cont.*,contnoti.note as notification_note,contnoti.id as notification_id  from contracts cont inner join  contract_notifications contnoti on cont.contract_id=contnoti.contract_id where processed=0 and date="'.$currentDate.'"'; 
    $contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
     foreach($contractReader as $contractInfo){
      $notificationID       = $contractInfo['notification_id'];
      $notificationNote     = $contractInfo['notification_note'];
      $contractName         = $contractInfo['contract_title'];
      $notificationContID   = $contractInfo['contract_id'];
      $vendor_id            = $contractInfo['vendor_id'];
      $user_id              = $contractInfo['user_id'];
      $sbo_user_id          = $contractInfo['sbo_user_id'];
      $commercial_user_id   = $contractInfo['commercial_user_id'];
      $procurement_user_id  = $contractInfo['procurement_user_id'];
      $userArr = array($user_id,$sbo_user_id,$commercial_user_id,$procurement_user_id);
      $notificationComments  = "Custom Notification Reminder for <b>".$contractName."</b>: ".$notificationNote;
      foreach($userArr as $value){
        if(!empty($value)){

          $sql  = 'select full_name,email from users where user_id='.$value; 
          $userReader = Yii::app()->db->createCommand($sql)->queryRow();

          $notificationFlag = $notificationContID.' Custom Notification Reminder for ';
          $notification = new Notification();
          $notification->rs = array();
          $notification->rs['id'] = 0;
          $notification->rs['user_type'] = 'Client';
          $notification->rs['notification_flag'] = $notificationFlag;
          $notification->rs['user_id'] = $value;
          $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) . '">'.$notificationComments.'</a>';
          $notification->rs['notification_date'] = date("Y-m-d H:i");
          $notification->rs['read_flag'] = 0;
          $notification->rs['notification_type'] = 'Contract';
          $notification->write();

          $email = $userReader['email'];
          $name = $userReader['full_name'];
          $subject = 'Contract Custom Notification Reminder';
          $notificationText = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) . '" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
          $url = AppUrl::bicesUrl('contracts/edit/' . $notificationContID);
          EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
        }
      }
      $updateReader = Yii::app()->db->createCommand('update contract_notifications set processed=1 where id='.$notificationID)->execute();
    }

 
   
    $sql  = 'select doc.document_type,doc.approved_by,doc.approved_by_name,uploaded_by,doc.expiry_date,doc.id as doc_id,cont.* from contract_documents as doc 
          INNER JOIN contracts as cont on doc.contract_id = cont.contract_id where status ="Approved"  and date_format(approved_datetime,"%Y-%m-%d")="'.$currentDate.'"'; 
    $contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    
    if(!empty($contractReader)){
     foreach($contractReader as $contractInfo){
      $expiryDate           = $contractInfo['expiry_date'];
      $contractName         = $contractInfo['contract_title'];
      $notificationContID   = $contractInfo['contract_id'];
      $docID                = $contractInfo['doc_id'];
      $vendor_id            = $contractInfo['vendor_id'];
      $user_id              = $contractInfo['user_id'];
      $sbo_user_id          = $contractInfo['sbo_user_id'];
      $commercial_user_id   = $contractInfo['commercial_user_id'];
      $procurement_user_id  = $contractInfo['procurement_user_id'];
      $userArr = array($user_id=>$user_id,$sbo_user_id=>$sbo_user_id,$commercial_user_id=>$commercial_user_id,$procurement_user_id=>$procurement_user_id);
      $uploadedDocument = FunctionManager::contractDocument($contractInfo['document_type']);
      $approvedByID = $contractInfo['uploaded_by'];
      $userName = $contractInfo['approved_by_name'];
      $notificationFlag = $docID.'ClientApprovedContractDocument';
      $notificationComments  = "User: <b>".$userName."</b> has approved document <b>".$uploadedDocument."</b> for <b>".$contractName."</b> "; 
    
      unset($userArr[$contractInfo['approved_by']]);

      foreach($userArr as $value){
        if(!empty($value)){
          $notificationFlag .=$value;
          $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$value.' and user_type="Client"'; 
          $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

          if(empty($checkReader)){

            $sql  = 'select full_name,email from users where user_id='.$value; 
            $userReader = Yii::app()->db->createCommand($sql)->queryRow();

            $notification = new Notification();
            $notification->rs = array();
            $notification->rs['id'] = 0;
            $notification->rs['user_type'] = 'Client';
            $notification->rs['notification_flag'] = $notificationFlag;
            $notification->rs['user_id'] = $value;
            $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) .'?tab=document">'.addslashes($notificationComments).'</a>';
            $notification->rs['notification_date'] = date("Y-m-d H:i");
            $notification->rs['read_flag'] = 0;
            $notification->rs['notification_type'] = 'Contract';
            $notification->write();

            $email = $userReader['email'];
            $name = $userReader['full_name'];
            $subject = 'Contract Notification';
            $notificationText = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) . '?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
            $url = AppUrl::bicesUrl('contracts/edit/' . $notificationContID).'?tab=document';
            EmailManager::userNotification($email,$name,$subject,$notificationText,$url);


          }
        }
      }
    }
    }


  // Start: Uploaded New contract document today.
  /* $sql = 'select vs.vendor_name, doc.date_created,doc.id as doc_id,cont.* from contract_documents as doc 
      INNER JOIN contracts as cont on doc.contract_id = cont.contract_id 
      INNER JOIN vendors as vs on cont.vendor_id = vs.vendor_id
      where DATE(doc.date_created)="'.$currentDate.'"';
    $newcontractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    if(!empty($contractReader)){
     foreach($contractReader as $contractInfo){
      $contractName         = $contractInfo['contract_title'];
      $notificationContID   = $contractInfo['contract_id'];
      $docID                = $contractInfo['doc_id'];
      $vendor_id            = $contractInfo['vendor_id'];
      $user_id              = $contractInfo['user_id'];
      $sbo_user_id          = $contractInfo['sbo_user_id'];
      $commercial_user_id   = $contractInfo['commercial_user_id'];
      $procurement_user_id  = $contractInfo['procurement_user_id'];
      $userArr = array($user_id,$sbo_user_id,$commercial_user_id,$procurement_user_id);
      $notificationComments  = "<b>".$contractInfo['vendor_name']."</b> has uploader a new document to Contract ".$contractName." for approval ";
      
      foreach($userArr as $value){
        if(!empty($value)){
          $user_id = $value;
           $notificationFlag = $docID.$currentDate.'ContractDocumentcreatedToday'.$user_id;
          $cCheck  = FunctionManager::checkNotification($notificationFlag,$user_id,'Client');
          if(empty($cCheck)){
            $sql  = 'select full_name,email from users where user_id='.$value; 
            $userReader = Yii::app()->db->createCommand($sql)->queryRow();

            $notification = new Notification();
            $notification->rs = array();
            $notification->rs['id'] = 0;
            $notification->rs['user_type'] = 'Client';
            $notification->rs['notification_flag'] = $notificationFlag;
            $notification->rs['user_id'] = $value;
            $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) .'?tab=document">'.$notificationComments.'</a>';
            $notification->rs['notification_date'] = date("Y-m-d H:i");
            $notification->rs['read_flag'] = 0;
            $notification->rs['notification_type'] = 'Contract';
            $notification->write();

            $email = $userReader['email'];
            $name = $userReader['full_name'];
            $subject = 'Contract Notification';
            $notificationText = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) .'?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
            $url = AppUrl::bicesUrl('contracts/edit/' . $notificationContID).'?tab=document';
            EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
          }
        }
      }

      $sql  = "update contract_documents set notified_date='".$currentDate."' where id=".$contractInfo['doc_id']; 
      $updateReader = Yii::app()->db->createCommand($sql)->execute();
      // Vendor Notification 
     }
    }
  */

  // End: Uploaded New contract document today.

    $sql  = 'select doc.expiry_date,doc.id as doc_id,cont.* from contract_documents as doc 
          INNER JOIN contracts as cont on doc.contract_id = cont.contract_id where 
          expiry_date >="'.$currentDate.'" and expiry_date<="'.$dateDays7.'" '; 
    $contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
   
    if(!empty($contractReader)){
     foreach($contractReader as $contractInfo){
      $expiryDate         = $contractInfo['expiry_date'];
      $contractName       = $contractInfo['contract_title'];
      $notificationContID = $contractInfo['contract_id'];
      $docID              = $contractInfo['doc_id'];
      $vendor_id          = $contractInfo['vendor_id'];
      $user_id            = $contractInfo['user_id']; 
      $sbo_user_id        = $contractInfo['sbo_user_id'];
      $commercial_user_id = $contractInfo['commercial_user_id'];
      $procurement_user_id= $contractInfo['procurement_user_id'];
      $userArr = array($user_id,$sbo_user_id,$commercial_user_id,$procurement_user_id);
      $notificationComments  = "Contract: <b>".$contractName."</b> Document will expire on ".$expiryDate;
      
      foreach($userArr as $value){
        if(!empty($value)){
          $user_id = $value;
           $notificationFlag = $docID.$expiryDate.'ContractDocumentexpirein07days'.$user_id;
          $cCheck  = FunctionManager::checkNotification($notificationFlag,$user_id,'Client');
          if(empty($cCheck)){
            $sql  = 'select full_name,email from users where user_id='.$value; 
            $userReader = Yii::app()->db->createCommand($sql)->queryRow();

            $notification = new Notification();
            $notification->rs = array();
            $notification->rs['id'] = 0;
            $notification->rs['user_type'] = 'Client';
            $notification->rs['notification_flag'] = $notificationFlag;
            $notification->rs['user_id'] = $value;
            $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) .'?tab=document">'.$notificationComments.'</a>';
            $notification->rs['notification_date'] = date("Y-m-d H:i");
            $notification->rs['read_flag'] = 0;
            $notification->rs['notification_type'] = 'Contract';
            $notification->write();

            $email = $userReader['email'];
            $name = $userReader['full_name'];
            $subject = 'Contract Notification';
            $notificationText = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) .'?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
            $url = AppUrl::bicesUrl('contracts/edit/' . $notificationContID).'?tab=document';
            // EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
          }
        }
      }

      $sql  = "update contract_documents set notified_date='".$currentDate."' where id=".$contractInfo['doc_id']; 
      $updateReader = Yii::app()->db->createCommand($sql)->execute();
      // Vendor Notification 
    }
    }
     $sql  = 'select doc.expiry_date,doc.id as doc_id,cont.* from contract_documents as doc 
          INNER JOIN contracts as cont on doc.contract_id = cont.contract_id
           where expiry_date> "'.$dateDays7.'" and expiry_date<="'.$dateDays30.'" '; 
    $contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    
    if(!empty($contractReader)){
     foreach($contractReader as $contractInfo){
      $expiryDate           = $contractInfo['expiry_date'];
      $contractName         = $contractInfo['contract_title'];
      $notificationContID   = $contractInfo['contract_id'];
      $docID                = $contractInfo['doc_id'];
      $vendor_id            = $contractInfo['vendor_id'];
      $user_id              = $contractInfo['user_id'];
      $sbo_user_id          = $contractInfo['sbo_user_id'];
      $commercial_user_id   = $contractInfo['commercial_user_id'];
      $procurement_user_id  = $contractInfo['procurement_user_id'];
      $userArr = array($user_id,$sbo_user_id,$commercial_user_id,$procurement_user_id);
      $notificationComments  = "Contract: <b>".$contractName."</b> Document will expire on ".$expiryDate;
      
      foreach($userArr as $value){
        if(!empty($value)){
          $notificationFlag = $docID.$expiryDate.'contractDocumentexpirein30days'.$value;
          $user_id = $value;
          $cCheck  = FunctionManager::checkNotification($notificationFlag,$user_id,'Client');
          if(empty($cCheck)){
          $sql  = 'select full_name,email from users where user_id='.$value; 
          $userReader = Yii::app()->db->createCommand($sql)->queryRow();

          $notification = new Notification();
          $notification->rs = array();
          $notification->rs['id'] = 0;
          $notification->rs['user_type'] = 'Client';
          $notification->rs['notification_flag'] = $notificationFlag;
          $notification->rs['user_id'] = $value;
          $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) .'?tab=document">'.$notificationComments.'</a>';
          $notification->rs['notification_date'] = date("Y-m-d H:i");
          $notification->rs['read_flag'] = 0;
          $notification->rs['notification_type'] = 'Contract';
          $notification->write();

          $email = $userReader['email'];
          $name = $userReader['full_name'];
          $subject = 'Contract Notification';
          $notificationText = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) .'?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
          $url = AppUrl::bicesUrl('contracts/edit/' . $notificationContID).'?tab=document';
          // EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
         }
        }
      }
      $sql  = "update contract_documents set notified_date='".$currentDate."' where id=".$contractInfo['doc_id']; 
      $updateReader = Yii::app()->db->createCommand($sql)->execute();
     }
    }

    $sql  ='select doc.uploaded_by_type,uploaded_by,doc.expiry_date,doc.id as doc_id,cont.* from 
            contract_documents as doc 
            INNER JOIN contracts as cont on doc.contract_id = cont.contract_id 
            where date_format(date_created,"%Y-%m-%d")="'.$currentDate.'"'; 
    $contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();

    if(!empty($contractReader)){
     foreach($contractReader as $contractInfo){
      $expiryDate           = $contractInfo['expiry_date'];
      $contractName         = $contractInfo['contract_title'];
      $notificationContID   = $contractInfo['contract_id'];
      $docID                = $contractInfo['doc_id'];
      $vendor_id            = $contractInfo['vendor_id'];
      $user_id              = $contractInfo['user_id'];
      $sbo_user_id          = $contractInfo['sbo_user_id'];
      $commercial_user_id   = $contractInfo['commercial_user_id'];
      $procurement_user_id  = $contractInfo['procurement_user_id'];
      $userArr = array($user_id,$sbo_user_id,$commercial_user_id,$procurement_user_id);
      $uploadedBytype = $contractInfo['uploaded_by_type'];
      $uploadedByID = $contractInfo['uploaded_by'];
      if($uploadedBytype=='Vendor'){
        $sql  = 'select * from vendors where vendor_id='.$uploadedByID; 
        $uploadedByReader = Yii::app()->db->createCommand($sql)->queryRow();
        $supplierName = $uploadedByReader['vendor_name'];
        $notificationFlag = $docID.'supplierContractDocumentUploaded';
        $notificationComments  = "<b>".$supplierName."</b> has uploaded a new document to Contract <b>".$contractName."</b>";
        $subject = $supplierName.' has uploaded a new document to Contract '.$contractName.' for approval';
      }else {
        $sql  = 'select * from users where user_id='.$uploadedByID; 
        $uploadedByReader = Yii::app()->db->createCommand($sql)->queryRow();
        $userName = $uploadedByReader['full_name'];
        $notificationFlag = $docID.'clientContractDocumentUploaded';
        $notificationComments  = "<b>".$userName."</b> has uploaded a new document to Contract <b>".$contractName."</b>";
        $subject = 'Contract Notification';
      }
      $notificationFlagVend = $notificationFlag;

      foreach($userArr as $value){
        if(!empty($value)){
          $notificationFlag .= $value;
          $user_id = $value;
          $cCheck  = FunctionManager::checkNotification($notificationFlag,$user_id,'Client');
          if(empty($cCheck)){
           $sql  = 'select full_name,email from users where user_id='.$value; 
           $userReader = Yii::app()->db->createCommand($sql)->queryRow();
           $notification = new Notification();
           $notification->rs = array();
           $notification->rs['id'] = 0;
           $notification->rs['user_type'] = 'Client';
           $notification->rs['notification_flag'] = $notificationFlag;
           $notification->rs['user_id'] = $value;
           $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) .
           '?tab=document">'.$notificationComments.'</a>';
           $notification->rs['notification_date'] = date("Y-m-d H:i");
           $notification->rs['read_flag'] = 0;
           $notification->rs['notification_type'] = 'Contract';
           $notification->write();

           $email = $userReader['email'];
           $name  = $userReader['full_name'];
           $notificationText = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) . '?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
           $url = AppUrl::bicesUrl('contracts/edit/' . $notificationContID).'?tab=document';
           EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
         }
        }
       }
      }
    }
    // END: When a user approves a document

    
    $this->quoteSystemNotification();
    $this->vendorDocuemtnSystemNotification();
    $this->SupplierUploadedDocument();
    // END: When a user approves a document
  }

  public function SupplierUploadedDocument(){
    // Start : Supplier Uploaded Vendor Document
    $sql = 'select ce.id as cron_id,vrd.id as doc_id, u.user_id , vrd.vendor_id, u.email,u.full_name, v.vendor_name from 
      vendor_request_documents  as vrd
      LEFT JOIN cron_email as ce on vrd.id = ce.record_id
      LEFT JOIN vendors as v on v.vendor_id = vrd.vendor_id
      LEFT JOIN vendor_owner as v_o on ce.user_id = v_o.vendor_id
      LEFT JOIN users as u on (vrd.user_id = u.user_id or v_o.user_id = u.user_id)
      where ce.user_type="Supplier Uploaded Vendor Document" and ce.status="Pending" and vrd.status="Uploaded" group by u.user_id '; 
    $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    if(!empty($vendorReader)){
  
     foreach($vendorReader as $vendorInfo){  
      $vendorName         = $vendorInfo['document_title'];
      $notificationVendID = $vendorInfo['vendor_id'];
      $docID              = $vendorInfo['doc_id'];
      $cronID             = $vendorInfo['cron_id']; 
      $vendor_id          = $vendorInfo['vendor_id'];
      $user_id            = $vendorInfo['user_id'];
      $email              = $vendorInfo['email'];
      $name               = $vendorInfo['full_name'];
      $userName           = $vendorInfo['vendor_name'];
      $notificationFlag = $user_id.'SupplierUploadedVendorDocument'.$docID;
      $notificationComments  = "<b>".$userName."</b> has uploaded a new document for approval";
      $subject  = $userName." has uploaded a new document for approval";
        if(!empty($docID)){
          $cCheck  = FunctionManager::checkNotification($notificationFlag,$user_id,'Client');
          if(empty($cCheck)){
          $notification = new Notification();
          $notification->rs = array();
          $notification->rs['id'] = 0;
          $notification->rs['user_type'] = 'Client';
          $notification->rs['notification_flag'] = $notificationFlag;
          $notification->rs['user_id'] = $user_id;
          $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $notificationVendID) .
          '?tab=document">'.$notificationComments.'</a>';
          $notification->rs['notification_date'] = date("Y-m-d H:i");
          $notification->rs['read_flag'] = 0;
          $notification->rs['notification_type'] = 'Vendor';
          $notification->write();
         
          $notificationText = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $notificationVendID) . '?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
          $url = AppUrl::bicesUrl('vendors/edit/' . $notificationVendID).'?tab=document';
          EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
         }

      }
    
     if(!empty($cronID)){
        $sql = 'update cron_email set status="Sent",sent_at="'.date("Y-m-d").'" where id='.$cronID;
        $updateCron = Yii::app()->db->createCommand($sql)->execute();
     }
    } 
   }
  }

  public function vendorDocuemtnSystemNotification(){
    date_default_timezone_set(Yii::app()->params['timeZone']);
    $days1       = strtotime('- 1 day');
    $days7       = strtotime('+ 7 days');
    $days30      = strtotime('+ 30 days');
    $dateDays1   = date('Y-m-d', $days1);
    $dateDays7   = date('Y-m-d', $days7);
    $dateDays30  = date('Y-m-d', $days30);
    $currentDate = date("Y-m-d");
    $sql  = 'select doc.uploaded_by,doc.id as doc_id,doc.document_type,doc.document_title,doc.expiry_date,doc.id as doc_id,own.*,doc.vendor_id from vendor_documents as doc 
          LEFT JOIN vendor_owner as own on doc.vendor_id = own.vendor_id where expiry_date>"'.$currentDate.'" and expiry_date<="'.$dateDays7.'"';
    
    $documentReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    if(!empty($documentReader)){
      foreach($documentReader as $docInfo){
      $notificationContID = $docInfo['doc_id'];
      $expiryDate         = $docInfo['expiry_date'];
      $vendor_id          = $docInfo['vendor_id'];
      $user_id            = !empty($docInfo['user_id'])?$docInfo['user_id']:$docInfo['uploaded_by'];
      $userArr            = array($user_id);
      $uploadedDocument   = FunctionManager::vendorDocument($docInfo['document_type']);
      $notificationFlag   = $notificationContID.$expiryDate.'vendordocumentexpiry07days'.$user_id;
      $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$user_id.' and user_type="Client"'; 
      $checkReader = Yii::app()->db->createCommand($sql)->queryRow();
      $notificationComments  = "<b>".$docInfo['document_title']." (".$uploadedDocument.")</b> expires in 7 days. Please update";

        if(empty($checkReader)){

          $sql  = 'select full_name,email from users where user_id='.$user_id; 
          $userReader = Yii::app()->db->createCommand($sql)->queryRow();    
          $notification = new Notification();
          $notification->rs = array();
          $notification->rs['id'] = 0;
          $notification->rs['user_type'] = 'Client';
          $notification->rs['notification_flag'] = $notificationFlag;
          $notification->rs['user_id'] = $user_id;
          $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id) .'?tab=document">'.addslashes($notificationComments).'</a>';
          $notification->rs['notification_date'] = date("Y-m-d H:i");
          $notification->rs['read_flag'] = 0;
          $notification->rs['notification_type'] = 'Vendor';
          $notification->write();

          $email = $userReader['email'];
          $name = $userReader['full_name'];
          $subject = 'Supplier Notification';
          $notificationText = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id) . '" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
          $url = AppUrl::bicesUrl('vendors/edit/' . $vendor_id);
          EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
        } 
      }
    }

    $sql  = 'select doc.uploaded_by,doc.id as doc_id,doc.document_type,doc.document_title,doc.expiry_date,doc.id as doc_id,own.*,doc.vendor_id from vendor_documents as doc 
          LEFT JOIN vendor_owner as own on doc.vendor_id = own.vendor_id where expiry_date>"'.$dateDays7.'" and expiry_date<="'.$dateDays30.'"';
    $documentReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    
    if(!empty($documentReader)){
      foreach($documentReader as $docInfo){
      $notificationContID= $docInfo['doc_id'];
      $expiryDate        = $docInfo['expiry_date'];
      $vendor_id         = $docInfo['vendor_id'];
      $user_id           = !empty($docInfo['user_id'])?$docInfo['user_id']:$docInfo['uploaded_by'];
      $userArr           = array($user_id);
      $uploadedDocument  = FunctionManager::vendorDocument($docInfo['document_type']);
      $notificationFlag  = $notificationContID.$expiryDate.'vendordocumentexpiry30days'.$user_id;

      $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$user_id.' and user_type="Client"'; 
      $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

        if(empty($checkReader)){
          $sql  = 'select full_name,email from users where user_id='.$user_id; 
          $userReader = Yii::app()->db->createCommand($sql)->queryRow();
          $notificationComments  = "Document <b>".$docInfo['document_title']." (".$uploadedDocument.")</b> expires in 30 days. Please update";

          $notification = new Notification();
          $notification->rs = array();
          $notification->rs['id'] = 0;
          $notification->rs['user_type'] = 'Client';
          $notification->rs['notification_flag'] = $notificationFlag;
          $notification->rs['user_id'] = $user_id;
          $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id) .'?tab=document">'.addslashes($notificationComments).'</a>';
          $notification->rs['notification_date'] = date("Y-m-d H:i");
          $notification->rs['read_flag'] = 0;
          $notification->rs['notification_type'] = 'Vendor';
          $notification->write();

          $email = $userReader['email'];
          $name = $userReader['full_name'];
          $subject = 'Supplier Notification';
          $notificationText = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id).'?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
          $url = AppUrl::bicesUrl('vendors/edit/' . $vendor_id).'?tab=document';
          EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
        }
      }
    }
    // expired
    // $sql  = 'select doc.uploaded_by,doc.id as doc_id,doc.document_type,doc.document_title,doc.expiry_date,doc.id as doc_id,own.*,doc.vendor_id from vendor_documents as doc 
    //       LEFT JOIN vendor_owner as own on doc.vendor_id = own.vendor_id where expiry_date="'.$currentDate.'"';
    // $documentReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    
    // if(!empty($documentReader)){
    //   foreach($documentReader as $docInfo){
    //   $notificationContID   = $docInfo['doc_id'];
    //   $expiryDate           = $docInfo['expiry_date'];
    //   $vendor_id            = $docInfo['vendor_id'];
    //   $user_id              = !empty($docInfo['user_id'])?$docInfo['user_id']:$docInfo['uploaded_by'];
    //   //$userArr              = !empty($docInfo['user_id'])?$docInfo['user_id']:$docInfo['uploaded_by'];
    //   $uploadedDocument = FunctionManager::vendorDocument($docInfo['document_type']);
    //   $notificationFlag     = $notificationContID.$expiryDate.'vendordocumentexpired'.$user_id;

    //   $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$user_id.' and user_type="Client"'; 
    //   $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

    //     if(empty($checkReader)){

    //       $sql  = 'select full_name,email from users where user_id='.$user_id; 
    //       $userReader = Yii::app()->db->createCommand($sql)->queryRow();

    //       $notificationComments  = "Document <b>".$docInfo['document_title']." (".$uploadedDocument.")</b> has expired. Please update";

    //       $notification = new Notification();
    //       $notification->rs = array();
    //       $notification->rs['id'] = 0;
    //       $notification->rs['user_type'] = 'Client';
    //       $notification->rs['notification_flag'] = $notificationFlag;
    //       $notification->rs['user_id'] = $user_id;
    //       $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id) .'?tab=document">'.addslashes($notificationComments).'</a>';
    //       $notification->rs['notification_date'] = date("Y-m-d H:i");
    //       $notification->rs['read_flag'] = 0;
    //       $notification->rs['notification_type'] = 'Vendor';
    //       $notification->write();

    //       $email = $userReader['email'];
    //       $name = $userReader['full_name'];
    //       $subject = 'Supplier Notification';
    //       $notificationText = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id).'?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
    //       $url = AppUrl::bicesUrl('vendors/edit/' . $vendor_id).'?tab=document';
    //       EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
    //     }
    //   }
    // }

    // Approved
    $sql  = 'select doc.id as doc_id,doc.document_type,doc.document_title,doc.id as doc_id,own.user_id as user_id,doc.vendor_id,doc.approved_by_name,doc.approved_by,ven.vendor_name from vendor_documents as doc 
          INNER JOIN vendors as ven on doc.vendor_id = ven.vendor_id
          LEFT JOIN vendor_owner as own on doc.vendor_id = own.vendor_id
           where date_format(approved_datetime,"%Y-%m-%d")="'.$currentDate.'"';
    $documentReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    
    if(!empty($documentReader)){
      foreach($documentReader as $docInfo){
      $notificationContID   = $docInfo['doc_id'];
      $vendor_id            = $docInfo['vendor_id'];
      $approvedBy           = $docInfo['approved_by_name'];
      $vendorName           = $docInfo['vendor_name'];
      $user_id              = !empty($docInfo['user_id'])?$docInfo['user_id']:$docInfo['approved_by'];
      $userArr              = array($user_id);
      $uploadedDocument     = FunctionManager::vendorDocument($docInfo['document_type']);
      $notificationFlag     = $notificationContID.'vendordocumentapprovedby'.$user_id;

      $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$user_id.' and user_type="Client"'; 
      $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

        if(empty($checkReader)){

          $sql  = 'select full_name,email from users where user_id='.$user_id; 
          $userReader = Yii::app()->db->createCommand($sql)->queryRow();

          $notificationComments  = "User <b>".$approvedBy."</b> has approved document <b>".$docInfo['document_title']." (".$uploadedDocument.")</b> for supplier <b>".$vendorName."</b>";
           
          $notification = new Notification();
          $notification->rs = array();
          $notification->rs['id'] = 0;
          $notification->rs['user_type'] = 'Client';
          $notification->rs['notification_flag'] = $notificationFlag;
          $notification->rs['user_id'] = $user_id;
          $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id) .'?tab=document">'.addslashes($notificationComments).'</a>';
          $notification->rs['notification_date'] = date("Y-m-d H:i");
          $notification->rs['read_flag'] = 0;
          $notification->rs['notification_type'] = 'Vendor';
          $notification->write();

          $email = $userReader['email'];
          $name = $userReader['full_name'];
          $subject = 'Supplier Notification';
          $notificationText = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id).'?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
          $url = AppUrl::bicesUrl('vendors/edit/' . $vendor_id).'?tab=document';
          EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
        }
      }
    }
    
    // Added
    // $sql  = 'select doc.uploaded_by_type,doc.uploaded_by,doc.id as doc_id,doc.document_type,doc.document_title,doc.id as doc_id,own.user_id as user_id,doc.vendor_id,doc.approved_by_name,ven.vendor_name from vendor_documents as doc 
    //       INNER JOIN vendors as ven on doc.vendor_id = ven.vendor_id
    //       LEFT JOIN vendor_owner as own on doc.vendor_id = own.vendor_id
    //        where date_format(doc.date_created,"%Y-%m-%d")="'.$currentDate.'"';
    // $documentReader = Yii::app()->db->createCommand($sql)->query()->readAll();

    // if(!empty($documentReader)){
    //   foreach($documentReader as $docInfo){

    //   $uploadedBytype       = $docInfo['uploaded_by_type'];
    //   $uploadedByID         = $docInfo['uploaded_by'];
    //   $notificationContID   = $docInfo['doc_id'];
    //   $vendor_id            = $docInfo['vendor_id'];
    //   $approvedBy           = $docInfo['approved_by_name'];
    //   $vendorName           = $docInfo['vendor_name'];
    //   $user_id              = !empty($docInfo['user_id'])?$docInfo['user_id']:$docInfo['uploaded_by'];
    //   $userArr              = array($user_id);
    //   $uploadedDocument = FunctionManager::vendorDocument($docInfo['document_type']);

    //   if($uploadedBytype=='Vendor'){
    //     $notificationFlag     = $notificationContID.'vendordocumentuploadeddby'.$user_id;
    //     $notificationComments  = "Supplier <b>".$vendorName."</b> has uploaded a new document for approval";
    //   }else {
    //     $sql  = 'select * from users where user_id='.$uploadedByID; 
    //     $uploadedByReader = Yii::app()->db->createCommand($sql)->queryRow();
    //     $userName = $uploadedByReader['full_name'];
    //     $notificationFlag     = $notificationContID.'vendordocumentuploadeddby'.$user_id;
    //     $notificationComments  = "User <b>".$userName."</b> has uploaded a new document for approval";
    //   }
      
    //   $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$user_id.' and user_type="Client"'; 
    //   $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

    //     if(empty($checkReader)){

    //       $sql  = 'select full_name,email from users where user_id='.$user_id; 
    //       $userReader = Yii::app()->db->createCommand($sql)->queryRow();

    //       $notification = new Notification();
    //       $notification->rs = array();
    //       $notification->rs['id'] = 0;
    //       $notification->rs['user_type'] = 'Client';
    //       $notification->rs['notification_flag'] = $notificationFlag;
    //       $notification->rs['user_id'] = $user_id;
    //       $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id) .'?tab=document">'.$notificationComments.'</a>';
    //       $notification->rs['notification_date'] = date("Y-m-d H:i");
    //       $notification->rs['read_flag'] = 0;
    //       $notification->rs['notification_type'] = 'Vendor';
    //       $notification->write();

    //       $email = $userReader['email'];
    //       $name = $userReader['full_name'];
    //       $subject = 'Supplier Notification';
    //       $notificationText = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id).'?tab=document" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
    //       $url = AppUrl::bicesUrl('vendors/edit/' . $vendor_id).'?tab=document';
    //       EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
    //     }
    //   }
    // }
    
    //- @basheeralamCoder Start: Uploaded new vendors document   
     $sql  = 'select vs.vendor_name as vendor_name, doc.uploaded_by,doc.id as doc_id,doc.document_type,doc.document_title,doc.date_created,doc.id as doc_id,own.*,doc.vendor_id from vendor_documents as doc 
       LEFT JOIN vendor_owner as own on doc.vendor_id = own.vendor_id
       INNER JOIN vendors as vs on doc.vendor_id = vs.vendor_id 
       
       where date_format(doc.date_created,"%Y-%m-%d")="'.$currentDate.'" and doc.uploaded_by_type="Client" ';

    $documentReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    if(!empty($documentReader)){
      foreach($documentReader as $docInfo){
      $notificationContID   = $docInfo['doc_id'];
      $expiryDate           = $docInfo['expiry_date'];
      $vendor_id            = $docInfo['vendor_id'];
      $user_id              = !empty($docInfo['user_id'])?$docInfo['user_id']:$docInfo['uploaded_by'];
      $userArr              = array($user_id);
      $uploadedDocument     = FunctionManager::vendorDocument($docInfo['document_type']);
      $notificationFlag     = $notificationContID.$currentDate.'vendordocumentTodays'.$user_id;
      $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$user_id.' and user_type="Client"'; 
      $checkReader = Yii::app()->db->createCommand($sql)->queryRow();
        $notificationComments  = "<b>".$docInfo['vendor_name']."</b> has uploaded a new document for approval";

        if(empty($checkReader)){

          $sql  = 'select full_name,email from users where user_id='.$user_id; 
          $userReader = Yii::app()->db->createCommand($sql)->queryRow();
          $notification = new Notification();
          $notification->rs = array();
          $notification->rs['id'] = 0;
          $notification->rs['user_type'] = 'Client';
          $notification->rs['notification_flag'] = $notificationFlag;
          $notification->rs['user_id'] = $user_id;
          $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id) .'?tab=document">'.addslashes($notificationComments).'</a>';
          $notification->rs['notification_date'] = date("Y-m-d H:i");
          $notification->rs['read_flag'] = 0;
          $notification->rs['notification_type'] = 'Vendor';
          $notification->write();

          $email = $userReader['email'];
          $name = $userReader['full_name'];
          $subject = 'Supplier Notification';
          $notificationText = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_id) . '" style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
          $url = AppUrl::bicesUrl('vendors/edit/' . $vendor_id);
          EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
        } 
      }
    }
    //- @basheeralamCoder End: Uploaded new vendors document 
  }

  public function quoteSystemNotification(){
    date_default_timezone_set(Yii::app()->params['timeZone']);
    $days1          = strtotime('- 1 day');
    $dateDays1      = date('Y-m-d', $days1);
    $currentDate    = date("Y-m-d");
    $sql  = 'select created_by_user_id,commercial_lead_user_id,quote_id,quote_name,quote_status,
    closing_date from quotes where  date_format(closing_date,"%Y-%m-%d")="'.$dateDays1.'"'; 
    $quoteReader = Yii::app()-> db->createCommand($sql)->query()->readAll();
    
    if(!empty($quoteReader)){
     foreach($quoteReader as $quoteInfo){
      $expiryDate         = $quoteInfo['closing_date'];
      $quoteName          = $quoteInfo['quote_name'];
      $notificationQuoteID= $quoteInfo['quote_id'];
      $user_id            = $quoteInfo['created_by_user_id'];
      $secondary_id       = $quoteInfo['commercial_lead_user_id'];
      $userArr            = array($user_id,$secondary_id);
      

      $notificationFlag = $notificationQuoteID.$expiryDate.'quoteADaybeforeClosing';
      $notificationComments = "<b>".$notificationQuoteID." ".$quoteName."</b> is ending soon";   
      foreach($userArr as $value){
        if(!empty($value)){
          
          $notificationFlag .= $value;
          $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$value.' and user_type="Client"'; 
          $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

          if(empty($checkReader)){
            
            $sql  = 'select full_name,email from users where user_id='.$value; 
            $userReader = Yii::app()->db->createCommand($sql)->queryRow();

            $notification = new Notification();
            $notification->rs = array();
            $notification->rs['id'] = 0;
            $notification->rs['user_type'] = 'Client';
            $notification->rs['notification_flag'] = $notificationFlag;
            $notification->rs['user_id'] = $value;
            $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $notificationQuoteID) .'">'.$notificationComments.'</a>';
            $notification->rs['notification_date'] = date("Y-m-d H:i");
            $notification->rs['read_flag'] = 0;
            $notification->rs['notification_type'] = 'Quote';
            $notification->write();
   
            $email = $userReader['email'];
            $name = $userReader['full_name'];
            $subject = 'eSourcing Notification';
            $notificationText = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $notificationQuoteID).'"  style="font-size:18px;color:#54595F;text-decoration: none;">'.$notificationComments.'</a>';
            $url = AppUrl::bicesUrl('quotes/edit/' . $notificationQuoteID);
            EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
          }
        }
      }
      

      $sql = "SELECT q.* FROM  quote_vendors q WHERE q.quote_id = $notificationQuoteID ";
      $vendors = Yii::app()->db->createCommand($sql)->query()->readAll();
       // Vendor Notification 
        if(!empty($vendors)){/*
          $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
          foreach($vendorReader as $value){

            $valueVendorID = $value['vendor_id'];
            $notificationFlag = $notificationQuoteID.$expiryDate.'vendoRquoteADaybeforeClosing'.$valueVendorID;

            $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$valueVendorID.' and user_type="Vendor"'; 
            $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

            if(empty($checkReader)){
              $email = $value['emails'];
              $name  = $value['contact_name'];
              $subject = 'Notification';
              $url = Yii::app()->createAbsoluteUrl('supplier/vendor/documents');
              $notificationText = '<a href="'.$url.'">'.addslashes($notificationComments).'</a>';
              $notificationTextEmail = '<a href="'.$url.'" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$notificationComments.'</a>';
              
              $notification = new Notification();
              $notification->rs = array();
              $notification->rs['id'] = 0;
              $notification->rs['user_type'] = 'Vendor';
              $notification->rs['notification_flag'] = $notificationFlag;
              $notification->rs['user_id'] = $valueVendorID;
              $notification->rs['notification_text'] = $notificationText;
              $notification->rs['notification_date'] = date("Y-m-d H:i");
              $notification->rs['read_flag'] = 0;
              $notification->rs['notification_type'] = 'Quote';
              $notification->write();

              EmailManager::userNotification($email,$name,$subject,$notificationTextEmail,$url);
            }
          }
          */}
        }
    }
    // END: When a user approves a document

    //START: When a quote is cancelled
    $sql  = 'select created_by_user_id,commercial_lead_user_id,quote_id,quote_name,cancelled_date from quotes where  quote_status = "Cancelled" and date_format(cancelled_date,"%Y-%m-%d")="'.$currentDate.'"'; 
    $quoteReader = Yii::app()-> db->createCommand($sql)->query()->readAll();
    
    if(!empty($quoteReader)){
     foreach($quoteReader as $quoteInfo){
      $expiryDate           = $quoteInfo['cancelled_date'];
      $quoteName            = $quoteInfo['quote_name'];
      $notificationQuoteID  = $quoteInfo['quote_id'];
      $user_id              = $quoteInfo['created_by_user_id'];
      $secondary_id         = $quoteInfo['commercial_lead_user_id'];
      $userArr              = array($user_id,$secondary_id);
      
      $notificationFlag = $notificationQuoteID.$expiryDate.'quoteCancelled';
      $notificationComments  = "<b>".$notificationQuoteID." ".$quoteName."</b> has been cancelled";   
      foreach($userArr as $value){
        if(!empty($value)){/*
          
          $notificationFlag .= $value;
          $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$value.' and user_type="Client"'; 
          $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

          if(empty($checkReader)){
            
            $sql  = 'select full_name,email from users where user_id='.$value; 
            $userReader = Yii::app()->db->createCommand($sql)->queryRow();

            $notification = new Notification();
            $notification->rs = array();
            $notification->rs['id'] = 0;
            $notification->rs['user_type'] = 'Client';
            $notification->rs['notification_flag'] = $notificationFlag;
            $notification->rs['user_id'] = $value;
            $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $notificationQuoteID) .'">'.$notificationComments.'</a>';
            $notification->rs['notification_date'] = date("Y-m-d H:i");
            $notification->rs['read_flag'] = 0;
            $notification->rs['notification_type'] = 'Quote';
            $notification->write();

            $email = $userReader['email'];
            $name = $userReader['full_name'];
            $subject = 'Notification';
            $notificationText = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $notificationQuoteID).'"  style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$notificationComments.'</a>';
            $url = AppUrl::bicesUrl('quotes/edit/' . $notificationQuoteID);
            EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
          }
        */}
      }
      

      $sql = "SELECT q.* FROM quote_vendors q WHERE q.quote_id = $notificationQuoteID ";
      $vendors = Yii::app()->db->createCommand($sql)->query()->readAll();
       // Vendor Notification 
        if(!empty($vendors)){/*
          $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
            foreach($vendorReader as $value){

            $valueVendorID = $value['vendor_id'];
            $notificationFlag = $notificationQuoteID.$expiryDate.'vendoRquoteADaybeforeClosing'.$valueVendorID;

            $sql  = 'select * from notifications where notification_flag="'.$notificationFlag.'" and user_id='.$valueVendorID.' and user_type="Vendor"'; 
            $checkReader = Yii::app()->db->createCommand($sql)->queryRow();

            if(empty($checkReader)){
              $email = $value['emails'];
              $name  = $value['contact_name'];
              $subject = 'Notification';
              $url = Yii::app()->createAbsoluteUrl('supplier/vendor/documents');
              $notificationText = '<a href="'.$url.'">'.addslashes($notificationComments).'</a>';
              $notificationTextEmail = '<a href="'.$url.'" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$notificationComments.'</a>';
              
              $notification = new Notification();
              $notification->rs = array();
              $notification->rs['id'] = 0;
              $notification->rs['user_type'] = 'Vendor';
              $notification->rs['notification_flag'] = $notificationFlag;
              $notification->rs['user_id'] = $valueVendorID;
              $notification->rs['notification_text'] = $notificationText;
              $notification->rs['notification_date'] = date("Y-m-d H:i");
              $notification->rs['read_flag'] = 0;
              $notification->rs['notification_type'] = 'Quote';
              $notification->write();

              EmailManager::userNotification($email,$name,$subject,$notificationTextEmail,$url);
            }
          }
          */}
        }
    }

    // END: When a quote is  cancelled
  }

  public function actionSavingNotification(){
        $days1          = strtotime('+ 1 days');
        $dateDays1      = date('Y-m-d', $days1);
        $days7          = strtotime('+ 7 days');
        $dateDays7      = date('Y-m-d', $days7);
        $currentDate    = date("Y-m-d");
        $sql ='SELECT s.title saving_title, s.id saving_id,ms.title as milestone_title,ms.id as milestone_id, ms.due_date,u.email ,u.user_id,u.full_name,
            CASE
             WHEN date_format(ms.due_date,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays1.'" THEN "24 hours"
             WHEN date_format(ms.due_date,"%Y-%m-%d") between "'.$dateDays1.'" and "'.$dateDays7.'" THEN "7 days"
              ELSE "NULL"
            END as duration
           FROM  saving_milestone as ms
           INNER JOIN savings as s on ms.saving_id = s.id
           INNER JOIN users as u on s.user_id = u.user_id 
           WHERE date_format(ms.due_date,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays7.'" ';
        
        $savingReader = Yii::app()->db->createCommand($sql)->query()->readAll();
        foreach($savingReader as $savingInfo){
          $savingTitle          = $savingInfo['saving_title'];
          $milestoneTitle       = $savingInfo['milestone_title'];
          $milestoneID          = $savingInfo['milestone_id'];
          $savingID             = $savingInfo['saving_id'];
          $email                = $savingInfo['email'];
          $name                 = $savingInfo['full_name'];
          $milestoneDate        = $savingInfo['due_date'];
          $durationDays         = $savingInfo['duration'];
          $user_id              = !empty($savingInfo['user_id'])?$savingInfo['user_id']:0;
       
          $comments  = "Milestone <b>".$milestoneTitle."</b> is due in ".$durationDays; 
          $flag = $milestoneID.$milestoneDate.'savingmilestoneduedate'.$durationDays.$user_id;
          $cCheck  = FunctionManager::checkNotification($flag,$user_id,'Client');
          if(empty($cCheck)){
            if(strpos(Yii::app()->getBaseUrl(true),"usg")){
             $text = '<a href="' . AppUrl::bicesUrl('strategicSourcing/edit/' . $savingID) . '">'.$comments.'</a>';
             $textEmail = '<a href="' . AppUrl::bicesUrl('strategicSourcing/edit/' . $savingID) . '" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$comments.'</a>';
             $url = AppUrl::bicesUrl('strategicSourcing/edit/' . $savingID);
            }else{
              $text = '<a href="' . AppUrl::bicesUrl('savings/edit/' . $savingID) . '">'.$comments.'</a>';
              $textEmail = '<a href="' . AppUrl::bicesUrl('savings/edit/' . $savingID) . '" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$comments.'</a>';
              $url = AppUrl::bicesUrl('savings/edit/' . $savingID);
            }
            
            $subject = 'Milestone Notification';
            FunctionManager::saveNotification($user_id,$text,$flag,'Client','Saving Milestone');
            EmailManager::userNotification($email,$name,$subject,$textEmail,$url);
          }
        }
  }

  public function actionContractNoticePeriod()
  {
    date_default_timezone_set(Yii::app()->params['timeZone']);
    $days7          = strtotime('+ 7 days');
    $days14         = strtotime('+ 14 days');
    $days30         = strtotime('+ 30 days');
    $days60         = strtotime('+ 60 days');
    $days90         = strtotime('+ 90 days');
    $dateDays7      = date('Y-m-d', $days7);
    $dateDays14     = date('Y-m-d', $days14);
    $dateDays30     = date('Y-m-d', $days30);
    $dateDays60     = date('Y-m-d', $days60);
    $dateDays90     = date('Y-m-d', $days90);

    $currentDate    = date("Y-m-d");
    $sql  = 'select * from (select "user_related" as type,c.contract_title,c.contract_id,c.break_clause as notice_date,
            u.user_id,u.full_name,u.email,
            "" as vendor_id," " as vendor_name,"" as vendor_email,
            CASE
             WHEN date_format(break_clause,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays7.'" THEN "7 days"
             WHEN date_format(break_clause,"%Y-%m-%d") between "'.$dateDays7.'" and "'.$dateDays14.'" THEN "14 days"
             WHEN date_format(break_clause,"%Y-%m-%d") between "'.$dateDays14.'" and "'.$dateDays30.'" THEN "30 days"  
              ELSE "NULL"
            END as duration

             from contracts c
            INNER JOIN users u on u.user_id in(c.user_id,sbo_user_id,commercial_user_id,procurement_user_id) 
        where date_format(break_clause,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays30.'" 
          union 
        select "vendor_related" as type, c.contract_title,c.contract_id,c.break_clause as notice_date,
            "" as user_id,"" as full_name,"" as email,
            v.vendor_id,v.vendor_name,v.emails as vendor_email,
          CASE
             WHEN date_format(break_clause,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays7.'" THEN "7 days"
             WHEN date_format(break_clause,"%Y-%m-%d") between "'.$dateDays7.'" and "'.$dateDays14.'" THEN "14 days"
             WHEN date_format(break_clause,"%Y-%m-%d") between "'.$dateDays14.'" and "'.$dateDays30.'" THEN "30 days"
              ELSE "NULL"
            END as duration from contracts c

        INNER JOIN ( select inv.vendor_id,vendor_name,emails,invt.vendor_id as supper_vendor_id from vendors inv left join vendor_teammember invt on inv.vendor_id=invt.teammember_id  )  v on c.vendor_id=v.vendor_id or c.vendor_id=v.supper_vendor_id 

        where date_format(break_clause,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays30.'" 
          ) as cont order by notice_date asc';
    
    $contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    foreach($contractReader as $contractInfo){
      $contractName         = $contractInfo['contract_title'];
      $contID               = $contractInfo['contract_id'];
      $user_id              = !empty($contractInfo['user_id'])?$contractInfo['user_id']:0;
      $vendor_id            = $contractInfo['vendor_id'];
      $noticeDate           = $contractInfo['notice_date'];
      $durationDays         = $contractInfo['duration'];

      $comments= "Contract <b>".$contractName."</b> Notice Period Is Due In ".$durationDays;
      $flag    = $contID.$noticeDate.'contractnoticeperiod'.$durationDays.$user_id;
      $cCheck  = FunctionManager::checkNotification($flag,$user_id,'Client');
      $vFlag   = $contID.$noticeDate.'contractnoticeperiod'.$durationDays.'vendor'.$vendor_id;
      $vCheck  = FunctionManager::checkNotification($vFlag,$vendor_id,'Vendor');

      if($contractInfo['type']=="user_related" && empty($cCheck)){
          $text  = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $contID) . '">'.$comments.'</a>';
          $email = $contractInfo['email'];
          $name  = $contractInfo['full_name'];
          $subject = 'Contract Notification';
          $textEmail = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $contID) . '" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$comments.'</a>';
          $url = AppUrl::bicesUrl('contracts/edit/' . $contID);
          FunctionManager::saveNotification($user_id,$text,$flag,'Client','Contract');
          EmailManager::userNotification($email,$name,$subject,$textEmail,$url);
      }else if($contractInfo['type']=="vendor_related" && empty($vCheck)){  // Vendor Notification 
            /*$email = $contractInfo['vendor_email'];
            $name  = $contractInfo['vendor_name'];
            $subject = 'Notification';
            $url = Yii::app()->createAbsoluteUrl('supplier/contracts/view',array('id'=>$contID));
            $text = '<a href="'.$url.'">'.$comments.'</a>';
            $textEmail = '<a href="'.$url.'" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$comments.'</a>';
            
            FunctionManager::saveNotification($vendor_id,$text,$vFlag,'Vendor','Contract');
            EmailManager::userNotification($email,$name,$subject,$textEmail,$url);*/
      }
    }
  }
  public function actionContractEndDate()
  {
    date_default_timezone_set(Yii::app()->params['timeZone']);
    $days7          = strtotime('+ 7 days');
    $days14         = strtotime('+ 14 days');
    $days30         = strtotime('+ 30 days');
    $days60         = strtotime('+ 60 days');
    $days90         = strtotime('+ 90 days');
    $days100        = strtotime('+ 100 days');
    $dateDays7      = date('Y-m-d', $days7);
    $dateDays14     = date('Y-m-d', $days14);
    $dateDays30     = date('Y-m-d', $days30);
    $dateDays60     = date('Y-m-d', $days60);
    $dateDays90     = date('Y-m-d', $days90);
    $dateDays100    = date('Y-m-d', $days100);
    $currentDate    = date("Y-m-d");

    $sql  = 'select * from (select "user_related" as type,c.contract_title,c.contract_id,c.contract_end_date as end_date,
            u.user_id,u.full_name,u.email,
            "" as vendor_id," " as vendor_name,"" as vendor_email,
            CASE
             WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays7.'" THEN "7 days"
             WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays7.'" and "'.$dateDays14.'" THEN "14 days"
             WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays14.'" and "'.$dateDays30.'" THEN "30 days"
             WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays30.'" and "'.$dateDays60.'" THEN "60 days"
             WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays60.'" and "'.$dateDays90.'" THEN "90 days"
             WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays90.'" and "'.$dateDays100.'" THEN "100 days"
              ELSE "NULL"
            END as duration

            from contracts c
            INNER JOIN users u on u.user_id in(c.user_id,sbo_user_id,commercial_user_id,procurement_user_id) 
             where date_format(contract_end_date,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays100.'" 
          union 
        select "vendor_related" as type, c.contract_title,c.contract_id,c.contract_end_date as end_date,
            "" as user_id,"" as full_name,"" as email,
            v.vendor_id,v.vendor_name,v.emails as vendor_email,
          CASE
              WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays7.'" THEN "7 days"
              WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays7.'"  and "'.$dateDays14.'" THEN "14 days"
              WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays14.'" and "'.$dateDays30.'" THEN "30 days"
              WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays30.'" and "'.$dateDays60.'" THEN "60 days"
              WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays60.'" and "'.$dateDays90.'" THEN "90 days"
              WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays90.'" and "'.$dateDays100.'" THEN "100 days"
              ELSE "NULL"
            END as duration from contracts c

        INNER JOIN ( select inv.vendor_id,vendor_name,emails,invt.vendor_id as supper_vendor_id from vendors inv left join vendor_teammember invt on inv.vendor_id=invt.teammember_id  )  v on c.vendor_id=v.vendor_id or c.vendor_id=v.supper_vendor_id 
        where date_format(contract_end_date,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays100.'" 
      
          ) as cont order by end_date asc';
          
        $contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    foreach($contractReader as $contractInfo){
      $contractName         = $contractInfo['contract_title'];
      $contID               = $contractInfo['contract_id'];
      $user_id              = $contractInfo['user_id'];
      $vendor_id            = $contractInfo['vendor_id'];
      $endDate              = $contractInfo['end_date'];
      $durationDays         = $contractInfo['duration'];
      $comments  = "Contract <b>".$contractName."</b> Notice Period is ".date("d/m/Y",strtotime($endDate));
      $comments  = "Contract <b>".$contractName."</b> expires in ".$durationDays;
      
      $flag   = $contID.$endDate.'contractexpiresin'.$durationDays.$user_id;
      $cCheck = FunctionManager::checkNotification($flag,$user_id,'Client');
      $vFlag  = $contID.$endDate.'contractnoticeperiod'.$durationDays.'vendor'.$vendor_id;
      $vCheck = FunctionManager::checkNotification($vFlag,$vendor_id,'Vendor');



      if($contractInfo['type']=="user_related" && empty($cCheck)){
          $text  = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $contID) . '">'.$comments.'</a>';
          $email = $contractInfo['email'];
          $name  = $contractInfo['full_name'];
          $subject = 'Contract Notification';
          $textEmail = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $contID) . '" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$comments.'</a>';
          $url = AppUrl::bicesUrl('contracts/edit/' . $contID);

          FunctionManager::saveNotification($user_id,$text,$flag,'Client','Contract');
          EmailManager::userNotification($email,$name,$subject,$textEmail,$url);
      }else if($contractInfo['type'] == "vendor_related" && empty($vCheck)){  // Vendor Notification 
          /*  $email = $contractInfo['vendor_email'];
            $name  = $contractInfo['vendor_name'];
            $subject = 'Notification';
            $url = Yii::app()->createAbsoluteUrl('supplier/contracts/view',array('id'=>$contID));
            $text = '<a href="'.$url.'">'.$comments.'</a>';
            $textEmail = '<a href="'.$url.'" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$comments.'</a>';
            
            FunctionManager::saveNotification($vendor_id,$text,$vFlag,'Vendor','Contract');
            EmailManager::userNotification($email,$name,$subject,$textEmail,$url);*/
      }
    }
  }

  public function actionVendorEmails(){

    set_time_limit(0);
		error_reporting(0);

    date_default_timezone_set(Yii::app()->params['timeZone']);
    $currentDate = date("Y-m-d H:i:s");
    $sql = "SELECT user_type,cm.id as cron_id,qvc.contact_name,v.vendor_id,record_id,v.reference, qvc.contact_email as emails, qvc.id as qvc_id FROM `cron_email` as cm 
    inner join quote_vendors v on cm.user_id=v.vendor_id and v.quote_id=record_id and cm.send_to='Vendor'
    inner join quote_vendor_contact qvc on qvc.quote_vendor_id= v.id where cm.status='Pending'";
    $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    // echo "<pre>"; print_r($vendorReader); exit;
    if(!empty($vendorReader)){
      foreach($vendorReader as $value){
        $vendorID   = $value['vendor_id'];
        $vendorName = $value['contact_name'];
        $vendorEmail= $value['emails'];
        $recordID   = $value['record_id'];
        $cronID     = $value['cron_id'];
        $reference  = $value['reference'];
        $qouteVendID= $value['qvc_id'];

        // Start:   This email is sent to vendors for whom quote is re-opened
        if($value['user_type']=='Re-Open Quote'){
          if(!empty($vendorEmail)){
            $sql = "SELECT q.quote_name,q.quote_id,q.opening_date,q.closing_date,q.quote_status as quote_status,qrh.notes, qrh.created_by_id as user_id, qrh.created_by_name as user_name FROM `quote_reopen_history` qrh inner join quotes as q on qrh.quote_id=q.quote_id where qrh.vendor_id=". $vendorID." and qrh.quote_id=".$recordID;
            $quoteHistory = Yii::app()->db->createCommand($sql)->queryRow();
            if(EmailManager::vendorQuoteReopen($quoteHistory,$vendorName,$vendorEmail,$reference)){
              $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
              $updateCron = Yii::app()->db->createCommand($sql)->execute();
            } 
          }
        }
        // End:   This email is sent to vendors for whom quote is re-opened

        // Start:   This email is sent to vendors for whom quote is re-opened
        if($value['user_type']=='Quote Invite'){
          if(!empty($vendorEmail)){
            $sql = "SELECT q.quote_name,q.quote_id,q.opening_date,q.closing_date,q.quote_status as quote_status FROM quotes as q where q.quote_id=".$recordID;
            $quoteHistory = Yii::app()->db->createCommand($sql)->queryRow();
              
             if(EmailManager::vendorQuoteInvitation($quoteHistory,$vendorName,$vendorEmail,$reference)){
                $sql1 = 'update quote_vendor_contact set status=1 where id='.$qouteVendID; 
                Yii::app()->db->createCommand($sql1)->execute(); 
               
               $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
               $updateCron = Yii::app()->db->createCommand($sql)->execute();
            
             } 
          }
        }
        // End:   This email is sent to vendors for whom quote is re-opened

        // Start:   This email is sent to vendors for whom quote is cancelled
        if($value['user_type']=='Cancellation Quote'){
          if(!empty($vendorEmail)){
            $sql = "SELECT q.quote_name,q.quote_id,q.opening_date,q.closing_date,q.quote_status as quote_status FROM quotes as q where q.quote_id=".$recordID;
            $quoteReader= Yii::app()->db->createCommand($sql)->queryRow();
          
            $subject = 'eSourcing Notification';
            $notificationText = "<b>".$quoteReader['quote_id']." ".$quoteReader['quote_name']."</b> has been cancelled";
            $url = Yii::app()->createAbsoluteUrl('supplier/default/login');
              
            if(EmailManager::userNotification($vendorEmail,$vendorName,$subject,$notificationText,$url)){
              $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
              $updateCron = Yii::app()->db->createCommand($sql)->execute();
            }
          }
        }
        // End:   This email is sent to vendors for whom quote is cancelled
      
      }

    }
  }

  public function actionUserEmails(){

    set_time_limit(0);
		error_reporting(0);

    date_default_timezone_set(Yii::app()->params['timeZone']);
    $currentDate = date("Y-m-d H:i:s");
    $sql = "SELECT user_type,id as cron_id,record_id,user_id FROM `cron_email`  where status='Pending' and send_to='User'";

    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();

   
    if(!empty($reader)){
      foreach($reader as $value){
        $recordID = $value['record_id'];
        $cronID   = $value['cron_id'];
        $userID   = $value['user_id'];
        $userID   = $value['user_id'];

        // Start:   This email is sent to vendors for whom quote is re-opened
        if($value['user_type']=='Quote Invite'){
          $sql = "SELECT q.quote_name,q.quote_id,q.opening_date,q.closing_date,q.quote_status as quote_status  FROM quotes as q where q.quote_id=".$recordID;
          $quoteHistory = Yii::app()->db->createCommand($sql)->queryRow();
         if(EmailManager::userQuote($userID,$quoteHistory)){
          $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
          $updateCron = Yii::app()->db->createCommand($sql)->execute();
         } 
        }
        // End:   This email is sent to vendors for whom quote is re-opened
         // Start:   This email is sent to vendors for whom quote is cancelled
        if($value['user_type']=='Cancellation Quote'){
            $sql = "SELECT * FROM users where user_id=".$userID;
            $userReader = Yii::app()->db->createCommand($sql)->queryRow();
            $userEmail  = $userReader['email'];
            $userName   = $userReader['full_name'];

            $sql = "SELECT q.quote_name,q.quote_id,q.opening_date,q.closing_date,q.quote_status as quote_status  FROM quotes as q where q.quote_id=".$recordID;
            $quoteReader = Yii::app()->db->createCommand($sql)->queryRow();
            $subject = 'eSourcing Notification';
            $notificationText = "<span style='color:#54595F;'><b>".$quoteReader['quote_id']." ".$quoteReader['quote_name']."</b> has been cancelled</span>";
              $url = Yii::app()->createAbsoluteUrl('app/login');
              if(EmailManager::userNotification($userEmail,$userName,$subject,$notificationText,$url)){
                $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
                $updateCron = Yii::app()->db->createCommand($sql)->execute();
              }
          }
        // End:   This email is sent to vendors for whom quote is cancelled
      }

    }
  
  
  }



  public function actionHubspotApi(){
    
    $conn = mysqli_connect('localhost', APP_FOREMAN_USER, APP_FOREMAN_PASS, APP_FOREMAN_DB);
    if (mysqli_connect_errno()) error_log( "Failed to connect to MySQL: " . mysqli_connect_error() );

    $sql = "SELECT id,firstname,lastname,admin_email,mobile_no,subdomain FROM foreman_clients where (send_to is NULL or send_to = '') ";
    $result = mysqli_query($conn, $sql);
    if ( $result > 0 ) {
     while($userData = mysqli_fetch_array($result,MYSQLI_ASSOC)){
      $client = Factory::createWithAccessToken('pat-eu1-5e1fc94a-f4eb-4040-b18c-725a15f80f09');
      $properties = [
       'company'   => $userData['subdomain'],
       'email'   => $userData['admin_email'],
       'firstname' => $userData['firstname'],
       'lastname'  => $userData['lastname'],
       'phone'   => $userData['mobile_no'],
       'website'   => $userData['subdomain'].'oboloo.software'
      ];
      
     $objectInputForCreate = new \HubSpot\Client\Crm\Contacts\Model\SimplePublicObjectInput(
      ['properties' => $properties,'associations' => null]);
      
      try {
       $apiResponse = $client->crm()->contacts()->basicApi()->create($objectInputForCreate);
       $sql = " UPDATE foreman_clients SET send_to=1 WHERE id = '". $userData['id']."'";
       mysqli_query($conn, $sql);
      } catch (ApiException $e) {
       echo "Exception when calling basic_api->create: ", $e->getMessage();
      }
    }
   }
   mysqli_close($conn);
  }

  public function actionContractAndVendorRequestDocumentEmails(){

    set_time_limit(0);
		error_reporting(0);

    date_default_timezone_set(Yii::app()->params['timeZone']);
    $currentDate = date("Y-m-d H:i:s");
    //Start: Request contract document
    $sql = "SELECT ce.id as cron_id, crd.vendor_id,crd.contract_title,crd.note,crd.reference,crd.supplier_contact_name,crd.supplier_contact_email,crd.contract_type FROM `cron_email` ce inner join (select inn_crd.*,inn_c.contract_title from contract_request_documents inn_crd inner join contracts as inn_c on inn_crd.contract_id=inn_c.contract_id) as crd on ce.record_id = crd.id
    where  ce.status='Pending' and ce.send_to='Vendor' and ce.user_type='Request Contract Document'";
    $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    
    foreach($vendorReader as $value){
    
      $vendorID      = $value['vendor_id'];
      $contactName   = unserialize($value['supplier_contact_name']);
      $contactEmail  = unserialize($value['supplier_contact_email']);
      $contractTitle = $value['contract_title'];
      $note      = $value['note'];
      $cronID    = $value['cron_id'];
      $reference = $value['reference'];
      $contractType  = FunctionManager::contractDocument($value['contract_type']);
      // foreach($contactEmail as $key=>$contactValue){
      //   $vendorContactName  = $contactName[$key];
      //   $vendorContactEmail = $contactValue;
        $vendorContactName = $contactName; 
        $vendorContactEmail = $contactEmail;
        if(EmailManager::vendorRequestContractDocument($vendorContactName,$vendorContactEmail,$reference,$contractType,$contractTitle,$note)){
          $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
          $updateCron = Yii::app()->db->createCommand($sql)->execute();
        } 
      // }
    }
   //END: Request contract document

    //Start: Request vendor document
    $sql = "SELECT ce.id as cron_id, vrd.vendor_id,vrd.note,vrd.reference,vrd.supplier_contact_name,vrd.supplier_contact_email,vrd.vendor_document_type FROM 
    `cron_email` ce inner join vendor_request_documents as vrd on ce.record_id = vrd.id
        
    where  ce.status='Pending' and ce.send_to='Vendor' and ce.user_type='Request Vendor Document'";
    $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    
    foreach($vendorReader as $value){
      $vendorID     = $value['vendor_id'];
      $contactName  = unserialize($value['supplier_contact_name']);
      $contactEmail = unserialize($value['supplier_contact_email']);
      $note         = $value['note'];
      $cronID       = $value['cron_id'];
      $reference    = $value['reference'];
      $contractType = FunctionManager::vendorDocument($value['vendor_document_type']);
      // foreach($contactEmail as $key=>$contactValue){
      //   $vendorContactName  = $contactName[$key];
      //   $vendorContactEmail = $contactValue;
       $vendorContactName  = $contactName;
       $vendorContactEmail = $contactEmail;
        if(EmailManager::vendorRequestDocument($vendorContactName,$vendorContactEmail,$reference,$contractType,$note)){
          $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
          $updateCron = Yii::app()->db->createCommand($sql)->execute();
        } 
      // }
    
    }
   //END: Request vendor document


    //Start: Request Vendor Document Onboard
    $sql = "SELECT ce.id as cron_id, vrd.vendor_id,vrd.note,vrd.reference,vrd.supplier_contact_name,vrd.supplier_contact_email,vrd.vendor_document_type FROM 
    `cron_email` ce inner join vendor_request_documents_onboard as vrd on ce.record_id = vrd.id
        
    where  ce.status='Pending' and ce.send_to='Vendor' and ce.user_type='Request Vendor Document Onboard'";
    $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    foreach($vendorReader as $value){
      $vendorID     = $value['vendor_id'];
      $contactName  = unserialize($value['supplier_contact_name']);
      $contactEmail = unserialize($value['supplier_contact_email']);
      $note         = $value['note'];
      $cronID       = $value['cron_id'];
      $reference    = $value['reference'];
      $contractType = FunctionManager::vendorDocumentOnboard($value['vendor_document_type']);
      foreach($contactEmail as $key=>$contactValue){
        $vendorContactName  = $contactName[$key];
        $vendorContactEmail = $contactValue;
        if(EmailManager::vendorRequestDocumentOnboard($vendorContactName,$vendorContactEmail,$reference,$contractType,$note)){
          $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
          $updateCron = Yii::app()->db->createCommand($sql)->execute();
        } 
      }
    }
   //END: Request Vendor Document Onboard
  
  }

  public function actionContributorInvaite(){
    set_time_limit(0);
		error_reporting(0);
    date_default_timezone_set(Yii::app()->params['timeZone']);
    $currentDate = date("Y-m-d H:i:s");
    //Start: Request contract document
    $sql = "SELECT ce.id as cron_id, ce.user_id, qu.* FROM `cron_email` ce
    inner join quote_user qu on qu.quote_id = ce.record_id and ce.user_id = qu.user_id
    where  ce.status='Pending' and ce.send_to='User' and ce.user_type='Quote Contributor'";
    $contributorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    foreach($contributorReader as $vValue){
      $cronID = $vValue['cron_id'];
      $QuoteID = $vValue['quote_id'];
      $contributorID = $vValue['user_id'];
      if(EmailManager::contributeUserQuote($QuoteID, $contributorID)){
        $sql = 'update cron_email set status="Sent",sent_at="'.$currentDate.'" where id='.$cronID;
        $updateCron = Yii::app()->db->createCommand($sql)->execute();
      }
    }
      
  }
}
