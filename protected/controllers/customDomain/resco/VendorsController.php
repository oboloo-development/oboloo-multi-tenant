<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');
class VendorsController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('vendors/list'));
	}
	public function actionAnalysis()
	{	date_default_timezone_set(Yii::app()->params['timeZone']);
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'vendors_analysis';

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
		$this->getCurrency($user_currency);


		// by default, show reports for year to date time frame
		$year_start_date = date("Y-01-01");
		$current_date = date("Y-m-d"); 
		
		// get top vendors, top locations and top categories data for the time being
		$view_data = array();
		$report = new Report();
		$view_data['top_vendors'] = $report->getReportTotals("", $year_start_date, $current_date, "V", "", "", "", 10);

		// preferred vs. non preferred spending by category
		$preferred_vendors_used = $non_preferred_vendors_used = 0;
		$vendor = new Vendor();
		$view_data['vendors_combo'] = $vendor->getVendorsCountsAndAmounts($year_start_date, $current_date);
		$view_data['vendors_by_category'] = $vendor->getVendorsByCategory($year_start_date, $current_date);
		$view_data['category_vendors_combo'] = array();
		foreach ($view_data['vendors_by_category'] as $category_index => $category_data)
		{
			$view_data['category_vendors_combo'][$category_index] = array( 'vendor_count' => count($category_data) );
			
			$total_for_category = 0;
			$category_name = "";
			foreach ($category_data as $category_vendor_row) 
			{
				$total_for_category += $category_vendor_row['total'];
				$category_name = $category_vendor_row['category'];
				if ($category_vendor_row['preferred_flag']) $preferred_vendors_used += 1;
				else $non_preferred_vendors_used += 1;
			}
			
			$view_data['category_vendors_combo'][$category_index]['vendor_total'] = $total_for_category; 
			$view_data['category_vendors_combo'][$category_index]['category'] = $category_name; 
		}

		// and hence independent preferred vs. non preferred counts
		$total_vendors_used = $preferred_vendors_used + $non_preferred_vendors_used;
		if ($total_vendors_used)
		{
			$view_data['preferred_vendors_used'] = ($preferred_vendors_used * 100) / $total_vendors_used;
			$view_data['non_preferred_vendors_used'] = ($non_preferred_vendors_used * 100) / $total_vendors_used;
		}
		else $view_data['preferred_vendors_used'] = $view_data['non_preferred_vendors_used'] = 0; 

		// even monthly category by vendor needs to be sorted chronologically
		$view_data['vendors_by_month'] = $vendor->getVendorsByMonth($year_start_date, $current_date);
		ksort($view_data['vendors_by_month']);

		$view_data['vendors_by_department'] = $vendor->getVendorsByDepartment($year_start_date, $current_date);
		$view_data['metrics']['vendor'] = $vendor->getDashboardMetrics($year_start_date, $current_date);		
		
		$spend = new Report();
		$view_data['metrics']['spend'] = $spend->getDashboardMetrics($year_start_date, $current_date);
		$this->render('analysis', $view_data);
	}

	
	public function actionList()
	{	
		date_default_timezone_set(Yii::app()->params['timeZone']);
		// is someone already logged in?
	    error_reporting(0);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'vendors_list';
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));
		$documentsTitle = FunctionManager::vendorDocument();
		$currentDocumentDate = date('Y-m-d');
		// by default, show reports for year to date time frame
        $user_selected_year = Yii::app()->input->post('user_selected_year');
        $user_selected_year = date("Y");
        $startDate = "2010-01-01";
        $currentDate = $user_selected_year . "-12-31";

		// get list of all vendors (with pagination of course)
		$vendor = new Vendor();

		// and display
		$view_data['vendors'] = array();

		$view_data['vendorStatistics'] = $vendor->getVendorStatistics();
		 
		$view_data['vendorMetric'] = $vendor->getVendorMetric();
		// echo "<pre>"; print_r($vendor->getVendorMetric()); exit;
		/*$view_data['vendors_by_category'] = $vendor->getNonAndPreferredVendorsByIndustry($startDate, $currentDate);*/
		$view_data['vendors_by_category'] = '';
	
		$report = new Report();
		$view_data['performance_avg'] = $report->getVendorAvgPerformance();

		 // echo $performance_avg['total_vendors']; exit;

		$notification = new Notification();
        $view_data['notifications'] = $notification->getLastestForSupplier(5);

		$contract = new Contract();
        $view_data['top_contracts'] = $contract->getTopContracts($startDate, date("Y-m-d"));
		if (Yii::app()->request->isAjaxRequest)
		{
			$preferred_icon = '<i class="fa fa-check" style="color: #00bc9d;text-align:center;display:inline-block;padding-right:16px;"></i>';
			$non_preferred_icon = '';

			$contractIcon = '<i class="fa fa-check " style="color: #00bc9d; padding-left: 28px;"></i>';
			$contractIconNone = '';

			$industry_id = 0;
			if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id']))
				$industry_id = $_REQUEST['industry_id'];			

			$subindustry_id = 0;
			if (isset($_REQUEST['subindustry_id']) && !empty($_REQUEST['subindustry_id']))
				$subindustry_id = $_REQUEST['subindustry_id'];			

			$preferred_flag = -1;
			$supplier_status = '';
			if (isset($_REQUEST['preferred_flag'])) $preferred_flag = $_REQUEST['preferred_flag'];
			if (isset($_REQUEST['supplier_status'])) $supplier_status = $_REQUEST['supplier_status'];

			$location_id = 0;
			if (isset($_REQUEST['location_id']) && !empty($_REQUEST['location_id']))
				$location_id = $_REQUEST['location_id'];
			
			$department_id = 0;
			if (isset($_REQUEST['department_id']) && !empty($_REQUEST['department_id']))
				$department_id = $_REQUEST['department_id'];	

			set_time_limit(0);
			$total_vendors = 0;
			if (!empty($industry_id) || !empty($subindustry_id) || $preferred_flag != -1)
				$total_vendors = $vendor->getVendorCount($industry_id, $subindustry_id, $preferred_flag);
			else $total_vendors = $vendor->getTotalRows();
			
			$searched_records = $total_vendors;
			$search_for = "";
			if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value']))
			{
				$search_for = $_REQUEST['search']['value'];
				$searched_records = $vendor->getSearchedRows($search_for, $industry_id, $subindustry_id, $preferred_flag, $location_id, $department_id);
			}
			
			$order_by = array();
			if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])){
				$order_by = $_REQUEST['order'];
			}else{
				//$order_by[1] = 'vendor_name';
			}
			
			$database_values = $vendor->getVendors(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $industry_id, $subindustry_id, $preferred_flag,$supplier_status, 'vendors', '', $location_id, $department_id);
			$vendors = array();
			$total_vendors=0;
			foreach ($database_values as $vendor_data)
			{
				$avgScore = '';
				if(10*$vendor_data['avg_score']==0 and 10*$vendor_data['avg_score']<=1){
                  $avgScore = "<span class='btn btn-primary' style='background:#c1bfbf !important;border-color:#c1bfbf !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$vendor_data['avg_score'])."%</span>";
                }else if(10*$vendor_data['avg_score']>=1 and 10*$vendor_data['avg_score']<=60){
                  $avgScore = "<span class='btn btn-primary' style='background:#F0DFDF !important;border-color:#F0DFDF !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$vendor_data['avg_score'])."%</span>";
                }else if(10*$vendor_data['avg_score']>60 and 10*$vendor_data['avg_score']<=80){
                  $avgScore = "<span class='btn btn-primary' style='background:#FDEC90 !important;border-color:#FDEC90 !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$vendor_data['avg_score'])."%</span>";
                }else{
                  $avgScore = "<span class='btn btn-primary' style='background:#DDF1D5 !important;border-color:#DDF1D5 !important ;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$vendor_data['avg_score'])."%</span>";
                }

				$riskIcon='';
				if(!empty($vendor_data['risk_score_value'])){
					if($vendor_data['risk_score_value']==1){
						$riskIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#00bc9d"></i>';
					}else if($vendor_data['risk_score_value']==2){
						$riskIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#FDEC90"></i>';
					}else {
						$riskIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#f7778c "></i>';
					}
				}

				$susIcon='';
				if(!empty($vendor_data['score_value'])){
					if($vendor_data['score_value']==1){
						$susIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#00bc9d;"></i>';
					}else if($vendor_data['score_value']==2){
						$susIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#FDEC90;"></i>';
					}else {
						$susIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#f7778c;"></i>';
					}
				}


				

				$expredDoc = empty($vendor_data['expired_document'])?0:$vendor_data['expired_document'];
				$pendingDoc = empty($vendor_data['total_pending'])?0:$vendor_data['total_pending'];

				$expiredDocColor = $expredDoc==0?'#DDF1D5':'#F0DFDF';
				$pendingDocColor = $pendingDoc==0?'#DDF1D5':'#F0DFDF';
				
				$expredDoc = "<div style='text-align:center;padding-right: 28px;'><span class='btn btn-primary' style='background: ".$expiredDocColor." !important;border-color: #F0DFDF  !important;border-radius: 10px;color:#2d9ca2 !important'>".$expredDoc."</span></div>";
				$pendingDoc = "<div style='text-align:center;padding-right: 28px;'><span class='btn btn-primary' style='background: ".$pendingDocColor." !important;border-color: #F0DFDF  !important;border-radius: 10px;color:#2d9ca2 !important'>".$pendingDoc."</span></div>"; 
	
				$current_data = array();
                $total_vendors++;
				$edit_link = '';
				$edit_link .= '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_data['vendor_id']) . '">';
                $edit_link .= '<button class="btn btn-sm btn-success view-btn">View/Edit</button></a>';
				$current_data[] = $edit_link;
				
				$current_data[] = '<span class="notranslate">'.$vendor_data['vendor_name'].'</span>'.($vendor_data['active']==0?" <span class='deactivated_vendor'></span>":"");
				$current_data[] = $vendor_data['industry'];
				$current_data[] = $vendor_data['subindustry'];
				$current_data[] = $expredDoc;
				$current_data[] = $pendingDoc;
				//$current_data[] = $netScore;
				$current_data[] = "<div style='text-align:center;padding-right: 16px;'>".$avgScore."</div>";

				if ($vendor_data['preferred_flag']) $current_data[] = $preferred_icon;
				else $current_data[] = $non_preferred_icon;

				$current_data[] = (!empty($vendor_data['contract_id'])?$contractIcon:$contractIconNone);
				
				$current_data[] ="<div style='text-align:center;padding-right:16px;'>".$susIcon."</div>";
				$current_data[] ="<div style='text-align:center;padding-right:16px;'>".$riskIcon."</div>";
          
				$vendors[] = $current_data;
				
			}
		

			$total_vendors = $vendor->getVendors(0, 'none', 'none', $search_for, $order_by, $industry_id, $subindustry_id, $preferred_flag,$supplier_status, 'vendors', '', $location_id, $department_id);
			$total_vendors = count($total_vendors);
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $total_vendors,
                "recordsFiltered" => $total_vendors,
                "data"            => $vendors
            );
			echo json_encode($json_data);exit;	
		}
		else
		{
			$industry = new Industry();
			$view_data['industries'] = $industry->getAll(array('soft_deleted' =>  '0','ORDER' => 'value'));
			$location = new Location();
			$view_data['locations'] = $location->getData(array('order' => 'location_name'));
			$user = new User();
			$userData = $user->getOne(array('user_id'=>Yii::app()->session['user_id']));
			$view_data['vendor_visit'] = $userData['vendor_visit'];
			if($userData['vendor_visit'] !=1){
				$user->rs = array();
				$user->rs['user_id'] = $userData['user_id'];
				$user->rs['vendor_visit'] = 1;
				$user->write();
			}
			$view_data['vendors'] = array();
			$this->render('list', $view_data);
		}		
	}

	public function actionArchiveList()
	{	date_default_timezone_set(Yii::app()->params['timeZone']);
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'vendors_list';

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));

		// by default, show reports for year to date time frame
        $user_selected_year = Yii::app()->input->post('user_selected_year');
        
      
        $user_selected_year = date("Y");

        $startDate = "2010-01-01";
        $currentDate = $user_selected_year . "-12-31";

		// get list of all vendors (with pagination of course)
		$vendor = new Vendor();

		// and display
		$view_data['vendors'] = array();
		
		
		
		if (Yii::app()->request->isAjaxRequest)
		{
			$preferred_icon = '<i class="fa fa-check" style="color: #00bc9d;padding-left: 20px;"></i>';
			$non_preferred_icon = '';
			
			$contractIcon = '<i class="fa fa-check " style="color: #00bc9d; padding-left: 20px;"></i>';
			$contractIconNone = '';
			

			$industry_id = 0;
			if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id']))
				$industry_id = $_REQUEST['industry_id'];			

			$subindustry_id = 0;
			if (isset($_REQUEST['subindustry_id']) && !empty($_REQUEST['subindustry_id']))
				$subindustry_id = $_REQUEST['subindustry_id'];			

			$preferred_flag = -1;
			$supplier_status = '';
			if (isset($_REQUEST['preferred_flag'])) $preferred_flag = $_REQUEST['preferred_flag'];
			if (isset($_REQUEST['supplier_status'])) $supplier_status = $_REQUEST['supplier_status'];

			$location_id = 0;
			if (isset($_REQUEST['location_id']) && !empty($_REQUEST['location_id']))
				$location_id = $_REQUEST['location_id'];
			
			$department_id = 0;
			if (isset($_REQUEST['department_id']) && !empty($_REQUEST['department_id']))
				$department_id = $_REQUEST['department_id'];	

			set_time_limit(0);
			$total_vendors = 0;
			if (!empty($industry_id) || !empty($subindustry_id) || $preferred_flag != -1)
				$total_vendors = $vendor->getVendorCount($industry_id, $subindustry_id, $preferred_flag);
			else $total_vendors = $vendor->getTotalRows();
			
			$searched_records = $total_vendors;
			$search_for = "";
			if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value']))
			{
			 $search_for = $_REQUEST['search']['value'];
			 $searched_records = $vendor->getSearchedRows($search_for, $industry_id, $subindustry_id, $preferred_flag);
			}
			
			$order_by = array();
			if (isset($_REQUEST['order']) && is_array($_REQUEST['order']))
			  $order_by = $_REQUEST['order'];
			
			$database_values = $vendor->getVendors(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $industry_id, $subindustry_id, $preferred_flag,$supplier_status,
				'vendors','yes',$location_id,$department_id);
			$vendors = array();
			$total_vendors=0;
			foreach ($database_values as $vendor_data)
			{
				$current_data = array();
                $total_vendors++;
				$edit_link = '';
				$edit_link .= '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_data['vendor_id']). '" class="notranslate">';
                $edit_link .= '<button class="btn btn-sm btn-success view-btn notranslate">View/Edit</button></a>';
				$current_data[] = $edit_link;
				
				$current_data[] = '<span class="notranslate">'.$vendor_data['vendor_name'].($vendor_data['active']==0?" <span class='deactivated_vendor'></span>":"").'</span>';
				$current_data[] = '<span class="notranslate">'.$vendor_data['industry'].'</span>';
				$current_data[] = '<span class="notranslate">'.$vendor_data['subindustry'].'</span>';
				
				if ($vendor_data['preferred_flag']) $current_data[] = '<span class="notranslate">'.$preferred_icon.'</span>';
				else $current_data[] = '<span class="notranslate">'.$non_preferred_icon.'</span>';

				$current_data[] = '<span class="notranslate">'.(!empty($vendor_data['contract_id'])?$contractIcon:$contractIconNone).'</span>';
				
				$vendors[] = $current_data;
			}
			$total_vendors = $vendor->getVendors(0, 'none', 'none', $search_for, $order_by, $industry_id, $subindustry_id, $preferred_flag,$supplier_status,
				'vendors','yes',$location_id,$department_id);
			$total_vendors = count($total_vendors);
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $total_vendors,
                "recordsFiltered" => $total_vendors,
                "data"            => $vendors
            );
			echo json_encode($json_data);		
		}
		else
		{
			$industry = new Industry();
			$view_data['industries'] = $industry->getAll(array( 'ORDER' => 'value'));
			$location = new Location();
			$view_data['locations'] = $location->getData(array('order' => 'location_name'));
			
			$view_data['vendors'] = array();
			$sql ='update users set vendor_visit=1 where user_id='.Yii::app()->session['user_id'];
			$vendorVisit = Yii::app()->db->createCommand($sql)->execute();
			$view_data['vendor_visit'] = $vendorVisit;

			$this->render('archive_list', $view_data);
		}		
	}
	
	public function actionAddNew()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

        if (!isset(Yii::app()->session['user_type'])  || !in_array(Yii::app()->session['user_type'],array(1,4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	
		
		// may be the vendor name already exists?
		$vendor = new Vendor();
		$vendor_name = Yii::app()->input->post('vendor_name');
		$vendor_id = $vendor->getIdForName($vendor_name);
		
		// now update other parameters as necessary
		$input_fields = array('contact_name', 'industry_id', 'subindustry_id', 'payment_term_id', 'shipping_method_id');
		$vendor->rs['vendor_id'] = $vendor_id;
		foreach ($input_fields as $input_field)
			$vendor->rs[$input_field] = Yii::app()->input->post($input_field);
		$vendor->write();
	}

	public function actionEdit($vendor_id = 0)
	{  
	   date_default_timezone_set(Yii::app()->params['timeZone']);
	   error_reporting(0);
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'vendors_edit';
		$redID = 0;
	    if (!isset(Yii::app()->session['user_type'])  || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted'))
		{	
            $override_data = array();
			$vendor = new Vendor();
			if(!empty($_POST['emails']) && empty($_POST['vendor_id'])){
				$vendorDuplicate = $vendor->getOne(array('emails' => $_POST['emails'],'vendor_id !='=>$vendor_id));

				if(!empty($vendorDuplicate['emails'])){
					Yii::app()->user->setFlash('error', "Ops! &nbsp;   Supplier with this email already taken.");
					if(!empty($_POST['vendor_id'])){
						$redID = $_POST['vendor_id'];
						$vendor_id = $redID; 
					}
					 $this->redirect(AppUrl::bicesUrl('vendors/edit/'.$redID ));
				}
			}else if(!empty($_POST['vendor_id'])){
				$vendorDuplicate = $vendor->getOne(array('emails' => $_POST['emails'],'vendor_id !='=>$_POST['vendor_id']));
				if(!empty($vendorDuplicate['emails'])){
					Yii::app()->user->setFlash('error', "Ops! &nbsp;   Supplier with this email already taken.");
					if(!empty($_POST['vendor_id'])){
						$redID = $_POST['vendor_id'];
						$vendor_id = $redID; 
					}
					 $this->redirect(AppUrl::bicesUrl('vendors/edit/'.$redID ));
				}
			}

			$documentForm = Yii::app()->input->post('form_document');

			// START: Due to some reason active is changing, so I am writing the below code
			if(!empty($_POST['vendor_id'])){
				$oldRecord = $vendor->getOne(array('vendor_id'=>$_POST['vendor_id']));
			}
			  
			if (Yii::app()->input->post('active'))
			{
				$vendor->fields['active'] = 'N'; 
				$override_data['active'] = 1;
				$vendor->saveData($override_data);
			}

			if(!empty($_POST['vendor_id'])){
				$vendor->fields['active'] = 'N'; 
				$override_data['active'] = $oldRecord['active'];
				$override_data['quick_created'] = $oldRecord['active'];
			// END: Due to some reason active is changing, so I am writing the below code
				$vendor->saveData($override_data);
			}else{
				$vendor->saveData();
			}
			$vendor_id = $vendor->rs['vendor_id'];
			$uploadedFiles = Yii::app()->session['uploaded_files'];
			if(!empty($uploadedFiles))
				foreach ($uploadedFiles as $fileValue){
					if (empty($fileValue['vendor_id'])){
						$file_name = $fileValue['file_name'];
						$file_description =$fileValue['file_description'];
						$document_type = $fileValue['file_type'];
						$document_status = $fileValue['document_status'];
						$expiry_date = $fileValue['expiry_date'];
						
						if (!is_dir('uploads/vendor_documents')) mkdir('uploads/vendor_documents');
						if (!is_dir('uploads/vendor_documents/' . $vendor_id))
							mkdir('uploads/vendor_documents/' . $vendor_id);
						if (!is_dir('uploads/vendor_documents/'))
							mkdir('uploads/vendor_documents/');
						if(@rename('uploads/vendor_documents/' . $file_name, 'uploads/vendor_documents/' . $vendor_id . '/' . $file_name)){
							$vendor->saveDocument($vendor_id,$file_description,$file_name,$document_type,$document_status,$expiry_date);
						}
					}
				}

			$vendor_payment = new VendorPayment();
			$vendor_payment->delete(array('vendor_id' => $vendor->rs['vendor_id']));

			$vendor_location = new VendorLocation();
			$vendor_location->delete(array('vendor_id' => $vendor->rs['vendor_id']));
			foreach($_POST['location_id'] as $location_id){
				$vendLocation = new VendorLocation();
				$vendLocation->rs = array();
				$vendLocation->rs['vendor_id'] = $vendor_id;
				$vendLocation->rs['location_id']  = $location_id;
				$vendLocation->rs['created_at']   = date('Y-m-d H:i:s');
				$vendLocation->write();
			}

			$vendor_dept = new VendorDepartment();
			$vendor_dept->delete(array('vendor_id' => $vendor->rs['vendor_id']));
			foreach($_POST['department_id'] as $department_id){
				$vendLocation = new VendorDepartment();
				$vendLocation->rs = array();
				$vendLocation->rs['vendor_id'] = $vendor_id;
				$vendLocation->rs['location_id']  = 0;
				$vendLocation->rs['department_id']= $department_id;
				$vendLocation->rs['created_at']   = date('Y-m-d H:i:s');
				$vendLocation->write();
			}
			
			//Start: vendor log
			//$oldVendorRecord
			$comment = '';
			 if(!empty($oldRecord['vendor_id'])){
                 
                $comment = '';
                if(trim($oldRecord['vendor_name'])!=trim($_POST['vendor_name'])){
                    $comment .= '<strong>Supplier Name:</strong> <span class="title-text"><b>'. $oldRecord['vendor_name']. ' </b>Changed to <b>'.$_POST['vendor_name'].'</b></span><br/>';
                }

                if(trim($oldRecord['contact_name'])!=trim($_POST['contact_name'])){
                    $comment .= '<strong>Contact Name:</strong> <span class="title-text"><b>'. (!empty($oldRecord['contact_name'])?$oldRecord['contact_name']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['contact_name'])?$_POST['contact_name']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['emails'])!=trim($_POST['emails'])){
                    $comment .= '<strong>Email Address:</strong> <span class="title-text"><b>'. $oldRecord['emails']. ' </b>Changed to <b>'.$_POST['emails'].'</b></span><br/>';
                }

                if(trim($oldRecord['phone_1'])!=trim($_POST['phone_1'])){
                    $comment .= '<strong>Phone Number:</strong> <span class="title-text"><b>'.  (!empty($oldRecord['phone_1'])?$oldRecord['phone_1']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['phone_1'])?$_POST['phone_1']:'NULL').'</b></span><br/>';
                }

                if(trim($oldRecord['phone_2'])!=trim($_POST['phone_2'])){
                    $comment .= '<strong>Mobile Number:</strong> <span class="title-text"><b>'. (!empty($oldRecord['phone_2'])?$oldRecord['phone_2']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['phone_2'])?$_POST['phone_2']:'NULL').'</b></span><br/>';
                }

                if(trim($oldRecord['external_id'])!=trim($_POST['external_id'])){
                    $comment .= '<strong>Company Assigned Supplier ID:</strong> <span class="title-text"><b>'. (!empty($oldRecord['external_id'])?$oldRecord['external_id']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['external_id'])?$_POST['external_id']:'NULL').'</b></span><br/>';
                }

                if(trim($oldRecord['industry_id'])!=trim($_POST['industry_id'])){

                	$insutry = new Industry();
                    $insutryPosted = $insutry->getOne(array('id'=>$_POST['industry_id']));
                    $insutryExisted = $insutry->getOne(array('id'=>$oldRecord['industry_id']));

                    $comment .= '<strong>Supplier Industry:</strong> <span class="title-text"><b>'. (!empty($insutryExisted['value'])?$insutryExisted['value']:'NULL'). ' </b>Changed to <b>'.(!empty($insutryPosted['value'])?$insutryPosted['value']:'NULL').'</b></span><br/>';
                }

                if(trim($oldRecord['subindustry_id'])!=trim($_POST['subindustry_id'])){

                	$subinsutry = new Subindustry();
                    $subinsutryPosted = $subinsutry->getOne(array('id'=>$_POST['subindustry_id']));
                    $subinsutryExisted = $subinsutry->getOne(array('id'=>$oldRecord['subindustry_id']));


                    $comment .= '<strong>Supplier Sub Industry:</strong> <span class="title-text"><b>'. (!empty($subinsutryExisted['value'])?$subinsutryExisted['value']:'NULL'). ' </b>Changed to <b>'.(!empty($subinsutryPosted['value'])?$subinsutryPosted['value']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['number'])!=trim($_POST['number'])){
                    $comment .= '<strong>Company Number:</strong> <span class="title-text"><b>'. (!empty($oldRecord['number'])?$oldRecord['number']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['number'])?$_POST['number']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['tax_number'])!=trim($_POST['tax_number'])){
                    $comment .= '<strong>Tax Number:</strong> <span class="title-text"><b>'. (!empty($oldRecord['tax_number'])?$oldRecord['tax_number']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['tax_number'])?$_POST['tax_number']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['payment_term_id'])!=trim($_POST['payment_term_id'])){
                	
                	$payment_term = new PaymentTerm();
                	$paymentPosted = $payment_term->getOne(array('id'=>$_POST['payment_term_id']));
                    $paymentExisted = $payment_term->getOne(array('id'=>$oldRecord['payment_term_id']));

                    $comment .= '<strong>Payment Term:</strong> <span class="title-text"><b>'. (!empty($paymentExisted['value'])?$paymentExisted['value']:'NULL'). ' </b>Changed to <b>'.(!empty($paymentPosted['value'])?$paymentPosted['value']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['shipping_term_id'])!=trim($_POST['shipping_term_id'])){

                	$shppingTerm = new ShippingTerm();
                	$shppingTermPosted = $shppingTerm->getOne(array('id'=>$_POST['shipping_term_id']));
                    $shppingTermExisted = $shppingTerm->getOne(array('id'=>$oldRecord['shipping_term_id']));

                    $comment .= '<strong>Incoterms:</strong> <span class="title-text"><b>'. (!empty($shppingTermExisted['value'])?$shppingTermExisted['value']:'NULL'). ' </b>Changed to <b>'.(!empty($shppingTermPosted['value'])?$shppingTermPosted['value']:'NULL').'</b></span><br/>';
                }
				 
                if(trim($oldRecord['shipping_method_id'])!=trim($_POST['shipping_method_id'])){

                	$shippingType = new ShippingMethod();
                	$shippingTypePosted = $shippingType->getOne(array('id'=>$_POST['shipping_method_id']));
                    $shippingTypeExisted = $shippingType->getOne(array('id'=>$oldRecord['shipping_method_id']));


                    $comment .= '<strong>Shipping Method:</strong> <span class="title-text"><b>'. (!empty($shippingTypeExisted['value'])?$shippingTypeExisted['value']:'NULL'). ' </b>Changed to <b>'.(!empty($shippingTypePosted['value'])?$shippingTypePosted['value']:'NULL').'</b></span><br/>';
                }
                if(!empty($_POST['preferred_flag']) && trim($oldRecord['preferred_flag'])!=trim($_POST['preferred_flag'])){
                    $comment .= '<strong>To indicate that this is a preferred supplier:</strong> <span class="title-text"><b>'. (!empty($oldRecord['preferred_flag'])?$oldRecord['preferred_flag']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['preferred_flag'])?'Prefered':'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['website'])!=trim($_POST['website'])){
                    $comment .= '<strong>Company Website:</strong> <span class="title-text"><b>'. (!empty($oldRecord['website'])?$oldRecord['website']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['website'])?$_POST['website']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['address_1'])!=trim($_POST['address_1'])){
                    $comment .= '<strong>Address Line 1:</strong> <span class="title-text"><b>'. (!empty($oldRecord['address_1'])?$oldRecord['address_1']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['address_1'])?$_POST['address_1']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['address_2'])!=trim($_POST['address_2'])){
                    $comment .= '<strong>Address Line 2:</strong> <span class="title-text"><b>'. (!empty($oldRecord['address_2'])?$oldRecord['address_2']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['address_2'])?$_POST['address_2']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['city'])!=trim($_POST['city'])){
                    $comment .= '<strong>City:</strong> <span class="title-text"><b>'. (!empty($oldRecord['city'])?$oldRecord['city']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['city'])?$_POST['city']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['state'])!=trim($_POST['state'])){
                    $comment .= '<strong>County/State:</strong> <span class="title-text"><b>'. (!empty($oldRecord['state'])?$oldRecord['state']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['state'])?$_POST['state']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['country'])!=trim($_POST['country'])){
                    $comment .= '<strong>Country:</strong> <span class="title-text"><b>'. (!empty($oldRecord['country'])?$oldRecord['country']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['country'])?$_POST['country']:'NULL').'</b></span><br/>';
                }
                if(trim($oldRecord['zip'])!=trim($_POST['zip'])){
                    $comment .= '<strong>Zip/Postal Code:</strong> <span class="title-text"><b>'. (!empty($oldRecord['zip'])?$oldRecord['zip']:'NULL'). ' </b>Changed to <b>'.(!empty($_POST['zip'])?$_POST['zip']:'NULL').'</b></span><br/>';
                }
                
            }
		 
			if(!empty($comment)){
				$vendorLog = new VendorLog();

				$vendorLog->rs = array();
				$vendorLog->rs['vendor_id'] = $oldRecord['vendor_id'];
				$vendorLog->rs['user_id'] = Yii::app()->session['user_id'];
				// $vendorLog->rs['contract_status'] = Yii::app()->input->post('contract_status');
				$vendorLog->rs['comment'] = !empty($comment)?$comment:"";
				$vendorLog->rs['created_datetime'] = date('Y-m-d H:i:s');
				$vendorLog->rs['updated_datetime'] = date('Y-m-d H:i:s');
				$vendorLog->write();
				// $contractName = Yii::app()->input->post('contract_title');
			}
			//End: vendor log
			if(!empty($documentForm)){
              $this->redirect(AppUrl::bicesUrl('vendors/edit/'.$vendor->rs['vendor_id'].'?tab=document'));
            }else{
			  Yii::app()->user->setFlash('success', "Success! Supplier updated successfully.");
			  $this->redirect(AppUrl::bicesUrl('vendors/edit/' . $vendor->rs['vendor_id']));
			}
		}
		// get data for the selected vendor
		$view_data = array('vendor_id' => $vendor_id);
		$vendor = new Vendor();

		$sql = "select sum(score_value)/count(id) as avg_score from vendor_performance where vendor_id=".$vendor_id;
		$view_data['avg_score'] = Yii::app()->db->createCommand($sql)->queryRow();

		$view_data['vendor'] = $vendor->getOne(array('vendor_id' => $vendor_id));

		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));
		
		// and also payment information for the vendor
		$vendor_payment = new VendorPayment();
		$view_data['vendor_payments'] = $vendor_payment->getData(array('vendor_id' => $vendor_id));

		$vendor_location = new VendorLocation();
		$view_data['vendor_location'] = $vendor_location->getData(array('vendor_id' => $vendor_id));

		$vendor_department = new VendorDepartment();
		$view_data['vendor_department'] = $vendor_department->getData(array('vendor_id' => $vendor_id));
		// show comment work

		// need data for various drop downs
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll(array('ORDER' => 'id'));

		$payment_type = new PaymentType();
		$view_data['payment_types'] = $payment_type->getAll();

		$shipping_term = new ShippingTerm();
		$view_data['shipping_terms'] = $shipping_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));
		$user_selected_year = date("Y");

        $year_start_date = "2010-01-01";
        $current_date = $user_selected_year . "-12-31";

		Yii::app()->session['user_selected_year'] = $user_selected_year;

		$order = new Order();
		$view_data['orders'] = $order->getSupplierOrders($vendor_id,$year_start_date, $current_date);

		$view_data['vendors_combo'] = $vendor->getVendorOrderAndAmounts($vendor_id,$year_start_date, $current_date);
		$contract = new Contract();
		$view_data['contracts'] = $contract->getVendorContracts($vendor_id,$year_start_date, $current_date);

		$view_data['contracts_by_location'] = $contract->getVendorContractLocationTotals($vendor_id,$year_start_date, $current_date);
		$quote = new Quote();
		$saving = new Saving();
		$venderAprrovalLogs = new VendorApprovalLog();
		$view_data['quotes'] = $quote->getVendorQuotes($vendor_id);
		$view_data['savings'] = $saving->getVendorSavings($vendor_id);
        $view_data['documentList'] = $vendor->getVendorsDocuments($vendor_id);
        $view_data['documentListOnboard'] = $vendor->getVendorsDocumentsOnboard($vendor_id);
        $view_data['vendorStatistics'] = $vendor->getVendorDetailStatistics($vendor_id);
        $view_data['owners'] = $vendor->getOwners($vendor_id);
        $view_data['quickComparison'] = $vendor->getQuickComparision($vendor_id);
        $view_data['vendorPerformance'] = $vendor->getVendorPerformance($vendor_id);
        $view_data['vendorRisk'] = $vendor->getVendorRisk($vendor_id);
        $view_data['vendorSustainability'] = $vendor->getVendorSustainability($vendor_id);
        $view_data['documenArchivetList'] = $vendor->getVendorsDocuments($vendor_id,"archive");
        $view_data['documenArchivetListOnboard'] = $vendor->getVendorsDocumentsOnboard($vendor_id,"archive");
        $view_data['vendorApprovelLogs']  = $venderAprrovalLogs->getApprovalLogs($vendor_id);
		// and display
		unset(Yii::app()->session['uploaded_files']);unset(Yii::app()->session['uploaded_files_display']);
		$vendorLogs = new VendorLog();
		$view_data['vendorLogs'] = $vendorLogs->getVendorLogs($vendor_id);

		//Start vendor document filter 
		$view_data['filter_document_type'] = !empty($_POST['filter_document_type']) ? $_POST['filter_document_type'] : '';
		$view_data['flter_document_status'] = !empty($_POST['flter_document_status']) ? $_POST['flter_document_status'] : '';
		$view_data['fltr_doc_contract_supplier_status'] = !empty($_POST['fltr_doc_contract_supplier_status']) ? $_POST['fltr_doc_contract_supplier_status'] : '';
		//END vendor document filter 

		$vendorApprovalHistory = new VendorApprovalHistory();
		$view_data['vendor_approval_history'] = $vendorApprovalHistory->getApprovalHistroy($vendor_id);
		$this->render('edit', $view_data);

	}

	public function actionArchiveEdit($vendor_id = 0)
	{  	date_default_timezone_set(Yii::app()->params['timeZone']);
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'vendors_edit';
		$redID = 0;

        if (!isset(Yii::app()->session['user_type'])  || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		// if form was submitted ... first save the data to the database
		
		// get data for the selected vendor
		$view_data = array('vendor_id' => $vendor_id);
		$vendor = new Vendor();
		$view_data['vendor'] = $vendor->getVendors($vendor_id , 0,10,"",array(),0,0,-1,"",'archive_vendors');

		// and also payment information for the vendor
		$vendor_payment = new VendorPayment();
		$view_data['vendor_payments'] = $vendor_payment->getData(array('vendor_id' => $vendor_id));

		// need data for various drop downs
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll(array('ORDER' => 'id'));

		$payment_type = new PaymentType();
		$view_data['payment_types'] = $payment_type->getAll();

		$shipping_term = new ShippingTerm();
		$view_data['shipping_terms'] = $shipping_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));

		$user_selected_year = date("Y");
        $year_start_date = "2010-01-01";
        $current_date = $user_selected_year . "-12-31";

		Yii::app()->session['user_selected_year'] = $user_selected_year;

		$order = new Order();
		$view_data['orders'] = $order->getSupplierOrders($vendor_id,$year_start_date, $current_date);

		$view_data['vendors_combo'] = $vendor->getVendorOrderAndAmounts($vendor_id,$year_start_date, $current_date);
		$contract = new Contract();
		$view_data['contracts'] = $contract->getVendorContracts($vendor_id,$year_start_date, $current_date);

		$view_data['contracts_by_location'] = $contract->getVendorContractLocationTotals($vendor_id,$year_start_date, $current_date);
		$quote = new Quote();
		$view_data['quotes'] = $quote->getVendorQuotes($vendor_id);
        $view_data['documentList'] = $vendor->getVendorsDocuments($vendor_id);
        $view_data['vendorStatistics'] = $vendor->getVendorDetailStatistics($vendor_id);
       
        $view_data['owners'] = $vendor->getOwners($vendor_id);
        $view_data['quickComparison'] = $vendor->getQuickComparision($vendor_id);
        $view_data['documenArchivetList'] = $vendor->getVendorsDocuments($vendor_id,"archive");
		// and display
		unset(Yii::app()->session['uploaded_files']);unset(Yii::app()->session['uploaded_files_display']);
		$this->render('archive_edit', $view_data);

	}

	public function actionSaveArhive(){
		date_default_timezone_set(Yii::app()->params['timeZone']);
        $this->checklogin();
        $vendorID = $_GET['vendor-id'];

        if(empty($vendorID)){
            $this->redirect(AppUrl::bicesUrl('vendors/list'));
        }

        if(empty($_GET['archive-type'])){

            // $sql = "INSERT INTO archive_vendors SELECT * FROM vendors WHERE vendors.vendor_id=".$vendorID;
            $sql = "update vendors set vendor_archive='yes' WHERE vendors.vendor_id=".$vendorID;
            $archive = Yii::app()->db->createCommand($sql)->execute();
            if($archive){

                $userID   = Yii::app()->session['user_id'];
                $userName = Yii::app()->session['full_name'];
                $dateTime = date("Y-m-d H:i:s");

                $sql = "INSERT INTO archive_vendors_log (vendor_id,user_id,user_name,type,created_datetime) VALUES ('".$vendorID."','".$userID."','".$userName."','Archived','".$dateTime."')";
                $archiveLog = Yii::app()->db->createCommand($sql)->execute();

                // $sql = "delete from vendors where vendor_id=".$vendorID;
                // $contract = Yii::app()->db->createCommand($sql)->execute();

                Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success! </strong>Supplier archived successfully.</div>');

            }
            $this->redirect(AppUrl::bicesUrl('vendors/list'));
        }else if(!empty($_GET['archive-type']) && $_GET['archive-type']=='un-archive'){
            //$sql = "INSERT INTO vendors SELECT * FROM archive_vendors WHERE archive_vendors.vendor_id=".$vendorID;

            $sql = "update vendors set vendor_archive=NULL WHERE vendors.vendor_id=".$vendorID;
            $archive = Yii::app()->db->createCommand($sql)->execute();
            if($archive){

                $userID   = Yii::app()->session['user_id'];
                $userName = Yii::app()->session['full_name'];
                $dateTime = date("Y-m-d H:i:s");

                $sql = "INSERT INTO archive_vendors_log (vendor_id,user_id,user_name,type,created_datetime) VALUES ('".$vendorID."','".$userID."','".$userName."','Un-archive','".$dateTime."')";
                
                $archiveLog = Yii::app()->db->createCommand($sql)->execute();

                // $sql = "delete from archive_vendors where vendor_id=".$vendorID;
                // $contract = Yii::app()->db->createCommand($sql)->execute();

                Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success! </strong>Supplier Un-archived successfully.</div>');

            }
            $this->redirect(AppUrl::bicesUrl('vendors/archiveList'));

        }
        
    }

	public function actionQuickAddVendor(){
		error_reporting(0);
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$vendorEmail = !empty($_POST['vendor_emails'])?$_POST['vendor_emails']:'';
		$vendorName = $_POST['vendor_name'];
		$vendor = new Vendor();
		if (!empty($vendorEmail) || !empty($vendorName))
		{
			if (!empty($vendorEmail))
				$vendorDuplicate = $vendor->getOne(array('emails' => $vendorEmail));

			if(!empty($vendorDuplicate['emails'])){
				$msg =1;
			}else {
				/*if($vendor['vendor_archive'] != null || $vendor['vendor_archive'] !="")*/

			$contactName = $_POST['new_vendor_contact_name'];
			$address1    = $_POST['new_vendor_address'];
			$address2 	 = $_POST['new_vendor_address2'];
			$city 		 = $_POST['new_vendor_city'];
			$state 		 = $_POST['new_vendor_state'];
			$zip 		 = $_POST['new_vendor_zip'];

			//$vendor->fields['active'] = 'N'; 
			$override_data['active']  = 1;
			$override_data['profile_status'] = 'Active';
			$override_data['emails']         = $vendorEmail;
			$override_data['vendor_name']    = $vendorName;
			$override_data['contact_name']   = $contactName;
			$override_data['address_1']      = $address1;
			$override_data['address_2']      = $address2;
			$override_data['city']           = $city;
			$override_data['state']          = $state;
			$override_data['zip']            = $zip;
			$override_data['quick_created']  = !empty($_POST['created_by_activity']) && $_POST['created_by_activity']=='Quikc Sourcing Evaluation'?'Yes and Quick Sourcing Evaluation':'Yes';
			$override_data['user_id'] = Yii::app()->session['user_id'];


			$vendor->saveData($override_data);
			if(!empty($vendor->rs['vendor_id'])){
				$msg =!empty($_POST['created_by_activity']) && $_POST['created_by_activity']=='Quikc Sourcing Evaluation'?$vendor->rs['vendor_id']:$vendor->rs['vendor_id'];
				$quickVendorAdded = Yii::app()->session['quick_vendor'];
				$quickVendorAdded[$vendor->rs['vendor_id']]['vendor_id'] = $vendor->rs['vendor_id']; 
				$quickVendorAdded[$vendor->rs['vendor_id']]['vendor_contact'] =  $_POST['new_vendor_contact_name'];
				$quickVendorAdded[$vendor->rs['vendor_id']]['vendor_address'] =  $_POST['new_vendor_address'];
				$quickVendorAdded[$vendor->rs['vendor_id']]['vendor_address2'] =  $_POST['new_vendor_address2'];
				$quickVendorAdded[$vendor->rs['vendor_id']]['vendor_city'] =  $_POST['new_vendor_city'];
				$quickVendorAdded[$vendor->rs['vendor_id']]['vendor_state'] =  $_POST['new_vendor_state'];
				$quickVendorAdded[$vendor->rs['vendor_id']]['vendor_zip'] =  $_POST['new_vendor_zip'];
				Yii::app()->session['quick_vendor'] = $quickVendorAdded;
				//$vendor->welcomeEmail($vendor->rs['vendor_id']);
			}else{
				$msg=!empty($_POST['created_by_activity']) && $_POST['created_by_activity']=='Quikc Sourcing Evaluation'?'':3;
			} }
		}

		echo  $msg;exit;
	}

	public function actionGetVendorDetail(){
		$vendorID = !empty($_POST['vendor_id'])?$_POST['vendor_id']:0;
		$sql="select * from (

		      select contact_email as emails,contact_name as contact_name,vendor_id from supplier_contacts where contact_name !='' and vendor_id=".$vendorID."
		      union
		      select emails,contact_name as contact_name,vendor_id from vendors v where  contact_name !='' and vendor_id=".$vendorID."

	         ) as contact_info group by contact_name order by contact_name asc";
        $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    
        $nameDropdown ='<option value="">select</option>';
        if(!empty($_POST['quote_id'])){
			$quoteID = $_POST['quote_id'] ;
        	 foreach($vendorReader as $value){
        	  $sql = "select * from quote_vendor_contact where vendor_id=".$vendorID." and quote_id=".$quoteID;
        	  $quoteVendor = Yii::app()->db->createCommand($sql)->queryAll();
			  $selectedVendArr = [];
			  foreach($quoteVendor as $selectedVend){
				$selectedVendArr[] = $selectedVend['vendor_id'];
			  }

			  if(!empty($quoteVendor) &&  $selectedVendArr && in_array($value['vendor_id'], $selectedVendArr)){
				$selectedName='selected="selected"';
			  }else{
				$selectedName='';
			  }
        		
        		$nameDropdown .='<option value="'.$value['contact_name'].' - '.$value['emails'].'" '.$selectedName.'>'.$value['contact_name'].' - '.$value['emails'].'</option>';
        	 }
        }else {
        	foreach($vendorReader as $value){
	        	$nameDropdown .='<option value="'.$value['contact_name'].' - '.$value['emails'].'">'.$value['contact_name'].' - '.$value['emails'].'</option>';
	        }
        }

      /*  $emailDropdown ='<option value="">select</option>';
        foreach($vendorReader as $value){
        	$emailDropdown .='<option value="'.$value['emails'].'">'.$value['emails'].'</option>';
        }*/
		$data = array();
		$data['contact_name'] = $nameDropdown;
		$data['emails'] = '';
		echo json_encode($data);exit;
	}


	public function actionGetVendorContactEmail(){
		$vendorID 	 = !empty($_POST['vendor_id'])?$_POST['vendor_id']:0;
		$contactName = $_POST['contact_name'];
		$sql="select contact_email as emails,contact_name as contact_name from supplier_contacts 
		where vendor_id=".$vendorID." and contact_name='".$contactName."'
			union 
		select emails,contact_name as contact_name from vendors where vendor_id=".$vendorID." and contact_name='".$contactName."' ";

        $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll(); 
        $emailDropdown ='<option value="">select</option>';

        if(!empty($_POST['quote_id'])){
        		$quoteID = $_POST['quote_id'];
        		foreach($vendorReader as $value){
        			$sql = "select * from quote_vendors where vendor_id=".$vendorID." and quote_id=".$quoteID;
        			$quoteVendor = Yii::app()->db->createCommand($sql)->queryRow();
        			if(!empty($quoteVendor) && $quoteVendor['emails']==$value['emails']){
        				$selectedEmail='selected="selected"';
        			}else{
        				$selectedEmail='';
        			}
        			$emailDropdown .='<option value="'.$value['emails'].'" '.$selectedEmail.'>'.$value['emails'].'</option>';
        		} 
        }else {
        	foreach($vendorReader as $value){
        		$emailDropdown .='<option value="'.$value['emails'].'">'.$value['emails'].'</option>';
        	}
       	}
		$data = array();
		$data['emails'] =  $emailDropdown;
		echo json_encode($data);exit;
	}



	public function actionCreateByModal(){
       	$view_data = array();
        $data = array();

       	if(!empty($_POST['emails'])){
       		$vendor = new Vendor();
			
			$vendorDuplicate = $vendor->getOne(array('emails' => $_POST['emails']));
			if(!empty($vendorDuplicate['emails'])){
				$alert = "Supplier with this email already taken. Try another";
				$redID = $vendorDuplicate['vendor_id'];
				$dupplicate =1;
				// $this->redirect(AppUrl::bicesUrl('vendors/edit/'.$redID ));
			}else{
				$override_data = array();
				$override_data['quick_created'] = 'No';
				//$override_data['active'] = !empty($_POST['active'])?$_POST['active']:0;
				$override_data['active'] = !empty($_POST['approver_id'])?0:1;
				$override_data['approver_status'] = !empty($_POST['approver_id'])?'In Review':'';
				
				$override_data['profile_status'] = !empty($_POST['approver_id'])?'Inactive':'Active';
				$override_data['user_id'] = Yii::app()->session['user_id'];
				
				$vendor->saveData($override_data);
				 
				if(!empty($vendor->rs['vendor_id'])){
					$alert = "<strong> Success! </strong>Supplier submitted successfully. ";
					//$vendor->welcomeEmail($vendor->rs['vendor_id']);
					if($vendor->rs['active']==1){
						//EmailManager::vendorAcivation($vendor->rs['vendor_id'],1,Yii::app()->session['full_name']);
					}
					$data['vendor_id'] =   $vendor->rs['vendor_id'];
					$data['vendor_name'] = $vendor->rs['vendor_name'];
					$dupplicate =0;
					
				} }
			
			
			$data['alert'] = $alert;
			$data['dupplicate'] = $dupplicate;

			echo json_encode($data);exit;
       		
       	}
       	$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('soft_deleted' => '0','ORDER' => 'value'));

        $this->renderPartial('create_vendor_modal', $view_data);

    }

	public function actionUploadDocument(){
		 
		date_default_timezone_set(Yii::app()->params['timeZone']);
		error_reporting(0);
		if (!empty($_FILES)){
			$vendor = new Vendor;
			if (isset($_FILES['file']) && is_array($_FILES['file'])){
				$vendor_id = Yii::app()->input->post('vendor_id');
				if(empty($vendor_id)){
					$vendor_id ='';
				}
				if (!is_dir('uploads/vendor_documents')) mkdir('uploads/vendor_documents');
				if (!is_dir('uploads/vendor_documents/' . $vendor_id))
					mkdir('uploads/vendor_documents/' . $vendor_id);
				if (!is_dir('uploads/vendor_documents/'))
					mkdir('uploads/vendor_documents/');

				if (!isset($_FILES['file']['error']) || empty($_FILES['file']['error']))
				{
					$file_name = time().'_'.$_FILES['file']['name'];
                    $file_name = str_replace("'","",$file_name);
                    $file_name = str_replace('"','',$file_name);
                    $file_name = str_replace(' ','_',$file_name);
                    $file_description = addslashes($_POST['file_description']);

					$document_type = Yii::app()->input->post('document_type');
					$document_status = Yii::app()->input->post('document_status');
					$expiry_date = Yii::app()->input->post('expiry_date');
					
					$uploadedFiles = Yii::app()->session['uploaded_files'];

					$uploadedFiles[] = array('file_name'=>$file_name,'file_description'=>$file_description,'vendor_id'=>$vendor_id,'file_type'=>$document_type,'document_status'=>$document_status,'expiry_date'=>$expiry_date);

					
					//move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/orders/' . $file_name);
					if(move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/vendor_documents/' . (!empty($vendor_id)?$vendor_id. '/'. $file_name: $file_name))){
						Yii::app()->session['uploaded_files'] = $uploadedFiles;
						if(!empty($vendor_id)){
							$vendor->saveDocument($vendor_id,$file_description, $file_name,$document_type,$document_status,$expiry_date);
							$documentList = $vendor->getVendorsDocuments($vendor_id);
						}else {
							$uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
							$uploadedFilesDisplay[$document_type][] =  array(
																		'id'=>0,
																		'document_file'=>$file_name,
																		'document_title'=>$file_description,
																		'vendor_id'=>0,
																		'status'=>$document_status,
																		'expiry_date'=>$expiry_date,
																		'date_created'=>date("Y-m-d")
																	);
							Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
							$documentList = Yii::app()->session['uploaded_files_display'];
						}
					}
					
					$url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
          			echo json_encode(['url' => $url]); exit;
					
				}
			}
		}

	}

	public function actionApproveDocument(){  
		error_reporting(0);
		$vendor = new Vendor;
		if (null !=Yii::app()->input->post('document_ids')){
			$documentIDs = Yii::app()->input->post('document_ids');
			$vendor->approveDocument($documentIDs);
			$vendor_id = Yii::app()->input->post('vendor_id');
			$url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
          	echo json_encode(['url' => $url]); exit;
		}
	}

	public function actionApproveDocumentOnboard(){  
		error_reporting(0);
		$vendor = new Vendor;
		if (null !=Yii::app()->input->post('document_ids')){
			$documentIDs = Yii::app()->input->post('document_ids');
			$vendor->approveDocumentOnboard($documentIDs);
			$vendor_id = Yii::app()->input->post('vendor_id');
			$url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
          	echo json_encode(['url' => $url]); exit;
		}
	}


	public function actionDocumentArchive(){ 
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
       
        $vendor = new Vendor;
        if (null != Yii::app()->input->post('document_ids')){
          $documentIDs = Yii::app()->input->post('document_ids'); 
          $vendor->archiveDocument($documentIDs);
          $vendor_id = Yii::app()->input->post('vendor_id');
          $url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
          echo json_encode(['url' => $url]); exit; 
        }
    }

    public function actionDocumentUnArchive(){ 
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
     	
     	$vendor = new Vendor;
        if (null !=Yii::app()->input->post('document_ids')){
            $documentIDs = Yii::app()->input->post('document_ids'); 
            $vendor->unArchiveDocument($documentIDs);
            $vendor_id = Yii::app()->input->post('vendor_id');
            $url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
          	echo json_encode(['url' => $url]); exit; 
        }
    }

    public function actionDocumentArchiveOnboard(){ 
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
       
        $vendor = new Vendor;
        if (null !=Yii::app()->input->post('document_ids')){
            $documentIDs = Yii::app()->input->post('document_ids'); 
            $vendor->archiveDocumentOnboard($documentIDs);
            $vendor_id = Yii::app()->input->post('vendor_id');
            $url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
          	echo json_encode(['url' => $url]); exit;
        }
    }

     public function actionDocumentUnArchiveOnboard(){ 
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
     	
     	 $vendor = new Vendor;
        if (null !=Yii::app()->input->post('document_ids')){
            $documentIDs = Yii::app()->input->post('document_ids'); 
            $vendor->unArchiveDocumentOnboard($documentIDs);
            $vendor_id = Yii::app()->input->post('vendor_id');
            $url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
          	echo json_encode(['url' => $url]); exit; 
        }
    }

	public function actionEditDocument(){  
		error_reporting(0);
		$vendor = new Vendor;
		//if (!empty($_POST)){
		 $postedArr = $_POST;
		 $vendor->editDocument($postedArr);
		 $vendor_id = $_POST['vendor_id'];
		 $url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
		 echo json_encode(['url' => $url]); exit; 	
		//}
	}

	public function actionEditDocumentOnboard(){  
		error_reporting(0);
		$vendor = new Vendor;
		//if (!empty($_POST)){
			$postedArr = $_POST;
			$vendor->editDocumentOnboard($postedArr);
			$vendor_id =$_POST['vendor_id'];
			$url = AppUrl::bicesUrl('vendors/edit/'.$vendor_id.'?tab=document');
          	echo json_encode(['url' => $url]); exit;	
		//}
	}

	public function actionDocumentDownload()
	{
		$id = $_GET['id'];
        $sql = "select document_file,vendor_id from vendor_documents WHERE id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
       
	    $file = Yii::app()->basePath."/../uploads/vendor_documents/".$fileReader['vendor_id']."/".$fileReader['document_file'];
		if ($fileReader) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}

	}

	public function actionDocumentDownloadOnboard()
	{
		$id = $_GET['id'];

        $sql = "select document_file,vendor_id from vendor_documents_onboard WHERE id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();

       
	    $file = Yii::app()->basePath."/../uploads/vendor_documents_onboard/".$fileReader['vendor_id']."/".$fileReader['document_file'];
       
		if ($fileReader) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}

	}

	public function actionDeleteDocument()
	{
		$id = $_POST['documentID'];
        //$sql = "select document_file,vendor_id from vendor_documents WHERE status='Pending' and id =".$id;
        $sql = "select document_file,vendor_id from vendor_documents WHERE id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
        if(!empty($fileReader)){
        	$vendor_id = $fileReader['vendor_id'];
        	$fileName = $fileReader['document_file'];
        	$file = Yii::app()->basePath."/../uploads/vendor_documents/".$vendor_id."/".$fileName;
        	if(unlink($file)){
        		//$sql = "delete from vendor_documents WHERE status='Pending' and id =".$id;
        		$sql = "delete from vendor_documents WHERE  id =".$id;
        		$fileReader = Yii::app()->db->createCommand($sql)->execute();
        	}
        }
        $vendor = new Vendor;
		$this->renderPartial('_documents',array('documentList'=>$vendor->getVendorsDocuments($vendor_id),'documenArchivetList'=>$vendor->getVendorsDocuments($vendor_id,"archive"),'vendor_id'=>$vendor_id));
	}

	public function actionDeleteDocumentOther()
	{
		$ids = explode("_",$_POST['documentID']);

		$uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
		$documentFileDislplay = $uploadedFilesDisplay[$ids[0]][$ids[1]]['document_file'];
		unset($uploadedFilesDisplay[$ids[0]][$ids[1]]);
		if(empty($uploadedFilesDisplay[$ids[0]])){
			unset($uploadedFilesDisplay[$ids[0]]);
		}
		Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
		$documentList = Yii::app()->session['uploaded_files_display'];

		$uploadedFiles = Yii::app()->session['uploaded_files'];
		$uploadedFilesRowKey = '';
		foreach($uploadedFiles as $key=>$fileVal){
			if($fileVal['file_name']==$documentFileDislplay){
				$uploadedFilesRowKey =$key;
				break;
			}
		}
		unset($uploadedFiles[$uploadedFilesRowKey]);

	
		Yii::app()->session['uploaded_files'] = $uploadedFiles;
        if(!empty($documentFileDislplay)){
        	$file = Yii::app()->basePath."/../uploads/vendor_documents/".$documentFileDislplay;
        	if(unlink($file)){
        	}
        }
        $vendor = new Vendor;
		$this->renderPartial('_documents',array('documentList'=>$documentList, 'documenArchivetList'=>$vendor->getVendorsDocuments($vendor_id,"archive"),'vendor_id'=>0));
	}

	public function actionImport()
	{
		// is someone already logged in?
		$this->checklogin();
        if (!isset(Yii::app()->session['user_type']) || Yii::app()->session['user_type'] != 1)
			$this->redirect(AppUrl::bicesUrl('orders/list'));	
		else
		{
	        if (!isset(Yii::app()->session['admin_flag']) || Yii::app()->session['admin_flag'] != 1)
				$this->redirect(AppUrl::bicesUrl('app/dashboard'));
		}			
        $this->layout = 'main';
		$this->render('import');
	}

	public function actionImportData()
	{
		if (isset($_FILES['file']) && isset($_FILES['file']['tmp_name']))
		{
			$vendor = new Vendor();
			$vendor->importData($_FILES['file']);
		}
	}

	public function actionGetIndustries()
	{
		$search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
		$industries = array();

		$industry = new Industry();
		$search_results = $industry->getAll(array('value LIKE' => '%' . $search_string . '%', 'soft_deleted' => '0', 'ORDER' => 'value'));

		foreach ($search_results as $industry_data)
			$industries[] = array("value" => $industry_data['value'], "data" => $industry_data['id']);

		echo json_encode(array('suggestions' => $industries));
	}

	public function actionGetSubIndustries(){

		$search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
		$industry_id = isset($_REQUEST['industry_id']) ? $_REQUEST['industry_id'] : "";
		$sub_industry_id = isset($_REQUEST['subindustry_id']) ? $_REQUEST['subindustry_id'] : "";

		$industries = array();

		$industry = new Subindustry();
		$search_results = $industry->getAll(array('value LIKE' => '%' . $search_string . '%','soft_deleted' => "0", 'ORDER' => 'value', 'code' => $industry_id));

		foreach ($search_results as $industry_data)
			$industries[] = array("value" => $industry_data['value'], "data" => $industry_data['id']);

		if(!empty($sub_industry_id)){
			$search_results = $industry->getAll(array('id' => $sub_industry_id));

			foreach ($search_results as $industry_data)
				$industries[] = array("value" => $industry_data['value'], "data" => $industry_data['id']);
		}


		echo json_encode(array('suggestions' => $industries));
	}


	public function actionUpdateItemStatus()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		
		// only admin can delete item
		if (in_array(Yii::app()->session['user_type'],array(1,3,4)))
		{
			$delete_item_id = isset($_REQUEST['delete_item_id']) ? $_REQUEST['delete_item_id'] : "";
			$active_flag = isset($_REQUEST['active_flag']) ? $_REQUEST['active_flag'] : 0;
			
			if ($delete_item_id)
			{
				$vendor = new Vendor();
				$vendor->rs = array();
				$vendor->rs['vendor_id'] = $delete_item_id;
				$vendor->rs['active'] = $active_flag;
				$vendor->rs['profile_status'] = $active_flag==0?'Inactive':'Active';
				$vendor->write();
				// Start: if password empty then create password and send welcome email
				$oldRecord = $vendor->getOne(array('vendor_id'=>$vendor->rs['vendor_id']));
				if($vendor->rs['active']==1 && empty($oldRecord['password'])){
					 //EmailManager::vendorAcivation($vendor->rs['vendor_id'] ,Yii::app()->session['full_name']);
				}
				// END: if password empty then create password and send welcome emai
			}
		}
	}

	public function actionSaveOwner()
	{   date_default_timezone_set(Yii::app()->params['timeZone']);
		$owner = explode(",",$_POST['owner']);
		$vendorID= $_POST['vendor_id'];
		foreach($owner as $ownerID){
			$vendorOwner = new VendorOwner();
			$vendorOwner->rs = array();
			$vendorOwner->rs['user_id'] = $ownerID;
			$vendorOwner->rs['vendor_id'] = $vendorID;
			$vendorOwner->rs['created_at'] = date('Y-m-d H:i:s');
			$vendorOwner->write();
		}
		$body = "";
		echo $body;
	}

    public function actionOwnerList(){
    	$deleteBtn='';
    	if(in_array(Yii::app()->session['user_type'],array(4))){
    		$deleteBtn=1;
        }

        $disabled = FunctionManager::sandbox(); 
    	$vendor_id = $_POST['vendor_id'];
        $vendor = new Vendor();
        $owners = $vendor->getOwnersVendor($vendor_id);
    	$body='<h4 class="heading">Users Currently Receiving Notifications</h4><br /><table class="table table-striped table-bordered"><thead><tr>
    	<th>Username</th>
        <th>Date Added</th>
        <th></th>
        </tr></thead>
        <tbody>';
        foreach($owners as $value){
        	$id = $value['owner_id'];
        	if(!empty($deleteBtn)){
        		$deleteBtn= '<a style="cursor: pointer; padding: 5px;" onclick="deleteOwner('.$id.')" '.$disabled.'>
               <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
        	}
        	$body .='<tr><td>'.$value['full_name'].'</td><td>'.date(FunctionManager::dateFormat(),strtotime($value['created_at'])).'</td><td>'.$deleteBtn.'</td></tr>';
        }

        $body .='</tbody></table>';
        echo $body;exit;
    }

    public function actionOwnerDelete(){
    	$owner_id = $_POST['owner_id'];
        $sql = "DELETE from vendor_owner where id=".$owner_id;
        $ownerReader = Yii::app()->db->createCommand($sql)->execute();

        if(!empty($ownerReader)){
        	$msg=1;
        }else{
            $msg=0;
        }
        echo $msg;exit;
    }

    public function actionCheckOwnerDoucment(){
    	$vendor_id = $_POST['vendor_id'];
        $user_id = Yii::app()->session['user_id'];
        $sql = "SELECT * from vendor_owner own where own.vendor_id=".$vendor_id." and user_id=".$user_id;
        $ownerReader = Yii::app()->db->createCommand($sql)->queryRow();

        if(!empty($ownerReader) && count($ownerReader)>0){
        	$msg=1;
        }else{
            $msg=0;
        }
        echo $msg;exit;
    }

    public function actionCreateSupplierContract(){

		$vendorID     = $_POST['supplier_contact_id'];
		$contactName  =  $_POST['contact_name'];
		$contactEmail = $_POST['contact_email'];
		$contactPosition = $_POST['contact_position'];
		$contactPhone 	 = $_POST['contact_phone_no'];
		$contactLocation = $_POST['contact_location'];
		$userID       = Yii::app()->session['user_id'];
		$userName     = Yii::app()->session['full_name'];
		$dateTime     = date("Y-m-d H:i:s");
		$contactID    = 0;

		$sql = "select contact_email as email from supplier_contacts where contact_email='".$contactEmail."' union select emails as email from vendors where emails='".$contactEmail."'";
		$checkContact = Yii::app()->db->createCommand($sql)->queryRow();

        if(empty($checkContact)){
			$contact = new SupplierContact();
			$contact->rs = array();
			$contact->rs['vendor_id']       = $vendorID;
			$contact->rs['contact_name']    = $contactName;
			$contact->rs['contact_email']   = $contactEmail;
			$contact->rs['created_by_id'] 	= $userID;
			$contact->rs['created_by_name'] = $userName;
			$contact->rs['contact_position']= $contactPosition;
			$contact->rs['contact_phone_no']= $contactPhone;
			$contact->rs['contact_location']= $contactLocation;
			$contact->rs['created_at'] = $dateTime;
			$contact->write();
			$data = array();
			if(!empty($contact->rs['id'])){
				$contactID = $contact->rs['id'];
				$code = 1; 
				//$this->createVendorTeam($contact->rs['vendor_id'],$contact->rs['contact_email'],$contact->rs['contact_name']);
			}
		}else{
			$code = 2; 
		}
		$data['contract_id'] = $contactID;
		$data['code'] = $code;
		echo json_encode($data);
		exit;
    }

    public function createVendorTeam($vendorID,$email,$name){

    	$vendor = new Vendor();
		$vendor->rs = array();
		$vendor->rs['active'] = 1;
		$vendor->rs['emails'] = $email;
		$vendor->rs['vendor_name'] = $name;
		$vendor->rs['member_type'] = 2;
		$vendor->write();

		if(!empty($vendor->rs['vendor_id'])){
    	 $sql = "insert into vendor_teammember set vendor_id=".$vendorID." , teammember_id=".$vendor->rs['vendor_id'];
    	 $execute = Yii::app()->db->createCommand($sql)->execute();
    	 EmailManager::vendorAcivation($vendor->rs['vendor_id'],1,Yii::app()->session['full_name']);
    	}
    }

    public function actionGetSupplierContract(){
    	   $data = array();
			$total_contacts=0;
			$vendorID = $_POST['vendor_id'];
			$disabled = FunctionManager::sandbox();
			$sql = "select * from  supplier_contacts where vendor_id=".$vendorID." order by id DESC";
			$vendorContact = Yii::app()->db->createCommand($sql)->query()->readAll();
			foreach ($vendorContact as $value)
			{
				$current_data = array();
                $total_contacts++;
				$deleteFun = "deleteSupplierContact('".$value['id']."')";
				$delete_link = '<a style="cursor: pointer; padding: 5px;"  onclick="'.$deleteFun.'"><span class="glyphicon glyphicon-trash" aria-hidden="true" '. $disabled .'></span></a>';

				$current_data[] = '<span class="notranslate">'.$value['contact_name'].'</span>';
				$current_data[] = '<span class="notranslate">'.$value['contact_email'].'</span>';
				$current_data[] = '<span class="notranslate">'.$value['contact_position'].'</span>';
				$current_data[] = '<span class="notranslate">'.$value['contact_phone_no'].'</span>';
				$current_data[] = '<span class="notranslate">'.$value['contact_location'].'</span>';
				$current_data[] = $value['created_by_name'];
				$current_data[] = date("d/m/Y h:i:s A",strtotime($value['created_at']));

				$current_data[] = $delete_link;
				
				$data[] = $current_data;
			}
			$sql = "select emails,contact_name,phone_1 from  vendors where vendor_id=" . $vendorID;
			$vendorReader = Yii::app()->db->createCommand($sql)->queryRow();
			$current_data = array();
			$total_contacts++;
			$delete_link = '';
			$current_data[] = '<span class="notranslate">' . $vendorReader['contact_name'] . '</span>';
			$current_data[] = '<span class="notranslate">' . $vendorReader['emails'] . '</span>';
			$current_data[] = '<span class="notranslate"></span>';
			$current_data[] = '<span class="notranslate">' . $vendorReader['phone_1'] . '</span>';
			$current_data[] = '<span class="notranslate"></span>';
			$current_data[] = '';
			$current_data[] = '';
			$current_data[] = $delete_link;
			$data[] = $current_data;
			$total_contacts = count($vendorContact)+1;
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $total_contacts,
                "recordsFiltered" => $total_contacts,
                "data"            => $data
            );
			echo json_encode($json_data);		
		}

	 public function actionDeleteSupplierContract(){
		$id = $_POST['id'];
		$sql = "select * from  supplier_contacts where id=".$id;
		$vendorContactReader= Yii::app()->db->createCommand($sql)->queryRow();
		$sql = "delete from  supplier_contacts where id=".$id;
		
		$vendorContact = Yii::app()->db->createCommand($sql)->execute();
		$data = array();
		$mesgeID=0;
		if(!empty($vendorContact)){
			$mesgeID = 1;
			$sql = "delete from  vendors where emails='".$vendorContactReader['contact_email']."'";
		    $vendor = Yii::app()->db->createCommand($sql)->execute();
		}
		$data['mesgeID'] = $mesgeID;
		echo json_encode($data);
		exit;
    }

    public function actionSupplierPerformance(){
    	date_default_timezone_set(Yii::app()->params['timeZone']);
    	$this->checklogin();
        $this->layout = 'main';

        $title = !empty($_POST['title'])?$_POST['title']:'';
        $description = !empty($_POST['description'])?$_POST['description']:'';
        $scoreReason = !empty($_POST['score_reason'])?$_POST['score_reason']:'';
        $score = !empty($_POST['score'])?$_POST['score']:'0';
        $vendorID = !empty($_POST['vendor_id'])?$_POST['vendor_id']:'0';
        $userID       = Yii::app()->session['user_id'];
        $userName     = Yii::app()->session['full_name'];
        $dateTime     = date("Y-m-d H:i:s");

        $title = addslashes($title);
        $description = addslashes($description);
        $scoreReason = addslashes($scoreReason);

        $performance = new VendorPerformance;
        $performance->rs = array();
		$performance->rs['vendor_id']  = $vendorID;
		$performance->rs['title'] = $title;
		$performance->rs['description'] = $description;
		$performance->rs['score_reason'] = $scoreReason;
		$performance->rs['score_value'] = $score;
		$performance->rs['created_by_id'] = $userID;
		$performance->rs['created_by_name'] = $userName;
		$performance->rs['created_at'] = $dateTime;
		$performance->write();

		if(!empty($performance->rs['id'])){
			Yii::app()->user->setFlash('success', 'Supplier Performance Record submitted successfully.');
			$this->redirect(AppUrl::bicesUrl('vendors/edit/'.$vendorID.'?tab=vendor-performance'));

		}

		
    }

    public function actionSupplierRisk(){
    	date_default_timezone_set(Yii::app()->params['timeZone']);
    	$this->checklogin();
        $this->layout = 'main';
       
        $riskScore = !empty($_POST['risk_score'])?$_POST['risk_score']:'0';
        $description = !empty($_POST['risk_score_reason'])?$_POST['risk_score_reason']:'';
        $vendorID = !empty($_POST['risk_vendor_id'])?$_POST['risk_vendor_id']:'0';
        $userID       = Yii::app()->session['user_id'];
        $userName     = Yii::app()->session['full_name'];
        $dateTime     = date("Y-m-d H:i:s");
        $description = addslashes($description);;
        $risk = new VendorRisk;
        $risk->rs = array();
		$risk->rs['vendor_id']  = $vendorID;
		$risk->rs['risk_description'] = $description;;
		$risk->rs['risk_score_value'] = $riskScore;
		$risk->rs['created_by_id'] = $userID;
		$risk->rs['created_by_name'] = $userName;
		$risk->rs['created_at'] = $dateTime;
		$risk->write();
		if(!empty($risk->rs['id'])){
			Yii::app()->user->setFlash('success', 'Supplier Risk Record submitted successfully.');
			$this->redirect(AppUrl::bicesUrl('vendors/edit/'.$vendorID.'?tab=vendor-risk'));

		}

		
    }

    public function actionSupplierSustainability(){

    	date_default_timezone_set(Yii::app()->params['timeZone']);
    	$this->checklogin();
        $this->layout = 'main';
       
        $sustainabilityScore = !empty($_POST['sustainability_score'])?$_POST['sustainability_score']:'0';
        $description = !empty($_POST['sustainability_score_reason'])?$_POST['sustainability_score_reason']:'';
 
        $vendorID = !empty($_POST['sustainability_vendor_id'])?$_POST['sustainability_vendor_id']:'0';
        $userID       = Yii::app()->session['user_id'];
        $userName     = Yii::app()->session['full_name'];
        $dateTime     = date("Y-m-d H:i:s");
        $description = addslashes($description);;
        $risk = new VendorSustainability;
        $risk->rs = array();
		$risk->rs['vendor_id']  = $vendorID;
		$risk->rs['description'] = $description;;
		$risk->rs['score_value'] = $sustainabilityScore;
		$risk->rs['created_by_id'] = $userID;
		$risk->rs['created_by_name'] = $userName;
		$risk->rs['created_at'] = $dateTime;
		$risk->write();
		if(!empty($risk->rs['id'])){
			Yii::app()->user->setFlash('success', 'Supplier Risk Record submitted successfully.');
			$this->redirect(AppUrl::bicesUrl('vendors/edit/'.$vendorID.'?tab=vendor-sustainability'));

		}
    }


     public function actionRequestVendorDocument(){
        $msg = 0;
        if(!empty($_POST['request_document_vendor_id'])){
            $userID   = Yii::app()->session['user_id'];
            $userName = Yii::app()->session['full_name'];
            $dateTime = date("Y-m-d H:i:s");
            $vendorID = $_POST['request_document_vendor_id'];
            //$contractIDs = $_POST['contract_id'];

            if(!empty($_POST['vendor_type_new'])){
                $documentTypeNew = addslashes($_POST['vendor_type_new']);
                $sql = "INSERT INTO `vendor_document_type` (`id`, `name`, `created_type`, `btn_style`, `created_at`) VALUES (NULL, '".$documentTypeNew ."', 'Cusom', NULL, '".$dateTime."');";
                $reader = Yii::app()->db->createCommand($sql)->execute();
                $documentType = Yii::app()->db->lastInsertID;
            }else{
                $documentType = $_POST['document_vendor_type'];
            }
			
			$vendorArr = $_POST['vendor_contact_name'][0];
			$vendorContactName = explode(" - ", $vendorArr);
			
            $contactName  = serialize($vendorContactName[0]);
            $contactEmail = serialize($vendorContactName[1]);
			// $contactName  = serialize($_POST['vendor_contact_name']);
            // $contactEmail = serialize($_POST['vendor_contact_email']);
            $note   = addslashes($_POST['request_document_note']);
            
           /* foreach($contractIDs as $contractID){*/
            /*    if($contractID){*/
                $reference = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)))), 1, 24);
                $contact = new VendorRequestDocument();
                $contact->rs = array();
                $contact->rs['vendor_id']       = $vendorID;
                $contact->rs['user_id'] = $userID;
                $contact->rs['user_name'] = $userName;
                $contact->rs['vendor_document_type'] = $documentType;
                $contact->rs['supplier_contact_name'] = $contactName;
                $contact->rs['supplier_contact_email'] = $contactEmail;
                $contact->rs['note'] = $note;
                $contact->rs['status'] = 'Pending';
                $contact->rs['reference'] = $reference;
                $contact->rs['created_at'] = $dateTime;
                
                $contact->write();
                $data = array();
                if(!empty($contact->rs['id'])){
                    $msg = 1;
                    $cronEmail = new CronEmail;
                    $cronEmail->rs = array();
                    $cronEmail->rs['record_id'] = $contact->rs['id'];
                    $cronEmail->rs['send_to'] = 'Vendor'; 
                    $cronEmail->rs['user_id'] = $vendorID;
                    $cronEmail->rs['user_type'] = 'Request Vendor Document';
                    $cronEmail->rs['status'] = 'Pending';
                    $cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
                    $cronEmail->write();
                }
            /*}
            }*/
        }

        $data = array();
        $data['msg'] = $msg;
        $data['url'] = AppUrl::bicesUrl('vendors/edit/' . $_POST['request_document_vendor_id'] .'?tab=document');
        echo json_encode($data);exit;
    }

    public function actionRequestVendorDocumentOnboard(){
        $msg = 0;


        if(!empty($_POST['request_document_vendor_id'])){
            $userID       = Yii::app()->session['user_id'];
            $userName     = Yii::app()->session['full_name'];
            $dateTime     = date("Y-m-d H:i:s");
            $vendorID = $_POST['request_document_vendor_id'];
            //$contractIDs = $_POST['contract_id'];

            if(!empty($_POST['vendor_type_new'])){
            	// echo "<pre>"; print_r($_POST); exit;
                $documentTypeNew = addslashes($_POST['vendor_type_new']);
                $sql ="INSERT INTO `vendor_document_type_onboard` (`id`, `name`, `created_type`, `btn_style`, `created_at`) VALUES (NULL, '".$documentTypeNew ."', 'Cusom', NULL, '".$dateTime."');";
                $reader       = Yii::app()->db->createCommand($sql)->execute();
                $documentType = Yii::app()->db->lastInsertID;
            }else{
                $documentType = $_POST['document_vendor_type'];
            }
            $contactName  = serialize($_POST['vendor_contact_name']);
            $contactEmail = serialize($_POST['vendor_contact_email']);
            $note         = addslashes($_POST['request_document_note']);
            
           /* foreach($contractIDs as $contractID){*/
            /*    if($contractID){*/
                $reference = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)))), 1, 24);
                $contact = new VendorRequestDocumentOnboard();
                $contact->rs = array();
                $contact->rs['vendor_id']       = $vendorID;
                $contact->rs['user_id'] = $userID;
                $contact->rs['user_name'] = $userName;
                $contact->rs['vendor_document_type'] = $documentType;
                $contact->rs['supplier_contact_name'] = $contactName;
                $contact->rs['supplier_contact_email'] = $contactEmail;
                $contact->rs['note'] = $note;
                $contact->rs['status'] = 'Pending';
                $contact->rs['reference'] = $reference;
                $contact->rs['created_at'] = $dateTime;
                
                $contact->write();
                $data = array();
                if(!empty($contact->rs['id'])){
                    $msg = 1;
                    $cronEmail = new CronEmail;
                    $cronEmail->rs = array();
                    $cronEmail->rs['record_id'] = $contact->rs['id'];
                    $cronEmail->rs['send_to'] = 'Vendor'; 
                    $cronEmail->rs['user_id'] = $vendorID;
                    $cronEmail->rs['user_type'] = 'Request Vendor Document Onboard';
                    $cronEmail->rs['status'] = 'Pending';
                    $cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
                    $cronEmail->write();
                }
            /*}
            }*/
        }

        $data = array();
        $data['msg'] = $msg;
        echo json_encode($data);exit;
    }

    public function actionLoadingRequestDocument()
    {
        $vendorID = $_POST['vendor_id'];
        $vendor = new Vendor;
        $documentListPendingRequest = $vendor->getVendorDocumentsPendingRequest($vendorID);
        $this->renderPartial('_vendor_document_pending',array('documentListPendingRequest'=>$documentListPendingRequest,'vendor_id'=>$vendorID));
    }

    public function actionLoadingRequestDocumentOnboard()
    {
        $vendorID = $_POST['vendor_id'];
        $vendor = new Vendor;
        $documentListPendingRequest = $vendor->getVendorDocumentsPendingRequestOnboard($vendorID);
        $this->renderPartial('_vendor_document_pending_onboard',array('documentListPendingRequest'=>$documentListPendingRequest,'vendor_id'=>$vendorID));
    }

    public function actionDeleteDocumentPending()
    {
        $id = $_POST['request_id'];
        $vendorID = $_POST['vendor_id'];
        $sql = "delete from vendor_request_documents WHERE  id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->execute();
        $vendor = new Vendor;
        $documentListPendingRequest = $vendor->getVendorDocumentsPendingRequest($vendorID);
        $this->renderPartial('_vendor_document_pending',array('documentListPendingRequest'=>$documentListPendingRequest,'vendor_id'=>$vendorID));
    }

    public function actionDeleteDocumentPendingOnboard()
    {
        $id = $_POST['request_id'];
        $vendorID = $_POST['vendor_id'];
        $sql = "delete from vendor_request_documents_onboard WHERE  id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->execute();
        $vendor = new Vendor;
        $documentListPendingRequest = $vendor->getVendorDocumentsPendingRequestOnboard($vendorID);
        $this->renderPartial('_vendor_document_pending_onboard',array('documentListPendingRequest'=>$documentListPendingRequest,'vendor_id'=>$vendorID));
    }

	public function actionExport()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$vendor = new Vendor();
		$vendor->exportVendor();
	}

	public function actionReview()
	{	date_default_timezone_set(Yii::app()->params['timeZone']);
		// is someone already logged in?
	    error_reporting(0);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'review_list';

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));
		$documentsTitle = FunctionManager::vendorDocument();
		$currentDocumentDate = date('Y-m-d');
		// by default, show reports for year to date time frame
        $user_selected_year = Yii::app()->input->post('user_selected_year');
     
        $user_selected_year = date("Y");

        $startDate = "2010-01-01";
        $currentDate = $user_selected_year . "-12-31";

		// get list of all vendors (with pagination of course)
		$vendor = new Vendor();
		// and display
		$view_data['vendors'] = array();

		$view_data['vendorStatistics'] = $vendor->getVendorStatistics(); 
		$view_data['vendorMetric'] = $vendor->getVendorMetric();
		$view_data['vendors_by_category'] = '';
		$report = new Report();
		$view_data['performance_avg'] = $report->getVendorAvgPerformance();

		 // echo $performance_avg['total_vendors']; exit;

		$notification = new Notification();
        $view_data['notifications'] = $notification->getLastestForSupplier(5);

		$contract = new Contract();
        $view_data['top_contracts'] = $contract->getTopContracts($startDate, date("Y-m-d"));


        $industry_id = 0;
	    if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id'])){
	      $industry_id = $_REQUEST['industry_id']; 

	    }
	   
	    $subindustry_id = 0;
	    if (isset($_REQUEST['subindustry_id']) && !empty($_REQUEST['subindustry_id'])){
	      $subindustry_id = $_REQUEST['subindustry_id'];      
	    }

	   
	    $preferred_flag = -1;
	    $supplier_status = '';
	    if (isset($_REQUEST['preferred_flag'])){
	      $preferred_flag = $_REQUEST['preferred_flag'];
	    } 

	    if (isset($_REQUEST['supplier_status'])){
	      $supplier_status = $_REQUEST['supplier_status'];
	    } 

	    // When click if clear filter buttom should reset the search , show default vendor list 
	    if(!empty($_REQUEST['reset_form_post_request'])){
	    	$industry_id = 0;
	    	$preferred_flag = -1;
	    	$supplier_status = '';
	    	$subindustry_id = 0;
	    }
        $database_values = $vendor->getVendorsReview(0, $industry_id, $subindustry_id, $preferred_flag,$supplier_status);
        
		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('soft_deleted' =>  '0','ORDER' => 'value'));
		$user = new User();
		$userData = $user->getOne(array('user_id'=>Yii::app()->session['user_id']));
		$view_data['vendor_visit'] = $userData['vendor_visit'];
		if($userData['vendor_visit'] !=1){
			$user->rs = array();
			$user->rs['user_id'] = $userData['user_id'];
			$user->rs['vendor_visit'] = 1;
			$user->write();
		}

		$view_data['vendors'] = array();
		$view_data['database_values'] = $database_values;
		$view_data['industry_id'] = $industry_id;
		$view_data['subindustry_id'] = $subindustry_id;
		$view_data['supplier_status'] = $supplier_status;
		$view_data['preferred_flag'] = $preferred_flag;
		$this->render('review', $view_data);
				
	}


	public function actionApproverVendor(){
		
		$vendorId 	   = $_POST['vendor_id'];
		$approverId    = $_POST['approver_id'];
		$vendorStatus  = $_POST['approver_status'];
		$userId  = Yii::app()->session['user_id'];
		$userName  = Yii::app()->session['full_name'];
		$createdAt = date("Y-m-d H:i:s");
		 
		$vendorComment = addslashes($_POST['approver_comment']);
		$approvedTime  = $_POST['approver_datetime'];

		if($vendorStatus=="Approved"){
			$active = 1;
			$profileStatus = "Active";
		}else{
			$active = 0;
			$profileStatus = "Inactive";
		}
		

		$vendor = new Vendor();
		$vendorCheck = $vendor->checkVendorApprovel($vendorId);
		
		$sql =" update vendors set approver_id='".$approverId."', approver_status='".$vendorStatus."', approver_comment='".$vendorComment."',approver_datetime='".$approvedTime."',active='".$active."',profile_status='".$profileStatus."' where vendor_id=".$vendorId;
		$vendorUpdate = Yii::app()->db->createCommand($sql)->execute();

	     $sql = "INSERT INTO vendor_approval_history(user_id,vendor_id,user_name,approver_id,approver_status,approver_comment,approver_datetime,created_at)
                VALUES ('$userId','$vendorId','$userName','$approverId','$vendorStatus','$vendorComment','$approvedTime','$createdAt')";
                $vendorApprovalHostory = Yii::app()->db->createCommand($sql)->execute();      


        

		// get vendor aprrover by Sent For Approval
		
		
		 if($vendorStatus == "Sent For Approval" || $vendorStatus == "Approved" || $vendorStatus == "Rejected"){	

		 	if($vendorStatus == "Approved" || $vendorStatus == "Rejected"){
		 		$userType = "v.user_id";

		 	}else {
		 		$userType = "v.approver_id";
		 	}

		 	   $sql="
					select v.vendor_name,v.vendor_id,v.approver_comment,v.approver_status,u.full_name,u.user_id,u.email, v.emails AS vendorsEmail from vendors v 
		  			inner join users u  on ".$userType." = u.user_id

					where vendor_id=".$vendorId;
		    	$vendorApproverStatus = Yii::app()->db->createCommand($sql)->queryRow();
		    	$vendorName   = $vendorApproverStatus['vendor_name'];
		    	$vendorID   = $vendorApproverStatus['vendor_id'];
		    	$vendorStatus = $vendorApproverStatus['approver_status'];
		    	$userEmail  = $vendorApproverStatus['email'];
		    	$vendorsEmail  = $vendorApproverStatus['vendorsEmail'];
		    	$userName  = $vendorApproverStatus['full_name'];
		    	$userID  = $vendorApproverStatus['user_id'];
		    	$subject = 'Supplier '.$vendorStatus;
		    	$comments = $vendorApproverStatus['approver_comment'];

	          /* EmailManager::vendorApprovalBySentForApprovalEmail($userEmail,$vendorName,$vendorID,$vendorStatus,$userName,$subject,$comments);*/
	           $notificationFlag = $vendorID.'clientSupplier'.$vendorStatus.$userID;
	            if($vendorStatus == "Sent For Approval"){
	            	$notificationComments  = "You have a request to approve supplier ".$vendorName;
	            	$subject = 'Supplier Approval Request';
	            }else if($vendorStatus == "Approved"){
	            	$notificationComments  = "Supplier: <b>".$vendorName."</b> has been approved";
	            	$subject = 'Supplier Approved';
	            }else{
	            	$subject = 'Supplier Rejected';
	            	$notificationComments  = "Supplier: <b>".$vendorName."</b> has been rejected";
	            }
	           // $notificationComments  = "Supplier: <b>".$vendorName."</b> Status Changed to <b>".$vendorStatus."</b>";
	          $notification = new Notification();
	          $notification->rs = array();
	          $notification->rs['id'] = 0;
	          $notification->rs['user_type'] = 'Client';
	          $notification->rs['notification_flag'] = $notificationFlag;
	          $notification->rs['user_id'] = $userID;
	          $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendorID) .'">'.$notificationComments.'</a>';
	          $notification->rs['notification_date'] = date("Y-m-d H:i");
	          $notification->rs['read_flag'] = 0;
	          $notification->rs['notification_type'] = 'Supplier Status';
	          $notification->write();

	          // if($vendorStatus == "Sent For Approval"){
	          // 	 $subject = 'You have a request to approve supplier'.$vendorStatus.' Notification';
	          // 	}else{
	          // 		$subject = 'Supplier Status '.$vendorStatus.' Notification';
	          // 	}
          
          
          $notificationText = '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendorID) . '" style="font-size:18px;color:#2d9ca2;text-decoration:none">'.$notificationComments.'</a>';
          $url = AppUrl::bicesUrl('vendors/edit/' . $vendorID);
           
           EmailManager::userNotification($userEmail,$userName,$subject,$notificationText,$url);
          
          // EmailManager::userNotification($userEmail,$userName,$subject,$notificationText,$url);
	      } 

		$vendorApproval = new VendorApprovalLog();
		$log = $vendorApproval->getApprovalLogs($vendorId);


		$comment = '';
		 if(!empty($vendorCheck['approver_status'])){
	        if($vendorCheck['approver_id'] !=$approverId ){
	          $sql = "SELECT full_name  FROM users WHERE  user_id=".$approverId;
	          $userReader = Yii::app()->db->createCommand($sql)->queryRow();
	          $comment .= '<b>User Approver:</b> <span class="title-text"><b>'. $vendorCheck['full_name']. '</b> Changed to <b>'.$userReader['full_name'].'</b></span><br/>';
	        }
	        if($vendorCheck['approver_status'] != $vendorStatus ){
	          $comment .= '<b>Status:</b> <span class="title-text"><b>'. $vendorCheck['approver_status']. '</b> Changed to <b>'.$vendorStatus.'</b></span><br/>';
	        }
	        if($vendorCheck['approver_comment'] != $vendorComment ){
	          $comment .= '<b>Comment:</b> <span class="title-text"><b>'. $vendorCheck['approver_comment']. '</b> Changed to <b>'.$vendorComment.'</b></span><br/>';
	        }
	        if($vendorCheck['approver_datetime'] != $approvedTime ){
	          $comment .= '<b>Date:</b> <span class="title-text"><b>'. $vendorCheck['approver_datetime']. '</b> Changed to <b>'.$approvedTime.'</b></span><br/>';
	        }

            if(!empty($comment)){
	            $vendorLog = new VendorApprovalLog();
	            $vendorLog->rs = array();
	            $vendorLog->rs['vendor_id'] 	= $vendorId;
	            $vendorLog->rs['user_id'] 		= Yii::app()->session['user_id'];
	            $vendorLog->rs['comment'] 		= !empty($comment)?$comment:"";
	            $vendorLog->rs['vendor_status'] = $vendorStatus;
	            $vendorLog->rs['created_datetime'] = date('Y-m-d H:i:s');
	            $vendorLog->rs['updated_datetime'] = date('Y-m-d H:i:s');
	            $vendorLog->write();

        	}

    	}

		//if($vendorUpdate){
			Yii::app()->user->setFlash('success', "Success! Supplier status changed successfully.");
			$this->redirect(AppUrl::bicesUrl('vendors/edit/'.$vendorId ));
		//}
	} 

}
