<?php

class LocationsController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('locations/list'));
	}

	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'locations';
		
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	

		// get list of all locations (with pagination of course)
		$location = new Location();
		$search_parameters = array('order' => 'location_name');
		$locations = $location->getData($search_parameters);

		// and display
		$view_data = array();
		$view_data['locations'] = $locations;
		$this->render('list', $view_data);
	}

	public function actionEdit($location_id = 0)
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'locations';
		
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	

		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted'))
		{
			$location = new Location();
			$location->saveData();
			$newloc='';
			if($location_id==0){
              $newloc='/newloc/'.$location->rs['location_id'];
              Yii::app()->user->setFlash('success', "Location saved successfully");
			}
			$this->redirect(AppUrl::bicesUrl('locations/edit/' . $location->rs['location_id'].$newloc));
		}
		
		// get data for the selected location
		$view_data = array('location_id' => $location_id);
		$location = new Location();
		$view_data['location'] = $location->getOne(array('location_id' => $location_id));

		// and display
		$this->render('edit', $view_data);
	}

	public function actionImport()
	{
		// is someone already logged in?
//		$this->checklogin();
//        $this->layout = 'main';
//        if (!isset(Yii::app()->session['user_type']) || Yii::app()->session['user_type'] != 1)
//			$this->redirect(AppUrl::bicesUrl('orders/list'));
//		else
//		{
//	        if (!isset(Yii::app()->session['admin_flag']) || Yii::app()->session['admin_flag'] != 1)
//				$this->redirect(AppUrl::bicesUrl('app/dashboard'));
//		}
		$this->render('import');
	}

	public function actionImportData()
	{
		if (isset($_FILES['file']) && isset($_FILES['file']['tmp_name']))
		{
			$location = new Location();
			$location->importData($_FILES['file']['tmp_name']);
		}
	}

	public function actionGetDepartments()
	{
		$location_id = isset($_REQUEST['location_id']) ? $_REQUEST['location_id'] : 0;
		$single = isset($_REQUEST['single']) ? $_REQUEST['single'] : 1;
		
		$location = new Location();
		
		echo json_encode($location->getDepartments($location_id,$single));
	}

	public function actionGetDepartmentsByPerms()
	{
		$location_id = isset($_REQUEST['location_id']) ? $_REQUEST['location_id'] : 0;
		$user_id = $_REQUEST['user_id'];
		$field_name = $_REQUEST['field_name'];
		$location = new Location();
		echo json_encode($location->getDepartmentsByPerms($user_id,$location_id,$field_name));
	}

	public function actionGetLocations()
	{
		$department_id = isset($_REQUEST['dept_id']) ? $_REQUEST['dept_id'] : 0;
		$location = new Location();
		echo json_encode($location->getDepartmentForMultiLocations($department_id));
	}

	public function actionGetAccessDepartments()
	{
		$location_id = isset($_REQUEST['location_id']) ? $_REQUEST['location_id'] : 0;
		$single = isset($_REQUEST['single']) ? $_REQUEST['single'] : 1;
		$location = new Location();
	
		echo json_encode($location->getAccesstDepartments($location_id,$single));
	}

}



    
