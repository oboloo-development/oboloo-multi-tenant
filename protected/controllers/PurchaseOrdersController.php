<?php

require_once('protected/vendors/tcpdf/tcpdf.php');
require_once('protected/vendors/tcpdf/config/lang/eng.php');
 
class PurchaseOrdersController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('purchaseOrders/list'));
	}

	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'purchase_orders';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		//$this->getCurrency($user_currency);

		// get contracts as per filters applied
		$view_data = array();
		$location_id = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$po_status = Yii::app()->input->post('po_status');
		$purchase_order = new PurchaseOrder();
		
		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');
		
		$db_from_date = "";
		if (!empty($from_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $from_date);
	       	$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['from_date'] = $db_from_date;
		}

		$db_to_date = "";
		if (!empty($to_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $to_date);
	       	$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['to_date'] = $db_to_date;
		}

		if(!empty($department_id)){
			$location = new Location();
			//$department_selected = implode(',',$department_id);

			//$sqlCond = " department_id IN ($department_selected)";
			//$sql = "SELECT * FROM departments WHERE ".$sqlCond." ORDER BY department_name";
			//$department_selected = Yii::app()->db->createCommand($sql)->query()->readAll();
			$department_info = $location->getDepartments($location_id);
		}else
		$department_info='';

		// need filters too
		$location = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;
		$view_data['department_info'] = $department_info;
		
		// and display
		$view_data['department_id'] = $department_id;
		$view_data['po_status'] = $po_status;
		$view_data['purchase_orders'] = $purchase_order->getOrders(0, $location_id, $department_id, $po_status, $db_from_date, $db_to_date);
		$this->render('list', $view_data);
	}

	public function actionSubmitOrder()
	{
		// is someone already logged in?
		$this->clearSession();
		$this->pageTitle = "Submit PO";
		$this->layout = 'main';

		$error = 0;
		$invitation_reference = "";
		$form_submitted = Yii::app()->input->post('form_submitted');
		if ($form_submitted)
		{
			$reference = Yii::app()->input->post('reference');
			$invitation_reference = $reference;

			$po = new PurchaseOrder();
			$po_data = $po->getOrderByReference($reference);
			if ($po_data && is_array($po_data) && isset($po_data['po_id']) && isset($po_data['vendor_id']))
			{
				$reference_key = $po_data['po_id'] . '_' . $po_data['order_id'] . '_' . $po_data['vendor_id'];
				$order_id = base64_encode($reference_key);
				$this->redirect(AppUrl::bicesUrl('purchaseOrders/submitOrderDetails/?submit_order_id=' . $order_id));
			}
			else $error = 1;
		}

		$this->render('submit_order', array('error' => $error, 'invitation_reference' => $invitation_reference));
	}

	public function actionSubmitOrderDetails()
	{
		// is someone already logged in?
		$this->clearSession();
		$this->pageTitle = "Submit Order Detail";
		$this->layout = 'main';
		$view_data = array();

		$order_found = false;
		$reference_key = isset($_REQUEST['submit_order_id']) ? $_REQUEST['submit_order_id'] : "";
		//echo $reference_key;die;
		if (!empty($reference_key)) {
			//CVarDumper::dump($_POST,10,1);die;
			$purchase_order = new PurchaseOrder();
			if (Yii::app()->input->post('form_submitted'))
			{
				$order_id = $order_submission_data['order_id'] = Yii::app()->input->post('order_id');
				$po_id = $order_submission_data['po_id'] = Yii::app()->input->post('po_id');
				$vendor_id = $order_submission_data['vendor_id'] = Yii::app()->input->post('vendor_id');
				$order_submission_data['status'] = Yii::app()->input->post('status');
				$order_submission_data['tracking_number'] = Yii::app()->input->post('tracking_number');
				$order_submission_data['courier_company'] = Yii::app()->input->post('courier_company');
				$order_submission_data['note'] = Yii::app()->input->post('note');
				$purchase_order->saveOrderSubmissionData($order_submission_data);
				// files uploaded if any
				$total_documents = Yii::app()->input->post('total_documents');
				for ($idx=1; $idx<=$total_documents; $idx++)
				{
					$delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idx);
					if ($delete_document_flag) continue;

					if (isset($_FILES['order_file_' . $idx]) && is_array($_FILES['order_file_' . $idx]))
					{
						if (!is_dir('uploads/orders')) mkdir('uploads/orders');
						if (!is_dir('uploads/orders/' . $order_id))
							mkdir('uploads/orders/' . $order_id);
						/*if (!is_dir('uploads/orders/' . $order_id . '/' . $vendor_id))
							mkdir('uploads/orders/' . $order_id . '/' . $vendor_id);*/
						if (!is_dir('uploads/orders/invoices/' . $order_id)) 
							mkdir('uploads/orders/invoices/' . $order_id);

						if (!isset($_FILES['order_file_' . $idx]['error']) || empty($_FILES['order_file_' . $idx]['error']))
						{
							$file_name = time().'_'.$_FILES['order_file_' . $idx]['name'];
							move_uploaded_file($_FILES['order_file_' . $idx]['tmp_name'], 'uploads/orders/invoices/' . $order_id . '/'. $file_name);
							//Save documents
							$orderInvoice = new OrderInvoice();
							$orderInvoice->rs = array();
							$orderInvoice->rs['order_id'] = $order_id;
							$orderInvoice->rs['vendor_id'] = $vendor_id;
							$orderInvoice->rs['created_by_type'] = 1;
							//$orderInvoice->rs['invoice_number'] = $invoice_number;
							$orderInvoice->rs['file_name'] = $file_name;
							$orderInvoice->rs['status'] = 1;
							$orderInvoice->rs['created_at'] = date('Y-m-d H:i:s');
							$orderInvoice->write();

							$qouteVendorFile = new OrderVendorFile();
							$qouteVendorFile->rs = array();
							$qouteVendorFile->rs['order_id'] = $order_id;
							$qouteVendorFile->rs['vendor_id'] = $vendor_id;
							$qouteVendorFile->rs['file_name'] = $file_name;
							$qouteVendorFile->write();
						}

					}
				}

				$purchase_order->submitOrder($order_id,$vendor_id);

				$order_found = true;
			}


			$po_id = $order_id = $vendor_id = 0;
			 $complete_order_id = base64_decode($reference_key);

			if (count(explode("_", $complete_order_id)) >= 3)
				list($po_id, $order_id, $vendor_id) = explode("_", $complete_order_id);

			$view_data = array('order_id' => $order_id,'po_id'=>$po_id,'vendor_id'=>$vendor_id,'submit_order_id'=>$reference_key);
			$order = new Order();
			$view_data['order'] = $order->getOrders($order_id);
			if (!$view_data['order'] || !is_array($view_data['order'])) $order_id = 0;

			$view_data['order_invoice'] = $order->orderInvoice($order_id);
			// need some drop down values
			$location = new Location();
			$view_data['locations'] = $location->getData(array('order' => 'location_name'));

			$department = new Department();
			$view_data['departments'] = $department->getData(array('order' => 'department_name'));

			$project = new Project();
			$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

			$user = new User();
			$view_data['users'] = $user->getData(array('order' => 'full_name'));

			// other drop downs may be needed for vendor quick add modal window
			$payment_term = new PaymentTerm();
			$view_data['payment_terms'] = $payment_term->getAll();

			$shipping_method = new ShippingMethod();
			$view_data['shipping_methods'] = $shipping_method->getAll();

			$currency_rate = new CurrencyRate();
			$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

			$industry = new Industry();
			$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));

			// and also product information for the order
			$order_detail = new OrderDetail();
			$view_data['order_details'] = $order_detail->getOrderDetails($order_id);


			// and finally approvals if any
			if ($order_id) {
				$route = new OrderRoute();
				$view_data['approvals'] = $route->getApprovalRoute('O', $order_id);
			} else $view_data['approvals'] = array();

			// if approved, is purchase order already created?
			if ($order_id) {
				$purchase_order = new PurchaseOrder();
				$view_data['purchase_order'] = $purchase_order->getOne(array('order_id' => $order_id));
			}

			if ($order_id) {
				$order_vendor_detail = new OrderVendorDetail();
				$view_data['order_vendor_detail'] = $order_vendor_detail->getOne(array('order_id' => $order_id));
			}

			//CVarDumper::dump($view_data,10,1);die;

		}

//		if (!$order_found) $this->redirect(AppUrl::bicesUrl('purchaseOrders/submitOrder'));
//		else
//		{
			$this->render('submit_order_details', $view_data);
		//}
	}

	public function actionEdit($po_id = 0)
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		$userType = Yii::app()->session['user_type'];

		$purchase_order = new PurchaseOrder();

		if(!empty($_POST['po_id'])){ $po_id = $_POST['po_id']; }
		$purchasePermission = $purchase_order->checkPermission($po_id);

		if (!isset($userType) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
		{
			$this->redirect(AppUrl::bicesUrl('purchaseOrders/list'));
		}else if(empty($purchasePermission)){
			$this->redirect(AppUrl::bicesUrl('purchaseOrders/list'));
		}
		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted'))
		{						
			// save actual order data
			$purchase_order->saveData();
			// change the original order status also if necessary
			if (Yii::app()->input->post('create_po'))
			{
				$purchase_order->rs['po_status'] = 'PO Not Sent To Supplier';
				$purchase_order->write();
				$status_order = new Order();
				$status_order->rs['order_id'] = $purchase_order->rs['order_id'];
				$status_order->rs['order_status'] = 'Ordered - PO Not Sent To Supplier';
				$status_order->write();
				$this->redirect(AppUrl::bicesUrl('purchaseOrders/edit/' . $purchase_order->rs['po_id']));
			}

			// change the original order status also if necessary
			if (isset($purchase_order->rs['po_status']) && $purchase_order->rs['po_status'] != 'Pending')
			{
				$status_order = new Order();
				$status_order->rs['order_id'] = $purchase_order->rs['order_id'];
				$status_order->rs['order_status'] = $purchase_order->rs['po_status'];
				$status_order->write();
			}

			// and discount, charges as applied
			$purchase_order->saveOrderLevelData($purchase_order->rs['po_id']);

			// receipts if any were uploaded
			if (isset($_FILES['file']) && is_array($_FILES['file']) && count($_FILES['file']))
			{
				if (!is_dir('uploads/purchase_orders')) mkdir('uploads/purchase_orders');
				if (!is_dir('uploads/purchase_orders/' . $purchase_order->rs['po_id'])) 
					mkdir('uploads/purchase_orders/' . $purchase_order->rs['po_id']);
				
				for ($idx=0; $idx<count($_FILES['file']['name']); $idx++)
				{
					$receipt_file = array();
					$receipt_file['name'] = $_FILES['file']['name'][$idx];
					$receipt_file['type'] = $_FILES['file']['type'][$idx];
					$receipt_file['tmp_name'] = $_FILES['file']['tmp_name'][$idx];
					$receipt_file['error'] = $_FILES['file']['error'][$idx];
					$receipt_file['size'] = $_FILES['file']['size'][$idx];
					
					if (!isset($receipt_file['error']) || empty($receipt_file['error']))
					{
			            $file_name = $receipt_file['name'];
			            move_uploaded_file($receipt_file['tmp_name'], 'uploads/purchase_orders/' . $purchase_order->rs['po_id'] . '/' . $file_name);
					}
				}
			}

			$this->redirect(AppUrl::bicesUrl('purchaseOrders/edit/' . $purchase_order->rs['po_id']));
		}

		// no direct access to create purchase order
		if (!$po_id) $this->redirect(AppUrl::bicesUrl('purchaseOrders/list'));

		// get data for the selected vendor
		$view_data = array('po_id' => $po_id);
		$purchase_order = new PurchaseOrder();
		$view_data['purchase_order'] = $purchase_order->getOrders($po_id);

		$view_data['purchase_permission'] = $purchasePermission;

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$department = new Department();
		$view_data['departments'] = $department->getAll(array('order' => 'department_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));

		// other drop downs may be needed for vendor quick add modal window
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$shipping_term = new ShippingTerm();
		$view_data['shipping_terms'] = $shipping_term->getAll();

		// finally order details for display only purposes
		if ($po_id && $view_data['purchase_order'] && is_array($view_data['purchase_order']) 
				&& isset($view_data['purchase_order']['order_id']) && $view_data['purchase_order']['order_id'])
		{
			$order_id = $view_data['purchase_order']['order_id'];

			$order = new Order();
			$view_data['order_info'] = $order->getOrders($order_id);
			$view_data['purchase_order']['order_payment'] = $view_data['order_info']['total_price']; 
			
			$order_detail = new OrderDetail();
			$view_data['order_details'] = $order_detail->getOrderDetails($order_id);

			$vendor_id = $view_data['order_info']['vendor_id'];
			$vendor = new Vendor();
			$view_data['po_vendor'] = $vendor->getVendors($vendor_id);

			$location_id = $view_data['order_info']['location_id'];
			$location = new Location();
			$view_data['po_location'] = $location->getOne(array('location_id' => $location_id));

			$route = new OrderRoute();
			$view_data['approvals'] = $route->getApprovalRoute('O', $order_id);
		}
		else 
		{
			$view_data['order_info'] = $view_data['order_details'] = $view_data['approvals'] = array();
			$view_data['purchase_order']['order_payment'] = $view_data['purchase_order']['po_total']
				+ $view_data['purchase_order']['po_discount'] - $view_data['purchase_order']['po_other_charges'];  
		}  
		
		// and display
		$this->render('edit', $view_data);
	}

	public function actionDeleteFile()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		// which order record the file relates to?
		$po_id = Yii::app()->input->post('po_id');
		$file_name = Yii::app()->input->post('file_name');
		
		// directory where this file resides
		$complete_file_name = 'uploads/purchase_orders/' . $po_id . '/' . $file_name;
		if (is_file($complete_file_name)) @unlink($complete_file_name);		
	}

	
	public function actionPDF()
	{
	    // is someone already logged in?
	    error_reporting(0);
		$this->checklogin();
        $this->layout = 'main';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);

		// which order record the file relates to?
		$po_id = isset($_REQUEST['po_id']) ? $_REQUEST['po_id'] : 0;

		// get data for the selected purchase order
		$view_data = array('po_id' => $po_id);
		$purchase_order = new PurchaseOrder();
		$view_data['purchase_order'] = $purchase_order->getOrders($po_id);

		$order_id = $view_data['purchase_order']['order_id'];
		$order = new Order();
		$view_data['order_info'] = $order->getOrders($order_id);
		$view_data['purchase_order']['order_payment'] = $view_data['order_info']['total_price']; 
			
		$order_detail = new OrderDetail();
		$view_data['order_details'] = $order_detail->getOrderDetails($order_id);
		
		$vendor_id = $view_data['order_info']['vendor_id'];
		$vendor = new Vendor();
		$view_data['vendor'] = $vendor->getVendors($vendor_id);
		
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getOne(array('id' => $view_data['vendor']['payment_term_id']));
		
		$location_id = $view_data['order_info']['location_id'];
		$location = new Location();
		$view_data['location'] = $location->getOne(array('location_id' => $location_id));

		//company name
		$view_data['company_name'] = Yii::app()->session['company_name'];

		$company = new Company();
		$view_data['company'] = $company->getCompany();

		// first create HTML output
		$html_output = $this->renderPartial('pdf', $view_data, true);
		
		// and then display as PDF
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $the_creator_alpha_omega_and_all_that_jazz_rolled_into_THE_ONE = 'Spend 365 Tool';
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($the_creator_alpha_omega_and_all_that_jazz_rolled_into_THE_ONE);
        $pdf->SetTitle($the_creator_alpha_omega_and_all_that_jazz_rolled_into_THE_ONE);
        $pdf->SetSubject($the_creator_alpha_omega_and_all_that_jazz_rolled_into_THE_ONE);

        // set default monospaced font
        $pdf->SetFont('Helvetica', '', 11);

        // set margins
        $pdf->SetMargins(5, 3, 2);

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 0);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // convert HTML to PDF
        $pdf->AddPage();
        $pdf->writeHTML($html_output, true, false, true, false, '');
		
		// and allow to download
		/*$attachmentFile =  Yii::getPathOfAlias('webroot')."/uploads/purchase_orders/attachment/pdf-test-new.pdf"; 
		$pdf->Output($attachmentFile, 'F');*/
		$pdf->Output($view_data['purchase_order']['po_number'] . ".pdf", 'I');
		exit;		
	}

	public function savePDFAttachment($po_id)
	{
	    // is someone already logged in?
	    error_reporting(0);
		$this->checklogin();
        $this->layout = 'main';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);

		// get data for the selected purchase order
		$view_data = array('po_id' => $po_id);
		$purchase_order = new PurchaseOrder();
		$view_data['purchase_order'] = $purchase_order->getOrders($po_id);

		$order_id = $view_data['purchase_order']['order_id'];
		$order = new Order();
		$view_data['order_info'] = $order->getOrders($order_id);
		$view_data['purchase_order']['order_payment'] = $view_data['order_info']['total_price']; 
			
		$order_detail = new OrderDetail();
		$view_data['order_details'] = $order_detail->getOrderDetails($order_id);
		
		$vendor_id = $view_data['order_info']['vendor_id'];
		$vendor = new Vendor();
		$view_data['vendor'] = $vendor->getVendors($vendor_id);
		
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getOne(array('id' => $view_data['vendor']['payment_term_id']));
		
		$location_id = $view_data['order_info']['location_id'];
		$location = new Location();
		$view_data['location'] = $location->getOne(array('location_id' => $location_id));

		//company name
		$view_data['company_name'] = Yii::app()->session['company_name'];

		$company = new Company();
		$view_data['company'] = $company->getCompany();

		// first create HTML output
		$html_output = $this->renderPartial('pdf', $view_data, true);
		
		// and then display as PDF
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $the_creator_alpha_omega_and_all_that_jazz_rolled_into_THE_ONE = 'Spend 365 Tool';
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($the_creator_alpha_omega_and_all_that_jazz_rolled_into_THE_ONE);
        $pdf->SetTitle($the_creator_alpha_omega_and_all_that_jazz_rolled_into_THE_ONE);
        $pdf->SetSubject($the_creator_alpha_omega_and_all_that_jazz_rolled_into_THE_ONE);

        // set default monospaced font
        $pdf->SetFont('Helvetica', '', 11);

        // set margins
        $pdf->SetMargins(5, 3, 2);

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 0);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // convert HTML to PDF
        $pdf->AddPage();
        $pdf->writeHTML($html_output, true, false, true, false, '');
		$fileDir =  Yii::getPathOfAlias('webroot')."/uploads/purchase_orders";
		// and allow to download
		if (!is_dir($fileDir)) {
			//Create our directory if it does not exist
			mkdir($fileDir);
		}
		$fileDir .= "/attachment";
		if (!is_dir($fileDir)) {
			//Create our directory if it does not exist
			mkdir($fileDir);
		}
		$fileDir .= "/".$po_id;
		if (!is_dir($fileDir)) {
			//Create our directory if it does not exist
			mkdir($fileDir);
		}
		$attachmentFile =  $fileDir."/".$po_id.".pdf"; 
		$pdf->Output($attachmentFile, 'F');
		 		
	}

	public function actionUpdateOnlyStatus()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		// if form was submitted ... first save the data to the database
		 $po_id = Yii::app()->input->post('po_confirm_id_from_modal');
		if (Yii::app()->input->post('modal_confirm_form_submitted'))
		{			
			if ($po_id)
			{   $order = new Order();
				$purchase_order = new PurchaseOrder();
				$po_data = $purchase_order->getOne(array('po_id' => $po_id));
				if ($po_data && is_array($po_data) && $po_data['po_status'] == 'PO Not Sent To Supplier')
				{
					$orderArr = $order->getOne(array('order_id' => $po_data['order_id']));
					$vendor_id =  $orderArr['vendor_id'];

					$purchase_order->rs = array();
					$purchase_order->rs['po_id'] = $po_id;
					$purchase_order->rs['po_status'] = "PO Sent To Supplier";
					$purchase_order->rs['vendor_id'] = $vendor_id;
					$purchase_order->rs['po_sent_date'] = date("Y-m-d H:i:s");
					$purchase_order->write();
					$this->savePDFAttachment($po_id);
					$purchase_order->sendInvites($po_id, $po_data['order_id']);
					
					// same goes with related order too 
					
					$order->rs = array();
					$order->rs['order_id'] = $po_data['order_id'];
					$order->rs['order_status'] = "Ordered - PO Sent To Supplier";
					$order->write();
				}
			}
		}

		$this->redirect(AppUrl::bicesUrl('purchaseOrders/edit/' . $po_id));
	}

	public function actionUpdateVendorLocation(){

		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';

		if (Yii::app()->input->post('modal_form_confirm'))
		{
			//CVarDumper::dump(Yii::app()->input->post('vendor_id'),10,1);die;


			$vendor = new Vendor();
			$order_vendor = $vendor->getOne(array('vendor_id' => Yii::app()->input->post('vendor_id')));
//			$vendor->rs = array();
//			$vendor->rs['contact_name'] = Yii::app()->input->post('contact_name');
//			$vendor->rs['address_1'] = Yii::app()->input->post('address_1');
//			$vendor->rs['city'] = Yii::app()->input->post('city');
//			$vendor->rs['state'] = Yii::app()->input->post('state');
//			$vendor->rs['zip'] = Yii::app()->input->post('zip');
//			$vendor->rs['phone_1'] = Yii::app()->input->post('phone_1');
//			$vendor->rs['emails'] = $order_vendor['emails'];
//			$vendor->saveData();

			$vendor_submission_date['contact_name'] = Yii::app()->input->post('contact_name');
			$vendor_submission_date['address_1'] = Yii::app()->input->post('address_1');
			$vendor_submission_date['city'] = Yii::app()->input->post('city');
			$vendor_submission_date['state'] = Yii::app()->input->post('state');
			$vendor_submission_date['zip'] = Yii::app()->input->post('zip');
			$vendor_submission_date['phone_1'] = Yii::app()->input->post('phone_1');
			$vendor_submission_date['vendor_id'] = Yii::app()->input->post('vendor_id');
			$vendor_submission_date['emails'] = Yii::app()->input->post('emails');
			
			$vendor->updateVendor($vendor_submission_date);

			$location = new Location();
			$location->getOne(array('location_id' => Yii::app()->input->post('location_id')));
			$location->rs = array();
			$location->rs['location_name'] = Yii::app()->input->post('location_name');
			$location->rs['contact_person'] = Yii::app()->input->post('contact_person');
			$location->rs['ship_address'] = Yii::app()->input->post('ship_address');
			$location->rs['ship_city'] = Yii::app()->input->post('ship_city');
			$location->rs['ship_state'] = Yii::app()->input->post('ship_state');
			$location->rs['ship_zip_code'] = Yii::app()->input->post('ship_zip_code');
			$location->rs['phone'] = Yii::app()->input->post('phone');
			$location->rs['email'] = Yii::app()->input->post('email');
			$location->saveData();
			
		}

	}

	public function actionVendorLocationData() {
		$this->checklogin();
		$this->layout = 'main';

		$location_id = Yii::app()->input->post('location_id');
		$vendor_id = Yii::app()->input->post('vendor_id');

		$location = new Location();
		$location_contacts = $location->getLocationContactData($location_id);

		$vendor = new Vendor();
		$vendor_contacts = $vendor->getVendorContactData($vendor_id);

		echo json_encode(array_merge($vendor_contacts,$location_contacts));exit;
	}

	public function actionDeleteVendorFile()
	{
		// which order record the file relates to?
		$quote_id = Yii::app()->input->post('quote_id');
		$vendor_id = Yii::app()->input->post('vendor_id');
		$file_name = Yii::app()->input->post('file_name');

		// directory where this file resides
		$complete_file_name = 'uploads/orders/' . $quote_id . '/' . $vendor_id . '/' . $file_name;
		if (is_file($complete_file_name)) @unlink($complete_file_name);
		echo $complete_file_name;
	}

}
