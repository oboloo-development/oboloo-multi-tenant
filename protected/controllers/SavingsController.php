<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');
class SavingsController extends CommonController
{
	public $layout = 'main';
	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('quotes/list'));
	}

	
	public function actionList()
	{   
		error_reporting(0);
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		unset(Yii::app()->session['quick_vendor']);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'savings_list';
		if(!empty($_POST['clearsearch'])){
		  $company_data = new Company();
          $company_details = $company_data->getOne();
          $user_currency = Yii::app()->session['user_currency'] = $company_details['currency_id'];
    	}else{
    	  $user_currency = Yii::app()->input->post('user_currency');
    	}
		// and also currency preference by the user
		
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		
		// get list of all orders (with pagination of course)
		$saving = new Saving();
		// get orders as per filters applied
		$view_data     = array();
		$vendor_ids    = Yii::app()->input->post('vendor_id');
		$location_id   = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		
	    $saving_status = Yii::app()->input->post('status');
	    $view_data['saving_status_id'] = $saving_status;

		$category_id = Yii::app()->input->post('category_id');
        $subcategory_id = Yii::app()->input->post('subcategory_id');
		
		$due_date = Yii::app()->input->post('due_date');
		$view_data['due_date'] = $due_date;
    
		$db_from_date = "";
		if (!empty($due_date))
		{
		 if(FunctionManager::dateFormat()=="d/m/Y"){
       	   list($input_date, $input_month, $input_year) = explode("/", $due_date);
       	   $db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
       	 }else{
	       	 $db_from_date = date("Y-m-d",strtotime($due_date));
	     }
		 $view_data['due_date'] = $db_from_date;
		}

		$view_data['saving_status'] = $saving_status;
		$view_data['category_id'] = $category_id;
        $view_data['subcategory_id'] = $subcategory_id;

        if(!empty($_POST['report_from_date']) || !empty($_POST['report_to_date'])){
        	if(FunctionManager::dateFormat()=="d/m/Y"){
        		$chartDateFrom = date("Y-m-d", strtotime(strtr($_POST['report_from_date'], '/', '-')));
        		$chartDateTo   = date("Y-m-d", strtotime(strtr($_POST['report_to_date'], '/', '-')));
        	}else{
            	$chartDateFrom = date("Y-m-d", strtotime($_POST['report_from_date']));
            	$chartDateTo   = date("Y-m-d", strtotime($_POST['report_to_date']));

            }
		}else{
        	$chartDateFrom = date("Y-m-01",strtotime("-1 year"));
        	$chartDateTo   = date("Y-m-t",strtotime("+1 year"));
    	}

    	if(!empty($_POST['report_from_date']) || !empty($_POST['report_to_date'])){	
    	  if(FunctionManager::dateFormat()=="d/m/Y"){
    		$chartDateFromMonth = date("Y-m-d", strtotime(strtr($_POST['report_from_date'], '/', '-')));
    		$chartDateToMonth   = date("Y-m-d", strtotime(strtr($_POST['report_to_date'], '/', '-')));
    	  }else{
        	$chartDateFromMonth = date("Y-m-d", strtotime($_POST['report_from_date']));
        	$chartDateToMonth   = date("Y-m-d", strtotime($_POST['report_to_date']));
          }
		}else {
        	$chartDateFromMonth = date("Y-m-01",strtotime("-6 months"));
        	$chartDateToMonth = date("Y-m-t",strtotime("+6 months"));
    	}
    	
    	$location = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;
		$view_data['department_id'] = $department_id;


		$category = new Category();
        $view_data['categories'] = $category->getAll();
        if(!empty($category_id)){
        	$subcategory = new Subcategory();
			$view_data['subcategory'] =$subcategory->getAll(array('code'=>$category_id));
		}else{
			$view_data['subcategory'] = '';
		}

		$vendor = new Vendor();
		$view_data['vendors'] = $vendor->getAll(array('ORDER' => 'vendor_name'));
		$view_data['vendor_ids'] = $vendor_ids;
    	 
        $metrixArr = $saving->getMetrix($chartDateFrom, $chartDateTo);
        $view_data['metrix_1'] = $metrixArr['metrix_1'];
        $view_data['metrix_3'] = $metrixArr['metrix_3'];
        $view_data['metrix_2'] = $metrixArr['metrix_2'];
        $view_data['metrix_4'] = $metrixArr['metrix_4'];
        $chartListArr = $saving->getChartList($chartDateFromMonth, $chartDateToMonth);

       	$view_data['chart_1'] = $chartListArr['chart_1'];
       	$view_data['chart_2'] = $chartListArr['chart_2'];
       	$view_data['chart_3'] = $chartListArr['chart_3'];
       	
		$view_data['savings'] = $saving->getSavings($location_id, $department_id,$category_id, $subcategory_id, $saving_status, $db_from_date, $vendor_ids);

		$view_data['saving_status'] = $saving->getStatus();
	 
		$user = new User();
		$userData = $user->getOne(array('user_id'=>Yii::app()->session['user_id']));
		$view_data['saving_visit'] = $userData['saving_visit'];
		if($view_data['saving_visit'] !=1){
			$user->rs = array();
			$user->rs['user_id'] = $userData['user_id'];
			$user->rs['saving_visit'] = 1;
			$user->write();
		}

		$this->render('list', $view_data);
	}

	 public function actionListAjax(){
        error_reporting(0);

        $saving = new Saving();
        $industry_id = 0;
            if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id']))
                $industry_id = $_REQUEST['industry_id'];            

            $subindustry_id = 0;
            if (isset($_REQUEST['subindustry_id']) && !empty($_REQUEST['subindustry_id']))
                $subindustry_id = $_REQUEST['subindustry_id'];          

            $preferred_flag = -1;
            $supplier_status = '';
            if (isset($_REQUEST['preferred_flag'])) $preferred_flag = $_REQUEST['preferred_flag'];
            if (isset($_REQUEST['supplier_status'])) $supplier_status = $_REQUEST['supplier_status'];
                    

            set_time_limit(0);
            $total_vendors = 0;
            
            $search_for = "";
            if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value']))
            {
                $search_for = $_REQUEST['search']['value'];
                
            }
            
            $order_by = array();
            if(isset($_REQUEST['order']) && is_array($_REQUEST['order'])){
              $order_by = $_REQUEST['order'];
            }else{
              $order_by[1] = 'contract_title';
            }
            
            $location_id = Yii::app()->input->post('location_id');
            $department_id = Yii::app()->input->post('department_id');
            $category_id = Yii::app()->input->post('category_id');
            $subcategory_id = Yii::app()->input->post('subcategory_id');
            $saving_status = Yii::app()->input->post('status');
            $due_date = Yii::app()->input->post('due_date');
            $due_date_to = Yii::app()->input->post('due_date_to');
            

            
            $savings = $saving->getSavingsAjax(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by,$location_id, $department_id, $category_id, $subcategory_id,$saving_status,$due_date,$due_date_to);

            $dataArr = array();
            $i=1;
            $subdomain = FunctionManager::subdomainByUrl();
            foreach ($savings as $value)
            {   
               $style="";
				 
				if(strtolower(trim($value['saving_status']))=="completed"){
					$style ="style='background:#6cce47;border-color:#6cce47'";
				}else if(strtolower(trim($value['saving_status']))=="in progress"){
					$style ="style='background:#f0ad4e;border-color:#f0ad4e'";
				}else if(strtolower(trim($value['saving_status']))=="not started"){
					$style ="style='background:#fb5a5a !important ;border-color:#fb5a5a !important'";
				}	
				$imgFullPath = '';
                if(!empty($value['profile_img'])){
                    $imgFullPath = Yii::app()->baseUrl . "/../images/".$subdomain."/users/".$value['user_id']."/".$value['profile_img'];

                }else if(!empty($value['user_id'])){
                  $imgFullPath = CommonFunction::userTextAvatar($value['full_name']);
                }
                
				if(!empty($imgFullPath)){
                 $imgFullPath = CHtml::image($imgFullPath);
                }
                $current_data = array();
                $current_data[] = '<a href="'.AppUrl::bicesUrl('savings/edit/' . $value['id']).'">
                            <button class="btn btn-sm btn-success view-btn" >View/Edit
                            </button>
                        </a>';
				$profileImg = '<div class="image-container">'.$imgFullPath.' </i><div class="profile-username"> '. $value['user_name'].' </div> </div> ';

                $current_data[] = $value['id'];
                $current_data[] = $value['title'];
                $current_data[] = $value['created_at'] !='0000-00-00 00:00:00'?date(FunctionManager::dateFormat(), strtotime($value['created_at'])):'';
                $current_data[] = $value['due_date'] !='0000-00-00 00:00:00'?date(FunctionManager::dateFormat(), strtotime($value['due_date'])):'';
                $current_data[] = $profileImg;
                $current_data[] = $value['currency'];
                $current_data[] = $value['total_base_spend']>0?$value['currency_symbol'].number_format($value['total_base_spend'],0,"",""):'';
                $current_data[] = $value['total_project_saving']>0?$value['currency_symbol'].number_format($value['total_project_saving'],0,"",""):'';
                $current_data[] = $value['total_project_saving_perc']>0?number_format($value['total_project_saving_perc'],0,"","").'%':'';
                $current_data[] = $value['realised_saving']>0?$value['currency_symbol'].number_format($value['realised_saving'],0,"",""):'';
                $current_data[] = $value['realised_saving_perc']>0?number_format($value['realised_saving_perc'],0,"","").'%':'';
                $current_data[] = '<button type="submit" class="btn btn-success" '.$style.'>'.$value['saving_status'].'</button>';

                $dataArr[]  = $current_data;
                
            }

            $database_values_total = $saving->getSavingsAjax(0, 'none','none' , $search_for, $order_by,$location_id, $department_id, $category_id, $subcategory_id,$saving_status,$due_date,$due_date_to);;
            $total_savings=count($database_values_total);
            
            $json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $total_savings,
                "recordsFiltered" => $total_savings,
                "data"            => $dataArr
            );
            echo json_encode($json_data);exit;  

     }

	public function actionEdit($saving_id = 0)
	{

		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'savings_list';

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		error_reporting(0);
		$currency_rate = new CurrencyRate();
		// if form was submitted ... first save the data to the database
         
		if(isset($_POST['title']))
		{
           	if(!empty($_POST['new_vendor_name'])){
       		 $quickaddedVendorArr = array_keys(Yii::app()->session['quick_vendor']);
       		 $vendorID = !empty($quickaddedVendorArr[0])?$quickaddedVendorArr[0]:'0';
           	}else{
           	 $vendorID = !empty($_POST['vendor_id']) ? $_POST['vendor_id']:'0';
           	}

           	$currencyID = !empty($_POST['currency_id']) ? $_POST['currency_id']:'0';
           	if(!empty($currencyID)){
           	 $currency = $currency_rate->getOne(array('id' => $currencyID));
           	 $currencyRate = $currency['rate'];
           	}else { $currencyRate = 0;}

           	$userID = Yii::app()->session['user_id'];
            $updateSaving = !empty($_POST['update_saving'])?addslashes($_POST['update_saving']):'';
            $saving_id = !empty($_POST['saving_id'])?$_POST['saving_id']:$saving_id;
           	$title = !empty($_POST['title'])?addslashes($_POST['title']):'';
           	$savingDisc = !empty($_POST['saving_desc'])? $_POST['saving_desc']:'';
           	$status = !empty($_POST['status'])?$_POST['status']:0;
           	$categoryID = !empty($_POST['category_id'])?$_POST['category_id']:'0';
           	$subcategoryID = !empty($_POST['subcategory_id'])?$_POST['subcategory_id']:'0';
           	$approver_id = !empty($_POST['approver_id'])?$_POST['approver_id']:0;
           	$approver_status = !empty($_POST['approver_id'])?'In Review':'';
           	$locationID = !empty($_POST['location_id'])?$_POST['location_id']:'';
           	$departmentID = !empty($_POST['department_id'])?$_POST['department_id']:'0';
           	$contractID = !empty($_POST['contract_id'])?$_POST['contract_id']:'0';
           	$notes = !empty($_POST['notes'])?addslashes($_POST['notes']):'';
           	$totalProjectSaving = !empty($_POST['total_project_saving'])?$_POST['total_project_saving']:'0';
           	$totalProjectSavingPerc = !empty($_POST['total_project_saving_perc'])?$_POST['total_project_saving_perc']:'0';
           	$realisedSaving = !empty($_POST['realised_saving'])?$_POST['realised_saving']:'0';
           	$realisedSavingPerc = !empty($_POST['realised_saving_perc'])?$_POST['realised_saving_perc']:'0';
			$startDate = !empty($_POST['start_date'])?$_POST['start_date']:'0000-00-00 ';
			if(FunctionManager::dateFormat()=="d/m/Y"){
			 $convFormatDue = strtr($_POST['saving_due_date'], '/', '-');
			 $convFormatCreate = strtr($_POST['created_at'], '/', '-');
			}else{
			 $convFormatDue = $_POST['saving_due_date'];
			 $convFormatCreate = $_POST['created_at'];
			}

			$savingDueDate = !empty($_POST['saving_due_date'])?date("Y-m-d ", strtotime($convFormatDue)):'0000-00-00 00:00:00';
		    $createdAt = !empty($_POST['created_at'])?date("Y-m-d", strtotime($convFormatCreate)):date("Y-m-d");

           	$userName = !empty($_POST['user_name'])?$_POST['user_name']:Yii::app()->session['full_name'];

  			// IN update case this function call for saving logs 
			if(!empty($saving_id)){
			 $this->savingLogsInsert($saving_id);	
			}
			
			if(!empty($approver_id)){
			 $userObj = new User;
			 $approverRecord = $userObj->getOne(array('user_id'=>$approver_id));
			 $approver_name = $approverRecord['full_name'];
			}else{
			$approver_name = '';
			}

			$saving = new Saving;
			$saving->rs = array();
			$saving->rs['id'] = $saving_id;
			if($saving_id<1){
				$saving->rs['user_id'] = $userID;
				$saving->rs['user_name'] = $userName;
			}
			$saving->rs['title'] = $title;
			$saving->rs['saving_desc'] = $savingDisc;
			$saving->rs['status'] = $status;
			$saving->rs['approver_id'] = $approver_id;
            $saving->rs['approver_name'] = $approver_name;
            $saving->rs['approver_status'] = $approver_status;
			$saving->rs['currency_id'] = $currencyID;
			$saving->rs['currency_rate'] = $currencyRate;
			$saving->rs['category_id'] = $categoryID;
			$saving->rs['subcategory_id'] = $subcategoryID;
			$saving->rs['saving_archive'] ='no';
			$saving->rs['location_id'] = $locationID;
			$saving->rs['department_id'] = $departmentID;
			$saving->rs['vendor_id'] = $vendorID;
			$saving->rs['notes'] = $notes;
			$saving->rs['total_project_saving'] = $totalProjectSaving;
			$saving->rs['total_project_saving_perc'] = $totalProjectSavingPerc;
			$saving->rs['realised_saving'] = $realisedSaving;
			//$saving->rs['realised_saving_perc'] = $realisedSavingPerc;
			$saving->rs['start_date'] = $startDate;
			$saving->rs['due_date'] = $savingDueDate;
			$saving->rs['contract_id'] = $contractID;
			$saving->rs['created_at'] = $createdAt;
			$saving->rs['updated_at'] = $createdAt;
			$saving->write();
			 
			if(!empty($saving->rs['id'])){
				if(empty($updateSaving)){ 
				$mesg = empty($saving_id)?'Saving record created successfully.':'Saving record updated successfully';
				if(empty($saving_id)){
					$saving->saveRecordCurrency($saving->rs['id']);
				}
				$saving_id = $saving->rs['id'];
				$savingMilestones = Yii::app()->session['saving_milestones'];
				
				if(!empty($savingMilestones)){
					$createdAt = date("Y-m-d");
					foreach($savingMilestones as $key=>$value){
						$dueDate = $value['due_date'];

						if(!empty($dueDate) && FunctionManager::dateFormat()=="d/m/Y"){
							$dueDate = strtr($dueDate, '/', '-');
						} 
						$dueDate = !empty($dueDate)?date("Y-m-d", strtotime($dueDate)):'0000-00-00 00:00:00';
						
						$savingMilestone = new SavingMilestone;
						$savingMilestone->rs = array();
						$savingMilestone->rs['id'] = 0;
						$savingMilestone->rs['saving_id'] = $saving_id;
						$savingMilestone->rs['title'] = addslashes($value['title']);
						$savingMilestone->rs['due_date'] = $dueDate;
						$savingMilestone->rs['base_line_spend'] = $value['base_line_spend'];
						$savingMilestone->rs['cost_reduction'] = $value['cost_reduction'];
						$savingMilestone->rs['cost_avoidance'] = $value['cost_avoidance'];
						$savingMilestone->rs['created_at'] = $createdAt;
						$savingMilestone->write();
        			}
				}
				FunctionManager::savingCalculationSave($saving_id);
				}
			}

		    if(!empty($_POST['quote_id'])){
			    $quote = new Quote;
			    $quote->rs = array();
			    $quote->rs['quote_id'] = $_POST['quote_id'];
			    $quote->rs['awarded_saving_id'] = $saving_id;
			    $quote->rs['awarded_saving_by'] = Yii::app()->session['user_id'];
			    $quote->rs['awarded_saving_datetime'] = date("Y-m-d H:i:s");
			    $quote->write();
			    Yii::app()->user->setFlash('success', $mesg);
			    if(!empty($_POST['redirect_url']) && $_POST['redirect_url']=='edit'){
					$this->redirect(AppUrl::bicesUrl('savings/edit/'.$saving_id));
				}else{
			    	$this->redirect(AppUrl::bicesUrl('quotes/edit/'.$_POST['quote_id']));
				}
			}

			

			Yii::app()->user->setFlash('saving_message', $mesg);
			if(!empty($_POST['redirect_url']) && $_POST['redirect_url']=='edit'){
				$action = "edit/".$saving_id; 
			}else{
				$action = "list/"; 
			}

			$this->redirect(AppUrl::bicesUrl('savings/'.$action));
		}


		// get data for the selected vendor
		$view_data = array('saving_id' => $saving_id);
		$saving = new Saving;
		$savingRecord = $saving->getOne(array('id' => $saving_id));
		$view_data['saving'] = $savingRecord;
	   // need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));
		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));
		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));
		// and also product and supplier information for the quote
		$view_data['milestones'] = $saving->getMilestones($saving_id);
		$view_data['saving_status'] = $saving->getStatus();
		$category = new Category();
        $view_data['categories'] = $category->getAll();

        $savingLogs = new SavingLog();
		$view_data['savingLogs'] = $savingLogs->getSavingLogs($saving_id); 
   
        $view_data['currency_rate']  = $currency_rate->getAllCurrencyRate();
        $view_data['currency_rate_detail'] = $currency_rate->getOne(array('id' => $savingRecord['currency_id']));
        $vendor = new Vendor();
        $view_data['vendor']  = $vendor->getOne(array('vendor_id' => $savingRecord['vendor_id']));
        $savingStatus = FunctionManager::savingStatusIgnore();
        // Chart
        $sql = "SELECT m.title,sum(m.cost_avoidance) as p_avoidance,
		sum(m.cost_reduction) as p_reduction,sum(m.realised_cost_avoidance) as r_avoidance,
		sum(m.realised_cost_reduction) as r_reduction FROM saving_milestone m where m.saving_id=".$saving_id;
      	$milestone = Yii::app()->db->createCommand($sql)->queryRow();
		$avoidanceArr = array($milestone['p_avoidance'],$milestone['r_avoidance']);
		$redutionArr  = array($milestone['p_reduction'],$milestone['r_reduction']);
		$view_data['avoidance_chart2'] = $avoidanceArr;
		$view_data['reduction_chart2'] = $redutionArr;

	    $sql = "SELECT m.title,m.cost_avoidance+m.cost_reduction as total_project_saving,
		m.total_realised_saving, m.status as status  FROM saving_milestone m
		where m.saving_id=".$saving_id." order by total_project_saving desc";

      	$milestone = Yii::app()->db->createCommand($sql)->query()->readAll();
		$projectSavingArr = $realisedSavingArr = $milestoneArr = array();
		
		foreach($milestone as $value){
			 $projectSavingArr[]  = $value['total_project_saving'];
			if($value['status'] == 'Completed'){
			 $realisedSavingArr[] = $value['total_realised_saving']; 
			}else{
			 $realisedSavingArr[] = 0;	
			}
			$milestoneArr[] = $value['title'];
		}

		$view_data['project_saving_chart1'] = $projectSavingArr;
		$view_data['realised_saving_chart1'] = $realisedSavingArr;
		$view_data['milestone_chart1'] = $milestoneArr;
		$view_data['totalBaseLineSpend'] = CommonFunction::getBaseLineSpend($saving_id);
		$view_data['totalBaselineSpendRealizedSaving'] = CommonFunction::totalBaselineSpendRealizedSaving($saving_id);
		$view_data['milestones']  = $saving->getMilestones($saving_id);
		$view_data['savingRecord']= $saving->getOne(array('id' => $saving_id));
	

        // Chart
		$this->render('edit', $view_data);
		unset(Yii::app()->session['quick_vendor']);
	}

	public function actionCreateByModal(){
       // get data for the selected vendor
 
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'quotes_edit';
        unset(Yii::app()->session['saving_milestones']);
		unset(Yii::app()->session['quick_vendor']);
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		$view_data = array();
		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$user = new User();
		$view_data['users'] = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

		$currency_rate = new CurrencyRate();
        $view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

        $saving = new Saving();
        $view_data['saving_status'] = $saving->getStatus();

        $category = new Category();
        $view_data['categories'] = $category->getAll(array('soft_deleted' => "0"));
        $this->renderPartial('create_saving_modal', $view_data);
    }

	public function actionGetVendorDetails()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		// which vendor details are needed?
		$vendor_ids = Yii::app()->input->post('vendor_ids');
		if(!empty($vendor_ids) && !empty(Yii::app()->input->post('quote_id')))
		{   $quote_id = Yii::app()->input->post('quote_id');
			$quote = new Quote();
			$quoteVendor = $quote->getQuoteVendorInfo($vendor_ids, $quote_id);
			if(!empty($quoteVendor)){
				echo json_encode($quoteVendor);
			}else{
				$vendor = new Vendor();
				echo json_encode($vendor->getData(array('vendor_id IN' => explode(",", $vendor_ids))));
			}
		}
		else if(!empty($vendor_ids))
		{
			$vendor = new Vendor();
			echo json_encode($vendor->getData(array('vendor_id IN' => explode(",", $vendor_ids))));
		}
		else echo json_encode(array());
	}

	public function actionAddMileStone(){  
       date_default_timezone_set(Yii::app()->params['timeZone']);
       error_reporting(0);
      
       $savingMilestones = Yii::app()->session['saving_milestones'];
       $title = !empty($_POST['title'])?$_POST['title']:'';
       //$dueDate = !empty($_POST['due_date'])?date('Y-m-d',strtotime($_POST['due_date'])):'';
      
       $dueDate = $_POST['due_date'];
       $dueDate = !empty($dueDate)?$dueDate:'';
       // $savingsStartDate = !empty($savingsStartDate)?$savingsStartDate:'';
       $savingDuration = !empty($_POST['saving_duration'])?round($_POST['saving_duration']):'0';
       $savingsdueDate = !empty($_POST['miletone_due_date'])?$_POST['miletone_due_date']:'';
       $baseLineSpend = !empty($_POST['base_line_spend'])?$_POST['base_line_spend']:'0';
       $costReduction = !empty($_POST['cost_reduction'])?$_POST['cost_reduction']:'0';
       $costAvoidance = !empty($_POST['cost_avoidance'])?$_POST['cost_avoidance']:'0';
       $durationMothly = !empty($_POST['duration_monthly'])?$_POST['duration_monthly']:'';
       $fieldArr = array();
       $fieldArr['title'] = $title;
       $fieldArr['due_date'] = $dueDate;
       $fieldArr['savings_due_date'] = $savingsdueDate;
       // $fieldArr['start_date'] = $savingsStartDate;
       $fieldArr['saving_duration'] = $savingDuration;
       $fieldArr['saving_monthly'] = $durationMothly;
       $fieldArr['base_line_spend'] = $baseLineSpend;
       $fieldArr['cost_reduction'] = $costReduction;
       $fieldArr['cost_avoidance'] = $costAvoidance;
       $savingMilestones[] = $fieldArr;
       Yii::app()->session['saving_milestones'] = $savingMilestones;
       
			
       $exp = "<table class='table table-striped table-bordered' style='width: 100%;'>
			    <tr>
			   	 <th>Milestone Name</th>
				 <th>Due Date</th>
				 <th>Baseline Spend</th>
				 <th>Cost Reduction</th>
				 <th>Cost Avoidance</th>
       			</tr>";
       	$projectSaving = $projectSavingPer = $baseLine = 0;
       	$dueDate = '';
       	$savingsdueDate = '';
        foreach($savingMilestones as $key=>$value){
        	$dueDate = $value['due_date'];
        	$savingsdueDate = $value['savings_due_date'];
        	$disabled = FunctionManager::sandbox();
			$projectSaving += $value['cost_reduction']+$value['cost_avoidance'];
    		$baseLine += $value['base_line_spend'];
    		$baseLine  = $baseLine==0 ? 1:$baseLine;
    		$projectSavingPer= $projectSaving/$baseLine;
			
			$savingDuration = "";
        	$exp .="<tr>
					 <td>".$value['title']."</td>
					 <td '".$disabled."'>".$value['due_date']."</td>
					 <td>".$value["base_line_spend"]."</td>
					 <td>".$value['cost_reduction']."</td>
					 <td>".$value["cost_avoidance"]."</td>
					</tr>";
        }
        $exp .="</table>";

 
       $data = array();
       $data['mesg'] =1;
       $data['record_list'] = $exp;
       $data['total_project_saving'] = $projectSaving;
       $data['total_project_saving_perc'] = $projectSavingPer * 100;
       $data['due_date'] = $dueDate;
       $data['savings_due_date'] = $savingsdueDate;
       echo json_encode($data);exit;

    }

    public function actionDeleteMilestone()
    {    
    	$key = $_POST['recordKey'];
		$savingMilestones = Yii::app()->session['saving_milestones'];
		$disabled = '';

		unset($savingMilestones[$key]);
		Yii::app()->session['saving_milestones'] = $savingMilestones;

		$exp = "<table class='table table-striped table-bordered' style='width: 100%;'>
       				<tr>
       				<th>Milestone Name</th>
       				<th>Due Date</th>
       				<th>Baseline Spend</th>
       				<th>Cost Reduction</th>
       				<th>Cost Avoidance</th>
       				<th>Action</th>
       		</tr>";
       	$dueDate = '';
       	$projectSaving = $projectSavingPer = 0;

        foreach($savingMilestones as $key=>$value){
        	if(empty($value['cost_reduction'])){
        		$value['cost_reduction'] = 0;
        	}
        	if(empty($value['cost_avoidance'])){
        		$value['cost_avoidance'] = 0;
        		
        	}
        	if(empty($value['base_line_spend'])){
        		$value['base_line_spend'] = 1;
        		
        	}
        	$projectSaving += $value['cost_reduction']+$value['cost_avoidance'];
        	$projectSavingPer = $projectSaving/$value['base_line_spend'];
        	$dueDate = $value['due_date'];

        	$deleteFunc = 'deleteMilestone("'.$key.'")';
			$mileStoneTitle = "<td>".$value['title']."</td>";
        	$exp .="<tr>".$mileStoneTitle."<td>".$value['due_date']."</td><td>".$value['base_line_spend']."</td><td>".$value['cost_reduction']."</td><td>".$value['cost_avoidance']."</td><td><a style='cursor: pointer; padding: 5px;' onclick='".$deleteFunc."' '". $disabled ."' ><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a></td></tr>";
        }
        $exp .="</table>";

       $data = array();
       $data['mesg'] =1;
       $data['record_list'] =$exp;
       $data['total_project_saving'] = $projectSaving;
       $data['total_project_saving_perc'] = $projectSavingPer * 100; 
       $data['due_date'] = $dueDate;
       echo json_encode($data);exit;
    }

    public function actionDeleteSavingMilestone()
    { 
    	$key = $_POST['recordKey'];
    	
    	$sql = "select saving_id from  saving_milestone where id=".$key;
    	$record = Yii::app()->db->createCommand($sql)->queryRow();
    	$savingID =  $record['saving_id'];

    	$sql = "delete from  saving_milestone where id=".$key;
    	$deleteRecorder = Yii::app()->db->createCommand($sql)->execute();
    	if(!empty($deleteRecorder)){
    		$mesg = 1;
    	}else{
    		$mesg = 2;
    	}
    	FunctionManager::savingCalculationSave($savingID);
       $data = array();
       $data['mesg'] =$mesg;
       echo json_encode($data);exit;
    }

    public function actionDeletePlan()
    { 
    	$key = $_POST['recordKey'];
    	
    	$sql = "select saving_id from  milestone_field where id=".$key;
    	$record = Yii::app()->db->createCommand($sql)->queryRow();

    	$sql = "delete from  milestone_field where id=".$key;
    	$deleteRecorder = Yii::app()->db->createCommand($sql)->execute();
    	if(!empty($deleteRecorder)){
    		$mesg = 1;
    	}else{
    		$mesg = 2;
    	}
    	FunctionManager::savingCalculationSave($record['saving_id']);
       $data = array();
       $data['mesg'] =$mesg;
       echo json_encode($data);exit;
    }

    public function actionAddSavingMilestone(){ 
    	 
    	$title = !empty($_POST['title'])?$_POST['title']:'';
		$dueDate = $_POST['due_date'];
		if(!empty($dueDate) && FunctionManager::dateFormat()=="d/m/Y"){
			$dueDate = strtr($dueDate, '/', '-');
		}
		$dueDate = !empty($dueDate)?date("Y-m-d", strtotime($dueDate)):'0000-00-00 ';
		$baseLineSpend = !empty($_POST['base_line_spend'])?$_POST['base_line_spend']:'0';
		$costReduction = !empty($_POST['cost_reduction'])?$_POST['cost_reduction']:'0';
		$costAvoidance = !empty($_POST['cost_avoidance'])?$_POST['cost_avoidance']:'0';

		$notes = !empty($_POST['notes'])?addslashes($_POST['notes']):'';
		$saving_id = $_POST['saving_id'];
		$createdAt = date("Y-m-d");
		 
		$savingMilestone = new SavingMilestone;
		$savingMilestone->rs = array();
		$savingMilestone->rs['id'] = 0;
		$savingMilestone->rs['saving_id'] = $saving_id;
		$savingMilestone->rs['title'] = $title;
		$savingMilestone->rs['due_date'] = $dueDate;
		$savingMilestone->rs['base_line_spend'] = $baseLineSpend;
		$savingMilestone->rs['cost_reduction'] = $costReduction;
		$savingMilestone->rs['cost_avoidance'] = $costAvoidance;
		$savingMilestone->rs['notes'] = $notes;
		$savingMilestone->rs['created_at'] = $createdAt;

		$savingMilestone->write();
		if($savingMilestone->rs['id']>0){
			$mesg = "1";
			FunctionManager::savingCalculationSave($saving_id);
			Yii::app()->user->setFlash('saving_message', 'Milestone added successfully.');
		}else{
			$mesg = "2";
			Yii::app()->user->setFlash('saving_message', 'Problem occured while saving Milestone, try again.');
		}

		//Start: insert Log in ass saving milestone
			$commentLog ='';
			if(!empty($title)){
	          $commentLog .= '<b>Milestone Title:</b> <span class="title-text"><b>'.  $title. '</b></span><br/>';
	        }
			if(!empty($baseLineSpend)){
	          $commentLog .= '<b>Baseline Spend:</b> <span class="title-text"><b>'.  $baseLineSpend. '</b></span><br/>';
	        }
			if(!empty($costReduction)){
	          $commentLog .= '<b>Projected Cost Reduction:</b> <span class="title-text"><b>'.  $costReduction. '</b></span><br/>';
	        }
			if(!empty($costAvoidance)){
	          $commentLog .= '<b>Projected Cost Avoidance:</b> <span class="title-text"><b>'.  $costAvoidance. '</b></span><br/>';
	        }
	     	
            if(!empty($commentLog)){
				$log = new SavingLog();
				$log->rs = array();
				$log->rs['saving_id'] = $saving_id;
				$log->rs['user_id'] = Yii::app()->session['user_id'];
				$log->rs['comment'] = !empty($commentLog) ? $commentLog:"";
				$log->rs['created_datetime'] = date('Y-m-d H:i:s');
				$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
				$log->write();
        	} 
		//END: insert Log in ass saving milestone


		$data = array();
		$data['mesg'] = $mesg;
		$data['milestoneUrl'] = AppUrl::bicesUrl('savings/edit/'.$saving_id, array('tab' => 'milestone'));
		echo json_encode($data);exit;
	}

	public function actionEditMilestone(){
		$id = $_POST['milestone_edit_id'];
    	$title = !empty($_POST['edit_milestone_title'])?$_POST['edit_milestone_title']:'';
		$dueDate = $_POST['edit_due_date'];
		if(!empty($dueDate) && FunctionManager::dateFormat()=="d/m/Y"){
			$dueDate = strtr($dueDate, '/', '-');
		} 

		//echo "<pre>"; print_r($_POST); echo "</pre>"; exit; 

		$dueDate = !empty($dueDate) ? date("Y-m-d", strtotime($dueDate)) : '0000-00-00';
		$baseLineSpend = !empty($_POST['edit_base_line_spend'])?$_POST['edit_base_line_spend']:'0';
		$costReduction = !empty($_POST['edit_cost_reduction'])?$_POST['edit_cost_reduction']:'0';
		$costAvoidance = !empty($_POST['edit_cost_avoidance'])?$_POST['edit_cost_avoidance']:'0';
		$realisedCostReduction = !empty($_POST['edit_realised_cost_reduction'])?$_POST['edit_realised_cost_reduction']:'0';
		$realisedCostAvoidance = !empty($_POST['edit_realised_cost_avoidance'])?$_POST['edit_realised_cost_avoidance']:'0';
		$notes = !empty($_POST['edit_milestone_notes'])?$_POST['edit_milestone_notes']:'';
		$saving_id = $_POST['edit_saving_id'];
		$createdAt = date("Y-m-d");

		$savingMilestone = new SavingMilestone;
		$savingMilestone->rs = array();
		$savingMilestone->rs['id'] = $id;
		$savingMilestone->rs['saving_id'] = $saving_id;
		$savingMilestone->rs['due_date'] = $dueDate;
		$savingMilestone->rs['base_line_spend'] = $baseLineSpend;
		$savingMilestone->rs['cost_reduction'] = $costReduction;
		$savingMilestone->rs['cost_avoidance'] = $costAvoidance;
		$savingMilestone->rs['notes'] = addslashes($notes);
		$savingMilestone->write();
		if($savingMilestone->rs['id']>0){
			$milestone_id = $savingMilestone->rs['id'];
			$saving_id    = $savingMilestone->rs['saving_id'];
			$mesg = "1";
			if(!empty($savingMilestone->rs['status']) && $savingMilestone->rs['status'] == 'Completed'){
			 // FunctionManager::savingCalculationSaveWhenMilestoneEdit($saving_id);
			}
			// it's will update on all status of milstone
			FunctionManager::savingCalculationSaveWhenMilestoneEdit($saving_id);
			
		}

		Yii::app()->user->setFlash('saving_message','Milestone updated successfully');
		$this->redirect(array('savings/edit/'.$saving_id));
	}
 
          
	public function actionMilestoneComplete(){ 
		$id = $_POST['complete_milestone_edit_id'];
		$savingMilestone = new SavingMilestone;
    	$oldMilestone = $savingMilestone->getOne(array('id' => $id));
		//  echo "<pre>"; print_r($_POST); echo "</pre>"; exit;
		$realisedReduction = !empty($_POST['total_realised_cost_reduction']) ? $_POST['total_realised_cost_reduction']:'0';
		$realiseAvoidance  = !empty($_POST['total_realised_cost_avoidance']) ? $_POST['total_realised_cost_avoidance']:'0';
		$totalRealisedSaving = !empty($_POST['total_realised_saving']) ? $_POST['total_realised_saving']:'0';
		$totalProjectRealisedPerc = !empty($_POST['total_project_realised_perc']) ? $_POST['total_project_realised_perc']:'0';
		
		$spendline = $oldMilestone['base_line_spend']>0?$oldMilestone['base_line_spend']:1;

		$saving_id = $_POST['complete_edit_saving_id'];
		$completedAt = date("Y-m-d");

		$savingMilestone = new SavingMilestone;
		$savingMilestone->rs = array();
		$savingMilestone->rs['id'] = $id;
		$savingMilestone->rs['saving_id'] = $saving_id;

		$savingMilestone->rs['realised_cost_reduction'] = $realisedReduction;
		$savingMilestone->rs['realised_cost_avoidance'] = $realiseAvoidance;
		$savingMilestone->rs['total_realised_saving'] = $totalRealisedSaving;
		$savingMilestone->rs['total_realised_saving_perc'] = $totalProjectRealisedPerc;
 
		$savingMilestone->rs['completed_by_id'] = Yii::app()->session['user_id'];
		$savingMilestone->rs['completed_by_name'] = Yii::app()->session['full_name'];
		$savingMilestone->rs['completed_date'] = Yii::app()->session['full_name'];
		$savingMilestone->rs['completed_date'] = $completedAt;
		$savingMilestone->rs['status'] = 'Completed';
		$savingMilestone->write();
		if($savingMilestone->rs['id']>0){
			FunctionManager::savingCalculationSave($saving_id);
		}
	
		Yii::app()->user->setFlash('saving_message','Milestone completed successfully');
		$this->redirect(array('savings/edit/'.$saving_id));
	}

	

	public function actionSavingCalculationFields(){
		$savingID = $_POST['saving_id'];
		$sql = "SELECT s.total_project_saving,s.total_project_saving/sum(sm.base_line_spend)*100 as total_project_saving_perc,s.realised_saving,s.realised_saving/sum(sm.base_line_spend)*100 as realised_saving_perc,s.due_date FROM savings s
		left join saving_milestone as sm on s.id = sm.saving_id  where id=".$savingID;
		$reader = Yii::app()->db->createCommand($sql)->queryRow();
		if(!empty($reader)){
			$projectSaving = $reader['total_project_saving'] !="0.00"?$reader['total_project_saving']:"";
			$projectSavingPer = $reader['total_project_saving_perc'] !="0.00"?$reader['total_project_saving_perc']:"";
			$realisedSaving = $reader['realised_saving'] !="0.00"?$reader['realised_saving']:"";
			$realisedSavingPer = $reader['realised_saving_perc'] !="0.00"?$reader['realised_saving_perc']:"";
			$dueDate = $reader['due_date'] !="0000-00-00"?date(FunctionManager::dateFormat(),strtotime($reader['due_date'])):"";
		}
		$data = array();
		$data['total_project_saving'] = $projectSaving;
		$data['total_project_saving_perc'] = $projectSavingPer;
		$data['realised_saving'] = $realisedSaving;
		$data['realised_saving_perc'] = $realisedSavingPer;
		$data['due_date'] = $dueDate;
		echo json_encode($data);exit;
    }

    public function actionSavingChart1(){
		$savingID = $_POST['saving_id'];
		$sql = "SELECT title,cost_avoidance+cost_reduction as total_project_saving,total_realised_saving FROM saving_milestone m where m.saving_id=".$savingID." order by total_project_saving desc";

      	$milestone = Yii::app()->db->createCommand($sql)->query()->readAll();
		$data = array();
		$projectSavingArr = $realisedSavingArr = $milestoneArr = array();

		foreach($milestone as $value){
			$projectSavingArr[] = $value['total_project_saving'];
			$realisedSavingArr[] = $value['total_realised_saving'];
			//$milestoneArr[] = explode(" ",$value['title']);
			$milestoneArr[] = $value['title'];
		}
		$data['project_saving'] = $projectSavingArr;
		$data['realised_saving'] = $realisedSavingArr;
		$data['milestone'] = $milestoneArr;

		echo json_encode($data);exit;
    }

    public function actionSavingChart2(){
		$savingID = $_POST['saving_id'];
		$sql = "SELECT title,sum(cost_avoidance) as p_avoidance,sum(cost_reduction) as p_reduction,sum(realised_cost_avoidance) as r_avoidance,sum(realised_cost_reduction) as r_reduction FROM saving_milestone m where m.saving_id=".$savingID;
      	$milestone = Yii::app()->db->createCommand($sql)->queryRow();
		$data = array();
		 
		$avoidanceArr = array($milestone['p_avoidance'],$milestone['r_avoidance']);
		$redutionArr = array($milestone['p_reduction'],$milestone['r_reduction']);
		 
		$data['avoidance'] = $avoidanceArr;
		$data['reduction'] = $redutionArr;
		echo json_encode($data,JSON_NUMERIC_CHECK);exit;
    }
		
	public function actionDurationValue(){
		$duration = isset($_POST['duration']) ? $_POST['duration'] : "";
		$duration = $duration-1;
		$totalPlanned = isset($_POST['cost_reduction']) ? $_POST['cost_reduction'] : "0";
		if(FunctionManager::dateFormat()=="d/m/Y"){
			$startDate = strtr($_POST['start_date'], '/', '-');
			$startDate = strtr($_POST['start_date'], '/', '-');
		}else{
			$startDate = $_POST['start_date'];
		}

		$endDate = date(FunctionManager::dateFormat(),strtotime($startDate . "+$duration months"));
		$startDateMonth =  date("m",strtotime($startDate)); //month
		 
		$durationNumber = $startDateMonth+$duration;
		$perDurationPlanned = round($totalPlanned/($duration+1));

		$durationList = '<br /><div class="col-md-6 col-xs-12" style="text-align: center;"><label class="control-label">Amount</label></div><div class="clearfix"></div><br />';
		$j=1;
		for($i=0;$i<=$duration;$i++){
		   /*$showMonth =  date("Y-$i-d");
		   $showMonth =  date("M-y",strtotime($showMonth));
		   $showMonthIndex =  date("$i-Y");*/
		   $showMonth =  date("M-y",strtotime($startDate . "+$i months"));
		   $showMonthIndex =  date("m-y",strtotime($startDate . "+$i months"));
		   $durationList .= '
		   <div class="col-md-2 col-xs-12">
		   <label class="control-label">'.$showMonth.'</label>
		   </div>
		   <div class="col-md-4 col-xs-12"><input class="form-control notranslate milestone-plann-duration" type="text" name="'.$showMonthIndex.'" value="'.$perDurationPlanned.'">
		   </div><div class="clearfix"></div><br />';
		   $j++;
		}
		/*$durationList = 
        '<label class="control-label">Amount</label><br>
        <label class="control-label">Apr-22</label><input type="" name=""><br>
        <label class="control-label">Apr-22</label><input type="" name=""><br>
        <label class="control-label">Apr-22</label><input type="" name=""><br>
        <label class="control-label">Apr-22</label><input type="" name=""><br>
        <label class="control-label">Apr-22</label><input type="" name="">';*/

      echo json_encode(array(
            	'durationList' => $durationList,
      			'endDate' => $endDate,
      			//'totalPlanned' => $totalPlanned
      			)
  			);

	}

	public function actionPlannedEdit(){
		$edit_planned_id = $_POST['edit_planned_id'];
		$edit_planned_saving_id = $_POST['edit_planned_saving_id'];
		$edit_planned_milestone_id = $_POST['edit_planned_milestone_id'];
		$edit_planned_month = $_POST['edit_planned_month'];
		$edit_planned_amount = $_POST['edit_planned_amount'];

		// $sql = "UPDATE milestone_field SET field_name = '".$edit_planned_month."', field_value = '".$edit_planned_amount."' WHERE id = ".$edit_planned_id;
		// $milestone = Yii::app()->db->createCommand($sql)->execute();
		$milestoneField = new MilestoneField;
		$milestoneField->rs = array();
		$milestoneField->rs['id'] = $edit_planned_id;
		$milestoneField->rs['field_name'] = $edit_planned_month;
		$milestoneField->rs['field_value'] = $edit_planned_amount;
		$milestoneField->write();
		Yii::app()->user->setFlash('saving_message','Planned Savings Updated Successfully.');
		$this->redirect(array('savings/edit/'.$edit_planned_saving_id));

		
	}

	public function actionmarkCompleteEdit(){
		
		$completedAt = date("Y-m-d h:i:s");
		$plannedID = $_POST['edit_planned_id'];
		$milestoneID = $_POST['edit_planned_milestone_id'];
		$savingID = $_POST['edit_planned_saving_id'];
		$realisedReduction = $_POST['complete_edit_realised_cost_reduction'];
		$realiseAvoidance = $_POST['complete_edit_realised_cost_avoidance'];
		$totalReleased = $_POST['complete_edit_realised_saving'];
		$amount = $_POST['edit_planned_amount'];
		$milestoneField = new MilestoneField;
		$milestoneField->rs = array();
		$milestoneField->rs['id'] = $plannedID;
		$milestoneField->rs['field_value'] = $amount;
		$milestoneField->rs['total_realised_savings'] = $realisedReduction;
		$milestoneField->rs['status'] = 'Completed';
		$milestoneField->rs['completed_by_id'] = Yii::app()->session['user_id'];
		$milestoneField->rs['completed_by_name'] = Yii::app()->session['full_name'];
		$milestoneField->rs['completed_date'] = $completedAt;
		$milestoneField->write();
		if(!empty($milestoneField->rs['id'])){			
			$savingMilestone = new SavingMilestone;
			$savingMilestone->rs = array();
			$savingMilestone->rs['id'] = $milestoneID;
			$savingMilestone->rs['saving_id'] = $savingID;
			$savingMilestone->rs['realised_cost_reduction'] = $realisedReduction;
			$savingMilestone->rs['realised_cost_avoidance'] = $realiseAvoidance;
			$savingMilestone->rs['total_realised_saving']   = $realisedReduction+$realiseAvoidance;//$totalReleased;
			$savingMilestone->write();
			if($savingMilestone->rs['id']>0){
				FunctionManager::savingCalculationSave($savingID);
			}
			$alert = 'Planned Savings competed successfully.';
		}else{
			$alert = 'There was a problem, please, try again.';
		}
        
		Yii::app()->user->setFlash('saving_message',$alert);
		$this->redirect(array('savings/edit/'.$savingID));
	}

	private function savingLogsInsert($saving_id){
		
		// @Coder: basheer alam 
		// date: 13/02/2023
		// Start: saving log
		
		$oldRecord = '';
		if(!empty($saving_id)){
		 $savingCommentExit = new Saving;
		 $oldRecord = $savingCommentExit->getOne(array('id'=>$saving_id));
		}
		
		$comment = '';
		if(!empty($oldRecord['id'])){
	     if(strcasecmp($oldRecord['title'], $_POST['title']) != 0){
	      $comment .= '<strong>Saving Title:</strong> <span class="title-text"><b>'. $oldRecord['title']. ' </b>changed to <b>'.$_POST['title'].'</b></span><br/>';
	     }
	     if(strcasecmp($oldRecord['notes'], $_POST['notes']) != 0){
	      $comment .= '<strong>Savings Notes:</strong> <span class="title-text"><b>'.
	      $oldRecord['notes']. ' </b>changed to <b>'.$_POST['notes'].'</b></span><br/>';
	     }

	     if(strcasecmp($oldRecord['status'], $_POST['status']) != 0){
	      $savingStatusModel   = new SavingStatus;
          $savingStatusPosted  = $savingStatusModel->getOne(array('id'=>Yii::app()->input->post('status')));
          $savingStatusExisted = $savingStatusModel->getOne(array('id'=>$oldRecord['status']));
                    
          if(empty($savingStatusExisted)){
           $savingStatusName = 'NULL';
          }else{
           $savingStatusName = $savingStatusExisted['value'];
          }

          $comment .= '<b>Status:</b> 
          <span class="title-text"><b>'.$savingStatusName. '</b> changed to <b>'.$savingStatusPosted['value'].'</b></span><br/>';
	     }

	    if(strcasecmp($oldRecord['currency_id'], $_POST['currency_id']) != 0){
	      $currencyRateModel = new CurrencyRate;
          $currenyPosted = $currencyRateModel->getOne(array('id'=>Yii::app()->input->post('currency_id')));
          $currenyExisted = $currencyRateModel->getOne(array('id'=>$oldRecord['currency_id']));

         if(empty($currenyExisted)){
          $currencyExistedName = 'NULL';
         }else{
          $currencyExistedName = $currenyExisted['currency'];
         }

         $comment .= '<b>Currency:</b> <span class="title-text"><b>'.
         $currencyExistedName. '</b> changed to <b>'.$currenyPosted['currency'].'</b></span><br/>';
	    }

	    if(strcasecmp($oldRecord['category_id'], $_POST['category_id']) != 0){
	     $category = new Category();
	     $categoryPosted = $category->getOne(array('id'=>Yii::app()->input->post('category_id')));
	     $categoryExisted = $category->getOne(array('id'=>$oldRecord['category_id']));
	     $comment .= '<b>Category:</b> <span class="title-text"><b>'. 
	     $categoryExisted['value']. '</b> changed to <b>'.$categoryPosted['value'].'</b></span><br/>';
	    }

        if(strcasecmp($oldRecord['subcategory_id'], $_POST['subcategory_id']) != 0){
         $subcategory = new Subcategory();
         $subcategoryPosted = $subcategory->getOne(array('id'=>Yii::app()->input->post('subcategory_id')));
         $subcategoryExisted = $subcategory->getOne(array('id'=>$oldRecord['subcategory_id']));
         $comment .= '<b>Sub Category:</b> <span class="title-text"><b>'. $subcategoryExisted['value']. '</b> changed to <b>'.$subcategoryPosted['value'].'</b></span><br/>';
        }

        if(strcasecmp($oldRecord['location_id'], $_POST['location_id']) != 0){
         $location = new Location();
         $locationPosted = $location->getOne(array('location_id'=>Yii::app()->input->post('location_id')));
         $locationExisted= $location->getOne(array('location_id'=>$oldRecord['location_id']));
         $comment .= '<b>Location:</b> <span class="title-text"><b>'. $locationExisted['location_name']. ' </b>changed to <b>'.
         $locationPosted['location_name'].'</b></span><br/>';
        }

        if(strcasecmp($oldRecord['department_id'], $_POST['department_id']) != 0){
         $department = new Department();
         $departmentPosted = $department->getOne(array('department_id'=>Yii::app()->input->post('department_id')));
         $departmentExisted= $department->getOne(array('department_id'=>$oldRecord['department_id']));
         $comment .= '<b>Department:</b> <span class="title-text"><b>'. $departmentExisted['department_name']. ' </b>changed to <b>'.
         $departmentPosted['department_name'].'</b></span><br/>';
        }

        if(!empty($comment)){
		  $log = new SavingLog();
		  $log->rs = array();
		  $log->rs['saving_id'] = $oldRecord['id'];
		  $log->rs['user_id'] = Yii::app()->session['user_id'];
		  $log->rs['comment'] = !empty($comment)?$comment:"";
		  $log->rs['created_datetime'] = date('Y-m-d H:i:s');
		  $log->rs['updated_datetime'] = date('Y-m-d H:i:s');
		  $log->write();
		}
	  }
	 //End: saving log	
	}

	public function actionSavingApprove(){
		$savingId  = $_POST['saving_id'];
		$approverId= $_POST['approver_id'];
		$status    = $_POST['status'];
		$userId    = Yii::app()->session['user_id'];
		$userName  = Yii::app()->session['full_name'];
		$createdAt = date("Y-m-d H:i:s");
		$comment = addslashes($_POST['approver_comment']);
		$approvedTime  = $_POST['approver_datetime'];
		$userObj = new User();
		
		if(!empty($approverId)){
			$approverRecord = $userObj->getOne(array('user_id'=>$approverId));
			$approver_name = $approverRecord['full_name'];
		}else{
			$approver_name = '';
		}
		
		$savingObj = new Saving();
		$savingCheck = $savingObj->checkApprovel($savingId);
		//$statusTitle = $savingObj->getStatus($status);
		//print_r();
		if($status == "Approved" || $status == "Rejected"){
	 	 $userTypeID = $savingCheck['user_id'];
	 	}else {
	 	 $userTypeID = $approverId;
	 	}

		$userRecord = $userObj->getOne(array('user_id'=>$userTypeID));
		$savingObj->rs = array();
		$savingObj->rs['id'] = $savingId;
		$savingObj->rs['approver_id'] = $approverId;
		$savingObj->rs['approver_name'] = $approver_name;
		$savingObj->rs['approver_status'] = $status;
		$savingObj->write();

		$historyObj = new SavingApprovalHistory();
		$historyObj->rs = array();
		$historyObj->rs['id'] = 0;
		$historyObj->rs['user_id'] = $userId;
		$historyObj->rs['saving_id'] = $savingId;
		$historyObj->rs['user_name'] = $userName;
		$historyObj->rs['approver_id'] = $approverId;
		$historyObj->rs['status'] = $status;
		$historyObj->rs['approver_comment'] = $comment;
		$historyObj->rs['approver_datetime'] = $approvedTime;
		$historyObj->rs['created_at'] = $createdAt;
		$historyObj->write();

		$savingRecord = $savingObj->getOne(array('id'=>$savingId));
		$savingTitle   		= $savingRecord['title'];
		$userEmail  		= $userRecord['email'];
		$userName  			= $userRecord['full_name'];
		$subject			= 'Supplier '.$status;
		$notificationFlag 	= $savingId.'clientSaving'.$status.$userId;

		if($status == "Sent For Approval" || $status == "Approved" || $status == "Rejected"){	
		if($status == "Sent For Approval"){
        	$notificationComments  = "You have a request to approve Savings ".$savingTitle;
        	$subject = 'Savings Approval Request';
        }else if($status == "Approved"){
        	$notificationComments  = "Savings: <b>".$savingTitle."</b> has been approved";
        	$subject = 'Savings Approved';
        }else{
        	$subject = 'Savings Rejected';
        	$notificationComments  = "Savings: <b>".$savingTitle."</b> has been rejected";
        }
		
		$notification = new Notification();
		$notification->rs = array();
		$notification->rs['id'] = 0;
		$notification->rs['user_type'] = 'Client';
		$notification->rs['notification_flag'] = $notificationFlag;
		$notification->rs['user_id'] = $userTypeID;
		$notification->rs['notification_text'] ='<a href="' . AppUrl::bicesUrl('savings/edit/' . $savingId) .'">'.$notificationComments.'</a>';
		$notification->rs['notification_date'] = date("Y-m-d H:i");
		$notification->rs['read_flag'] = 0;
		$notification->rs['notification_type'] = 'Savings Status';
		$notification->write();
		$notificationText = '<a href="' . AppUrl::bicesUrl('savings/edit/' . $savingId) . '" style="font-size:18px;color:#2d9ca2;text-decoration:none">'.$notificationComments.'</a>';
		$url = AppUrl::bicesUrl('savings/edit/' . $savingId);
		EmailManager::userNotification($userEmail,$userName,$subject,$notificationText,$url);
		}

		$commentLog = '';
		 if(!empty($savingCheck['approver_status'])){
	        if(strcasecmp($savingCheck['approver_id'], $approverId) != 0){
	          $sql = "SELECT full_name  FROM users WHERE  user_id=".$approverId;
	          $userReader = Yii::app()->db->createCommand($sql)->queryRow();
	          $commentLog .= '<b>User Approver:</b> <span class="title-text"><b>'. $savingCheck['approver_name']. '</b> Changed to <b>'.$userReader['full_name'].'</b></span><br/>';
	        }
	        if(strcasecmp($savingCheck['approver_status'], $status) != 0){

	         $commentLog .= '<b>Status:</b> <span class="title-text"><b>'. $savingCheck['approver_status'] . '</b> Changed to <b>'.$status .'</b></span><br/>';
	        }

	        if(!empty($comment)){
	          $commentLog .= '<b>Comment:</b> <span class="title-text"><b>'. $comment. '</b></span><br/>';
	        }
	     	
            if(!empty($commentLog)){
				$log = new SavingLog();
				$log->rs = array();
				$log->rs['saving_id'] = $savingId;
				$log->rs['user_id'] = Yii::app()->session['user_id'];
				$log->rs['comment'] = !empty($commentLog) ? $commentLog:"";
				$log->rs['created_datetime'] = date('Y-m-d H:i:s');
				$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
				$log->write();
        	} 
    	}
    	
		Yii::app()->user->setFlash('success', "Success! Savings status changed successfully.");
		$this->redirect(AppUrl::bicesUrl('savings/edit/'.$savingId ));
    }

    public function actionArchiveList()
    {   
        date_default_timezone_set(Yii::app()->params['timeZone']);
        error_reporting(0);
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'archive_list';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);
        
        if (! isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
            $this->redirect(AppUrl::bicesUrl('home'));
        
        // get list of all contracts (with pagination of course)
        $year_start_date = date("2010-01-01");
        $current_date =  date("Y-m-d");


        
       // $firstDateYear = date("Y"). "-01-01";
        $firstDateYear =  date("Y",strtotime(date("Y").' -1 year'));
        $lastDateYear = date("Y"). "-12-31";
        $contract = new Contract();
        $view_data = array();

		$saving = new Saving();
		$view_data['saving_status'] = $saving->getStatus();
        // need filters too
        $location = new Location();
        $view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
        
        $category = new Category();
        $view_data['categories'] = $category->getAll();
        
        // get contracts as per filters applied
        $location_id = Yii::app()->input->post('location_id');
        $department_id = Yii::app()->input->post('department_id');
        $category_id = Yii::app()->input->post('category_id');
        $subcategory_id = Yii::app()->input->post('subcategory_id');
        $status_code = Yii::app()->input->post('contract_status');
    
        if(!empty($department_id)){
            $location = new Location();
            $department_info = $location->getDepartments($location_id);
        }else
         $department_info='';
         $view_data['location_id'] = $location_id;
         $view_data['department_info'] = $department_info;
         $view_data['department_id'] = $department_id;
         $view_data['category_id'] = $category_id;
         $view_data['subcategory_id'] = $subcategory_id;
		
         $this->render('archive_list', $view_data);
    }


    public function actionListArchiveAjax(){
        error_reporting(0);
        $saving = new Saving();
        $industry_id = 0;
            if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id']))
                $industry_id = $_REQUEST['industry_id'];            

            $subindustry_id = 0;
            if (isset($_REQUEST['subindustry_id']) && !empty($_REQUEST['subindustry_id']))
                $subindustry_id = $_REQUEST['subindustry_id'];          

            $preferred_flag = -1;
            $supplier_status = '';
            if (isset($_REQUEST['preferred_flag'])) $preferred_flag = $_REQUEST['preferred_flag'];
            if (isset($_REQUEST['supplier_status'])) $supplier_status = $_REQUEST['supplier_status'];
               
            set_time_limit(0);
            $total_vendors = 0;
            
            $search_for = "";
            if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value'])){
             $search_for = $_REQUEST['search']['value'];
            }
            
            $order_by = array();
            if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])){
             $order_by = $_REQUEST['order'];
            }else{
             $order_by[1] = 'contract_title';
            }
            
            $location_id   = Yii::app()->input->post('location_id');
            $department_id = Yii::app()->input->post('department_id');
            $category_id   = Yii::app()->input->post('category_id');
            $subcategory_id= Yii::app()->input->post('subcategory_id');
            $saving_status = Yii::app()->input->post('status');
            $due_date 	   = Yii::app()->input->post('due_date');
            $due_date_to   = Yii::app()->input->post('due_date_to');
            
            $savings = $saving->getSavingsAjax(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by,$location_id,
            $department_id, $category_id, $subcategory_id,$saving_status,$due_date,$due_date_to);
            $dataArr = array();
            $i=1;
            $subdomain = FunctionManager::subdomainByUrl();
            foreach ($savings as $value)
            {   
               $style="";
				if(strtolower(trim($value['saving_status']))=="completed"){
					$style ="style='background:#6cce47;border-color:#6cce47'";
				}else if(strtolower(trim($value['saving_status']))=="in progress"){
					$style ="style='background:#f0ad4e;border-color:#f0ad4e'";
				}else if(strtolower(trim($value['saving_status']))=="not started"){
					$style ="style='background:#fb5a5a !important ;border-color:#fb5a5a !important'";
				}	
				$imgFullPath = '';
                if(!empty($value['profile_img'])){
                    $imgFullPath = Yii::app()->baseUrl . "/../images/".$subdomain."/users/".$value['user_id']."/".$value['profile_img'];

                }else if(!empty($value['user_id'])){
                  $imgFullPath = CommonFunction::userTextAvatar($value['full_name']);
                }
                 if(!empty($imgFullPath)){
                    $imgFullPath = CHtml::image($imgFullPath);
                }
                $current_data = array();
                $current_data[] = '<a href="'.AppUrl::bicesUrl('savings/edit/' . $value['id']).'">
                            <button class="btn btn-sm btn-success view-btn" >View/Edit
                            </button>
                        </a>';
				
				$profileImg = '<div class="image-container">'.$imgFullPath.' </i><div class="profile-username"> '. $value['user_name'].' </div> </div> ';

                $current_data[] = $value['id'];
                $current_data[] = $value['title'];
                $current_data[] = $value['created_at'] !='0000-00-00 00:00:00'?date(FunctionManager::dateFormat(), strtotime($value['created_at'])):'';
                $current_data[] = $value['due_date'] !='0000-00-00 00:00:00'?date(FunctionManager::dateFormat(), strtotime($value['due_date'])):'';
                $current_data[] = $profileImg;
                $current_data[] = $value['currency'];
                $current_data[] = $value['total_base_spend']>0?$value['currency_symbol'].number_format($value['total_base_spend'],0,"",""):'';
                $current_data[] = $value['total_project_saving']>0?$value['currency_symbol'].number_format($value['total_project_saving'],0,"",""):'';
                $current_data[] = $value['total_project_saving_perc']>0?number_format($value['total_project_saving_perc'],0,"","").'%':'';
                $current_data[] = $value['realised_saving']>0?$value['currency_symbol'].number_format($value['realised_saving'],0,"",""):'';
                $current_data[] = $value['realised_saving_perc']>0?number_format($value['realised_saving_perc'],0,"","").'%':'';
                $current_data[] = '<button type="submit" class="btn btn-success" '.$style.'>'.$value['saving_status'].'</button>';
                $dataArr[]  = $current_data;
            }

            $database_values_total = $saving->getSavingsAjax(0, 'none', 'none', $search_for, $order_by,$location_id,
            $department_id, $category_id, $subcategory_id,$saving_status,$due_date,$due_date_to);
            $total_savings=count($database_values_total);

            $json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $total_savings,
                "recordsFiltered" => $total_savings,
                "data"            => $dataArr
            );
            echo json_encode($json_data);exit;  
    }

     public function actionSaveArhive(){
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $this->checklogin();
        $savingID = $_GET['saving-id'];

        if(empty($savingID)){
            $this->redirect(AppUrl::bicesUrl('savings/list'));
        }



        if(empty($_GET['archive-type'])){
         $sql = "update savings set saving_archive='yes' WHERE id=".$savingID;
         $archive = Yii::app()->db->createCommand($sql)->execute();
         if($archive)
          $this->archiveLog('yes', $savingID);
          Yii::app()->user->setFlash('success', 'Saving archived successfully.');
         $this->redirect(AppUrl::bicesUrl('savings/archiveList'));
        }else if(!empty($_GET['archive-type']) && $_GET['archive-type']=='un-archive'){
         $sql = "update savings set saving_archive='no' WHERE id=".$savingID;
         $archive = Yii::app()->db->createCommand($sql)->execute();
         if($archive){
          $this->archiveLog('no', $savingID);
          Yii::app()->user->setFlash('success', 'Saving Un-archived successfully.');
         }

         $this->redirect(AppUrl::bicesUrl('savings/list'));
        }
    }

    private function archiveLog($archive='', $savingID){
    	$saving = new Saving();
    	$oldRecord = $saving->getOne(array('id' => $savingID));
    	$comment = '';
	    if($archive == 'yes'){
	      $comment .= '<strong>Savings '.$oldRecord['title'].':</strong> <span class="title-text"> <b>archived </b>changed to <b>Un-archived</b></span><br/>';
	    }else{
	     $comment .= '<strong>Savings '.$oldRecord['title'].':</strong> <span class="title-text"><b> Un-archived </b>changed to <b>archived</b></span><br/>';	
	    }

	    if(!empty($comment)){
		  $log = new SavingLog();
		  $log->rs = array();
		  $log->rs['saving_id'] = $oldRecord['id'];
		  $log->rs['user_id'] = Yii::app()->session['user_id'];
		  $log->rs['comment'] = !empty($comment)?$comment:"";
		  $log->rs['created_datetime'] = date('Y-m-d H:i:s');
		  $log->rs['updated_datetime'] = date('Y-m-d H:i:s');
		  $log->write();
		}
    }

	public function actionExportCSV(){
		$saving = new Saving();
        $saving->export();
	} 
}
