<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');

class ContractsController extends CommonController
{

    public $layout = 'main';

    public function actionIndex()
    {
        $this->redirect(AppUrl::bicesUrl('contracts/list'));
    }

    public function actionAnalysis()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'contracts_analysis';

        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        // by default, show reports for year to date time frame
        $user_selected_year = Yii::app()->session['user_selected_year'];
        if (empty($user_selected_year)) {
            $user_selected_year = date("Y");
            $year_start_date = $user_selected_year . "-01-01";
            $current_date = $user_selected_year . "-12-31";
        } else {
            $year_start_date = $user_selected_year . "-01-01";
            $current_date = $user_selected_year . "-12-31";
        }

        // get top vendors, top locations and top categories data for the time being
        $view_data = array();
        $contract = new Contract();
        $view_data['top_contracts'] = $contract->getTopContracts($year_start_date, date("Y-m-d"));
        $view_data['contract_statuses'] = $contract->getContractStatusTotals($year_start_date, $current_date);
        $view_data['contracts_by_location'] = $contract->getContractLocationTotals($year_start_date, $current_date);
        $view_data['contracts_by_category'] = $contract->getContractCategoryTotals($year_start_date, $current_date);
        $view_data['expiring_contracts'] = $contract->getExpiringContracts();
        $view_data['metrics']['contract'] = $contract->getDashboardMetrics($year_start_date, $current_date);

        $this->render('analysis', $view_data);
    }

    public function actionList()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        unset(Yii::app()->session['quick_vendor']);
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'contracts_list';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        /* $search_for = "Mgr";
        echo var_dump(filter_var($search_for, FILTER_SANITIZE_NUMBER_INT));
        exit;*/
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(1, 3, 4)))
            $this->redirect(AppUrl::bicesUrl('home'));
        // get list of all contracts (with pagination of course)
        $year_start_date = date("2010-01-01");
        $current_date =  date("Y-m-d");

        // $firstDateYear = date("Y"). "-01-01";
        $firstDateYear =  date("Y", strtotime(date("Y") . ' -1 year'));
        $lastDateYear = date("Y") . "-12-31";
        $contract = new Contract();
        $view_data = array();

        $vendor = new Vendor();

        $byCategory = $vendor->getVendorsAndContractsIndustry($year_start_date, $lastDateYear);
        $view_data['count_by_category'] = $byCategory['count_by_category'];
        // $view_data['contractListViewPermission'] = $contract->getContractListPermission();

        // get top vendors, top locations and top categories data for the time being
        $view_data['top_contracts'] = $contract->getTopContracts($firstDateYear, $current_date);
        $view_data['contract_statuses'] = $contract->getContractStatusTotals($year_start_date, $current_date);
        $view_data['contracts_by_location'] = $contract->getContractLocationTotals($year_start_date, $current_date);
        $view_data['contracts_by_category'] = $contract->getContractCategoryTotals($year_start_date, $current_date);
        //$view_data['metrics']['contract'] = $contract->getDashboardMetrics($year_start_date, $current_date);
        $view_data['metrics']['contract']  = $contract->getMetrics($year_start_date, $current_date, "checkPermission");
        $view_data['metrics']['contract_popup']  = $contract->getMetrics($year_start_date, $current_date, "checkPermission");
        // and display expired_only_count
        $view_data['contracts_by_status'] = $contract->getContractStatusTotals($year_start_date, $current_date);

        $view_data['expiring_contracts'] = $contract->getExpiringAndNoticePeriodContracts();
        $view_data['expiring_contracts_list'] = $contract->getExpiringAndNoticePeriodContractsList();
        //echo "<pre>";print_r($view_data['expiring_contracts_list']);exit;
        $view_data['vendors'] = array();

        $report = new Report;
        $view_data['contractCount'] = $report->contractByStatus();

        // need filters too
        $location = new Location();
        $view_data['locations'] = $location->getAll(array(
            'ORDER' => 'location_name'
        ));

        $category = new Category();
        $view_data['categories'] = $category->getAll();

        // get contracts as per filters applied
        $location_id = Yii::app()->input->post('location_id');
        $department_id = Yii::app()->input->post('department_id');
        $category_id = Yii::app()->input->post('category_id');
        $subcategory_id = Yii::app()->input->post('subcategory_id');
        $status_code = Yii::app()->input->post('contract_status');

        $notification = new Notification();
        $view_data['notifications'] = $notification->getLastestForContract(10);


        $view_data['contracts'] = $contract->getContracts(0, $location_id, $department_id, $category_id, $subcategory_id, $status_code);

        // print $location_id; exit;

        if (!empty($department_id)) {
            $location = new Location();
            $department_info = $location->getDepartments($location_id);
        } else
            $department_info = '';
        $view_data['location_id'] = $location_id;
        $view_data['department_info'] = $department_info;
        $view_data['department_id'] = $department_id;
        $view_data['category_id'] = $category_id;
        $view_data['subcategory_id'] = $subcategory_id;

        $user = new User();
        $userData = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

        $view_data['contract_visit'] = $userData['contract_visit'];
        if ($userData['contract_visit'] != 1) {
            $user->rs = array();
            $user->rs['user_id'] = $userData['user_id'];
            $user->rs['contract_visit'] = 1;
            $user->write();
        }


        $this->render('list', $view_data);
    }

    public function actionListAjax()
    {
        error_reporting(0);
        $contract = new Contract();
        $industry_id = 0;
        if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id']))
            $industry_id = $_REQUEST['industry_id'];

        $subindustry_id = 0;
        if (isset($_REQUEST['subindustry_id']) && !empty($_REQUEST['subindustry_id']))
            $subindustry_id = $_REQUEST['subindustry_id'];

        $preferred_flag = -1;
        $supplier_status = '';
        if (isset($_REQUEST['preferred_flag'])) $preferred_flag = $_REQUEST['preferred_flag'];
        if (isset($_REQUEST['supplier_status'])) $supplier_status = $_REQUEST['supplier_status'];

        set_time_limit(0);
        $total_vendors = 0;

        $search_for = "";
        if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value'])) {
            $search_for = $_REQUEST['search']['value'];
        }

        $order_by = array();
        if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])) {
            $order_by = $_REQUEST['order'];
        } else {
            $order_by[1] = 'contract_title';
        }

        $location_id = Yii::app()->input->post('location_id');
        $department_id = Yii::app()->input->post('department_id');
        $category_id = Yii::app()->input->post('category_id');
        $subcategory_id = Yii::app()->input->post('subcategory_id');
        $status_code = Yii::app()->input->post('contract_status');

        $contracts = $contract->getContractsAjax(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $status_code);

        $contractArr = array();
        $i = 1;
        $subdomain = FunctionManager::subdomainByUrl();
        foreach ($contracts as $value) {
            $class = '';
            $icon = '';
            $endDate = date("Y-m-d", strtotime($value['contract_end_date']));

            $afterMonth = date("Y-m-d", strtotime("+1 month"));
            $after3Month = date("Y-m-d", strtotime("+3 months"));

            if ($endDate < date("Y-m-d")) {
                $color = '#c1bfbf';
                $expiresTip = 'Passed Contract End Date';
                $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="left" title="' . $expiresTip . '" aria-hidden="true" style="color:' . $color . '"></i></span>';
            } else {
                if ($endDate >=  date("Y-m-d") && $endDate <= $afterMonth) {
                    $color = '#f7778c';
                    $expiresTip = 'End Date Within 30 Days';
                    $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="left" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                } else if ($endDate >=  date("Y-m-d") && $endDate <= $after3Month) {
                    $color = '#fdec90';
                    $expiresTip = 'End Date Within 3 Months';
                    $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="left" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                } else if ($endDate >=  date("Y-m-d") && $endDate > $after3Month) {
                    $color = '#00bc9d';
                    $expiresTip = 'End Date Over 3 Months';
                    $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="left" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                }
            }

            $edit_link = '';
            $contractTtitle = '';
            $vendorName  = '';

            $edit_link .= '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $value['contract_id']) . '">';
            $edit_link .= '<button class="btn btn-sm btn-success view-btn">View/Edit</button></a>';
            $contractTtitle .= '<a style="text-decoration:none" href="' . AppUrl::bicesUrl('contracts/edit/' . $value['contract_id']) . '">';
            $contractTtitle .= $value['contract_title'] . '</a>';
            $vendorName .= '<a style="text-decoration:none" href="' . AppUrl::bicesUrl('vendors/edit/' . $value['vendor_id']) . '">';
            $vendorName .= $value['vendor'] . '</a>';

            $tool_currency = Yii::app()->session['user_currency'];
            $price_total = number_format($value['estimated_contract_value'], 0);
            $currency_id = $value['currency_id'];
            $contract_start_date = $contract_end_date = $break_clause = "";
            if ($value['contract_start_date'] != "0000-00-00") {
                $contract_start_date = date(FunctionManager::dateFormat(), strtotime($value['contract_start_date']));
            }
            if ($value['break_clause'] != "0000-00-00 00:00:00") {
                $break_clause = date(FunctionManager::dateFormat(), strtotime($value['break_clause']));
            }

            if ($value['contract_end_date'] != "0000-00-00") {
                $contract_end_date  = '';
                $contract_end_date .= date(FunctionManager::dateFormat(), strtotime($value['contract_end_date']));
                $contract_end_date .=  $icon;
            }

            $imgFullPath = '';
            if (!empty($value['profile_img'])) {
                $imgFullPath = Yii::app()->baseUrl . "/../images/" . $subdomain . "/users/" . $value['sbo_user_id'] . "/" . $value['profile_img'];
            } else if (!empty($value['sbo_user_id']) && !empty($value['contract_manager'])) {
                $imgFullPath = CommonFunction::userTextAvatar($value['full_name']);
            }
            if (!empty($imgFullPath)) {
                $imgFullPath = CHtml::image($imgFullPath);
            }
            $profileImg = '<div class="image-container">' . $imgFullPath . ' </i><div class="profile-username"> ' . $value['contract_manager'] . ' </div> </div> ';
            $current_data = array();
            $current_data[] = $edit_link;
            $current_data[] = $contractTtitle;
            $current_data[] = $value['category'];
            $current_data[] = isset($value['contract_manager']) ? $profileImg : '';

            $current_data[] = '<span class="notranslate">' . $vendorName . '</span>';
            $current_data[] = $value['currency'];
            $current_data[] = $value['currency_symbol'] . $price_total;
            $current_data[] = $contract_start_date;
            $current_data[] = $break_clause;
            $current_data[] = $contract_end_date;

            $contractArr[]  = $current_data;
            $i++;
        }

        $total_contracts = $contract->getContractsAjax(0, 'none', 'none', $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $status_code);;
        $total_contracts = count($total_contracts);
        $json_data = array(
            "draw"            => intval($_REQUEST['draw']),
            "recordsTotal"    => $total_contracts,
            "recordsFiltered" => $total_contracts,
            "data"            => $contractArr
        );
        echo json_encode($json_data);
        exit;
    }
    public function actionListArchiveAjax()
    {
        error_reporting(0);
        $contract = new Contract();
        $industry_id = 0;
        if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id']))
            $industry_id = $_REQUEST['industry_id'];

        $subindustry_id = 0;
        if (isset($_REQUEST['subindustry_id']) && !empty($_REQUEST['subindustry_id']))
            $subindustry_id = $_REQUEST['subindustry_id'];

        $preferred_flag = -1;
        $supplier_status = '';
        if (isset($_REQUEST['preferred_flag'])) $preferred_flag = $_REQUEST['preferred_flag'];
        if (isset($_REQUEST['supplier_status'])) $supplier_status = $_REQUEST['supplier_status'];


        set_time_limit(0);
        $total_vendors = 0;



        $search_for = "";
        if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value'])) {
            $search_for = $_REQUEST['search']['value'];
        }

        $order_by = array();
        if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])) {
            $order_by = $_REQUEST['order'];
        } else {
            $order_by[1] = 'contract_title';
        }

        $location_id = Yii::app()->input->post('location_id');
        $department_id = Yii::app()->input->post('department_id');
        $category_id = Yii::app()->input->post('category_id');
        $subcategory_id = Yii::app()->input->post('subcategory_id');
        $status_code = Yii::app()->input->post('contract_status');

        $contracts = $contract->getContractsAjax(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $status_code);

        $contractArr = array();
        $archiveContract = 0;
        $subdomain = FunctionManager::subdomainByUrl();
        foreach ($contracts as $value) {
            $class = '';
            $icon = '';
            $archiveContract++;
            $endDate = date("Y-m-d", strtotime($value['contract_end_date']));

            $afterMonth = date("Y-m-d", strtotime("+1 month"));
            $after3Month = date("Y-m-d", strtotime("+3 months"));

            if ($endDate < date("Y-m-d")) {
                $color = '#c1bfbf';
                $expiresTip = 'Passed Contract End Date';
                $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="left" title="' . $expiresTip . '" aria-hidden="true" style="color:' . $color . '"></i></span>';
            } else {
                if ($endDate >=  date("Y-m-d") && $endDate <= $afterMonth) {
                    $color = '#f7778c';
                    $expiresTip = 'End Date Within 30 Days';
                    $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="left" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                } else if ($endDate >=  date("Y-m-d") && $endDate <= $after3Month) {
                    $color = '#fdec90';
                    $expiresTip = 'End Date Within 3 Months';
                    $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="left" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                } else if ($endDate >=  date("Y-m-d") && $endDate > $after3Month) {
                    $color = '#00bc9d';
                    $expiresTip = 'End Date Over 3 Months';
                    $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="left" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                }
            }

            $edit_link = '';
            $contractTtitle = '';
            $vendorName  = '';

            $edit_link .= '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $value['contract_id']) . '">';
            $edit_link .= '<button class="btn btn-sm btn-success view-btn">View/Edit</button></a>';
            $contractTtitle .= '<a style="text-decoration:none" href="' . AppUrl::bicesUrl('contracts/edit/' . $value['contract_id']) . '">';
            $contractTtitle .= $value['contract_title'] . '</a>';
            $vendorName .= '<a style="text-decoration:none" href="' . AppUrl::bicesUrl('vendors/edit/' . $value['vendor_id']) . '">';
            $vendorName .= $value['vendor'] . '</a>';

            $tool_currency = Yii::app()->session['user_currency'];
            $price_total = number_format($value['estimated_contract_value'], 0);
            $currency_id = $value['currency_id'];
            $contract_start_date = $contract_end_date = $break_clause = "";
            if ($value['contract_start_date'] != "0000-00-00") {
                $contract_start_date = date(FunctionManager::dateFormat(), strtotime($value['contract_start_date']));
            }
            if ($value['break_clause'] != "0000-00-00 00:00:00") {
                $break_clause = date(FunctionManager::dateFormat(), strtotime($value['break_clause']));
            }
            if ($value['contract_end_date'] != "0000-00-00") {
                $contract_end_date  = '';
                $contract_end_date .= date(FunctionManager::dateFormat(), strtotime($value['contract_end_date']));
                $contract_end_date .=  $icon;
            }

            $imgFullPath = '';
            if (!empty($value['profile_img'])) {
                $imgFullPath = Yii::app()->baseUrl . "/../images/" . $subdomain . "/users/" . $value['sbo_user_id'] . "/" . $value['profile_img'];
            } else if (!empty($value['sbo_user_id']) && !empty($value['contract_manager'])) {
                $imgFullPath = CommonFunction::userTextAvatar($value['full_name']);
            }

            if (!empty($imgFullPath)) {
                $imgFullPath = CHtml::image($imgFullPath);
            }

            $profileImg = '<div class="image-container">' . $imgFullPath . ' </i><div class="profile-username"> ' . $value['contract_manager'] . ' </div> </div> ';

            $current_data = array();
            $current_data[] = $edit_link;
            $current_data[] = $contractTtitle;
            $current_data[] = $value['category'];
            $current_data[] = isset($value['contract_manager']) ? $profileImg : '';

            $current_data[] = '<span class="notranslate">' . $vendorName . '</span>';
            $current_data[] = '<span class="td-center"">' . $value['currency'] . '</span>';
            $current_data[] = $value['currency_symbol'] . $price_total;
            $current_data[] = $contract_start_date;
            $current_data[] = $break_clause;
            $current_data[] = $contract_end_date;

            $contractArr[]  = $current_data;
        }
        $archiveContracts = $contract->getContractsAjax(0, 'none', 'none', $search_for, $order_by, $location_id, $department_id, $category_id, $subcategory_id, $status_code);
        $archiveContracts = count($archiveContracts);
        $json_data = array(
            "draw"            => intval($_REQUEST['draw']),
            "recordsTotal"    => $archiveContracts,
            "recordsFiltered" => $archiveContracts,
            "data"            => $contractArr
        );
        echo json_encode($json_data);
        exit;
    }

    public function actionArchiveList()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        error_reporting(0);
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'contracts_list';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(1, 3, 4)))
            $this->redirect(AppUrl::bicesUrl('home'));

        // get list of all contracts (with pagination of course)
        $year_start_date = date("2010-01-01");
        $current_date =  date("Y-m-d");



        // $firstDateYear = date("Y"). "-01-01";
        $firstDateYear =  date("Y", strtotime(date("Y") . ' -1 year'));
        $lastDateYear = date("Y") . "-12-31";
        $contract = new Contract();
        $view_data = array();

        // need filters too
        $location = new Location();
        $view_data['locations'] = $location->getAll(array(
            'ORDER' => 'location_name'
        ));

        $category = new Category();
        $view_data['categories'] = $category->getAll();

        // get contracts as per filters applied
        $location_id = Yii::app()->input->post('location_id');
        $department_id = Yii::app()->input->post('department_id');
        $category_id = Yii::app()->input->post('category_id');
        $subcategory_id = Yii::app()->input->post('subcategory_id');
        $status_code = Yii::app()->input->post('contract_status');

        if (!empty($department_id)) {
            $location = new Location();
            $department_info = $location->getDepartments($location_id);
        } else
            $department_info = '';
        $view_data['location_id'] = $location_id;
        $view_data['department_info'] = $department_info;
        $view_data['department_id'] = $department_id;
        $view_data['category_id'] = $category_id;
        $view_data['subcategory_id'] = $subcategory_id;
        $this->render('archive_list', $view_data);
    }

    public function actionEdit($contract_id = 0)
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'contracts_edit';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(1, 3, 4)))
            $this->redirect(AppUrl::bicesUrl('home'));

        // if form was submitted ... first save the data to the database
        if (Yii::app()->input->post('form_submitted')) {
            // what if vendor id is blank?

            $contract = new Contract();
            $override_data = array();
            $vendor_id = Yii::app()->input->post('vendor_id');
            $userObj = new User();

            if (!empty($_POST['sbo_user_id'])) {
                $manager = $userObj->getOne(array('user_id' => $_POST['sbo_user_id']));
                $_POST['contract_manager'] = $manager['full_name'];
            } else {
                $_POST['contract_manager'] = '';
            }

            if (!empty($_POST['commercial_user_id'])) {
                $manager = $userObj->getOne(array('user_id' => $_POST['commercial_user_id']));
                $_POST['commercial_lead'] = $manager['full_name'];
            } else {
                $_POST['commercial_lead'] = '';
            }

            if (!empty($_POST['procurement_user_id'])) {
                $manager = $userObj->getOne(array('user_id' => $_POST['procurement_user_id']));
                $_POST['procurement_lead'] = $manager['full_name'];
            } else {
                $_POST['procurement_lead'] = '';
            }

            if (!empty($_POST['additional_user_1'])) {
                $manager = $userObj->getOne(array('user_id' => $_POST['additional_user_1']));
                $_POST['additional_user_lead_1'] = $manager['full_name'];
            } else {
                $_POST['additional_user_lead_1'] = '';
            }

            if (!empty($_POST['additional_user_2'])) {
                $manager = $userObj->getOne(array('user_id' => $_POST['additional_user_2']));
                $_POST['additional_user_lead_2'] = $manager['full_name'];
            } else {
                $_POST['additional_user_lead_2'] = '';
            }

            $financialForm = Yii::app()->input->post('form_financial');
            $summaryForm = Yii::app()->input->post('form_summary');
            $timelineForm = Yii::app()->input->post('form_timeline');
            $peopleForm = Yii::app()->input->post('form_people');
            $commentsForm = Yii::app()->input->post('form_comments');
            $notificationForm = Yii::app()->input->post('form_notificatioin');
            $documentForm = Yii::app()->input->post('form_document');
            if (FunctionManager::dateFormat() == "d/m/Y") {
                $startFormat = strtr(Yii::app()->input->post('contract_start_date'), '/', '-');
                $endFormat = strtr(Yii::app()->input->post('contract_end_date'), '/', '-');
            } else {
                $startFormat = Yii::app()->input->post('contract_start_date');
                $endFormat = Yii::app()->input->post('contract_end_date');
            }

            $contract_id = $contractIDNoti = Yii::app()->input->post('contract_id');
            // notifications are stored in a different table
            $notification = new ContractNotification();

            $notes = Yii::app()->input->post('notification');
            $notes_dates = Yii::app()->input->post('notification_date');

            for ($idx = 0; $idx <= count($notes); $idx++) {
                $note_data = array(
                    'id' => 0,
                    'contract_id' => $contractIDNoti
                );
                $note_data['note'] = isset($notes[$idx]) ? $notes[$idx] : "";
                $note_date = isset($notes_dates[$idx]) ? $notes_dates[$idx] : "";
                if (!empty($note_date))
                    $note_data['date'] =  $note_date;
                else
                    $note_data['date'] = date("d/m/Y");

                if (!empty($note_data['note']))
                    $notification->saveData($note_data);
            }
            if (!empty($_POST['notification_update'])) {
                $notes = $_POST['notification_update'];
                foreach ($notes as $key => $notesInfo) {

                    $note = isset($notesInfo) ? $notesInfo : "";

                    if (FunctionManager::dateFormat() == "d/m/Y") {
                        $note_date = isset($_POST['notification_date_update'][$key]) ?  date("Y-m-d", strtotime(strtr($_POST['notification_date_update'][$key], '/', '-'))) : "";
                    } else {
                        $note_date = isset($_POST['notification_date_update'][$key]) ?  date("Y-m-d", strtotime($_POST['notification_date_update'][$key])) : "";
                    }

                    if (empty($note_date))
                        $note_date = date("d/m/Y");

                    if (!empty($note)) {
                        $sqlCloumn = 'note="' . $note . '"';
                        $note_date;
                        $sqlCloumn .= ',date="' . $note_date . '"';
                        $clause = ' where id=' . $key;
                        $notification->executeQuery("UPDATE contract_notifications SET $sqlCloumn $clause");
                    }
                }
            }

            $existingContract = $contract->madeContractLogComment(Yii::app()->input->post('contractId'));

            $quickVendorAdded = 0;
            if (!empty(Yii::app()->session['quick_vendor'])) {
                $quickaddedVendorArr = Yii::app()->session['quick_vendor'];
                $quickaddedVendorArr = max($quickaddedVendorArr);
                $vendor_id = $quickaddedVendorArr['vendor_id'];
                $quickVendorAdded = 1;
            }

            $override_data['vendor_id'] = $vendor_id;
            $vendorObj = new Vendor();
            $vendorReader = $vendorObj->getOne(array('vendor_id' => $vendor_id));
            $override_data['contact_name'] = !empty(Yii::app()->input->post('contact_name')) ? Yii::app()->input->post('contact_name') : '';
            $override_data['website'] = $vendorReader['emails'];
        
            if (null == Yii::app()->input->post('user_id')) {
                $override_data['user_id']   = Yii::app()->session['user_id'];
                $override_data['user_name'] = Yii::app()->session['full_name'];
                $contract->saveData($override_data);
                Yii::app()->user->setFlash('success', "Contract added successfully.");
            } else {
                $contract->saveData($override_data);
                Yii::app()->user->setFlash('success', "Contract added successfully.");
            }

            //Start : Save multipule contract locations and contract department against this contract ID
            if((is_array($_POST['location_id']) && !empty($_POST['location_id'])) &&
                (is_array($_POST['department_id']) && !empty($_POST['department_id']))){
                $this->saveContractLocationsAndDepartments($_POST['location_id'], $_POST['department_id'], $contract->rs['contract_id']);
            }
            // END : Save multipule contract locations and contract department against this contract ID
            

            $contract_id = $contract->rs['contract_id'];
            $contractTitle = Yii::app()->input->post('contract_title');
            if (!empty($quickVendorAdded)) {
                EmailManager::vendorAcivation($vendor_id, 1, $contract->rs['user_name']);
            }
            // save actual contract data

            $doNotNotifySupplier = '';
            if (count($existingContract) > 0) {

                $comment = '';
                if ($existingContract[0]['contract_title'] != Yii::app()->input->post('contract_title')) {
                    $comment .= '<strong>Contract Title:</strong> <span class="title-text"><b>' . $existingContract[0]['contract_title'] . ' </b>changed to <b>' . Yii::app()->input->post('contract_title') . '</b></span><br/>';
                }
                if ($existingContract[0]['contract_reference'] != Yii::app()->input->post('contract_reference')) {
                    $comment .= '<b>Reference #:</b> <span class="title-text"><b>' . $existingContract[0]['contract_reference'] . ' </b>changed to <b>' . Yii::app()->input->post('contract_reference') . '</b></span><br/>';
                }
                if ($existingContract[0]['contract_type'] != Yii::app()->input->post('contract_type')) {
                    $comment .= '<b>Type:</b> <span class="title-text"><b>' . $existingContract[0]['contract_type'] . ' </b>changed to <b>' . Yii::app()->input->post('contract_type') . '</b></span><br/>';
                }

                if ($existingContract[0]['currency_id'] != Yii::app()->input->post('currency_id')) {

                    $currencyRateModel = new CurrencyRate;
                    $currenyPosted = $currencyRateModel->getOne(array('id' => Yii::app()->input->post('currency_id')));
                    $currenyExisted = $currencyRateModel->getOne(array('id' => $existingContract[0]['currency_id']));

                    if (empty($currenyExisted)) {
                        $currencyExistedName = 'NULL';
                    } else {
                        $currencyExistedName = $currenyExisted['currency'];
                    }

                    $comment .= '<b>Currency:</b> <span class="title-text"><b>' . $currencyExistedName . '</b> Changed to <b>' . $currenyPosted['currency'] . '</b></span><br/>';
                }



                if ($existingContract[0]['contract_manager'] != Yii::app()->input->post('contract_manager')) {
                    $comment .= '<b>Managed By:</b> <span class="title-text"><b>' . $existingContract[0]['contract_manager'] . ' </b>changed to <b> ' . Yii::app()->input->post('contract_manager') . '</b></span><br/>';
                }
                if ($existingContract[0]['commercial_lead'] != Yii::app()->input->post('commercial_lead')) {
                    $comment .= '<b>Additional User To Receive Notifications:</b> <span class="title-text"><b>' . $existingContract[0]['commercial_lead'] . '</b> Changed to <b>' . Yii::app()->input->post('commercial_lead') . '</b></span><br/>';
                }
                if ($existingContract[0]['procurement_lead'] != Yii::app()->input->post('procurement_lead')) {
                    $comment .= '<b>Additional User To Receive Notifications:</b> <span class="title-text"><b>' . $existingContract[0]['procurement_lead'] . '</b> Changed to <b>' . Yii::app()->input->post('procurement_lead') . '</b></span><br/>';
                }

                if ($existingContract[0]['additional_user_lead_1'] != Yii::app()->input->post('additional_user_lead_1')) {
                    $comment .= '<b>Additional User To Receive Notifications:</b> <span class="title-text"><b>' . $existingContract[0]['additional_user_lead_1'] . '</b> Changed to <b>' . Yii::app()->input->post('additional_user_lead_1') . '</b></span><br/>';
                }

                if ($existingContract[0]['additional_user_lead_2'] != Yii::app()->input->post('additional_user_lead_2')) {
                    $comment .= '<b>Additional User To Receive Notifications:</b> <span class="title-text"><b>' . $existingContract[0]['additional_user_lead_2'] . '</b> Changed to <b>' . Yii::app()->input->post('additional_user_lead_2') . '</b></span><br/>';
                }

                if ($existingContract[0]['vendor_id'] != Yii::app()->input->post('vendor_id')) {
                    $vendor = new Vendor();
                    $vendorRecord = $vendor->getOne(array('vendor_id' => $existingContract[0]['vendor_id']));
                    $vendorRecordNew = $vendor->getOne(array('vendor_id' => Yii::app()->input->post('vendor_id')));
                    $comment .= '<b>Supplier:</b> <span class="title-text"><b>' . $vendorRecord['vendor_name'] . '</b> Changed to <b>' . $vendorRecordNew['vendor_name'] . '</b></span><br/>';
                }

                if (strtolower(trim($existingContract[0]['contact_name'])) != strtolower(trim(Yii::app()->input->post('contact_name')))) {
                    $comment .= '<b>Contact Name:</b> <span class="title-text"><b>' . $existingContract[0]['contact_name'] . '</b> Changed to <b>' . Yii::app()->input->post('contact_name') . '</b></span><br/>';
                }

                if (strtolower(trim($existingContract[0]['contact_info'])) != strtolower(trim(Yii::app()->input->post('contact_info')))) {
                    $comment .= '<b>Phone:</b> <span class="title-text"><b>' . $existingContract[0]['contact_info'] . ' </b> Changed to <b>' . Yii::app()->input->post('contact_info') . '</b></span><br/>';
                }

                if ($existingContract[0]['location_id'] != Yii::app()->input->post('location_id')) {

                    $location = new Location();
                    $locationPosted = $location->getOne(array('location_id' => Yii::app()->input->post('location_id')));
                    $categoryExisted = $location->getOne(array('location_id' => $existingContract[0]['location_id']));

                    $comment .= '<b>Location:</b> <span class="title-text"><b>' . $categoryExisted['location_name'] . ' </b>Changed to <b>' . $locationPosted['location_name'] . '</b></span><br/>';
                }

                if ($existingContract[0]['department_id'] != Yii::app()->input->post('department_id')) {

                    $department = new Department();
                    $departmentPosted = $department->getOne(array('department_id' => Yii::app()->input->post('department_id')));
                    $departmentExisted = $department->getOne(array('department_id' => $existingContract[0]['department_id']));


                    $comment .= '<b>Department:</b> <span class="title-text"><b>' . $departmentExisted['department_name'] . ' </b>Changed to <b>' . $departmentPosted['department_name'] . '</b></span><br/>';
                }

                if ($existingContract[0]['contract_category_id'] != Yii::app()->input->post('contract_category_id')) {
                    $category = new Category();
                    $categoryPosted = $category->getOne(array('id' => Yii::app()->input->post('contract_category_id')));
                    $categoryExisted = $category->getOne(array('id' => $existingContract[0]['contract_category_id']));
                    $comment .= '<b>Category:</b> <span class="title-text"><b>' . $categoryExisted['value'] . '</b> Changed to <b>' . $categoryPosted['value'] . '</b></span><br/>';
                }

                if ($existingContract[0]['contract_subcategory_id'] != Yii::app()->input->post('contract_subcategory_id')) {

                    $subcategory = new Subcategory();
                    $subcategoryPosted = $subcategory->getOne(array('id' => Yii::app()->input->post('contract_subcategory_id')));
                    $subcategoryExisted = $subcategory->getOne(array('id' => $existingContract[0]['contract_subcategory_id']));

                    $comment .= '<b>Sub Category:</b> <span class="title-text"><b>' . $subcategoryExisted['value'] . '</b> Changed to <b>' . $subcategoryPosted['value'] . '</b></span><br/>';
                }

                if ($existingContract[0]['contract_description'] != Yii::app()->input->post('contract_description')) {
                    $comment .= '<b>Description:</b> <span class="title-text"><b>' . $existingContract[0]['contract_description'] . '</b> Changed to <b>' . Yii::app()->input->post('contract_description') . '</b></span><br/>';
                }

                if ($existingContract[0]['contract_comments'] != Yii::app()->input->post('contract_comments')) {
                    $comment .= '<b>Comments:</b> <span class="title-text"><b>' . $existingContract[0]['contract_comments'] . ' </b>Changed to <b>' . Yii::app()->input->post('contract_comments') . '</b></span><br/>';
                    $doNotNotifySupplier = 'yes';
                }

                if ($existingContract[0]['contract_value'] == 0) {
                    $existingContract[0]['contract_value'] = '0';
                } else {
                    $existingContract[0]['contract_value'] == $existingContract[0]['contract_value'];
                }

                if (/*!empty($financialForm) && */$existingContract[0]['contract_value'] != Yii::app()->input->post('contract_value') /*&& empty($_POST['notification'])*/) {

                    $comment .= '<b>Estimated Total Contract Value:</b> <span class="title-text"><b>' . $existingContract[0]['contract_value'] . '</b> Changed to <b>' . Yii::app()->input->post('contract_value') . '</b></span><br/>';
                }


                if (($existingContract[0]['contract_budget'] == 0)) {
                    $existingContract[0]['contract_budget'] = '';
                } else if (!empty($financialForm)) {
                    $existingContract[0]['contract_budget'] = $existingContract[0]['contract_budget'];
                }

                if ($existingContract[0]['contract_budget'] != Yii::app()->input->post('contract_budget')) {
                    $comment .= '<b>Estimated Annual Contract Value:</b> <span class="title-text"><b>' . $existingContract[0]['contract_budget'] . ' </b>Changed to <b>' . Yii::app()->input->post('contract_budget') . '</b></span><br/>';
                }

                if ($existingContract[0]['original_budget'] == 0) {
                    $existingContract[0]['original_budget'] = '';
                } else {
                    $existingContract[0]['original_budget'] = $existingContract[0]['original_budget'];
                }

                if ($existingContract[0]['original_budget'] != Yii::app()->input->post('original_budget')) {
                    $comment .= '<b>Original Contract Value:</b> <span class="title-text"><b>' . $existingContract[0]['original_budget'] . ' </b>Changed to <b>' . Yii::app()->input->post('original_budget') . '</b></span><br/>';
                }
                if ($existingContract[0]['original_annual_budget'] == 0) {
                    $existingContract[0]['original_annual_budget'] = '';
                } else {
                    $existingContract[0]['original_annual_budget'] = $existingContract[0]['original_annual_budget'];
                }

                if ($existingContract[0]['original_annual_budget'] != Yii::app()->input->post('original_annual_budget')) {
                    $comment .= '<b>Original Contract Annual Value:</b> <span class="title-text"><b>' . $existingContract[0]['original_annual_budget'] . ' </b> Changed to <b>' . Yii::app()->input->post('original_annual_budget') . '</b></span><br/>';
                }

                if ($existingContract[0]['po_number'] != Yii::app()->input->post('po_number')) {
                    $comment .= '<b>PO Number:</b> <span class="title-text"><b>' . $existingContract[0]['po_number'] . ' </b>Change to <b>' . Yii::app()->input->post('po_number') . '</b></span><br/>';
                }
                if ($existingContract[0]['cost_centre'] != Yii::app()->input->post('cost_centre')) {
                    $comment .= '<b>Cost Centre:</b> <span class="title-text"><b>' . $existingContract[0]['cost_centre'] . '</b> Changed to <b>' . Yii::app()->input->post('cost_centre') . '</b></span><br/>';
                }

                $starDateP = !empty(Yii::app()->input->post('contract_start_date')) ? Yii::app()->input->post('contract_start_date') : '0000-00-00';
                $starDateE = $existingContract[0]['contract_start_date'];

                if (FunctionManager::dateFormat() == "d/m/Y")
                    $starDatePFormated = date(FunctionManager::dateFormat(), strtotime(strtr($starDateP, '/', '-')));
                else {
                    $starDatePFormated = date(FunctionManager::dateFormat(), strtotime($starDateP));
                }
                $starDateEFormated = date(FunctionManager::dateFormat(), strtotime($starDateE));
                if ($starDateEFormated != $starDatePFormated) {
                    if ($starDateE == "0000-00-00") {
                        $startDate = "NULL";
                    } else {
                        $startDate = date(FunctionManager::dateFormat(), strtotime($starDateE));
                    }
                    $comment .= '<b>Contract Start Date:</b> <span class="title-text"><b>' . $startDate . '</b> Changed to <b>' . $starDateP . '</b></span><br/>';
                }

                $endDateP = !empty(Yii::app()->input->post('contract_end_date')) ? Yii::app()->input->post('contract_end_date') : '0000-00-00';
                $endDateE = $existingContract[0]['contract_end_date'];

                if (FunctionManager::dateFormat() == "d/m/Y")
                    $endDatePFormated = date(FunctionManager::dateFormat(), strtotime(strtr($endDateP, '/', '-')));
                else {
                    $endDatePFormated = date(FunctionManager::dateFormat(), strtotime($endDateP));
                }
                $endDateEFormated = date(FunctionManager::dateFormat(), strtotime($endDateE));


                if ($endDatePFormated != $endDateEFormated) {

                    if ($endDateE  == "0000-00-00") {
                        $endDate = "NULL";
                    } else {
                        $endDate = date(FunctionManager::dateFormat(), strtotime($endDateE));
                    }

                    $comment .= '<b>Contract End Date:</b> <span class="title-text"><b>' . $endDate . ' </b>Changed to <b>' . $endDateP . '</b></span><br/>';
                }


                $endDateP = !empty(Yii::app()->input->post('original_end_date')) ? Yii::app()->input->post('original_end_date') : '0000-00-00';
                $endDateE = $existingContract[0]['original_end_date'];

                if (FunctionManager::dateFormat() == "d/m/Y")
                    $endDatePFormated = date(FunctionManager::dateFormat(), strtotime(strtr($endDateP, '/', '-')));
                else {
                    $endDatePFormated = date(FunctionManager::dateFormat(), strtotime($endDateP));
                }
                $endDateEFormated = date(FunctionManager::dateFormat(), strtotime($endDateE));


                if ($endDatePFormated != $endDateEFormated) {
                    if ($endDateE == "0000-00-00") {
                        $originalEndDate = "NULL";
                    } else {
                        $originalEndDate = date(FunctionManager::dateFormat(), strtotime($endDateE));
                    }
                    // $comment .= '<b>Original End Date:</b> <span class="title-text"><b>'.$originalEndDate. ' </b>Changed to <b>'.$endDateP.'</b></span><br/>';
                }
                $noticePeriodP = !empty(Yii::app()->input->post('break_clause')) ? Yii::app()->input->post('break_clause') : '0000-00-00';
                $noticePeriodE = $existingContract[0]['break_clause'];

                if (FunctionManager::dateFormat() == "d/m/Y")
                    $noticePeriodPFormated = date(FunctionManager::dateFormat(), strtotime(strtr($noticePeriodP, '/', '-')));
                else {
                    $noticePeriodPFormated = date(FunctionManager::dateFormat(), strtotime($noticePeriodP));
                }
                $noticePeriodEFormated = date(FunctionManager::dateFormat(), strtotime($noticePeriodE));
                if ($noticePeriodPFormated != $noticePeriodEFormated) {

                    if ($noticePeriodE == "0000-00-00 00:00:00") {
                        $noticeDate = "NULL";
                    } else {
                        $noticeDate = date(FunctionManager::dateFormat(), strtotime($noticePeriodE));
                    }

                    $comment .= '<b>Notice Period Date:</b> <span class="title-text"><b>' . $noticeDate . ' </b>Changed to <b>' . $noticePeriodP . '</b></span><br/>';
                    $doNotNotifySupplier = 'yes';
                }
                if ($existingContract[0]['termination_terms'] != Yii::app()->input->post('termination_terms')) {
                    $comment .= '<b>Termination Terms:</b> <span class="title-text"><b>' . $existingContract[0]['termination_terms'] . ' </b>Changed to <b>' . Yii::app()->input->post('termination_terms') . '</b></span><br/>';
                }
                if ($existingContract[0]['termination_comments'] != Yii::app()->input->post('termination_comments')) {
                    $comment .= '<b>Termination Comments:</b> <span class="title-text"><b>' . $existingContract[0]['termination_comments'] . ' </b>Changed to <b>' . Yii::app()->input->post('termination_comments') . '</b></span><br/>';
                }

                if ($existingContract[0]['extension_options'] != Yii::app()->input->post('extension_options')) {
                    $comment .= '<b>Extension Options:</b> <span class="title-text"><b>' . $existingContract[0]['extension_options'] . '</b> Changed to <b>' . Yii::app()->input->post('extension_options') . '</b></span><br/>';
                }

                if ($existingContract[0]['extension_comments'] != Yii::app()->input->post('extension_comments')) {
                    $comment .= '<b>Extension Comments:</b> <span class="title-text"><b>' . $existingContract[0]['extension_comments'] . ' </b>Changed to <b>' . Yii::app()->input->post('extension_comments') . '</b></span><br/>';
                }
            }
            

            if (!empty(Yii::app()->input->post('contractId'))) {
                // $contract->changeToContract(Yii::app()->input->post('contractId'));
                $existingContract = $contract->madeContractLogComment(Yii::app()->input->post('contractId'));
                // save contracts logs

                if (!empty($comment)) {
                    $contractLog = new ContractLog();
                    $contractLog->rs = array();
                    $contractLog->rs['contract_id'] = Yii::app()->input->post('contractId');
                    $contractLog->rs['user_id'] = Yii::app()->session['user_id'];
                    $contractLog->rs['contract_status'] = Yii::app()->input->post('contract_status');
                    $contractLog->rs['comment'] = !empty($comment) ? $comment : "";
                    $contractLog->rs['created_datetime'] = date('Y-m-d H:i:s');
                    $contractLog->rs['updated_datetime'] = date('Y-m-d H:i:s');
                    $contractLog->write();

                    if ($_POST['contract_comments']) {
                        $contractCommentHistory      =  new ContractCommentsHistory();
                        $contractCommentHistory->rs  =  array();
                        $contractCommentHistory->rs['contract_id'] = Yii::app()->input->post('contract_id');
                        $contractCommentHistory->rs['user_id'] = Yii::app()->session['user_id'];
                        $contractCommentHistory->rs['comment']  =  $_POST['contract_comments'];
                        $contractCommentHistory->rs['datetime'] =   date('Y-m-d H:i:s');

                        $contractCommentHistory->write();
                    }
                    $contractName = Yii::app()->input->post('contract_title');
                    $logComments = !empty($comment) ? $comment : "";
                    $notificationComments = "Change to Contract <b>" . $contractName . "</b> - " . $logComments;

                    $notificationContID = Yii::app()->input->post('contractId');
                    $vendor_id   = Yii::app()->input->post('vendor_id');
                    $user_id     = Yii::app()->input->post('user_id');
                    $sbo_user_id = Yii::app()->input->post('sbo_user_id');
                    $commercial_user_id  = Yii::app()->input->post('commercial_user_id');
                    $procurement_user_id = Yii::app()->input->post('procurement_user_id');
                    $additional_user_1   = Yii::app()->input->post('additional_user_1');
                    $additional_user_2   = Yii::app()->input->post('additional_user_2');
                    //$userArr = array($user_id,$sbo_user_id,$commercial_user_id,$procurement_user_id);
                    $userArr = array(
                        $user_id     => $user_id,
                        $sbo_user_id => $sbo_user_id,
                        $commercial_user_id  => $commercial_user_id,
                        $procurement_user_id => $procurement_user_id,
                        $additional_user_1   => $additional_user_1,
                        $additional_user_2   => $additional_user_2
                    );
                    // $userArr = array($user_id=>$user_id,$sbo_user_id=>$sbo_user_id,$commercial_user_id=>$commercial_user_id,$procurement_user_id=>$procurement_user_id);

                    foreach ($userArr as $value) {
                        if (!empty($value)) {

                            $sql  = 'select full_name,email from users where user_id=' . $value;
                            $userReader = Yii::app()->db->createCommand($sql)->queryRow();

                            $notification = new Notification();
                            $notification->rs = array();
                            $notification->rs['id'] = 0;
                            $notification->rs['user_type'] = 'Client';
                            $notification->rs['notification_flag'] = 'Contract Change log';
                            $notification->rs['user_id'] = $value;
                            $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) . '">' . $notificationComments . '</a>';
                            $notification->rs['notification_date'] = date("Y-m-d H:i");
                            $notification->rs['read_flag'] = 0;
                            $notification->rs['notification_type'] = 'Contract';
                            $notification->write();

                            $email = $userReader['email'];
                            $name  = $userReader['full_name'];
                            $subject = 'Change To Contract';
                            $notificationText = '<a href="' . AppUrl::bicesUrl('contracts/edit/' . $notificationContID) . '" style="font-size:18px;color:#54595F;text-decoration: none;">' . $notificationComments . '</a>';
                            $url = AppUrl::bicesUrl('contracts/edit/' . $notificationContID);
                            // EmailManager::userNotification($email,$name,$subject,$notificationText,$url);
                        }
                    }
                    // Vendor Notification 
                    Yii::app()->user->setFlash('success', "Contract updated successfully.");
                }
            }

            if (!empty($_POST['created_by_modal'])) {
                $data = array();
                $data['contract_id'] = $contract_id;
                echo json_encode($data);
                exit;
            }

            $total_documents = Yii::app()->input->post('total_documents');
            for ($idx = 1; $idx <= $total_documents; $idx++) {
                $delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idx);
                if ($delete_document_flag) continue;

                if (isset($_FILES['contract_file_' . $idx]) && is_array($_FILES['contract_file_' . $idx])) {
                    if (!is_dir('uploads/contracts')) mkdir('uploads/contracts');
                    if (!is_dir('uploads/contracts/' . $contract->rs['contract_id']))
                        mkdir('uploads/contracts/' . $contract->rs['contract_id']);

                    if (!isset($_FILES['contract_file_' . $idx]['error']) || empty($_FILES['contract_file_' . $idx]['error'])) {
                        $file_name = $_FILES['contract_file_' . $idx]['name'];
                        move_uploaded_file($_FILES['contract_file_' . $idx]['tmp_name'], 'uploads/contracts/' . $contract->rs['contract_id'] . '/' . $file_name);

                        $file_description = Yii::app()->input->post('file_desc_' . $idx);
                        $contract->saveContractFile($contract->rs['contract_id'], $file_name, $file_description);
                    }
                }
            }
            if (empty($contract_id)) {
                $contract_id = $contract->rs['contract_id'];
            }

            $uploadedFiles = Yii::app()->session['uploaded_files'];

            if (!empty($uploadedFiles))
                foreach ($uploadedFiles as $fileValue) {
                    if (empty($fileValue['contract_id'])) {
                        $file_name = $fileValue['file_name'];
                        $file_description = $fileValue['file_description'];
                        $document_type = $fileValue['file_type'];
                        $document_status = $fileValue['document_status'];
                        $expiry_date = $fileValue['expiry_date'];

                        if (!is_dir('uploads/vendor_documents')) mkdir('uploads/contract_documents');
                        if (!is_dir('uploads/contract_documents/' . $contract_id))
                            mkdir('uploads/contract_documents/' . $contract_id);
                        if (!is_dir('uploads/contract_documents/'))
                            mkdir('uploads/contract_documents/');
                        if (@rename('uploads/contract_documents/' . $file_name, 'uploads/contract_documents/' . $contract_id . '/' . $file_name)) {
                            $contract->saveContractDocument($contract_id, $file_description, $file_name, $document_type, $document_status, $expiry_date);
                        }
                    }
                }
            if (!empty($summaryForm)) {
                $this->redirect(AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=summary'));
            } else if (!empty($financialForm)) {
                $this->redirect(AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=finance'));
            } else if (!empty($timelineForm)) {
                $this->redirect(AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=timeline'));
            } else if (!empty($peopleForm)) {
                $this->redirect(AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=people'));
            } else if (!empty($commentsForm)) {
                $this->redirect(AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=comments'));
            } else if (!empty($notificationForm)) {
                $this->redirect(AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=notification'));
            } else if (!empty($documentForm)) {
                $this->redirect(AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=document'));
            } else {
                $view_data['contract'] = $contract->getContracts($contract_id);
                $this->redirect(AppUrl::bicesUrl('contracts/list'));
            }
        }
        // get data for the selected contract including notifications if any
        $view_data = array(
            'contract_id' => $contract_id
        );

        $contract = new Contract();
        
        $saving = new Saving();
        $view_data['contract'] = $contract->getContracts($contract_id);
        $view_data['savings'] = $saving->getContractSavings($contract_id);

		$contract_location = new ContractLocation();
		$contract_location = $contract_location->getData(array('contract_id' => $contract_id));
        $view_data['contract_locationIDs'] = array_column($contract_location, 'location_id');

        $contract_department = new ContractDepartment();
		$contract_department = $contract_department->getData(array('contract_id' => $contract_id));
		$view_data['contract_departmentIDs'] = array_column($contract_department, 'department_id');

        $view_data['notifications'] = array();
        if ($contract_id) {
            $notification = new ContractNotification();
            $view_data['notifications'] = $notification->getNotifications($contract_id);
        }

        // need some drop down values
        $location = new Location();
        $view_data['locations'] = $location->getData(array(
            'order' => 'location_name'
        ));

        $project = new Project();
        $view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

        $category = new Category();
        $view_data['categories'] = $category->getAll();

        // other drop downs may be needed for vendor quick add modal window
        $payment_term = new PaymentTerm();
        $view_data['payment_terms'] = $payment_term->getAll();

        $shipping_method = new ShippingMethod();
        $view_data['shipping_methods'] = $shipping_method->getAll();

        $currency_rate = new CurrencyRate();
        $view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

        $industry = new Industry();
        $view_data['industries'] = $industry->getAll(array(
            'ORDER' => 'value'
        ));

        $contractType = new ContractType();
        $view_data['contract_types'] = $contractType->getAll();

        $contractStatus = new ContractStatus();
        $view_data['contract_statuses'] = $contractStatus->getAll();

        $vendor = new Vendor();

        
        $vendor_id = $view_data['contract']['vendor_id'];
        $view_data['quickComparison'] = $vendor->getQuickComparision($vendor_id);
        $contractLogs = new ContractLog();
        $view_data['contractLogs'] = $contractLogs->getContractLogs($contract_id);
        $contractCommHistory = new ContractCommentsHistory();
        $view_data['contractCommHistory'] = $contractCommHistory->getcontractCommentsHistory($contract_id);

        $view_data['documentList'] = $contract->getContractsDocuments($contract_id, "active");
        $view_data['documenArchivetList'] = $contract->getContractsDocuments($contract_id, "archive");
        $view_data['contractStatistics'] = $contract->getContractStatistics($contract_id);
        $quote = new Quote();

        $quoteRecord = $quote->getOne(array('awarded_contract_id' => $contract_id));
        if (!empty($quoteRecord)) {
            $quote_id = $quoteRecord['quote_id'];
            $view_data['quote'] = $quoteRecord;
            $view_data['scoringCriteria'] = $quote->scoringCriteria($quote_id);
            $view_data['quoteVendors'] = $quote->getQuoteVendors($quote_id);
            $view_data['quote_stacked_bar_graph'] = $quote->getQuoteGraph($quote_id, '');
            $view_data['quoteAnswerVendor'] = $quote->getAnwserVendors($quote_id);
            //Yii::app()->user->setFlash('success', "User added successfully.");
        }

        // and display
        $view_data['filter_document_type'] = !empty($_POST['filter_document_type']) ? $_POST['filter_document_type'] : '';
        $view_data['flter_document_status'] = !empty($_POST['flter_document_status']) ? $_POST['flter_document_status'] : '';
        $view_data['fltr_doc_contract_supplier_status'] = !empty($_POST['fltr_doc_contract_supplier_status']) ? $_POST['fltr_doc_contract_supplier_status'] : '';
        unset(Yii::app()->session['uploaded_files_display']);
        unset(Yii::app()->session['uploaded_files']);
        unset(Yii::app()->session['quick_vendor']);
        $this->render('edit', $view_data);
        $this->renderPartial('/vendors/modal_add', $view_data);
    }
    public function actionArchiveEdit($contract_id = 0)
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'contracts_edit';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(1, 3, 4)))
            $this->redirect(AppUrl::bicesUrl('home'));

        $view_data = array(
            'contract_id' => $contract_id
        );
        $contract = new Contract();
        $view_data['contract'] = $contract->getArchiveContracts($contract_id);

        $view_data['notifications'] = array();
        if ($contract_id) {
            $notification = new ContractNotification();
            $view_data['notifications'] = $notification->getNotifications($contract_id);
        }

        // need some drop down values
        $location = new Location();
        $view_data['locations'] = $location->getData(array(
            'order' => 'location_name'
        ));

        $project = new Project();
        $view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

        $category = new Category();
        $view_data['categories'] = $category->getAll();

        // other drop downs may be needed for vendor quick add modal window
        $payment_term = new PaymentTerm();
        $view_data['payment_terms'] = $payment_term->getAll();

        $shipping_method = new ShippingMethod();
        $view_data['shipping_methods'] = $shipping_method->getAll();

        $currency_rate = new CurrencyRate();
        $view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

        $industry = new Industry();
        $view_data['industries'] = $industry->getAll(array(
            'ORDER' => 'value'
        ));

        $contractType = new ContractType();
        $view_data['contract_types'] = $contractType->getAll();

        $contractStatus = new ContractStatus();
        $view_data['contract_statuses'] = $contractStatus->getAll();

        $vendor = new Vendor();
        $vendor_list = $vendor->getAll(array('active' => 1, 'member_type' => 0, 'ORDER' => 'vendor_name', 'limit' => 100));
        $view_data['vendor_list'] = $vendor_list;

        $contractLogs = new ContractLog();
        $view_data['contractLogs'] = $contractLogs->getContractLogs($contract_id);

        $view_data['documentList'] = $contract->getContractsDocuments($contract_id);
        $view_data['contractStatistics'] = $contract->getContractStatistics($contract_id, 'archive_contracts');
        //CVarDumper::dump($view_data['contractLogs'],10,1);die;

        // and display
        unset(Yii::app()->session['uploaded_files_display']);
        unset(Yii::app()->session['uploaded_files']);
        $this->render('archive_edit', $view_data);
        $this->renderPartial('/vendors/modal_add', $view_data);
    }



    public function actionUploadDocument()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        if (!empty($_FILES)) {
            $contract = new Contract;
            if (isset($_FILES['file']) && is_array($_FILES['file'])) {
                $contractID = $_POST['contract_id'];
                if (empty($contractID)) {
                    $contractID = '';
                }
                $path = Yii::getPathOfAlias('webroot') . '/uploads/contract_documents';
                if (!is_dir($path)) mkdir($path);
                if (!is_dir($path . '/' . $contractID))
                    mkdir($path . '/' . $contractID);
                if (!is_dir($path . '/'))
                    mkdir($path . '/');

                if (!empty($_FILES['file']['name'])) {
                    $file_name = time() . '_' . $_FILES['file']['name'];
                    $file_name = str_replace("'", "", $file_name);
                    $file_name = str_replace('"', '', $file_name);
                    $file_name = str_replace(' ', '_', $file_name);
                    $file_description = addslashes($_POST['file_description']);
                    $document_type = $_POST['document_type'];
                    $document_status = $_POST['document_status'];
                    $expiry_date = $_POST['expiry_date'];

                    $uploadedFiles = Yii::app()->session['uploaded_files'];

                    $uploadedFiles[] = array('file_name' => $file_name, 'file_description' => $file_description, 'contract_id' => $contractID, 'file_type' => $document_type, 'document_status' => $document_status, 'expiry_date' => $expiry_date);


                    //move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/orders/' . $file_name);
                    if (move_uploaded_file($_FILES['file']['tmp_name'], $path . '/' . (!empty($contractID) ? $contractID . '/' . $file_name : $file_name))) {

                        Yii::app()->session['uploaded_files'] = $uploadedFiles;

                        if (!empty($contractID)) {
                            $contract->saveContractDocument($contractID, $file_description, $file_name, $document_type, $document_status, $expiry_date);
                            $documentList = $contract->getContractsDocuments($contractID);
                        } else {
                            $uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
                            $uploadedFilesDisplay[$document_type][] =  array(
                                'id' => 0,
                                'document_file' => $file_name,
                                'document_title' => $file_description,
                                'contract_id' => 0,
                                'status' => $document_status,
                                'expiry_date' => $expiry_date,
                                'date_created' => date("Y-m-d")
                            );
                            Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
                            $documentList = Yii::app()->session['uploaded_files_display'];
                        }
                    }

                    $url = AppUrl::bicesUrl('contracts/edit/' . $contractID . '?tab=document');
                    echo json_encode(['url' => $url]);
                    exit;
                }
            }
        }
    }

    public function actionApproveDocument()
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $contract_id = Yii::app()->input->post('contract_id');
        $contract = new Contract;
        if (null != Yii::app()->input->post('document_ids')) {
            $documentIDs = Yii::app()->input->post('document_ids');
            $contract->approveDocument($documentIDs);
        }

        $url = AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=document');
        echo json_encode(['url' => $url]);
        exit;
    }

    public function actionDocumentArchive()
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $contract_id = Yii::app()->input->post('contract_id');
        $contract = new Contract;
        if (null != Yii::app()->input->post('document_ids')) {
            $documentIDs = Yii::app()->input->post('document_ids');
            $contract->archiveDocument($documentIDs);
        }
        $url = AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=document');
        echo json_encode(['url' => $url]);
        exit;
    }

    public function actionDocumentUnArchive()
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $contract_id = Yii::app()->input->post('contract_id');
        $contract = new Contract;
        if (null != Yii::app()->input->post('document_ids')) {
            $documentIDs = Yii::app()->input->post('document_ids');
            $contract->unArchiveDocument($documentIDs);

            $contractDetail = $contract->getOne(array('contract_id' => $contract_id));
            $location_id = $contractDetail['location_id'];
            $department_id = $contractDetail['department_id'];
            //$this->renderPartial('_documents',array('documentList'=>$contract->getContractsDocuments($contract_id),'documenArchivetList'=>$contract->getContractsDocuments($contract_id,"archive"),'contract_id'=>$contract_id,'location_id'=>$location_id,'department_id'=>$department_id));

        }

        $url = AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=document');
        echo json_encode(['url' => $url]);
        exit;
    }


    public function actionEditDocument()
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $contract = new Contract;
        $postedArr = $_POST;
        $contract->editDocument($postedArr);
        $contract_id = $_POST['contract_id'];
        $url = AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=document');
        echo json_encode(['url' => $url]);
        exit;
    }

    public function actionDocumentDownload()
    {
        $id = $_GET['id'];
        $sql = "select document_file,contract_id from contract_documents WHERE id =" . $id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
        $contractID = $fileReader['contract_id'];

        $file = Yii::app()->basePath . "/../uploads/contract_documents/" . $contractID . "/" . $fileReader['document_file'];

        if ($fileReader) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            //header("Content-Type: application/pdf");
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function actionDeleteDocument()
    {
        $id = $_POST['documentID'];
        $contractID = 0;
        //$sql = "select document_file,vendor_id from vendor_documents WHERE status='Pending' and id =".$id;
        $sql = "select document_file,contract_id from contract_documents WHERE id =" . $id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
        if (!empty($fileReader)) {
            $contractID = $fileReader['contract_id'];
            $fileName = $fileReader['document_file'];
            $file = Yii::app()->basePath . "/../uploads/contract_documents/" . $contractID . "/" . $fileName;
            if (unlink($file)) {
                //$sql = "delete from vendor_documents WHERE status='Pending' and id =".$id;
                $sql = "delete from contract_documents WHERE  id =" . $id;
                $fileReader = Yii::app()->db->createCommand($sql)->execute();
            }
        }
        $contract = new Contract;

        $contractDetail = $contract->getOne(array('contract_id' => $contractID));
        $location_id = $contractDetail['location_id'];
        $department_id = $contractDetail['department_id'];

        $this->renderPartial('_documents', array('documentList' => $contract->getContractsDocuments($contractID), 'documenArchivetList' => $contract->getContractsDocuments($contractID, "archive"), 'contract_id' => $contractID, 'location_id' => $location_id, 'department_id' => $department_id));
    }

    public function actionLoadingRequestDocument()
    {
        $contract_id = $_POST['contract_id'];
        $contract = new Contract;
        $documentListPendingRequest = $contract->getContractsDocumentsPendingRequest($contract_id);
        $this->renderPartial('_contract_document_pending', array('documentListPendingRequest' => $documentListPendingRequest, 'contract_id' => $contract_id));
    }

    public function actionDeleteDocumentPending()
    {
        $id = $_POST['request_id'];
        $contract_id = $_POST['contract_id'];
        $sql = "delete from contract_request_documents WHERE  id =" . $id;
        $fileReader = Yii::app()->db->createCommand($sql)->execute();
        $contract = new Contract;
        $documentListPendingRequest = $contract->getContractsDocumentsPendingRequest($contract_id);
        $this->renderPartial('_contract_document_pending', array('documentListPendingRequest' => $documentListPendingRequest, 'contract_id' => $contract_id));
    }

    public function actionDeleteDocumentOther()
    {
        $ids = explode("_", $_POST['documentID']);

        $uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
        $documentFileDislplay = $uploadedFilesDisplay[$ids[0]][$ids[1]]['document_file'];
        unset($uploadedFilesDisplay[$ids[0]][$ids[1]]);
        if (empty($uploadedFilesDisplay[$ids[0]])) {
            unset($uploadedFilesDisplay[$ids[0]]);
        }
        Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
        $documentList = Yii::app()->session['uploaded_files_display'];

        $uploadedFiles = Yii::app()->session['uploaded_files'];
        $uploadedFilesRowKey = '';
        foreach ($uploadedFiles as $key => $fileVal) {
            if ($fileVal['file_name'] == $documentFileDislplay) {
                $uploadedFilesRowKey = $key;
                break;
            }
        }
        unset($uploadedFiles[$uploadedFilesRowKey]);


        Yii::app()->session['uploaded_files'] = $uploadedFiles;
        if (!empty($documentFileDislplay)) {
            $file = Yii::app()->basePath . "/../uploads/contract_documents/" . $documentFileDislplay;
            if (unlink($file)) {
            }
        }
        $vendor = new Vendor;
        $contract = new Contract;
        $contractDetail = $contract->getOne(array('contract_id' => $contractID));
        $location_id = $contractDetail['location_id'];
        $department_id = $contractDetail['department_id'];
        $this->renderPartial('_documents', array('documentList' => $documentList, 'contractID' => 0, 'location_id' => $location_id, 'department_id' => $department_id));
    }

    public function actionRequestContractDocument()
    {
        $msg = 0;

        if (!empty($_POST['request_document_vendor_id']) && !empty($_POST['request_document_contract_id'])) {
            $userID   = Yii::app()->session['user_id'];
            $userName = Yii::app()->session['full_name'];
            $dateTime = date("Y-m-d H:i:s");
            $vendorID = $_POST['request_document_vendor_id'];
            //$contractIDs = $_POST['contract_id'];

            if (!empty($_POST['contract_type_new'])) {
                $contractTypeNew = addslashes($_POST['contract_type_new']);
                $sql = "INSERT INTO `contract_document_type` (`id`, `name`, `created_type`, `btn_style`, `created_at`) VALUES (NULL, '" . $contractTypeNew . "', 'Cusom', NULL, '" . $dateTime . "');";
                $reader = Yii::app()->db->createCommand($sql)->execute();
                $contractType = Yii::app()->db->lastInsertID;
            } else {
                $contractType = $_POST['document_contract_type'];
            }

            $vendorArr = $_POST['vendor_contact_name'][0];
            $vendorContactName = explode(" - ", $vendorArr);

            $contactName  = serialize($vendorContactName[0]);
            $contactEmail = serialize($vendorContactName[1]);

            $note   = addslashes($_POST['request_document_note']);
            $contractID = $_POST['request_document_contract_id'];

            $reference = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24 / strlen($x)))), 1, 24);
            $contact = new ContractRequestDocument();
            $contact->rs = array();
            $contact->rs['vendor_id']       = $vendorID;
            $contact->rs['contract_id']    = $contractID;
            $contact->rs['user_id'] = $userID;
            $contact->rs['user_name'] = $userName;
            $contact->rs['contract_type'] = $contractType;
            $contact->rs['supplier_contact_name'] = $contactName;
            $contact->rs['supplier_contact_email'] = $contactEmail;
            $contact->rs['note'] = $note;
            $contact->rs['status'] = 'Pending';
            $contact->rs['reference'] = $reference;
            $contact->rs['created_at'] = $dateTime;

            $contact->write();
            $data = array();
            if (!empty($contact->rs['id'])) {
                $msg = 1;
                $cronEmail = new CronEmail;
                $cronEmail->rs = array();
                $cronEmail->rs['record_id'] = $contact->rs['id'];
                $cronEmail->rs['send_to'] = 'Vendor';
                $cronEmail->rs['user_id'] = $vendorID;
                $cronEmail->rs['user_type'] = 'Request Contract Document';
                $cronEmail->rs['status'] = 'Pending';
                $cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
                $cronEmail->write();
            }
        }

        $data = array();
        $url = AppUrl::bicesUrl('contracts/edit/' . $contractID . '?tab=document');
        $data['msg'] = $msg;
        $data['url'] = $url;
        echo json_encode($data);
        exit;
    }


    public function actionCreateByModal()
    {
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(1, 3, 4)))
            $this->redirect(AppUrl::bicesUrl('home'));

        // get data for the selected contract including notifications if any

        $contract_id = 0;
        $view_data = array(
            'contract_id' => $contract_id
        );
        $contract = new Contract();
        $view_data['contract'] = $contract->getContracts($contract_id);

        $view_data['notifications'] = array();
        if ($contract_id) {
            $notification = new ContractNotification();
            $view_data['notifications'] = $notification->getNotifications($contract_id);
        }
        // need some drop down values
        $location = new Location();
        $view_data['locations'] = $location->getData(array(
            'order' => 'location_name'
        ));
        $project = new Project();
        $view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

        $category = new Category();
        $view_data['categories'] = $category->getAll(array('soft_deleted' => "0"));

        // other drop downs may be needed for vendor quick add modal window
        $payment_term = new PaymentTerm();
        $view_data['payment_terms'] = $payment_term->getAll();

        $shipping_method = new ShippingMethod();
        $view_data['shipping_methods'] = $shipping_method->getAll();

        $currency_rate = new CurrencyRate();
        $view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

        $industry = new Industry();
        $view_data['industries'] = $industry->getAll(array(
            'ORDER' => 'value'
        ));

        $contractType = new ContractType();
        $view_data['contract_types'] = $contractType->getAll();

        $contractStatus = new ContractStatus();
        $view_data['contract_statuses'] = $contractStatus->getAll();

        $contractLogs = new ContractLog();
        $view_data['contractLogs'] = $contractLogs->getContractLogs($contract_id);

        //CVarDumper::dump($view_data['contractLogs'],10,1);die;

        // and display
        unset(Yii::app()->session['uploaded_files']);
        $this->renderPartial('create_contract_modal', $view_data);
        $this->renderPartial('/vendors/modal_add', $view_data);
        exit;
    }

    public function actionSaveArhive()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $this->checklogin();
        $contractID = $_GET['contract-id'];

        if (empty($contractID)) {
            $this->redirect(AppUrl::bicesUrl('contracts/list'));
        }

        if (empty($_GET['archive-type'])) {

            //$sql = "INSERT INTO archive_contracts SELECT * FROM contracts WHERE contracts.contract_id=".$contractID;
            $sql = "update contracts set contract_archive='yes' WHERE contracts.contract_id=" . $contractID;
            $archive = Yii::app()->db->createCommand($sql)->execute();
            if ($archive) {

                $userID   = Yii::app()->session['user_id'];
                $userName = Yii::app()->session['full_name'];
                $dateTime = date("Y-m-d H:i:s");

                $sql = "INSERT INTO archive_contracts_log (contract_id,user_id,user_name,type,created_datetime) VALUES  ('" . $contractID . "','" . $userID . "','" . $userName . "','Archived','" . $dateTime . "')";
                $archiveLog = Yii::app()->db->createCommand($sql)->execute();

                /* $sql = "delete from contracts where contract_id=".$contractID;
                $contract = Yii::app()->db->createCommand($sql)->execute();*/

                Yii::app()->user->setFlash('success', '<strong>Success! </strong>Contract archived successfully.');
            }
            $this->redirect(AppUrl::bicesUrl('contracts/list'));
        } else if (!empty($_GET['archive-type']) && $_GET['archive-type'] == 'un-archive') {
            //$sql = "INSERT INTO contracts SELECT * FROM archive_contracts WHERE archive_contracts.contract_id=".$contractID;
            $sql = "update contracts set contract_archive=NULL WHERE contracts.contract_id=" . $contractID;
            $archive = Yii::app()->db->createCommand($sql)->execute();
            if ($archive) {

                $userID   = Yii::app()->session['user_id'];
                $userName = Yii::app()->session['full_name'];
                $dateTime = date("Y-m-d H:i:s");

                $sql = "INSERT INTO archive_contracts_log (contract_id,user_id,user_name,type,created_datetime) VALUES ('" . $contractID . "','" . $userID . "','" . $userName . "','Un-archive','" . $dateTime . "')";
                $archiveLog = Yii::app()->db->createCommand($sql)->execute();

                /* $sql = "delete from archive_contracts where contract_id=".$contractID;
                $contract = Yii::app()->db->createCommand($sql)->execute();*/

                Yii::app()->user->setFlash('success', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success! </strong>Contract Un-archived successfully.</div>');
            }
            $this->redirect(AppUrl::bicesUrl('contracts/archiveList'));
        }
        //$this->redirect(AppUrl::bicesUrl('contracts/edit',array('id'=>$contractID));
    }

    public function actionTitlecontract()
    {
        $bla = new Contract();
        $abc = $bla->getContractsTitle();
        print_r($abc);
        exit();
    }

    public function actionDeleteFile()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $contract = new Contract();

        // which contract record the file relates to?
        $contract_id = Yii::app()->input->post('contract_id');
        $file_name = Yii::app()->input->post('file_name');

        // directory where this file resides
        $complete_file_name = 'uploads/contracts/' . $contract_id . '/' . $file_name;
        if (is_file($complete_file_name))
            @unlink($complete_file_name);

        $contract->deleteFile($contract_id, $file_name);
    }

    public function actionLoadFileOld()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';

        // which contract record the file relates to?
        $contract_id = Yii::app()->input->post('id');
        $file_name = Yii::app()->input->post('file_name');

        echo '<iframe style="width: 800px; min-height: 630px;" src="' . Yii::app()->baseUrl . '/uploads/contracts/' . $contract_id . '/' . $file_name . '"></iframe>';
        die;
        $iframe =  '<iframe style="width: 800px; min-height: 630px;" src="' . Yii::app()->baseUrl . '/uploads/contracts/' . $contract_id . '/' . $file_name . '"></iframe>';
        return $file_name;
    }

    public function actionLoadFile($id, $file)
    {
        $ext = explode('.', $file);
        //CVarDumper::dump($ext,10,1);die;
        if ($ext[1] == 'docx' || $ext[1] == 'doc') {
            echo $iframe =  '<iframe style="width: 800px; min-height: 630px;" src="https://docs.google.com/gview?url=http://dev.spend-365.com/uploads/contracts/' . $id . '/' . $file . '&embedded=true"></iframe>';
        } else {
            echo $iframe =  '<iframe style="width: 800px; min-height: 630px;" src="' . Yii::app()->baseUrl . '/uploads/contracts/' . $id . '/' . $file . '"></iframe>';
        }
    }

    public function actionImport()
    {
        // is someone already logged in?
        $this->checklogin();
        if (!isset(Yii::app()->session['user_type']) || Yii::app()->session['user_type'] != 1)
            $this->redirect(AppUrl::bicesUrl('home'));
        else {
            if (!isset(Yii::app()->session['admin_flag']) || Yii::app()->session['admin_flag'] != 1)
                $this->redirect(AppUrl::bicesUrl('app/dashboard'));
        }
        $this->layout = 'main';
        $this->render('import');
    }

    public function actionImportData()
    {
        if (isset($_FILES['file']) && isset($_FILES['file']['tmp_name'])) {
            $product = new Contract();
            $product->importData($_FILES['file']['tmp_name']);
        }
    }

    public function actionExport()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $contract = new Contract();
        $contract->export();
    }

    public function actionProcessNotifications()
    {
        set_time_limit(0);
        $contract = new Contract();
        $contract->processNotifications();
    }

    public function actionRefreshNotifications()
    {
        $contract = new Contract();
        $contract->refreshNotifications();
    }

    public function actionGetContractExpiry()
    {
        $contract = new Contract();
        $contract->contractExpiryDays();
    }

    public function actionGetContractsTitle()
    {
        $search_string = isset($_GET['query']) ? $_GET['query'] : "";
        $vendor = new Contract();
        $search_results = $vendor->getAll(array(
            'contract_title LIKE' => '%' . $search_string . '%'
        ), false, 'contract_id');

        echo json_encode(array(
            'suggestions' => $search_results
        ));
    }

    private function saveContractLocationsAndDepartments($locations=0, $department=0, $contractID){
        
        if(is_array($locations) && !empty($locations)){
            $contractLoc = new ContractLocation();
            $contractLoc->delete(['contract_id' => $contractID]);

            foreach($locations as $contractLocIDs){
                $contractLocSave = new ContractLocation();
                $contractLocSave->rs = [];
                $contractLocSave->rs['contract_id'] = $contractID;
                $contractLocSave->rs['location_id'] = $contractLocIDs;
                $contractLocSave->rs['created_at']  = date('Y-m-d H:i:s');
                $contractLocSave->write();
            }
        }

        if(is_array($department) && !empty($department)){
            $contractDept = new ContractDepartment();
            $contractDept->delete(['contract_id' => $contractID]);

            foreach($department as $contractDeptIDs){ 
                $contractDeptSave = new ContractDepartment();
                $contractDeptSave->rs = [];
                $contractDeptSave->rs['contract_id'] = $contractID;
                $contractDeptSave->rs['location_id'] = 0;
                $contractDeptSave->rs['department_id'] = $contractDeptIDs;
                $contractDeptSave->rs['created_at']  = date('Y-m-d H:i:s');
                $contractDeptSave->write();
            }
        }
    }
}
