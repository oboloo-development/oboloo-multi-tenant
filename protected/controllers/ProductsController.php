<?php

class ProductsController extends CommonController
{

    public $layout = 'main';

    public function actionIndex()
    {
        $this->redirect(AppUrl::bicesUrl('products/list'));
    }

    public function actionList()
    {
        error_reporting(0);
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'products_list';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        //CVarDumper::dump($_POST,10,1);die;

        // get orders as per filters applied
        $category_id = Yii::app()->input->post('category_id');
        $subcategory_id = Yii::app()->input->post('subcategory_id');
        $product_type = Yii::app()->input->post('product_type');
        $vendor_id = Yii::app()->input->post('vendor_id');
        $contract_id = Yii::app()->input->post('contract_id');

        if (! isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));
        
        // get list of all products (with pagination of course)
        $product = new Product();
        
        // and display
        $view_data = array();
        
//        if (Yii::app()->request->isAjaxRequest) {
//            $total_products = $product->getTotalRows();
//
//            $searched_records = $total_products;
//            $search_for = "";
//            if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && ! empty($_REQUEST['search']['value'])) {
//                $search_for = $_REQUEST['search']['value'];
//                $searched_records = $product->getSearchedRows($search_for);
//            }
//
//            $order_by = array();
//            if (isset($_REQUEST['order']) && is_array($_REQUEST['order']))
//                $order_by = $_REQUEST['order'];
//
//            $database_values = $product->getProducts(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by);
//            $products = array();
//            foreach ($database_values as $product_data) {
//                $current_data = array();
//
//                $edit_link = '';
//                $edit_link .= '<a href="' . AppUrl::bicesUrl('products/edit/' . $product_data['product_id']) . '">';
//                $edit_link .= '<button class="btn btn-sm btn-success">View/Edit</button></a>';
//
//                /*
//                 * $delete_link = '';
//                 * $delete_link .= '<a onclick="deleteProduct(' . $product_data['product_id'] . ');">';
//                 * if ($product_data['active'] == 1)
//                 * $delete_link .= '<span id="delete_icon_' . $product_data['product_id'] . '" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';
//                 * else
//                 * $delete_link .= '<span id="delete_icon_' . $product_data['product_id'] . '" class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>';
//                 *
//                 * $current_data[] = $edit_link . '&nbsp;' . $delete_link;
//                 */
//                $current_data[] = $edit_link;
//
//                foreach (array(
//                    'product_name',
//                    'vendor_name',
//                    'category',
//                    'subcategory',
//                    'brand',
//                    'upc'
//                ) as $attribute)
//                    $current_data[] = $product_data[$attribute];
//
//                if (is_numeric($product_data['price']))
//                    $current_data[] = Yii::app()->session['user_currency_symbol'] . ' ' . number_format($product_data['price'], 2);
//                else
//                    $current_data[] = "";
//
//                $products[] = $current_data;
//            }
//
//            $json_data = array(
//                "draw" => intval($_REQUEST['draw']),
//                "recordsTotal" => intval($total_products),
//                "recordsFiltered" => intval($searched_records),
//                "data" => $products
//            );
//            echo json_encode($json_data);
//        } else {

            $view_data['category_id'] = $category_id;
            $view_data['subcategory_id'] = $subcategory_id;
            $view_data['product_type'] = $product_type;
            $view_data['vendor_id'] = $vendor_id;
            $view_data['contract_id'] = $contract_id;

            $category = new Category();
            $view_data['categories'] = $category->getAll();
            $vendor = new Vendor();
            $view_data['vendors'] = $vendor->getProdVendors();
            $contract = new Contract();
            $view_data['contracts'] = $contract->getProdContracts();
            
            // need a few drop down values
       
           
           // if (!empty($category_id) || !empty($subcategory_id) || !empty($product_type) || !empty($vendor_id)) {
              //  $view_data['products'] = $product->getAllProducts($category_id, $subcategory_id, $product_type, $vendor_id);
            //}else{
               // $view_data['products'] = array();
            //}
            $this->render('list', $view_data);
        //}
    }

    public function actionListAjax()
    {
        if (! isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

       
        $requestData= $_REQUEST;
        $action= !empty($requestData['url_action'])?$requestData['url_action']:""; 
        $search_value = $requestData['search']['value'];

        $category_id = !empty($requestData['category_id'])?$requestData['category_id']:0;
        $subcategory_id = !empty($requestData['subcategory_id'])?$requestData['subcategory_id']:0;
        $product_type = !empty($requestData['product_type'])?$requestData['product_type']:0;
        $vendor_id = !empty($requestData['vendor_id'])?$requestData['vendor_id']:0;
        $contract_id =!empty( $requestData['contract_id'])?$requestData['contract_id']:0;

        $columns = array( 
            // datatable column index  => database column name
            0=>'product_id',
            1=>'product_name',
            2=>'v.vendor_name', 
            3=>'c.value', 
            4=>'s.value',
            5=>'brand',
            6=>'price',
        );

       $tool_currency = Yii::app()->session['user_currency'];

        $sql = "select cr_r.id from currency_rates cr_r where LOWER(cr_r.currency)='".strtolower($tool_currency)."'";
        $toolRateReader = Yii::app()->db->createCommand($sql)->queryRow();

        $sql = "SELECT p.*,p.price as org_price,getTotalInToolCurrency(p.price,p.currency_id,".$toolRateReader['id'].") as price, c.value AS category, s.value as subcategory,v.vendor_name
                  FROM products p
                  LEFT JOIN vendors v ON p.vendor_id = v.vendor_id
                  LEFT JOIN categories c ON p.category_id = c.id
                  LEFT JOIN sub_categories s ON p.subcategory_id = s.id WHERE p.status = 1";
  
    

        $sqlCount = "SELECT COUNT(*) AS total_rows 
                  FROM products p
                  LEFT JOIN vendors v ON p.vendor_id = v.vendor_id
                  LEFT JOIN categories c ON p.category_id = c.id
                  LEFT JOIN sub_categories s ON p.subcategory_id = s.id WHERE p.status = 1";

        /*if search is applicable*/
        if(!empty($search_value)){
          //  $sql.=' where (CONCAT(can.first_name," ",can.last_name) like "%'.$search_value.'%"';
           // $sql.=' OR sub.id like "%'.$search_value.'%"';
           // $sql.=' OR ven.organization like "%'.$search_value.'%"'; 
           // $sql.=' OR can.current_location like "%'.$search_value.'%"';
           // $sql.=' OR j.title like "%'.$search_value.'%")'; 
        }

        if (!empty($category_id)){
            $sql .= " AND p.category_id = $category_id";
            $sqlCount .= " AND p.category_id = $category_id";
        }

        if (!empty($subcategory_id)){
            $sql .= " AND p.subcategory_id = $subcategory_id";
            $sqlCount .= " AND p.subcategory_id = $subcategory_id";
        }
        if (!empty($product_type)){

           /* $checkNumber = str_replace( ',', '', $product_type );

            if( is_numeric( $checkNumber ) ) {
                $product_type = $checkNumber;
            }*/

            $sql .= " AND ( ";
            $sql .= "p.product_type = '".$product_type."' or p.product_name like '%".$product_type."%' or p.brand = '".$product_type."' or p.upc = '".$product_type."' or p.sku_code = '".$product_type."' or p.product_id = '".$product_type."'";
            $sql .= " ) ";

            $sqlCount .= " AND ( ";
            $sqlCount .= "p.product_type = '".$product_type."' or p.product_name like '%".$product_type."%' or p.brand = '".$product_type."' or p.upc = '".$product_type."' or p.sku_code = '".$product_type."' or p.product_id = '".$product_type."'";
            $sqlCount .= " ) ";
        }
        if (!empty($vendor_id)){
            $sql .= " AND p.vendor_id = $vendor_id";
            $sqlCount .= " AND p.vendor_id = $vendor_id";
        }
        if (!empty($contract_id)){
            $sql .= " AND p.contract_id = $contract_id";
            $sqlCount .= " AND p.contract_id = $contract_id";
        }

        $sql .= " group by p.product_id";
    
        /*search ends.*/
        $endLimit = $requestData['length'];
        
        if(empty($requestData['order'][0]['column'])){
            $requestData['order'][0]['column'] = 1;
            $requestData['order'][0]['dir'] = 'desc';
        }

        $order =$columns[$requestData['order'][0]['column']];
        //$order = "p.product_id DESC"; 
        $order =" ORDER BY ".$order." ".$requestData['order'][0]['dir'];
        $sql.=$order;
        $sqlCount.=  $order;
        //$sql.=" ORDER BY ".$order;
        //$sqlCount.=" ORDER BY ".$order;
        
        
        $totalData = Yii::app()->db->createCommand($sqlCount)->queryScalar();
       
        
        $sql.=" LIMIT ".$requestData['start']." ,".$endLimit;

        $products = Yii::app()->db->createCommand($sql)->queryAll();
        
        // when there is no search parameter then total number 
     
        $data=[];
  
        if(!empty($products)){
           foreach ($products as $product) 
            {
                $nestedData=[];
                $text_decoration = ' style="text-decoration: none;" ';
                if ($product['active'] == 0) $text_decoration = ' style="text-decoration: line-through;" ';

                $price_text_decoration = ' style="text-decoration: none; text-align: right;" ';
                if ($product['active'] == 0) $price_text_decoration = ' style="text-decoration: line-through; text-align: right;" ';


                $product_name='<span class="'.$text_decoration.'">'.$product['product_name'].'</span>';
                $vendor_name='<span class="'.$text_decoration.'">'.!empty($product['vendor_name']) ? $product['vendor_name']:''.'</span>';
                $category='<span class="'.$text_decoration.'">'.$product['category'].'</span>';
                $subcategory='<span class="'.$text_decoration.'">'.$product['subcategory'].'</span>';
                $brand='<span class="'.$text_decoration.'">'.$product['brand'].'</span>';

                //$price='<nobr><span class="'.$text_decoration.'">'.Yii::app()->session['user_currency_symbol'] . ' ' . number_format($product['price'], 2).'</span></nobr>';
              

                $pric=Yii::app()->session['user_currency_symbol'] . ' ' .number_format(!empty($product['price'])?$product['price']:0, 2);

                $price='<nobr><span class="'.$text_decoration.'">'.$pric.'</span></nobr>';
                $org_price='<nobr><span class="'.$text_decoration.'">'.FunctionManager::showCurrencySymbol('',$product['currency_id']).' '.(!empty($product['org_price'])?$product['org_price']:0).'</span></nobr>';

                $nestedData[]='<a href="'.AppUrl::bicesUrl('products/edit/' . $product['product_id']).'"><button class="btn btn-sm btn-success">View/Edit</button></a>';
                $nestedData[]=$product_name;
                $nestedData[]=$vendor_name;
                $nestedData[]=$category;
                $nestedData[]=$subcategory;
                $nestedData[]= $brand;
                $nestedData[]=$price;
                $nestedData[]=$org_price;
                
                $data[] = $nestedData;
            }
        }
            
        /*end create array for data tables.*/
        $json_data = array( 
        "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
        "recordsTotal"    => intval( $totalData ),  // total number of records
        "recordsFiltered" => intval( $totalData ), // total number of records 
        "data"            => $data  // total data array
        ); 
        echo  json_encode($json_data); 
                
    }

  

    public function actionEdit($product_id = 0)
    {   error_reporting(0);
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'products_edit';
        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);
        
        if (! isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));
        
        // if form was submitted ... first save the data to the database
        if (Yii::app()->input->post('form_submitted')) {
            $product = new Product();
            if (Yii::app()->input->post('active')) {
                $product->fields['active'] = 'N';
                $override_data['active'] = 1;
                $product->saveData($override_data);
            } else
                $product->saveData();
               
            $this->redirect(AppUrl::bicesUrl('products/edit/' . $product->rs['product_id']));
        }
        
        // get data for the selected product
        $view_data = array(
            'product_id' => $product_id
        );

        $product = new Product();
        $view_data['product'] = $product->getProducts($product_id);

        $vendor = new Vendor();
        $view_data['vendors'] = $vendor->getProductVendors();

        $contract = new Contract();
        $view_data['contracts'] = $contract->getContracts();
        
        // need a few drop down values
        $category = new Category();
        $view_data['categories'] = $category->getAll();

        $currency_rate = new CurrencyRate();
        $view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();
        
        // and display
        $this->render('edit', $view_data);
    }

    public function actionImport()
    {
        // is someone already logged in?
        $this->checklogin();
        if (! isset(Yii::app()->session['user_type']) || Yii::app()->session['user_type'] != 1)
            $this->redirect(AppUrl::bicesUrl('orders/list'));
        else {
            if (! isset(Yii::app()->session['admin_flag']) || Yii::app()->session['admin_flag'] != 1)
                $this->redirect(AppUrl::bicesUrl('app/dashboard'));
        }
        $this->layout = 'main';
        $this->render('import');
    }

    public function actionImportData()
    {
        if (isset($_FILES['file']) && isset($_FILES['file']['tmp_name'])) {
            $product = new Product();
            $product->importData($_FILES['file']);
        }
    }

    public function actionExport()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $product = new Product();
        $product->export();
    }

    public function actionGetCategories()
    {
        $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
        $categories = array();
        
        $category = new Category();
        $search_results = $category->getAll(array(
            'value LIKE' => '%' . $search_string . '%',
             'soft_deleted' => "0",
            'ORDER' => 'value'
        ));
        
        foreach ($search_results as $category_data)
            if ($category_data['id'] != 0)
                $categories[] = array(
                    "value" => $category_data['value'],
                    "data" => $category_data['id']
                );
        
        echo json_encode(array(
            'suggestions' => $categories
        ));
    }

    public function actionGetSubCategories()
    {
        $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
        $category_id = isset($_REQUEST['category_id']) ? $_REQUEST['category_id'] : "";
        $sub_category_id = isset($_REQUEST['subcategory_id']) ? $_REQUEST['subcategory_id'] : "";
        
        $categories = array();
        
         $category_id =$category_id;

        $category = new Subcategory();

        if(is_array($category_id)){
            $subCat = array(
            'value LIKE' => '%' . $search_string . '%',
            'soft_deleted' => "0",
            'ORDER' => 'value',
            'code in' => $category_id
        );
        }else{
             $subCat = array(
            'value LIKE' => '%' . $search_string . '%',
             'soft_deleted' => "0",
            'ORDER' => 'value',
            'code' => $category_id
        );
        }

        $search_results = $category->getAll($subCat);
        foreach ($search_results as $category_data)
            if ($category_data['id'] != 0)
                $categories[] = array(
                    "value" => $category_data['value'],
                    "data" => $category_data['id']
                );

        if(!empty($sub_category_id)){
        $search_results = $category->getAll(array('id'=>$sub_category_id));
        foreach ($search_results as $category_data)
                $categories[] = array(
                    "value" => $category_data['value'],
                    "data" => $category_data['id']
                );
        }
        
        echo json_encode(array(
            'suggestions' => $categories
        ));
    }

    public function actionUpdateItemStatus()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        
        // only admin can delete item
        if (Yii::app()->session['user_type'] == 1) {
            $delete_item_id = isset($_REQUEST['delete_item_id']) ? $_REQUEST['delete_item_id'] : "";
            $active_flag = isset($_REQUEST['active_flag']) ? $_REQUEST['active_flag'] : 0;
            
            if ($delete_item_id) {
                $product = new Product();
                $product->rs = array();
                $product->rs['product_id'] = $delete_item_id;
                $product->rs['active'] = $active_flag;
                $product->write();
            }
        }
    }

    public function actionDeleteItemStatus()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $delete_item_id = isset($_REQUEST['delete_item_id']) ? $_REQUEST['delete_item_id'] : "";
        
        if ($delete_item_id) {
            $product = new Product();
            $product->rs = array();
            $product->rs['product_id'] = $delete_item_id;
            $product->rs['status'] = '0';
            $product->write();
            return 'OK';
        }
        
//         if ((new Product())->delete([
//             'product_id' => $_POST['delete_item_id']
//         ])) {
//             return 'OK';
//         }
        // return $data;
    }
}
