<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');
class SavingsUsgController extends CommonController
{
	public $layout = 'main';
	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('home'));
	}

	
	public function actionList()
	{   error_reporting(0);
	
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		unset(Yii::app()->session['quick_vendor']);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'savings_list';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);

		// get list of all orders (with pagination of course)
		$saving = new SavingUsg();

		// get orders as per filters applied
		$view_data = array();
		$vendor_ids    = Yii::app()->input->post('vendor_id');
		$location_id   = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
	    $saving_status = Yii::app()->input->post('status');
	    $view_data['saving_status_id'] = $saving_status;

		$category_id 	= Yii::app()->input->post('category_id');
        $subcategory_id = Yii::app()->input->post('subcategory_id');

		
		$due_date = Yii::app()->input->post('due_date');
		$view_data['due_date'] = $due_date;

		$db_from_date = "";
		if (!empty($due_date))
		{
			if(FunctionManager::dateFormat() == "d/m/Y"){
	       		list($input_date, $input_month, $input_year) = explode("/", $due_date);
	       		$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
	       	}else{
	       		$db_from_date = date("Y-m-d",strtotime($due_date));
	       	}
			$view_data['due_date'] = $db_from_date;
		}

		$view_data['saving_status']  = $saving_status;
		$view_data['category_id']    = $category_id;
        $view_data['subcategory_id'] = $subcategory_id;
	
        if(!empty($_POST['report_from_date']) || !empty($_POST['report_to_date'])){
        	if(FunctionManager::dateFormat() == "d/m/Y"){
        	  $chartDateFrom = date("Y-m-d", strtotime(strtr($_POST['report_from_date'], '/', '-')));
        	  $chartDateTo   = date("Y-m-d", strtotime(strtr($_POST['report_to_date'], '/', '-')));
        	}else{
              $chartDateFrom = date("Y-m-d", strtotime($_POST['report_from_date']));
              $chartDateTo   = date("Y-m-d", strtotime($_POST['report_to_date']));
            }
		}else{
         $chartDateFrom = "2019-01-01";
         $chartDateTo   = date("Y-m-d",strtotime("+2 years"));
    	}

    	if(!empty($_POST['report_from_date']) || !empty($_POST['report_to_date'])){

         if(FunctionManager::dateFormat() == "d/m/Y"){
          $chartDateFromMonth = date("Y-m-d", strtotime(strtr($_POST['report_from_date'], '/', '-')));
          $chartDateToMonth   = date("Y-m-d", strtotime(strtr($_POST['report_to_date'], '/', '-')));
         }else{
          $chartDateFromMonth = date("Y-m-d", strtotime($_POST['report_from_date']));
          $chartDateToMonth   = date("Y-m-d", strtotime($_POST['report_to_date']));
         }

		}else{
         $chartDateFromMonth = "2019-01-01";
         $chartDateToMonth   = date("Y-m-d",strtotime("+2 years"));
    	}
    	
    	$location = new Location();
		$view_data['locations']   	= $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] 	= $location_id;
		$view_data['department_id'] = $department_id;

		$category = new Category();
        $view_data['categories'] = $category->getAll();
        
        if(!empty($category_id)){
         $subcategory = new Subcategory();
		 $view_data['subcategory'] = $subcategory->getAll(array('code'=>$category_id));
		}else{
		 $view_data['subcategory'] = '';
		}

		$view_data['chartDateFrom'] = $chartDateFrom;
		$view_data['chartDateTo']   = $chartDateTo;

		$vendor = new Vendor();
		$view_data['vendors']    = $vendor->getAll(array('ORDER' => 'vendor_name'));
		$view_data['vendor_ids'] = $vendor_ids;

        $metrixArr 			   = $saving->getMetrix($chartDateFrom, $chartDateTo);
       
        $view_data['metrix_1'] = $metrixArr['metrix_1'];
        $view_data['metrix_3'] = $metrixArr['metrix_3'];
        $view_data['metrix_2'] = $metrixArr['metrix_2'];
        $view_data['metrix_4'] = $metrixArr['metrix_4'];
        
        $chartListArr 		   = $saving->getChartList($chartDateFromMonth, $chartDateToMonth);
       	$view_data['chart_1']  = $chartListArr['chart_1'];
       	$view_data['chart_2']  = $chartListArr['chart_2'];
       	$view_data['chart_3']  = $chartListArr['chart_3'];
       	
		$view_data['savings']  = $saving->getSavings($location_id, $department_id,$category_id, $subcategory_id, $saving_status, $db_from_date, $vendor_ids);
		$view_data['saving_status'] = $saving->getStatus();
	 
	// START : First time users in to system it's start tour of this page.
		$user = new User();
		$userData = $user->getOne(array('user_id'=>Yii::app()->session['user_id']));
		$view_data['saving_visit'] = $userData['saving_visit'];
		if($view_data['saving_visit'] !=1){
			$user->rs = array();
			$user->rs['user_id'] = $userData['user_id'];
			$user->rs['saving_visit'] = 1;
			$user->write();
		}

	// END : First time users in to system it's start tour of this page.
		$user  = new User();
		$query = "Select user_id,full_name from users order by full_name asc";
		$get_all_user = Yii::app()->db->createCommand($query)->query()->readAll();
	
		$view_data['get_all_user'] = $get_all_user;

		$savingsArea = new SavingArea();
        $view_data['savingsArea'] = $savingsArea->getAll();

        $location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$new_or_incre_sav = "select new_or_incremental_savings from savings where new_or_incremental_savings is not null ";
		$view_data['new_or_incre_sav'] = Yii::app()->db->createCommand($new_or_incre_sav)->query()->readAll();

        $savingsBussinessUnit = new SavingBusinessUnit();
		$view_data['savingsBussinessUnit'] = $savingsBussinessUnit->getAll();
		$this->render('list', $view_data);
	}

	 public function actionListAjax(){
        error_reporting(0);

        $saving = new SavingUsg();
        $industry_id = 0;
            if (isset($_REQUEST['industry_id']) && !empty($_REQUEST['industry_id']))
                $industry_id = $_REQUEST['industry_id'];            

            $subindustry_id = 0;
            if (isset($_REQUEST['subindustry_id']) && !empty($_REQUEST['subindustry_id']))
                $subindustry_id = $_REQUEST['subindustry_id'];          

            $preferred_flag  = -1;
            $supplier_status = '';
            if (isset($_REQUEST['preferred_flag'])) $preferred_flag   = $_REQUEST['preferred_flag'];
            if (isset($_REQUEST['supplier_status'])) $supplier_status = $_REQUEST['supplier_status'];
                    

            set_time_limit(0);
            $total_vendors = 0;
            
            $search_for = "";
            if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value']))
            {
                $search_for = $_REQUEST['search']['value'];
                
            }
            
            $order_by = array();
            if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])){
                $order_by = $_REQUEST['order'];
            }else{
                $order_by[1] = 'contract_title';
            }
            
            $location_id 	 = Yii::app()->input->post('location_id');
            $department_id   = Yii::app()->input->post('department_id');
            $category_id     = Yii::app()->input->post('category_id');
            $subcategory_id  = Yii::app()->input->post('subcategory_id');
            $saving_status   = Yii::app()->input->post('status');
            $due_date 		 = Yii::app()->input->post('due_date');
            $due_date_to 	 = Yii::app()->input->post('due_date_to');
            $initiative_owner= Yii::app()->input->post('initiative_owner');
            $savings_area 	 = Yii::app()->input->post('savings_area');
            $savings_type 	 = Yii::app()->input->post('savings_type');
            $business_unit 	 = Yii::app()->input->post('business_unit');
            $savings_location_country   = Yii::app()->input->post('savings_location_country');
            $new_or_incremental_savings = Yii::app()->input->post('new_or_incremental_savings');
            $new_archived = Yii::app()->input->post('new_archived');

            $savings = $saving->getSavingsAjax(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by,$location_id, $department_id, $category_id, $subcategory_id,$saving_status,$due_date,
            	$due_date_to,$initiative_owner,$savings_area, $savings_type, $business_unit,
            	$savings_location_country,$new_or_incremental_savings,$new_archived);
 
            $dataArr = array();
            $i=1;
            $subdomain = FunctionManager::subdomainByUrl();
            foreach ($savings as $value)
            {   
                $style="";
				if(strtolower(trim($value['saving_status']))=="completed"){
					$style ="style='background:#6cce47;border-color:#6cce47'";
				}else if(strtolower(trim($value['saving_status']))=="on-hold"){
					$style ="style='background:#f0ad4e;border-color:#f0ad4e'";
				}else if(strtolower(trim($value['saving_status']))=="active"){
					$style ="style='background:#3b4fb4;border-color:#3b4fb4'";
				}else if(strtolower(trim($value['saving_status']))=="cancelled"){
					$style ="style='background:#fb5a5a !important ;border-color:#fb5a5a !important'";
				}	

				$imgFullPath = '';
                if(!empty($value['profile_img'])){
                    $imgFullPath = Yii::app()->baseUrl . "/../images/".$subdomain."/users/".$value['user_id']."/".$value['profile_img'];
                }else if(!empty($value['user_id'])){
                  $imgFullPath = Yii::app()->baseUrl . "/../images/DefaultPicture.jpg";
                }
                if(!empty($imgFullPath)){
                    $imgFullPath = CHtml::image($imgFullPath);
                }
                $current_data   = array();
                $current_data[] = '<a href="'.AppUrl::bicesUrl('savingsUsg/edit/' . $value['id']).'">
                            <button class="btn btn-sm btn-success view-btn" >View/Edit
                            </button>
                        </a>';


                $savingStartDate = date(FunctionManager::dateFormat(), strtotime($value['saving_start_date']));
                if(!empty($savingStartDate) || $savingStartDate != '01/01/1970' || $savingStartDate !='0000-00-00 00:00:00')
                {
                  $savingStartDate = $savingStartDate;
                }else{
                  $savingStartDate = '';
                }

                $due_date = date(FunctionManager::dateFormat(), strtotime($value['due_date']));
                if(!empty($due_date) || $due_date !='0000-00-00 00:00:00' || $due_date != '01/01/1970'){
                	$due_date = $due_date;
                }else{
                	$due_date = '';
                }
                $current_data[] = $value['id'];
                $current_data[] = $value['title'];
                $current_data[] =  $savingStartDate != '01/01/1970' ? $savingStartDate : '';
                $current_data[] = $due_date != '11/30/-0001' ? $due_date:'';
                $current_data[] = "<span class='table-user-img'>".$imgFullPath."</span> ".$value['user_name'];
                $current_data[] = $value['currency'];
                if(!empty($value['saving_type'])){
                	$saving_type = FunctionManager::savingType($value['saving_type']);
            	}else{
            		$saving_type = '';
            	}
                $current_data[]  =  $saving_type;

                // bellow planned_savings = total_project_saving-realised_saving
                //$current_data[] = $value['currency_symbol'].($value['total_project_saving']-$value['realised_saving']);
                $current_data[] ="<span class=". ($value['total_project_saving'] < 0 ? 'text-red' :'') .">".
                				 $value['currency_symbol'].number_format($value['total_project_saving'])."</span>";
                $current_data[] ="<span class=". ($value['realised_saving'] < 0 ? 'text-red' :'') .">".
                				 $value['currency_symbol'].number_format($value['realised_saving'])."</span>";
                //$current_data[] = $value['realised_saving_perc']>0?number_format($value['realised_saving_perc'],0,"","").'%':'';
                $current_data[] = '<button type="submit" class="btn btn-success" '.$style.'>'.$value['saving_status'].'</button>';

                $dataArr[]  = $current_data;
                $i++;
            }
            if(empty($search_for)){
            	$database_values_total = $saving->getAll(array('ORDER' => 'id'));
            	$total_savings = count($database_values_total);
        	}else{
        		$total_savings = count($savings);
        	}
            
            
            $json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $total_savings,
                "recordsFiltered" => $total_savings,
                "data"            => $dataArr
            );
            echo json_encode($json_data);exit;  
     }

	public function actionEdit($saving_id = 0)
	{
		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'savings_list';
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		error_reporting(0);
		$currency_rate = new CurrencyRate();
		// if form was submitted ... first save the data to the database
         
		if(isset($_POST['title']))
		{
           	if(!empty($_POST['new_vendor_name'])){
       		 $quickaddedVendorArr = array_keys(Yii::app()->session['quick_vendor']);
       		 $vendorID = !empty($quickaddedVendorArr[0])?$quickaddedVendorArr[0]:'0';
           	}else{
           	 $vendorID = !empty($_POST['vendor_id'])?$_POST['vendor_id']:'0';
           	}
           	$currencyID = !empty($_POST['currency_id'])?$_POST['currency_id']:'0';
           	if(!empty($currencyID)){
           	 $currency = $currency_rate->getOne(array('id' => $currencyID));
           	 $currencyRate = $currency['rate'];
           	}else { $currencyRate = 0;}

           	$userObj = new User();
           	$userID = Yii::app()->session['user_id'];
            $updateSaving = !empty($_POST['update_saving'])?addslashes($_POST['update_saving']):'';
            $saving_id = !empty($_POST['saving_id'])?$_POST['saving_id']:$saving_id;
           	$title = !empty($_POST['title'])?addslashes($_POST['title']):'';
           	$status = !empty($_POST['status'])?$_POST['status']:0;
           	$approver_id = !empty($_POST['approver_id'])?$_POST['approver_id']:0;
           	$approver_status = !empty($_POST['approver_id'])?'Sent for approval':'';
           	$categoryID = !empty($_POST['category_id'])?$_POST['category_id']:'0';
           	$subcategoryID = !empty($_POST['subcategory_id'])?$_POST['subcategory_id']:'0';
           	$materialTypeID = !empty($_POST['material_type_id'])?$_POST['material_type_id']:'0';

	       	$oracleSupplierName = !empty($_POST['oracle_supplier_name'])?$_POST['oracle_supplier_name']:'';
	       	$oracleSupplierNumber = !empty($_POST['oracle_supplier_number'])?$_POST['oracle_supplier_number']:'';
	       	$countryName  = !empty($_POST['country'])?$_POST['country']:'';
	        $savingType   = !empty($_POST['saving_type'])?$_POST['saving_type']:'0';
	        $savingArea   = !empty($_POST['saving_area'])?$_POST['saving_area']:'';
	        $savingBusiness = !empty($_POST['business_unit'])?$_POST['business_unit']:'';
	        $specialEvent = !empty($_POST['special_event'])?$_POST['special_event']:'';
	        $eventName    = !empty($_POST['event_name'])?$_POST['event_name']:'';
            $newOrIncrementalSavings = !empty($_POST['new_or_incremental_savings'])?$_POST['new_or_incremental_savings']:'';
            $new_archived = !empty($_POST['new_archived'])?$_POST['new_archived']:'';
           	$locationID   = !empty($_POST['location_id'])?$_POST['location_id']:'';
           	$departmentID = !empty($_POST['department_id'])?$_POST['department_id']:'0';
           	$contractID   = !empty($_POST['contract_id'])?$_POST['contract_id']:'0';
           	$notes = !empty($_POST['notes'])?addslashes($_POST['notes']):'';
           	$totalProjectSaving = !empty($_POST['total_project_saving'])?$_POST['total_project_saving']:'0';
           	$totalProjectSavingPerc = !empty($_POST['total_project_saving_perc'])?$_POST['total_project_saving_perc']:'0';
           	$realisedSaving = !empty($_POST['realised_saving'])?$_POST['realised_saving']:'0';
           	$realisedSavingPerc = !empty($_POST['realised_saving_perc'])?$_POST['realised_saving_perc']:'0';
			$startDate = !empty($_POST['start_date'])?$_POST['start_date']:'0000-00-00 ';
			if(FunctionManager::dateFormat()=="d/m/Y"){
				$convFormatDue = strtr($_POST['saving_due_date'], '/', '-');
				$convFormatCreate = strtr($_POST['created_at'], '/', '-');
			}else{
				$convFormatDue = $_POST['saving_due_date'];
				$convFormatCreate = $_POST['created_at'];
			}
			$savingDueDate = !empty($_POST['saving_due_date'])?date("Y-m-d ", strtotime($convFormatDue)):'0000-00-00 00:00:00';
		    $createdAt = !empty($_POST['created_at'])?date("Y-m-d", strtotime($convFormatCreate)):date("Y-m-d");
           	$userName = !empty($_POST['user_name'])?$_POST['user_name']:Yii::app()->session['full_name'];

           	$current_budget	= !empty($_POST['current_budget']) && $_POST['current_budget'] == 1 ? $_POST['current_budget'] : 0;
           	$initially_verified = !empty($_POST['initially_verified']) && $_POST['initially_verified'] == 1 ? $_POST['initially_verified'] : 0;
           	$verified = !empty($_POST['verified']) && $_POST['verified'] == 1 ? $_POST['verified'] : 0;
           	$supported_by_px = !empty($_POST['supported_by_px']) && $_POST['supported_by_px'] == 1 ? $_POST['supported_by_px'] : 0;

           	if(!empty($saving_id)){
				$saving = new SavingUsg;
				$oldRecord = $saving->getOne(array('id'=>$saving_id));
			}
			if(!empty($approver_id)){
				$approverRecord = $userObj->getOne(array('user_id'=>$approver_id));
				$approver_name = $approverRecord['full_name'];
			}else{
				$approver_name = '';
			}

			$saving = new SavingUsg;
			$saving->rs = array();
			$saving->rs['id'] = $saving_id;
			if($saving_id<1){
			 $saving->rs['user_id'] = $userID;
			 $saving->rs['user_name']= $userName;
			}
			$saving->rs['title'] = $title;
            $saving->rs['approver_id'] = $approver_id;
            $saving->rs['approver_name'] = $approver_name;
            $saving->rs['approver_status'] = $approver_status;
			$saving->rs['status'] = $status;
			$saving->rs['currency_id'] = $currencyID;
			$saving->rs['currency_rate'] = $currencyRate;
			$saving->rs['material_type_id'] = $materialTypeID;
			$saving->rs['category_id'] = $categoryID;
			$saving->rs['subcategory_id'] = $subcategoryID;
			$saving->rs['oracle_supplier_name'] = $oracleSupplierName;
			$saving->rs['oracle_supplier_number'] = $oracleSupplierNumber;
			$saving->rs['country'] = $countryName;
			$saving->rs['saving_type'] = $savingType;
			$saving->rs['saving_area'] = $savingArea;
			$saving->rs['business_unit'] = $savingBusiness;
			$saving->rs['special_event'] = $specialEvent;
			$saving->rs['event_name'] = $eventName;
			$saving->rs['new_or_incremental_savings'] = $newOrIncrementalSavings;
			$saving->rs['new_archived'] = $new_archived;
			$saving->rs['current_budget']= $current_budget;
			$saving->rs['initially_verified'] = $initially_verified;
			$saving->rs['verified'] = $verified;
			$saving->rs['supported_by_px'] = $supported_by_px;
			$saving->rs['location_id'] = $locationID;
			$saving->rs['material_type_id'] = $materialTypeID;
			$saving->rs['department_id'] = $departmentID;
			$saving->rs['vendor_id'] = $vendorID;
			$saving->rs['notes'] = $notes;
			$saving->rs['total_project_saving'] = $totalProjectSaving;
			$saving->rs['total_project_saving_perc'] = $totalProjectSavingPerc;
			$saving->rs['realised_saving'] = $realisedSaving;
			$saving->rs['realised_saving_perc'] = $realisedSavingPerc;
			$saving->rs['start_date'] = $startDate;
			$saving->rs['due_date'] = $savingDueDate;
			$saving->rs['contract_id'] = $contractID;
			$saving->rs['created_at'] = $createdAt;
			$saving->rs['updated_at'] = $createdAt;
			$saving->write();

			$savingID= Yii::app()->db->lastInsertID;
			if(!empty($approver_status)){
			  $this->getSentForApprovalEmailNotification($savingID, $approver_id, $approver_status);
			}

			$savingID= Yii::app()->db->lastInsertID;
			if(!empty($approver_status)){
			  $this->getSentForApprovalEmailNotification($savingID, $approver_id, $approver_status);
			}

			if(!empty($saving->rs['id'])){
			  if(empty($updateSaving)){ 
		     	$mesg = empty($saving_id)?'Saving record created successfully.':'Saving record updated successfully';
			  if(empty($saving_id)){
				$saving->saveRecordCurrency($saving->rs['id']);
			  }
			   $saving_id = $saving->rs['id'];
			   $savingMilestones = Yii::app()->session['saving_milestones'];
				// cost_reduction saving_monthly
			  if(!empty($savingMilestones)){
				 $createdAt = date("Y-m-d");
				 foreach($savingMilestones as $key=>$value){
					$dueDate = $value['due_date'];
					if(!empty($dueDate) && FunctionManager::dateFormat()=="d/m/Y"){
					 $dueDate = strtr($dueDate, '/', '-');
					} 
					 
					$dueDate = !empty($dueDate)?date("Y-m-d", strtotime($dueDate)):'0000-00-00 00:00:00';
						
					$savingMilestone = new SavingMilestone;
					$savingMilestone->rs = array();
					$savingMilestone->rs['id'] = 0;
					$savingMilestone->rs['saving_id'] = $saving_id;
					$savingMilestone->rs['title'] = addslashes($value['title']);
					$savingMilestone->rs['due_date'] = $dueDate;
					$savingMilestone->rs['base_line_spend'] = $value['base_line_spend'];
					$savingMilestone->rs['cost_reduction'] = $value['cost_reduction'];
					$savingMilestone->rs['cost_avoidance'] = $value['cost_avoidance'];
					$savingMilestone->rs['created_at'] = $createdAt;
						//Start: Milestone Saving due date
					if(FunctionManager::savingSpecific()){
					  $milestoneDueDate = $value['savings_due_date'];
					if(!empty($milestoneDueDate) && FunctionManager::dateFormat()=="d/m/Y"){
					  $milestoneDueDate = strtr($milestoneDueDate, '/', '-');
					} 
					
					$milestoneDueDate = !empty($milestoneDueDate)?date("Y-m-d", strtotime($milestoneDueDate)):'0000-00-00 00:00:00';
					$savingMilestone->rs['savings_due_date'] = $milestoneDueDate;

					$savingsStartDate = $value['start_date'];
					if(!empty($savingsStartDate) && FunctionManager::dateFormat()=="d/m/Y"){
					 $savingsStartDate = strtr($savingsStartDate, '/', '-');
					} 
					 $savingsStartDate = !empty($savingsStartDate)?date("Y-m-d", strtotime($savingsStartDate)):'0000-00-00 00:00:00';
					 $savingMilestone->rs['start_date'] = $savingsStartDate;
					 $savingDuration = $value['saving_duration'];
					 $savingMilestone->rs['saving_duration'] = $savingDuration;
					}
					
					//End: Milestone Saving due date
					$savingMilestone->write();

					// Start: Duration Planning Per month 
					if(FunctionManager::savingSpecific() && !empty($savingMilestone->rs['id'])){
					 $milestone_id = $savingMilestone->rs['id'];
					 $savingMonthly = $value['saving_monthly'];

					 foreach($savingMonthly as $key=>$fieldValue){			
						$milestoneField = new MilestoneField;
						$milestoneField->rs = array();
						$milestoneField->rs['id'] = 0;
						$milestoneField->rs['saving_id'] = $saving_id;
						$milestoneField->rs['milestone_id'] = $milestone_id;
						$milestoneField->rs['field_name'] = $fieldValue['fieldName'];
						$milestoneField->rs['field_value'] = $fieldValue['fieldValue'];
						$milestoneField->write();
					 }
				    }
					//End: Duration Planning Per month 

        			}
				}
				FunctionManager::savingCalculationSave($saving_id);
				}

				if(FunctionManager::savingSpecific()){
				//Start: saving log
				$comment = '';
			 	if(!empty($oldRecord['id'])){
	                $comment = '';
	                if(trim($oldRecord['title'])!=trim($_POST['title'])){
	                    $comment .= '<strong>Savings Initiative Name:</strong> <span class="title-text"><b>'. $oldRecord['title']. ' </b>changed to <b>'.$_POST['title'].'</b></span><br/>';
	                }
	                if(trim($oldRecord['notes'])!=trim($_POST['notes'])){
	                    $comment .= '<strong>Savings Initiative Description:</strong> <span class="title-text"><b>'. $oldRecord['notes']. ' </b>changed to <b>'.$_POST['notes'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['status'])!=trim($_POST['status'])){
	                    $savingStatusModel = new SavingStatus;
                        $savingStatusPosted = $savingStatusModel->getOne(array('id'=>Yii::app()->input->post('status')));
                        $savingStatusExisted = $savingStatusModel->getOne(array('id'=>$oldRecord['status']));

                    if(empty($savingStatusExisted)){
                        $savingStatusName = 'NULL';
                    }else{
                        $savingStatusName = $savingStatusExisted['value'];
                    }

                    $comment .= '<b>Status:</b> <span class="title-text"><b>'.$savingStatusName. '</b> changed to <b>'.$savingStatusPosted['value'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['currency_id'])!=trim($_POST['currency_id'])){
	                    $currencyRateModel = new CurrencyRate;
                        $currenyPosted = $currencyRateModel->getOne(array('id'=>Yii::app()->input->post('currency_id')));
                        $currenyExisted = $currencyRateModel->getOne(array('id'=>$oldRecord['currency_id']));

                    if(empty($currenyExisted)){
                        $currencyExistedName = 'NULL';
                    }else{
                        $currencyExistedName = $currenyExisted['currency'];
                    }

                    $comment .= '<b>Currency:</b> <span class="title-text"><b>'.$currencyExistedName. '</b> changed to <b>'.$currenyPosted['currency'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['category_id'])!=trim($_POST['category_id'])){
	                $category = new Category();
	                $categoryPosted = $category->getOne(array('id'=>Yii::app()->input->post('category_id')));
                    $categoryExisted = $category->getOne(array('id'=>$oldRecord['category_id']));
                    $comment .= '<b>Category:</b> <span class="title-text"><b>'. $categoryExisted['value']. '</b> changed to <b>'.$categoryPosted['value'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['subcategory_id'])!=trim($_POST['subcategory_id'])){
	                $subcategory = new Subcategory();
	                $subcategoryPosted = $subcategory->getOne(array('id'=>Yii::app()->input->post('subcategory_id')));
                    $subcategoryExisted = $subcategory->getOne(array('id'=>$oldRecord['subcategory_id']));
                    $comment .= '<b>Sub Category:</b> <span class="title-text"><b>'. $subcategoryExisted['value']. 
                     '</b> changed to <b>'.$subcategoryPosted['value'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['location_id'])!=trim($_POST['location_id'])){
	                $location = new Location();
                    $locationPosted = $location->getOne(array('location_id'=>Yii::app()->input->post('location_id')));
                    $locationExisted = $location->getOne(array('location_id'=>$oldRecord['location_id']));
                    $comment .= '<b>Location:</b> <span class="title-text"><b>'. $locationExisted['location_name']. 
                    	' </b>changed to <b>'.$locationPosted['location_name'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['saving_area'])!=trim($_POST['saving_area'])){
	                $savingArea = new SavingArea;
                    $savingAreaPosted = $savingArea->getOne(array('id'=>Yii::app()->input->post('saving_area')));
                    $savingAreaExisted = $savingArea->getOne(array('id'=>$oldRecord['saving_area']));
                    if(empty($savingAreaExisted)){
                        $savingAreaName = 'NULL';
                    }else{
                        $savingAreaName = $savingAreaExisted['value'];
                    }
                    $comment .= '<b>Savings Area:</b> <span class="title-text"><b>'.$savingAreaName. '</b> changed to <b>'.$savingAreaPosted['value'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['business_unit'])!=trim($_POST['business_unit'])){
	                $businessUnit = new SavingBusinessUnit;
                    $businessUnitPosted = $businessUnit->getOne(array('id'=>Yii::app()->input->post('business_unit')));
                    $businessUnitExisted = $businessUnit->getOne(array('id'=>$oldRecord['business_unit']));
                    if(empty($businessUnitExisted)){
                        $businessUnitName = 'NULL';
                    }else{
                        $businessUnitName = $businessUnitExisted['value'];
                    }
                    $comment .= '<b>Business Unit:</b> <span class="title-text"><b>'.$businessUnitName. '</b> changed to <b>'.$businessUnitPosted['value'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['oracle_supplier_name'])!=trim($_POST['oracle_supplier_name'])){
	                $comment .= '<strong>Oracle Supplier Name:</strong> <span class="title-text"><b>'. $oldRecord['oracle_supplier_name']. ' </b>changed to <b>'.$_POST['oracle_supplier_name'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['oracle_supplier_number'])!=trim($_POST['oracle_supplier_number'])){
	                $comment .= '<strong>Oracle Supplier Number:</strong> <span class="title-text"><b>'. $oldRecord['oracle_supplier_number']. ' </b>changed to <b>'.$_POST['oracle_supplier_number'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['country'])!=trim($_POST['country'])){
	                $comment .= '<strong>country:</strong> <span class="title-text"><b>'. $oldRecord['country']. ' </b>changed to <b>'.$_POST['country'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['saving_type'])!=trim($_POST['saving_type'])){
	                  $savingTypeOld = FunctionManager::savingType($oldRecord['saving_type']);
	                  $savingTypeExisted = FunctionManager::savingType($_POST['saving_type']);
	                 $comment .= '<strong>Savings Type:</strong> <span class="title-text"><b>'. $savingTypeOld. ' </b>changed to <b>'.$savingTypeExisted.'</b></span><br/>';
	                }

	                if(trim($oldRecord['special_event'])!=trim($_POST['special_event'])){
	                 $comment .= '<strong>Savings Resulted From A Special Event:</strong> <span class="title-text"><b>'. $oldRecord['special_event']. ' </b>changed to <b>'.$_POST['special_event'].'</b></span><br/>';
	                }

	                if(trim($oldRecord['event_name'])!=trim($_POST['event_name'])){
	                 $comment .= '<strong>Event Name & Details:</strong> <span class="title-text"><b>'. $oldRecord['event_name']. ' </b>changed to <b>'.$_POST['event_name'].'</b></span><br/>';
	                }	
	               
	                if(trim($oldRecord['new_or_incremental_savings'])!=trim($_POST['new_or_incremental_savings'])){
	                 $comment .= '<strong>New Or Incremental Saving:</strong> <span class="title-text"><b> '. $oldRecord['new_or_incremental_savings']. ' </b>changed to <b>'.$_POST['new_or_incremental_savings'].'</b></span><br/>';
	                }	

	                if(trim($oldRecord['new_archived'])!=trim($_POST['new_archived'])){
	                 $comment .= '<strong>Archived?: </strong> <span class="title-text"><b>'. ($oldRecord['new_archived'] == 1 ? ' Yes ' : ' No'  ).' </b>changed to<b>'.($_POST['new_archived'] == 1 ? ' Yes ': ' No ' ).'</b></span><br/>';
	                }	

	                if(!empty($comment)){
						$log = new SavingLog();
						$log->rs = array();
						$log->rs['saving_id'] = $oldRecord['id'];
						$log->rs['user_id'] = Yii::app()->session['user_id'];
						$log->rs['comment'] = !empty($comment)?$comment:"";
						$log->rs['created_datetime'] = date('Y-m-d H:i:s');
						$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
						$log->write();
					}
				}
				//End: saving log

			} //function

			}

			Yii::app()->user->setFlash('saving_message', $mesg);
			if(!empty($_POST['redirect_url']) && $_POST['redirect_url']=='edit'){
				$action = "edit/".$saving_id; 
			}else{
				$action = "list/"; 
			}

			$this->redirect(AppUrl::bicesUrl('savingsUsg/'.$action));
		}


		// get data for the selected vendor
		$view_data = array('saving_id' => $saving_id);
		$saving = new SavingUsg;
		$savingRecord = $saving->getOne(array('id' => $saving_id));
		$view_data['saving'] = $savingRecord;
	   // need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));
		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));
		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));
		// and also product and supplier information for the quote
		$view_data['milestones'] = $saving->getMilestones($saving_id);
		$view_data['saving_status'] = $saving->getStatus();

		$view_data['saving_status_title'] = $saving->getStatus($savingRecord['status']);

		$category = new Category();
        $view_data['categories'] = $category->getAll();

		$savingLogs = new SavingLog();
		$view_data['savingLogs'] = $savingLogs->getSavingLogs($saving_id); 
		$savingsArea = new SavingArea();
		$view_data['savingsArea'] = $savingsArea->getAll();
		$savingsBussinessUnit = new SavingBusinessUnit();
		$view_data['savingsBussinessUnit'] = $savingsBussinessUnit->getAll();
	
        $view_data['currency_rate']  = $currency_rate->getAllCurrencyRate();
        $view_data['currency_rate_detail'] = $currency_rate->getOne(array('id' => $savingRecord['currency_id']));
        $vendor = new Vendor();
        $view_data['vendor']  = $vendor->getOne(array('vendor_id' => $savingRecord['vendor_id']));
        $savingStatus = FunctionManager::savingStatusIgnore();
        // Chart
        $usgReport = new UsgReport();
        $plannedRealized = $usgReport->plannedRealisedGraph($saving_id);
		$view_data['planned_total'] = $plannedRealized['planned_total'];
		$view_data['realized_total'] = $plannedRealized['realized_total'];

	    $sql = "SELECT m.title,m.cost_avoidance+m.cost_reduction as total_project_saving,m.total_realised_saving FROM saving_milestone m

		where m.saving_id=".$saving_id." order by total_project_saving desc";

      	$milestone = Yii::app()->db->createCommand($sql)->query()->readAll();
		$projectSavingArr = $realisedSavingArr = $milestoneArr = array();

		foreach($milestone as $value){
			$projectSavingArr[]  = $value['total_project_saving'];
			 $realisedSavingArr[] = $value['total_realised_saving']; 
			//$milestoneArr[] = explode(" ",$value['title']);
			$milestoneArr[] = $value['title'];
		}

		$view_data['project_saving_chart1'] = $projectSavingArr;
		$view_data['realised_saving_chart1'] = $realisedSavingArr;
		$view_data['milestone_chart1'] = $milestoneArr;
        // Chart
		$this->render('/savingsUsg/edit', $view_data);
		unset(Yii::app()->session['quick_vendor']);
	}

	public function actionCreateByModal(){
       // get data for the selected vendor

		// is someone already logged in?
		date_default_timezone_set(Yii::app()->params['timeZone']);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'quotes_edit';
        unset(Yii::app()->session['saving_milestones']);
		unset(Yii::app()->session['quick_vendor']);
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		$view_data = array();
		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$user = new User();
		$view_data['users'] = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

		$currency_rate = new CurrencyRate();
        $view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

        if(FunctionManager::savingSpecific()){
        $savingsArea = new SavingArea();
        $view_data['savingsArea'] = $savingsArea->getAll();

        $savingsBussinessUnit = new SavingBusinessUnit();
        $view_data['savingsBussinessUnit'] = $savingsBussinessUnit->getAll(); }
        $saving = new SavingUsg();
        $view_data['saving_status'] = $saving->getStatus();

        $category = new Category();
        $view_data['categories'] = $category->getAll(array('soft_deleted' => "0"));

        $material_type = new MaterialType();
        $view_data['material_type'] = $material_type->getAll(array('soft_deleted' => "0"));
        if(!empty($_POST['strSouAnnSav'])){
         $view_data['openModalOnstrSouAnnSav'] = $_POST['strSouAnnSav'];
        }
        $this->renderPartial('create_saving_modal', $view_data);
    }

	public function actionGetVendorDetails()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		// which vendor details are needed?
		$vendor_ids = Yii::app()->input->post('vendor_ids');
		if(!empty($vendor_ids) && !empty(Yii::app()->input->post('quote_id')))
		{   $quote_id = Yii::app()->input->post('quote_id');
			$quote = new Quote();
			$quoteVendor = $quote->getQuoteVendorInfo($vendor_ids, $quote_id);
			if(!empty($quoteVendor)){
				echo json_encode($quoteVendor);
			}else{
				$vendor = new Vendor();
				echo json_encode($vendor->getData(array('vendor_id IN' => explode(",", $vendor_ids))));
			}
		}
		else if(!empty($vendor_ids))
		{
			$vendor = new Vendor();
			echo json_encode($vendor->getData(array('vendor_id IN' => explode(",", $vendor_ids))));
		}
		else echo json_encode(array());
	}

	public function actionListMilestone(){
		error_reporting(0);
		$saving = new SavingUsg();
		$savingID = $_POST['saving_id'];
		$milestones = $saving->getMilestones($savingID);

		$savingRecord = $saving->getOne(array('id' => $savingID));

		$sql="select * from currency_rates where id=".$savingRecord['currency_id'];
		$currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
 

		$data = array();
		$total_milestone=0;
		foreach ($milestones as $value)
		{
			$current_data = array();
            $total_milestone++;
            $dueDate = !in_array($value['due_date'],array('0000-00-00','0000-00-00 00:00:00'))?date(FunctionManager::dateFormat(), strtotime($value['due_date'])):'';
            $deleteFunc = 'deleteMilestone("'.$value['id'].'")';
            $spendLine = $value['base_line_spend']>0?number_format($value['base_line_spend'],0,"",""):1;
            $deduction = number_format($value['cost_reduction'],0,"","");
            $avoidance = number_format($value['cost_avoidance'],0,"","");
            $mileStoneTitle = '';
			if(FunctionManager::savingSpecific()){
				$sql = "SELECT * FROM milestone_field WHERE milestone_id =".$value['id']." and saving_id=".$savingID;

				$durationQuery = Yii::app()->db->createCommand($sql)->query()->readAll();
				$savingDuration =  round($value['saving_duration']);
				$savingMilestoneDueDate = !in_array($value['savings_due_date'],array('0000-00-00','0000-00-00 00:00:00'))?date(FunctionManager::dateFormat(), strtotime($value['savings_due_date'])):'';
				$current_data[] = $savingMilestoneDueDate;
                $costReductionLabel = 'Total Planned Savings';
                $costReductionClass = 'col-md-6';
                $dueDateLabel       = 'Savings Expiration Date';
                $durationOption     = '';
                for($i = 1; $i<=12; $i++){
		             $durationOption .='<option '.($savingDuration==$i?"selected":"").'>'.$i.'</option>';
		        }
			}else{
				$current_data[] = $value['title'];
				$mileStoneTitle =  '<div class="col-md-12">
                	<label class="control-label">Milestone Name</label><br />
                	<input type="text" class="form-control notranslate" name="edit_milestone_title" id="edit_milestone_title" placeholder="Milestone Title" value="'.$value['title'].'" style="width:100%" /></div><div class="clearfix"></div><br />';

                $costReductionLabel = 'Projected Cost Reduction';
                $costReductionClass = 'col-md-4 col-xs-12';
                $dueDateLabel       = 'Due Date';
			}
			
			$current_data[] = $dueDate;
			if(FunctionManager::savingSpecific()){
				$baseLine = '';
			}else{
				$current_data[] = $currencyReader['currency_symbol'].$value['base_line_spend'];
				$baseLine = '<div class="col-md-4 col-xs-12">
        			<label class="control-label">Baseline Spend</label><br />
        			<input type="number" class="form-control notranslate" name="edit_base_line_spend" id="edit_base_line_spend" value="'.$value['base_line_spend'].'" placeholder="Baseline Spend"    />
        			</div>';
			}
			$current_data[] = $currencyReader['currency_symbol'].$deduction;
			if(FunctionManager::savingSpecific()){
				$costAvoidance ='';
			}else{
				$current_data[] = $currencyReader['currency_symbol'].$avoidance;
				$costAvoidance ='<div class="col-md-4 col-xs-12">
        			<label class="control-label">Projected Cost Avoidance </label><br />
        			<input type="number" class="form-control notranslate" name="edit_cost_avoidance" id="edit_cost_avoidance" placeholder="Projected Cost Avoidance" value="'.$avoidance.'" /> 
        			</div>';
			}
			
			$current_data[] = $value['realised_cost_reduction']>0?$currencyReader['currency_symbol'].number_format($value['realised_cost_reduction'],0,"",""):'';
			$current_data[] = $value['realised_cost_avoidance']>0?$currencyReader['currency_symbol'].number_format($value['realised_cost_avoidance'],0,"",""):'';

			//$current_data[] = $value['notes'];
			$disabled = FunctionManager::sandbox();
			$current_data[] = date(FunctionManager::dateFormat(), strtotime($value['created_at']));

			$formID = "savingedit".$value['id'];
			$popUpID = "savingedit".$value['id'];
            $actionLink='<a data-toggle="modal" data-target="#'.$popUpID.'" href="#"><i class="fa fa-edit" style="font-size:18px"></i></a>';
            if(FunctionManager::savingSpecific()){
            	$modelHeadingName = "Financials";
            	$modelFieldNotes = "Financials Notes";
                $savingMilestoneDuration  ='  <div class="col-md-6">
        			<label class="control-label">Savings Duration (in Months)</label> <br />
					<select class="form-control notranslate" placeholder="Savings Duration (in Months)" name="edit_field_value"  value=""   id="edit_field_value" style="width:100%;">
						'.$durationOption.'
					</select>
                    </div>
                    '.$durationTextField.'
       				<div class="clearfix"></div><br />';
            }else{
            	$modelHeadingName = "Milestone";
            	$modelFieldNotes = "Milestone Notes";
            	// $savingMilestoneStartDate ='';
            	$savingMilestoneDuration  ='';
            }
			$infoModal = '<div id="'.$popUpID.'" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:left;font-size: 20px">'.$modelHeadingName.'</h4></div>
                	<div class="modal-body"><form  id="'.$formID.'" action="" method="post" style="text-align: left;">
                	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
                	<script>
                	$(".generaldate").datetimepicker({
    format: "'.FunctionManager::dateFormatJS().'"});
                	</script>	 
                	'.$milestoneTitle.'
                	
                	'.$savingMilestoneDueDate.'
                	'.$savingMilestoneStartDate.'
                    
        			'.$savingMilestoneDuration.'
                  

                    <div class="col-md-12">
        			<label class="control-label">'.$dueDateLabel.'</label><br />
        			<input type="text" onchange = "loadgeneraldate();" class="form-control notranslate generaldate " name="edit_due_date" autocomplete="off" id="edit_due_date" value="'.$dueDate.'" placeholder="'.$dueDateLabel.'" style="width:100%" />
                     </div><div class="clearfix"></div><br />
                    '.$baseLine.'
        			'.$costAvoidance.'
        			 <div class="'.$costReductionClass.'">
        			<label class="control-label">'.$costReductionLabel.'</label><br />
        			<input type="number" class="form-control notranslate" name="edit_cost_reduction" id="edit_cost_reduction" value="'.$deduction.'" placeholder="Projected Cost Reduction"  style="width:100%"/></div>
        			<input type="hidden" class="notranslate" name="milestone_edit_id"  value="'.$value['id'].'"/>
        			<input type="hidden" class="notranslate"  name="edit_saving_id"  value="'.$savingID.'"/>
        			 <div class="clearfix"></div><br />
        			 
        			<div class="col-md-12">
        			<label class="control-label">'.$modelFieldNotes.'</label><br />
        			<textarea class="form-control notranslate" name="edit_milestone_notes" id="edit_milestone_notes" rows="4" placeholder="Savings Notes" style="width:100%" >'.$value['notes'].' </textarea><br /><br />
       				</div>
                	</form>
                   
                   
                	<div class="modal-footer"><button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button> <button  type="button" class="btn btn sm green-btn"  '. $disabled .'  onclick="editMilestone('.$formID.');" >Save</button></div></div></div></div></div> 

                	';

                // Start: Milestone complete modal
                if(strtolower(trim($value['status'])) !="completed"){

                $milestoneID = $value['id'];
                $milestoneRealsedCalculation = 'milestoneRealsedCalculation('.$milestoneID.','.$spendLine.')';
             	$formIDComplete = "savingmilestonecomplete".$milestoneID;
				$popUpIDComplete = "savingmilestonecomplete".$milestoneID;
            	$actionLinkComplete='<a data-toggle="modal" data-target="#'.$popUpIDComplete.'" href="#" class="btn btn-primary submit-btn" '. $disabled .' style="text-decoration:none;font-size:10px;" >Mark As Complete</a>';
				if(FunctionManager::savingSpecific()){
					$markHeadingName = "Financials";
					$confirmMilestoneCompletion = "Confirm Financials Completion";
					$totalRealisedField='';
				}else{
					$markHeadingName = "Milestone";
					$confirmMilestoneCompletion = "Confirm Milestone Completion";
					$totalRealisedField='<label class="control-label">Total Realised Savings % </label><br />
        			<input type="number" class="form-control notranslate" name="complete_edit_realised_saving_perc" id="complete_edit_realised_saving_perc'.$milestoneID.'" placeholder="Projected Cost Avoidance" value="'.number_format($value['total_project_realised_perc'],0,"","").'" style="width:98%" readonly /> ';
				}
				$infoModalComplete = '<div id="'.$popUpIDComplete.'" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:left">'.$markHeadingName.'</h4></div>
                	<div class="modal-body"><form  id="'.$formIDComplete.'" action="" method="post">
                	<table style="width:100%;text-align:left">
                	 
                    <tr><td style="text-align:left">
                     
        			<label class="control-label">Realised Cost Reduction</label><br />
        			<input type="number" class="form-control notranslate" name="complete_edit_realised_cost_reduction" id="complete_edit_realised_cost_reduction'.$milestoneID.'" value="'.($value['realised_cost_reduction']>0?round($value['realised_cost_reduction']):$deduction).'" placeholder="Realised Cost Reduction"  style="width:98%"  onkeyup="'.$milestoneRealsedCalculation.'"  onchange="'.$milestoneRealsedCalculation.'"   /> <br /><br />
        			</td>
                   <td style="text-align:left">
        			<label class="control-label">Realised Cost Avoidance</label><br />
        			<input type="number" class="form-control notranslate" name="complete_edit_realised_cost_avoidance" id="complete_edit_realised_cost_avoidance'.$milestoneID.'" value="'.($value['realised_cost_avoidance']>0?round($value['realised_cost_avoidance']):$avoidance).'" placeholder="Realised Cost Avoidance" style="width:98%"    onkeyup="'.$milestoneRealsedCalculation.'" onchange="'.$milestoneRealsedCalculation.'"/> <br /><br />
       				</td>
       				</tr>
       				<tr>
                    <td style="text-align:left">
        			<label class="control-label">Total Realised Savings </label><br />
        			<input type="number" class="form-control notranslate" name="complete_edit_realised_saving" id="complete_edit_realised_saving'.$milestoneID.'" placeholder="Projected Cost Avoidance" value="'.($value['total_realised_saving'] !="0.00"?round($value['total_realised_saving']):$deduction+$avoidance).'"  style="width:98%" readonly  /> 
        			<input type="hidden" class="notranslate" name="complete_milestone_edit_id"  value="'.$value['id'].'"/>
        			<input type="hidden" class="notranslate" name="complete_edit_saving_id"  value="'.$savingID.'"/>
        			</td><td style="text-align:left">
        			'.$totalRealisedField.'"
        			</td></tr>
        			<br /><br /></table></td></tr>
        			</table>
                	</form>
                   
                	<div class="modal-footer"><button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Cancel</button> <button  type="button" class="btn btn-primary btn sm" onclick="milestoneComplete('.$formIDComplete.');">'.$confirmMilestoneCompletion.'</button></div></div></div></div></div> 

                	';
                }else{
                	$actionLinkComplete='<span class="btn" style="background:#6cce47;border-color:#6cce47;color:#fff;font-size:10px;">Completed</span>';
                	$infoModalComplete = '';
                }
  
                // End: Milestone complete modal
  			if (FunctionManager::checkEnvironment(true)) {
  				$deleteMilestoneRow = "<a style='cursor: pointer; padding: 5px;' onclick='".$deleteFunc."'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a>";
  			}
			$current_data[] = $actionLink.$deleteMilestoneRow.$actionLinkComplete.$infoModalComplete.$infoModal;
			
			$data[] = $current_data;
		}

		$total_milestone=count($milestones);

		$json_data = array(
            "draw"            => intval( $_REQUEST['draw'] ),
            "recordsTotal"    => $total_milestone,
            "recordsFiltered" => $total_milestone,
            "data"            => $data
        );
		echo json_encode($json_data);	
	}


	public function actionAddMileStone(){  
       date_default_timezone_set(Yii::app()->params['timeZone']);
       error_reporting(0);

       $savingMilestones = Yii::app()->session['saving_milestones'];
       $title = !empty($_POST['title'])?$_POST['title']:'';
       //$dueDate = !empty($_POST['due_date'])?date('Y-m-d',strtotime($_POST['due_date'])):'';
      
       $dueDate = $_POST['due_date'];
       $savingsdueDate = $_POST['miletone_due_date'];
       // $savingsStartDate = $_POST['milestone_start_date'];
       $dueDate = !empty($dueDate)?$dueDate:'';
       // $savingsStartDate = !empty($savingsStartDate)?$savingsStartDate:'';
       $savingDuration = !empty(round($_POST['saving_duration']))?round($_POST['saving_duration']):'0';
       $savingsdueDate = !empty($savingsdueDate)?$savingsdueDate:'';
       $baseLineSpend = !empty($_POST['base_line_spend'])?$_POST['base_line_spend']:'0';
       $costReduction = !empty($_POST['cost_reduction'])?$_POST['cost_reduction']:'0';
       $costAvoidance = !empty($_POST['cost_avoidance'])?$_POST['cost_avoidance']:'0';

       $durationMothly = !empty($_POST['duration_monthly'])?$_POST['duration_monthly']:'';

       $fieldArr = array();
       $fieldArr['title'] = $title;
       $fieldArr['due_date'] = $dueDate;
       $fieldArr['savings_due_date'] = $savingsdueDate;
       // $fieldArr['start_date'] = $savingsStartDate;
       $fieldArr['saving_duration'] = $savingDuration;
       $fieldArr['saving_monthly'] = $durationMothly;
       $fieldArr['base_line_spend'] = $baseLineSpend;
       $fieldArr['cost_reduction'] = $costReduction;
       $fieldArr['cost_avoidance'] = $costAvoidance;
       $savingMilestones[] = $fieldArr;
       Yii::app()->session['saving_milestones'] = $savingMilestones;

       if(FunctionManager::savingSpecific()){
			$mileStoneHeading =  "";
			$mileStoneSavingDueDate = "<th>Savings Expiration Date</th>";
			$milestoneBaseline = '';
			$milestoneCostAvoidance = '';
			$milestoneCostReducation = '<th>Total Planned Savings</th>';
			$savingStartDate = '<th>Savings Start Date</th>';
			$savingDuration = '<th>Savings Duration (in Months) </th>';
			
		}else{
			$mileStoneHeading = "<th>Milestone Name</th>";
			$mileStoneSavingDueDate = "<th>Due Date</th>";
			$milestoneBaseline = '<th>Baseline Spend</th>';
			$milestoneCostAvoidance = '<th>Cost Avoidance</th>';
			$milestoneCostReducation = '<th>Cost Reduction</th>';
			$savingStartDate = '';
			$savingDuration = '';
			
		}
       $exp = "<table class='table table-striped table-bordered' style='width: 100%;'>
       				<tr>".$mileStoneHeading."
       				".$savingStartDate."
       				".$mileStoneSavingDueDate."
       				".$milestoneBaseline."
       				".$milestoneCostReducation."
       				".$milestoneCostAvoidance."
       				".$savingDuration."
       			
       				
       		</tr>";
       		// <th>Action</th>
       	$projectSaving = $projectSavingPer = $baseLine = 0;
       	$dueDate = '';
       	$savingsdueDate = '';
        foreach($savingMilestones as $key=>$value){

        	$dueDate = $value['due_date'];
        	$savingsdueDate = $value['savings_due_date'];
        	$disabled = FunctionManager::sandbox();
        	$deleteFunc =$key;
        	if(FunctionManager::savingSpecific()){
        		$projectSavingPer = 0;
        		$projectSaving += $value['cost_reduction'];
        		$savingStartDate = $value['start_date'];
				$mileStoneTitle = "";
				$mileStoneSavingDueDate = "<td>".$value['savings_due_date']."</td>";
				$baseline = "";
				$costAvoidance = "";
				// $savingsStartDate = "<td>".$value['start_date']."</td>";
				$savingDuration   = "<td>".$value['saving_duration']."</td>";
			}else{
				$projectSaving += $value['cost_reduction']+$value['cost_avoidance'];
        		$baseLine += $value['base_line_spend'];
        		$projectSavingPer = $projectSaving/$baseLine;
        		$savingStartDate = "";
				$mileStoneTitle = "<td>".$value['title']."</td>";
				$mileStoneSavingDueDate = "";
				$baseline = '<td>'.$value["base_line_spend"].'</td>';
				$costAvoidance = '<td>'.$value["cost_avoidance"].'</td>';
				// $savingsStartDate = "";
				$savingDuration = "";
			}
        	$exp .="<tr>".$mileStoneTitle.$mileStoneSavingDueDate."<td '".$disabled."'>".$value['due_date']."</td>".$baseline."<td>".$value['cost_reduction']."</td>".$costAvoidance.$savingDuration."</tr>";
        	// <td><a style='cursor: pointer; padding: 5px;' ". $disabled ." onclick='deleteMilestone(".$deleteFunc.")'><span class='glyphicon ". $disabled ." glyphicon-trash' aria-hidden='true' ></span></a></td>
        }
        $exp .="</table>";

 
       $data = array();
       $data['mesg'] =1;
       $data['record_list'] = $exp;
       $data['total_project_saving'] = $projectSaving;
       $data['total_project_saving_perc'] = $projectSavingPer * 100;
       $data['due_date'] = $dueDate;
       $data['savings_due_date'] = $savingsdueDate;
       // $data['start_date'] = $milestoneStartDate;

       echo json_encode($data);exit;

    }

    public function actionDeleteMilestone()
    {    
    	$key = $_POST['recordKey'];
		$savingMilestones = Yii::app()->session['saving_milestones'];
		$disabled = '';

		unset($savingMilestones[$key]);
		Yii::app()->session['saving_milestones'] = $savingMilestones;

		if(FunctionManager::savingSpecific()){
			$mileStoneHeading = "";
		}else{
			$mileStoneHeading = "<th>Milestone Name</th>";
		}
		$exp = "<table class='table table-striped table-bordered' style='width: 100%;'>
       				<tr>".$mileStoneHeading."
       				<th>Due Date</th>
       				<th>Baseline Spend</th>
       				<th>Cost Reduction</th>
       				<th>Cost Avoidance</th>
       				<th>Action</th>
       		</tr>";
       	$dueDate = '';
       	$projectSaving = $projectSavingPer = 0;

        foreach($savingMilestones as $key=>$value){
        	if(empty($value['cost_reduction'])){
        		$value['cost_reduction'] = 0;
        	}
        	if(empty($value['cost_avoidance'])){
        		$value['cost_avoidance'] = 0;
        		
        	}
        	if(empty($value['base_line_spend'])){
        		$value['base_line_spend'] = 1;
        		
        	}
        	$projectSaving += $value['cost_reduction']+$value['cost_avoidance'];
        	$projectSavingPer = $projectSaving/$value['base_line_spend'];
        	$dueDate = $value['due_date'];

        	$deleteFunc = 'deleteMilestone("'.$key.'")';

        	if(FunctionManager::savingSpecific()){
				$mileStoneTitle = "";
			}else{
				$mileStoneTitle = "<td>".$value['title']."</td>";
			}

        	$exp .="<tr>".$mileStoneTitle."<td>".$value['due_date']."</td><td>".$value['base_line_spend']."</td><td>".$value['cost_reduction']."</td><td>".$value['cost_avoidance']."</td><td><a style='cursor: pointer; padding: 5px;' onclick='".$deleteFunc."' '". $disabled ."' ><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a></td></tr>";
        }
        $exp .="</table>";

       $data = array();
       $data['mesg'] =1;
       $data['record_list'] =$exp;
       $data['total_project_saving'] = $projectSaving;
       $data['total_project_saving_perc'] = $projectSavingPer * 100; 
       $data['due_date'] = $dueDate;
       echo json_encode($data);exit;
    }

    public function actionDeleteSavingMilestone()
    { 
    	$key = $_POST['recordKey'];
    	
    	$sql = "select saving_id from  saving_milestone where id=".$key;
    	$record = Yii::app()->db->createCommand($sql)->queryRow();

    	$sql = "delete from  saving_milestone where id=".$key;
    	$deleteRecorder = Yii::app()->db->createCommand($sql)->execute();
    	if(!empty($deleteRecorder)){
    		$mesg = 1;
    	}else{
    		$mesg = 2;
    	}
    	FunctionManager::savingCalculationSave($record['saving_id']);
       $data = array();
       $data['mesg'] =$mesg;
       echo json_encode($data);exit;
    }

    public function actionDeletePlan()
    { 
    	$key = $_POST['recordKey'];
    	
    	$sql = "select saving_id from  milestone_field where id=".$key;
    	$record = Yii::app()->db->createCommand($sql)->queryRow();

    	$sql = "delete from  milestone_field where id=".$key;
    	$deleteRecorder = Yii::app()->db->createCommand($sql)->execute();
    	if(!empty($deleteRecorder)){
    		$mesg = 1;
    	}else{
    		$mesg = 2;
    	}

    	FunctionManager::savingCalculationSave($record['saving_id']);
    	//$this->saveOrUpdateMileStone($record['saving_id']);
    	$savings = new SavingUsg();
    	$savingObj = new SavingUsg();
		$savingObj->rs = array();
		$savingObj->rs['id'] = $record['saving_id'];
		$savingObj->rs['total_project_saving'] = $savings->projectSavings($record['saving_id']);
		$savingObj->rs['realised_saving'] = $savings->realizedSavings($record['saving_id']);
		$savingObj->write();

       $data = array();
       $data['mesg'] =$mesg;
       echo json_encode($data);exit;
    }

    public function actionAddSavingMilestone(){ 
    	$title = !empty($_POST['title'])?$_POST['title']:'';
		$dueDate = $_POST['due_date'];
		if(!empty($dueDate) && FunctionManager::dateFormat()=="d/m/Y"){
			$dueDate = strtr($dueDate, '/', '-');
		}
		$dueDate = !empty($dueDate)?date("Y-m-d", strtotime($dueDate)):'0000-00-00 ';
		$baseLineSpend = !empty($_POST['base_line_spend'])?$_POST['base_line_spend']:'0';
		$costReduction = !empty($_POST['cost_reduction'])?$_POST['cost_reduction']:'0';
		$costAvoidance = !empty($_POST['cost_avoidance'])?$_POST['cost_avoidance']:'0';

		$notes = !empty($_POST['notes'])?addslashes($_POST['notes']):'';
		$saving_id = $_POST['saving_id'];
		$createdAt = date("Y-m-d");
		 
		$savingMilestone = new SavingMilestone;
		$savingMilestone->rs = array();
		$savingMilestone->rs['id'] = 0;
		$savingMilestone->rs['saving_id'] = $saving_id;
		$savingMilestone->rs['title'] = addslashes($title);
		$savingMilestone->rs['due_date'] = $dueDate;
		$savingMilestone->rs['base_line_spend'] = $baseLineSpend;
		$savingMilestone->rs['cost_reduction'] = $costReduction;
		$savingMilestone->rs['cost_avoidance'] = $costAvoidance;
		$savingMilestone->rs['notes'] = $notes;
		$savingMilestone->rs['created_at'] = $createdAt;
		if(FunctionManager::savingSpecific()){
			$savingmilestonedueDate = $_POST['savings_due_date'];
			if(!empty($savingmilestonedueDate) && FunctionManager::dateFormat()=="d/m/Y"){
				$savingmilestonedueDate = strtr($savingmilestonedueDate, '/', '-');
			} 
			$savingmilestonedueDate = !empty($savingmilestonedueDate)?date("Y-m-d", strtotime($savingmilestonedueDate)):'0000-00-00';
			$savingMilestone->rs['savings_due_date'] = $savingmilestonedueDate;

			
			// $savingsStartDate = $_POST['saving_start_date'];
			// if(!empty($savingsStartDate) && FunctionManager::dateFormat()=="d/m/Y"){
			// 	$savingsStartDate = strtr($savingsStartDate, '/', '-');
			// } 
			// $savingsStartDate = !empty($savingsStartDate)?date("Y-m-d", strtotime($savingsStartDate)):'0000-00-00';

			// $savingMilestone->rs['start_date'] = $savingsStartDate;

			$savingDuration = !empty($_POST['saving_duration'])?$_POST['saving_duration']:'0';
			$savingMilestone->rs['saving_duration'] = $savingDuration;
			 

		}
		$savingMilestone->write();
		if($savingMilestone->rs['id']>0){
			// Start: Duration Planning Per month 
			if(FunctionManager::savingSpecific() && !empty($savingMilestone->rs['id'])){

				$savingMonthly = !empty($_POST['duration_monthly'])?$_POST['duration_monthly']:'';
				$milestone_id = $savingMilestone->rs['id'];
				foreach($savingMonthly as $key=>$fieldValue){
					$milestoneField = new MilestoneField;
					$milestoneField->rs = array();
					$milestoneField->rs['id'] = 0;
					$milestoneField->rs['saving_id'] = $saving_id;
					$milestoneField->rs['milestone_id'] = $milestone_id;
					$milestoneField->rs['field_name'] = $fieldValue['fieldName'];
					$milestoneField->rs['field_value'] = $fieldValue['fieldValue'];
					$milestoneField->write();
				}
			}
			//End: Duration Planning Per month 
			$mesg = "1";
			FunctionManager::savingCalculationSave($saving_id);
			Yii::app()->user->setFlash('saving_message', 'Milestone added successfully.');
		}else{
			$mesg = "2";
			Yii::app()->user->setFlash('saving_message', 'Problem occured while saving Milestone, try again.');
		}
		$data = array();
		$data['mesg'] = $mesg;

		echo json_encode($data);exit;
	}

	public function actionEditMilestone(){ 
		 
		$id = $_POST['milestone_edit_id'];
    	$title = !empty($_POST['edit_milestone_title'])?$_POST['edit_milestone_title']:'';
		$dueDate = $_POST['edit_due_date'];
		if(!empty($dueDate) && FunctionManager::dateFormat()=="d/m/Y"){
			$dueDate = strtr($dueDate, '/', '-');
		} 
		$dueDate = !empty($dueDate)?date("Y-m-d", strtotime($dueDate)):'0000-00-00';

		$baseLineSpend = !empty($_POST['edit_base_line_spend'])?$_POST['edit_base_line_spend']:'0';
		$costReduction = !empty($_POST['edit_cost_reduction'])?$_POST['edit_cost_reduction']:'0';
		$costAvoidance = !empty($_POST['edit_cost_avoidance'])?$_POST['edit_cost_avoidance']:'0';
		$notes = !empty($_POST['edit_milestone_notes'])?$_POST['edit_milestone_notes']:'';
		$saving_id = $_POST['edit_saving_id'];
		$createdAt = date("Y-m-d");
		$savingMilestone = new SavingMilestone;
		$savingMilestone->rs = array();
		$savingMilestone->rs['id'] = $id;
		$savingMilestone->rs['saving_id'] = $saving_id;
		$savingMilestone->rs['title'] = addslashes($title);
		$savingMilestone->rs['due_date'] = $dueDate;
		$savingMilestone->rs['base_line_spend'] = $baseLineSpend;
		$savingMilestone->rs['cost_reduction'] = $costReduction;
		$savingMilestone->rs['cost_avoidance'] = $costAvoidance;
		$savingMilestone->rs['notes'] = addslashes($notes);
		//$savingMilestone->rs['created_at'] = $createdAt;

		if(FunctionManager::savingSpecific()){
			// $savingmilestonedueDate = $_POST['milestone_saving_due_date'];
			// if(!empty($savingmilestonedueDate) && FunctionManager::dateFormat()=="d/m/Y"){
			// 	$savingmilestonedueDate = strtr($savingmilestonedueDate, '/', '-');
			// } 
			// $savingmilestonedueDate = !empty($savingmilestonedueDate)?date("Y-m-d", strtotime($savingmilestonedueDate)):'0000-00-00';
			// $savingMilestone->rs['savings_due_date'] = $savingmilestonedueDate;

			/*$savingsStartDate = $_POST['edit_start_date'];
			if(!empty($savingsStartDate) && FunctionManager::dateFormat()=="d/m/Y"){
				$savingsStartDate = strtr($savingsStartDate, '/', '-');
			} 
			$savingsStartDate = !empty($savingsStartDate)?date("Y-m-d", strtotime($savingsStartDate)):'0000-00-00';

			$savingMilestone->rs['start_date'] = $savingsStartDate;*/

			$savingDuration = !empty($_POST['edit_field_value'])?$_POST['edit_field_value']:'0';
			$savingMilestone->rs['saving_duration'] = $savingDuration;

		}

		$savingMilestone->write();
		if($savingMilestone->rs['id']>0){
			$milestone_id = $savingMilestone->rs['id'];
			$saving_id    = $savingMilestone->rs['saving_id'];
			// Start: Duration Planning Per month 
			if(FunctionManager::savingSpecific() && !empty($savingMilestone->rs['id'])){
				$savingMonthly = $_POST['duration_monthly'];
				 
				if(!empty($_POST['duration_monthly'])){
				$milestone_id = $savingMilestone->rs['id'];
				$sql = "delete from  milestone_field WHERE milestone_id =".$milestone_id;
				Yii::app()->db->createCommand($sql)->execute();

				foreach($savingMonthly as $key=>$fieldValue){
					$milestoneField = new MilestoneField;
					$milestoneField->rs = array();
					$milestoneField->rs['id'] = 0;
					$milestoneField->rs['saving_id'] = $saving_id;
					$milestoneField->rs['milestone_id'] = $milestone_id;
					$milestoneField->rs['field_name'] = $key;
					$milestoneField->rs['field_value'] = $fieldValue;
					$milestoneField->write();
				}}
			}
			//End: Duration Planning Per month 
			$mesg = "1";
			FunctionManager::savingCalculationSave($saving_id);
		}else{
			$mesg = "2";
		}
		$data = array();
		$data['mesg'] = $mesg;
		echo json_encode($data);exit;
	}
 
          
	public function actionMilestoneComplete(){ 
		 
		$id = $_POST['complete_milestone_edit_id'];
		$savingMilestone = new SavingMilestone;
    	$oldMilestone = $savingMilestone->getOne(array('id' => $id));
		 
		$realisedReduction = !empty($_POST['complete_edit_realised_cost_reduction'])?$_POST['complete_edit_realised_cost_reduction']:'0';
		$realiseAvoidance = !empty($_POST['complete_edit_realised_cost_avoidance'])?$_POST['complete_edit_realised_cost_avoidance']:'0';
		$spendline = $oldMilestone['base_line_spend']>0?$oldMilestone['base_line_spend']:1;

		$saving_id = $_POST['complete_edit_saving_id'];
		$completedAt = date("Y-m-d");

		$savingMilestone = new SavingMilestone;
		$savingMilestone->rs = array();
		$savingMilestone->rs['id'] = $id;
		$savingMilestone->rs['saving_id'] = $saving_id;

		$savingMilestone->rs['realised_cost_reduction'] = $realisedReduction;
		$savingMilestone->rs['realised_cost_avoidance'] = $realiseAvoidance;
		$savingMilestone->rs['total_realised_saving'] = $realisedReduction+$realiseAvoidance;
		$savingMilestone->rs['total_realised_saving_perc'] = ($realisedReduction+$realiseAvoidance)/$spendline;
 
		$savingMilestone->rs['completed_by_id'] = Yii::app()->session['user_id'];
		$savingMilestone->rs['completed_by_name'] = Yii::app()->session['full_name'];
		$savingMilestone->rs['completed_date'] = Yii::app()->session['full_name'];
		$savingMilestone->rs['completed_date'] = $completedAt;
		$savingMilestone->rs['status'] = 'Completed';
		$savingMilestone->write();
		if($savingMilestone->rs['id']>0){
			$mesg = "1";
			FunctionManager::savingCalculationSave($saving_id);
		}else{
			$mesg = "2";
		}
		$data = array();
		$data['mesg'] = $mesg;
		echo json_encode($data);exit;
	}

	

	public function actionSavingCalculationFields(){
		$savingID = $_POST['saving_id'];
		$sql = "SELECT s.total_project_saving,s.total_project_saving/sum(sm.base_line_spend)*100 as total_project_saving_perc,s.realised_saving,s.realised_saving/sum(sm.base_line_spend)*100 as realised_saving_perc,s.due_date FROM savings s
		left join saving_milestone as sm on s.id = sm.saving_id  where s.id=".$savingID;
		$reader = Yii::app()->db->createCommand($sql)->queryRow();
		if(!empty($reader)){
			$projectSaving = $reader['total_project_saving'] !="0.00"?$reader['total_project_saving']:"";
			$projectSavingPer = $reader['total_project_saving_perc'] !="0.00"?$reader['total_project_saving_perc']:"";
			$realisedSaving = $reader['realised_saving'] !="0.00"?$reader['realised_saving']:"";
			$realisedSavingPer = $reader['realised_saving_perc'] !="0.00"?$reader['realised_saving_perc']:"";
			$dueDate = $reader['due_date'] !="0000-00-00"?date(FunctionManager::dateFormat(),strtotime($reader['due_date'])):"";
		}
		$data = array();
		$data['total_project_saving'] = $projectSaving;
		$data['total_project_saving_perc'] = $projectSavingPer;
		$data['realised_saving'] = $realisedSaving;
		$data['realised_saving_perc'] = $realisedSavingPer;
		$data['due_date'] = $dueDate;
		echo json_encode($data);exit;
    }

    public function actionSavingChart1(){
		$savingID = $_POST['saving_id'];
		$sql = "SELECT title,cost_avoidance+cost_reduction as total_project_saving,total_realised_saving FROM saving_milestone m where m.saving_id=".$savingID." order by total_project_saving desc";

      	$milestone = Yii::app()->db->createCommand($sql)->query()->readAll();
		$data = array();
		$projectSavingArr = $realisedSavingArr = $milestoneArr = array();

		foreach($milestone as $value){
			$projectSavingArr[] = $value['total_project_saving'];
			$realisedSavingArr[] = $value['total_realised_saving'];
			//$milestoneArr[] = explode(" ",$value['title']);
			$milestoneArr[] = $value['title'];
		}
		$data['project_saving'] = $projectSavingArr;
		$data['realised_saving'] = $realisedSavingArr;
		$data['milestone'] = $milestoneArr;

		echo json_encode($data);exit;
    }

    public function actionSavingChart2(){
		$savingID = $_POST['saving_id'];
		$sql = "SELECT title,sum(cost_avoidance) as p_avoidance,sum(cost_reduction) as p_reduction,sum(realised_cost_avoidance) as r_avoidance,sum(realised_cost_reduction) as r_reduction FROM saving_milestone m where m.saving_id=".$savingID;
      	$milestone = Yii::app()->db->createCommand($sql)->queryRow();
		$data = array();
		 
		$avoidanceArr = array($milestone['p_avoidance'],$milestone['r_avoidance']);
		$redutionArr = array($milestone['p_reduction'],$milestone['r_reduction']);
		 
		$data['avoidance'] = $avoidanceArr;
		$data['reduction'] = $redutionArr;
		echo json_encode($data,JSON_NUMERIC_CHECK);exit;
    }
		
	public function actionDurationValue(){
		$duration = isset($_POST['duration']) ? $_POST['duration'] : "";
		if(!in_array($duration,array("1","2"))){
			$durationEndDate = $duration/*-1*/;
		}else{$durationEndDate = $duration;}
		$duration = $duration-1;
		$totalPlanned = isset($_POST['cost_reduction']) ? $_POST['cost_reduction'] : "0";
		if(FunctionManager::dateFormat()=="d/m/Y"){
			$startDate = strtr($_POST['start_date'], '/', '-');
			$startDate = strtr($_POST['start_date'], '/', '-');
		}else{
			$startDate = $_POST['start_date'];
		}

		$endDate = date(FunctionManager::dateFormat(),strtotime($startDate . "+$durationEndDate months"));
		$startDateMonth =  date("m",strtotime($startDate)); //month
		 
		$durationNumber = $startDateMonth+$duration;
		$perDurationPlanned = round($totalPlanned/($duration+1));

		$durationList = '<br /><div class="col-md-6 col-xs-12" style="text-align: center;"><label class="control-label">Amount</label></div><div class="clearfix"></div><br />';
		$j=1;
		for($i=0;$i<=$duration;$i++){
		   /*$showMonth =  date("Y-$i-d");
		   $showMonth =  date("M-y",strtotime($showMonth));
		   $showMonthIndex =  date("$i-Y");*/
		   $showMonth =  date("M-y",strtotime($startDate . "+$i months"));
		   $showMonthIndex =  date("Y-m-d",strtotime($startDate . "+$i months"));
		   $durationList .= '
		   <div class="col-md-2 col-xs-12">
		   <label class="control-label">'.$showMonth.'</label>
		   </div>
		   <div class="col-md-4 col-xs-12"><input class="form-control notranslate milestone-plann-duration" type="text" name="'.$showMonthIndex.'" value="'.$perDurationPlanned.'">
		   </div><div class="clearfix"></div><br />';
		   $j++;
		}
		/*$durationList = 
        '<label class="control-label">Amount</label><br>
        <label class="control-label">Apr-22</label><input type="" name=""><br>
        <label class="control-label">Apr-22</label><input type="" name=""><br>
        <label class="control-label">Apr-22</label><input type="" name=""><br>
        <label class="control-label">Apr-22</label><input type="" name=""><br>
        <label class="control-label">Apr-22</label><input type="" name="">';*/

      echo json_encode(array(
            	'durationList' => $durationList,
      			'endDate' => $endDate,
      			//'totalPlanned' => $totalPlanned
      			)
  			);

	}

    //Basheer Alam / Planned Saving field store against saving ID 
	public function actionPlannedStore(){
		$edit_planned_saving_id = $_POST['planned_saving_id'];
		$edit_month = $_POST['planned_month'];
		$edit_planned_amount = $_POST['planned_amount'];

		if(!empty($edit_month) && FunctionManager::dateFormat()=="d/m/Y"){
			$edit_month = strtr($edit_month, '/', '-');
		} 
		$edit_planned_month = !empty($edit_month)?date("Y-m-d ", strtotime($edit_month)):'0000-00-00';

		$milestoneField = new MilestoneField;
		$milestoneField->rs = array();
		$milestoneField->rs['id'] = 0;
		$milestoneField->rs['saving_id']  = $edit_planned_saving_id;
		$milestoneField->rs['field_name'] = $edit_planned_month;
		$milestoneField->rs['field_value']= $edit_planned_amount;
		$milestoneField->write();
		
       	// Case if milestone ID exist update the record, other wise insert
       	$this->saveOrUpdateMileStone($edit_planned_saving_id);

		$savingsUpdate = new SavingUsg();
		$savingObj = new SavingUsg();
		$savingObj->rs = array();
		$savingObj->rs['id'] = $edit_planned_saving_id;
		$savingObj->rs['due_date'] = $this->milestoneFieldsGetOneRecordAscOrDesc($edit_planned_saving_id)['milestoneDueDate'];
		$savingObj->rs['total_project_saving'] = $savingsUpdate->projectSavings($edit_planned_saving_id);
		$savingObj->write();

		
		
		Yii::app()->user->setFlash('saving_message','Planned Savings Added Successfully.');
		$this->redirect(array('savingsUsg/edit/'.$edit_planned_saving_id.'?tab=financial'));
	}

   

	
	public function actionPlannedEdit(){
		$edit_planned_id = $_POST['edit_planned_id'];
		$edit_planned_saving_id = $_POST['edit_planned_saving_id'];
		$edit_planned_milestone_id = $_POST['edit_planned_milestone_id'];
		$edit_month = $_POST['edit_planned_month'];

		if(!empty($edit_month) && FunctionManager::dateFormat()=="d/m/Y"){
			$edit_month = strtr($edit_month, '/', '-');
		} 
		$edit_planned_month = !empty($edit_month)?date("Y-m-d ", strtotime($edit_month)):'0000-00-00';

		$edit_planned_amount = $_POST['edit_planned_amount'];

		// $sql = "UPDATE milestone_field SET field_name = '".$edit_planned_month."', field_value = '".$edit_planned_amount."' WHERE id = ".$edit_planned_id;
		// $milestone = Yii::app()->db->createCommand($sql)->execute();
		$oldRecord = '';
		if(!empty($edit_planned_id)){
			$milestoneFieldOld = new MilestoneField;
			$oldRecord = $milestoneFieldOld->getOne(array('id'=>$edit_planned_id));
		}

		$milestoneField = new MilestoneField;
		$milestoneField->rs = array();
		$milestoneField->rs['id'] = $edit_planned_id;
		$milestoneField->rs['field_name'] = $edit_planned_month;
		$milestoneField->rs['field_value'] = $edit_planned_amount;
		$milestoneField->write();
		$savingsUpdate = new SavingUsg();
		$savingObj = new SavingUsg();
		$savingObj->rs = array();
		$savingObj->rs['id'] = $edit_planned_saving_id;
		$savingObj->rs['total_project_saving'] = $savingsUpdate->projectSavings($edit_planned_saving_id);
		$savingObj->write();

		// Case if milestone ID exist update the record, other wise insert
       	//$this->saveOrUpdateMileStone($edit_planned_saving_id);

		$comment = '';
		if(trim($oldRecord['field_name'])!=trim($edit_planned_month)){
        $comment .= '<strong> Savings Financials Month-Year:</strong> <span class="title-text"><b>'. date("Y-m-d ", strtotime($oldRecord['field_name'])). ' </b>changed to <b>'.$edit_month.'</b></span><br/>';
        }

        if(trim($oldRecord['field_value'])!=trim($edit_planned_amount)){
        $comment .= '<strong> Savings Financials Amount:</strong> <span class="title-text"><b>'. $oldRecord['field_value']. ' </b>changed to <b>'.$edit_planned_amount.'</b></span><br/>';
        }


		if(!empty($comment)){
			$log = new SavingLog();
			$log->rs = array();
			$log->rs['saving_id'] = $edit_planned_saving_id;
			$log->rs['user_id'] = Yii::app()->session['user_id'];
			$log->rs['comment'] = !empty($comment)?$comment:"";
			$log->rs['created_datetime'] = date('Y-m-d H:i:s');
			$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
			$log->write();
		}
		Yii::app()->user->setFlash('saving_message','Planned Savings Updated Successfully.');
		$this->redirect(array('savingsUsg/edit/'.$edit_planned_saving_id.'?tab=financial'));
	}
	public function actionmarkCompleteEdit(){
	    error_reporting(0);
		$completedAt = date("Y-m-d h:i:s");
		$plannedID = $_POST['edit_planned_id'];
		$milestoneID = !empty($_POST['edit_planned_milestone_id'])?$_POST['edit_planned_milestone_id']:0;
		$savingID = $_POST['edit_planned_saving_id'];
		$realisedReduction = $_POST['complete_edit_realised_cost_reduction'];
		$realiseAvoidance = $_POST['complete_edit_realised_cost_avoidance'];
		$totalReleased = $_POST['complete_edit_realised_saving'];
		$comments = addslashes($_POST['complete_edit_comment']);
		$amount = $_POST['edit_planned_amount'];
		$oldRecord = '';
		if(!empty($plannedID)){
			$milestoneFieldOld = new MilestoneField;
			$oldRecord = $milestoneFieldOld->getOne(array('id'=>$plannedID));
		}
		$milestoneField = new MilestoneField;
		$milestoneField->rs = array();
		$milestoneField->rs['id'] = $plannedID;
		$milestoneField->rs['field_value'] = $amount;
		$milestoneField->rs['total_realised_savings'] = $realisedReduction;
		$milestoneField->rs['status'] = 'Completed';
		$milestoneField->rs['comments'] = $comments;
		$milestoneField->rs['comments_by_id'] = Yii::app()->session['user_id'];
		$milestoneField->rs['comments_by_name'] = Yii::app()->session['full_name'];
		$milestoneField->rs['comments_date'] = $completedAt;
		$milestoneField->rs['completed_by_id'] = Yii::app()->session['user_id'];
		$milestoneField->rs['completed_by_name'] = Yii::app()->session['full_name'];
		$milestoneField->rs['completed_date'] = $completedAt;
		$milestoneField->write();

		//Start: comments
		if(!empty($comments)){
			$milestoneFieldComment = new MilestoneFieldComment;
			$milestoneFieldComment->rs = array();
			$milestoneFieldComment->rs['milestone_field_id'] = $plannedID;
			$milestoneFieldComment->rs['user_id'] = Yii::app()->session['user_id'];
			$milestoneFieldComment->rs['user_name'] = Yii::app()->session['full_name'];
			$milestoneFieldComment->rs['comment'] = $comments;
			$milestoneFieldComment->rs['datetime'] = $completedAt;
			$milestoneFieldComment->write();
		}
		//End: comments

		if(!empty($milestoneField->rs['id'])){			
			$savingMilestone = new SavingMilestone;
			$savingMilestone->rs = array();
			$savingMilestone->rs['id'] = $milestoneID;
			$savingMilestone->rs['saving_id'] = $savingID;
			$savingMilestone->rs['realised_cost_reduction'] = $realisedReduction;
			$savingMilestone->rs['realised_cost_avoidance'] = 0;
			$savingMilestone->rs['total_realised_saving']   = $realisedReduction;//$totalReleased;
			$savingMilestone->write();
			if($savingMilestone->rs['id']>0){
				//FunctionManager::savingCalculationSaveUSG($savingID);
			}
			$alert = 'Planned Savings competed successfully.';
		}else{
			$alert = 'There was a problem, please, try again.';
		}
		$savingsUpdate = new SavingUsg();
		$savingObj = new SavingUsg();
		$savingObj->rs = array();
		$savingObj->rs['id'] = $savingID;
		$savingObj->rs['total_project_saving'] = $savingsUpdate->projectSavings($savingID);
		$savingObj->rs['realised_saving'] = $savingsUpdate->realizedSavings($savingID);
		$savingObj->write();


		$comment = '';
        if(trim($oldRecord['field_value'])!=trim($amount)){
        $comment .= '<strong> Mark As Complete Planned Savings:</strong> <span class="title-text"><b>'. $oldRecord['field_value']. ' </b>changed to <b>'.$amount.'</b></span><br/>';
        }
        if(trim($oldRecord['total_realised_savings'])!=trim($realisedReduction)){
        $comment .= '<strong> Mark As Complete Total Realised Savings:</strong> <span class="title-text"><b>'. $oldRecord['total_realised_savings']. ' </b>changed to <b>'.$realisedReduction.'</b></span><br/>';
        }
        if(!empty($comments)){
        	$comment .= '<strong> Mark As Complete Comments:</strong> <span class="title-text"><b>'. $comments.'</b></span><br/>';
    	}
		if(!empty($comment)){
			$log = new SavingLog();
			$log->rs = array();
			$log->rs['saving_id'] = $savingID;
			$log->rs['user_id'] = Yii::app()->session['user_id'];
			$log->rs['comment'] = !empty($comment)?$comment:"";
			$log->rs['created_datetime'] = date('Y-m-d H:i:s');
			$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
			$log->write();
		}
        
		Yii::app()->user->setFlash('saving_message',$alert);
		$this->redirect(array('savingsUsg/edit/'.$savingID.'?tab=financial'));
	}

	public function actionSavingApprove(){
		$savingId 	   = $_POST['saving_id'];
		$approverId    = $_POST['approver_id'];
		$status  = $_POST['status'];
		$userId  = Yii::app()->session['user_id'];
		$userName  = Yii::app()->session['full_name'];
		$createdAt = date("Y-m-d H:i:s");
		$comment = addslashes($_POST['approver_comment']);
		$approvedTime  = $_POST['approver_datetime'];
		$userObj = new User();
		if(!empty($approverId)){
			$approverRecord = $userObj->getOne(array('user_id'=>$approverId));
			$approver_name = $approverRecord['full_name'];
		}else{
			$approver_name = '';
		}
		$savingObj = new SavingUsg();
		$savingCheck = $savingObj->checkApprovel($savingId);
		//$statusTitle = $savingObj->getStatus($status);
		if($status == "Approved" || $status == "Rejected"){
	 		$userTypeID = $savingCheck['user_id'];
	 	}else {
	 		$userTypeID = $approverId;
	 	}
		$userRecord = $userObj->getOne(array('user_id'=>$userTypeID));
		$savingObj->rs = array();
		$savingObj->rs['id'] = $savingId;
		$savingObj->rs['approver_id'] = $approverId;
		$savingObj->rs['approver_name'] = $approver_name;
		$savingObj->rs['approver_status'] = $status;
		$savingObj->write();

		$historyObj = new SavingApprovalHistory();
		$historyObj->rs = array();
		$historyObj->rs['id'] = 0;
		$historyObj->rs['user_id'] = $userId;
		$historyObj->rs['saving_id'] = $savingId;
		$historyObj->rs['user_name'] = $userName;
		$historyObj->rs['approver_id'] = $approverId;
		$historyObj->rs['status'] = $status;
		$historyObj->rs['approver_comment'] = $comment;
		$historyObj->rs['approver_datetime'] = $approvedTime;
		$historyObj->rs['created_at'] = $createdAt;
		$historyObj->write();
		$savingRecord = $savingObj->getOne(array('id'=>$savingId));
		$savingTitle   		= $savingRecord['title'];
		$userEmail  		= $userRecord['email'];
		$userName  			= $userRecord['full_name'];
		$subject			= 'Supplier '.$status;
		$notificationFlag 	= $savingId.'clientSaving'.$status.$userId;
		if($status == "Sent For Approval" || $status == "Approved" || $status == "Rejected"){	
		if($status == "Sent For Approval"){
        	$notificationComments  = "You have a request to approve Savings ".$savingTitle;
        	$subject = 'Savings Approval Request';
        }else if($status == "Approved"){
        	$notificationComments  = "Savings: <b>".$savingTitle."</b> has been approved";
        	$subject = 'Savings Approved';
        }else{
        	$subject = 'Savings Rejected';
        	$notificationComments  = "Savings: <b>".$savingTitle."</b> has been rejected";
        }
		/*$subject = 'Saving Status: '.$status;
		$notificationComments  = "Savings: <b>".$savingTitle." Status ".$status."</b>";*/
		$notification = new Notification();
		$notification->rs = array();
		$notification->rs['id'] = 0;
		$notification->rs['user_type'] = 'Client';
		$notification->rs['notification_flag'] = $notificationFlag;
		$notification->rs['user_id'] = $userTypeID;
		$notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('savingsUsg/edit/' . $savingId) .'">'.$notificationComments.'</a>';
		$notification->rs['notification_date'] = date("Y-m-d H:i");
		$notification->rs['read_flag'] = 0;
		$notification->rs['notification_type'] = 'Savings Status';
		$notification->write();
		$notificationText = '<a href="' . AppUrl::bicesUrl('savingsUsg/edit/' . $savingId) . '" style="font-size:18px;color:#2d9ca2;text-decoration:none">'.$notificationComments.'</a>';
		$url = AppUrl::bicesUrl('savingsUsg/edit/' . $savingId);
		EmailManager::userNotification($userEmail,$userName,$subject,$notificationText,$url);
		}

		$commentLog = '';
		 if(!empty($savingCheck['approver_status'])){
	        if($savingCheck['approver_id'] !=$approverId ){
	          $sql = "SELECT full_name  FROM users WHERE  user_id=".$approverId." order by full_name asc ";
	          $userReader = Yii::app()->db->createCommand($sql)->queryRow();
	          $commentLog .= '<b>User Approver:</b> <span class="title-text"><b>'. $savingCheck['approver_name']. '</b> Changed to <b>'.$userReader['full_name'].'</b></span><br/>';
	        }
	        if($savingCheck['approver_status'] != $status ){
	         //$statusTitleOld = $savingObj->getStatus($savingCheck['status']);
	         $statusTitleOld = $savingCheck['approver_status'];
	          $commentLog .= '<b>Status:</b> <span class="title-text"><b>'. $statusTitleOld. '</b> Changed to <b>'.$statusTitle.'</b></span><br/>';
	        }
	        if(!empty($comment)){
	          $commentLog .= '<b>Comment:</b> <span class="title-text"><b>'. $comment. '</b></span><br/>';
	        }
	        /*if(!empty($approvedTime) ){
	        	if(FunctionManager::dateFormat()=="d/m/Y")
                 $approvedTime = date(FunctionManager::dateFormat(), strtotime(strtr($approvedTime, '/', '-')));
               else{
               $approvedTime = date(FunctionManager::dateFormat(),strtotime($approvedTime)) ;
               }

	          $comment .= '<b>Date:</b> <span class="title-text"><b>'. $approvedTime. '</b></span><br/>';
	        }*/
            if(!empty($comment)){
				$log = new SavingLog();
				$log->rs = array();
				$log->rs['saving_id'] = $savingId;
				$log->rs['user_id'] = Yii::app()->session['user_id'];
				$log->rs['comment'] = !empty($commentLog)?$commentLog:"";
				$log->rs['created_datetime'] = date('Y-m-d H:i:s');
				$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
				$log->write();
        	}
    	}
		Yii::app()->user->setFlash('success', "Success! Savings status changed successfully.");
		$this->redirect(AppUrl::bicesUrl('savingsUsg/edit/'.$savingId ));
    }

    public function actionSavingValidate(){
		$savingId 	   = $_POST['saving_id'];
		$approverId    = $_POST['validate_approver_id'];
		$status  = $_POST['validate_approver_status'];
		$userId  = Yii::app()->session['user_id'];
		$userName  = Yii::app()->session['full_name'];
		$createdAt = date("Y-m-d H:i:s");
		$comment = addslashes($_POST['approver_comment']);
		$approvedTime  = $_POST['approver_datetime'];
		$userObj = new User();
		if(!empty($approverId)){
			$approverRecord = $userObj->getOne(array('user_id'=>$approverId));
			$approver_name = $approverRecord['full_name'];
		}else{
			$approver_name = '';
		}
		$savingObj = new SavingUsg();
		$savingCheck = $savingObj->checkValidateApprovel($savingId);
		//$statusTitle = $savingObj->getStatus($status);
		if($status == "Approved" || $status == "Rejected"){
	 		$userTypeID = $savingCheck['user_id'];
	 	}else {
	 		$userTypeID = $approverId;
	 	}
		$userRecord = $userObj->getOne(array('user_id'=>$userTypeID));
		$savingObj->rs = array();
		$savingObj->rs['id'] = $savingId;
		$savingObj->rs['validate_approver_id'] = $approverId;
		$savingObj->rs['validate_approver_name'] = $approver_name;
		$savingObj->rs['validate_approver_status'] = $status;
		$savingObj->write();

		$historyObj = new ValidateSavingApprovalHistory();
		$historyObj->rs = array();
		$historyObj->rs['id'] = 0;
		$historyObj->rs['user_id'] = $userId;
		$historyObj->rs['saving_id'] = $savingId;
		$historyObj->rs['user_name'] = $userName;
		$historyObj->rs['approver_id'] = $approverId;
		$historyObj->rs['status'] = $status;
		$historyObj->rs['approver_comment'] = $comment;
		$historyObj->rs['approver_datetime'] = $approvedTime;
		$historyObj->rs['created_at'] = $createdAt;
		$historyObj->write();
		$savingRecord = $savingObj->getOne(array('id'=>$savingId));
		$savingTitle   		= $savingRecord['title'];
		$userEmail  		= $userRecord['email'];
		$userName  			= $userRecord['full_name'];
		$subject			= 'Supplier '.$status;
		$notificationFlag 	= $savingId.'validateSaving'.$status.$userId;
		if($status == "Sent For Approval" || $status == "Approved" || $status == "Rejected"){	
		if($status == "Sent For Approval"){
        	$notificationComments  = "You have a request to approve Savings ".$savingTitle;
        	$subject = 'Savings Approval Request';
        }else if($status == "Approved"){
        	$notificationComments  = "Savings: <b>".$savingTitle."</b> has been approved";
        	$subject = 'Savings Approved';
        }else{
        	$subject = 'Savings Rejected';
        	$notificationComments  = "Savings: <b>".$savingTitle."</b> has been rejected";
        }
		/*$subject = 'Saving Status: '.$status;
		$notificationComments  = "Savings: <b>".$savingTitle." Status ".$status."</b>";*/
		$notification = new Notification();
		$notification->rs = array();
		$notification->rs['id'] = 0;
		$notification->rs['user_type'] = 'Client';
		$notification->rs['notification_flag'] = $notificationFlag;
		$notification->rs['user_id'] = $userTypeID;
		$notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('savingsUsg/edit/' . $savingId) .'">'.$notificationComments.'</a>';
		$notification->rs['notification_date'] = date("Y-m-d H:i");
		$notification->rs['read_flag'] = 0;
		$notification->rs['notification_type'] = 'Savings Status';
		$notification->write();
		$notificationText = '<a href="' . AppUrl::bicesUrl('savingsUsg/edit/' . $savingId) . '" style="font-size:18px;color:#2d9ca2;text-decoration:none">'.$notificationComments.'</a>';
		$url = AppUrl::bicesUrl('savingsUsg/edit/' . $savingId);
		EmailManager::userNotification($userEmail,$userName,$subject,$notificationText,$url);
		}

		$commentLog = '';
		 if(!empty($savingCheck['approver_status'])){
	        if($savingCheck['approver_id'] !=$approverId ){
	          $sql = "SELECT full_name  FROM users WHERE  user_id=".$approverId." order by full_name asc ";
	          $userReader = Yii::app()->db->createCommand($sql)->queryRow();
	          $commentLog .= '<b>User Approver:</b> <span class="title-text"><b>'. $savingCheck['approver_name']. '</b> Changed to <b>'.$userReader['full_name'].'</b></span><br/>';
	        }
	        if($savingCheck['approver_status'] != $status ){
	         //$statusTitleOld = $savingObj->getStatus($savingCheck['status']);
	         $statusTitleOld = $savingCheck['approver_status'];
	          $commentLog .= '<b>Status:</b> <span class="title-text"><b>'. $statusTitleOld. '</b> Changed to <b>'.$statusTitle.'</b></span><br/>';
	        }
	        if(!empty($comment)){
	          $commentLog .= '<b>Comment:</b> <span class="title-text"><b>'. $comment. '</b></span><br/>';
	        }
	        /*if(!empty($approvedTime) ){
	        	if(FunctionManager::dateFormat()=="d/m/Y")
                 $approvedTime = date(FunctionManager::dateFormat(), strtotime(strtr($approvedTime, '/', '-')));
               else{
               $approvedTime = date(FunctionManager::dateFormat(),strtotime($approvedTime)) ;
               }

	          $comment .= '<b>Date:</b> <span class="title-text"><b>'. $approvedTime. '</b></span><br/>';
	        }*/
            if(!empty($comment)){
				$log = new SavingLog();
				$log->rs = array();
				$log->rs['saving_id'] = $savingId;
				$log->rs['user_id'] = Yii::app()->session['user_id'];
				$log->rs['comment'] = !empty($commentLog)?$commentLog:"";
				$log->rs['created_datetime'] = date('Y-m-d H:i:s');
				$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
				$log->write();
        	}
    	}
		Yii::app()->user->setFlash('success', "Success! Savings status changed successfully.");
		$this->redirect(AppUrl::bicesUrl('savingsUsg/edit/'.$savingId ));
    }

	public function actionexportCSV(){
		$saving = new SavingUsg();
        $saving->export();
        
	} 


	public function actionImportUpdate(){
		
		date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);

        $filePath = 'uploads/imports/usg_savings.csv';

        $row = 1;
        $checkRecords = 0;
        $successMsg = '';

        if (($handle = fopen($filePath, "r")) !== FALSE) {
         while (($data = fgetcsv($handle)) !== FALSE) {
            $num = count($data);
            	
            if($row > 1){
                $sayingTitle          = addslashes(trim($data[3]));
                $status               = $data[4];
                $locationId           = addslashes(trim($data[17]));
                $departmentId         = addslashes(trim($data[18]));
                $savingArea           = $data[26];
                $businessUnit         = addslashes(trim($data[27]));
              	$startDate 	  		  = date('Y-m-d', strtotime(strtr($data[37], '/', '-')));
              	$duedate 	  		  = date("Y-m-d", strtotime(strtr($data[38], '/', '-')));
                $contractId           = addslashes(trim($data[39]));

                $sql = " SELECT id,start_date FROM savings  where  lower(title) = '".strtolower($sayingTitle)."' and status= '".$status."' and location_id = '".$locationId."' and department_id = '".$departmentId."' and saving_area = '".$savingArea."' and business_unit ='".$businessUnit."' and contract_id= ".$contractId;
        	    $fieldReader = Yii::app()->db->createCommand($sql)->queryRow();

        	    if(!empty($fieldReader['id'])) {
        	     $sql = " update savings set start_date='".$startDate."' where id=".$fieldReader['id'];
        	     $savinginsUpdate = Yii::app()->db->createCommand($sql)->execute();
        	    }
        	    
        	    if(!empty($fieldReader['id'])){
        	     $sql = " SELECT id,savings_due_date FROM saving_milestone where saving_id = ".$fieldReader['id']; 
        	     $mileReader = Yii::app()->db->createCommand($sql)->queryRow();

        	    $sql = " update saving_milestone set savings_due_date='".$startDate."' where id='".$mileReader['id']."' and saving_id=".$fieldReader['id'];
        	    $savinginsUpdate = Yii::app()->db->createCommand($sql)->execute();
        	    }

			}
        	 $row++;
        	}
        fclose($handle);
        }

         echo $successMsg = 'Savings data imported.';

	}
	public function actionImport(){

		exit;
		date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);

        $filePath = 'uploads/imports/usg_savings.csv';

        $row = 1;
        $checkRecords = 0;
        $successMsg = '';

        if (($handle = fopen($filePath, "r")) !== FALSE) {
         while (($data = fgetcsv($handle)) !== FALSE) {
            $num = count($data);
            
            if($row>1){

            	$userId               = $data[1];
                $userName             = $data[2];
                $sayingTitle          = addslashes(trim($data[3]));
                $status               = addslashes(trim($data[4]));
                $currencyID           = addslashes(trim($data[11]));
                $currencyRate         = addslashes(trim($data[12]));
                $locationId           = addslashes(trim($data[17]));
                $departmentId         = addslashes(trim($data[18]));
                $countryName		  = addslashes(trim($data[20]));
                $savingType           = addslashes(trim($data[21]));
                $oracleSupplierName   = addslashes(trim($data[24]));
                $oracleSupplierNumber = addslashes(trim($data[25]));
                $savingArea           = addslashes(trim($data[26]));
                $businessUnit         = addslashes(trim($data[27]));
                $specialEvent		  = addslashes(trim($data[28]));
                $eventName		  	  = addslashes(trim($data[29]));
                $notes                = addslashes(trim($data[30]));
                $totalProjectSaving   = addslashes(trim($data[31]));
                $realisedSaving   	  = addslashes(trim($data[33]));
                $newIncrementalSavings= addslashes(trim($data[35]));
                $archive              = trim($data[36]);
              	$startDate 	  		  = date('Y-m-d',strtotime($data[37]));
              	$duedate 	  		  = date("Y-m-d", strtotime(strtr($data[38], '/', '-')));
                $contractId           = addslashes(trim($data[39]));

	           $sql = 'INSERT INTO savings (user_id ,user_name,title,status,currency_id,currency_rate,location_id,department_id,country,saving_type,oracle_supplier_name,oracle_supplier_number,saving_area,business_unit,special_event,event_name,notes,total_project_saving,realised_saving,new_or_incremental_savings,new_archived,start_date,due_date,contract_id ,created_at) 
	           VALUES ("'.$userId.'", "'.$userName.'", "'.$sayingTitle.'","'.$status.'", "'.$currencyID.'",  
	           	"'.$currencyRate.'", "'.$locationId.'", "'.$departmentId.'", "'.$countryName.'", "'.$savingType.'", "'.$oracleSupplierName.'", "'.$oracleSupplierNumber.'", "'.$savingArea.'", "'.$businessUnit.'", "'.$specialEvent.'", "'.$eventName.'", "'.$notes.'", "'.$totalProjectSaving.'", "'.$realisedSaving.'", "'.$newIncrementalSavings.'", "'.$archive.'",  "'.$startDate.'", "'.$duedate.'", "'.$contractId.'", "'.date("Y-m-d").'") ';

	            $command= Yii::app()->db->createCommand($sql);
	            $sqlResult = $command->execute();
	            
	            $saving_id= Yii::app()->db->lastInsertID;
	            // Start: Milestone
        		$savingMilestone = new SavingMilestone;
				$savingMilestone->rs = array();
				$savingMilestone->rs['id']  = 0;
				$savingMilestone->rs['saving_id']  			 = $saving_id;	
				$savingMilestone->rs['due_date']   			 = $duedate;
				$savingMilestone->rs['savings_due_date']  	 = $startDate;	
				$savingMilestone->rs['cost_reduction'] 		 = $totalProjectSaving;
				$savingMilestone->rs['total_realised_saving']= $realisedSaving;
				$savingMilestone->rs['created_at'] 			 = date("Y-m-d");
				$savingMilestone->write();
	            //End: Milestone
 
	            if(!empty($sqlResult)){   	
	             // $successMsg = 'Vendors data imported.';
	            }else{
	            	$successMsg = 'Looks like some data not imported';
	            } 
	        
        }
         $row++;
        }
        fclose($handle);
        }

         echo $successMsg = 'Savings data imported.';
	}

	public function actionFieldsimport(){
		exit;
        set_time_limit(0);
        error_reporting(0);

        $filePath = 'uploads/imports/usg_Milestone_Field.csv';

        $row = 1;
        $checkRecords = 0;
        $successMsg   = '';
      
        if (($handle = fopen($filePath, "r")) !== FALSE) {       
         while (($data = fgetcsv($handle)) !== FALSE) {
            $num = count($data);
            if($row>1){
            	$savingId    		  = addslashes(trim($data[1]));
            	$milestone_id 		  = addslashes(trim($data[1]));
            	$fieldDate  		  = date('Y-m-d',strtotime($data[3]));
            	$fieldValue 		  = addslashes(trim($data[4]));
            	$realizedSavings 	  = addslashes(trim($data[5]));
            	$fieldStatus 		  = addslashes(trim($data[6]));
            	
            	$fields = new MilestoneField;
            	$fields->rs = array();
				$fields->rs['id']  = 0;
				$fields->rs['saving_id']  	= $savingId;	
				$fields->rs['milestone_id'] = $milestone_id;
				$fields->rs['field_name'] 	= $fieldDate;
				$fields->rs['field_value']  = $fieldValue;
				$fields->rs['total_realised_savings'] = $realizedSavings;
				$fields->rs['total_realised_savings'] = $realizedSavings;
				$fields->rs['status']   	= $fieldStatus;
				$fields->write();
				
       		}
         $row++;
         }
         fclose($handle);
        }

         echo $successMsg = 'Savings Fields data imported.';
	}

	public function actionUpdateSavingsAndMiletone(){
		set_time_limit(0);
        error_reporting(0);
		/*$sql = " SELECT * FROM milestone_field  order by saving_id asc";
        $fieldReader = Yii::app()->db->createCommand($sql)->query()->readAll();
		foreach($fieldReader as $fieldValue){
				$m = date("m",strtotime($fieldValue['field_name']));
				$d = date("d",strtotime($fieldValue['field_name']));
				$y = date("Y",strtotime($fieldValue['field_name']));
 
			$sql = " update milestone_field set field_name ='".date("Y-d-m",strtotime($fieldValue['field_name']))."' where id=".$fieldValue['id'];
        	 
        $savinginsUpdate = Yii::app()->db->createCommand($sql)->execute();*/

        $sql = " SELECT * FROM saving_milestone  order by saving_id asc";
        $fieldReader = Yii::app()->db->createCommand($sql)->query()->readAll();
		foreach($fieldReader as $fieldValue){
				 
 
			$sql = " update saving_milestone set due_date ='".date("Y-d-m",strtotime($fieldValue['due_date']))."',savings_due_date ='".date("Y-d-m",strtotime($fieldValue['savings_due_date']))."' where id=".$fieldValue['id'];
        	 
        $savinginsUpdate = Yii::app()->db->createCommand($sql)->execute();

		}

		
		set_time_limit(0);
        error_reporting(0);
		$milestoneField = new MilestoneField;

		$sql = " SELECT * FROM milestone_field group by saving_id order by saving_id";
        	 
        $fieldReader = Yii::app()->db->createCommand($sql)->query()->readAll();
		foreach($fieldReader as $fieldValue){
		$milestoneID=$fieldValue['milestone_id'];
		$savingID=$fieldValue['saving_id'];
		$savingMilestone = new SavingMilestone;
		$savingsUpdate = new SavingUsg();
		/*$savingMilestone = new SavingMilestone;
		$savingMilestone->rs = array();
		$savingMilestone->rs['id'] = $milestoneID;
		$savingMilestone->rs['saving_id'] = $savingID;
		$savingMilestone->rs['realised_cost_reduction'] = $fieldValue['total_realised_savings'];
		$savingMilestone->rs['realised_cost_avoidance'] = 0;
		$savingMilestone->rs['total_realised_saving']   = $fieldValue['total_realised_savings'];
		$savingMilestone->write();
		if($savingMilestone->rs['id']>0){
			//FunctionManager::savingCalculationSaveUSG($savingID);
		}	
*/
		$sql = " update savings set total_project_saving='".$savingsUpdate->projectSavings($savingID)."',realised_saving='".$savingsUpdate->realizedSavings($savingID)."' where id=".$savingID;
        	 
        $savinginsUpdate = Yii::app()->db->createCommand($sql)->execute();


		/*$savingsUpdate = new SavingUsg();
		$savingObj = new SavingUsg();
		$savingObj->rs = array();
		$savingObj->rs['id'] = $savingID;
		$savingObj->rs['total_project_saving'] = $savingsUpdate->projectSavings($savingID);
		$savingObj->rs['realised_saving'] = $savingsUpdate->realizedSavings($savingID);
		$savingObj->write();*/
		}
	}


	
	// Get All Categories Loaded
	public function actionGetCategories() {
        $material_type_id = isset($_REQUEST['material_type_id']) ? $_REQUEST['material_type_id'] : "";
        $category_id = isset($_REQUEST['category_id']) ? $_REQUEST['category_id'] : "";
    	$categories = array();

        if(!empty($material_type_id)){
        $category = new Category();
        $search_results = $category->getAll(array('material_type_id'=> $material_type_id));
        foreach ($search_results as $category_data)
                $categories[] = array(
                    "value" => $category_data['value'],
                    "data" => $category_data['id']
                );
        }

        echo json_encode(array(
            'suggestions' => $categories
        ));
	}

	// Import Matrial Type and categories
	public function actionMaterialImport(){
		exit;
        set_time_limit(0);
        error_reporting(0);

        $filePath = 'uploads/imports/usg-material-type.csv';
        $row = 1;
        $checkRecords = 0;
        $successMsg   = '';
      
        if (($handle = fopen($filePath, "r")) !== FALSE) {       
         while (($data = fgetcsv($handle)) !== FALSE) {
            $num = count($data);

            if($row > 1){
              $materialValue = trim($data[0]);
              $categoriesValue = trim($data[1]);
              $subcategoriesValue = trim($data[2]);

              $materialID = 0;
              if(!empty($materialValue)) {

              	$sql = " SELECT * FROM material_types WHERE lower(value) ='".strtolower($materialValue)."'";
              	$check_material = Yii::app()->db->createCommand($sql)->queryRow();
       			
           		if(!empty($check_material['id'])){
           		  $materialID = $check_material['id'];
           		}else{	
                 $material = new MaterialType();
                 $material->rs = array();
			     $material->rs['id']  = 0;
			     $material->rs['value']  = $materialValue;
			     $material->rs['soft_deleted'] = 0;
			     $material->write();
			 	 $materialID = Yii::app()->db->lastInsertID;
			 	 
       		  }

       		$sql = " SELECT * FROM categories where lower(value) ='". strtolower(trim($categoriesValue)) ."' and material_type_id=".$materialID;
            $catReader = Yii::app()->db->createCommand($sql)->queryRow();

            $categoryID = $catReader['id'];
       
        		if($subcategoriesValue) {
       			 $subcategory = new Subcategory();
       			 $subcategory->rs = array();
			     $subcategory->rs['id']    = $subcatReader['id'] ? $subcatReader['id']: 0 ;
			   	 $subcategory->rs['code']  = $categoryID;
			   	 $subcategory->rs['value'] = trim($subcategoriesValue);
			   	 $subcategory->rs['soft_deleted'] = 0;
			   	 $subcategory->write();   
			   }	
       		
       	   }
         }
         $row++;
        }
         fclose($handle);
        }
	 
      }


	public function actionGetUsgDepartments()
	{
		$query = "SELECT * FROM departments  ORDER BY department_name";
		$res   = Yii::app()->db->createCommand($query)->query()->readAll();
		echo json_encode($res); exit;
	}


    private function saveOrUpdateMileStone($savingId){
	    

		$sql = "select id from saving_milestone where saving_id=".$savingId;
		$milestoneID = Yii::app()->db->createCommand($sql)->queryRow();
		
		if(!empty($milestoneID['id'])){
		 $milestoneID = $milestoneID['id'];
		}else{
		 $milestoneID = 0;	
		}

	    $milestoneDates = $this->milestoneFieldsGetOneRecordAscOrDesc($savingId);
        $savingMilestone = new SavingMilestone;
		$savingMilestone->rs = array();
		$savingMilestone->rs['id']  = $milestoneID;
		$savingMilestone->rs['saving_id'] = $savingId;	
		$savingMilestone->rs['due_date']  = $milestoneDates['milestoneDueDate'];
		$savingMilestone->rs['savings_due_date']= $milestoneDates['milestoneStartDate'];	
		$savingMilestone->write();
     
	}	
         

	private function milestoneFieldsGetOneRecordAscOrDesc($savingId){
		$dates = [];
		
		$sql = "SELECT field_name FROM `milestone_field` where saving_id='".$savingId."' ORDER BY `milestone_field`.`field_name` ASC LIMIT 1";
		$milestoneStartDate = Yii::app()->db->createCommand($sql)->queryRow();

		$sql = "SELECT field_name FROM `milestone_field` where saving_id='".$savingId."' ORDER BY `milestone_field`.`field_name` DESC LIMIT 1";
		$milestoneDueDate = Yii::app()->db->createCommand($sql)->queryRow();

		$milestoneStartDate = date('Y-m-d', strtotime($milestoneStartDate['field_name'])); 
		$milestoneDueDate  = date('Y-m-d', strtotime($milestoneDueDate['field_name']. " +1 month")); 
		return $dates = ['milestoneStartDate' => $milestoneStartDate, 'milestoneDueDate' => $milestoneDueDate];
	}

 
	public function actionGetSavingDocumentList(){
		
		$sql = "select * from saving_document where saving_id=".$_POST['saving_id']." ORDER BY title ASC ";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		$data = '';
		foreach($rows as $value){
			$data .= '<tr class="tr_'.$value['id'].'">';
        	$data .= "<td>". $value['title']."</td>";
        	$data .= "<td>". $value['file']."</td>";
        	$data .= '<td class="text-center">
		        		<a href="'. $this->createUrl('savingsUsg/savingDownloadDocument',array('saving_doc_id'=>$value['id'])) .'" 
		        		class="btn btn-primary download" target="_blank"><span class="fa fa-download"></span></a>
						<button class="btn btn-danger" onclick="deleteSavingDocument('.$value['id'].')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
        	$data .= "</tr>";
    	}
        echo json_encode($data);exit;
	}

	public function actionSaveDocument(){
		
		$savingId= $_POST['saving_id'];
		$doc_title= $_POST['doc_title'];
	
		date_default_timezone_set(Yii::app()->params['timeZone']);
       $uploaded = 0;
        if (!empty($_FILES)){
            
            if (isset($_FILES['doc_file']) && is_array($_FILES['doc_file'])){
                $path = Yii::getPathOfAlias('webroot').'/uploads/saving_doc';
               
                if (!is_dir($path)) mkdir($path);
                if (!is_dir($path.'/temp'))
                    mkdir($path.'/temp');
                    $path .= '/temp';
                if (!is_dir($path.'/'))
                    mkdir($path.'/');
                if (!empty($_FILES['doc_file']['name']))
                {
                    $file_name = time().'_'.$_FILES['doc_file']['name'];
                    $file_name = str_replace("'","",$file_name);
                    $file_name = str_replace('"','',$file_name);
                    $file_name = str_replace(' ','_',$file_name);
                   
                    if(move_uploaded_file($_FILES['doc_file']['tmp_name'], $path.'/'. $file_name)){
		       		 $fileSource = Yii::getPathOfAlias('webroot')."/uploads/saving_doc/temp/".$file_name;
		       		 if (!is_dir('uploads/saving_doc')) mkdir('uploads/saving_doc');
					 if (!is_dir('uploads/saving_doc/' . $savingId)) 
					 	mkdir('uploads/saving_doc/' . $savingId);
					 	$fileTarget = Yii::getPathOfAlias('webroot')."/uploads/saving_doc/$savingId/".$file_name;
		       		 	copy($fileSource,$fileTarget);
		       		 	
					 	$savingDoc = new SavingUsgDocument();
					 	$savingDoc->rs = array();
						$savingDoc->rs['id'] = 0;
						$savingDoc->rs['saving_id'] = $savingId;	
						$savingDoc->rs['title'] = $doc_title;
						$savingDoc->rs['file']  = $file_name;	
						$savingDoc->rs['status']= 'Active';	
						$savingDoc->write();

					 unlink($fileSource);
                    }  
                }
            }
         }
        
		echo json_encode(array('uploaded' => 1));
	 }

	public function actionSavingDocumentsDelete(){
	 	$savingDocID = $_POST['savingDocID'];
	 	$msg = 0;

	 	$savingDoc = new SavingUsgDocument();
	 	$savingDocFile = $savingDoc->getOne(array('id'=>$savingDocID));
		$file = Yii::app()->basePath."/../uploads/saving_doc/".$savingDocFile['saving_id']."/".$savingDocFile['file'];
	 	unlink($file);
	 	
	 	if(!empty($savingDocID)){
	 	 $sql = "delete from saving_document where id =".$savingDocID;
	 	 Yii::app()->db->createCommand($sql)->execute();
       	 $msg = 1;
	 	}
	 	echo json_encode(['msg' => $msg]); exit;
	}

	public function actionSavingDownloadDocument()
	{  
		$savingDocID = $_GET['saving_doc_id'];
	 	$savingDoc = new SavingUsgDocument();
	 	$savingDocFile = $savingDoc->getOne(array('id'=>$savingDocID));
		$file = Yii::app()->basePath."/../uploads/saving_doc/".$savingDocFile['saving_id']."/".$savingDocFile['file'];
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        //header("Content-Type: application/pdf");
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
	}
 
	private function getSentForApprovalEmailNotification($savingId, $approverId, $approverStatus){
		
		$savingId  = $savingId;
		$approverId= $approverId;
		$status    = $approverStatus;
		$userId    = Yii::app()->session['user_id'];
		$userName  = Yii::app()->session['full_name'];
	
		$userObj = new User();
		$savingObj = new SavingUsg();
		$savingCheck = $savingObj->checkApprovel($savingId);
		
	 	$userTypeID = $approverId;
		$userRecord = $userObj->getOne(array('user_id'=>$userTypeID));
		
		$savingRecord = $savingObj->getOne(array('id'=>$savingId));
		$savingTitle   		= $savingRecord['title'];
		$userEmail  		= $userRecord['email'];
		$userName  			= $userRecord['full_name'];
		$subject			= 'Supplier '.$status;

		$notificationFlag 	= $savingId.'clientSaving'.$status.$userId;

		if($status == "Sent for approval"){	
         $notificationComments  = "You have a request to approve Savings ".$savingTitle;
         $subject = 'Savings Approval Request';
		 $notification = new Notification();
		 $notification->rs = array();
		 $notification->rs['id'] = 0;
		 $notification->rs['user_type'] = 'Client';
		 $notification->rs['notification_flag'] = $notificationFlag;
		 $notification->rs['user_id'] = $userTypeID;
		 $notification->rs['notification_text'] = '<a href="' . AppUrl::bicesUrl('savingsUsg/edit/' . $savingId) .'">'.$notificationComments.'</a>';
		 $notification->rs['notification_date'] = date("Y-m-d H:i");
		 $notification->rs['read_flag'] = 0;
		 $notification->rs['notification_type'] = 'Savings Status';
		 $notification->write();
		 $notificationText = '<a href="' . AppUrl::bicesUrl('savingsUsg/edit/' . $savingId) . '" style="font-size:18px;color:#2d9ca2;text-decoration:none">'.$notificationComments.'</a>';
		 $url = AppUrl::bicesUrl('savingsUsg/edit/' . $savingId);
		 EmailManager::userNotification($userEmail,$userName,$subject,$notificationText,$url);
		}
	}
}