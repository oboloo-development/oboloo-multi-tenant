<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');
/**
 * This class defines vatious user action methods such as new user registration,
 * login, logout, reset password and time out upon certain period of inactivity
 */
class SetupController extends CommonController {

    public $layout = 'main';

    /**
     * This method is used to display the login form to the user. Also when the user
     * enters username and password on the form, the same method checks if such an user
     * with appropriate password (stored in encrypted format in the database) exists.
     * Next it performs various checks such as this is not an inactive account and also
     * that the user is not logging after a long time etc. If all these checks passed,
     * then the method sets up certain session variables and redirects the user to the
     * dashboard page. If any of the checks fail, then the user is redirected back to
     * the login page and appropriate error message is also then displayed to the user
     */
public function actionError()
{

}

public function actionProfile() {
    // is someone already logged in?
    $this->checklogin();
    $this->layout = 'main';
    $this->current_option = 'profile';
    $user_id = Yii::app()->session['user_id'];

    if(isset($_POST)){
        
        if (!empty($_FILES['profile_img']['name'])){
            $img  = $_FILES['profile_img']['name'];
            $imgArr = explode(".", $img);
            $imgName = time().'.'.end($imgArr);
            $path = Yii::getPathOfAlias('webroot').'/images/users';
            if (!is_dir($path)) mkdir($path);
            if (!is_dir($path.'/' . $user_id))
                mkdir($path.'/' . $user_id);
            if (!is_dir($path.'/'))
                mkdir($path.'/');

            if(move_uploaded_file($_FILES['profile_img']['tmp_name'], $path.'/' .$user_id. '/'. $imgName)){

             }

        }else { $imgName=''; }

        $user = new User();
        $user->rs = array();
        $user->rs['user_id'] = $user_id;
        $user->rs['language_code'] = Yii::app()->input->post('language_code');
        $user->rs['position'] = Yii::app()->input->post('position');
        $user->rs['contact_number'] = Yii::app()->input->post('contact_number');
        if(!empty($imgName)){
            $user->rs['profile_img'] = $imgName;
        }
        // echo "<pre>"; print_r($user); exit;
        $user->write();
        Yii::app()->session['default_language_code'] = Yii::app()->input->post('language_code');

        $data=array();
        $sql = "update user_setup set profile=1,profile_date='".date("Y-m-d H:i:s")."'  where user_id=".Yii::app()->session['user_id'];
        $setup = Yii::app()->db->createCommand($sql)->execute();
        if(!empty($setup)){
            $profileSetup = 1;
        }else{
            $profileSetup = 0;
        }
        $data['profile_saved'] = $profileSetup;
        echo json_encode($data);
    }
      
    }

    
    public function actionCheckupSetup() {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main_setup';
        $this->current_option = 'profile';
        $user_id = Yii::app()->session['user_id'];
        $sql = "select * from  user_setup where user_id=".$user_id;
        $setup = Yii::app()->db->createCommand($sql)->queryRow();
        
        $data['profile'] = $setup['profile'];
        $data['company'] = $setup['company'];
        $data['location'] = $setup['location'];
        $data['department'] = $setup['department'];
        $data['currency'] = $setup['currency'];
        $data['vendor'] = $setup['vendor'];
        echo json_encode($data);
    }
    
    public function actionCompany(){

            if(isset($_POST)){
            $company_details = new Company();
            $company         = $company_details->getOne();

            $company_details->rs = array();
            $company_details->rs['id'] = $company['id'];
            $company_details->rs['company_name'] = Yii::app()->input->post('company_name');
            $company_details->rs['company_number'] = Yii::app()->input->post('company_number');
            $company_details->rs['address_1'] = Yii::app()->input->post('address_1');
            $company_details->rs['address_2'] = Yii::app()->input->post('address_2');

            //$company_details->rs['tax_number'] = Yii::app()->input->post('tax_number');
            $company_details->rs['city'] = Yii::app()->input->post('city');
            $company_details->rs['state'] = Yii::app()->input->post('state');
            $company_details->rs['zipcode'] = Yii::app()->input->post('zipcode');
            $company_details->rs['country'] = Yii::app()->input->post('country');

            //$company_details->rs['default_language_code'] = Yii::app()->input->post('default_language_code');
            $company_details->rs['currency_id'] = Yii::app()->input->post('currency_id');
            $company_details->rs['default_date_format'] = Yii::app()->input->post('default_date_format');
            $company_details->rs['default_time_format'] = Yii::app()->input->post('default_time_format');
            
            if (!empty($_FILES['file']['name']) && is_array($_FILES['file']) && count($_FILES['file']))
            {
                $company_details->rs['logo'] = $_FILES['file']['name'];
            }
            $company_details->write();

            // Start: session
            Yii::app()->session['company_name'] = isset($company_details->rs['company_name'])?$company_details->rs['company_name']:'';
            Yii::app()->session['company_number'] = isset($company_details->rs['company_number'])?$company_details->rs['company_number']:'';
            Yii::app()->session['phone_number'] = isset($company_details->rs['phone_number'])?$company_details->rs['phone_number']:'';
            Yii::app()->session['company_website'] = isset($company_details->rs['company_website'])?$company_details->rs['company_website']:'';
            Yii::app()->session['default_language_code'] = isset($company_details->rs['default_language_code'])?$company_details->rs['default_language_code']:'';
            //Yii::app()->session['user_currency'] = $view_data['company_details']['currency_id'];
            Yii::app()->session['user_currency_dropdown'] = isset($company_details->rs['currency_id'])?$company_details->rs['currency_id']:'';
            Yii::app()->session['default_date_format'] = isset($company_details->rs['default_date_format'])?$company_details->rs['default_date_format']:'';
            Yii::app()->session['default_time_format'] = isset($company_details->rs['default_time_format'])?$company_details->rs['default_time_format']:'';
            // End: session
            if(!empty($company_details->rs['currency_id'])){
                $userID   = Yii::app()->session['user_id'];
                $userName = Yii::app()->session['full_name'];
                $sql = 'UPDATE currency_rates SET rate = 1, status=0, updated_at="'.date("Y-m-d H:i:s").'",user_id="'.$userID.'",user_name="'.$userName.'" WHERE currency = "'.$company_details->rs['currency_id'].'"';
                Yii::app()->db->createCommand($sql)->execute();

            }
            $sql = "update user_setup set company=1,company_date='".date("Y-m-d H:i:s")."'  where user_id=".Yii::app()->session['user_id'];
            $setup = Yii::app()->db->createCommand($sql)->execute();
            if(!empty($setup)){
                $companySetup = 1;
            }else{
                $companySetup = 0;
            }
            $data['company_saved'] = $companySetup;
            echo json_encode($data);
            }
      
    } 

    public function actionLocation(){
            if(isset($_POST)){
                $location = new Location();
                $location->saveData();
                $sql = "update user_setup set location=1,location_date='".date("Y-m-d H:i:s")."'  where user_id=".Yii::app()->session['user_id'];
                $setup = Yii::app()->db->createCommand($sql)->execute();
            if(!empty($setup)){
                $locationSetup = 1;
            }else{
                $locationSetup = 0;
            }
            $data['location_saved'] = $locationSetup;
            echo json_encode($data);
        }
    }

     public function actionDeptartment(){

        if(!empty($_POST)){
       
        $department = new Department();
        $department->saveData();
        
        $locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";
        $department->saveLocationsSetup($department->rs['department_id'], $locations);

        $location = new Location();
        $location_names = array();
        foreach ($location->getData() as $location_data)
            $location_names[$location_data['location_id']] = $location_data['location_name'];
        
        $new_locations_text = "";
        if (!empty($locations))
        {
            foreach ($locations as $location_id)
            {
                if (isset($location_names[$location_id]))
                {
                    if ($new_locations_text == "") $new_locations_text = $location_names[$location_id];
                    else $new_locations_text = $new_locations_text . ", " . $location_names[$location_id];
                }
            }
        }
        $sql = "update user_setup set department=1,department_date='".date("Y-m-d H:i:s")."'  where user_id=".Yii::app()->session['user_id'];
        $setup = Yii::app()->db->createCommand($sql)->execute();
        if(!empty($setup)){
            $departmentSetup = 1;
        }else{
            $departmentSetup = 0;
        }
        $data['deparment_saved'] = $departmentSetup;
        echo json_encode($data);
        }
    }

    public function actionGetLocations(){
        $location = new Location();
        $data = array();

        $dropdown = "<option value=''>Select Location</option>";
        $locationReader = $location->getData(array('order' => 'location_name'));
        foreach($locationReader as $value){
             $dropdown .= "<option value='".$value['location_id']."'>".$value['location_name']."</option>";
        }
        $data['location'] = $dropdown;
        echo json_encode($data);
    }

    public function actionCurrency(){
        if(isset($_POST['country'])){
        $currency = $_POST['currency'];
        $currencySymbol = $_POST['currency_symbol'];
        $country = $_POST['country'];

        $sql = " select * from  currency_rates WHERE country ='".$country."' and currency_symbol='".$currencySymbol."'";
        $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
        if(!empty($currencyReader['id'])){
            $currencyID = $currencyReader['id'];
        }else{
            $currencyID = 0;
        }
        $userID   = Yii::app()->session['user_id'];
        $userName = Yii::app()->session['full_name'];
 

       // $rate = 1;//$_POST['rate'];
        $rate = $_POST['rate'];
        $rates = new CurrencyRate();
        $rates->rs = array();
        $rates->rs['id'] = $currencyID;
        $rates->rs['currency'] = $currency;
        $rates->rs['currency_symbol'] = $currencySymbol;
        $rates->rs['rate'] = $rate;
        $rates->rs['status'] = 0;
        $rates->rs['user_id'] = $userID;
        $rates->rs['user_name'] = $userName;
        $rates->rs['created_at'] = date("Y-m-d H:i:s");
        $rates->rs['updated_at'] = date("Y-m-d H:i:s");
        $rates->write();
        $sql = "update user_setup set currency=1,currency_date='".date("Y-m-d H:i:s")."'  where user_id=".Yii::app()->session['user_id'];
        $setup = Yii::app()->db->createCommand($sql)->execute();

        if(!empty($setup)){
        $currencySetup = 1;
        }else{
        $currencySetup = 0;
        }
        $data['currency_saved'] = $currencySetup;
        echo json_encode($data);
        }
    }


    public function actionIndexSetup(){
       
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main_setup';
        $this->current_option = 'profile';
        $user_id = Yii::app()->session['user_id'];
        $form_submitted = Yii::app()->input->post('form_submitted');
        //echo "<pre>";print_r($_POST);exit;
        if($form_submitted){
            
            if (!empty($_FILES['profile_img']['name'])){
                $img  = $_FILES['profile_img']['name'];
                $imgArr = explode(".", $img);
                $imgName = time().'.'.end($imgArr);
                $path = Yii::getPathOfAlias('webroot').'/images/users';
                if (!is_dir($path)) mkdir($path);
                if (!is_dir($path.'/' . $user_id))
                    mkdir($path.'/' . $user_id);
                if (!is_dir($path.'/'))
                    mkdir($path.'/');

                if(move_uploaded_file($_FILES['profile_img']['tmp_name'], $path.'/' .$user_id. '/'. $imgName)){
   
                 }

            }else { $imgName=''; }

            $user = new User();
            $user->rs = array();
            $user->rs['user_id'] = $user_id;
            $user->rs['language_code'] = Yii::app()->input->post('language_code');
            $user->rs['position'] = Yii::app()->input->post('position');
            $user->rs['contact_number'] = Yii::app()->input->post('contact_number');
            if(!empty($imgName)){
                $user->rs['profile_img'] = $imgName;
            }
            $user->write();
            Yii::app()->session['default_language_code'] = $user->rs['language_code'];
        }
        $user = new User();
        $view_data['user'] = $user->getUsers($user_id,$softDeleted=2);
        $location = new Location();
        $view_data['locations'] = $location->getData(array('order' => 'location_name'));
        $view_data['locations_departments'] = $location->getLocationAndDepartment(array('order' => 'location_id'));
        $this->render('system_setup', $view_data);

        
    }

   public function actionVendor(){
     $view_data = array();
        $data = array();
        if(!empty($_POST['emails'])){
            $vendor = new Vendor();
            
            $vendorDuplicate = $vendor->getOne(array('emails' => $_POST['emails']));
            if(!empty($vendorDuplicate['emails'])){
                $alert = "Supplier with this email already taken. Try another";
                $redID = $vendorDuplicate['vendor_id'];
                $dupplicate =1;
                // $this->redirect(AppUrl::bicesUrl('vendors/edit/'.$redID ));
            }else{
                $override_data = array();
                $override_data['quick_created'] = 'No';
                //$override_data['active'] = !empty($_POST['active'])?$_POST['active']:0;
                $override_data['active'] = 1;
                $override_data['profile_status'] = 'Active';
                
                $vendor->saveData($override_data);

                if(!empty($vendor->rs['vendor_id'])){
                    $alert = "<strong> Success! </strong>Supplier submitted successfully. ";
                    //$vendor->welcomeEmail($vendor->rs['vendor_id']);
                    if($vendor->rs['active']==1){
                        //EmailManager::vendorAcivation($vendor->rs['vendor_id'],1,Yii::app()->session['full_name']);
                    }
                    $data['vendor_id'] = $vendor->rs['vendor_id'];
                    $dupplicate =0;
                    
                } }
                       
            $data['alert'] = $alert;
            $data['dupplicate'] = $dupplicate;
            $sql = "update user_setup set vendor=1 where user_id=".Yii::app()->session['user_id'];
            $setup = Yii::app()->db->createCommand($sql)->execute();
            if(!empty($setup)){
                $companySetup = 1;
            }else{
                $companySetup = 0;
            }
            $data['company_saved'] = $companySetup;
            echo json_encode($data);exit;

            
        }
        $industry = new Industry();
        $view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));

        $this->renderPartial('create_vendor_modal', $view_data);
   }
    
   public function actionGetSubIndustries()
    {
        $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
        $industry_id = isset($_REQUEST['industry_id']) ? $_REQUEST['industry_id'] : "";

        $industries = array();

        $industry = new Subindustry();
        $search_results = $industry->getAll(array('value LIKE' => '%' . $search_string . '%', 'ORDER' => 'value', 'code' => $industry_id));

        foreach ($search_results as $industry_data)
            $industries[] = array("value" => $industry_data['value'], "data" => $industry_data['id']);

        echo json_encode(array('suggestions' => $industries));
    }
  

}
