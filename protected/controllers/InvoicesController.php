<?php

class InvoicesController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('invoices/list'));
	}

	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'invoices_list';

		// get list of all invoices (with pagination of course)
		$order = new Order();

		// get invoices as per filters applied
		$view_data = array();
		$location_id = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$order_status = Yii::app()->input->post('order_status');
		
		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');

		$db_from_date = "";
		if (!empty($from_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $from_date);
	       	$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['from_date'] = $db_from_date;
		}

		$db_to_date = "";
		if (!empty($to_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $to_date);
	       	$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['to_date'] = $db_to_date;
		}
		if(!empty($department_id)){
			$location = new Location();
			$department_info = $location->getDepartments($location_id);
		}else
		$department_info='';

		
		// need filters too
		$location = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;
		
		// and display
		$view_data['department_info'] = $department_info;
		$view_data['department_id'] = $department_id;
		$view_data['order_status'] = $order_status;
		$view_data['invoices'] = $order->getInvoices($location_id, $department_id, $order_status, $db_from_date, $db_to_date);
		$this->render('list', $view_data);
	}

}
