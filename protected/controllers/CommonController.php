<?php

/**
 * This is an abstract class (meaning it cannot be instanciated into an object)
 * and hence various controller classes are derived from this one. Because of this
 * reason, some commonly required methods such as checking for system being offline
 * and whether an user has logged in or not are abstracted in here. Hence any
 * of the derived classes then do not need to write these methods repeatedly.
 */

Yii::import('application.vendors.*');

class CommonController extends CController
{
	public $layout = 'main';
	public $current_option = '';

	public function init(){


		$allowAction = 'login';
		$allowAction2 = 'logout';
		$allowAction3 = 'changePassword';
		$allowAction4 = 'forgot';
		$allowAction5 = 'indexSetup';
		$allowAction6 = 'fillCurrencyField';
		$allowAction7 = 'verifyLogin';
		$allowAction8 = 'getDepartments';
		$allowAction9 = 'microsoftLogin';
		$allowAction10 = 'microsoftAutoLogin';
		
		
		
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		// Start: if user done setup
		if(!empty(Yii::app()->session['user_id'])){
		$sql = "select * from  user_setup where user_id=".Yii::app()->session['user_id'];
		$setup = Yii::app()->db->createCommand($sql)->queryRow();
        
		if(
		   strtolower($this->id) != 'setup' &&
		   strpos($url,$allowAction) == false &&  
		   strpos($url,$allowAction2) == false &&  
		   strpos($url,$allowAction5) == false &&
		   strpos($url,$allowAction6) == false &&
		   strpos($url,$allowAction7) == false &&
		   strpos($url,$allowAction8) == false &&
		   strpos($url,$allowAction9) == false &&
		   strpos($url,$allowAction10) == false &&
		   !empty($setup) && 
		   ($setup['profile'] !=1 || $setup['company'] !=1  || $setup['location'] !=1  || $setup['department'] !=1  || $setup['currency'] !=1 || $setup['vendor'] !=1)
		  ){
			$this->redirect(AppUrl::bicesUrl('app/indexSetup'));
		}}
		// End: if user done setup
		if(strtolower($this->id) != 'emailcronejob' && strpos($url,$allowAction) == false &&  strpos($url,$allowAction2) == false 
		 && strpos($url,$allowAction3) == false &&  strpos($url,$allowAction4) == false
		 &&  strpos($url,$allowAction5) == false && strpos($url,$allowAction9) == false 
		 && strpos($url,$allowAction10) == false 
		 && Yii::app()->session['last_activity'] < time()-Yii::app()->session['expire_time'] ) {
		    $this->redirect(AppUrl::bicesUrl('logout'));
		}else{ //if we haven't expired:
		 
    		Yii::app()->session['last_activity'] = time(); //this was the moment of last activity.
		}
		parent::init();
	}

 


	/**
	 * This method clears variable session variable values such as logged in user id,
	 * full name of the user logged in and so on. Usually this is called for log out.
	 */
	public function clearSession()
	{
		$session_variables = array('user_id', 'user_type', 'admin_flag', 'full_name', 'exclude_tax_from_dashboard', 'user_currency', 'company_name', 'company_number','default_language_code','user_currency','default_date_format','default_time_format','2fa','expire_time','last_activity');
		foreach ($session_variables as $session_variable)
		{
			Yii::app()->session[$session_variable] = 0;
			unset(Yii::app()->session[$session_variable]);
		}

    	unset($_COOKIE['googtrans']); 
    	setcookie('googtrans', null, -1, '/');
   
	}

	/**
 	 * \param $type - whether to check for user logged in OR then user not logged in - optional (default: true - check if the user has logged in)
 	 * \param $offline_check - whether to check if the system is in offline mode or not - optional (default: true - check if the system is offline)
	 *
	 * This method checks if someone has logged in the system or not. Depending
	 * on the input variable $type, the check can be performed in a "negative"
	 * manner i.e. the user will be redirect do different page depending upon
	 * the login status. Also, in some cases, even if the system is offline, the
	 * offline check then can be "overridden", e.g. for system administrators.
	 */
	public function checklogin($type = 1, $offline_check = true)
	{
		$user = new User();
		if ($type == 1)
		{
			$requested_url = str_replace(Yii::app()->request->baseUrl . '/', '', Yii::app()->request->requestUri);
            if ($requested_url == 'login') $requested_url = '';

			if (!$user->isLoggedIn())
            {
                if (empty($requested_url)) $this->redirect(AppUrl::bicesUrl('login'));
                else $this->redirect(AppUrl::bicesUrl('login/?requested_url=' . $requested_url));
            }
		}
		else if ($type == 0)
		{
			if ($user->isLoggedIn()) $this->redirect(AppUrl::bicesUrl('dashboard'));
		}
	}

	// return currency value agasit posted value
	public function getCurrency($user_currency){

		if (empty($user_currency)) {
			if (isset(Yii::app()->session['user_currency'])){
				$user_currency = Yii::app()->session['user_currency'];
			}
			else{
				$user_currency = Yii::app()->session['user_currency_dropdown'];
			}
		}
		Yii::app()->session['user_currency'] = $user_currency;
		$sql = "select * from currency_rates where LOWER(currency)='".$user_currency."'";
        $detail_order = Yii::app()->db->createCommand($sql)->queryRow();
        if(!empty($detail_order)){
        	Yii::app()->session['user_currency_symbol'] = $detail_order['currency_symbol'];
        	Yii::app()->session['user_currency'] = $user_currency;
        }
        Yii::app()->session['user_currency'];
		/*switch ($user_currency) {
			case 'GBP' : Yii::app()->session['user_currency_symbol'] = '&#163;';
				return   Yii::app()->session['user_currency_symbol_unicode'] = '\u00A3';
				break;
			case 'USD' : Yii::app()->session['user_currency_symbol'] = '$';
				return   Yii::app()->session['user_currency_symbol_unicode'] = '$';
				break;
			case 'CAD' : Yii::app()->session['user_currency_symbol'] = '$';
				return   Yii::app()->session['user_currency_symbol_unicode'] = '$';
				break;
			case 'CHF' : Yii::app()->session['user_currency_symbol'] = '&#8355;';
				return   Yii::app()->session['user_currency_symbol_unicode'] = '\u20A3';
				break;
			case 'EUR' : Yii::app()->session['user_currency_symbol'] = '&#8364;';
				return   Yii::app()->session['user_currency_symbol_unicode'] = '\u20AC';
				break;
			case 'JPY' : Yii::app()->session['user_currency_symbol'] = '&#165;';
				return   Yii::app()->session['user_currency_symbol_unicode'] = '\u00A5';
				break;
			case 'INR' : Yii::app()->session['user_currency_symbol'] = '&#x20B9;';
				return   Yii::app()->session['user_currency_symbol_unicode'] = '\u20B9';
				break;
			default :  Yii::app()->session['user_currency_symbol'] = '&#163;';
				return Yii::app()->session['user_currency_symbol_unicode'] = '\u00A3';
				break;
		}*/
	}


}
