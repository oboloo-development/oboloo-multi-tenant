<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');

class OrdersController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('orders/list'));
	}
	public function actionList()
	{
		error_reporting(0);
		//CVarDumper::dump($_POST,10,1);die;
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'orders_list';

		// get list of all orders (with pagination of course)
		$order = new Order();

		// get orders as per filters applied
		$view_data = array();
		$location_id = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$order_status = Yii::app()->input->post('order_status');
		$spend_type = Yii::app()->input->post('spend_type');
		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		//unset(Yii::app()->session['user_currency']);
		// get currency on the base of posted value
		$this->getCurrency($user_currency);
		
		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');

		$db_from_date = "";
		if (!empty($from_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $from_date);
	       	$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['from_date'] = $db_from_date;
		}

		$db_to_date = "";
		if (!empty($to_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $to_date);
	       	$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['to_date'] = $db_to_date;
		}

		if(!empty($department_id)){
			$location = new Location();
			//$department_selected = implode(',',$department_id);

			//$sqlCond = " department_id IN ($department_selected)";
			//$sql = "SELECT * FROM departments WHERE ".$sqlCond." ORDER BY department_name";
			//$department_selected = Yii::app()->db->createCommand($sql)->query()->readAll();
			$department_info = $location->getDepartments($location_id);
		}else
		$department_info='';

		// need filters too
		$location = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;
		$view_data['department_info'] = $department_info;
		// and display
		$view_data['department_id'] = $department_id;
		$view_data['order_status'] = $order_status;
		$view_data['spend_type'] = $spend_type;
		$view_data['orders'] = $order->getOrders(0,$spend_type,$location_id, $department_id, $order_status, $db_from_date, $db_to_date);
		$view_data['tool_currency'] = $user_currency;
	
		$this->render('list', $view_data);
	}

	public function actionEditInvoices()
	{
		// is someone already logged in?
		error_reporting(0);
		$this->checklogin();
        $this->layout = 'main';

		$order_id = Yii::app()->input->post('order_id_for_invoices');
		$invoice_number = Yii::app()->input->post('invoice_number');
		$vendor_id = Yii::app()->input->post('vendor_id_for_invoices');
		$order = new Order();
		$order->rs = array();
		$order->rs['order_id'] = $order_id;
		if(!empty($invoice_number))
		$order->rs['invoice_number'] = $invoice_number;
		$order->write();
		
		// invoices if any were uploaded
		if (Yii::app()->input->post('form_submitted_for_invoices'))
		{
			if (isset($_FILES['invoice_file']) && is_array($_FILES['invoice_file']) && count($_FILES['invoice_file']))
			{
				if (!is_dir('uploads/orders/invoices')) mkdir('uploads/orders/invoices');
				if (!is_dir('uploads/orders/invoices/' . $order_id)) 
					mkdir('uploads/orders/invoices/' . $order_id);
				
				for ($idx=0; $idx<count($_FILES['invoice_file']['name']); $idx++)
				{
					$invoice_file = array();
					$invoice_file['name'] = $_FILES['invoice_file']['name'][$idx];
					$invoice_file['type'] = $_FILES['invoice_file']['type'][$idx];
					$invoice_file['tmp_name'] = $_FILES['invoice_file']['tmp_name'][$idx];
					$invoice_file['error'] = $_FILES['invoice_file']['error'][$idx];
					$invoice_file['size'] = $_FILES['invoice_file']['size'][$idx];
					
					if (!isset($invoice_file['error']) || empty($invoice_file['error']))
					{
						$file_name = time().'_'.$invoice_file['name'];
						$orderInvoice = new OrderInvoice();
						$orderInvoice->rs = array();
						$orderInvoice->rs['order_id'] = $order_id;
						$orderInvoice->rs['vendor_id'] = $vendor_id;
						$orderInvoice->rs['created_by_type'] = 2;
						$orderInvoice->rs['invoice_number'] = $invoice_number;
						$orderInvoice->rs['file_name'] = $file_name;
						$orderInvoice->rs['status'] = 2;
						$orderInvoice->rs['created_at'] = date('Y-m-d H:i:s');
						$orderInvoice->write();
			            move_uploaded_file($invoice_file['tmp_name'], 'uploads/orders/invoices/' . $order_id . '/' . $file_name);
					}
				}
			}
		
			$order_invoice = $_POST['invoice'];
			if(!empty($order_invoice)){
			foreach ($order_invoice as $invoiceID=>$invoiceNumer) {
            	$invoice_id = $uploaded_file['id'];
          /*  if(!empty($_POST['approve_invoice_only'])){
            	$sql = "UPDATE order_invoices SET status = 2 WHERE id =".$invoiceID;
        	}else if(!empty($_POST['update_invoice_only'])){
            	$sql = "UPDATE order_invoices SET invoice_number = '".$invoiceNumer."' WHERE id =".$invoiceID;
        	}else if(!empty($_POST['approve_update_invoice'])){*/
        		$order_invoice_status = !empty($_POST['approve_invoice_only'][$invoiceID])?2:1;
        		
            	$sql = "UPDATE order_invoices SET invoice_number = '".$invoiceNumer."',status = ".$order_invoice_status." WHERE id =".$invoiceID;
        	//}
            Yii::app()->db->createCommand($sql)->execute();
        	}}
		}

		$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
	}

	public function actionChangeStatus()
	{
		
		if(!empty($_POST['order_id'])){
		 	$status = $_POST['order_status'];
		 	$order_id = $_POST['order_id'];
            $sql = "UPDATE orders SET order_status = '".$status."' WHERE order_id =".$order_id;
            if(Yii::app()->db->createCommand($sql)->execute()){
            	Yii::app()->user->setFlash('success', "Cancelled Successfully");
            	$order = new Order;
            	$order->orderChangeEmail($order_id);
            	$this->redirect(AppUrl::bicesUrl('orders/list'));
            }else {
            	Yii::app()->user->setFlash('error', "There is a problem, try again or contact to administrator");
            	$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
            }
            	
        }
	}

	public function actionInvoiceDownload()
	{
		$invoiceID = $_GET['invoice-id'];
		$orderID = $_GET['order-id'];
        $sql = "select file_name from order_invoices WHERE id =".$invoiceID;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();

       
	    $file = Yii::app()->basePath."/../uploads/orders/invoices/".$orderID."/".$fileReader['file_name'];
       
		if ($fileReader) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}

	}

	public function actionEdit($order_id = 0)
	{
		// is someone already logged in?
		error_reporting(0);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'orders_edit';
		// and also currency preference by the user
		//unset(Yii::app()->session['user_currency']);
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		//$this->getCurrency($user_currency);
		$vendorOrderArr = array();
		$vendorIDSArr = array();// 

		// if form was submitted ... first save the data to the database

		if (Yii::app()->input->post('form_submitted'))
		{ 
			
			$rand = rand ( 10000 , 99999 );
			
			// START: Check if already recurrent_number is there, so no duplicate can occure for irrelevant orders
			$checkSql = "select recurrent_number from orders where recurrent_number='".$rand."'";
			$checkSqlReader = Yii::app()->db->createCommand($checkSql)->queryRow();
			if(!empty($checkSqlReader)){
				$rand = time();	
			}
			// END: Check if already recurrent_number is there, so no duplicate can occure for irrelevant orders
			$files_only_upload = Yii::app()->input->post('files_only_upload');
			$order_id = Yii::app()->input->post('order_id');
			if (!empty($order_id))
			{
				// what if vendor id is blank?
				$override_data = array();
				$vendor_id = Yii::app()->input->post('vendor_id');
				$override_data['recurrent_number'] = Yii::app()->input->post('recurrent_number');
				if (empty($vendor_id))
				{
					$vendor_name = Yii::app()->input->post('vendor_name');
					$vendor = new Vendor();
					$override_data['vendor_id'] = $vendor->getIdForName($vendor_name);
				}

				// and also by default tax is being applied at 20% :(
				//$tax_flag = Yii::app()->input->post('tax_flag');
				//if (!isset($tax_flag) || !$tax_flag) $override_data['tax_rate'] = 0;

				if(!empty( Yii::app()->input->post('total_price'))){
					//$override_data['calc_price'] = Yii::app()->input->post('total_price')*Yii::app()->input->post('currency_rate');
					$override_data['calc_price'] = Yii::app()->input->post('total_price');
				}

				// save actual order data
				$order = new Order();
				$order->saveData($override_data);
				$order_id = $order->rs['order_id'];
				// products in the order
				$order_detail = new OrderDetail();
				$order_detail->delete(array('order_id' => $order->rs['order_id']));
				$product_types = Yii::app()->input->post('product_type');
				$product_names = Yii::app()->input->post('product_name');
				$product_ids = Yii::app()->input->post('product_id');
				$vendor_ids = Yii::app()->input->post('vendor_id');
				$quantities = Yii::app()->input->post('quantity');
				$unit_prices = Yii::app()->input->post('unit_price');
				$taxs = Yii::app()->input->post('tax');
				$calc_taxs = Yii::app()->input->post('calc_tax');
				$calc_unit_prices = Yii::app()->input->post('calc_unit_price');
				$shipping_costs = Yii::app()->input->post('shipping_cost');

				$total_records = count($product_types);
              
				for ($idx=0; $idx<=$total_records; $idx++)
				{
					$order_data = array('order_id' => $order->rs['order_id']);
					//$order_data = $order_id;
					$order_data['product_type'] = isset($product_types[$idx]) ? $product_types[$idx] : "";
					$product_name = isset($product_names[$idx]) ? $product_names[$idx] : "";
					$order_data['product_id'] = isset($product_ids[$idx]) ? $product_ids[$idx] : 0;
					$order_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
					$order_data['quantity'] = isset($quantities[$idx]) ? $quantities[$idx] : 1;
					$order_data['unit_price'] = isset($unit_prices[$idx]) ? $unit_prices[$idx] : 0;
					$order_data['tax'] = isset($taxs[$idx]) ? $taxs[$idx] : 0;
					$order_data['calc_tax'] = isset($calc_taxs[$idx]) ? $calc_taxs[$idx] : 0;
					$order_data['calc_unit_price'] = isset($calc_unit_prices[$idx]) ? $calc_unit_prices[$idx] : 0;

					$order_data['shipping_cost'] = isset($shipping_costs[$idx]) ? $shipping_costs[$idx] : 0;

					// new products can be added I guess like the vendor above :(
					if (!empty($order_data['product_type']) && !empty($order_data['unit_price']))
					{
						$order_product = new Product();
						if (empty($order_data['product_id']))
						{
							$order_data['product_id'] = $order_product->getIdForName($product_name);

							$order_product->rs = array();
							$order_product->rs['product_id'] = $order_data['product_id'];
							$order_product->rs['price'] = round($order_data['unit_price'] / $order_data['quantity'], 2);
							$order_product->write();
						}

						if (!empty($order_data['product_id'])) $order_detail->saveData($order_data);
					}

					if ($order->rs['order_status'] == 'Submitted')
					{
						$approval_route_created = $order->createRouteForApproval($order->rs['order_id']);
						//CVarDumper::dump($approval_route_created,10,1);die;
						if (!$approval_route_created)
						{
							// do not submit the order if no approval route found
							$non_submitted_order = new Order();
							$non_submitted_order->rs = array();
							$non_submitted_order->rs['order_id'] = $order->rs['order_id'];
							$non_submitted_order->rs['order_status'] = 'Pending';
							$non_submitted_order->write();

							// let the user know what happened
							Yii::app()->session['order_status_changed_id'] = $order->rs['order_id'];
						}
					}

					$total_documents = Yii::app()->input->post('total_documents');
					$uploadedFiles = Yii::app()->session['uploaded_files'];
					if(!empty($uploadedFiles))
						foreach ($uploadedFiles as $fileValue){
						if (empty($fileValue['order_id'])){
							$file_name = $fileValue['file_name'];
							$file_description = $fileValue['file_description'];
							if (!is_dir('uploads/orders')) mkdir('uploads/orders');
							if (!is_dir('uploads/orders/' . $order->rs['order_id']))
								mkdir('uploads/orders/' . $order->rs['order_id']);

							if(@rename('uploads/orders/' . $file_name, 'uploads/orders/' . $order->rs['order_id'] . '/' . $file_name)){
										$order->saveOrderFile($order->rs['order_id'], $file_name, $file_description);
							}
						}
					}
				}
			} else {



			// receipts if any were uploaded
//			if (isset($_FILES['file']) && is_array($_FILES['file']) && count($_FILES['file']))
//			{
//				if (!is_dir('uploads/orders')) mkdir('uploads/orders');
//				if (!is_dir('uploads/orders/' . $order_id))
//					mkdir('uploads/orders/' . $order_id);
//
//				for ($idx=0; $idx<count($_FILES['file']['name']); $idx++)
//				{
//					$receipt_file = array();
//					$receipt_file['name'] = $_FILES['file']['name'][$idx];
//					$receipt_file['type'] = $_FILES['file']['type'][$idx];
//					$receipt_file['tmp_name'] = $_FILES['file']['tmp_name'][$idx];
//					$receipt_file['error'] = $_FILES['file']['error'][$idx];
//					$receipt_file['size'] = $_FILES['file']['size'][$idx];
//
//					if (!isset($receipt_file['error']) || empty($receipt_file['error']))
//					{
//			            $file_name = $receipt_file['name'];
//			            move_uploaded_file($receipt_file['tmp_name'], 'uploads/orders/' . $order_id . '/' . $file_name);
//					}
//				}
//			}
				// products in the order
				$order_detail = new OrderDetail();
				//$order_detail->delete(array('order_id' => $order->rs['order_id']));
	
				$product_types = Yii::app()->input->post('product_type');
				$product_names = Yii::app()->input->post('product_name');
				$product_ids = Yii::app()->input->post('product_id');
				$vendor_ids = Yii::app()->input->post('vendor_id');
				$quantities = Yii::app()->input->post('quantity');
				$unit_prices = Yii::app()->input->post('unit_price');
				$taxs = Yii::app()->input->post('tax');
				$calc_taxs = Yii::app()->input->post('calc_tax');
				$calc_unit_prices = Yii::app()->input->post('calc_unit_price');
				
				$shipping_costs = Yii::app()->input->post('shipping_cost');

				$total_records = count($product_types)-1;
				
				for ($idx=0; $idx<=$total_records; $idx++)
				{
					// what if vendor id is blank?
					$override_data = array();
					$override_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
					$override_data['recurrent_number'] = $rand;
					// and also by default tax is being applied at 20% :(
					//$tax_flag = Yii::app()->input->post('tax_flag');
					//if (!isset($tax_flag) || !$tax_flag) $override_data['tax_rate'] = 0;

					if(!empty( Yii::app()->input->post('total_price'))){
						//$override_data['calc_price'] = Yii::app()->input->post('total_price')/Yii::app()->input->post('currency_rate');
						$override_data['calc_price'] = Yii::app()->input->post('total_price');
					}

					// save actual order data
					if(!in_array($override_data['vendor_id'],$vendorIDSArr)){
						$order = new Order();
						$taxAmount = !empty(Yii::app()->input->post('tax_rate'))?Yii::app()->input->post('tax_rate'):0;
						$shipAmount = !empty(Yii::app()->input->post('ship_amount'))?Yii::app()->input->post('ship_amount'):0;
						$override_data['calc_price'] = $override_data['calc_price']+$taxAmount+$shipAmount;
						$order->saveData($override_data);
						$order_id = $order->rs['order_id'];
						$vendorIDSArr[] = $override_data['vendor_id'];
						$vendorOrderArr[$override_data['vendor_id']] = $order_id;
					}else{
						$order_id = $vendorOrderArr[$override_data['vendor_id']];
					}
				   
					

					$order_data = array('order_id' => $order->rs['order_id']);
					//$order_data = $order_id;
					$order_data['product_type'] = isset($product_types[$idx]) ? $product_types[$idx] : "";
					$product_name = isset($product_names[$idx]) ? $product_names[$idx] : "";
					$order_data['product_id'] = isset($product_ids[$idx]) ? $product_ids[$idx] : 0;
					$order_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
					$order_data['quantity'] = isset($quantities[$idx]) ? $quantities[$idx] : 1;
					$order_data['unit_price'] = isset($unit_prices[$idx]) ? $unit_prices[$idx] : 0;
					$order_data['tax'] = isset($taxs[$idx]) ? $taxs[$idx] : 0;
					$order_data['calc_tax'] = isset($calc_taxs[$idx]) ? $calc_taxs[$idx] : 0;
					$order_data['calc_unit_price'] = isset($calc_unit_prices[$idx]) ? $calc_unit_prices[$idx] : 0;
					$order_data['shipping_cost'] = isset($shipping_costs[$idx]) ? $shipping_costs[$idx] : 0;

					// new products can be added I guess like the vendor above :(
					if (!empty($order_data['product_type']) && !empty($order_data['unit_price']))
					{
						$order_product = new Product();
						if (empty($order_data['product_id']))
						{
							$order_data['product_id'] = $order_product->getIdForName($product_name);
							
							$order_product->rs = array();
							$order_product->rs['product_id'] = $order_data['product_id'];
							$order_product->rs['price'] = round($order_data['unit_price'] / $order_data['quantity'], 2);
							$order_product->write();
						}
						if (!empty($order_data['product_id'])){ $order_detail->saveData($order_data); }
					}

					// and then routing information for approvals too
					if ($order->rs['order_status'] == 'Submitted')
					{
						$approval_route_created = $order->createRouteForApproval($order->rs['order_id']);
						//CVarDumper::dump($approval_route_created,10,1);die;
						if (!$approval_route_created)
						{
							// do not submit the order if no approval route found
							$non_submitted_order = new Order();
							$non_submitted_order->rs = array();
							$non_submitted_order->rs['order_id'] = $order->rs['order_id'];
							$non_submitted_order->rs['order_status'] = 'Pending';
							$non_submitted_order->write();

							// let the user know what happened
							Yii::app()->session['order_status_changed_id'] = $order->rs['order_id'];
						}
					}

					$total_documents = Yii::app()->input->post('total_documents');
					$uploadedFiles = Yii::app()->session['uploaded_files'];
					if(!empty($uploadedFiles))
						foreach ($uploadedFiles as $fileValue){
						if (empty($fileValue['order_id'])){
							$file_name = $fileValue['file_name'];
							$file_description = $fileValue['file_description'];
							if (!is_dir('uploads/orders')) mkdir('uploads/orders');
							if (!is_dir('uploads/orders/' . $order->rs['order_id']))
								mkdir('uploads/orders/' . $order->rs['order_id']);

							if(@rename('uploads/orders/' . $file_name, 'uploads/orders/' . $order->rs['order_id'] . '/' . $file_name)){
										$order->saveOrderFile($order->rs['order_id'], $file_name, $file_description);
							}
						}
					}

				}

			}

			if(Yii::app()->request->isAjaxRequest){
				echo "Order submitted";
				exit;
			}else{
				if (!$files_only_upload)
					$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order->rs['order_id']));
				else
					$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
			}
		}
 
		// get data for the selected vendor
		$view_data = array('order_id' => $order_id);
		$order = new Order();
		$view_data['order'] = $order->getOrders($order_id);
		if (!$view_data['order'] || !is_array($view_data['order'])) $order_id = 0;

		if ($order_id && (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4))))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if ($view_data['order']['user_id'] != $my_user_id
					&& $view_data['order']['created_by_user_id'] != $my_user_id)
				$this->redirect(AppUrl::bicesUrl('orders/list/'));
		}

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));

		// other drop downs may be needed for vendor quick add modal window
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));
			
		// and also product information for the order
		$order_detail = new OrderDetail();
		$view_data['order_details'] = $order_detail->getOrderDetails($order_id);

		// and also product information for the order
		$order_vendor_detail = new OrderVendorDetail();
		$view_data['order_vendor_detail'] = $order_vendor_detail->getOrderVendorDetails($order_id);

		// and also product information for the order
		$view_data['related_orders'] = $order->getRelatedOrders($order_id);

		// and finally approvals if any
		if ($order_id)
		{
			$route = new OrderRoute();
			$view_data['approvals'] = $route->getApprovalRouteO($view_data['order']);
		}
		else $view_data['approvals'] = array();

		// if approved, is purchase order already created?
		if ($order_id)
		{
			$purchase_order = new PurchaseOrder();
			$view_data['purchase_order'] = $purchase_order->getOne(array('order_id' => $order_id));
		}
		else $view_data['purchase_order'] = false;

		// and display
		if(!empty($order_id)){
			$view_data['order_invoice'] = $order->orderInvoice($order_id);
		}
		unset(Yii::app()->session['uploaded_files']);
		$this->render('edit', $view_data);
		$this->renderPartial('/vendors/modal_add', $view_data);
	}

	public function actionCreateByModal(){

		// is someone already logged in?
		error_reporting(0);
		 
		// and also currency preference by the user
		//unset(Yii::app()->session['user_currency']);
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		//$this->getCurrency($user_currency);
		$vendorOrderArr = array();
		$vendorIDSArr = array();// 

		// if form was submitted ... first save the data to the database
	
 		$order_id=0;
		// get data for the selected vendor
		$view_data = array('order_id' => $order_id);
		$order = new Order();
		$view_data['order'] = $order->getOrders($order_id);
		if (!$view_data['order'] || !is_array($view_data['order'])) $order_id = 0;

		if ($order_id && (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4))))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if ($view_data['order']['user_id'] != $my_user_id
					&& $view_data['order']['created_by_user_id'] != $my_user_id)
				$this->redirect(AppUrl::bicesUrl('orders/list/'));
		}

		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));

		// other drop downs may be needed for vendor quick add modal window
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));
			
		// and also product information for the order
		$order_detail = new OrderDetail();
		$view_data['order_details'] = $order_detail->getOrderDetails($order_id);

		// and also product information for the order
		$order_vendor_detail = new OrderVendorDetail();
		$view_data['order_vendor_detail'] = $order_vendor_detail->getOrderVendorDetails($order_id);

		// and also product information for the order
		$view_data['related_orders'] = $order->getRelatedOrders($order_id);

		// and finally approvals if any
		if ($order_id)
		{
			$route = new OrderRoute();
			$view_data['approvals'] = $route->getApprovalRouteO($view_data['order']);
		}
		else $view_data['approvals'] = array();

		// if approved, is purchase order already created?
		if ($order_id)
		{
			$purchase_order = new PurchaseOrder();
			$view_data['purchase_order'] = $purchase_order->getOne(array('order_id' => $order_id));
		}
		else $view_data['purchase_order'] = false;

		// and display
		if(!empty($order_id)){
			$view_data['order_invoice'] = $order->orderInvoice($order_id);
		}
		unset(Yii::app()->session['uploaded_files']);
		$this->renderPartial('create_modal', $view_data);
		exit;
		$this->renderPartial('/vendors/modal_add', $view_data);
		
	

	}

	public function actionUploadDocument(){  
		error_reporting(0);
		if (!empty($_FILES)){
			$order = new Order;
			$total_documents = Yii::app()->input->post('total_documents');
			if (isset($_FILES['file']) && is_array($_FILES['file'])){
				$order_id = Yii::app()->input->post('order_id');
				if(empty($order_id)){
					$order_id ='';
				}
				if (!is_dir('uploads/orders')) mkdir('uploads/orders');
				if (!is_dir('uploads/orders/' . $order_id))
					mkdir('uploads/orders/' . $order_id);
				if (!is_dir('uploads/orders/'))
					mkdir('uploads/orders/');

				if (!isset($_FILES['file']['error']) || empty($_FILES['file']['error']))
				{
					$file_name = $_FILES['file']['name'];
					$file_description = Yii::app()->input->post('file_description');
					$uploadedFiles = Yii::app()->session['uploaded_files'];

					$uploadedFiles[] = array('file_name'=>$file_name,'file_description'=>$file_description,'orders'=>$orders);

					Yii::app()->session['uploaded_files'] = $uploadedFiles;
					//move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/orders/' . $file_name);
					if(move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/orders/' . (!empty($order_id)?$order_id. '/'. $file_name: $file_name)) && !empty($order_id)){
						$order->saveOrderFile($order_id, $file_name, $file_description);
					}
					$display = '<table class="table"><tbody>';
					foreach(Yii::app()->session['uploaded_files'] as $key=>$value){
						$display .= '<tr><td>'.$value['file_name'].'</td><td>'.$value['file_description'].'</td><td><span class="glyphicon glyphicon-trash" aria-hidden="true"  style="cursor:grab"  onclick="(confirm(\'Are you sure?\')?deleteSessionDocument('.$key.'):\'\')"></span></td></tr>';	
					}
					$display .= '</tbody></table>';
				}
				echo $display;exit;
			}
		}
	}

	public function actionDeleteSessionDocument(){  
		error_reporting(0);
		if (isset($_POST['IDX'])){
			$order = new Order;
				
			$uploadedFiles = Yii::app()->session['uploaded_files'];

			$delFile = $uploadedFiles[$_POST['IDX']];
			// which expense record the file relates to? 
			$_POST['file_name'] = $delFile['file_name'];
			$_POST['order_id'] = $delFile['order_id'];
			$file_name = Yii::app()->input->post('file_name');
			$this->actionDeleteFile();
			unset($uploadedFiles[$_POST['IDX']]);


			//$uploadedFiles[] = array('file_name'=>$file_name,'file_description'=>$file_description,'expense_id'=>$expense_id);

			Yii::app()->session['uploaded_files'] = $uploadedFiles;
			
			$display = '<table class="table"><tbody>';
			foreach(Yii::app()->session['uploaded_files'] as $key=>$value){
				$display .= '<tr><td>'.$value['file_name'].'</td><td>'.$value['file_description'].'</td><td><span class="glyphicon glyphicon-trash" aria-hidden="true" style="cursor:grab" onclick="(confirm(\'Are you sure?\')?deleteSessionDocument('.$key.'):\'\')"></span></td></tr>';	
			}
			$display .= '</tbody></table>';
			echo $display;exit;
		}
	}

	public function actionClone($order_id = 0)
	{
		// is someone already logged in?
		error_reporting(0);
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'orders_edit';
		// and also currency preference by the user
		//unset(Yii::app()->session['user_currency']);
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		//$this->getCurrency($user_currency);
		$vendorOrderArr = array();
		$vendorIDSArr = array();// 

		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted'))
		{ 
			$rand = rand ( 10000 , 99999 );
			// START: Check if already recurrent_number is there, so no duplicate can occure for irrelevant orders
			$checkSql = "select recurrent_number from orders where recurrent_number='".$rand."'";
			$checkSqlReader = Yii::app()->db->createCommand($checkSql)->queryRow();
			if(!empty($checkSqlReader)){
				$rand = time();	
			}
			// END: Check if already recurrent_number is there, so no duplicate can occure for irrelevant orders
			$files_only_upload = Yii::app()->input->post('files_only_upload');
			$order_id = Yii::app()->input->post('order_id');
			// products in the order
			$order_detail = new OrderDetail();
			//$order_detail->delete(array('order_id' => $order->rs['order_id']));

			$product_types = Yii::app()->input->post('product_type');
			$product_names = Yii::app()->input->post('product_name');
			$product_ids = Yii::app()->input->post('product_id');
			$vendor_ids = Yii::app()->input->post('vendor_id');
			$quantities = Yii::app()->input->post('quantity');
			$unit_prices = Yii::app()->input->post('unit_price');
			$taxs = Yii::app()->input->post('tax');
			$calc_taxs = Yii::app()->input->post('calc_tax');
			$calc_unit_prices = Yii::app()->input->post('calc_unit_price');
			
			$shipping_costs = Yii::app()->input->post('shipping_cost');

			$total_records = count($product_types)-1;
		
			for ($idx=0; $idx<=$total_records; $idx++)
			{
				// what if vendor id is blank?
				$override_data = array();
				$override_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
				$override_data['recurrent_number'] = $rand;
			
				if(!empty( Yii::app()->input->post('total_price'))){
					$override_data['calc_price'] = Yii::app()->input->post('total_price');
				}
				// save actual order data
				if(!empty($override_data['vendor_id']) && !in_array($override_data['vendor_id'],$vendorIDSArr)){
					$order = new Order();
					$taxAmount = !empty(Yii::app()->input->post('tax_rate'))?Yii::app()->input->post('tax_rate'):0;
					$shipAmount = !empty(Yii::app()->input->post('ship_amount'))?Yii::app()->input->post('ship_amount'):0;
					$override_data['calc_price'] = $override_data['calc_price']+$taxAmount+$shipAmount;
					$order->saveData($override_data);
					$order_id = $order->rs['order_id'];
					$vendorIDSArr[] = $override_data['vendor_id'];
					$vendorOrderArr[$override_data['vendor_id']] = $order_id;
				}else{
					$order_id = $vendorOrderArr[$override_data['vendor_id']];
				}
				$order_data = array('order_id' => $order->rs['order_id']);
				//$order_data = $order_id;
				$order_data['product_type'] = isset($product_types[$idx]) ? $product_types[$idx] : "";
				$product_name = isset($product_names[$idx]) ? $product_names[$idx] : "";
				$order_data['product_id'] = isset($product_ids[$idx]) ? $product_ids[$idx] : 0;
				$order_data['vendor_id'] = isset($vendor_ids[$idx]) ? $vendor_ids[$idx] : 0;
				$order_data['quantity'] = isset($quantities[$idx]) ? $quantities[$idx] : 1;
				$order_data['unit_price'] = isset($unit_prices[$idx]) ? $unit_prices[$idx] : 0;
				$order_data['tax'] = isset($taxs[$idx]) ? $taxs[$idx] : 0;
				$order_data['calc_tax'] = isset($calc_taxs[$idx]) ? $calc_taxs[$idx] : 0;
				$order_data['calc_unit_price'] = isset($calc_unit_prices[$idx]) ? $calc_unit_prices[$idx] : 0;
				$order_data['shipping_cost'] = isset($shipping_costs[$idx]) ? $shipping_costs[$idx] : 0;

				// new products can be added I guess like the vendor above :(
				if (!empty($order_data['product_type']) && !empty($order_data['unit_price']))
				{
					$order_product = new Product();
					if (empty($order_data['product_id']))
					{
						$order_data['product_id'] = $order_product->getIdForName($product_name);
						
						$order_product->rs = array();
						$order_product->rs['product_id'] = $order_data['product_id'];
						$order_product->rs['price'] = round($order_data['unit_price'] / $order_data['quantity'], 2);
						$order_product->write();
					}
					if (!empty($order_data['product_id'])){ $order_detail->saveData($order_data); }
				}

				// and then routing information for approvals too
				if ($order->rs['order_status'] == 'Submitted')
				{
					$approval_route_created = $order->createRouteForApproval($order->rs['order_id']);
					//CVarDumper::dump($approval_route_created,10,1);die;
					if (!$approval_route_created)
					{
						// do not submit the order if no approval route found
						$non_submitted_order = new Order();
						$non_submitted_order->rs = array();
						$non_submitted_order->rs['order_id'] = $order->rs['order_id'];
						$non_submitted_order->rs['order_status'] = 'Pending';
						$non_submitted_order->write();

						// let the user know what happened
						Yii::app()->session['order_status_changed_id'] = $order->rs['order_id'];
					}
				}

				$total_documents = Yii::app()->input->post('total_documents');
				for ($idxInner=1; $idxInner<=$total_documents; $idxInner++)
				{
					$delete_document_flag = Yii::app()->input->post('delete_document_flag_' . $idxInner);
					if ($delete_document_flag) continue;

					if (isset($_FILES['order_file_' . $idxInner]) && is_array($_FILES['order_file_' . $idxInner]))
					{
						if (!is_dir('uploads/orders')) mkdir('uploads/orders');
						if (!is_dir('uploads/orders/' . $order->rs['order_id']))
							mkdir('uploads/orders/' . $order->rs['order_id']);

						if (!isset($_FILES['quote_file_' . $idxInner]['error']) || empty($_FILES['quote_file_' . $idxInner]['error']))
						{
							$file_name = $_FILES['order_file_' . $idxInner]['name'];
							move_uploaded_file($_FILES['order_file_' . $idxInner]['tmp_name'], 'uploads/orders/' . $order->rs['order_id'] . '/' . $file_name);

							$file_description = Yii::app()->input->post('file_desc_' . $idxInner);
							$order->saveOrderFile($order->rs['order_id'], $file_name, $file_description);
						}
					}
				}

			}
			if (!$files_only_upload)
				$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order->rs['order_id']));
			else
				$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
		}
 

		// get data for the selected vendor
		$view_data = array('order_id' => $order_id);
		$order = new Order();
		$view_data['order'] = $order->getOrders($order_id);
		if (!$view_data['order'] || !is_array($view_data['order'])) $order_id = 0;

		if ($order_id && (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4))))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if ($view_data['order']['user_id'] != $my_user_id
					&& $view_data['order']['created_by_user_id'] != $my_user_id)
				$this->redirect(AppUrl::bicesUrl('orders/list/'));
		}
		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$project = new Project();
		$view_data['projects'] = $project->getAll(array('ORDER' => 'project_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));

		// other drop downs may be needed for vendor quick add modal window
		$payment_term = new PaymentTerm();
		$view_data['payment_terms'] = $payment_term->getAll();

		$shipping_method = new ShippingMethod();
		$view_data['shipping_methods'] = $shipping_method->getAll();

		$currency_rate = new CurrencyRate();
		$view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();

		$industry = new Industry();
		$view_data['industries'] = $industry->getAll(array('ORDER' => 'value'));
			
		// and also product information for the order
		$order_detail = new OrderDetail();
		$view_data['order_details'] = $order_detail->getOrderDetails($order_id);

		// and also product information for the order
		$order_vendor_detail = new OrderVendorDetail();
		$view_data['order_vendor_detail'] = $order_vendor_detail->getOrderVendorDetails($order_id);

		// and also product information for the order
		$view_data['related_orders'] = $order->getRelatedOrders($order_id);

		// and finally approvals if any
		if ($order_id)
		{
			$route = new OrderRoute();
			$view_data['approvals'] = $route->getApprovalRouteO($view_data['order']);
		}
		else $view_data['approvals'] = array();

		// if approved, is purchase order already created?
		if ($order_id)
		{
			$purchase_order = new PurchaseOrder();
			$view_data['purchase_order'] = $purchase_order->getOne(array('order_id' => $order_id));
		}
		else $view_data['purchase_order'] = false;

		// and display
		if(!empty($order_id)){
			$view_data['order_invoice'] = $order->orderInvoice($order_id);
		}
		$this->render('clone', $view_data);
		$this->renderPartial('/vendors/modal_add', $view_data);
	}

	public function actionUpdateOnlyStatus()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		// if form was submitted ... first save the data to the database
		$order_id = Yii::app()->input->post('order_id_from_modal');
		$order_status = Yii::app()->input->post('order_status_from_modal');
		if (Yii::app()->input->post('modal_form_submitted'))
		{			
			if ($order_id)
			{
				$order = new Order();
				$order->rs = array();
				$order->rs['order_id'] = $order_id;
				$order->rs['order_status'] = $order_status;
				$order->write();
				
				$not_applicable_statuses_for_po = array('Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier', 'Approved');
				if (!in_array($order_status, $not_applicable_statuses_for_po))
				{
					$purchase_order = new PurchaseOrder();
					$po_id = Yii::app()->input->post('po_id_from_modal');

					if (!$po_id)
					{
						$po_data = $purchase_order->getOne(array('order_id' => $order_id));
						if ($po_data && is_array($po_data) && isset($po_data['po_id']))
							$po_id = $po_data['po_id'];
					}
					
					if (!empty($po_id))
					{
						$purchase_order->rs = array();
						$purchase_order->rs['po_id'] = $po_id;
						$purchase_order->rs['po_status'] = $order_status;
						$purchase_order->write();
					}
				}
			}
		}

		$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
	}

	public function actionMoveSupplierInvoices()
	{
		// is someone already logged in?
		$this->checklogin();
		$this->layout = 'main';

		// if form was submitted ... first save the data to the database
		$order_id = Yii::app()->input->post('order_id_from_modal');
		$order_status = Yii::app()->input->post('order_status_from_modal');
		$order = new Order();
		$docs = $order->getSupplierDocs($order_id);
		$order_invoice = $order->orderInvoice($order_id);
		foreach ($order_invoice as $uploaded_file) {
            if($uploaded_file['status']==1){
            $invoice_id = $uploaded_file['id'];
            $sql = "UPDATE order_invoices SET status = '2' WHERE id =".$invoice_id;
            Yii::app()->db->createCommand($sql)->execute();
        }}

	/*	foreach($docs as $doc):
			copy('uploads/orders/' . $order_id . '/'. $doc['vendor_id'] . '/' . $doc['file_name'] , 'uploads/orders/invoices/' . $order_id . '/' . $doc['file_name']);
		endforeach;*/

		$this->redirect(AppUrl::bicesUrl('orders/edit/' . $order_id));
	}

	public function actionGetProducts()
	{
		$search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";

		$productPriceInGBP = 0;

		if (isset($_REQUEST['product_type']) && $_REQUEST['product_type'] == 'Product')
		{
			$products = array();

			$product = new Product();
			$search_results = $product->getAll(array('active' => 1, 'product_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'product_name', 'limit' => 100));

			foreach ($search_results as $product_data){
				$currency_id = $product_data['currency_id'];
	    		$sql = "select rate,currency from currency_rates where id=".$currency_id;
	        	$currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
	        	if(!empty($currencyReader['rate'])){
	        		$currencyRate = $currencyReader['rate'];
	        	}else{
	        		$currencyRate=1;
	        	}

	        	$productPriceInGBP = $product_data['price']/$currencyRate;

				$products[] = array("value" => $product_data['product_name'],
									"data" => array('id' => $product_data['product_id'],
													 'price' => $product_data['price'],
													'currency_id' => $currency_id,
													//'currency_id_symbol'=>FunctionManager::showCurrencySymbol('',$currency_id),
													'currency_id_symbol'=>$currencyReader['currency'],
													'GBP_price'=>$productPriceInGBP
												));
			}

			echo json_encode(array('suggestions' => $products));
		}
		else
		{
			$bundles = array();

			$bundle = new Bundle();
			$search_results = $bundle->getAll(array('active' => 1, 'bundle_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'bundle_name'));

			foreach ($search_results as $bundle_data){
				$currency_id = $bundle_data['currency_id'];
	    		$sql = "select rate,currency from currency_rates where id=".$currency_id;
	        	$currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
	        	if(!empty($currencyReader['rate'])){
	        		$currencyRate = $currencyReader['rate'];
	        	}else{
	        		$currencyRate=1;
	        	}

	        	$productPriceInGBP = round($bundle_data['price']/$currencyRate,2);

				$bundles[] = array("value" => $bundle_data['bundle_name'],
									"data" => array('id' => $bundle_data['bundle_id'],
													 'price' => $bundle_data['price'],
													 'currency_id' => $bundle_data['currency_id'],
													 //'currency_id_symbol'=>FunctionManager::showCurrencySymbol('',$bundle_data['currency_id']),
													 'currency_id_symbol'=>$currencyReader['currency'],
													 'GBP_price'=>$productPriceInGBP

													));
			}

			echo json_encode(array('suggestions' => $bundles));
		}

	}

	public function actionChangeProductPric(){
		if(isset($_REQUEST['product_currency_id']) && isset($_REQUEST['price'])){
			$currencyID = $_REQUEST['product_currency_id'];
			$price = $_REQUEST['price'];
			$data = array();
			$data['GBP_price'] = FunctionManager::priceInGBPByID($price,$currencyID);
			echo json_encode($data);
		}
	}

	public function actionGetCurrencyRates()
	{
		$data = array();
		$currencyRate = new CurrencyRate();
		$currency = $currencyRate->getCurrencyRates($_POST['currency_id']);

		$currencySymbol=FunctionManager::showCurrencySymbol(strtolower($currency['currency']));

		$data['rate'] = $currency['rate'];
		$data['currency_symbol'] = $currencySymbol;
		$data['currency'] = $currency['currency'];

		echo json_encode($data);

	}

    public function actionGetRates()
    {
        $data = array();
        $currencyRate = new CurrencyRate();
        $currency = $currencyRate->getCurrencyRates($_POST['currency_id']);

        $currencySymbol=FunctionManager::showCurrencySymbol(strtolower($currency['currency']));

        /*$data['rate'] = $currency['rate'];
        $data['currency_symbol'] = $currencySymbol;
        $data['currency'] = $currency['currency'];*/

        echo $currency['rate'];

    }

	public function actionGetVendors()
	{
		$search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
		
		$text_field_name = "value";
		$id_field_name = "data";
		
		$select2_call = false;
		if (is_array($search_string)) 
		{
			$search_string = $search_string['term'];
			$select2_call = true;
			$text_field_name = "text";
			$id_field_name = "id";
		}

		$vendors = array();

		$vendor = new Vendor();
		$search_results = $vendor->getAll(array('active' => 1,'member_type' => 0, 'vendor_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'vendor_name'));

		foreach ($search_results as $vendor_data){
			if($vendor_data['vendor_archive']==null  or $vendor_data['vendor_archive']==''){
				$vendors[] = array($text_field_name => $vendor_data['vendor_name'], $id_field_name => $vendor_data['vendor_id']);
			}
		}

		if ($select2_call) echo json_encode($vendors);
		else echo json_encode(array('suggestions' => $vendors));
	}

	public function actionDeleteFile()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$order = new Order();

		// which order record the file relates to?
		$order_id = Yii::app()->input->post('order_id');
		$file_name = Yii::app()->input->post('file_name');
		
		if(!empty($order_id)){
		// directory where this file resides
			$complete_file_name = 'uploads/orders/' . $order_id . '/' . $file_name;
			if (is_file($complete_file_name)) @unlink($complete_file_name);
			$order->deleteFile($order_id, $file_name);
		}else{
		// directory where this file resides
			$complete_file_name = 'uploads/orders/'. $file_name;
			if (is_file($complete_file_name)) @unlink($complete_file_name);
		}
	}
	public function actionDeleteInvoice()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		// which order record the file relates to?
		$order_id = Yii::app()->input->post('order_id');
		$invoiceID = Yii::app()->input->post('invoice_id');
		$sql = "select file_name from order_invoices WHERE id =".$invoiceID;
        $invoiceReader = Yii::app()->db->createCommand($sql)->queryRow();
        $fileName = $invoiceReader['file_name'];
		$sql = "delete from order_invoices  WHERE id =".$invoiceID;
        Yii::app()->db->createCommand($sql)->execute();
		
		// directory where this file resides
		$complete_file_name = 'uploads/orders/invoices/' . $order_id . '/' . $fileName;
		if (is_file($complete_file_name)) @unlink($complete_file_name);		
	}

	public function actionExport()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$order = new Order();
		$order->export();
	}

}
