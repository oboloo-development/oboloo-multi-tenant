<?php

class AccountsController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('accounts/list'));
	}

	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';

		// get list of all accounts (with pagination of course)
		$account = new Account();
		$accounts = $account->getAccounts();

		// need data for various drop downs
		$view_data = array();
		$account_type = new AccountType();
		$view_data['account_types'] = $account_type->getAll();

		$department = new Department();
		$view_data['departments'] = $department->getAll(array('order' => 'department_name'));

		// display various accounts to the users
		$view_data['parent_accounts'] = $account->getData(array('parent_account_id' => 0, 'order' => 'account_name'));
		$view_data['accounts'] = $account->sortForDisplay($accounts);
		$this->render('list', $view_data);
	}

	public function actionSave()
	{
		$this->checklogin();
		$account = new Account();
		$account->saveData();
		echo $account->rs['account_id'];
	}

	public function actionGetParentAccounts()
	{
		$this->checklogin();
		$account = new Account();
		echo json_encode($account->getData(array('parent_account_id' => 0, 'order' => 'account_name')));
	}

}
