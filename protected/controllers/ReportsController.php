<?php

class ReportsController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'report_build';

		// and also currency preference by the user
		$user_currency = Yii::app()->input->post('user_currency');
		// get currency on the base of posted value
		$this->getCurrency($user_currency);

		// get orders as per filters applied
		$view_data = array();
		$location_id = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$order_status = Yii::app()->input->post('order_status');
		$vendor_id = Yii::app()->input->post('vendor_id');
		$vendor_name = Yii::app()->input->post('vendor_name');
				
		// parameters to run report for
		$from_date = Yii::app()->input->post('from_date');
		if (empty($from_date)) $from_date = date("01/01/Y");
		
		
		$to_date = Yii::app()->input->post('to_date');
		if (empty($to_date)) $to_date = date("d/m/Y");
		
		// dates in DB are in different format
		list($input_date, $input_month, $input_year) = explode("/", $from_date);
		$report_from_date = $input_year . '-' . $input_month . '-' . $input_date;
		
		list($input_date, $input_month, $input_year) = explode("/", $to_date);
		$report_to_date = $input_year . '-' . $input_month . '-' . $input_date;
		
		// and which dimensions to run it for
		$view_data = array();
		$report_type = Yii::app()->input->post('report_type');
		$view_data['report_type'] = $report_type;
		
		$dim_1 = Yii::app()->input->post('dim_1');
		$dim_2 = Yii::app()->input->post('dim_2');
		$dim_3 = Yii::app()->input->post('dim_3');
		$dim_4 = Yii::app()->input->post('dim_4');
		
		// need filters too
		$location = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;		
		$view_data['department_id'] = $department_id;

		// and get data as per selected filters
		$view_data['order_status'] = $order_status;
		$view_data['vendor_id'] = $vendor_id;
		$view_data['vendor_name'] = $vendor_name;
		$report = new Report();
		$report->vendor_id = $vendor_id;
		$report->location_id = $location_id;
		$report->department_id = $department_id;
		$report->order_status = $order_status;
		$view_data['report_data'] = $report->getReportTotals
			($report_type, $report_from_date, $report_to_date, $dim_1, $dim_2, $dim_3, $dim_4);

		// and display with values as selected
		$view_data['from_date'] = $from_date;
		$view_data['to_date'] = $to_date;
		$view_data['dim_1'] = $dim_1;
		$view_data['dim_2'] = $dim_2;
		$view_data['dim_3'] = $dim_3;
		$view_data['dim_4'] = $dim_4;
		$this->render('index', $view_data);
	}

	public function actionGetPivotChartData()
	{
		$this->checklogin();

		// which parameters do we need pivot chart for
		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');
		$dim_1 = Yii::app()->input->post('dim_1');
		$dim_2 = Yii::app()->input->post('dim_2');
		
		$dimension_1 = Yii::app()->input->post('dimension_1');
		if (empty($dimension_1)) $dimension_1 = array();
		$dimension_2 = Yii::app()->input->post('dimension_2');
		if (empty($dimension_2)) $dimension_2 = array();
		
		// dates in DB are in different format
		list($input_date, $input_month, $input_year) = explode("/", $from_date);
		$report_from_date = $input_year . '-' . $input_month . '-' . $input_date;
		
		list($input_date, $input_month, $input_year) = explode("/", $to_date);
		$report_to_date = $input_year . '-' . $input_month . '-' . $input_date;
		
		// first get all the data
		$report = new Report();
		$report_data = $report->getReportTotals("", $report_from_date, $report_to_date, $dim_1, $dim_2, "", "");
		
		// now retrieve only the needed records
		$dim_1_ids = $dim_2_ids = $dim_1_labels = $dim_2_labels = $pivot_data = array();
		foreach ($report_data as $report_row)
		{
			if (!isset($report_row['dim_1_id']) || !isset($report_row['dim_2_id'])) continue;
			
			$include_dim_1 = $include_dim_2 = false;
			if (count($dimension_1) <= 0 || in_array($report_row['dim_1_id'], $dimension_1))
				$include_dim_1 = true;
			if (count($dimension_2) <= 0 || in_array($report_row['dim_2_id'], $dimension_2))
				$include_dim_2 = true;
			
			if ($include_dim_1 && $include_dim_2) 
			{
				if (!in_array($report_row['dim_1'], $dim_1_labels))
					$dim_1_labels[] = addslashes($report_row['dim_1']);
				if (!in_array($report_row['dim_2'], $dim_2_labels))
					$dim_2_labels[] = addslashes($report_row['dim_2']);

				if (!in_array($report_row['dim_1_id'], $dim_1_ids))
					$dim_1_ids[] = $report_row['dim_1_id'];
				if (!in_array($report_row['dim_2_id'], $dim_2_ids))
					$dim_2_ids[] = $report_row['dim_2_id'];

				$row_key = $report_row['dim_1_id'] . '-' . $report_row['dim_2_id'];
				$pivot_data[$row_key] = $report_row;
			}
		}

		foreach ($dim_2_ids as $dim_2_id)
		{
			$current_dim_data = array();
			foreach ($dim_1_ids as $dim_1_id)
			{
				$row_key = $dim_1_id . '-' . $dim_2_id;
				if (!isset($pivot_data[$row_key]))
					$pivot_data[$row_key] = array('total' => 0); 
			}
		}

		uasort($pivot_data, function($a, $b) {
		    return $b['total'] - $a['total'];
		});
		
		$dim_1_ids = $dim_2_ids = $dim_1_labels = $dim_2_labels = array();
		foreach ($pivot_data as $report_row)
		{
			if (!isset($report_row['dim_1_id']) || !isset($report_row['dim_2_id'])) continue;
			
			$include_dim_1 = $include_dim_2 = false;
			if (count($dimension_1) <= 0 || in_array($report_row['dim_1_id'], $dimension_1))
				$include_dim_1 = true;
			if (count($dimension_2) <= 0 || in_array($report_row['dim_2_id'], $dimension_2))
				$include_dim_2 = true;
			
			if ($include_dim_1 && $include_dim_2) 
			{
				if (!in_array($report_row['dim_1'], $dim_1_labels))
					$dim_1_labels[] = addslashes($report_row['dim_1']);
				if (!in_array($report_row['dim_2'], $dim_2_labels))
					$dim_2_labels[] = addslashes($report_row['dim_2']);

				if (!in_array($report_row['dim_1_id'], $dim_1_ids))
					$dim_1_ids[] = $report_row['dim_1_id'];
				if (!in_array($report_row['dim_2_id'], $dim_2_ids))
					$dim_2_ids[] = $report_row['dim_2_id'];
			}
		}
		
		// format the records so that for every node data set point is defined
		$complete_data = array();
		foreach ($dim_2_ids as $dim_2_id)
		{
			$current_dim_data = array();
			foreach ($dim_1_ids as $dim_1_id)
			{
				$row_key = $dim_1_id . '-' . $dim_2_id;
				if (isset($pivot_data[$row_key])) 
					$current_dim_data[] = round($pivot_data[$row_key]['total'], 2);
				else $current_dim_data[] = 0;
			}
			$complete_data[] = $current_dim_data;
		}

		// and return as json array
		$return_data = array();
		$return_data['data'] = $complete_data;
		$return_data['main_labels'] = $dim_1_labels;
		$return_data['secondary_labels'] = $dim_2_labels;
		echo json_encode($return_data);
	}

	public function actionPivot()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';	
		$this->current_option = 'report_pivot';

		$location_id   = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$order_status  = Yii::app()->input->post('order_status');
		$vendor_id 	   = Yii::app()->input->post('vendor_id');

		// need filters too
		$view_data = array();
		$location  = new Location();
		$view_data['locations'] = $location->getAll(array('ORDER' => 'location_name'));
		$view_data['location_id'] = $location_id;
		$view_data['department_id'] = $department_id;

		$this->render('pivot', $view_data);
	}

	public function actionPivotSaving()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';	
		$this->current_option = 'report_pivot';



		$location_id  = Yii::app()->input->post('location_id');
		$department_id= Yii::app()->input->post('department_id');
		$order_status = Yii::app()->input->post('order_status');
		$vendor_id    = Yii::app()->input->post('vendor_id');
		$user_id 	  = Yii::app()->input->post('user_id');
		
		// need filters too
		$view_data = array();
		$user = new User();
		
		// @Logix Master Alam
		// 16/02/2023
		// Only those locations will show in the list, which gives access from users "Access Rights Table" columns
		// name locations => saving_edit='yes', saving_view='yes savings Mangament 

        if(Yii::app()->session['user_type'] !=4){
		  $sql = "select l.* from locations l INNER JOIN user_privilege up On l.location_id = up.location_id 
			where (up.saving_view='yes' or up.saving_edit='yes') and up.user_id=".Yii::app()->session['user_id']." 
			group by location_name ORDER by location_name asc ";
		}else{
		  $sql = "select * from locations order by location_name asc ";
		}
		
		$view_data['locations'] = Yii::app()->db->createCommand($sql)->queryAll();
		$view_data['location_id'] = $location_id;
		$view_data['department_id'] = $department_id;
		$view_data['customSubDomain'] = '';
		$customisedSubDomain = FunctionManager::savingSpecific();
		if(!empty($customisedSubDomain) && in_array('usg',FunctionManager::subdomains())){
		  if($customisedSubDomain === 'usg'){
			$view_data['customSubDomain'] = $customisedSubDomain;
			$this->render('pivot_saving_'.$customisedSubDomain, $view_data);
		  }else{
			$this->render('pivot_saving', $view_data);
		  }
		}else{
			$this->render('pivot_saving', $view_data);
		}
	}


	public function actionGetPivotData()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'report_pivot';

		// get orders as per filters applied
		$view_data = array();
		$location_id   = Yii::app()->input->post('location_id');
		$department_id = Yii::app()->input->post('department_id');
		$order_status  = Yii::app()->input->post('order_status');
		$vendor_id 	   = Yii::app()->input->post('vendor_id');
		$vendor_name   = Yii::app()->input->post('vendor_name');
				
		// parameters to run report for
		$from_date = Yii::app()->input->post('from_date');
		if (empty($from_date)) $from_date = date("01/01/Y");
		
		$to_date = Yii::app()->input->post('to_date');
		if (empty($to_date)) $to_date = date("d/m/Y");
		
		// dates in DB are in different format
		list($input_date, $input_month, $input_year) = explode("/", $from_date);
		$report_from_date = $input_year . '-' . $input_month . '-' . $input_date;
		
		list($input_date, $input_month, $input_year) = explode("/", $to_date);
		$report_to_date = $input_year . '-' . $input_month . '-' . $input_date;
				
		// and get data as per selected filters
		$view_data['location_id'] 	= $location_id;
		$view_data['department_id'] = $department_id;
		$view_data['order_status']  = $order_status;
		$view_data['vendor_id'] 	= $vendor_id;
		$view_data['vendor_name'] 	= $vendor_name;
		
		// and display with values as selected
		$view_data['from_date'] = $from_date;
		$view_data['to_date'] 	= $to_date;

		$report = new Report();
		$report->vendor_id 	   = $vendor_id;
		$report->location_id   = $location_id;
		$report->department_id = $department_id;
		$report->order_status  = $order_status;
		echo json_encode($report->getPivotData($report_from_date, $report_to_date));
	}

	public function actionGetPivotSavingData()
	{
		error_reporting(0);
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'report_pivot';

		// get orders as per filters applied
		$view_data = array();
		$location_id  = Yii::app()->input->post('location_id');
		$department_id= Yii::app()->input->post('department_id');
		$order_status = Yii::app()->input->post('order_status');
		$vendor_id 	  = Yii::app()->input->post('vendor_id');
		$vendor_name  = Yii::app()->input->post('vendor_name');
		$currency_id  = Yii::app()->input->post('currency_id');
		$user_id      = Yii::app()->input->post('user_id');
				
		// parameters to run report for
		/*$from_date = Yii::app()->input->post('from_date');
		if (empty($from_date)) $from_date = date("Y-m-d",strtotime("-1 years"));

		
		$to_date = Yii::app()->input->post('to_date');
		if (empty($to_date)) $to_date = date("Y-m-d",strtotime("+1 years"));*/
		
		// dates in DB are in different format
		list($input_date, $input_month, $input_year) = explode("/", $from_date);
		$report_from_date = $input_year . '-' . $input_month . '-' . $input_date;
		
		list($input_date, $input_month, $input_year) = explode("/", $to_date);
		$report_to_date = $input_year . '-' . $input_month . '-' . $input_date;
		if(isset($_POST['from_date']) && isset($_POST['to_date'])){
		if(FunctionManager::dateFormat()=="d/m/Y"){
			$report_from_date = date("Y-m-d", strtotime(strtr($_POST['from_date'], '/', '-')));
			$report_to_date   = date("Y-m-d", strtotime(strtr($_POST['to_date'], '/', '-')));
		}else{
	    	$report_from_date = date("Y-m-d", strtotime($_POST['from_date']));
	    	$report_to_date   = date("Y-m-d", strtotime($_POST['to_date']));
	    }}else{
	    	$report_from_date = date("Y-m-d",strtotime("-1 years"));
	    	$report_to_date   = date("Y-m-d",strtotime("+1 years"));
	    }
		
		$location_id_array = implode(',', $location_id);
		$location_ids = array_map('intval', explode(',', $location_id_array));
		
		$department_id_array = implode(',', $department_id);
		$department_ids = array_map('intval', explode(',', $department_id_array));
		
		// and get data as per selected filters
		$view_data['location_id']  = $location_ids;
		$view_data['department_id']= $department_ids;
		$view_data['order_status'] = $order_status;
		$view_data['vendor_id']	   = $vendor_id;
		$view_data['vendor_name']  = $vendor_name;
		
		// and display with values as selected
		$view_data['from_date'] = $from_date;
		$view_data['to_date'] 	= $to_date;

		$report = new Report();
		
		$report->vendor_id 	  = $vendor_id;
		$report->location_id  = $location_ids;
		$report->department_id= $department_ids;
		$report->order_status = $order_status;
		$report->currency_id  = $currency_id;
		$report->user_id 	  = $user_id;
		echo json_encode($report->getPivotSavingData($report_from_date, $report_to_date));
	}

	//
	public function actionGetPivotSavingDatausg()
	{ 
		error_reporting(0);
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'report_pivot';

		// get orders as per filters applied
		$view_data = array();
		$location_id  = Yii::app()->input->post('location_id');
		$department_id= Yii::app()->input->post('department_id');
		$order_status = Yii::app()->input->post('order_status');
		//$vendor_id  = Yii::app()->input->post('vendor_id');
		$vendor_name  = Yii::app()->input->post('vendor_name');
		$currency_id  = Yii::app()->input->post('currency_id');
		$user_id      = Yii::app()->input->post('user_id');

		// parameters to run report for
		$from_date = Yii::app()->input->post('from_date');
		if (empty($from_date)) $from_date = date("01/01/Y");
		
		$to_date = Yii::app()->input->post('to_date');
		if (empty($to_date)) $to_date = date("d/m/Y");
		
		// dates in DB are in different format
		/*list($input_date, $input_month, $input_year) = explode("/", $from_date);
		$report_from_date = $input_year . '-' . $input_month . '-' . $input_date;
		
		list($input_date, $input_month, $input_year) = explode("/", $to_date);
		$report_to_date = $input_year . '-' . $input_month . '-' . $input_date;*/

		if(!empty($_POST['from_date']) && empty($_POST['to_date'])){
		if(FunctionManager::dateFormat()=="d/m/Y"){
			$report_from_date = date("Y-m-d", strtotime(strtr($_POST['from_date'], '/', '-')));
			$report_to_date = date("Y-m-d", strtotime(strtr($_POST['to_date'], '/', '-')));
		}else{
	     	$report_from_date = date("Y-m-d", strtotime($_POST['from_date']));
	    	$report_to_date = date("Y-m-d", strtotime($_POST['to_date']));
	    }}else{
	    	$report_from_date = date("Y-m-d",strtotime("-4 years"));
	    	$report_to_date = date("Y-m-d",strtotime("+4 years"));
	    }
		
		// and get data as per selected filters
		$view_data['location_id']  = $location_id;
		$view_data['department_id']= $department_id;
		$view_data['order_status'] = $order_status;
		$view_data['vendor_id']    = $vendor_id;
		$view_data['vendor_name']  = $vendor_name;
		$view_data['user_id'] = $user_id;
		
		// and display with values as selected
		$view_data['from_date'] = $from_date;
		$view_data['to_date'] = $to_date;
		
		$report = new Report();
		
		$report->vendor_id 	  = $vendor_id;
		$report->location_id  = $location_id;
		$report->department_id= $department_id;
		$report->order_status = $order_status;
		$report->currency_id  = $currency_id;
		$report->user_id = $user_id;
		echo json_encode($report->getPivotSavingDatausg($report_from_date, $report_to_date));
	}	
}
