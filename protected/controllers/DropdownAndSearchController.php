<?php

/**
 * This is an abstract class (meaning it cannot be instanciated into an object)
 * and hence various controller classes are derived from this one. Because of this
 * reason, some commonly required methods such as checking for system being offline
 * and whether an user has logged in or not are abstracted in here. Hence any
 * of the derived classes then do not need to write these methods repeatedly.
 */
 
class DropdownAndSearchController extends CommonController
{
	public function actionGetCategory()
    {
        $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
        $categories = array();
        
        $category = new Category();
        $search_results = $category->getAll(array(
            'value LIKE' => '%' . $search_string . '%',
             'soft_deleted' => "0",
            'ORDER' => 'value'
        ));
        
        foreach ($search_results as $category_data)
            if ($category_data['id'] != 0)
                $categories[] = array(
                    "value" => $category_data['value'],
                    "data" => $category_data['id']
                );
        
        echo json_encode(array(
            'suggestions' => $categories
        ));
    }

    public function actionGetSubCategory()
    {
        $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
        $category_id = isset($_REQUEST['category_id']) ? $_REQUEST['category_id'] : "";
        if(!empty($category_id)){
        $categories = array();
        
         $category_id =$category_id;

        $category = new Subcategory();

        if(is_array($category_id)){
            $subCat = array(
            'value LIKE' => '%' . $search_string . '%',
            'soft_deleted' => "0",
            'ORDER' => 'value',
            'code in' => $category_id
        );
        }else{
             $subCat = array(
            'value LIKE' => '%' . $search_string . '%',
             'soft_deleted' => "0",
            'ORDER' => 'value',
            'code' => $category_id
        );
        }
        $search_results = $category->getAll($subCat);
        
        foreach ($search_results as $category_data)
            if ($category_data['id'] != 0)
                $categories[] = array(
                    "value" => $category_data['value'],
                    "data" => $category_data['id']
                );
        
        echo json_encode(array(
            'suggestions' => $categories
        ));
    	}
    }

    public function actionGetSupplier()
    {
        $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
        $vendors = array();
        
        $vendor = new Vendor();
        $search_results = $vendor->getAll(array(
            'vendor_name LIKE' => '%' . $search_string . '%',
            'member_type' =>0,
            'active' =>1,
            'ORDER' => 'vendor_name'
        ));
        
        foreach ($search_results as $vendor_data)
        if ($vendor_data['vendor_id'] != 0 && $vendor_data['vendor_archive']==null  || $vendor_data['vendor_archive']=='')
            $vendors[] = array(
                "value" => $vendor_data['vendor_name'],
                "data" => $vendor_data['vendor_id']
            );
        
        echo json_encode(array(
            'suggestions' => $vendors
        ));
    }

    /* Start: Vendor */
    public function actionGetVendors()
    {
        $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
        
        $text_field_name = "value";
        $id_field_name = "data";
        
        $select2_call = false;
        if (is_array($search_string)) 
        {
            $search_string = $search_string['term'];
            $select2_call = true;
            $text_field_name = "text";
            $id_field_name = "id";
        }

        $vendors = array();
        $condition = array('active' => 1,'member_type' => 0, 'vendor_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'vendor_name');

       if(isset($_REQUEST['action']) && in_array($_REQUEST['action'],array('createBySourcingEvaluationModal','quickcontractcreate'))){
           $condition['quick_created  NOT IN'] = array("Yes and Quick Sourcing Evaluation","Yes");
       }
        $vendor = new Vendor();
        $search_results = $vendor->getAll($condition);

        foreach ($search_results as $vendor_data){
            if($vendor_data['vendor_archive']==null  || $vendor_data['vendor_archive']==''){
                $vendors[] = array($text_field_name => $vendor_data['vendor_name'], $id_field_name => $vendor_data['vendor_id']);
            }
        }

        if ($select2_call) echo json_encode($vendors);
        else echo json_encode(array('suggestions' => $vendors));
    }
    /* End: Vendor */

}
