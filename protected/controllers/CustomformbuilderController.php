<?php
class CustomformbuilderController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('customformbuilder/list'));
	}
	
	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'list';

		$sql = "select * from questionnaires_forms order by id desc ";
		$questionnaires_forms = Yii::app()->db->createCommand($sql)->query()->readAll();

		$user = new User();
		$user_data = $user->getOne(array('user_id'=> Yii::app()->session['user_id']));
		$this->render('list',['form' => $questionnaires_forms, 'user_data' => $user_data]);
	}

	public function actionCreate(){
		$this->checklogin();
        $this->layout = 'main';
		$this->render('create');
	}

	public function actionSave()
	{
		$this->checklogin();
		$form = new QuestionnaireForm();
		$form->rs = array();
		$form->rs['id'] = 0;
		$form->rs['user_id']= Yii::app()->session['user_id'];
		$form->rs['username']= Yii::app()->session['full_name'];
		$form->rs['name'] = addslashes($_POST['formName']);
		$form->rs['created_datetime'] = date("Y-m-d");
		$form->rs['updated_datetime'] = null;
		$form->write();
		$formId = Yii::app()->db->lastInsertID;
		if(!empty($_POST['form'])){
		  foreach($_POST['form'] as $formObj) {
		   $question = new FormQuestion();
		   $question->rs = array();
		   $question->rs['id'] = 0;
		   $question->rs['form_id'] = $formId;
		   $question->rs['question'] = addslashes(json_encode($formObj, JSON_UNESCAPED_UNICODE));
		   $question->rs['created_datetime'] = date("Y-m-d");
		   $question->rs['updated_datetime'] = null;
		   $question->write();
		  }	
		}
		echo json_encode(["status" => 1]);exit;  
	}
	
	public function actionedit($id){
	  $this->checklogin(); 
	  $sql ='select id,name,user_id from questionnaires_forms where id='.$id;
	  $editFormReader = Yii::app()->db->createCommand($sql)->queryRow();

	  $user = new User();
	  $user_data = $user->getOne(array('user_id'=> Yii::app()->session['user_id']));
	  $this->render('edit',['editform' => $editFormReader, 'user_data' => $user_data]);
	}

	public function actionUpdate(){
		
		if(!empty($_POST['form_id']) || isset($_POST['form_id'])){	
		 $form = new QuestionnaireForm();
		 $form->rs = array();
		 $form->rs['id'] = $_POST['form_id'];
		 $form->rs['user_id'] = Yii::app()->session['user_id'];
		 $form->rs['username'] = Yii::app()->session['full_name'];
		 $form->rs['name'] = addslashes($_POST['formName']);
		 $form->rs['updated_datetime'] = date("Y-m-d");
		 $form->update();
		
		//  Update case first delete the this question against question then insert new rows below
		$sql = "delete from form_questions WHERE form_id =".$_POST['form_id'];
        $fileReader = Yii::app()->db->createCommand($sql)->execute();
		
		// form question one by one inserted 
		foreach($_POST['form'] as $formObj) {
			$question = new FormQuestion();
			$question->rs = array();
			$question->rs['id'] = 0;
			$question->rs['form_id'] = $_POST['form_id'];
			$question->rs['question'] = addslashes(json_encode($formObj, JSON_UNESCAPED_UNICODE));
			$question->rs['updated_datetime'] = date("Y-m-d");
			$question->write();
		}	
		echo json_encode(["status" => 1]);exit;  
	   }
	}

	
	public function actionDelete(){

		if($_POST['status'] == 'yes'){
		  $sql = "delete from questionnaires_forms where id =".$_POST['formId'];
		  Yii::app()->db->createCommand($sql)->execute();

		  $sql = "delete from form_questions where form_id =".$_POST['formId'];
		  Yii::app()->db->createCommand($sql)->execute();
		}

		echo json_encode(["uploaded_response" => 1]);
	}

	public function actionSaveFormClone(){
		$this->checklogin();
		$questionReader = Yii::app()->db->createCommand('SELECT question FROM form_questions where form_id='.$_POST['questionaire_id'])->queryAll();
		if(!empty($questionReader)){
		 $form = new QuestionnaireForm();
		 $form->rs = array();
		 $form->rs['id'] = 0;
		 $form->rs['user_id']= Yii::app()->session['user_id'];
		 $form->rs['username']= Yii::app()->session['full_name'];
		 $form->rs['name'] = addslashes($_POST['new_form_name']);
		 $form->rs['created_datetime'] = date("Y-m-d");
		 $form->rs['updated_datetime'] = null;
		 $form->write();
		 $formId = Yii::app()->db->lastInsertID;

		 foreach($questionReader as $formObj) {
			$question = new FormQuestion();
			$question->rs = array();
			$question->rs['id'] = 0;
			$question->rs['form_id'] = $formId;
			$question->rs['question'] = addslashes($formObj['question']);
			$question->rs['created_datetime'] = date("Y-m-d");
			$question->rs['updated_datetime'] = null;
			$question->write();
		 } 
		}

		if(!empty($questionReader)){
			Yii::app()->user->setFlash('success', '<strong>Questionnaires</strong> is empty.');
		}	
	 
	  Yii::app()->user->setFlash('success', '<strong>successfully</strong> inserted eSourcing Questionnaires.');
	  $this->redirect(AppUrl::bicesUrl('customformbuilder/list'));
	}

	public function actionListAjax(){
		error_reporting(0);
		set_time_limit(0);
		
		$user_id = "";
		if(!empty($_POST['user_id'])){
			$user_id = $_POST['user_id'];
		}
		$order_by = array();
		if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])) {
			$order_by = $_REQUEST['order'];
		}

		$search_for = "";
		if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value'])) {
			$search_for = $_REQUEST['search']['value'];
		}
		$formQues = new QuestionnaireForm();
		$questionnaires_forms = $formQues->getQuestionnaireTableSetting(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $user_id);
		
		$user = new User();
		$user_data = $user->getOne(array('user_id'=> Yii::app()->session['user_id']));

		$questionnaireArr = array();
		foreach ($questionnaires_forms as $formList)
		{   
			$delete = "";
			if((!empty($formList['user_id']) && $formList['user_id'] == $user_data['user_id']) || $user_data['admin_flag'] == 1){
				$delete = '<a href="javascript:void(0)" onclick="deleteForm('.$formList['id'].',\'yes\')" 
				class="btn btn-sm btn-danger delete_form_'.$formList['id'].'">
				<i class="fa fa-trash" aria-hidden="true"></i></a>';
			} else{
				$delete = '<a href="javascript:void(0)" onclick="deleteForm('.$formList['id'].',\'no\')" 
				class="btn btn-sm btn-danger delete_form_'.$formList['id'].'">
				<i class="fa fa-trash" aria-hidden="true"></i></a>';
			}

			$edit_link = '<a href="'.Yii::app()->createUrl('customformbuilder/edit/' . $formList['id']).'"><button class="btn btn-sm btn-success view-btn">Edit</button></a>';
			$current_data = array();
			$current_data[] = $edit_link;
			$current_data[] = '<span class="notranslate">'.$formList['name'].'</span>';
			$current_data[] = $formList['username'];
			$current_data[] = date("Y-m-d",strtotime($formList['created_datetime']));
			$current_data[] = date("Y-m-d",strtotime($formList['updated_datetime']));
			$current_data[] = $delete;
			$questionnaireArr[]  = $current_data;
		}

		$total_questionnaire = $questionnaires_forms = $formQues->getQuestionnaireTableSetting(0, 'none', 'none', $search_for, $order_by, $user_id);
		$total_questionnaire = count($total_questionnaire);
		$json_data = array(
			"draw"            => intval( $_REQUEST['draw'] ),
			"recordsTotal"    => $total_questionnaire,
			"recordsFiltered" => $total_questionnaire,
			"data"            => $questionnaireArr
		);
		echo json_encode($json_data);exit;  
	}

	
}
