<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');

class UsersUsgController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('usersUsg/list'));
	}

	public function actionList()
	{
		
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'users';
		
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(4)))
			$this->redirect(AppUrl::bicesUrl('home'));

		// get list of all vendors (with pagination of course)
		$user = new UserUsg();

		// and display
		$view_data = array();
		$view_data['users'] = $user->getUsers();
		$this->render('list', $view_data);
	}

	public function actionEdit($user_id = 0)
	{

		//CVarDumper::dump($_POST,10,1);die;
		// is someone already logged in?
		error_reporting(0);
		$this->checklogin();
        $this->layout = 'main';
        /* if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(4)) || (Yii::app()->session['admin_flag']==0 and $user_id == 0))
		   $this->redirect(AppUrl::bicesUrl('home'));*/
		  
		// if form was submitted ... first save the data to the database
		if (Yii::app()->input->post('form_submitted'))
		{ 
			Yii::app()->session['duplicate_username'] = "";
			unset(Yii::app()->session['duplicate_username']);
			
			Yii::app()->session['duplicate_email'] = "";
			unset(Yii::app()->session['duplicate_email']);

			Yii::app()->session['bad_password'] = "";
			unset(Yii::app()->session['bad_password']);

			$user_id = Yii::app()->input->post('user_id');
			$user_type = Yii::app()->input->post('user_type');
			$user_type_id = Yii::app()->input->post('user_type_id');
			$username = Yii::app()->input->post('username');
			$email = Yii::app()->input->post('email');
			$password = Yii::app()->input->post('password');
			$status =  null !==Yii::app()->input->post('status')?Yii::app()->input->post('status'):0;
			
			$user = new UserUsg();
			$old_user = $user->getUsers($user_id);
			$bad_password = "";
			/*if ($user_id == 0 || !empty($password))
				$bad_password = $user->isBadPassword($password);*/
			
			$duplicate_email = false;
			$user_data = $user->getOne(array('user_id !=' => $user_id, 'email' => $email));
			if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
				$duplicate_email = true;
			
			$duplicate_username = false;
			$user_data = $user->getOne(array('user_id !=' => $user_id, 'username' => $username));
			if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
				$duplicate_username = true;

			if ($duplicate_username) Yii::app()->session['duplicate_username'] = $username;
			if ($duplicate_email) Yii::app()->session['duplicate_email'] = $email;
			if (!empty($bad_password)) Yii::app()->session['bad_password'] = $bad_password;
			
			if (!$duplicate_email && !$duplicate_username && empty($bad_password)) {

			    //$user->saveData();
				Yii::app()->user->setFlash('success', "User info saved successfully.");
				// Send activation email
				
				if($old_user['status'] !=1 && isset($status) && !empty($status)){
					
					//$user->activateUser($username, $email,'');
				// Start: addtional user update amount for subscription
				if(strpos(Yii::app()->getBaseUrl(true),"multi-local") ){
					 $subdomain = 'bondoporaja';
					//$url = 'http://saas-local.com/saas_oct05/clients/updateAddtionalLicense';
				}else{
					//$url = 'https://oboloo.software/saas/clients/updateAddtionalLicense';
					$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
				}
				if(strpos(Yii::app()->getBaseUrl(true),"devoboloo")){
					$url = 'https://devoboloo.com/saas/clients/updateAddtionalLicense';
				}else{
					$url = 'https://oboloo.software/saas/clients/updateAddtionalLicense';
				}
				$postRequest = array(
				    'subdomain' => $subdomain,
				);
				$cURLConnection = curl_init($url);
				curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
				curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
				// Start: to remove https restriction for development server
				curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, FALSE);
				// End: to remove https restriction for development server
				$apiResponse = curl_exec($cURLConnection);
				// $err = curl_error($curl);
				curl_close($cURLConnection);

				// $apiResponse - available data from the API request
				$jsonArrayResponse  =  json_decode($apiResponse);

				// End: addtional user update amount for subscription
				
				if($jsonArrayResponse->response==1){
				   $_POST['status_payment'] = 0;
				   $user->saveData();

				   Yii::app()->user->setFlash('success', "User added successfully.");
				   $return = EmailManager::userAcivation($email, $username);
				   
			    }else{
			    	$_POST['status'] = 0;
				    /*$user->saveData();*/
			    	Yii::app()->user->setFlash('success', "Additional License refused, try again or contact to oboloo support team.");
			    }
			    // End adding Payment to next cycle and pro rata data amount
					//$return = EmailManager::userAcivation($email, $username);
				}else{
					
					 $user->saveData();
					if(empty($user_id)){
					 	Yii::app()->user->setFlash('success', "User added successfully.");
					}else{
						Yii::app()->user->setFlash('success', "User edited successfully.");
					}
 
				}
			}
			
			if(!empty($user_type_id) || $user_type==1 || $user_type==3 || $user_type==4){
				if(empty($user_id) && is_array($user->rs) && !empty($user->rs['user_id'])){
					$user_id = $user->rs['user_id'];
				}else if(!empty($user->rs['user_type_id'])){ 
					$user_type_id = $user->rs['user_type_id'];
				}
				$user = $user->getUsers($user_id);
				$_POST['user_id'] = $user_id;
				$_POST['user_type_id'] = $user_type_id;

				$user = new UserUsg();
				$user->saveUserPrivilege($_POST);
				
			}

			$this->redirect(AppUrl::bicesUrl('usersUsg/list'));
		}

		// need some drop down values like user types i.e. roles
		$view_data = array('user_id' => $user_id);
		$user_type = new UserType();
		$view_data['user_types'] = $user_type->getAll();
		// get data for the selected user
		$user = new UserUsg();
		$view_data['user'] = $user->getUsers($user_id,$softDeleted=2);
		$view_data['manager'] = $user->getUsers();

		// get data for the selected user ACL
		$user = new UserACL();
		$view_data['acl'] = $user->getUserACL($user_id);

		$userPrivilege = new UserPrivilege();
		$view_data['privilege'] = $userPrivilege->getUserPrivilege($user_id);
		
		// need some drop down values
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));
		$view_data['locations_departments'] = $location->getLocationAndDepartment(array('order' => 'location_id'));

		// and display
		$this->render('edit', $view_data);
	}


    public function actionUserSetup(){
        if(!empty($_POST['email'])){
			$fullNameArr = $_POST['fullname'];
			$emailArr = $_POST['email'];
			$userTypeArr = $_POST['user_type_id'];
			 
			foreach($fullNameArr as $key=>$nameValue){
				$user_id = 0;
				$user_type = $userTypeArr[$key];
				$email = $emailArr[$key];
				$userName = trim(explode("@",$email)[0]);
				$password = '';
				$status = 1;

				$user = new UserUsg();
				 
				$duplicate_email = false;
				$user_data = $user->getOne(array('email' => $email));
				if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id']){
					$duplicate_email = true;
				}
				
				$duplicate_username = false;
				$user_data = $user->getOne(array('username' => $userName));
				if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id']){
					$duplicate_username = true;
				}

				
				if(!$duplicate_email || !$duplicate_username) {
					$userObj = new UserUsg();
	                $userObj->rs = array();
	                $userObj->rs['user_id']   = 0;
	                $userObj->rs['full_name'] = $nameValue;
	                $userObj->rs['email']     = $email;
	                $userObj->rs['username']  = $userName;
	                $userObj->rs['password']    = $password;
	                $userObj->rs['status']      = $status;
	                $userObj->rs['user_type_id']     = $user_type; 
	                $userObj->rs['created_datetime'] = date("Y-m-d H:i");
	                $userObj->rs['updated_datetime'] = date("Y-m-d H:i");
	                $userObj->rs['status_payment']   = 0;
	                $userObj->write();
	                 
	                $user_id = $userObj->rs['user_id'];

					// Yii::app()->user->setFlash('success', "User(s) added successfully.");
					// Send activation email
					$return = EmailManager::userAcivation($email, $userName);
				}
				if($user_type==1 || $user_type==3 || $user_type==4){
					$user = $user->getUsers($user_id);
					$_POST['user_id'] = $user['user_id'];
					$_POST['user_type_id'] = $user['user_type_id'];

					$user = new UserUsg();
					$user->saveUserPrivilege($_POST);
				}
			}
			$this->redirect(AppUrl::bicesUrl('home'));
		
        }
    }


	public function actionDelete($user_id){
		$user = new UserUsg;
		/*if(Yii::app()->session['admin_flag']==1){*/
		if(strpos(Yii::app()->getBaseUrl(true),"multi-local.com") 
			|| strpos(Yii::app()->getBaseUrl(true),"local")){
			 $subdomain = 'oboloo1';
			//$url = 'http://saas-local.com/saas_oct05/clients/updateAddtionalLicense';
		}else{
			//$url = 'https://oboloo.software/saas/clients/updateAddtionalLicense';
			$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
		}
	 	//$url = 'https://oboloo.software/saas/clients/deleteAddtionalLicense';
	 	if(strpos(Yii::app()->getBaseUrl(true),"devoboloo")){
			$url = 'https://devoboloo.com/saas/clients/deleteAddtionalLicense';
		}else{
			$url = 'https://oboloo.software/saas/clients/deleteAddtionalLicense';
		}

		$postRequest = array(
		    'subdomain' => $subdomain,
		);

		$cURLConnection = curl_init($url);
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, FALSE);

		$apiResponse = curl_exec($cURLConnection);
		//$err = curl_error($curl);
		curl_close($cURLConnection);

		// $apiResponse - available data from the API request
		 $jsonArrayResponse  =  json_decode($apiResponse);
		 
		// End: addtional user update amount for subscription
		 
		if(isset($jsonArrayResponse) && ($jsonArrayResponse->success==1 || $jsonArrayResponse->success==true)){
		if($user->executeQuery("DELETE FROM users WHERE user_id = $user_id"))
			Yii::app()->user->setFlash('success', "User deleted");
		}else{
			Yii::app()->user->setFlash('success', "Problem, try again");
		}
		//}
		 
	 	$this->redirect(AppUrl::bicesUrl('usersUsg/list'));
	}
	
	public function actionGetUsers()
	{
	    $search_string = isset($_REQUEST['query']) ? $_REQUEST['query'] : "";
	    
	    $text_field_name = "value";
	    $position_field_name = "position";
	    $contact_field_name = "contact_phone";
	    $email_field_name = "contact_email";
	    $id_field_name = "data";
	    
	    $select2_call = false;
	    if (is_array($search_string))
	    {
	        $search_string = $search_string['term'];
	        $select2_call = true;
	        $text_field_name = "text";
	        $id_field_name = "id";
	        $position_field_name = "text";
	        $contact_field_name = "text";
	        $email_field_name = "text";
	    }
	    
	    $users = array();
	    
	    $user = new UserUsg();
	    $search_results = $user->getAll(array('status' => 1, 'admin_flag' => 0, 'full_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'full_name', 'limit' => 100));
	    
	    foreach ($search_results as $user_data)
	        $users[] = array($text_field_name => $user_data['full_name'], $id_field_name => $user_data['user_id'],$position_field_name=>$user_data['position'],$contact_field_name=>$user_data['contact_number'],$email_field_name =>$user_data['email']);
	        
	        if ($select2_call) echo json_encode($users);
	        else echo json_encode(array('suggestions' => $users));
	}

	public function actionCheckPermission()
	{
		$location_id = $_POST['location_id'];
		$department_id = $_POST['department_id'];
		$field_name = $_POST['field_name'];
		$userPer = new UserUsg;
		$statusArr="";
		if(isset(Yii::app()->session['user_id']) && !empty($location_id) && !empty($department_id)){
	      $appOrder = $userPer->checkPermission(Yii::app()->session['user_id'],$location_id,$department_id,$field_name);
	    if(isset($appOrder) && $appOrder[$field_name]=="yes"){
	    	$statusArr =  array('Pending','Submitted','Approved', 'Closed', 'Declined', 'Ordered - PO Not Sent To Supplier', 'Received','Paid', 'Cancelled', 'Invoice Received', 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier');;
	    }else{
	    	$statusArr =  array('Pending', 'Submitted');
	    }
		}
		echo json_encode($statusArr);

	}

	public function actionUserDeactivate(){
		 $user_status = $_POST['user_status']; // user id
		//echo $user_status; exit;
		$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
		  
		if(SubscriptionManager::getTrialStatus()==1){
			$sql  = 'update users SET status=0 where user_id='.$user_status; 
            $userReader = Yii::app()->db->createCommand($sql)->queryRow();
			Yii::app()->user->setFlash('danger', " Deactivate User successfully.");
		}else if(!empty($user_status)){
			
          	if(strpos(Yii::app()->getBaseUrl(true),"multi-local.com") 
			|| strpos(Yii::app()->getBaseUrl(true),"local")){
			 $subdomain = 'oboloo1';
			//$url = 'http://saas-local.com/saas_oct05/clients/updateAddtionalLicense';
		}else{
			//$url = 'https://oboloo.software/saas/clients/updateAddtionalLicense';
			$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
		}
			
		if(strpos(Yii::app()->getBaseUrl(true),"devoboloo")){
			$url = 'https://devoboloo.com/saas/clients/deleteAddtionalLicense';
		}else{
			$url = 'https://oboloo.software/saas/clients/deleteAddtionalLicense';
		}

		$postRequest = array(
		    'subdomain' => $subdomain,
		);
	     
		$cURLConnection = curl_init($url);
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, FALSE);

		$apiResponse = curl_exec($cURLConnection);
		//$err = curl_error($curl);
		curl_close($cURLConnection);

		// $apiResponse - available data from the API request
		 $jsonArrayResponse  =  json_decode($apiResponse);
		 

		// End: addtional user update amount for subscription
		 
		if(isset($jsonArrayResponse) && ($jsonArrayResponse->success==1 || $jsonArrayResponse->success==true)){
			 $sql  = 'update users SET status=0 where user_id='.$user_status; 
             $userReader = Yii::app()->db->createCommand($sql)->queryRow();
			Yii::app()->user->setFlash('danger', " Deactivate User successfully.");
		}else{
			Yii::app()->user->setFlash('success', "Problem, try again");
		}

		} 
		$this->redirect(AppUrl::bicesUrl('usersUsg/list'));
	}

	public function actionUserActivate(){
		$user_status = $_POST['user_status'];
		$user = new UserUsg();
		$old_user = $user->getUsers($user_status);
		 if($old_user['status'] !=1 && !empty($user_status)){	
	        if(strpos(Yii::app()->getBaseUrl(true),"multi-local.com") 
				|| strpos(Yii::app()->getBaseUrl(true),"local")){
				 $subdomain = 'bondoporaja';
				//$url = 'http://saas-local.com/saas_oct05/clients/updateAddtionalLicense';
			}else{
				//$url = 'https://oboloo.software/saas/clients/updateAddtionalLicense';
				$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
			}
			if(strpos(Yii::app()->getBaseUrl(true),"devoboloo")){
					$url = 'https://devoboloo.com/saas/clients/updateAddtionalLicense';
				}else{
					$url = 'https://oboloo.software/saas/clients/updateAddtionalLicense';
				}

				$postRequest = array(
				    'subdomain' => $subdomain,
				);
				$cURLConnection = curl_init($url);
				curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
				curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

				// Start: to remove https restriction for development server

				curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, FALSE);
				
				// End: to remove https restriction for development server

				$apiResponse = curl_exec($cURLConnection);
				// $err = curl_error($curl);
				curl_close($cURLConnection);

				// $apiResponse - available data from the API request
				$jsonArrayResponse  =  json_decode($apiResponse);

				// End: addtional user update amount for subscription
				 
			if($jsonArrayResponse->response==1){
					$sql  = 'update users SET status=1 where user_id='.$user_status; 
	             	$userReader = Yii::app()->db->createCommand($sql)->queryRow();

					Yii::app()->user->setFlash('success', " User Activate successfully.");
			}else{
				Yii::app()->user->setFlash('success', "Problem, try again");
			}

			// End adding Payment to next cycle and pro rata data amount
			$return = EmailManager::userAcivation($old_user['email'], $old_user['username']);
			}

		 $this->redirect(AppUrl::bicesUrl('usersUsg/list'));
	}


}
