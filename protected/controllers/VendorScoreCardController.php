<?php
class VendorScoreCardController extends CommonController
{
    public $layout = "main";
    public function actionIndex()
    {
        $this->redirect(AppUrl::bicesUrl('vendorScoreCard/list'));
    }

    public function actionList()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        // is someone already logged in?
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'vendor_scorecard_list';
        $view_data['scorecard'] = array();
        $this->render('list', $view_data);
    }

    public function actionListAjax()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'vendor_scorecard_list';

        set_time_limit(0);

        $user_id = "";
        if (!empty($_POST['user_id'])) {
            $user_id = $_POST['user_id'];
        }

        $archive_status = 0;
        if (!empty($_POST['archive_status'])) {
            $archive_status = $_POST['archive_status'] == 'archive' ? 1 : 0;
        }
        
        $order_by = array();
        if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])) {
            $order_by = $_REQUEST['order'];
        }

        $search_for = "";
        if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value'])) {
            $search_for = $_REQUEST['search']['value'];
        }
        $scoreCardData = new VendorScoringName();
        $scoreCards = $scoreCardData->getVendorScoreCardTableSetting(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $user_id, $archive_status);

        $user = new User();
        $user_data = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

        $scoreCardArr = array();
        foreach ($scoreCards as $scoreCard) {
            $archive = "";
            if ((!empty($scoreCard['user_id']) && $user_data['user_type_id'] == 4) && $archive_status == 0) {
                $archive = '<a href="javascript:void(0)" style="text-decoration: none;" onclick="archiveVendScoreCard(' . $scoreCard['id'] . ',\'yes\', 1)" 
				class="btn btn-sm btn-primary delete_form_' . $scoreCard['id'] . '">Archive</a>';
            }else if((!empty($scoreCard['user_id']) && $user_data['user_type_id'] != 4) && $archive_status == 0){
                $archive = '<a href="javascript:void(0)" style="text-decoration: none;  cursor: not-allowed;"
				class="btn btn-sm btn-primary">Archive</a>';
            }

            if ($user_data['user_type_id'] == 4 && $archive_status == 1) {
                $archive = '<a href="javascript:void(0)" style="text-decoration: none;" onclick="archiveVendScoreCard(' . $scoreCard['id'] . ',\'yes\', 0)" 
				class="btn btn-sm btn-primary delete_form_' . $scoreCard['id'] . '">Unarchive</a>';
            }else if((!empty($scoreCard['user_id']) && $user_data['user_type_id'] != 4) && $archive_status == 1){
                $archive = '<a href="javascript:void(0)" style="text-decoration: none; cursor: not-allowed;" 
				class="btn btn-sm btn-primary">Unarchive</a>';
            }

            $createdDatetime = $scoreCard['created_datetime'];
            $updatedDatetime = $scoreCard['updated_datetime'];
            // Check if the datetime values are not empty and are in a valid format
            if (!empty($createdDatetime) && $createdDatetime != '0000-00-00') {
                $createdDate = date(FunctionManager::dateFormat(), strtotime($scoreCard['created_datetime']));
            } else {
                $createdDate = "";
            }

            if (!empty($updatedDatetime) && $updatedDatetime != '0000-00-00') {
                $updatedDate = date(FunctionManager::dateFormat(), strtotime($scoreCard['updated_datetime']));
            } else {
                $updatedDate = "";
            }

            $edit_link = '<a href="' . Yii::app()->createUrl('vendorScoreCard/edit/' . $scoreCard['id']) . '"><button class="btn btn-sm btn-success view-btn">Edit</button></a>';
            $current_data = array();
            $current_data[] = $edit_link;
            $current_data[] = '<span class="notranslate">' . $scoreCard['name'] . '</span>';
            $current_data[] = '<span class="notranslate">' . $scoreCard['total_score'] . ' %</span>';
            $current_data[] = $scoreCard['username'];
            $current_data[] = $createdDate;
            $current_data[] = $updatedDate;
            $current_data[] = $archive;
            $scoreCardArr[]  = $current_data;
        }

        $total_scoreCard =  $scoreCardData->getVendorScoreCardTableSetting(0, 'none', 'none', $search_for, $order_by, $user_id,$archive_status);
        $total_scoreCard = count($total_scoreCard);
        $json_data = array(
            "draw"            => intval($_REQUEST['draw']),
            "recordsTotal"    => $total_scoreCard,
            "recordsFiltered" => $total_scoreCard,
            "data"            => $scoreCardArr
        );
        echo json_encode($json_data);
        exit;
    }

    public function actionDelete()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'vendor_scorecard_list';

        if ($_POST['status'] == 'yes') {
            
            $sql = "update vendor_scoring_name set status=".$_POST['archive']." where id =" . $_POST['id'];
            Yii::app()->db->createCommand($sql)->execute();

            // $sql = "update  vendor_scoring set status=1 where scorecard_id in(" . $_POST['id'] . ") ";
            // Yii::app()->db->createCommand($sql)->execute();
        }

        echo json_encode(["uploaded_response" => 1]);
    }

    public function actionDefaultValue()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'default_value';
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('vendorScoreCard/list'));


        // some cases need additional filters
        $additional_data_filter = Yii::app()->input->post('additional_data_filter');

        // same method can manage data for multiple data types
        $data_type = 'supplier_scoring_criteria'; //Yii::app()->input->post('data_type');

        // no need to persist parent values across data types
        $data_type_change = Yii::app()->input->post('data_type_change');

        // create proper data management class
        $settings = new VendorScoring();

        // if form was submitted ... first save the data to the database
        $score = 0;
        $form_submitted = Yii::app()->input->post('form_submitted');
        $scorecard = Yii::app()->input->post('scorecard');

        if ($form_submitted && $data_type && !empty($scorecard)) {

            $scoreCardName = new VendorScoringName();
            $scoreCardName->rs = array();
            $scoreCardName->rs['id'] = 0;
            $scoreCardName->rs['name'] = addslashes($scorecard);
            $scoreCardName->rs['user_id'] = Yii::app()->session['user_id'];
            $scoreCardName->rs['username'] = Yii::app()->session['full_name'];
            $scoreCardName->rs['created_datetime'] = date("Y-m-d");
            $scoreCardName->rs['updated_datetime'] = null;
            $scoreCardName->write();
            $scoreCardNameID = Yii::app()->db->lastInsertID;

            if ($settings) {

                foreach ($_POST['value'] as $key => $valueData) {
                    $settings->rs = array();
                    $settings->rs['id'] = 0;
                    $settings->rs['scorecard_id'] = $scoreCardNameID;
                    $settings->rs['value'] = addslashes($valueData);
                    $settings->rs['score'] = $_POST['score'][$key];

                    $settings->rs['user_id'] = Yii::app()->session['user_id'];
                    $settings->rs['username'] = Yii::app()->session['full_name'];
                    $settings->rs['created_datetime'] = date("Y-m-d");
                    $settings->rs['updated_datetime'] = null;
                    $settings->write();

                    $scoringID = Yii::app()->db->lastInsertID;
                    if(!empty($_POST['questions']) && is_array($_POST['questions'])) {
                     foreach ($_POST['questions'][$key] as $scoreIndex => $scoreName) {
                        $vendorScoring = new  vendorScoringQuestion();
                        $vendorScoring->rs = array();
                        $vendorScoring->rs['id'] = 0;
                        $vendorScoring->rs['user_id'] = Yii::app()->session['user_id'];
                        $vendorScoring->rs['username'] = Yii::app()->session['full_name'];
                        $vendorScoring->rs['scorecard_id'] = $scoreCardNameID;
                        $vendorScoring->rs['vendor_scoring_id'] = $scoringID;
                        $vendorScoring->rs['question']    = addslashes($scoreName);
                        $vendorScoring->rs['created_datetime'] = date("Y-m-d");
                        $vendorScoring->rs['updated_datetime'] = null;
                        $vendorScoring->write();
                     }
                    }

                    Yii::app()->user->setFlash('success', "Default Values added successfully.");
                }
            }

            Yii::app()->session['selected_data_type'] = $data_type;
            if ($additional_data_filter)
                Yii::app()->session['additional_data_filter'] = $additional_data_filter;
            $this->redirect(AppUrl::bicesUrl('vendorScoreCard/list'));
        }

        $view_data = array('data_type' => $data_type, 'data' => array(), 'additional_data_filter' => $additional_data_filter);

        if ($settings) {
            $view_data['data'] = '';
        }
        // and display
        $this->render('default_value', $view_data);
    }

    function actionEdit($id = 0)
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'default_value';
        
        // some cases need additional filters
        $additional_data_filter = Yii::app()->input->post('additional_data_filter');
        // same method can manage data for multiple data types
        $data_type = 'supplier_scoring_criteria';

        $settings = new VendorScoring();
        // get data for the selected data type
        $view_data = array('data_type' => $data_type, 'data' => array(), 'additional_data_filter' => $additional_data_filter);
        if ($settings) {
            $scoreCardName = new VendorScoringName();
            $scoreCardName = $scoreCardName->getOne(['id' =>  $id]);
            $view_data['scoreCardName'] = $scoreCardName['name'];
            $view_data['vendor_scoring_name_id'] = $scoreCardName['id'];
            $view_data['data'] = $settings->getAll(['scorecard_id' =>  $scoreCardName['id'], 'order' => 'id asc']);
        }

        // Chart Query
        $sql = " select score as s_valu, value as scoring_title from vendor_scoring where scorecard_id in(" . $scoreCardName['id'] . ") order by s_valu DESC ";
        $scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $pie_score_series = array();
        $pie_score_label = array();
        foreach ($scoreReader as $value) {
            if ($value['s_valu'] > 0) {
                $pie_score_series[] = intval($value['s_valu']);
                $pie_score_label[] = $value['scoring_title'];
            }
        }

        $view_data['pie_score_series'] = $pie_score_series;
        $view_data['pie_score_label'] = $pie_score_label;

        // and display
        $this->render('default_value_edit', $view_data);
    }

    public function actionUpdateDefaultValue()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'default_value';

        // some cases need additional filters
        $additional_data_filter = Yii::app()->input->post('additional_data_filter');
        if (empty($additional_data_filter) && isset(Yii::app()->session['additional_data_filter']) && !empty(Yii::app()->session['additional_data_filter'])) {
            $additional_data_filter = Yii::app()->session['additional_data_filter'];
            Yii::app()->session['additional_data_filter'] = "";
            unset(Yii::app()->session['additional_data_filter']);
        }
        // same method can manage data for multiple data types
        $data_type = 'supplier_scoring_criteria';
        if (empty($data_type) && isset(Yii::app()->session['selected_data_type']) && !empty(Yii::app()->session['selected_data_type'])) {
            $data_type = Yii::app()->session['selected_data_type'];
            Yii::app()->session['selected_data_type'] = "";
            unset(Yii::app()->session['selected_data_type']);
        }

        // no need to persist parent values across data types
        $data_type_change = Yii::app()->input->post('data_type_change');
        if ($data_type_change)
            $additional_data_filter = 0;

        if (!empty($_POST['vendor_scoring_name_id'])) {
            $scoreCardName = new VendorScoringName();
            $scoreCardName->rs = array();
            $scoreCardName->rs['id'] = $_POST['vendor_scoring_name_id'];
            $scoreCardName->rs['name'] = addslashes($_POST['scorecard']);
            $scoreCardName->rs['updated_datetime'] = date("Y-m-d");
            $scoreCardName->write();

            $sql = "delete from vendor_scoring where scorecard_id in(" . $_POST['vendor_scoring_name_id'] . ") ";
            Yii::app()->db->createCommand($sql)->execute();
        }

        // create proper data management class
        $settings = new VendorScoring();

        // if form was submitted ... first save the data to the database
        $score = 0;
        $form_submitted = Yii::app()->input->post('form_submitted');

        if ($form_submitted && $data_type) {
            if ($settings) {

                $total_rows = Yii::app()->input->post('total_rows');
                for ($i = 1; $i <= $total_rows; $i++) {
                    $id = Yii::app()->input->post('id_' . $i);
                    $value = Yii::app()->input->post('value_' . $i);
                    if ($data_type == 'contract_status') {
                        $statusCode = Yii::app()->input->post('statusCode_' . $i);
                    } else if ($data_type == 'supplier_scoring_criteria') {
                        $score = Yii::app()->input->post('score_' . $i);
                        if (empty($score)) {
                            $score = 0;
                        }
                    }
                    $deleted_flag = Yii::app()->input->post('deleted_flag_' . $i);

                    if (empty($id))
                        $id = 0;
                    if (empty($deleted_flag))
                        $deleted_flag = 0;

                    if ($deleted_flag)
                        $settings->delete(array('id' => $id));
                    else {
                        $value = rtrim(ltrim($value));
                        if (!empty($value)) {
                            $settings->rs = array();
                            $settings->rs['id'] = 0;
                            $settings->rs['value'] = addslashes($value);
                            $settings->rs['scorecard_id'] = $_POST['vendor_scoring_name_id'];
                            $settings->rs['user_id'] = Yii::app()->session['user_id'];
                            $settings->rs['username'] = Yii::app()->session['full_name'];
                            $settings->rs['created_datetime'] = date("Y-m-d");
                            if ($data_type == 'supplier_scoring_criteria') {
                                $settings->rs['score'] = $score;
                            }

                            $settings->write();
                            $settingsID = Yii::app()->db->lastInsertID;
                            //echo "<pre>"; print_r($_POST['questions'][$i]); echo "</pre>"; exit;
                        
                            if(isset($_POST['questions'][$i])) {
                             $sql = "delete from vendor_scoring_questions where scorecard_id in(" . $settingsID . ") ";
                             Yii::app()->db->createCommand($sql)->execute();
                              foreach ($_POST['questions'][$i] as $scoreIndex => $scoreName) {
                        
                                  $vendorScoring = new  vendorScoringQuestion();
                                  $vendorScoring->rs = array();
                                  $vendorScoring->rs['id'] = 0;
                                  $vendorScoring->rs['user_id'] = Yii::app()->session['user_id'];
                                  $vendorScoring->rs['username'] = Yii::app()->session['full_name'];
                                  $vendorScoring->rs['scorecard_id'] = $_POST['vendor_scoring_name_id'];
                                  $vendorScoring->rs['vendor_scoring_id'] = $settingsID;
                                  $vendorScoring->rs['question'] = addslashes($scoreName);
                                  $vendorScoring->rs['created_datetime'] = date("Y-m-d");
                                  $vendorScoring->rs['updated_datetime'] = null;
                                  $vendorScoring->write();
                            
                              }
                            }
                            Yii::app()->user->setFlash('success', "Default Values Updated successfully.");
                        }
                    }
                }
            }
            $this->redirect(AppUrl::bicesUrl('vendorScoreCard/list'));
        }
    }

    public function actionCreateVendorScoreCard()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';
    
        $scoreCardName = new VendorScoringName();
        $scoreCardName = $scoreCardName->getOne(array('id' => $_POST['scorecard_id'][0]));

        $vendorScore = new VendorScoreCard();
        $vendorScore->rs = array();
        $vendorScore->rs['id'] = 0;
        $vendorScore->rs['vendor_id'] = $_POST['vendor_id'];
        $vendorScore->rs['user_id'] = Yii::app()->session['user_id'];
        $vendorScore->rs['username'] = Yii::app()->session['full_name'];
        $vendorScore->rs['created_by'] = Yii::app()->session['full_name'];
        $vendorScore->rs['scorecard_id'] = $_POST['scorecard_id'][0];
        $vendorScore->rs['scorecard_name'] = $scoreCardName['name'];
        $vendorScore->rs['created_datetime'] = date("Y-m-d");
        $vendorScore->rs['updated_datetime'] = null;
        $vendorScore->write();
        $vendorScoreNameID = Yii::app()->db->lastInsertID;

        if (!empty($_POST['scorecard_id']) && $vendorScoreNameID) {
            foreach ($_POST['scorecard_id'] as $index => $scoreCardID) {
                $vendorScore = new VendorScoreCardSelected();
                $vendorScore->rs = array();
                $vendorScore->rs['id'] = 0;
                $vendorScore->rs['vendor_id'] = $_POST['vendor_id'];
                $vendorScore->rs['user_id'] = Yii::app()->session['user_id'];
                $vendorScore->rs['username'] = Yii::app()->session['full_name'];
                $vendorScore->rs['scorecard_id'] = $scoreCardID;
                $vendorScore->rs['vendor_score_card_id'] = $vendorScoreNameID;
                $vendorScore->rs['created_datetime'] = date("Y-m-d");
                $vendorScore->rs['updated_datetime'] = null;
                $vendorScore->write();

                foreach ($_POST['score_name'][$scoreCardID] as $scoreIndex => $scoreName) {
                    $vendScoreCardSelectEval = new  VendorScoreCardSelectedEvaluation();
                    $vendScoreCardSelectEval->rs = array();
                    $vendScoreCardSelectEval->rs['id'] = 0;
                    $vendScoreCardSelectEval->rs['vendor_id'] = $_POST['vendor_id'];
                    $vendScoreCardSelectEval->rs['user_id'] = Yii::app()->session['user_id'];
                    $vendScoreCardSelectEval->rs['username'] = Yii::app()->session['full_name'];
                    $vendScoreCardSelectEval->rs['scorecard_id'] = $scoreCardID;
                    $vendScoreCardSelectEval->rs['vendor_score_card_id'] = $vendorScoreNameID;
                    $vendScoreCardSelectEval->rs['category_area']  = $scoreName;
                    $vendScoreCardSelectEval->rs['default_score']    = $_POST['defualt_score'][$scoreCardID][$scoreIndex];
                    $vendScoreCardSelectEval->rs['created_datetime'] = date("Y-m-d");
                    $vendScoreCardSelectEval->rs['updated_datetime'] = null;
                    $vendScoreCardSelectEval->write();

                    $vendScoreCardSelectEvalID = Yii::app()->db->lastInsertID;
                    if ($vendScoreCardSelectEvalID) {
                        foreach ($_POST['questions'][$scoreCardID][$scoreIndex] as $key => $question) {
                            $vendSelectedQuestEval = new  VendScardSelectedQuestionEvaluation();
                            $vendSelectedQuestEval->rs = array();
                            $vendSelectedQuestEval->rs['id'] = 0;
                            $vendSelectedQuestEval->rs['vendor_id'] = $_POST['vendor_id'];
                            $vendSelectedQuestEval->rs['scorecard_id'] = $scoreCardID;
                            $vendSelectedQuestEval->rs['vend_score_card_id'] = $vendorScoreNameID;
                            $vendSelectedQuestEval->rs['vend_scorecard_select_eval_id'] = $vendScoreCardSelectEvalID;
                            $vendSelectedQuestEval->rs['question'] = addslashes($question);
                            $vendSelectedQuestEval->rs['question_notes'] = addslashes($_POST['question_notes'][$scoreCardID][$scoreIndex][$key]);
                            $vendSelectedQuestEval->rs['score_evaluation'] = $_POST['score_evaluation'][$scoreCardID][$scoreIndex][$key];
                            $vendSelectedQuestEval->rs['created_datetime'] = date("Y-m-d");
                            $vendSelectedQuestEval->rs['updated_datetime'] = null;
                            $vendSelectedQuestEval->write();
                        }
                    }
                }
            }
            Yii::app()->user->setFlash('success', 'Supplier Scorecard Record submitted successfully.');
            $this->redirect(AppUrl::bicesUrl('vendors/edit/' . $_POST['vendor_id'] . '?tab=scorecard'));
        } else {
            Yii::app()->user->setFlash('success', 'Supplier Scorecard Not submitted successfully.');
            $this->redirect(AppUrl::bicesUrl('vendors/edit/' . $_POST['vendor_id'] . '?tab=scorecard'));
        }
    }

    public function actionVendorScoreCardUpdate($id = 0)
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';


        if (!empty($id)) {
            $sql = "delete from vendor_score_card_selected where vendor_id=" . $_POST['vendor_id'] . " and vendor_score_card_id in(" . $id . ") ";
            Yii::app()->db->createCommand($sql)->execute();

            $sql = "delete from vendor_scorecard_selected_evaluation where vendor_id=" . $_POST['vendor_id'] . " and vendor_score_card_id =". $id;
            Yii::app()->db->createCommand($sql)->execute();

            $sql1 = "delete from vend_scard_selected_question_evaluation where vendor_id=" . $_POST['vendor_id'] . " and vend_score_card_id =".$id;
            Yii::app()->db->createCommand($sql1)->execute();

        }

        $vendorScore = new VendorScoreCard();
        $vendorScore->rs = array();
        $vendorScore->rs['id'] = $id;
        $vendorScore->rs['vendor_id'] = $_POST['vendor_id'];
        $vendorScore->rs['user_id'] = Yii::app()->session['user_id'];
        $vendorScore->rs['username'] = Yii::app()->session['full_name'];
        $vendorScore->rs['updated_by'] = Yii::app()->session['full_name'];
        $vendorScore->rs['updated_date'] = date("Y-m-d");
        $vendorScore->rs['updated_datetime'] = date("Y-m-d");
        $vendorScore->update();
        $vendorScoreCardId = $vendorScore->rs['id'];

        if (!empty($_POST['scorecard_id'])) {
            foreach ($_POST['scorecard_id'] as $index => $scoreCardID) {
                $vendorScore = new VendorScoreCardSelected();
                $vendorScore->rs = array();
                $vendorScore->rs['id'] = 0;
                $vendorScore->rs['vendor_id'] = $_POST['vendor_id'];
                $vendorScore->rs['user_id'] = Yii::app()->session['user_id'];
                $vendorScore->rs['username'] = Yii::app()->session['full_name'];
                $vendorScore->rs['scorecard_id'] = $scoreCardID;
                $vendorScore->rs['vendor_score_card_id'] = $vendorScoreCardId;
                $vendorScore->rs['updated_datetime'] = date("Y-m-d");
                $vendorScore->write();

                foreach ($_POST['score_name'][$scoreCardID] as $scoreIndex => $scoreName) {
                    $vendScoreCardSelectEval = new  VendorScoreCardSelectedEvaluation();
                    $vendScoreCardSelectEval->rs = array();
                    $vendScoreCardSelectEval->rs['id'] = 0;
                    $vendScoreCardSelectEval->rs['vendor_id'] = $_POST['vendor_id'];
                    $vendScoreCardSelectEval->rs['user_id'] = Yii::app()->session['user_id'];
                    $vendScoreCardSelectEval->rs['username'] = Yii::app()->session['full_name'];
                    $vendScoreCardSelectEval->rs['scorecard_id'] = $scoreCardID;
                    $vendScoreCardSelectEval->rs['vendor_score_card_id'] = $vendorScoreCardId;
                    $vendScoreCardSelectEval->rs['category_area']  = addslashes($scoreName);
                    $vendScoreCardSelectEval->rs['default_score']    = $_POST['defualt_score'][$scoreCardID][$scoreIndex];
                    $vendScoreCardSelectEval->rs['updated_datetime'] = date("Y-m-d");
                    $vendScoreCardSelectEval->write();
                    $vendScoreCardSelectEvalID = Yii::app()->db->lastInsertID;

                    // Now, let's add questions
                    if ($vendScoreCardSelectEvalID) {
                        foreach ($_POST['questions'][$scoreCardID][$scoreIndex] as $key => $question) {
                            $vendScardSelectedQuestionEvaluation = new VendScardSelectedQuestionEvaluation();
                            $vendScardSelectedQuestionEvaluation->rs = array();
                            $vendScardSelectedQuestionEvaluation->rs['id'] = 0;
                            $vendScardSelectedQuestionEvaluation->rs['vendor_id'] = $_POST['vendor_id'];
                            $vendScardSelectedQuestionEvaluation->rs['scorecard_id'] = $scoreCardID;
                            $vendScardSelectedQuestionEvaluation->rs['vend_score_card_id'] = $vendorScoreCardId;
                            $vendScardSelectedQuestionEvaluation->rs['vend_scorecard_select_eval_id'] = $vendScoreCardSelectEvalID;
                            $vendScardSelectedQuestionEvaluation->rs['question'] = addslashes($question);
                            $vendScardSelectedQuestionEvaluation->rs['question_notes'] = addslashes($_POST['question_notes'][$scoreCardID][$scoreIndex][$key]);
                            $vendScardSelectedQuestionEvaluation->rs['score_evaluation'] = $_POST['score_evaluation'][$scoreCardID][$scoreIndex][$key];
                            $vendScardSelectedQuestionEvaluation->rs['updated_datetime'] = date("Y-m-d");
                            $vendScardSelectedQuestionEvaluation->write();
                        }
                    }
                }
            }
        }
        Yii::app()->user->setFlash('success', 'Supplier Scorecard Record submitted successfully.');
        $this->redirect(AppUrl::bicesUrl('vendors/edit/' . $_POST['vendor_id'] . '?tab=scorecard'));
    }

    public function actionGetScoreCardList()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';

        $allScorecards = [];
        if (!empty($_POST['scorecardID'])) {
                $scorecdID = $_POST['scorecardID'];
                $scorecard = new VendorScoringName();
                $scorecardDetail = $scorecard->getOne(['id' => $scorecdID]);

                $scoreCardList = new VendorScoring();
                $scoreCardLists = $scoreCardList->getAll(['scorecard_id' => $scorecardDetail['id'], 'order' => 'id asc']);

                $allScorecards[] = [
                    'name' => $scorecardDetail['name'],
                    'card_id' => $scorecardDetail['id'],
                    'scoreCardLists' => $scoreCardLists
                ];
            }
    
        $view_data = ['allScorecards' => $allScorecards];

        $this->renderPartial('../vendors/__scorecard_selected_list', $view_data);
    }

    public function actionGetScoreCardEditList()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';

        $allScorecards = [];
        if (!empty($_POST['scorecardID'])) {

            if(is_array($_POST['scorecardID'])){
             foreach ($_POST['scorecardID'] as $scorecdID) {
            
                $vendorScorecard = new VendorScoreCard();
                $vendorScorecard = $vendorScorecard->getOne(['id' => $_POST['scoreCardId'], 'vendor_id' => $_POST['vendor_id'], 'order' => 'id asc']);

                $vendScoreCardSelectEval = new  VendorScoreCardSelectedEvaluation();
                $vendScoreCardSelectEval = $vendScoreCardSelectEval->getAll(['scorecard_id' => $vendorScorecard['scorecard_id'], 'vendor_score_card_id' => $vendorScorecard['id'], 'vendor_id' => $_POST['vendor_id'], 'order' => 'id asc']);

                $getQuestion = [];
                foreach ($vendScoreCardSelectEval as $list) {
                    $sql = "select * from vend_scard_selected_question_evaluation  
                        where vend_scorecard_select_eval_id=" . $list['id'] . " and  scorecard_id=" . $vendorScorecard['scorecard_id'] . " and vendor_id=" . $_POST['vendor_id'] . " order by id asc ";
                    $getQuestion[] = Yii::app()->db->createCommand($sql)->queryAll();
                }

                $allScorecards[] = [
                    'name' => $vendorScorecard['scorecard_name'],
                    'card_id' =>  $vendorScorecard['scorecard_id'],
                    'scoreCardLists' => $vendorScorecard,
                    'getQuestion' => $getQuestion,
                    'vendor_id' => $_POST['vendor_id']
                ];
            }

            }else{
                $scorecdID = $_POST['scorecardID'];
                $vendorScorecard = new VendorScoreCard();
                $vendorScorecard = $vendorScorecard->getOne(['id' => $_POST['scoreCardId'], 'vendor_id' => $_POST['vendor_id'], 'order' => 'id asc']);

                $vendScoreCardSelectEval = new  VendorScoreCardSelectedEvaluation();
                $vendScoreCardSelectEval = $vendScoreCardSelectEval->getAll(['scorecard_id' => $vendorScorecard['scorecard_id'], 'vendor_score_card_id' => $vendorScorecard['id'], 'vendor_id' => $_POST['vendor_id'], 'order' => 'id asc']);

                $getQuestion = [];
                foreach ($vendScoreCardSelectEval as $list) {
                    $sql = "select * from vend_scard_selected_question_evaluation  
                        where vend_scorecard_select_eval_id=" . $list['id'] . " and  scorecard_id=" . $vendorScorecard['scorecard_id'] . " and vendor_id=" . $_POST['vendor_id'] . " order by id asc ";
                    $getQuestion[] = Yii::app()->db->createCommand($sql)->queryAll();
                }

                $allScorecards[] = [
                    'name' => $vendorScorecard['scorecard_name'],
                    'card_id' =>  $vendorScorecard['scorecard_id'],
                    'scoreCardLists' => $vendScoreCardSelectEval,
                    'getQuestion' => $getQuestion,
                    'vendor_id' => $_POST['vendor_id']
                ];

            }
        }

        $scorecard = new VendorScoringName();
        $scorecardStatus = $scorecard->getOne(['id' => $_POST['scorecardID']]);
        
        $view_data = ['allScorecards' => $allScorecards, 'cardStatus' => $scorecardStatus['status'], 'id' => $_POST['scoreCardId']];
        $this->renderPartial('../vendors/__scorecard_selected_edit_list', $view_data);
        exit;
    }

    function actionScoreCadrDropdwon()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';

        $where = "WHERE 1";
        if (!empty($_POST['scorecardArr'])) {
            $where = "WHERE id NOT IN (" . implode(',', $_POST['scorecardArr']) . ")";
        }
        $query = "SELECT id, name FROM vendor_scoring_name {$where} ORDER BY name ASC";
        $scoreCard = Yii::app()->db->createCommand($query)->queryAll();

        $nameDropdown = '<option value="">Select Scorecard Name</option>';
        foreach ($scoreCard as $value) {
            $nameDropdown .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        echo json_encode(['nameDropdown' => $nameDropdown, 'selectedID' => '']);
        exit;
    }

    function actionVendorScoreCardListAjax()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'vendor_scorecard_list';
        set_time_limit(0);

        $user_id = "";
        if (!empty($_POST['user_id'])) {
            $user_id = $_POST['user_id'];
        }
        $user_id = "";
        if (!empty($_POST['user_id'])) {
            $user_id = $_POST['user_id'];
        }
        $order_by = array();
        if (isset($_REQUEST['order']) && is_array($_REQUEST['order'])) {
            $order_by = $_REQUEST['order'];
        }

        $search_for = "";
        if (isset($_REQUEST['search']) && is_array($_REQUEST['search']) && isset($_REQUEST['search']['value']) && !empty($_REQUEST['search']['value'])) {
            $search_for = $_REQUEST['search']['value'];
        }
        $scoreCardData = new VendorScoreCard();
        $scoreCards = $scoreCardData->getVendorScoreCardList(0, $_REQUEST['start'], $_REQUEST['length'], $search_for, $order_by, $user_id);

        $user = new User();
        $user_data = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

        $scoreCardArr = array();
        foreach ($scoreCards as $scoreCard) {
            $delete = "";
            if ((!empty($scoreCard['user_id']) && $scoreCard['user_id'] == $user_data['user_id']) || $user_data['admin_flag'] == 1) {
                $delete = '<a href="javascript:void(0)" onclick="deleteVendScoreCard(' . $scoreCard['id'] . ',\'yes\')" 
				class="btn btn-sm btn-danger delete_form_' . $scoreCard['id'] . '">
				<i class="fa fa-trash" aria-hidden="true"></i></a>';
            } else {
                $delete = '<a href="javascript:void(0)" onclick="deleteVendScoreCard(' . $scoreCard['id'] . ',\'no\')" 
				class="btn btn-sm btn-danger delete_form_' . $scoreCard['id'] . '">
				<i class="fa fa-trash" aria-hidden="true"></i></a>';
            }

            $createdDate = date(FunctionManager::dateFormat(), strtotime($scoreCard['created_datetime']));
            $updatedDate = date(FunctionManager::dateFormat(), strtotime($scoreCard['updated_datetime']));
            if ($createdDate == '01/01/1970') {
                $createdDate = "";
            }
            if ($updatedDate == '01/01/1970 00:00 AM' || $updatedDate == '30/11/-0001 00:00 AM') {
                $updatedDate = "";
            }
            $edit_link = '<a href="' . Yii::app()->createUrl('vendorScoreCard/vendorScoreCardEdit/' . $scoreCard['id']) . '"><button class="btn btn-sm btn-success view-btn">Edit</button></a>';
            $current_data = array();
            $current_data[] = $edit_link;
            $current_data[] = '<span class="notranslate">' . $scoreCard['quote_name'] . '</span>';
            $current_data[] = '<span class="notranslate">' . $scoreCard['notes'] . '</span>';
            $current_data[] = '<span class="notranslate">' . $scoreCard['score_card_name'] . '</span>';
            $current_data[] = '<span class="notranslate">' . $scoreCard['category_name'] . '</span>';
            $current_data[] = '<span class="notranslate">' . $scoreCard['subcategory_name'] . '</span>';
            $current_data[] = $scoreCard['username'];
            $current_data[] = $createdDate;
            $current_data[] = $updatedDate;
            $current_data[] = $delete;
            $scoreCardArr[]  = $current_data;
        }

        $total_scoreCard =  $scoreCardData->getVendorScoreCardList(0, 'none', 'none', $search_for, $order_by, $user_id);
        $total_scoreCard = count($total_scoreCard);
        $json_data = array(
            "draw"            => intval($_REQUEST['draw']),
            "recordsTotal"    => $total_scoreCard,
            "recordsFiltered" => $total_scoreCard,
            "data"            => $scoreCardArr
        );
        echo json_encode($json_data);
        exit;
    }

    public function actionVendScoreCardDelete()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';

        if ($_POST['id']) {
            $vendCardID = $_POST['id'];
            $sql = "delete from vendor_score_card where vendor_id=" . $_POST['vendor_id'] . " and id =" . $vendCardID;
            Yii::app()->db->createCommand($sql)->execute();

            $sql = "delete from vendor_score_card_selected where vendor_id=" . $_POST['vendor_id'] . " and vendor_score_card_id in(" . $vendCardID . ") ";
            Yii::app()->db->createCommand($sql)->execute();

            $sql = "select id from vendor_scorecard_selected_evaluation where vendor_id=" . $_POST['vendor_id'] . " and vendor_score_card_id in(" . $vendCardID . ") ";
            $res = Yii::app()->db->createCommand($sql)->queryAll();

            foreach($res as $deleteID){
             $sql = "delete from vend_scard_selected_question_evaluation where vendor_id=" . $_POST['vendor_id'] . " and vend_scorecard_select_eval_id in(" . $deleteID['id'] . ") ";
             Yii::app()->db->createCommand($sql)->execute();
            }

            $sql = "delete from vendor_scorecard_selected_evaluation where vendor_id=" . $_POST['vendor_id'] . " and vendor_score_card_id in(" . $vendCardID . ") ";
            Yii::app()->db->createCommand($sql)->execute();
        }

        $success = Yii::app()->user->setFlash('success', 'Supplier Scorecard deleted successfully.');
        $url =  AppUrl::bicesUrl('vendors/edit/' . $_POST['vendor_id'] . '?tab=scorecard');
        echo json_encode(["updateded_res" => 1, 'success' => $success, 'url' => $url]);
    }

    public function actionVendScoreCardArchive()
    {
        error_reporting(0);
        $this->checklogin();
        $this->layout = 'main';
        
        if ($_POST['id']) {
            $vendCardID = $_POST['id'];
            $sql = "update vendor_score_card set archive=".$_POST['status']." where vendor_id=" . $_POST['vendor_id'] . " and id =" . $vendCardID;
            Yii::app()->db->createCommand($sql)->execute();
        }

        $status = $_POST['status'] == 1 ? 'Archive' : 'Unarchive';
        $success = Yii::app()->user->setFlash('success', 'Supplier Scorecard '.$status.' successfully.');
        $url =  AppUrl::bicesUrl('vendors/edit/' . $_POST['vendor_id'] . '?tab=scorecard');
        echo json_encode(["updateded_res" => 1, 'success' => $success, 'url' => $url]);
    }

}
