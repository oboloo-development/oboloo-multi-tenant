<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');
/**
 * This class defines vatious user action methods such as new user registration,
 * login, logout, reset password and time out upon certain period of inactivity
 */
class AppController extends CommonController
{

    public $layout = 'main';

    /**
     * This method is used to redirect the user either to the login page or to the dashboard
     */
    public function actionIndex()
    {

        $this->redirect(AppUrl::bicesUrl('login'));
    }
    /**
     * This method is used to display the login form to the user. Also when the user
     * enters username and password on the form, the same method checks if such an user
     * with appropriate password (stored in encrypted format in the database) exists.
     * Next it performs various checks such as this is not an inactive account and also
     * that the user is not logging after a long time etc. If all these checks passed,
     * then the method sets up certain session variables and redirects the user to the
     * dashboard page. If any of the checks fail, then the user is redirected back to
     * the login page and appropriate error message is also then displayed to the user
     */
    public function actionError()
    {
    }

    public function actionLogin($error = 0)
    {
        error_reporting(0);
        $this->clearSession();
        // is someone already logged in?
        $requested_url = isset($_REQUEST['requested_url']) ? $_REQUEST['requested_url'] : '';
        $this->pageTitle = "Sign In";
        // $this->checklogin(0, false);
        $this->layout = 'main_login';

        // same screen has login and new user registration options
        $action_register = Yii::app()->input->post('action_register');
        $action_login = Yii::app()->input->post('action_login');

        // may be the location is already known?
        $view_data = array();
        $location = new Location();
        $view_data['locations'] = $location->getData(array('order' => 'location_name'));

        // either new user registration is being done
        if ($action_register) {
            // fields entered by the new user
            $fullname = Yii::app()->input->post('new_fullname');
            $username = Yii::app()->input->post('new_username');
            $password = Yii::app()->input->post('new_password');
            $email = Yii::app()->input->post('new_email');
            $location_id = Yii::app()->input->post('location_id');

            // create basic account
            $user = new User();
            $user->rs['full_name'] = $fullname;
            $user->rs['username'] = $username;
            $user->rs['password'] = md5($password);
            $user->rs['email'] = $email;
            $user->rs['location_id'] = $location_id;
            $user->write();
            $user->sendRegistrationEmail($password);

            // let them know that it will need to be activated
            $view_data['register_success'] = 1;
            $this->render('login', $view_data);
        }

        // or then an existing user is trying to login
        if ($action_login) {
            $user = new User();
            if (FunctionManager::checkEnvironment() && !empty($_POST['current_username']) && !empty($_POST['current_password'])) {
                $username = Yii::app()->input->post('current_username');
                $password = Yii::app()->input->post('current_password');
                // check if the user name and password match with database record
                $user_data = $user->loginWithoutEncode($username, $password);
                $fa = 'yes';
                $redirectUrl = AppUrl::bicesUrl('home');
            } else if (!empty($_POST['domain_request']) && $_POST['domain_request'] == "live") {
                $username = Yii::app()->input->post('username');
                $password = Yii::app()->input->post('password');
                $user_data = $user->loginWithoutEncode($username, $password);
                $redirectUrl = AppUrl::bicesUrl('home');
                $fa = 'yes';
            } else {
                $username = Yii::app()->input->post('username');
                $password = Yii::app()->input->post('password');
                // check if the user name and password match with database record
                $user_data = $user->login($username, $password);
                $redirectUrl = AppUrl::bicesUrl('app/verifyLogin');
                $fa = '';
            }/*else{
                $username = Yii::app()->input->post('username');
                $password = Yii::app()->input->post('password');
                $user_data = $user->login($username, $password);
                $redirectUrl = AppUrl::bicesUrl('app/verifyLogin');
                $fa = '';
            }*/
            // if(strpos(Yii::app()->getBaseUrl(true),"local")){ $fa = 'yes';$redirectUrl = AppUrl::bicesUrl('home');}


            if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id']) {
                if (isset($user_data['status']) && $user_data['status'] == 1) {
                    // set up session variables indicating successful login
                    Yii::app()->session['user_id'] = $user_data['user_id'];
                    Yii::app()->session['user_type'] = $user_data['user_type_id'];
                    Yii::app()->session['member_type'] = $user_data['member_type'];
                    Yii::app()->session['admin_flag'] = $user_data['admin_flag'];
                    Yii::app()->session['full_name'] = $user_data['full_name'];
                    Yii::app()->session['current_user_email'] = $user_data['email'];
                    Yii::app()->session['default_language_code'] = !empty($user_data['language_code']) ? $user_data['language_code'] : "en";
                    // also company information

                    Yii::app()->session['last_activity'] = time();
                    Yii::app()->session['expire_time'] = 30 * 60;
                    //your last activity was now, having logged in.
                    //$_SESSION['expire_time'] = 3*60*60; //expire time in seconds: three hours (you must     change this)

                    $company_data = new Company();
                    $company_details = $company_data->getOne();
                    // Yii::app()->session['company_name'] = $company_details['company_name'];
                    Yii::app()->session['company_number'] = $company_details['company_number'];

                    //Yii::app()->session['user_currency']         = $company_details['currency_id'];

                    Yii::app()->session['user_currency_dropdown'] = $company_details['currency_id'];
                    Yii::app()->session['user_currency']          = $company_details['currency_id'];
                    Yii::app()->session['default_date_format']    = $company_details['default_date_format'];

                    Yii::app()->session['default_time_format']    = $company_details['default_time_format'];
                    if (!FunctionManager::checkEnvironment()) {
                        Yii::app()->session['live_subdomain'] = Yii::app()->input->post('user_domain_name');
                    }

                    // may be the user wanted to go to a specific place
                    // if (!empty($requested_url))
                    // $this->redirect(AppUrl::bicesUrl($requested_url));
                    // else {
                    if ($fa == 'yes') {
                        Yii::app()->session['2fa'] = $fa;
                    }
                    $this->redirect($redirectUrl);
                    // }
                } else
                    $error = 2;
            } else
                $error = 1;

            $view_data['error'] = $error;
            $this->render('login', $view_data);
        }

        if (!$action_login && !$action_register) {
            $this->render('login', $view_data);
        }
    }

    public function actionIndexSetup()
    {

        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main_setup';
        $this->current_option = 'profile';
        $user_id = Yii::app()->session['user_id'];
        $form_submitted = Yii::app()->input->post('form_submitted');
        //echo "<pre>";print_r($_POST);exit;
        $subdomain = FunctionManager::subdomainByUrl();
        if ($form_submitted) {

            if (!empty($_FILES['profile_img']['name'])) {
                $img  = $_FILES['profile_img']['name'];
                $imgArr = explode(".", $img);
                $imgName = time() . '.' . end($imgArr);
                $path = Yii::getPathOfAlias('webroot') . '/images/' . $subdomain;
                if (!is_dir($path)) mkdir($path);
                $path = $path . '/users';
                if (!is_dir($path)) mkdir($path);
                if (!is_dir($path . '/' . APP_SUBDOMAIN_URL))
                    mkdir($path . '/' . APP_SUBDOMAIN_URL);
                if (!is_dir($path . '/' . $user_id))
                    mkdir($path . '/' . $user_id);
                if (!is_dir($path . '/'))
                    mkdir($path . '/');
                if (move_uploaded_file($_FILES['profile_img']['tmp_name'], $path . '/' . $user_id . '/' . $imgName)) {
                }
            } else {
                $imgName = '';
            }

            $user = new User();
            $user->rs = array();
            $user->rs['user_id'] = $user_id;
            $user->rs['language_code'] = Yii::app()->input->post('language_code');
            $user->rs['position'] = Yii::app()->input->post('position');
            $user->rs['contact_number'] = Yii::app()->input->post('contact_number');
            if (!empty($imgName)) {
                $user->rs['profile_img'] = $imgName;
            }
            $user->write();
            Yii::app()->session['default_language_code'] = $user->rs['language_code'];
        }
        $user = new User();
        $view_data['user'] = $user->getUsers($user_id, $softDeleted = 2);
        $location = new Location();
        $view_data['locations'] = $location->getData(array('order' => 'location_name'));
        $view_data['locations_departments'] = $location->getLocationAndDepartment(array('order' => 'location_id'));
        $this->render('system_setup', $view_data);
    }

    public function actionSetupUser()
    {
        $this->checklogin();
        $this->layout = 'main_user_setup';
        $user_type = new UserType();
        $view_data['user_types'] = $user_type->getAll();
        $this->render('new_user_setup', $view_data);
    }

    public function ActionModuleSetup()
    {

        $this->render('modules_setup');
    }


    public function actionVerifyLogin()
    {

        $this->layout = 'main_login';
        $view_data = array();
        $error_message = '';
        $user_id = Yii::app()->session['user_id'];
        $user = new User;
        $user_data = $user->getOne(array('user_id' => $user_id));
        if (strpos(Yii::app()->getBaseUrl(true), "multi-local")) {
            Yii::app()->session['2fa'] = 'yes';
            $this->redirect(AppUrl::bicesUrl('home'));
        }
        include(Yii::getPathOfAlias('ext.GoogleAuthenticator.GoogleAuthenticator') . '.php');
        $pga = new PHPGangsta_GoogleAuthenticator();
        if (empty($user_data['google_secret_code'])) {
            $secret = $pga->createSecret();
            $qr_code = $pga->getQRCodeGoogleUrl('oboloo', $secret);
            $view_data['qr_code'] = $qr_code;
            $view_data['secret'] = $secret;
        } else {
            $secret = $user_data['google_secret_code'];
            $view_data['secret'] = $secret;
        }
        $oneCode = $pga->getCode($secret);

        if (empty(Yii::app()->input->post('code'))) {
            $referesh = 0;
            if (isset($_GET['ref']) && $_GET['ref'] == 3) {
                $this->redirect(array('login'));
            } else
            if (!EmailManager::userCode($user_data['email'], $user_data['username'], $oneCode)) {
                //$error_message = 'Authentication code did not send, please, referesh the page.';
                if (empty($_GET['ref'])) {
                    $referesh = 1;
                } else if (!empty($_GET['ref'])) {
                    $referesh = $_GET['ref'];
                    $referesh++;
                }

                $this->redirect(array('verifyLogin', 'ref' => $referesh));
            }
        }



        // may be the location is already known?

        // or then an existing user is trying to login
        if (!empty(Yii::app()->input->post('code'))) {
            $code = Yii::app()->input->post('code');

            if (!empty(Yii::app()->input->post('google_secret_code'))) {
                $secreteCodeUser = Yii::app()->input->post('google_secret_code');
                $sql = 'update users set google_secret_code="' . $secreteCodeUser . '" where user_id=' . $user_id;
                Yii::app()->db->createCommand($sql)->execute();
            } else {
                $secreteCodeUser = $user_data['google_secret_code'];
            }

            if ($code == "") {
                $error_message = 'Please enter authentication code to validated!';
            } else {
                if ($pga->verifyCode($secreteCodeUser, $code)) {
                    // success
                    Yii::app()->session['2fa'] = 'yes';
                    $this->redirect(AppUrl::bicesUrl('home'));
                } else {
                    EmailManager::userCode($user_data['email'], $user_data['username'], $oneCode);
                    $error_message = 'Invalid Authentication Code!';
                }
            }
        }

        $view_data['user_data'] = $user_data;
        $view_data['user_data'] = $user_data;
        $view_data['error_message'] = $error_message;

        $this->render('verify_login', $view_data);
    }


    /**
     * This method logs the user out and clears out various session variables
     */
    public function actionLogout()
    {
        // clear session
        $this->clearSession();

        // and redirect to login page
        $this->redirect(AppUrl::bicesUrl('login'));
    }

    /**
     * This method displays the dashboard to the logged in user
     */
    public function actionHome()
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        // make sure some one is logged in
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'home';
        $year_start_date = date("2010-01-01");
        $current_date =  date("Y-m-d");
        $view_data = array();
        $quote = new Quote();
        $contract = new Contract();
        $view_data['expiring_contracts'] = $contract->getExpiringAndNoticePeriodContracts();
        $view_data['expiring_contracts_list'] = $contract->getExpiringAndNoticePeriodContractsList();
        $view_data['contracts_by_location'] = $contract->getContractLocationTotals($year_start_date, $current_date);
        $view_data['location_quote_count'] =  $quote->getLocationQuote();
        $vendor = new Vendor();
        $view_data['vendorStatistics'] = $vendor->getVendorStatistics();
        $notification = new Notification();
        $view_data['notifications'] = $notification->getLastest(10);

        $user = new User();
        $userData = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

        $view_data['home_visit'] = $userData['home_visit'];
        if ($userData['home_visit'] != 1) {
            $user->rs = array();
            $user->rs['user_id'] = $userData['user_id'];
            $user->rs['home_visit'] = 1;
            $user->write();
        }

        if (isset($_POST['report_from_date']) && isset($_POST['report_to_date'])) {
            if (FunctionManager::dateFormat() == "d/m/Y") {
                $chartDateFromMonth = date("Y-m-d", strtotime(strtr($_POST['report_from_date'], '/', '-')));
                $chartDateToMonth = date("Y-m-d", strtotime(strtr($_POST['report_to_date'], '/', '-')));
            } else {
                $chartDateFromMonth = date("Y-m-d", strtotime($_POST['report_from_date']));
                $chartDateToMonth = date("Y-m-d", strtotime($_POST['report_to_date']));
            }
        } else {
            $chartDateFromMonth = date("Y-m-d", strtotime("-6 months"));
            $chartDateToMonth = date("Y-m-d", strtotime("+6 months"));
        }

        $saving = new Saving();
        $chartListArr = $saving->getChartList($chartDateFromMonth, $chartDateToMonth);

        $view_data['chart_3'] = $chartListArr['chart_3'];


        $this->render('home', $view_data);
    }

    public function actionDashboard()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $this->redirect(array('home'));

        // make sure some one is logged in
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'dashboard';

        // the user may want to exclude tax from calculations :(
        $exclude_tax = Yii::app()->input->post('exclude_tax');
        if (empty($exclude_tax))
            $exclude_tax = 0;
        else
            $exclude_tax = 1;
        Yii::app()->session['exclude_tax_from_dashboard'] = $exclude_tax;

        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        // by default, show reports for year to date time frame
        $user_selected_year = Yii::app()->input->post('user_selected_year');

        if (empty($user_selected_year)) {
            if (isset(Yii::app()->session['user_selected_year']))
                $user_selected_year = Yii::app()->session['user_selected_year'];
            if (empty($user_selected_year))
                $user_selected_year = date("Y");
        }

        //$year_start_date =  "2018-01-01";
        //$current_date =  "2018-12-31";
        $year_start_date = $user_selected_year . "-01-01";
        $current_date = $user_selected_year . "-12-31";

        Yii::app()->session['user_selected_year'] = $user_selected_year;

        $current_page_url = Yii::app()->input->post('current_page_url');

        if (!empty($current_page_url) && strpos($current_page_url, 'dashboard') === false) {

            $this->redirect(AppUrl::bicesUrl(str_replace(Yii::app()->baseURL . '/', '', $current_page_url)));
        }


        // get top vendors, top locations and top categories data for the time being
        $view_data = array();
        $report = new Report();
        $view_data['top_vendors'] = $report->getReportTotals("", $year_start_date, $current_date, "V", "", "", "", 10);
        // $view_data['top_departments'] = $report->getReportTotals("", $year_start_date, $current_date, "D", "", "", "", 10);
        $view_data['top_departments'] = array();
        $view_data['top_locations'] = $report->getReportTotals("", $year_start_date, $current_date, "L", "", "", "", 10);
        $view_data['order_statuses'] = $report->getOrderStatusTotals($year_start_date, $current_date);

        // year over year data hopefully is available
        $view_data['year_over_year'] = $report->getReportTotals("", "2001-01-01", $current_date, "Y", "", "", "", 10);
        // also sort chronologically for bar chart
        $sort_by_year = array();
        foreach ($view_data['year_over_year'] as $row_key => $a_year_data) {
            if (isset($a_year_data['report_year']))
                $sort_by_year[$row_key] = $a_year_data['report_year'];
            else
                $sort_by_year[$row_key] = 9999;
        }
        array_multisort($sort_by_year, SORT_ASC, $view_data['year_over_year']);


        // need budget for each department also
        $departments = array();
        foreach ($view_data['top_departments'] as $report_row)
            if (isset($report_row['department_id']) && !in_array($report_row['department_id'], $departments))
                $departments[] = $report_row['department_id'];
        $budget = new Budget();
        $budgets = $budget->getDepartmentBudgets($departments);
        foreach ($view_data['top_departments'] as &$department_row) {
            $department_row['budget'] = 0;
            if (
                isset($department_row['department_id']) &&
                isset($budgets[$department_row['department_id']])
            )
                $department_row['budget'] = $budgets[$department_row['department_id']];
        }

        // year to date data
        $data_start_date = ($user_selected_year - 2);
        $view_data['current_year'] = $report->getReportTotals("", $data_start_date, $current_date, "M", "", "", "", 0);
        $view_data['current_year_start_date'] = $data_start_date;
        $view_data['selected_current_year'] = $user_selected_year;
        // display all months in case not found
        $current_year = $user_selected_year;
        $months_found = array();
        foreach ($view_data['current_year'] as $a_month_data)
            if (isset($a_month_data['report_month']))
                $months_found[] = $a_month_data['report_year'] . sprintf("%02d", $a_month_data['report_month']);
        for ($y = ($current_year - 2); $y <= $current_year; $y++) {
            for ($i = 1; $i <= 12; $i++) {
                $row_key = $y . sprintf("%02d", $i);
                if (!in_array($row_key, $months_found)) {
                    $zero_data = array(
                        'report_year' => $y, 'report_month' => $i, 'type' => 'O',
                        'total' => 0, 'percent' => 0, 'dim_2' => '', 'dim_3' => '', 'dim_4' => ''
                    );
                    $zero_data['dim_1'] = date("M Y", strtotime($y . '-' . sprintf("%02d", $i) . '-01'));
                    $view_data['current_year'][] = $zero_data;
                }
            }
        }

        // need to look
        if ($current_year == '2019') {
            // unset($view_data['current_year'][0]);
            //unset($view_data['current_year'][12]);
            //unset($view_data['current_year'][20]);
        }
        // also sort chronologically for bar chart
        $sort_by_month = array();
        foreach ($view_data['current_year'] as $row_key => $a_month_data) {
            if (isset($a_month_data['report_month']) && isset($a_month_data['report_year']))
                $sort_by_month[$row_key] = $a_month_data['report_year'] . sprintf("%02d", $a_month_data['report_month']);
            else
                $sort_by_month[$row_key] = 9999;
        }
        array_multisort($sort_by_month, SORT_ASC, $view_data['current_year']);

        // preferred vs. non preferred spending by category
        $preferred_vendors_used = $non_preferred_vendors_used = 0;
        $vendor = new Vendor();
        $view_data['vendors_combo'] = $vendor->getVendorsCountsAndAmounts($year_start_date, $current_date);
        $view_data['vendors_by_category'] = $vendor->getVendorsByCategory($year_start_date, $current_date);
        $view_data['category_vendors_combo'] = array();

        //CVarDumper::dump($view_data['vendors_by_category'],10,1);die;

        foreach ($view_data['vendors_by_category'] as $category_index => $category_data) {
            $view_data['category_vendors_combo'][$category_index] = array('vendor_count' => count($category_data));

            $total_for_category = 0;
            $category_name = "";
            foreach ($category_data as $category_vendor_row) {
                $total_for_category += $category_vendor_row['total'];
                $category_name = $category_vendor_row['category'];
                if ($category_vendor_row['preferred_flag'])
                    $preferred_vendors_used += 1;
                else
                    $non_preferred_vendors_used += 1;
            }

            $view_data['category_vendors_combo'][$category_index]['vendor_total'] = $total_for_category;
            $view_data['category_vendors_combo'][$category_index]['category'] = $category_name;
        }

        // and hence independent preferred vs. non preferred counts
        $total_vendors_used = $preferred_vendors_used + $non_preferred_vendors_used;
        if ($total_vendors_used) {
            $view_data['preferred_vendors_used'] = ($preferred_vendors_used * 100) / $total_vendors_used;
            $view_data['non_preferred_vendors_used'] = ($non_preferred_vendors_used * 100) / $total_vendors_used;
        } else
            $view_data['preferred_vendors_used'] = $view_data['non_preferred_vendors_used'] = 0;

        // even monthly category by vendor needs to be sorted chronologically
        $view_data['vendors_by_month'] = $vendor->getVendorsByMonth($year_start_date, $current_date);
        ksort($view_data['vendors_by_month']);

        $view_data['vendors_by_department'] = $vendor->getVendorsByDepartment($year_start_date, $current_date);
        $view_data['department_vendors_combo'] = array();
        /*
          foreach ($view_data['vendors_by_department'] as $department_index => $department_data)
          {
          $view_data['department_vendors_combo'][$department_index] = array( 'vendor_count' => count($department_data) );

          $total_for_department = 0;
          $department_name = "";
          foreach ($department_data as $department_vendor_row)
          {
          $total_for_department += $department_vendor_row['total'];
          $department_name = $department_vendor_row['department'];
          }

          $view_data['department_vendors_combo'][$department_index]['vendor_total'] = $total_for_department;
          $view_data['department_vendors_combo'][$department_index]['department'] = $department_name;
          }
         */

        // departments spending by category
        $department = new Department();
        $view_data['departments'] = $department->getData(array('order' => 'department_name'));
        // $view_data['departments_by_category'] = $department->getDepartmentsByCategory($year_start_date, $current_date);
        $view_data['departments_by_category'] = array();
        $view_data['departments_by_spend_type'] = $department->getLocationsBySpendType($year_start_date, $current_date);

        // purchase analysis e.g. invoices
        $view_data['invoices_paid_unpaid'] = $report->getInvoicePaymentStatistics($year_start_date, $current_date);
        $view_data['location_purchases'] = $report->getLocationSpendStatistics($year_start_date, $current_date);

        // contracts are now added to the system
        $contract = new Contract();
        $view_data['top_contracts'] = $contract->getTopContracts($year_start_date, $current_date);
        $view_data['contract_statuses'] = $contract->getContractStatusTotals($year_start_date, $current_date);
        $view_data['contracts_by_location'] = $contract->getContractLocationTotals($year_start_date, $current_date);
        $view_data['contracts_by_category'] = $contract->getContractCategoryTotals($year_start_date, $current_date);
        $view_data['expiring_contracts'] = $contract->getExpiringContracts();

        // various KPI or metrics for each tab displayed at the top
        $view_data['metrics'] = $this->getDashboardMetrics($year_start_date, $current_date);
        $view_data['order_count'] = $report->getOrderSpendCount($year_start_date, $current_date);
        $view_data['order_pending_count'] = $report->getOrderSpendPendingCount($year_start_date, $current_date);
        // and then display dashboard
        $location = new Location();
        $view_data['locations'] = $location->getData(array('order' => 'location_name'));

        // stats data for modal
        $view_data['order_spend_stats'] = $report->getOrderSpendStats($year_start_date, $current_date);
        $view_data['expense_spend_stats'] = $report->getExpenseSpendStats($year_start_date, $current_date);
        $view_data['order_spend_approve_stats'] = $report->getOrderToApproveStats($year_start_date, $current_date);
        $view_data['expense_spend_approve_stats'] = $report->getExpenseSpendToApproveStats($year_start_date, $current_date);
        $view_data['order_spend_paid_stats'] = $report->getOrderSpendToPaidStats($year_start_date, $current_date);
        $view_data['order_to_receive'] = $report->getOrderToReceive($year_start_date, $current_date);
        $view_data['order_spend_vendor_stats'] = $vendor->getTop10Vendors($year_start_date, $current_date);
        $view_data['vendor_used'] = $vendor->getSupplierUsed($year_start_date, $current_date);
        $view_data['vendor_nonprefered'] = $vendor->getSuppPreferNonPreferSpend($year_start_date, $current_date);
        $view_data['vendor_nonprefered_count'] = $vendor->getSuppPreferNonPreferSpend($year_start_date, $current_date, 0, "count");
        $view_data['order_count_spend_vendor_stats'] = $vendor->getVendorsForOrder($year_start_date, $current_date);
        $view_data['expense_count_spend_vendor_stats'] = $vendor->getVendorsForExpense($year_start_date, $current_date);
        $view_data['order_spend_non_preferred_vendor_stats'] = $vendor->getNonPreferredVendorsForOrder($year_start_date, $current_date);
        $view_data['expense_spend_non_preferred_vendor_stats'] = $vendor->getNonPreferredVendorsForExpense($year_start_date, $current_date);
        $view_data['order_spend_received_stats'] = $report->getOrderSpendToReceivedStats($year_start_date, $current_date);
        /* START: No need for future once all metrices are completed */
        $view_data['order_avg_suppliers_category_stats'] = $report->getOrderAvgSuppliersCategoryStats($year_start_date, $current_date);
        /* END: No need for future once all metrices are completed */
        $view_data['order_avg_suppliers_category'] = $report->getOrderAvgSuppliersCategory($year_start_date, $current_date);
        $view_data['contract_active_value_stats'] = $contract->getContractActiveValuesStats();
        $view_data['contract_expire_30days_stats'] = $contract->getContractExpire30DaysStats();
        $view_data['contract_expire_91days_stats'] = $contract->getContractExpire91DaysStats();
        $view_data['budget_total_stats'] = $budget->getTotalBudget($year_start_date);
        $view_data['budget_left_stats'] = $budget->getTotalBudgetLeft($year_start_date, $current_date);
        $view_data['budget_avg_stats'] = $budget->getAverageBudgetLeft($year_start_date, $current_date);
        $view_data['budget_over_stats'] = $budget->getOverBudget($year_start_date, $current_date);

        $view_data['budget_current_month_stats'] = $budget->getCurrentMonthBudgetLeft();



        $view_data['complete_view_data'] = $view_data;
        //CVarDumper::dump($view_data['spend_stats'],10,1);die;

        $this->render('dashboard', $view_data);
    }

    private function getDashboardMetrics($start_date, $end_date)
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $metrics = array();
        // spend tab
        $spend = new Report();
        $metrics['spend'] = $spend->getDashboardMetrics($start_date, $end_date);
        // vendor tab
        $vendor = new Vendor();
        $metrics['vendor'] = $vendor->getDashboardMetrics($start_date, $end_date);
        // department tab
        // $department = new Department();
        // $metrics['department'] = $department->getDashboardMetrics($start_date, $end_date);
        // contracts tab
        $contract = new Contract();
        $metrics['contract'] = $contract->getMetrics($start_date, $end_date);
        // vendor tab
        $budget = new Budget();
        $metrics['budget'] = $budget->getDashboardMetrics($start_date, $end_date);
        // return this for display on the main dashboard
        return $metrics;
    }

    /**
     * This method displays forgot password form to the user. The
     * same method is then used to send email containing a random
     * string to be used as one time use only password to the user
     */
    public function actionForgot()
    {
        // make sure some one is not logged in

        $this->pageTitle = "Forgot Password";
        $this->layout = 'main_login';
        $this->checklogin(0);

        // may be the form was submitted
        $view_data['success'] = $view_data['error'] = 0;
        $form_submitted = Yii::app()->input->post('form_submitted');
        if ($form_submitted) {
            $username = Yii::app()->input->post('username');
            $email = Yii::app()->input->post('email');
            //$user = new User();

            //$return = $user->forgot($username, $email);
            $return = EmailManager::userPasswordForgotReuest($email, $username);

            if ($return)
                $view_data['success'] = 1;
            else
                $view_data['error'] = 1;
        }

        // display forgot password form
        $this->render('forgot', $view_data);
    }

    public function actionChangePassword()
    {
        $this->layout = 'main_login';
        // make sure some one is not logged in
        $this->pageTitle = "Change Password";
        $this->checklogin(0);

        // may be the form was submitted
        $view_data['success'] = $view_data['error'] = 0;
        $form_submitted = Yii::app()->input->post('form_submitted');
        if ($form_submitted) {
            $username = Yii::app()->input->post('username');
            $email = Yii::app()->input->post('email');
            $user = new User();
            $return = EmailManager::userPasswordChangeReuest($email, $username/*,$this->renderPartial('/email_manager/password_change_request')*/);
            if ($return)
                $view_data['error'] = 1;
            else
                $view_data['success'] = 1;
        }

        // display forgot password form
        $this->render('change_password', $view_data);
    }

    public function actionChangePasswordSave()
    {
        // make sure some one is not logged in


        $this->pageTitle = "Change Password";
        $this->checklogin(0);

        $checkDate = date("m-d");
        $emailDate = base64_decode($_GET['time']);
        $user_id = base64_decode($_GET['user-id']);

        $user = new User;
        $user_data = $user->getOne(array('user_id' => $user_id));
        $userID = $user_data['user_id'];
        FunctionManager::userLanguage($userID);
        if ($checkDate == $emailDate) {
            $view_name = 'change_password_save';
            // may be the form was submitted
            $view_data['success'] = $view_data['error'] = 0;
            $form_submitted = Yii::app()->input->post('form_submitted');
            if ($form_submitted) {
                $current_pass = Yii::app()->input->post('password');
                $new_pass = Yii::app()->input->post('password_new');
                $new_pass_confirm = Yii::app()->input->post('password_new_confirm');

                if ($user_data['password'] != md5($current_pass)) {
                    $view_data['error'] = 2;
                } else if ($new_pass != $new_pass_confirm) {
                    $view_data['error'] = 3;
                } else if (strlen($new_pass) < 6) {
                    $view_data['error'] = 4;
                }

                if ($view_data['error'] == 0) {
                    $user->executeQuery("UPDATE users SET password = MD5('" . $new_pass . "') WHERE user_id = $user_id");
                    //$user = new User();
                    //$return = $user->changePasswordSave($user_data,$new_pass);
                    $return = EmailManager::userPasswordChanged($user_data['email'], $user_data['username'], $new_pass);
                    if (!$return)
                        $view_data['success'] = 1;
                    $view_name = 'change_password_alert';
                }
            }
        } else {
            $view_data['error'] = 1;
            $view_name = 'change_password_alert';
        }

        $this->render($view_name, $view_data);
    }


    /**
     * This method is used to validate new user registration information
     * such as username and email address. If the same username and email
     * address exists in the database, then such information is returned
     * in an array in json format since the method is called via AJAX. It
     * is also used to check if the captcha code entered is right or not
     */
    public function actionValidateRegistration()
    {
        $username = Yii::app()->input->post('username');
        $email = Yii::app()->input->post('email');
        $return_values = array();

        $user = new User();
        foreach (array('username', 'email') as $field) {
            $user_data = $user->getOne(array($field => $$field));
            if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
                $return_values[$field] = 1;
            else
                $return_values[$field] = 0;
        }

        echo json_encode($return_values);
    }

    public function actionValidatePassword()
    {
        $password = Yii::app()->input->post('password');
        $return_values = array();

        $user = new User();
        $return_values = $user->isBadPassword($password);
        echo json_encode($return_values);
    }

    /**
     * This method is used to validate if the information entered on
     * the forgot password form is correct or not. Username entered
     * must be available in the users database and also the captcha
     * code entered by the user should match what is in the session
     * It returns a json formatted array since it is called via AJAX
     */
    public function actionValidateForgot()
    {
        $username = Yii::app()->input->post('username');
        $return_values = array();

        $return_values['username'] = 1;
        $user = new User();
        $user_data = $user->getOne(array('username' => $username));
        if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
            $return_values['username'] = 0;

        echo json_encode($return_values);
    }

    public function actionCurrencyRates()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'currency_rates';

        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

        // if the form is submitted, save values
        $currencies = array('USD', 'EUR', 'JPY', 'INR', 'CHF', 'CAD');
        $form_submitted = Yii::app()->input->post();

        if (!empty($form_submitted)) {
            $symbolArr = $form_submitted['symbol'];
            unset($form_submitted['symbol']);

            //symbol
            foreach ($form_submitted as $currency => $currValue) {

                $command = Yii::app()->db->createCommand();
                $command->update('currency_rates', array(
                    'rate' => $currValue,
                    'currency_symbol' => $symbolArr[$currency],
                ), 'currency=:currency', array(':currency' => $currency));
                /*if (!empty($rate)) {
                    $currency_rate = new CurrencyRate();
                    $override_data['currency'] = 
                    $currency_rate->delete(array('currency' => $a_currency));
                    $currency_rate->rs = array();
                    $currency_rate->rs['id'] = 0;
                    $currency_rate->rs['currency'] = $a_currency;
                    $currency_rate->rs['rate'] = $rate;
                    $currency_rate->write();
                    $currency_rate->saveData($override_data);
                }*/
            }
        }

        // select existing rates
        $view_data = array();
        $rates = new CurrencyRate();
        $view_data['rates'] = $rates->getAll(array('ORDER' => 'currency'));
        $baseCurrency = Yii::app()->session['user_currency_dropdown'];
        $sql = "SELECT * FROM `currency_rates` where currency='" . $baseCurrency . "' and status='0' ";
        $baseCurrency = Yii::app()->db->createCommand($sql)->queryAll();
        $view_data['baseCurrency'] = $baseCurrency;

        // and display to the user
        $this->render('currency_rates', $view_data);
    }

    public function actionSaveCurrency()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $this->checklogin();
        if (!in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('home'));

        $currency = $_POST['currency'];
        $currencySymbol = $_POST['currency_symbol'];
        $country = $_POST['country'];

        $sql = " select * from  currency_rates WHERE country ='" . $country . "' and currency_symbol='" . $currencySymbol . "'";
        $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
        if (!empty($currencyReader['id'])) {
            $currencyID = $currencyReader['id'];
        } else {
            $currencyID = 0;
        }
        $userID   = Yii::app()->session['user_id'];
        $userName = Yii::app()->session['full_name'];


        // $rate = 1;//$_POST['rate'];
        $rate = $_POST['rate'];
        $rates = new CurrencyRate();
        $rates->rs = array();
        $rates->rs['id'] = $currencyID;
        $rates->rs['currency'] = $currency;
        $rates->rs['currency_symbol'] = $currencySymbol;
        $rates->rs['rate'] = $rate;
        $rates->rs['status'] = 0;
        $rates->rs['user_id'] = $userID;
        $rates->rs['user_name'] = $userName;
        $rates->rs['created_at'] = date("Y-m-d H:i:s");
        $rates->rs['updated_at'] = date("Y-m-d H:i:s");
        $rates->write();

        echo json_encode(array('currency_id' => $rates->rs['id']));
    }
    public function actionFillCurrencyField()
    {
        $country = $_POST['country'];
        $sql = " select * from  currency_rates WHERE country ='" . $country . "'";
        $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();

        $rates = new CurrencyRate();
        $data = array();
        if (!empty($currencyReader)) {
            $data['currency_name'] = $currencyReader['currency_name'];
            $data['currency_code'] = $currencyReader['currency'];
            $data['currency_symbol'] = $currencyReader['currency_symbol'];
            $data['rate'] = $currencyReader['rate'];
            $data['status'] = $currencyReader['status'];
        } else {
            $data['currency_name'] = '';
            $data['currency_code'] = '';
            $data['currency_symbol'] = '';
            $data['rate'] = '';
            $data['status'] = '';
        }
        echo json_encode($data);
    }
    public function actionEditCurrency()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $currencyID = $_POST['currency_id'];
        //$currency = $_POST['currency'];
        //$currencySymbol = $_POST['currency_symbol'];
        $rate = $_POST['rate'];

        $rates = new CurrencyRate();
        $rs = array();
        $rs['id'] = $currencyID;
        // $rs['currency'] = $currency;
        // $rs['currency_symbol'] = $currencySymbol;
        $userID   = Yii::app()->session['user_id'];
        $userName = Yii::app()->session['full_name'];
        $rs['rate'] = $rate;
        $rs['user_id'] = $userID;
        $rs['user_name'] = $userName;
        $rs['created_at'] = date("Y-m-d H:i:s");
        $rs['updated_at'] = date("Y-m-d H:i:s");
        $rates->saveData($rs);
        echo json_encode(array('msg' => 'Edited Successfully'));
    }

    public function actionDeleteConfigurationItem()
    {
        $tableName = '';
        $itemType = $_POST['selected_item_type'];
        $item = $_POST['selected_item'];

        if ($itemType == 'subcategory') {
            $sql = "update sub_categories set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'category') {
            $sql = "update categories set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "update sub_categories set soft_deleted=1 where code in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'subindustry') {
            $sql = "update sub_industries set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'industry') {
            $sql = "update industries set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "update sub_industries set soft_deleted=1 where code in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'payment_term') {
            $sql = "delete from payment_terms where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'shipping_method') {
            $sql = "delete from shipping_methods where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'shipping_term') {
            $sql = "delete from shipping_terms where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'saving_area') {
            $sql = "update saving_area set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'saving_business_unit') {
            $sql = "update saving_business_unit set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'saving_country') {
            $sql = "update saving_country set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'saving_country') {
            $sql = "update saving_country set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'material_type') {
            $sql = "update material_types set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'supplier_document_type') {
            $sql = "update vendor_document_type set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else if ($itemType == 'contract_document_type') {
            $sql = "update contract_document_type set soft_deleted=1 where id in(" . $item . ")";
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "Configuration item deleted successfully.";
            $status = 1;
        } else {
            $sql = "Configuration item did not delete contact system administrator.";
            $status = 0;
        }
        Yii::app()->user->setFlash('success', $sql);

        echo json_encode(array('status' => $status));
        exit;
    }
    public function actionDeleteCurrency()
    {
        $id = $_POST['id'];
        $sql = "update currency_rates set status='1' WHERE id =" . $id;
        $fileReader = Yii::app()->db->createCommand($sql)->execute();
        if (!empty($fileReader)) {
            echo json_encode(array('msg' => 'Currency deleted Successfully.'));
        } else {
            echo json_encode(array('msg' => 'Currency did not delete, try again.'));
        }
    }
    public function actionCurrencyRatesAjax()
    {
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

        $rates = new CurrencyRate();
        $view_data['rates'] = $rates->getAll(array('ORDER' => 'currency'));

        $requestData = $_REQUEST;
        $action = !empty($requestData['url_action']) ? $requestData['url_action'] : "";
        $search_value = $requestData['search']['value'];

        $columns = array(
            // datatable column index  => database column name
            // 0=>'id',
            0 => 'id',
            1 => 'country',
            2 => 'currency_name',
            3 => 'currency',
            4 => 'currency_symbol',
            5 => 'rate',
            6 => 'updated_at',
            7 => 'user_name',
            //8=>''
        );

        $baseCurrency = Yii::app()->session['user_currency_dropdown'];

        $sql = "SELECT * FROM `currency_rates` where currency !='" . $baseCurrency . "' and rate>0 and status='0' and country not in('United States','Euro Member','Japan','United Kingdom','Australia','Canada')";

        $sqlTopCurrency = "SELECT * FROM `currency_rates` where currency !='" . $baseCurrency . "' and rate>0 and status='0' and country in('United States','Euro Member','Japan','United Kingdom','Australia','Canada')";


        /*if search is applicable*/
        if (!empty($search_value)) {
            $sql .= ' and (country like "%' . $search_value . '%"';
            $sql .= ' OR currency_name like "%' . $search_value . '%"';
            $sql .= ' OR currency like "%' . $search_value . '%"';
            $sql .= ' OR user_name like "%' . $search_value . '%"';
            $sql .= ' OR currency_symbol like "%' . $search_value . '%"';
            $sql .= ' OR created_at like "%' . $search_value . '%"';
            $sql .= ' OR rate like "%' . $search_value . '%")';

            $sqlTopCurrency .= ' and (country like "%' . $search_value . '%"';
            $sqlTopCurrency .= ' OR currency_name like "%' . $search_value . '%"';
            $sqlTopCurrency .= ' OR currency like "%' . $search_value . '%"';
            $sqlTopCurrency .= ' OR user_name like "%' . $search_value . '%"';
            $sqlTopCurrency .= ' OR currency_symbol like "%' . $search_value . '%"';
            $sqlTopCurrency .= ' OR created_at like "%' . $search_value . '%"';
            $sqlTopCurrency .= ' OR rate like "%' . $search_value . '%")';
        }
        /*search ends.*/
        $endLimit = $requestData['length'];

        if (empty($requestData['order'][0]['column'])) {
            /*$requestData['order'][0]['column'] = 0;
            $requestData['order'][0]['dir'] = 'desc';*/
        }

        $order = $columns[$requestData['order'][0]['column']];
        //$order = "p.product_id DESC"; 
        $order = " ORDER BY " . $order . " " . $requestData['order'][0]['dir'];

        $sql .= $order;

        $sql .= " LIMIT " . $requestData['start'] . " ," . $endLimit;
        $projects = Yii::app()->db->createCommand($sql)->queryAll();
        $sqlTopCurrencyReader = Yii::app()->db->createCommand($sqlTopCurrency)->queryAll();
        $totalData = count($projects) + count($sqlTopCurrencyReader);

        // when there is no search parameter then total number 

        $data = [];


        if (!empty($sqlTopCurrencyReader)) {
            foreach ($sqlTopCurrencyReader as $proValue) {
                $nestedData = [];

                $currencyID = $proValue['id'];
                $country = $proValue['country'];
                $currencyName = $proValue['currency_name'];
                $userName = $proValue['user_name'];
                $currency = $proValue['currency'];
                $currencySymbol = $proValue['currency_symbol'];
                $rate = $proValue['rate'];
                $baseCurrency =  !empty(Yii::app()->session['user_currency_dropdown']) && Yii::app()->session['user_currency_dropdown'] == $currency ? '<input type="checkbox" checked disabled /> <span class="baseCurrncy"> Base Currency</span>' : '';

                $disabled = FunctionManager::sandbox();
                $popUpID = "curr" . $currencyID;
                $formID = "edit" . $currencyID;
                $actionLink = '<a data-toggle="modal" data-target="#' . $popUpID . '" href="#" class="btn btn-sm btn-success view-btn" style="text-decoration:none">Edit</a><a  href="#" ' . $disabled . ' class="btn btn-sm btn-danger "   style="text-decoration:none" onclick="deleteCurrency(' . $currencyID . ')">Delete</a>&nbsp;&nbsp;&nbsp;';


                $infoModal = '<div id="' . $popUpID . '" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Currency Overview</h4></div>
                    <div class="modal-body"><form id="' . $formID . '" action="" method="post">

                    <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Country  <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="country" id="country" value="' . $country . '" required="required" readonly />
                     </div></div><div class="clearfix"></div>

                    <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency Name <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="currency_name" id="currency_name"   value="' . $currencyName . '" required="required" readonly />
                     </div></div><div class="clearfix"></div>


                     <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency Code <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="currency" id="currency" placeholder="Currency Name" value="' . $currency . '" required="required" readonly />
                    <input type="hidden"  name="currency_id" id="currency_id"value="' . $currencyID . '"  /></div></div><div class="clearfix"></div>

                    <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="currency_symbol" id="currency_symbol" placeholder="Currency Symbol" value="' . $currencySymbol . '" required="required" readonly />
                    </div></div><div class="clearfix"></div>


                     <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Exchange Rate <span style="color: #a94442;">*</span></label>
                    <input type="number" step="0.01" class="form-control" name="rate" id="rate" placeholder="Currency Symbol" value="' . $rate . '" required="required"  />
                    </div></div>

                    ';

                /*  <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency Rate<span style="color: #a94442;">*</span></label>
                   //  <input type="text" class="form-control" name="rate" id="rate" placeholder="Currency Rate" value="'.$rate.'" required="required" />
                   // </div></div>*/

                $infoModal .= '<div class="clearfix"></div>
                   </div></form>
                    <div class="edit_msg_area"></div>
                    <div class="modal-footer"><button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button> <button  type="button" ' . $disabled . ' class="btn green-btn btn sm" onclick="editCurrency(' . $formID . ');">Save</button></div></div</div></div>style
                    <style>
                    .control-label {min-width: 22% !important;}
                    .form-control {width: 50%  !important;}
                    .form-group {margin-bottom: 14px !important; width: 100%;}
                    </style>

                    ';


                //$nestedData[]='<span class="notranslate"> '.$currencyID.'</span>';
                $nestedData[] = '<span class="notranslate"> ' . $country . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currencyName . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currency . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currencySymbol . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $rate . '</span>';
                $nestedData[] = $proValue['updated_at'] != "0000-00-00 00:00:00" ? date(FunctionManager::dateFormat(), strtotime($proValue['updated_at'])) : '';
                $nestedData[] = '<span class="notranslate"> ' . $userName . '</span>';
                $nestedData[] = $actionLink . $infoModal;
                /*$nestedData[]='<span class="notranslate"> '.$baseCurrency.'</span>';*/
                //$nestedData[]='<span class="notranslate"> '.$baseCurrency.'</span>';
                // $nestedData[]=$rate;

                $data[] = $nestedData;
            }
        }

        if (!empty($projects)) {
            foreach ($projects as $proValue) {
                $nestedData = [];

                $currencyID = $proValue['id'];
                $country = $proValue['country'];
                $currencyName = $proValue['currency_name'];
                $userName = $proValue['user_name'];
                $currency = $proValue['currency'];
                $currencySymbol = $proValue['currency_symbol'];
                $rate = $proValue['rate'];
                $baseCurrency =  !empty(Yii::app()->session['user_currency_dropdown']) && Yii::app()->session['user_currency_dropdown'] == $currency ? '<input type="checkbox" checked disabled /> <span class="baseCurrncy"> Base Currency</span>' : '';


                $popUpID = "curr" . $currencyID;
                $formID = "edit" . $currencyID;
                $actionLink = '<a data-toggle="modal" data-target="#' . $popUpID . '" href="#" class="btn btn-sm btn-success view-btn" style="text-decoration:none">Edit</a><a  href="#" class="btn btn-sm btn-danger " style="text-decoration:none" onclick="deleteCurrency(' . $currencyID . ')">Delete</a>&nbsp;&nbsp;&nbsp;';


                $infoModal = '<div id="' . $popUpID . '" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Currency Overview</h4></div>
                    <div class="modal-body"><form id="' . $formID . '" action="" method="post">

                    <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Country  <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="country" id="country" value="' . $country . '" required="required" readonly />
                     </div></div><div class="clearfix"></div>

                    <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency Name <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="currency_name" id="currency_name"   value="' . $currencyName . '" required="required" readonly />
                     </div></div><div class="clearfix"></div>


                     <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency Code <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="currency" id="currency" placeholder="Currency Name" value="' . $currency . '" required="required" readonly />
                    <input type="hidden"  name="currency_id" id="currency_id"value="' . $currencyID . '"  /></div></div><div class="clearfix"></div>

                    <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="currency_symbol" id="currency_symbol" placeholder="Currency Symbol" value="' . $currencySymbol . '" required="required" readonly />
                    </div></div><div class="clearfix"></div>


                     <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="rate" id="rate" placeholder="Currency Symbol" value="' . $rate . '" required="required"  />
                    </div></div>

                    ';

                /*  <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Currency Rate<span style="color: #a94442;">*</span></label>
                   //  <input type="text" class="form-control" name="rate" id="rate" placeholder="Currency Rate" value="'.$rate.'" required="required" />
                   // </div></div>*/

                $infoModal .= '<div class="clearfix"></div>
                   </div></form>
                    <div class="edit_msg_area"></div>
                    <div class="modal-footer"><button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button> <button  type="button" class="btn btn-primary btn sm" onclick="editCurrency(' . $formID . ');">Edit</button></div></div</div></div>style
                    <style>
                    .control-label {min-width: 22% !important;}
                    .form-control {width: 50%  !important;}
                    .form-group {margin-bottom: 14px !important; width: 100%;}
                    </style>

                    ';


                //$nestedData[]='<span class="notranslate"> '.$currencyID.'</span>';
                $nestedData[] = '<span class="notranslate"> ' . $country . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currencyName . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currency . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currencySymbol . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $rate . '</span>';
                $nestedData[] = $proValue['updated_at'] != "0000-00-00 00:00:00" ? date(FunctionManager::dateFormat(), strtotime($proValue['updated_at'])) : '';
                $nestedData[] = '<span class="notranslate"> ' . $userName . '</span>';
                /*$nestedData[]='<span class="notranslate"> '.$baseCurrency.'</span>';*/
                //$nestedData[]='<span class="notranslate"> '.$baseCurrency.'</span>';
                // $nestedData[]=$rate;
                $nestedData[] = $actionLink . $infoModal;
                $data[] = $nestedData;
            }
        }

        /*end create array for data tables.*/
        $json_data = array(
            "draw"            => intval($requestData['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval($totalData),  // total number of records
            "recordsFiltered" => intval($totalData), // total number of records 
            "data"            => $data  // total data array
        );
        echo  json_encode($json_data);
    }

    public function actionBaseCurrencyRatesAjax()
    {
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

        $rates = new CurrencyRate();
        $view_data['rates'] = $rates->getAll(array('ORDER' => 'currency'));

        $requestData = $_REQUEST;
        $action = !empty($requestData['url_action']) ? $requestData['url_action'] : "";
        $search_value = $requestData['search']['value'];

        $columns = array(
            // datatable column index  => database column name
            // 0=>'id',
            0 => 'id',
            1 => 'country',
            2 => 'currency_name',
            3 => 'currency',
            4 => 'currency_symbol',
            5 => 'rate',
            6 => 'Change Date',
            7 => 'user_name',
            8 => ''
        );

        $baseCurrency = Yii::app()->session['user_currency_dropdown'];

        $sql = "SELECT * FROM `currency_rates` where currency='" . $baseCurrency . "' and status='0' ";


        /*if search is applicable*/
        if (!empty($search_value)) {
            $sql .= ' where id like "%' . $search_value . '%"';
            $sql .= ' OR currency like "%' . $search_value . '%"';
            $sql .= ' OR currency_symbol like "%' . $search_value . '%"';
            $sql .= ' OR rate like "%' . $search_value . '%"';
        }


        /*search ends.*/
        $endLimit = abs($requestData['length']);

        if (empty($requestData['order'][0]['column'])) {
            /*$requestData['order'][0]['column'] = 0;
            $requestData['order'][0]['dir'] = 'desc';*/
        }

        $order = $columns[$requestData['order'][0]['column']];
        //$order = "p.product_id DESC"; 
        $order = " ORDER BY " . $order . " " . $requestData['order'][0]['dir'];
        $sql .= $order;
        $sql .= " LIMIT " . $requestData['start'] . " ," . $endLimit;

        $projects = Yii::app()->db->createCommand($sql)->queryAll();
        $totalData = count($projects);

        // when there is no search parameter then total number 

        $data = [];

        if (!empty($projects)) {
            foreach ($projects as $proValue) {
                $nestedData = [];
                $currencyID = $proValue['id'];
                $country = $proValue['country'];
                $currencyName = $proValue['currency_name'];
                $userName = $proValue['user_name'];
                $currency = $proValue['currency'];
                $currencySymbol = $proValue['currency_symbol'];
                $rate = $proValue['rate'];
                $baseCurrency =  '<input type="checkbox" checked disabled /> <span class="baseCurrncy"> Base Currency</span>';

                $popUpID = "curr" . $currencyID;
                $formID = "edit" . $currencyID;
                $actionLink = '<a data-toggle="modal" data-target="#' . $popUpID . '" href="#" class="btn btn-sm btn-success view-btn" style="text-decoration:none">Edit</a>&nbsp;&nbsp;&nbsp;';
                $nestedData[] = '<span class="notranslate"> ' . $country . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currencyName . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currency . '</span>';
                $nestedData[] = '<span class="notranslate"> ' . $currencySymbol . '</span>';

                $nestedData[] = '<span class="notranslate"> ' . $rate . '</span>';
                $nestedData[] = $proValue['updated_at'] != "0000-00-00 00:00:00" ? date(FunctionManager::dateFormat(), strtotime($proValue['updated_at'])) : '';
                $nestedData[] = '<span class="notranslate"> ' . $userName . '</span>';
                /*$nestedData[]='<span class="notranslate"> '.$baseCurrency.'</span>';*/
                $nestedData[] = '<span class="notranslate"> ' . $baseCurrency . '</span>';
                // $nestedData[]=$rate;  
                $nestedData[] = '<span><a href="' . AppUrl::bicesUrl('app/companyDetails') . '" class="btn btn-sm btn-success view-btn" style="text-decoration:none">Base Currency</a></span>';
                $data[] = $nestedData;
            }
        }

        /*end create array for data tables.*/
        $json_data = array(
            "draw"            => intval($requestData['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval($totalData),  // total number of records
            "recordsFiltered" => intval($totalData), // total number of records 
            "data"            => $data  // total data array
        );
        echo  json_encode($json_data);
    }

    public function actionSettings()
    {
        // is someone already logged in?
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'settings';
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

        // some cases need additional filters
        $additional_data_filter = Yii::app()->input->post('additional_data_filter');
        if (empty($additional_data_filter) && isset(Yii::app()->session['additional_data_filter']) && !empty(Yii::app()->session['additional_data_filter'])) {
            $additional_data_filter = Yii::app()->session['additional_data_filter'];
            Yii::app()->session['additional_data_filter'] = "";
            unset(Yii::app()->session['additional_data_filter']);
        }
        // same method can manage data for multiple data types
        $data_type = Yii::app()->input->post('data_type');
        if (empty($data_type) && isset(Yii::app()->session['selected_data_type']) && !empty(Yii::app()->session['selected_data_type'])) {
            $data_type = Yii::app()->session['selected_data_type'];
            Yii::app()->session['selected_data_type'] = "";
            unset(Yii::app()->session['selected_data_type']);
        }

        // no need to persist parent values across data types
        $data_type_change = Yii::app()->input->post('data_type_change');
        if ($data_type_change)
            $additional_data_filter = 0;

        // create proper data management class
        $settings = false;
        switch ($data_type) {
            case 'account_type':
                $settings = new AccountType();
                break;
            case 'category':
                $settings = new Category();
                break;
            case 'subcategory':
                $settings = new Subcategory();
                break;
            case 'contract_type':
                $settings = new ContractType();
                break;
            case 'contract_status':
                $settings = new ContractStatus();
                break;
            case 'expense_type':
                $settings = new ExpenseType();
                break;
            case 'industry':
                $settings = new Industry();
                break;
            case 'subindustry':
                $settings = new Subindustry();
                break;
            case 'payment_term':
                $settings = new PaymentTerm();
                break;
            case 'payment_type':
                $settings = new PaymentType();
                break;
            case 'shipping_method':
                $settings = new ShippingMethod();
                break;
            case 'shipping_term':
                $settings = new ShippingTerm();
                break;
            case 'saving_area':
                $settings = new SavingArea();
                break;
            case 'saving_business_unit':
                $settings = new SavingBusinessUnit();
                break;
            case 'saving_country':
                $settings = new SavingLocationCountry();
                break;
            case 'project_status':
                $settings = new ProjectStatus();
                break;
            case 'saving_status':
                $settings = new SavingStatus();
                break;
            case 'contract_document_type':
                $settings = new ContractDocumentType();
                break;
            case 'supplier_document_type':
                $settings = new VendorDocumentType();
                break;
            default:
                break;
        }

        // if form was submitted ... first save the data to the database
        $score = 0;
        $form_submitted = Yii::app()->input->post('form_submitted');
        if ($form_submitted && $data_type) {
            if ($settings) {
                $total_rows = Yii::app()->input->post('total_rows');
                for ($i = 1; $i <= $total_rows; $i++) {
                    $id = Yii::app()->input->post('id_' . $i);
                    $value = Yii::app()->input->post('value_' . $i);
                    if ($data_type == 'contract_status') {
                        $statusCode = Yii::app()->input->post('statusCode_' . $i);
                    }
                    $deleted_flag = Yii::app()->input->post('deleted_flag_' . $i);
                    if (empty($id))
                        $id = 0;
                    if (empty($deleted_flag))
                        $deleted_flag = 0;
                    if ($deleted_flag  && $data_type == 'industry') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'subindustry') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'category') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'subcategory') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'supplier_document_type') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'contract_document_type') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag) {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                        $settings->delete(array('id' => $id));
                    } else {
                        $value = rtrim(ltrim($value));
                        if (!empty($value)) {
                            $settings->rs = array();
                            $settings->rs['id'] = $id;
                            if (in_array($data_type, array('supplier_document_type', 'contract_document_type'))) {
                                $settings->rs['name'] = $value;
                            } else {
                                $settings->rs['value'] = $value;
                            }
                            if ($additional_data_filter)
                                $settings->rs['code'] = $additional_data_filter;
                            $settings->write();
                            Yii::app()->user->setFlash('success', "Configurations Saved");
                        }
                    }
                }
            }

            Yii::app()->session['selected_data_type'] = $data_type;
            if ($additional_data_filter)
                Yii::app()->session['additional_data_filter'] = $additional_data_filter;
            $this->redirect(AppUrl::bicesUrl('app/settings'));
        }

        // get data for the selected data type
        $view_data = array('data_type' => $data_type, 'data' => array(), 'additional_data_filter' => $additional_data_filter);
        if ($settings) {
            if (substr($data_type, 0, 3) == 'sub') {
                if ($data_type == 'subindustry') {
                    $view_data['data'] = $settings->getAll(array('code' => $additional_data_filter, 'soft_deleted' =>  '0', 'ORDER' => 'value'));
                } else if ($data_type == 'subcategory') {
                    $view_data['data'] = $settings->getAll(array('code' => $additional_data_filter, 'soft_deleted' =>  '0', 'ORDER' => 'value'));
                } else if ($additional_data_filter) {
                    $view_data['data'] = $settings->getAll(array('code' => $additional_data_filter, 'ORDER' => 'value'));
                }
                $view_data['additional_data_filters'] = array();

                if ($data_type == 'subcategory') {
                    $category = new Category();
                    foreach ($category->getAll(array('soft_deleted' =>  '0')) as $category_data)
                        $view_data['additional_data_filters'][$category_data['id']] = $category_data['value'];
                }

                if ($data_type == 'subindustry') {
                    $industry = new Industry();
                    foreach ($industry->getAll(array('soft_deleted' =>  '0')) as $industry_data)
                        $view_data['additional_data_filters'][$industry_data['id']] = $industry_data['value'];
                }
            } else if (in_array($data_type, array('industry', 'category', 'saving_country'))) {
                $view_data['data'] = $settings->getAll(array('soft_deleted' =>  '0'));
            } else if (in_array($data_type, array('supplier_document_type', 'contract_document_type'))) {
                if ($data_type == 'supplier_document_type') {
                    $tbl = "vendor_document_type";
                } else {
                    $tbl = "contract_document_type";
                }

                $sql = "select id, name as value from " . $tbl . " where soft_deleted=0 and created_type='General' ORDER BY name ASC ";
                $view_data['data'] = Yii::app()->db->createCommand($sql)->queryAll();
            } else {
                $view_data['data'] = $settings->getAll();
            }
        }

        // and display
        $this->render('settings', $view_data);
    }

    public function actionSettingsusg()
    {

        // is someone already logged in?
        date_default_timezone_set(Yii::app()->params['timeZone']);
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'settings';
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

        // some cases need additional filters
        $additional_data_filter = Yii::app()->input->post('additional_data_filter');
        if (empty($additional_data_filter) && isset(Yii::app()->session['additional_data_filter']) && !empty(Yii::app()->session['additional_data_filter'])) {
            $additional_data_filter = Yii::app()->session['additional_data_filter'];
            Yii::app()->session['additional_data_filter'] = "";
            unset(Yii::app()->session['additional_data_filter']);
        }

        // same method can manage data for multiple data types
        $data_type = Yii::app()->input->post('data_type');
        if (empty($data_type) && isset(Yii::app()->session['selected_data_type']) && !empty(Yii::app()->session['selected_data_type'])) {
            $data_type = Yii::app()->session['selected_data_type'];
            Yii::app()->session['selected_data_type'] = "";
            unset(Yii::app()->session['selected_data_type']);
        }

        // no need to persist parent values across data types
        $data_type_change = Yii::app()->input->post('data_type_change');
        if ($data_type_change)
            $additional_data_filter = 0;

        // create proper data management class
        $settings = false;
        switch ($data_type) {
            case 'saving_business_unit':
                $settings = new SavingBusinessUnit();
                break;
            case 'material_type':
                $settings = new MaterialType();
                break;
            case 'category':
                $settings = new Category();
                break;
            case 'subcategory':
                $settings = new Subcategory();
                break;
            case 'saving_area':
                $settings = new SavingArea();
                break;
            case 'saving_country':
                $settings = new SavingLocationCountry();
                break;
            default:
                break;
        }

        // if form was submitted ... first save the data to the database
        $score = 0;
        $form_submitted = Yii::app()->input->post('form_submitted');
        if ($form_submitted && $data_type) {
            if ($settings) {
                $total_rows = Yii::app()->input->post('total_rows');
                for ($i = 1; $i <= $total_rows; $i++) {
                    $id = Yii::app()->input->post('id_' . $i);
                    $value = Yii::app()->input->post('value_' . $i);
                    if ($data_type == 'contract_status') {
                        $statusCode = Yii::app()->input->post('statusCode_' . $i);
                    }
                    $deleted_flag = Yii::app()->input->post('deleted_flag_' . $i);

                    if (empty($id))
                        $id = 0;
                    if (empty($deleted_flag))
                        $deleted_flag = 0;
                    if ($deleted_flag  && $data_type == 'category') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'subcategory') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'material_type') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'saving_business_unit') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'saving_area') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag  && $data_type == 'saving_country') {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                    } else if ($deleted_flag) {
                        $settings->rs = array();
                        $settings->rs['id'] = $id;
                        $settings->rs['soft_deleted'] = 1;
                        $settings->write();
                        Yii::app()->user->setFlash('success', "Deleted successfully");
                        $settings->delete(array('id' => $id));
                    } else { //
                        $value = rtrim(ltrim($value));
                        if (!empty($value)) {
                            $settings->rs = array();
                            $settings->rs['id'] = $id;
                            $settings->rs['value'] = $value;
                            if ($data_type == 'contract_status') {
                                $settings->rs['code'] = $statusCode;
                            } else if ($data_type == 'supplier_document_type') {
                                $settings->rs['score'] = $score;
                            }

                            if ($additional_data_filter)
                                $settings->rs['code'] = $additional_data_filter;
                            if (!empty($additional_data_filter) && $data_type == 'category') {
                                $settings->rs['material_type_id'] = $additional_data_filter;
                            }
                            $settings->write();
                            Yii::app()->user->setFlash('success', "Configurations Saved");
                        }
                    }
                }
            }

            Yii::app()->session['selected_data_type'] = $data_type;
            if ($additional_data_filter)
                Yii::app()->session['additional_data_filter'] = $additional_data_filter;
            $this->redirect(AppUrl::bicesUrl('app/settingsusg'));
        }


        // get data for the selected data type
        $view_data = array('data_type' => $data_type, 'data' => array(), 'additional_data_filter' => $additional_data_filter);
        if ($settings) {
            if (substr($data_type, 0, 3) == 'sub' || substr($data_type, 0, 3) == 'cat') {
                if ($data_type == 'category') {
                    $view_data['data'] = $settings->getAll(array('material_type_id' => $additional_data_filter, 'soft_deleted' =>  '0', 'ORDER' => 'value'));
                } else if ($additional_data_filter) {
                    $view_data['data'] = $settings->getAll(array('code' => $additional_data_filter, 'ORDER' => 'value'));
                }
                $view_data['additional_data_filters'] = array();

                if ($data_type == 'subcategory') {
                    $category = new Category();
                    $catMaterial = new MaterialType();
                    foreach ($category->getAll(array('soft_deleted' =>  '0')) as $category_data) {
                        $catMaterilaRecord = $catMaterial->getOne(array('id' => $category_data['material_type_id']));
                        $mtName = '';
                        if (!empty($catMaterilaRecord['id'])) {
                            $mtName = ' (' . $catMaterilaRecord['value'] . ')';
                        }
                        $view_data['additional_data_filters'][$category_data['id']] = $category_data['value'] . $mtName;
                    }
                }
            }
            if ($data_type == 'subcategory') {
                $view_data['data'] = $settings->getAll(array('soft_deleted' =>  '0'));
            } else if (in_array($data_type, array('saving_business_unit', 'material_type', 'category', 'saving_area', 'saving_country', 'subcategory'))) {
                $view_data['data'] = $settings->getAll(array('soft_deleted' =>  '0'));
            } else {
                $view_data['data'] = $settings->getAll();
            }
        }

        // echo "<pre>"; print_r($sub); echo "</pre>"; exit;
        // and display
        $this->render('settings_usg', $view_data);
    }

    public function actionDefaultValue()
    {
        // is someone already logged in?

        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'default_value';
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

        // some cases need additional filters
        $additional_data_filter = Yii::app()->input->post('additional_data_filter');
        if (empty($additional_data_filter) && isset(Yii::app()->session['additional_data_filter']) && !empty(Yii::app()->session['additional_data_filter'])) {
            $additional_data_filter = Yii::app()->session['additional_data_filter'];
            Yii::app()->session['additional_data_filter'] = "";
            unset(Yii::app()->session['additional_data_filter']);
        }
        // same method can manage data for multiple data types
        $data_type = 'supplier_scoring_criteria'; //Yii::app()->input->post('data_type');
        if (empty($data_type) && isset(Yii::app()->session['selected_data_type']) && !empty(Yii::app()->session['selected_data_type'])) {
            $data_type = Yii::app()->session['selected_data_type'];
            Yii::app()->session['selected_data_type'] = "";
            unset(Yii::app()->session['selected_data_type']);
        }

        // no need to persist parent values across data types
        $data_type_change = Yii::app()->input->post('data_type_change');
        if ($data_type_change)
            $additional_data_filter = 0;

        // create proper data management class
        $settings = new VendorScoring();


        // if form was submitted ... first save the data to the database
        $score = 0;
        $form_submitted = Yii::app()->input->post('form_submitted');


        if ($form_submitted && $data_type) {
            if ($settings) {

                $total_rows = Yii::app()->input->post('total_rows');
                for ($i = 1; $i <= $total_rows; $i++) {
                    $id = Yii::app()->input->post('id_' . $i);
                    $value = Yii::app()->input->post('value_' . $i);
                    if ($data_type == 'contract_status') {
                        $statusCode = Yii::app()->input->post('statusCode_' . $i);
                    } else if ($data_type == 'supplier_scoring_criteria') {
                        $score = Yii::app()->input->post('score_' . $i);
                        if (empty($score)) {
                            $score = 0;
                        }
                    }
                    $deleted_flag = Yii::app()->input->post('deleted_flag_' . $i);

                    if (empty($id))
                        $id = 0;
                    if (empty($deleted_flag))
                        $deleted_flag = 0;

                    if ($deleted_flag)
                        $settings->delete(array('id' => $id));
                    else {
                        $value = rtrim(ltrim($value));
                        if (!empty($value)) {
                            $settings->rs = array();
                            $settings->rs['id'] = $id;
                            $settings->rs['value'] = $value;
                            if ($data_type == 'contract_status') {
                                $settings->rs['code'] = $statusCode;
                            } else if ($data_type == 'supplier_scoring_criteria') {
                                $settings->rs['score'] = $score;
                            }
                            if ($additional_data_filter)
                                $settings->rs['code'] = $additional_data_filter;
                            $settings->write();
                            Yii::app()->user->setFlash('success', "Default Values added successfully.");
                        }
                    }
                }
            }

            Yii::app()->session['selected_data_type'] = $data_type;
            if ($additional_data_filter)
                Yii::app()->session['additional_data_filter'] = $additional_data_filter;
            $this->redirect(AppUrl::bicesUrl('app/defaultValue'));
        }

        // get data for the selected data type
        $view_data = array('data_type' => $data_type, 'data' => array(), 'additional_data_filter' => $additional_data_filter);

        if ($settings) {

            if (substr($data_type, 0, 3) == 'sub') {

                if ($additional_data_filter) {
                    $view_data['data'] = $settings->getAll(array('code' => $additional_data_filter, 'ORDER' => 'value'));
                }

                $view_data['additional_data_filters'] = array();
                if ($data_type == 'subcategory') {
                    $category = new Category();
                    foreach ($category->getAll() as $category_data)
                        $view_data['additional_data_filters'][$category_data['id']] = $category_data['value'];
                }

                if ($data_type == 'category') {
                    $material = new MaterialType();
                    foreach ($material->getAll() as $material_data)
                        $view_data['additional_data_filters'][$material_data['id']] = $material_data['value'];
                }

                if ($data_type == 'subindustry') {
                    $industry = new Industry();
                    foreach ($industry->getAll() as $industry_data)
                        $view_data['additional_data_filters'][$industry_data['id']] = $industry_data['value'];
                }
            } else
                $view_data['data'] = $settings->getAll();
        }
        // and display
        $this->render('default_value', $view_data);
    }

    public function actionDeleteDefaultValue()
    {
        $defaultID = $_POST['valueID'];
        $sql1 = "delete from vendor_scoring where id=" . $defaultID;
        $sql = "delete from vendor_scoring_questions where vendor_scoring_id in(" . $defaultID . ") ";
        Yii::app()->db->createCommand($sql1)->execute();
        Yii::app()->db->createCommand($sql)->execute();
    }

    public function actionExport()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $data_type = isset($_REQUEST['data_type']) ? $_REQUEST['data_type'] : "";

        $data_object = false;
        switch ($data_type) {
            case 'category':
                $data_object = new Category();
                break;
            case 'industry':
                $data_object = new Industry();
                break;
            case 'subcategory':
                $data_object = new Subcategory();
                break;
            case 'subindustry':
                $data_object = new Subindustry();
                break;
            default:
                break;
        }

        if ($data_object)
            $data_object->export();
        else
            $this->redirect(AppUrl::bicesUrl('app/settings'));
    }

    public function actionImportTemplates()
    {
        $this->redirect(AppUrl::bicesUrl('quotes/list'));
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'import_templates';

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(1, 3, 4)))
            $this->redirect(AppUrl::bicesUrl('home'));

        if (isset(Yii::app()->session['admin_flag']) && Yii::app()->session['admin_flag'] == 1)
            if (!is_dir('uploads/imports'))
                mkdir('uploads/imports');

        // display import templates to everyone
        $this->render('import_templates', array());
    }

    public function actionImport()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'import_admin';
        /* if (false && !isset(Yii::app()->session['admin_flag']) || Yii::app()->session['admin_flag'] != 1)
        { $this->redirect(AppUrl::bicesUrl('app/importTemplates')); 
        }*/

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(1, 3, 4))) {
            //$this->redirect(AppUrl::bicesUrl('app/importTemplates')); 
        }



        $path = 'uploads/imports';
        if (!is_dir($path)) {
            mkdir($path);
        }
        $path .= '/history';
        if (!is_dir($path)) {
            mkdir($path);
        }
        $path .= '/' . date('Y-m-d');
        if (!is_dir($path)) {
            mkdir($path);
        }

        // if something was uploaded, then store it as a template

        $form_submitted = isset($_REQUEST['form_submitted']) ? $_REQUEST['form_submitted'] : 0;
        $time = time();
        if ($form_submitted) {
            $template_type = isset($_REQUEST['template_type']) ? $_REQUEST['template_type'] : "";
            if (!empty($template_type)) {
                $template_file_name = $path . '/' . $template_type . '~Template.' . rand() . '.csv';
                if (is_file($template_file_name))
                    @unlink($template_file_name);
                if (isset($_FILES['template_file']) && isset($_FILES['template_file']['tmp_name']))
                    if (move_uploaded_file($_FILES['template_file']['tmp_name'], $template_file_name)) {

                        unset(Yii::app()->session['contract_import']);

                        if (strtolower(trim($template_type)) == "vendor") {
                            // $this->vendorImportReview($template_file_name);
                            $this->render('import_vendor_review', array('filePath' => $template_file_name));
                            // $this->vendorImport($template_file_name);
                        } else if (strtolower(trim($template_type)) == "product") {
                            $this->productImport($template_file_name);
                        } else if (strtolower(trim($template_type)) == "expense") {
                            $this->expenseImport($template_file_name);
                        } else if (strtolower(trim($template_type)) == "order") {
                            $this->orderImport($template_file_name);
                        } else if (strtolower(trim($template_type)) == "contract") {
                            // $data = $this->contractImportReview($template_file_name);
                            $this->render('import_contract_review', array('filePath' => $template_file_name));
                            //$this->contractImport($template_file_name);
                        }
                    }
            }

            $this->redirect(AppUrl::bicesUrl('app/import'));
        } else if (!empty($_POST['contract_import'])) {
            $this->contractImportSession();
        } else if (!empty($_POST['contract_import_cancel'])) {
            unset(Yii::app()->session['contract_import']);
        } else if (!empty($_POST['vendor_import'])) {
            $this->vendorImportSession();
        } else if (!empty($_POST['vendor_import_cancel'])) {
            unset(Yii::app()->session['vendor_import']);
        }
        // display import templates to everyone
        $this->render('import', array());
    }

    public function vendorImportReview($filePath)
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);

        $row = 1;
        $checkRecords = 0;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            // Start: Check Count
            while (($data = fgetcsv($handle)) !== FALSE) {

                if ($row > 1  && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]) || !empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9]))) {
                    $checkRecords++;
                }
                $row++;
            }
            if ($checkRecords > 100) {
                Yii::app()->user->setFlash('error', '<strong>Warning!</strong> You are trying to import more than 100 records. Only 100 or less can be uploaded at one time');
                $this->redirect(array('app/import'));
            }
        }

        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
                $num = count($data);
                if ($row > 1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]) || !empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9]))) {
                    $vendorName                  = addslashes(trim($data[0]));
                    $contactName                 = addslashes(trim($data[1]));
                    $email                      = addslashes(trim($data[2]));
                    $address1                   = addslashes(trim($data[3]));
                    $address2                   = addslashes(trim($data[4]));
                    $city                       = addslashes(trim($data[5]));
                    $stateOrCounty              = addslashes(trim($data[6]));
                    //$country                    = addslashes(trim($data[7]));
                    $zip                        = addslashes(trim($data[7]));
                    $phone                      = addslashes(trim($data[8]));
                    $mobile                     = addslashes(trim($data[9]));
                    $createdDate                = date('Y-m-d H:i:s');
                    $active                     = 1;

                    $checkVendor = new Vendor();
                    $password = $checkVendor->generateRandomString(10);
                    $vendorCheckReader = $checkVendor->getOne(array('emails' => $email));
                    /*if(!empty($vendorCheckReader)){
                    continue;
                }*/
                    $sql = "INSERT INTO vendors (vendor_name,contact_name,emails,address_1,address_2,city,state,zip,phone_1,phone_2,created_datetime,updated_datetime,active) VALUES ('" . $vendorName . "', '" . $contactName . "','" . $email . "', '" . $address1 . "', '" . $address2 . "', '" . $city . "', '" . $stateOrCounty . "', '" . $zip . "','" . $phone . "', '" . $mobile . "','" . $createdDate . "','" . $createdDate . "','" . $active . "')";

                    $command = Yii::app()->db->createCommand($sql);
                    $sqlResult = $command->execute();
                    if (!empty($sqlResult)) {
                        // $insert_id = Yii::app()->db->getLastInsertID();
                        // EmailManager::vendorAcivation($insert_id ,Yii::app()->session['full_name']);
                        Yii::app()->user->setFlash('success', '<strong>Success!</strong> Vendors data imported.');
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
                    }
                }
                $row++;
            }
            fclose($handle);
        }
    }

    public function vendorImportSession()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);


        // $num = count($data);
        $recordsImport = Yii::app()->session['vendor_import'];
        foreach ($recordsImport as $key => $data) {

            $vendorName                  = $data['vendorName'];
            $contactName                 = $data['contactName'];
            $email                      = $data['email'];
            $address1                   = $data['address1'];
            $address2                   = $data['address2'];
            $city                       = $data['city'];
            $stateOrCounty              = $data['stateOrCounty'];
            //$country                    = addslashes(trim($data[7]));
            $zip                        = $data['zip'];
            $phone                      = $data['phone'];
            $mobile                     = $data['mobile'];
            $createdDate                = date('Y-m-d H:i:s');
            $active                     = 1;

            $checkVendor = new Vendor();
            $password = $checkVendor->generateRandomString(10);
            $vendorCheckReader = $checkVendor->getOne(array('emails' => $email));
            /*if(!empty($vendorCheckReader)){
                    continue;
                }*/
            $sql = "INSERT INTO vendors (vendor_name,contact_name,emails,address_1,address_2,city,state,zip,phone_1,phone_2,created_datetime,updated_datetime,active) VALUES ('" . $vendorName . "', '" . $contactName . "','" . $email . "', '" . $address1 . "', '" . $address2 . "', '" . $city . "', '" . $stateOrCounty . "', '" . $zip . "','" . $phone . "', '" . $mobile . "','" . $createdDate . "','" . $createdDate . "','" . $active . "')";

            $command = Yii::app()->db->createCommand($sql);
            $sqlResult = $command->execute();
            if (!empty($sqlResult)) {
                // $insert_id = Yii::app()->db->getLastInsertID();
                // EmailManager::vendorAcivation($insert_id ,Yii::app()->session['full_name']);
                Yii::app()->user->setFlash('success', '<strong>Success!</strong> Vendors data imported.');
            } else {
                Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
            }
        }
    }


    public function vendorImport($filePath)
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);

        $row = 1;
        $checkRecords = 0;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            // Start: Check Count
            while (($data = fgetcsv($handle)) !== FALSE) {

                if ($row > 1  && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]) || !empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9]))) {
                    $checkRecords++;
                }
                $row++;
            }
            if ($checkRecords > 100) {
                Yii::app()->user->setFlash('error', '<strong>Warning!</strong> You are trying to import more than 100 records. Only 100 or less can be uploaded at one time');
                $this->redirect(array('app/import'));
            }
        }

        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
                $num = count($data);
                if ($row > 1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]) || !empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9]))) {
                    $vendorName                  = addslashes(trim($data[0]));
                    $contactName                 = addslashes(trim($data[1]));
                    $email                      = addslashes(trim($data[2]));
                    $address1                   = addslashes(trim($data[3]));
                    $address2                   = addslashes(trim($data[4]));
                    $city                       = addslashes(trim($data[5]));
                    $stateOrCounty              = addslashes(trim($data[6]));
                    //$country                    = addslashes(trim($data[7]));
                    $zip                        = addslashes(trim($data[7]));
                    $phone                      = addslashes(trim($data[8]));
                    $mobile                     = addslashes(trim($data[9]));
                    $createdDate                = date('Y-m-d H:i:s');


                    $checkVendor = new Vendor();
                    $password = $checkVendor->generateRandomString(10);
                    $vendorCheckReader = $checkVendor->getOne(array('emails' => $email));
                    /*if(!empty($vendorCheckReader)){
                    continue;
                }*/
                    $sql = "INSERT INTO vendors (vendor_name,contact_name,emails,address_1,address_2,city,state,zip,phone_1,phone_2,created_datetime,updated_datetime) VALUES ('" . $vendorName . "', '" . $contactName . "','" . $email . "', '" . $address1 . "', '" . $address2 . "', '" . $city . "', '" . $stateOrCounty . "', '" . $zip . "','" . $phone . "', '" . $mobile . "','" . $createdDate . "','" . $createdDate . "')";

                    $command = Yii::app()->db->createCommand($sql);
                    $sqlResult = $command->execute();
                    if (!empty($sqlResult)) {
                        // $insert_id = Yii::app()->db->getLastInsertID();
                        // EmailManager::vendorAcivation($insert_id ,Yii::app()->session['full_name']);
                        Yii::app()->user->setFlash('success', '<strong>Success!</strong> Vendors data imported.');
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
                    }
                }
                $row++;
            }
            fclose($handle);
        }
    }

    /* public function actionVendorCSVImport(){
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);

        $termsMissed = $industryMissed = $subindustryMissed =array();

        $path = Yii::app()->basePath.'/../uploads/SupplierData.csv';
        $row = 1;
        if (($handle = fopen($path, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $num = count($data);

            if($row>5){
              
               $vendorName                  = addslashes(trim($data[0]));
               $contactName                 = addslashes(trim($data[1]));
                $email                      = addslashes(trim($data[2]));
                $phone                      = addslashes(trim($data[3]));
                $mobile                     = addslashes(trim($data[4]));

                $assignedSupplierID         = addslashes(trim($data[5]));
                $industry                   = addslashes(trim($data[6]));
                $subIndustry                = addslashes(trim($data[7]));
                $taxNumber                  = addslashes(trim($data[8]));

                $address1                   = addslashes(trim($data[9]));
                $city                       = addslashes(trim($data[10]));
                $country                  = addslashes(trim($data[12]));
                //$country                    = addslashes(trim($data[7]));
                $zip                        = addslashes(trim($data[13]));
                $paymentTerms               = addslashes(trim($data[14]));
                $createdDate                = date('Y-m-d H:i:s');

                $sql ="select * from payment_terms where value='".$paymentTerms."'";
                $terms = Yii::app()->db->createCommand($sql)->queryRow();
                $paymentTermID = 0;
                if(!empty($terms)){
                    $paymentTermID = $terms['id'];
                }else{
                    $termsMissed[] = $paymentTerms;
                }

               
                $sql ="select * from industries where value='".$industry."'";
                $industryReader = Yii::app()->db->createCommand($sql)->queryRow();
                $industryID = 0;
                if(!empty($industryReader)){
                    $industryID = $industryReader['id'];
                }else{
                    $industryMissed[] = $industry;

                }

                 $sql ="select * from sub_industries where value='".$subIndustry."'";
                $subindustryReader = Yii::app()->db->createCommand($sql)->queryRow();
                $subIndustryID = 0;
                if(!empty($subindustryReader)){
                    $subIndustryID = $subindustryReader['id'];
                }else{
                    $subindustryMissed[] = $subIndustry;
                }



             
            $sql = "INSERT INTO vendors (vendor_name,contact_name,emails,address_1,external_id,industry_id,subindustry_id,tax_number,city,country,zip,phone_1,phone_2,payment_term_id,created_datetime,updated_datetime) VALUES ('".$vendorName."', '".$contactName."','".$email."', '".$address1."', '". $assignedSupplierID."','". $industryID."','". $subIndustryID."','". $taxNumber."', '". $city."', '".$country."', '".$zip."','".$phone."', '".$mobile."','".$paymentTermID."','".$createdDate."','".$createdDate."')";

            $command= Yii::app()->db->createCommand($sql);
            $sqlResult = $command->execute();
            if(!empty($sqlResult)){
             
                Yii::app()->user->setFlash('success', '<strong>Success!</strong> Vendors data imported.');
            }else{
                Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
            }
        }
         $row++;
        }
        fclose($handle);
        }
      
        echo "<pre>";print_r($termsMissed);
        echo "<pre>";print_r($industryMissed);
        echo "<pre>";print_r($subindustryMissed);
        exit;

    }*/

    public function actionContractCSVImport()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);

        $categoryMissed = $departmentMissed = $subCategoryMissed = $locationMissed =  $supplierMissed = $departmentMissed = array();

        $path = Yii::app()->basePath . '/../uploads/Book7.csv';
        $row = 1;
        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);

                if ($row > 1 && $row < 115) {


                    $contract_title     = addslashes(trim($data[0]));
                    $contract_reference = addslashes(trim($data[1]));
                    $type = ''; //addslashes(trim($data[2]));
                    $location = addslashes(trim($data[3]));
                    $department = addslashes(trim($data[4]));
                    $category = addslashes(trim($data[5]));
                    $subCategory = addslashes(trim($data[6]));
                    $supplier = addslashes(trim($data[7]));
                    $contract_start_date = trim($data[8]);

                    if (!empty($contract_start_date)) {
                        $contract_start_date = str_replace('/', '-', $contract_start_date);
                        $contract_start_date = date('Y-m-d', strtotime($contract_start_date));
                    } else {
                        $contract_start_date = '0000-00-00';
                    }

                    $contract_end_date  = trim($data[9]);
                    if (!empty($contract_end_date)) {
                        $contract_end_date  = str_replace('/', '-', $contract_end_date);
                        $contract_end_date  = date('Y-m-d', strtotime($contract_end_date));
                    } else {
                        $contract_end_date = '0000-00-00';
                    }

                    $notice_period  = trim($data[10]);
                    if (!empty($notice_period)) {
                        $notice_period  = str_replace('/', '-', $notice_period);
                        $notice_period  = date('Y-m-d', strtotime($notice_period));
                    } else {
                        $notice_period = '0000-00-00 00:00:00';
                    }

                    $createdDate    = date('Y-m-d H:i:s');



                    $sql = "select vendor_id from vendors where vendor_name ='" . $supplier . "'";
                    $supReader = Yii::app()->db->createCommand($sql)->queryRow();
                    $vendor_id = 0;
                    if (!empty($supReader)) {
                        $vendor_id = $supReader['vendor_id'];
                    } else {
                        $supplierMissed[] = $supplier;
                    }

                    $sql = "select * from locations where location_name='" . $location . "'";
                    $locReader = Yii::app()->db->createCommand($sql)->queryRow();
                    $location_id = 0;
                    if (!empty($locReader)) {
                        $location_id = $locReader['location_id'];
                    } else {
                        $locationMissed[] = $location;
                    }

                    $sql = "select * from departments where department_name='" . $department . "'";
                    $deptReader = Yii::app()->db->createCommand($sql)->queryRow();
                    $department_id = 0;
                    if (!empty($deptReader)) {
                        $department_id = $deptReader['department_id'];
                    } else {
                        $departmentMissed[] = $department;
                    }

                    $sql = "select * from categories where value='" . $category . "'";
                    $catReader = Yii::app()->db->createCommand($sql)->queryRow();
                    $category_id = 0;
                    if (!empty($catReader)) {
                        $category_id = $catReader['id'];
                    } else {
                        $categoryMissed[] = $category;
                    }

                    $sql = "select * from sub_categories where value='" . $subCategory . "'";
                    $subcatReader = Yii::app()->db->createCommand($sql)->queryRow();
                    $sub_category_id = 0;
                    if (!empty($subcatReader)) {
                        $sub_category_id = $subcatReader['id'];
                    } else {
                        $subCategoryMissed[] = $subCategory;
                    }


                    $userID = Yii::app()->session['user_id'];
                    if (!empty($userID)) {
                        $userObj = new User();
                        $userReader = $userObj->getOne(array('user_id' => $userID));
                        if (isset($userReader) && !empty($userReader['user_id'])) {
                            $userName = $userReader['full_name'];
                        }
                    }

                    $sql = "INSERT INTO contracts (user_id,user_name,contract_title,contract_reference,contract_type,location_id,department_id,contract_category_id,contract_subcategory_id,vendor_id,contract_start_date,contract_end_date,break_clause,created_datetime,updated_datetime)
                        VALUES ('" . $userID . "','" . $userName . "','" . $contract_title . "', '" . $contract_reference . "','" . $type . "','" . $location_id . "','" . $department_id . "','" . $category_id . "','" . $sub_category_id . "','" . $vendor_id . "','" . $contract_start_date . "','" . $contract_end_date . "','" . $notice_period . "','" . $createdDate . "','" . $createdDate . "')";
                    $command = Yii::app()->db->createCommand($sql);
                    $sqlResult = $command->execute();


                    if (!empty($sqlResult)) {

                        Yii::app()->user->setFlash('success', '<strong>Success!</strong> Vendors data imported.');
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        // echo "<pre>";print_r($supplierMissed);
        // echo "<pre>";print_r($locationMissed);
        // echo "<pre>";print_r($departmentMissed);
        // echo "<pre>";print_r($categoryMissed);
        // echo "<pre>";print_r($subCategoryMissed);
        // exit;
    }

    public function actionCategoryCSVImport()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);

        $categoryMissed = $departmentMissed = $subCategoryMissed = $locationMissed =  $supplierMissed = $departmentMissed = array();

        $path = Yii::app()->basePath . '/../Master oboloo Development - Reduced Categories.csv';
        $row = 1;
        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
                $num = count($data);
                if ($row > 1 && $row < 1402) {
                    $cateName     = addslashes(trim($data[0]));
                    $subcateName     = addslashes(trim($data[1]));

                    $sql = "select id from categories where value='" . $cateName . "'";
                    $command = Yii::app()->db->createCommand($sql);
                    $checkCat = $command->queryRow();

                    if (!empty($cateName) && empty($checkCat)) {
                        $sql = "INSERT INTO categories (value) VALUES ('" . $cateName . "')";
                        $command = Yii::app()->db->createCommand($sql);
                        $sqlResult = $command->execute();
                        $categoryID = Yii::app()->db->lastInsertID;
                    } else {
                        $categoryID = $checkCat['id'];
                    }

                    // sub
                    if (!empty($subcateName)) {
                        $sql = "select value from sub_categories where value='" . $subcateName . "' and  value='" . $categoryID . "'";
                        $command = Yii::app()->db->createCommand($sql);
                        $checkCat = $command->queryRow();

                        if (empty($checkCat)) {
                            $sql = "INSERT INTO sub_categories (value,code) VALUES ('" . $subcateName . "','" . $categoryID . "')";
                            $command = Yii::app()->db->createCommand($sql);
                            $sqlResult = $command->execute();
                        }
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        /*echo "<pre>";print_r($supplierMissed);
        echo "<pre>";print_r($locationMissed);
        echo "<pre>";print_r($departmentMissed);
        echo "<pre>";print_r($categoryMissed);
        echo "<pre>";print_r($subCategoryMissed);
        exit;
*/
    }

    public function actionIndustriesCSVImport()
    {
        //  date_default_timezone_set(Yii::app()->params['timeZone']);
        //   set_time_limit(0);
        //   error_reporting(0);

        //   $categoryMissed = $departmentMissed = $subCategoryMissed = $locationMissed =  $supplierMissed = $departmentMissed = array();

        //   $path = Yii::app()->basePath.'/../Industry & Sub Industry JL.csv';
        //  $row = 1;
        //   if (($handle = fopen($path, "r")) !== FALSE) {
        //   while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        //       $num = count($data);
        //       if($row>1 && $row<616){
        //          $industName     = addslashes(trim($data[0]));
        //          $subindustName     = addslashes(trim($data[1]));

        //          $sql = "select id from industries where value='".$industName."'";
        //           $command= Yii::app()->db->createCommand($sql);
        //           $checkCat = $command->queryRow();

        //           if(empty($checkCat)){  
        //              $sql = "INSERT INTO industries (value) VALUES ('".$industName."')";
        //            $command= Yii::app()->db->createCommand($sql);$sqlResult = $command->execute();
        //               $categoryID= Yii::app()->db->lastInsertID;
        //           }else{
        //               $categoryID=$checkCat['id'];
        //           }

        // //          // sub
        //           if(!empty($subindustName)){
        //           $sql = "select value from sub_industries where value='".$subindustName."' and  value='".$categoryID."'";
        //          $command= Yii::app()->db->createCommand($sql);
        //           $checkCat = $command->queryRow();

        //           if(empty($checkCat)){  
        //              $sql = "INSERT INTO sub_industries (value,code) VALUES ('".$subindustName."','".$categoryID."')";
        //               $command= Yii::app()->db->createCommand($sql);$sqlResult = $command->execute();
        //           }}
        //       }
        //    $row++;
        //   }
        //   fclose($handle);
        //   }
        /*echo "<pre>";print_r($supplierMissed);
        echo "<pre>";print_r($locationMissed);
        echo "<pre>";print_r($departmentMissed);
        echo "<pre>";print_r($categoryMissed);
        echo "<pre>";print_r($subCategoryMissed);
        exit;
*/
    }

    public function productImport($filePath)
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);
        $row = 1;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($row > 1) {
                    $productName        = addslashes(trim($data[0]));
                    $productType        = addslashes(trim($data[1]));
                    $productCategory    = addslashes(trim($data[2]));
                    $productSubCategory = addslashes(trim($data[3]));
                    $supplier           = addslashes(trim($data[4]));
                    $contract           = addslashes(trim($data[5]));
                    $brand              = addslashes(trim($data[6]));
                    $url                = addslashes(trim($data[7]));
                    $upc                = addslashes(trim($data[8]));
                    $sku                = addslashes(trim($data[9]));
                    $length             = addslashes(trim($data[10]));
                    $width              = addslashes(trim($data[11]));
                    $height             = addslashes(trim($data[12]));
                    $weight             = addslashes(trim($data[13]));
                    $size               = addslashes(trim($data[14]));
                    /* $price              = addslashes(trim(str_replace(',', '',$data[15])));
                $price              = str_replace('$', '',$price);*/

                    $price              = $data[15];
                    $currency           = trim($data[16]);

                    $price = preg_replace('/[^0-9-.]+/', '', $price);

                    // $price = filter_var( str_replace(",", "", $price), FILTER_SANITIZE_NUMBER_FLOAT);
                    $currencyID = 0;
                    if (!empty($currency)) {
                        $currencyReader = Yii::app()->db->createCommand('select id from currency_rates where LOWER(currency)="' . strtolower($currency) . '"')->queryRow();
                        $currencyID = $currencyReader['id'];
                    }

                    $createdDate        = date('Y-m-d H:i:s');
                    $productCategoryID  = $productSubCategoryID = $supplierID = $contractID = '';

                    if (!empty($productCategory)) {
                        $category = new Category();
                        $categoryReader = $category->getOne(array('value' => $productCategory));
                        if (isset($categoryReader) && !empty($categoryReader['id'])) {
                            $productCategoryID = $categoryReader['id'];
                        } else {
                            $sql = "INSERT INTO categories (value) VALUES ('" . $productCategory . "')";
                            $command = Yii::app()->db->createCommand($sql);
                            $sqlResult = $command->execute();
                            $productCategoryID = Yii::app()->db->lastInsertID;
                        }
                    }

                    if (!empty($productSubCategory)) {
                        $categorySub = new Subcategory();
                        $categorySubReader = $categorySub->getOne(array('value' => $productSubCategory));
                        if (isset($categorySubReader) && !empty($categorySubReader['id'])) {
                            $productSubCategoryID = $categorySubReader['id'];
                        } else {
                            $sql = "INSERT INTO sub_categories (value,code) VALUES ('" . $productSubCategory . "','" . $productCategoryID . "')";
                            $command = Yii::app()->db->createCommand($sql);
                            $sqlResult = $command->execute();
                            $productSubCategoryID = Yii::app()->db->lastInsertID;
                        }
                    }

                    if (!empty($supplier)) {
                        $vendor = new Vendor();
                        $vendorReader = $vendor->getOne(array('vendor_name' => $supplier));
                        if (isset($vendorReader) && !empty($vendorReader['vendor_id'])) {
                            $supplierID = $vendorReader['vendor_id'];
                        }
                    }

                    if (!empty($contract)) {
                        $contractObj = new Contract();
                        $contractReader = $contractObj->getOne(array('contract_title' => $contract));
                        if (isset($contractReader) && !empty($contractReader['contract_id'])) {
                            $contractID = $contractReader['contract_id'];
                        }
                    }

                    $sql = "INSERT INTO products (product_name, product_type, category_id,subcategory_id,vendor_id,contract_id,upc,sku_code,brand,size,price,url,length,width,height,weight,active,created_datetime,updated_datetime,currency_id) VALUES ('" . $productName . "', '" . $productType . "','" . $productCategoryID . "', '" . $productSubCategoryID . "', '" . $supplierID . "', '" . $contractID . "', '" . $upc . "', '" . $sku . "','" . $brand . "', '" . $size . "', '" . $price . "', '" . $url . "', '" . $length . "', 
                 '" . $width . "', '" . $height . "','" . $weight . "', '1','" . $createdDate . "','" . $createdDate . "','" . $currencyID . "')";
                    $command = Yii::app()->db->createCommand($sql);
                    $sqlResult = $command->execute();
                    if (!empty($sqlResult)) {
                        Yii::app()->user->setFlash('success', '<strong>Success!</strong> Products data imported.');
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
                    }
                }
                $row++;
            }
            fclose($handle);
        }
    }

    public function expenseImport($filePath)
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);
        $row = 1;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($row > 1) {
                    $location           = addslashes(trim($data[0]));
                    $department         = addslashes(trim($data[1]));
                    $project            = addslashes(trim($data[2]));
                    $spendType          = addslashes(trim($data[3]));
                    //$currencyRateUnit   = addslashes(trim($data[3]));
                    $currencyRateUnit       = addslashes(trim($data[4]));
                    $currencyRate       = addslashes(trim($data[5]));
                    $expenseTitle       = addslashes(trim($data[6]));
                    $userFullName       = addslashes(trim($data[7]));
                    $expenseNotes       = addslashes(trim($data[8]));
                    $expenseType        = addslashes(trim($data[9]));

                    $expenseDate        = $data[10];
                    $expenseDate        = str_replace('/', '-', $expenseDate);

                    $expenseDate        = date('Y-m-d', strtotime($expenseDate));
                    $expensePrice       = trim($data[11]);
                    $taxRate            = trim($data[12]);
                    $taxAmount          = trim($data[13]);
                    $supplier           = addslashes(trim($data[14]));
                    $notes              = addslashes(trim($data[15]));
                    $status             = trim($data[16]);
                    // START Remove comma and currency symbol

                    $currencyRate = preg_replace('/[^0-9-.]+/', '', $currencyRate);
                    $expensePrice = preg_replace('/[^0-9-.]+/', '', $expensePrice);
                    $taxRate = preg_replace('/[^0-9-.]+/', '', $taxRate);
                    $taxAmount = preg_replace('/[^0-9-.]+/', '', $taxAmount);

                    // End Remove comma and currency symbol

                    // START Get dropdown ids  by name/value
                    $supplierID = $locationID = $departmentID = $projectID = $userID = $expenseTypeID =  $currencyID = '';
                    $createdDate        = date('Y-m-d H:i:s');

                    if (!empty($supplier)) {
                        $vendor = new Vendor();
                        $vendorReader = $vendor->getOne(array('vendor_name' => $supplier));
                        if (isset($vendorReader) && !empty($vendorReader['vendor_id'])) {
                            $supplierID = $vendorReader['vendor_id'];
                        }
                    }

                    if (!empty($location)) {
                        $locationObj = new Location();
                        $locationReader = $locationObj->getOne(array('location_name' => $location));
                        if (isset($locationReader) && !empty($locationReader['location_id'])) {
                            $locationID = $locationReader['location_id'];
                        }
                    }

                    if (!empty($department)) {
                        $departmentObj = new Department();
                        $departmentReader = $departmentObj->getOne(array('department_name' => $department));
                        if (isset($departmentReader) && !empty($departmentReader['department_id'])) {
                            $departmentID = $departmentReader['department_id'];
                        }
                    }

                    if (!empty($project)) {
                        $projectObj = new Project();
                        $projectReader = $projectObj->getOne(array('project_name' => $project));
                        if (isset($projectReader) && !empty($projectReader['project_id'])) {
                            $projectID = $projectReader['project_id'];
                        }
                    }

                    if (!empty($userFullName)) {
                        $userObj = new User();
                        $userReader = $userObj->getOne(array('full_name' => $userFullName));
                        if (isset($userReader) && !empty($userReader['user_id'])) {
                            $userID = $userReader['user_id'];
                        }
                    }

                    if (!empty($expenseType)) {
                        $expenseTypeObj = new ExpenseType();
                        $expenseTypeReader = $expenseTypeObj->getOne(array('value' => $expenseType));
                        if (isset($expenseTypeReader) && !empty($expenseTypeReader['id'])) {
                            $expenseTypeID = $expenseTypeReader['id'];
                        }
                    }

                    if (!empty($currencyRateUnit)) {
                        $currencyRateObj = new CurrencyRate();
                        $currencyRateReader = $currencyRateObj->getOne(array('currency' => $currencyRateUnit));
                        if (isset($currencyRateReader) && !empty($currencyRateReader['id'])) {
                            $currencyID = $currencyRateReader['id'];
                        }
                    }
                    // END Get dropdown ids  by name/value

                    $createBy = Yii::app()->session['user_id'];

                    $sql = "INSERT INTO expenses (location_id, department_id, project_id,currency_id,currency_rate,expense_name,user_id,description,created_by_user_id,total_expenses,calc_expenses,created_datetime,updated_datetime,expense_status) VALUES ('" . $locationID . "', '" . $departmentID . "','" . $projectID . "', '" . $currencyID . "', '" . $currencyRate . "', '" . $expenseTitle . "', '" . $userID . "','" . $expenseNotes . "', '" . $createBy . "', '" . $expensePrice . "', '" . $expensePrice . "','" . $createdDate . "','" . $createdDate . "','" . $status . "')";
                    $command = Yii::app()->db->createCommand($sql);
                    $sqlResult = $command->execute();
                    if (!empty($sqlResult)) {
                        $expenseID = Yii::app()->db->lastInsertID;
                        $sql = "INSERT INTO expense_details (expense_id, expense_type_id, vendor_id,expense_date,expense_price,tax_rate,expense_notes) VALUES ('" . $expenseID . "', '" . $expenseTypeID . "','" . $supplierID . "', '" . $expenseDate . "', '" . $expensePrice . "', '" . $taxRate . "', '" . $notes . "')";
                        $command = Yii::app()->db->createCommand($sql);
                        $sqlResult = $command->execute();
                        if (!empty($sqlResult)) {
                            Yii::app()->user->setFlash('success', '<strong>Success!</strong> Expenses data imported.');
                        } else {
                            Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
                        }
                    }
                }
                $row++;
            }
            fclose($handle);
        }
    }

    public function orderImport($filePath)
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);
        $row = 1;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($row > 1) {
                    $location           = addslashes(trim($data[0]));
                    $department         = addslashes(trim($data[1]));
                    $project            = addslashes(trim($data[2]));
                    $spendType          = addslashes(trim($data[3]));
                    $currencyRateUnit   = addslashes(trim($data[4]));
                    $currencyRate       = addslashes(trim($data[5]));
                    $orderDate          = $data[6];
                    $userFullName       = addslashes(trim($data[7]));
                    $orderNotes         = addslashes(trim($data[8]));
                    $productName        = addslashes(trim($data[9]));
                    $supplier           = addslashes(trim($data[10]));

                    $orderDate        = str_replace('/', '-', $orderDate);

                    $orderDate        = date('Y-m-d', strtotime($orderDate));

                    $productPrice     = trim($data[11]);
                    $quantity         = trim($data[12]);
                    $taxPer           = trim($data[13]);
                    $shipping         = trim($data[14]);
                    $taxAmount        = trim($data[15]);
                    $shippingCost     = trim($data[16]);
                    $totalPrice       = addslashes(trim($data[17]));
                    $status           = trim($data[18]);

                    // START Remove comma and currency symbol

                    $productPrice = preg_replace('/[^0-9-.]+/', '', $productPrice);
                    $taxPer = preg_replace('/[^0-9-.]+/', '', $taxPer);
                    $shipping = preg_replace('/[^0-9-.]+/', '', $shipping);
                    $taxAmount = preg_replace('/[^0-9-.]+/', '', $taxAmount);
                    $shippingCost = preg_replace('/[^0-9-.]+/', '', $shippingCost);
                    $totalPrice = preg_replace('/[^0-9-.]+/', '', $totalPrice);
                    $currencyRate = preg_replace('/[^0-9-.]+/', '', $currencyRate);

                    // End Remove comma and currency symbol

                    // START Get dropdown ids  by name/value
                    $supplierID = $locationID = $departmentID = $projectID = $userID = $productID =  $currencyID = $productID = '';

                    $createdDate        = date('Y-m-d H:i:s');

                    if (!empty($supplier)) {
                        $vendor = new Vendor();
                        $vendorReader = $vendor->getOne(array('vendor_name' => $supplier));
                        if (isset($vendorReader) && !empty($vendorReader['vendor_id'])) {
                            $supplierID = $vendorReader['vendor_id'];
                        }
                    }

                    if (!empty($location)) {
                        $locationObj = new Location();
                        $locationReader = $locationObj->getOne(array('location_name' => $location));
                        if (isset($locationReader) && !empty($locationReader['location_id'])) {
                            $locationID = $locationReader['location_id'];
                        }
                    }

                    if (!empty($department)) {
                        $departmentObj = new Department();
                        $departmentReader = $departmentObj->getOne(array('department_name' => $department));
                        if (isset($departmentReader) && !empty($departmentReader['department_id'])) {
                            $departmentID = $departmentReader['department_id'];
                        }
                    }

                    if (!empty($project)) {
                        $projectObj = new Project();
                        $projectReader = $projectObj->getOne(array('project_name' => $project));
                        if (isset($projectReader) && !empty($projectReader['project_id'])) {
                            $projectID = $projectReader['project_id'];
                        }
                    }

                    if (!empty($userFullName)) {
                        $userObj = new User();
                        $userReader = $userObj->getOne(array('full_name' => $userFullName));
                        if (isset($userReader) && !empty($userReader['user_id'])) {
                            $userID = $userReader['user_id'];
                        }
                    }

                    if (!empty($productName)) {
                        $productObj = new Product();
                        $productReader = $productObj->getOne(array('product_name' => $productName));
                        if (isset($productReader) && !empty($productReader['product_id'])) {
                            $productID = $productReader['product_id'];
                        }
                    }

                    if (!empty($currencyRateUnit)) {
                        $currencyRateObj = new CurrencyRate();
                        $currencyRateReader = $currencyRateObj->getOne(array('currency' => $currencyRateUnit));
                        if (isset($currencyRateReader) && !empty($currencyRateReader['id'])) {
                            $currencyID = $currencyRateReader['id'];
                        }
                    }
                    // END Get dropdown ids  by name/value

                    $createBy = Yii::app()->session['user_id'];

                    $sql = "INSERT INTO orders (location_id, department_id, project_id,spend_type,currency_id,currency_rate,order_date,user_id,vendor_id,total_price,calc_price,tax_rate,ship_amount,description,created_by_user_id,created_datetime,updated_datetime,order_status) VALUES ('" . $locationID . "', '" . $departmentID . "','" . $projectID . "', '" . $spendType . "', '" . $currencyID . "', '" . $currencyRate . "', '" . $orderDate . "', '" . $userID . "','" . $supplierID . "','" . $totalPrice . "','" . $totalPrice . "','" . $taxAmount . "','" . $shippingCost . "','" . $orderNotes . "','" . $createBy . "', '" . $createdDate . "','" . $createdDate . "','" . $status . "')";

                    $command = Yii::app()->db->createCommand($sql);
                    $sqlResult = $command->execute();
                    if (!empty($sqlResult)) {
                        $orderID = Yii::app()->db->lastInsertID;
                        $sql = "INSERT INTO order_details (order_id, product_id,vendor_id,quantity,unit_price,tax,shipping_cost) VALUES ('" . $orderID . "', '" . $productID . "','" . $supplierID . "', '" . $quantity . "', '" . $productPrice . "', '" . $taxPer . "', '" . $shipping . "')";
                        $command = Yii::app()->db->createCommand($sql);
                        $sqlResult = $command->execute();
                        Yii::app()->user->setFlash('success', '<strong>Success!</strong> Orders data imported.');
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
                    }
                }
                $row++;
            }
            fclose($handle);
        }
    }

    public function contractImport($filePath)
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);
        $row = 1;
        $checkRecords = 0;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            // Start: Check Count
            while (($data = fgetcsv($handle)) != FALSE) {

                if ($row > 1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]))) {
                    $checkRecords++;
                }
                $row++;
            }
        }

        if ($checkRecords > 100) {
            Yii::app()->user->setFlash('error', '<strong>Warning!</strong> You are trying to import more than 100 records. Only 100 or less can be uploaded at one time');
            $this->redirect(array('app/import'));
        }

        //End: Check Count
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            $row = 1;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                // $num = count($data);
                // echo '<pre>';
                // print_r($data);
                // exit;

                if ($row > 1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]))) {
                    $contract_title     = addslashes(trim($data[0]));
                    $contract_reference = addslashes(trim($data[1]));

                    $contract_start_date = trim($data[2]);
                    if (!empty($contract_start_date)) {
                        $contract_start_date = str_replace('/', '-', $contract_start_date);
                        $contract_start_date = date('Y-m-d', strtotime($contract_start_date));
                    }

                    $contract_end_date  = trim($data[3]);
                    if (!empty($contract_end_date)) {
                        $contract_end_date  = str_replace('/', '-', $contract_end_date);
                        $contract_end_date  = date('Y-m-d', strtotime($contract_end_date));
                    }

                    $break_clause       = trim($data[4]);
                    if (!empty($break_clause)) {
                        $break_clause       = str_replace('/', '-', $break_clause);
                        $break_clause       = date('Y-m-d', strtotime($break_clause));
                    }

                    $description        = trim($data[5]);
                    // $contract_value     = trim($data[6]);
                    // // START Remove comma and currency symbol
                    // $contract_value = preg_replace('/[^0-9-.]+/', '', $contract_value);
                    // End Remove comma and currency symbol
                    // START Get dropdown ids  by name/value
                    $supplierID = $locationID = $departmentID = $projectID = $userID = $categoryID = $subcategoryReaderID = $contracttypeID = $contractstatusID =  $currencyID = '';
                    $createdDate        = date('Y-m-d H:i:s');
                    $userID = Yii::app()->session['user_id'];
                    if (!empty($userID)) {
                        $userObj = new User();
                        $userReader = $userObj->getOne(array('user_id' => $userID));
                        if (isset($userReader) && !empty($userReader['user_id'])) {
                            $userName = $userReader['full_name'];
                        }
                    }

                    // END Get dropdown ids  by name/value
                    $sql = "INSERT INTO contracts (user_id,user_name,contract_title,contract_reference,contract_start_date,contract_end_date, break_clause,contract_description,created_datetime,updated_datetime)
                        VALUES ('" . $userID . "','" . $userName . "','" . $contract_title . "', '" . $contract_reference . "','" . $contract_start_date . "','" . $contract_end_date . "','" . $break_clause . "','" . $description . "','" . $createdDate . "','" . $createdDate . "')";
                    $command = Yii::app()->db->createCommand($sql);


                    $sqlResult = $command->execute();
                    $i = 1;
                    if (!empty($sqlResult)) {
                        $contractID = Yii::app()->db->lastInsertID;
                        $i++;
                        Yii::app()->user->setFlash('success', '<strong>Success!</strong> Contracts data imported.');
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
                    }
                }
                $row++;
            }
            fclose($handle);
        }
    }

    public function contractImportSession()
    {

        //End: Check Count
        $recordsImport = Yii::app()->session['contract_import'];
        foreach ($recordsImport as $key => $data) {

            $contract_title = $data['contract_title'];
            $contract_reference = $data['contract_reference'];
            $contract_start_date = $data['contract_start_date'];
            $contract_end_date = $data['contract_end_date'];
            $break_clause = $data['break_clause'];
            $description = $data['description'];

            // $contract_value     = trim($data[6]);
            // // START Remove comma and currency symbol
            // $contract_value = preg_replace('/[^0-9-.]+/', '', $contract_value);
            // End Remove comma and currency symbol
            // START Get dropdown ids  by name/value
            $supplierID = $locationID = $departmentID = $projectID = $userID = $categoryID = $subcategoryReaderID = $contracttypeID = $contractstatusID =  $currencyID = '';
            $createdDate        = date('Y-m-d H:i:s');
            $userID = Yii::app()->session['user_id'];
            if (!empty($userID)) {
                $userObj = new User();
                $userReader = $userObj->getOne(array('user_id' => $userID));
                if (isset($userReader) && !empty($userReader['user_id'])) {
                    $userName = $userReader['full_name'];
                }
            }

            // END Get dropdown ids  by name/value
            $sql = "INSERT INTO contracts (user_id,user_name,contract_title,contract_reference,contract_start_date,contract_end_date, break_clause,contract_description,created_datetime,updated_datetime)
                        VALUES ('" . $userID . "','" . $userName . "','" . $contract_title . "', '" . $contract_reference . "','" . $contract_start_date . "','" . $contract_end_date . "','" . $break_clause . "','" . $description . "','" . $createdDate . "','" . $createdDate . "')";
            $command = Yii::app()->db->createCommand($sql);


            $sqlResult = $command->execute();
            $i = 1;
            if (!empty($sqlResult)) {
                $contractID = Yii::app()->db->lastInsertID;
                $i++;
                Yii::app()->user->setFlash('success', '<strong>Success!</strong> Contracts data imported.');
            } else {
                Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
            }
        }
    }
    public function contractImportReview($filePath)
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);
        $row = 1;
        $checkRecords = 0;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            // Start: Check Count
            while (($data = fgetcsv($handle)) != FALSE) {

                if ($row > 1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]))) {
                    $checkRecords++;
                }
                $row++;
            }
        }

        if ($checkRecords > 100) {
            Yii::app()->user->setFlash('error', '<strong>Warning!</strong> You are trying to import more than 100 records. Only 100 or less can be uploaded at one time');
            $this->redirect(array('app/import'));
        }

        //End: Check Count
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            $row = 1;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                // $num = count($data);
                // echo '<pre>';
                // print_r($data);
                // exit;

                if ($row > 1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]))) {
                    $contract_title     = addslashes(trim($data[0]));
                    $contract_reference = addslashes(trim($data[1]));

                    $contract_start_date = trim($data[2]);
                    if (!empty($contract_start_date)) {
                        $contract_start_date = str_replace('/', '-', $contract_start_date);
                        $contract_start_date = date('Y-m-d', strtotime($contract_start_date));
                    }

                    $contract_end_date  = trim($data[3]);
                    if (!empty($contract_end_date)) {
                        $contract_end_date  = str_replace('/', '-', $contract_end_date);
                        $contract_end_date  = date('Y-m-d', strtotime($contract_end_date));
                    }

                    $break_clause       = trim($data[4]);
                    if (!empty($break_clause)) {
                        $break_clause       = str_replace('/', '-', $break_clause);
                        $break_clause       = date('Y-m-d', strtotime($break_clause));
                    }

                    $description        = trim($data[5]);
                    // $contract_value     = trim($data[6]);
                    // // START Remove comma and currency symbol
                    // $contract_value = preg_replace('/[^0-9-.]+/', '', $contract_value);
                    // End Remove comma and currency symbol
                    // START Get dropdown ids  by name/value
                    $supplierID = $locationID = $departmentID = $projectID = $userID = $categoryID = $subcategoryReaderID = $contracttypeID = $contractstatusID =  $currencyID = '';
                    $createdDate        = date('Y-m-d H:i:s');
                    $userID = Yii::app()->session['user_id'];
                    if (!empty($userID)) {
                        $userObj = new User();
                        $userReader = $userObj->getOne(array('user_id' => $userID));
                        if (isset($userReader) && !empty($userReader['user_id'])) {
                            $userName = $userReader['full_name'];
                        }
                    }

                    // END Get dropdown ids  by name/value
                    $sql = "INSERT INTO contracts (user_id,user_name,contract_title,contract_reference,contract_start_date,contract_end_date, break_clause,contract_description,created_datetime,updated_datetime)
                        VALUES ('" . $userID . "','" . $userName . "','" . $contract_title . "', '" . $contract_reference . "','" . $contract_start_date . "','" . $contract_end_date . "','" . $break_clause . "','" . $description . "','" . $createdDate . "','" . $createdDate . "')";
                    $command = Yii::app()->db->createCommand($sql);


                    $sqlResult = $command->execute();
                    $i = 1;
                    if (!empty($sqlResult)) {
                        $contractID = Yii::app()->db->lastInsertID;
                        $i++;
                        Yii::app()->user->setFlash('success', '<strong>Success!</strong> Contracts data imported.');
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Warning!</strong> Looks like some data not imported, please, contact to administrator.');
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        $this->render('import_contract_review', array('contract' => $contractID));
    }

    public function actionDeleteTemplate()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'import_admin';
        if (!isset(Yii::app()->session['admin_flag']) || Yii::app()->session['admin_flag'] != 1)
            $this->redirect(AppUrl::bicesUrl('app/importTemplates'));
        else if (!is_dir('uploads/imports'))
            mkdir('uploads/imports');

        $template_type = isset($_REQUEST['template_type']) ? $_REQUEST['template_type'] : "";
        if (!empty($template_type)) {
            $template_file_name = 'uploads/imports/' . $template_type . '~Template.csv';
            if (is_file($template_file_name))
                @unlink($template_file_name);
        }

        $this->redirect(AppUrl::bicesUrl('app/importTemplates'));
    }

    public function actionInstall()
    {
        // check if already some tables exist
        $table_found = false;
        $user = new User();
        $database_name = "";
        $database_info = $user->executeQuery("SELECT DATABASE() AS 'current_database_name'", true);
        if ($database_info && is_array($database_info) && isset($database_info['current_database_name']))
            $database_name = $database_info['current_database_name'];

        if (!empty($database_name)) {
            $table_info = $user->executeQuery("SELECT * FROM information_schema.tables WHERE table_schema = '$database_name' AND table_name = 'users'", true);
            if ($table_info && is_array($table_info) && isset($table_info['TABLE_NAME']) && $table_info['TABLE_NAME'] == 'users')
                $table_found = true;
        }

        if ($table_found)
            $this->redirect(AppUrl::bicesUrl('login'));
        else {
            $db_commands = file_get_contents('database_schema.sql');
            foreach (explode(";", $db_commands) as $a_query) {
                $a_query = preg_replace('!\s+!', ' ', $a_query);
                $a_query = rtrim(ltrim($a_query));
                if (!empty($a_query))
                    $user->executeQuery($a_query);
            }
            $this->redirect(AppUrl::bicesUrl('login'));
        }
    }

    public function actionExportChart()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        // is someone already logged in?

        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'export_chart';

        // few defaults from the session
        $user_selected_year = Yii::app()->session['user_selected_year'];
        $year_start_date = $user_selected_year . "-01-01";
        $current_date = $user_selected_year . "-12-31";

        // we need data for different dimensions                
        $preferred_vendors_used = $non_preferred_vendors_used = 0;
        $vendor = new Vendor();
        $report = new Report();
        $contract = new Contract();
        $department = new Department();
        $budget = new Budget();

        // which chart is to be exported?
        $dimension_name = 'Dimension Name';
        $export_data = array();
        $exportable_data = array();
        $canvas_id = isset($_REQUEST['canvas_id']) ? $_REQUEST['canvas_id'] : "";
        $location_id = isset($_REQUEST['location_id']) ? $_REQUEST['location_id'] : 0;
        $department_id = isset($_REQUEST['department_id']) ? $_REQUEST['department_id'] : 0;

        // get the export data for the chart needed
        switch ($canvas_id) {
            case 'spend_vendors':
            case 'top_vendors':
                $report->exportTopSuppliers();
                break;

            case 'spend_locations':
            case 'top_locations':
                $dimension_name = 'Locations';
                $export_data = $report->exportTopLocations($year_start_date, $current_date);
                break;

            case 'year_over_year':
                $dimension_name = 'Years';
                $export_data = $report->getReportTotals("", "2001-01-01", $current_date, "Y", "", "", "", 10);
                $sort_by_year = array();
                foreach ($export_data as $row_key => $a_year_data) {
                    if (isset($a_year_data['report_year']))
                        $sort_by_year[$row_key] = $a_year_data['report_year'];
                    else
                        $sort_by_year[$row_key] = 9999;
                }
                array_multisort($sort_by_year, SORT_ASC, $export_data);
                break;

            case 'year_to_date':
                $report->exportMonthlySpend();
                break;

            case 'top_contracts':
            case 'spend_contracts':
                $dimension_name = 'Contracts';
                $export_data = $contract->getTopContracts($year_start_date, $current_date);
                break;

            case 'expiring_contracts':
                $dimension_name = 'Contracts';
                foreach ($contract->getExpiringContracts() as $export_row) {
                    $exportable_data_row = array();
                    $exportable_data_row['month'] = $export_row['month_name'];
                    $exportable_data_row['count'] = $export_row['total'];
                    $exportable_data_row['contracts_value'] = $export_row['value'];
                    $exportable_data[] = $exportable_data_row;
                }
                break;

            case 'contracts_by_status':
                $dimension_name = 'Contracts';
                foreach ($contract->getContractStatusTotals($year_start_date, $current_date) as $export_row) {
                    $exportable_data_row = array();
                    $exportable_data_row['contract_status'] = $export_row['status'];
                    $exportable_data_row['count'] = $export_row['total'];
                    $exportable_data[] = $exportable_data_row;
                }
                break;

            case 'contracts_by_location':
                $dimension_name = 'Contracts';
                foreach ($contract->getContractLocationTotals($year_start_date, $current_date) as $export_row) {
                    $exportable_data_row = array();
                    $exportable_data_row['location'] = $export_row['location_name'];
                    $exportable_data_row['count'] = $export_row['total'];
                    $exportable_data[] = $exportable_data_row;
                }
                break;

            case 'contracts_by_category':
                $dimension_name = 'Contracts';
                foreach ($contract->getContractCategoryTotals($year_start_date, $current_date) as $export_row) {
                    $exportable_data_row = array();
                    $exportable_data_row['category'] = $export_row['category_name'];
                    $exportable_data_row['count'] = $export_row['total'];
                    $exportable_data[] = $exportable_data_row;
                }
                break;

            case 'vendors_by_category':
                $dimension_name = 'Suppliers';
                foreach ($vendor->getVendorsByCategory($year_start_date, $current_date) as $export_row) {
                    foreach ($export_row as $vendor_data) {
                        $exportable_data_row = array();
                        $exportable_data_row['category'] = $vendor_data['category'];
                        $exportable_data_row['vendor'] = $vendor_data['vendor_name'];
                        $exportable_data_row['amount'] = round($vendor_data['total'], 2);
                        $exportable_data[] = $exportable_data_row;
                    }
                }
                break;

            case 'vendors_by_month':
                $dimension_name = 'Suppliers';
                $export_data = $vendor->getVendorsByMonth($year_start_date, $current_date);
                ksort($export_data);
                foreach ($export_data as $export_key => $export_row) {
                    foreach ($export_row as $category_data) {
                        $exportable_data_row = array();
                        $exportable_data_row['month'] = date("M Y", strtotime($export_key . '01'));
                        $exportable_data_row['category'] = $category_data['category_name'];
                        $exportable_data_row['vendors_used'] = $category_data['vendor_count'];
                        $exportable_data[] = $exportable_data_row;
                    }
                }
                break;

            case 'vendor_combo_chart':
                $dimension_name = 'Suppliers';
                foreach ($vendor->getVendorsCountsAndAmounts($year_start_date, $current_date) as $export_row) {
                    $exportable_data_row = array();
                    $exportable_data_row['vendor'] = $export_row['vendor_name'];
                    $exportable_data_row['amount'] = round($export_row['total'], 2);
                    $exportable_data_row['count'] = $export_row['count'];
                    $exportable_data[] = $exportable_data_row;
                }
                break;

            case 'category_vendors_combo':
                $dimension_name = 'Suppliers';
                foreach ($vendor->getVendorsByCategory($year_start_date, $current_date) as $category_data) {
                    $exportable_data_row = array();

                    $total_for_category = 0;
                    $category_name = "";
                    foreach ($category_data as $category_vendor_row) {
                        $total_for_category += $category_vendor_row['total'];
                        $category_name = $category_vendor_row['category'];
                    }

                    $exportable_data_row['category'] = $category_name;
                    $exportable_data_row['count'] = count($category_data);
                    $exportable_data_row['amount'] = round($total_for_category, 2);
                    $exportable_data[] = $exportable_data_row;
                }
                break;

            case 'departments_by_spend_type':
                $dimension_name = 'Purchases';
                foreach ($department->getLocationsBySpendType($year_start_date, $current_date) as $export_row) {
                    foreach ($export_row as $location_data) {
                        $exportable_data_row = array();
                        $exportable_data_row['location'] = $location_data['location_name'];
                        $exportable_data_row['category'] = $location_data['category'];
                        $exportable_data_row['amount'] = round($location_data['total'], 2);
                        $exportable_data[] = $exportable_data_row;
                    }
                }
                break;

            case 'vendors_by_type':
                $dimension_name = 'Suppliers';
                $preferred_vendors_used = $non_preferred_vendors_used = 0;
                $preferred_vendors_used_pct = $non_preferred_vendors_used_pct = 0;
                foreach ($vendor->getVendorsByCategory($year_start_date, $current_date) as $category_data) {
                    foreach ($category_data as $category_vendor_row) {
                        if ($category_vendor_row['preferred_flag'])
                            $preferred_vendors_used += 1;
                        else
                            $non_preferred_vendors_used += 1;
                    }
                }

                // and hence independent preferred vs. non preferred counts
                $total_vendors_used = $preferred_vendors_used + $non_preferred_vendors_used;
                if ($total_vendors_used) {
                    $preferred_vendors_used_pct = ($preferred_vendors_used * 100) / $total_vendors_used;
                    $non_preferred_vendors_used_pct = ($non_preferred_vendors_used * 100) / $total_vendors_used;
                }

                $export_row = array();
                $export_row['vendor_type'] = 'Preferred';
                $export_row['count'] = $preferred_vendors_used;
                $export_row['percentage'] = $preferred_vendors_used_pct;
                $exportable_data[] = $export_row;

                $export_row = array();
                $export_row['vendor_type'] = 'Non Preferred';
                $export_row['count'] = $non_preferred_vendors_used;
                $export_row['percentage'] = $non_preferred_vendors_used_pct;
                $exportable_data[] = $export_row;

                $export_row = array();
                $export_row['vendor_type'] = 'Total';
                $export_row['count'] = $total_vendors_used;
                $export_row['percentage'] = 100;
                $exportable_data[] = $export_row;

                break;

            case 'invoices_paid_unpaid':
                $dimension_name = 'Invoices';
                $invoices_data = $report->getInvoicePaymentStatistics($year_start_date, $current_date);

                $export_row = array();
                $export_row['invoice_type'] = 'Paid';
                $export_row['count'] = $invoices_data['paid'];
                $exportable_data[] = $export_row;

                $export_row = array();
                $export_row['invoice_type'] = 'Unpaid';
                $export_row['count'] = $invoices_data['unpaid'];
                $exportable_data[] = $export_row;
                break;

            case 'locations_invoice_status':
                $dimension_name = 'Locations';
                foreach ($report->getLocationSpendStatistics($year_start_date, $current_date) as $export_row) {
                    $exportable_data_row = array();
                    $exportable_data_row['location'] = $export_row['location_name'];
                    $exportable_data_row['current'] = round($export_row['spend_total'], 2);
                    $exportable_data_row['future'] = round($export_row['spend_future'], 2);
                    $exportable_data[] = $exportable_data_row;
                }
                break;

            case 'orders_by_status':
                $dimension_name = 'OrderStatus';
                foreach ($report->getOrderStatusTotals($year_start_date, $current_date) as $export_status => $export_count) {
                    $exportable_data_row = array();
                    $exportable_data_row['order_status'] = $export_status;
                    $exportable_data_row['count'] = $export_count;
                    $exportable_data[] = $exportable_data_row;
                }
                break;

            case 'departments_by_monthly_spend':
                $dimension_name = 'BudgetAndSpent';

                $location = new Location();
                $location_data = $location->getOne(array('location_id' => $location_id));

                if (isset($_REQUEST['budget_year']) && !empty($_REQUEST['budget_year']))
                    $user_selected_year = $_REQUEST['budget_year'];

                $export_row = array();
                $export_row['location'] = $location_data['location_name'];
                foreach ($budget->getDepartmentSpendAndBudgets($user_selected_year, $location_id, $department_id) as $spend_and_budgets) {
                    $export_row['department'] = $spend_and_budgets['department_name'];
                    $export_row['budget'] = round($spend_and_budgets['budget'], 2);
                    $export_row['amount_spent'] = round($spend_and_budgets['spent'], 2);
                }
                $exportable_data[] = $export_row;

                break;

            case 'spend_departments':
                $dimension_name = 'TopDepartments';

                $location = new Location();
                $location_data = $location->getOne(array('location_id' => $location_id));

                foreach ($department->getYearlySortedData($location_id, $year_start_date, $current_date) as $department_data) {
                    $export_row = array();
                    $export_row['location'] = $location_data['location_name'];
                    $export_row['department'] = $department_data['department_name'];
                    $export_row['amount'] = round(str_replace(",", "", $department_data['total']), 2);
                    $export_row['percent'] = round($department_data['percent'], 2);
                    $exportable_data[] = $export_row;
                }

                break;

            case 'budgets_by_departments':
                $dimension_name = 'DepartmentsBudgets';

                $location = new Location();
                $location_data = $location->getOne(array('location_id' => $location_id));

                foreach ($department->getBudgetSpendData($location_id, $year_start_date, $current_date) as $department_data) {
                    $export_row = array();
                    $export_row['location'] = $location_data['location_name'];
                    $export_row['department'] = $department_data['department_name'];
                    $export_row['budget'] = round($department_data['budget'], 2);
                    $export_row['amount'] = round($department_data['spend'], 2);
                    $exportable_data[] = $export_row;
                }

                break;

            case 'vendors_by_departments':
                $dimension_name = 'DepartmentsCategories';

                $location = new Location();
                $location_data = $location->getOne(array('location_id' => $location_id));

                foreach ($department->getSpendByCategory($location_id, $year_start_date, $current_date) as $department_data) {
                    $export_row = array();
                    $export_row['location'] = $location_data['location_name'];
                    $export_row['department'] = $department_data['department_name'];
                    $export_row['category'] = $department_data['category'];
                    $export_row['amount'] = round($department_data['total'], 2);
                    $exportable_data[] = $export_row;
                }

                break;

            case 'department_vendors_combo':
                $dimension_name = 'DepartmentsSuppliers';

                $location = new Location();
                $location_data = $location->getOne(array('location_id' => $location_id));

                foreach ($department->getSpendByVendor($location_id, $year_start_date, $current_date) as $department_data) {
                    $export_row = array();
                    $export_row['location'] = $location_data['location_name'];
                    $export_row['department'] = $department_data['department_name'];
                    $export_row['amount'] = round($department_data['vendor_total'], 2);
                    $export_row['supplier_count'] = $department_data['vendor_count'];
                    $exportable_data[] = $export_row;
                }

                break;

            case 'supplier_type_area':
                $dimension_name = 'DepartmentsSuppliers';

                $location = new Location();
                $location_data = $location->getOne(array('location_id' => $location_id));

                foreach ($department->getVendorTypeCount($location_id, $year_start_date, $current_date) as $department_data) {
                    $export_row = array();
                    $export_row['location'] = $location_data['location_name'];
                    $export_row['department'] = $department_data['department_name'];
                    $export_row['preferred'] = $department_data['preferred'];
                    $export_row['non_preferred'] = $department_data['non_preferred'];
                    $export_row['supplier_count'] = $department_data['preferred'] + $department_data['non_preferred'];
                    $exportable_data[] = $export_row;
                }

                break;

            default:
                break;
        }

        //CVarDumper::dump($export_data,10,1);die;
        if (count($exportable_data) <= 0) {
            foreach ($export_data as $export_row) {
                $exportable_data_row = array();
                //if(!empty($export_row['total'])){
                $exportable_data_row['location'] = isset($export_row['location']) ? $export_row['location'] : '';
                $exportable_data_row['department'] = isset($export_row['department']) ? $export_row['department'] : '';
                $exportable_data_row['vendor'] = isset($export_row['vendor']) ? $export_row['vendor'] : '';
                $exportable_data_row['user'] = isset($export_row['user_name']) ? $export_row['user_name'] : '';
                $exportable_data_row[$dimension_name] = $export_row['dim_1'];
                $exportable_data_row['amount'] = round($export_row['total'], 2);
                $exportable_data_row['percentage'] = round($export_row['percent'], 4);
                $exportable_data[] = $exportable_data_row;
                // }
            }
        }

        $export = new Common();
        $export->export($exportable_data, $dimension_name);
    }

    public function actionCompanyDetails()
    {
        date_default_timezone_set(Yii::app()->params['timeZone']);
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'company_details';
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'], array(4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

        // CVarDumper::dump($_POST,10,1);die;

        // if the form is submitted, save values
        $form_submitted = Yii::app()->input->post('form_submitted');
        if ($form_submitted) {


            $company_details = new Company();

            $company_details_old = new Company();
            $company_details_old = $company_details->getOne();
            $company_details->delete();



            if (!empty($_FILES['company_logo']['name'])) {
                $img  = $_FILES['company_logo']['name'];
                $imgArr = explode(".", $img);
                $imgName = time() . '.' . end($imgArr);
                $path = Yii::getPathOfAlias('webroot') . '/uploads/companies';
                if (!is_dir($path)) mkdir($path);
                if (!is_dir($path . '/'))
                    mkdir($path . '/');

                if (move_uploaded_file($_FILES['company_logo']['tmp_name'], $path . '/' . $imgName)) {
                }
            } else {
                if (!empty($company_details_old['logo'])) {
                    $imgName = $company_details_old['logo'];
                } else {
                    $imgName = '';
                }
            }


            // if (!empty($_FILES['company_logo']['name']) && is_array($_FILES['company_logo']) && count($_FILES['company_logo']))
            // {  
            //     $company_details = new Company();
            //     $company = $company_details->getOne();
            //     // directory where this file resides
            //     // $complete_file_name = 'uploads/companies/' . $company['logo'];
            //     // if (is_file($complete_file_name)) @unlink($complete_file_name);
            //     // if (!is_dir('uploads/companies')) mkdir('uploads/companies');
            //     // move_uploaded_file($_FILES['company_logo']['tmp_name'], 'uploads/companies/' . $_FILES['company_logo']['name']);


            // }
            $company_details->rs = array();
            $company_details->rs['id'] = 0;
            $company_details->rs['company_name'] = Yii::app()->input->post('company_name');
            $company_details->rs['company_number'] = Yii::app()->input->post('company_number');
            $company_details->rs['address_1'] = Yii::app()->input->post('address_1');
            $company_details->rs['address_2'] = Yii::app()->input->post('address_2');

            //$company_details->rs['tax_number'] = Yii::app()->input->post('tax_number');
            $company_details->rs['city'] = Yii::app()->input->post('city');
            $company_details->rs['state'] = Yii::app()->input->post('state');
            $company_details->rs['zipcode'] = Yii::app()->input->post('zipcode');
            $company_details->rs['country'] = Yii::app()->input->post('country');
            $company_details->rs['phone_number'] = Yii::app()->input->post('phone_number');
            $company_details->rs['company_website'] = Yii::app()->input->post('company_website');

            //$company_details->rs['default_language_code'] = Yii::app()->input->post('default_language_code');
            $company_details->rs['currency_id'] = Yii::app()->input->post('currency_id');
            $company_details->rs['default_date_format'] = Yii::app()->input->post('default_date_format');
            $company_details->rs['default_time_format'] = Yii::app()->input->post('default_time_format');

            if (!empty($imgName)) {
                $company_details->rs['logo'] = $imgName;
            }

            $company_details->write();
            if (!empty($company_details->rs['currency_id'])) {
                $userID   = Yii::app()->session['user_id'];
                $userName = Yii::app()->session['full_name'];

                $sql = 'UPDATE currency_rates SET rate = 1, status=0, updated_at="' . date("Y-m-d H:i:s") . '",user_id="' . $userID . '",user_name="' . $userName . '" WHERE currency = "' . $company_details->rs['currency_id'] . '"';
                Yii::app()->db->createCommand($sql)->execute();
            }
        }

        // select existing rates
        $view_data = array();
        $company_details = new Company();
        $view_data['company_details'] = $company_details->getOne();
        if ($view_data['company_details'] && is_array($view_data['company_details'])) {
            Yii::app()->session['company_name'] = $view_data['company_details']['company_name'];
            Yii::app()->session['company_number'] = $view_data['company_details']['company_number'];
            Yii::app()->session['phone_number'] = $view_data['company_details']['phone_number'];
            Yii::app()->session['company_website'] = $view_data['company_details']['company_website'];

            Yii::app()->session['default_language_code'] = $view_data['company_details']['default_language_code'];

            //Yii::app()->session['user_currency'] = $view_data['company_details']['currency_id'];

            Yii::app()->session['user_currency_dropdown'] = $view_data['company_details']['currency_id'];
            Yii::app()->session['user_currency'] = $view_data['company_details']['currency_id'];

            Yii::app()->session['default_date_format'] = $view_data['company_details']['default_date_format'];

            Yii::app()->session['default_time_format'] = $view_data['company_details']['default_time_format'];
        }
        $currency_rate = new CurrencyRate();
        $view_data['currency_rate'] = $currency_rate->getAllCurrencyRate();
        // and display to the user
        $this->render('company_details', $view_data);
    }

    public function actionProfile()
    {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'profile';
        $user_id = Yii::app()->session['user_id'];
        $form_submitted = Yii::app()->input->post('form_submitted');
        $subdomain = FunctionManager::subdomainByUrl();
        if ($form_submitted) {
            if (!empty($_FILES['profile_img']['name'])) {
                $img  = $_FILES['profile_img']['name'];
                $imgArr = explode(".", $img);
                $imgName = time() . '.' . end($imgArr);
                $path = Yii::getPathOfAlias('webroot') . '/images/' . $subdomain;
                if (!is_dir($path)) mkdir($path);
                $path = $path . '/users';
                if (!is_dir($path)) mkdir($path);
                if (!is_dir($path . '/' . $user_id))
                    mkdir($path . '/' . $user_id);
                if (!is_dir($path . '/'))
                    mkdir($path . '/');
                if (move_uploaded_file($_FILES['profile_img']['tmp_name'], $path . '/' . $user_id . '/' . $imgName)) {
                }
            } else {
                $imgName = '';
            }

            $user = new User();
            $user->rs = array();
            $user->rs['user_id'] = $user_id;
            $user->rs['language_code'] = Yii::app()->input->post('language_code');
            $user->rs['full_name'] = Yii::app()->input->post('full_name');
            $user->rs['email'] = Yii::app()->input->post('email');
            $user->rs['location_id'] = !empty(Yii::app()->input->post('location_id')) ? Yii::app()->input->post('location_id') : '';
            $user->rs['department_id'] = !empty(Yii::app()->input->post('department_id')) ? Yii::app()->input->post('location_id') : '';
            $user->rs['contact_number'] = Yii::app()->input->post('contact_number');
            $user->rs['position'] = Yii::app()->input->post('position');

            if (!empty($imgName)) {
                $user->rs['profile_img'] = $imgName;
            }
            $user->write();
            Yii::app()->session['default_language_code'] = $user->rs['language_code'];
        }
        $user = new User();
        $view_data['user'] = $user->getUsers($user_id, $softDeleted = 2);
        $location = new Location();
        $view_data['locations'] = $location->getData(array('order' => 'location_name'));
        $view_data['locations_departments'] = $location->getLocationAndDepartment(array('order' => 'location_id'));
        $this->render('profile', $view_data);
    }

    public function actionDeleteProfileImg()
    {
        $user_id = Yii::app()->session['user_id'];
        $user = new User();
        $userData = $user->getOne(array('user_id' => $user_id));
        $imgName = $userData['profile_img'];

        $user->rs = array();
        $user->rs['user_id'] = $user_id;
        $user->rs['profile_img'] = '';
        $user->write();
        $subdomain = FunctionManager::subdomainByUrl();
        $path = Yii::getPathOfAlias('webroot') . '/images/' . $subdomain . '/users';
        $file =  $path . '/' . $user_id . '/' . $imgName;
        if (unlink($file)) {
        }

        $this->redirect(AppUrl::bicesUrl('app/profile'));
    }

    public function actionQrcode()
    {
        $user = new User();
        $user->rs = array();
        $user->rs['user_id'] = Yii::app()->session['user_id'];
        $user->rs['google_secret_code'] = '';
        $user->write();

        Yii::app()->user->setFlash('success', "QR code is reset successfully.");
        $this->redirect(AppUrl::bicesUrl('app/qrcodeRefresh'));
    }

    public function actionQrcodeRefresh()
    {
        $this->render('qrcode_refresh');
    }

    public function actionFeedback()
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        // make sure some one is logged in
        $this->checklogin();
        $this->layout = 'main';
        if (!empty($_POST['message'])) {

            $name = Yii::app()->session['full_name'];
            $company = Yii::app()->session['company_name'];
            $admin_email = Yii::app()->session['current_user_email'];
            $message = $_POST['message'];



            EmailManager::feedback($name, $admin_email, $company, $message);
            Yii::app()->user->setFlash('success', '<strong>Success! </strong>Feedback submitted successfully.');
        }

        $this->render('feedback', $view_data);
    }


    public function actionRaiseTicket()
    {
        error_reporting(0);
        date_default_timezone_set(Yii::app()->params['timeZone']);
        // make sure some one is logged in
        $this->checklogin();
        $this->layout = 'main';
        if (!empty($_POST['message'])) {

            $name = Yii::app()->session['full_name'];
            $admin_email = Yii::app()->session['current_user_email'];
            $message = $_POST['message'];
            $filePath = '';
            if (!empty($_FILES['file']['name'])) {
                $tmp_path = $_FILES["file"]["tmp_name"];
                $file_name = time() . '_' . $_FILES['file']['name'];
                $path = Yii::getPathOfAlias('webroot') . '/uploads/rise_ticket';
                if (!is_dir($path)) mkdir($path);

                if (move_uploaded_file($tmp_path, $path . '/' . $file_name)) {
                    $filePath = $path . '/' . $file_name;
                }
            }

            $company_data = new Company();
            $company_details = $company_data->getOne();
            $company = !empty($company_details['company_name']) ? $company_details['company_name'] : '';

            EmailManager::raiseTicket($name, $admin_email, $company, $message, $filePath);

            Yii::app()->user->setFlash('success', '<strong>Success </strong>- Your Ticket Has Been Raised Successfully');
        }

        $this->render('raise_ticket', $view_data);
    }
    //    public function actionExportChartWithDetail()
    //    {
    //        // is someone already logged in?
    //        $this->checklogin();
    //        $this->layout = 'main';
    //        $report = new Report();
    //        $report->export();
    //    }

    public function actionAccountManagement()
    {
        $this->render('account_management');
    }

    public function actionCancelSubscription()
    {

        $this->layout = 'main';
        $view_data = array();
        $error_message = '';
        $user_id = Yii::app()->session['user_id'];
        $name = Yii::app()->session['full_name'];
        $company = !empty(Yii::app()->session['company_name']) ? Yii::app()->session['company_name'] : '';
        $email = Yii::app()->session['current_user_email'];
        // print_r($user_id); exit;

        if (!empty($user_id)) {
            EmailManager::userCancelSubscription($name, $email, $company);
        }

        if (!empty($user_id)) {
            $cancelSubscription = 1;
        } else {
            $cancelSubscription = 0;
        }
        $data['subscription_check'] = $cancelSubscription;
        echo json_encode($data);
    }

    public function actionDeleteContractImport()
    {
        $recordArr = Yii::app()->session['contract_import'];
        $row_number = $_POST['row_number'];
        unset($recordArr[$row_number]);
        //echo "<pre>";print_r(12);exit;
        Yii::app()->session['contract_import'] = $recordArr;
        echo json_encode(array('msg' => 1));
    }

    public function actionDeleteVendorImport()
    {
        $recordArr = Yii::app()->session['vendor_import'];
        $row = $_POST['vendor_number'];
        unset($recordArr[$row]);
        Yii::app()->session['vendor_import'] = $recordArr;
        echo json_encode(array('msg' => 1));
    }

    public function actionEnvironment()
    {
        //create array of data to be posted
        /*$post_data = array();
        $post_data['username'] = 'Admin365';
        $post_data['password'] = '4ada02913a04a5fd961db8122b829da3';
        $post_data['action_login'] = 1;
        //traverse array and prepare data for posting (key1=value1)
        foreach ($post_data as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        //create the final string to be posted using implode()
        $post_string = implode ('&', $post_items);
        //create cURL connection
        $curl_connection = 
        curl_init('https://sandbox.oboloo.software/login');
        //set options
        curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
        //set data to be posted
        curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
        //perform our request
        $result = curl_exec($curl_connection);
        //show information regarding the request
        echo "<pre>";
        print_r($result);
        echo curl_errno($curl_connection). '-' .curl_error($curl_connection);
        //close the connection
        curl_close($curl_connection); */
        $this->render('environment');
    }

    public function actionSandboxUser()
    {
        $userDomain = $_POST['current_user_id'];
        if (!empty($userDomain)) {
            // echo $userDomain;
        } else {
            return "hello";
        }
    } 
    
    public function actionMicrosoftLogin() {
        // Construct the URL with necessary data as GET parameters
        $currentDomain = Yii::app()->request->serverName;
		$subdomain = strtok($currentDomain, '.');
        $redirectUrl = 'https://oboloo.software/microsoftLogin.php';
        $queryParams = http_build_query(array('action' => $_POST['action'], 'currentDomain' => $subdomain));
        $redirectUrl .= '?' . $queryParams;

        // Redirect the user to the constructed URL
        header('Location: ' . $redirectUrl);
        exit;
	}

    public function actionMicrosoftAutoLogin() {
        if(!empty($_GET['action'])){
            $getUser = new User(); // Assuming User is your Yii model
            $email = base64_decode($_GET['action']);
            $user_data = $getUser->getOne(['email' => $email]);
            
            if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id']) {
                if (isset($user_data['status']) && $user_data['status'] == 1) {
                    // Set up session variables indicating successful login
                    Yii::app()->session['user_id'] = $user_data['user_id'];
                    Yii::app()->session['user_type'] = $user_data['user_type_id'];
                    Yii::app()->session['member_type'] = $user_data['member_type'];
                    Yii::app()->session['admin_flag'] = $user_data['admin_flag'];
                    Yii::app()->session['full_name'] = $user_data['full_name'];
                    Yii::app()->session['current_user_email'] = $user_data['email'];
                    Yii::app()->session['default_language_code'] = !empty($user_data['language_code']) ? $user_data['language_code'] : "en";
                    // Also company information

                    Yii::app()->session['last_activity'] = time();
                    Yii::app()->session['expire_time'] = 30 * 60;
                    
                    $company_data = new Company(); // Assuming Company is your Yii model
                    $company_details = $company_data->getOne();
                    Yii::app()->session['company_number'] = $company_details['company_number'];
                    Yii::app()->session['user_currency_dropdown'] = $company_details['currency_id'];
                    Yii::app()->session['user_currency'] = $company_details['currency_id'];
                    Yii::app()->session['default_date_format'] = $company_details['default_date_format'];

                    Yii::app()->session['default_time_format'] = $company_details['default_time_format'];
                    if (!FunctionManager::checkEnvironment()) {
                        Yii::app()->session['live_subdomain'] = Yii::app()->input->post('user_domain_name');
                    }
                    $fa = 'yes';
                    if($fa=='yes'){
                        Yii::app()->session['2fa'] = $fa;
                    }
                    $redirectUrl = AppUrl::bicesUrl('home');
                    $this->redirect($redirectUrl);
                }
            }else{
                $this->redirect(AppUrl::bicesUrl('login', ['sso-error' => '"This user email does not exist in the database."']));
            }
        }else{
         $this->redirect(AppUrl::bicesUrl('login', ['sso-error' => '"This user email does not exist in the database."']));
        }
	}

}
