<?php

class BudgetController extends CommonController {

    public $layout = 'main';

    public function actionIndex() {
        $this->redirect(AppUrl::bicesUrl('budget/list'));
    }

    public function actionList() {
        
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'budget_list';

        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        // by default, show reports for year to date time frame
        $user_selected_year = Yii::app()->input->post('user_selected_year');

        if (empty($user_selected_year)) {
            if (isset(Yii::app()->session['user_selected_year']))
                $user_selected_year = Yii::app()->session['user_selected_year'];
            if (empty($user_selected_year))
                $user_selected_year = date("Y");
        }

        Yii::app()->session['user_selected_year'] = $user_selected_year;
        
        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(3,4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));

        // if form was submitted ... first save the data to the database
        $budget = new Budget();
        if (Yii::app()->input->post('form_submitted')) {
            //$budget->delete();

            $location_id = Yii::app()->input->post('location_id');
            $department_id = Yii::app()->input->post('department_id');
            $year = Yii::app()->input->post('year');
            $budgets = Yii::app()->input->post('budget');
            for ($idx = 0; $idx <= count($location_id); $idx++) {
                $budget_data = array('id' => 0);
                $budget_data['location_id'] = isset($location_id[$idx]) ? $location_id[$idx] : 0;
                $budget_data['department_id'] = isset($department_id[$idx]) ? $department_id[$idx] : 0;
                $budget_data['year'] = isset($year[$idx]) ? $year[$idx] : 0;
                $budget_data['budget'] = isset($budgets[$idx]) ? $budgets[$idx] : 0;

                
                if (!empty($budget_data['location_id']) || !empty($budget_data['department_id']) || !empty($budget_data['year']) || !empty($budget_data['budget'])) {
                    // do not allow duplicates
                    $command = Yii::app()->db->createCommand('delete from budgets where location_id='.$budget_data['location_id'].' and year='.$budget_data['year']);
                    if($command->query()){

                    }
                }
            }
            for ($idx = 0; $idx <= count($location_id); $idx++) {
                $budget_data = array('id' => 0);
                $budget_data['location_id'] = isset($location_id[$idx]) ? $location_id[$idx] : 0;
                $budget_data['department_id'] = isset($department_id[$idx]) ? $department_id[$idx] : 0;
                $budget_data['year'] = isset($year[$idx]) ? $year[$idx] : 0;
                $budget_data['budget'] = isset($budgets[$idx]) ? $budgets[$idx] : 0;

                if (!empty($budget_data['location_id']) || !empty($budget_data['department_id']) || !empty($budget_data['year']) || !empty($budget_data['budget'])) {
                    // do not allow duplicates
                    $search_duplicate = array();
                    $search_duplicate['location_id'] = $budget_data['location_id'];
                    $search_duplicate['department_id'] = $budget_data['department_id'];
                    $search_duplicate['year'] = $budget_data['year'];
                   // $budget->delete($search_duplicate);
                   /* $command = Yii::app()->db->createCommand('delete from budgets where location_id='.$budget_data['location_id'].' and year='.$budget_data['year']);
                    if($command->query()){

                    }*/
                    //

                    // and save only single instances
                    $budget->saveData($budget_data);
                }
            }


            $this->redirect(AppUrl::bicesUrl('budget'));
        }

        // need some drop down values
        $view_data = array();
        $location = new Location();
        $view_data['locations'] = $location->getData(array('order' => 'location_name'));

        $view_data['budgets'] = $budget->getBudgets();
        $this->render('list', $view_data);
    }

    public function actionGetDepartmentSpendData() {
        $this->checklogin();
        $this->layout = 'main';

        $year = Yii::app()->input->post('year');
        $location_id = Yii::app()->input->post('location_id');
        $department_id = Yii::app()->input->post('department_id');

        $budget = new Budget();
        $spend_and_budgets = $budget->getDepartmentSpendAndBudgets($year, $location_id, $department_id);

        echo json_encode($spend_and_budgets);
    }

    public function actionAnalysis() {
        // is someone already logged in?
        $this->checklogin();
        $this->layout = 'main';
        $this->current_option = 'budget_analysis';

        // by default, show reports for year to date time frame
        $user_selected_year = Yii::app()->session['user_selected_year'];
        if (empty($user_selected_year))
            $user_selected_year = date("Y");
        $year_start_date = $user_selected_year . "-01-01";
        $current_date = $user_selected_year . "-12-31";

        // and also currency preference by the user
        $user_currency = Yii::app()->input->post('user_currency');
        // get currency on the base of posted value
        $this->getCurrency($user_currency);

        // get top vendors, top locations and top categories data for the time being
        $view_data = array();
        $budget = new Budget();
        $view_data['metrics']['budget'] = $budget->getDashboardMetrics($year_start_date, $current_date);

        $spend = new Report();
        $view_data['metrics']['spend'] = $spend->getDashboardMetrics($year_start_date, $current_date);

        $location = new Location();
        $view_data['locations'] = $location->getData(array('order' => 'location_name'));

        $this->render('analysis', $view_data);
    }

    public function actionDelete() {
        
        $budgetID = $_GET['budget-id'];

         if (empty($budgetID))
            $this->redirect(AppUrl::bicesUrl('budget/list'));

        $command = Yii::app()->db->createCommand();
        $command->delete('budgets', 'id=:id', array(':id'=>$budgetID));
            $this->redirect(AppUrl::bicesUrl('budget/list'));
        

    }

}
