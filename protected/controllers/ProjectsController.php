<?php

class ProjectsController extends CommonController
{
	public $layout = 'main';

	public function actionView()
	{
		$this->checklogin();
		$this->current_option = 'projects';
		$user_id = Yii::app()->session['user_id'];
		$projectID = $_GET['project-id'];
		$view_data = array();

		$location = new Location();
		$locations = $location->getData(array('order' => 'location_name'));
		$view_data['locations'] = $locations;

		$department = new Department();
		$departments= $department->getData(array('order' => 'department_name'));
		$view_data['departments'] = $departments;

		$projectObj = new Project;
		$project = $projectObj->getOne(array('project_id'=>$projectID));
		$view_data['project'] = $project;

		$user = new User();
		$users = $user->getData(array('order' => 'full_name'));
		$view_data['users'] = $users;

		$createdByUser = $user->getOne(array('user_id' => $project['created_by']));
		$view_data['createdByUser'] = $createdByUser;
		$ownerUser = $user->getOne(array('user_id' => $project['owner']));
		$view_data['ownerUser'] = $ownerUser;

		$view_data['projectID'] = $projectID;

			// by default, show reports for year to date time frame
		$user_selected_year = Yii::app()->input->post('user_selected_year');

		if (empty($user_selected_year)) {
			if (isset(Yii::app()->session['user_selected_year']))
				$user_selected_year = Yii::app()->session['user_selected_year'];
			if (empty($user_selected_year))
				$user_selected_year = date("Y");
		}
		$year_start_date = $user_selected_year . "-01-01";
		$current_date = $user_selected_year . "-12-31";

		$order = new Order();
		$view_data['orders'] = $order->getProjectOrders($projectID,$year_start_date, $current_date);

		$view_data['projects_combo'] = $projectObj->getProjectOrderAndAmounts($projectID,$year_start_date, $current_date);

		$contract = new Contract();
		$view_data['contracts'] = $contract->getProjectContracts($projectID,$year_start_date, $current_date);

		$quote = new Quote();
		$view_data['quotes'] = $quote->getProjectQuotes($projectID);

		$view_data['contracts_by_location'] = $contract->getProjectContractLocationTotals($projectID,$year_start_date, $current_date);


		$this->render('view', $view_data);
	}
	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('projects/list'));
	}
	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'projects';

		$user_id = Yii::app()->session['user_id'];

        if (!isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	

		// get list of all departments (with related locations too)
		$project = new Project();
		$projects = $project->getProjectsWithLocations();

		// which location has which department active? 
		$view_data = array();
		$location = new Location();
		$view_data['locations'] = $location->getData(array('order' => 'location_name'));

		$department = new Department();
		$view_data['departments'] = $department->getData(array('order' => 'department_name'));

		$user = new User();
		$view_data['users'] = $user->getData(array('order' => 'full_name'));
		$view_data['user'] = $user->getOne(array('user_id' => $user_id));
		
		// and display
		$view_data['projects'] = $projects;
		$this->render('list', $view_data);
	}

	public function actionlistAjax()
	{
		 if (! isset(Yii::app()->session['user_type']) || !in_array(Yii::app()->session['user_type'],array(1,3,4)))
            $this->redirect(AppUrl::bicesUrl('orders/list'));
        
        $location = new Location();
		$locations = $location->getData(array('order' => 'location_name'));

		$department = new Department();
		$departments= $department->getData(array('order' => 'department_name'));

		$user = new User();
		$users = $user->getData(array('order' => 'full_name'));

        $requestData= $_REQUEST;
        $action= !empty($requestData['url_action'])?$requestData['url_action']:""; 
        $search_value = $requestData['search']['value'];

        $columns = array( 
            // datatable column index  => database column name
            0=>'project_id',
            1=>'project_name',
            2=>'loc.location_name',
            3=>'dep.department_name', 
            4=>'cre.full_name', 
            5=>'own.full_name',
            6=>'pstatus',
            7=>'',
        );

        $sql = "SELECT pro.project_name,pro.status,pro.created_at,pro.project_id,loc.location_name,dep.department_name,cre.full_name as created_by_user,own.full_name as owner_user,loc.location_id,dep.department_id,pro.owner,pro.created_by,pro_status.value as pstatus
        FROM `projects` pro
        left join locations loc on pro.location_id=loc.location_id
        left join departments dep on pro.department_id=dep.department_id
        left join users cre on pro.created_by=cre.user_id
        left join users own on pro.owner=own.user_id
        left join project_status pro_status on pro.status=pro_status.id";
 

        /*if search is applicable*/
        if(!empty($search_value)){
          $sql.=' where project_name like "%'.$search_value.'%"';
          $sql.=' OR pro_status.value like "%'.$search_value.'%"';
          $sql.=' OR location_name like "%'.$search_value.'%"'; 
          $sql.=' OR department_name like "%'.$search_value.'%"';
          $sql.=' OR cre.full_name like "%'.$search_value.'%"'; 
          $sql.=' OR own.full_name like "%'.$search_value.'%"'; 
          $sql.=' OR project_id like "%'.$search_value.'%"';  
        }

    
        /*search ends.*/
        $endLimit = $requestData['length'];
        
        if(empty($requestData['order'][0]['column'])){
            $requestData['order'][0]['column'] = 1;
            $requestData['order'][0]['dir'] = 'desc';
        }

        $order =$columns[$requestData['order'][0]['column']];
        //$order = "p.product_id DESC"; 
        $order =" ORDER BY ".$order." ".$requestData['order'][0]['dir'];
        $sql.=$order;
              
  
       
        
        $sql.=" LIMIT ".$requestData['start']." ,".$endLimit;

        $projects = Yii::app()->db->createCommand($sql)->queryAll();
        $totalData = count($projects);
        
        // when there is no search parameter then total number 
     
        $data=[];
  
        if(!empty($projects)){
           foreach ($projects as $proValue) 
            {
                $nestedData=[];
                
                $projectID=$proValue['project_id'];
                $projectName=$proValue['project_name'];
                $location=$proValue['location_name'];
                $department=$proValue['department_name'];
                $createdBy=$proValue['created_by_user'];
                $owner_user =$proValue['owner_user'];
                $status=$proValue['pstatus'];
                $createdAt=date("d/m/Y",strtotime($proValue['created_at']));

				if (strtolower($status) == 'Scoping'){
					$buttonClass = 'btn-info';
				}else if (strtolower($status) == 'complete'){
				  $buttonClass = 'btn-success';
				}else if (strtolower($status) == 'in progress'){
				  $buttonClass = 'btn-warning';
				}else{
				  $buttonClass = 'btn-info';
				}

                $nestedData[]=$projectID;
                $nestedData[]=$projectName;
                $nestedData[]=$location;
                $nestedData[]=$department;
                $nestedData[]=$createdBy;
                $nestedData[]= $owner_user;
                $nestedData[]='<button class="btn btn-sm '.$buttonClass.'" title="'.$status.'">'.$status.'</button>';

                $locOption='';
                foreach($locations as $location) {
                	$locOption .='<option value="'.$location['location_id'].'" '.(isset($proValue['location_id']) && $location['location_id'] == $proValue['location_id']?'selected="SELECTED"':"").' >';
                	$locOption .=$location['location_name'].' </option>';
                } 

                $depOption='';
                foreach($departments as $depValue) {
                	$depOption .='<option value="'.$depValue['department_id'].'" '.(isset($proValue['department_id']) && $depValue['department_id'] == $proValue['department_id']?'selected="SELECTED"':"").' >';
                	$depOption .=$depValue['department_name'].' </option>';
                } 

               
                $ownOption=$createdByOption='';
                foreach($users as $userValue) {
                	$ownOption .='<option value="'.$userValue['user_id'].'" '.(isset($proValue['owner']) && $userValue['user_id'] == $proValue['owner']?'selected="SELECTED"':"").'>';
                	$createdByOption .='<option value="'.$userValue['user_id'].'" '.(isset($proValue['created_by']) && $userValue['user_id'] == $proValue['created_by']?'selected="SELECTED"':"").' >';

                	$ownOption .=$userValue['full_name'].' </option>';
                	$createdByOption .=$userValue['full_name'].' </option>';
                } 

                $statusOption='';
                $projectStatus = FunctionManager::projectStatus();
                foreach ($projectStatus as $key=>$status) {
               		$statusOption .='<option value="'.$key.'" '.($key == $proValue['status']?'selected="SELECTED"':"").'>'.$status.'</option>';
                }

              	$popUpID = "pro".$projectID;
              	$formID = "edit".$projectID;
                $actionLink='<a data-toggle="modal" data-target="#'.$popUpID.'" href="#"><i class="fa fa-edit" style="font-size:18px"></i></a>&nbsp;&nbsp;&nbsp;
                	<a href="'.Yii::app()->createUrl('projects/view',array('project-id'=>$projectID)).'"><i class="fa fa-eye" style="font-size:18px"></i></a>';

           
                $infoModal = '<div id="'.$popUpID.'" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Project Detail</h4></div>
                	<div class="modal-body"><form id="'.$formID.'" action="" method="post">
                	 <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Project Name <span style="color: #a94442;">*</span></label>
                	<input type="text" class="form-control" name="project_name" id="project_name_modal" placeholder="Project Name" value="'.$projectName.'" required="required" />
                	<input type="hidden"  name="project_id" id="project_id"value="'.$projectID.'"  /></div></div><div class="clearfix"></div>
                	<div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid"><label class="control-label">Location <span style="color: #a94442;">*</span></label><select name="location_id" id="location_id" class="form-control" data="required" onchange="loadDepartmentsForSingleLocation(this);"><option value="">Select Location</option>'.$locOption.'</select></div></div><div class="clearfix"></div>
                	<div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12 valid"><label class="control-label">Department <span style="color: #a94442;">*</span></label><select name="department_id" id="department_id_detail" class="form-control"><option value="">Select Department</option>'.$depOption.'</select></div></div><div class="clearfix"></div>
                	<div class="form-group">
                	<div class="col-md-12 col-sm-12 col-xs-12 "><label class="control-label">Project Owner</label>
                	<select class="form-control" name="owner" id="owner">'.$ownOption.'</select></div></div><div class="clearfix"></div>
                	<div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12"><label class="control-label">Created by</label>
                	<input type="text" class="form-control" name="created_by" value="'.$createdBy.'" readonly="readonly" />
                	<input type="hidden" class="form-control" name="created_by" id="created_by" value="'.$proValue['created_by'].'" readonly="readonly" /></div></div><div class="clearfix"></div>
                	 <div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12 "><label class="control-label">Project Status</label><select class="form-control" name="status" id="status">'.$statusOption.'</select></div></div><div class="clearfix"></div>
                	<div class="form-group"> <div class="col-md-12 col-sm-12 col-xs-12  date-input">
                	 <label class="control-label">Created on</label>
                	 <input type="text" class="form-control" name="created_at" id="created_at"
                    value="'.$createdAt.'" readonly="readonly"></div></div></div></form>
                    <div class="edit_msg_area"></div>
                	<div class="modal-footer"><button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button> <button  type="button" class="btn btn-primary btn sm" onclick="editProject('.$formID.');">Edit</button></div></div</div></div>style
                	<style>
                	.control-label {min-width: 22% !important;}
                	.form-control {width: 50%  !important;}
                	.form-group {margin-bottom: 14px !important; width: 100%;}
                	</style>

                	';
                 $nestedData[]=$actionLink.$infoModal;
                $data[] = $nestedData;
            }
        }
            
        /*end create array for data tables.*/
        $json_data = array( 
        "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
        "recordsTotal"    => intval( $totalData ),  // total number of records
        "recordsFiltered" => intval( $totalData ), // total number of records 
        "data"            => $data  // total data array
        ); 
        echo  json_encode($json_data); 
	}

	
	public function actionSave()
	{
		
		$this->checklogin();
        if (!in_array(Yii::app()->session['user_type'],array(1,3,4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	

		$projectName = $_POST['project_name'];
		$locationID = $_POST['location_id'];
		$departmentID = $_POST['department_id'];
		$owner = $_POST['owner'];
		$createdBy = $_POST['created_by'];
		$status = $_POST['status'];
		$created_at = $_POST['created_at'];

		$project = new Project();
		$project->rs = array();
		$project->rs['project_name'] = $projectName;
		$project->rs['location_id'] = $locationID;
		$project->rs['department_id'] = $departmentID;
		$project->rs['owner'] = $owner;
		$project->rs['created_by'] = $createdBy;
		$project->rs['status'] = $status;
		$project->rs['created_at'] = date('Y-m-d',strtotime($created_at));
		$project->write();
		$locations = isset($_REQUEST['locations']) ? $_REQUEST['locations'] : "";
		$departments = isset($_REQUEST['departments']) ? $_REQUEST['departments'] : "";
		$project->saveLocations($project->rs['project_id'], $locations,$departments);
		
		$location = new Location();
		$location_names = array();
		foreach ($location->getData() as $location_data)
			$location_names[$location_data['location_id']] = $location_data['location_name'];

		$department = new Department();
		$department_names = array();
		foreach ($department->getData() as $department_data)
			$department_names[$department_data['department_id']] = $department_data['department_name'];
		
		$new_locations_text = "";
		if (!empty($locations))
		{
			foreach (explode(",", $locations) as $location_id)
			{
				if (isset($location_names[$location_id]))
				{
					if ($new_locations_text == "") $new_locations_text = $location_names[$location_id];
					else $new_locations_text = $new_locations_text . ", " . $location_names[$location_id];
				}
			}
		}

		$new_departments_text = "";
		if (!empty($departments))
		{
			foreach (explode(",", $departments) as $department_id)
			{
				if (isset($department_names[$department_id]))
				{
					if ($new_departments_text == "") $new_departments_text = $department_names[$department_id];
					else $new_departments_text = $new_departments_text . ", " . $department_names[$department_id];
				}
			}
		}
		
		echo json_encode(array('new_project_id' => $project->rs['project_id'], 'new_locations_text' => $new_locations_text,'new_departments_text' => $new_departments_text));
	}


	public function actionEdit()
	{
		
		/*$this->checklogin();
        if (!in_array(Yii::app()->session['user_type'],array(1,3,4)))
			$this->redirect(AppUrl::bicesUrl('orders/list'));	*/

		
		$projectID = $_POST['project_id'];
		$projectName = $_POST['project_name'];
		$locationID = $_POST['location_id'];
		$departmentID = $_POST['department_id'];
		$owner = $_POST['owner'];
		$createdBy = $_POST['created_by'];
		$status = $_POST['status'];
		$created_at = $_POST['created_at'];

		$project = new Project();
		$rs = array();
		$rs['project_id'] = $projectID;
		$rs['project_name'] = $projectName;
		$rs['location_id'] = $locationID;
		$rs['department_id'] = $departmentID;
		$rs['owner'] = $owner;
		$rs['created_by'] = $createdBy;
		$rs['status'] = $status;
		//$rs['created_at'] = date('Y-m-d',strtotime($created_at));
		$project->saveData($rs);
		
		echo json_encode(array('msg' => 'Edited Successfully'));
	}

	public function actionGetDepartmentChartData()
	{
		$this->checklogin();

		// by default, show reports for year to date time frame
		$user_selected_year = Yii::app()->input->post('user_selected_year');
		if (empty($user_selected_year)) $user_selected_year = date("Y");		
		$year_start_date = $user_selected_year . "-01-01";
		$current_date = $user_selected_year . "-12-31";

		// everything is by location now
		$department = new Department();
		$location_id = Yii::app()->input->post('location_id');
		if (empty($location_id)) $location_id = 0;

		// budget and spend for this year
		$return_data['budget_data'] = $department->getBudgetSpendData($location_id, $year_start_date, $current_date);
		
		// top departments for this year
		$return_data['top_departments'] = $department->getYearlySortedData($location_id, $year_start_date, $current_date);
				
		// departments by category
		$return_data['departments_by_category'] = $department->getSpendByCategory($location_id, $year_start_date, $current_date);
				
		// vendors by departments
		$return_data['department_vendors_combo'] = $department->getSpendByVendor($location_id, $year_start_date, $current_date);
				
		// vendors by departments
		$return_data['department_vendors_type'] = $department->getVendorTypeCount($location_id, $year_start_date, $current_date);
				
		// metrics on the dashboard
		$return_data['top_metrics'] = $department->getDashboardMetrics($location_id, $year_start_date, $current_date);
		
		// return all data together
		echo json_encode($return_data);
	}

	public function actionGetDepartmentModalData()
	{
		$this->checklogin();

		// by default, show reports for year to date time frame
		$user_selected_year = Yii::app()->input->post('user_selected_year');
		if (empty($user_selected_year)) $user_selected_year = date("Y");
		$year_start_date = $user_selected_year . "-01-01";
		$current_date = $user_selected_year . "-12-31";

		// everything is by location now
		$department = new Department();
		$location_id = Yii::app()->input->post('location_id');
		if (empty($location_id)) $location_id = 0;

		// budget and spend for this year
		$return_data['budget_data'] = $department->getBudgetSpendData($location_id, $year_start_date, $current_date);
		$return_data['avg_suppliers'] = $department->getDepartmentAvgSuppliers($location_id, $year_start_date, $current_date);
		$return_data['avg_categories'] = $department->getSupplierAvgCategories($location_id, $year_start_date, $current_date);

		// return all data together
		echo json_encode($return_data);
	}

}
