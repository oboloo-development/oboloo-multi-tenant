<?php

class NotificationsController extends CommonController
{
	public $layout = 'main';

	public function actionIndex()
	{
		$this->redirect(AppUrl::bicesUrl('notifications/list'));
	}

	public function actionList()
	{
		// is someone already logged in?
		$this->checklogin();
        $this->layout = 'main';
		$this->current_option = 'notifications';

		// may be we need only notifications for a certain period
		$from_date = Yii::app()->input->post('from_date');
		$to_date = Yii::app()->input->post('to_date');
		$notification_filter = Yii::app()->input->post('notification_filter');
		
		$db_from_date = "";
		if (!empty($from_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $from_date);
	       	$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['from_date'] = $db_from_date;
		}

		$db_to_date = "";
		if (!empty($to_date))
		{
	       	list($input_date, $input_month, $input_year) = explode("/", $to_date);
	       	$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['to_date'] = $db_to_date;
		}

		$notification_type ="";
		if(!empty($notification_filter)){
			$view_data['notification_type'] = $notification_filter;
		}
		
		// and display various approvals to the user
		$notification = new Notification();
		$view_data['notifications'] = $notification->getMyNotifications(true, $db_from_date, $db_to_date, $notification_filter);
		$this->render('list', $view_data);
	}

	public function actionDeleteNotifications()
	{
		$notifications_to_delete = isset($_REQUEST['notification_ids_to_delete']) ? $_REQUEST['notification_ids_to_delete'] : "0";
		$notification = new Notification();		
		$notification->deleteNotifications($notifications_to_delete);
		$this->redirect(AppUrl::bicesUrl('notifications/list'));		
	}

}
