<?php $tool_currency = Yii::app()->session['user_currency'];
$user_currency_symbol = Yii::app()->session['user_currency_symbol'];
$expLabel = $expSeriesTot = $expSeriesVal =  array();
$quteLocName = rtrim($quteLocName,",");
$quteCount = rtrim($quteCount,","); 

foreach ($expiring_contracts as $key=>$expiring_contract_row) {
        $expLabel[] = addslashes($key); 
        $expSeriesCount[] = $expiring_contract_row['expiry_count'];
        $expSeriesNoticeCount[] = $expiring_contract_row['noticeperiod_count'];
  }

$locSeries1 = $locSeries2 = $locSeries3 = $locSeries4 = array();
foreach ($contracts_by_location as $a_contract) {
  $locName = addslashes($a_contract['location_name']);
  $locLabel[] = $locName;
  $locSeries1[$locName]= $a_contract['End Date Within 30 Days'];
  $locSeries2[$locName]= $a_contract['End Date Within 3 Months'];
  $locSeries3[$locName]= $a_contract['Passed Contract End Date'];
  $locSeries4[$locName]= $a_contract['End Date Over 3 Months'];
}

$s1Sum = array_sum($locSeries1);
$s2Sum = array_sum($locSeries2);
$s3Sum = array_sum($locSeries3);
$s4Sum = array_sum($locSeries4);

$locSeries = array('End Date Within 30 Days'=>$s1Sum,'End Date Within 3 Months'=>$s2Sum,'Passed Contract End Date'=>$s3Sum,'End Date Over 3 Months'=>$s4Sum);
arsort($locSeries);
$ctr = 1;
foreach($locSeries as $key=>$value){

    if($key=="End Date Within 30 Days"){
      $locSeries["End Date Within 30 Days"] = $locSeries1;
    }else if($key=="End Date Within 3 Months"){
      $locSeries["End Date Within 3 Months"] = $locSeries2;
    }else if($key=="Passed Contract End Date"){
      $locSeries["Passed Contract End Date"] = $locSeries3;
    }else if($key=="End Date Over 3 Months"){
      $locSeries["End Date Over 3 Months"] = $locSeries4;
    }

    if($ctr == 1){
      arsort($locSeries[$key]);
      $locNameArr = array_keys($locSeries[$key]);
      $locLabel = $locNameArr;
    }

    $ctrInner=0;
    foreach($locNameArr as $locKey=>$LocArr){
       $orderValue = $locSeries[$key][$LocArr];
       unset($locSeries[$key][$LocArr]);
       $locSeries[$key][$ctrInner] = $orderValue;
        $ctrInner++;
    }
    $ctr++;
}
$colorArr = array();
$colorArr1  = array_keys($locSeries);
$colorArr[array_keys($colorArr1,'End Date Within 30 Days')[0]] = '#f7778c';
$colorArr[array_keys($colorArr1,'End Date Within 3 Months')[0]] = '#efa65f';
$colorArr[array_keys($colorArr1,'Passed Contract End Date')[0]] = '#aaa';
$colorArr[array_keys($colorArr1,'End Date Over 3 Months')[0]] = '#48d6a8';
ksort($colorArr);
 
$quteLocName = $quteCount = '';
foreach ($location_quote_count as $value) { 
        $quteLocName .= '"'.$value['location_name'].'",';
        $quteCount .= $value['total_quotes'].',';
  }

 $quteLocName = rtrim($quteLocName,",");
 $quteCount = rtrim($quteCount,","); 
?>
<div class="right_col" role="main" >

    <div class="row-fluid tile_count nomargin-topbottom homebtn-screen-cont">
        <div class="span6"><br /><br />
            <h3>
            	Hello <?php echo Yii::app()->session['full_name'];?> 
            </h3> <br  />
            <h3>
               
            </h3>
        </div>
        <div class="clearfix"> </div> <br />
  <!-- <div class="col-md-2 col-sm-2 col-xs-12">
    <a href="https://oboloo.com/support-hub/" class="btn btn-success round-shape"  class="home_heading"  target="_blank">User Guides</a>
  </div> -->
  <div class="col-md-2 col-sm-2 col-xs-12">
    <!-- <a href="<?php //echo AppUrl::bicesUrl('app/raiseTicket'); ?>"  class="btn btn-orange round-shape" target="_blank">Raise a Ticket</a> -->
  </div>
 </div>
<div class="clearfix"></div> <br /><br />
  <div class="row home-graphs">
    <div class="clearfix"></div>
   <div class="col-md-6 col-sm-6 col-xs-12">
     <div class="clearfix"></div>
              <div class="x_panel notification_home">
                <div class="x_title">
                  <h2>Recent Notifications </h2>
                  <h2 class="pull-right"><a href="<?php echo AppUrl::bicesUrl('notifications/list'); ?>" class="btn btn-success expiring-contract-btn">All Notifications</a></h2>
                  <div class="clearfix"></div>

                </div>
                <div class="x_content" id="notification_scroll" style="">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget" class="">
              <?php 
              foreach ($notifications as $notification) {?>
              <li>
                <div class="block">
                  <div class="block_content">
                    <h2 class="title">
                      <div class="row">
                        <div class="col-md-2">
                      <?php 
                      if(strtolower($notification['notification_type']) == 'contract'){
                        $btnStyle="background-color: #fdaaaa;border-color: #fdaaaa;font-size:8px;";
                        $btnText = "Contract";
                       }elseif(strtolower($notification['notification_type']) ==  'supplier status' || strtolower($notification['notification_type'])=='vendor'){ 
                        $btnStyle="background-color: #fec98f;border-color: #fec98f;font-size:8px;";
                        $btnText = "Supplier";
                        }elseif(strtolower($notification['notification_type']) ==  'saving milestone'){ 
                          $btnStyle="background-color: #98ddfc;border-color: #98ddfc;font-size:8px;";
                          $btnText = "Savings";
                        }elseif(empty($notification['notification_type']) || strtolower($notification['notification_type']) ==  'quote'){
                          $btnStyle="background-color: #a0e7a0;border-color: #a0e7a0;font-size:8px;";
                          $btnText = "eSourcing";
                        }else{
                          $btnStyle="";
                          $btnText = "Other";
                        } ?>
                        <button class="btn btn-success" style="<?php echo $btnStyle;?>"><?php echo $btnText;?></button>
                      </div>
                      <div class="col-md-10">
                      <?php  
                      // echo "<pre>"; print_r($notification); exit; 
                      $str = $notification['notification_text'];
                      $wordsNumber = 1000;
                      echo FunctionManager::getNWordsFromString($str,$wordsNumber); ?>
                      <div class="byline">
                      <span><?php echo Notification::getTimeAgo($notification['notification_date']); ?></span>
                      </div>
                      </div>
                      </div>
                     <!-- End :row -->
                     </h2>
                    <!-- End : H2 title -->
                   
                  </div>
                </div>
              </li>
          <?php } ?>
          </ul></div></div></div></div>

  <div class="col-md-6 col-sm-6 col-xs-12 " >
  <div class="col-md-12 col-sm-12 col-xs-12 mbl-vw pl-0 pr-0">
  <div class="x_panel tile rem0-mr10" >
    <div class="x_title">
      <h2>eSourcing Activities By Location</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content"><div id="full_scoring_by_location_count"></div><br /></div>
  </div></div>
  <div class="col-md-12 col-sm-12 col-xs-12  hidden-xs pl-0 pr-0" id="chart_area_17" >
    <div class="x_panel tile">
        <div class="x_title rem0-mr10">
          <h2 >Contracts By Location</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div id="contracts_by_location"></div>
        </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_17">
    <div class="x_panel tile overflow_hidden">
    <div class="x_title"><h2>Expiring Contracts </h2>
      <ul class="nav navbar-right panel_toolbox w-20">
        <li><a onclick="collapseChart(17);"><i class="fa fa-arrows-h"></i></a></li>
      </ul>
      <div class="clearfix"></div></div>
    <div class="x_content">
    <table class="tile_info ml-0">
      <?php $idx = 0;
      foreach ($expiring_contracts as $dataset_idx=>$a_expiring)
      {
        if ($dataset_idx === 'total') continue;
        $idx += 1;
        switch ($idx)
        {
          case 1  : $color = 'blue'; break;
          case 2  : $color = 'purple'; break;
          case 3  : $color = 'green'; break;
          case 4  : $color = 'turquoise'; break;
          case 5  : $color = 'red'; break;
          case 6  : $color = 'antique-white'; break;
          case 7  : $color = 'aqua-marine'; break;
          case 8  : $color = 'bisque'; break;
          case 9  : $color = 'khaki'; break;
          case 10  : $color = 'blue'; break;
          case 11  : $color = 'purple'; break;
          case 12  : $color = 'green'; break;
          case 13  : $color = 'turquoise'; break;
          case 14  : $color = 'red'; break;
          default : $color = 'salmon'; break;
        }
        ?>
        <tr>
          <td style="width: 5%;"><i class="fa fa-square" style="color: <?php echo $color; ?>!important;"></i></td>
          <td style="vertical-align: middle;">
            <?php echo !empty($a_expiring['month_name'])?addslashes($a_expiring['month_name']):""; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format((!empty($a_expiring['value'])?$a_expiring['value']:0) ); ?>
          </td>
        </tr>
    <?php } ?></table></div></div></div>
</div>

  <div class="clearfix"></div>


 <div class="col-md-12 col-sm-12 col-xs-12 hidden-xs">
    <div class="x_panel tile overflow_hidden">
        <div class="x_title">
          <h2 class="pull-left">Contracts Expiring Or That Have A Notice Period In the Next 12 Months</h2>
           <h2 class="pull-left"><a class="btn btn-success expiring-contract-btn" onclick="$('#expiring_contract_modal').modal('show');">Contract List</a></h2>
  <div class="clearfix"></div>
         <!--  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
  <li><a onclick="$('#expiring_contract_modal').modal('show');"><i class="fa fa-arrows-h"></i></a></li>
            <li><a onclick="exportChart('expiring_contracts');"><i class="fa fa-cloud-download"></i></a></li>
          </ul> -->
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div id="expiring_contracts"></div>
        </div>
    </div>
  </div>

<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_17">
  <div class="x_panel tile overflow_hidden">
  <div class="x_title">
    <h2>Expiring Contracts</h2>
    <ul class="nav navbar-right panel_toolbox w-20" >
    <li><a onclick="collapseChart(17);"><i class="fa fa-arrows-h"></i></a></li>
    <li><a onclick="exportChart('expiring_contracts');"><i class="fa fa-cloud-download"></i></a></li>
    </ul>
  <div class="clearfix"></div>
  </div>
  <div class="x_content">
  <table class="tile_info ml-0">
  <?php
  $idx = 0;
  foreach ($expiring_contracts as $dataset_idx=>$a_expiring)
  {
    if ($dataset_idx === 'total') continue;
    $idx += 1;
    switch ($idx)
    {
      case 1  : $color = 'blue'; break;
      case 2  : $color = 'purple'; break;
      case 3  : $color = 'green'; break;
      case 4  : $color = 'turquoise'; break;
      case 5  : $color = 'red'; break;
      case 6  : $color = 'antique-white'; break;
      case 7  : $color = 'aqua-marine'; break;
      case 8  : $color = 'bisque'; break;
      case 9  : $color = 'khaki'; break;
      case 10  : $color = 'blue'; break;
      case 11  : $color = 'purple'; break;
      case 12  : $color = 'green'; break;
      case 13  : $color = 'turquoise'; break;
      case 14  : $color = 'red'; break;
      default : $color = 'salmon'; break;
    }
    ?>

    <tr>
      <td style="width: 5%;">
        <i class="fa fa-square" style="color: <?php echo $color; ?>!important;"></i>
      </td>
      <td style="vertical-align: middle;">
        <?php echo addslashes($a_expiring['month_name']); ?> - <?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($a_expiring['value']); ?>
      </td>
    </tr>

    <?php
  }
  ?>
  </table>
  </div>
  </div>
</div>
 <div class="col-md-6 col-sm-6 col-xs-12 " >
  <div class="x_panel tile">
    <div class="x_title"><h2>Contracts By Status</h2><div class="clearfix"></div> </div>
    <div class="x_content">
     <div id="contract_status_pie_chart"></div><br />
   </div>
  </div>
 </div>
 <div class="col-md-6 col-sm-6 col-xs-12">
  <div class="x_panel tile"><div class="x_title"><h2>% Of Expired Supplier Documents</h2>
    <div class="clearfix"></div></div><div class="x_content">
      <div id="document_vs_valid_vs_expired"></div><br /></div>
  </div>
 </div>

 <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel"><div class="x_title"><h2>Projected Vs Realised Savings By Milestone Month (Base Currency)</h2><div class="clearfix"></div></div>
      <div class="x_content">
        <div id="realize_project_month_chart"></div>
      </div>
    </div>
  </div>

<!--   <div class="col-md-6 col-sm-6 col-xs-12">
  <div class="x_panel tile overflow_hidden"><div class="x_content"><br /><br /><div id="contract_approved_document"></div><br /></div>
  </div></div>
  <div class="clearfix"></div> -->
<!--   <div class="col-md-6 col-sm-6 col-xs-12">
  <div class="x_panel tile overflow_hidden"><div class="x_content"><br /><br /><div id="spplier_completed_field"></div><br /></div>
  </div></div> -->
<!--   <div class="col-md-6 col-sm-6 col-xs-12">
  <div class="x_panel tile overflow_hidden"><div class="x_content"><br /><br /><div id="supplier_approved_document"></div><br /></div>
  </div></div> -->

 </div>
</div>

<div class="modal fade" id="expiring_contract_modal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 62%;">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title font-1rem">Contracts Expiring or That Have A Notice Period In the Next 12 Months</h4>
            </div>
            <div class="clearfix"> </div>
            <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab p1">
                  <table id="contract_expiring_next_months" class="table table-striped table-bordered" style="width: 100%;">
                    <thead>
                    <tr><th>Contract Title</th><th style="width: 19%;">Supplier Name</th><th style="width: 17%;">Managed By</th><th style="width: 13%;">End Date</th><th>Notice Period Date</th></tr>
                    </thead>
                     <tbody>
                      <?php foreach ($expiring_contracts_list as $expiring_row) {
                        $vendorID = $expiring_row['vendor_id'];
                        ?>
                         <tr><td><a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $expiring_row['contract_id']); ?>" style="text-decoration:none"><?php echo $expiring_row['title'];?></a></td>
                          <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $vendorID); ?>" style="text-decoration:none"><?php echo $expiring_row['vendor_name'];?></a></td>
                          <td><?php if (!empty($expiring_row['contract_manager'])) {
                            echo $expiring_row['contract_manager'];
                          }?></td>
                         <td><?php 
                          if ($expiring_row['end_date'] !="0000-00-00") 
                            echo date(FunctionManager::dateFormat(), strtotime($expiring_row['end_date']));
                          ?></td>
                         <td><?php  if ($expiring_row['notice_period_date'] !="0000-00-00 00:00:00") 
                           echo date(FunctionManager::dateFormat(), strtotime($expiring_row['notice_period_date']));?></td>
                         </tr>
                      <?php } ?>
                     </tbody>
                  </table>
                </div>

              </div>

            </div>
          </div>
        </div>
      </div>



<?php 
    if(strpos(Yii::app()->getBaseUrl(true),"sandbox")){
      echo  $this->renderPartial('/app/help_information_popup/sandbox_help_auto_pupop');
     }
  ?>
<style type="text/css">
  #contract_expiring_next_months_wrapper {display: block; float: none; margin: 20px !important;}
</style>
<script src="https://unpkg.com/@popperjs/core@^2.0.0"></script>
<script type="text/javascript">


 $(document).ready(function() {
  $("#contract_expiring_next_months").dataTable({
        "order": [[ 1,"desc" ]]
  });
  $('[data-toggle="tooltip"]').tooltip();
});

/*colors = ['#2d9ca2','#66DA26', '#546E7A', '#E91E63', '#FF9800','#2E93fA','#2196F3', '#3a62ba', '#7b9333', '#344189','#','#','#','#','#'];*/

colors = ['#008ffb','#00e396', '#feb019', '#E91E63', '#FF9800','#5fed64','#59f7ea', '#5385f5', '#cbf257', '#f7ae77','#','#','#','#','#'];

function createPieChart()
{   

    var quote_id = $("#quote_id").val();
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/listScoreDataChart'); ?>',
        type: 'POST', dataType: 'json',
        data: {},
        success: function(chart_data) {
 /*      var score_values = chart_data.score_values;
       var score_labels = chart_data.score_labels;*/
       var pie_score_series = [];
       var pie_score_label  = chart_data.pie_score_label;
      for (i = 0; i < chart_data.pie_score_series.length; i++) {
          pie_score_series.push(parseFloat(chart_data.pie_score_series[i]));
      }
      
    // START: Pie Chart
    var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 235,
              width: 500,
              type: 'pie',
              id:"scoringPieChart"

            },

            tooltip: {
            y: {
            formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
            return value+'%'
              } 
             }
            },

           //legend: {show: true},
            dataLabels: {enabled: false},
            labels: pie_score_label,
            series: pie_score_series,
            responsive: [{
               breakpoint: 380,
                options: {
                    chart: {
                       width: 'auto'
                        //height: 'auto',

                    },
                    legend: {
                        show: false
                    }
                }
            }],
            
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_pie_chart"),
            options
        );
         chart.render();

          ApexCharts.exec('scoringPieChart', 'updateOptions', {
           labels:  pie_score_label,
          }, false, true);
          ApexCharts.exec('scoringPieChart', 'updateSeries', pie_score_series,true);

    // END: Pie Chart
       }
      });

  }
function createQuoteBarChart()
{   
    

    var quote_id = '';
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('report/quoteSupplier'); ?>',
        type: 'POST', dataType: 'json',
        data: {},
        success: function(chart_data) {
       var qoute_series = chart_data.qoute_series;
       var qoute_label = chart_data.qoute_label;
       var qoute_vendor = chart_data.qoute_vendor;
       var quote_currency = chart_data.quote_currency;
       var pie_contract_series = [];
       var pie_contract_label  = chart_data.contract_label;
       var colors = chart_data.contract_color;
      for (i = 0; i < chart_data.contract_count.length; i++) {
          pie_contract_series.push(parseFloat(chart_data.contract_count[i]));
      }
      
    // START: Pie Chart
    var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 215,
              // width: 433,
              type: 'pie',
              //id:"scoringPieChart"
            },
           // legend: {show: false},
            colors:colors,
            dataLabels: {enabled: false},
            labels: pie_contract_label,
            series: pie_contract_series,
            responsive: [{
               breakpoint: 433,
                options: {
                    chart: {
                       width: 'auto'
                        //height: 'auto',

                    },
                    legend: {
                        show: false
                    }
                }
            }],
            
        }
        var chart = new ApexCharts(
            document.querySelector("#contract_status_pie_chart"),
            options
        );
         chart.render();

        /*  ApexCharts.exec('scoringPieChart', 'updateOptions', {
           labels:  pie_score_label,
          }, false, true);
          ApexCharts.exec('scoringPieChart', 'updateSeries', pie_score_series,true);*/

    // END: Pie Chart

    // Start: Contract approved document 

      /*var contractDocApprovedPercentage =  chart_data.documentApprovedPercentage;
        var options = {
        chart: {
        height: 230,
        type: "radialBar",
        },

        series: [contractDocApprovedPercentage],
        labels: ['Contract Documents Approved'],
        colors: ["#2d9ca2"],
        plotOptions: {
        radialBar: {
          hollow: {
            margin: 0,
            size: "80%",
            background:'#fff'
           
          },
          track: {
            dropShadow: {
              enabled: true,
              top:0,
              left:0,
              blur:0,
              opacity:0.5
              
            }
          },
          dataLabels: {
            name: {
             // offsetY: -10,
              color: "#2d9ca2",
              fontSize: "12px"
            },
            value: {
              color: "#2d9ca2",
              fontSize: "12px",
              show: true,
              formatter: function (val) {
                return val+"%"
              }
            }
          }
        }
        },

        stroke: {
        lineCap: "round"
        },

        };

        var chart = new ApexCharts(document.querySelector("#contract_approved_document"), options);

        chart.render();
*/
        // End: Contract approved document 

        // Start: Supplier approved document 

      /*var vendorDocApprovedPercentage =  chart_data.vendorDocApprovedPercentage;
        var options = {
        chart: {
        height: 230,
        type: "radialBar",
        },

        series: [vendorDocApprovedPercentage],
        labels: ['Suppliers Documents Approved'],
        colors: ["#2d9ca2"],
        plotOptions: {
        radialBar: {
          hollow: {
            margin: 0,
            size: "80%",
            background:'#fff'
           
          },
          track: {
            dropShadow: {
              enabled: true,
              top:0,
              left:0,
              blur:0,
              opacity:0.5
              
            }
          },
          dataLabels: {
            name: {
             // offsetY: -10,
              color: "#2d9ca2",
              fontSize: "12px"
            },
            value: {
              color: "#2d9ca2",
              fontSize: "12px",
              show: true,
              formatter: function (val) {
                return val+"%"
              }
            }
          }
        }
        },

        stroke: {
        lineCap: "round"
        },

        };

        var chart = new ApexCharts(document.querySelector("#supplier_approved_document"), options);

        chart.render();*/

        // End: Supplier approved document

         // Start: Supplier fields completed

      /*var supplierFields =  chart_data.supplierFieldsPercentage;
        var options = {
        chart: {
        height: 230,
        type: "radialBar",
        },

        series: [supplierFields],
        labels: ['Suppliers Fields Populated'],
        colors: ["#2d9ca2"],
        plotOptions: {
        radialBar: {
          hollow: {
            margin: 0,
            size: "80%",
            background:'#fff'
           
          },
          track: {
            dropShadow: {
              enabled: true,
              top:0,
              left:0,
              blur:0,
              opacity:0.5
              
            }
          },
          dataLabels: {
            name: {
             // offsetY: -10,
              color: "#2d9ca2",
              fontSize: "12px"
            },
            value: {
              color: "#2d9ca2",
              fontSize: "12px",
              show: true,
              formatter: function (val) {
                return val+"%"
              }
            }
          }
        }
        },

        stroke: {
        lineCap: "round"
        },

        };

        var chart = new ApexCharts(document.querySelector("#spplier_completed_field"), options);

        chart.render();*/

        // End: Supplier fields completed

        

       }
      });

  }
  createPieChart();
  createQuoteBarChart();

 function createApexChartContCombo(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){


    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;
         
      $.each(series.notice_count, function(key, value) {
          seriesData.push(value);
        });
      seriesL.push({'name':'Contracts With Notice Period','data':seriesData});

      $.each(series.count, function(key, value) {
          seriesMixedData.push(value);
        });
      seriesL.push({'name':'Contracts Expiring','data':seriesMixedData});
     
      
  

    var categoriesL =[];
     $.each(categories, function(key, value) {
      if(value !="+12 Months"){
        valueArr = value.split(' ');
        categoriesL.push(valueArr[0]);
      }else{
         categoriesL.push(value);
      }
    });    
   var options = {
          series: seriesL,
          chart: {
          fontFamily: 'Poppins !important',
          type: 'bar',
          height: 200,
          width:'1100px',
          //zoom:'100%',
           toolbar: {
          show: false
         }
        },
        colors: colorL,
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: categoriesL,
        },
        yaxis: {
          labels: {
             formatter: function(val) { return parseInt(val); },
          },
          /*title: {
            text: 'Count'
          },*/
          //tickAmount: 5,
         //  min: 0,
           //max: 20,
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              if(val === null || val =='' || val == 0) {
                return '  NO DATA';
            } else {
                return val;
            }
              
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#expiring_contracts"), options);
        chart.render();
  
 }
  colors= ['#ad6eff', '#6eb5ff'];
  createApexChartContCombo(183,'100%','line',false,<?php echo json_encode(array('notice_count'=>$expSeriesNoticeCount,'','count'=>$expSeriesCount)) ?>,<?php echo json_encode($expLabel);?>,"#expiring_contracts",colors,'Count');
  var options = {
          series: [{  name: "Full Sourcing Activities",
          data: [<?php echo  $quteCount;?>]
        }],
          chart: {
          download: false, 
          toolbar: {
            show: false
          },
          height: 225,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: ['#008ffb','#00e396', '#feb019', '#E91E63', '#FF9800','#5fed64','#59f7ea', '#5385f5', '#cbf257', '#f7ae77','#','#','#','#','#'],
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [<?php echo  $quteLocName;?>],
          labels: {
            style: {
              colors: "#000000",
              fontSize: '10px'
            }
          }
        }
        };

    var chart = new ApexCharts(document.querySelector("#full_scoring_by_location_count"), options);
        chart.render();

     function createApexCont(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;
    var distributedL=true;

    if(chartIDL=="#expiring_contracts"){
      $.each(series.spend, function(key, value) {
          seriesData.push(value);
        });
        seriesL.push({'name':'Spend','type':'column','data':seriesData});
        $.each(series.count, function(key, value) {
            seriesMixedData.push(value);
        });
        seriesL.push({'name':'Count','type':'line','colors':'yellow','data':seriesMixedData});
    }else {
    
    $.each(series, function(key, value) {  
        seriesL.push({'name':key,'data':value});
    });
    stackedL= true;
    distributedL = false;
  }

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value);
    });

    if(chartIDL=="#spend_contracts"){horizontalL=true;}
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }},
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        },
        stacked: stackedL,
        //stackType: '100%',
        toolbar: {
            tools: {
                download: false
            }
          },
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        legend: {
                position: 'bottom',
                offsetX: 5,
                offsetY: 0
            },
      colors: colorL,
      plotOptions: {bar: {columnWidth: '35%',distributed: distributedL,horizontal: horizontalL}},
      dataLabels: {enabled: lableL},
      //stroke: { width: [4,  4]}
       //stroke: {width: [4, 4]},
      series: seriesL,
      opposite: true,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      fill: {opacity: 1},
      xaxis: {categories: categoriesL,
        labels: {
                trim: true,
            show: false,
            minHeight: undefined,
              maxHeight: 80,
              rotate: -2,

                formatter: function (value) {
                  valueL = chartIDL!="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },}},
     
    yaxis: {

      "labels": {
            "formatter": function (val) {
                return  Math.trunc(val);
            }
        }
      }
   }
    
    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }
   
 /*  var colors = ['#f7778c', '#efa65f', '#aaa', '#48d6a8']; //, '#775DD0', '#546E7A', '#26a69a', '#D10CE8'*/

     var colors =  [<?php echo '"'.implode('","',$colorArr).'"';?>];
    createApexCont(200,'100%','bar',false,<?php echo json_encode($locSeries) ?>,<?php echo json_encode($locLabel);?>,"#contracts_by_location",colors,'Count');

    // START: % of documents that are valid and % documents that have expired
    var colors = ['rgb(72, 214, 168)','rgb(247, 119, 140)']; 
    var options = {
        chart: {
          fontFamily: 'Poppins !important',
          height: 215,
          // width: 400,
          type: 'pie',
          id:""

        },
       // legend: {show: false},
       colors:colors,
        dataLabels: {enabled: false},
        labels: [/*'Total Documents',*/'Valid Documents','Expired Documents'],
        series: [<?php echo $vendorStatistics['documentValidPerc'];?>,<?php echo $vendorStatistics['documentExpiredPerc'];?>],
        responsive: [{
           breakpoint: 380,
            options: {
                chart: {
                   width: 'auto'
                    //height: 'auto',
                },
                legend: {show: false},
            }
        }],
         
       tooltip: {
            y: {
                formatter: function(value) {
                 return value+"%";
               },
                 
            },}
    }
    var chart = new ApexCharts(
        document.querySelector("#document_vs_valid_vs_expired"),
        options
    );
    chart.render();

 // END: % of documents that are valid and % documents that have expired

      // Start: Projected vs Realised Savings by Milestone Month
   var options = {
          series: [{
          name: 'Projected Savings',
          // data: ['1200','1100','1600','1810','1000','4684','740'
          data: [<?php echo implode(',', $chart_3['project']); ?>]
        }, {
          name: 'Realised Savings',
          // data: ['800','153','0','0','20','0','600']
          data: [<?php echo implode(',', $chart_3['realised']); ?>]
        }
        ],
          chart: {
          fontFamily: 'Poppins !important',
          type: 'bar',
          height: 270,
          width: '1100px',
          stacked: false,
          distributed: false,
           horizontal: true,
          toolbar: {
            show: false
          },
          zoom: {
            enabled: true
          }
        },
        colors: ['#efa65f','#48d6a8'],
        dataLabels: {enabled: false,},
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '95%',
          },
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {

          //type: 'datetime',
          categories: [<?php echo "'".implode("','",$chart_3['month'])."'"; ?>],
          labels : {
            show:true,
            style: {
              //colors: ['#2d9ca2','#2d9ca2'],
              fontSize: '11px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
          },
          }
        },
        yaxis: {
          //tickAmount: 1000,
           labels : {
            style: {
              /*colors: '#2d9ca2',*/
              fontSize: '11px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
              // useSeriesColors: ['#efa65f','#efa65f']
          },
          }
        },
        legend: {
          position: 'bottom',
          offsetX: 0,
          labels: {
          colors: ['#efa65f','#48d6a8'],
          useSeriesColors: ['#efa65f','#48d6a8']
          },
        },
        fill: {
          opacity: 1
        }
        };

        var chart = new ApexCharts(document.querySelector("#realize_project_month_chart"), options);
        chart.render();
        // End: Projected vs Realised Savings by Milestone Month

</script>
<?php 
   //if(strpos(Yii::app()->getBaseUrl(true),"sandbox")){
    if($home_visit !=1){?>
      <script type="text/javascript">
        $(document).ready(function(){
          $('.quick_module_modal').trigger('click');
         //tour.start();
        });
      </script>
<?php //}
 }?>

<script type="text/javascript">
  $('document').ready(function(){
    $(".quick-show-information").on('click', function(){
      $('.quick_module_modal').trigger('click');
   });
 });
</script>

 



