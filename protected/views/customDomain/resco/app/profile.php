<?php
  $username_value = "";
  if (isset(Yii::app()->session['duplicate_username'])) $username_value = Yii::app()->session['duplicate_username'];
  else if (isset($user) && isset($user['username'])) $username_value = $user['username'];

  $email_value = "";
  if (isset(Yii::app()->session['duplicate_email'])) $email_value = Yii::app()->session['duplicate_email'];
  else if (isset($user) && isset($user['email'])) $email_value = $user['email'];
?> <?php $disabled = FunctionManager::sandbox(); ?>
<div class="right_col" role="main">
    <div class="row-fluid tile_count">
      <div class="col-lg-12">
       <?php  $alert = Yii::app()->user->getFlash('success');
       if(!empty($alert)){?>
            <div class="alert alert-success">
                <?php echo $alert; ?>
            </div>
        <?php } ?>
        <div class="span6">
          <div class="col-lg-8">
             <h3>Edit Profile Details</h3>
          </div>  
         
        </div>
        <div class="clearfix"> </div><br><br>
    </div>
    <div class="clearfix"></div>
<div class="row tile_count">
  
 
      <form id="company_form" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('app/profile'); ?>">
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Full Name <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="full_name" id="full_name"
                    <?php if (isset($user['full_name']) && !empty($user['full_name'])) echo 'value="' . $user['full_name'] . '"'; else echo 'placeholder="Full Name"'; ?> >
          </div>
      </div>
        <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Email Address <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="email" id="email"
                      <?php if (isset($user['email']) && !empty($user['email'])) echo 'value="' . $email_value . '"'; else echo 'placeholder="Email Address"'; ?> >
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Logon Username <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="username" id="username"
                      <?php echo 'value="' . $username_value . '"';?> readonly  />
           
          </div>
      </div>

     <div class="form-group">
      <div class="col-md-3 col-sm-3 col-xs-12 date-input">
        <label class="control-label">Position</label>
        <input type="text" class="form-control notranslate" name="position" id="position"
                    <?php if (isset($user['position']) && !empty($user['position'])) echo 'value="' . $user['position'] . '"'; else echo 'placeholder="Position"'; ?> >
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Contact Number</label>
              <input type="text" class="form-control notranslate" name="contact_number" id="contact_number"
                      <?php if (isset($user['contact_number']) && !empty($user['contact_number'])) echo 'value="' . $user['contact_number'] . '"'; else echo 'placeholder="Contact Number"'; ?> >
          </div>
        <div class="clearfix"> <br /><br /> </div>
           <!-- <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Location<span style="color: #a94442;"></span></label>
        <select name="location_id" id="location_id" class="form-control notranslate" onchange="loadDepartmentsForSingleLocation(0);">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($user['location_id']) && $location['location_id'] == $user['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

           <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Department<span style="color: #a94442;"></span></label>
              <select name="department_id" id="department_id" class="form-control notranslate">
                  <option value="">Select Department</option>
              </select>
          </div> 
          <div class="clearfix"></div>
      </div> -->
        <div class="col-md-3 col-sm-3 col-xs-6">
          <?php $languagesTool="To add a new language not found below please contact us through Raise a Ticket"; ?>
          
            <label class="control-label languages">Select Language</label>

            <?php $languages = array(
                          ''=>'Select Language',
                          'en'=>'English',
                          'ar'=>' العربية',
                          'zh-CN'=>' 简体中文',
                          'zh-TW'=>'中國傳統的',
                          'nl'=>' Nederlands',
                          'fr'=>'Français',
                          'de'=>'Deutsche',
                          'hi'=>'हिन्दी',
                          'it'=>'Italiana',
                          'ja'=>'日本人',
                          'ko'=>'한국어',
                          'pl'=>'Polskie',
                          'pt'=>'Português',
                          // 'ru'=>'русский',
                          'es'=>'Español',
                          'th'=>'ไทย',
            )?>
            <select name="language_code" id="language_code" class="form-control select2_multiple notranslate">
                  <?php foreach ($languages as $code=>$language) { ?>
                      <option value="<?php echo $code; ?>" <?php echo  !empty($user['language_code']) && $user['language_code']==$code?"selected='selected'":""?>>
                          <?php echo $language; ?>
                      </option>
                  <?php } ?>
              </select>
         
        </div>

        
         <div class="col-md-3 col-sm-3 col-xs-6">
            <label class="control-label">Profile Picture</label>
            <input type="file" name="profile_img"  class="notranslate" style="border: none" id="profile_img"><br />
          <?php if (isset($user['profile_img']) && !empty($user['profile_img'])) { ?> 
            <a href="<?php echo $this->createUrl('app/deleteProfileImg');?>" style="text-decoration: underline;color: #3b4fb4;" <?php echo $disabled; ?> onclick="return confirm('Are you sure want to delete profile picture');">Delete Profile Picture</a>
          <?php } ?>
            <div class="clearfix"></div>
          </div>
      
      </div>

    <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />

    <a href="javascript:void(0);" onclick="$('#company_form').submit();"   <?php echo $disabled; ?>>
        <button type="button" class="btn btn-success submit-btn1"   <?php echo $disabled; ?>>
            Save Profile
        </button>
    </a>
    </div>
    </div>

  </form>
  </div>
  <br><br>
      <!--   <div class="two-factor">
           <div class="col-lg-12 ">
            <p>Click below to refresh your two factor authentication QR code This will be available when you next log into oboloo.</p><br>
            <a href="<?php echo $this->createUrl('app/qrcode'); ?>"  onclick="return confirm('Confirm to refresh the QR code?');"class="btn btn-info">Refresh Two Factor Authentication QR Code</a>
          </div>
        </div> -->
</div>
</div>
<?php //$this->redirect(AppUrl::bicesUrl('home'));?>

 
<style type="text/css">
body{
 background: #ebecf6;
}
</style>
 
<script type="text/javascript">



    function loadDepartmentsForSingleLocation(department_id)
    {
        var location_id = $('#location_id').val();
        var single = 1;

        if (location_id == 0)
            $('#department_id').html('<option value="">Select Department</option>');
        else
        {
            $.ajax({
                type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
                url: BICES.Options.baseurl + '/locations/getDepartments',
                success: function(options) {
                    var options_html = '<option value="">Select Department</option>';
                    for (var i=0; i<options.length; i++)
                        options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                    $('#department_id').html(options_html);
                    if (department_id != 0) $('#department_id').val(department_id);
                },
                error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
            });
        }
    }


$(document).ready( function() {
  // $(".select2_multiple").select2({
  //   containerCssClass: "notranslate",
  //   dropdownCssClass: "notranslate",
  // });

  <?php if (isset($user['location_id']) && !empty($user['location_id'])) { ?>
  <?php if (isset($user['department_id']) && !empty($user['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $user['department_id']; ?>);
  <?php } else { ?>
    loadDepartmentsForSingleLocation(0);
  <?php } ?>

  <?php } ?>
});
</script>

<style type="text/css">
