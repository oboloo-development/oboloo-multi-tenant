<div id="sandbox_help_sidebar_open_module_modal" class="modal fade" role="dialog">
<div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content info_contant" style="background-color: #ffff !important;">
      <div class="modal-header info_header">
        <h1 class="heading-title">oboloo Tutorial</h1>
        <!-- <h5 style="padding-bottom: 10px; font-size: 10px;"><a herf="#" class="home_redirect">Home ></a> Getting Started</h5> -->
        <h2 class="text-center sec-title">Which module would you like a tutorial of?</h2>
      </div>
      <div class="modal-body" >
        <!-- Start Create a Sourcing Activity -->
        <div class="row info_row">
          <div class="col-md-2">
            <a href="<?php echo $this->createUrl('quotes/list',array('tour'=>'eSourcingTour'));  ?>" class="">
              <div class="img-svg">
                <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/trolley.png'); ?>"   class="img-responsive" />
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('quotes/list',array('tour'=>'eSourcingTour'));  ?>" class="">eSourcing</a></h3>
            <!-- <p class="elementor-icon-box-description">Create eSourcing activities with questionnaires & evaluations</p> -->
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('quotes/list',array('tour'=>'eSourcingTour'));  ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create a Sourcing Activity -->

        <!-- Start Create a Contract -->
        <div class="row info_row">
          <div class="col-md-2">
            <a href="<?php echo $this->createUrl('contracts/list',array('tour'=>'contractTour'));  ?>" class="">
              <div class="img-svg">
                <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/contract.png'); ?>"   class="img-responsive" />
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('contracts/list',array('tour'=>'contractTour'));  ?>" class="">Contract Management</a></h3>
            <!-- <p class="elementor-icon-box-description">Create, upload & build your own contract repository</p> -->
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('contracts/list',array('tour'=>'contractTour'));  ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create a Contract -->

        <!-- Start Create a Supplier -->
        <div class="row info_row">
          <div class="col-md-2">
            <a href="#" class="<?php echo $this->createUrl('vendors/list',array('tour'=>'supplierTour'));  ?>">
              <div class="img-svg">
                <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/network.png'); ?>">
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('vendors/list',array('tour'=>'supplierTour'));  ?>" class="">Supplier Management</a></h3>
            <!-- <p class="elementor-icon-box-description">Create, upload & build your own Supplier repository</p> -->
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('vendors/list',array('tour'=>'supplierTour'));  ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create a Supplier -->

        <!-- Start Create a Savings Record -->
        <div class="row info_row" style="border-bottom:0px !important;">
          <div class="col-md-2">
            <a href="<?php echo $this->createUrl('savings/list',array('tour'=>'savingsTour'));  ?>" class="">
              <div class="img-svg">
                <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/bar-chart.png') ?>">
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('savings/list',array('tour'=>'savingsTour'));  ?>" class="">Savings Management</a></h3>
            <!-- <p class="elementor-icon-box-description">Put the values that are important to your company (Super Users Only)</p> -->
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('savings/list',array('tour'=>'savingsTour'));  ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create a Savings Record -->

      </div>
     </div>
    </div>
  </div>

