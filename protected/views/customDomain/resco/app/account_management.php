<?php

if(strpos(Yii::app()->getBaseUrl(true),"local") || strpos(Yii::app()->getBaseUrl(true),"multi")){
   $subdomain = 'oboloo1';
 }else{ $subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));}
 $postRequest = array(
    'subdomain' => $subdomain,
);

// $postRequest = array(
//     'subdomain' => 'bondoporaja',
// );

if(strpos(Yii::app()->getBaseUrl(true),"devoboloo")){
$cURLConnection = curl_init('https://devoboloo.com/saas/clients/callForSubdomain');
}else{
$cURLConnection = curl_init('https://oboloo.software/saas/clients/callForSubdomain');
}
curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, false);


$apiResponse = curl_exec($cURLConnection);
curl_close($cURLConnection);

// $apiResponse - available data from the API request
$jsonArrayResponse  =  json_decode($apiResponse);
 
 //echo "<pre>"; print_r($jsonArrayResponse); exit;
//echo $jsonArrayResponse->next_estimated_bill.'<br />'; 
?>





<div class="right_col" role="main">
    <div class="row-fluid tile_count">
      <div class="col-lg-12">
        <div class="alert alert-success" >
             Your subscription cancellation request sent successfully.
        </div>
        
        <div class="span6">
          <div class="col-lg-8">
             <h3 style="margin-bottom: 10px;">Account Management</h3>
             <h5>Here you can find your subscription billing and option to cancel</h5>
          </div>  
         
        </div>
        <div class="clearfix"> </div><br><br>
    </div>
    <div class="clearfix"></div>
  <div class="row tile_count">
    
       <div class="col-md-4 col-xs-12 col-left " style="padding-left: 0px; margin-left: -46px;">
       
              <form class="form-horizontal" action="/action_page.php">
                <div class="form-group">
                <label class="control-label col-md-6 col-xs-12">Your Plan:</label>
                <div class="col-md-6 col-xs-12">
                <input type="text" name="plan" class="form-control"  value="<?php echo $jsonArrayResponse->plan; ?>" readonly="readonly">
                </div></div>
                <div class="form-group">
                <label class="control-label col-md-6 col-xs-12">Next Billing Cycle:</label>
                <div class="col-md-6 col-xs-12">
                <input type="text"  name="next_bill" class="form-control" value="<?php echo date(FunctionManager::dateFormat(), strtotime($jsonArrayResponse->next_billing_cycle)); ?>" readonly="readonly"></div>
                </div>
                <div class="form-group">
                <label class="control-label col-md-6 col-xs-12" >Next Estimated Bill (Inc Tax):</label>
                <div class="col-md-6 col-xs-12">
                 <input type="text" name="next_estimated" class="form-control" 
                  value="<?php echo $jsonArrayResponse->next_estimated_bill; ?>" readonly="readonly">
                </div></div>
                
              </form>

         
           
          </div>
      
        
         <div class="col-xs-8 empty-col"></div>
    
       <?php //$on_Trail_hidden = SubscriptionManager::getTrialStatus();
             $db = getDatabaseForSubdomain(APP_SUBDOMAIN_URL);
            if($db['is_trial'] == 0){ ?>
              <div class="col-xs-12" style="margin-top: 40px;">
                 <button type="button" class="btn btn-danger"  data-toggle="modal" data-target="#subscription_modal">
                  Cancel Subscription
                 </button>
                <a href="<?php echo $jsonArrayResponse->update_url;?>" target="_blank" class="btn btn-info">Update Payment Method</a>
                <p style="font-size: 14px;">
                   You can cancel your subscription at any point by clicking the Cancel Subscription button.
                </p>
              </div>
        <?php } ?>
        <!-- left-col End -->
      </div>

       <div class="col-xs-9 col-right">
         
       </div>
     </div>
  </div>
 
</div>


<div id="subscription_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="padding: 140px 0px 140px 0px;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:0px; padding-top: 15px; padding-bottom: 0px;">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: 28px; margin-right: 18px; color: #fff;">&times;</button>
        <h3 class="modal-title text-center top-h3" style="border: 1px solid #ef4848!important; background-color: #ef4848 !important;">
        Are you sure you want to cancel your subscription?</h3>
      </div>
      <div class="modal-body" style="padding-top:0px;">
      <div class="panel panel-default "style=" border-radius: 0px; margin-bottom: 0px; padding: 8px 0px 8px 0px; background-color: #F1F1F1;">
        <div class="panel-body" >
          <form id="cancel_subscription_form" class="form-horizontal form-label-left input_mask" method="post" action="">
          <div class="subscription_parah">
            <p class="modal-p text-danger" style="font-weight: 600;">This will cancel your entire companies oboloo subscription, not just your user licence. To cancel a single user licence go to the user management page under Settings > Users </p>
            <br><br><p class="modal-p">You can cancel your subscription renewal payments while your membership is active up to 24 hours before the renewal date.</p><br>
            <p class="modal-p">If you cancel the subscription less than 24 hours before it is due to renew, you are likely to be charged for the next subscription period. In this case you will retain access to the subscription service until the next renewal date.
            </p> <br><br>    
          </div>
        </div>
       <div class="modal-footer">
        <input type="hidden" class="notranslate" name="project_id" id="project_id" value="0" />
         <a href="<?php echo $jsonArrayResponse->cancel_url;?>" target="_blank" class="btn btn-danger " style="background-color: #ef4848 !important; border-color: #ef4848;">Yes</a>

       <!--  <button id="" type="submit" class="btn btn-danger "  style="background-color: #ef4848 !important; border-color: #ef4848;" >Yes</button> -->
        <button type="button" class="btn  btn-success submit-btn1" data-dismiss="modal">No</button>
       </div>
     </form>
     </div>      
      </div>  
     </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    // actionCancelSubscription
  $('.alert-success').hide();
  $("#cancel_subscription_form").submit(function(e) {
    e.preventDefault();   
    // alert("hello"); 
     var formData = new FormData(this);
    $.ajax({
        url: '<?php echo $this->createUrl('app/CancelSubscription'); ?>',
        type: 'POST',
         data: formData,
        dataType: "json",
        success: function (data) {
          if (data.subscription_check == 1) {
            $('#subscription_modal').modal('hide');
            $('.alert-success').show();
            $('.alert-success').delay(5000).fadeOut('slow');
          }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

</script>

  <style type="">
  
    .top-h3{  
     padding-top: 20px;
    padding-bottom: 20px;
    color: #fff !important;
    border-radius: 0px !important;
  }

  .parah{font-size: 14px;}
  .bold{}
  .modal-p{font-size: 13px;}
  .modal-footer{ padding: 0px;
    padding-top: 15px;
    text-align: right;
    border-top: 1px solid #e5e5e5;}
  .panel-default{}

  @media (max-width: 768px) {
    .col-left{margin-left: 0px !important; }
   
}
</style>