<?php
$tool_currency = Yii::app()->session['user_currency'];
$user_currency_symbol = Yii::app()->session['user_currency_symbol'];
	$status_labels = "";
	$status_data = "";
	$max_data = 0;
	foreach ($contract_statuses as $total_contracts)
	{
		if ($status_labels == "") $status_labels = "'" . $total_contracts['status'] . "'";
		else $status_labels = $status_labels . ", '" . $total_contracts['status'] . "'";
		
		if ($status_data == "") $status_data = $total_contracts['total'];
		else $status_data = $status_data . "," . $total_contracts['total'];
		
		if ($max_data < $total_contracts['total']) $max_data = $total_contracts['total'];
	}
	
	if ($max_data == 0) $max_data = 1;
	else if ($max_data == 1) $max_data = 2;
	else if ($max_data < 5) $max_data = 5;
	else if ($max_data < 10) $max_data = 10;
	else if ($max_data < 20) $max_data = 20;
	else if ($max_data < 50) $max_data = 50;
	else if ($max_data < 100) $max_data = 100;
	else if ($max_data < 200) $max_data = 200;
	else if ($max_data < 500) $max_data = 500;
	else if ($max_data < 1000) $max_data = 1000;

	$category_wise_contract_data = $contracts_by_category;
	$contracts_by_category = array();
	foreach ($category_wise_contract_data as $category_wise_contract_row)
	{
		$category_wise_contract_row['full_category_name'] = $category_wise_contract_row['category_name'];
		$current_category_name = $category_wise_contract_row['category_name']; 
		if (strlen($current_category_name) >= 7) 
		{
			$abbr_category_name = "";
			$current_category_name = ucwords($current_category_name);
			foreach (explode(" ", $current_category_name) as $current_category_word)
			{
				if ($abbr_category_name == "") $abbr_category_name = substr($current_category_word, 0, 1);
				else $abbr_category_name = $abbr_category_name . " " . substr($current_category_word, 0, 1);
			}
			$current_category_name = $abbr_category_name;
		}
		$category_wise_contract_row['category_name'] = $current_category_name;
		$contracts_by_category[] = $category_wise_contract_row;
	}

	// START: Top 10 suppliers Spend with Order Count

	$contCatTotal = array_column($contracts_by_category, 'total','category_name');

	$contCatSeries = array_values($contCatTotal);
	$contCatLabel = array_keys($contCatTotal);

	$contCatFullLabel = array_column($contracts_by_category, 'full_category_name');


	// END: Top 10 suppliers Spend with Order Count

?>

          <div class="row tile_count">
          	<div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#contract_active_value_modal').modal('show');"><div id="spark19"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#contract_active_value_modal').modal('show');"><div id="spark20"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#contract_expire_30days_modal').modal('show');"><div id="spark21"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
             <a style="cursor: pointer;" onclick="$('#contract_expire_30days_modal').modal('show');"><div id="spark22"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
             <a style="cursor: pointer;" onclick="$('#contract_expire_91days_modal').modal('show');"><div id="spark23"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
              <a style="cursor: pointer;" onclick="$('#contract_expire_91days_modal').modal('show');"><div id="spark24"></div></a>
            </div>
          </div>


		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_17">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Expiring Contracts</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								<li><a onclick="expandChart1(17);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('expiring_contracts');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
			                	<div id="expiring_contracts"></div>
				           		
			              	</div>
			           	</div>
			        	</div>

					    <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_17">
						 <div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Expiring Contracts</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(17);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('expiring_contracts');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php
									$idx = 0;
									foreach ($expiring_contracts as $dataset_idx=>$a_expiring)
									{
										if ($dataset_idx === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											case 10  : $color = 'blue'; break;
											case 11  : $color = 'purple'; break;
											case 12  : $color = 'green'; break;
											case 13  : $color = 'turquoise'; break;
											case 14  : $color = 'red'; break;
											default : $color = 'salmon'; break;
										}
										?>

										<tr>
											<td style="width: 5%;"><i class="fa fa-square" style="color: <?php echo $color; ?>!important;"></i></td>
											<td style="vertical-align: middle;">
												<?php echo addslashes($a_expiring['month_name']); ?> - <?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($a_expiring['value']); ?>
											</td>
										</tr>

										<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_18">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Contracts</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(18);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('top_contracts');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<div id="top_contracts" height="157px"></div>
			              	</div>
			           	</div>
			        	</div>

						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_18">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Contracts</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(18);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('top_contracts');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
					            			<?php
					            				$idx = 0;
					            				foreach ($top_contracts as $dataset_idx => $a_dataset)
												{
													if ($dataset_idx === 'total') continue;
													$idx += 1;
													switch ($idx)
													{
														case 1  : $color = "rgba(220,220,220,0.5)"; break;
														case 2  : $color = "rgba(151,220,187,0.5)"; break;
														case 3  : $color = "rgba(187,220,151,0.5)"; break;
														case 4  : $color = "rgba(220,151,187,0.5)"; break;
														case 5  : $color = "rgba(220,187,151,0.5)"; break;
														case 6  : $color = "rgba(187,151,220,0.5)"; break;
														case 7  : $color = "rgba(120,151,187,0.5)"; break;
														case 8  : $color = "rgba(151,120,187,0.5)"; break;
														case 9  : $color = "rgba(187,120,151,0.5)"; break;
														case 10 : $color = "rgba(151,187,120,0.5)"; break;
														default : $color = "rgba(151,187,205,0.5)"; break;

													}
					            			?>
					                          		<tr>
					                            		<td style="width: 5%;"><i class="fa fa-square" style="color: <?php echo $color; ?> !important;"></i></td>
					                            		<td style="vertical-align: middle;">
					                              			<?php echo $a_dataset['dim_1']; ?> - <?php echo number_format($a_dataset['percent']) . '%'; ?>
					                            		</td>
					                          		</tr>
					                        <?php 
					                        	}
					                        ?>
					                        </table>
			              </div>
			           </div>
			        </div>

					<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_19">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Contracts By Status</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="expandChart(19);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('contracts_by_status');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<div id="contracts_by_status_apex"></div>
							</div>
						</div>
					</div>


						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_19">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Contracts By Status</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="collapseChart(19);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('contracts_by_status');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
							  <div class="x_content">
								  <table class="tile_info" style="margin-left: 0px;">
						           			<?php
						           				$idx = 0;
						           				foreach (explode(",", $status_labels) as $a_status)
												{
													$a_status = str_replace("'", "", $a_status);
													$a_status = rtrim(ltrim($a_status));
													$idx += 1;

													switch ($idx)
													{
														case 1  : $color = "#F7464A"; break;
														case 2  : $color = "#46BFBD"; break;
														case 3  : $color = "#FDB45C"; break;
														case 4  : $color = "#949FB1"; break;
														case 5  : $color = "#4D5360"; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
						           			?>

						                         		<tr>
						                            		<td>
						                              			<i class="fa fa-square" style="color: <?php echo $color; ?> !important;"></i>
						                              			<?php
						                              				echo $a_status;
																	if (isset($contract_statuses[$a_status]))
																		echo ' - (' .  $contract_statuses[$a_status]['total'] . ')';
						                              			?>
						                            		</td>
						                          		</tr>

						                    <?php
												}
									        ?>
										</table>
			              	</div>
			           	</div>
			        	</div>

		            </div>
		            <div class="clearfix"><br /></div>
				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_20">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Contracts By Location</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								<li><a onclick="expandChart(20);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('contracts_by_location');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<div id="contracts_by_location"></div>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_20">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Contracts By Status</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(20);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('contracts_by_location');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php
									$idx = 0;
									foreach ($contracts_by_location as $dataset_idx=>$a_location)
									{
										if ($dataset_idx === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = "rgba(220,220,220,0.5)"; break;
											case 2  : $color = "rgba(151,220,187,0.5)"; break;
											case 3  : $color = "rgba(187,220,151,0.5)"; break;
											case 4  : $color = "rgba(220,151,187,0.5)"; break;
											case 5  : $color = "rgba(220,187,151,0.5)"; break;
											case 6  : $color = "rgba(187,151,220,0.5)"; break;
											case 7  : $color = "rgba(120,151,187,0.5)"; break;
											case 8  : $color = "rgba(151,120,187,0.5)"; break;
											case 9  : $color = "rgba(187,120,151,0.5)"; break;
											case 10 : $color = "rgba(151,187,120,0.5)"; break;
											default : $color = "rgba(151,187,205,0.5)"; break;
										}
										?>

										<tr>
											<td style="width: 5%;"><i class="fa fa-square" style="color: <?php echo $color; ?>!important;"></i></td>
											<td style="vertical-align: middle;">
												<?php echo addslashes($a_location['location_name']); ?> - <?php echo $a_location['total'] ; ?>
											</td>
										</tr>

										<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_21">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Contracts By Category</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								<li><a onclick="expandChart(21);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('contracts_by_category');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<div id="contracts_by_category"></div>
			              	</div>
			           	</div>
			        	</div>

					   <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_21">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Contracts By Category</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(21);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('contracts_by_category');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php
									$idx = 0;
									//print_r($category_vendors_combo);die;
									foreach ($contracts_by_category as $contracts_idx => $a_contract)
									{
										if ($contracts_idx === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = "rgba(220,220,220,0.5)"; break;
											case 2  : $color = "rgba(151,220,187,0.5)"; break;
											case 3  : $color = "rgba(187,220,151,0.5)"; break;
											case 4  : $color = "rgba(220,151,187,0.5)"; break;
											case 5  : $color = "rgba(220,187,151,0.5)"; break;
											case 6  : $color = "rgba(187,151,220,0.5)"; break;
											case 7  : $color = "rgba(120,151,187,0.5)"; break;
											case 8  : $color = "rgba(151,120,187,0.5)"; break;
											case 9  : $color = "rgba(187,120,151,0.5)"; break;
											case 10 : $color = "rgba(151,187,120,0.5)"; break;
											default : $color = "rgba(151,187,205,0.5)"; break;
										}
										?>
										<tr>
											<td style="width: 5%;"><i class="fa fa-square" style="color: <?php echo $color; ?> !important;"></i></td>
											<td style="vertical-align: middle;">
												<?php echo addslashes($a_contract['category_name']); ?> (<?php echo addslashes($a_contract['full_category_name']); ?>) - <?php echo $a_contract['total'] ; ?>
											</td>
										</tr>
										<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>


<script type="text/javascript">
	
// Need to capture all these in one function later and remove duplicate functions
function apexMetriceSpendSpark19(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $metrics['contract']['active_spark19Exp'];?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            'fontFamily':'Poppins',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark19('spark19','Spend','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($metrics['contract']['active_total']); ?>','Active Contracts Value','<?php echo $metrics['contract']['active_spark19Exp'];?>');

// Need to capture all these in one function later and remove duplicate functions
function apexMetriceSpendSpark20(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $metrics['contract']['active_spark20Exp'];?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark20('spark20','Count','<?php echo number_format($metrics['contract']['active_count']); ?>','Active Contracts Count','<?php echo $metrics['contract']['active_spark20Exp'];?>');

   // Need to capture all these in one function later and remove duplicate functions
function apexMetriceSpendSpark21(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $metrics['contract']['expiry_spark21Exp'];?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            color:'red',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            color:'red',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark21('spark21','Value','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($metrics['contract']['expire_current_total']); ?>','Expire in 30 Days','<?php echo $metrics['contract']['expiry_spark21Exp'];?>');

      // Need to capture all these in one function later and remove duplicate functions
function apexMetriceSpendSpark22(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $metrics['contract']['expiry_spark22Exp'];?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            color:'red',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            color:'red',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark22('spark22','Contract Count','<?php echo number_format($metrics['contract']['expire_current_count']); ?>','Expire in 30 Days','<?php echo $metrics['contract']['expiry_spark22Exp'];?>');

         // Need to capture all these in one function later and remove duplicate functions
function apexMetriceSpendSpark23(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $metrics['contract']['expiry_spark23Exp'];?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            color:'orange',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            color:'orange',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark23('spark23','Contract Value','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($metrics['contract']['expire_later_total']); ?>','Expire in 3 Months','<?php echo $metrics['contract']['expiry_spark23Exp'];?>');

   function apexMetriceSpendSpark24(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $metrics['contract']['expiry_spark24Exp'];?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            color:'orange',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            color:'orange',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark24('spark24','Contract Count','<?php echo number_format($metrics['contract']['expire_later_count']); ?>','Expire in 3 Months','<?php echo $metrics['contract']['expiry_spark24Exp'];?>');

function createApexCont(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;

    if(chartIDL=="#expiring_contracts"){
    	$.each(series.spend, function(key, value) {
          seriesData.push(value);
        });
      	seriesL.push({'name':'Spend','type':'column','data':seriesData});
      	$.each(series.count, function(key, value) {
          	seriesMixedData.push(value);
        });
      	seriesL.push({'name':'Count','type':'line','colors':'yellow','data':seriesMixedData});
    }else {
		$.each(series, function(key, value) {  
	  		seriesData.push(value);
		});
		seriesL.push({'name':toolTip,'data':seriesData});
	}

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value);
    });

    if(chartIDL=="#spend_contracts"){horizontalL=true;}
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }},
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        },
        stacked: stackedL,
        //stackType: '100%',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        legend: {
                position: 'right',
                offsetX: 0,
                offsetY: 50
            },
      colors: colorL,
      plotOptions: {bar: {columnWidth: '35%',distributed: true, horizontal: horizontalL}},
      dataLabels: {enabled: lableL},
      //stroke: { width: [4,  4]}
       stroke: {width: [4, 4]},
      series: seriesL,
      opposite: true,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      fill: {opacity: 1},
      xaxis: {categories: categoriesL,
      	labels: {
                trim: true,
		        show: true,
		        minHeight: undefined,
            	maxHeight: 80,
            	rotate: -2,

                formatter: function (value) {
                  valueL = chartIDL!="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },}},
     
  	yaxis: {

  		"labels": {
            "formatter": function (val) {
                return  Math.trunc(val);
            }
        }
	  	},
   }
    
    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }
   var colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];
   createApexCont(330,'100%','bar',false,<?php echo json_encode($contCatSeries) ?>,<?php echo json_encode($contCatFullLabel);?>,"#contracts_by_category",colors,'Count');

   var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 292,
              width: 500,
              type: 'pie',
            },
            legend: {show: false},
            dataLabels: {enabled: false},
            labels: [<?php 
								$i = 1; 	
								foreach ($top_contracts as $a_contract)
								{ 
									echo "'".addslashes($a_contract['dim_1'])."'";
									if ($i != count($top_contracts)) echo ',';
									$i += 1;
								}
					?>],
            series: [<?php 
								$i = 1; 	
								foreach ($top_contracts as $a_contract)
								{ 
									echo round($a_contract['total']);
									if ($i != count($top_contracts)) echo ',';
									$i += 1;
								}
					?>],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 300
                    },
                    
                }
            }]

        }
        var chart = new ApexCharts(
            document.querySelector("#top_contracts"),
            options
        );
        
        chart.render();

     

            var options = {
            chart: {
                height: 292,
                width: 500,
                type: 'donut',
            },
            dataLabels: {
                enabled: true,
                 formatter: function (val, opts) {
            		return  Math.trunc(val)+"%";
        		},
            },
            series: [ <?php echo $status_data; ?> ],
            labels: [ <?php echo $status_labels; ?> ],
            /*fill: {
                type: 'gradient',
            },*/
            legend: {
            	show:false,
                formatter: function(val, opts) {
                    return val + " - " + opts.w.globals.series[opts.seriesIndex]
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
        }
        var chart = new ApexCharts(
            document.querySelector("#contracts_by_status_apex"),
            options
        );
        chart.render();
        const paper = chart.paper()

<?php $i = 1; 
$locLabel = $locSeries = $expLabel = $expSeriesTot = $expSeriesVal =  array();
foreach ($contracts_by_location as $a_contract) {
	$locLabel[] = addslashes($a_contract['location_name']);
	$locSeries[]= $a_contract['total'];
}
foreach ($expiring_contracts as $expiring_contract_row) {
 	$expLabel[] = addslashes($expiring_contract_row['month_name']); 
 	$expSeriesTot[] = $expiring_contract_row['total'];
 	$expSeriesVal[] = round($expiring_contract_row['value'], 2);
}
?>

createApexCont(330,'100%','bar',false,<?php echo json_encode($locSeries) ?>,<?php echo json_encode($locLabel);?>,"#contracts_by_location",colors,'Count');


 function createApexChartContCombo(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;

    if(chartIDL=="#vendors_by_category"){
    	$.each(series.preferred, function(key, value) { 
          seriesData.push(value);
        });
      seriesL.push({'name':'Preferred','data':seriesData});
      $.each(series.non_preferred, function(key, value) { alert(value);
          seriesMixedData.push(value);
        });
      seriesL.push({'name':'Non-Preferred','data':seriesMixedData});
      stackedL = true;
      //horizontalL=true;

    }else if(chartIDL=="#expiring_contracts" || chartIDL=="#vendor_combo_chart"){
    	$.each(series.spend, function(key, value) {
          seriesData.push(value);
        });
      seriesL.push({'name':'Spend','type':'column','data':seriesData});
      $.each(series.count, function(key, value) {
          seriesMixedData.push(value);
        });
      seriesL.push({'name':'Count','type':'line','data':seriesMixedData});
    }else if(type=='line'){
        $.each(series, function(key, value) { 
          seriesL.push({'data':value['data'],'name':value['name']});
        });
    }else{
      $.each(series, function(key, value) {  
          seriesData.push(value);
        });
      seriesL.push({'name':toolTip,'data':seriesData});
    }

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value);
    });
    if(chartIDL=="#spend_contracts"){horizontalL=true;}
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }},
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        },
        stacked: stackedL,
        //stackType: '100%',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        legend: {
                position: 'bottom',
                offsetX: 0,
                offsetY: -5
            },
      colors: colorL,
      plotOptions: {bar: {columnWidth: '35%',distributed: true, horizontal: horizontalL},line:{ style: {colors: ['#F44336', '#E91E63', '#9C27B0']},}},
      dataLabels: {enabled: lableL},
      //stroke: { width: [4,  4]}
       stroke: {width: [4, 4]},
      series: seriesL,
      opposite: true,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      fill: {opacity: 1},
      xaxis: {categories: categoriesL,labels: {
                formatter: function (value) {
                  valueL = chartIDL!="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },
                style: {fontSize: '12px'}}},
      yaxis: [{
      	labels: {
                formatter: function (value) {
                  valueL = chartIDL=="#spend_contracts" || value<20?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },
                style: {fontSize: '12px'}
              },},
              
              { axisTicks: {show: true} },
              { axisBorder: {show: true,} },
              
          	],
          	yaxis: [
	          	  	{ seriesName: 'P',
	          	  	  axisTicks: {show: true},
				      //axisBorder: { show: true,},
				      title: { text: "Spend"},
				      labels: {
				      	formatter: function (value) {
				      		valueL = chartIDL=="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return valueL
				      	},
				      }},
				  	{   opposite: true,
				  		seriesName: 'Count',
				  		//axisTicks: { show: true},
				  		//axisBorder: { show: true,},
				  		//title: { text: "Count"},
				  		labels: {
				  		show: false,
				      	formatter: function (value) {
				      		valueL = chartIDL=="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return value
				      	},
				      }

				  	}
			  	],
			markers: {size: 5,hover: {size: 6},/*colors: ['#9C27B0']*/},

          }
    

    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }

createApexChartContCombo(270,'100%','line',false,<?php echo json_encode(array('spend'=>$expSeriesVal,'','count'=>$expSeriesTot)) ?>,<?php echo json_encode($expLabel);?>,"#expiring_contracts",colors,'Count');

</script>