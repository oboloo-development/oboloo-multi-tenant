<div class="right_col" role="main">
    <div class="row-fluid tile_count">
      <div class="col-lg-12">
       <?php  $alert = Yii::app()->user->getFlash('success');
       if(!empty($alert)){?>
            <div class="alert alert-success">
                <?php echo $alert; ?>
            </div>
        <?php } ?>
        <div class="span6">
          <div class="col-lg-8">
             <h3>Raising a ticket</h3>
          </div>  
         
        </div>
        <div class="clearfix"> </div><br><br>
    </div>
    <div class="clearfix"></div>
<div class="row tile_count">
    <form id="company_form" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('app/raiseTicket'); ?>">
      <div class="col-lg-6">
     <?php $alert=Yii::app()->user->getFlash('success');
          if(!empty($alert)) { ?>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $alert;?></div>
        <?php } ?>

    <!--   <div class="form-group ">
          <label class="control-label">Full Name</label>
          <input type="text" name="name" class="form-control" value="<?php echo Yii::app()->session['full_name'];?>" placeholder="Full Name"  required />
      </div>
      <div class="form-group ">
          <label class="control-label">Company Name</label>
          <input type="text" name="company_name" class="form-control" value="<?php echo (isset(Yii::app()->session['company_name']) ? Yii::app()->session['company_name'] : ''); ?>" placeholder="Company Name"  required/>
      </div>

      <div class="form-group">
          <label class="control-label">Your Email</label>
          <input type="email" class="form-control" name="adminemailaddress" value="<?php echo (isset(Yii::app()->session['current_user_email']) ? Yii::app()->session['current_user_email'] : ''); ?>" placeholder="Your Email" value="" required/>
      </div>
  --> 
     
      <div class="form-group ">
         <label class="control-label">Please describe how we can help you</label>
         <textarea name="message" cols="40" rows="10" class="form-control"  placeholder="Please describe how we can help you" required></textarea>
      </div>
       <div class="form-group ">
         <label class="control-label">Please upload supporting documents/screenshots</label>
         <input type="file" name="file" class="form-control" >
      </div>
      <div class="clearfix"></div>
     <!-- <div class="col-md-1"></div> --><br />
     <?php $disabled = FunctionManager::sandbox(); ?>
       <button class="btn btn-success btn-margin" type="submit" id="btnSubmit" <?php echo $disabled; ?>>Send Message</button>
       <div class="clearfix"></div>
     </div>
    </form>
  </div>
  <br><br>
        
</div>
</div>
<style type="text/css">
body{
 background: #ebecf6;
}
</style>