<?php $companyLogo = FunctionManager::companyLogo();?>
<script type="text/javascript" src="<?php echo AppUrl::jsUrl('app.js'); ?>"></script>
<div class="contiainer">

  <div class="login_wrapper"> <br />
     <img class="img-responsive logo" src="<?php echo AppUrl::bicesUrl($companyLogo); ?>"   />
          <div class="clearfix"> </div>
    <div class="form login_form">
      <section class="login_content">
        <div class="clearfix"></div>

    <?php if (isset($register_success) && $register_success == 1) { ?>
        <div id="message_area">
           <h3> You have successfully registered. Please contact your administrator to activate your account. </h3>
        </div>
    <?php } ?>

    <?php if (isset($error) && $error == 2) { ?>
        <div id="message_area" >
           <h3 style="color: red !important;"> Your account has not been activated as yet.<br />Please contact your administrator.
            </h3>
        </div>
    <?php } ?>

    <div class="clearfix"></div>
          <form novalidate id="login_form" name="login_form" method="post" action="<?php echo AppUrl::bicesUrl('login'); ?>">
          <h1>Login Form</h1>
          <div class="item form-group">
            <input id="username" name="username" type="text" class="form-control" placeholder="Username" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="item form-group">
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required="required" />
          </div>
          <div class="login_btns">
            <button class="btn btn-default submit pull-right logocolorbtn" onclick="return checkLogin();">Log in</button><div class="clearfix"><br></div>
             <a class="to_forgot  pull-right" href="<?php echo AppUrl::bicesUrl('app/changePassword'); ?>">Change Password?</a>
            <a class="to_forgot  pull-right" href="<?php echo AppUrl::bicesUrl('app/forgot'); ?>">Lost your password?</a>
            
          </div>
          <input type="hidden" name="action_login" id="action_login" value="1" />

          <div class="clearfix"></div>
        </form>
          <?php if (isset($error) && $error == 1) { ?>
           <div id="message_area" style="color: red; margin-top: 10px;">
             Invalid username/password combination specified.<br />Please try again.
             <br /><br />
           </div>
          <?php } ?>
         </div>
        <div class="login_helpfularea">
          <div class="social-icons-wrapper">
            <a class="social-icon-login" href="https://www.linkedin.com/company/18585513" target="_blank"> <img src="<?php echo AppUrl::bicesUrl('images/icons/linkedin.png'); ?>" alt="" width="50px;" height="50px;" /></a>
            <a class="social-icon-login" href="https://twitter.com/oboloosoftware" target="_blank"><img src="<?php echo AppUrl::bicesUrl('images/icons/Twitter.PNG'); ?>" alt="" width="50px;" height="50px;"/></a>
            <!-- <a class="social-icon-login" href="https://www.facebook.com/oboloosoftware/" target="_blank"><img src="<?php //echo AppUrl::bicesUrl('images/icons/face.PNG'); ?>" alt="" width="50px;" height="49px;"/></a> -->
          </div>
          <div class="login_helpfularea_text">
          By logging into the oboloo system you agree to oboloo's <a href="https://oboloo.com/privacy-cookies-policy"  class="login-helpul-link" target="_blank">Privacy Policy</a>, <a href="https://oboloo.com/Terms-of-Service/" class="login-helpul-link" target="_blank">Terms of Use</a> & <a href="https://oboloo.com/acceptable-use-policy" class="login-helpul-link" target="_blank">Acceptable Use Policy</a>
          © <?php echo date('Y'); ?> oboloo Limited. All rights reserved. Republication or redistribution of oboloo content, including by framing or similar means, is prohibited without the prior written consent of oboloo Limited. oboloo & oboloo and the oboloo logo are registered trademarks of oboloo Limited and its affiliated companies. Trademark number: UK00003466421  Company Number 12420854. ICO Reference Number: ZA764971'
          <br><br>
        <a href="https://app.monspark.com/status/lyf9onlvUG" class="login-helpul-link" target="_blank" style="margin-right:10px;">Service Status</a>  Version 2.21  <a href="https://oboloo.com/release-notes/" class="login-helpul-link" target="_blank"  style="margin-left:10px;">Release Notes</a>

        </div></div><div class="clearfix"></div>
      </section><div class="clearfix"></div>
 

    <div class="clearfix"></div>

  </div>
</div>
  <style type="text/css">
body { background: #fff !important; }
</style>


