<?php
	$tool_currency = Yii::app()->session['user_currency'];
	$sparklineData = '';
    // START: Spartk13
    
    $arrayForOrd = array_column($order_spend_stats, 'total_amount');
    array_multisort($arrayForOrd, SORT_DESC, $order_spend_stats);

    $spark13Exp = '';
    error_reporting(0);
    $orderSpendTotalAmount =0; $monthlyData = array();
    if(count($order_spend_stats)>0)
    {
        foreach ($order_spend_stats as $order)
        {
            $monthlyData[$order['month_year']] = $monthlyData[$order['month_year']]+round($order['total_amount']);
            
        }

        foreach ($monthlyData as $key=>$orderValue)
        {
            $orderSpendTotalAmount += $orderValue;
            $spark13Exp .= '{ x: "'.$key.'",y: '.$orderValue.'},';
        }
    }
    // END: Spartk13

    // START: Spark14 Order count
  
    $arrayForOrd = array_column($order_count, 'total_orders');
    array_multisort($arrayForOrd, SORT_DESC, $order_count);

    $spark14Exp= '';
    $totalOrders=0;
    if(!empty($order_count))
    {
        foreach ($order_count as $order)
        {
            $totalOrders += round($order['total_orders']);
            $spark14Exp .= '{ x: "'.$order['month_year'].'",y: '.round($order['total_orders']).'},';
        }
    }
    // END: Spartk14 Order count
    // START: Spark15 Order count
    
    $arrayForOrd = array_column($order_spend_approve_stats, 'total_amount');
    array_multisort($arrayForOrd, SORT_DESC, $order_spend_approve_stats);
    $spark15Exp = '';
    $totalToApprovOrders =0;
    if(count($order_spend_approve_stats)>0)
    {
        foreach ($order_spend_approve_stats as $order)
        {
            $totalToApprovOrders += round($order['total_amount']);
            $spark15Exp .= '{ x: "'.$order['month_year'].'",y: '.round($order['total_amount']).'},';
        }
        $spark15Exp = rtrim($spark15Exp,",");
    }
    // END: Spartk15 Order to approve count

    // START: Spark16 Order count
    
    $arrayForOrd = array_column($order_pending_count, 'total_orders');
    array_multisort($arrayForOrd, SORT_DESC, $order_pending_count);
    $spark16Exp= '';
    $totalPendingOrders=0;
    if(!empty($order_pending_count))
    {
        foreach ($order_pending_count as $order)
        {
            $totalPendingOrders += round($order['total_orders']);
            $spark16Exp .= '{ x: "'.$order['month_year'].'",y: '.round($order['total_orders']).'},';
        }
    }
    // END: Spartk16 Order count

    // START: Spark17 Order count
   
    $arrayForOrd = array_column($order_spend_paid_stats, 'total_amount');
    array_multisort($arrayForOrd, SORT_DESC, $order_spend_paid_stats);

    $spark17Exp= '';
    $totalPaiedAmount=0;
    if(!empty($order_spend_paid_stats))
    {
        foreach ($order_spend_paid_stats as $order)
        {
            $totalPaiedAmount += round($order['total_amount']);
            $spark17Exp .= '{ x: "Order #'.$order['order_id'].'",y: '.round($order['total_amount']).'},';
        }
    }
    // END: Spartk17 Order count

    // START: Spark18 Order count

    $arrayForOrd = array_column($order_to_receive, 'total_orders');
    array_multisort($arrayForOrd, SORT_DESC, $order_to_receive);

    $spark18Exp= '';
    $ordersToReceive=0;
    if(!empty($order_to_receive))
    {
        foreach ($order_to_receive as $order)
        {
            $ordersToReceive += round($order['total_orders']);
            $spark18Exp .= '{ x: "'.$order['month_year'].'",y: '.round($order['total_orders']).'},';
        }
    }
    // END: Spartk18 Order count

	$department_labels = $order_data = $expense_data = array();
	foreach ($departments_by_spend_type as $data_points)
	{
		$current_department_name = "";
		$orders = $expenses = 0;
		
		foreach ($data_points as $data_point)
		{
			$current_department_name = $data_point['location_name'];
			if ($data_point['type'] == 'E') $expenses += round($data_point['total'], 2);
			else $orders += round($data_point['total'], 2);
		}
		$department_labels[] =$current_department_name;

		$expense_data[] = $expenses;
		$order_data[]   = $orders;
	}
	$status_labels = array();
	$status_data = array();
	$max_data = 0;
	foreach ($order_statuses as $order_status => $total_orders)
	{
		$status_labels[]= $order_status;
		$status_data[] = $total_orders;
		if ($max_data < $total_orders) $max_data = $total_orders;
	}
	
	if ($max_data == 0) $max_data = 1;
	else if ($max_data == 1) $max_data = 2;
	else if ($max_data < 5) $max_data = 5;
	else if ($max_data < 10) $max_data = 10;
	else if ($max_data < 20) $max_data = 20;
	else if ($max_data < 50) $max_data = 50;
	else if ($max_data < 100) $max_data = 100;
	else if ($max_data < 200) $max_data = 200;
	else if ($max_data < 500) $max_data = 500;
	else if ($max_data < 1000) $max_data = 1000;

	// START: Spend by Location - Total vs. Future
	$locationSpend = array_column($location_purchases, 'spend_total','location_name');

	$locSpendSeries = array_values($locationSpend);
	$locationSpendLabels = array_keys($locationSpend);

	$locationSpend = array_column($location_purchases, 'spend_future','location_name');
	$locFutureSpendSeries = array_values($locationSpend);

	// END: Spend by Location - Total vs. Future
?>

          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6">
            	 <a style="cursor: pointer;" onclick="$('#total_year_spend_modal').modal('show');"><div id="spark13"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
              <a style="cursor: pointer;" onclick="$('#total_year_ordercount_modal').modal('show');"><div id="spark14"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
              <a style="cursor: pointer;" onclick="$('#approved_spend_modal').modal('show');"><div id="spark15"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
              <a style="cursor: pointer;" onclick="$('#approved_ordertoapprove_modal').modal('show');"><div id="spark16"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
             <a style="cursor: pointer;" onclick="$('#paid_spend_modal').modal('show');"><div id="spark17"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#received_spend_modal').modal('show');"><div id="spark18"></div></a>
             
            </div>
          </div>


		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_30">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Locations By Procurement Orders vs. Travel/Expenses</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(30);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('departments_by_spend_type');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<div id="departments_by_spend_type"></div> 
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_30">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Locations By Procurement Orders vs. Travel/Expenses</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(30);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('departments_by_spend_type');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php
									$idx = 0;
									foreach ($departments_by_spend_type as $data_index =>$data_points)
									{
										$current_department_name = "";
									    $orders = $expenses = 0;

										if ($data_index === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}

										?>

										<tr>
											<td style="font-weight: bold;">
												<i class="fa fa-square <?php echo $color; ?>" style="margin-left: 10px;margin-top:-4px;"></i>
												<?php echo $data_points[0]['location_name']; ?></td>

										</tr>

										<?php

										foreach ($data_points as $data_point) {
											$current_department_name = $data_point['location_name'];
											if ($data_point['type'] == 'E') $expenses += round($data_point['total'], 2);
											else $orders += round($data_point['total'], 2);
										}
											?>
											<tr>
												<td>
													<table class="tile_info" style="margin-left: 0px;">
														<tr>
															<td style="width: 5%;">
																<i  style="margin-left: 35px;background-color:#E1E1E1;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
															</td>
															<td >
																<?php echo 'Procurement Orders'; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' .$orders; ?>
															</td>
														</tr>
														<tr>
															<td style="width: 5%;">
																<i  style="margin-left: 35px;background-color:#B7D8E8;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
															</td>
															<td>
																<?php echo 'Travel/Expenses'; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' . $expenses; ?>
															</td>
														</tr>
														<tr><td colspan="2" style="height: 10px;">&nbsp;</td></tr>
													</table>
												</td>
											</tr>
											<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		           

				<div class="row">

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_32">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Preferred Vs. Non Preferred Suppliers Used</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								<li><a onclick="expandChart(32);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendors_by_type');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<div id="vendors_by_type"></div>
			              	</div>
			           	  </div>
			        	</div>

					    <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_32">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Preferred Vs. Non Preferred Suppliers Used</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(32);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('vendors_by_type');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
										<tr>
											<td style="width: 5%;"><i class="fa fa-square" style="color: #007613;"></i></td>
											<td style="vertical-align: middle;">
												Preffered - <?php echo round($preferred_vendors_used, 2); ?>
											</td>
										</tr>

									   <tr>
										<td style="width: 5%;"><i class="fa fa-square" style="color: #E4941B"></i></td>
										<td style="vertical-align: middle;">
											Non Preffered - <?php echo number_format($non_preferred_vendors_used); ?>
										</td>
									   </tr>

								</table>
							</div>
						</div>
					</div>

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_33">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Invoices Paid vs. Unpaid</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart(33);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('invoices_paid_unpaid');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<div id="invoices_paid_unpaid"></div>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_33">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Invoices Paid vs. Unpaid</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(33);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('vendors_by_type');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<tr>
										<td style="width: 5%;"><i class="fa fa-square" style="color: #59FF68;"></i></td>
										<td style="vertical-align: middle;">
											Paid - <?php echo $invoices_paid_unpaid['paid']; ?>
										</td>
									</tr>

									<tr>
										<td style="width: 5%;"><i class="fa fa-square red"></i></td>
										<td style="vertical-align: middle;">
											Unpaid - <?php echo $invoices_paid_unpaid['unpaid']; ?>
										</td>
									</tr>

								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            

				<div class="row">

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_28">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Spend by Location - Total vs. Pending</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart(28);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('locations_invoice_status');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<div id="locations_invoice_status"></div>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_28">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Spend by Location - Total vs. Pending</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(28);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('locations_invoice_status');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php

									$idx = 0;
									foreach ($location_purchases as $data_index =>$data_location_purchases)
									{

										if ($data_index === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}

										?>

										<tr>
											<td style="font-weight: bold;">
												<i class="fa fa-square <?php echo $color; ?>" style="margin-left: 10px;margin-top:-4px;"></i>
												<?php echo $data_location_purchases['location_name']; ?></td>

										</tr>


										<tr>
											<td>
												<table class="tile_info" style="margin-left: 0px;">
													<tr>
														<td style="width: 5%;">
															<i  style="margin-left: 35px;background-color:#E1E1E1;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
														</td>
														<td >
															<?php echo 'Total'; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' .number_format($data_location_purchases['spend_total']); ?>
														</td>
													</tr>
													<tr>
														<td style="width: 5%;">
															<i  style="margin-left: 35px;background-color:#B7D8E8;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
														</td>
														<td>
															<?php echo 'Future'; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format($data_location_purchases['spend_future']); ?>
														</td>
													</tr>
													<tr><td colspan="2" style="height: 10px;">&nbsp;</td></tr>
												</table>
											</td>
										</tr>
										<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>


						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_29">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Order Totals By Status</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(29);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('orders_by_status');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
		                      	<div id="orders_by_status"></div>
			              	</div>
			           	</div>
			        	</div>
						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_29">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Order Totals By Status</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(29);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('orders_by_status');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
						           			<?php
						           				$idx = 0;
						           				foreach (explode(",", $status_labels) as $a_status)
												{
													$a_status = str_replace("'", "", $a_status);
													$a_status = rtrim(ltrim($a_status));
													$idx += 1;
																	
													switch ($idx)
													{
														case 1  : $color = "#F7464A"; break;
														case 2  : $color = "#46BFBD"; break;
														case 3  : $color = "#FDB45C"; break;
														case 4  : $color = "#949FB1"; break;
														case 5  : $color = "#4D5360"; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
						           			?>
						
						                         		<tr>
						                            		<td>
						                            			<nobr>
						                              			<i class="fa fa-square" style="color: <?php echo $color; ?> !important;"></i>
						                              			<?php 
						                              				echo $a_status;
																	if (isset($order_statuses[$a_status]))
																		echo ' - (' .  $order_statuses[$a_status] . ')';
						                              			?>
						                              			</nobr>
						                            		</td>
						                          		</tr>
							                        
						                    <?php 
												}
									        ?>					            			
										</table>
			              </div>
			           </div>
			           </div>


		            </div>
		            <div class="clearfix"><br /></div>
		            

<script type="text/javascript">
	

 // Need to capture all these in one function later

   function apexMetriceSpendSpark13(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark13Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark13('spark13','Spend','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($orderSpendTotalAmount); ?>','Total Year Spend',<?php echo $spark13Exp;?>);

   function apexMetriceSpendSpark14(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark14Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark14('spark14','Orders','<?php echo number_format($totalOrders, 0);?>','Total Year Orders',<?php echo $spark14Exp;?>);

   function apexMetriceSpendSpark15(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark15Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            'fontFamily':'Poppins',
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            'fontFamily':'Poppins',
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark15('spark15','Total Spend','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalToApprovOrders); ?>','Spend To Approve','<?php echo $spark15Exp;?>');

   function apexMetriceSpendSpark16(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark16Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark16('spark16','Orders','<?php echo number_format($totalPendingOrders, 0); ?>','Orders To Approve','<?php echo $spark16Exp;?>');

   function apexMetriceSpendSpark17(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark17Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark17('spark17','Orders','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalPaiedAmount); ?>','Orders To Be Paid','<?php echo $spark17Exp;?>');

function apexMetriceSpendSpark18(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark18Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
        	'fontFamily':'Poppins',
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark18('spark18','Orders','<?php echo number_format($ordersToReceive, 0); ?>','Orders To Be Received','<?php echo $spark17Exp;?>');

    /**/var options = {
            chart: {
                height: 230,
                type: 'bar',
                stacked: true,
                stackType: '100%'
            },
            //plotOptions: {bar: {columnWidth: '35%',distributed: false, horizontal: false}},
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            series: [{
                name: 'Procurement Orders',
                data: [<?php echo implode(",",$order_data);?>]
            },{
                name: 'Travel/Expenses',
                data: [<?php echo implode(",",$expense_data);?>]
            }],
            xaxis: {
                categories: [<?php echo "'".implode("','",$department_labels)."'";?>],
                labels: {
                trim: true,
		        show: true,
		        minHeight: undefined,
            	maxHeight: 70,
            	rotate: -5,
		       /* formatter: function (value) {
                return value.substring(0, 4)+'...';
                },
                tooltip: {
            		formatter: function (value) {
                		return value.substring(0, 1)+'...';
                	},
        		},*/
            }},
            yaxis:{ 
				    labels: {
				      	formatter: function (value) {
				      		//valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return value
				      	},
				     }
				},
            fill: {
                opacity: 1
            },
            
            legend: {
                position: 'bottom',
                offsetX: 0,
                offsetY: 0
            },
        }

       var chart = new ApexCharts(
            document.querySelector("#departments_by_spend_type"),
            options
        );
        chart.render();


        /**/var options = {
            chart: {
                height: 240,
                type: 'bar',
                stacked: true,
                stackType: '100%'
            },
            //plotOptions: {bar: {columnWidth: '35%',distributed: false, horizontal: false}},
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            series: [{
                name: 'Total Spend',
                data: [<?php echo implode(",",$locSpendSeries);?>]
            },{
                name: 'Pending Spend',
                data: [<?php echo implode(",",$locFutureSpendSeries);?>]
            }],
            xaxis: {
                categories: [<?php echo "'".implode("','",$locationSpendLabels)."'";?>],
                labels: {
                trim: true,
		        show: true,
		        minHeight: undefined,
            	maxHeight: 70,
            	rotate: -5,
		       /* formatter: function (value) {
                return value.substring(0, 4)+'...';
                },
                tooltip: {
            		formatter: function (value) {
                		return value.substring(0, 1)+'...';
                	},
        		},*/
            }},
            yaxis:{ 
				    labels: {
				      	formatter: function (value) {
				      		valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return valueL
				      	},
				     }
				},
            fill: {
                opacity: 1
            },
            
            legend: {
                position: 'bottom',
                offsetX: 0,
                offsetY: 0
            },
        }

       var chart = new ApexCharts(
            document.querySelector("#locations_invoice_status"),
            options
        );
        chart.render();

       
     var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 292,
              width: 600,
              type: 'pie',
            },
           // legend: {show: false},
            dataLabels: {enabled: false},
            labels: ["Preferred Suppliers","Non Preferred Suppliers"],
            series: [<?php echo number_format($preferred_vendors_used,0);?>,<?php echo number_format($non_preferred_vendors_used,0);?>],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 300
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]

        }
        var chart = new ApexCharts(
            document.querySelector("#vendors_by_type"),
            options
        );
        
        chart.render();

        var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 292,
              width: 600,
              type: 'pie',
            },
           // legend: {show: false},
            dataLabels: {enabled: false},
            labels: ["Paid Invoices","Non Paid Invoices"],
            series: [<?php echo number_format($invoices_paid_unpaid['paid'],0);?>,<?php echo number_format($invoices_paid_unpaid['unpaid'],0);?>],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 300
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]

        }
        var chart = new ApexCharts(
            document.querySelector("#invoices_paid_unpaid"),
            options
        );
        
        chart.render();


        var options = {
            chart: {
                height: 240,
                type: 'area',
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },
            series: [{
                name: 'Orders',
                data: [<?php echo implode(",",$status_data);?>]
            }],

            xaxis: {
                categories: [<?php echo "'".implode("','",$status_labels)."'";?>], 
                labels: {
                trim: true,
		        show: true,
		        minHeight: undefined,
            	maxHeight: 170,
            	rotate: -45,
            	}             
            },
            toolbar: {
        tools: {
          selection: false
        }
      },
      markers: {
        size: 6,
        hover: {
          size: 10
        }
      },
            /*tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
            }*/
        }

        var chart = new ApexCharts(
            document.querySelector("#orders_by_status"),
            options
        );

        chart.render();

</script>