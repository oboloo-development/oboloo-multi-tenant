<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Review Data Before Uploading</h3><br />
        </div>
        <div class="clearfix"> </div>
    </div>
    <h1 class="alertdeleteRow"></h1> 
	  <table id="vendor_table" class="table table-striped table-bordered contract-table" style="width: 100%;">
      <thead>
        <tr>
          <th></th>
          <th>Vendor Name</th>
          <th>Contact Name</th>
          <th class="th-center">email</th>
          <th>Address1</th>
          <th >Address2</th>
          <th class="th-center" >City</th>
          <th class="th-center" >State Or County</th>
          <th class="th-center" >Zip</th>
          <th class="th-center" >Phone</th>
          <th class="th-center" >Mobile</th>
          <th class="th-center" >Created Date</th>
          <th class="th-center" >Delete</th>
        </tr>
      </thead>

      <tbody>
       <?php 
        date_default_timezone_set(Yii::app()->params['timeZone']);
        set_time_limit(0);
        error_reporting(0);

        $row = 1;
        $checkRecords = 0;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            // Start: Check Count
            while (($data = fgetcsv($handle)) !== FALSE) {
               
                if($row>1  && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]) || !empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9]))){
                    $checkRecords++;
                }
                $row++;
            }
            if($checkRecords>100){
                Yii::app()->user->setFlash('error', '<strong>Warning!</strong> You are trying to import more than 100 records. Only 100 or less can be uploaded at one time');
                $this->redirect(array('app/import'));
            }
        }
        
        if (($handle = fopen($filePath, "r")) !== FALSE) {
        $row = 1;
        $recordArr = array();
        while (($data = fgetcsv($handle)) !== FALSE) {

            
            if($row>1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]) || !empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9]))){
               $vendorName                  = addslashes(trim($data[0]));
               $contactName                 = addslashes(trim($data[1]));
                $email                      = addslashes(trim($data[2]));
                $address1                   = addslashes(trim($data[3]));
                $address2                   = addslashes(trim($data[4]));
                $city                       = addslashes(trim($data[5]));
                $stateOrCounty              = addslashes(trim($data[6]));
                //$country                    = addslashes(trim($data[7]));
                $zip                        = addslashes(trim($data[7]));
                $phone                      = addslashes(trim($data[8]));
                $mobile                     = addslashes(trim($data[9]));
                $createdDate                = date('Y-m-d H:i:s');


                $checkVendor = new Vendor();
                $password = $checkVendor->generateRandomString(10);
                $vendorCheckReader = $checkVendor->getOne(array('emails' => $email));

                ?>

                <tr class="row_cont_<?php echo $row;?>">
                  <td ><?php echo $row; ?></td>
                  <td ><?php echo $vendorName; ?></td>
                  <td><?php echo $contactName; ?></td>
                  <td><?php echo $email; ?></td>
                  <td><?php echo $address1; ?></td>
                  <td><?php echo $address2; ?></td>
                  <td><?php echo $city; ?></td>
                  <td><?php echo $stateOrCounty; ?></td>
                  <td><?php echo $zip; ?></td>
                  <td><?php echo $phone; ?></td>
                  <td><?php echo $mobile; ?></td>
                  <td><?php echo $createdDate; ?></td>
                  <td><button type="submit" class="btn btn-danger btn-xs" onclick="deleteVendorRow(<?php echo $row ;?>);">Delete</button></</td>
                </tr>
                <?php
                  $recordArr[$row] = array(
                            'vendorName'=>$vendorName,
                            'contactName'=>$contactName,
                            'email'=>$email,
                            'address1'=>$address1,
                            'address2'=>$address2,
                            'city'=>$city,
                            'stateOrCounty'=>$stateOrCounty,
                            'zip'=>$zip,
                            'phone'=>$phone,
                            'mobile'=>$mobile,
                            'createdDate'=>$createdDate,
                            );
                 }
            $row++; }
            if(!empty($recordArr)){
              $recordArrTest = Yii::app()->session['vendor_import'] = $recordArr;
            }
        } ?>
      </tbody>
  </table>

 <div class="col-md-12 col-xs-12" >
    <div class="col-md-3 col-xs-12 outer" >
    <div class="inner">
     <form action="<?php echo AppUrl::bicesUrl('app/import'); ?>" method="post" id="uploadVendorData">
      <input type="hidden" name="vendor_import" value="1">
      <a href="#" class="btn btn-info" onclick="uploadVendorData();">Upload Selected Data</a>
     </form>
    </div>

     <div class="inner">
      <form action="<?php echo AppUrl::bicesUrl('app/import'); ?>" method="post" onsubmit="return confirm('Do you want to cancel?');">
       <input type="hidden"  name="vendor_import_cancel" value="1">
       <button type="submit" name="submit" class="btn btn-danger">Cancel</button>
      </form> 
     </div>

    </div>
 </div>
</div>

<script type="text/javascript">
   $(document).ready(function(){

    $('#vendor_table').dataTable({
        "pageLength": 100,
        "columnDefs": [ 
          { "targets": 0 , "orderable": 100 },
      /*{ type: 'date-uk', targets: 7 },
      { type: 'date-uk', targets: 8 },
      { type: 'date-uk', targets: 9 },         */
      /*{ "type": "sort-month-year", targets: 5 }, */        
        ],
        "order": []
    });

   });

    function deleteVendorRow(vendor_number){
        var vendor_number = vendor_number;
        // console.log(row_number);
        var result = confirm("Want to delete?");
        if (result) {
          $.ajax({
            url: '<?php echo $this->createUrl('app/deleteVendorImport'); ?>',
            type: 'POST',
             data: {vendor_number:vendor_number},
            dataType: "json",
            success: function (data) {
                if(data.msg ==1){
                  $(".row_cont_"+vendor_number).hide();
                  $(".row_cont_"+vendor_number).empty();
                  $(".alertdeleteRow").html('<div class="alert alert-danger alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Supplier </strong>Deleted Successfully</div>');
                }
            },
            
        });
        }
     }

     function uploadVendorData(){
      $.confirm({
        title: '',
        content: "Are you sure you want to upload the selected data? Once uploaded, records can only be archived or edited" +
        '<form action="<?php echo AppUrl::bicesUrl('app/import'); ?>" class="formName">' +
        '<input type="hidden" name="vendor_import" value="1">'+
        '</form>',
        buttons: {
        formSubmit: {
        text: 'Upload Selected Data',
        btnClass: 'btn-blue',
        action: function () {
          $('#uploadVendorData').submit();
        }
        },
        cancel: function () {
        //close
        },
        },
        onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
      });
     }

</script>


<style type="text/css">
  #form_sumit_btn{background-color: #1abb9c!important; border-color: #1abb9c!important; }
  .center { display: block; margin-left: auto; margin-right: auto; width: 30%;}
    .outer{  width:100%; margin-top: 10px; }
.inner{ display: inline-block;}
.jconfirm .jconfirm-box div.jconfirm-content-pane .jconfirm-content { overflow: hidden; text-align: center;line-height: normal; }
</style>
     
    