<?php
$tool_currency = Yii::app()->session['user_currency'];
$user_currency_symbol = Yii::app()->session['user_currency_symbol'];

	$orderSpendTotalAmount =0; 
    if(count($order_spend_stats)>0)
    {
        foreach ($order_spend_stats as $order)
        {
            $orderSpendTotalAmount += $order['total_amount'];
            
        }
    }

	$avg_budget_left = 0;
	if ($metrics['budget']['total_budget'])
		$avg_budget_left = (($metrics['budget']['total_budget'] - $metrics['spend']['spend_total']) * 100) / $metrics['budget']['total_budget']; 
?>
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Budget</span>
              <div class="count"><a style="cursor: pointer;" onclick="$('#total_budget_modal').modal('show');"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['budget']['total_budget']); ?></a></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Year Spend</span>
              <div class="count"><a style="cursor: pointer;" onclick="$('#total_year_spend_modal').modal('show');"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($orderSpendTotalAmount); ?></a></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Budget Left</span>
              <div class="count"><a style="cursor: pointer;" onclick="$('#total_left_budget_modal').modal('show');"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['budget']['total_budget'] - $metrics['spend']['spend_total']); ?></a></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-list"></i> Average Budget Left</span>
              <div class="count"><a style="cursor: pointer;" onclick="$('#total_avg_budget_modal').modal('show');"><?php echo number_format($avg_budget_left) . '%'; ?></a></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="color: red !important;">
              <span class="count_top"><i class="fa fa-list"></i> Departments Over Budget</span>
              <div class="count"><a style="cursor: pointer;" onclick="$('#total_over_budget_modal').modal('show');"><?php echo number_format($metrics['budget']['departments_over_budget']); ?></a></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-list"></i> Budget Pct This Month</span>
              <div class="count"><a style="cursor: pointer;" onclick="$('#total_current_month_budget_modal').modal('show');"><?php echo number_format($metrics['budget']['budget_pct_current_month']) . '%'; ?></a></div>
            </div>
          </div>


				<div class="clearfix"><br /></div>
                <div class="row" style="margin-left: 25%;">
				<div class="col-md-4 location" style="text-align: center;">
					<select name="budget_location_id[]" id="budget_location_id" multiple class="form-control" onchange="loadBudgetDepartments(); createDepartmentChart(1);">
						<?php
						$i = 0 ;
						foreach ($locations as $location) {
							if(isset($location_id[$i])){
								?>
								<option value="<?php echo $location['location_id']; ?>"
									<?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
									<?php echo $location['location_name']; ?>
								</option>
							<?php } else { ?>
								<option value="<?php echo $location['location_id']; ?>">
									<?php echo $location['location_name']; ?>
								</option>
							<?php } ?>
							<?php
							$i++;
						} ?>
					</select>
				</div>
				<div class="col-md-3" style="text-align: center;">
					<select name="budget_department_id" id="budget_department_id" class="form-control" onchange="createDepartmentChart(1);">
					</select>
				</div>
             <!--
			  <div class="col-md-2" style="text-align: right;">
					<select name="year" id="year" class="form-control" onchange="createDepartmentChart(1);">
						<?php for ($y=(date("Y") - 3); $y<=(date("Y") + 1); $y++) { ?>
							<option value="<?php echo $y; ?>"
									<?php if ($y == date("Y")) echo ' selected="SELECTED" '; ?>>
								<?php echo $y; ?>
							</option>
						<?php } ?>
					</select>
			  </div>
			  -->
			 </div>
				<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_50">
			              <div class="x_panel tile fixed_height_600 overflow_hidden">
			                <div class="x_title">
			                  <div class="col-md-4">
			                  		<h2>Departments Spend vs Budget</h2>
			                  </div>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(50);"><i class="fa fa-arrows-h"></i></a></li>
				                  <li><a onclick="exportBudgetChart('departments_by_monthly_spend');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<div id="departments_by_monthly_spend"></div>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_50">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Departments Spend vs Budget</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(50);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('departments_by_monthly_spend');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<tr>
										<thead>
										<th></th>
										<th>Department</th>
										<th style="text-align: center;">Budget</th>
										<th style="text-align: center;">Spent</th>
										</thead>
									</tr>
									<tbody id="department_budget_vs_spent_table_ajax_tbody">
									</tbody>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>


<style>

	.location  .multiselect {
		width: 180%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}

	.btn .caret {
		float: right;
		margin-top: 10px;

	}

</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	$('#budget_location_id').multiselect(getOptions(true));

var monthly_spend_chart_data;
var monthly_spend_chart_settings;
var monthly_spend_chart;

$(document).ready(function() {
	loadBudgetDepartments();
	createDepartmentChart(0);
});


function createDepartmentChart(changed_period)
{
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('budget/getDepartmentSpendData'); ?>',
        type: 'POST', dataType: 'json', 
        data: { 
        		department_id: $('#budget_department_id').val(), 
        		location_id: $('#budget_location_id').val(), 
        		year: $('#user_selected_year').val() 
        },
        success: function(budget_data) {
            var chart_labels = new Array();
            var budgets = new Array();
            var spends = new Array();
            var td_chart_colors_bs = new Array();
            var maxValue = 0;
			var td_table_html_bs = "";
            
            for (var i=0; i<budget_data.length; i++) {
            	chart_labels[i] = budget_data[i].department_name;
            	budgets[i] = budget_data[i].budget;
            	spends[i] = budget_data[i].spent;
            	
            	if (budget_data[i].spent > maxValue) maxValue = budget_data[i].spent;
            	if (budget_data[i].budget > maxValue) maxValue = budget_data[i].budget;

				if (i == 1) td_chart_colors_bs[i] = '#3498DB';
				else if (i == 2) td_chart_colors_bs[i] = '#9B59B6';
				else if (i == 3) td_chart_colors_bs[i] = '#1ABB9C';
				else if (i == 4) td_chart_colors_bs[i] = '#00CED1';
				else if (i == 5) td_chart_colors_bs[i] = '#E74C3C';
				else if (i == 6) td_chart_colors_bs[i] = '#FAEBD7';
				else if (i == 7) td_chart_colors_bs[i] = '#7FFFD4';
				else if (i == 8) td_chart_colors_bs[i] = '#FFE4C4';
				else if (i == 9) td_chart_colors_bs[i] = '#BDB76B';
				else td_chart_colors_bs[i] = '#FFA07A';

				td_table_html_bs += '<tr>';
				td_table_html_bs += '<td style="width: 5%;"><i class="fa fa-square" style="color: ' + td_chart_colors_bs[i] + ' !important;"></i></td>';
				td_table_html_bs += '<td style="vertical-align: middle;">' + budget_data[i].department_name + '</td>';
				td_table_html_bs += '<td style="text-align: center;vertical-align: middle;"><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + budget_data[i].budget + '</td>';
				td_table_html_bs += '<td style="text-align: center;vertical-align: middle;"><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + budget_data[i].spent + '</td>';
				td_table_html_bs += '</tr>';
			}

			$('#department_budget_vs_spent_table_ajax_tbody').html(td_table_html_bs);
            
			var options = {
            chart: {
                height: 350,
                type: 'bar',
              /*  stacked: true,
                stackType: '100%',*/
                toolbar: {
                    show: true
                },
                zoom: {
                    enabled: true
                },
                id:"apexChartLocBudget"
            },
            plotOptions: {bar: {horizontal: false,}},
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            series: [
            			{ name: 'Spent', backgroundColor: '#949FB1', data: spends }, 
						{ name: 'Budget', backgroundColor: '#FDB45C', data: budgets } 
					],
			dataLabels: {
                enabled: true,
                formatter: function (value) {
				      		valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return valueL
				      	},
            },
            xaxis: {
                categories: chart_labels,
                labels: {
                trim: true,
		        show: true,
		        minHeight: undefined,
            	maxHeight: 70,
            	rotate: -5,
		       /* formatter: function (value) {
                return value.substring(0, 4)+'...';
                },
                tooltip: {
            		formatter: function (value) {
                		return value.substring(0, 1)+'...';
                	},
        		},*/
            }},
            yaxis:{ 
				   labels: {
				      	formatter: function (value) {
				      		valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return valueL
				      	},
				     }
				},
            fill: {
                opacity: 1
            },
            legend: {
                position: 'bottom',
                offsetX: 0,
                offsetY: 50
            },

            tooltip: {
					y: {
					 formatter: function (value) {
				      	valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      	return valueL
				      	},
					},
					marker: {
					show: true,
					},
				}
        }
        var chart = new ApexCharts(
            document.querySelector("#departments_by_monthly_spend"),
            options
        );
        chart.render();
        console.log("New Line");
        console.log(changed_period);
        if (changed_period == 1) {
	        ApexCharts.exec('apexChartLocBudget', 'updateOptions', {
	        	xaxis: {
	        		categories: chart_labels,
	        	}
	        }, false, true);
	        ApexCharts.exec('apexChartLocBudget', 'updateSeries', [
	        				{ name: 'Spent', backgroundColor: '#949FB1', data: spends }, 
							{ name: 'Budget', backgroundColor: '#FDB45C', data: budgets }
						],true);
    	}
			
			/*if (changed_period == 1) monthly_spend_chart.destroy();
			if (maxValue != 0) delete monthly_spend_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'];
			else monthly_spend_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'] = 1000;*/
			//monthly_spend_chart = new Chart($('#departments_by_monthly_spend'), monthly_spend_chart_settings);
        }
    });
	
}


function loadBudgetDepartments()
{
	if ($('#budget_location_id').val() == 0) $('#budget_department_id').html('');
	else
	{
	    $.ajax({
	        type: "POST", data: { location_id: $('#budget_location_id').val() }, 
	        dataType: "json", async: false,
	        url: BICES.Options.baseurl + '/locations/getDepartments',
	        success: function(options) {
	        	var options_html = '';
	        	for (var i=0; i<options.length; i++)
	        		options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
	        	$('#budget_department_id').html(options_html);
	        },
	        error: function() { $('#budget_department_id').html(''); }
	    });
	}
}


function exportBudgetChart(canvas_id)
{
    var department_id = $('#budget_department_id').val(); 
    var location_id = $('#budget_location_id').val(); 

	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id + '&location_id=' + location_id + '&department_id=' + department_id;
}



</script>
