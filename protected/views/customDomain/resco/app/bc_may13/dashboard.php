<?php $tool_currency = Yii::app()->session['user_currency'];
$user_currency_symbol = Yii::app()->session['user_currency_symbol'];
// Start Apex Library for monthly report
$month = 01;
$months = array();
for($month; $month <= 12; $month++) {
    $months[] = date('M',strtotime(date("Y-$month-01")));
}
$monthlySeriesData = array();

foreach($current_year  as $m_key=>$month_value){
  if($m_key !="total"){
    if(isset($month_value['total']))
      $m_total = $month_value['total'];
    else
      $m_total = 0;
    $mothYear = date("m-Y",strtotime($month_value['dim_1']));
    $monthlySeriesData[$mothYear] = round((isset($monthlySeriesData[$mothYear])?$monthlySeriesData[$mothYear]:0)+$m_total,2);
  }
}

$current_year_date = date('Y');
$diff_date = $current_year_date-$current_year_start_date;

$end = date('Y-m-d', strtotime('+5 years'));

$monthlySeriesNewArr = array();

$monthlySeries = '[';
for($i=0; $i<$diff_date+1;  $i++){
  $year = intval($current_year_start_date)+$i;
  $data = array();
  $monthlySeries .= '{
    name:"'.($year).'",
    data: [';
  for($j=1; $j<=12; $j++){
    $randNum = rand(10,100);
    if(!in_array($j,array(10,11,12))){
      $monthJ ="0".$j; 
    }else{
      $monthJ =$j; 
    }
    $data[] = (isset($monthlySeriesData[$monthJ."-".$year])?$monthlySeriesData[$monthJ."-".$year]:0);
    $monthlySeries .= (isset($monthlySeriesData[$monthJ."-".$year])?$monthlySeriesData[$monthJ."-".$year]:0).",";
  }
  $monthlySeriesNewArr[]=array('name'=>$year,'data'=>$data);
  $monthlySeries .= "]
},
";
}
$monthlySeries .= ']';
// Start: Apex Library for vendor pie report
$pieVendorTotal = array_column($top_vendors, 'total','vendor');
foreach($pieVendorTotal as $key=>$value)
{
    if(empty($key))
        unset($pieVendorTotal[$key]);
}
$pie_series = "[".implode(',',array_values ($pieVendorTotal ))."]";
$pie_label = "[".'"'.implode('","',array_keys($pieVendorTotal )).'"'."]";
// END: Apex Library for vendor pie report

// Start: Apex Library for location bar
$locationTotal = array_column($top_locations, 'total','location');
foreach($locationTotal as $key=>$value)
{
    if(empty($key))
        unset($locationTotal[$key]);
}
$location_series = array_values($locationTotal);
$location_label = array_keys($locationTotal);
// END: Apex Library for for location bar

// Start: Apex Library for year to year bar
$yearlyTotal = array_column($year_over_year, 'total','dim_1');
foreach($yearlyTotal as $key=>$value)
{
    if(empty($key))
        unset($yearlyTotal[$key]);
    else if($key=="Total")
        unset($yearlyTotal[$key]);
}
$yearly_series = array_values($yearlyTotal);
$yearly_label = array_keys($yearlyTotal);
// END: Apex Library for for year to year bar


// Start: Apex Library for year to year bar
$topContractsTotal = array_column($top_contracts, 'total','dim_1');
foreach($topContractsTotal as $key=>$value)
{
    if(empty($key))
        unset($topContractsTotal[$key]);
    else if($key=="Total")
        unset($topContractsTotal[$key]);
}
$top_contract_series = array_values($topContractsTotal);
$top_contract_label = array_keys($topContractsTotal);
// END: Apex Library for for year to year bar

?>
<!-- page content -->
<div class="right_col" role="main">
	<form action="<?php echo AppUrl::bicesUrl('dashboard'); ?>" method="post" name="dashboard_form" id="dashboard_form">
    <div class="clearfix tile_count">
        <div class="col-md-7 col-sm-7 col-xs-12 pull-left">
            <h3>Dashboard</h3>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 text-responsive" style="text-align: right;">
			<h5 class="dashboard-check">
				<input type="checkbox" name="exclude_tax" id="exclude_tax" onclick="$('#dashboard_form').submit();" 
					<?php if (isset(Yii::app()->session['exclude_tax_from_dashboard']) && Yii::app()->session['exclude_tax_from_dashboard']) echo ' checked="CHECKED" '; ?> />
				<label for="exclude_tax">
					<?php if($this->current_option == 'dashboard') { ?>
					Select here to exclude tax from analysis
					<?php } else { ?>
					Check here to exclude tax from numbers below
					<?php } ?>
				</label>
			</h5>
        </div>
        <div class="clearfix"> </div>
    </div>
	</form>

<div class=" tile_count" role="tabpanel" data-example-id="togglable-tabs">
  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
    <li role="presentation" class="active">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="spend-tab" role="tab" data-toggle="tab" aria-expanded="true">
            Spend Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content2" role="tab" id="vendor-tab" data-toggle="tab" aria-expanded="false">
            Supplier Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content3" role="tab" id="purchase-tab" data-toggle="tab" aria-expanded="false">
            Purchase Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content4" role="tab" id="department-tab" data-toggle="tab" aria-expanded="false">
            Department Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content5" role="tab" id="contract-tab" data-toggle="tab" aria-expanded="false">
            Contract Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content6" role="tab" id="budget-tab" data-toggle="tab" aria-expanded="false">
            Budget Analysis
        </a>
    </li>
  </ul>
  <div class="clearfix"> </div>
 
<style type="text/css">
  #chart {
  max-width: 650px;
  margin: 35px auto;
}
</style>
  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="spend-tab">
    	<?php $this->renderPartial('spend_analysis', $complete_view_data); ?>
    </div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="vendor-tab">
    	<?php 
      $complete_view_data['pie_series'] = $pie_series;
      $complete_view_data['pie_label'] = $pie_label;
      $this->renderPartial('vendor_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="purchase-tab">
    	<?php $this->renderPartial('purchase_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="department-tab">
    	<?php $this->renderPartial('department_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="contract-tab">
    	<?php $this->renderPartial('contract_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="budget-tab">
    	<?php $this->renderPartial('budget_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

</div>

</div>


</div>

<!-- Top 10 Supplier Spend -->
<div class="modal fade" id="supplier_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Top 10 Supplier Spend</h4>
          </div>
          <div class="clearfix"> </div>
          <div role="tabpanel" class="tab-pane active" id="tab_content106" aria-labelledby="order-spend-supplier-tab" style="padding: 15px;">
              <table id="top_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
                  <thead>
                  <tr>
                      <th>Supplier ID</th>
                      <th>Name</th>
                      <th>Industry</th>
                      <th>Sub Industry</th>
                      <th>Amount Spent</th>
                      <th>Phone</th>
                  </tr>
                  </thead>

                  <tbody>

                  <?php
                      foreach ($order_spend_vendor_stats as $vendor) { ?>
                          <tr>
                              <td>
                                  <?php echo !empty($vendor['vendor_id'])?$vendor['vendor_id']:"NULL"; ?>
                              </td>
                              <td><?php echo !empty($vendor['vendor_name'])?$vendor['vendor_name']:"NULL"; ?></td>
                              <td><?php echo !empty($vendor['industry'])?$vendor['industry']:"NULL"; ?></td>
                              <td><?php echo !empty($vendor['subindustry'])?$vendor['subindustry']:"NULL"; ?></td>
                              <td><?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format( $vendor['total']); ?></td>
                              <td>
                                  <?php
                                  echo $vendor['phone_1'];
                                  if (!empty($vendor['phone_2']))
                                      echo '<br />' . $vendor['phone_2'];
                                    else
                                      echo "NULL";
                                  ?>
                              </td>
                          </tr>

                      <?php } ?>

                  </tbody>

              </table>
          </div>
      </div>
  </div>
</div>
<!-- Supplier Count Spend -->
<div class="modal fade" id="supplier_count_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Total Suppliers Used</h4>
          </div>
          <div class="clearfix"> </div>
          <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background-color: #fff;">
                  <li role="presentation" class="active" style="margin-left: 0px;">
                      <a class="has-feedback-left pull-right" href="#tab_content108" id="order-supplier-count-tab" role="tab" data-toggle="tab" aria-expanded="true">
                          Goods and Services
                      </a>
                  </li>
                  <li role="presentation" class="">
                      <a href="#tab_content109" role="tab" id="expense-supplier-count-tab" data-toggle="tab" aria-expanded="false">
                          Travel and Expenses
                      </a>
                  </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="tab_content108" aria-labelledby="order-supplier-count-tab" style="padding: 15px;">
                      <table id="count_order_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Supplier ID</th>
                              <th>Name</th>
                              <th>Industry</th>
                              <th>Sub Industry</th>
                              <th>Amount Spent</th>
                              <th>Phone</th>
                          </tr>
                          </thead>

                          <tbody>

                          <?php
                              foreach ($order_count_spend_vendor_stats as $vendor) { ?>
                                  <tr>
                                      <td>
                                          <?php echo !empty($vendor['vendor_id'])?$vendor['vendor_id']:"NULL"; ?>
                                      </td>
                                      <td><?php echo !empty($vendor['vendor_name'])?$vendor['vendor_name']:"NULL";  ?></td>
                                      <td><?php echo !empty($vendor['industry'])?$vendor['industry']:"NULL";  ?></td>
                                      <td><?php echo !empty($vendor['subindustry'])?$vendor['subindustry']:"NULL";  ?></td>
                                      <td><?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format( $vendor['total']); ?></td>
                                      <td>
                                          <?php
                                          echo $vendor['phone_1'];
                                          if (!empty($vendor['phone_2']))
                                              echo '<br />' . $vendor['phone_2'];
                                          else 
                                            echo "NULL";
                                          ?>
                                      </td>
                                  </tr>

                              <?php } ?>

                          </tbody>

                      </table>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="tab_content109" aria-labelledby="expense-supplier-count-tab" style="padding: 15px;">
                      <table id="count_expense_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Supplier ID</th>
                              <th>Name</th>
                              <th>Industry</th>
                              <th>Sub Industry</th>
                              <th>Amount Spent</th>
                              <th>Phone</th>
                          </tr>
                          </thead>

                          <tbody>

                          <?php
                              foreach ($expense_count_spend_vendor_stats as $vendor) { ?>
                                  <tr>
                                      <td>
                                          <?php echo !empty($vendor['vendor_id'])?$vendor['vendor_id']:"NULL"; ?>
                                      </td>
                                      <td><?php echo !empty($vendor['vendor_name'])?$vendor['vendor_name']:"NULL"; ?></td>
                                      <td><?php echo !empty($vendor['industry'])?$vendor['industry']:"NULL"; ?></td>
                                      <td><?php echo !empty($vendor['subindustry'])?$vendor['subindustry']:"NULL"; ?></td>
                                      <td><?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format( $vendor['total']); ?></td>
                                      <td>
                                          <?php
                                          echo $vendor['phone_1'];
                                          if (!empty($vendor['phone_2']))
                                              echo '<br />' . $vendor['phone_2'];
                                          else
                                            echo "NULL";
                                          ?>
                                      </td>
                                  </tr>

                              <?php } ?>

                          </tbody>

                      </table>
                  </div>
              </div>

          </div>
      </div>
  </div>
</div>
<!-- Supplier Count Spend -->
<div class="modal fade" id="sup_analysis_supplier_count_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Total Suppliers Used</h4>
          </div>
          <div class="clearfix"> </div>
          <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background-color: #fff;">
                  <li role="presentation" class="active" style="margin-left: 0px;">
                      <a class="has-feedback-left pull-right" href="#sup_analysis_tab_content108" id="order-supplier-count-tab" role="tab" data-toggle="tab" aria-expanded="true">
                          Goods and Services
                      </a>
                  </li>
                  <li role="presentation" class="">
                      <a href="#sup_analysis_tab_content109" role="tab" id="expense-supplier-count-tab" data-toggle="tab" aria-expanded="false">
                          Travel and Expenses
                      </a>
                  </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="sup_analysis_tab_content108" aria-labelledby="order-supplier-count-tab" style="padding: 15px;">
                      <table id="sup_analysis_count_order_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Month/Year</th>
                            <th>Total Vendors</th> 
                          </tr>
                          </thead>

                          <tbody>

                          <?php $i=1;
                              foreach ($vendor_used as $vendor) {
                                if(strtolower($vendor['type'])=='order'){
                               ?>
                                  <tr><td><?php echo $i; ?></td>
                                      <td><?php echo !empty($vendor['month_year'])?$vendor['month_year']:"NULL";  ?></td>
                                      <td><?php echo !empty($vendor['total_vendors'])?$vendor['total_vendors']:"NULL";  ?></td>
                                  </tr>
                              <?php } } ?>
                          </tbody>
                      </table>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="sup_analysis_tab_content109" aria-labelledby="expense-supplier-count-tab" style="padding: 15px;">
                      <table id="sup_analysis_count_expense_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Month/Year</th>
                            <th>Total Vendors</th> 
                          </tr>
                          </thead>
                          <tbody>
                          <?php
                              foreach ($vendor_used as $vendor) { 
                                if(strtolower($vendor['type'])=='expense'){
                                ?>
                                 <tr><td><?php echo $i; ?></td>
                                      <td><?php echo !empty($vendor['month_year'])?$vendor['month_year']:"NULL";  ?></td>
                                      <td><?php echo !empty($vendor['total_vendors'])?$vendor['total_vendors']:"NULL";  ?></td>
                                  </tr>
                              <?php } } ?>
                          </tbody>
                      </table>
                  </div>
              </div>

          </div>
      </div>
  </div>
</div>
<!-- NON Preferred Spend -->
<div class="modal fade" id="non_preferred_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Non Preferred Spend</h4>
          </div>
          <div class="clearfix"> </div>
          <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background-color: #fff;">
                  <li role="presentation" class="active" style="margin-left: 0px;">
                      <a class="has-feedback-left pull-right" href="#tab_content110" id="order-non-preferred-spend-tab" role="tab" data-toggle="tab" aria-expanded="true">
                          Goods and Services
                      </a>
                  </li>
                  <li role="presentation" class="">
                      <a href="#tab_content111" role="tab" id="expense-non-preferred-spend-tab" data-toggle="tab" aria-expanded="false">
                          Travel and Expenses
                      </a>
                  </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="tab_content110" aria-labelledby="order-non-preferred-spend-tab" style="padding: 15px;">
                      <table id="order-non-preferred-spend_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Month/Year</th>
                            <th>Total Amount</th> 
                          </tr>
                          </thead>

                          <tbody>

                          <?php $i=1;$vendor='';
                              foreach ($vendor_nonprefered as $vendor) {
                                if(strtolower($vendor['type'])=='order'){
                               ?>
                                  <tr><td><?php echo $i; ?></td>
                                      <td><?php echo !empty($vendor['month_year'])?$vendor['month_year']:"NULL";  ?></td>
                                      <td><?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format(!empty($vendor['total_amount'])?$vendor['total_amount']*FunctionManager::currencyRate($tool_currency):"NULL");  ?></td>
                                  </tr>
                              <?php } } ?>
                     

                          </tbody>

                      </table>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="tab_content111" aria-labelledby="expense-non-preferred-spend-tab" style="padding: 15px;">
                      <table id="expense-non-preferred-spend_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Month/Year</th>
                            <th>Total Amount</th> 
                          </tr>
                          </thead>

                          <tbody>

                          <?php $i=1;$vendor='';
                              foreach ($vendor_nonprefered as $vendor) {
                                if(strtolower($vendor['type'])=='expense'){
                               ?>
                                  <tr><td><?php echo $i; ?></td>
                                      <td><?php echo !empty($vendor['month_year'])?$vendor['month_year']:"NULL";  ?></td>
                                      <td><?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format(!empty($vendor['total_amount'])?$vendor['total_amount']:"NULL");  ?></td>
                                  </tr>
                              <?php } } ?>
                     


                          </tbody>

                      </table>
                  </div>
              </div>

          </div>
      </div>
  </div>
</div>

<!-- NON Preferred Spend -->
<div class="modal fade" id="non_preferred_count_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Non Preferred Spend</h4>
          </div>
          <div class="clearfix"> </div>
          <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background-color: #fff;">
                  <li role="presentation" class="active" style="margin-left: 0px;">
                      <a class="has-feedback-left pull-right" href="#count_tab_content110" id="order-non-preferred-spend-tab" role="tab" data-toggle="tab" aria-expanded="true">
                          Goods and Services
                      </a>
                  </li>
                  <li role="presentation" class="">
                      <a href="#count_tab_content111" role="tab" id="expense-non-preferred-spend-tab" data-toggle="tab" aria-expanded="false">
                          Travel and Expenses
                      </a>
                  </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="count_tab_content110" aria-labelledby="order-non-preferred-spend-tab" style="padding: 15px;">
                      <table id="order-non-preferred-spend_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Month/Year</th>
                            <th>Vendors</th> 
                          </tr>
                          </thead>

                          <tbody>

                          <?php $i=1;$vendor='';
                              foreach ($vendor_nonprefered_count as $vendor) {
                                if(strtolower($vendor['type'])=='order'){
                               ?>
                                  <tr><td><?php echo $i; ?></td>
                                      <td><?php echo !empty($vendor['month_year'])?$vendor['month_year']:"NULL";  ?></td>
                                      <td><?php echo !empty($vendor['total_vendors'])?$vendor['total_vendors']:"NULL";  ?></td>
                                  </tr>
                              <?php } } ?>
                     

                          </tbody>

                      </table>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="count_tab_content111" aria-labelledby="expense-non-preferred-spend-tab" style="padding: 15px;">
                      <table id="expense-non-preferred-spend_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Month/Year</th>
                            <th>Total Amount</th> 
                          </tr>
                          </thead>

                          <tbody>

                          <?php $i=1;$vendor='';
                              foreach ($vendor_nonprefered_count as $vendor) {
                                if(strtolower($vendor['type'])=='expense'){
                               ?>
                                  <tr><td><?php echo $i; ?></td>
                                      <td><?php echo !empty($vendor['month_year'])?$vendor['month_year']:"NULL";  ?></td>
                                      <td><?php echo !empty($vendor['total_vendors'])?$vendor['total_vendors']:"NULL";  ?></td>
                                  </tr>
                              <?php } } ?>
                     


                          </tbody>

                      </table>
                  </div>
              </div>

          </div>
      </div>
  </div>
</div>
<!-- Spend To Amount Paid -->
<div class="modal fade" id="amount_paid_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Order Amount To Be Paid</h4>
          </div>
          <div class="clearfix"> </div>
          <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

              <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="tab_content1040" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
                      <table id="order_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Order ID</th>
                              <th>Supplier</th>
                              <th>Date</th>
                              <th>Total Price</th>
                              <th>Location</th>
                          </tr>
                          </thead>

                          <tbody>
                          <?php
                          if(count($order_spend_paid_stats)>0) {
                              foreach ($order_spend_paid_stats as $order) {
                                $totalPaiedAmount = $order['total_amount'];
                               ?>
                                  <tr>
                                      <td><?php echo !empty($order['order_id'])?$order['order_id']:"NULL"; ?></td>
                                      <td>
                                          <a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a>
                                      </td>
                                      <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                                      <td>
                                          <nobr>
                                            <?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalPaiedAmount*FunctionManager::currencyRate($tool_currency), 0); ?>
                                          </nobr>
                                      </td>

                                      <td>
                                          <?php echo !empty($order['location'])?$order['location']:"NULL"; ?>
                                      </td>
                                  </tr>

                              <?php }
                          } else {
                              ?>
                              <tr>
                                  <td style="width: 100%;" colspan="8">No Record Found</td>
                              </tr>
                          <?php } ?>
                          </tbody>

                      </table>
                  </div>

              </div>

          </div>
      </div>
  </div>
</div>
<!-- Spend To Order Received -->
<div class="modal fade" id="received_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
              <div class="modal-dialog">
                  <!-- Modal content -->
                  <div class="modal-content">
                      <div class="modal-header alert-info">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Orders To Be Received</h4>
                      </div>
                      <div class="clearfix"> </div>
                      <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

                          <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="tab_content1041" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
                                  <table id="order_table_tow" class="table table-striped table-bordered" style="width: 100%;">
                                     <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Month/Year</th>
                                      <th>Total Orders</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php
                                    if(count($order_to_receive)>0) {
                                      $i=1;
                                      foreach ($order_to_receive as $order) { ?>
                                        <tr>
                                          <td><?php echo $i++; ?></td>
                                          <td><?php echo $order['month_year']; ?></td>
                                          <td><?php echo $order['total_orders']; ?></td>
                                        </tr>
                                      <?php }
                                    } else {
                                      ?>
                                      <tr>
                                        <td style="width: 100%;" colspan="8">No Record Found</td>
                                      </tr>
                                    <?php } ?>

                                      </tbody>

                                  </table>
                              </div>

                          </div>

                      </div>
                  </div>
              </div>
          </div>


<!-- Ok after this -->


<!-- Spend To Order Received -->
<div class="modal fade" id="avg_per_category_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content">
		  <div class="modal-header alert-info">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Suppliers / Category</h4>
		  </div>
		  <div class="clearfix"> </div>
		  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

			  <div id="myTabContent" class="tab-content">
				  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
					  <table id="order_avg_suppliers_category" class="table table-striped table-bordered" style="width: 100%;">
						  <thead>
						  <tr>
							  <th>Category</th>
							  <th>Supplier Count</th>
						  </tr>
						  </thead>

						  <tbody>

						  <?php
							  foreach ($order_avg_suppliers_category as $category) { ?>
								  <tr>
									  <td><?php echo $category['category']; ?></td>
									  <td><?php echo $category['total_vendors']; ?></td>
								  </tr>

							  <?php } ?>

						  </tbody>

					  </table>
				  </div>

			  </div>

		  </div>
	  </div>
  </div>
</div>
<!-- Total Budget -->
<div class="modal fade" id="department_total_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content">
		  <div class="modal-header alert-info">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Total Budget & Spent</h4>
		  </div>
		  <div class="clearfix"> </div>
		  <div id="department-total-budget" style="padding: 5px;"></div>

	  </div>
  </div>
</div>
<!-- Budget Left -->
<div class="modal fade" id="department_left_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			  <div class="modal-header alert-info">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Percentage Budget Left</h4>
			  </div>
			  <div class="clearfix"> </div>
			  <div id="department-left-budget" style="padding: 5px;"></div>

		  </div>
	  </div>
  </div>

<!-- Budget Left -->
<div class="modal fade" id="department_over_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			  <div class="modal-header alert-info">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Departments Over Budget</h4>
			  </div>
			  <div class="clearfix"> </div>
			  <div id="department-over-budget" style="padding: 5px;"></div>

		  </div>
	  </div>
  </div>

<!--  Average Suppliers / Department -->
<div class="modal fade" id="department_avg_suppliers_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			  <div class="modal-header alert-info">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Average Suppliers / Department</h4>
			  </div>
			  <div class="clearfix"> </div>
			  <div id="department-avg_suppliers" style="padding: 5px;"></div>

		  </div>
	  </div>
</div>
<!-- Average Suppliers / Categories -->
<div class="modal fade" id="department_avg_categories_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			  <div class="modal-header alert-info">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Average Suppliers / Categories</h4>
			  </div>
			  <div class="clearfix"> </div>
			  <div id="department-avg_categories" style="padding: 5px;"></div>

		  </div>
	  </div>
</div>

<!-- Contract Active Values -->
<div class="modal fade" id="contract_active_value_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Active Contracts Value</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_active_value" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>#</th>
										  <th>Contract Name</th>
										  <th>Total Amount</th>
                      <th>Total Count</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php $i=1;
										  foreach ($metrics['contract']['active_all'] as $contract) { ?>
											  <tr>
                          <td><?php echo $i++;?></td>
												  <td><?php echo $contract['contract_title']?></td>
												  <td><?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($contract['contract_total'],2); ?></td>
                          <td><?php echo number_format($contract['count_total']); ?></td>
											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Contract Expire 30 days -->
<div class="modal fade" id="contract_expire_30days_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Expire in 30 Days</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_expire_30days" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>#</th>
										  <th>Duration</th>
										  <th>Total Amount</th>
                      <th>Total Count</th>
									  </tr>
									  </thead>

									  <tbody>

                    <?php $i=1;
                    if(count($metrics['contract']['expiries_all'])>0) {
                        foreach ($metrics['contract']['expiries_all'] as $contract) { ?>
                            <tr>
                                <td><?php echo $i++;?></td>
                                <td><?php echo $contract['contract_title']; ?></td>
                                <td><?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($contract['contract_total']); ?></td>
                                <td><?php echo number_format($contract['count_total']); ?></td>
                            </tr>
                        <?php }
                    } else {
                        ?>
                        <tr>
                            <td style="width: 100%;" colspan="8">No Record Found</td>
                        </tr>
                    <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Contract Expire 90 days -->
<div class="modal fade" id="contract_expire_91days_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Expire in 3 Months</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_expire_91days" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>#</th>
										  <th>Contract Title</th>
										  <th>Total Amount</th>
										  <th>Total Count</th>
									  </tr>
									  </thead>

									  <tbody>

                      <?php $i=1;
                      if(count($metrics['contract']['expiries_later_all'])>0) {
                          foreach ($metrics['contract']['expiries_later_all'] as $contract) { ?>
                              <tr>
                                  <td><?php echo $i++;?></td>
                                  <td><?php echo $contract['contract_title']; ?></td>
                                  <td><?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($contract['contract_total']); ?></td>
                                  <td><?php echo number_format($contract['count_total']); ?></td>
                              </tr>

                          <?php }
                      } else {
                          ?>
                          <tr>
                              <td style="width: 100%;" colspan="8">No Record Found</td>
                          </tr>
                      <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Total Budget -->
<div class="modal fade" id="total_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Total Budget</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="total_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($budget_total_stats as $budget) { ?>
											  <tr>
												   <td><?php echo $budget['location']; ?></td>
												  <td><?php echo $budget['department']; ?></td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget']);
														  ?>
													  </nobr>
												  </td>
											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Left Budget -->
<div class="modal fade" id="total_left_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Total Budget Left</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="left_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
										  <th>Left</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($budget_left_stats as $budget) { ?>
											  <tr>
												  <td><?php echo $budget['location']; ?></td>
												  <td><?php echo $budget['department']; ?></td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget']);
														  ?>
													  </nobr>
												  </td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['left']);
														  ?>
													  </nobr>
												  </td>
											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Avg Budget -->
<div class="modal fade" id="total_avg_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Average Budget Left</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="average_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
										  <th>Left</th>
										  <th>% Left</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($budget_avg_stats as $budget) { ?>
											  <tr>
												  <td><?php echo $budget['location']; ?></td>
												  <td><?php echo $budget['department']; ?></td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget']);
														  ?>
													  </nobr>
												  </td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['left']);
														  ?>
													  </nobr>
												  </td>

												  <td>
													  <nobr>
														  <?php
														  echo '%'.number_format($budget['percentage']);
														  ?>
													  </nobr>
												  </td>

											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Over Budget -->
<div class="modal fade" id="total_over_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Departments Over Budget</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="over_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
										  <th>used</th>

									  </tr>
									  </thead>

									  <tbody>
                                      <?php
                                      if(count($budget_over_stats)>0) {
                                          foreach ($budget_over_stats as $budget) { ?>
                                              <tr>
                                                  <td><?php echo $budget['location']; ?></td>
                                                  <td><?php echo $budget['department']; ?></td>
                                                  <td>
                                                      <nobr>
                                                          <?php
                                                          echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget']);
                                                          ?>
                                                      </nobr>
                                                  </td>
                                                  <td>
                                                      <nobr>
                                                          <?php
                                                          echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['left']);
                                                          ?>
                                                      </nobr>
                                                  </td>

                                              </tr>

                                          <?php }
                                      } else {
                                          ?>
                                          <tr>
                                              <td style="width: 100%;" colspan="3">No Record Found</td>
                                          </tr>
                                      <?php } ?>
									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Current Month Budget -->
<div class="modal fade" id="total_current_month_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Budget Pct This Month</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="left_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
										  <th>Left</th>
										  <th>% Used</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($budget_current_month_stats as $budget) { ?>
											  <tr>
												  <td><?php echo $budget['location']; ?></td>
												  <td><?php echo $budget['department']; ?></td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget']);
														  ?>
													  </nobr>
												  </td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['left']);
														  ?>
													  </nobr>
												  </td>

												  <td>
													  <nobr>
														  <?php
														  echo '%'.number_format(100-$budget['percentage']);
														  ?>
													  </nobr>
												  </td>

											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>

<style>
	  .dataTables_filter {width: 72%;}
</style>
  <script type="text/javascript">
	  $(document).ready( function() {

		  <?php if (isset($vendor['subindustry_id']) && !empty($vendor['subindustry_id'])) { ?>
		  loadSubindustries(<?php echo $vendor['subindustry_id']; ?>);
		  <?php } ?>

		  $('#order_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#expense_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#order_spent_to_approve_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 0}
			  ],
			  "order": []
		  });

		  $('#expense_spent_to_approve_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#top_supplier_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#supplier_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 0}
			  ],
			  "order": []
		  });
		  $('#sup_analysis_count_order_supplier_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 0}
			  ],
			  "order": []
		  });
      
		  $('#sup_analysis_count_expense_supplier_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 0}
			  ],
			  "order": []
		  });
       $('#count_order_supplier_table').dataTable({
        "columnDefs": [
          {"targets": 0, "width": "6%", "orderable": false},
          {"type": "sort-month-year", targets: 0}
        ],
        "order": []
      });
      $('#count_expense_supplier_table').dataTable({
        "columnDefs": [
          {"targets": 0, "width": "6%", "orderable": false},
          {"type": "sort-month-year", targets: 0}
        ],
        "order": []
      });
		  $('#order-non-preferred-spend_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 2}
			  ],
			  "order": []
		  });

//		  $('#order_avg_suppliers_category').dataTable({
//			  "columnDefs": [
//				  {"targets": 0, "width": "6%", "orderable": false},
//				  {"type": "sort-month-year", targets: 3}
//			  ],
//			  "order": []
//		  });


		  $('#expense-non-preferred-spend_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 2}
			  ],
			  "order": []
		  });

		  $('#contract_active_value').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 0}
			  ],
			  "order": []
		  });

//		  $('#contract_expire_30days').dataTable({
//			  "columnDefs": [
//				  {"targets": 0, "width": "6%", "orderable": false},
//				  {"type": "sort-month-year", targets: 3}
//			  ],
//			  "order": []
//		  });
//
//		  $('#contract_expire_91days').dataTable({
//			  "columnDefs": [
//				  {"targets": 0, "width": "6%", "orderable": false},
//				  {"type": "sort-month-year", targets: 3}
//			  ],
//			  "order": []
//		  });

	  });
  </script>

<script type="text/javascript">

var contract_by_status_chart_created = false;
var orders_by_status_chart_created = false;

$(document).ready(function() {

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  	if ($(e.target).attr("href") == '#tab_content5')
  	{
  		if (contract_by_status_chart_created == false)
  		{
  			var contracts_polar = new Chart($('#contracts_by_status'), contracts_polar_config);
  			contract_by_status_chart_created = true;
  		}
  	}

  	if ($(e.target).attr("href") == '#tab_content3')
  	{
  		if (orders_by_status_chart_created == false)
  		{
		    var polar = new Chart($('#orders_by_status'), polar_config);
  			orders_by_status_chart_created = true;
  		}
  	}

});

	var chart_labels = [];
	var chart_full_labels = [];
	var chart_data = [];
	var chart_colors = [];

	<?php
		for ($j=1; $j<=7; $j++)
		{
			if ($j == 3 || $j == 1 || $j == 2 || $j == 4 || $j == 6 || $j == 7) continue;

			echo 'chart_labels[' . $j . '] = [];';
			echo 'chart_full_labels[' . $j . '] = [];';
			echo 'chart_data[' . $j . '] = [];';
			echo 'chart_colors[' . $j . '] = [];';

			$canvas_id = '';
			$dataset = array();

			if ($j == 1)
			{
				$canvas_id = 'year_to_date';
				$dataset = $current_year;
			}
			else if ($j == 2)
			{
				$canvas_id = 'spend_vendors';
				$dataset = $top_vendors;
			}
			else if ($j == 3)
			{
				$canvas_id = 'spend_departments';
				$dataset = $top_departments;
			}
			else if ($j == 4)
			{
				$canvas_id = 'spend_locations';
				$dataset = $top_locations;
			}
			else if ($j == 5)
			{
				$canvas_id = 'top_vendors';
				$dataset = $top_vendors;
			}
			else if ($j == 6)
			{
				$canvas_id = 'year_over_year';
				$dataset = $year_over_year;
			}
			else if ($j == 7)
			{
				$canvas_id = 'spend_contracts';
				$dataset = $top_contracts;
			}

			$i = 0;
			foreach ($dataset as $idx => $a_data_row)
			{
				if ($idx === 'total') continue;
	?>

				<?php if ($j == 7) { ?>
					chart_labels[<?php echo $j; ?>][<?php echo $i; ?>] = "<?php if (strlen($a_data_row['dim_1']) >= 18) { $name = substr($a_data_row['dim_1'], 0, 18); $name = rtrim(ltrim($name)); $pos = strrpos($name, ' '); if ($pos !== false && $pos > 12) $name = substr($name, 0, $pos); echo $name; } else echo $a_data_row['dim_1']; ?>";
					chart_full_labels[<?php echo $j; ?>][<?php echo $i; ?>] = "<?php echo $a_data_row['dim_1']; ?>";
				<?php } else { ?>
					chart_labels[<?php echo $j; ?>][<?php echo $i; ?>] = "<?php echo $a_data_row['dim_1']; ?>";
				<?php } ?>
				chart_data[<?php echo $j; ?>][<?php echo $i; ?>] = <?php echo round($a_data_row['total'], 2); ?>;

	<?php
				$i += 1;

				$color = '';
				switch ($i)
				{
					case 1  : $color = '#3498DB'; break;
					case 2  : $color = '#9B59B6'; break;
					case 3  : $color = '#1ABB9C'; break;
					case 4  : $color = '#00CED1'; break;
					case 5  : $color = '#E74C3C'; break;
					case 6  : $color = '#FAEBD7'; break;
					case 7  : $color = '#7FFFD4'; break;
					case 8  : $color = '#FFE4C4'; break;
					case 9  : $color = '#BDB76B'; break;
					default : $color = '#FFA07A'; break;
				}

				echo 'chart_colors[' . $j . '][' . ($i - 1) . '] = "' . $color . '";';

			}
	?>

			createChart('<?php echo $canvas_id; ?>', chart_data[<?php echo $j; ?>], chart_labels[<?php echo $j; ?>], chart_colors[<?php echo $j; ?>], chart_full_labels[<?php echo $j; ?>]);
	<?php

		}
	?>

});

function createChart(div_id, data_values, data_labels, data_colors, chart_full_labels)
{
	//console.log(data_values);
	var chart_type = 'doughnut';
	if (div_id == 'spend_departments') chart_type = 'pie';
	//if (div_id == 'year_to_date') chart_type = 'line';
	if (div_id == 'spend_locations') chart_type = 'bar';
	if (div_id == 'year_over_year') chart_type = 'bar';
	if (div_id == 'spend_contracts') chart_type = 'horizontalBar';

	var chart_settings = false;

	if (chart_type == 'line')
	{
		var yearly_data_1 = new Array();
		var yearly_data_2 = new Array();
		var yearly_data_3 = new Array();

		for (var i=0; i<=11; i++) yearly_data_1[i] = data_values[i];
		for (var i=12; i<=23; i++) yearly_data_2[i-12] = data_values[i];
		for (var i=24; i<=35; i++) yearly_data_3[i-24] = data_values[i];

		chart_settings = {
			type: chart_type,
			tooltipFillColor: "rgba(51, 51, 51, 0.55)",
			data: {
				labels: [ "", "", "", "", "", "", "", "", "", "", "", "" ],
				datasets: [
							{ label: "<?php echo date("Y") - 2; ?>", data: yearly_data_1, fill: false, lineTension: 0, borderColor: 'orange', backgroundColor: 'orange' },
							{ label: "<?php echo date("Y") - 1; ?>", data: yearly_data_2, fill: false, lineTension: 0, borderColor: 'aqua', backgroundColor: 'aqua' },
							{ label: "<?php echo date("Y"); ?>", data: yearly_data_3, fill: false, lineTension: 0, borderColor: 'purple', backgroundColor: 'purple' }
				]
			},
			options: {
				legend: false,
				responsive: true,
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							var suffix = data.datasets[tooltipItem.datasetIndex].label;

							if (tooltipItem.index == 0) suffix = 'Jan ' + suffix;
							if (tooltipItem.index == 1) suffix = 'Feb ' + suffix;
							if (tooltipItem.index == 2) suffix = 'Mar ' + suffix;
							if (tooltipItem.index == 3) suffix = 'Apr ' + suffix;
							if (tooltipItem.index == 4) suffix = 'May ' + suffix;
							if (tooltipItem.index == 5) suffix = 'Jun ' + suffix;
							if (tooltipItem.index == 6) suffix = 'Jul ' + suffix;
							if (tooltipItem.index == 7) suffix = 'Aug ' + suffix;
							if (tooltipItem.index == 8) suffix = 'Sep ' + suffix;
							if (tooltipItem.index == 9) suffix = 'Oct ' + suffix;
							if (tooltipItem.index == 10) suffix = 'Nov ' + suffix;
							if (tooltipItem.index == 11) suffix = 'Dec ' + suffix;

						    if (parseInt(value) >= 1000){
						    	return [ suffix, '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")];
						    } else {
						    	return [ suffix, '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value];
						    }
						}
					}
				},
	            scales: { yAxes: [{ ticks: {
	            								min: 0,
												callback: function(value, index, values) {
              										if (parseInt(value) >= 1000) {
                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              										} else {
                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
              										}
	            						   		}
	            						   }
	            				 }]
	            	}
			}
		};
	}
	else
	{
		if (div_id == 'spend_contracts')
			chart_settings = {
				type: chart_type,
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: data_labels,
					full_labels: chart_full_labels,
					datasets: [{
						data: data_values,
						backgroundColor: data_colors
					}]
				},
				options: {
					legend: false,
					responsive: true,
					tooltips: {
						callbacks: {
							label: function(tooltipItem, data) {
								var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								var prefix = data.full_labels[tooltipItem.index];
								var output = "";

							    if (parseInt(value) >= 1000){
							    	output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							    } else {
							    	output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
							    }

							    if (prefix.length >= 15) return ['(' + prefix + ')', output];
							    else return output;
							}
						}
					},
		            scales: { xAxes: [{ ticks: {
		            								min: 0,
													callback: function(value, index, values) {
	              										if (parseInt(value) >= 1000) {
	                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              										} else {
	                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              										}
		            						   		}
		            						   }
		            				 }]
		            	}
				}
			};
		else
		{
			if (chart_type == 'doughnut')
				chart_settings = {
					type: chart_type,
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: data_labels,
						datasets: [{
							data: data_values,
							backgroundColor: data_colors
						}]
					},
					options: {
						legend: false,
						responsive: true,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									var prefix = data.labels[tooltipItem.index];

								    if (parseInt(value) >= 1000){
								    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								    } else {
								    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
								    }
								}
							}
						},
			            scales: { yAxes: [{ ticks: {
			            								min: 0,
														callback: function(value, index, values) {
		              										if (parseInt(value) >= 1000) {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		              										} else {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
		              										}
			            						   		}
			            						   }
			            				 }]
			            	}
					}
				};
			else
				chart_settings = {
					type: chart_type,
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: data_labels,
						datasets: [{
							data: data_values,
							backgroundColor: data_colors
						}]
					},
					options: {
						legend: false,
						responsive: true,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								    if (parseInt(value) >= 1000){
								    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								    } else {
								    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
								    }
								}
							}
						},
			            scales: { yAxes: [{ ticks: {
			            								min: 0,
														callback: function(value, index, values) {
		              										if (parseInt(value) >= 1000) {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		              										} else {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
		              										}
			            						   		}
			            						   }
			            				 }]
			            	}
					}
				};
		}
	}

	if (chart_type == 'doughnut') delete chart_settings['options']['scales'];
	var chart = new Chart( $('#' + div_id), chart_settings);
}

function expandChart(display_index)
{
	var show_index = display_index;
	var hide_index = 0;
	if (display_index % 2 != 0) hide_index = show_index - 1;
	else hide_index = show_index + 1;


	if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
	else
	{
		$('#chart_area_' + hide_index).hide(1000);
		$('#table_area_' + hide_index).hide(1000);
		
		$('#chart_area_' + show_index).show(1000);
		$('#table_area_' + show_index).show(1000);
	}
}

function expandChart1(display_index)
{
	var show_index = display_index;

	if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
	else
	{
		$('#chart_area_' + show_index).hide(1000);
		$('#table_area_' + show_index).show(1000);
	}
}

function collapseChart(display_index)
{
	var show_index = display_index;
	var hide_index = 0;
	if (display_index % 2 != 0) hide_index = show_index - 1;
	else hide_index = show_index + 1;

	$('#table_area_' + show_index).hide(1000);
	$('#table_area_' + hide_index).hide(1000);
	
	if (!$('#chart_area_' + show_index).is(':visible')) 
		$('#chart_area_' + show_index).show(2000);
	if (!$('#chart_area_' + hide_index).is(':visible')) 
		$('#chart_area_' + hide_index).show(2000);
}


function exportChart(canvas_id)
{
	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id;
}

</script>


<!-- Total Year Spend and Total Order Modal-->
<div class="modal fade" id="total_year_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content -->
    <div class="modal-content">
      <div class="modal-header alert-info">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Total Year Spend</h4>
      </div>
      <div class="clearfix"> </div>
      <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background-color: #fff;">
          <li role="presentation" class="active" style="margin-left: 0px;">
            <a class="has-feedback-left pull-right" href="#tab_content100" id="order-spend-tab" role="tab" data-toggle="tab" aria-expanded="true">
              Goods and Services
            </a>
          </li>
          <li role="presentation" class="">
            <a href="#tab_content101" role="tab" id="expesne-spend-tab" data-toggle="tab" aria-expanded="false">
              Travel and Expenses
            </a>
          </li>
        </ul>
        <div id="myTabContent" class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="tab_content100" aria-labelledby="order-spend-tab" style="padding: 15px;">
            <table id="order_table_three" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
               <thead>
             <tr>
              <th>#</th>
              <th>Month/Year</th>
              <th>Total Amount</th>
            </tr>
              </thead>
              <tbody>
              <?php $i=1;
                if(!empty($order_spend_stats)){
                  foreach ($order_spend_stats as $order) { 
                     if(strtolower($order['type'])=='order'){
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $order['month_year']; ?></td>
                      <td><?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($order['total_amount']); ?></td>
                    </tr>

                  <?php } }               
              } else {
                ?>
                <tr>
                  <td style="width: 100%;" colspan="8">No Record Found</td>
                </tr>
              <?php } ?>

                </tbody>

            </table>
          </div>

          <div role="tabpanel" class="tab-pane" id="tab_content101" aria-labelledby="expense-spend-tab" style="padding: 15px;">
            <table id="expense_table_three" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
             <tr>
              <th>#</th>
              <th>Month/Year</th>
              <th>Total Amount</th>
            </tr>
              </thead>
              <tbody>
              <?php $i=1;
                if(!empty($order_spend_stats)){
                  foreach ($order_spend_stats as $order) { 
                     if(strtolower($order['type'])=='expense'){
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $order['month_year']; ?></td>
                      <td><?php echo  html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($order['total_amount']); ?></td>
                    </tr>

                  <?php } }               
              } else {
                ?>
                <tr>
                  <td style="width: 100%;" colspan="8">No Record Found</td>
                </tr>
              <?php } ?>

                </tbody>

            </table>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<!-- START: Spend Total Orders for the duration ( Graph Spark2 )-->
<div class="modal fade" id="total_year_ordercount_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content -->
    <div class="modal-content">
      <div class="modal-header alert-info">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Total Yearly Orders</h4>
      </div>
      <div class="clearfix"> </div>
      <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
       
        <div id="myTabContent" class="tab-content">
          
            <table id="order_table_four" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                <th>#</th>
                <th>Month/Year</th>
                <th>Total Orders</th>
              </tr>
              </thead>

              <tbody>

              <?php
              if(count($order_count)>0) {
                $i=1;
                foreach ($order_count as $order) { ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $order['month_year']; ?></td>
                    <td><?php echo $order['total_orders']; ?></td>
                  </tr>
                <?php }
              } else {
                ?>
                <tr>
                  <td style="width: 100%;" colspan="8">No Record Found</td>
                </tr>
              <?php } ?>

              </tbody>

            </table>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- END: Spend Total Orders for the duration -->


<!-- START: Orders to approve for the duration ( Graph Spark4 )-->
<div class="modal fade" id="approved_ordertoapprove_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content -->
    <div class="modal-content">
      <div class="modal-header alert-info">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Total Orders To Approve</h4>
      </div>
      <div class="clearfix"> </div>
      <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
       
        <div id="myTabContent" class="tab-content">
          
            <table id="order_table_five" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                <th>#</th>
                <th>Month/Year</th>
                <th>Total Orders</th>
              </tr>
              </thead>

              <tbody>

              <?php
              if(count($order_pending_count)>0) {
                $i=1;
                foreach ($order_pending_count as $order) { ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $order['month_year']; ?></td>
                    <td><?php echo $order['total_orders']; ?></td>
                  </tr>
                <?php }
              } else {
                ?>
                <tr>
                  <td style="width: 100%;" colspan="8">No Record Found</td>
                </tr>
              <?php } ?>

              </tbody>

            </table>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- END: Spend Orders to approve for the duration -->





<!-- Spend To Approve -->
<div class="modal fade" id="approved_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
              <div class="modal-dialog">
                  <!-- Modal content -->
                  <div class="modal-content">
                      <div class="modal-header alert-info">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Spend To Approve</h4>
                      </div>
                      <div class="clearfix"> </div>
                      <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
                          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background-color: #fff;">
                              <li role="presentation" class="active" style="margin-left: 0px;">
                                  <a class="has-feedback-left pull-right" href="#tab_content102" id="order-spend-approve-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                      Goods and Services
                                  </a>
                              </li>
                              <li role="presentation" class="">
                                  <a href="#tab_content103" role="tab" id="expesne-spend-approve-tab" data-toggle="tab" aria-expanded="false">
                                      Travel and Expenses
                                  </a>
                              </li>
                          </ul>
                          <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="tab_content102" aria-labelledby="order-spend-approve-tab" style="padding: 15px;">
                                  <table id="order_spent_to_approve_table" class="table table-striped table-bordered" style="width: 100%;">
                                      <thead>
                                     <tr>
                                      <th>#</th>
                                      <th>Month/Year</th>
                                      <th>Total Amount</th>
                                    </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                          foreach ($order_spend_approve_stats as $order) { ?>
                                            <tr>
                                              <td><?php echo $i++; ?></td>
                                              <td><?php echo $order['month_year']; ?></td>
                                              <td><?php echo  html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($order['total_amount']); ?></td>
                                            </tr>

                                          <?php }  ?>

                                      </tbody>

                                  </table>
                              </div>

                              <div role="tabpanel" class="tab-pane" id="tab_content103" aria-labelledby="expense-spend-approve-tab" style="padding: 15px;">
                                  <table id="expense_spent_to_approve_table" class="table table-striped table-bordered" style="width: 100%;">
                                      <thead>
                                      <tr>
                                          <th>ID</th>
                                          <th>Name</th>
                                          <th>Total Price</th>
                                          <th>User</th>
                                      </tr>
                                      </thead>

                                      <tbody>

                                      <?php
                                          foreach ($expense_spend_approve_stats as $expense) { ?>
                                              <tr>
                                                  <td><?php echo $expense['expense_id']; ?></td>
                                                  <td><?php echo $expense['expense_name']; ?></td>

                                                  <td>
                                                      <nobr>
                                                          <?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($expense['total_expenses']); ?>
                                                      </nobr>
                                                  </td>
                                                  <td><a href="<?php echo AppUrl::bicesUrl('users/edit/' . $expense['user_id']); ?>"><?php echo $expense['user']; ?></a></td>
                                              </tr>

                                          <?php } ?>
                                      </tbody>

                                  </table>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
          </div>
<!-- Spend To Paid -->

<div class="modal fade" id="paid_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
              <div class="modal-dialog">
                  <!-- Modal content -->
                  <div class="modal-content">
                      <div class="modal-header alert-info">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Orders To Be Paid</h4>
                      </div>
                      <div class="clearfix"> </div>
                      <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

                          <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="tab_content104" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
                                  <table id="order_table_six" class="table table-striped table-bordered" style="width: 100%;">
                                      <thead>
                                      <tr>
                                          <th>Order ID</th>
                                          <th>Supplier</th>
                                          <th>Date</th>
                                          <th>Total Price</th>
                                          <th>Location</th>
                                      </tr>
                                      </thead>

                                      <tbody>

                                      <?php
                                      if(count($order_spend_paid_stats)>0) {
                                          foreach ($order_spend_paid_stats as $order) { 
                                            $totalPaiedAmount = $order['total_amount'];
                                            ?>
                                              <tr>
                                                  <td><?php echo !empty($order['order_id'])?$order['order_id']:"NULL"; ?></td>
                                                  <td>
                                                      <a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a>
                                                  </td>
                                                  <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                                                  <td>
                                                      <nobr>
                                                          <?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalPaiedAmount); ?>
                                                      </nobr>
                                                  </td>

                                                  <td>
                                                      <?php echo !empty($order['location'])?$order['location']:"NULL"; ?>
                                                  </td>
                                              </tr>

                                          <?php }
                                      } else {
                                          ?>
                                          <tr>
                                              <td style="width: 100%;" colspan="8">No Record Found</td>
                                          </tr>
                                      <?php } ?>

                                      </tbody>

                                  </table>
                              </div>

                          </div>

                      </div>
                  </div>
              </div>
          </div>




<style type="text/css">
.apexcharts-tooltip-title {
    font-family: popin;
    font-size: 12px;
    background: #12A79C !important;
    color: #fff;
    font-family:Poppins !important;
  }
  div.dataTables_filter input {
    margin-left: 0.5em !important;
    padding: 0 !important;
}

.tile_count {
    margin-bottom: 10px;
    margin-top: 10px;
}


</style>

<script type="text/javascript"> 

  function createApexChart(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    if(type=='line'){
        $.each(series, function(key, value) { 
          seriesL.push({'data':value['data'],'name':value['name']});
        });
    }else{
      $.each(series, function(key, value) {  
          seriesData.push(value);
        });
      seriesL.push({'name':toolTip,'data':seriesData});
    }

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value);
    });  
    var chartIDL = chartID;
    if(chartIDL=="#spend_contracts"){horizontalL=true;}
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }
      },
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        }
      },
      legend: {position: 'bottom',offsetX: 0,offsetY: 0},
      colors: colors,
      plotOptions: {bar: {columnWidth: '35%',distributed: true, horizontal: horizontalL}},
      dataLabels: {enabled: lableL},
      stroke: {curve: 'straight'},
      series: seriesL,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      xaxis: {categories: categoriesL,labels: {
                formatter: function (value) {
                  valueL = chartIDL!="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },
                style: {fontSize: '12px'}}},
      yaxis: {labels: {
                formatter: function (value) {
                  valueL = chartIDL=="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },
                style: {fontSize: '12px'}
              },},
    }

    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }

  var colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];
  createApexChart(215,'100%','line',false,<?php echo json_encode($monthlySeriesNewArr) ?>,<?php echo json_encode($months);?>,"#spend_line_chart");

  createApexChart(225,'100%','bar',false,<?php echo json_encode($location_series) ?>,<?php echo json_encode($location_label);?>,"#spend_locations",colors); 

  createApexChart(265,'100%','bar',false,<?php echo json_encode($yearly_series) ?>,<?php echo json_encode($yearly_label);?>,"#year_over_year",colors); 

  createApexChart(265,'100%','bar',false,<?php echo json_encode($top_contract_series) ?>,<?php echo json_encode($top_contract_label);?>,"#spend_contracts",colors); 

  
  
     var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 260,
              width: 610,
              type: 'pie',
            },
           // legend: {show: false},
            dataLabels: {enabled: false},
            labels: <?php echo $pie_label;?>,
            series: <?php echo $pie_series;?>,
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 300
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            
        }
        var chart = new ApexCharts(
            document.querySelector("#spend_pie_vendors"),
            options
        );
        chart.render();
        var chart = new ApexCharts(
            document.querySelector("#vendor_pie_vendors"),
            options
        );
        chart.render();
  </script>


