<?php $tool_currency = Yii::app()->session['user_currency'];?>
<!-- page content -->
          <div class="right_col" role="main">

	<form action="<?php echo AppUrl::bicesUrl('dashboard'); ?>" method="post" name="dashboard_form" id="dashboard_form">
    <div class="clearfix tile_count">
        <div class="col-md-7 col-sm-7 col-xs-12 pull-left">
            <h3>Dashboard



            </h3>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 text-responsive" style="text-align: right;">
			<h5 class="dashboard-check">
				<input type="checkbox" name="exclude_tax" id="exclude_tax" onclick="$('#dashboard_form').submit();" 
					<?php if (isset(Yii::app()->session['exclude_tax_from_dashboard']) && Yii::app()->session['exclude_tax_from_dashboard']) echo ' checked="CHECKED" '; ?> />
				<label for="exclude_tax">
					<?php if($this->current_option == 'dashboard') { ?>
					Select here to exclude tax from analysis
					<?php } else { ?>
					Check here to exclude tax from numbers below
					<?php } ?>
				</label>
			</h5>
        </div>
        <div class="clearfix"> </div>
    </div>
	</form>

<div class=" tile_count" role="tabpanel" data-example-id="togglable-tabs">
  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
    <li role="presentation" class="active">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="spend-tab" role="tab" data-toggle="tab" aria-expanded="true">
            Spend Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content2" role="tab" id="vendor-tab" data-toggle="tab" aria-expanded="false">
            Supplier Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content3" role="tab" id="purchase-tab" data-toggle="tab" aria-expanded="false">
            Purchase Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content4" role="tab" id="department-tab" data-toggle="tab" aria-expanded="false">
            Department Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content5" role="tab" id="contract-tab" data-toggle="tab" aria-expanded="false">
            Contract Analysis
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content6" role="tab" id="budget-tab" data-toggle="tab" aria-expanded="false">
            Budget Analysis
        </a>
    </li>
  </ul>
  <div class="clearfix"> </div>
 
<style type="text/css">
  #chart {
  max-width: 650px;
  margin: 35px auto;
}
</style>
  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="spend-tab">
    	<?php $this->renderPartial('spend_analysis', $complete_view_data); ?>
    </div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="vendor-tab">
    	<?php $this->renderPartial('vendor_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="purchase-tab">
    	<?php $this->renderPartial('purchase_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="department-tab">
    	<?php $this->renderPartial('department_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="contract-tab">
    	<?php $this->renderPartial('contract_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="budget-tab">
    	<?php $this->renderPartial('budget_analysis', $complete_view_data); ?>
	</div>
    <div class="clearfix"> </div>

</div>

</div>


</div>

<!-- Top 10 Supplier Spend -->
<div class="modal fade" id="supplier_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Top 10 Supplier Spend</h4>
          </div>
          <div class="clearfix"> </div>
          <div role="tabpanel" class="tab-pane active" id="tab_content106" aria-labelledby="order-spend-supplier-tab" style="padding: 15px;">
              <table id="top_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
                  <thead>
                  <tr>
                      <th>Supplier ID</th>
                      <th>Name</th>
                      <th>Industry</th>
                      <th>Sub Industry</th>
                      <th>Amount Spent</th>
                      <th>Phone</th>
                  </tr>
                  </thead>

                  <tbody>

                  <?php
                      foreach ($order_spend_vendor_stats as $vendor) { ?>
                          <tr>
                              <td>
                                  <?php echo $vendor['vendor_id']; ?>
                              </td>
                              <td><?php echo $vendor['vendor_name']; ?></td>
                              <td><?php echo $vendor['industry']; ?></td>
                              <td><?php echo $vendor['subindustry']; ?></td>
                              <td><?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format( $vendor['total'], 2); ?></td>
                              <td>
                                  <?php
                                  echo $vendor['phone_1'];
                                  if (!empty($vendor['phone_2']))
                                      echo '<br />' . $vendor['phone_2'];
                                  ?>
                              </td>
                          </tr>

                      <?php } ?>

                  </tbody>

              </table>
          </div>
      </div>
  </div>
</div>
<!-- Supplier Count Spend -->
<div class="modal fade" id="supplier_count_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Total Suppliers Used</h4>
          </div>
          <div class="clearfix"> </div>
          <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background-color: #fff;">
                  <li role="presentation" class="active" style="margin-left: 0px;">
                      <a class="has-feedback-left pull-right" href="#tab_content108" id="order-supplier-count-tab" role="tab" data-toggle="tab" aria-expanded="true">
                          Goods and Services
                      </a>
                  </li>
                  <li role="presentation" class="">
                      <a href="#tab_content109" role="tab" id="expense-supplier-count-tab" data-toggle="tab" aria-expanded="false">
                          Travel and Expenses
                      </a>
                  </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="tab_content108" aria-labelledby="order-supplier-count-tab" style="padding: 15px;">
                      <table id="count_order_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Supplier ID</th>
                              <th>Name</th>
                              <th>Industry</th>
                              <th>Sub Industry</th>
                              <th>Amount Spent</th>
                              <th>Phone</th>
                          </tr>
                          </thead>

                          <tbody>

                          <?php
                              foreach ($order_count_spend_vendor_stats as $vendor) { ?>
                                  <tr>
                                      <td>
                                          <?php echo $vendor['vendor_id']; ?>
                                      </td>
                                      <td><?php echo $vendor['vendor_name']; ?></td>
                                      <td><?php echo $vendor['industry']; ?></td>
                                      <td><?php echo $vendor['subindustry']; ?></td>
                                      <td><?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format( $vendor['total'], 2); ?></td>
                                      <td>
                                          <?php
                                          echo $vendor['phone_1'];
                                          if (!empty($vendor['phone_2']))
                                              echo '<br />' . $vendor['phone_2'];
                                          ?>
                                      </td>
                                  </tr>

                              <?php } ?>

                          </tbody>

                      </table>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="tab_content109" aria-labelledby="expense-supplier-count-tab" style="padding: 15px;">
                      <table id="count_expense_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Supplier ID</th>
                              <th>Name</th>
                              <th>Industry</th>
                              <th>Sub Industry</th>
                              <th>Amount Spent</th>
                              <th>Phone</th>
                          </tr>
                          </thead>

                          <tbody>

                          <?php
                              foreach ($expense_count_spend_vendor_stats as $vendor) { ?>
                                  <tr>
                                      <td>
                                          <?php echo $vendor['vendor_id']; ?>
                                      </td>
                                      <td><?php echo $vendor['vendor_name']; ?></td>
                                      <td><?php echo $vendor['industry']; ?></td>
                                      <td><?php echo $vendor['subindustry']; ?></td>
                                      <td><?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format( $vendor['total'], 2); ?></td>
                                      <td>
                                          <?php
                                          echo $vendor['phone_1'];
                                          if (!empty($vendor['phone_2']))
                                              echo '<br />' . $vendor['phone_2'];
                                          ?>
                                      </td>
                                  </tr>

                              <?php } ?>

                          </tbody>

                      </table>
                  </div>
              </div>

          </div>
      </div>
  </div>
</div>
<!-- NON Preferred Spend -->
<div class="modal fade" id="non_preferred_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Non Preferred Spend</h4>
          </div>
          <div class="clearfix"> </div>
          <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background-color: #fff;">
                  <li role="presentation" class="active" style="margin-left: 0px;">
                      <a class="has-feedback-left pull-right" href="#tab_content110" id="order-non-preferred-spend-tab" role="tab" data-toggle="tab" aria-expanded="true">
                          Goods and Services
                      </a>
                  </li>
                  <li role="presentation" class="">
                      <a href="#tab_content111" role="tab" id="expense-non-preferred-spend-tab" data-toggle="tab" aria-expanded="false">
                          Travel and Expenses
                      </a>
                  </li>
              </ul>
              <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="tab_content110" aria-labelledby="order-non-preferred-spend-tab" style="padding: 15px;">
                      <table id="order-non-preferred-spend_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Supplier ID</th>
                              <th>Name</th>
                              <th>Industry</th>
                              <th>Sub Industry</th>
                              <th>Amount Spent</th>
                              <th>Phone</th>
                          </tr>
                          </thead>

                          <tbody>

                          <?php
                              foreach ($order_spend_non_preferred_vendor_stats as $vendor) { ?>
                                  <tr>
                                      <td>
                                          <?php echo $vendor['vendor_id']; ?>
                                      </td>
                                      <td><?php echo $vendor['vendor_name']; ?></td>
                                      <td><?php echo $vendor['industry']; ?></td>
                                      <td><?php echo $vendor['subindustry']; ?></td>
                                      <td><?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format( $vendor['total'], 2); ?></td>
                                      <td>
                                          <?php
                                          echo $vendor['phone_1'];
                                          if (!empty($vendor['phone_2']))
                                              echo '<br />' . $vendor['phone_2'];
                                          ?>
                                      </td>
                                  </tr>

                              <?php } ?>

                          </tbody>

                      </table>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="tab_content111" aria-labelledby="expense-non-preferred-spend-tab" style="padding: 15px;">
                      <table id="expense-non-preferred-spend_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Supplier ID</th>
                              <th>Name</th>
                              <th>Industry</th>
                              <th>Sub Industry</th>
                              <th>Amount Spent</th>
                              <th>Phone</th>
                          </tr>
                          </thead>

                          <tbody>

                          <?php
                              foreach ($expense_spend_non_preferred_vendor_stats as $vendor) { ?>
                                  <tr>
                                      <td>
                                          <?php echo $vendor['vendor_id']; ?>
                                      </td>
                                      <td><?php echo $vendor['vendor_name']; ?></td>
                                      <td><?php echo $vendor['industry']; ?></td>
                                      <td><?php echo $vendor['subindustry']; ?></td>
                                      <td><?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format( $vendor['total'], 2); ?></td>
                                      <td>
                                          <?php
                                          echo $vendor['phone_1'];
                                          if (!empty($vendor['phone_2']))
                                              echo '<br />' . $vendor['phone_2'];
                                          ?>
                                      </td>
                                  </tr>

                              <?php } ?>

                          </tbody>

                      </table>
                  </div>
              </div>

          </div>
      </div>
  </div>
</div>
<!-- Spend To Amount Paid -->
<div class="modal fade" id="amount_paid_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content -->
      <div class="modal-content">
          <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Order Amount To Be Paid</h4>
          </div>
          <div class="clearfix"> </div>
          <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

              <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="tab_content1040" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
                      <table id="order_table" class="table table-striped table-bordered" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Order ID</th>
                              <th>Supplier</th>
                              <th>Date</th>
                              <th>Total Price</th>
                              <th>Location</th>
                          </tr>
                          </thead>

                          <tbody>
                          <?php
                          if(count($order_spend_paid_stats)>0) {
                              foreach ($order_spend_paid_stats as $order) {
                                $totalPaiedAmount = $order['total_amount'];
                               ?>
                                  <tr>
                                      <td><?php echo $order['order_id']; ?></td>
                                      <td>
                                          <a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a>
                                      </td>
                                      <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                                      <td>
                                          <nobr>
                                            <?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalPaiedAmount*FunctionManager::currencyRate($tool_currency), 0); ?>
                                          </nobr>
                                      </td>

                                      <td>
                                          <?php echo $order['location']; ?>
                                      </td>
                                  </tr>

                              <?php }
                          } else {
                              ?>
                              <tr>
                                  <td style="width: 100%;" colspan="8">No Record Found</td>
                              </tr>
                          <?php } ?>
                          </tbody>

                      </table>
                  </div>

              </div>

          </div>
      </div>
  </div>
</div>
<!-- Spend To Order Received -->
<div class="modal fade" id="received_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
              <div class="modal-dialog">
                  <!-- Modal content -->
                  <div class="modal-content">
                      <div class="modal-header alert-info">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Orders To Be Received</h4>
                      </div>
                      <div class="clearfix"> </div>
                      <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

                          <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="tab_content1041" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
                                  <table id="order_table" class="table table-striped table-bordered" style="width: 100%;">
                                      <thead>
                                      <tr>
                                          <th>Order ID</th>
                                          <th>Supplier</th>
                                          <th>Date</th>
                                          <th>Total Price</th>
                                          <th>Location</th>
                                      </tr>
                                      </thead>

                                      <tbody>

                                      <?php
                                      if(count($order_spend_received_stats)>0) {
                                          foreach ($order_spend_received_stats as $order) { ?>
                                              <tr>
                                                  <td><?php echo $order['order_id']; ?></td>
                                                  <td>
                                                      <a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a>
                                                  </td>
                                                  <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                                                  <td>
                                                      <nobr>
                                                          <?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($order['total_price'], 2); ?>
                                                      </nobr>
                                                  </td>

                                                  <td>
                                                      <?php echo $order['location']; ?>
                                                  </td>
                                              </tr>

                                          <?php }
                                      } else {
                                          ?>
                                          <tr>
                                              <td style="width: 100%;" colspan="8">No Record Found</td>
                                          </tr>
                                      <?php } ?>

                                      </tbody>

                                  </table>
                              </div>

                          </div>

                      </div>
                  </div>
              </div>
          </div>


<!-- Ok after this -->


<!-- Spend To Order Received -->
<div class="modal fade" id="avg_per_category_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content">
		  <div class="modal-header alert-info">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Avg Suppliers / Category</h4>
		  </div>
		  <div class="clearfix"> </div>
		  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

			  <div id="myTabContent" class="tab-content">
				  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
					  <table id="order_avg_suppliers_category" class="table table-striped table-bordered" style="width: 100%;">
						  <thead>
						  <tr>
							  <th>Category</th>
							  <th>Supplier</th>
							  <th>Count</th>
						  </tr>
						  </thead>

						  <tbody>

						  <?php
							  foreach ($order_avg_suppliers_category_stats as $category) { ?>
								  <tr>
									  <td><?php echo $category['category']; ?></td>
									  <td>
										  <a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $category['vendor_id']); ?>"><?php echo $category['vendor']; ?></a>
									  </td>
									  <td><?php echo $category['vendor_count']; ?></td>
								  </tr>

							  <?php } ?>

						  </tbody>

					  </table>
				  </div>

			  </div>

		  </div>
	  </div>
  </div>
</div>
<!-- Total Budget -->
<div class="modal fade" id="department_total_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content">
		  <div class="modal-header alert-info">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Total Budget & Spent</h4>
		  </div>
		  <div class="clearfix"> </div>
		  <div id="department-total-budget" style="padding: 5px;"></div>

	  </div>
  </div>
</div>
<!-- Budget Left -->
<div class="modal fade" id="department_left_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			  <div class="modal-header alert-info">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Percentage Budget Left</h4>
			  </div>
			  <div class="clearfix"> </div>
			  <div id="department-left-budget" style="padding: 5px;"></div>

		  </div>
	  </div>
  </div>

<!-- Budget Left -->
<div class="modal fade" id="department_over_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			  <div class="modal-header alert-info">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Departments Over Budget</h4>
			  </div>
			  <div class="clearfix"> </div>
			  <div id="department-over-budget" style="padding: 5px;"></div>

		  </div>
	  </div>
  </div>

<!--  Average Suppliers / Department -->
<div class="modal fade" id="department_avg_suppliers_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			  <div class="modal-header alert-info">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Average Suppliers / Department</h4>
			  </div>
			  <div class="clearfix"> </div>
			  <div id="department-avg_suppliers" style="padding: 5px;"></div>

		  </div>
	  </div>
</div>
<!-- Average Suppliers / Categories -->
<div class="modal fade" id="department_avg_categories_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
		  <!-- Modal content-->
		  <div class="modal-content">
			  <div class="modal-header alert-info">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title">Average Suppliers / Categories</h4>
			  </div>
			  <div class="clearfix"> </div>
			  <div id="department-avg_categories" style="padding: 5px;"></div>

		  </div>
	  </div>
</div>

<!-- Contract Active Values -->
<div class="modal fade" id="contract_active_value_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Active Contracts Value</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_active_value" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Supplier</th>
										  <th>Reference</th>
										  <th>Title</th>
										  <th>Start Date</th>
										  <th>End Date</th>
										  <th>Amount</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($contract_active_value_stats as $contract) { ?>
											  <tr>
												  <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $contract['vendor_id']); ?>"><?php echo $contract['vendor_name']; ?></a></td>
												  <td><?php echo $contract['contract_reference']; ?></td>
												  <td><?php echo $contract['contract_title']; ?></td>
												  <td><?php echo date("F j, Y", strtotime($contract['contract_start_date'])); ?></td>
												  <td><?php echo date("F j, Y", strtotime($contract['contract_end_date'])); ?></td>
												  <td style="text-align: right;">
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($contract['contract_value'], 2);
														  ?>
													  </nobr>
												  </td>
											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Contract Expire 30 days -->
<div class="modal fade" id="contract_expire_30days_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Expire in 30 Days</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_expire_30days" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Supplier</th>
										  <th>Reference</th>
										  <th>Title</th>
										  <th>Start Date</th>
										  <th>End Date</th>
										  <th>Amount</th>
									  </tr>
									  </thead>

									  <tbody>

                                      <?php
                                      if(count($contract_expire_30days_stats)>0) {
                                          foreach ($contract_expire_30days_stats as $contract) { ?>
                                              <tr>
                                                  <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $contract['vendor_id']); ?>"><?php echo $contract['vendor_name']; ?></a></td>
                                                  <td><?php echo $contract['contract_reference']; ?></td>
                                                  <td><?php echo $contract['contract_title']; ?></td>
                                                  <td><?php echo date("F j, Y", strtotime($contract['contract_start_date'])); ?></td>
                                                  <td><?php echo date("F j, Y", strtotime($contract['contract_end_date'])); ?></td>
                                                  <td style="text-align: right;">
                                                      <nobr>
                                                          <?php
                                                          echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($contract['contract_value'], 2);
                                                          ?>
                                                      </nobr>
                                                  </td>
                                              </tr>

                                          <?php }
                                      } else {
                                          ?>
                                          <tr>
                                              <td style="width: 100%;" colspan="8">No Record Found</td>
                                          </tr>
                                      <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Contract Expire 90 days -->
<div class="modal fade" id="contract_expire_91days_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Expire in 3 Months</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_expire_91days" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Supplier</th>
										  <th>Reference</th>
										  <th>Title</th>
										  <th>Start Date</th>
										  <th>End Date</th>
										  <th>Amount</th>
									  </tr>
									  </thead>

									  <tbody>

                                      <?php
                                      if(count($contract_expire_91days_stats)>0) {
                                          foreach ($contract_expire_91days_stats as $contract) { ?>
                                              <tr>
                                                  <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $contract['vendor_id']); ?>"><?php echo $contract['vendor_name']; ?></a></td>
                                                  <td><?php echo $contract['contract_reference']; ?></td>
                                                  <td><?php echo $contract['contract_title']; ?></td>
                                                  <td><?php echo date("F j, Y", strtotime($contract['contract_start_date'])); ?></td>
                                                  <td><?php echo date("F j, Y", strtotime($contract['contract_end_date'])); ?></td>
                                                  <td style="text-align: right;">
                                                      <nobr>
                                                          <?php
                                                          echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($contract['contract_value'], 2);
                                                          ?>
                                                      </nobr>
                                                  </td>
                                              </tr>

                                          <?php }
                                      } else {
                                          ?>
                                          <tr>
                                              <td style="width: 100%;" colspan="8">No Record Found</td>
                                          </tr>
                                      <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Total Budget -->
<div class="modal fade" id="total_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Total Budget</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="total_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($budget_total_stats as $budget) { ?>
											  <tr>
												   <td><?php echo $budget['location']; ?></td>
												  <td><?php echo $budget['department']; ?></td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget'], 2);
														  ?>
													  </nobr>
												  </td>
											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Left Budget -->
<div class="modal fade" id="total_left_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Total Budget Left</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="left_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
										  <th>Left</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($budget_left_stats as $budget) { ?>
											  <tr>
												  <td><?php echo $budget['location']; ?></td>
												  <td><?php echo $budget['department']; ?></td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget'], 2);
														  ?>
													  </nobr>
												  </td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['left'], 2);
														  ?>
													  </nobr>
												  </td>
											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Avg Budget -->
<div class="modal fade" id="total_avg_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Average Budget Left</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="average_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
										  <th>Left</th>
										  <th>% Left</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($budget_avg_stats as $budget) { ?>
											  <tr>
												  <td><?php echo $budget['location']; ?></td>
												  <td><?php echo $budget['department']; ?></td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget'], 2);
														  ?>
													  </nobr>
												  </td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['left'], 2);
														  ?>
													  </nobr>
												  </td>

												  <td>
													  <nobr>
														  <?php
														  echo '%'.number_format($budget['percentage'], 2);
														  ?>
													  </nobr>
												  </td>

											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Over Budget -->
<div class="modal fade" id="total_over_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Departments Over Budget</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="over_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
										  <th>used</th>

									  </tr>
									  </thead>

									  <tbody>
                                      <?php
                                      if(count($budget_over_stats)>0) {
                                          foreach ($budget_over_stats as $budget) { ?>
                                              <tr>
                                                  <td><?php echo $budget['location']; ?></td>
                                                  <td><?php echo $budget['department']; ?></td>
                                                  <td>
                                                      <nobr>
                                                          <?php
                                                          echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget'], 2);
                                                          ?>
                                                      </nobr>
                                                  </td>
                                                  <td>
                                                      <nobr>
                                                          <?php
                                                          echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['left'], 2);
                                                          ?>
                                                      </nobr>
                                                  </td>

                                              </tr>

                                          <?php }
                                      } else {
                                          ?>
                                          <tr>
                                              <td style="width: 100%;" colspan="3">No Record Found</td>
                                          </tr>
                                      <?php } ?>
									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Current Month Budget -->
<div class="modal fade" id="total_current_month_budget_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Budget Pct This Month</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="left_budget" class="table table-striped table-bordered" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Location</th>
										  <th>Department</th>
										  <th>Budget</th>
										  <th>Left</th>
										  <th>% Used</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php
										  foreach ($budget_current_month_stats as $budget) { ?>
											  <tr>
												  <td><?php echo $budget['location']; ?></td>
												  <td><?php echo $budget['department']; ?></td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['budget'], 2);
														  ?>
													  </nobr>
												  </td>
												  <td>
													  <nobr>
														  <?php
														  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($budget['left'], 2);
														  ?>
													  </nobr>
												  </td>

												  <td>
													  <nobr>
														  <?php
														  echo '%'.number_format(100-$budget['percentage'], 2);
														  ?>
													  </nobr>
												  </td>

											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>

<style>
	  .dataTables_filter {width: 72%;}
</style>
  <script type="text/javascript">
	  $(document).ready( function() {

		  <?php if (isset($vendor['subindustry_id']) && !empty($vendor['subindustry_id'])) { ?>
		  loadSubindustries(<?php echo $vendor['subindustry_id']; ?>);
		  <?php } ?>

		  $('#order_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#expense_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#order_spent_to_approve_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#expense_spent_to_approve_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#top_supplier_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#supplier_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });
		  $('#count_order_supplier_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });
		  $('#count_expense_supplier_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });
		  $('#order-non-preferred-spend_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

//		  $('#order_avg_suppliers_category').dataTable({
//			  "columnDefs": [
//				  {"targets": 0, "width": "6%", "orderable": false},
//				  {"type": "sort-month-year", targets: 3}
//			  ],
//			  "order": []
//		  });


		  $('#expense-non-preferred-spend_table').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

		  $('#contract_active_value').dataTable({
			  "columnDefs": [
				  {"targets": 0, "width": "6%", "orderable": false},
				  {"type": "sort-month-year", targets: 3}
			  ],
			  "order": []
		  });

//		  $('#contract_expire_30days').dataTable({
//			  "columnDefs": [
//				  {"targets": 0, "width": "6%", "orderable": false},
//				  {"type": "sort-month-year", targets: 3}
//			  ],
//			  "order": []
//		  });
//
//		  $('#contract_expire_91days').dataTable({
//			  "columnDefs": [
//				  {"targets": 0, "width": "6%", "orderable": false},
//				  {"type": "sort-month-year", targets: 3}
//			  ],
//			  "order": []
//		  });

	  });
  </script>

<script type="text/javascript">

var contract_by_status_chart_created = false;
var orders_by_status_chart_created = false;

$(document).ready(function() {

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  	if ($(e.target).attr("href") == '#tab_content5')
  	{
  		if (contract_by_status_chart_created == false)
  		{
  			var contracts_polar = new Chart($('#contracts_by_status'), contracts_polar_config);
  			contract_by_status_chart_created = true;
  		}
  	}

  	if ($(e.target).attr("href") == '#tab_content3')
  	{
  		if (orders_by_status_chart_created == false)
  		{
		    var polar = new Chart($('#orders_by_status'), polar_config);
  			orders_by_status_chart_created = true;
  		}
  	}

});

	var chart_labels = [];
	var chart_full_labels = [];
	var chart_data = [];
	var chart_colors = [];

	<?php
		for ($j=1; $j<=7; $j++)
		{
			if ($j == 3) continue;

			echo 'chart_labels[' . $j . '] = [];';
			echo 'chart_full_labels[' . $j . '] = [];';
			echo 'chart_data[' . $j . '] = [];';
			echo 'chart_colors[' . $j . '] = [];';

			$canvas_id = '';
			$dataset = array();

			if ($j == 1)
			{
				$canvas_id = 'year_to_date';
				$dataset = $current_year;
			}
			else if ($j == 2)
			{
				$canvas_id = 'spend_vendors';
				$dataset = $top_vendors;
			}
			else if ($j == 3)
			{
				$canvas_id = 'spend_departments';
				$dataset = $top_departments;
			}
			else if ($j == 4)
			{
				$canvas_id = 'spend_locations';
				$dataset = $top_locations;
			}
			else if ($j == 5)
			{
				$canvas_id = 'top_vendors';
				$dataset = $top_vendors;
			}
			else if ($j == 6)
			{
				$canvas_id = 'year_over_year';
				$dataset = $year_over_year;
			}
			else if ($j == 7)
			{
				$canvas_id = 'spend_contracts';
				$dataset = $top_contracts;
			}

			$i = 0;
			foreach ($dataset as $idx => $a_data_row)
			{
				if ($idx === 'total') continue;
	?>

				<?php if ($j == 7) { ?>
					chart_labels[<?php echo $j; ?>][<?php echo $i; ?>] = "<?php if (strlen($a_data_row['dim_1']) >= 18) { $name = substr($a_data_row['dim_1'], 0, 18); $name = rtrim(ltrim($name)); $pos = strrpos($name, ' '); if ($pos !== false && $pos > 12) $name = substr($name, 0, $pos); echo $name; } else echo $a_data_row['dim_1']; ?>";
					chart_full_labels[<?php echo $j; ?>][<?php echo $i; ?>] = "<?php echo $a_data_row['dim_1']; ?>";
				<?php } else { ?>
					chart_labels[<?php echo $j; ?>][<?php echo $i; ?>] = "<?php echo $a_data_row['dim_1']; ?>";
				<?php } ?>
				chart_data[<?php echo $j; ?>][<?php echo $i; ?>] = <?php echo round($a_data_row['total'], 2); ?>;

	<?php
				$i += 1;

				$color = '';
				switch ($i)
				{
					case 1  : $color = '#3498DB'; break;
					case 2  : $color = '#9B59B6'; break;
					case 3  : $color = '#1ABB9C'; break;
					case 4  : $color = '#00CED1'; break;
					case 5  : $color = '#E74C3C'; break;
					case 6  : $color = '#FAEBD7'; break;
					case 7  : $color = '#7FFFD4'; break;
					case 8  : $color = '#FFE4C4'; break;
					case 9  : $color = '#BDB76B'; break;
					default : $color = '#FFA07A'; break;
				}

				echo 'chart_colors[' . $j . '][' . ($i - 1) . '] = "' . $color . '";';

			}
	?>

			createChart('<?php echo $canvas_id; ?>', chart_data[<?php echo $j; ?>], chart_labels[<?php echo $j; ?>], chart_colors[<?php echo $j; ?>], chart_full_labels[<?php echo $j; ?>]);
	<?php

		}
	?>

});

function createChart(div_id, data_values, data_labels, data_colors, chart_full_labels)
{
	//console.log(data_values);
	var chart_type = 'doughnut';
	if (div_id == 'spend_departments') chart_type = 'pie';
	if (div_id == 'year_to_date') chart_type = 'line';
	if (div_id == 'spend_locations') chart_type = 'bar';
	if (div_id == 'year_over_year') chart_type = 'bar';
	if (div_id == 'spend_contracts') chart_type = 'horizontalBar';

	var chart_settings = false;

	if (chart_type == 'line')
	{
		var yearly_data_1 = new Array();
		var yearly_data_2 = new Array();
		var yearly_data_3 = new Array();

		for (var i=0; i<=11; i++) yearly_data_1[i] = data_values[i];
		for (var i=12; i<=23; i++) yearly_data_2[i-12] = data_values[i];
		for (var i=24; i<=35; i++) yearly_data_3[i-24] = data_values[i];

		chart_settings = {
			type: chart_type,
			tooltipFillColor: "rgba(51, 51, 51, 0.55)",
			data: {
				labels: [ "", "", "", "", "", "", "", "", "", "", "", "" ],
				datasets: [
							{ label: "<?php echo date("Y") - 2; ?>", data: yearly_data_1, fill: false, lineTension: 0, borderColor: 'orange', backgroundColor: 'orange' },
							{ label: "<?php echo date("Y") - 1; ?>", data: yearly_data_2, fill: false, lineTension: 0, borderColor: 'aqua', backgroundColor: 'aqua' },
							{ label: "<?php echo date("Y"); ?>", data: yearly_data_3, fill: false, lineTension: 0, borderColor: 'purple', backgroundColor: 'purple' }
				]
			},
			options: {
				legend: false,
				responsive: true,
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							var suffix = data.datasets[tooltipItem.datasetIndex].label;

							if (tooltipItem.index == 0) suffix = 'Jan ' + suffix;
							if (tooltipItem.index == 1) suffix = 'Feb ' + suffix;
							if (tooltipItem.index == 2) suffix = 'Mar ' + suffix;
							if (tooltipItem.index == 3) suffix = 'Apr ' + suffix;
							if (tooltipItem.index == 4) suffix = 'May ' + suffix;
							if (tooltipItem.index == 5) suffix = 'Jun ' + suffix;
							if (tooltipItem.index == 6) suffix = 'Jul ' + suffix;
							if (tooltipItem.index == 7) suffix = 'Aug ' + suffix;
							if (tooltipItem.index == 8) suffix = 'Sep ' + suffix;
							if (tooltipItem.index == 9) suffix = 'Oct ' + suffix;
							if (tooltipItem.index == 10) suffix = 'Nov ' + suffix;
							if (tooltipItem.index == 11) suffix = 'Dec ' + suffix;

						    if (parseInt(value) >= 1000){
						    	return [ suffix, '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")];
						    } else {
						    	return [ suffix, '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value];
						    }
						}
					}
				},
	            scales: { yAxes: [{ ticks: {
	            								min: 0,
												callback: function(value, index, values) {
              										if (parseInt(value) >= 1000) {
                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              										} else {
                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
              										}
	            						   		}
	            						   }
	            				 }]
	            	}
			}
		};
	}
	else
	{
		if (div_id == 'spend_contracts')
			chart_settings = {
				type: chart_type,
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: data_labels,
					full_labels: chart_full_labels,
					datasets: [{
						data: data_values,
						backgroundColor: data_colors
					}]
				},
				options: {
					legend: false,
					responsive: true,
					tooltips: {
						callbacks: {
							label: function(tooltipItem, data) {
								var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								var prefix = data.full_labels[tooltipItem.index];
								var output = "";

							    if (parseInt(value) >= 1000){
							    	output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							    } else {
							    	output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
							    }

							    if (prefix.length >= 15) return ['(' + prefix + ')', output];
							    else return output;
							}
						}
					},
		            scales: { xAxes: [{ ticks: {
		            								min: 0,
													callback: function(value, index, values) {
	              										if (parseInt(value) >= 1000) {
	                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              										} else {
	                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              										}
		            						   		}
		            						   }
		            				 }]
		            	}
				}
			};
		else
		{
			if (chart_type == 'doughnut')
				chart_settings = {
					type: chart_type,
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: data_labels,
						datasets: [{
							data: data_values,
							backgroundColor: data_colors
						}]
					},
					options: {
						legend: false,
						responsive: true,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									var prefix = data.labels[tooltipItem.index];

								    if (parseInt(value) >= 1000){
								    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								    } else {
								    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
								    }
								}
							}
						},
			            scales: { yAxes: [{ ticks: {
			            								min: 0,
														callback: function(value, index, values) {
		              										if (parseInt(value) >= 1000) {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		              										} else {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
		              										}
			            						   		}
			            						   }
			            				 }]
			            	}
					}
				};
			else
				chart_settings = {
					type: chart_type,
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: data_labels,
						datasets: [{
							data: data_values,
							backgroundColor: data_colors
						}]
					},
					options: {
						legend: false,
						responsive: true,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								    if (parseInt(value) >= 1000){
								    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								    } else {
								    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
								    }
								}
							}
						},
			            scales: { yAxes: [{ ticks: {
			            								min: 0,
														callback: function(value, index, values) {
		              										if (parseInt(value) >= 1000) {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		              										} else {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
		              										}
			            						   		}
			            						   }
			            				 }]
			            	}
					}
				};
		}
	}

	if (chart_type == 'doughnut') delete chart_settings['options']['scales'];
	var chart = new Chart( $('#' + div_id), chart_settings);
}

function expandChart(display_index)
{
	var show_index = display_index;
	var hide_index = 0;
	if (display_index % 2 != 0) hide_index = show_index - 1;
	else hide_index = show_index + 1;


	if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
	else
	{
		$('#chart_area_' + hide_index).hide(1000);
		$('#table_area_' + hide_index).hide(1000);
		
		$('#chart_area_' + show_index).show(1000);
		$('#table_area_' + show_index).show(1000);
	}
}

function expandChart1(display_index)
{
	var show_index = display_index;

	if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
	else
	{
		$('#chart_area_' + show_index).hide(1000);
		$('#table_area_' + show_index).show(1000);
	}
}

function collapseChart(display_index)
{
	var show_index = display_index;
	var hide_index = 0;
	if (display_index % 2 != 0) hide_index = show_index - 1;
	else hide_index = show_index + 1;

	$('#table_area_' + show_index).hide(1000);
	$('#table_area_' + hide_index).hide(1000);
	
	if (!$('#chart_area_' + show_index).is(':visible')) 
		$('#chart_area_' + show_index).show(2000);
	if (!$('#chart_area_' + hide_index).is(':visible')) 
		$('#chart_area_' + hide_index).show(2000);
}


function exportChart(canvas_id)
{
	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id;
}

</script>



<!-- START: Spend Total Orders for the duration ( Graph Spark2 )-->

<!-- END: Spend Total Orders for the duration -->


<!-- END: Spend Orders to approve for the duration -->


<!-- Spend To Paid -->
<div class="modal fade" id="paid_spend_modal" role="dialog" data-keyboard="false" data-backdrop="static">
              <div class="modal-dialog">
                  <!-- Modal content -->
                  <div class="modal-content">
                      <div class="modal-header alert-info">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Orders To Be Paid</h4>
                      </div>
                      <div class="clearfix"> </div>
                      <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

                          <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="tab_content104" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
                                  <table id="order_table" class="table table-striped table-bordered" style="width: 100%;">
                                      <thead>
                                      <tr>
                                          <th>Order ID</th>
                                          <th>Supplier</th>
                                          <th>Date</th>
                                          <th>Total Price</th>
                                          <th>Location</th>
                                      </tr>
                                      </thead>

                                      <tbody>

                                      <?php
                                      if(count($order_spend_paid_stats)>0) {
                                          foreach ($order_spend_paid_stats as $order) { 
                                            if(!empty($order['order_id'])){
                                            $totalPaiedAmount = $order['total_amount'];
                                            ?>
                                              <tr>
                                                  <td><?php echo $order['order_id']; ?></td>
                                                  <td>
                                                      <a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a>
                                                  </td>
                                                  <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                                                  <td>
                                                      <nobr>
                                                          <?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalPaiedAmount*FunctionManager::currencyRate($tool_currency), 0); ?>
                                                      </nobr>
                                                  </td>

                                                  <td>
                                                      <?php echo $order['location']; ?>
                                                  </td>
                                              </tr>

                                          <?php } }
                                      } else {
                                          ?>
                                          <tr>
                                              <td style="width: 100%;" colspan="8">No Record Found</td>
                                          </tr>
                                      <?php } ?>

                                      </tbody>

                                  </table>
                              </div>

                          </div>

                      </div>
                  </div>
              </div>
          </div>