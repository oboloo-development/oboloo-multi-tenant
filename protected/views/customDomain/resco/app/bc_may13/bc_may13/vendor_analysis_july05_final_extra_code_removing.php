<?php
	$tool_currency = Yii::app()->session['user_currency'];
	$user_currency_symbol = Yii::app()->session['user_currency_symbol'];
	// START: Spark7 Order count  ( Top 10 Supplier Spend )
	$spark7Exp= '';
	$totalVendorAmount=0;
	if(!empty($order_spend_vendor_stats))
	{
	    foreach ($order_spend_vendor_stats as $vendor)
	    {
	        $totalVendorAmount += $vendor['total'];
	        $spark7Exp .= '{ x: "'.(!empty($vendor['vendor_name'])?$vendor['vendor_name']:$vendor['vendor_id']).'",y: '.$vendor['total'].'},';
	    }
	}

	// END: Spartk7 Order count
    
    // START : spark8
	$spark8Exp= '';
	$totalVendorUsed=0;
	$totalVendorArr=array();
	if(!empty($vendor_used))
	{
	    foreach ($vendor_used as $vendor)
	    { $totalVendorArr[$vendor['month_year']] = $totalVendorArr[$vendor['month_year']]+$vendor['total_vendors'];}
	    foreach ($totalVendorArr as $key=>$vendorValue)
	    {
	    	$totalVendorUsed += $vendorValue;
	        $spark8Exp .= '{ x: "'.$key.'",y: '.$vendorValue.'},';
	    }
	}
	// END: Spartk8 Order count

	// START : spark9 Non Prefere Vendors
	$spark9Exp= '';
	$nonPreferAmount=$vendorValue=0;
	$vendNonPreferArr=array();
	if(!empty($vendor_nonprefered))
	{
	    foreach ($vendor_nonprefered as $vendor)
	    { $vendNonPreferArr[$vendor['month_year']] = $vendNonPreferArr[$vendor['month_year']]+$vendor['total_amount'];}
	    foreach ($vendNonPreferArr as $key=>$vendorValue)
	    {
	    	$nonPreferAmount += $vendorValue;
	        $spark9Exp .= '{ x: "'.$key.'",y: '.$vendorValue.'},';
	    }
	}
	// END: Spartk9 Order count

	// START : spark10 Non Prefere Vendors
	$spark10Exp= '';
	$nonPreferCount=$vendorValue=$vendor=0;
	$vendNonPreferArr=array();
	if(!empty($vendor_nonprefered_count))
	{
	    foreach ($vendor_nonprefered_count as $vendor)
	    { $vendNonPreferArr[$vendor['month_year']] = $vendNonPreferArr[$vendor['month_year']]+$vendor['total_vendors'];}
	    foreach ($vendNonPreferArr as $key=>$vendorValue)
	    {
	    	$nonPreferCount += $vendorValue;
	        $spark10Exp .= '{ x: "'.$key.'",y: '.$vendorValue.'},';
	    }
	}
	// END: Spartk10 Order count

	// START : spark11 Non Prefere Vendors
	$spark11Exp= '';
	$vendCatCount=$vendorValue=$vendor=0;
	$vendCatArr=array();
	if(!empty($order_avg_suppliers_category))
	{
	    foreach ($order_avg_suppliers_category as $vendor)
	    { $vendCatArr[$vendor['category']] = $vendCatArr[$vendor['category']]+$vendor['total_vendors'];}
	    foreach ($vendCatArr as $key=>$vendorValue)
	    {
	    	$vendCatCount += $vendorValue;
	        $spark11Exp .= '{ x: "'.$key.'",y: '.$vendorValue.'},';
	    }
	}
	// END: spark11 Order count

	// START: Spark12 Order count
    $spark12Exp= '';
    $totalOrders=0;
    if(!empty($order_count))
    {
        foreach ($order_count as $order)
        {
            $totalOrders += $order['total_orders'];
            $spark12Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_orders'].'},';
        }
    }
    // END: Spartk12 Order count

	$short_category_labels = "";
	$category_labels = $preferred_data = $non_preferred_data = array();

	foreach ($vendors_by_category as $category_index => $data_points)
	{
		$current_category_name = "";
		$preferred = $non_preferred = 0;
		
		foreach ($data_points as $data_point)
		{
			$current_category_name = $data_point['category'];
			if ($data_point['preferred_flag']) $preferred += round($data_point['total'], 2);
			else $non_preferred += round($data_point['total'], 2);
		}
		$category_labels[] = $current_category_name;
		$preferred_data[] = $preferred;
		$non_preferred_data[] = $non_preferred;

	}
	//echo "<pre>"; //print_r($vendors_by_month);
	
	$categories_by_month = array();
	foreach ($vendors_by_month as $month_index => $month_data)
		foreach ($month_data as $category_key => $category_data)
			$categories_by_month[$category_data['category_name']] = array();
	
	$month_labels = array();
	foreach ($vendors_by_month as $month_index => $month_data)
	{
		$current_month_label = date("M Y", strtotime($month_index . '01'));
		$month_labels[] =  $current_month_label;
		
		foreach ($categories_by_month as $category_key => $category_data)
		{
			$category_data_found = 0;
			foreach ($month_data as $category_index => $month_category_data)
			{
				if ($category_key == $month_category_data['category_name'])
				{
					$category_data_found = $month_category_data['vendor_count'];
					break;
				}
			}
			$categories_by_month[$category_key][] = $category_data_found;
		}
	}
	
	// START: Category Spend Vs. Supplier Count

	$vendCatTotal = array_column($category_vendors_combo, 'vendor_total','category');

	$vendCatSeries = array_values($vendCatTotal);
	$vendCatLabel = array_keys($vendCatTotal);

	$vendCatTotal = array_column($category_vendors_combo, 'vendor_count','category');
	$vendCountCatSeries = array_values($vendCatTotal);

	// END: Category Spend Vs. Supplier Count

	// START: Top 10 suppliers Spend with Order Count

	$vendOrderTotal = array_column($vendors_combo, 'total','vendor_name');

	$vendOrdSeries = array_values($vendOrderTotal);
	$vendOrdLabel = array_keys($vendOrderTotal);

	$vendOrderTotal = array_column($vendors_combo, 'count','vendor_name');
	$vendCountOrdSeries = array_values($vendOrderTotal);

	// END: Top 10 suppliers Spend with Order Count

?>
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6">
            <a style="cursor: pointer;" onclick="$('#supplier_spend_modal').modal('show');"><div id="spark7"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
            <a style="cursor: pointer;" onclick="$('#sup_analysis_supplier_count_modal').modal('show');"><div id="spark8"></div></a>
            </div>
        
            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#non_preferred_spend_modal').modal('show');"><div id="spark9"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#non_preferred_count_spend_modal').modal('show');"><div id="spark10"></div></a>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#avg_per_category_modal').modal('show');"><div id="spark11"></div></a>
            </div>

          
            <div class="col-md-2 col-sm-4 col-xs-6">
             <a style="cursor: pointer;" onclick="$('#total_year_ordercount_modal').modal('show');"><div id="spark12"></div></a>
            </div>
          </div>



		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_7">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Category Spend Vs. Supplier Count</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(7);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('category_vendors_combo');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<div id="category_vendors_combo"></div>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_7">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Category Spend Vs. Supplier Count</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(7);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('category_vendors_combo');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<thead>
									<th>#</th>
									<th>Category</th>
									<th>Count</th>
									<th>Total</th>
									</thead>
									<?php
									$idx = 0;
									//print_r($category_vendors_combo);die;
									foreach ($category_vendors_combo as $dataset_idx => $a_dataset)
									{
										if ($dataset_idx === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}
										?>
										<tbody>
										<tr>
											<td style="width: 4%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
											<td style="width: 32%;vertical-align: middle;"><?php echo addslashes($a_dataset['category']); ?></td>
											<td style="width: 32%;vertical-align: middle;"><?php echo addslashes($a_dataset['vendor_count']); ?></td>
											<td style="width: 32%vertical-align: middle;"><?php echo Yii::app()->session['user_currency_symbol'] . ' ' .  number_format($a_dataset['vendor_total'], 2); ?></td>
										</tr>
										</tbody>
										<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>

				</div>
				
				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_8">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title" style="height: 40px;">
			                  <h2>Top Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(8);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('top_vendors');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
		                   		<!-- <div id="top_vendors"></div> -->
		                   		<div id="vendor_pie_vendors"></div>
	                        </div>
	                      </div>
	                   </div>

						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_8">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(8);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('top_vendors');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
					            			<?php
					            				$idx = 0;
					            				foreach ($top_vendors as $dataset_idx => $a_dataset)
												{
													if ($dataset_idx === 'total') continue;
													$idx += 1;
													switch ($idx)
													{
														case 1  : $color = 'blue'; break;
														case 2  : $color = 'purple'; break;
														case 3  : $color = 'green'; break;
														case 4  : $color = 'turquoise'; break;
														case 5  : $color = 'red'; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
					            			?>
					                          		<tr>
					                            		<td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
					                            		<td style="vertical-align: middle;">
					                              			<?php echo $a_dataset['dim_1']; ?> - <?php echo number_format($a_dataset['percent'], 2) . '%'; ?>
					                            		</td>
					                          		</tr>
					                        <?php 
					                        	}
					                        ?>
					                        </table>
			              </div>
			           </div>
			        </div>

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_9">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Categories By Preferred vs. Non-Preferred Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(9);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendors_by_category');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
			                	<div id="vendors_by_category"></div>
		                   		<!-- <canvas id="vendors_by_category" height="140px"></canvas> -->
	                        </div>
	                      </div>
	                   </div>

						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_9">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Categories By Preferred vs. Non-Preferred Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(9);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendors_by_category');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
					            			<?php
					            				$idx = 0;
												foreach ($vendors_by_category as $category_index => $data_points)
												{
													$idx += 1;
													switch ($idx)
													{
														case 1  : $color = 'blue'; break;
														case 2  : $color = 'purple'; break;
														case 3  : $color = 'green'; break;
														case 4  : $color = 'turquoise'; break;
														case 5  : $color = 'red'; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
													
													$current_category_name = "";
													$current_category_total = 0;
													foreach ($data_points as $a_category_row)
													{
														$current_category_name = $a_category_row['category'];
														$current_category_total += $a_category_row['total'];
													}
													
					            			?>
					                          		<tr>
					                            		<td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
					                            		<td style="vertical-align: middle;">
					                            			<nobr>
					                              				<?php echo $current_category_name; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($current_category_total, 2); ?>
					                              			</nobr>
					                            		</td>
					                          		</tr>
					                        <?php 
					                        	}
					                        ?>
					                        </table>
			              </div>
			           </div>
			        </div>
	                   
	                   
		            </div>
		            <div class="clearfix"><br /></div>
		          

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_10">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Suppliers Used By Month And Category</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(10);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<div id="vendors_by_month"></div>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_10">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Suppliers Used By Month And Category</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(10);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php
									$idx = 0;
									foreach ($vendors_by_month as $month_index => $month_data)
									{
										$current_month_label = date("M Y", strtotime($month_index . '01'));

											if ($month_index === 'total') continue;
											$idx += 1;
											switch ($idx)
											{
												case 1  : $color = '#E1E1E1'; break;
												case 2  : $color = '#B2F5D5'; break;
												case 3  : $color = '#D3F5B6'; break;
												case 4  : $color = '#F4B0D3'; break;
												case 5  : $color = '#F3D4B4'; break;
												case 6  : $color = '#D5B0F2'; break;
												case 7  : $color = '#A8C2E0'; break;
												case 8  : $color = 'bisque'; break;
												case 9  : $color = 'khaki'; break;
												default : $color = 'salmon'; break;
											}

										   ?>

											<tr>
												<td style="font-weight: bold;"><?php echo $current_month_label; ?></td>

											</tr>

                                           <?php

											foreach ($month_data as $category_index => $month_category_data)
											{
												?>
													<tr>
														<td>
															<table class="tile_info" style="margin-left: 0px;">
																<tr>
																	<td style="width: 5%;">
																		<i class="fa fa-square" style="margin-left: 10px;color:<?php echo $color; ?> "></i></td>
																	<td style="vertical-align: middle;">
																		<?php echo $month_category_data['category_name']; ?> - <?php echo $month_category_data['vendor_count']; ?>
																	</td>
																</tr>
															</table>
															</td>
													</tr>
													<?php
										    }
									}
									?>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_11">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top 10 suppliers Spend with Order Count</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(11);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendor_combo_chart');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<div id="vendor_combo_chart"></div>
			              	</div>
			           	</div>
			        	</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_11">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Top 10 suppliers Spend With Order Count</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(11);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<thead>
									<th>#</th>
									<th>Vendor Name</th>
									<th>Count</th>
									<th>Total</th>
									</thead>
									<?php
									$idx = 0;
									foreach ($vendors_combo as $vendor_index => $combo)
									{

										if ($vendor_index === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											case 10  : $color = 'blue'; break;
											case 11  : $color = 'purple'; break;
											case 12  : $color = 'green'; break;
											case 13  : $color = 'turquoise'; break;
											case 14  : $color = 'red'; break;
											case 15  : $color = 'antique-white'; break;
											case 16  : $color = 'aqua-marine'; break;
											case 17  : $color = 'bisque'; break;
											case 18  : $color = 'khaki'; break;
											case 19  : $color = 'blue'; break;
											case 20  : $color = 'purple'; break;
											case 21  : $color = 'green'; break;
											case 22  : $color = 'turquoise'; break;
											case 23  : $color = 'red'; break;
											case 24  : $color = 'antique-white'; break;
											case 25  : $color = 'aqua-marine'; break;
											case 26  : $color = 'bisque'; break;
											case 27  : $color = 'khaki'; break;
											case 28  : $color = 'blue'; break;
											case 29  : $color = 'purple'; break;
											case 30  : $color = 'green'; break;
											case 31  : $color = 'turquoise'; break;
											case 32  : $color = 'red'; break;
											case 33  : $color = 'antique-white'; break;
											case 34  : $color = 'aqua-marine'; break;
											case 35  : $color = 'bisque'; break;
											case 36  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}

										?>


											<tr>
											<td style="width: 4%;"><i class="fa fa-square <?php echo $color; ?>" style="margin-left: 10px;"></i></td>
											<td style="width: 39%;vertical-align: middle;"><?php echo $combo['vendor_name']; ?></td>
											<td style="width: 25%;vertical-align: middle;"><?php echo $combo['count']; ?></td>
											<td style="width: 32%;vertical-align: middle;"> <?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format($combo['total'],2); ?></td>
											</tr>
											<?php

									}
									?>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		           



<script type="text/javascript">
$(document).ready(function() {
/*
	var monthly_chart_settings = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ <?php echo $month_labels; ?> ],
			datasets: [

					<?php
						$i = 1;
						foreach ($categories_by_month as $category_key => $category_data)
						{
							if ($i == 1) $backgroundColor = "rgba(220,220,220,0.5)";
							else if ($i == 2) $backgroundColor = "rgba(151,220,187,0.5)";
							else if ($i == 3) $backgroundColor = "rgba(187,220,151,0.5)";
							else if ($i == 4) $backgroundColor = "rgba(220,151,187,0.5)";
							else if ($i == 5) $backgroundColor = "rgba(220,187,151,0.5)";
							else if ($i == 6) $backgroundColor = "rgba(187,151,220,0.5)";
							else if ($i == 7) $backgroundColor = "rgba(120,151,187,0.5)";
							else if ($i == 8) $backgroundColor = "rgba(151,120,187,0.5)";
							else if ($i == 9) $backgroundColor = "rgba(187,120,151,0.5)";
							else if ($i == 10) $backgroundColor = "rgba(151,187,120,0.5)";
							else $backgroundColor = "rgba(151,187,205,0.5)";
					?>
							{
								label: '<?php echo addslashes($category_data['name']); ?>',
								backgroundColor: '<?php echo $backgroundColor; ?>',
								data: [

									<?php
										$d = 1;
										foreach ($category_data['data'] as $category_data_point)
										{
											echo $category_data_point;
											if ($d != count($category_data['data'])) echo ',';
											$d += 1;
										}
									?>

								]

							} <?php if ($i != count($categories_by_month)) echo ','; ?>

					<?php
							$i += 1;
						}
					?>
				]
		},
		options: {
			responsive: true,
			maintainAspectRatio: false,
            scales: {
            	xAxes: [{ stacked: true, ticks: { autoSkip: false, maxRotation: 0, minRotation: 0 } }],
            	yAxes: [{ stacked: true }]
            }
		}
	};*/

	//var month_chart = new Chart($('#vendors_by_month'), monthly_chart_settings);


	var chart_settings = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ <?php echo $short_category_labels; ?> ],
			full_labels: [ <?php echo $category_labels; ?> ],
			datasets: [ 
					{ label: 'Preferred Suppliers', backgroundColor: "rgba(220,220,220,0.5)", data: [ <?php echo $preferred_data; ?> ] }, 
					{ label: 'Non Preferred Suppliers', backgroundColor: "rgba(151,187,205,0.5)", data: [ <?php echo $non_preferred_data; ?> ] } 
				]
		},
		options: { 
			responsive: true,
			maintainAspectRatio: true,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						var output = "";
						var category_name = data.full_labels[tooltipItem.index];
						var prefix = "";
						if (tooltipItem.datasetIndex == 0) prefix = 'Preferred'; else prefix = 'Non-Preferred';
						
					    if (parseInt(value) >= 1000){
					    	output = prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					    } else {
					    	output = prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
					    }

					    if (category_name.length >= 7) return ['(' + category_name + ')', output];
					    else return output;
					}						
				}
			},
            scales: {
            	xAxes: [{ stacked: true, ticks: { autoSkip: false, maxRotation: 90, minRotation: 90 } }],
            	yAxes: [{ stacked: true, 
						  ticks: { 
		            				min: 0, 
									callback: function(value, index, values) {
	              						if (parseInt(value) >= 1000) {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              						} else {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              						}	            	
		            				}
		            		}                									 
	            		}]
            }			 
		}
	};

});	
</script>
<?php 
	
	$nonPreferAmount =  html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($nonPreferAmount*FunctionManager::currencyRate($tool_currency), 2);

?>
<!--START: Apex Chart Metrices -->
<script type="text/javascript">
	var spark7 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: "Spend",
            data: [<?php echo $spark7Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalVendorAmount, 2); ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Top 10 Suppliers Spend',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var spark7 = new ApexCharts(document.querySelector("#spark7"), spark7);
    spark7.render();

 var spark8 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: "Spend",
            data: [<?php echo $spark8Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: '<?php echo $totalVendorUsed; ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Total Suppliers Used',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var spark8 = new ApexCharts(document.querySelector("#spark8"), spark8);
    spark8.render();



  function apexMetriceSpend(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark9Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpend('spark9','Spend','<?php echo $nonPreferAmount ?>','Non Preferred Spend',<?php echo $spark9Exp;?>);

   // Need to capture all these in one function later

   function apexMetriceSpendSpark10(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark10Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark10('spark10','Count','<?php echo $nonPreferCount; ?>','Non Preferred Count',<?php echo $spark10Exp;?>);

 // Need to capture all these in one function later

   function apexMetriceSpendSpark11(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark11Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark11('spark11','Count','<?php echo $vendCatCount; ?>','Suppliers / Category',<?php echo $spark11Exp;?>);

   // Need to capture all these in one function later

   function apexMetriceSpendSpark12(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark12Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark12('spark12','Orders','<?php echo number_format($totalOrders, 0); ?>','Total Year Orders',<?php echo $spark12Exp;?>);


 function createApexChartB(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;

    if(chartIDL=="#vendors_by_category"){
    	$.each(series.preferred, function(key, value) { 
          seriesData.push(value);
        });
      seriesL.push({'name':'Preferred','data':seriesData});
      $.each(series.non_preferred, function(key, value) { alert(value);
          seriesMixedData.push(value);
        });
      seriesL.push({'name':'Non-Preferred','data':seriesMixedData});
      stackedL = true;
      //horizontalL=true;

    }else if(chartIDL=="#category_vendors_combo" || chartIDL=="#vendor_combo_chart"){
    	$.each(series.spend, function(key, value) {
          seriesData.push(value);
        });
      seriesL.push({'name':'Spend','type':'column','data':seriesData});
      $.each(series.count, function(key, value) {
          seriesMixedData.push(value);
        });
      seriesL.push({'name':'Count','type':'line','colors':'yellow','data':seriesMixedData});
    }else if(type=='line'){
        $.each(series, function(key, value) { 
          seriesL.push({'data':value['data'],'name':value['name']});
        });
    }else{
      $.each(series, function(key, value) {  
          seriesData.push(value);
        });
      seriesL.push({'name':toolTip,'data':seriesData});
    }

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value);
    });
    if(chartIDL=="#spend_contracts"){horizontalL=true;}
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }},
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        },
        stacked: stackedL,
        //stackType: '100%',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        legend: {
                position: 'right',
                offsetX: 0,
                offsetY: 50
            },
      colors: colorL,
      plotOptions: {bar: {columnWidth: '35%',distributed: true, horizontal: horizontalL}},
      dataLabels: {enabled: lableL},
      //stroke: { width: [4,  4]}
       stroke: {width: [4, 4]},
      series: seriesL,
      opposite: true,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      fill: {opacity: 1},
      xaxis: {categories: categoriesL,labels: {
                formatter: function (value) {
                  valueL = chartIDL!="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },
                style: {fontSize: '12px'}}},
      /*yaxis: [{
      	labels: {
                formatter: function (value) {console.log($(this));
                  valueL = chartIDL=="#spend_contracts" || value<20?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },
                style: {fontSize: '12px'}
              },},
              
              { axisTicks: {show: true} },
              { axisBorder: {show: true,} },
              
          	],*/
          	yaxis: [
	          	  	{ seriesName: 'P',
	          	  	  axisTicks: {show: true},
				      //axisBorder: { show: true,},
				      title: { text: "Spend"},
				      labels: {
				      	formatter: function (value) {
				      		valueL = chartIDL=="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return valueL
				      	},
				      }},
				  	{   opposite: true,
				  		seriesName: 'Count',
				  		//axisTicks: { show: true},
				  		//axisBorder: { show: true,},
				  		//title: { text: "Count"},
				  		labels: {
				  		show: false,
				      	formatter: function (value) {
				      		valueL = chartIDL=="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return value
				      	},
				      }

				  	}
			  	],
          }
    

    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }
   var colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];

   createApexChartB(220,'100%','line',false,<?php echo json_encode(array('spend'=>$vendCatSeries,'count'=>$vendCountCatSeries)) ?>,<?php echo json_encode($vendCatLabel);?>,"#category_vendors_combo",colors); 

   createApexChartB(300,'100%','line',false,<?php echo json_encode(array('spend'=>$vendOrdSeries,'count'=>$vendCountOrdSeries)) ?>,<?php echo json_encode($vendOrdLabel);?>,"#vendor_combo_chart",colors); 

    //createApexChartB(250,'100%','bar',false,<?php echo json_encode(array('preferred'=>$preferred_data,'non_preferred'=>$non_preferred_data)) ?>,<?php echo json_encode($category_labels);?>,"#vendors_by_category",colors); 

      /**/var options = {
            chart: {
                height: 220,
                type: 'bar',
                stacked: true,
                stackType: '100%'
            },
            //plotOptions: {bar: {columnWidth: '35%',distributed: false, horizontal: false}},
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            series: [{
                name: 'Preferred',
                data: [<?php echo implode(",",$preferred_data);?>]
            },{
                name: 'Non-Preferred',
                data: [<?php echo implode(",",$non_preferred_data);?>]
            }],
            xaxis: {
                categories: [<?php echo "'".implode("','",$category_labels)."'";?>],
                labels: {
                trim: true,
		        show: true,
		        minHeight: undefined,
            	maxHeight: 70,
            	rotate: -5,
		       /* formatter: function (value) {
                return value.substring(0, 4)+'...';
                },
                tooltip: {
            		formatter: function (value) {
                		return value.substring(0, 1)+'...';
                	},
        		},*/
            }},
            yaxis:{ 
				    labels: {
				      	formatter: function (value) {
				      		//valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return value
				      	},
				     }
				},
            fill: {
                opacity: 1
            },
            
            legend: {
                position: 'right',
                offsetX: 0,
                offsetY: 50
            },
        }

       var chart = new ApexCharts(
            document.querySelector("#vendors_by_category"),
            options
        );
        
        chart.render();/**/

        /**/var options = {
            chart: {
                height: 220,
                type: 'bar',
                stacked: true,
                stackType: '100%'
            },
            //plotOptions: {bar: {columnWidth: '35%',distributed: false, horizontal: false}},
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
           series: [
            	<?php foreach($categories_by_month as $name=>$catInfo){ if(!empty($name)){?>{name: "<?php echo $name;?>",data: [<?php echo implode(",",$catInfo);?>]},<?php }} ?>
            ],
            xaxis: {
                categories: [<?php echo "'".implode("','",$month_labels)."'";?>],
                labels: {
                trim: true,
		        show: true,
		        minHeight: undefined,
            	maxHeight: 70,
            	rotate: -5,
		       /* formatter: function (value) {
                return value.substring(0, 4)+'...';
                },
                tooltip: {
            		formatter: function (value) {
                		return value.substring(0, 1)+'...';
                	},
        		},*/
            }},
            yaxis:{ 
				    labels: {
				      	formatter: function (value) {
				      		//valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
				      		return value
				      	},
				     }
				},
            fill: {
                opacity: 1
            },
            
            legend: {
                position: 'right',
                offsetX: 0,
                offsetY: 50
            },
        }

       var chart = new ApexCharts(
            document.querySelector("#vendors_by_month"),
            options
        );
        
        chart.render();/**/
</script>
<!--END: Apex Chart Metrices -->


