<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Add Import Templates</h3>
        </div>
        <div class="clearfix"> </div>
    </div>

	<form name="template_form" id="template_form" class="horizontal-form" role="form" 
			method="post" enctype="multipart/form-data" action="<?php echo AppUrl::bicesUrl('app/import'); ?>">
	
		<div class="form-group">
			<div class="col-md-3">
				<select name="template_type" id="template_type" class="form-control">
					<option value="">Select Template To Upload</option>
					<option value="Vendor">Supplier Import Template</option>
					<option value="Product">Product Import Template</option>
					<!-- <option value="Location">Location Import Template</option> -->
					<option value="Order">Order Import Template</option>
					<option value="Expense">Expense Import Template</option>
                    <option value="Contract">Contract Import Template</option>
				</select>
			</div>

			<div class="col-md-6">
				<input type="file" class="form-control" name="template_file" id="template_file" />
			</div>

			<div class="col-md-2">
				<button class="btn btn-primary" onclick="$('#template_form').submit();">
					Upload Template
				</button>
			</div>

		</div>
	
		<input type="hidden" name="form_submitted" id="form_submitted" value="1" />
	
	</form>

</div>