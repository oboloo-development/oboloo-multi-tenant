<?php
	$tool_currency = Yii::app()->session['user_currency'];
	// START: Spark7 Order count  ( Top 10 Supplier Spend )
	$spark7Exp= '';
	$totalVendorAmount=0;
	if(!empty($order_spend_vendor_stats))
	{
	    foreach ($order_spend_vendor_stats as $vendor)
	    {
	        $totalVendorAmount += $vendor['total'];
	        $spark7Exp .= '{ x: "'.(!empty($vendor['vendor_name'])?$vendor['vendor_name']:$vendor['vendor_id']).'",y: '.$vendor['total'].'},';
	    }
	}
	// END: Spartk7 Order count
    
    // START : spark8
	$spark8Exp= '';
	$totalVendorUsed=0;
	$totalVendorArr=array();
	if(!empty($vendor_used))
	{
	    foreach ($vendor_used as $vendor)
	    { $totalVendorArr[$vendor['month_year']] = $totalVendorArr[$vendor['month_year']]+$vendor['total_vendors'];}
	    foreach ($totalVendorArr as $key=>$vendorValue)
	    {
	    	$totalVendorUsed += $vendorValue;
	        $spark8Exp .= '{ x: "'.$key.'",y: '.$vendorValue.'},';
	    }
	}
	// END: Spartk8 Order count

	// START : spark9 Non Prefere Vendors
	$spark9Exp= '';
	$nonPreferAmount=$vendorValue=0;
	$vendNonPreferArr=array();
	if(!empty($vendor_nonprefered))
	{
	    foreach ($vendor_nonprefered as $vendor)
	    { $vendNonPreferArr[$vendor['month_year']] = $vendNonPreferArr[$vendor['month_year']]+$vendor['total_amount'];}
	    foreach ($vendNonPreferArr as $key=>$vendorValue)
	    {
	    	$nonPreferAmount += $vendorValue;
	        $spark9Exp .= '{ x: "'.$key.'",y: '.$vendorValue.'},';
	    }
	}
	// END: Spartk9 Order count

	// START : spark10 Non Prefere Vendors
	$spark10Exp= '';
	$nonPreferCount=$vendorValue=$vendor=0;
	$vendNonPreferArr=array();
	if(!empty($vendor_nonprefered_count))
	{
	    foreach ($vendor_nonprefered_count as $vendor)
	    { $vendNonPreferArr[$vendor['month_year']] = $vendNonPreferArr[$vendor['month_year']]+$vendor['total_vendors'];}
	    foreach ($vendNonPreferArr as $key=>$vendorValue)
	    {
	    	$nonPreferCount += $vendorValue;
	        $spark10Exp .= '{ x: "'.$key.'",y: '.$vendorValue.'},';
	    }
	}
	// END: Spartk10 Order count

	// START : spark11 Non Prefere Vendors
	$spark11Exp= '';
	$vendCatCount=$vendorValue=$vendor=0;
	$vendCatArr=array();
	if(!empty($order_avg_suppliers_category))
	{
	    foreach ($order_avg_suppliers_category as $vendor)
	    { $vendCatArr[$vendor['category']] = $vendCatArr[$vendor['category']]+$vendor['total_vendors'];}
	    foreach ($vendCatArr as $key=>$vendorValue)
	    {
	    	$vendCatCount += $vendorValue;
	        $spark11Exp .= '{ x: "'.$key.'",y: '.$vendorValue.'},';
	    }
	}
	// END: spark11 Order count

	// START: Spark12 Order count
    $spark12Exp= '';
    $totalOrders=0;
    if(!empty($order_count))
    {
        foreach ($order_count as $order)
        {
            $totalOrders += $order['total_orders'];
            $spark12Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_orders'].'},';
        }
    }
    // END: Spartk12 Order count

	$short_category_labels = $category_labels = $preferred_data = $non_preferred_data = "";
	foreach ($vendors_by_category as $category_index => $data_points)
	{
		$current_category_name = "";
		$preferred = $non_preferred = 0;
		
		foreach ($data_points as $data_point)
		{
			$current_category_name = $data_point['category'];
			if ($data_point['preferred_flag']) $preferred += round($data_point['total'], 2);
			else $non_preferred += round($data_point['total'], 2);
		}

		if ($category_labels === "") $category_labels = "'" . $current_category_name . "'";
		else $category_labels = $category_labels . ", '" . $current_category_name . "'";

		if (strlen($current_category_name) >= 7) 
		{
			$abbr_category_name = "";
			$current_category_name = ucwords($current_category_name);
			foreach (explode(" ", $current_category_name) as $current_category_word)
			{
				if ($abbr_category_name == "") $abbr_category_name = substr($current_category_word, 0, 1);
				else $abbr_category_name = $abbr_category_name . " " . substr($current_category_word, 0, 1);
			}
			$current_category_name = $abbr_category_name;
		}
		if ($short_category_labels === "") $short_category_labels = "'" . $current_category_name . "'";
		else $short_category_labels = $short_category_labels . ", '" . $current_category_name . "'";

		if ($preferred_data === "") $preferred_data = $preferred;
		else $preferred_data = $preferred_data . ',' . $preferred;

		if ($non_preferred_data === "") $non_preferred_data = $non_preferred;
		else $non_preferred_data = $non_preferred_data . ',' . $non_preferred;
	}
	
	$categories_by_month = array();
	foreach ($vendors_by_month as $month_index => $month_data)
		foreach ($month_data as $category_key => $category_data)
			$categories_by_month[$category_key] = array('name' => $category_data['category_name'], 'data' => array());
	
	$month_labels = "";
	foreach ($vendors_by_month as $month_index => $month_data)
	{
		$current_month_label = date("M Y", strtotime($month_index . '01'));
		if ($month_labels === "") $month_labels = "'" . $current_month_label . "'";
		else $month_labels = $month_labels . ", '" . $current_month_label . "'";
		
		foreach ($categories_by_month as $category_key => $category_data)
		{
			$category_data_found = 0;
			foreach ($month_data as $category_index => $month_category_data)
			{
				if ($category_key == $category_index)
				{
					$category_data_found = $month_category_data['vendor_count'];
					break;
				}
			}
			$categories_by_month[$category_key]['data'][] = $category_data_found;
		}
	}
?>
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6">
            <a style="cursor: pointer;" onclick="$('#supplier_spend_modal').modal('show');"><div id="spark7"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
            <a style="cursor: pointer;" onclick="$('#sup_analysis_supplier_count_modal').modal('show');"><div id="spark8"></div></a>
            </div>
        
            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#non_preferred_spend_modal').modal('show');"><div id="spark9"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#non_preferred_count_spend_modal').modal('show');"><div id="spark10"></div></a>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#avg_per_category_modal').modal('show');"><div id="spark11"></div></a>
            </div>

          
            <div class="col-md-2 col-sm-4 col-xs-6">
             <a style="cursor: pointer;" onclick="$('#total_year_ordercount_modal').modal('show');"><div id="spark12"></div></a>
            </div>
          </div>



		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_7">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Category Spend Vs. Supplier Count</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(7);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('category_vendors_combo');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="category_vendors_combo" style="position: relative; height:40vh; width:80vw"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_7">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Category Spend Vs. Supplier Count</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(7);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('category_vendors_combo');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<thead>
									<th>#</th>
									<th>Category</th>
									<th>Count</th>
									<th>Total</th>
									</thead>
									<?php
									$idx = 0;
									//print_r($category_vendors_combo);die;
									foreach ($category_vendors_combo as $dataset_idx => $a_dataset)
									{
										if ($dataset_idx === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}
										?>
										<tbody>
										<tr>
											<td style="width: 4%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
											<td style="width: 32%;vertical-align: middle;"><?php echo addslashes($a_dataset['category']); ?></td>
											<td style="width: 32%;vertical-align: middle;"><?php echo addslashes($a_dataset['vendor_count']); ?></td>
											<td style="width: 32%vertical-align: middle;"><?php echo Yii::app()->session['user_currency_symbol'] . ' ' .  number_format($a_dataset['vendor_total'], 2); ?></td>
										</tr>
										</tbody>
										<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>

				</div>
				
				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_8">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title" style="height: 40px;">
			                  <h2>Top Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(8);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('top_vendors');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content" style="text-align: center;">
		                   		<canvas id="top_vendors" height="140px"></canvas>
	                        </div>
	                      </div>
	                   </div>

						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_8">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(8);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('top_vendors');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
					            			<?php
					            				$idx = 0;
					            				foreach ($top_vendors as $dataset_idx => $a_dataset)
												{
													if ($dataset_idx === 'total') continue;
													$idx += 1;
													switch ($idx)
													{
														case 1  : $color = 'blue'; break;
														case 2  : $color = 'purple'; break;
														case 3  : $color = 'green'; break;
														case 4  : $color = 'turquoise'; break;
														case 5  : $color = 'red'; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
					            			?>
					                          		<tr>
					                            		<td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
					                            		<td style="vertical-align: middle;">
					                              			<?php echo $a_dataset['dim_1']; ?> - <?php echo number_format($a_dataset['percent'], 2) . '%'; ?>
					                            		</td>
					                          		</tr>
					                        <?php 
					                        	}
					                        ?>
					                        </table>
			              </div>
			           </div>
			        </div>

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_9">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Categories By Preferred vs. Non-Preferred Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(9);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendors_by_category');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content" style="text-align: center;">
		                   		<canvas id="vendors_by_category" height="140px"></canvas>
	                        </div>
	                      </div>
	                   </div>

						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_9">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Categories By Preferred vs. Non-Preferred Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(9);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendors_by_category');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
					            			<?php
					            				$idx = 0;
												foreach ($vendors_by_category as $category_index => $data_points)
												{
													$idx += 1;
													switch ($idx)
													{
														case 1  : $color = 'blue'; break;
														case 2  : $color = 'purple'; break;
														case 3  : $color = 'green'; break;
														case 4  : $color = 'turquoise'; break;
														case 5  : $color = 'red'; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
													
													$current_category_name = "";
													$current_category_total = 0;
													foreach ($data_points as $a_category_row)
													{
														$current_category_name = $a_category_row['category'];
														$current_category_total += $a_category_row['total'];
													}
													
					            			?>
					                          		<tr>
					                            		<td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
					                            		<td style="vertical-align: middle;">
					                            			<nobr>
					                              				<?php echo $current_category_name; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($current_category_total, 2); ?>
					                              			</nobr>
					                            		</td>
					                          		</tr>
					                        <?php 
					                        	}
					                        ?>
					                        </table>
			              </div>
			           </div>
			        </div>
	                   
	                   
		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_10">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Suppliers Used By Month And Category</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(10);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="vendors_by_month"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_10">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Suppliers Used By Month And Category</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(10);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php
									$idx = 0;
									foreach ($vendors_by_month as $month_index => $month_data)
									{
										$current_month_label = date("M Y", strtotime($month_index . '01'));

											if ($month_index === 'total') continue;
											$idx += 1;
											switch ($idx)
											{
												case 1  : $color = '#E1E1E1'; break;
												case 2  : $color = '#B2F5D5'; break;
												case 3  : $color = '#D3F5B6'; break;
												case 4  : $color = '#F4B0D3'; break;
												case 5  : $color = '#F3D4B4'; break;
												case 6  : $color = '#D5B0F2'; break;
												case 7  : $color = '#A8C2E0'; break;
												case 8  : $color = 'bisque'; break;
												case 9  : $color = 'khaki'; break;
												default : $color = 'salmon'; break;
											}

										   ?>

											<tr>
												<td style="font-weight: bold;"><?php echo $current_month_label; ?></td>

											</tr>

                                           <?php

											foreach ($month_data as $category_index => $month_category_data)
											{
												?>
													<tr>
														<td>
															<table class="tile_info" style="margin-left: 0px;">
																<tr>
																	<td style="width: 5%;">
																		<i class="fa fa-square" style="margin-left: 10px;color:<?php echo $color; ?> "></i></td>
																	<td style="vertical-align: middle;">
																		<?php echo $month_category_data['category_name']; ?> - <?php echo $month_category_data['vendor_count']; ?>
																	</td>
																</tr>
															</table>
															</td>
													</tr>
													<?php
										    }
									}
									?>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>


				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_11">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top 10 suppliers Spend with Order Count</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(11);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendor_combo_chart');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="vendor_combo_chart"></canvas>
			              	</div>
			           	</div>
			        	</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_11">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Top 10 suppliers Spend With Order Count</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(11);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<thead>
									<th>#</th>
									<th>Vendor Name</th>
									<th>Count</th>
									<th>Total</th>
									</thead>
									<?php
									$idx = 0;
									foreach ($vendors_combo as $vendor_index => $combo)
									{

										if ($vendor_index === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											case 10  : $color = 'blue'; break;
											case 11  : $color = 'purple'; break;
											case 12  : $color = 'green'; break;
											case 13  : $color = 'turquoise'; break;
											case 14  : $color = 'red'; break;
											case 15  : $color = 'antique-white'; break;
											case 16  : $color = 'aqua-marine'; break;
											case 17  : $color = 'bisque'; break;
											case 18  : $color = 'khaki'; break;
											case 19  : $color = 'blue'; break;
											case 20  : $color = 'purple'; break;
											case 21  : $color = 'green'; break;
											case 22  : $color = 'turquoise'; break;
											case 23  : $color = 'red'; break;
											case 24  : $color = 'antique-white'; break;
											case 25  : $color = 'aqua-marine'; break;
											case 26  : $color = 'bisque'; break;
											case 27  : $color = 'khaki'; break;
											case 28  : $color = 'blue'; break;
											case 29  : $color = 'purple'; break;
											case 30  : $color = 'green'; break;
											case 31  : $color = 'turquoise'; break;
											case 32  : $color = 'red'; break;
											case 33  : $color = 'antique-white'; break;
											case 34  : $color = 'aqua-marine'; break;
											case 35  : $color = 'bisque'; break;
											case 36  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}

										?>


											<tr>
											<td style="width: 4%;"><i class="fa fa-square <?php echo $color; ?>" style="margin-left: 10px;"></i></td>
											<td style="width: 39%;vertical-align: middle;"><?php echo $combo['vendor_name']; ?></td>
											<td style="width: 25%;vertical-align: middle;"><?php echo $combo['count']; ?></td>
											<td style="width: 32%;vertical-align: middle;"> <?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format($combo['total'],2); ?></td>
											</tr>
											<?php

									}
									?>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>



<script type="text/javascript">
$(document).ready(function() {

	var monthly_chart_settings = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ <?php echo $month_labels; ?> ],
			datasets: [

					<?php
						$i = 1;
						foreach ($categories_by_month as $category_key => $category_data)
						{
							if ($i == 1) $backgroundColor = "rgba(220,220,220,0.5)";
							else if ($i == 2) $backgroundColor = "rgba(151,220,187,0.5)";
							else if ($i == 3) $backgroundColor = "rgba(187,220,151,0.5)";
							else if ($i == 4) $backgroundColor = "rgba(220,151,187,0.5)";
							else if ($i == 5) $backgroundColor = "rgba(220,187,151,0.5)";
							else if ($i == 6) $backgroundColor = "rgba(187,151,220,0.5)";
							else if ($i == 7) $backgroundColor = "rgba(120,151,187,0.5)";
							else if ($i == 8) $backgroundColor = "rgba(151,120,187,0.5)";
							else if ($i == 9) $backgroundColor = "rgba(187,120,151,0.5)";
							else if ($i == 10) $backgroundColor = "rgba(151,187,120,0.5)";
							else $backgroundColor = "rgba(151,187,205,0.5)";
					?>
							{
								label: '<?php echo addslashes($category_data['name']); ?>',
								backgroundColor: '<?php echo $backgroundColor; ?>',
								data: [

									<?php
										$d = 1;
										foreach ($category_data['data'] as $category_data_point)
										{
											echo $category_data_point;
											if ($d != count($category_data['data'])) echo ',';
											$d += 1;
										}
									?>

								]

							} <?php if ($i != count($categories_by_month)) echo ','; ?>

					<?php
							$i += 1;
						}
					?>
				]
		},
		options: {
			responsive: true,
			maintainAspectRatio: false,
            scales: {
            	xAxes: [{ stacked: true, ticks: { autoSkip: false, maxRotation: 0, minRotation: 0 } }],
            	yAxes: [{ stacked: true }]
            }
		}
	};

	var month_chart = new Chart($('#vendors_by_month'), monthly_chart_settings);


	var chart_settings = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ <?php echo $short_category_labels; ?> ],
			full_labels: [ <?php echo $category_labels; ?> ],
			datasets: [ 
					{ label: 'Preferred Suppliers', backgroundColor: "rgba(220,220,220,0.5)", data: [ <?php echo $preferred_data; ?> ] }, 
					{ label: 'Non Preferred Suppliers', backgroundColor: "rgba(151,187,205,0.5)", data: [ <?php echo $non_preferred_data; ?> ] } 
				]
		},
		options: { 
			responsive: true,
			maintainAspectRatio: true,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						var output = "";
						var category_name = data.full_labels[tooltipItem.index];
						var prefix = "";
						if (tooltipItem.datasetIndex == 0) prefix = 'Preferred'; else prefix = 'Non-Preferred';
						
					    if (parseInt(value) >= 1000){
					    	output = prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					    } else {
					    	output = prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
					    }

					    if (category_name.length >= 7) return ['(' + category_name + ')', output];
					    else return output;
					}						
				}
			},
            scales: {
            	xAxes: [{ stacked: true, ticks: { autoSkip: false, maxRotation: 90, minRotation: 90 } }],
            	yAxes: [{ stacked: true, 
						  ticks: { 
		            				min: 0, 
									callback: function(value, index, values) {
	              						if (parseInt(value) >= 1000) {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              						} else {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              						}	            	
		            				}
		            		}                									 
	            		}]
            }			 
		}
	};
	
	var chart = new Chart($('#vendors_by_category'), chart_settings);


	var category_combo_data = {
		labels: [
			<?php $c = 1; foreach ($category_vendors_combo as $category_combo_row) { ?>
				
				'<?php echo addslashes($category_combo_row['category']); ?>' <?php if ($c != count($category_vendors_combo)) echo ','; ?> 
				
			<?php $c += 1; } ?>
		],
		
		datasets: [
		
			{
                type: 'line',
                label: 'Supplier Count',
                borderColor: 'purple',
                borderWidth: 2,
                yAxisID: 'A',
                fill: false,
                data: [

						<?php $c = 1; foreach ($category_vendors_combo as $category_combo_row) { ?>
							
							<?php echo $category_combo_row['vendor_count']; ?> 
							<?php if ($c != count($category_vendors_combo)) echo ','; ?> 
							
						<?php $c += 1; } ?>

                ]				
			},
			
			{
                type: 'bar',
                label: 'Amount Spent',
                backgroundColor: 'lightgreen',
                yAxisID: 'B',
                data: [
                
						<?php $c = 1; foreach ($category_vendors_combo as $category_combo_row) { ?>
							
							<?php echo round($category_combo_row['vendor_total'], 2); ?> 
							<?php if ($c != count($category_vendors_combo)) echo ','; ?> 
							
						<?php $c += 1; } ?>
						
                ]				
			}
		
		]
	};	
	
    var category_combo_chart = 
    	new Chart($('#category_vendors_combo'), 
    			{
                	type: 'bar',
                	data: category_combo_data,
                	options: {
						responsive: true,
						maintainAspectRatio: false,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									if (tooltipItem.datasetIndex == 0) return value;
									else
									{
									    if (parseInt(value) >= 1000){
									    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									    } else {
									    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
									    }
									}
								}						
							}
						},
                		scales: { xAxes: [{ ticks: {  maxRotation: 0, minRotation: 0 } }],
                					yAxes: [
                								{ 
                									id: 'B', type: 'linear', position: 'left',
													ticks: { 
		            									min: 0, 
														callback: function(value, index, values) {
	              											if (parseInt(value) >= 1000) {
	                											return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              											} else {
	                											return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              											}	            	
		            						   			}
		            						   		}                									 
                								}, 
                								{ id: 'A', type: 'linear', position: 'right', ticks: { min: 0, stepSize: 1 } }
                							] 
 						} 
 					}
            	}
        );


	var vendor_combo_data = {
		labels: [
			<?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>
				
				'<?php
				   echo addslashes(substr($vendor_combo_row['vendor_name'], 0, 10)); ?>' <?php if ($c != count($vendors_combo)) echo ',';
				 ?>
				
			<?php $c += 1; } ?>
		],

		full_labels: [
			<?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>
				
				'<?php echo addslashes($vendor_combo_row['vendor_name']); ?>' <?php if ($c != count($vendors_combo)) echo ','; ?>
				
			<?php $c += 1; } ?>
		],
		
		datasets: [
		
			{
                type: 'line',
                label: 'Supplier Count',
                borderColor: 'purple',
                borderWidth: 2,
                fill: false,
                yAxisID: 'Y1',
                data: [

						<?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>

							<?php echo $vendor_combo_row['count']; ?>
							<?php if ($c != count($vendors_combo)) echo ','; ?>
							
						<?php $c += 1; } ?>

                ]				
			},
			
			{
                type: 'bar',
                label: 'Amount Spent',
                backgroundColor: 'lightgreen',
                fill: false,
                yAxisID: 'Y2',
                data: [
                
						<?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>

							<?php echo round($vendor_combo_row['total'], 2); ?>
							<?php if ($c != count($vendors_combo)) echo ','; ?>
							
						<?php $c += 1; } ?>
						
                ]				
			}
		
		]
	};	
	
    var vendor_combo_chart = 
    	new Chart($('#vendor_combo_chart'),
    			{
                	type: 'bar',
                	data: vendor_combo_data,
                	options: { 
                		scaleUse2Y: true, 
                		responsive: true,
						maintainAspectRatio: false,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									var prefix = data.full_labels[tooltipItem.index];
									var output = "";
	
									if (tooltipItem.datasetIndex == 0)
									{
									    if (prefix.length >= 10) return ['(' + prefix + ')', value];
									    else return value;
									}
									else
									{
									    if (parseInt(value) >= 1000){
									    	output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									    } else {
									    	output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
									    }

									    if (prefix.length >= 10) return ['(' + prefix + ')', output];
									    else return output;
									}
								}						
							}
						},
                		scales: { 
                			xAxes: [{ ticks: { maxRotation: 0, minRotation: 0 } }],
                			yAxes: [
                						{ 
                							id: 'Y2', type: 'linear', position: 'left', display: true,
											ticks: { 
		            							min: 0, 
												callback: function(value, index, values) {
	              									if (parseInt(value) >= 1000) {
	                									return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              									} else {
	                									return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              									}	            	
		            							}
		            						}                									 
                						}, 
                						{ id: 'Y1', type: 'linear', position: 'right', display: true, ticks: { min: 0, stepSize: 1 }, gridLines: { drawOnChartArea: false } }
                				] 
                		} 
                	}
            	}
        );
	
	
});	
</script>
<?php 
	
	$nonPreferAmount =  html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($nonPreferAmount*FunctionManager::currencyRate($tool_currency), 2);

?>
<!--START: Apex Chart Metrices -->
<script type="text/javascript">
	var spark7 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: "Spend",
            data: [<?php echo $spark7Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalVendorAmount, 2); ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Top 10 Suppliers Spend',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var spark7 = new ApexCharts(document.querySelector("#spark7"), spark7);
    spark7.render();

 var spark8 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: "Spend",
            data: [<?php echo $spark8Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: '<?php echo $totalVendorUsed; ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Total Suppliers Used',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var spark8 = new ApexCharts(document.querySelector("#spark8"), spark8);
    spark8.render();



  function apexMetriceSpend(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark9Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpend('spark9','Spend','<?php echo $nonPreferAmount ?>','Non Preferred Spend',<?php echo $spark9Exp;?>);

   // Need to capture all these in one function later

   function apexMetriceSpendSpark10(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark10Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark10('spark10','Count','<?php echo $nonPreferCount; ?>','Non Preferred Count',<?php echo $spark10Exp;?>);

 // Need to capture all these in one function later

   function apexMetriceSpendSpark11(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark11Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark11('spark11','Count','<?php echo $vendCatCount; ?>','Suppliers / Category',<?php echo $spark11Exp;?>);

   // Need to capture all these in one function later

   function apexMetriceSpendSpark12(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    dataLabels: {enabled: false},
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark12Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark12('spark12','Orders','<?php echo number_format($totalOrders, 0); ?>','Total Year Orders',<?php echo $spark12Exp;?>);
   
</script>
<!--END: Apex Chart Metrices -->


