<div class="right_col" role="main">
    <div class="row-fluid tile_count">
      <div class="col-lg-12 p-0">
       <?php  $alert = Yii::app()->user->getFlash('success');
       if(!empty($alert)){?>
            <div class="alert alert-success">
                <?php echo $alert; ?>
            </div>
        <?php } ?>
        <div class="span6">
          <div class="col-lg-8 p-0">
             <h3>Refresh QR Login Code</h3>
          </div>  
         
        </div>
        <div class="clearfix"> </div><br><br>
    </div>
    <div class="clearfix"></div>
  <div class="row tile_count">
      <div class="two-factor">
           <div class="col-lg-12 ">
            <p>Click below to refresh your two factor authentication QR code This will be available when you next log into oboloo.</p><br>
            <a href="<?php echo $this->createUrl('app/qrcode'); ?>" class="btn btn-gb"  onclick="return confirm('Confirm to refresh the QR code?');">Refresh QR Code</a>
          </div>
        </div>
  </div>
 </div>
</div>

<style>
.btn-gb{
    background: #1abb9c;
    background-color: #1abb9c;
    color: #ffff;
}
.btn-gb:hover{
    background: #1abb9c;
    background-color: #1abb9c;
    color: #ffff;
}
</style>