<div>
  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content" style="text-shadow:none;">
         
    <?php if (isset($success) && $success == 1) { ?>
        <div id="message_area" style="text-align: center; color: red;font-size: 20px;">
            Your password changed successfully.
        </div>  <br /><br />
    <?php } ?>

    <?php if (isset($error) && $error == 1) { ?>
        <div id="message_area" style="text-align: center; color: red;font-size: 20px;">
            No such account was not found. Please contact your administrator.
            <br /><br />
        </div>
    <?php } ?>  <br /><br />
    <a style="text-decoration: underline;font-size: 20px;" href="<?php echo AppUrl::bicesUrl('login'); ?>">Login Here</a>
      </section>
    </div>

    <div id="status_area">
    </div>
    <div class="clearfix"></div>


  </div>
</div>
