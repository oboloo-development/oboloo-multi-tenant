<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Review Data Before Uploading</h3><br />
        </div>
        <div class="clearfix"> </div>
    </div>
        <h1 class="alertdeleteRow"></h1> 
	  <table id="contract_table" class="table table-striped table-bordered contract-table" style="width: 100%;">
      <thead>
        <tr>
          <th style="width:5%"></th>
          <th style="width:14%">Contract Title</th>
          <th style="width:13% ">Reference</th>
          <th class="th-center" style="width: 10% ">Start Date</th>
          <th style="width:10% ">End Date</th>
          <th class="th-center" style="width: 7%">Notice Period Date</th>
          <th class="th-center" style="width: 7%">Description</th>
          <th class="th-center" style="width: 7%">Delete</th>
        </tr>
      </thead>

      <tbody>
       <?php 
       set_time_limit(0);
        error_reporting(0);
        $row = 1;
        $checkRecords = 0;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            // Start: Check Count
            while (($data = fgetcsv($handle)) != FALSE) {


               
                if($row>1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]))){
                    $checkRecords++;
                }
                $row++;
            }
        }
       
        if($checkRecords>100){
            Yii::app()->user->setFlash('error', '<strong>Warning!</strong> You are trying to import more than 100 records. Only 100 or less can be uploaded at one time');
            $this->redirect(array('app/import'));
        }

            //End: Check Count
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            $row = 1;
            $recordArr = array();
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                // echo '<pre>';
                // print_r($data);
                // exit;
 
                

                if($row>1 && !empty(Yii::app()->session['contract_import']) && !in_array($row,array_keys(Yii::app()->session['contract_import']))){
                   continue;

                }
                if($row>1 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]))){
                    $contract_title     = addslashes(trim($data[0]));
                   /* echo '<pre>';
                	print_r($data);
                    exit;*/
                    $contract_reference = addslashes(trim($data[1]));

                    $contract_start_date= trim($data[2]);
                    if(!empty($contract_start_date)){
                        $contract_start_date= str_replace('/', '-', $contract_start_date);
                        $contract_start_date= date('Y-m-d', strtotime($contract_start_date));
                    }

                    $contract_end_date  = trim($data[3]);
                    if(!empty($contract_end_date)){
                        $contract_end_date  = str_replace('/', '-', $contract_end_date);
                        $contract_end_date  = date('Y-m-d', strtotime($contract_end_date));
                    }

                    $break_clause       = trim($data[4]);
                    if(!empty($break_clause)){
                        $break_clause       = str_replace('/', '-', $break_clause);
                        $break_clause       = date('Y-m-d', strtotime($break_clause));
                    }

                    $description        = trim($data[5]);
                    // $contract_value     = trim($data[6]);
                    // // START Remove comma and currency symbol
                    // $contract_value = preg_replace('/[^0-9-.]+/', '', $contract_value);
                    // End Remove comma and currency symbol
                    // START Get dropdown ids  by name/value
                    $supplierID = $locationID = $departmentID = $projectID = $userID = $categoryID = $subcategoryReaderID = $contracttypeID = $contractstatusID =  $currencyID = '';
                    $createdDate        = date('Y-m-d H:i:s');
                   $userID = Yii::app()->session['user_id'];
                    if(!empty($userID)){
                        $userObj = new User();
                        $userReader= $userObj->getOne(array('user_id' => $userID));
                        if(isset($userReader) && !empty($userReader['user_id'])){
                            $userName = $userReader['full_name'];
                        }
                    }

          		?>

                <tr class="row_cont_<?php echo $row;?>">
                <td><?php echo $row; ?></td>
                <td><?php echo $contract_title; ?></td>
                <td><?php echo $contract_reference; ?></td>
                <td><?php echo $contract_start_date; ?></td>
                <td><?php echo $contract_end_date; ?></td>
                <td><?php echo $break_clause; ?></td>
                <td><?php echo $description; ?></td>
                <td><button type="submit" class="btn btn-danger btn-xs" onclick="deleteContractRow(<?php echo $row ;?>);">Delete</button></</td>
                </tr>
                <?php 

                $recordArr[$row] = array(
                            'contract_title'=>$contract_title,
                            'contract_reference'=>$contract_reference,
                            'contract_start_date'=>$contract_start_date,
                            'contract_end_date'=>$contract_end_date,
                            'break_clause'=>$break_clause,
                            'description'=>$description,
                            );
                 } 
                  $row++;}
                    if(!empty($recordArr)){
                       $recordArrTest =  Yii::app()->session['contract_import'] = $recordArr;
                    }
                  } ?>

          		

      </tbody>
    <
  </table>
  <div class="col-md-12 col-xs-12 " >
    <div class="col-md-3 col-xs-12 outer" >
    <div class="inner">
     <form action="<?php echo AppUrl::bicesUrl('app/import'); ?>" method="post" id="uploadContractData">
      <input type="hidden" name="contract_import" value="1">
      <a href="#" class="btn btn-info" onclick="uploadContractData();">Upload Selected Data</a>
     </form>
    </div>

     <div class="inner">
      <form action="<?php echo AppUrl::bicesUrl('app/import'); ?>" method="post" onsubmit="return confirm('Do you really want to submit the form?');">
       <input type="hidden"  name="contract_import_cancel" value="1">
       <button type="submit" name="submit" class="btn btn-danger">Cancel</button>
      </form> 
     </div>

    </div>
 </div>

</div>



 

</div>
<script type="text/javascript">
	 $(document).ready(function(){

    $('#contract_table').dataTable({
        "pageLength": 100,
        "columnDefs": [ 
        	{ "targets": 0 , "orderable": 100 },
			/*{ type: 'date-uk', targets: 7 },
			{ type: 'date-uk', targets: 8 },
			{ type: 'date-uk', targets: 9 },         */
			/*{ "type": "sort-month-year", targets: 5 }, */        
        ],
        "order": []
    });

	 });
     
     function deleteContractRow(row_number){
        var row_number = row_number;
        var result = confirm("Want to delete?");
        if (result) {
          $.ajax({
            url: '<?php echo $this->createUrl('app/deleteContractImport'); ?>',
            type: 'POST',
             data: {row_number:row_number},
            dataType: "json",
            success: function (data) {
                if(data.msg ==1){
                  $(".row_cont_"+row_number).hide();
                  $(".row_cont_"+row_number).empty();
                  $(".alertdeleteRow").html('<div class="alert alert-danger alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Contract </strong>Deleted Successfully.</div>');
                }
            },
            
        });
      }
     }

     function uploadContractData(){
      $.confirm({
        title: '',
        content: " Are you sure you want to upload the selected data? Once uploaded, records can only be archived or edited" +
        '<form action="<?php echo AppUrl::bicesUrl('app/import'); ?>" class="formName">' +
        '<input type="hidden" name="contract_import" value="1">'+
        '</form>',
        buttons: {
        formSubmit: {
        text: 'Upload Selected Data',
        btnClass: 'btn-blue',
        action: function () {
          $('#uploadContractData').submit();
        }
        },
        cancel: function () {
        //close
        },
        },
        onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
      });
     }
</script>

<style type="text/css">
	#form_sumit_btn{background-color: #1abb9c!important; border-color: #1abb9c!important; }
	.center { display: block; margin-left: auto; margin-right: auto; width: 30%;}
    .outer{  width:100%; margin-top: 10px; }
.inner{ display: inline-block;}
.jconfirm .jconfirm-box div.jconfirm-content-pane .jconfirm-content { overflow: hidden; text-align: center;line-height: normal; }
</style>