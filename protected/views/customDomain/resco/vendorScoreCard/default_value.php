<div class="right_col" role="main">

    <div class="row-fluid tile_count">
      <?php $alert=Yii::app()->user->getFlash('success');
          if(!empty($alert)) { ?>
          <div class="clearfix"></div>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $alert;?></div>
        <?php } ?>
        
        <div class="col-md-6 col-xs-12 p-0">
          <h3>Create New Supplier Scorecard</h3>
        </div>
        <div class="col-md-6 col-xs-12 ">
          <a href="<?php echo Yii::app()->createUrl('vendorScoreCard/list') ?>" class="btn btn-default pull-right" style="background-color: #F79820;color: #fff;border-color: #F79820;">Return To Supplier Scorecard List</a>
        </div>

        <div class="span6 pull-left">
            <h4 class="subheading"><br>Create a supplier scorecard tailored to your specific supplier evaluation requirements. Define criteria under each scorecard Area with weighted scoring. </h4>  <br /><br />
        </div>

        <div class="clearfix"> </div>
    </div>
    <div class="row">
<?php
	$data_types = array(		
		'supplier_scoring_criteria'  => 'Supplier Scoring Criteria',
	);
?>

<div class="col-md-7 col-sm-7 col-xs-12">
  <div class="tile_count" style="margin-top: -3px;">
    <form id="settings_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('vendorScoreCard/defaultValue'); ?>">
      <div class="form-group">
         <div class="col-md-10">
          <label>Score Card Title <span style="color:red;">*</span></label>
          <input type="text" class="form-control notranslate" name="scorecard" placeholder="Score Card Title" id="scorecard" required />
         </div>    
      </div>
      <div class="clearfix"><br /></div>
  	  <div id="datatype_values">
       <div class="form-group">
         <div class="col-md-10">
          <div class="row">
           <div class="col-md-9 ">
            <label style="font-size: 13px; padding-left: 5px;">Category/Area</label>
           </div>
           <div class="col-md-3 text-center">
            <label style="font-size: 13px;">Score (%)</label>
           </div>
          </div>     
        </div>    
        </div>
        <?php $disabled = FunctionManager::sandbox(); ?>
  	    <div class="form-group" id="data_row">
  	      <div class="col-md-10 col-sm-10 col-xs-10">
  				 <div style="float: left; width: 80%;">
  					<input type="text" class="form-control notranslate value_1" name="value[1]"  placeholder="Type New Area/Category" required />
  				 </div>
  				<div style="float: left;width: 2%">&nbsp;</div>
          <div style="float: left;width: 18%;">
           <input type="number" autocomplete="off" class="form-control text-center score score_1 notranslate" name="score[1]"  
            min="0" step="0.01" required />
          </div>
  	    </div>
  	    </div>
          <div class="add_question_row_1">
           <div class="mt7 col-md-7 col-sm-7 col-xs-7 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 scoreCardQuestion_1_1">
            <input type="text" autocomplete="off" class="form-control question_1" name="questions[1][1]" placeholder="Type New Criteria Here" required />
           </div>  
           <div class="col-md-10 col-sm-10 col-xs-10 add_question_more_1"></div>
           <div class="col-md-10 col-sm-10 col-xs-10">
            <button type="button" class="btn btn-primary pull-right mt7" onclick="addQuestion(1)">Add Criteria</button>
  	       </div>
          </div>
  	  </div>
      <div class="clearfix"> </div>
  	  
      <div class="form-group">
      <div class="clearfix"> <br /> </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
      <input type="hidden" class="notranslate" name="data_type_change" id="data_type_change" value="0" />
      </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-8 col-sm-6 col-xs-6">
          <div class="scoring_alert text-center" ></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
           <div class="scoring_percentage_alert text-center-md" style="margin-left: 27px;"></div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
      <?php if (isset($data_type) && !empty($data_type)) { ?>
          <div style="    margin-top: 15px;">
          <a style="cursor: pointer;" onclick="addRow(); return false;" class="btn btn-primary submit-btn">Add new Category/Area</a>
            <?php if($disabled == 'disabled') { ?>
            <a href="javascript:void(0);" <?php echo $disabled; ?>>
              <button type="button" class="btn btn-success " <?php echo $disabled; ?>>Save Data</button>
              <a style="display: none;" class="btn btn-success"  <?php echo $disabled; ?>> Save Data</a>
            </a>
          <?php } else { ?>
            <button type="submit" class="btn btn-success submit-btn1 score_btn">Save Data</button>
            <a style="display: none;" class="btn btn-success submit-btn1 second_btn" id="second_btn" > Save Data</a>
          <?php } ?>
          </div>
      <?php } ?>
      </div>
      </div>
    </form>
  </div>
</div>
</div>


<script type="text/javascript">
let scoreCard = 1;
scoreCardQuestion = 1;
function addRow()
{
  scoreCard += 1; 
  scoreCardQuestion += 1;
	$('#datatype_values').append(`
        <div class="clearfix"></div>
        <div class="scorecard_${scoreCard}">
  	    <div class="mt-32 form-group_${scoreCard} " id="data_row_${scoreCard}">
  	      <div class="col-md-10 col-sm-10 col-xs-8">
  				 <div style="float: left; width: 80%;">
  					<input type="text" class="form-control notranslate value_${scoreCard}" name="value[${scoreCard}]"  placeholder="Type New Area/Category" required />
  				 </div>
  				 <div style="float: left;width: 2%">&nbsp;</div>
           <div style="float: left;width: 18%;">
            <input type="number" autocomplete="off" class="form-control text-center score score_${scoreCard} notranslate" name="score[${scoreCard}]"  
             min="0" step="0.01" required />
          </div>
  	    </div>
        <div class="col-md-2 col-sm-2 col-xs-4">
         <button type="button" onclick="onDelete(${scoreCard})" class="btn btn-danger">Delete</button>
        </div>
  	    </div>
          <div class="add_question_row_${scoreCard}">
            <div class="mt7 col-md-7 col-sm-7 col-xs-7 col-md-offset-3 col-sm-offset-3 col-xs-offset-3  scoreCardQuestion_${scoreCardQuestion}_${scoreCard}">
             <input type="text" autocomplete="off" class="form-control question_1" name="questions[${scoreCard}][${scoreCardQuestion}]" placeholder="Type New Criteria Here" required />
            </div>  
           <div class="col-md-10 col-sm-10 col-xs-10 add_question_more_${scoreCard}"></div>
           <div class="col-md-10 col-sm-10 col-xs-10">
            <button type="button" class="btn btn-primary pull-right mt7" onclick="addQuestion(${scoreCard})">Add Criteria</button>
  	       </div>
        </div>
      </div>
  `);

  equalizeSideBarWithBody();
}

function addQuestion(parentID){
  scoreCardQuestion += 1; 
  console.log(scoreCardQuestion);
  $('.add_question_more_'+parentID).append(`
    <div style="float: right; width: 80%; display: flex;" class="mt7 scoreCardQuestion_${scoreCardQuestion}_${parentID}">
     <button type="button" class="btn btn-danger" onclick="questionDelete(${scoreCardQuestion}, ${parentID});">Delete</button>
     <input type="text" autocomplete="off" class="form-control question_${parentID}" name="questions[${parentID}][${scoreCardQuestion}]" placeholder="Type New Criteria Here" required />
    </div>  
  `);
 equalizeSideBarWithBody();
}

$("#new_row_code").hide();
function onDelete(scorecardID){
  if (confirm('Are you sure you want to delete this row? This action cannot be reversed.')) {
    $(`.scorecard_${scorecardID}`).remove();
    loadScoring();
  }
}

function questionDelete(scorecardQuesID, parentID){
  if (confirm('Are you sure you want to delete this question? This action cannot be reversed.')) {
    $(`.scoreCardQuestion_${scorecardQuesID}_${parentID}`).remove();
  }
}

function loadScoring(){
	var sum = 0;
	$(".score").each(function(){ 
		sum += parseFloat($(this).val());
		if(sum > 100 || sum < 100){
            $(".second_btn").show();
			$('.second_btn').attr('disabled','disabled');
            $('.second_btn').click( function( e ) {
            e.preventDefault();
            return false;
    });
            $(".score_btn").hide();
			$(".scoring_alert").html('<strong style="color:red">Default Values must add up to 100% </strong>');

		}else {
			$('.score_btn').removeAttr('disabled','disabled');
            $(".score_btn").show();
             $(".second_btn").hide();
			$(".scoring_alert").html('');
		}
	});

  $(".scoring_percentage_alert").html('<strong>'+sum+'%/100%</strong>');
}


$(document).on('keyup', ".score", function () {
  loadScoring();
});

</script>
  <?php 
    if (FunctionManager::checkEnvironment(true)) { ?>
      <script type="text/javascript">
        $('.score_btn').removeAttr('disabled','disabled');
        $(".scoring_alert").html('');
     </script>
   <?php } else if(FunctionManager::checkEnvironment(false)){ ?>
    <script type="text/javascript">
      loadScoring();
    </script>
  <?php } ?>

