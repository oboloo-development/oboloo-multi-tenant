<div class="right_col" role="main">
    <div class="row-fluid tile_count">
      <div class="col-md-6 col-xs-12 p-0">
         <h3 class="mt-15">Supplier Scorecard</h3>
      </div>
      <div class="col-md-6 col-xs-12">
        <a href="<?php echo Yii::app()->createUrl('vendorScoreCard/defaultValue') ?>" class="btn green-btn pull-right">Create New Supplier Scorecard</a>
      </div>
      <div class="clearfix"><br/></div> 
      <div class="row">
      <?php $quoteCreated=Yii::app()->user->getFlash('success');
      if(!empty($quoteCreated)) { ?>
      <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $quoteCreated;?></div>
    <?php } ?>
    <div class="alert create-alert-success alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Create Updated successfully.</div>
      <div class="col-md-12">
      <form class="form-horizontal mt-26" id="contract_list_form" name="contract_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('vendorScoreCard/list'); ?>">
			<div class="col-md-4 col-sm-4 col-xs-12 pl-0" >
				<div class="form-group">
					<select name="user_id" class="form-control select_users" id="user_id"
						class="form-control">
						<option value="">All User</option>
          	  	  <?php 
                  $query = "select * from users where user_id not in (2,3) order by full_name asc ";
                  $usersName = Yii::app()->db->createCommand($query)->queryAll();
          	  	  foreach ($usersName as $key=>$user) { ?>
          	  	  		<option value="<?= $user['user_id']; ?>"
							    <?php if (isset($_POST['user_id']) && $_POST['user_id'] == $user['user_id']) echo ' selected="SELECTED" '; ?>>
          	  	  			<?= $user['full_name']; ?>
          	  	  		</option>
          	  	  <?php } ?>
          	  </select>
				</div>
			</div>
      <div class="col-md-4 col-sm-4 col-xs-12 pl-0" >
				<div class="form-group">
					<select name="archive_status" id="archive_status" class="form-control">
            <option value="">Archived Status</option>
            <option value="archive">Archived</option>
            <option value="unarchive">Active</option>
          </select>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 search-contract text-left" style="padding: 0px;">
				<button class="btn btn-info" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
				<button class="btn btn-primary " onclick="loadQuestionnaires(); return false;" style="margin-right: -7px">Apply filters</button>
			</div>
			
		</form>
		<div class="clearfix"></div>
      <table id="scorecard_table" class="table table-striped table-bordered suppliers-table" style="width: 100%">
       <thead>
        <tr>
          <th style="width: 6%;" tabindex="-1" class="nosort"> </th>
          <th style="width: 15%;">Title</th>
          <th style="width: 15%;">Score (%)</th>
          <th style="width: 15%;">Creator</th>
          <th style="width: 15%;">Created Date</th>
          <th style="width: 15%;">Last Updated At</th>
          <th style="width: 15%;">Action</th>
        </tr>
       </thead>
          <tbody>
          </tbody>
        </table>
        </div>
        </div>
    </div>
</div>

<script>
  $(document).ready(function() {
    $('.create-alert-success').hide();
    $('#add_new_scorecard').submit(function(e) {
        e.preventDefault(); // Prevent the default form submission
      
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('vendorScoreCard/create/'); ?>",
            data: $(this).serialize(), // Serialize form data
            success: function(response) {
              let status = JSON.parse(response);
                if(status.status == 1 || status.status  == "1"){
                  $('#myModalScorecard').modal('hide');
                  $('#scorecard_table').DataTable().ajax.reload();
                  $('.value').val("");
                  $('.score').val("");
                  $('.create-alert-success').show();
                }
            },
            error: function(error) {
             console.error('Error:', error);
             alert('An error occurred while submitting the form.');
            }
        });
    });
});

  function archiveVendScoreCard(key, status, archive) {
   var key = key;
   var status = status;
   var archiveStatus = archive == 1 ? 'archive': 'unarchive';
   if(status === 'yes'){
    $.confirm({
      title: false,
      content: '<spam style="font-size:11px">Are you sure you want to '+archiveStatus+' the Scorecard?</span>',
      buttons: {
        Yes : {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              dataType: 'json',
              type: "post",
              url: "<?php echo AppUrl::bicesUrl('vendorScoreCard/delete/'); ?>",
              data: {
                id: key, status :status,archive:archive
              },
              success: function(option) {
                if(option.uploaded_response == 1 && status === 'yes'){
                  $(".delete_form_row_"+key).remove();
                  $('#scorecard_table').DataTable().ajax.reload();
                }
              }
            });
          }
        },
        No: {
          text: "Close",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });
   }else{
    $.confirm({
      title: false,
      content: '<spam style="font-size:11px">You do not have permission to delete this Scorecard. Only the person who created the Scorecard or a super user can delete Scorecard</span>',
      buttons: {
        No: {
          text: "Close",
          btnClass: 'btn-red',
          action: function() {
            $('#scorecard_table').DataTable().ajax.reload();
          }
        },
      }
    });
   }
   $("#file_upload_alert").delay(1000 * 5).fadeOut();
  }

  $(document).ready( function () {
    var table = $('#scorecard_table').DataTable({
        "columnDefs": [{
         "targets": [1],
        }],
        "createdRow": function(row, data, index) {
          if (data[0].indexOf('glyphicon glyphicon-ok') >= 0)
            for (var i = 1; i <= 6; i++)
              $('td', row).eq(i).css('text-decoration', 'line-through');
        },
        "order": [
          [1, "asc"]
        ],
        "pageLength": 50,
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?php echo AppUrl::bicesUrl('vendorScoreCard/listAjax'); ?>",
          "type": "POST",
          data: function(input_data) {
            input_data.user_id = $('#user_id').val();
            input_data.archive_status = $('#archive_status').val();
          }
        },
        "oLanguage": {
          "sProcessing": "<h1>Please wait ... retrieving data</h1>"
        },
        "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          if (aData[1].search("<span class='deactivated_vendor'></span>") > 0) {
            $(nRow).addClass('deactivated_record');
          }
        }
      });
  });

  function clearFilter() {
    $("option:selected").removeAttr("selected");
    $('.select_location_multiple').trigger("change")
    $('#scorecard_table').DataTable().ajax.reload();
  }

  function loadQuestionnaires()
  {
    $('#scorecard_table').DataTable().ajax.reload();
  }

  function btnEdit(id){
      var recordId = id;
      $.ajax({
          type: "POST",
          url: "<?php echo AppUrl::bicesUrl('vendorScoreCard/editForm'); ?>",
          data: {recordId: recordId},
          success: function(response) {
              // Inject the modal markup into the body and display the modal
              $('body').append(response);
              $('#editModal_'+recordId).modal('show');
          },complete: function (data) {
            
          }
      });
    
  }
</script>



      
   

