    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog" style="width: 1000px;min-width: 700px; ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Scorecard</h4>
                </div>

                 <form method="post" id="create_scorecard_modal_form" action="<?php echo AppUrl::bicesUrl('vendorScoreCard/createVendorScoreCard'); ?>" autocomplete="off" >
                    <div class="modal-body">
                     <input type="hidden" name="vendor_id" value="<?= $vendor_id; ?>">
                     <div class="form-group">
                      <div class="col-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">Scorecard Name <span style="color: #a94442;">*</span></label>
                        <select name="scorecard_id[]" id="scorecard_list_id" class="form-control notranslate scorecard_list_id" onchange="selectScoreCardTable($(this).val())" required>
                         <option value="">Select</option>
                         <?php 
                         $scoreCardName = new VendorScoringName();
                         $scoreCardName = $scoreCardName->getAll(array('status' => 0, 'order' => 'id asc'));
                         foreach ($scoreCardName as $cardName) { ?>
                         <option value="<?= $cardName['id'] ?>"><?= $cardName['name'] ?></option>
                         <?php } ?>
                        </select>
                        <span class="error-scorecard_list" style="color: red;"></span>
                      </div>
                        <div class="clearfix"><br /></div>
                        <div class="scorecard_more_id"></div>
                        <div class="clearfix"><br /></div>
                     </div>
                     <div class="col-lg-12 col-md-12 col-xs-12">
                      <div class="scorecard_table"></div>
                     </div>

                    </div>
                    <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     <button type="button" class="btn btn-primary" id="submitFormButton">Save changes</button>
                    </div>
                 </form>

            </div>
        </div>
    </div>

<script>
 $(document).ready(function() {
    $('#submitFormButton').click(function() {
        var valid = true; // Initialize as true
        
        if($('.scorecard_list_id').val() ==""){
            valid = false;
            $('.error-scorecard_list').text('This field is required.');
        }else{
            $('.error-scorecard_list').text('');
        }

        $('.scores-validation-question').each(function() {
            var inputValue = $(this).val();
            var errorMessage = $(this).siblings('.error-message');
            
            if (inputValue === '' || inputValue === '0') {
                $(this).prop('required', true);
                $(this).css('border', '5px solid');
                errorMessage.text('This option is required.');
                valid = false; // Set to false if any element doesn't meet the validation criteria
            } else {
                $(this).css('border', '1px');
                errorMessage.text('');
            }
        });

        // If you check 'valid' here, it will correctly reflect the overall validity of all elements
        if (valid) {
            $('#create_scorecard_modal_form').submit();
        }
    });
 });
</script>