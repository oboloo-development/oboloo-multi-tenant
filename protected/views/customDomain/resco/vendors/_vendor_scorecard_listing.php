<?php

$allScorecards = [];
if (!empty($scoreLists)) {
    foreach ($scoreLists as $scorecdID) {
        $scorecard = new VendorScoreCard();
        $scorecardDetail = $scorecard->getOne(['id' => $scorecdID['vendor_score_card_id'], 'vendor_id' => $vendor_id]);
    
        $vendScoreCardSelectEval = new  VendorScoreCardSelectedEvaluation();
        $vendScoreCardSelectEval = $vendScoreCardSelectEval->getAll(['scorecard_id' => $scorecardDetail['scorecard_id'], 'vendor_score_card_id' => $scorecardDetail['id'], 'vendor_id' => $vendor_id, 'order' => 'id asc']);

        $getQuestion = [];
        foreach ($vendScoreCardSelectEval as $list) {
            $sql = "select * from vend_scard_selected_question_evaluation  
                        where vend_scorecard_select_eval_id=" . $list['id'] . " and  scorecard_id=" . $scorecardDetail['scorecard_id'] . " and vendor_id=" . $vendor_id . " order by id asc ";
            $getQuestion[] = Yii::app()->db->createCommand($sql)->queryAll();
        }

        $allScorecards[] = [
            'name' => $scorecardDetail['name'],
            'card_id' =>  $scorecardDetail['id'],
            'scoreCardLists' => $vendScoreCardSelectEval,
            'getQuestion' => $getQuestion,
            'vendor_id' => $_POST['vendor_id']
        ];
    }

        foreach ($allScorecards as $key => $scorecard) {    
            $totalScore = 0;
            $totalWtdRating = 0;
            foreach ($scorecard['scoreCardLists'] as $key => $list) {
                $totalScore += $list['default_score'];
                $scores = [];
                
                foreach ($scorecard['getQuestion'][$key] as $getQuestion) {
                    $scores[] = (float)$getQuestion['score_evaluation'];
                }
                        
                $areaRatingAvg = array_sum($scores) / count($scores);
                if (!empty($scores)) {
                    $wtdRating = ($areaRatingAvg * $list['default_score']) / 100;
                    $areaRatingAvgTT += $wtdRating;
                }    
            }   
        }

    foreach ($allScorecards as $key => $scorecard) { ?>
        <h3 class="page-title heading-title"><?= $scorecard['name'] ?></h3>
        <table class="table table-bordered table-striped table-responsive">
            <thead>
                <th>Area</th>
                <th>Scorecard Criteria</th>
                <th class="text-center">Rating Scale (1-5)</th>
                <th>Comments</th>
                <th class="text-center">Area Weight</th>
                <th class="text-center">Area Rating</th>
                <th class="text-center">Wtd. Rating</th>
            </thead>
            <tbody>
                <?php
                $totalScore = 0;
                $totalWtdRating = 0;
                foreach ($scorecard['scoreCardLists'] as $key => $list) {
                 $totalScore += $list['default_score'];
                 $scores = [];
                ?>
                    <tr>
                        <td class="ver-center" style="vertical-align: middle;">
                            <p class="scoreTitle"><?= $list['category_area']; ?></p>
                        </td>
                        <td>
                            <?php // Check if there are corresponding questions
                            if (!empty($scorecard['getQuestion'][$key])) {
                                foreach ($scorecard['getQuestion'][$key] as $getQuestion) { ?>
                                 <p><?= $getQuestion['question'] ?></p>
                            <?php }
                            }
                            ?>
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            <?php // Check if there are corresponding questions score
                            if (!empty($scorecard['getQuestion'][$key])) {
                                foreach ($scorecard['getQuestion'][$key] as $getQuestion) {
                                  $scores[] = (float)$getQuestion['score_evaluation']; ?>
                                  <p><b><?= $getQuestion['score_evaluation'] ?></b></p>
                            <?php }
                            } ?>
                        </td>
                        <td>
                            <?php // Check if there are corresponding questions notes
                            if (!empty($scorecard['getQuestion'][$key])) {
                                foreach ($scorecard['getQuestion'][$key] as $getQuestion) {?>
                                  <p><?= $getQuestion['question_notes'] ?></p>
                            <?php }
                            } ?>
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            <p class="scoreTitle "><?= number_format($list['default_score']) ?>%</p>
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            <?php $areaRatingAvg = array_sum($scores) / count($scores);
                            if (!empty($scores)) {
                                echo '<p class="scoreTitle">' . number_format($areaRatingAvg, 2) . '</p>';
                            } else {
                                echo '<p class="scoreTitle">0</p>';
                            }
                            ?>
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            <?php
                            if (!empty($scores)) {
                                $wtdRating = ($areaRatingAvg * $list['default_score']) / 100;
                                $totalWtdRating += $wtdRating;
                                echo '<p class="scoreTitle">' . number_format($wtdRating, 4) . '</p>';
                            } else {
                                echo '<p class="scoreTitle">0</p>';
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
                <!-- STARTS: The Below tr for last Totals -->
                <tr>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td>
                  <p class="scoreTitle text-center"><?= number_format($totalScore) ?>%</p>
                 </td>
                 <td>
                  <p class="scoreTitle text-center"></p>
                 </td>
                 <td>
                  <p class="scoreTitle text-center"><?= number_format($totalWtdRating,2) ?></p>
                 </td>
                </tr>
                <!-- END: The Below tr for last Totals -->
            </tbody>
        </table>
<?php
    }
} ?>