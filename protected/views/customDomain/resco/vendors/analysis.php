<?php
	$category_labels = $preferred_data = $non_preferred_data = "";
	foreach ($vendors_by_category as $category_index => $data_points)
	{
		$current_category_name = "";
		$preferred = $non_preferred = 0;
		
		foreach ($data_points as $data_point)
		{
			$current_category_name = $data_point['category'];
			if ($data_point['preferred_flag']) $preferred += round($data_point['total'], 2);
			else $non_preferred += round($data_point['total'], 2);
		}

		if ($category_labels === "") $category_labels = "'" . $current_category_name . "'";
		else $category_labels = $category_labels . ", '" . $current_category_name . "'";

		if ($preferred_data === "") $preferred_data = $preferred;
		else $preferred_data = $preferred_data . ',' . $preferred;

		if ($non_preferred_data === "") $non_preferred_data = $non_preferred;
		else $non_preferred_data = $non_preferred_data . ',' . $non_preferred;
	}
	
	$categories_by_month = array();
	foreach ($vendors_by_month as $month_index => $month_data)
		foreach ($month_data as $category_key => $category_data)
			$categories_by_month[$category_key] = array('name' => $category_data['category_name'], 'data' => array());
	
	$month_labels = "";
	foreach ($vendors_by_month as $month_index => $month_data)
	{
		$current_month_label = date("M Y", strtotime($month_index . '01'));
		if ($month_labels === "") $month_labels = "'" . $current_month_label . "'";
		else $month_labels = $month_labels . ", '" . $current_month_label . "'";
		
		foreach ($categories_by_month as $category_key => $category_data)
		{
			$category_data_found = 0;
			foreach ($month_data as $category_index => $month_category_data)
			{
				if ($category_key == $category_index)
				{
					$category_data_found = $month_category_data['vendor_count'];
					break;
				}
			}
			$categories_by_month[$category_key]['data'][] = $category_data_found;
		}
	}
		
?>
          <div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Supplier Analysis</h3>
        </div>
        <div class="clearfix"> </div>
    </div>


          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Top 10 Supplier Spend</span>
              <div class="count"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['vendor']['top_vendors_total'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-list"></i> Total Suppliers Used</span>
              <div class="count"><?php echo number_format($metrics['vendor']['vendors_count'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Non Preferred Spend</span>
              <div class="count"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['vendor']['non_preferred_total'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-list"></i> Non Preferred Count</span>
              <div class="count"><?php echo number_format($metrics['vendor']['non_preferred_count'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-users"></i> Avg Suppliers / Category</span>
              <div class="count"><?php echo number_format($metrics['vendor']['avg_per_category'], 2); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-tags"></i> Total Year Orders</span>
              <div class="count"><?php echo number_format($metrics['spend']['spend_count'], 0); ?></div>
            </div>
          </div>



		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-24">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Categories Spend with Supplier Count</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('category_vendors_combo');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="category_vendors_combo" width="960px"></canvas>
			              	</div>
			           	</div>
			        	</div>
				</div>
				
				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_7">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(7);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('top_vendors');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content" style="text-align: center;">
		                   		<canvas id="top_vendors" height="170px"></canvas>
	                        </div>
	                      </div>
	                   </div>

						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_7">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(7);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('top_vendors');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
			            		<table class="tile_info">
			            			<?php
			            				$idx = 0;
			            				foreach ($top_vendors as $dataset_idx => $a_dataset)
										{
											if ($dataset_idx === 'total') continue;
											$idx += 1;
											switch ($idx)
											{
												case 1  : $color = 'blue'; break;
												case 2  : $color = 'purple'; break;
												case 3  : $color = 'green'; break;
												case 4  : $color = 'turquoise'; break;
												case 5  : $color = 'red'; break;
												case 6  : $color = 'antique-white'; break;
												case 7  : $color = 'aqua-marine'; break;
												case 8  : $color = 'bisque'; break;
												case 9  : $color = 'khaki'; break;
												default : $color = 'salmon'; break;
											}
			            			?>
			                          		<tr>
			                            		<td>
			                              			<i class="fa fa-square <?php echo $color; ?>"></i>
			                              			<?php echo $a_dataset['dim_1']; ?> - <?php echo number_format($a_dataset['percent'], 2) . '%'; ?>
			                            		</td>
			                          		</tr>
			                        <?php 
			                        	}
			                        ?>
			                        </table>
			              	</div>
			           	</div>
			        	</div>

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_8">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Categories By Preferred vs. Non-Preferred Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('vendors_by_category');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content" style="text-align: center;">
		                   		<canvas id="vendors_by_category" height="170px"></canvas>
	                        </div>
	                      </div>
	                   </div>
		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Suppliers Used By Month And Category</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="vendors_by_month" height="170px"></canvas>
			              	</div>
			           	</div>
			        	</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Supplier Spend with Order Count</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('vendor_combo_chart');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="vendor_combo_chart" height="170px"></canvas>
			              	</div>
			           	</div>
			        	</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

</div>

<script type="text/javascript">
$(document).ready(function() {

	var monthly_chart_settings = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ <?php echo $month_labels; ?> ],
			datasets: [ 

					<?php
						$i = 1; 	
						foreach ($categories_by_month as $category_key => $category_data)
						{
							if ($i == 1) $backgroundColor = "rgba(220,220,220,0.5)";
							else if ($i == 2) $backgroundColor = "rgba(151,220,187,0.5)";
							else if ($i == 3) $backgroundColor = "rgba(187,220,151,0.5)";
							else if ($i == 4) $backgroundColor = "rgba(220,151,187,0.5)";
							else if ($i == 5) $backgroundColor = "rgba(220,187,151,0.5)";
							else if ($i == 6) $backgroundColor = "rgba(187,151,220,0.5)";
							else if ($i == 7) $backgroundColor = "rgba(120,151,187,0.5)";
							else if ($i == 8) $backgroundColor = "rgba(151,120,187,0.5)";
							else if ($i == 9) $backgroundColor = "rgba(187,120,151,0.5)";
							else if ($i == 10) $backgroundColor = "rgba(151,187,120,0.5)";
							else $backgroundColor = "rgba(151,187,205,0.5)";
					?>
							{
								label: '<?php echo addslashes($category_data['name']); ?>',
								backgroundColor: '<?php echo $backgroundColor; ?>',
								data: [
									
									<?php
										$d = 1;
										foreach ($category_data['data'] as $category_data_point)
										{
											echo $category_data_point;
											if ($d != count($category_data['data'])) echo ',';
											$d += 1;
										}
									?>
									
								]
							
							} <?php if ($i != count($categories_by_month)) echo ','; ?>
					
					<?php
							$i += 1; 
						}
					?>
				]
		},
		options: { 
			responsive: true,
            scales: {
            	xAxes: [{ stacked: true, ticks: { autoSkip: false, maxRotation: 0, minRotation: 0 } }],
            	yAxes: [{ stacked: true }]
            }			 
		}
	};
	
	var month_chart = new Chart($('#vendors_by_month'), monthly_chart_settings);


	var chart_settings = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ <?php echo $category_labels; ?> ],
			datasets: [ 
					{ label: 'Preferred Suppliers', backgroundColor: "rgba(220,220,220,0.5)", data: [ <?php echo $preferred_data; ?> ] }, 
					{ label: 'Non Preferred Suppliers', backgroundColor: "rgba(151,187,205,0.5)", data: [ <?php echo $non_preferred_data; ?> ] } 
				]
		},
		options: { 
			responsive: true,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						var prefix = "";
						if (tooltipItem.datasetIndex == 0) prefix = 'Preferred'; else prefix = 'Non-Preferred';
						
					    if (parseInt(value) >= 1000){
					    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					    } else {
					    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
					    }
					}						
				}
			},
            scales: {
            	xAxes: [{ stacked: true, ticks: { autoSkip: false, maxRotation: 0, minRotation: 0 } }],
            	yAxes: [{ stacked: true, 
						  ticks: { 
		            				min: 0, 
									callback: function(value, index, values) {
	              						if (parseInt(value) >= 1000) {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              						} else {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              						}	            	
		            				}
		            		}                									 
	            		}]
            }			 
		}
	};
	
	var chart = new Chart($('#vendors_by_category'), chart_settings);


	var category_combo_data = {
		labels: [
			<?php $c = 1; foreach ($category_vendors_combo as $category_combo_row) { ?>
				
				'<?php echo addslashes($category_combo_row['category']); ?>' <?php if ($c != count($category_vendors_combo)) echo ','; ?> 
				
			<?php $c += 1; } ?>
		],
		
		datasets: [
		
			{
                type: 'line',
                label: 'Supplier Count',
                borderColor: 'purple',
                borderWidth: 2,
                yAxisID: 'A',
                fill: false,
                data: [

						<?php $c = 1; foreach ($category_vendors_combo as $category_combo_row) { ?>
							
							<?php echo $category_combo_row['vendor_count']; ?> 
							<?php if ($c != count($category_vendors_combo)) echo ','; ?> 
							
						<?php $c += 1; } ?>

                ]				
			},
			
			{
                type: 'bar',
                label: 'Amount Spent',
                backgroundColor: 'lightgreen',
                yAxisID: 'B',
                data: [
                
						<?php $c = 1; foreach ($category_vendors_combo as $category_combo_row) { ?>
							
							<?php echo round($category_combo_row['vendor_total'], 2); ?> 
							<?php if ($c != count($category_vendors_combo)) echo ','; ?> 
							
						<?php $c += 1; } ?>
						
                ]				
			}
		
		]
	};	
	
    var category_combo_chart = 
    	new Chart($('#category_vendors_combo'), 
    			{
                	type: 'bar',
                	data: category_combo_data,
                	options: { 
                		scaleUse2Y: true, 
                		responsive: true, 
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									if (tooltipItem.datasetIndex == 0) return value;
									else
									{
									    if (parseInt(value) >= 1000){
									    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									    } else {
									    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
									    }
									}
								}						
							}
						},
                		scales: { xAxes: [{ ticks: { maxRotation: 0, minRotation: 0 } }],
                					yAxes: [
                								{ 
                									id: 'B', type: 'linear', position: 'left',
													ticks: { 
		            									min: 0, 
														callback: function(value, index, values) {
	              											if (parseInt(value) >= 1000) {
	                											return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              											} else {
	                											return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              											}	            	
		            						   			}
		            						   		}                									 
                								}, 
                								{ id: 'A', type: 'linear', position: 'right', ticks: { min: 0, stepSize: 1 } }
                							] 
 						} 
 					}
            	}
        );


	var vendor_combo_data = {
		labels: [
			<?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>
				
				'<?php echo addslashes($vendor_combo_row['vendor_name']); ?>' <?php if ($c != count($vendors_combo)) echo ','; ?> 
				
			<?php $c += 1; } ?>
		],
		
		datasets: [
		
			{
                type: 'line',
                label: 'Supplier Count',
                borderColor: 'purple',
                borderWidth: 2,
                fill: false,
                yAxisID: 'Y1',
                data: [

						<?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>
							
							<?php echo $vendor_combo_row['count']; ?> 
							<?php if ($c != count($vendors_combo)) echo ','; ?> 
							
						<?php $c += 1; } ?>

                ]				
			},
			
			{
                type: 'bar',
                label: 'Amount Spent',
                backgroundColor: 'lightgreen',
                fill: false,
                yAxisID: 'Y2',
                data: [
                
						<?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>
							
							<?php echo round($vendor_combo_row['total'], 2); ?> 
							<?php if ($c != count($vendors_combo)) echo ','; ?> 
							
						<?php $c += 1; } ?>
						
                ]				
			}
		
		]
	};	
	
    var vendor_combo_chart = 
    	new Chart($('#vendor_combo_chart'), 
    			{
                	type: 'bar',
                	data: vendor_combo_data,
                	options: { 
                		scaleUse2Y: true, 
                		responsive: true, 
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									if (tooltipItem.datasetIndex == 0) return value;
									else
									{
									    if (parseInt(value) >= 1000){
									    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									    } else {
									    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
									    }
									}
								}						
							}
						},
                		scales: { 
                			xAxes: [{ ticks: { maxRotation: 0, minRotation: 0 } }],
                			yAxes: [
                						{ 
                							id: 'Y2', type: 'linear', position: 'left', display: true,
											ticks: { 
		            							min: 0, 
												callback: function(value, index, values) {
	              									if (parseInt(value) >= 1000) {
	                									return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              									} else {
	                									return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              									}	            	
		            							}
		            						}                									 
                						}, 
                						{ id: 'Y1', type: 'linear', position: 'right', display: true, ticks: { min: 0, stepSize: 1 }, gridLines: { drawOnChartArea: false } }
                				] 
                		} 
                	}
            	}
        );
	
	var chart_labels = [];
	var chart_data = [];
	var chart_colors = [];
	
	<?php
		for ($j=1; $j<=7; $j++)
		{
			if ($j != 5) continue;
			
			echo 'chart_labels[' . $j . '] = [];';
			echo 'chart_data[' . $j . '] = [];';
			echo 'chart_colors[' . $j . '] = [];';
			
			$canvas_id = '';
			$dataset = array();

			if ($j == 1) 
			{
				$canvas_id = 'year_to_date';
				$dataset = $current_year;
			}
			else if ($j == 2) 
			{
				$canvas_id = 'spend_vendors';
				$dataset = $top_vendors;
			}
			else if ($j == 3) 
			{
				$canvas_id = 'spend_departments';
				$dataset = $top_departments;
			}
			else if ($j == 4) 
			{
				$canvas_id = 'spend_locations';
				$dataset = $top_locations;
			}
			else if ($j == 5) 
			{
				$canvas_id = 'top_vendors';
				$dataset = $top_vendors;
			}
			else if ($j == 6) 
			{
				$canvas_id = 'year_over_year';
				$dataset = $year_over_year;
			}
			else if ($j == 7) 
			{
				$canvas_id = 'spend_contracts';
				$dataset = $top_contracts;
			}
			
			$i = 0; 
			foreach ($dataset as $idx => $a_data_row) 
			{
				if ($idx === 'total') continue; 
	?>

				chart_labels[<?php echo $j; ?>][<?php echo $i; ?>] = "<?php echo $a_data_row['dim_1']; ?>";
				chart_data[<?php echo $j; ?>][<?php echo $i; ?>] = <?php echo round($a_data_row['total'], 2); ?>;

	<?php 
				$i += 1; 
	
				$color = '';
				switch ($i)
				{
					case 1  : $color = '#3498DB'; break;
					case 2  : $color = '#9B59B6'; break;
					case 3  : $color = '#1ABB9C'; break;
					case 4  : $color = '#00CED1'; break;
					case 5  : $color = '#E74C3C'; break;
					case 6  : $color = '#FAEBD7'; break;
					case 7  : $color = '#7FFFD4'; break;
					case 8  : $color = '#FFE4C4'; break;
					case 9  : $color = '#BDB76B'; break;
					default : $color = '#FFA07A'; break;
				}
				
				echo 'chart_colors[' . $j . '][' . ($i - 1) . '] = "' . $color . '";';
	
			}
	?>
	
			createChart('<?php echo $canvas_id; ?>', chart_data[<?php echo $j; ?>], chart_labels[<?php echo $j; ?>], chart_colors[<?php echo $j; ?>]);
	<?php 
	 
		}
	?>
	
	
});

function createChart(div_id, data_values, data_labels, data_colors)
{
	var chart_type = 'doughnut';
	if (div_id == 'spend_departments') chart_type = 'pie';
	if (div_id == 'year_to_date') chart_type = 'line';
	if (div_id == 'spend_locations') chart_type = 'bar';
	if (div_id == 'year_over_year') chart_type = 'bar';
	if (div_id == 'spend_contracts') chart_type = 'horizontalBar';
	
	var chart_settings = false;
	
	if (chart_type == 'line')
	{
		var yearly_data_1 = new Array();
		var yearly_data_2 = new Array();
		var yearly_data_3 = new Array();
		
		for (var i=0; i<=11; i++) yearly_data_1[i] = data_values[i];
		for (var i=12; i<=23; i++) yearly_data_2[i-12] = data_values[i];
		for (var i=24; i<=35; i++) yearly_data_3[i-24] = data_values[i];
		
		chart_settings = {
			type: chart_type,
			tooltipFillColor: "rgba(51, 51, 51, 0.55)",
			data: {
				labels: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
				datasets: [
							{ label: "<?php echo date("Y") - 2; ?>", data: yearly_data_1, fill: false, lineTension: 0, borderColor: 'orange', backgroundColor: 'orange' },
							{ label: "<?php echo date("Y") - 1; ?>", data: yearly_data_2, fill: false, lineTension: 0, borderColor: 'aqua', backgroundColor: 'aqua' },
							{ label: "<?php echo date("Y"); ?>", data: yearly_data_3, fill: false, lineTension: 0, borderColor: 'purple', backgroundColor: 'purple' }
				]
			},
			options: { 
				legend: false, 
				responsive: true, 
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						    if (parseInt(value) >= 1000){
						    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						    } else {
						    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
						    }
						}						
					}
				},
	            scales: { yAxes: [{ ticks: { 
	            								min: 0, 
												callback: function(value, index, values) {
              										if (parseInt(value) >= 1000) {
                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              										} else {
                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
              										}
	            						   		}
	            						   } 
	            				 }] 
	            	}
			}
		};
	}
	else
	{
		if (div_id == 'spend_contracts')
			chart_settings = {
				type: chart_type,
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: data_labels,
					datasets: [{
						data: data_values,
						backgroundColor: data_colors
					}]
				},
				options: { 
					legend: false, 
					responsive: true,
					tooltips: {
						callbacks: {
							label: function(tooltipItem, data) {
								var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							    if (parseInt(value) >= 1000){
							    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							    } else {
							    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
							    }
							}						
						}
					},
		            scales: { xAxes: [{ ticks: { 
		            								min: 0,
													callback: function(value, index, values) {
	              										if (parseInt(value) >= 1000) {
	                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              										} else {
	                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              										}	            	
		            						   		}
		            						   } 
		            				 }] 
		            	}
				}
			};
		else
		{
			if (chart_type == 'doughnut')
				chart_settings = {
					type: chart_type,
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: data_labels,
						datasets: [{
							data: data_values,
							backgroundColor: data_colors
						}]
					},
					options: { 
						legend: false, 
						responsive: true,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									var prefix = data.labels[tooltipItem.index];
									
								    if (parseInt(value) >= 1000){
								    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								    } else {
								    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
								    }
								}						
							}
						},
			            scales: { yAxes: [{ ticks: { 
			            								min: 0, 
														callback: function(value, index, values) {
		              										if (parseInt(value) >= 1000) {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		              										} else {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
		              										}	            	
			            						   		}
			            						   } 
			            				 }] 
			            	}
					}
				};
			else	
				chart_settings = {
					type: chart_type,
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: data_labels,
						datasets: [{
							data: data_values,
							backgroundColor: data_colors
						}]
					},
					options: { 
						legend: false, 
						responsive: true,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								    if (parseInt(value) >= 1000){
								    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								    } else {
								    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
								    }
								}						
							}
						},
			            scales: { yAxes: [{ ticks: { 
			            								min: 0, 
														callback: function(value, index, values) {
		              										if (parseInt(value) >= 1000) {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		              										} else {
		                										return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
		              										}	            	
			            						   		}
			            						   } 
			            				 }] 
			            	}
					}
				};
		}
	}
	
	if (chart_type == 'doughnut') delete chart_settings['options']['scales'];
	var chart = new Chart( $('#' + div_id), chart_settings);		
}	

function exportChart(canvas_id)
{
	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id;
}

	
</script>
