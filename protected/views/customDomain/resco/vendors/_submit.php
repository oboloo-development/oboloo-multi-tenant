<?php $disabled = FunctionManager::sandbox(); ?>
<div class="form-group">
  <div class="clearfix"> <br /> </div>
    <?php if(!empty($vendor_id) && strtolower($this->action->id)=='edit' && strtolower($vendor['vendor_archive']) !="yes"){ ?>
  <div class="col-md-4 col-sm-4 col-xs-12">
    <button type="button"  <?php echo $disabled; ?> onclick="if(checkUrl()){$('#vendor_form').submit();}" class="btn btn-primary submit-btn" style="background-color: #1abb9c !important;  border-color: #1abb9c !important;">
          <?php if (isset($vendor_id) && $vendor_id) echo 'Save Supplier'; else echo 'Add Supplier'; ?>
    </button>
<?php if (isset($vendor_id) && $vendor_id && $vendor['active'] == 1 && strtolower($vendor['vendor_archive']) !="yes") { ?>
    <button type="button"  <?php echo $disabled; ?> class="btn btn-danger" onclick="$('#delete_confirm_modal').modal('show');">
        Deactivate Supplier
    </button>
<?php }
    else if(isset($vendor_id) && $vendor_id && $vendor['active'] == 0 && strtolower($vendor['vendor_archive']) !="yes") { ?>
    <button type="button"  <?php echo $disabled; ?> class="btn btn-danger" onclick="$('#activate_confirm_modal').modal('show');" >
        Activate Supplier
    </button>
<?php } ?>

  </div>
  <?php if (FunctionManager::checkEnvironment(true) && strtolower($vendor['vendor_archive']) !="yes") { ?>
    <div class="col-md-2 col-sm-2 col-xs-12 text-right">
      
      <?php if($disabled == 'disabled') {?>
        <a class="btn btn-danger" <?php echo $disabled; ?>  class="btn btn-danger">
         Un-archive Supplier
        </a>
      <?php } else { ?>
       <a href="<?php echo $this->createUrl('vendors/saveArhive',array('vendor-id'=>$vendor_id));?>"  
        style="background-color: #5bc0de !important;  border-color: #5bc0de !important;" 
        class="btn btn-danger" onclick="return confirm('Are you sure you want to archive this supplier')"  />Archive Supplier</a>
      <?php } ?>

    </div>
   <?php } }


    if(FunctionManager::checkEnvironment(true) && !empty($vendor_id) && strtolower($vendor['vendor_archive']) =="yes"){ ?>
    <div class="col-md-3 col-sm-3 col-xs-12 un-archive">

     <?php if($disabled == 'disabled') {?>
      <a class="btn btn-primary" <?php echo $disabled; ?>>Un-archive Supplier</a>
    <?php } else { ?>
      <a href="<?php echo $this->createUrl('vendors/saveArhive',array('vendor-id'=>$vendor_id,'archive-type'=>'un-archive'));?>"     class="btn btn-primary"  
        <?php echo $disabled; ?> onclick="return confirm('Are you sure you want to Un-archive this supplier')"/>Unarchive Supplier
      </a>
   <?php } ?>
  </div>
    <?php } ?>
   <?php   ?>
  </div>
