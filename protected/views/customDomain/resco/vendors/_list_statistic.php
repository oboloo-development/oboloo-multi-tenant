<?php
$tool_currency = Yii::app()->session['user_currency'];
$user_currency_symbol = Yii::app()->session['user_currency_symbol'];
$category_labels = $preferred_data = $non_preferred_data = array();
/*  foreach ($vendors_by_category as $value)
  {
    
    $category_labels[] = $value['industry'];
    $preferred_data[] = $value['preferred'];
    $non_preferred_data[] = $value['non_preferred'];

  }
*/
  $category_labels = "['".implode("'],['",$category_labels)."']";
  $category_labels = str_replace(" ","','",$category_labels);

  $topContractsTotal = array_column($top_contracts, 'total','dim_1');
  foreach($topContractsTotal as $key=>$value)
  {
      if(empty($key))
          unset($topContractsTotal[$key]);
      else if($key=="Total")
          unset($topContractsTotal[$key]);
  }
  $top_contract_series = array_values($topContractsTotal);
  $top_contract_label = array_keys($topContractsTotal);

  $categoryVendor = $categoryContract = array();


  //$vendorContractCategory = array_keys($count_by_category);
  $categoryVendor = '';
  $vendorContractCategory = array();
  // foreach($count_by_category as $value){
  //   $vendorContractCategory[] = !empty($value['industry'])?$value['industry']:"No Industry Selected";
  //   $categoryVendor .= (!empty($value['total_vendors'])?$value['total_vendors']:0).",";
  //   $categoryContract[] = !empty($value['total_contracts'])?$value['total_contracts']:0;
  // }

  /*CVarDumper::dump($vendorContractCategory,10,1);
  CVarDumper::dump($categoryVendor,10,1);
  CVarDumper::dump($categoryContract,10,1);die; */


 ?>


<div class="row mbl-row-pl-10">
  <div class="col-md-4 col-sm-4 col-xs-12">
  <div class="x_panel tile" ><div class="x_title"><h2>% Of Documents Approved</h2><div class="clearfix"></div></div><div class="x_content"><div id="document_vs_approved_vs_pending"></div><br /></div>
  </div></div>
  <div class="col-md-4 col-sm-4 col-xs-12">
  <div class="x_panel tile"><div class="x_title" style=""><h2>% Of Documents That Have Expired</h2><div class="clearfix"></div></div><div class="x_content"><div id="document_vs_valid_vs_expired"></div><br /></div>
  </div></div>
  <div class="col-md-4 col-sm-4 col-xs-12">
  <div class="x_panel tile" ><div class="x_title"><h2>% Of Preferred Suppliers</h2><div class="clearfix"></div></div><div class="x_content"><div id="vendor_vs_contract"></div><br /></div>
  </div></div>

</div>
 <div class="col-md-6 col-sm-6 col-xs-12 suppliers-notifications">
              <div class="x_panel notification_vendor" id="notification_scroll" style="margin-left: -15px; height: 335px !important; ">
                <div class="x_title">
                  <h2>Recent Supplier Notifications </h2>
                  <h2 class="pull-right"><a href="<?php echo AppUrl::bicesUrl('notifications/list'); ?>" class="btn btn-success expiring-contract-btn">All Notifications</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                    <?php foreach ($notifications as $notification) { ?>
                      <li>
                        <div class="block">
                          <div class="block_content">
                          <h2 class="title">
                          <div class="row">
                          <div class="col-md-2">
                      <?php 
                      $btnStyle="background-color: #fec98f;border-color: #fec98f;font-size:8px;";
                      $btnText = "Supplier";
                       ?>
                        <button class="btn btn-success" style="<?php echo $btnStyle;?>"><?php echo $btnText; ?></button>
                      </div>
                      <div class="col-md-10">
                      <?php  
                      // echo "<pre>"; print_r($notification); exit; 
                      $str = $notification['notification_text'];
                      $wordsNumber = 1000;
                      echo FunctionManager::getNWordsFromString($str,$wordsNumber); ?>
                      <div class="byline">
                      <span><?php echo Notification::getTimeAgo($notification['notification_date']); ?></span>
                      </div>
                      </div>
                      </div>
                     <!-- End :row -->
                    </h2>
                           
                          </div>
                        </div>
                      </li>
                    <?php } ?>
          </ul></div></div></div></div>

<div class="col-md-6 col-sm-6 col-xs-12 tile_count hidden-xs p-0" style="margin-top:0px !important;">
  <div class="x_panel tile">
    <div class="x_title">
      <h2>Number Of Suppliers By % Of Performance Score</h2> 
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
     <div id="vendor_contract_category"></div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="row tile_count">
  <div class="col-md-4 col-sm-4 col-xs-12 hidden-xs"><div id="preferred_supplier"></div></div>
  <div class="col-md-4 col-sm-4 col-xs-12 hidden-xs"><div id="supplier_under_contract"></div></div>
  <div class="col-md-4 col-sm-4 col-xs-12 hidden-xs"><div id="no_document"></div></div>
</div>
<!-- <div class="row tile_count"> -->
<!--   <div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_9">
    <div class="x_panel tile overflow_hidden">
      <div class="x_title">
        <h2>Categories By Preferred vs. Non-Preferred Suppliers (%)</h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="vendors_by_category"></div>
          </div>
        </div>
    </div> -->
<!-- <div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel tile overflow_hidden" style="">
  <div class="x_title">
    <h2>Top Ten Contracts by Spend</h2>
    
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
  <div id="spend_contracts"></div>
  </div>
</div>
</div> -->
<!-- <div class="clearfix"></div>  -->

<!-- </div> -->

<script type="text/javascript">

    


    // START: % of documents uploaded and approved and awaiting approval
    colors = ['rgb(72, 214, 168)','rgb(239, 166, 95)'];
    var options = {
        chart: {
          fontFamily: 'Poppins !important',
           height: 215,
          width: 340,
          type: 'pie',
          id:""

        },
       // legend: {show: false},
        colors:colors,
        dataLabels: {enabled: false},
        labels: [/*'Total Documents',*/'Approved Documents','Pending Documents'],
        series: [/*<?php echo $vendorStatistics['totalDocumentPerc'];?>,*/<?php echo $vendorStatistics['documentApprovedPerc'];?>,<?php echo $vendorStatistics['documentPendingdPerc'];?>],
        responsive: [{
           breakpoint: 280,
            options: {
                chart: {
                  width: 'auto',
                 /* height: 'auto',*/
                },
                legend: {show: false},
            }
        }],

       tooltip: {
            y: {
                formatter: function(value) {
                 return value+"%";
               },
                 
            },}
        
    }
    var chart = new ApexCharts(
        document.querySelector("#document_vs_approved_vs_pending"),
        options
    );
    chart.render();

    // END: % of documents uploaded and approved and awaiting approval

     // START: % of documents that are valid and % documents that have expired
      colors = ['rgb(72, 214, 168)','rgb(247, 119, 140)'];
    var options = {
        chart: {
          fontFamily: 'Poppins !important',
          height: 215,
          width: 330,
          type: 'pie',
          id:""

        },
       // legend: {show: false},
        colors:colors,
        dataLabels: {enabled: false},
        labels: [/*'Total Documents',*/'Valid Documents','Expired Documents'],
        series: [/*<?php echo $vendorStatistics['totalDocumentPerc'];?>,*/<?php echo $vendorStatistics['documentValidPerc'];?>,<?php echo $vendorStatistics['documentExpiredPerc'];?>],
        responsive: [{
           breakpoint: 280,
            options: {
                chart: {
                   width: 'auto'
                    //height: 'auto',
                },
                legend: {show: false},
            }
        }],
         
       tooltip: {
            y: {
                formatter: function(value) {               
                    return value+"%";   
               },
                 
            },}
    }
    var chart = new ApexCharts(
        document.querySelector("#document_vs_valid_vs_expired"),
        options
    );
    chart.render();


    // START: Vendor Vs Contract
     colors = ['rgb(72, 214, 168)','rgb(170, 170, 170)'];
    var options = {
        chart: {
          fontFamily: 'Poppins !important',
          height: 215,
          width: 350,
          type: 'pie',
          id:""

        },
       // legend: {show: false},
        colors:colors,
        dataLabels: {enabled: false},
        labels: ['Preferred Suppliers','Non-Preferred Suppliers'],
        series: [<?php echo $vendorStatistics['preferredVendorPercentage'];?>,<?php echo $vendorStatistics['nonPreferredVendorPercentage'];?>],
        responsive: [{
           breakpoint: 380,
            options: {
                chart: {
                   width: 'auto'
                    //height: 'auto',
                },
                legend: {show: false},
            }
        }],
       tooltip: {
            y: {
                formatter: function(value) {
                 return value+"%";
               },
                 
            },}
        
    }
    var chart = new ApexCharts(
        document.querySelector("#vendor_vs_contract"),
        options
    );
    chart.render();

 


  

// chart.render();

 


// var options = {
//   chart: {
//     height: 240,
//     type: "radialBar",
//   },

//   series: [<?php echo $vendorStatistics['vendorMissingDocumentsPercentage'];?>],
//   labels: ['Missing Documentation'],
//   colors: ["#2d9ca2"],
//   plotOptions: {
//     radialBar: {
//       hollow: {
//         margin: 0,
//          size: "80%",
//         background:'#fff',
       
//       },
//       track: {
//         dropShadow: {
//           enabled: true,
//           top:0,
//           left:0,
//           blur:0,
//           opacity:0.5,
          
//         }
//       },
//       dataLabels: {
//         name: {
//          // offsetY: -10,
//           color: "#2d9ca2",
//           fontSize: "12px",
//           wordBreak:"break-word",
//         },
//         value: {
//           color: "#2d9ca2",
//           fontSize: "12px",
//           show: true,
//           formatter: function (val) {
//             return val+"%";
//           }
//         }
//       }
//     }
//   },
  
//   stroke: {
//     lineCap: "round"
//   },
  
// };

// var chart = new ApexCharts(document.querySelector("#no_document"), options);



// chart.render();
/*var options = {
      chart: {
          height: 260,
          type: 'bar',
          stacked: true,
          stackType: '100%'
      },
      //plotOptions: {bar: {columnWidth: '35%',distributed: false, horizontal: false}},
      responsive: [{
          breakpoint: 380,
          options: {
              legend: {
                  position: 'bottom',
                  offsetX: -10,
                  offsetY: 0
              }
          }
      }],
      series: [{
          name: 'Preferred',
          data: [<?php echo implode(",",$preferred_data);?>]
      },{
          name: 'Non-Preferred',
          data: [<?php echo implode(",",$non_preferred_data);?>]
      }],
      xaxis: {
          categories: [<?php echo $category_labels;?>],
          labels: {
          trim: false,

      show: true,
      formatter: function (value) {
            //valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
            return value;
            //return value;
      },
      //minHeight: undefined,
        //maxHeight: 90,
        //rotate: -5,
     
      }},
      yaxis:{ 
      labels: {
          formatter: function (value) {
            //valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
            return value;
          },
       }
  },
      fill: {
          opacity: 1
      },
      
      legend: {
          position: 'top',
          offsetX: 0,
          offsetY: 0
      },
  }

 var chart = new ApexCharts(
      document.querySelector("#vendors_by_category"),
      options
  );
  
  chart.render();/**/

  function createApexChart(height=350,width='35px',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    if(type=='line'){
        $.each(series, function(key, value) { 
          seriesL.push({'data':value['data'],'name':value['name']});
        });
    }else{
      $.each(series, function(key, value) {  
          seriesData.push(value);
        });
      seriesL.push({'name':toolTip,'data':seriesData});
    }

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value);
    });  
    var chartIDL = chartID;
    if(chartIDL=="#spend_contracts"){horizontalL=true;}
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }
      },
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        }
      },
      legend: {show: false,position: 'bottom',offsetX: 0,offsetY: 0},
      colors: colors,
      plotOptions: {bar: {columnWidth: '35%',distributed: true, horizontal: horizontalL,dataLabels: {
              position: 'bottom'
            },}},
      /*dataLabels: {
          enabled: true,
          //textAnchor: 'start',
          style: {
            colors: ['#fff']
          },
          formatter: function (val, opt) {
            return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
          },
          offsetX: 0,
          dropShadow: {
            enabled: true
          }
        },*/
      dataLabels: {enabled: lableL},
      stroke: {curve: 'straight'},
      series: seriesL,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      xaxis: {categories: categoriesL,labels: {
                // formatter: function (value) {
                //   valueL = chartIDL!="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                // return "";
                // },
                style: {fontSize: '10px'}}},
       yaxis: {
          /*labels: {
            show: false
          }*/
        },
      markers: {size: 5,hover: {size: 6}},
    }

    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }
  var colors = ['#2d9ca2','#66DA26', '#546E7A', '#E91E63', '#FF9800','#FDEC90','#2196F3', '#3a62ba', '#7b9333', '#344189','#','#','#','#','#'];
  /*createApexChart(260,'100%','bar',false,<?php echo json_encode($top_contract_series) ?>,<?php //echo json_encode($top_contract_label);?>,"#spend_contracts",colors); */



  function barAndLineChart(height=350,width='400',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = '450px';
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;
    var xaxisL   = true;
    if(chartIDL=="#category_vendors_combo" || chartIDL=="#vendor_contract_category"){

      var xaxisL   = false;
      $.each(series.supplier, function(key, value) {
          seriesData.push(value);
        });
      seriesL.push({'name':'Suppliers','type':'column','data':seriesData});
      $.each(series.contract, function(key, value) {
          seriesMixedData.push(value);
        });
      //seriesL.push({'name':'Contracts','type':'line','colors':'yellow','data':seriesMixedData});
      //xaxisL=false;
    }else if(type=='line'){
        $.each(series, function(key, value) { 
          seriesL.push({'data':value['data'],'name':value['name']});
        });
    }else{
      $.each(series, function(key, value) {  
          seriesData.push(value);
        });
      seriesL.push({'name':toolTip,'data':seriesData});
    }

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value.split(" "));
    });
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: false
        },
        dynamicAnimation: {
          enabled: false,
          speed: 350
        }},
        height: heightL,
        width: '400px',
        type: typeL,
        
        //stacked: stackedL,
        //stackType: '100%',
        
        },
        responsive: [{
            breakpoint: 350,
            options: {
                legend: {
                    position: 'top',
                    offsetX: 0,
                    offsetY: 0
                }
            }
        }],
        legend: {
                position: 'top',
                offsetX: 0,
                offsetY: 0
            },
      colors: colorL,
      plotOptions: {bar: {columnWidth: '35%',distributed: true, horizontal: horizontalL}},
      dataLabels: {enabled: lableL},
      //stroke: { width: [4,  4]}
       stroke: {width: [4, 4]},
      series: seriesL,
      //opposite: true,
      title: {
      },
      // grid: {
      //   row: {
      //     colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
      //     opacity: 0.5
      //   },
      // },
      fill: {opacity: 1},
      xaxis: {categories: categoriesL,labels: {
                formatter: function (value) {
                  //valueL = String(value).substr(0,3);
                return value
                },
                style: {fontSize: '10px'},
                 show:false,
                
              },
          tooltip: {enabled: false,}
        },
         
      
            yaxis: [
                  { seriesName: 'P',
              
              /*axisTicks: {show: true,color:'#008FFB'},
              axisBorder: { show: true,color:'#008FFB'},
              title: { text: "Suppliers"},*/

             min:0,
            tickAmount:2,
              labels: {
                formatter: function (value) {
                  return value.toFixed(0)
                },

              },},
           
          ],
      markers: {size: 5,hover: {size: 6},/*colors: ['#9C27B0']*/},
          }
    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }

   //barAndLineChart(275,'100%','line',false,<?php echo json_encode(array("supplier"=>$categoryVendor));?>,<?php echo json_encode($vendorContractCategory);?>,"#vendor_contract_category",colors);
   var colors = ['#F0DFDF','#F0DFDF','#F0DFDF','#F0DFDF','#F0DFDF','#F0DFDF','#FDEC90','#FDEC90','rgb(72, 214, 168)','rgb(72, 214, 168)'];
   var options = {
          series: [{ name: "Total Suppliers",
          data: [<?php echo implode(",",array_values($performance_avg)); ?>]
        }],
          chart: {
          fontFamily: 'Poppins !important',
          download: false, 
          toolbar: {
            show: false
          },
          height: 239,
          width: 530,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '35%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [<?php echo "'".implode("','",array_keys($performance_avg))."'"; ?>],
          labels: {
            colors: ['rgb(72, 214, 168)'],
            style: {
               // colors: ['rgb(72, 214, 168)'],
              fontSize: '10px'
            },
                 
                 show:true,
                
              },
        },
        yaxis: {
            show:false,

            min:0,
            tickAmount:2,
            labels: {
              color: colors,
                formatter: function (value) {
                  /*value = value.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                  valueL = value;*/
                  return value.toFixed(0)
                },
              },}
        };

        var chart = new ApexCharts(document.querySelector("#vendor_contract_category"), options);
        chart.render();

</script>
 <style>
   #preferred_supplier, #supplier_under_contract, #no_document{
    background-color: #fff;
    box-shadow: 0px 0px 21px #b9beea;
   }
 </style>