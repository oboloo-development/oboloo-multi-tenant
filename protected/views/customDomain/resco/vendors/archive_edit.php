<div class="right_col" role="main">
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            
             <h4>
            
            <?php
            if(!empty($vendor['vendor_name'])){?>
              <br /><strong class="notranslate">Supplier Name: </strong>: <span class="title-text notranslate"><?php echo $vendor['vendor_name'];?></span>
            <?php }

            if(!empty($vendor['contact_name'])){?>
              <br /><br /><strong class="notranslate">Contract Name: </strong>: <span class="title-text notranslate"> <?php echo $vendor['contact_name'];?></span>
            <?php }
            if(!empty($vendor['emails'])){?>
              <br /><br /><strong class="notranslate">Email: </strong>: <span class="title-text notranslate"> <?php echo $vendor['emails'];?></span>
            <?php }
            if(!empty($vendor['phone_1'])){?>
               <br /> <br /><strong class="notranslate">Contract Phone Number: </strong>: <span class="title-text notranslate"> <?php echo $vendor['phone_1'];?></span>
            <?php }
             if(!empty($vendor['phone_2'])){?>
                <br /><br /><strong class="notranslate">Contract Mobile Number:  </strong>: <span class="title-text notranslate"><?php echo $vendor['phone_2'];?></span>
            <?php } ?></h4>

        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('vendors/archiveList'); ?>">
                <button type="button" class="btn btn-info">
                    <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Return To Archived Suppliers
                </button>
            </a>
            <a href="<?php echo AppUrl::bicesUrl('vendors/list'); ?>">
                <button type="button" class="btn btn-info">
                    <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Unarchived Suppliers
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
        <?php if(Yii::app()->user->hasFlash('error')): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
        <?php elseif (!empty(Yii::app()->user->getFlash('success'))): ?>
        <div class="alert alert-success" role="alert">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
      <?php endif; ?>
    </div>

    <?php  $this->renderPartial("_detail_statistic",array('vendorStatistics'=>$vendorStatistics));?>

<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs">
  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
    <li role="presentation" class="<?php echo empty($_GET['tab'])?'active':''?>">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="vendor-tab" role="tab" data-toggle="tab" aria-expanded="true">
            Supplier Details
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content2" role="tab" id="contact-tab" data-toggle="tab" aria-expanded="false">
            Contact Info
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content3" role="tab" id="comments-tab" data-toggle="tab" aria-expanded="false">
            Comments
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content4" role="tab" id="payment-tab" data-toggle="tab" aria-expanded="false">
             Payment & Shipping
        </a>
    </li>
    <?php if(true || !empty($vendor_id)){?>
    <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='document'?'active':''?>">
        <a href="#tab_content8" role="tab" id="document-tab" data-toggle="tab" aria-expanded="false">
            Documents
        </a>
    </li>
  <?php } ?>

 

  
    
     <!--  <li role="presentation" class="">
          <a href="#tab_content5" role="tab" id="spend-tab" data-toggle="tab" aria-expanded="false">
              Spend Analysis
          </a>
      </li> -->
      <li role="presentation" class="">
          <a href="#tab_content6" role="tab" id="contract-tab" data-toggle="tab" aria-expanded="false">
              Contracts
          </a>
      </li>
      <li role="presentation" class="">
          <a href="#tab_content7" role="tab" id="quote-tab" data-toggle="tab" aria-expanded="false">
              Quotes
          </a>
      </li>
      <li role="presentation" class="">
          <a href="#tab_content9" role="tab" id="owner-tab" data-toggle="tab" aria-expanded="false">
              Supplier Owners
          </a>
      </li>
      <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='supplier-comparision'?'active':''?>">
        <a href="#tab_content10" role="tab" id="supplier-comparision" data-toggle="tab" aria-expanded="false">
            Supplier Comparison
        </a>
      </li>
  </ul>
  <form id="vendor_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('vendors/edit'); ?>">

    <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($vendor_id)) echo $vendor_id; ?>" />
    <input type="hidden" name="form_submitted" id="form_submitted" value="1" />

  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade <?php echo empty($_GET['tab'])?'active':''?> in" id="tab_content1" aria-labelledby="vendor-tab">
          <div class="form-group">
			  <?php if (isset($vendor_id) && $vendor_id && $vendor['active'] == 0) { ?>
	              <div class="col-md-4 col-sm-4 col-xs-8 date-input">
                      <label class="control-label">Supplier Name</label>
	                  <input type="text" class="form-control has-feedback-left notranslate" name="vendor_name" id="vendor_name"
	                        <?php if (isset($vendor['vendor_name']) && !empty($vendor['vendor_name'])) echo 'value="' . $vendor['vendor_name'] . '"'; else echo 'placeholder="Supplier Name"'; ?> >
	                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
	              </div>
		          <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Status</label>
		          	  <select name="active" id="active" class="form-control notranslate">
		          	  	   <option value="0">Inactive</option>
		          	  	   <option value="1">Active</option>
		          	  </select>
		          </div>
		      <?php } else { ?>
	              <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                      <label class="control-label">Supplier Name</label>
	                  <input type="text" class="form-control has-feedback-left notranslate" name="vendor_name" id="vendor_name"
	                        <?php if (isset($vendor['vendor_name']) && !empty($vendor['vendor_name'])) echo 'value="' . $vendor['vendor_name'] . '"'; else echo 'placeholder="Supplier Name"'; ?> >
	                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
	              </div>
	          <?php } ?>
            <div class="clearfix"></div>
            <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Contact Name</label>
                <input type="text" class="form-control has-feedback-left contact_name notranslate" name="contact_name" id="contact_name"
                        <?php if (isset($vendor['contact_name']) && !empty($vendor['contact_name'])) echo 'value="' . $vendor['contact_name'] . '"'; else echo 'placeholder="Contact Name"'; ?> >
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>
            </div>


          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Email Address  <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control has-feedback-left notranslate" name="emails" id="emails" required="required" <?php if (isset($vendor['emails']) && !empty($vendor['emails'])) { echo 'value="' . $vendor['emails'] . '"'; echo 'readonly';} else echo 'placeholder="Email Address"'; ?> >
                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>

            <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6 date-input">
                <label class="control-label">Phone Number</label>
                <input type="number" class="form-control has-feedback-left phone_1 notranslate" name="phone_1" id="phone_1"
                        <?php if (isset($vendor['phone_1']) && !empty($vendor['phone_1'])) echo 'value="' . $vendor['phone_1'] . '"'; else echo 'placeholder="Phone Number"'; ?> >
                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 date-input">
                <label class="control-label">Mobile Number</label>
                <input type="number" class="form-control has-feedback-left phone_2 notranslate" name="phone_2" id="phone_2"
                        <?php if (isset($vendor['phone_2']) && !empty($vendor['phone_2'])) echo 'value="' . $vendor['phone_2'] . '"'; else echo 'placeholder="Mobile Number"'; ?> >
                <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>

              <div class="clearfix"></div>
               <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Company Assigned Supplier ID</label>
                <input type="text" class="form-control has-feedback-left notranslate" name="external_id" id="external_id"
                        <?php if (isset($vendor['external_id']) && !empty($vendor['external_id'])) echo 'value="' . $vendor['external_id'] . '"'; else echo 'placeholder="Company Assigned Supplier ID"'; ?> >
                <span class="fa fa-male form-control-feedback left" aria-hidden="true"></span>
              </div></div>
          </div>


         <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Supplier Status  <span style="color: #a94442;">*</span></label>
                <select name="profile_status" id="profile_status" class="form-control notranslate" required="required">
                <option value="Active" <?php if (isset($vendor['profile_status']) && $vendor['profile_status'] == 'Active') echo ' selected="SELECTED" '; ?>>Active</option>
                <option value="Inactive" <?php if (isset($vendor['profile_status']) && $vendor['profile_status'] == 'Inactive') echo ' selected="SELECTED" '; ?>>Inactive</option>
                <option value="Under Review" <?php if (isset($vendor['profile_status']) && $vendor['profile_status'] == 'Under Review') echo ' selected="SELECTED" '; ?>>Under Review</option>
              </select>
            </div>
        </div>

          

          <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <label class="control-label">Supplier Industry <span style="color: #a94442;">*</span></label>
				<select name="industry_id" id="industry_id" class="form-control notranslate" onchange="loadSubindustries(0);" required="required">
					<option value="">Select Supplier Industry</option>
					<?php foreach ($industries as $industry) { ?>
						<option value="<?php echo $industry['id']; ?>"
								<?php if (isset($vendor['industry_id']) && $vendor['industry_id'] == $industry['id']) echo ' selected="SELECTED" '; ?>>
							<?php echo $industry['value']; ?>
						</option>
					<?php } ?>
				</select>
              </div>
          </div>

          <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <label class="control-label">Supplier Sub Industry <span id="requiredIndi" style="color: #a94442;">*</span></label>
				<select name="subindustry_id" id="subindustry_id" class="form-control notranslate" required="required">
					<option value="0">Select Supplier Sub Industry</option>
				</select>
              </div>
          </div>

          <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Company Number</label>
              <input type="number" class="form-control notranslate" name="number" id="number"
                    <?php if (isset($vendor['number']) && !empty($vendor['number'])) echo 'value="' . $vendor['number'] . '"'; else echo 'placeholder="Company Number"'; ?> >
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Tax Number</label>
              <input type="number" class="form-control notranslate" name="tax_number" id="tax_number"
                    <?php if (isset($vendor['tax_number']) && !empty($vendor['tax_number'])) echo 'value="' . $vendor['tax_number'] . '"'; else echo 'placeholder="Tax Number"'; ?> >
          </div>
      </div>

          <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="checkbox">
                      <label>
                          <input type="checkbox" class="flat notranslate" name="preferred_flag" id="preferred_flag" value="1"
                            <?php if (isset($vendor['preferred_flag']) && $vendor['preferred_flag']) echo 'checked="checked"'; ?>>
                          Check this box to indicate that this is a preferred supplier
                      </label>
                  </div>
              </div>
          </div>

        <?php $this->renderPartial('_submit',array('vendor_id'=>$vendor_id,'vendor'=>$vendor));?>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="contact-tab">
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Contact Name</label>
                <input type="text" class="form-control has-feedback-left contact_name notranslate" name="contact_name" id="contact_name"
                        <?php if (isset($vendor['contact_name']) && !empty($vendor['contact_name'])) echo 'value="' . $vendor['contact_name'] . '"'; else echo 'placeholder="Contact Name"'; ?> >
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>
         <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Email Address  <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control has-feedback-left notranslate" name="emails" id="emails" required="required" <?php if (isset($vendor['emails']) && !empty($vendor['emails'])) { echo 'value="' . $vendor['emails'] . '"'; echo 'readonly';} else echo 'placeholder="Email Address"'; ?> >
                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>

        <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                  <label class="control-label">Address Line 1</label>
                  <input type="text" class="form-control has-feedback-left notranslate" name="address_1" id="address_1"
                        <?php if (isset($vendor['address_1']) && !empty($vendor['address_1'])) echo 'value="' . $vendor['address_1'] . '"'; else echo 'placeholder="Address Line 1"'; ?> >
                  <span class="fa fa-address-card-o form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>

          <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                  <label class="control-label">Address Line 2</label>
                  <input type="text" class="form-control has-feedback-left notranslate" name="address_2" id="address_2"
                        <?php if (isset($vendor['address_2']) && !empty($vendor['address_2'])) echo 'value="' . $vendor['address_2'] . '"'; else echo 'placeholder="Address Line 2"'; ?> >
                  <span class="fa fa-address-card-o form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>

            
          <div class="form-group">
              <div class="col-md-3 col-sm-3 col-xs-6">
                  <label class="control-label">City</label>
                  <input type="text" class="form-control notranslate" name="city" id="city"
                        <?php if (isset($vendor['city']) && !empty($vendor['city'])) echo 'value="' . $vendor['city'] . '"'; else echo 'placeholder="City"'; ?> >
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6">
                  <label class="control-label">County/State</label>
                  <input type="text" class="form-control notranslate" name="state" id="state"
                        <?php if (isset($vendor['state']) && !empty($vendor['state'])) echo 'value="' . $vendor['state'] . '"'; else echo 'placeholder="County/State"'; ?> >
              </div>
          </div>

          <div class="form-group">
              <div class="col-md-3 col-sm-3 col-xs-6">
                  <label class="control-label">Country</label>
                  <input type="text" class="form-control notranslate" name="country" id="country"
                        <?php if (isset($vendor['country']) && !empty($vendor['country'])) echo 'value="' . $vendor['country'] . '"'; else echo 'placeholder="Country"'; ?> >
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6">
                  <label class="control-label">Zip/Postal Code</label>
                  <input type="text" class="form-control notranslate" name="zip" id="zip"
                        <?php if (isset($vendor['zip']) && !empty($vendor['zip'])) echo 'value="' . $vendor['zip'] . '"'; else echo 'placeholder="Zip/Postal Code"'; ?> >
              </div>
          </div>

        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6 date-input">
                <label class="control-label">Phone Number</label>
                <input type="text" class="form-control has-feedback-left phone_1 notranslate" name="phone_1" id="phone_1"
                        <?php if (isset($vendor['phone_1']) && !empty($vendor['phone_1'])) echo 'value="' . $vendor['phone_1'] . '"'; else echo 'placeholder="Phone Number"'; ?> >
                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 date-input">
                <label class="control-label">Mobile Number</label>
                <input type="text" class="form-control has-feedback-left phone_2 notranslate" name="phone_2" id="phone_2"
                        <?php if (isset($vendor['phone_2']) && !empty($vendor['phone_2'])) echo 'value="' . $vendor['phone_2'] . '"'; else echo 'placeholder="Mobile Number"'; ?> >
                <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>

        <div class="form-group" style="display: none;">
            <div class="col-md-3 col-sm-3 col-xs-6 date-input">
                <label class="control-label">Fax Number</label>
                <input type="text" class="form-control has-feedback-left notranslate" name="fax" id="fax"
                        <?php if (isset($vendor['fax']) && !empty($vendor['fax'])) echo 'value="' . $vendor['fax'] . '"'; else echo 'placeholder="Fax Number"'; ?> >
                <span class="fa fa-fax form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 date-input">
                <label class="control-label">Website URL</label>
                <input type="text" class="form-control has-feedback-left notranslate" name="website" id="website"
                        <?php if (isset($vendor['website']) && !empty($vendor['website'])) echo 'value="' . $vendor['website'] . '"'; else echo 'placeholder="Website URL"'; ?> >
                <span class="fa fa-desktop form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>
        <?php $this->renderPartial('_submit',array('vendor_id'=>$vendor_id,'vendor'=>$vendor));?>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="comments-tab">
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Supplier Comments</label>
                <textarea class="form-control notranslate" name="comments" id="comments" rows="4" <?php if (!isset($vendor['comments']) || empty($vendor['comments'])) echo 'placeholder="Supplier comments if any ..."'; ?>><?php if (isset($vendor['comments']) && !empty($vendor['comments'])) echo $vendor['comments']; ?></textarea>
            </div>
        </div>
     <?php $this->renderPartial('_submit',array('vendor_id'=>$vendor_id,'vendor'=>$vendor));?>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="payment-tab">

      

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Payment Term</label>
                <select name="payment_term_id" id="payment_term_id" class="form-control notranslate">
                    <option value="0">Select Payment Term</option>
                    <?php foreach ($payment_terms as $payment_term) { ?>
                        <option value="<?php echo $payment_term['id']; ?>"
                                <?php if (isset($vendor['payment_term_id']) && $vendor['payment_term_id'] == $payment_term['id']) echo ' selected="SELECTED"'; ?>>
                            <?php echo $payment_term['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Shipping Term</label>
                <select name="shipping_term_id" id="shipping_term_id" class="form-control notranslate">
                    <option value="0">Select Shipping Term</option>
                    <?php foreach ($shipping_terms as $shipping_term) { ?>
                        <option value="<?php echo $shipping_term['id']; ?>"
                                <?php if (isset($vendor['shipping_term_id']) && $vendor['shipping_term_id'] == $shipping_term['id']) echo ' selected="SELECTED"'; ?>>
                            <?php echo $shipping_term['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Shipping Method</label>
                <select name="shipping_method_id" id="shipping_method_id" class="form-control notranslate">
                    <option value="0">Select Shipping Method</option>
                    <?php foreach ($shipping_methods as $shipping_method) { ?>
                        <option value="<?php echo $shipping_method['id']; ?>"
                                <?php if (isset($vendor['shipping_method_id']) && $vendor['shipping_method_id'] == $shipping_method['id']) echo ' selected="SELECTED"'; ?>>
                            <?php echo $shipping_method['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="clearfix"> <br /> </div>
        </div>

       <!--  <?php if (isset($vendor_payments) && is_array($vendor_payments) && count($vendor_payments)) { ?>

            <?php foreach ($vendor_payments as $a_payment) { ?>

                <div class="form-group">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <label class="control-label">Payment Type Name</label>
                        <input type="text" class="form-control" name="payment_type_name[]" value="<?php echo $a_payment['payment_type_name']; ?>">
                    </div>

                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <label class="control-label">Payment Type</label>
                        <select name="payment_type_id[]" class="form-control">
                            <option value="0">Payment Type</option>
                            <?php foreach ($payment_types as $payment_type) { ?>
                                <option value="<?php echo $payment_type['id']; ?>"
                                        <?php if ($a_payment['payment_type_id'] == $payment_type['id']) echo ' selected="SELECTED" '; ?>>
                                    <?php echo $payment_type['value']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <label class="control-label">Currency</label>
                        <select name="currency_code[]" class="form-control">
                            <option value="">Select Currency</option>
                            <option value="CAD" <?php if ($a_payment['currency_code'] == 'CAD') echo ' selected="SELECTED" '; ?>>CAD</option>
                            <option value="EUR" <?php if ($a_payment['currency_code'] == 'EUR') echo ' selected="SELECTED" '; ?>>&euro; EURO</option>
                            <option value="GBP" <?php if ($a_payment['currency_code'] == 'GBP') echo ' selected="SELECTED" '; ?>>&pound; GBP</option>
                            <option value="USD" <?php if ($a_payment['currency_code'] == 'USD') echo ' selected="SELECTED" '; ?>>$ USD</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <a onclick="$(this).parent().parent().remove(); return false;" class="btn btn-link">
                            <span class="fa fa-remove hidden-xs"></span>
                        </a>
                    </div>
                </div>

            <?php } ?>

        <?php } ?> -->

       <!--  <fieldset id="additional-field-model">
        <div class="form-group">
            <div class="col-md-2 col-sm-2 col-xs-4">
                <label class="control-label">Payment Type Name</label>
                <input type="text" class="form-control" name="payment_type_name[]" placeholder="Payment Type Name">
            </div>

            <div class="col-md-2 col-sm-2 col-xs-4">
                <label class="control-label">Payment Type</label>
                <select name="payment_type_id[]" class="form-control">
                    <option value="0">Payment Type</option>
                    <?php foreach ($payment_types as $payment_type) { ?>
                        <option value="<?php echo $payment_type['id']; ?>">
                            <?php echo $payment_type['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-4">
                <label class="control-label">Currency</label>
                <select name="currency_code[]" class="form-control">
                    <option value="">Select Currency</option>
                    <option value="CAD">CAD</option>
                    <option value="EUR">&euro; EURO</option>
                    <option value="GBP">&pound; GBP</option>
                    <option value="USD">$ USD</option>
                </select>
            </div>

            <div class="col-md-2" style="margin-top: 30px;">
                <a href="javascript:void(0);" class="btn btn-link remove-this-field">
                    <span class="fa fa-remove hidden-xs"></span>
                </a>
                <a href="javascript:void(0);" class="btn btn-link create-new-field" style="margin-top: 0px;">
                    <span class="fa fa-plus hidden-xs"></span>
                </a>
            </div>
        </div>
        </fieldset> -->
        <?php $this->renderPartial('_submit',array('vendor_id'=>$vendor_id,'vendor'=>$vendor));?>
    </div>
      <div class="clearfix"> </div>
     <!--  <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="spend-tab" style="padding: 15px;">
          <?php if(count($orders)>0) {?>
          <table id="order_table" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                  <th>Action</th>
                  <th>Order ID</th>
                  <th>Supplier</th>
                  <th>Date</th>
                  <th>Amount in Tool Currency</th>
                  <th>Amount in Order Currency</th>
                  <th>User</th>
                  <th style="width: 140px;">Status</th>
              </tr>
              </thead>

              <tbody>

              <?php
          
                  foreach ($orders as $order) { ?>
                      <tr>
                          <td>
                              <a href="<?php echo AppUrl::bicesUrl('orders/edit/' . $order['order_id']); ?>">
                                  <button type="button" class="btn btn-sm btn-success">View</button>
                              </a>
                          </td>
                          <td style="text-align: right;"><?php echo $order['order_id']; ?></td>
                          <td>
                              <a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a>
                          </td>
                          <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                          <td style="text-align: right;">
                              <nobr>
                                  <?php
                                  switch ($order['currency']) {
                                      case 'GBP' :
                                          $currency = '&#163;';
                                          break;
                                      case 'USD' :
                                          $currency = '$';
                                          break;
                                      case 'CAD' :
                                          $currency = '$';
                                          break;
                                      case 'CHF' :
                                          $currency = '&#8355;';
                                          break;
                                      case 'EUR' :
                                          $currency = '&#8364;';
                                          break;
                                      case 'JPY' :
                                          $currency = '&#165;';
                                          break;
                                      case 'INR' :
                                          $currency = '&#x20B9;';
                                          break;
                                      default :
                                          $currency = '&#163;';
                                          break;
                                  }
                                  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($order['total_price'], 2); ?>
                              </nobr>
                          </td>
                          <td style="text-align: right;">
                              <nobr>
                                  <?php
                                  echo $currency . ' ' . number_format($order['calc_price'], 2);
                                  ?>
                              </nobr>
                          </td>

                          <td>
                              <a href="<?php echo AppUrl::bicesUrl('users/edit/' . $order['user_id']); ?>"><?php echo $order['user']; ?></a>
                          </td>
                          <td>
                              <?php
                              if (empty($order['order_status'])) $order['order_status'] = 'Pending';
                              $status_style = "";

                              if ($order['order_status'] == 'Paid' || $order['order_status'] == 'Closed' || $order['order_status'] == 'Received')
                                  $status_style = ' style="border: 1px solid green !important; background-color: green !important; color: #fff !important;" ';
                              if ($order['order_status'] == 'Declined')
                                  $status_style = ' style="border: 1px solid #990000 !important; background-color: #990000 !important; color: #fff !important;" ';
                              if ($order['order_status'] == 'More Info Needed')
                                  $status_style = ' style="border: 1px solid #CC3333 !important; background-color: #CC3333 !important; color: #fff !important;" ';
                              if ($order['order_status'] == 'Pending' || $order['order_status'] == 'Ordered - PO Not Sent To Supplier'
                                  || $order['order_status'] == 'Submitted' || $order['order_status'] == 'Ordered - PO Sent To Supplier'
                              )
                                  $status_style = ' style="border: 1px solid orange !important; background-color: orange !important; color: #fff !important;" ';
                              if (!empty($order['description'])) {
                                  $toolTip = $order['description'];
                              } else {
                                  $toolTip = 'N/A';
                              }

                              ?>
                              <button class="btn btn-sm btn-success status"
                                      title="<?php echo $toolTip; ?>" <?php echo $status_style; ?>>
                                  <?php echo $order['order_status']; ?>
                              </button>
                          </td>
                      </tr>

                  <?php } ?>

              </tbody>

          </table>

          <?php } if(count($orders)>0) { ?>
          <div class="clearfix"> </div>
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_11">
                  <div class="x_panel tile overflow_hidden">
                      <div class="x_title">
                          <h2>Supplier Spend with Order</h2>
                          <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                              <li><a onclick="expandChart1(11);"><i class="fa fa-arrows-h"></i></a></li>
                              <li><a onclick="exportChart('vendor_combo_chart');"><i class="fa fa-cloud-download"></i></a></li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content" style="width: 100%">
                          <canvas id="vendor_combo_chart"></canvas>
                      </div>
                  </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_11">
                  <div class="x_panel tile overflow_hidden">
                      <div class="x_title">
                          <h2>Supplier Spend with Order</h2>
                          <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                              <li><a onclick="collapseChart(11);"><i class="fa fa-arrows-h"></i></a></li>
                              <li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                          <table class="tile_info" style="margin-left: 0px;">
                              <thead>
                              <th>Action</th>
                              <th>Order Date</th>
                              <th>Total</th>
                              </thead>
                              <?php
                              $idx = 0;
                              foreach ($vendors_combo as $vendor_index => $combo)
                              {

                                  if ($vendor_index === 'total') continue;
                                  $idx += 1;
                                  switch ($idx)
                                  {
                                      case 1  : $color = 'blue'; break;
                                      case 2  : $color = 'purple'; break;
                                      case 3  : $color = 'green'; break;
                                      case 4  : $color = 'turquoise'; break;
                                      case 5  : $color = 'red'; break;
                                      case 6  : $color = 'antique-white'; break;
                                      case 7  : $color = 'aqua-marine'; break;
                                      case 8  : $color = 'bisque'; break;
                                      case 9  : $color = 'khaki'; break;
                                      case 10  : $color = 'blue'; break;
                                      case 11  : $color = 'purple'; break;
                                      case 12  : $color = 'green'; break;
                                      case 13  : $color = 'turquoise'; break;
                                      case 14  : $color = 'red'; break;
                                      case 15  : $color = 'antique-white'; break;
                                      case 16  : $color = 'aqua-marine'; break;
                                      case 17  : $color = 'bisque'; break;
                                      case 18  : $color = 'khaki'; break;
                                      case 19  : $color = 'blue'; break;
                                      case 20  : $color = 'purple'; break;
                                      case 21  : $color = 'green'; break;
                                      case 22  : $color = 'turquoise'; break;
                                      case 23  : $color = 'red'; break;
                                      case 24  : $color = 'antique-white'; break;
                                      case 25  : $color = 'aqua-marine'; break;
                                      case 26  : $color = 'bisque'; break;
                                      case 27  : $color = 'khaki'; break;
                                      case 28  : $color = 'blue'; break;
                                      case 29  : $color = 'purple'; break;
                                      case 30  : $color = 'green'; break;
                                      case 31  : $color = 'turquoise'; break;
                                      case 32  : $color = 'red'; break;
                                      case 33  : $color = 'antique-white'; break;
                                      case 34  : $color = 'aqua-marine'; break;
                                      case 35  : $color = 'bisque'; break;
                                      case 36  : $color = 'khaki'; break;
                                      default : $color = 'salmon'; break;
                                  }

                                  ?>


                                  <tr>
                                      <td style="width: 4%;"><i class="fa fa-square <?php echo $color; ?>" style="margin-left: 10px;"></i></td>
                                      <td style="width: 39%;vertical-align: middle;"><?php echo date("F j, Y", strtotime($combo['order_date'])); ?></td>
                                      <td style="width: 32%;vertical-align: middle;"> <?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format($combo['total_price'],2); ?></td>
                                  </tr>
                                  <?php

                              }
                              ?>
                          </table>
                      </div>
                  </div>
              </div>

          </div>
          <div class="clearfix"><br /></div>
          <?php } ?>

      </div>
 -->
      <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="contract-tab" style="padding: 15px;">
        <?php if(count($contracts)>0) {?>
          <table id="contract_table" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                  <th>Action</th>
                  <th>Supplier</th>
                  <th>Reference</th>
                  <th>Title</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Amount in Tool Currency</th>
                  <th>Amount in Order Currency</th>
              </tr>
              </thead>

              <tbody>

              <?php 
              
              foreach ($contracts as $contract)
              {
                  $class = '';
                  if (strtotime($contract['contract_end_date']) <= time()) $class = ' class="danger" style="color: red;" ';
                  else
                  {
                      if (strtotime($contract['contract_end_date']) > strtotime("+1 Month")) $class = ' class="success" ';
                      else $class = ' class="warning" style="color: orange;" ';
                  }
                  ?>
                  <tr <?php if (!empty($class)) echo $class; ?>>
                      <td>
                          <a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract['contract_id']); ?>">
                              <button type="button" class="btn btn-sm btn-success">View</button>
                          </a>
                      </td>
                      <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $contract['vendor_id']); ?>"><?php echo $contract['vendor']; ?></a></td>
                      <td><?php echo $contract['contract_reference']; ?></td>
                      <td><?php echo $contract['contract_title']; ?></td>
                      <td><?php echo date(FunctionManager::dateFormat(), strtotime($contract['contract_start_date'])); ?></td>
                      <td><?php echo date(FunctionManager::dateFormat(), strtotime($contract['contract_end_date'])); ?></td>
                      <td style="text-align: right;">
                          <nobr>
                              <?php
                              switch ($contract['currency']) {
                                  case 'GBP' : $currency = '&#163;';
                                      break;
                                  case 'USD' : $currency = '$';
                                      break;
                                  case 'CAD' : $currency = '$';
                                      break;
                                  case 'CHF' : $currency = '&#8355;';
                                      break;
                                  case 'EUR' : $currency = '&#8364;';
                                      break;
                                  case 'JPY' : $currency = '&#165;';
                                      break;
                                  case 'INR' : $currency = '&#x20B9;';
                                      break;
                                  default :  $currency = '&#163;';
                                      break;
                              }
                              echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($contract['contract_value_GB'], 2); ?>
                          </nobr>
                      </td>
                      <td style="text-align: right;">
                          <nobr>
                              <?php
                              echo $currency . ' ' . number_format($contract['contract_value'], 2);
                              ?>
                          </nobr>
                      </td>
                  </tr>

              <?php } ?>
             
              </tbody>

          </table>
          <?php } if(count($contracts)>0) { ?>
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_20">
                  <div class="x_panel tile overflow_hidden">
                      <div class="x_title">
                          <h2>Contracts By Location</h2>
                          <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                              <li><a onclick="expandChart1(20);"><i class="fa fa-arrows-h"></i></a></li>
                              <li><a onclick="exportChart('contracts_by_location');"><i class="fa fa-cloud-download"></i></a></li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                          <canvas id="contracts_by_location" height="180px"></canvas>
                      </div>
                  </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_20">
                  <div class="x_panel tile overflow_hidden">
                      <div class="x_title">
                          <h2>Contracts By Location</h2>
                          <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                              <li><a onclick="collapseChart(20);"><i class="fa fa-arrows-h"></i></a></li>
                              <li><a onclick="exportChart('contracts_by_location');"><i class="fa fa-cloud-download"></i></a></li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                          <table class="tile_info" style="margin-left: 0px;">
                              <?php
                              $idx = 0;
                              foreach ($contracts_by_location as $dataset_idx=>$a_location)
                              {
                                  if ($dataset_idx === 'total') continue;
                                  $idx += 1;
                                  switch ($idx)
                                  {
                                      case 1  : $color = "rgba(220,220,220,0.5)"; break;
                                      case 2  : $color = "rgba(151,220,187,0.5)"; break;
                                      case 3  : $color = "rgba(187,220,151,0.5)"; break;
                                      case 4  : $color = "rgba(220,151,187,0.5)"; break;
                                      case 5  : $color = "rgba(220,187,151,0.5)"; break;
                                      case 6  : $color = "rgba(187,151,220,0.5)"; break;
                                      case 7  : $color = "rgba(120,151,187,0.5)"; break;
                                      case 8  : $color = "rgba(151,120,187,0.5)"; break;
                                      case 9  : $color = "rgba(187,120,151,0.5)"; break;
                                      case 10 : $color = "rgba(151,187,120,0.5)"; break;
                                      default : $color = "rgba(151,187,205,0.5)"; break;
                                  }
                                  ?>

                                  <tr>
                                      <td style="width: 5%;"><i class="fa fa-square" style="color: <?php echo $color; ?>!important;"></i></td>
                                      <td style="vertical-align: middle;">
                                          <?php echo addslashes($a_location['location_name']); ?> - <?php echo $a_location['total'] ; ?>
                                      </td>
                                  </tr>

                                  <?php
                              }
                              ?>
                          </table>
                      </div>
                  </div>
              </div>

          </div>
          <div class="clearfix"><br /></div>
          <?php } ?>

      </div>

      <div role="tabpanel" class="tab-pane fade" id="tab_content7" aria-labelledby="quote-tab" style="padding: 15px;">
          <?php if(count($quotes)>0) {?>
          <table id="quote_table" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                  <th>Action</th>
                  <th>Quote ID</th>
                  <th>Name</th>
                  <th>Opening Date</th>
                  <th>Closing Date</th>
                  <th>User</th>
                  <th>Status</th>
              </tr>
              </thead>

              <tbody>

              <?php
              foreach ($quotes as $quote) {
                  ?>
                  <tr>
                      <td>
                          <a href="<?php echo AppUrl::bicesUrl('quotes/edit/' . $quote['quote_id']); ?>">
                              <button type="button" class="btn btn-sm btn-success">View</button>
                          </a>
                      </td>
                      <td style="text-align: right;"><?php echo $quote['quote_id']; ?></td>
                      <td><?php echo $quote['quote_name']; ?></td>
                      <td><?php echo date(FunctionManager::dateFormat()." H:iA", strtotime($quote['opening_date'])); ?></td>
                      <td><?php echo date(FunctionManager::dateFormat()." H:iA", strtotime($quote['closing_date'])); ?></td>
                      <td> <a href="<?php echo AppUrl::bicesUrl('users/edit/'.$quote['user_id']); ?>"><?php echo $quote['full_name']; ?></a></td>
                      <td>
                          <?php
                          if (strtotime($quote['closing_date']) <= time()) echo 'Complete<br />';

                          if ($quote['submit_count'] <= 0)
                          {
                              if (strtotime($quote['closing_date']) > time()) echo 'Awaiting Quotes';
                          }
                          else
                          {
                              if ($quote['invite_count'] == 1)
                                  echo $quote['submit_count'] . ' of ' . $quote['invite_count'] . ' Quote received';
                              else
                                  echo $quote['submit_count'] . ' of ' . $quote['invite_count'] . ' Quotes received';
                          }
                          ?>
                      </td>
                  </tr>

              <?php } ?>

              </tbody>

          </table>
        <?php } ?>
          <div class="clearfix"><br /></div>
      </div>

      <div role="tabpanel" class="tab-pane fade" id="tab_content9" aria-labelledby="supplier-tab" style="padding: 15px;">
        <?php $this->renderPartial('_owners',array('owners'=>$owners,'vendor_id'=>$vendor_id,));?>
      </div>

      <div role="tabpanel" class="tab-pane fade" id="tab_content10" aria-labelledby="supplier-tab" style="padding: 15px;">
        <?php $this->renderPartial('_supplier_comparision',array('quickComparison'=>$quickComparison,'vendor_id'=>$vendor_id,));?>
      </div>

      
      <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab']=='document'?'in active':''?>" id="tab_content8" aria-labelledby="quote-tab" style="padding: 15px;">
        <?php $this->renderPartial('documents',array('vendor_id'=>$vendor_id,'documentList'=>$documentList,'documenArchivetList'=>$documenArchivetList));?>
       <div class="clearfix"><br /></div>
     </div>
  </div>


  </form>
</div>
</div>

<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Deactivate  Supplier Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to deactivate  this Supplier?</p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          	  NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          	  YES
          </button>
        </div>
      </div>
	</div>
</div>

<div class="modal fade" id="activate_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Activate Supplier Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to activate this Supplier?</p>
        </div>
        <div class="modal-footer">
          <button id="no_activate" type="button" class="alert-success btn btn-default" data-dismiss="modal">
              NO
          </button>
          <button id="yes_activate" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
              YES
          </button>
        </div>
      </div>
  </div>
</div>
<style>
    .ui-tooltip {
        width: 200px;
        text-align: center;
        box-shadow: none;
        padding: 0;
    }
    .ui-tooltip-content {
        position: relative;
        padding: 0.5em;
    }
    .ui-tooltip-content::after, .ui-tooltip-content::before {
        content: "";
        position: absolute;
        border-style: solid;
        display: block;
        left: 90px;
    }
    .bottom .ui-tooltip-content::before {
        bottom: -10px;
        border-color: #AAA transparent;
        border-width: 10px 10px 0;
    }
    .bottom .ui-tooltip-content::after {
        bottom: -7px;
        border-color: white transparent;
        border-width: 10px 10px 0;
    }
    .top .ui-tooltip-content::before {
        top: -10px;
        border-color: #AAA transparent;
        border-width: 0 10px 10px;
    }
    .top .ui-tooltip-content::after {
        top: -7px;
        border-color: white transparent;
        border-width: 0 10px 10px;
    }
</style>
<script type="text/javascript">

    var vendor_combo_data = {
        labels: [
            <?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>

            '<?php
				   echo date("M j", strtotime($vendor_combo_row['order_date'])); ?>' <?php if ($c != count($vendors_combo)) echo ',';
				 ?>

            <?php $c += 1; } ?>
        ],

        full_labels: [
            <?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>

            '<?php echo date("F j", strtotime($vendor_combo_row['order_date'])); ?>' <?php if ($c != count($vendors_combo)) echo ','; ?>

            <?php $c += 1; } ?>
        ],

        datasets: [


            {
                type: 'bar',
                label: 'Amount Spent',
                backgroundColor: 'lightgreen',
                fill: false,
                yAxisID: 'Y2',
                data: [

                    <?php $c = 1; foreach ($vendors_combo as $vendor_combo_row) { ?>

                    <?php echo round($vendor_combo_row['total_price'], 2); ?>
                    <?php if ($c != count($vendors_combo)) echo ','; ?>

                    <?php $c += 1; } ?>

                ]
            }

        ]
    };

    var vendor_combo_chart =
        new Chart($('#vendor_combo_chart'),
            {
                type: 'bar',
                data: vendor_combo_data,
                options: {
                    scaleUse2Y: true,
                    responsive: true,
                    maintainAspectRatio: false,
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                var prefix = data.full_labels[tooltipItem.index];
                                var output = "";

                                if (tooltipItem.datasetIndex == 0)
                                {
                                    return value;
                                }
                                else
                                {
                                    if (parseInt(value) >= 1000){
                                        return output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else {
                                        return output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
                                    }
                                }
                            }
                        }
                    },
                    scales: {
                        xAxes: [{ ticks: { maxRotation: 0, minRotation: 0 } }],
                        yAxes: [
                            {
                                id: 'Y2', type: 'linear', position: 'left', display: true,
                                ticks: {
                                    min: 0,
                                    callback: function(value, index, values) {
                                        if (parseInt(value) >= 1000) {
                                            return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else {
                                            return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
                                        }
                                    }
                                }
                            },
                            { id: 'Y1', type: 'linear', position: 'right', display: true, ticks: { min: 0, stepSize: 1 }, gridLines: { drawOnChartArea: false } }
                        ]
                    }
                }
            }
        );
</script>
<script type="text/javascript">

        var location_contracts_chart_config = {
            type: 'bar',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    <?php $i = 1; foreach ($contracts_by_location as $a_contract) { ?>
                    "<?php echo addslashes($a_contract['location_name']); ?>",
                    <?php $i += 1; } ?>
                ],
                datasets: [

                    {
                        label: 'Location',
                        backgroundColor: [
                            <?php
                                $i = 1;
                                foreach ($contracts_by_location as $a_contract)
                                {
                                    if ($i == 1) $backgroundColor = "rgba(220,220,220,0.5)";
                                    else if ($i == 2) $backgroundColor = "rgba(151,220,187,0.5)";
                                    else if ($i == 3) $backgroundColor = "rgba(187,220,151,0.5)";
                                    else if ($i == 4) $backgroundColor = "rgba(220,151,187,0.5)";
                                    else if ($i == 5) $backgroundColor = "rgba(220,187,151,0.5)";
                                    else if ($i == 6) $backgroundColor = "rgba(187,151,220,0.5)";
                                    else if ($i == 7) $backgroundColor = "rgba(120,151,187,0.5)";
                                    else if ($i == 8) $backgroundColor = "rgba(151,120,187,0.5)";
                                    else if ($i == 9) $backgroundColor = "rgba(187,120,151,0.5)";
                                    else if ($i == 10) $backgroundColor = "rgba(151,187,120,0.5)";
                                    else $backgroundColor = "rgba(151,187,205,0.5)";

                                    echo "'" . $backgroundColor . "'";
                                    if ($i != count($contracts_by_location)) echo ',';
                                    $i += 1;
                                }
                            ?>
                        ],

                        data: [
                            <?php
                                $i = 1;
                                foreach ($contracts_by_location as $a_contract)
                                {
                                    echo round($a_contract['total'], 2);
                                    if ($i != count($contracts_by_location)) echo ',';
                                    $i += 1;
                                }
                            ?>

                        ]
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: { yAxes: [ { ticks: { min: 0, stepSize: 1 } } ] }
            }
        };

        var location_contracts_chart = new Chart($('#contracts_by_location'), location_contracts_chart_config);

</script>


<script type="text/javascript">
$(document).ready( function() {


  $('#owners').select2().val(null).trigger('change');
  $("#owners").select2({placeholder: 'Select Owner'});

	<?php if (isset($vendor['subindustry_id']) && !empty($vendor['subindustry_id'])) { ?>
		loadSubindustries(<?php echo $vendor['subindustry_id']; ?>);
	<?php } ?>

    $('#order_table').dataTable({
        "columnDefs": [
            { "targets": 0, "width": "6%", "orderable": false },
            { "type": "sort-month-year", targets: 3 }
        ],
        "order": []
    });

    $('#contract_table').dataTable({
        "columnDefs": [
            { "targets": 0, "width": "6%", "orderable": false },
            { "type": "sort-month-year", targets: 3 }
        ],
        "order": []
    });

    $('#quote_table').dataTable({
        "columnDefs": [
            { "targets": 0, "width": "6%", "orderable": false },
            { "type": "sort-month-year", targets: 3 }
        ],
        "order": []
    });

    $( ".status" ).tooltip({
        position: {
            my: "center bottom",
            at: "center top-10",
            collision: "flip",
            using: function( position, feedback ) {
                $( this ).addClass( feedback.vertical )
                    .css( position );
            }
        }
    });

    $('#vendor_name').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>'
    });	
	
    $( "#vendor_form" ).validate( {
        rules: {
            vendor_name: "required",
            industry: "required",
            subindustry: "required"
        },
        messages: {
            vendor_name: "Supplier Name is required",
            industry: "Industry is a required field",
            subindustry: "Sub Industry is a required field"
        },
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-error" ).removeClass( "has-success" );
			$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-success" ).removeClass( "has-error" );
			$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field"
    });

	$('#delete_confirm_modal .modal-footer button').on('click', function(event) {
	  var button = event.target; // The clicked button
	
	  $(this).closest('.modal').one('hidden.bs.modal', function() {
	      if (button.id == 'yes_delete') changeItemStatus(0);
      });
   });

  $('#activate_confirm_modal .modal-footer button').on('click', function(event) {
    var button = event.target; // The clicked button
  
    $(this).closest('.modal').one('hidden.bs.modal', function() {
        if (button.id == 'yes_activate') changeItemStatus(1);
      });
   });

  
    
});

$("#requiredIndi").hide();

function loadSubindustries(input_subindustry_id)
{
	var industry_id = $('#industry_id').val();
	
	if (industry_id==""){
		$('#subindustry_id').html('<option value="">Select Supplier Sub Industry</option>');
    $('#subindustry_id').removeAttr("required");
    $("#requiredIndi").hide();
	}else
	{
	    $.ajax({
	        type: "POST", data: { industry_id: industry_id }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('vendors/getSubIndustries/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="">Select Supplier Sub Industry</option>';

            if(options.suggestions.length){
              $('#subindustry_id'). attr("required", "required");
              $("#requiredIndi").show();
            }else{
               $('#subindustry_id').removeAttr("required");
               $("#requiredIndi").hide();
            }

	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	        	$('#subindustry_id').html(options_html);
	        	if (input_subindustry_id != 0) $('#subindustry_id').val(input_subindustry_id); 
	        },
	        error: function() { $('#subindustry_id').html('<option value="">Select Supplier Sub Industry</option>'); }
	    });
	}
}

function changeItemStatus(active_flag)
{   var active_flag = active_flag;
    $.ajax({
        type: "POST", data: { delete_item_id: $('#vendor_id').val(), active_flag: active_flag },
        url: "<?php echo AppUrl::bicesUrl('vendors/updateItemStatus/'); ?>",
        success: function() { location = "<?php echo AppUrl::bicesUrl('vendors/edit/'); ?>" + $('#vendor_id').val(); }
    });
}

function expandChart1(display_index)
{
    var show_index = display_index;

    if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
    else
    {
        $('#chart_area_' + show_index).hide(1000);
        $('#table_area_' + show_index).show(1000);
    }
}

function collapseChart(display_index)
{
    var show_index = display_index;
    var hide_index = 0;
    if (display_index % 2 != 0) hide_index = show_index - 1;
    else hide_index = show_index + 1;

    $('#table_area_' + show_index).hide(1000);
    $('#table_area_' + hide_index).hide(1000);

    if (!$('#chart_area_' + show_index).is(':visible'))
        $('#chart_area_' + show_index).show(2000);
    if (!$('#chart_area_' + hide_index).is(':visible'))
        $('#chart_area_' + hide_index).show(2000);
}

function uploadDocument(uploadBtnIDX,e)
{  
  e.preventDefault();
  var uploadedObj = $('#order_file_'+uploadBtnIDX);
  var uploadedObjDesc = $('#file_desc_'+uploadBtnIDX); 
  var uploadedDocType = $('#document_type_'+uploadBtnIDX);
  var uploadedDocStatus = $('#document_status_'+uploadBtnIDX);
  var uploadedDocExpiryDate = $('#expiry_date_'+uploadBtnIDX);

  var file_data = $('#order_file_'+uploadBtnIDX).prop('files')[0];
  var field_data = $('#file_desc_'+uploadBtnIDX).val();
  var field_type = $('#document_type_'+uploadBtnIDX).val();
  if($('#document_status_'+uploadBtnIDX).is(':checked')){
    var field_status = 'Approved';
  }else{
    var field_status = 'Pending';
  }
  var expiry_date = $('#expiry_date_'+uploadBtnIDX).val();

  var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('file_description', field_data);
    form_data.append('document_type',field_type);
    form_data.append('vendor_id',$('#vendor_id').val());
    form_data.append('document_status',field_status);
    form_data.append('expiry_date',expiry_date);                             
    $.ajax({
        url: BICES.Options.baseurl + '/vendors/uploadDocument', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(uploaded_response){
          $("#document_list_cont").html(uploaded_response);
          uploadedObj.val(null);
          uploadedObjDesc.val(null);
          uploadedDocType.val(null);
          uploadedDocStatus.prop('checked', false);
          uploadedDocExpiryDate.val(null);
          // display response from the PHP script, if any
        }
     });
}

function editSingleDocument(formID,modalID)
{   
  var modalID = modalID;
  var formID = formID;
  var expiry_date = $("#expiry_date_"+formID).val();
  var vendor_id = $("#vendor_id_"+formID).val();
  var document_id = $("#document_id_"+formID).val();
  $.ajax({
        url: BICES.Options.baseurl + '/vendors/editDocument', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        data: {vendor_id:vendor_id,document_id:document_id,expiry_date:expiry_date},//$('#'+formID).serialize(),                         
        type: 'post',
        success: function(uploaded_response){
          $('#'+modalID).modal('hide');
          $('.modal-backdrop').remove();
          $("#document_list_cont").html(uploaded_response);          
          
        }
     });
    
}


function approveDocument(documentType,e)
{  
  e.preventDefault();
  var arrayIDs=[];
  var documentTypeID = documentType;
  inputField = $("input[name^='status_']");
  inputField.each(function(index){
    fieldName = $(this).attr('name');
    if(fieldName.indexOf('status_'+documentTypeID) !== -1 && $(this).is(':checked'))
      arrayIDs.push($(this).val());
  });

  $.ajax({
        url: BICES.Options.baseurl + '/vendors/approveDocument', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        data: {document_ids:arrayIDs,vendor_id:$('#vendor_id').val()},                         
        type: 'post',
        success: function(uploaded_response){
          $("#document_list_cont").html(uploaded_response);          
            // display response from the PHP script, if any
        }
     });
}

function deleteDocument(documentID)
{
  var docID = documentID;
  $('#delete_document_' + documentID).confirmation({
    title: "Are you sure you want to delete the document?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('vendors/deleteDocument/'); ?>",
            data: { documentID: docID },
            success: function(uploaded_response){
              $("#document_list_cont").html(uploaded_response);
            }
        });
    },
    onCancel: function() {  }
  });
  
}

function deleteDocumentOther(documentID)
{
  var docID = documentID;
  $('#delete_document_' + documentID).confirmation({
    title: "Are you sure you want to delete the document?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('vendors/deleteDocumentOther/'); ?>",
            data: { documentID: docID },
            success: function(uploaded_response){
              $("#document_list_cont").html(uploaded_response);
            }
        });
    },
    onCancel: function() {  }
  });
  
}

$(".contact_name").change(function(){
      $(".contact_name").val($(this).val());
});

$(".phone_1").change(function(){
      $(".phone_1").val($(this).val());
});

$(".phone_2").change(function(){
      $(".phone_2").val($(this).val());
});

$('#document-tab').click(function () {
     checkOwnerDoucment()
});
 function checkOwnerDoucment(){
     $.ajax({
      url: BICES.Options.baseurl + '/vendors/checkOwnerDoucment', // point to server-side PHP script 
      dataType: 'text',  // what to expect back from the PHP script, if anything
      data: {vendor_id:$('#vendor_id').val()},                         
      type: 'post',
      success: function(result){
      if(result==1){
        $("#document_area_1").show();
        $(".approve_btn").show();
        $('input[name=status]').prop('disabled', false);
         $('input[name^=status]').removeAttr('disabled');
        //[]
      }else{
        $("#document_area_1").hide();
        $(".approve_btn").hide();
        $('input[name^=status]').prop('disabled', true);

        
      }
      }
    });
  }
  checkOwnerDoucment();
/* var today = new Date(); 
$('.expiry_date_1').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    minDate:today,
    locale: {
    format: 'DD/MM/YYYY'
    }
  });

  $('.expiry_date_1').on('apply.daterangepicker', function(ev, picker) {
    $('#expiry_date_1').val(picker.startDate.format('DD/MM/YYYY')); 
  });*/


</script>
<style type="text/css">
  .h4, h4 {
    font-size: 14px;
}
.modal
{
    overflow-y: auto !important;
}

.modal-open
{
   overflow:auto !important;
   overflow-x:hidden !important;
   padding-right: 0 !important;
}
</style>
