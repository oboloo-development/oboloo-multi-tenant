<div class="col-md-12 col-sm-12 col-xs-12">
<div class="pull-left" style="width: 71%">
<h4 class="heading">Onboard Documents</h4>
<div class="x_title" style="border-bottom:none">
    <h4 class="subheading"><br />Want to receive notifications about this supplier or upload & approve supplier documents? Add yourself as a Supplier Owner<br /></h4></div>

 <div class="clearfix"></div><br /><br />
  <div class="col-md-6 col-sm-6 col-xs-16">
  <div id="owner_list_cont_onboard"></div></div>
</div>
 <div class="clearfix"></div><br /><br />
  <h4 class="heading ownerpermission pull-left" style="width: 71%;margin-left: 5px;">Upload A Document On Behalf Of A Supplier</h4>
  <button type="button" class="btn btn-primary request-cntrt-css ownerpermission" onclick="$('#request_contract_document_modal_onboard').modal('show');">
    <span class="glyphicon glyphicon-file mr-2"></span> Request A New Document From Supplier
</button><div class="clearfix"></div><br />
  <img class="processing_oboloo" src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/../images/icons/processin_oboloo.gif'; ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">
            <div class="form-group" id="document_area_1_onboard" style="margin-bottom: 0px;">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <label class="control-label">Document <span style="color: #a94442;">*</span></label>
                    <input class="form-control" type="file" name="order_file_1" id="order_file_onboard_1" />
                    <span class="text-danger" style="font-size: 10px;">Maximum File Size of 2MB</span>

                </div>
               <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 0px;">
                    <label class="control-label">Document Title <span style="color: #a94442;">*</span></label>
                    <input type="text" class="form-control" name="file_desc_1" id="file_desc_onboard_1" placeholder="File/Document Description" /> 
                </div>
                 <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 0px;">
                  <label class="control-label">Document Type <span style="color: #a94442;">*</span></label>
                  <?php $documentsTitle = FunctionManager::vendorDocumentOnboard();?>
                  <select class="form-control" name="document_type_1" id="document_type_onboard_1">
                    <option value="">Select Document Type</option>
                    <?php foreach($documentsTitle as $key=>$value){?>
                    <option value="<?php echo $key?>"><?php echo $value;?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12" style="margin-bottom: 0px;">
                <label class="control-label">Expiry Date <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control expiry_date_1" name="expiry_date_1" id="expiry_date_onboard_1" placeholder="Expiry Date">
                </div>
                <!-- <div class="col-md-5 col-sm-5 col-xs-12">
                  <label class="control-label" style="padding-top: 14%;">&nbsp;
                  <input type="checkbox" name="document_status_1" id="document_status_1" / >Approve
                  </label>
                </div> -->
                
                <div class="col-md-1 col-sm-1 col-xs-12 text-right"><br />
                 <?php $disabled = FunctionManager::sandbox(); ?>
                <button id="btn_upload_onboard_1" class="btn btn-info submit-btn" <?php echo $disabled; ?> onclick="uploadDocumentOnboard(1,event);" style="    margin-top: 8px;">Upload</button>
          </div>
            </div><div class="clearfix"></div><br /><br />

            <div id="document_list_cont_pending_onboard"></div>
            <div id="document_list_cont_onboard">
              <?php 
              
              $this->renderPartial('_documents_onboard',array('documentList'=>$documentListOnboard,'documenArchivetList'=>$documenArchivetListOnboard,'vendor_id'=>$vendor_id));?>
            </div>
            <?php $this->renderPartial('_vendor_document_onboard',array('vendor_id'=>$vendor_id));?>
           
 <?php
    if(!empty($vendorApprovelLogs)){
        $this->renderPartial('approver_log',array('vendorApprovelLogs'=>$vendorApprovelLogs));
    }?> 

<script>
  $(document).ready( function() {
    $('#order_file_onboard_1').bind('change', function() {
     if(this.files[0].size > 2097152){
        alert("Maximum File Size of 2MB");
        this.value = "";
       }    
    });
  });
</script>

 
 <style >
   .request-cntrt-css {background-color: #F79820 !important;color: #fff !important;border-color: #F79820 !important;}
   .request-cntrt-css:hover {background-color: #F79820 !important;color: #fff !important;border-color: #F79820 !important;}
   
  </style>
</div>