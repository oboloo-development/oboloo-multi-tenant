<?php $disabled = FunctionManager::sandbox(); ?>
<div class="modal fade" id="request_contract_document_modal_onboard" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg"><div class="modal-content" style=""> 

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Request A New Onboarding Document From Supplier</h4>
        <div id="request_document_form_alert_onboard" class="alert alert-success" role="alert"></div>
      </div>
    <form id="request_vendor_form_onboard" class="form-horizontal col-md-12 form-label-left" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('vendors/requestVendorDocumentOnboard'); ?>">    
        <div class="form-group">
           
          <div class="col-md-6" id="document_contract_type_cont_onboard">
            <label class="control-label">Document Type  <span style="color: #a94442;">*</span></label>
            <?php $documentsTitle = FunctionManager::vendorDocumentOnboard();?>
            <select class="form-control select_search notranslate" name="document_vendor_type" id="document_vendor_type_onboard" required="required">
              <option value="">Select Document Type</option>
              <?php foreach($documentsTitle as $key=>$value){?>
              <option value="<?php echo $key?>"><?php echo $value;?></option>
              <?php } ?>
            </select>
           
          </div>

          <div class="col-md-6" id="document_contract_type_new_cont_onboard">
            <label class="control-label">Document Type  <span style="color: #a94442;">*</span></label>
            <input type="text" name="vendor_type_new" id="vendor_type_new_onboard" class="form-control notranslate" placeholder="New Document Type" />
          </div>


           <div class="col-md-5 pull-right" id="btn_document_type_onboard" title="Create New Document Type" style="margin-top: 28px;margin-right: 27px;">
           <button type="button" class="btn btn-primary request-cntrt-css">+ Create New Document Type</button></div>

           <div class="col-md-5 pull-right" id="btn_remove_document_type_onboard" title="Remove New Document Type" style="margin-top: 28px;margin-right: 27px;"> 
            <button type="button" class="btn btn-primary request-cntrt-css">- Remove New Document Type</button></div>
          

          <div class="clearfix"></div><br />
         
     </div>
     <div class="form-group"> 
        <div class="supplier_invite_container">
        <div class="vendor_new_QIX">
          
         <div class="col-md-6 date-input valid">
          <label class="control-label">Supplier Contact <span style="color: #a94442;">*</span></label><select class="form-control notranslate vendor_contact_new_QIX vendor_new_QIX"  required="required"  name="vendor_contact_name[]" onChange="contactSelect(this.value,'QIX')">
          </select>
         </div>

         <div class="col-md-6 date-input valid">
          <label class="control-label">Supplier Email <span style="color: #a94442;">*</span></label><select class="form-control notranslate vendor_email_new_QIX vendor_new_QIX"  required="required" name="vendor_contact_email[]" onChange="emailSelect(this.value,'QIX')">
          </select>
         </div>
          <div class="col-md-2 col-sm-1 col-xs-12" style="text-align: right;">
         <div class=" remove-supplier" id="" title=" Remove Supplier" onclick="removeSupplier(QIX);" style="color: #d86868; font-size: 30px; font-weight: 700;cursor: pointer;padding-top: 25px; ">-</div>
       </div>
         <div class="clearfix"></div><br /><br />
        </div>
      </div>
        <div id="more_supplier_container_onboard">
        </div>
        <div class="clearfix"></div><br />
         <!-- <div class=" pull-right" id="add_supplier_contianer" title=" Add Supplier" style="    font-size: 30px; font-weight: 700;margin-right: 12px;cursor: pointer;position: relative;bottom: 15px;">+</div> -->

      </div>
       <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12 valid">
              <label class="control-label">Description/Notes <span style="color: #a94442;">*</span></label>
              <textarea class="form-control notranslate" name="request_document_note" id="request_document_note_onboard" rows="4" placeholder="Description/Notes" required></textarea>
          </div>
      </div>
  <input type="hidden" name="request_document_vendor_id"  value="<?php echo $vendor_id;?>" />
  <input type="hidden" name="" id="total_contacts_onboard" value="1">
  <div class="modal-footer">
    <button type="submit" class="btn btn-success submit-btn" <?php echo $disabled; ?>>Submit Request</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div></form><div class="clearfix"></div></div></div></div>

  <script type="text/javascript">
  $(document).ready(function(){

    $("#document_contract_type_new_cont_onboard").hide();
    $("#btn_remove_document_type_onboard").hide();
    
    $(".select_search").select2({
    placeholder: "Select",
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });

  $("#btn_document_type_onboard").click(function(){

    $("#document_contract_type_new_cont_onboard").show();
    $("#vendor_type_new_onboard").attr('required','required');
    $("#btn_remove_document_type_onboard").show();

    $("#document_vendor_type_onboard").removeAttr('required');
   
     $("#document_contract_type_cont_onboard").hide();
     //$("#document_vendor_type_onboard").trigger('change');
     $(this).hide();
  });

  $("#btn_remove_document_type_onboard").click(function(){
    
    $("#document_contract_type_cont_onboard").show();
    $("#document_vendor_type_onboard").show();
    $("#document_vendor_type_onboard").attr('required','required');
    $("#document_vendor_type_onboard").trigger('change');
    $("#btn_document_type_onboard").show();

     $("#vendor_type_new_onboard").removeAttr('required');
     $("#document_contract_type_new_cont_onboard").hide();
    
     $(this).hide();
  });

  $("#request_document_form_alert_onboard").hide();
  $("#request_vendor_form_onboard").submit(function(e){
  e.preventDefault();
    var form = $(this);
    var url = form.attr('action');          
    $.ajax({
        dataType: "json",
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
        success: function(submitted_response){
          if(submitted_response.msg==1){
            $("#request_document_form_alert_onboard").show();
            $("#request_document_form_alert_onboard").html('Request Supplier Document submitted successfully.');
          }else{
            $("#request_document_form_alert_onboard").html('Problem occured while processing Request Supplier Document, try again');
            $("#request_document_form_alert_onboard").show();
          }
          $(".select_search").val('').trigger('change');
          $("#vendor_type_new_onboard").val('');
          
          $(".vendor_new_QIX").val('');
          $("#request_document_note_onboard").val('');
          loadingRequestDocument('<?php echo $vendor_id;?>');
        }
     });
     $("#request_document_form_alert_onboard").delay(1000*5).fadeOut();
  });
  });

  function supplierSelect(vendor_id,serialNum){
  var vendor_id = vendor_id;
  var serialNum = serialNum;

  var obj ='';
  if(serialNum=='QIX'){ 
    obj = $(".vendor_new_vendor_name_QIX");
  }else{
      obj = $(".vendor_new_vendor_name_"+serialNum);
  }
  $(obj).next('.select2-container').find('.select2-selection--single').removeClass('invalid');

   $.ajax({
          dataType: 'json',  
          type: "post",
          url: "<?php echo AppUrl::bicesUrl('vendors/getVendorDetail'); ?>",
          data: { vendor_id: vendor_id },
          success: function(data_option){

          if(serialNum=='QIX'){
             $(".vendor_contact_new_QIX").html(data_option.contact_name);
             //$(".vendor_email_new_QIX").val(data_option.emails);
          }else{
           $(".vendor_contact_new_"+serialNum).html(data_option.contact_name);
           //$(".vendor_email_new_"+serialNum).val(data_option.emails);
          }
          }
        });
}

supplierSelect('<?php echo $vendor_id;?>','QIX');

function contactSelect(contactName,serialNum){
  var obj='';
  if(serialNum=='QIX'){
    
     obj = $(".vendor_contact_new_QIX");
  }else{
      
     obj = $(".vendor_contact_new_"+serialNum);
  }
  $(obj).removeClass('invalid'); 
  var serialNum = serialNum;
  var contactName = contactName;

   $.ajax({
          dataType: 'json',  
          type: "post",
          url: "<?php echo AppUrl::bicesUrl('vendors/getVendorContactEmail'); ?>",
          data: { vendor_id: '<?php echo $vendor_id;?>',contact_name:contactName },
          success: function(data_option){

          if(serialNum=='QIX'){
             $(".vendor_email_new_QIX").html(data_option.emails);
          }else{
           $(".vendor_email_new_"+serialNum).html(data_option.emails);
          }
          }
        });
}


function emailSelect(contactName,serialNum){
  var obj='';
  if(serialNum=='QIX'){
    obj = $(".vendor_email_new_QIX");
  }else{
      obj = $(".vendor_email_new_"+serialNum);
  }
   $(obj).removeClass('invalid'); 
}

  $(document).ready(function(){
    $(".remove-supplier").hide();
    $("#add_supplier_contianer").click(function(){

        var total_contacts = $('#total_contacts_onboard').val();
        total_contacts = parseInt(total_contacts);
        total_contacts = total_contacts + 1;
        $('#total_contacts').val(total_contacts);

        var supplierCon = $(".supplier_invite_container").last().html();
   
       supplierCon = supplierCon.replace(/QIX/g, total_contacts);
        
   
      $("#more_supplier_container_onboard").append(supplierCon);
      $("#more_supplier_container_onboard .remove-supplier").show();

      

    });
   });

  function removeSupplier(idnumber){

     $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Do you want to remove this contact?</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                $(".vendor_new_"+idnumber).remove();
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },
                
            }
        });
  
}
function loadingRequestDocument(vendor_id){
   
    var vendor_id = vendor_id;
     $.ajax({
        type: "POST",
        url: "<?php echo AppUrl::bicesUrl('vendors/loadingRequestDocumentOnboard'); ?>",
        data: { vendor_id:vendor_id },
        success: function(uploaded_response){
          $("#document_list_cont_pending_onboard").html(uploaded_response);
        }
    });
}
loadingRequestDocument("<?php echo $vendor_id;?>");

function loadingRequestDocumentOnboard(vendor_id){
   
    var vendor_id = vendor_id;
     $.ajax({
        type: "POST",
        url: "<?php echo AppUrl::bicesUrl('vendors/loadingRequestDocumentOnboard'); ?>",
        data: { vendor_id:vendor_id },
        success: function(uploaded_response){
          $("#document_list_cont_pending_onboard").html(uploaded_response);
        }
    });
}
loadingRequestDocumentOnboard("<?php echo $vendor_id;?>");


function deleteDocumentPending(requestID)
{
  var requestID = requestID;
  
    $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Are you sure you want to delete this  Requested Refreshed Contract Document?</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                               $.ajax({
                                    type: "POST",
                                    url: "<?php echo AppUrl::bicesUrl('vendors/deleteDocumentPending'); ?>",
                                    data: { request_id: requestID,vendor_id:"<?php echo $vendor_id;?>" },
                                    success: function(uploaded_response){
                                      loadingRequestDocument("<?php echo $vendor_id;?>");
                                    }
                                });
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },
            }});
}


function deleteDocumentPendingOnboard(requestID)
{
  var requestID = requestID;
  
    $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Are you sure you want to delete this  Requested Refreshed Contract Document?</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                               $.ajax({
                                    type: "POST",
                                    url: "<?php echo AppUrl::bicesUrl('vendors/deleteDocumentPendingOnboard'); ?>",
                                    data: { request_id: requestID,vendor_id:"<?php echo $vendor_id;?>" },
                                    success: function(uploaded_response){
                                      loadingRequestDocumentOnboard("<?php echo $vendor_id;?>");
                                    }
                                });
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },
            }});
}

</script>
<style type="text/css">
  .modal-title { font-size: 20px !important }
</style>