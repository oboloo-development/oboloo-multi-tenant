 
 
 
	<div class="col-md-7 col-sm-7 col-xs-12">
		<h4>Supplier Log <br /><br /><br /></h4>
		<table class="table">
			<thead><tr><th>User</th><th>Date</th><th>Comments</th></tr></thead>
			<?php
			if(count($vendorApprovelLogs)) {
				foreach ($vendorApprovelLogs as $vendorLog) {
					?>
					<tr>
						<td><?php echo $vendorLog['user']; ?></td>
						<td><?php echo date(FunctionManager::dateFormat(), strtotime($vendorLog['created_datetime'])); ?></td>
						
						<td><?php echo $vendorLog['comment']; ?></td>
					</tr>
					<?php
				}
			}else{
			?>
				<tr><td colspan="4">No Record Found</td></tr>
			<?php } ?>
		</table>
	</div>