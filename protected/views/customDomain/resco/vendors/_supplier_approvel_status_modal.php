
<?php 
$disabled = FunctionManager::sandbox(); 
$user_id = Yii::app()->session['user_id'];?>
<div id="mysupplierStatusModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Supplier Status</h4>
      </div>
      <div class="modal-body" style="padding-bottom:20px;">

        <form action="<?php echo AppUrl::bicesUrl('vendors/approverVendor'); ?>" method="post">
           <label>Approver <span style="color: #a94442;">*</span> </label>
            <?php  $sql = " SELECT user_id,full_name FROM `users` where admin_flag !=1";
            $getUsers = Yii::app()->db->createCommand($sql)->queryAll(); 
             ?>
            <div class="form-group">
            <select  class="form-control select_user notranslate" name="approver_id" id="approver_id" style="border-radius: 10px;border: 1px solid #4d90fe;" required>
              <option value="">Select User</option>
                <?php foreach ($getUsers as $all_user) { ?>
                <option value="<?php echo $all_user['user_id'];?>" <?php 
                   if (!empty($vendor['approver_id']) && $vendor['approver_id'] == $all_user['user_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $all_user['full_name']; ?>
                </option>
                <?php } ?>
           </select>
          </div>
          <div class="form-group">
           <label>Status <span style="color: #a94442;">*</span> </label>
           <?php 
       
           $onChange = '';
           if($vendor['approver_id'] != $user_id){
               $onChange = 'onchange="checkVendorApprover(this.value)"';
              
           }?>
            <select  class="form-control notranslate" name="approver_status" id="approver_status" style="border-radius: 10px;border: 1px solid #4d90fe;" required <?php echo $onChange;?>>
              <option value="">Select Status</option>
                <option value="In Review" <?php if($vendor['approver_status'] =='In Review') echo 'selected="selected"'; ?>>In Review</option>
                <option value="Sent For Approval" <?php if($vendor['approver_status'] =='Sent For Approval') echo 'selected="selected"'; ?>>Sent For Approval</option>
                <option value="Approved" <?php if($vendor['approver_status'] =='Approved') echo 'selected="selected"'; ?>>Approved</option>
                <option value="Rejected" <?php if($vendor['approver_status'] =='Rejected') echo 'selected="selected"'; ?>>Rejected</option>
           </select>
          </div>
          <div class="form-group" style=" margin-top: 20px;">
            <label>Comment</label>
            <textarea class="form-control notranslate" name="approver_comment"><?php echo !empty($vendor['approver_comment'])?$vendor['approver_comment']:'';?></textarea>
          </div>
          <input type="hidden" name="vendor_id" value="<?php echo $vendor['vendor_id']; ?>">
          <div class="form-group" style=" margin-top: 20px;">
          <label>Date</label>
          <input type="text" class="form-control" value="<?php echo
              !empty($vendor['approver_datetime'])?date(FunctionManager::dateFormat().' H:i:s',strtotime($vendor['approver_datetime'])):date('d/m/Y H:i:s');?>" readonly />
           <input type="hidden" class="form-control" name="approver_datetime" value="<?php echo
              !empty($vendor['approver_datetime'])?date('Y-m-d H:i:s',strtotime($vendor['approver_datetime'])):date('Y-m-d H:i:s');?>" readonly />
          </div><br>
          <div class="col-md-12">
              <?php if(!empty($vendor_approval_history)) { ?>
          <table id="" class="table table-striped table-bordered" style="width:100%;">
           <thead>
            <tr>
               <th style="width:20%">Comments</th>
               <th style="width:20%">User Name</th>
               <th style="width:30%">Date and time</th>
            </tr>
             </thead>
             <tbody>

            <?php foreach ($vendor_approval_history as $vendorComment) { ?>
              <?php //echo  $vendorComment['approver_comment']; ?>
            <tr>
               <td><?php echo $vendorComment['approver_comment']; ?></td>
               <td><?php echo $vendorComment['user_name']; ?></td>
               <td><?php echo
              !empty($vendorComment['created_at'])?date(FunctionManager::dateFormat().' H:i:s',strtotime($vendorComment['created_at'])):date('d/m/Y H:i:s');?></td>
            </tr>
          <?php } ?>
            </tbody>
          </table>
        <?php } ?>
          </div>
          <div class="form-group" id="select_send_for_approvel_only" style=" margin-top: 20px;">
            <button type="submit" class="btn btn-primary" <?= $disabled; ?>>Save</button>
             
          </div>
          </form>
        </div>


      </div>
      
    </div>
  </div>
</div>

<?php  if($vendor['approver_id'] != $user_id){?>
<script type="text/javascript">
  $(document).ready(function(){
    $('#select_send_for_approvel_only').hide();
     /* $('#approver_status').on('change ',function() {

        if (this.value == 'Sent For Approval') {
            $('#select_send_for_approvel_only').show();
        } else if (this.value != 'Sent For Approval') {
            $('#select_send_for_approvel_only').hide();
        }
      });*/
  });
</script>
<?php } ?>
<script type="text/javascript">

 
  function checkVendorApprover(approvalstatus){
   if (approvalstatus == 'Sent For Approval' || approvalstatus == 'In Review') {
        $('#select_send_for_approvel_only').show();
    } else if (approvalstatus != 'Sent For Approval') {
        $('#select_send_for_approvel_only').hide();
    }
 }
</script>