<div class="pull-left" style="width: 71%">
<h4 class="heading">Supplier Sustainability</h4>
<h4 class="subheading"><br>Add sustainability scores and comments for this supplier here. The latest sustainability record will act as the current sustainability status.</h4>
</div>
<?php 
$disabledArchive = CommonFunction::disabledArchive('vendors', $vendor_id);
if(strtolower($disabledArchive) !='yes') { ?>
 <button type="button" class="btn btn-success pull-right" onclick="$('#add_supplier_sustainability_modal').modal('show');">
  <span class="glyphicon glyphicon-modal-window mr-2"></span> Add Supplier Sustainability Score
 </button><div class="clearfix"></div><br /><br />
<?php } if(count($vendorSustainability)>0) {?>
<div class="col-md-12">
<table id="vendor_sustainability_table" class="table table-striped table-bordered" style="width:100%;">
    <thead>
    <tr>
        <th style="width:20%">Score</th>
        <th style="width:30%">Reasons For Score</th>  
        <th style="width: 8%;">User</th>
        <th style="width: 8%;">Date & Time</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($vendorSustainability as $value) {
        ?>
        <tr>
            <?php if($value['score_value'] == 1){?>
                <td class="notranslate" style='word-break:break-all;'><span class="makeBtn1"><?php echo $value['score_value'];?></span></td>
            <?php } else if ($value['score_value'] == 2) {?>
            <td class="notranslate"  style='word-break:break-all;'><span class="makeBtn2"><?php echo $value['score_value']; ?></span></td> <?php } else if($value['score_value'] == 3) { ?>
                <td class="notranslate"  style='word-break:break-all;'><span class="makeBtn3"><?php echo $value['score_value']; ?></span></td>
            <?php } else return 0; ?>

             <td style='word-break:break-all'><?php echo $value['description']; ?></td> 
            <td class="notranslate" style='word-break:break-all'> <a href="<?php echo AppUrl::bicesUrl('users/edit/'.$value['created_by_id']); ?>"><?php echo $value['created_by_name']; ?></a></td>
            <td><?php echo date(FunctionManager::dateFormat()." H:i A", strtotime($value['created_at'])); ?></td>
         
        </tr>

    <?php } ?>
    </tbody>
</table>
</div>
<?php } ?>
<div class="clearfix"><br /></div>
<style type="text/css">
.makeBtn1, .makeBtn2, .makeBtn3 { padding: 6px 14px;
    border-radius: 10px;
    color: #2d9ca2 !important;
    border: 2px solid; }
.makeBtn1{background-color: #DDF1D5 !important; border-color:#DDF1D5 !important; }
.makeBtn2{ background-color: #FDEC90 !important; border-color:#FDEC90 !important;}
.makeBtn3{ background-color: #F0DFDF !important; border-color:#F0DFDF !important;}
</style>


