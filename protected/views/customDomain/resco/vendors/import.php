<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Import Suppliers</h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('vendors/list'); ?>">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Supplier List
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

    <div class="x_content">
      <p>Select one or more CSV files to import vendors. They will be automatically imported.</p>
      <p> </p>
      <p>The file should not have any header and must have fields in the following order: </p>
      <p> </p>
      <p>
          Supplier Name,Supplier Industry,Supplier Sub Industry,Preferred Flag (1 or 0), Address Line 1, Address Line 2, City, State, Zip/Postal Code, Country,
          Contact Person Name, Phone Number, Mobile Number, Fax, Website, Email Address,
          Additional Comments
      </p>

      <div class="row tile_count">
          <div class="clearfix"> </div>
      </div>

      <form name="import_form" id="import_form" action="<?php echo AppUrl::bicesUrl('vendors/importData'); ?> " method="post" enctype="multipart/form-data" class="dropzone">
          <div class="fallback">
              <input name="file[]" type="file" multiple />
          </div>
      </form>

    </div>

</div>
