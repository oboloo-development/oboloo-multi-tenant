<div class="col-md-12 col-sm-12 col-xs-12 p-0">
  <div class="x_title p-0" style="border-bottom:none"><h4 class="heading">Notifications</h4>
    <h4 class="subheading"><br />If you would like to receive automated notifications about this supplier then please add your username below<br /></h4></div>

  <?php //if(in_array(Yii::app()->session['user_type'],array(4))){?>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-16 p-0">
      <label> Username</label>
         <select multiple name="owners[]" id="owners" class="form-control select2_multiple notranslate">
          <option>select username</option>
          <?php foreach($owners as $value){?>
            <option value="<?php echo $value['user_id'];?>"><?php echo $value['full_name'];?></option>
          <?php }?>
        </select>
    </div><?php $disabled = FunctionManager::sandbox(); ?>
    <div class="col-md-1 col-sm-1 col-xs-12 text-right">
        <button id="owner_save_btn" class="btn btn-info <?php echo $disabled; ?>  " style="margin-top: 15px;">Save</button>
      </div>
  </div><div class="clearfix"></div><br /><br />
<?php //} ?>
  <br />
  <div class="col-md-6 col-sm-6 col-xs-16 p-0">
  <div id="owner_list_cont"></div></div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#owner_save_btn").click(function(event){
      event.preventDefault();
      var owner = $("#owners").val();
      owner = owner.toString(); 
       $.ajax({
        url: BICES.Options.baseurl + '/vendors/saveOwner', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        data: { owner: owner,vendor_id:'<?php echo $vendor_id;?>'},                         
        type: 'post',
        success: function(result){
        ownerID = owner.split(",")
        var i;
        for (i = 0; i < ownerID.length; ++i) {
             $("#owners option[value='"+ownerID[i]+"']").remove();
        }
        $('#owners').select2().val(null).trigger('change');
        loadVendorOWner();
        }
     });
    })
  });
   function loadVendorOWner(){
       $.ajax({
        url: BICES.Options.baseurl + '/vendors/ownerList', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        data: {vendor_id:'<?php echo $vendor_id;?>'},                         
        type: 'post',
        success: function(result){
        $("#owner_list_cont").html(result);
        }
      });
    }
    loadVendorOWner();

    function deleteOwner(ownerID){
      var owner_id = ownerID;
      if (confirm("Are you sure to delete?")) {
         $.ajax({
          url: BICES.Options.baseurl + '/vendors/ownerDelete', // point to server-side PHP script 
          dataType: 'text',  // what to expect back from the PHP script, if anything
          data: {owner_id:owner_id},                         
          type: 'post',
          success: function(result){
            loadVendorOWner();
            $('#owners').select2().val(null).trigger('change');
          }
        });
      }
    }
</script>