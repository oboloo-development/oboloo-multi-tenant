<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Archived Suppliers</h3>
        </div>
        <div class="span6 pull-right">
             <a href="<?php echo AppUrl::bicesUrl('vendors/list'); ?>">
                <button type="button" class="btn btn-info">
                    <span class="glyphicon glyphicon-list mr-2" aria-hidden="true"></span> Return To Active Suppliers
                </button>
            </a>
        </div>
         <div class="clearfix"> </div>
        <?php if(!empty(Yii::app()->user->hasFlash('success'))) { ?>
          <?php echo Yii::app()->user->getFlash('success');?>
        <?php } ?>

        <div class="clearfix"> </div>
        <div class="alert alert-success vendor_alert" style="display: none;"><strong>Success!</strong> Supplier submitted successfully.</div>
    </div>
    
    <br />
    <div class="row-fluid tile_count">
    	<form name="vendor_list_form" id="vendor_list_form" class="form-horizontal" role="form">
    		<div class="saving_filter_container">
          <div class="saving_filter_input">
            <select name="location_id" id="location_id" class="form-control select_industry_multiple border-select" onchange="loadDepartmentsForSingleLocation(0);">
              <option value="">Select Locations</option>
              <?php foreach ($locations as $location) { ?>
                <option value="<?php echo $location['location_id']; ?>" <?php if (isset($location_id) && $location_id == $location['location_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $location['location_name']; ?>
                </option>
              <?php } ?>
            </select>
          </div>
          <div class="saving_filter_input">
            <select name="department_id" id="department_id" class="form-control select_subindustry_multiple border-select">
              <option value="">Select Departments</option>
            </select>
          </div>
    			<div class="saving_filter_input">
    				<select name="industry_id" id="industry_id" class="form-control border-select select_industry_multiple notranslate" onchange="loadSubindustries();">
    					<option value="">All Industries</option>
    					<?php foreach ($industries as $industry) { ?>
    						<option value="<?php echo $industry['id']; ?>"
    								<?php if (isset($industry_id) && $industry_id == $industry['id']) echo ' selected="SELECTED" '; ?>>
    							<?php echo $industry['value']; ?>
    						</option>
    					<?php } ?>
    				</select>
    			</div>
    			<div class="saving_filter_input">
    				<select name="subindustry_id" id="subindustry_id" class="form-control border-select select_subindustry_multiple notranslate">
    					<option value="">All Subindustries</option>
    				</select>
    			</div>
          <div class="saving_filter_input">
            <select name="supplier_status" id="supplier_status" class="form-control border-select select_status_multiple select_type_multiple notranslate">
              <option value="">All Suppliers</option>
              <option value="1">Active Suppliers</option>
              <option value="0">Deactivated Suppliers</option>
            </select>
          </div>
          <div class="saving_filter_input">
            <select name="preferred_flag" id="preferred_flag" class="form-control border-select select_type_multiple notranslate">
              <option value="-1">Select Type</option>
              <option value="1" <?php if (isset($preferred_flag) && $preferred_flag == 1) echo ' selected="SELECTED" '; ?>>Preferred</option>
              <option value="0" <?php if (isset($preferred_flag) && $preferred_flag == 0) echo ' selected="SELECTED" '; ?>>Non-Preferred</option>
            </select>
          </div>
    			<div class="saving_filter_input">
            <div style="display: grid;">
             <button class="btn btn-info search-quote" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
             <button class="btn btn-primary " onclick="loadVendors(); return false;" style="margin-top: 0px !important;">Apply Filters</button>
            </div>
          </div>
    		</div>
    	</form>
	</div>
	
    <table id="vendor_table" class="table table-striped table-bordered" style="width: 100%">
      <thead>
        <tr>
          <th class="notranslate"> </th>
          <th class="notranslate">Name</th>
          <th class="notranslate">Industry</th>
          <th class="notranslate">Sub Industry</th>
         <!--  <th>Contact Name</th>
          <th>Phone</th> -->
          <th class="notranslate">Preferred</th>
          <th style="width:10%" class="notranslate">Contract</th>
        </tr>
      </thead>

      <tbody>

     

      </tbody>

  </table>

</div>



<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">De-Activate Supplier Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to de-activate this vendor: <span id="delete_name" style="font-weight: bold;"></span>?</p>
          <input type="hidden" name="delete_item_id" id="delete_item_id" value="0" />
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          	  NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          	  YES
          </button>
        </div>
      </div>
	</div>
</div>


<script type="text/javascript">
$(document).ready( function() {
    $('#vendor_table').dataTable({
        "columnDefs": [ {
            "targets": -1,
            "width": "6%",
           // "orderable": false
        } ],
        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
            	for (var i=1; i<=6; i++)
                	$('td', row).eq(i).css('text-decoration', 'line-through');
        },        
         "order": [[ 1, "asc" ]],
		    "pageLength": 100,        
        "processing": true,
        "serverSide": true,
        "ajax": {
        	"url": "<?php echo AppUrl::bicesUrl('vendors/archiveList'); ?>",
        	"type": "POST",
        	data: function ( input_data ) {
        		input_data.preferred_flag = $('#preferred_flag').val();
        		input_data.industry_id = $('#industry_id').val();
        		input_data.subindustry_id = $('#subindustry_id').val();
            input_data.supplier_status = $('#supplier_status').val();
            input_data.location_id = $('#location_id').val();
            input_data.department_id = $('#department_id').val();
  			  } 
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          if(aData[1].search("<span class='deactivated_vendor'></span>")>0){
            $(nRow).addClass('deactivated_record');
            /*$(nRow).css("background-color", "#ccc");*/
          }
         }
    });

	$('#delete_confirm_modal .modal-footer button').on('click', function(event) {
	  var button = event.target; // The clicked button
	
	  $(this).closest('.modal').one('hidden.bs.modal', function() {
	      if (button.id == 'yes_delete') changeItemStatus();
      });
   });
    
});

function loadVendors()
{
	$('#vendor_table').DataTable().ajax.reload();
}
function clearFilter() {
  $("option:selected").removeAttr("selected");
  $('.select_industry_multiple').trigger("change");
  $('.select_subindustry_multiple').trigger("change");
  $('.select_type_multiple').trigger("change");
  $('.select_status_multiple').trigger("change");
  $('#vendor_table').DataTable().ajax.reload();
}
function select2function(className,lableTitle){
  var lableTitle = lableTitle;
  $("."+className).select2({
     // placeholder: lableTitle,
     /* allowClear: true*/
    });
}
$(document).ready(function(){
  select2function('select_industry_multiple','All Locations');
  select2function('select_subindustry_multiple','All Departments');
  select2function('select_type_multiple','All Categories');
  select2function('select_status_multiple','All Sub Categories');
});
function loadSubindustries()
{
	var industry_id = $('#industry_id').val();
	
	if (industry_id == 0)
		$('#subindustry_id').html('<option value="0">All Subindustries</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { industry_id: industry_id }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('vendors/getSubIndustries/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="">All Subindustries</option>';
	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	        	$('#subindustry_id').html(options_html); 
	        },
	        error: function() { $('#subindustry_id').html('<option value="0">All Subindustries</option>'); }
	    });
	}
}


$(".vendor_alert").hide();
  function loadDepartmentsForSingleLocation(department_id) {
      var location_id = $('#location_id').val();
      if (location_id == 0)
        $('#department_id').html('<option value="0">Select Departments</option>');
      else {
        $.ajax({
          type: "POST",
          data: {
            location_id: location_id
          },
          dataType: "json",
          url: BICES.Options.baseurl + '/locations/getDepartments',
          success: function(options) {
            var options_html = '<option value="0">Select Departments</option>';
            for (var i = 0; i < options.length; i++) {
              options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
            }
            $('#department_id').html(options_html);
            if (department_id != 0) $('#department_id').val(department_id);
          },
          error: function() {
            $('#department_id').html('<option value="0">Select Departments</option>');
          }
        });
      }
    }
</script>
<style type="text/css">
.location .multiselect {width: 138%;}
.department .multiselect {width: 112%;}
.deactivated_record {background-color:#ccc !important}
</style>
