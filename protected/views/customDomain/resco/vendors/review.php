
<div class="right_col" role="main">
    <!-- <div class="vendor-tutorial"></div> -->
    <div class="row-fluid tile_count">
        <div class="span6 pull-left col-md-5">
            <h3>Supplier Review</h3>
        </div>

        <div class="clearfix"> </div>
         <?php if(!empty(Yii::app()->user->hasFlash('success'))) { ?>
          <?php echo Yii::app()->user->getFlash('success');?>
        <?php } ?>

        <div class="clearfix"> </div>
        <div class="alert alert-success vendor_alert" style="display: none;"><strong>Success!</strong> Supplier submitted successfully.</div>
    </div>
    <?php $subindustry_id = $_POST['subindustry_id']; ?>
    <br />
    <div class="row-fluid tile_count" >
    	<form name="vendor_list_form" method="post" action="<?php echo AppUrl::bicesUrl('vendors/review'); ?>" id="vendor_list_form" class="form-horizontal" role="form">

    		<div class="form-group">
          <div class="row">
      			<div class="col-md-3 col-sm-3 col-xs-12 ">
      				<select name="industry_id" id="industry_id" class="form-control border-select select_industry_multiple" onchange="loadSubindustries();">
      					<option value="">All Industries</option>
      					<?php foreach ($industries as $industry) { ?>
      						<option value="<?php echo $industry['id']; ?>"
      								<?php if (isset($industry_id) && $industry_id == $industry['id']) echo ' selected="SELECTED" '; ?>>
      							<?php echo $industry['value']; ?>
      						</option>
      					<?php } ?>
      				</select>
      			</div>
      			<div class="col-md-3 col-sm-3 col-xs-12">
      				<select name="subindustry_id" id="subindustry_id" class="form-control border-select select_subindustry_multiple">
      					<option value="">All Subindustries</option>
      				</select>
      			</div>
            <div class="col-md-3 col-sm-3 col-xs-12 ">
              <select name="supplier_status" id="supplier_status" class="form-control border-select select_status_multiple">
                <option value="">All Suppliers &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</option>
                <option value="1">Active Suppliers</option>
                <option value="0">Deactivated Suppliers</option>
              </select>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 ">
              <select name="preferred_flag" id="preferred_flag" class="form-control border-select select_type_multiple">
                <option value="-1">Select Type &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</option>
                <option value="1" <?php if (isset($preferred_flag) && $preferred_flag == 1) echo ' selected="SELECTED" '; ?>>Preferred</option>
                <option value="0" <?php if (isset($preferred_flag) && $preferred_flag == 0) echo ' selected="SELECTED" '; ?>>Non-Preferred</option>
              </select>
            </div>
      			<input type="hidden" name="reset_form_post_request" id="reset_form_post_request" value="0">
            <div class="col-md-12 col-sm-12 col-xs-12  search-supplier " style="margin-top: 20px">
              <div class="pull-right">
                <button class="btn btn-info search-quote" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
                <button class="btn btn-primary " onclick="loadVendors(); return false;" style="margin-top: 0px !important;">Apply Filters</button>
            </div>

          </div>
    		</div>
    	</form>
	</div>

<div class="clearfix"></div><div class="clearfix"></div>
    <?php $documentsTitle = FunctionManager::vendorDocument();?>
    <div id="scrollTop" style="overflow-x:auto;">
      <p></p>
    </div>
    <div id="scrollbottom" style="overflow-x:auto;">
     <table id="reveiw_table" class="table table-striped table-bordered suppliers-table info">
      <thead>
        <tr class="row-table">
          <th style="width: 6%;" tabindex="-1" class="nosort"> </th>
          <th class="notranslate" style="width: 16%;">Name</th>
          <th style="width: 2%;text-align: center;">Status</th>
          <th class="fit " style="width:  20% !important;text-align: center;">Average Performance Score</th>
          <th style="width: 4%;text-align: center;">Preferred</th>
          <th style="width: 4%;text-align: center;">Contract</th>
          <th style="width: 2%;text-align: center;">Risk</th>
          <th style="width: 2%;text-align: center;">Sustainability</th>
           <?php foreach($documentsTitle as $key=>$value){
              if(trim($value)=="Company Formation Certificate"){$width="width: 30%;";}
              else{$width="width: 15%;";}
            ?>
            <th class="fit" style="<?php echo $width;?> text-align: center;"><?php echo $value;?></th>
          <?php } ?> 
        </tr>
      </thead>
      <tbody>
      <?php

      $preferred_icon = '<i class="fa fa-check" style="color: #00bc9d;"></i>';
      $non_preferred_icon = '';
      $contractIcon = '<i class="fa fa-check " style="color: #00bc9d;"></i>';
      $contractIconNone = '';
      $currentDocumentDate = date('Y-m-d');
      foreach ($database_values as $vendor_data)
      {
        $avgScore = '';
        if(10*$vendor_data['avg_score']==0 and 10*$vendor_data['avg_score']<=1){
          $avgScore = "<span class='btn btn-primary' style='background:#c1bfbf !important;border-color:#c1bfbf !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$vendor_data['avg_score'])."%</span>";
        }else if(10*$vendor_data['avg_score']>=1 and 10*$vendor_data['avg_score']<=60){
          $avgScore = "<span class='btn btn-primary' style='background:#F0DFDF !important;border-color:#F0DFDF !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$vendor_data['avg_score'])."%</span>";
        }else if(10*$vendor_data['avg_score']>60 and 10*$vendor_data['avg_score']<=80){
          $avgScore = "<span class='btn btn-primary' style='background:#FDEC90 !important;border-color:#FDEC90 !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$vendor_data['avg_score'])."%</span>";
        }else{
          $avgScore = "<span class='btn btn-primary' style='background:#DDF1D5 !important;border-color:#DDF1D5 !important ;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$vendor_data['avg_score'])."%</span>";
        }
        $riskIcon='';
        if(!empty($vendor_data['risk_score_value'])){
          if($vendor_data['risk_score_value']==1){
            $riskIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#00bc9d"></i>';
          }else if($vendor_data['risk_score_value']==2){
            $riskIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#FDEC90"></i>';
          }else {
            $riskIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#f7778c "></i>';
          }
        }
        $sustainabilityIcon='';
        if(!empty($vendor_data['score_value'])){
          if($vendor_data['score_value']==1){
            $sustainabilityIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#00bc9d"></i>';
          }else if($vendor_data['score_value']==2){
            $sustainabilityIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#FDEC90"></i>';
          }else {
            $sustainabilityIcon = '<i class="fa fa-circle" aria-hidden="true" style="color:#f7778c "></i>';
          }
        }

        $expredDoc  = empty($vendor_data['expired_document'])?0:$vendor_data['expired_document'];
        $pendingDoc = empty($vendor_data['total_pending'])?0:$vendor_data['total_pending'];

        $expiredDocColor = $expredDoc  == 0  ?'#DDF1D5':'#F0DFDF';
        $pendingDocColor = $pendingDoc == 0  ?'#DDF1D5' :'#F0DFDF';
        
        $expredDoc = "<div style='text-align:center'><span class='btn btn-primary' style='background: ".$expiredDocColor." !important;border-color: #F0DFDF  !important;border-radius: 10px;color:#2d9ca2 !important'>".$expredDoc."</span></div>";
        $pendingDoc = "<div style='text-align:center'><span class='btn btn-primary' style='background: ".$pendingDocColor." !important;border-color: #F0DFDF  !important;border-radius: 10px;color:#2d9ca2 !important'>".$pendingDoc."</span></div>"; 
        $supStatus = '';
        if(!empty($vendor_data['approver_status'])){
          $supStatus = $vendor_data['approver_status'];
          if($supStatus=="Approved"){
              $color="#DDF1D5";
          }else if($supStatus=="Sent For Approval"){
              $color="#d5dbf1";
          }else if($supStatus=="Rejected"){
              $color="#F0DFDF";
          }else {
              $color="#DCDCDC";
          }
          $supStatus='<span class="title-text notranslate" > <span class="btn btn-primary" style="background: '.$color.' !important;border-color: '.$color.'  !important;border-radius: 10px;color:#2d9ca2 !important;padding: 6px 7px !important;">'.$supStatus.'</span>';
        }

        $edit_link = '';
        $edit_link .= '<a href="' . AppUrl::bicesUrl('vendors/edit/' . $vendor_data['vendor_id']) . '">';
        $edit_link .= '<button class="btn btn-sm btn-success view-btn">View/Edit</button></a>';

        $preferred ='';
        if($vendor_data['preferred_flag']){
          $preferred = $preferred_icon;
        }else{
           $preferred = $non_preferred_icon;
        } 
        ?>
          <tr>
          <td><?= $edit_link ?></td>
          <td class="notranslate"><?php echo  $vendor_data['vendor_name'].' '.($vendor_data['active']==0? " <span class='deactivated_vendor'></span>":""); ?></td>
          <td><?= $supStatus ?></td>
          <td><?= "<div style='text-align:center'>".$avgScore."</div>" ?></td>
          <td><?= $preferred ?></td>
          <td><?= (!empty($vendor_data['contract_id']) ? $contractIcon:$contractIconNone) ?></td>
          <td><?= $riskIcon ?></td>
          <td><?= $sustainabilityIcon ?></td>

          <?php foreach($documentsTitle as $docKey=>$documentData){ 
          $sql = "select count(*) as total_doc from vendor_documents where expiry_date >='".$currentDocumentDate."' and archive =0 and status='Approved' and document_type=".$docKey." and vendor_id=".$vendor_data['vendor_id'];
          $documentReader = Yii::app()->db->createCommand($sql)->queryRow();
         
          if($documentReader['total_doc']>0){
            $docMarked = '<i class="fa fa-check" style="color: #00bc9d; padding-left: 20px;"></i>';
          }else{
            $docMarked = '';
          } ?>
          <td><?php echo $docMarked; ?></td>
          <?php } ?>
          </tr>
       <?php } ?>
       
      </tbody>
     </table>
    </div>

</div>

<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">De-Activate Supplier Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to de-activate this vendor: <span id="delete_name" style="font-weight: bold;"></span>?</p>
          <input type="hidden" name="delete_item_id" id="delete_item_id" value="0" />
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          	  NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          	  YES
          </button>
        </div>
      </div>
	</div>
</div>

<style type="text/css">
  table.info { width: auto; ; border-collapse: collapse; }

  td {white-space: nowrap;}
  #scrollTop p {
    height: 1px;
    width: auto;
    min-width: auto;}
  .dataTables_wrapper { display: inline-block;}

  @media only screen and (min-width: 769px){
  .dataTables_wrapper > .row {  overflow: hidden !important;}
 }
</style>
<script type="text/javascript">

  $(document).ready(function () {
    loadSubindustries(<?= $subindustry_id ?>);
      $('#reveiw_table').DataTable({
          "order": [[ 1,"asc" ]],
          "pageLength": 50,    
      });
  });

  $("document").ready(function(){
    $("#scrollTop p").width($("#reveiw_table").width()+100);
    $("#scrollTop").on("scroll", function(){
      $("#scrollbottom").scrollLeft($(this).scrollLeft()); 
    });
    $("#scrollbottom").on("scroll", function(){
      $("#scrollTop").scrollLeft($(this).scrollLeft()); 
    });

  $('#delete_confirm_modal .modal-footer button').on('click', function(event) {
	  var button = event.target; // The clicked button

     $(this).closest('.modal').one('hidden.bs.modal', function() {
	      if (button.id == 'yes_delete') changeItemStatus();
      });
   });
    
  });

function loadVendors()
{

	$('#vendor_list_form').submit();
}
function clearFilter() {
  $('#reset_form_post_request').val(1);
    $('#vendor_list_form').submit();
}
function select2function(className,lableTitle){
  var lableTitle = lableTitle;
  $("."+className).select2({
     // placeholder: lableTitle,
     /* allowClear: true*/
    });
}
$(document).ready(function(){
  select2function('select_industry_multiple','All Locations');
  select2function('select_subindustry_multiple','All Departments');
  select2function('select_type_multiple','All Categories');
  select2function('select_status_multiple','All Sub Categories');
});

function loadSubindustries(subindustry_id = 0)
{
 
	var industry_id = $('#industry_id').val();
  var subindustry_id = subindustry_id;
  
	if (industry_id == 0)
		$('#subindustry_id').html('<option value="0">All Subindustries</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { industry_id: industry_id }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('vendors/getSubIndustries/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="">All Subindustries</option>';
	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	           $('#subindustry_id').html(options_html); 
              if(subindustry_id != 0) $('#subindustry_id').val(subindustry_id)
           
	        },
	        error: function() { $('#subindustry_id').html('<option value="0">All Subindustries</option>'); }
	    });
	}
}


function deleteVendor(item_id)
{
   	$('#delete_item_id').val(0);
   	$('#delete_name').text('');

	var item_name = "";
	var col_index = 0;
	$('#delete_icon_' + item_id).parent().parent().parent().find('td').each(function() {
		col_index += 1;
		if (col_index == 2) item_name = $(this).text();
	});

	$('#delete_item_id').val(item_id);
	if ($('#delete_icon_' + item_id).hasClass('glyphicon-ok')) changeItemStatus();
	else
	{
		$('#delete_item_id').val(item_id);
		$('#delete_name').text(item_name);
		$('#delete_confirm_modal').modal('show');
	}	
}

function changeItemStatus()
{
   	var delete_item_id = $('#delete_item_id').val();
   	var active_flag = 0;
	    	
   	if ($('#delete_icon_' + delete_item_id).hasClass('glyphicon-remove'))
   	{
   		active_flag = 0;
   		$('#delete_icon_' + delete_item_id).removeClass('glyphicon-remove');
   		$('#delete_icon_' + delete_item_id).addClass('glyphicon-ok');
   	}
   	else
   	{
   		active_flag = 1;
   		$('#delete_icon_' + delete_item_id).removeClass('glyphicon-ok');
   		$('#delete_icon_' + delete_item_id).addClass('glyphicon-remove');
  	}
	    	
    $.ajax({
        type: "POST", data: { delete_item_id: delete_item_id, active_flag: active_flag },
        url: "<?php echo AppUrl::bicesUrl('vendors/updateItemStatus/'); ?>"
    });
    
	var col_index = 0;
	$('#delete_icon_' + delete_item_id).parent().parent().parent().find('td').each(function() {
		col_index += 1;
		if (col_index != 1)
		{
			if (active_flag == 0) $(this).css("text-decoration", "line-through");
			else $(this).css("text-decoration", "none");
		}
	});
}
$(".vendor_alert").hide();
function addVendorModal(e){
  e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('vendors/createByModal'); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_vendor_cont').html(mesg);
            $('#create_vendor').modal('show');
        }
    });
}
 
</script>
<?php $this->renderPartial('/vendors/create_vendor');?>
<script type="text/javascript">

  $(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();  
   $('#reveiw_table_filter, #reveiw_table_length').parent().removeClass('col-sm-6');
 });
</script>


<style type="text/css">
.location .multiselect {width: 138%;}
.department .multiselect {width: 112%;}
.deactivated_record {background-color:#ccc !important}

.table td.fit, 
.table th.fit {
    white-space: nowrap;
    width: 1%;
}

table td{text-align: center !important;}
table  tr td:nth-child(2),table  tr td:nth-child(3) {text-align: left !important;}
@media (min-width:1025px) {
  .supplier-type {width:max-content}}
@media only screen and (min-width: 992px) {.supplier-type {width:max-content}}
@media only screen and (max-width: 490px)  {
  .supplier-type {width:auto; margin-top:20px; }
  .search-supplier,#vendor_list_form .col-xs-6 { margin-top:20px; }
}

.dataTables_length{width: 10%; margin-left: 26px;} 
.dataTables_filter{
  width: 0%; 
  float: left;
  text-align: left;
}
</style>