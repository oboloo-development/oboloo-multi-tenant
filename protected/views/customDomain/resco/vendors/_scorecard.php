<?php
$category = new Category();
$categories = $category->getAll(array('soft_deleted' => "0"));
$scoreCardName = new VendorScoringName();
$scoreCardName = $scoreCardName->getAll(array('order' => 'id asc'));

if(!empty($_GET['filtercardid'])){ $filtercardid = $_GET['filtercardid']; }
else {  $filtercardid = 0; }

$query = "select * from vendor_score_card where scorecard_id= ".$filtercardid." and vendor_id=".$vendor_id."  order by id desc ";
$scoreCards = Yii::app()->db->createCommand($query)->queryAll();
?>
<div class="col-md-12 col-sm-12 col-xs-12 p-0">
    <div class="x_title p-0" style="border-bottom:none">
        <div class="col-md-6 col-xs-12">
         <h4 class="heading">Scorecard</h4>
         <h4 class="subheading mt-15">Create new or view historic scorecards for this supplier</h4>
        </div>
        <div class="col-md-6 col-xs-12 pr-0">
         <button type="button" class="btn btn-success btn-md pull-right green-btn" data-toggle="modal" data-target="#myModal">Create New Scorecard</button>
        </div>
    </div>

    <!--START Create Modal -->
    <?php $this->renderPartial('_vendor_create_scorecard_modal', ['vendor_id'=> $vendor_id]); ?>
    <!-- END Create Modal -->
    <div class="clearfix"></div>
    <!-- Start: Fliter  -->
    <div class="col-md-6 pl-0 mb-20">
     <form method="GET" action="<?php echo AppUrl::bicesUrl('vendors/edit/' . $vendor_id); ?>">
      <input type="hidden" name="tab" value="scorecard">
        <div class="row">
            <div class="col-md-9">
             <select name="filtercardid" id="filter_scorecard_name" class="form-control notranslate">
                <option value="">Select Scorecard Name</option>
                <?php
                $sql ="select id,scorecard_id,scorecard_name from vendor_score_card where vendor_id=".$vendor_id." group by scorecard_id order by scorecard_name asc ";
                $scoreCardNameAll = Yii::app()->db->createCommand($sql)->query()->readAll();
                foreach ($scoreCardNameAll as $cardName) { ?>
                 <option value="<?= $cardName['scorecard_id'] ?>" <?= isset($_GET['filtercardid']) && $_GET['filtercardid'] === $cardName['scorecard_id'] ? "selected='selected'":''; ?>><?= $cardName['scorecard_name'] ?></option>
                <?php } ?>
             </select>
            </div>
            <div class="col-md-3">
             <button class="btn btn-primary pull-right" type="submit" style="margin-top: 0px !important;">Apply Filters</button>
            </div>
        </div>
     </form>
    </div>
    <div class="col-md-3 mb-20">
     <form method="GET" action="<?php echo AppUrl::bicesUrl('vendors/edit/' . $vendor_id); ?>">
      <input type="hidden" name="tab" value="scorecard">
      <button class="btn btn-info search-quote" type="submit" style="border-color: #46b8da;">Clear Filters</button>
     </form>
  </div>
    <!-- END: Filter -->

    <!-- Start: Edit Scoring Card -->
    <div class="clearfix"></div>
    <div class="panel-group mt-26 pb-30" id="accordion">
        <?php 
         if(!empty($scoreCards)){ 
          $counter = 0;
          foreach ($scoreCards as $scoreCard) { 
            
            $counter++; 
            if($counter == 2){?>
             <h4 class="heading h4">Historic Scorecards</h4>
             <?php } ?>
             <div class="panel panel-default">
                <div class="panel-heading bg-white">
                 <?php 
                  $scoreList = new VendorScoreCardSelected();
                  $scoreLists = $scoreList->getAll(array('vendor_id' => $vendor_id, 'vendor_score_card_id' => $scoreCard['id'], 'order' => 'id asc'));
                  $totalWtdRating = 0;
                 ?>

                 <div class="panel panel-default">
                  <table class="table mb-0 ">
                   <thead class="panel-head">
                    <th style="width: 16%;"><b><?= $scoreCard['scorecard_name'] ?></b></th>
                    <th style="width: 10%;"><b>Score : <?= number_format(totalWtdScoreRating($scoreLists, $vendor_id),2) ?></b></th>
                    <th style="width: 13%;">Created By : <?= $scoreCard['created_by'] ?></th>
                    <th style="width: 13%;">Created Date : <?= $scoreCard['created_datetime'] ?></th>
                    <th style="width: 13%;">Updated By : <?= $scoreCard['updated_by'] ?></th>
                    <th style="width: 13%;">Updated Date : <?= $scoreCard['updated_date'] ?></th>
                    <th style="width: 18%;">
                        <a data-toggle="collapse" class="pr-21 text-white" data-parent="#accordion" href="#collapseTable_<?= $scoreCard['id'] ?>"><span class="glyphicon glyphicon-chevron-down"></span> View</a>
                        <a data-toggle="collapse" class="pr-21 ml-20 btn-action text-white" data-parent="#accordion" href="#collapse_<?= $scoreCard['id'] ?>"><span class="glyphicon glyphicon-chevron-down"></span> Edit</a>
                        <!-- START: Only show for Admin user delete option-->
                        <?php if(Yii::app()->session['user_type'] == 4){ ?>
                         <a class="ml-20 btn-action" onclick="vendScoreCardDelete(<?= $scoreCard['id'] ?>, 1)"><i class="fa fa-trash text-white" aria-hidden="true" style="font-size: 16px;"></i></a>
                        <?php } ?>
                    <!-- END: Only show for Admin user delete option-->
                    </th>
                   </thead>
                  </table>
                    <div id="collapseTable_<?= $scoreCard['id'] ?>" class="panel-collapse collapse">
                     <div class="panel-body">
                      <?php $this->renderPartial('_vendor_scorecard_listing', array('vendor_id' => $vendor_id, 'scoreLists' => $scoreLists, 'scoreCardId' => $scoreCard['id']));?>
                     </div>
                    </div>
                 </div>
                
                </div>

                <div id="collapse_<?= $scoreCard['id'] ?>" class="panel-collapse collapse ">
                    <div class="panel-body">
                        <form class="scorecard_form" method="post" action="<?= AppUrl::bicesUrl('vendorScoreCard/vendorScoreCardUpdate/' . $scoreCard['id']); ?>" autocomplete="off">
                            <div class="modal-body">
                                <input type="hidden" name="vendor_id" value="<?= $vendor_id; ?>">

                                <div class="form-group">
                                    <div class="col-12 col-md-12 col-sm-12 col-xs-12">
                                     <label class="control-label"> Scorecard Name <span style="color: #a94442;">*</span></label>
                                       <input type="text" readonly class="form-control notranslate "  value="<?= $scoreCard['scorecard_name'] ?>" required />
                                       <input type="hidden" readonly name="scorecard_id[]" class="form-control notranslate scorecard_list_coursal_text_only" onchange="selectScoreCardEditTable($(this).val(), <?= $scoreCard['id'] ?>)"  value="<?= $scoreCard['scorecard_id'] ?>" required />
                                      <div class="clearfix"></div>
                                    </div>
                                      <p class="error-list_coursal" style="color: red;"></p>
                                </div>

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="scorecard_edit_table_<?= $scoreCard['id'] ?>"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-xs-12 mt-15">
                                <?php $disabled = Yii::app()->session['user_type'] == 4 ? '' : 'disabled'; ?> 
                                <button type="submit" class="btn btn-primary pull-right edit-scorecard-submit edit-scorecard-submit_<?= $scoreCard['id'] ?>" <?= $disabled ?>>Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
             </div>
            <?php }
            } else{ ?>
             <div class="panel panel-default">
                <div class="panel-heading bg-white">
                 <p><b>No results.</b></p>
                </div>
             </div>
            <?php } ?>
        </div>
        <?php 
            function totalWtdScoreRating($scoreLists,$vendor_id){
            $allScorecards = [];
            if (!empty($scoreLists)) {
                foreach ($scoreLists as $scorecdID) {
                    $scorecard = new VendorScoreCard();
                    $scorecardDetail = $scorecard->getOne(['id' => $scorecdID['vendor_score_card_id'], 'vendor_id' => $vendor_id]);
                
                    $vendScoreCardSelectEval = new  VendorScoreCardSelectedEvaluation();
                    $vendScoreCardSelectEval = $vendScoreCardSelectEval->getAll(['scorecard_id' => $scorecardDetail['scorecard_id'], 'vendor_score_card_id' => $scorecardDetail['id'], 'vendor_id' => $vendor_id, 'order' => 'id asc']);

                    $getQuestion = [];
                    foreach ($vendScoreCardSelectEval as $list) {
                        $sql = "select * from vend_scard_selected_question_evaluation  
                                    where vend_scorecard_select_eval_id=" . $list['id'] . " and  scorecard_id=" . $scorecardDetail['scorecard_id'] . " and vendor_id=" . $vendor_id . " order by id asc ";
                        $getQuestion[] = Yii::app()->db->createCommand($sql)->queryAll();
                    }

                    $allScorecards[] = [
                        'scoreCardLists' => $vendScoreCardSelectEval,
                        'getQuestion' => $getQuestion,
                    ];
                }

                foreach ($allScorecards as $key => $scorecard) {    
                    $totalWtdRating = 0;
                    foreach ($scorecard['scoreCardLists'] as $key => $list) {
                        $scores = [];
                        
                        foreach ($scorecard['getQuestion'][$key] as $getQuestion) {
                            $scores[] = (float)$getQuestion['score_evaluation'];
                        }
                                
                        $areaRatingAvg = array_sum($scores) / count($scores);
                        if (!empty($scores)) {
                            $totalWtdRating += ($areaRatingAvg * $list['default_score']) / 100;
                        }    
                    }   
                    return $totalWtdRating;
                    }    
                }
            }
        ?>
        <div class="clearfix"></div>
        <style>
         .select2-search__field { padding: 2px 13px !important; width: 100% !important; }
         .scoreTitle{ font-size: 12px; font-weight: bolder;}
         .rotate { -moz-transform: rotate(-180deg); -ms-transform: rotate(-180deg); transform: rotate(-180deg); }
         .btn-action{cursor: pointer; font-size: 10px !important;}
         .panel-head{background-color: #3b4fb4; color: #fff;}
        </style>
        <script>
         $(document).ready(function() {
            
            $("#scorecard_list_id").select2({
                placeholder: "Select Scorecard Name",
                allowClear: true,
                containerCssClass: "notranslate",
                dropdownCssClass: "notranslate"
            });
            $(".scorecard_list_coursal").select2({
                placeholder: "Select Scorecard Name",
                allowClear: true,
                containerCssClass: "notranslate",
                dropdownCssClass: "notranslate"
            });
            });
            
            function selectScoreCardTable(value) {
                $.ajax({
                    type: "POST",
                    data: {
                        scorecardID: value,
                        vendor_id: '<?= $vendor_id ?>'
                    },
                    dataType: "html",
                    url: "<?php echo AppUrl::bicesUrl('vendorScoreCard/getScoreCardList/'); ?>",
                    success: function(options) {
                        $('.scorecard_table').html(options);
                    },
                    error: function() {}
                });
            }

            $('.scorecard_list_coursal').each(function() {
                $(this).trigger('change');
            });

            $('.scorecard_list_coursal_text_only').each(function() {
                $(this).trigger('change');
            });

            function selectScoreCardEditTable(value, scoreCardId) {
                $.ajax({
                    dataType: "html",
                    type: "POST",
                    data: {
                        scorecardID: value,
                        vendor_id: '<?= $vendor_id ?>',
                        scoreCardId: scoreCardId
                    },
                    url: "<?php echo AppUrl::bicesUrl('vendorScoreCard/getScoreCardEditList'); ?>",
                    success: function(options) {

                        $('.scorecard_edit_table_' + Number(scoreCardId)).html(options);
                    },
                    error: function() {
                        // Handle errors if necessary
                    }
                });
            }


            function vendScoreCardDelete(key,status) {
                var key = key;
                $.confirm({
                    title: false, 
                    content: '<spam style="font-size:11px">Are you sure you want to delete the Scorecard?</span>',
                    buttons: {
                        Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function() {
                                $.ajax({
                                    dataType: 'json',
                                    type: "post",
                                    url: "<?php echo AppUrl::bicesUrl('vendorScoreCard/vendScoreCardDelete/'); ?>",
                                    data: {
                                        id: key,
                                        vendor_id: '<?= $vendor_id ?>',
                                    },
                                    success: function(option) {
                                        if (option.updateded_res == 1) {
                                            location.href = option.url
                                        }
                                    }
                                });
                            }
                        },
                        No: {
                            text: "Close",
                            btnClass: 'btn-red',
                            action: function() {

                            }
                        },
                    }
                });
            }
            
            $(document).ready(function() {
                // Add minus icon for collapse element which is open by default
                $(".collapse.in").each(function() {
                    $(this)
                    .siblings(".panel-heading")
                    .find(".glyphicon")
                    .addClass("rotate");
                });

                // Toggle plus minus icon on show hide of collapse element
                $(".collapse")
                    .on("show.bs.collapse", function() {
                    $(this)
                        .parent()
                        .find(".glyphicon")
                        .addClass("rotate");
                    })
                    .on("hide.bs.collapse", function() {
                    $(this)
                        .parent()
                        .find(".glyphicon")
                        .removeClass("rotate");
                    });
            });

            $(document).ready(function() {
                $('.edit-scorecard-submit').click(function() {
                    var valid = true;
                    // if($('.scorecard_list_coursal').val() ==""){
                    //   valid = false;
                    //   $('.error-list_coursal').text('This field is required.');
                    // }else{ $('.error-list_coursal').text(''); }

                    $('.scores-validation').each(function() {
                        var inputValue = $(this).val();
                        var errorMessage = $(this).siblings('.error-message');
                        if (inputValue === '' || inputValue === '0') {
                            $(this).prop('required', true);
                            $(this).css('border', '5px solid');
                            errorMessage.text('This option is required.'); 
                            valid = false;
                        }else{
                            $(this).css('border', '1px');
                            errorMessage.text('');
                        }
                    });
                        
                     if(valid){
                      $('.scorecard_form').submit();
                     }
                    });
                });
        </script>
    </div>