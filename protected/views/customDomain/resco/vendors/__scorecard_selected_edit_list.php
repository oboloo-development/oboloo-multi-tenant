<?php
foreach ($allScorecards as $key => $scorecard) { ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $scorecard['name'] ?></h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-md-12 p-0">
                    <div class="row">
                        <div class="col-md-9">
                            <label style="font-size: 13px; padding-left: 5px;">Category/Area</label>
                        </div>
                        <div class="col-md-3 text-center">
                            <label style="font-size: 13px;">Score (%)</label>
                        </div>
                    </div>
                </div>
            </div>
            <?php foreach ($scorecard['scoreCardLists'] as $key => $list) { ?>
              <div class="form-group mt-15">
                <div class="col-md-12 col-sm-12 col-xs-12 p-0">
                 <div style="float: left; width: 80%;">
                  <input type="text" class="form-control notranslate score_name" name="score_name[<?= $scorecard['card_id'] ?>][]" value="<?= $list['category_area']; ?>" readonly />
                 </div>
                 <div style="float: left;width: 20%;">
                  <input type="number" autocomplete="off" class="form-control text-center score score_1 notranslate" name="defualt_score[<?= $scorecard['card_id'] ?>][]" value="<?= $list['default_score']; ?>" min="0" step="0.01" readonly />
                 </div>
                </div>
              </div>

                <?php
                // Check if there are corresponding questions
                if (!empty($scorecard['getQuestion'][$key])) {
                 foreach ($scorecard['getQuestion'][$key] as $getQuestion) {?>
                    <div style="width: 100%; display: flex" class="mt7 scoreCardQuestion_<?= $list['scorecard_id'] ?>_<?= $list['id'] ?>">
                        <input type="text" autocomplete="off" class="form-control question " name="questions[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" placeholder="Type New Criteria Here" value="<?= $getQuestion['question'] ?>" style="flex:3" readonly />
                        <div  style="flex: 1">
                         <input type="hidden" name="score_evaluation[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" value="<?= $getQuestion['score_evaluation'] ?>" />
                         <select class="form-control scores-validation" style="flex: 1" disabled >
                          <option value="0">Select Option</option>
                          <option value="1" <?= ($getQuestion['score_evaluation'] == 1) ? 'selected="selected"' : ''; ?>>1</option>
                          <option value="2" <?= ($getQuestion['score_evaluation'] == 2) ? 'selected="selected"' : ''; ?>>2</option>
                          <option value="3" <?= ($getQuestion['score_evaluation'] == 3) ? 'selected="selected"' : ''; ?>>3</option>
                          <option value="4" <?= ($getQuestion['score_evaluation'] == 4) ? 'selected="selected"' : ''; ?>>4</option>
                          <option value="5" <?= ($getQuestion['score_evaluation'] == 5) ? 'selected="selected"' : ''; ?>>5</option>
                         </select>
                        </div>
                        <textarea name="question_notes[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" class="form-control score score_1 notranslate ml-20" style="flex:2" placeholder="Type Comments"><?= $getQuestion['question_notes'] ?></textarea>
                    </div>
                <?php
                 }
                } else {
                    // If no questions from the first source, use the second source
                    $vendorScoringQuestion = new vendorScoringQuestion();
                    $vendorScoringQuestion = $vendorScoringQuestion->getAll(['scorecard_id' => $list['scorecard_id'], 'vendor_scoring_id' => $list['id'], 'order' => 'id asc']);
                    foreach ($vendorScoringQuestion as $getQuestion) { ?>
                        <div style="width: 100%; display: flex" class="mt7  scoreCardQuestion_<?= $list['scorecard_id'] ?>_<?= $list['id'] ?>">
                            <input type="text" autocomplete="off" class="form-control question" name="questions[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" placeholder="Type New Criteria Here" value="<?= $getQuestion['question'] ?>" style="flex:3" readonly />
                            <div  style="flex: 1">
                             <select name="score_evaluation[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" class="form-control scores-validation" required>
                              <option value="0">Select Option</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                             </select>
                             <span class="error-message" style="color: red;"></span>
                            </div>
                            <textarea name="question_notes[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" class="form-control score score_1 notranslate" style="flex:2" placeholder="Type Comments"><?= $getQuestion['question_notes'] ?></textarea>
                        </div>
                        <?php
                    }
                }
             }
            ?>
        </div>
    </div>
<?php } ?>
<?php 

 if(isset($cardStatus) && $cardStatus == 1){?>
    <script>
        $('document').ready(function(){
            $('.edit-scorecard-submit_<?= $id ?>').attr('disabled', true);
        });
    </script>
<?php } ?>
