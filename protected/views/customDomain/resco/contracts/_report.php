<?php
	$tool_currency = Yii::app()->session['user_currency'];
	$user_currency_symbol = Yii::app()->session['user_currency_symbol'];
	$locLabel = $locSeries = $expLabel = $expSeriesTot = $expSeriesVal =  array();

	$status_labels = "";
	$status_data = "";
	$max_data = 0;
	foreach ($contract_statuses as $total_contracts)
	{
		if ($status_labels == "") $status_labels = "'" . $total_contracts['status'] . "'";
		else $status_labels = $status_labels . ", '" . $total_contracts['status'] . "'";
		
		if ($status_data == "") $status_data = $total_contracts['total'];
		else $status_data = $status_data . "," . $total_contracts['total'];
		
		if ($max_data < $total_contracts['total']) $max_data = $total_contracts['total'];
	}
	
	if ($max_data == 0) $max_data = 1;
	else if ($max_data == 1) $max_data = 2;
	else if ($max_data < 5) $max_data = 5;
	else if ($max_data < 10) $max_data = 10;
	else if ($max_data < 20) $max_data = 20;
	else if ($max_data < 50) $max_data = 50;
	else if ($max_data < 100) $max_data = 100;
	else if ($max_data < 200) $max_data = 200;
	else if ($max_data < 500) $max_data = 500;
	else if ($max_data < 1000) $max_data = 1000;
	foreach ($expiring_contracts as $key=>$expiring_contract_row) {
 	      $expLabel[] = addslashes($key); 
 	      $expSeriesCount[] = intval($expiring_contract_row['expiry_count']);
 	      $expSeriesNoticeCount[] = intval($expiring_contract_row['noticeperiod_count']);
  }

 /* if(isset($expiring_contracts['over12Months'])){
    $expLabel[] = "+12 Months";
    $expSeriesTot[] = $expiring_contracts['over12Months']['total'];
    $expSeriesVal[] = round($expiring_contracts['over12Months']['value'], 2);
  }*/
$locSeries1 = $locSeries2 = $locSeries3 = $locSeries4 = array();
foreach ($contracts_by_location as $a_contract) {
  $locName = addslashes($a_contract['location_name']);
  $locLabel[] = $locName;
  $locSeries1[$locName]= $a_contract['End Date Within 30 Days'];
  $locSeries2[$locName]= $a_contract['End Date Within 3 Months'];
  $locSeries3[$locName]= $a_contract['Passed Contract End Date'];
  $locSeries4[$locName]= $a_contract['End Date Over 3 Months'];
}

$s1Sum = array_sum($locSeries1);
$s2Sum = array_sum($locSeries2);
$s3Sum = array_sum($locSeries3);
$s4Sum = array_sum($locSeries4);

$locSeries = array('End Date Within 30 Days'=>$s1Sum,'End Date Within 3 Months'=>$s2Sum,'Passed Contract End Date'=>$s3Sum,'End Date Over 3 Months'=>$s4Sum);
arsort($locSeries);
$ctr = 1;
foreach($locSeries as $key=>$value){

    if($key=="End Date Within 30 Days"){
      $locSeries["End Date Within 30 Days"] = $locSeries1;
    }else if($key=="End Date Within 3 Months"){
      $locSeries["End Date Within 3 Months"] = $locSeries2;
    }else if($key=="Passed Contract End Date"){
      $locSeries["Passed Contract End Date"] = $locSeries3;
    }else if($key=="End Date Over 3 Months"){
      $locSeries["End Date Over 3 Months"] = $locSeries4;
    }

    if($ctr == 1){
      arsort($locSeries[$key]);
      $locNameArr = array_keys($locSeries[$key]);
      $locLabel = $locNameArr;
    }

    $ctrInner=0;
    foreach($locNameArr as $locKey=>$LocArr){
       $orderValue = $locSeries[$key][$LocArr];
       unset($locSeries[$key][$LocArr]);
       $locSeries[$key][$ctrInner] = $orderValue;
        $ctrInner++;
    }
    $ctr++;
}
$colorArr = array();
$colorArr1  = array_keys($locSeries);
$colorArr[array_keys($colorArr1,'End Date Within 30 Days')[0]] = '#f7778c';
$colorArr[array_keys($colorArr1,'End Date Within 3 Months')[0]] = '#efa65f';
$colorArr[array_keys($colorArr1,'Passed Contract End Date')[0]] = '#aaa';
$colorArr[array_keys($colorArr1,'End Date Over 3 Months')[0]] = '#48d6a8';
 ksort($colorArr);
$category_wise_contract_data = $contracts_by_category;
	$contracts_by_category = array();
	foreach ($category_wise_contract_data as $category_wise_contract_row)
	{
		$category_wise_contract_row['full_category_name'] = $category_wise_contract_row['category_name'];
		$current_category_name = $category_wise_contract_row['category_name']; 
		if (strlen($current_category_name) >= 7) 
		{
			$abbr_category_name = "";
			$current_category_name = ucwords($current_category_name);
			foreach (explode(" ", $current_category_name) as $current_category_word)
			{
				if ($abbr_category_name == "") $abbr_category_name = substr($current_category_word, 0, 1);
				else $abbr_category_name = $abbr_category_name . " " . substr($current_category_word, 0, 1);
			}
			$current_category_name = $abbr_category_name;
		}
		$category_wise_contract_row['category_name'] = $current_category_name;
		$contracts_by_category[] = $category_wise_contract_row;
	}

	// START: Top 10 suppliers Spend with Order Count

	$contCatTotal = array_column($contracts_by_category, 'total','category_name');

	$contCatSeries = array_values($contCatTotal);
	$contCatLabel = array_keys($contCatTotal);

	$contCatFullLabel = array_column($contracts_by_category, 'full_category_name');


	// END: Top 10 suppliers Spend with Order Count

$categoryVendor = $categoryContract = $vendorContractCategory = array();
//$vendorContractCategory = array_keys($count_by_category);
foreach($count_by_category as $value){
  if(!empty($value['total_contracts'])){
    $vendorContractCategory[] =  !empty($value['industry'])?$value['industry']:"No Industry Selected";
    $categoryVendor[] = !empty($value['total_vendors'])?$value['total_vendors']:0;
    $categoryContract[] = !empty($value['total_contracts'])?$value['total_contracts']:0;
  }
}
?>

<div class="row">


<div class="col-md-4 col-sm-4 col-xs-12" style="display: none;" id="table_area_19">
<div class="x_panel tile overflow_hidden">
<div class="x_title">
<h2>Contracts By Status</h2>
<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
<li><a onclick="collapseChart(19);"><i class="fa fa-arrows-h"></i></a></li>
<li><a onclick="exportChart('contracts_by_status');"><i class="fa fa-cloud-download"></i></a></li>
</ul>
<div class="clearfix"></div>
</div>
<div class="x_content">
<table class="tile_info" style="margin-left: 0px;">
		<?php
			$idx = 0;
			foreach (explode(",", $status_labels) as $a_status)
		{
			$a_status = str_replace("'", "", $a_status);
			$a_status = rtrim(ltrim($a_status));
			$idx += 1;

			switch ($idx)
			{
				case 1  : $color = "#F7464A"; break;
				case 2  : $color = "#46BFBD"; break;
				case 3  : $color = "#FDB45C"; break;
				case 4  : $color = "#949FB1"; break;
				case 5  : $color = "#4D5360"; break;
				case 6  : $color = 'antique-white'; break;
				case 7  : $color = 'aqua-marine'; break;
				case 8  : $color = 'bisque'; break;
				case 9  : $color = 'khaki'; break;
				default : $color = 'salmon'; break;
			}
		?>

         		<tr>
            		<td>
              			<i class="fa fa-square" style="color: <?php echo $color; ?> !important;"></i>
              			<?php
              				echo $a_status;
							if (isset($contract_statuses[$a_status]))
								echo ' - (' .  $contract_statuses[$a_status]['total'] . ')';
              			?>
            		</td>
          		</tr>

    <?php
		}
    ?>
</table>
</div>
</div>
</div>

</div>
<div class="clearfix"><br /></div>

<script type="text/javascript">
	function createApexCont(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;
    var distributedL=true;
    console.log(seriesData,seriesMixedData);
    if(chartIDL=="#expiring_contracts"){
    	$.each(series.spend, function(key, value) {
          seriesData.push(value);
        });
      	seriesL.push({'name':'Spend','type':'column','data':seriesData});
      	$.each(series.count, function(key, value) {
          	seriesMixedData.push(value);
        });
      	seriesL.push({'name':'Count','type':'line','colors':'yellow','data':seriesMixedData});
    }else {
    
		$.each(series, function(key, value) {  
        seriesL.push({'name':key,'data':value});
		});
    stackedL= true;
    distributedL = false;

	}

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value);
    });

    if(chartIDL=="#spend_contracts"){horizontalL=true;}
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }},
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        },
        stacked: stackedL,
        //stackType: '100%',
        toolbar: {
            tools: {
                download: false
            }
          },
        },
        responsive: [{
            breakpoint: 580,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        legend: {
                position: 'bottom',
                offsetX: 5,
                offsetY: 0
            },
      colors: colorL,
      plotOptions: {bar: {columnWidth: '40%',distributed: distributedL,horizontal: horizontalL}},
      dataLabels: {enabled: lableL},
      //stroke: { width: [4,  4]}
      // stroke: {width: [1, 1]},
      series: seriesL,
      opposite: true,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      fill: {opacity: 1},
      xaxis: {categories: categoriesL,
      	labels: {
                trim: true,
		        show: false,
		       // minHeight: undefined,
            	//maxHeight: 80,
            	//rotate: -2,

                formatter: function (value) {
                  valueL = chartIDL!="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },}},
     
  	yaxis: {

  		"labels": {
            "formatter": function (val) {
                return  Math.trunc(val);
            }
        }
	  	}
   }
    
    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }

//var colors = [$colorArr[, '#efa65f', '#aaa', '#48d6a8'];
var colors =  [<?php echo '"'.implode('","',$colorArr).'"';?>];
createApexCont(200,'100%','bar',false,<?php echo json_encode($locSeries) ?>,<?php echo json_encode($locLabel);?>,"#contracts_by_location",colors,'Count');


/*createApexCont(200,'100%','bar',false,<?php echo json_encode($contCatSeries) ?>,<?php echo json_encode($contCatFullLabel);?>,"#contracts_by_category",colors,'Count');*/

function createApexChartContCombo(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;

     
    	$.each(series.notice_count, function(key, value) {
          seriesData.push(value);
        });
      seriesL.push({'name':'Contracts with notice period','data':seriesData});

      $.each(series.count, function(key, value) {
          seriesMixedData.push(value);
        });
      seriesL.push({'name':'Contracts expiring','data':seriesMixedData});
     
      
  

    var categoriesL =[];
     $.each(categories, function(key, value) {
      if(value !="+12 Months"){
        valueArr = value.split(' ');
        categoriesL.push(valueArr[0]);
      }else{
         categoriesL.push(value);
      }
    });    
   var options = {
          series: seriesL,
          chart: {
          fontFamily: 'Poppins !important',
          type: 'bar',
          height: 200,
          width:'1100px',
           toolbar: {
          show: false
         }
        },
        colors: colorL,
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '65%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: categoriesL,
        },
        yaxis: {
          labels: {
             formatter: function(val) { return parseInt(val); },
          },
          /*title: {
            text: 'Count'
          },*/
          //tickAmount: 5,
         //  min: 0,
           //max: 20,
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              if(val === null || val =='' || val == 0) {
                return '  NO DATA';
            } else {
                return  val;
            }
              
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#expiring_contracts"), options);
        chart.render();
  }
  colors= ['#ad6eff', '#6eb5ff'];
  
  createApexChartContCombo(183,'100%','line',false,<?php echo json_encode(array('notice_count'=>$expSeriesNoticeCount,'','count'=>$expSeriesCount)) ?>,<?php echo json_encode($expLabel);?>,"#expiring_contracts",colors,'Count');


function barAndLineChart(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    var seriesMixedData =[];
    var chartIDL = chartID;
    var colorL = color;
    var stackedL = false;
    var xaxisL   = true;
    if(chartIDL=="#category_vendors_combo" || chartIDL=="#vendor_contract_category"){
     /* START: Supplier Data Commented

     $.each(series.supplier, function(key, value) {
          seriesData.push(value);
        });
      seriesL.push({'name':'Suppliers','type':'column','data':seriesData});
      END: Supplier Data Commented
      */
       xaxisL=false;
      $.each(series.contract, function(key, value) {
          seriesMixedData.push(value);
        });

      /* START: Supplier Data Commented
      seriesL.push({'name':'Contracts','type':'line','colors':'yellow','data':seriesMixedData}); END: Supplier Data Commented
      */
      seriesL.push({'name':'Contracts','type':'bar','colors':'yellow','data':seriesMixedData}); 
      //xaxisL=false;
    }else if(type=='line'){
        $.each(series, function(key, value) { 
          seriesL.push({'data':value['data'],'name':value['name']});
        });
    }else{
      $.each(series, function(key, value) {  
          seriesData.push(value);
        });
      seriesL.push({'name':toolTip,'data':seriesData});
    }

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value.split(" "));
    });
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }},
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        },
        stacked: stackedL,
        //stackType: '100%',
        toolbar: {
            tools: {
                download: false
            }
          }
        },
        responsive: [{
            breakpoint: 380,
            options: {
                legend: {
                    position: 'top',
                    offsetX: 10,
                    offsetY: 0
                }
            }
        }],
        legend: {
                position: 'top',
                offsetX: 0,
                offsetY: 0
            },
      colors: colorL,
      plotOptions: {bar: {columnWidth: '35%',distributed: true, horizontal: horizontalL}},
      dataLabels: {enabled: lableL},
      //stroke: { width: [4,  4]}
       stroke: {width: [4, 4]},
      series: seriesL,
      opposite: true,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      fill: {opacity: 1},
      xaxis: {categories: categoriesL,labels: {
                formatter: function (value) {
                  valueL = value;
                return valueL
                },
                style: {fontSize: '10px'},
                 show:xaxisL,
                  rotate: 0,
                  /*minHeight: 145,*/
              },
        },
        yaxis: [
                  /*

               /* START: Supplier Date Commented
            { seriesName: 'P',
              
              axisTicks: {show: true,color:'#008FFB'},
              axisBorder: { show: true,color:'#008FFB'},
              title: { text: "Suppliers"},

             min:0,
            tickAmount:2,
              labels: {
                formatter: function (value) {
                  
                  return value.toFixed(0)
                },
              },},

               /* END: Supplier Date Commented
              */
            {   
            seriesName: 'Contracts',
            min:0,
            tickAmount:2,
           /* 
            START: Supplier Date Commented 
            opposite: true,
            END: Supplier Date Commented */
            axisTicks: {
              show: true,
              color: '#00E396'
            },
            axisBorder: {
              show: true,
              color: '#00E396'
            },
            title: { text: "Contracts"},
            labels: {
                formatter: function (value) {
                  return value.toFixed(0)
                },
              },

            }
          ],
      markers: {size: 5,hover: {size: 6},/*colors: ['#9C27B0']*/},
          }
    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }

   barAndLineChart(205,'100%','line',false,<?php echo json_encode(array("supplier"=>$categoryVendor,"contract"=>$categoryContract));?>,<?php echo json_encode($vendorContractCategory);?>,"#vendor_contract_category",colors);


  // START: Pie Chart
    var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 200,
              width: 433,
              type: 'pie',
              //id:"scoringPieChart",

            },
           // legend: {show: false},
            colors:[<?php echo "'".implode("','",$contractCount['contract_color'])."'";?>],
            dataLabels: {enabled: false},
            labels: [<?php echo "'".implode("','",$contractCount['contract_label'])."'";?>],
            series: [<?php echo implode(",",$contractCount['contract_count']);?>],
            responsive: [{
               breakpoint: 433,
                options: {
                    chart: {
                       width: 'auto'
                        //height: 'auto',

                    },
                    legend: {
                        show: false
                    }
                }
            }],
            
        }
        var chart = new ApexCharts(
            document.querySelector("#contract_status_pie_chart"),
            options
        );
         chart.render();
         chart.addEventListener("dataPointSelection", function(event, chartContext, config) {
      // Get the clicked data point index
      var dataPointIndex = config.dataPointIndex;
      var dataPoint = config.w.config;
      showModalWithData(dataPointIndex, dataPoint);
    });

    function showModalWithData(dataPointIndex, dataPoint) {
        var clickedLabel = dataPoint.labels[dataPointIndex];
        var clickedSeries = dataPoint.series[dataPointIndex];
  
        var tableHtml = '<table class="table">';
        tableHtml += '<thead>';
        tableHtml += '<tr><th>' + clickedLabel + '</th></tr>';
        tableHtml += '</thead>';
        tableHtml += '<tbody>';
        tableHtml += '<tr>';
        tableHtml += '<td>' + clickedSeries + '</td>';
        tableHtml += '</tr>';
        tableHtml += '</tbody>';
        tableHtml += '</table>';
        var modalBody = document.createElement('div');
        modalBody.innerHTML = tableHtml;
        var modalElement = document.createElement('div');
        modalElement.classList.add('modal');
        modalElement.innerHTML = '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"></div></div></div>';
        modalElement.querySelector('.modal-body').appendChild(modalBody);

        document.body.appendChild(modalElement);
        $(modalElement).modal('show');
      }





function expandChart(display_index)
{
  var show_index = display_index;
  var hide_index = 0;
  if (display_index % 2 != 0) hide_index = show_index - 1;
  else hide_index = show_index + 1;


  if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
  else
  {
    $('#chart_area_' + hide_index).hide(1000);
    $('#table_area_' + hide_index).hide(1000);
    
    $('#chart_area_' + show_index).show(1000);
    $('#table_area_' + show_index).show(1000);
  }
}
function expandChart1(display_index)
{
  var show_index = display_index;

  if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
  else
  {
    $('#chart_area_' + show_index).hide(1000);
    $('#table_area_' + show_index).show(1000);
  }
}

function collapseChart(display_index)
{
  var show_index = display_index;
  var hide_index = 0;
  if (display_index % 2 != 0) hide_index = show_index - 1;
  else hide_index = show_index + 1;

  $('#table_area_' + show_index).hide(1000);
  $('#table_area_' + hide_index).hide(1000);
  
  if (!$('#chart_area_' + show_index).is(':visible')) 
    $('#chart_area_' + show_index).show(2000);
  if (!$('#chart_area_' + hide_index).is(':visible')) 
    $('#chart_area_' + hide_index).show(2000);
}


function exportChart(canvas_id)
{
  location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id;
}



</script>