<?php
	 $locationID = $contract['location_id'];
	 $departmentID = $contract['department_id'];

     $userPer = new User;
	 $editContract = $userPer->checkPermission(Yii::app()->session['user_id'],$locationID,$departmentID,"contract_edit");
	 $userTypeID = Yii::app()->session['user_type'];
     if($userTypeID =="4" && ($memberType !=1 || $memberType =="0")){
	  $editContract["contract_edit"]= "yes";
	 }

  	$disabled = FunctionManager::sandbox();
	if (isset($contract_id) && $contract_id) {
	if(
		$memberType ==1 ||  
		in_array(Yii::app()->session['user_type'],array(1,3,4)) || 
		Yii::app()->session['user_id']==$contract['user_id'] || 
	    Yii::app()->session['user_id']==$contract['sbo_user_id'] || 
	    Yii::app()->session['user_id']==$contract['commercial_user_id'] || 
	    Yii::app()->session['user_id']==$contract['procurement_user_id']) { ?>
	<div class="form-group">
		<?php if(
			( $memberType ==1 && $this->action->id=='edit' && strtolower($contract['contract_archive']) !='yes' ) ||  (
			$this->action->id=='edit' && 
			strtolower($contract['contract_archive']) !='yes' && 
			!empty($editContract) && 
			$editContract["contract_edit"]=="yes")){?>

		 <div class="col-md-3 col-sm-3 col-xs-6">
			<input type="submit" style="background-color: #1abb9c !important; border-color: #1abb9c !important;" class="btn btn-primary submit-btn" value="<?php if (isset($contract_id) && $contract_id) echo 'Save Contract'; else echo 'Add Contract'; ?>" <?php echo $disabled; ?> />
		 </div>

	   	<?php if (FunctionManager::checkEnvironment(true)) { ?>
	   	
		<div class="col-md-3 col-sm-3 col-xs-6 text-right">

		 <?php if($disabled == 'disabled') { ?>
		  <a  class="btn btn-danger" <?php echo $disabled; ?> />
			 Archive Contract
		  </a> 
		 <?php } else {?>
		  <a href="<?php echo $this->createUrl('contracts/saveArhive',array('contract-id'=>$contract_id));?>" style="background-color: #5bc0de !important; border-color: #5bc0de !important;" class="btn btn-danger" onclick="return confirm('Are you sure you want to archive this contract')" <?php echo $disabled; ?> />
			 Archive Contract
		  </a>
		 <?php } ?>
		 </div>

		 <?php }}

		 if(
		 	( $memberType ==1 && strtolower($contract['contract_archive'])=='yes' ) ||  (
		 	FunctionManager::checkEnvironment(true) && 
		 	strtolower($contract['contract_archive'])=='yes'  && 
		 	!empty($editContract) && $editContract["contract_edit"]=="yes")){ ?>
		 	<div class="col-md-3 col-sm-3 col-xs-12">
		 	<?php if($disabled == 'disabled') { ?>
			  <a  class="btn btn-primary" <?php echo $disabled; ?> />
				 Archive Contract
			  </a> 
		 	<?php } else { ?>
		 		<a href="<?php echo $this->createUrl('contracts/saveArhive',array('contract-id'=>$contract_id,'archive-type'=>'un-archive'));?>" class="btn btn-primary" onclick="return confirm('Are you sure you want to Un-archive this contract')" <?php echo $disabled; ?> />
		 		  Unarchive Contract
		 		</a>
		 	<?php } ?>
		 	</div>
		 <?php } ?>
		 	<?php } ?>
		<div class="clearfix">
			<br /> <br />
		</div>
	</div>
	<?php }
	 ?>

	<div class="clearfix"> <br /> </div>