<?php 

?>
 
<div class="x_title" style="border-bottom:none"><h4 class="heading">Comments</h4></div>
<h4 class="subheading" style="padding-bottom: 10px; padding-left: 6px;">Please add any additional comments that you think are relevant to this contact</h4>
<input type="hidden" class="notranslate" name="contractId" id="contractId" value="<?php if (isset($contract_id) && !empty($contract_id)) echo $contract_id; else echo '0'; ?>" />

<input type="hidden" class="notranslate" name="contract_id" id="contract_id" value="<?php if (isset($contract_id)) echo $contract_id; ?>" /> 
<input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
<input type="hidden" class="notranslate" name="form_comments" id="form_comments" value="1" />

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<label class="control-label notranslate">Comments</label>
		<textarea class="form-control notranslate" name="contract_comments"
			id="contract_comments" rows="4" placeholder="Additional Comments">
      <?php //if (isset($contract['contract_comments']) && !empty($contract['contract_comments'])) echo $contract['contract_comments']; ?></textarea>
	<br><br>
  <table id="table_scoring_criteria" class="table table table-striped table-bordered ">
  <tbody>
    <tr>
    <th>User</th>
    <th>Comments</th>
    <th>Date/Time</th>
    </tr>
    
 <?php  
   foreach($contractCommHistory as $valueCommHistory){?>
 	<tr>
    	<td><?php echo $valueCommHistory['user']; ?></td>
    	<td class="notranslate"><?php echo $valueCommHistory['comment']; ?></td>
    	<td><?php echo $valueCommHistory['datetime']; ?></td>
    </tr>
 <?php } ?>
    

    </tbody>
    </table>

	</div>
</div>
 <div class="clearfix"></div><br />
<?php $this->renderPartial('_submit',array('contract_id'=>$contract_id,'contract'=>$contract));?>
