<!-- START: Scoring Criteria -->
<div class="form-group"><br />
 <h3 style="padding: 10px;">Scoring Criteria</h3><br />
<div class="col-md-2 col-sm-2 col-xs-2 valid"> 
<?php 
$tool_currency = Yii::app()->session['user_currency'];
foreach($scoringCriteria as $value){?>

    <div class="col-md-2 col-sm-2 col-xs-2 valid">
    
    </div>
   
    <div class="clearfix"></div>
<?php }?></div>
<?php $i=1;?>
    <div class="clearfix"></div>
</div>
<form action="" id="scoring_criteria_form">
  <input type="hidden" name="quote_id" id="quote_id" value="<?php if (isset($quote['quote_id'])) echo $quote['quote_id']; ?>" />
<div class="table-responsive">
 <table class="table">
  <tbody>
    <tr>
    <th>Scoring Criteria</th>
    <th>Scoring Percentage</th>
    <?php foreach($quoteVendors as $valueVend){?>
        <th><?php echo $valueVend["vendor_name"];?></th>
    <?php } ?>
    </tr>
    <?php $i=1;$checkScoringCriteria=0;foreach($scoringCriteria as $value){
        if($value['score']>0){ $checkScoringCriteria=1;?>
    <tr>
      <th style="max-width: 27%;"><input type="text" class="form-control" disabled="disabled" value="<?php echo $value['value'];?>" style="padding:5px;"></th>
      <th style="max-width: 27%;"><input type="text" name="score_percentage[<?php echo $value['id'];?>]" class="form-control" placeholder="Enter Scoring Percentage" readonly="disabled" value="<?php echo $value['score'];?>" onkeypress="return isNumberKey(event)" required style="padding:5px;"></th>
    <?php $row=1;
        foreach($quoteVendors as $valueVend){?>
        <td class="text-left"><select required name="score_criteria[<?php echo $value['id'];?>][<?php echo $valueVend['vendor_id'];?>]"  class="score_<?php echo $i;?> vendor_<?php echo $valueVend['vendor_id'];?>  form-control" onChange="<?php /*checkUniqScore(this,<?php echo $i;?>);*/?>calculateVendScore(<?php echo $valueVend['vendor_id'];?>)">
                  <option value="">Select</option>
                  <?php for($j=1; $j<=count($quoteVendors); $j++) { ?>
                      <option value="<?php echo $j; ?>" <?php if (false) echo ' selected="SELECTED" '; ?>>
                          <?php echo $j; ?>
                      </option>
                  <?php } ?>
              </select>
              <span></span>
          </td>
    <?php $row++;} $i++; ?></tr> <?php } } ?>


    <tr><th colspan="2" style="text-align: right;"><h3>Total</h3></th>
    <?php foreach($quoteVendors as $valueVend){?>
        <th> <input type="text" class="form-control" readonly="readonly" value="" id="vendor_<?php echo $valueVend['vendor_id'];?>" style="padding:5px;"></th>
    <?php } ?>
    </tr>
    <?php if(!empty($checkScoringCriteria)){?>
    <tr><td colspan="<?php echo count($quoteVendors)+2;?>" class="text-right"><input type="submit" onClick="saveScoringCriteria(event)" title="Click here to save socring criteria" style="cursor: pointer;" class="btn btn-success" value="Submit" name="scoring_criteria_save" style="background: #2d9ca2 !important;"></td></tr>
   <?php }else{?>
    <tr class="odd"><td valign="top" colspan="<?php echo 2+count($quoteVendors);?>" class="dataTables_empty">No data available in table</td></tr>
  <?php } ?>

</tbody>
</table>
<div class="msg_area"></div>

</div></form>
  <!-- START: Scoring Criteria Graph-->
   <?php if(!empty($checkScoringCriteria)){?>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel tile overflow_hidden">
      <div class="x_title">
        <h4>Sourcing Activity Selection Criteria (%)</h4>
        <div class="clearfix"></div>
      </div>
      <div class="x_content"><div id="scoring_pie_chart"></div><br /></div>
    </div></div>

  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel tile overflow_hidden">
      <div class="x_title">
        <h4>Supplier Criteria Scoring</h4>
        <div class="clearfix"></div>
      </div>
      <div class="x_content"><div id="scoring_chart"></div></div>
    </div></div>
<?php } ?>
  <!-- End: Scoring Criteria Graph-->
  <div class="clearfix"></div><br /><br />
  <div class="msg_area"></div>
 <div class="clearfix"></div>
  <?php  $this->renderPartial('_anwser_supplier',array('quoteAnswerVendor'=>$quoteAnswerVendor));?>
   <div class="clearfix"></div>
 <!--  <button type="button" class="btn btn-info  pull-right" id="btn-answer" onclick="$('#anwser_supplier_modal').modal('show');" style="position: absolute;right: 0;"><span class="fa fa-list-alt"></span> Supplier Questionnaire Answers</button> -->
  <div class="clearfix"></div>
 <div class="clearfix"></div>
  <div id="scoring_list_table">

  </div>

  
  <div id="edit_score" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Edit Scoring Criteria</h4></div>
                  <div class="modal-body">
                    <form id="edit_score_form" action="" method="post">
                   </form>
                    <div class="edit_msg_area"></div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button>
                    <button  type="button" class="btn btn-primary btn sm" onclick="editScoring('edit_score_form');">Edit</button></div></div></div></div></div>

      <div class="clearfix"></div><br />
         <div class="form-group">
          <h3 style="padding: 10px 10px 10px 10px;">Supplier Total Cost Comparison</h3><br />
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php if (isset($quote_vendors) && is_array($quote_vendors) && count($quote_vendors)){
                    foreach ($quote_vendors as $quote_vendor){?>
                      <label class="control-label sup_evaluation"><input type="checkbox" name="evalution_vendor_id[]" id="evalution_vendor_id" onchange="displaySupplierEvaluation(<?php echo $quote_id; ?>);" value="<?php echo $quote_vendor['vendor_id']; ?>" > <?php echo $quote_vendor['vendor_name']; ?></label><br />
          <?php } } ?><br />
             </div>
         </div>
         <div class="clearfix"></div>
         <div id="vendor_evaluation_area">
             <div class="row">
             <div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_8">
                 <div class="x_panel tile overflow_hidden">
                     <div class="x_title">
                         <h4>Supplier Products</h4>
                         <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                             <li><a onclick="expandChart(8);"><i class="fa fa-arrows-h"></i></a></li>
                             <li><a onclick="exportChart(<?php echo $quote_id; ?>,1);"><i class="fa fa-cloud-download"></i></a></li>
                         </ul>
                         <div class="clearfix"></div>
                     </div>
                     <div class="x_content" style="text-align: center;">
                         <div id="chart" height="140px"></div>
                     </div>
                 </div>
             </div>
             <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_8">
                 <div class="x_panel tile overflow_hidden">
                     <div class="x_title">
                         <h4>Top Products</h4>
                         <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                             <li><a onclick="collapseChart(8);"><i class="fa fa-arrows-h"></i></a></li>
                             <li><a onclick="exportChart(<?php echo $quote_id; ?>,1);"><i class="fa fa-cloud-download"></i></a></li>
                         </ul>
                         <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                         <table class="tile_info" style="margin-left: 0px;">
                             <?php

                             $idx = 0;
                             foreach ($quote_stacked_bar_graph['products'] as $dataset_idx => $a_dataset)
                             {
                                 if ($dataset_idx === 'total') continue;
                                 $idx += 1;
                                 switch ($idx)
                                 {
                                     case 1  : $color = 'salmon'; break;
                                     case 2  : $color = 'purple'; break;
                                     case 3  : $color = 'green'; break;
                                     case 4  : $color = 'blue'; break;
                                     case 5  : $color = 'red'; break;
                                     case 6  : $color = 'antique-white'; break;
                                     case 7  : $color = 'aqua-marine'; break;
                                     case 8  : $color = 'bisque'; break;
                                     case 9  : $color = 'khaki'; break;
                                     default : $color = 'turquoise'; break;
                                 }
                                 ?>
                                 <tr>
                                     <td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
                                     <td style="vertical-align: middle;">
                                         <?php echo $a_dataset['product_name']; ?> - <?php echo /*Yii::app()->session['user_currency_symbol'] . '' .*/ number_format($a_dataset['product_price'], 2); ?>
                                     </td>
                                 </tr>
                                 <?php
                             }
                             ?>
                         </table>
                     </div>
                 </div>
             </div>
             <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_9">
                 <div class="x_panel tile overflow_hidden">
                     <div class="x_title">
                         <h4>Top Products</h4>
                         <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                             <li><a onclick="collapseChart(9);"><i class="fa fa-arrows-h"></i></a></li>
                             <li><a onclick="exportChart(<?php echo $quote_id; ?>,2);"><i class="fa fa-cloud-download"></i></a></li>
                         </ul>
                         <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                         <table class="tile_info" style="margin-left: 0px;">
                             <?php

                             $idx = 0;
                             foreach ($quote_answer_line_graph['data'] as $dataset_idx => $a_dataset)
                             {
                                 if ($dataset_idx === 'total') continue;
                                 $idx += 1;
                                 switch ($idx)
                                 {
                                     case 1  : $color = 'salmon'; break;
                                     case 2  : $color = 'purple'; break;
                                     case 3  : $color = 'green'; break;
                                     case 4  : $color = 'blue'; break;
                                     case 5  : $color = 'red'; break;
                                     case 6  : $color = 'antique-white'; break;
                                     case 7  : $color = 'aqua-marine'; break;
                                     case 8  : $color = 'bisque'; break;
                                     case 9  : $color = 'khaki'; break;
                                     default : $color = 'turquoise'; break;
                                 }
                                 ?>
                                 <tr>
                                     <td style="vertical-align: middle;">
                                        <?php echo $a_dataset['question']; ?> ?
                                     </td>
                                 </tr>
                                 <tr>
                                     <td>
                                         <?php
                                         if($a_dataset['question_type']=='free_text'){
                                             $answer = $a_dataset['free_text'];
                                         }elseif($a_dataset['question_type']=='yes_or_no'){
                                             $answer = $a_dataset['yes_no'];
                                         }else{
                                             //$quote = Quote();
                                             //$answer = $quote->getAnswer($a_dataset['quote_answer']);
                                             $answer = $a_dataset['quote_answer'];
                                         }
                                         ?>
                                         <table class="tile_info" style="margin-left: 0px;">
                                             <tr> s
                                                 <td style="width: 1%;">
                                                     <i  style="margin-left:5px;background-color:<?php echo $color; ?>;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
                                                 </td>
                                                 <td >
                                                     <?php echo $answer; ?>
                                                 </td>
                                             </tr>
                                             <tr><td colspan="2" style="height: 10px;">&nbsp;</td></tr>
                                         </table>
                                     </td>
                                 </tr>
                                 <?php
                             }
                             ?>
                         </table>
                     </div>
                 </div>
             </div>

         </div>
         <div class="row">
             <div class="col-md-12 col-sm-12 col-xs-12" id="table_area_10">
                 <div class="x_panel tile overflow_hidden">
                     <div class="x_title">
                         <h4>Supplier Analysis</h4>
                         <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                             <li><a onclick="collapseChart(10);"><i class="fa fa-arrows-h"></i></a></li>
                             <li><a onclick="exportChart(<?php echo $quote_id; ?>,1);"><i class="fa fa-cloud-download"></i></a></li>
                         </ul>
                         <div class="clearfix"></div>
                         
                     </div>
                     <div class="x_content">
                         <table class="table tile_info" id="simpleTable1"  style="margin-left: 0px;">
                             <?php
                             $heatMapSeries = '';
                             $vendor = array_unique($quote_stacked_bar_graph['vendor_name']);
                             $idx = 0; 
                             echo "<tr><th>Product Name</th>";
                             $vendorProduct = $quote_stacked_bar_graph['vendorProduct'];
                             asort($vendorProduct);
                             foreach ($vendor as $vendor_name)
                             { ?><th><?php echo $vendor_name;?></th><?php }
                              echo "</tr>";

                            foreach ($vendorProduct as $product_name=>$vendorInfo){?>
                             <tr><th><?php echo $product_name;$heatMapSeries.="{name:'".$product_name."',data:["; ?></th>
                              <?php foreach ($vendor as $vendor_name){?>
                                    <th><?php 
                                    $productPrice = $vendorProduct[$product_name][$vendor_name];
                                    $productPrice = number_format($productPrice*FunctionManager::currencyRate($tool_currency),0,".","");

                                    echo /*html_entity_decode(Yii::app()->session['user_currency_symbol']).*/$productPrice;
                                    $heatMapSeries.=$productPrice.",";
                                    ?></th>
                            <?php } $heatMapSeries.="]},"; echo "</tr>";}?>

                         </table>
                     </div>
                 </div>
             </div>
           </div></div>
<style type="text/css">
    th,td {cellspacing:10px;}
</style>
<script type="text/javascript">
    function checkUniqScore(obj,rowNumber){
        rowArr = [];
        var obj = obj;
        var rowNumber = rowNumber;
        $('.score_'+rowNumber).each(function(index,element) {
          if(jQuery.inArray($(this).val(), rowArr) !== -1){
                currValue = $(this).val();
              
               $('option:selected',obj).prop("selected", false);
               $(obj).next('span').html("<b style='color:red'>This Score ("+currValue+") is already selected for the same criteria. Try the other</b>");
              }else{
                 $(obj).next('span').html("");
              }

               rowArr.push($(this).val()) 
        });
    }

    function checkUniqScoreEdit(obj,rowNumber){
      rowArr = [];
      var obj = obj;
      var rowNumber = rowNumber;

      $('.scoreedit_'+rowNumber).each(function(index,element) {
        if(jQuery.inArray($(this).val(), rowArr) !== -1){
              currValue = $(this).val();
            
             $('option:selected',obj).prop("selected", false);
             $(obj).next('span').html("<b style='color:red'>This Score ("+currValue+") is already selected for the same criteria. Try the other</b>");
            }else{
               $(obj).next('span').html("");
            }

             rowArr.push($(this).val()) 
      });
    }
function calculateVendScore(vendID){
    var vendTotalScore = 0;
   $('.vendor_'+vendID).each(function(index,element) {
        currValue = parseInt($(this).val());
        if(currValue>0){
            vendTotalScore=parseInt(vendTotalScore)+currValue;
        }
    });
   
   $('#vendor_'+vendID).val(vendTotalScore);
}
$('.msg_area').hide();
function saveScoringCriteria(e)
{   e.preventDefault();
    $('#loading_icon').show();

    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/saveScoringCriteria'); ?>',
        type: 'POST', async: false, dataType: 'json',
        data: $("#scoring_criteria_form").serialize(),
        success: function(data) {
            loadScoring();
            $(".msg_area").html('<div id="edit_msg_area" class="alert alert-success" role="alert">'+data.msg+'</div>');
            $(".msg_area").show().delay(5000).fadeOut();
            location.reload();
        }
    });
    createLocationChart();
    checkScoringSubmission();

}

function checkScoringSubmission()
{   
    var quoteID = $("#quote_id").val();

    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/checkScoringSubmission'); ?>',
        type: 'POST',
        data: {quote_id:quoteID},
        success: function(data) {
            if(data=="0"){
              $('#scoring_criteria_form').hide();
            }else{

            }
            $("#scoring_list_table").show(data);
        }
    });
}

checkScoringSubmission();


function loadScoring()
{  
    var quoteID = $("#quote_id").val();

    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/listScoring'); ?>',
        type: 'POST',
        data: {quote_id:quoteID},
        success: function(data) {
            $("#scoring_list_table").html(data);
        }
    });

}

function loadEditForm(e,score)
{  
    e.preventDefault();
    var scoreID = score;
    var quoteID = $("#quote_id").val();

    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/editScoring'); ?>',
        type: 'POST',
        data: {score_id:scoreID,quote_id:quoteID},
        success: function(data) {
          $('#edit_score_form').html(data);
          $('#edit_score').modal('show'); 
           // $("#scoring_list_table").html(data);
        }
    });

}

function editScoring(formID)
{ 
  var formID = formID;
  var quoteID = $("#quote_id").val();
  $('#loading_icon').show();
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/saveEditScoring'); ?>',
        type: 'POST', async: false, dataType: 'json',
        data: $("#"+formID).serialize(),
        success: function(data) {

           $('#edit_score').modal('hide');
            loadScoring();
            $(".msg_area").html('<div id="edit_msg_area" class="alert alert-success" role="alert">'+data.msg+'</div>');
            $(".msg_area").show().delay(5000).fadeOut();
            location.reload();
        }
    });
    createLocationChart();
    checkScoringSubmission();

}
loadScoring();

function createLocationChart()
{   

    var quote_id = $("#quote_id").val();
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/listScoreDataChart'); ?>',
        type: 'POST', dataType: 'json',
        data: { quote_id: quote_id },
        success: function(chart_data) {
       var score_values = chart_data.score_values;
       var score_labels = chart_data.score_labels;
       var pie_score_series = [];
       var pie_score_label  = chart_data.pie_score_label;
      for (i = 0; i < chart_data.pie_score_series.length; i++) {
          pie_score_series.push(parseFloat(chart_data.pie_score_series[i]));
      }
      
      /**/
          var options = {
            chart: {
                height: 267,
                type: 'bar',
               // stacked: true,
               // stackType: '100%',
                id:"apexChartVendDeptscoring"
            },
            plotOptions: {bar: {distributed: true}},
            responsive: [{
                breakpoint: 380,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            series: [{name:'Score',data: score_values}],
            xaxis: {
                categories: score_labels,

                /*labels: {
                //trim: true,
            show: true,
            minHeight: undefined,
              maxHeight: 70,
              rotate: -5,
         
            }*/},
            yaxis:{ 
            tickAmount: 10,
            min: 0,
            max: 100,
            labels: {
              formatter: function (value) {
                  valueL = value+"%";
                  return valueL;
                },
             }
      },

      dataLabels: {
        enabled: false,
        enabledOnSeries: false,
        formatter: function(value, { seriesIndex, dataPointIndex, w }) {
          return /*w.config.series[seriesIndex].name+": "+*/w.config.series[seriesIndex].data[dataPointIndex]+"%";
        },
       /* formatter: function (val, opts) {
          
            return val;
        },*/
      },
      tooltip: {
          y: {
           /*formatter: function (value) {
                valueL = "<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },*/
          },
          marker: {
          show: true,
          },
      },
            fill: {
                opacity: 1
            },
            legend: {
                position: 'bottom',
                offsetX: 0,
                offsetY: 0
            },
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_chart"),
            options
        );
        chart.render();
        
         ApexCharts.exec('apexChartVendDeptscoring', 'updateOptions', {
            xaxis: {
              categories: score_labels,
            }
          }, false, true);
          ApexCharts.exec('apexChartVendDeptscoring', 'updateSeries', score_values,true);
    
    // START: Pie Chart
    var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 260,
              /*width: 210,*/
              type: 'pie',
              id:"scoringPieChart"

            },
           // legend: {show: false},
            dataLabels: {enabled: false},
            labels: pie_score_label,
            series: pie_score_series,
            responsive: [{
               breakpoint: 380,
                options: {
                    chart: {
                       width: 'auto'
                        //height: 'auto',

                    },
                    legend: {
                        show: false
                    }
                }
            }],

            tooltip: {
            y: {
                formatter: function(value) {
                 return value+"%";
               },
                 
            },}
            
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_pie_chart"),
            options
        );
         chart.render();

          ApexCharts.exec('scoringPieChart', 'updateOptions', {
           labels:  pie_score_label,
          }, false, true);
          ApexCharts.exec('scoringPieChart', 'updateSeries', pie_score_series,true);

    // END: Pie Chart
       }
      });

  }
  createLocationChart();

 var colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];
  var options = {
             chart: {
                height: 350,
                type: 'bar',
                 stacked: true,

            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '10%',
                    //endingShape: 'rounded'  
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [<?php echo $quote_stacked_bar_graph['datasets']; ?>],
            xaxis: {
                categories: [<?php echo "'".implode("','",explode(',',$quote_stacked_bar_graph['vendors']))."'"; ?>],
                labels: {
                    style: {
                        colors: colors,
                        fontSize: '12px',
                    }
                }
            },
             /*yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },*/
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return /*"$ " +*/ val /*+ " thousands"*/
                    }
                }
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#chart"),
            options
        );

        chart.render();

 
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 
        && (charCode < 48 || charCode > 57))
         return false;
      return true;
   }
       
    </SCRIPT>