
<?php  
$userObj = new User();
$usersName = $userObj->getOne(array('status' => 1, 'admin_flag' => 0));
// echo $usersName['full_name']; exit;
$user_currency_symbol = Yii::app()->session['user_currency_symbol'];
$topContractsTotal = array_column($top_contracts, 'total','dim_1');
foreach($topContractsTotal as $key=>$value)
{
    if(empty($key))
        unset($topContractsTotal[$key]);
    else if($key=="Total")
        unset($topContractsTotal[$key]);
}
$top_contract_series = array_values($topContractsTotal);
$top_contract_label = array_keys($topContractsTotal);
?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
    	 <?php $alert=Yii::app()->user->getFlash('success');
          if(!empty($alert)) { ?>
          <div class="clearfix"></div>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $alert;?></div>
        <?php } ?>
        <div class="col-md-5 pull-left">
            <h3>Contract Management</h3>
        </div>
         <div class="col-md-3 highlight-create" style="" >
        </div>
        <div class="col-md-4 contract-btn-tour-cont text-right">

        	 <button type="button" class="btn btn-warning click-modal-poup contract-btn-tour hidden-xs" onclick="addContractModal(event);">
                <span class="glyphicon glyphicon-plus mr-1" aria-hidden="true"></span> Add Contract
            </button>

			<?php if (isset(Yii::app()->session['admin_flag']) && Yii::app()->session['admin_flag'] == 1) { ?>
				<!--<a href="<?php /*echo AppUrl::bicesUrl('contracts/import'); */?>">
					<button type="button" class="btn btn-default">
						<span class="glyphicon glyphicon-import" aria-hidden="true"></span> Upload CSV
					</button>
				</a>-->
			<?php } ?>

           <!--  <a href="<?php //echo AppUrl::bicesUrl('contracts/export'); ?>">
                <button type="button" class="btn btn-success">
                    <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export CSV
                </button>
            </a> -->

            <a href="<?php echo AppUrl::bicesUrl('contracts/archiveList'); ?>" class="mbl-vw-btn">
                <button type="button" class="btn btn-info mbl-vw-btn">
                    <span class="glyphicon glyphicon-list mr-2" aria-hidden="true"></span> Archived Contracts
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
         
    </div>
    	 <div class="clearfix"> </div>
    	 <div class="row-fluid" style="margin-bottom: 10px">
          <div class=" tile_count">
            <div class="col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats1 " style="background-color: #48d6a8 !important">
            	<h4 onclick="$('#contract_active_value_modal').modal('show');" class="contract-metrice">
            		<?php echo number_format($metrics['contract']['active_count']); ?><br />
            		End Date Over 3 Months</h4>
            </div>
         
            <div class="col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats2" style="background-color: #efa65f !important">
         		<h4 onclick="$('#contract_expire_91days_modal').modal('show');" class="contract-metrice">
            		<?php echo number_format($metrics['contract']['expire_later_count']); ?><br />
            		End Date Within 3 Months</h4>
            </div>
            
              <div class="col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats3" style="background-color: #f7778c !important">
            	<h4 onclick="$('#contract_expire_30days_modal').modal('show');" class="contract-metrice">
            		<?php echo number_format($metrics['contract']['expire_current_count']); ?><br />
            		End Date Within 30 Days</h4>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats4" style="background-color: #aaa !important" >
            		<h4 onclick="$('#contract_list_expire_only').modal('show');" class="contract-metrice" ><?php echo number_format($metrics['contract']['expired_only_count']); ?><br />
            		Passed Contract End Date</h4>            	  	
            </div>
         </div>
     </div>
     <div class="clearfix"> </div>
				<div class="row contract-graphs">
						<div class="col-md-12 col-sm-12 col-xs-12 hidden-xs">
						      <div class="x_panel tile ">
			                <div class="x_title">
			                  <h2 class="pull-left">Contracts Expiring Or That Have A Notice Period In the Next 12 Months</h2>
			                  <h2 class="pull-left"><a class="btn btn-success expiring-contract-btn" onclick="$('#expiring_contract_modal').modal('show');">Contract List</a></h2>
								<div class="clearfix"></div>
			                <!--   <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								<li><a onclick="$('#expiring_contract_modal').modal('show');" style="color:#2d9ca2 ">Contract List</a></li>
			                    <li><a onclick="exportChart('expiring_contracts');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul> -->
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
			                	<div id="expiring_contracts"></div>
			              	</div>
			           	</div>
						</div>
						<div class="clearfix"></div>

   <div class="col-md-6 col-sm-6 col-xs-12" >
   		
         <div class="x_panel notification_contract contract-notifications" id="notification_scroll" style="height: 598px !important; margin-top: 0px;">
                <div class="x_title">
                  <h2>Recent Contract Notifications </h2>
                  <h2 class="pull-right"><a href="<?php echo AppUrl::bicesUrl('notifications/list'); ?>" class="btn btn-success expiring-contract-btn">All Notifications</a></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">

                    <?php foreach ($notifications as $notification) { 
               /* $str = $notification['notification_text'];
                $start = strpos($str,"<a");
                $end   = strpos($str,"</a>");
                $parsedStr = substr_replace($str,'',$start,$end);
                $notificationLabelArr[$parsedStr] = $parsedStr;*/
            ?>
              <li>
                <div class="block">
                  <div class="block_content">
                    <h2 class="title">
						         <div class="row">
                     <div class="col-md-2">
                      <?php 
                      $btnStyle="background-color:#fdaaaa;border-color: #fdaaaa;font-size:8px;";
                      $btnText = "Contract";
                      ?>
                      <button class="btn btn-success" style="<?php echo $btnStyle;?>"><?php echo $btnText; ?></button>
                      </div>
                      <div class="col-md-10">
                      <?php  
                      // echo "<pre>"; print_r($notification); exit; 
                      $str = $notification['notification_text'];
                      $wordsNumber = 1000;
                      echo FunctionManager::getNWordsFromString($str,$wordsNumber); ?>
                      <div class="byline">
                      <span><?php echo Notification::getTimeAgo($notification['notification_date']); ?></span>
                      </div>
                      </div>
                      </div>
                     <!-- End :row -->
                   	</h2>
                  </div>
                </div>
              </li>
          <?php } ?>
          </ul><div class="clearfix"></div></div><div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
	  <div class="col-md-6 col-sm-6 col-xs-12" >
	  	<div class="col-md-12 col-sm-12 col-xs-12 mbl-vw hidden-xs" id="chart_area_17" >
				  <div class="x_panel tile rem0-mr10" style="margin-left: 10px;">
					<div class="x_title">
					 <h2>Contracts By Location</h2>
					<!--  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
					<li><a onclick="expandChart(20);"><i class="fa fa-arrows-h"></i></a></li> -->
					<!-- <li><a onclick="exportChart('contracts_by_location');"><i class="fa fa-cloud-download"></i></a></li> -->
					<!--  </ul> -->
					<div class="clearfix"></div>
					 </div>
					<div class="x_content">
					<div id="contracts_by_location"></div>
					</div>
					</div>

					</div>
       <div class="col-md-12 col-sm-12 col-xs-12 mbl-vw"><div class="x_panel tile rem0-mr10" style="margin-left: 10px;"><div class="x_title"><h2>Contracts By Status</h2><div class="clearfix"></div></div><div class="x_content"><div id="contract_status_pie_chart"></div><br /></div></div></div>

				

					    <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_17">
						 <div class="x_panel tile ">
							<div class="x_title">
								<h2>Expiring Contracts </h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(17);"><i class="fa fa-arrows-h"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php
									$idx = 0;
									foreach ($expiring_contracts as $dataset_idx=>$a_expiring)
									{
										if ($dataset_idx === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											case 10  : $color = 'blue'; break;
											case 11  : $color = 'purple'; break;
											case 12  : $color = 'green'; break;
											case 13  : $color = 'turquoise'; break;
											case 14  : $color = 'red'; break;
											default : $color = 'salmon'; break;
										}
										?>

										<tr>
											<td style="width: 5%;"><i class="fa fa-square" style="color: <?php echo $color; ?>!important;"></i></td>
											<td style="vertical-align: middle;">
												<?php echo !empty($a_expiring['month_name'])?addslashes($a_expiring['month_name']):""; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format((!empty($a_expiring['value'])?$a_expiring['value']:0) ); ?>
											</td>
										</tr>

										<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>
			        </div>

		            </div>
		            <div class="clearfix"><br /></div>
		           <!--  <div class="row tile_count"><hr style="border: 1px solid #2d9ca2;" /></div> -->
		            <?php echo $this->renderPartial("_report",array('contracts_by_location'=>$contracts_by_location,'contracts_by_category'=>$contracts_by_category,'contract_statuses'=>$contract_statuses,'expiring_contracts'=>$expiring_contracts,'count_by_category'=>$count_by_category,'contractCount'=>$contractCount));?>
		            <div class="clearfix"></div>
		            <!-- <ul class="contract_status_colors">
		            	<li class="contract_status1">Green = Active</li>
		            	<li class="contract_status2">Amber = Expiring in 3 Months</li>
		            	<li class="contract_status3">Red = Expiring in 30 Days</li>
		            	<li class="contract_status4">Grey = Expired </li>
		            	<div class="clearfix"></div>
		            </ul><div class="clearfix"></div> -->       
	<form class="form-horizontal" id="contract_list_form" name="contract_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('contracts/list'); ?>">
		
			<div class="col-md-2 col-sm-2 col-xs-12 location">
				<div class="form-group">
				<select name="location_id[]" id="location_id" class="form-control select_location_multiple border-select"  searchable="Search here.."  
				onchange="loadDepartments(0);" >
					<option value="0">All Locations</option>
					<?php
					$i = 0 ;
					foreach ($locations as $location) {
						if(isset($location_id[$i])){
							?>
							<option value="<?php echo $location['location_id']; ?>"
								<?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
								<?php echo $location['location_name']; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $location['location_id']; ?>">
								<?php echo $location['location_name']; ?>
							</option>
						<?php } ?>
						<?php
						$i++;
					} ?>
				</select>
			</div></div>
			<div class="col-md-2 col-sm-2 col-xs-12 department">
				<div class="form-group">
				<select name="department_id[]" id="department_id" class="form-control select_department_multiple border-select" searchable="Search here..">
					 <option value="">All Departments</option>
					<?php if(!empty($department_info))
                        foreach($department_info as $dept_value){?>
                        <option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
                    <?php } ?>
                    
				</select> 
			</div></div>
		
			<div class="col-md-2 col-sm-2 col-xs-12">
			<div class="form-group">
			
				<select name="category_id" id="category_id" class="form-control select_category_multiple border-select" onchange="loadSubcategories(0);"  searchable="Search here..">
					<option value="0">All Categories</option>
					<?php foreach ($categories as $category) { ?>
						<option value="<?php echo $category['id']; ?>"
								<?php if (isset($category_id) && $category_id == $category['id']) echo ' selected="SELECTED" '; ?>>
							<?php echo $category['value']; ?>
						</option>
					<?php } ?>
				</select>
			</div></div>
			<div class="col-md-2 col-sm-2 col-xs-12">
				<div class="form-group">
				<select name="subcategory_id" id="subcategory_id" class="form-control select_subcategory_multiple border-select">
					<option value="0">All Subcategories</option>
				</select></div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12" >
				<div class="form-group">
					<select name="contract_status[]" class="form-control select_status_multiple" id="contract_status"
						class="form-control">
						<option value="">All Status</option>
          	  	  <?php 
          	  	  $contractStatus = FunctionManager::contractSearchStatus();
          	  	  foreach ($contractStatus as $key=>$status) { ?>
          	  	  		<option value="<?php echo $key; ?>"
							<?php if (isset($_POST['contract_status']) && in_array($key,$_POST['contract_status'])) echo ' selected="SELECTED" '; ?>>
          	  	  			<?php echo $status; ?>
          	  	  		</option>
          	  	  <?php } ?>
          	  </select>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 search-contract text-left" style="padding: 0px;">
				<button class="btn btn-info" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
				<button class="btn btn-primary " onclick="loadContracts(); return false;" style="margin-right: -7px">Apply filters</button>
			</div>
			
		</form>
		<div class="clearfix"></div>
	<div class="table-responsive">
    <table id="contract_table" class="table table-striped table-bordered contract-table" style="width: 100%;">
      <thead>
        <tr>
          <th style="width: 6% "> </th>
          <th  style="width: 14%">Title</th>
          <th style="width: 13% ">Category</th>
          <!-- <th>Location</th> -->
          <th class="th-center managedBy" style="width: 11%;">Managed By</th>
          <th style="width: 10%; text-align: center;" >Supplier</th>
          <th class="th-center" style="width: 7%">Currency</th>
          <th class="th-center" style="width: 10%">Estimated Value</th>
          <th class="th-center" style="width: 9%">Start Date</th>
          <th class="th-center" style="width: 11 %">Notice Period Date</th>
          <th class="th-center" style="width: 9%">End Date</th>

		  <!-- <th>Amount in Tool Currency</th>
		  <th>Estimated Total Contract Value</th> -->
        </tr>



      </thead>

      <tbody>
      </tbody>

  </table>
</div>
</div>
<style>
	 
	 
	 
	.btn .caret {
		float: right;
		margin-top: 10px;
	}
 
	.location  .multiselect {
		width: 138%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
		min-width: 135px;
	}
	/*.select2-container .select2-selection--multiple {border-radius: 10px !important;border: 1px solid #4d90fe !important;}*/
	.btn .caret {
		float: right;
		margin-top: 10px;
	}
	td{font-weight: normal;}
	.select2-selection__arrow { display: none !important; }
</style>
</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );



function loadContracts()
{
	$('#contract_table').DataTable().ajax.reload();
}

function clearFilter() {
  $("option:selected").removeAttr("selected");
  $('.select_location_multiple').trigger("change");
  $('.select_department_multiple').trigger("change");
  $('.select_category_multiple').trigger("change");
  $('.select_subcategory_multiple').trigger("change");
  $('.select_status_multiple').trigger("change");
  $('#contract_table').DataTable().ajax.reload();
}


$(document).ready( function() {

	  select2function('select_location_multiple','All Locations');
  	select2function('select_department_multiple','All Departments');
  	select2function('select_category_multiple','All Categories');
  	select2function('select_subcategory_multiple','All Sub Categories');
  	select2function('select_status_multiple','All Status');

	<?php if (isset($category_id) && !empty($category_id)) { ?>
		<?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>
			loadSubcategories(<?php echo $subcategory_id; ?>);
		<?php } else { ?>
			loadSubcategories(0);
		<?php } ?>
	<?php } ?>

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			// loadDepartments(<?php echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>
 

  $('#contract_table').dataTable({
        "columnDefs": [ {
            //"targets": -1,
            "targets": 0,
            
        		"ordering": false,
        		"info":     false
            /*"width": "6%",*/
        } ],

        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
            	for (var i=1; i<=6; i++)
                	$('td', row).eq(i).css('text-decoration', 'line-through');
        },        
        "order": [[ 1,"asc" ]],
		    "pageLength": 50,        
        "processing": true,
        "serverSide": true,
        "ajax": {
        	"url": "<?php echo AppUrl::bicesUrl('customDomain/resco/contracts/listAjax'); ?>",
        	"type": "POST",
        	data: function ( input_data ) {
        		input_data.location_id = $('#location_id').val();
        		input_data.department_id = $('#department_id').val();
        		input_data.category_id = $('#category_id').val();
                input_data.subcategory_id = $('#subcategory_id').val();
                input_data.contract_status = $('#contract_status').val();
  			}    
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          if(aData[1].search("<span class='deactivated_vendor'></span>")>0){
            $(nRow).addClass('deactivated_record');
            /*$(nRow).css("background-color", "#ccc");*/
          }
           for (var i=3; i<=9; i++){
            $('td:eq('+i+')', nRow).addClass('td-center');
        	}
        	for (var i=3; i==3; i++){
            $('td:eq('+i+')', nRow).addClass('managedBy');
        	}
        	for (var i=5; i==5; i++){
            $('td:eq('+i+')', nRow).addClass('table-currency');
        	}

         }
    });

    var userType = <?= json_encode(Yii::app()->session['user_type']) ?>;
	var baseUrl = <?= json_encode(Yii::app()->getBaseUrl(true)) ?>;
	if (userType == 4 && baseUrl.indexOf("sandbox") === -1) {
	 $('.dataTables_filter').append('<a href="<?php echo AppUrl::bicesUrl('contracts/export'); ?>"><button class="btn btn-yellow pull-right" style="margin-left: 10px;">Export CSV</button></a>');
	}
});

function select2function(className,lableTitle){
  var lableTitle = lableTitle;
  $("."+className).select2({
     // placeholder: lableTitle,
     /* allowClear: true*/
    });
}
function loadSubcategories(subcategory_id)
{
	var category_id = $('#category_id').val();
	
	if (category_id == 0)
		$('#subcategory_id').html('<option value="0">All Subcategories</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { category_id: category_id }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="0">All Subcategories</option>';
	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	        	$('#subcategory_id').html(options_html);
	        	if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id); 
	        },
	        error: function() { $('#subcategory_id').html('<option value="0">All Subcategories</option>'); }
	    });
	}
}

function exportChart(canvas_id)
{
	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id;
}

function addContractModal(e){
	e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('customDomain/resco/contracts/createByModal'); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_contract_cont').html(mesg);
            $('#create_contract').modal('show');
        }
    });
}

function createApexChart(height=350,width='100%',type='line',lable=false,series,categories,chartID,color=[],toolTip='Spend'){ 

    var horizontalL=false;
    var heightL = height;
    var widthL = width;
    var typeL = type;
    var lableL = lable;
    var seriesL = series; 
    var seriesL =[];
    var seriesData =[];
    if(type=='line'){
        $.each(series, function(key, value) { 
          seriesL.push({'data':value['data'],'name':value['name']});
        });
    }else{
      $.each(series, function(key, value) {  
          seriesData.push(value);
        });
      seriesL.push({'name':toolTip,'data':seriesData});
    }

    var categoriesL =[];
     $.each(categories, function(key, value) {
      categoriesL.push(value);
    });  
    var chartIDL = chartID;
    if(chartIDL=="#spend_contracts"){horizontalL=true;}
    var options = {
      chart: {
        fontFamily: 'Poppins !important',
        animations: {
        enabled: true,
        easing: 'elastic', // linear, easeout, easein, easeinout, swing, bounce, elastic
        speed: 800,
        animateGradually: {
          delay: 300,
          enabled: true
        },
        dynamicAnimation: {
          enabled: true,
          speed: 350
        }
      },
        height: heightL,
        width: width,
        type: typeL,
        zoom: {
          enabled: false
        },
        toolbar: {
            tools: {
                download: false
            },
          },
      },
      legend: {show: false,position: 'bottom',offsetX: 0,offsetY: 0},
      colors: colors,
      plotOptions: {bar: {columnWidth: '35%',distributed: true, horizontal: horizontalL}},
      dataLabels: {enabled: lableL},
      stroke: {curve: 'straight'},
      series: seriesL,
      title: {
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      xaxis: {categories: categoriesL,labels: {
                formatter: function (value) {
                  valueL = chartIDL!="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return "";
                },
                style: {fontSize: '10px' }}},
      yaxis: {
      		labels: {
                formatter: function (value) {
                  valueL = chartIDL=="#spend_contracts"?value:"<?php echo html_entity_decode($user_currency_symbol);?>"+ value;
                return valueL
                },
                style: {fontSize: '10px'}
              },},
      markers: {size: 5,hover: {size: 6}},
    }

    var chart = new ApexCharts(
      document.querySelector(chartIDL),
      options
    );
    chart.render();
  }
  var colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];
  /* createApexChart(200,'100%','bar',false,<?php echo json_encode($top_contract_series) ?>,<?php echo json_encode($top_contract_label);?>,"#spend_contracts",colors); */

</script>
<?php $this->renderPartial('/contracts/create_contract');?>

<style type="text/css">
.location .multiselect {width: 138%;}
.department .multiselect {width: 112%; display: none;}
</style>

<div class="modal fade" id="expiring_contract_modal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 62%;">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header alert-info">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title font-1rem">Contracts Expiring or That Have A Notice Period In the Next 12 Months</h4>
            </div>
            <div class="clearfix"> </div>
            <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab p1">
                  <table id="contract_expiring_next_months" class="table table-striped table-bordered"  style="width: 100%;">
                    <thead>
                    <tr><th>Contract Title</th><th style="width: 19%;">Supplier Name</th><th style="width: 17%;">Managed By</th><th style="width: 13%;">End Date</th><th>Notice Period Date</th></tr>
                    </thead>
                     <tbody>
                      <?php foreach ($expiring_contracts_list as $expiring_row) {
                        $vendorID = $expiring_row['vendor_id'];
                        ?>
                         <tr><td><a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $expiring_row['contract_id']); ?>" style="text-decoration:none"><?php echo $expiring_row['title'];?></a></td>
                          <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $vendorID); ?>" style="text-decoration:none"><?php echo $expiring_row['vendor_name'];?></a></td>
                          <td><?php if (!empty($expiring_row['contract_manager'])) {
                            echo $expiring_row['contract_manager'];
                          }?></td>
                         <td><?php 
                          if ($expiring_row['end_date'] !="0000-00-00") 
                            echo date(FunctionManager::dateFormat(), strtotime($expiring_row['end_date']));
                          ?></td>
                         <td><?php  if ($expiring_row['notice_period_date'] !="0000-00-00 00:00:00") 
                           echo date(FunctionManager::dateFormat(), strtotime($expiring_row['notice_period_date']));?></td>
                         </tr>
                      <?php } ?>
                     </tbody>
                  </table>
                </div>

              </div>

            </div>
          </div>
        </div>
      </div>


<!-- Contract Active Values -->
<div class="modal fade" id="contract_active_value_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog" style="width: 50%;">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title" style="font-size: 18px;">End Date Over 3 Months</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_active_value" class="table table-striped table-bordered contract_active_value" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Contract Title</th>
										  <th style="width: 19%;">Supplier Name</th>
										  <th style="width: 17%;">Managed By</th>
										  <th>End Date</th>
										  <th>Notice Period Date</th>
									  </tr>
									  </thead>

									  <tbody>

									  <?php $i=1;
										  foreach ($metrics['contract_popup']['active_all'] as $contract) {
										  	 $vendorID = $contract['vendor_id'];
										   ?>
											  <tr>
                          <!-- <td><?php echo $i++;?></td> -->
												  <td>
												  	<a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract['contract_id']); ?>" style="text-decoration:none">
		                       						 <?php echo $contract['contract_title']?></a></td>
		                       					  <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $vendorID); ?>" style="text-decoration:none">
		                       						 <?php echo $contract['vendor_name']; ?></a></td>
												  <td><?php if (!empty($contract['contract_manager'])) {
													echo $contract['contract_manager'];
													}?></td>
												  <td data-sort='<?php echo date("Y-m-d", strtotime($contract['contract_end_date']));?>'>
												  	<?php if ($contract['contract_end_date'] !="0000-00-00") 
		                      				echo date(FunctionManager::dateFormat(), strtotime($contract['contract_end_date'])); ?></td>

						              <td data-sort='<?php echo date("Y-m-d", strtotime($contract['break_clause']));?>'>
						              	<?php if ($contract['break_clause'] !="0000-00-00 00:00:00") echo date(FunctionManager::dateFormat(), strtotime($contract['break_clause']));?></td>
		                    
											  </tr>

										  <?php } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>


<!-- Contract Expired -->
<div class="modal fade" id="contract_list_expire_only" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog" style="width: 50%;">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title"  style="font-size:18px;">Passed Contract End Date</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="Contract_Expired" class="table table-striped table-bordered contract_expire_30days Contract_Expired" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Contract Title</th>
										  <th style="width: 19%;">Supplier Name</th>
										  <th style="width: 17%;">Managed By</th>
										  <th>End Date</th>
										  <th>Notice Period Date</th>
									  </tr>
									  </thead>

									  <tbody>

                    <?php $i=1;
                    //if(count($metrics['contract_popup']['expired_only_all'])>0) {
                        foreach ($metrics['contract_popup']['expired_only_all'] as $contract) {
                        $vendorID = $contract['vendor_id']; ?>
                            <tr>
                                <!-- <td><?php echo $i++;?></td> -->
                                <td><a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract['contract_id']); ?>" style="text-decoration:none"><?php echo $contract['contract_title']; ?></a></td>
								<td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $vendorID); ?>" style="text-decoration:none">
								<?php echo $contract['vendor_name']; ?></a></td>
								<td><?php if (!empty($contract['contract_manager'])) {
								echo $contract['contract_manager'];
								}?></td>
                <td data-sort='<?php echo date("Y-m-d", strtotime($contract["contract_end_date"]));?>'>
                	<?php echo  $contract['contract_end_date'] !="0000-00-00"?date("d/m/Y",strtotime($contract['contract_end_date'])):'';  ?></td>
                <td data-sort='<?php echo date("Y-m-d", strtotime($contract["break_clause"]));?>'>
                	<?php echo $contract['break_clause'] !="0000-00-00 00:00:00"?date("d/m/Y",strtotime($contract['contract_end_date'])):'';  ?></td>     
                            </tr>
                        <?php }
                    //}  ?>
									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>
<!-- Contract Expire 30 days -->
<div class="modal fade" id="contract_expire_30days_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog" style="width: 50%;">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title"  style="font-size:18px;">End Date Within 30 Days</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_expire_30days" class="table table-striped table-bordered contract_expire_30days table_expire_30days" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Contract Title</th>
										  <th style="width: 19%;">Supplier Name</th>
										  <th style="width: 17%;">Managed By</th>
										  <th>End Date</th>
										  <th>Notice Period Date</th>
										 </tr>
									  </thead>

									  <tbody>

                    <?php $i=1;
                    //if(count($metrics['contract_popup']['expiries_all'])>0) {
                        foreach ($metrics['contract_popup']['expiries_all'] as $contract) {
                        $vendorID = $contract['vendor_id']; ?>
                            <tr>
                                <!-- <td><?php echo $i++;?></td> -->
                                <td><a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract['contract_id']); ?>" style="text-decoration:none"><?php echo $contract['contract_title']; ?></a></td>
                              <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $vendorID); ?>" style="text-decoration:none"><?php echo $contract['vendor_name']; ?></a></td>
							  <td><?php if (!empty($contract['contract_manager'])) {
								echo $contract['contract_manager'];
								}?></td>
                <td data-sort='<?php echo date("Y-m-d", strtotime($contract["contract_end_date"]));?>'>
                	<?php echo  $contract['contract_end_date'] !="0000-00-00"?date("d/m/Y",strtotime($contract['contract_end_date'])):'';  ?></td>
                
                <td data-sort='<?php echo date("Y-m-d", strtotime($contract["break_clause"]));?>'>
                	<?php echo $contract['break_clause'] !="0000-00-00 00:00:00"?date("d/m/Y",strtotime($contract['contract_end_date'])):'';  ?></td>
                                
                            </tr>
                        <?php }
                   // } ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>


<!-- Contract Expire 90 days -->
<div class="modal fade" id="contract_expire_91days_modal" role="dialog" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog" style="width: 50%;">
				  <!-- Modal content-->
				  <div class="modal-content">
					  <div class="modal-header alert-info">
						  <button type="button" class="close" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title"  style="font-size: 18px;"> End Date Within 3 Months</h4>
					  </div>
					  <div class="clearfix"> </div>
					  <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

						  <div id="myTabContent" class="tab-content">
							  <div role="tabpanel" class="tab-pane active" id="tab_content1042" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
								  <table id="contract_expire_91days" class="table table-striped table-bordered contract_expire_91days" style="width: 100%;">
									  <thead>
									  <tr>
										  <th>Contract Title</th>
										  <th style="width: 19%;">Supplier Name</th>
										  <th style="width: 17%;">Managed By</th>
										  <th>End Date</th>
										  <th>Notice Period Date</th> 
									  </tr>
									  </thead>

									  <tbody>

                      <?php $i=1;
                      //if(count($metrics['contract_popup']['expiries_later_all'])>0) {
                          foreach ($metrics['contract_popup']['expiries_later_all'] as $contract) {
                           $vendorID = $contract['vendor_id']; ?>
                              <tr>
                                  <!-- <td><?php echo $i++;?></td> -->
                                  <td><a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract['contract_id']); ?>" style="text-decoration:none"><?php echo $contract['contract_title']; ?></a></td>
                                  <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $vendorID); ?>" style="text-decoration:none">
           						 <?php echo $contract['vendor_name']; ?></a></td>
								  <td><?php if (!empty($contract['contract_manager'])) {
									echo $contract['contract_manager'];
									}?></td>
                  
                  <td data-sort='<?php echo date("Y-m-d", strtotime($contract["contract_end_date"]));?>'><?php if ($contract['contract_end_date'] !="0000-00-00") 
		                      		echo date(FunctionManager::dateFormat(), strtotime($contract['contract_end_date'])); ?></td>
                  <td data-sort='<?php echo date("Y-m-d", strtotime($contract["break_clause"]));?>'><?php if ($contract['break_clause'] !="0000-00-00 00:00:00") 
		                    	    echo date(FunctionManager::dateFormat(), strtotime($contract['break_clause']));?></td>
                              </tr>

                          <?php }
                      //}  ?>

									  </tbody>

								  </table>
							  </div>

						  </div>

					  </div>
				  </div>
			  </div>
		  </div>


<!-- Total Budget -->


<style type="text/css">
.tooltip-inner{min-width:220px !important; color:#000; background:#eee;
 padding:20px 10px;font-weight:600;box-shadow:10px 5px 10px 5px #888;}
.tooltip .tooltip-arrow {border-top: 5px solid #eee !important;}
.tooltip-inner{font-size: 11px !important;}
.th-center, .tr-center,.td-center {text-align: center;}
#contract_active_value_wrapper .dataTables_filter,
#contract_expire_91days_wrapper .dataTables_filter,
#contract_expire_30days_wrapper .dataTables_filter,
#Contract_Expired_wrapper .dataTables_filter,
#contract_expiring_next_months_wrapper
{display: block; float: none; margin: 20px !important;}
.table-currency { padding-right:28px !important; }
#contract_table_filter {width:auto !important; }
</style>
 
</script>
<?php if(!empty($_GET['from']) && $_GET['from']=='information'){?>
<script type="text/javascript">
  $(document).ready(function(){
    $('.click-modal-poup').trigger('click');
  });
	$(document).ready(function(){
	 $('[data-toggle="tooltip"]').tooltip();  
	});
</script>
<?php } ?>
<script>
	$(document).ready(function() {
 // $('.contract_active_value, .contract_expire_91days, .Contract_Expired').dataTable();
// fixes change
  $(".contract_active_value").dataTable({
        order: [[3, 'asc']],
    });
  $(".contract_expire_91days").dataTable({
        order: [[3, 'asc']],
    });
  $(".Contract_Expired").dataTable({
        order: [[3, 'DESC']],
    });

  $(".table_expire_30days").dataTable({
        order: [[3, 'asc']],
    });

  $("#contract_expiring_next_months").dataTable({
        order: [[3, 'desc']],
    });
  
});
</script>




