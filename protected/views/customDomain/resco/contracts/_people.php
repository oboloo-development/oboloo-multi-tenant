<?php  $userObj = new User();
	    $usersList = $userObj->getAll(array('status' => 1, 'admin_flag' => 0, 'full_name LIKE' => '%' . $search_string . '%', 'ORDER' => 'full_name', 'limit' => 100)); ?>
<div class="x_title" style="border-bottom:none"><h4 class="heading">People</h4></div>
<h4 class="subheading" style="padding-bottom: 10px; padding-left: 6px;">oboloo users in this tab will receive notifications for this contract</h4>
<input type="hidden" class="notranslate" name="contractId" id="contractId" value="<?php if (isset($contract_id) && !empty($contract_id)) echo $contract_id; else echo '0'; ?>" />

<input type="hidden" class="notranslate" name="contract_id" id="contract_id" value="<?php if (isset($contract_id)) echo $contract_id; ?>" /> 
<input type="hidden" name="form_submitted" class="notranslate" id="form_submitted" value="1" />
<input type="hidden" name="form_people" class="notranslate" id="form_people" value="1" />

<!-- <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($contract['vendor_id']) && !empty($contract['vendor_id'])) echo $contract['vendor_id']; else echo '0'; ?>" /> -->


<!-- <div class="form-group">
	<div class="label-adjust"><label class="control-label">Contact Name</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control notranslate"
			name="contact_name" id="contact_name"
			<?php if (isset($contract['contact_name']) && !empty($contract['contact_name'])) echo 'value="' . $contract['contact_name'] . '"'; else echo 'placeholder="Contact Name"'; ?>>
	</div>
</div> -->


<!-- <div class="form-group">
	<div class="label-adjust"><label class="control-label">Phone</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control notranslate"
			name="contact_info" id="contact_info"
			<?php if (isset($contract['contact_info']) && !empty($contract['contact_info'])) echo 'value="' . $contract['contact_info'] . '"'; else echo 'placeholder="Phone"'; ?>>
	</div>
</div> -->

<div class="form-group">
	<div class="label-adjust"><label class="control-label">Created By</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control notranslate" id="full_name" name="full_name" readonly
			<?php if (empty($contract) && isset(Yii::app()->session['full_name']) && !empty(Yii::app()->session['full_name'])) {
				echo 'value="' . Yii::app()->session['full_name'] . '"';
			}else if (!empty($contract['user_name'])) {
                  echo 'value="' . $contract['user_name'] . '"';
			} else if (!empty($contract)) {
                  echo 'value="' . $contract['full_name'] . '"';
			}  else echo 'placeholder="'.Yii::app()->session['full_name'].'"'; ?>>
		    <input type="hidden" id="user_id" name="user_id" <?php if (empty($contract) && !empty(Yii::app()->session['user_id'])) { echo 'value="' . Yii::app()->session['user_id'] . '"';}
		    else if (!empty($contract)) {
                  echo 'value="' . $contract['user_id'] . '"';
			} 

		    ?>>
	</div>
</div>



<div class="form-group">
	<div class="label-adjust"><label class="control-label">Managed By</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">

		<select name="sbo_user_id" id="sbo_user_id" class="form-control select_internal notranslate">
			<option value=""></option>
          <?php foreach($usersList as $value){?>
            <option value="<?php echo $value['user_id'];?>" <?php if (isset($contract['sbo_user_id']) && $contract['sbo_user_id']==$value['user_id']){?> selected <?php } ?>><?php echo $value['full_name'];?></option>
          <?php }?>
        </select>

		<?php /* <input type="text" class="form-control notranslate"
			name="contract_manager" id="contract_manager"
			<?php if (isset($contract['contract_manager']) && !empty($contract['contract_manager'])) echo 'value="' . $contract['contract_manager'] . '"'; else echo 'placeholder="Internal User 1"'; ?>>
		<input type="hidden" name="sbo_user_id" class="notranslate" id="sbo_user_id" <?php if (isset($contract['sbo_user_id']) && !empty($contract['sbo_user_id'])) echo 'value="' . $contract['sbo_user_id'] . '"'; ?> />   */ ?>
	</div>
</div>

<div class="form-group">
	<div class="label-adjust"><label class="control-label">Additional User To Receive Notifications</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="commercial_user_id" id="commercial_user_id" class="form-control select_internal notranslate">
			<option value=""></option>
          <?php foreach($usersList as $value){?>
            <option value="<?php echo $value['user_id'];?>" <?php if (isset($contract['commercial_user_id']) && $contract['commercial_user_id']==$value['user_id']){?> selected <?php } ?>><?php echo $value['full_name'];?></option>
          <?php }?>
        </select>

		<?php /*<input type="text" class="form-control notranslate"
			name="commercial_lead" id="commercial_lead"
			<?php if (isset($contract['commercial_lead']) && !empty($contract['commercial_lead'])) echo 'value="' . $contract['commercial_lead'] . '"'; else echo 'placeholder="Internal User 2"'; ?>>
		<input type="hidden" class="notranslate" name="commercial_user_id" id="commercial_user_id" <?php if (isset($contract['commercial_user_id']) && !empty($contract['commercial_user_id'])) echo 'value="' . $contract['commercial_user_id'] . '"'; ?> /> */?>
	</div>
</div>


<div class="form-group">
	<div class="label-adjust"><label class="control-label">Additional User To Receive Notifications</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="procurement_user_id" id="procurement_user_id" class="form-control select_internal notranslate">
			<option value=""></option>
          <?php foreach($usersList as $value){?>
            <option value="<?php echo $value['user_id'];?>" <?php if (isset($contract['procurement_user_id']) && $contract['procurement_user_id']==$value['user_id']){?> selected <?php } ?>><?php echo $value['full_name'];?></option>
          <?php }?>
        </select>

		<?php /*<input type="text" class="form-control notranslate"
			name="procurement_lead" id="procurement_lead"
			<?php if (isset($contract['procurement_lead']) && !empty($contract['procurement_lead'])) echo 'value="' . $contract['procurement_lead'] . '"'; else echo 'placeholder="Internal User 3"'; ?>>
		<input type="hidden" name="procurement_user_id" class="notranslate" id="procurement_user_id" <?php if (isset($contract['procurement_user_id']) && !empty($contract['procurement_user_id'])) echo 'value="' . $contract['procurement_user_id'] . '"'; ?> /> */?>
	</div>
</div>

<div class="form-group">
	<div class="label-adjust"><label class="control-label">Additional User To Receive Notifications</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="additional_user_1" id="additional_user_1" class="form-control select_internal notranslate">
			<option value=""></option>
        <?php foreach($usersList as $value){?>
          <option value="<?php echo $value['user_id'];?>" <?php if (isset($contract['additional_user_1']) && $contract['additional_user_1']==$value['user_id']){?> selected <?php } ?>><?php echo $value['full_name'];?></option>
        <?php }?>
    </select>
	</div>
</div>


<div class="form-group">
	<div class="label-adjust"><label class="control-label">Additional User To Receive Notifications</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="additional_user_2" id="additional_user_2" class="form-control select_internal notranslate">
			<option value=""></option>
        <?php foreach($usersList as $value){?>
          <option value="<?php echo $value['user_id'];?>" <?php if (isset($contract['additional_user_2']) && $contract['additional_user_2']==$value['user_id']){?> selected <?php } ?>><?php echo $value['full_name'];?></option>
        <?php }?>
        </select>
	</div>
</div>

<!-- <div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-6 date-input valid">
	  <label class="control-label">Supplier</label>
	  <select  type="text" class="form-control vendor_id select_vendor_multiple notranslate" name="vendor_id" id="vendor_id" style="  border-radius: 10px;border: 1px solid #4d90fe;">
	  	<option value="" ></option>
	    <?php foreach($vendor_list as $vendorInfo){
    			if($vendorInfo['vendor_id']==$contract['vendor_id']){
    				$selected="selected='selected'";
    			}else{
    				$selected="";
    			}
	    	?>
	    	<option value="<?php echo $vendorInfo['vendor_id'];?>" <?php echo $selected;?>><?php echo $vendorInfo['vendor_name'];?></option>

	    <?php } ?>

	</select>
    </div>
</div> -->

<!-- <div class="form-group">
	<div class="label-adjust"><label class="control-label">Service Provider</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control has-feedback-left vendor_name"
			name="vendor_name" id="vendor_name"
			<?php if (isset($contract['vendor']) && !empty($contract['vendor'])) echo 'value="' . $contract['vendor'] . '"'; else echo 'placeholder="Service Provider"'; ?>>
		
		<span class="fa fa-user-circle-o form-control-feedback left"
			aria-hidden="true"></span>
	</div>
</div> -->
<!--  -->




<!-- <div class="form-group">
	<div class="label-adjust"><label class="control-label">Supplier Email</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control notranslate"
			name="website" id="website"
			<?php if (isset($contract['website']) && !empty($contract['website'])) echo 'value="' . $contract['website'] . '"'; else echo 'placeholder="Supplier Email"'; ?>>
	</div>
</div> -->
<!-- <div class="form-group">
	<div class="label-adjust"><label class="control-label">Supplier Telephone</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control notranslate"
			name="contact_info" id="contact_info"
			<?php if (isset($contract['contact_info']) && !empty($contract['contact_info'])) echo 'value="' . $contract['contact_info'] . '"'; else echo 'placeholder="Supplier Telephone"'; ?>>
	</div>
</div> -->
 <div class="clearfix"></div><br />
<?php $this->renderPartial('_submit',array('contract_id'=>$contract_id,'contract'=>$contract));?>