<?php
 // this disabled variable targer the archive and un-archive checkbox in the list.
if(strtolower($disabledArchive) == 'yes'){
  $disabledArchiveContract = 'disabled';    
} else{
  $disabledArchiveContract = ''; 
} ?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_title pull-left" style="border: none;"><h4 class="heading">Documents</h4>
  <div class="row">
    <div class="col-md-7">
      <h4 class="subheading"><br />Upload documents that relate to this contract here. Only users with appropriate access can approve contract documents. Please contact a Super User in your organisation to request access.<br /></h4>
    </div>
    <div class="col-md-5">
      <?php if($disabledArchive != 'yes'){?>
        <button type="button" class="btn btn-primary request-cntrt-css pull-right" onclick="$('#request_contract_document_modal').modal('show');">
        Request a new document from supplier
        </button>
      <?php } ?>
      <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#uploadContractDocumentType">Upload Document</button>
    </div>
  </div>
</div>
  
<div class="clearfix"></div>
</div> 
<!-- <div id="document_list_cont_pending"></div> -->
<div id="document_list_cont">
  <?php $this->renderPartial('_documents',
  array('contract_id'=>$contract_id, 'disabledArchive' => $disabledArchive,'documentList'=>$documentList,
  'documenArchivetList'=>$documenArchivetList,'contract_id'=>$contract_id,'location_id'=>$location_id,
  'department_id'=>$department_id,'filter_document_type' => $filter_document_type,
  'flter_document_status' => $flter_document_status,'fltr_doc_contract_supplier_status' => $fltr_doc_contract_supplier_status));?>
</div>
<?php $this->renderPartial('_contract_document',array('vendor_id'=>$vendor_id,'contract_id'=>$contract_id));?>
<style >
.request-cntrt-css {background-color: #F79820 !important;color: #fff !important;border-color: #F79820 !important;}
.request-cntrt-css:hover {background-color: #F79820 !important;color: #fff !important;border-color: #F79820 !important;}
</style>

<div id="uploadContractDocumentType" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 1000px !important;">
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Document</h4>
      </div>
      <div class="modal-body">
        <div class="clearfix"></div><br />
          <div class="spinnerContainer" class="text-center">
            <div class="progress">
              <div id="progressBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
              </div>
            </div>
          </div> 
          <div class="clearfix"></div>
          <div class="form-group" id="document_area_1">
            <div class="col-md-3 col-sm-3 col-xs-12">
              <label class="control-label">Document <span style="color: #a94442;">*</span></label>
              <input class="form-control notranslate" type="file" name="order_file_1" id="order_file_1" />
              <span class="text-danger" style="font-size: 10px;">Maximum File Size of 100MB</span>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <label class="control-label">Document Title <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" /> 
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <label class="control-label">Document Type  <span style="color: #a94442;">*</span></label>
              <?php $documentsTitle = FunctionManager::contractDocument();?>
              <select class="form-control notranslate" name="document_type_1" id="document_type_1">
                <option value="">Select Document Type</option>
                <?php foreach($documentsTitle as $key=>$value){?>
                <option value="<?php echo $key?>"><?php echo $value;?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <label class="control-label">Expiry Date</label>
              <input type="text" class="form-control notranslate expiry_date_1" name="expiry_date_1" id="expiry_date_1" placeholder="Expiry Date">
            </div>
            <div class="col-md-1 col-sm-1 col-xs-12 text-right"><br />
              <button id="btn_upload_1" class="btn btn-info submit-btn" onclick="uploadDocument(1,event);" style="margin-top: 10px;"
              <?php echo $disabled; echo $disabledArchiveContract; ?>>Upload</button>
            </div>
          </div>
          <div class="clearfix"></div><br /><br />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


