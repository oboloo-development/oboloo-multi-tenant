<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Import Contracts</h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('contracts/list'); ?>">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Contract List
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

    <div class="x_content">
      <p>Select one or more CSV files to import contracts. They will be automatically imported.</p>
      <p> </p>
      <p>The file should not have any header and must have fields in the following order: </p>
      <p> </p>
      <p>
          Contract Title , Reference #,Contract Type, Contract Status , Currency Rate , Currency Rate , Full Name ,
          Senior Business Contract Owner, Commercial Lead, Procurement Lead, Service Provider,Service Provider Contact name,
          Service Provider Email, Service Provider Telephone,Location,Department,Project,Spend Type,Contract Category,Contract Sub Category,
          Description, Comments, Total Contract Value, Annual Contract Value,Original Contract Value,Original Contract Annual Value,PO Number,Cost Centre,
          Contract Start Date,Contract End Date,Original End Date, Break Clause,Termination Comments,Extension Options, Extension Comments
      </p>

      <div class="row tile_count">
          <div class="clearfix"> </div>
      </div>

      <form name="import_form" id="import_form" action="<?php echo AppUrl::bicesUrl('products/importData'); ?> " method="post" enctype="multipart/form-data" class="dropzone">
          <div class="fallback">
              <input name="file[]" type="file" multiple />
          </div>
      </form>

    </div>

</div>
