<?php
  $disabled = FunctionManager::sandbox();
 ?>

<div role="main">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="    margin-top: 6px;"><span aria-hidden="true">&times;</span></button>
  <h3 class="pull-left"><?php echo 'Add Contract - Contract Summary';?></h3>
  <a href="https://oboloo.com/support-creating-a-contract/" class="btn btn-warning tutorial pull-right" target="_blank" style="margin-left: 18px;">Tutorial</a> 
  <div class="pull-right" style="text-align:center;padding-right: 21px;padding-top: 3px;">
<!--   <span class="step"></span> -->

</div></div>
  <div class="tile_count" role="tabpanel"
    data-example-id="togglable-tabs">
    <form id="contract_form"
      class="form-horizontal form-label-left input_mask"
      enctype="multipart/form-data" method="post"
      action="<?php echo AppUrl::bicesUrl('contracts/edit'); ?>">
     <div class="tab" style="display: block;"> 
      <input type="hidden" name="created_by_modal" value="1" />
     
      <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12 valid">
          <label class="control-label">Contract Title <span style="color: #a94442;">*</span></label>
          <input required type="text" class="form-control notranslate" name="contract_title"
            id="contract_title"
            <?php if (isset($contract['contract_title']) && !empty($contract['contract_title'])) echo 'value="' . $contract['contract_title'] . '"'; else echo 'placeholder="Contract Title"'; ?>>
          <input type="hidden" name="contractId" id="contractId" class="notranslate"
            value="<?php if (isset($contract_id) && !empty($contract_id)) echo $contract_id; else echo '0'; ?>" />
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <label class="control-label">Reference # </label>
          <input  type="text" class="form-control notranslate" name="contract_reference"
               id="contract_reference"
            <?php if (isset($contract['contract_reference']) && !empty($contract['contract_reference'])) echo 'value="' . $contract['contract_reference'] . '"'; else echo 'placeholder="Reference #"'; ?>>
        </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <label class="control-label">Contract Type</label>
            <select name="contract_type" id="contract_type" class="form-control notranslate">
            <option value="">Select Contract Type</option>
            <?php
            foreach ($contract_types as $contract_type) { ?>
            <option value="<?php echo $contract_type['value']; ?>"><?php echo $contract_type['value']; ?></option>
        <?php } ?>
            </select>
          </div>
        </div>
    

      <div class="form-group valid">
        <div class="label-adjust"><label class="control-label pull-left">Supplier</label>  <span style="color: #a94442;" class="supplier-aset pull-left">*</span><div class="clearfix"></div></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <select  class="form-control select_vendor_contract notranslate" name="vendor_id" id="vendor_id" style="border-radius: 10px;border: 1px solid #4d90fe;">
            
          </select>
          
      </div></div>
        <!-- <div class="col-md-12 col-sm-12  col-xs-12"><br />
        <span class="btn btn-danger btn-sm" id="hide_new_supplier"  title="Remove Quick Add Vendor fields">-</span>
        <span class="btn btn-primary btn-sm" id="new_supplier" title="Quick Add Vendor">+</span><strong> Add New Supplier Not In oboloo</strong>
        <br /><br />
      </div> -->
      <div class="new_supplier_con col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12 validate">
              <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
            <input type="text" class="form-control  notranslate" name="quick_vendor_name" id="quick_vendor_name" placeholder="Supplier Name">
            
            <span class="vendor_name_error" style="display: none;">Supplier Name requried</span>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 quick_vendor_field">
            <label class="control-label">Email Address <span style="color: #a94442;">*</span></label>
            <input type="text" class="form-control  notranslate" name="new_vendor_emails" id="new_vendor_emails"  placeholder="Email Address">
            
        </div>
       <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input quick_vendor_field">
                <label class="control-label ">Confirm Email Address <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control  emails_error notranslate" name="confm_emails" id="confm_emails" required="required" placeholder="Email Address" >
               
                <span class="emails_error" style="display: none;">Email Address and Confirm Email Address are not matching.</span>
            </div>

        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 quick_vendor_field"><label class="control-label">Contact Person <span style="color: #a94442;">*</span></label><input type="text" class="form-control notranslate" name="new_vendor_contact" id="new_vendor_contact" value="" placeholder="Contact Person"><span class="new_vendor_cont_error" style="display: none;">Supplier Name requried</span></div>

        <div class="col-md-6 col-sm-6 col-xs-6 quick_vendor_field"><label class="control-label">Address Line 1</label><input type="text" class="form-control notranslate" name="new_vendor_address" id="new_vendor_address"  value="" placeholder="Address Line 1"></div>

        <div class="col-md-6 col-sm-6 col-xs-6 quick_vendor_field"><label class="control-label">Address Line 2</label><input type="text" class="form-control notranslate" name="new_vendor_address2" id="new_vendor_address2" value="" placeholder="Address Line 2"></div></div><div class="form-group">
        
        <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">City</label><input type="text" class="form-control notranslate" name="new_vendor_city" id="new_vendor_city" value="" placeholder="City"></div>

        <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">State</label><input type="text" class="form-control notranslate" name="new_vendor_state" id="new_vendor_state" value="" placeholder="State"></div>

        <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">Post Code/Zip Code </label><input type="text" class="form-control notranslate" name="new_vendor_zip" id="new_vendor_zip" value="" placeholder="Code/Zip Code"></div>
        <div class="col-md-12 col-sm-12 col-xs-12 quick_vendor_field">
          <span class='quick_msg pull-right' style='color: #a94442;'></span><br />
          <span class="btn btn-primary btn-sm submit-btn pull-right" id="save_new_supplier" title="Save Quick Vendor">Save Quick Supplier</span>
        </div>
        <div class="clearfix"></div>
        </div>
      </div>

    <div class="form-group">
  

  <!-- <div class="col-md-6 col-sm-6 col-xs-6 valid">
    <label class="control-label">Currency Rate At Time Of Contract Creation</label>
    <input readonly type="text" class="form-control" name="currency_rate" id="currency_rate"
      <?php if (isset($contract['currency_rate']) && !empty($contract['currency_rate'])) { echo 'value="' . $contract['currency_rate'] . '"'; } else { echo 'value="1"'; } ?> >

  </div> -->

</div>
      
    
     
    <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6 valid">
            <label class="control-label">Linked Locations <span style="color: #a94442;">*</span></label>
            <select required name="location_id[]" id="location_id" class="form-control notranslate location_id_new" onchange="loadDepartmentsForSingleLocation(0);" multiple>
              <option value="">Select Locations</option>
              <?php foreach ($locations as $location) { ?>
                <option value="<?php echo $location['location_id']; ?>"><?php echo $location['location_name']; ?></option>
              <?php } ?>
            </select>

          </div>

          <div class="col-md-6 col-sm-6 col-xs-6 valid">
            <label class="control-label">Linked departments <span style="color: #a94442;">*</span></label>
            <select required name="department_id[]" id="department_id_new" class="form-control notranslate" multiple>
            </select>
          </div>
        </div>
     
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 valid">
          <label class="control-label">Contract Category <span style="color: #a94442;">*</span></label>
          <select required name="contract_category_id" id="contract_category_id"
            class="form-control notranslate" onchange="loadSubcategories(0);">
            <option value="">Select Contract Category</option>
          <?php foreach ($categories as $category) { ?>
            <option value="<?php echo $category['id']; ?>"
              <?php if (isset($contract['contract_category_id']) && $contract['contract_category_id'] == $category['id']) echo ' selected="SELECTED" '; ?>>
              <?php echo $category['value']; ?>
            </option>
          <?php } ?>
        </select>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 valid">
          <label class="control-label">Contract Sub Category <span style="color: #a94442;">*</span> </label>
          <select required name="contract_subcategory_id" id="contract_subcategory_id"
            class="form-control notranslate">
            <option value="">Select Contract Sub Category</option>
          </select>
        </div>
      </div>
       <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-6 date-input valid">
          <label class="control-label">Contract Start Date <span style="color: #a94442;">*</span> </label>
          <input type="text" class="form-control  notranslate"
            name="contract_start_date" id="contract_start_date"
            <?php if (isset($contract['contract_start_date']) && !empty($contract['contract_start_date'])) echo 'value="' . date("d/m/Y", strtotime($contract['contract_start_date'])) . '"'; else echo 'placeholder="Contract Start Date"'; ?>
            aria-hidden="true"></span>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 date-input valid">
        <label class="control-label">Contract End Date <span style="color: #a94442;">*</span> </label>
          <input type="text" class="form-control notranslate"
            name="contract_end_date" id="contract_end_date"
            <?php if (isset($contract['contract_end_date']) && !empty($contract['contract_end_date'])) echo 'value="' . date("d/m/Y", strtotime($contract['contract_end_date'])) . '"'; else echo 'placeholder="Contract End Date"'; ?> autocomplete="off" required>
        </div>
        
      </div>
      <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-6 date-input">
        <label class="control-label">Notice Period Date </label>
        <input type="text" class="form-control notice-period   notranslate" autocomplete="off" name="break_clause"
          id="break_clause"
          <?php if (isset($contract['break_clause']) && $contract['break_clause'] !="0000-00-00 00:00:00") echo 'value="' . date("d/m/Y", strtotime($contract['break_clause'])) . '"'; else echo 'placeholder="Notice Period Date"'; ?>
            aria-hidden="true"></span>
      </div>
    </div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 valid">
    <label class="control-label">Currency </label>
    <select name="currency_id" id="currency_id" class="form-control notranslate" onchange="loadCurrencyRate();">
      <?php foreach ($currency_rate as $rate) { ?>
        <option value="<?php echo $rate['id']; ?>"
          <?php if ((isset($contract['currency_id']) && $rate['id'] == $contract['currency_id']) || (isset(Yii::app()->session['user_currency_dropdown']) && Yii::app()->session['user_currency_dropdown'] == $rate['currency'])) echo ' selected="SELECTED" '; ?>>
          <?php echo $rate['currency']; ?>
        </option>
      <?php } ?>
    </select>
  </div>
      <div class="col-md-6 col-sm-6 col-xs-6 date-input"> 
        <label class="control-label">Estimated Total Contract Value </label>
        <input type="text" class="form-control notranslate"
          name="contract_value" id="contract_value" placeholder="Estimated Total Contract Value">
      </div>

    </div>
    
      <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <label class="control-label">Description</label>
          <textarea class="form-control notranslate" name="contract_description"
            id="contract_description" rows="4"
            <?php if (!isset($contract['contract_description']) || empty($contract['contract_description'])) echo 'placeholder="Contract description"'; ?>><?php if (isset($contract['contract_description']) && !empty($contract['contract_description'])) echo $contract['contract_description']; ?></textarea>
        </div>
      </div>
      </div>
      <div class="clearfix"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="hidden" name="contract_id" id="contract_id"
            value="<?php if (isset($contract_id)) echo $contract_id; ?>" /> <input
            type="hidden" name="form_submitted" id="form_submitted" value="1" />
        </div>
      </div>
    <div style="float:right; margin:0 5px;" id="submitBtnCont">
      <input type="submit" id="add_vendor_btn" style="background-color: #1abb9c !important; border-color: #1abb9c !important;"  class="btn btn-primary sub_btns submit-btn" value="Add Contract"  <?php echo $disabled; ?>>
    </div>
  <!-- Circles which indicates the steps of the form: -->
    </form>
  </div>
  <div class="clearfix"><br /></div>
</div>
<!-- Contract Document  Modal -->
<div class="modal fade" id="contract_document_model" style="width: 1000px; margin-left: 150px;" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="width: 1000px;">
      <div class="modal-header alert-info" style="width: 1000px;">
        <button type="button" class="close" data-dismiss="modal" style="width: 10px;float: none;">&times;</button>
        <h4 class="modal-title">Contract Document</h4>
      </div>
      <div class="clearfix"> </div>
      <div>
        <iframe id="content" style="width: 800px; min-height: 630px;" src=""></iframe>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="contract_addtional_field" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="
       position: relative;
    top: 51%;
    transform: translateY(-44%);
    right: 10%;
">
    <div class="modal-content pull-right" style="max-width: 538px; margin: auto; ">
      <div class="modal-body" style="padding: 29px;">
        <p style="font-size: 14px;">Would you like to complete the additional fields for this contract?</p>
      </div>
      <div class="pull-right">
         <a href="<?php echo $this->createUrl('departments/list');?>" id="contract_detail_link" class="btn btn-success" style="border-radius: 4px;">Yes</a>
         <a href="<?php echo $this->createUrl('contracts/list');?>"  class="btn btn-info" style="border-radius: 4px;">No</a>
      </div>
    </div>
  </div>
</div>

 
<script type="text/javascript">
$(document).ready(function(){
  $(".location_id_new").select2({
    placeholder: "Select Locations",
    allowClear: true,
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });

  $("#department_id_new").select2({
    placeholder: "Select Departments",
    allowClear: true,
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });

  $('[data-toggle="tooltip"]').tooltip(); 

  $('#contract_start_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'});
  $('#contract_end_date').datetimepicker({format: '<?php echo FunctionManager::dateFormatJS();?>'});
  $('.notice-period').datetimepicker({format: '<?php echo FunctionManager::dateFormatJS();?>'});

  $(".select_vendor_contract").select2({
        placeholder: "Select Supplier",
        allowClear: true,
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"
      }).on('select2:close', function() {
        var el = $(this);
      if(el.val()==="NEW_SUPPLIER") {
        $("#myModal2").show('modal');
      }
      });

  <?php if (isset($contract['contract_subcategory_id']) && !empty($contract['contract_subcategory_id'])) { ?>
    loadSubcategories(<?php echo $contract['contract_subcategory_id']; ?>);
  <?php } ?>
  
  function createDateRangePicker()
  {
    $('.note_date').daterangepicker({ 
      singleDatePicker: true, 
      singleClasses: "picker_3", 
      locale: { format: 'DD/MM/YYYY' } 
    });   
  }

    $( "#contract_form" ).validate( {
        rules: {
            location_id: "required",
            department_id: "required",
            contract_title: "required",
            contract_title: "required",
            vendor_id: "required",
            contract_start_date:"required",
            contract_end_date:"required",
            contract_category_id:"required",
            contract_subcategory_id:"required"
        },
        messages: {
            location_id: "Location is required",
            department_id: "Department is required",
            contract_title: "Contract title is required",
            vendor_id: "Supplier is required",
            contract_start_date: "Contract Start is required",
            contract_end_date: "Contract End is required",
            contract_category_id: "Contract Category is required",
            contract_subcategory_id: "Contract Sub Category is required",
      //contract_status: "Contract Status is required"
        },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      element.parents(".col-sm-6" ).addClass("has-feedback");
      element.parents(".valid" ).addClass("has-feedback");

      if ( element.prop( "type" ) === "checkbox" )
        error.insertAfter( element.parent( "label" ) );
      else error.insertAfter( element );

      if ( !element.next( "span" )[ 0 ] ){
        $( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
      }

    },
    success: function ( label, element ) {
      if ( !$( element ).next( "span" )[ 0 ] ){
        $( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
      }
     

    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
      //$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
    },
    unhighlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error" );
      //$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
    }
    });

  
  createDateRangePicker();
   


    function vendorName(obj){
     var vendorName = obj.val();
     $('.vendor_name').each(function(){
     $(this).val(vendorName);
     });
    }

    $('#commercial_lead').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
        onSelect: function(suggestion) { $('#commercial_user_id').val(suggestion.data); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#commercial_user_id').val(0); }
    });

   /* $('#contract_title').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('contracts/getContractsTitle'); ?>',
        onSelect: function(suggestion) { $('#contractId').val(suggestion.data); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#contractId').val(0); }
    });*/

    $('#procurement_lead').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
        onSelect: function(suggestion) { $('#procurement_user_id').val(suggestion.data); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#procurement_user_id').val(0); }
    });

  $('#contract_manager').devbridgeAutocomplete({
    serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
    onSelect: function(suggestion) { $('#sbo_user_id').val(suggestion.data); },
    onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#sbo_user_id').val(0); }
  });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field",
        "onCreate": function() { createDateRangePicker(); }
    });

});

function loadContractFile(file_idx, file_name)
{
  $('#contract_document_model').modal('show');
  $("#content").attr("src", "<?php echo Yii::app()->baseUrl ?>/contracts/loadFile/id/"+file_idx+"/file/"+file_name);

}

function deleteContractFile(file_idx, file_name)
{
  $('#delete_contract_file_link_' + file_idx).confirmation({
    title: "Are you sure you want to delete the attached file?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
      $('#existing_file_id_' + file_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('contracts/deleteFile/'); ?>",
            data: { contract_id: $('#contract_id').val(), file_name: file_name }
        });
    },
    onCancel: function() {  }
  });
}

function loadCurrencyRate()
{
  var currency_id = $('#currency_id').val();

  $.ajax({
    type: "POST", data: { currency_id: currency_id }, dataType: "json",
    url: BICES.Options.baseurl + '/orders/getRates',
    success: function(options) {
      $('#currency_rate').val(options);
    },
    error: function() { $('#currency_rate').html(1); }
  });
}


function loadSubcategories(input_subcategory_id)
{
  var category_id = $('#contract_category_id').val();
  
  if (category_id == 0)
    $('#contract_subcategory_id').html('<option value="">Select Contract Sub Category</option>');
  else
  {
      $.ajax({
          type: "POST", data: { category_id: category_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
          success: function(options) {
            var options_html = '<option value="">Select Contract Sub Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#contract_subcategory_id').html(options_html);
            if (input_subcategory_id != 0) $('#contract_subcategory_id').val(input_subcategory_id); 
          },
          error: function() { $('#contract_subcategory_id').html('<option value="">Select Contract Sub Category</option>'); }
      });
  }
}

loadDepartmentsForSingleLocation(0);
  function loadDepartmentsForSingleLocation(department_id) {
    var location_id = $('.location_id_new').val();
    if (location_id == 0)
      $('#department_id_new').html('');
    else {
      $.ajax({
        type: "POST",
        data: {
          location_id: location_id
        },
        dataType: "json",
        url: BICES.Options.baseurl + '/locations/getDepartments',
        success: function(options) {
          var options_html = '';
          for (var i = 0; i < options.length; i++) {
            options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
          }
          $('#department_id_new').html(options_html);
        },
        error: function() {
          $('#department_id_new').html('');
        }
      });
    }
  }

function addDocument()
{
  var total_documents = $('#total_documents').val();
  total_documents = parseInt(total_documents);
  total_documents = total_documents + 1;
  $('#total_documents').val(total_documents);

  var new_document_html = $('#new_document_code').html();
  new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
  $('#total_documents').before(new_document_html);
}

function deleteDocument(docidx)
{
  var display_document_count = 0;
  $("div[id^='document_area']").each(function () {
    if ($(this).is(':visible')) display_document_count += 1;
  });

  if (display_document_count <= 1) alert("You must have at least one document field in the contract form");
  else
  {
    if (confirm("Are you sure you want to delete this document?"))
    {
      $('#delete_document_flag_' + docidx).val(1);
      $('#document_area_' + docidx).hide();
      $('#end_document_area_' + docidx).hide();
    }
  }
}

function dropdownVendors(obj,selected_vendor){

  var testingobj = obj;
  var options_html="";
  var selected   = "";
  $.ajax({
          type: "POST", data: {action:'quickcontractcreate'}, dataType: "json",
          url: BICES.Options.baseurl + '/dropdownAndSearch/getVendors',
          success: function(options) {
              var options_html = '<option value="">Select Supplier</option>';
                  options_html += '<option value="NEW_SUPPLIER" style="margin-left=20px;"><a data-toggle="modal" href="#myModal2" class="btn btn-primary"><b>+ Add New</b></a></option>';
              for (var i=0; i<options.suggestions.length; i++){
                  if(options.suggestions[i].data==selected_vendor){
                    selected = 'selected="selected"';
                  }else{
                    selected = "";
                  }
                  options_html += '<option value="' + options.suggestions[i].data + '" '+selected+'>' +  options.suggestions[i].value + '</option>';
                }

                   $("#"+testingobj).html(options_html);
          }
         
      });
  
  }
dropdownVendors("vendor_id",0);
$("#vendor_id").trigger("change");
 
$('#contract_form').submit(function (e) { 
 
 var endDate = $("#contract_end_date").val();
 var startDate = $("#contract_start_date").val();
 var locationID = $("#location_id").val();
 var departmentID = $("#department_id_new").val();
 var contractTitle = $("#contract_title").val();
 var contractReference = $("#contract_reference").val();
 var vendor_id = $("#vendor_id").val();
 if(endDate !='' && startDate !='' && locationID !='' && departmentID !='' && contractTitle !='' && vendor_id !=''){
 

  $.ajax({
      url: '<?php echo AppUrl::bicesUrl('contracts/edit'); ?>',
      type: 'POST', async: false, dataType: 'json',
      data: $("#contract_form").serialize(),
      success: function(info) {
            console.log(info);
           $("#contract_detail_link").attr('href','<?php echo $this->createUrl('contracts/edit');?>/'+info.contract_id);
           $('#contract_addtional_field').modal('show'); 
      }
  });

}
  return false;
});

$(".new_supplier_con").hide();
$("#hide_new_supplier").hide();
$("#new_supplier").click(function(){
  $(".new_supplier_con").show();
  $("#hide_new_supplier").show();
  $(".quick_vendor_field").show();
  
  
});
$("#hide_new_supplier").click(function(){
  $(".new_supplier_con").hide();
  $("#quick_vendor_name").val("");
  $("#new_vendor_emails").val("");
  $("#hide_new_supplier").hide();
});

$("#save_new_supplier").click(function(){
    var email = $("#new_vendor_emails").val();
    var conf_email = $("#confm_emails").val();
    var new_vendor_name = $("#quick_vendor_name").val();
    var new_vendor_emails = $("#new_vendor_emails").val();
    var new_vendor_contact_name = $("#new_vendor_contact").val();
    var new_vendor_address = $("#new_vendor_address").val();
    var new_vendor_address2 = $("#new_vendor_address2").val();
    var new_vendor_city = $("#new_vendor_city").val();
    var new_vendor_state = $("#new_vendor_state").val();
    var new_vendor_zip = $("#new_vendor_zip").val();
    if(new_vendor_name ==""){
       $(".vendor_name_error").css('color', 'red', 'border-color', 'red').show();
       return flase;
    }
    if(email != conf_email) {
     // alert('Email Not Matching!');
      $(".emails_error").css('color', 'red').show();
      return false;
    }
    if(new_vendor_contact_name ==""){
       $(".new_vendor_cont_error").css('color', 'red', 'border-color', 'red').show();
       return flase;
    }
    if(new_vendor_emails  != ''){
     $(this).prop('disabled',true);
       $.ajax({
            type: "POST", 
            data: { vendor_name:new_vendor_name,vendor_emails:new_vendor_emails,new_vendor_contact_name:new_vendor_contact_name,new_vendor_address:new_vendor_address,new_vendor_address2:new_vendor_address2,new_vendor_city:new_vendor_city,new_vendor_state:new_vendor_state,new_vendor_zip:new_vendor_zip},
            url: BICES.Options.baseurl + '/vendors/quickAddVendor',
            success: function(options) {
               if(options==1){
                $(".quick_msg").html("Email is already taken.");
                 alert("Supplier with this Email is already taken. Please try another.");
               }else if(options==2){
                $(".quick_msg").html("New Supplier added successfully.");
                 alert("Supplier added successfully.");
                 dropdownVendors("vendor_id",0);
                  $(".quick_vendor_field").hide();
                  $("#quick_vendor_name");
                  $("#new_vendor_emails").val('');
                  $("#confm_emails").val('');
                  $("#new_vendor_contact").val('');
                  $("#new_vendor_address").val('');
                  $("#new_vendor_address2").val('');
                  $("#new_vendor_city").val('');
                  $("#new_vendor_state").val('');
                  $("#new_vendor_zip").val('');

               }else if(options==3){
                $(".quick_msg").html("Problem occured, try again.");
                 alert("Problem occured, try again.");
               }
              $("#save_new_supplier").prop('disabled',false);
             

              /* $(".new_supplier_con").hide();
               $("#hide_new_supplier").hide();*/
             
            }
        });
      
    }else{
      $(".quick_msg").before("Email is required");
    }
    $(".quick_msg").delay(5000).fadeOut(800);
 });

</script>
<?php echo $this->renderPartial("_create_supplier");?>
<style type="text/css">
.btn-file {
    padding:6px !important;
}
@media (max-width: @screen-xs-min) {
  .modal-xs { width: @modal-sm; }
}
/* Mark input boxes that gets an error on validation: */
.invalid {
  background-color: #ffdddd !important;
}
.supplier-aset{margin-top: 8px !important;margin-left: 2px !important;}

</style>
