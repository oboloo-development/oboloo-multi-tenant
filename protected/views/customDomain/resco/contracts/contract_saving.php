<h4 class="heading">Savings</h4><br />
          <?php if(!empty($savings) && count($savings)>0) {?>
          <table id="saving_table" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                <th> </th>
                <th style="width: 8%; text-align: center;">Saving ID</th>
                <th class="th-center notranslate" style="width: 10%">Saving Title</th>
                <!-- <th>Start Date</th> -->
                <th class="th-center">Created on </th> 
                <th class="th-center">Savings Due date</th>
                <th class="th-center">User</th>
                <th class="th-center">Currency</th>
                <th class="th-center">Baseline Spend</th>
                <th class="th-center">Projected Savings</th>
                <th class="th-center">Projected Savings %</th>
                <th class="th-center">Realised Savings</th>
                <th class="th-center">Realised Savings %</th>
                <th class="th-center">Status</th>
              </tr>
              </thead>

              <tbody>

              <?php foreach ($savings as $value) { 

              $style="";
              if(strtolower(trim($value['saving_status']))=="completed"){
                $style ="style='background:#6cce47;border-color:#6cce47'";
              }else if(strtolower(trim($value['saving_status']))=="in progress"){
                $style ="style='background:#f0ad4e;border-color:#f0ad4e'";
              }else if(strtolower(trim($value['saving_status']))=="not started"){
               
                $style ="style='background:#fb5a5a !important ;border-color:#fb5a5a !important'";
              }
                
            ?>
              <tr>
                    <td><a href="<?php echo $this->createAbsoluteUrl('savings/edit/' . $value['id']); ?>" class="btn btn-sm btn-success view-btn" style="text-decoration: none;" >View/Edit
                        </a>
                    </td>
                    <td style="text-align: center;"><?php echo $value['id']; ?></td>
                    <td style="text-align: center;" class="notranslate"><?php echo $value['title']; ?></td>
                    <td><p hidden="hidden" class="sample_date"><?php echo strtotime($value['created_at']);?></p><?php echo $value['created_at'] !='0000-00-00 00:00:00'?date(FunctionManager::dateFormat(), strtotime($value['created_at'])):''; ?></td>
                   <td><p hidden="hidden" class="sample_date"><?php echo strtotime($value['due_date']);?></p><?php echo $value['due_date'] !='0000-00-00 00:00:00'?date(FunctionManager::dateFormat(), strtotime($value['due_date'])):'';?></td>
                    <td style="margin-left: 10px; width: 10px;"><?php echo $value['user_name']; ?></td>
                    <td><?php echo $value['currency']; ?></td>
                    <td class="td-center"><?php echo $value['total_base_spend']>0?$value['currency_symbol'].number_format($value['total_base_spend'],0,"",""):'';?></td>
                    <td  class="td-center"><?php echo $value['total_project_saving']>0?$value['currency_symbol'].number_format($value['total_project_saving'],0,"",""):'';?></td>
                    <td  class="td-center"><?php echo $value['total_project_saving_perc']>0?number_format($value['total_project_saving_perc'],0,"","").'%':'';?></td>
                    <td  class="td-center"><?php echo $value['realised_saving']>0?$value['currency_symbol'].number_format($value['realised_saving'],0,"",""):'';?></td>
                    <td  class="td-center"><?php echo $value['realised_saving_perc']>0?number_format($value['realised_saving_perc'],0,"","").'%':'';?></td>
                    <td> 
                    <button type="submit" class="btn btn-success" <?php echo $style;?>><?php echo $value['saving_status']; ?></button></td>
              </tr>
                <?php } ?>
              </tbody>

          </table>
        <?php } ?>