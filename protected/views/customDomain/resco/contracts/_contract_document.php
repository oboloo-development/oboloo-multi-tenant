<?php $disabled = FunctionManager::sandbox(); ?>
<div class="modal fade" id="request_contract_document_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg"><div class="modal-content" style=""> 

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Request A New Document From Supplier</h4>
        <div id="request_document_form_alert" class="alert alert-success" role="alert"></div>
      </div>
    <form id="request_contact_form" class="form-horizontal col-md-12 form-label-left" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('contracts/requestContractDocument'); ?>">    
        <div class="form-group">
          <div class="col-md-6" id="document_contract_type_cont">
            <label class="control-label">Document Type  <span style="color: #a94442;">*</span></label>
            <?php $documentsTitle = FunctionManager::contractDocument();?>
            <select class="form-control select_search notranslate" name="document_contract_type" id="document_contract_type" required="required">
              <option value="">Select Document Type</option>
              <?php foreach($documentsTitle as $key=>$value){?>
              <option value="<?php echo $key?>"><?php echo $value;?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-6" id="document_contract_type_new_cont">
            <label class="control-label">Document Type  <span style="color: #a94442;">*</span></label>
            <input type="text" name="contract_type_new" id="contract_type_new" class="form-control notranslate" placeholder="New Document Type" />
          </div>
          <div class="clearfix"></div><br />
 
     </div>
     <div class="form-group"> 
        <div class="supplier_invite_container">
        <div class="vendor_new_QIX">
         <div class="col-md-12 date-input valid">
          <label class="control-label">Supplier Contact <span style="color: #a94442;">*</span></label><select class="form-control notranslate vendor_contact_new_QIX vendor_new_QIX"  required="required"  name="vendor_contact_name[]" onChange="contactSelect(this.value,'QIX')">
          </select>
         </div>
          <div class="col-md-2 col-sm-1 col-xs-12" style="text-align: right;">
         <div class=" remove-supplier" id="" title=" Remove Supplier" onclick="removeSupplier(QIX);" style="color: #d86868; font-size: 30px; font-weight: 700;cursor: pointer;padding-top: 25px; ">-</div>
       </div>
         <div class="clearfix"></div><br /><br />
        </div>
      </div>
        <div id="more_supplier_container">
        </div>
        <div class="clearfix"></div><br />
         <!-- <div class=" pull-right" id="add_supplier_contianer" title=" Add Supplier" style="    font-size: 30px; font-weight: 700;margin-right: 12px;cursor: pointer;position: relative;bottom: 15px;">+</div> -->

      </div>
       <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12 valid">
              <label class="control-label">Description/Notes <span style="color: #a94442;">*</span></label>
              <textarea class="form-control notranslate" name="request_document_note" id="request_document_note" rows="4" placeholder="Description/Notes" required></textarea>
          </div>
      </div>
  <input type="hidden" name="request_document_vendor_id"  value="<?php echo $vendor_id;?>" />
  <input type="hidden" name="request_document_contract_id"  value="<?php echo $contract_id;?>" />
  <input type="hidden" name="" id="total_contacts" value="1">
  <div class="modal-footer">
    <button type="submit" class="btn btn-success submit-btn"  <?php echo $disabled; ?> >Submit Request</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div></form><div class="clearfix"></div></div></div></div>

  <script type="text/javascript">
  $(document).ready(function(){

    $("#document_contract_type_new_cont").hide();
    $("#btn_remove_document_type").hide();
    
    $(".select_search").select2({
    placeholder: "Select",
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });

  $("#request_document_form_alert").hide();
  $("#request_contact_form").submit(function(e){
  e.preventDefault();
    var form = $(this);
    var url = form.attr('action');          
    $.ajax({
        dataType: "json",
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
        success: function(submitted_response){
          if(submitted_response.msg==1){
            $("#request_document_form_alert").show();
            $("#request_document_form_alert").html('Request Contract Document submitted successfully.');
          }else{
            $("#request_document_form_alert").html('Problem occured while processing Request Contract Document, try again');
            $("#request_document_form_alert").show();
          }
          $(".select_search").val('').trigger('change');
          $("#contract_type_new").val('');
          
          $(".vendor_new_QIX").val('');
          $("#request_document_note").val('');
          window.location.assign(submitted_response.url);
        }
     });
     $("#request_document_form_alert").delay(1000*5).fadeOut();
      setTimeout(function() {
              $("#request_contract_document_modal").hide();
              $('.modal-backdrop.in').css('opacity', '0');
      }, 5000);
  });
  });

  function supplierSelect(vendor_id,serialNum){
  var vendor_id = vendor_id;
  var serialNum = serialNum;

  var obj ='';
  if(serialNum=='QIX'){ 
    obj = $(".vendor_new_vendor_name_QIX");
  }else{
      obj = $(".vendor_new_vendor_name_"+serialNum);
  }
  $(obj).next('.select2-container').find('.select2-selection--single').removeClass('invalid');

   $.ajax({
          dataType: 'json',  
          type: "post",
          url: "<?php echo AppUrl::bicesUrl('vendors/getVendorDetail'); ?>",
          data: { vendor_id: vendor_id },
          success: function(data_option){

          if(serialNum=='QIX'){
             $(".vendor_contact_new_QIX").html(data_option.contact_name);
             //$(".vendor_email_new_QIX").val(data_option.emails);
          }else{
           $(".vendor_contact_new_"+serialNum).html(data_option.contact_name);
           //$(".vendor_email_new_"+serialNum).val(data_option.emails);
          }
          }
        });
}

supplierSelect('<?php echo $vendor_id;?>','QIX');

function contactSelect(contactName,serialNum){
  var obj='';
  if(serialNum=='QIX'){
    
     obj = $(".vendor_contact_new_QIX");
  }else{
      
     obj = $(".vendor_contact_new_"+serialNum);
  }
  $(obj).removeClass('invalid'); 
  var serialNum = serialNum;
  var contactName = contactName;

   $.ajax({
          dataType: 'json',  
          type: "post",
          url: "<?php echo AppUrl::bicesUrl('vendors/getVendorContactEmail'); ?>",
          data: { vendor_id: '<?php echo $vendor_id;?>',contact_name:contactName },
          success: function(data_option){

          if(serialNum=='QIX'){
             $(".vendor_email_new_QIX").html(data_option.emails);
          }else{
           $(".vendor_email_new_"+serialNum).html(data_option.emails);
          }
          }
        });
}


function emailSelect(contactName,serialNum){
  var obj='';
  if(serialNum=='QIX'){
    obj = $(".vendor_email_new_QIX");
  }else{
      obj = $(".vendor_email_new_"+serialNum);
  }
   $(obj).removeClass('invalid'); 
}

  $(document).ready(function(){
    $(".remove-supplier").hide();
    $("#add_supplier_contianer").click(function(){

        var total_contacts = $('#total_contacts').val();
        total_contacts = parseInt(total_contacts);
        total_contacts = total_contacts + 1;
        $('#total_contacts').val(total_contacts);

        var supplierCon = $(".supplier_invite_container").last().html();
   
       supplierCon = supplierCon.replace(/QIX/g, total_contacts);
        
   
      $("#more_supplier_container").append(supplierCon);
      $("#more_supplier_container .remove-supplier").show();

      

    });

    $("#request_contact_form").validate();
   });

  function removeSupplier(idnumber){

     $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Do you want to remove this contact?</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                $(".vendor_new_"+idnumber).remove();
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },
                
                
                
            }
        });
  
}
  </script>

<style type="text/css">
  .modal-title { font-size: 20px !important }
</style>