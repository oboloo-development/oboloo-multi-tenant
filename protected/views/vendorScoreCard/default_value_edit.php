<?php $scoreCardQuestion = 0; ?>
<div class="right_col" role="main">
  <div class="row-fluid tile_count">
    <?php $alert = Yii::app()->user->getFlash('success');
    if (!empty($alert)) { ?>
      <div class="clearfix"></div>
      <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $alert; ?></div>
    <?php } ?>

    <div class="col-md-6 col-xs-12 p-0">
     <h3>Manage Supplier Scorecard</h3>
    </div>
    <div class="col-md-6 col-xs-12 ">
     <a href="<?php echo Yii::app()->createUrl('vendorScoreCard/list') ?>" class="btn btn-default pull-right" style="background-color: #F79820;color: #fff;border-color: #F79820;">Return To Supplier Scorecard List</a>
    </div>

    <div class="span6 pull-left">
      <h4 class="subheading"><br>Create a supplier scorecard tailored to your specific supplier evaluation requirements. Define criteria under each scorecard Area with weighted scoring. </h4> <br /><br />
    </div>

    <div class="clearfix"> </div>
  </div>
  <div class="row">
    <div class="col-md-5 col-sm-5 col-xs-12">
      <div class="x_panel tile ">
        <div class="x_title">
          <h2>Current Default Values (%)</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div id="scoring_pie_chart"></div><br />
        </div>
      </div>
    </div>


    <?php
    $data_types = array(
      'supplier_scoring_criteria'  => 'Supplier Scoring Criteria',
    );
    ?>

    <div class="col-md-7 col-sm-7 col-xs-12">
      <div class="tile_count" style="margin-top: -3px;">
        <form id="settings_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('vendorScoreCard/updateDefaultValue'); ?>">
          <div class="form-group">
            <div class="col-md-10">
              <label>Score Card Title <span style="color:red;">*</span></label>
              <input type="text" class="form-control notranslate" name="scorecard" value="<?= $scoreCardName; ?>" placeholder="Type New Area/Category" id="scorecard" required />
            </div> 
          </div>
          <input type="hidden" name="vendor_scoring_name_id" value="<?= $vendor_scoring_name_id; ?>" />
          <div class="clearfix"><br /></div>
          <div id="datatype_values">
            <div class="form-group">
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-9 ">
                    <label style="font-size: 13px; padding-left: 5px;">Title</label>
                  </div>
                  <div class="col-md-3 text-center">
                    <label style="font-size: 13px;">Score (%)</label>
                  </div>
                </div>
              </div>
            </div>

            <?php $disabled = FunctionManager::sandbox();
                $permissionOnlySuperUser = Yii::app()->session['user_type'] != 4 ? 'disabled' : '';
             ?>
            <?php
             $total_rows = 0;
            foreach ($data as $row) {
              $total_rows += 1; ?>
              <div class="form-group" id="data_row_<?php echo $total_rows; ?>">
                <div class="col-md-10 col-sm-10 col-xs-10">
                  <div style="float: left; width: 80%;">
                    <input type="text" class="form-control notranslate" name="value_<?php echo $total_rows; ?>" id="value_<?php echo $total_rows; ?>" <?php if (isset($row['value']) && !empty($row['value'])) echo 'value="' . $row['value'] . '"';
                                                                                                                                                      else echo 'placeholder="Default Value Title"'; ?> required />
                  </div>
                  <div style="float: left;width: 2%">&nbsp;</div>
                  <div style="float: left;width: 18%;">
                    <input type="number" autocomplete="off" class="form-control text-center score notranslate" name="score_<?php echo $total_rows; ?>" id="score__<?php echo $total_rows; ?>" <?php if (isset($row['score'])) echo 'value="' . number_format($row['score']) . '"';
                                                                                                                                                                                              else echo '0'; ?> min="0" step="0.01" required  />
                  </div>
                  <input type="hidden" name="id_<?php echo $total_rows; ?>" id="id_<?php echo $total_rows; ?>" <?php if (isset($row['id']) && !empty($row['id'])) echo 'value="' . $row['id'] . '"';
                                                                                                                else echo '0'; ?> />
                  <input type="hidden" class="notranslate" name="deleted_flag_<?php echo $total_rows; ?>" id="deleted_flag_<?php echo $total_rows; ?>" value="0" />
                </div>
                
                <div class="col-md-2 col-sm-2 col-xs-4">
                  <a class="btn btn-sm btn-primary" <?php echo $disabled; ?> style="background: #f96969 !important;border:1px solid #f96969 !important;" onclick="deleteRow(<?php echo $total_rows; ?>,<?php echo $row['id']; ?>); return false;" <?= $permissionOnlySuperUser; ?>>Delete</a>
                </div>
                <?php 
                  $vendorScoringQuestion = new vendorScoringQuestion();
                  $vendorScoringQuestion = $vendorScoringQuestion->getAll(['scorecard_id' => $row['scorecard_id'],'vendor_scoring_id' => $row['id'],'order' => 'id asc']);
                  $count = 0;
                  foreach($vendorScoringQuestion as $question){
                    $count +=1;
                    if($count == 1){ 
                      $dNone = 'd-none';
                      $parentContrnerClass = "pl-15 mt7 col-md-9 col-sm-9 col-xs-9 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 scoreCardQuestion_<?= $scoreCardQuestion ?>_<?= $total_rows ?>"; }
                    else{ 
                      $dNone = '';
                      $parentContrnerClass = "mt7 col-md-10 col-sm-10 col-xs-10 mt7 scoreCardQuestion_<?= $scoreCardQuestion ?>_<?= $total_rows ?>";
                    }
                    if(!empty($question)){
                    $scoreCardQuestion += 1; ?>
                    <div style="display: flex;" class="<?= $parentContrnerClass; ?>"> 
                      <button type="button" class="btn btn-danger <?= $dNone; ?>"  onclick="questionDelete(<?= $total_rows ?>, <?= $scoreCardQuestion ?>);" <?= $permissionOnlySuperUser; ?>>Delete</button>
                      <input type="text" autocomplete="off" class="form-control question_<?= $total_rows ?>" name="questions[<?= $total_rows ?>][<?= $scoreCardQuestion ?>]" value="<?= $question['question']; ?>" placeholder="Type New Criteria Here" id="question" required />
                    </div>
                   <?php }
                  } ?>

                <div class="add_question_row_<?= $total_rows ?>">
                 <div class="col-md-10 col-sm-10 col-xs-10 add_question_more_<?= $total_rows ?>"></div>
                  <div class="col-md-10 col-sm-10 col-xs-10">
                  <button type="button" class="btn btn-primary pull-right mt7 addQuestion" onclick="addQuestion(<?= $total_rows ?>)" <?= $permissionOnlySuperUser; ?>>Add Criteria</button>
                  </div>
                </div>
              </div>
              
            <?php } ?>
            </div>
          
            
          </div>
          <div class="clearfix"> </div>

          <div class="form-group">
            <div class="clearfix"> <br /> </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
              <input type="hidden" class="notranslate" name="data_type_change" id="data_type_change" value="0" />
              <input type="hidden" class="notranslate" name="total_rows" id="total_rows" value="<?php echo $total_rows; ?>" />
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-8 col-sm-6 col-xs-6">
                <div class="scoring_alert text-center"></div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="scoring_percentage_alert text-center-md" style="margin-left: 27px;"></div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php if (isset($data_type) && !empty($data_type)) { ?>
                <div style="margin-top: 15px;">
                  <a style="cursor: pointer;" onclick="addRow(); return false;" class="btn btn-primary submit-btn" <?= $permissionOnlySuperUser; ?>>Add new Category/Area</a>
                  <?php if ($disabled == 'disabled') { ?>
                   <a href="javascript:void(0);" <?php echo $disabled; ?> <?= $permissionOnlySuperUser; ?>>
                    <button type="button" class="btn btn-success " <?php echo $disabled; ?> >Save Data</button>
                   </a>
                  <?php } else { ?>
                      <button type="submit" class="btn btn-success submit-btn1 score_btn <?= $permissionOnlySuperUser; ?>">Save Data</button>
                  <?php } ?>
                </div>
              <?php } ?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div id="new_row_code">
    <div id="new_row_code_area">
      <div class="form-group" id="data_row_ROWIDX">
        <div class="col-md-10 col-sm-10 col-xs-8">

          <div style="float: left; width: 80%;">
           <input type="text" class="form-control notranslate" name="value_ROWIDX" placeholder="Type New Area/Category" required />
          </div>
          <div style="float: left;width: 2%">&nbsp;</div>
          <div style="float: left;width: 18%;">
           <input type="number" class="form-control score text-center notranslate" name="score_ROWIDX" placeholder="Scoring" min="0" value="0" step="0.01" required />
          </div>

          <div class="add_question_row_ROWIDX">
           <div class="mt7 col-md-11 col-sm-9 col-xs-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 scoreCardQuestion_3_3 pr-0 scoreCardQuestion_ROWIDX_ROWIDXQEST">
            <input type="text" autocomplete="off" class="form-control question_QESTIDXS" name="questions[ROWIDX][QESTIDXS]" placeholder="Type New Criteria Here" required />
           </div>  

           <div class="col-md-12 col-sm-10 col-xs-10 p-0 add_question_more_ROWIDX"></div>
           <div class="col-md-12 col-sm-10 col-xs-10 p-0">
            <button type="button" class="btn btn-primary pull-right mt7 addQuestion" onclick="addQuestion(ROWIDX)" <?= $permissionOnlySuperUser; ?>>Add Criteria</button>
           </div>
          </div>

          <input type="hidden" class="notranslate" name="id_ROWIDX" id="id_ROWIDX" value="0" />
          <input type="hidden" class="notranslate" name="deleted_flag_ROWIDX" id="deleted_flag_ROWIDX" value="0" />
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4">
          <a class="btn btn-sm btn-primary" <?= $disabled ?> style="background: #f96969 !important;border:1px solid #f96969 !important; margin-top: 0px !important;" onclick="deleteRow(ROWIDX,0); return false;">Delete</a>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#new_row_code").hide();
  var scoreCardQuestion = "<?= $scoreCardQuestion ?>";
  function deleteRow(row_idx, valueID) {
    if (confirm('Are you sure you want to delete this row? This action cannot be reversed.')) {
      $('#deleted_flag_' + row_idx).val(1);
      $('#data_row_' + row_idx).remove();

      if (valueID != 0) {
        var valueID = valueID;
        $.ajax({
          url: '<?php echo AppUrl::bicesUrl('app/deleteDefaultValue'); ?>',
          type: 'POST',
          dataType: 'json',
          data: {
            valueID: valueID
          },
          success: function(chart_data) {}
        });
      }
    }

    loadScoring();
  }

  function addRow() {
    var total_rows = $('#total_rows').val();
    total_rows = parseInt(total_rows) + 1;
    $('#total_rows').val(total_rows);
    scoreCardQuestion +=1;
    var new_row_html = $('#new_row_code_area').html();
    new_row_html = new_row_html.replace(/ROWIDX/g, total_rows);
    new_row_html = new_row_html.replace(/QESTIDXS/g, Number(scoreCardQuestion));
    $('#datatype_values').append(new_row_html);

    equalizeSideBarWithBody();
  }

  function loadScoring() {
    var sum = 0;
    $(".score").each(function() {
      sum += parseFloat($(this).val());
      if (sum > 100 || sum < 100) {
        $(".second_btn").show();
        $('.second_btn').attr('disabled', 'disabled');
        $('.second_btn').click(function(e) {
          e.preventDefault();
          return false;
        });
        $(".score_btn").hide();
        $(".scoring_alert").html('<strong style="color:red">Default Values must add up to 100% </strong>');

      } else {
        $('.score_btn').removeAttr('disabled', 'disabled');
        $(".score_btn").show();
        $(".second_btn").hide();
        $(".scoring_alert").html('');
      }
    });
    $(".scoring_percentage_alert").html('<strong>' + sum + '%/100%</strong>');
  }

  $(document).on('keyup', ".score", function() {
    loadScoring();
  });


function addQuestion(rowID){
  scoreCardQuestion = Number(scoreCardQuestion) + 1;

  $('.add_question_more_'+rowID).append(`
    <div style="display: flex;" class=" mt7 scoreCardQuestion_${scoreCardQuestion}_${rowID}">
     <button type="button" class="btn btn-danger" onclick="questionDelete();">Delete</button>
     <input type="text" autocomplete="off" class="form-control question_${rowID}" name="questions[${rowID}][${scoreCardQuestion}]" placeholder="Type New Criteria Here" id="question" required />
    </div>  
  `);

  equalizeSideBarWithBody();
}

function questionDelete(){
  if (confirm('Are you sure you want to delete this question? This action cannot be reversed.')) {
    var parentDiv = event.target.closest('.mt7');
    if (parentDiv) {
        // Remove the parentDiv from the DOM
        parentDiv.remove();
    }
  }
}

</script>
<?php
if (FunctionManager::checkEnvironment(true)) { ?>
  <script type="text/javascript">
    $('.score_btn').removeAttr('disabled', 'disabled');
    $(".scoring_alert").html('');
  </script>
<?php } else if (FunctionManager::checkEnvironment(false)) { ?>
  <script type="text/javascript">
    loadScoring();
  </script>
<?php } ?>
<script type="text/javascript">
  /*$('.score').on('keyup' ,function(){
	loadScoring();
  });*/



  createPieChart();

  colors = ['#2d9ca2', '#66DA26', '#546E7A', '#E91E63', '#FF9800', '#2E93fA', '#2196F3', '#3a62ba', '#7b9333', '#344189', '#', '#', '#', '#', '#'];

  function createPieChart() {

    var quote_id = $("#quote_id").val();
    var pie_score_series = [];
    let pie_score_label  = <?= json_encode($pie_score_label); ?>;  // Convert PHP array to JS array directly

    let pie_score_arr = <?= json_encode($pie_score_series); ?>;   // Convert PHP array to JS array directly
      for (i = 0; i < pie_score_arr.length; i++) {
          pie_score_series.push(parseFloat(pie_score_arr[i]));
      }
        // START: Pie Chart
      var options = {
        chart: {
          fontFamily: 'Poppins !important',
          height: 200,
          width: 450,
          type: 'pie',
          id: "scoringPieChart"

        },
        tooltip: {
          y: {
            formatter: function(value, {
              series,
              seriesIndex,
              dataPointIndex,
              w
            }) {
              return value + '%'
            }
          }
        },
        legend: {
          show: false
        },
        dataLabels: {
          enabled: false
        },
        labels: pie_score_label,
        series: pie_score_series,
        responsive: [{
          breakpoint: 380,
          options: {
            chart: {
              width: 'auto'
              //height: 'auto',

            },
            legend: {
              show: false
            }
          }
        }],

      }
      var chart = new ApexCharts(
        document.querySelector("#scoring_pie_chart"),
        options
      );
      chart.render();

      ApexCharts.exec('scoringPieChart', 'updateOptions', {
        labels: pie_score_label,
      }, false, true);
      ApexCharts.exec('scoringPieChart', 'updateSeries', pie_score_series, true);

      // END: Pie Chart
  }

</script>