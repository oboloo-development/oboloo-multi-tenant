<?php
	$avg_budget_left = 0;
	if ($metrics['budget']['total_budget'])
		$avg_budget_left = (($metrics['budget']['total_budget'] - $metrics['spend']['spend_total']) * 100) / $metrics['budget']['total_budget']; 
?>

          <div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Budget Analysis</h3>
        </div>
        <div class="clearfix"> </div>
    </div>


          <div class="row tile_count">
            <div class="col-md-offset-1 col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Budget</span>
              <div class="count"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['budget']['total_budget'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Year Spend</span>
              <div class="count"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['spend']['spend_total'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Budget Left</span>
              <div class="count"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['budget']['total_budget'] - $metrics['spend']['spend_total'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-list"></i> Average Budget Left</span>
              <div class="count"><?php echo number_format($avg_budget_left, 2) . '%'; ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="color: red !important;">
              <span class="count_top"><i class="fa fa-list"></i> Departments Over Budget</span>
              <div class="count"><?php echo number_format($metrics['budget']['departments_over_budget'], 0); ?></div>
            </div>
          </div>


		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <div class="col-md-4">
			                  		<h2>Departments Spend vs Budget</h2>
			                  </div>
			                  <div class="col-md-3" style="text-align: right;">
			                  		<select name="location_id" id="location_id" class="form-control" onchange="loadBudgetDepartments(); createDepartmentChart(1);">			                  			
			                  			<?php foreach ($locations as $location) { ?>
			                  				<option value="<?php echo $location['location_id']; ?>"><?php echo $location['location_name']; ?></option>
			                  			<?php } ?>
			                  		</select>
			                  </div>
			                  <div class="col-md-2" style="text-align: right;">
			                  		<select name="department_id" id="department_id" class="form-control" onchange="createDepartmentChart(1);">
			                  		</select>
			                  </div>
			                  <div class="col-md-2" style="text-align: right;">
			                  		<select name="year" id="year" class="form-control" onchange="createDepartmentChart(1);">
			                  			<?php for ($y=(date("Y") - 3); $y<=(date("Y") + 1); $y++) { ?>
				                  			<option value="<?php echo $y; ?>" 
				                  					<?php if ($y == date("Y")) echo ' selected="SELECTED" '; ?>>
				                  				<?php echo $y; ?>
				                  			</option>
			                  			<?php } ?> 
			                  		</select>
			                  </div>
			                  <div class="col-md-1" style="text-align: right;">
				                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
					                  <li><a onclick="exportBudgetChart('departments_by_monthly_spend');"><i class="fa fa-cloud-download"></i></a></li>
				                  </ul>
				              </div>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="departments_by_monthly_spend" width="960px" height="400px"></canvas>
			              	</div>
			           	</div>
			        	</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

</div>

<script type="text/javascript">

var monthly_spend_chart_data;
var monthly_spend_chart_settings;
var monthly_spend_chart;

$(document).ready(function() {
	loadBudgetDepartments();
	createDepartmentChart(0);
});	

function createDepartmentChart(changed_period)
{
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('budget/getDepartmentSpendData'); ?>',
        type: 'POST', dataType: 'json', 
        data: { department_id: $('#department_id').val(), location_id: $('#location_id').val(), year: $('#year').val() },
        success: function(budget_data) {
            var chart_labels = new Array();
            var budgets = new Array();
            var spends = new Array();
            var maxValue = 0;
            
            for (var i=0; i<budget_data.length; i++) {
            	chart_labels[i] = budget_data[i].department_name;
            	budgets[i] = budget_data[i].budget;
            	spends[i] = budget_data[i].spent;
            	
            	if (budget_data[i].spent > maxValue) maxValue = budget_data[i].spent;
            	if (budget_data[i].budget > maxValue) maxValue = budget_data[i].budget;
            }
            
           	monthly_spend_chart_data = {
           		labels: chart_labels,
           		datasets: [
					{ label: 'Spent', backgroundColor: '#949FB1', data: spends }, 
					{ label: 'Budget', backgroundColor: '#FDB45C', data: budgets } 
           		]
			};

			monthly_spend_chart_settings = {
				type: 'bar',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: monthly_spend_chart_data,
				options: { 
						responsive: true,
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									var prefix = "";
									if (tooltipItem.datasetIndex == 0) prefix = 'Spent'; else prefix = 'Budget';
									
								    if (parseInt(value) >= 1000){
								    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								    } else {
								    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
								    }
								}						
							}
						},
						scales: {
							xAxes: [{ categoryPercentage: 0.3 }],
							yAxes: [{ 
										categoryPercentage: 0.3,
										ticks: { 
	            							min: 0, max: maxValue,
											callback: function(value, index, values) {
              									if (parseInt(value) >= 1000) {
                									return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              									} else {
                									return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
              									}	            	
	            							}
	            						}                									 
								}]
                    	}
					 }
			};
			
			if (changed_period == 1) monthly_spend_chart.destroy();
			if (maxValue != 0) delete monthly_spend_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'];
			else monthly_spend_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'] = 1000;
			monthly_spend_chart = new Chart($('#departments_by_monthly_spend'), monthly_spend_chart_settings);
        }
    });
	
}

function loadBudgetDepartments()
{
	if ($('#location_id').val() == 0) $('#department_id').html('');
	else
	{
	    $.ajax({
	        type: "POST", data: { location_id: $('#location_id').val() }, 
	        dataType: "json", async: false,
	        url: BICES.Options.baseurl + '/locations/getDepartments',
	        success: function(options) {
	        	var options_html = '';
	        	for (var i=0; i<options.length; i++)
	        		options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
	        	$('#department_id').html(options_html);
	        },
	        error: function() { $('#department_id').html(''); }
	    });
	}
}



function exportBudgetChart(canvas_id)
{
    var department_id = $('#department_id').val(); 
    var location_id = $('#location_id').val(); 
    var year = $('#year').val(); 

	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id + '&location_id=' + location_id + '&department_id=' + department_id + '&budget_year=' + year;
}

</script>
