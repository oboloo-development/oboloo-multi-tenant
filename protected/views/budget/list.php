<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <h3>Budgets</h3>
    </div>

<div class="row tile_count">
  <form id="budget_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('budget/list'); ?>">

      <?php
      $userObj = new User();
       $idx = 0; if (isset($budgets) && is_array($budgets) && count($budgets)) { ?>

          <?php
             $i = 1;
             foreach ($budgets as $a_budget) { $idx += 1;
                $approve = $userObj->checkPermission(Yii::app()->session['user_id'],0,0,"amend_budget");

                if(isset($approve) && $approve["amend_budget"]=="yes"){?>

              <div class="form-group">
                  <div class="col-md-2 col-sm-2 col-xs-4 date-input valid">
                      <?php if($i==1){ ?>
                      <label class="control-label">Location<span style="color: #a94442;">*</span></label>
                      <?php } ?>
                      <select required name="location_id[]" class="form-control" id="location_id_<?php echo $idx; ?>" onchange="loadDepartmentsForLocation(<?php echo $idx; ?>,<?php echo $a_budget['department_id'];?>);">
                          <option value="0">Select Location  </option>
                          <?php foreach ($locations as $location) { ?>
                              <option value="<?php echo $location['location_id']; ?>"
                                      <?php if ($a_budget['location_id'] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
                                  <?php echo $location['location_name']; ?>
                              </option>
                          <?php } ?>
                      </select>
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4 valid">
                       <?php if($i==1){ ?>
                      <label class="control-label">Department <span style="color: #a94442;">*</span></label>
                       <?php } ?>
                      <select required name="department_id[]" class="form-control" id="department_id_<?php echo $idx; ?>">
                          <option value="0">Select Department</option>
                      </select>
                  </div>

                  <div class="col-md-1 col-sm-1 col-xs-2 valid">
                      <?php if($i==1){ ?>
                          <label class="control-label">Year <span style="color: #a94442;">*</span></label>
                      <?php } ?>
                      <input type="text" required class="form-control text-right" name="year[]" value="<?php echo $a_budget['year']; ?>" placeholder="Year">
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4 valid">
                      <?php if($i==1){ ?>
                          <label class="control-label">Budget <span style="color: #a94442;">*</span></label>
                      <?php } ?>
                      <input type="text" required class="form-control text-right" name="budget[]" value="<?php echo $a_budget['budget']; ?>" placeholder="Budget">
                  </div>

                  <div class="col-md-2" <?php if($i==1){ ?> style="margin-top: 30px;" <?php } ?>>
                      <a href="<?php echo $this->createUrl('budget/delete',array('budget-id'=>$a_budget['id']));?>" class="btn btn-link" onclick="return confirm('Are you sure you want to delete this budget item?');">
                          <span class="fa fa-remove hidden-xs"></span>
                      </a>
                  </div>
              </div>

          <?php }
          $i++; }
          ?>

      <?php } else { ?>
      <div class="row tile_count" style="margin-left: 12px;">No Record Found</div>
      <?php } ?>

      <fieldset id="additional-field-model">
      <div class="form-group">
          <div class="col-md-2 col-sm-2 col-xs-4 valid">
              <select required name="location_id[]" class="form-control new_location">
                  <option value="0">Select Location</option>
                  <?php 
                  foreach ($locations as $location) {
                      
                      $approve = $userObj->checkPermission(Yii::app()->session['user_id'],$location['location_id'],0,"amend_budget");

                      if(isset($approve) && $approve["amend_budget"]=="yes"){
                      ?>
                      <option value="<?php echo $location['location_id']; ?>">
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php }
                  } ?>
              </select>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-4 valid">
              <select required name="department_id[]" class="form-control new_department">
                  <option value="0">Select Department</option>
              </select>
          </div>

          <div class="col-md-1 col-sm-1 col-xs-2 valid">
              <input type="text" required class="form-control text-right" name="year[]" value="" placeholder="Year">
          </div>

          <div class="col-md-2 col-sm-2 col-xs-4 valid">
              <input type="text" required class="form-control text-right" name="budget[]" value="" placeholder="Budget">
          </div>

          <div class="col-md-2">
              <a href="javascript:void(0);" class="btn btn-link remove-this-field">
                  <span class="fa fa-remove hidden-xs"></span>
              </a>
              <a href="javascript:void(0);" class="btn btn-link create-new-field">
                  <span class="fa fa-plus hidden-xs"></span>
              </a>
          </div>
      </div>
      </fieldset>

    <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
    <input type="hidden" name="total_rows" id="total_rows" value="<?php echo $idx; ?>" />

    </div>
    </div>

  </form>

  <a href="javascript:void(0);" onclick="$('#budget_form').submit();">
      <button type="button" class="btn btn-primary">
          Save Budgets
      </button>
  </a>

</div>
</div>


<script type="text/javascript">
$(document).ready( function() {
    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field",
        "onCreate": function() { createDepartmentSelectionFunctions(); }
    });

	function createDepartmentSelectionFunctions()
	{
		var total_rows = $('#total_rows').val();
		total_rows = parseInt(total_rows);

		$('.new_department').each(function() {
			total_rows += 1;
			$(this).removeAttr('id');
			$(this).attr('id', 'department_id_' + total_rows);
		});

		total_rows = $('#total_rows').val();
		total_rows = parseInt(total_rows);

		$('.new_location').each(function() {
			total_rows += 1;
			$(this).removeAttr('id');
			$(this).attr('id', 'location_id_' + total_rows);
			document.getElementById('location_id_' + total_rows).onchange = undefined;
			$(this).on('change', function() { loadDepartmentsForLocation(total_rows, 0); });
		});
	}

	createDepartmentSelectionFunctions();

      <?php $idx = 0; if (isset($budgets) && is_array($budgets) && count($budgets)) { ?>

          <?php foreach ($budgets as $a_budget) { $idx += 1; ?>
          	
          	loadDepartmentsForLocation(<?php echo $idx; ?>, <?php echo $a_budget['department_id']; ?>);

		  <?php } ?>
	  
	  <?php } ?>
})

function loadDepartmentsForLocation(location_idx, department_id)
{
	var location_id = $('#location_id_' + location_idx).val();
	var department_id = department_id;
	if (location_id == 0)
		$('#department_id_' + location_idx).html('<option value="0">Select Department</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { location_id: location_id,user_id:<?php echo Yii::app()->session['user_id'];?>,field_name:"amend_budget" }, dataType: "json",
	        url: BICES.Options.baseurl + '/locations/getDepartmentsByPerms',
	        success: function(options) {
	        	var options_html = '<option value="0">Select Department</option>';
	        	for (var i=0; i<options.length; i++)
	        		options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
	        	$('#department_id_' + location_idx).html(options_html);

	        	if (department_id != 0) $('#department_id_' + location_idx).val(department_id); 
	        },
	        error: function() { $('#department_id_' + location_idx).html('<option value="0">Select Department</option>'); }
	    });
	}
}

</script>
