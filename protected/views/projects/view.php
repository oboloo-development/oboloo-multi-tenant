<?php $formID="edit".$projectID;?>
<div class="right_col" role="main">
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Project - <?php echo $project['project_name'];?></h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('projects/list'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff; border-color:#F79820">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Projects List
                </button>
            </a>
        </div>

        <div class="clearfix"></div><div class="edit_msg_area"></div>

        <div class="clearfix"> </div>
        <?php if(Yii::app()->user->hasFlash('error')): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
        <?php elseif (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success" role="alert">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
      <?php endif; ?>
    </div>

<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs">
  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
    <li role="presentation" class="active">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="vendor-tab" role="tab" data-toggle="tab" aria-expanded="true">
            Project Details
        </a>
    </li>
    <li role="presentation" class="">
      <a href="#tab_content2" role="tab" id="spend-tab" data-toggle="tab" aria-expanded="false">
          Spend Analysis
      </a>
      </li>
    <li role="presentation" class="">
      <a href="#tab_content3" role="tab" id="contract-tab" data-toggle="tab" aria-expanded="false">
          Contracts
      </a>
    </li>
    <li role="presentation" class="">
      <a href="#tab_content4" role="tab" id="quote-tab" data-toggle="tab" aria-expanded="false">
          Quotes
      </a>
    </li>
  </ul>
  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="project-tab">
      <?php $this->renderPartial('_view',array('project'=>$project,'locations'=>$locations,'departments'=>$departments,'users'=>$users,'createdByUser'=>$createdByUser));?>
    </div>
    <div role="tabpanel" class="tab-pane" id="tab_content2" aria-labelledby="project-tab">
      <?php $this->renderPartial('_spend_analysis',array('project'=>$project,'orders'=>$orders,'projects_combo'=>$projects_combo));?>
    </div>
    <div role="tabpanel" class="tab-pane" id="tab_content3" aria-labelledby="project-tab">
      <?php $this->renderPartial('_contract',array('contracts'=>$contracts,'contracts_by_location'=>$contracts_by_location));?>
    </div>
    <div role="tabpanel" class="tab-pane" id="tab_content4" aria-labelledby="project-tab">
      <?php $this->renderPartial('_quote',array('quotes'=>$quotes));?>
    </div>
    
  </div>
</div>
</div>
<script type="text/javascript">
$(".edit_msg_area").hide();

<?php if (isset($project['location_id']) && !empty($project['location_id'])) { ?>

    <?php if (isset($project['department_id']) && !empty($project['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $project['department_id']; ?>);
    <?php } else { ?>
    loadDepartmentsForSingleLocation(0);
    <?php } } ?>
function loadDepartmentsForSingleLocation(department_id)
{ 
   var location_id = $('#location_id').val();
    var single = 1;
     
    {
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getDepartments',
            success: function(options) {
                var options_html = '<option value="">Select Department</option>';
                for (var i=0; i<options.length; i++){
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                }

                $('#department_id_detail').html(options_html);
                if (department_id != 0) $('#department_id_detail').val(department_id);
            },
            error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
        });
    }
}

function editProject(ele)
{        
        $('#loading_icon').show();
        var formObj = ele;
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('projects/edit'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: $("#"+formObj).serialize(),
            success: function(project_data) {
              $(".edit_msg_area").html('<div class="alert alert-success" role="alert">'+project_data.msg+'</div>');
              $(".edit_msg_area").show().delay(9000).fadeOut();
              //$('#project_table').DataTable().ajax.reload();
              //$('#project_table').DataTable().api().ajax.reload();
            }
        });
}
</script>

<script type="text/javascript">

    var project_combo_data = {
        labels: [
            <?php $c = 1; foreach ($projects_combo as $vendor_combo_row) { ?>

            '<?php
           echo date("M j", strtotime($vendor_combo_row['order_date'])); ?>' <?php if ($c != count($projects_combo)) echo ',';
         ?>

            <?php $c += 1; } ?>
        ],

        full_labels: [
            <?php $c = 1; foreach ($projects_combo as $vendor_combo_row) { ?>

            '<?php echo date("F j", strtotime($vendor_combo_row['order_date'])); ?>' <?php if ($c != count($projects_combo)) echo ','; ?>

            <?php $c += 1; } ?>
        ],

        datasets: [


            {
                type: 'bar',
                label: 'Amount Spent',
                backgroundColor: 'lightgreen',
                fill: false,
                yAxisID: 'Y2',
                data: [

                    <?php $c = 1; foreach ($projects_combo as $vendor_combo_row) { ?>

                    <?php echo round($vendor_combo_row['total_price'], 2); ?>
                    <?php if ($c != count($projects_combo)) echo ','; ?>

                    <?php $c += 1; } ?>

                ]
            }

        ]
    };

    var project_combo_chart =
        new Chart($('#project_combo_chart'),
            {
                type: 'bar',
                data: project_combo_data,
                options: {
                    scaleUse2Y: true,
                    responsive: true,
                    maintainAspectRatio: false,
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                var prefix = data.full_labels[tooltipItem.index];
                                var output = "";

                                if (tooltipItem.datasetIndex == 0)
                                {
                                    return value;
                                }
                                else
                                {
                                    if (parseInt(value) >= 1000){
                                        return output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else {
                                        return output = '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
                                    }
                                }
                            }
                        }
                    },
                    scales: {
                        xAxes: [{ ticks: { maxRotation: 0, minRotation: 0 } }],
                        yAxes: [
                            {
                                id: 'Y2', type: 'linear', position: 'left', display: true,
                                ticks: {
                                    min: 0,
                                    callback: function(value, index, values) {
                                        if (parseInt(value) >= 1000) {
                                            return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else {
                                            return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
                                        }
                                    }
                                }
                            },
                            { id: 'Y1', type: 'linear', position: 'right', display: true, ticks: { min: 0, stepSize: 1 }, gridLines: { drawOnChartArea: false } }
                        ]
                    }
                }
            }
        );
</script>

<script type="text/javascript">

        var location_contracts_chart_config = {
            type: 'bar',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    <?php $i = 1; foreach ($contracts_by_location as $a_contract) { ?>
                    "<?php echo addslashes($a_contract['location_name']); ?>",
                    <?php $i += 1; } ?>
                ],
                datasets: [

                    {
                        label: 'Location',
                        backgroundColor: [
                            <?php
                                $i = 1;
                                foreach ($contracts_by_location as $a_contract)
                                {
                                    if ($i == 1) $backgroundColor = "rgba(220,220,220,0.5)";
                                    else if ($i == 2) $backgroundColor = "rgba(151,220,187,0.5)";
                                    else if ($i == 3) $backgroundColor = "rgba(187,220,151,0.5)";
                                    else if ($i == 4) $backgroundColor = "rgba(220,151,187,0.5)";
                                    else if ($i == 5) $backgroundColor = "rgba(220,187,151,0.5)";
                                    else if ($i == 6) $backgroundColor = "rgba(187,151,220,0.5)";
                                    else if ($i == 7) $backgroundColor = "rgba(120,151,187,0.5)";
                                    else if ($i == 8) $backgroundColor = "rgba(151,120,187,0.5)";
                                    else if ($i == 9) $backgroundColor = "rgba(187,120,151,0.5)";
                                    else if ($i == 10) $backgroundColor = "rgba(151,187,120,0.5)";
                                    else $backgroundColor = "rgba(151,187,205,0.5)";

                                    echo "'" . $backgroundColor . "'";
                                    if ($i != count($contracts_by_location)) echo ',';
                                    $i += 1;
                                }
                            ?>
                        ],

                        data: [
                            <?php
                                $i = 1;
                                foreach ($contracts_by_location as $a_contract)
                                {
                                    echo round($a_contract['total'], 2);
                                    if ($i != count($contracts_by_location)) echo ',';
                                    $i += 1;
                                }
                            ?>

                        ]
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: { yAxes: [ { ticks: { min: 0, stepSize: 1 } } ] }
            }
        };

        var location_contracts_chart = new Chart($('#contracts_by_location'), location_contracts_chart_config);

</script>


<style type="text/css">
  .form-control {
    margin-bottom: 14px;
}
</style>