<?php
      $projectID = $project['project_id'];
      $formID="edit".$projectID;
      $projectName = $project['project_name'];
      $locOption='';
      foreach($locations as $location) {
        $locOption .='<option value="'.$location['location_id'].'" '.(isset($project['location_id']) && $location['location_id'] == $project['location_id']?'selected="SELECTED"':"").' >';
        $locOption .=$location['location_name'].' </option>';
      } 

      $depOption='';
      foreach($departments as $depValue) {
        $depOption .='<option value="'.$depValue['department_id'].'" '.(isset($project['department_id']) && $depValue['department_id'] == $project['department_id']?'selected="SELECTED"':"").' >';
        $depOption .=$depValue['department_name'].' </option>';
      } 

     $ownOption=$createdByOption='';
      foreach($users as $userValue) {
        $ownOption .='<option value="'.$userValue['user_id'].'" '.(isset($project['owner']) && $userValue['user_id'] == $project['owner']?'selected="SELECTED"':"").'>';
        $createdByOption .='<option value="'.$userValue['user_id'].'" '.(isset($project['created_by']) && $userValue['user_id'] == $project['created_by']?'selected="SELECTED"':"").' >';

        $ownOption .=$userValue['full_name'].' </option>';
        $createdByOption .=$userValue['full_name'].' </option>';
      } 

      $statusOption='';
      $projectStatus = FunctionManager::projectStatus();
      foreach ($projectStatus as $key=>$status) {
        $statusOption .='<option value="'.$key.'" '.($key == $project['status']?'selected="SELECTED"':"").'>'.$status.'</option>';
      }

      $createdBy=$createdByUser['full_name'];
      $createdAt = date("d/m/Y",strtotime($project['created_at']));


    
?>
<form id="<?php echo $formID;?>" action="" method="post">
<div class="col-md-6 col-sm-6 col-xs-12  valid">
<div class="form-group">
  <div class="col-md-12 col-sm-12 col-xs-12  valid">
    <label class="control-label">Project Name <span style="color: #a94442;">*</span></label>
    <input type="text" class="form-control" name="project_name" id="project_name_modal" placeholder="Project Name" value="<?php echo $projectName;?>" required="required" />
    <input type="hidden"  name="project_id" id="project_id"value="<?php echo $projectID;?>"  />
  </div></div><div class="clearfix"></div>

<div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12  valid">
  <label class="control-label">Location <span style="color: #a94442;">*</span></label>
  <select name="location_id" id="location_id" class="form-control" data="required" onchange="loadDepartmentsForSingleLocation(0);">
    <option value="">Select Location</option>
    <?php echo $locOption;?></select>
  </div></div><div class="clearfix"></div>

<div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12 valid">
  <label class="control-label">Department <span style="color: #a94442;">*</span></label>
  <select name="department_id" id="department_id_detail" class="form-control">
    <option value="">Select Department</option><?php echo $depOption;?></select></div></div><div class="clearfix"></div>
<div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12 ">
  <label class="control-label">Project Owner</label>
<select class="form-control" name="owner" id="owner">
<?php echo $ownOption;?>
</select></div></div><div class="clearfix"></div>

<div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12">
<label class="control-label">Created by</label>
<input type="text" class="form-control" name="created_by" value="<?php echo $createdBy;?>" readonly="readonly" />
<input type="hidden" class="form-control" name="created_by" id="created_by" value="<?php echo $project['created_by'];?>" readonly="readonly" /></div></div><div class="clearfix"></div>

<div class="form-group"><div class="col-md-12 col-sm-12 col-xs-12 ">
  <label class="control-label">Project Status</label>
  <select class="form-control" name="status" id="status">
    <?php echo $statusOption;?>
  </select></div></div><div class="clearfix"></div>

<div class="form-group"> <div class="col-md-12 col-sm-12 col-xs-12  date-input">
<label class="control-label">Created on</label>
<input type="text" class="form-control" name="created_at" id="created_at"
value="<?php echo $createdAt;?>" readonly="readonly"></div></div></div></form>
<div class="clearfix"></div><br />
<div class="col-md-6 col-sm-6 col-xs-6 text-right"><button  type="button" class="btn btn-primary btn sm" onclick="editProject('<?php echo $formID;?>');">Edit</button></div>
