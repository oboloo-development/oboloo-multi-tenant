<?php $record_user_id = 0;
if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Projects</h3>
        </div>

        <div class="span6 pull-right">
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bs-example-modal-lg" data-project-id="0" data-project-name="">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Project
            </button>
        </div>

        <div class="clearfix"> </div>
    </div>

    <table id="project_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Location</th>
          <th>Department</th>
          <th>Create By</th>
          <th>Owner</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>

      <tbody>

         <!--  <?php foreach ($projects as $project) { ?>

              <tr id="department_<?php echo $project['project_id']; ?>">
                    <td>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg"
                                data-project-id="<?php echo $project['project_id']; ?>" data-project-name="<?php echo $project['project_name']; ?>">
                            Edit
                        </button>
                    </td>
                  <td><?php echo $project['project_id']; ?></td>
                  <td><?php echo $project['project_name']; ?></td>
                  <td><?php echo $project['project_locations']; ?></td>
                  <td><?php echo $project['project_departments']; ?></td>
              </tr>

          <?php } ?> -->

      </tbody>

  </table>

</div>

<div id="project_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add New Project</h4>
      </div>
      <div class="modal-body">
        <form action="" method="post" id="project_create_form">
        <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
               <label class="control-label">Project Name <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control" name="project_name" id="project_name" placeholder="Project Name" value="" required="required" />
               
            </div>
        </div>
        <div class="clearfix"> <br /><br /> </div>
   <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12  valid">
        <label class="control-label">Location <span style="color: #a94442;">*</span></label>
        <select name="location_id" id="location_id" class="form-control" data="required" onchange="loadDepartmentsForSingleLocation(this);">
          <option value="">Select Location</option>
            <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($order['location_id']) && $location['location_id'] == $order['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
            <?php } ?>
        </select>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 valid">
              <label class="control-label">Department <span style="color: #a94442;">*</span></label>
              <select name="department_id" id="department_id_new" class="form-control">
                  <option value="">Select Department</option>
              </select>
          </div>
      </div>

        <div class="clearfix"> <br /><br />  </div>

        <div class="col-md-12 col-sm-12 col-xs-12 ">
              <label class="control-label">Project Owner</label>
              <select class="form-control" name="owner" id="owner">
                  <?php foreach ($users as $userValue) { ?>
                      <option value="<?php echo $userValue['user_id']; ?>"
                                <?php if ($record_user_id == $userValue['user_id']) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $userValue['full_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>


        <div class="clearfix"> <br /><br />  </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label">Created by</label>
               <input type="text" class="form-control" name="created_by" value="<?php echo $user['full_name'];?>" readonly="readonly" />

               <input type="hidden" class="form-control" name="created_by" id="created_by" value="<?php echo $user['user_id'];?>" readonly="readonly" />

             <!--  <select class="form-control" disabled="disabled">
                  <?php foreach ($users as $user) { ?>
                      <option value="<?php echo $user['user_id']; ?>"
                                <?php if ($record_user_id == $user['user_id']) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $user['full_name']; ?>
                      </option>
                  <?php } ?>
              </select> -->
          </div>

        <div class="clearfix"> <br /><br />  </div>

        <div class="col-md-12 col-sm-12 col-xs-12 ">
              <label class="control-label">Project Status</label>
              <select class="form-control" name="status" id="status">
                  <?php 

                  $projectStatus = FunctionManager::projectStatus();
                  foreach ($projectStatus as $key=>$status) { ?>
                      <option value="<?php echo $key; ?>">
                          <?php echo $status; ?>
                      </option>
                  <?php } ?>
              </select>
          </div><div class="clearfix"></div>

          <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12  date-input">
              <label class="control-label">Created on</label>
              <input type="text" class="form-control" name="created_at" id="created_at"
                    value="<?php echo date("d/m/Y");?>" readonly="readonly">
             
          </div></div>
          <div class="clearfix"> <br /><br />  </div></form>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="project_id" id="project_id" value="0" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="saveDepartmentButton" type="button" class="btn btn-primary" onclick="saveProject();">
            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading_icon" style="display: none;"></span>
            <span>Save Project</span>
        </button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  $(document).ready( function() {
  $(".edit_msg_area").hide();
  <?php if (isset($order['location_id']) && !empty($order['location_id'])) { ?>

    <?php if (isset($order['department_id']) && !empty($order['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $order['department_id']; ?>);
    <?php } else { ?>
    loadDepartmentsForSingleLocation(0);
    <?php } ?>

  <?php } ?>
  $('#project_table').dataTable({
        "columnDefs": [ {
            "targets": -1,
            "width": "6%",
           // "orderable": false
        } ],
        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
              for (var i=1; i<=6; i++)
                  $('td', row).eq(i).css('text-decoration', 'line-through');
        },        
        "order": [],
        "pageLength": 100,        
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?php echo AppUrl::bicesUrl('projects/listAjax'); ?>",
          "type": "POST",
          data: function ( input_data ) {
            input_data.search_para = input_data.search.value;
        } 
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" }
    });

  $('#delete_confirm_modal .modal-footer button').on('click', function(event) {
    var button = event.target; // The clicked button
  
    $(this).closest('.modal').one('hidden.bs.modal', function() {
        if (button.id == 'yes_delete') changeItemStatus();
      });
   });

});

function loadDepartmentsForSingleLocation(sel)
{ 
    var location_id = sel.value;
    var single = 1;
     
    {
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getDepartments',
            success: function(options) {
                var options_html = '<option value="">Select Department</option>';
                for (var i=0; i<options.length; i++){
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                }

                $('#department_id_new').html(options_html);
                $('#department_id_detail').html(options_html);
                //if (department_id != 0) $('#department_id').val(department_id);
            },
            //error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
        });
    }
}


var project_locations = new Array();
<?php
foreach ($projects as $project_id => $project_data) { ?>
project_locations[<?php echo $project_id; ?>] = '<?php if (isset($project_data['locations'])) echo implode(",", $project_data['locations']); ?>';
<?php } ?>

var project_departments = new Array();
<?php
foreach ($projects as $project_id => $project_data) { ?>
project_departments[<?php echo $project_id; ?>] = '<?php if (isset($project_data['departments'])) echo implode(",", $project_data['departments']); ?>';
<?php } ?>

$(document).ready( function() {
  

//    $('#project_table').dataTable({
//        "columnDefs": [{
//            "targets": 0,
//            "width": "12%",
//            "orderable": false
//        },
//        {
//            "targets": 5,
//            "visible": false,
//            "searchable": false
//        }],
//        "createdRow": function(row, data, dataIndex) {
//            $(row).attr('id', 'project_' + data[3]);
//        },
//        "order": []
//    });
})

$("#project_modal").on('show.bs.modal', function (e) {
    var project_id = $(e.relatedTarget).data('project-id');
    var project_name = $(e.relatedTarget).data('project-name');
    var current_project_locations = '';
    var current_project_departments = '';

    if (project_id == 0)
    {
        $('#myModalLabel').html('Add New Project');
        $('#project_name').val('');
        $('#locations').select2().val(null).trigger('change');
        $('#departments').select2().val(null).trigger('change');
    }
    else
    {
        $('#myModalLabel').html('Edit Project');
        $('#project_name').val(project_name);
        
        /*current_project_locations = project_locations[project_id];
        console.log(current_project_locations);
	    var location_ids = new Array();
	    if (current_project_locations.indexOf(",") >= 0)
	    	location_ids = current_project_locations.split(',');
	    else if ($.trim(current_project_locations) != "0")
	    	location_ids.push(current_project_locations);
	    
        $('#locations').select2().val(null).trigger('change');
        if (location_ids.length > 0)
            $('#locations').select2().val(location_ids).trigger('change');*/


      /*  current_project_departments = project_departments[project_id];
        var department_ids = new Array();
        if (current_project_departments.indexOf(",") >= 0)
            department_ids = current_project_departments.split(',');
        else if ($.trim(current_project_departments) != "0")
            department_ids.push(current_project_departments);*/

        /*$('#departments').select2().val(null).trigger('change');
        if (department_ids.length > 0)
            $('#departments').select2().val(department_ids).trigger('change');*/

    }
    $('#project_id').val(project_id);
});

function saveProject()
{
    var project_name = $.trim($('#project_name').val());
    var selected_location = $('#location_id').val();
    var selected_department = $('#department_id_new').val();
    var selected_owner = $('#owner :selected').val();
    var selected_created_by = $('#created_by').val();
    var selected_status = $('#status').val();
    var created_at = $('#created_at').val();

        $('#loading_icon').show();
        var saved_project_id = 0;
        var new_locations_text = "";
        var new_departments_text = "";

        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('projects/save'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: $("#project_create_form").serialize(),
            success: function(new_project_data) {
          		
            }
        });

        $('#project_modal').modal('hide');
        $('#project_table').DataTable().ajax.reload();

}


function editProject(ele)
{
   
        $('#loading_icon').show();
        var formObj = ele;
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('projects/edit'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: $(formObj).serialize(),
            success: function(project_data) {
              $(".edit_msg_area").html('<div id="edit_msg_area" class="alert alert-success" role="alert">'+project_data.msg+'</div>');
              $(".edit_msg_area").show().delay(500).fadeOut();


              //$('#project_table').DataTable().ajax.reload();
              //$('#project_table').DataTable().api().ajax.reload();
            }
        });

        
        proModal = $(formObj).attr('id').replace ( /[^\d.]/g, '' );
        $('#pro'+proModal).modal('hide');
        $('#project_table').DataTable().ajax.reload();
}

</script>
<style type="text/css">
.select2-selection--multiple {
    border: 1px solid #4d90fe  !important;
    outline: 0;
    border-radius: 10px !important;
}
#project_create_form .form-control,[id^="edit"] .form-control {
    width: 100% !important;
}
.control-label { color: #73879C !important; }
</style>