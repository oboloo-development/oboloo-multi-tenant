<div class="col-md-12">
<?php if(count($contracts)>0) {?>
          <table id="contract_table" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                  <th>Action</th>
                  <th>Supplier</th>
                  <th>Reference</th>
                  <th>Title</th>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Amount in Tool Currency</th>
                  <th>Amount in Order Currency</th>
              </tr>
              </thead>

              <tbody>

              <?php 
              
              foreach ($contracts as $contract)
              {
                  $class = '';
                  if (strtotime($contract['contract_end_date']) <= time()) $class = ' class="danger" style="color: red;" ';
                  else
                  {
                      if (strtotime($contract['contract_end_date']) > strtotime("+1 Month")) $class = ' class="success" ';
                      else $class = ' class="warning" style="color: orange;" ';
                  }
                  ?>
                  <tr <?php if (!empty($class)) echo $class; ?>>
                      <td>
                          <a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract['contract_id']); ?>">
                              <button type="button" class="btn btn-sm btn-success">View</button>
                          </a>
                      </td>
                      <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $contract['vendor_id']); ?>"><?php echo $contract['vendor']; ?></a></td>
                      <td><?php echo $contract['contract_reference']; ?></td>
                      <td><?php echo $contract['contract_title']; ?></td>
                      <td><?php echo date("F j, Y", strtotime($contract['contract_start_date'])); ?></td>
                      <td><?php echo date("F j, Y", strtotime($contract['contract_end_date'])); ?></td>
                      <td style="text-align: right;">
                          <nobr>
                              <?php
                              switch ($contract['currency']) {
                                  case 'GBP' : $currency = '&#163;';
                                      break;
                                  case 'USD' : $currency = '$';
                                      break;
                                  case 'CAD' : $currency = '$';
                                      break;
                                  case 'CHF' : $currency = '&#8355;';
                                      break;
                                  case 'EUR' : $currency = '&#8364;';
                                      break;
                                  case 'JPY' : $currency = '&#165;';
                                      break;
                                  case 'INR' : $currency = '&#x20B9;';
                                      break;
                                  default :  $currency = '&#163;';
                                      break;
                              }
                              echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($contract['contract_value_GB'], 2); ?>
                          </nobr>
                      </td>
                      <td style="text-align: right;">
                          <nobr>
                              <?php
                              echo $currency . ' ' . number_format($contract['contract_value'], 2);
                              ?>
                          </nobr>
                      </td>
                  </tr>

              <?php } ?>
             
              </tbody>

          </table>
          <?php } if(count($contracts)>0) { ?>
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_20">
                  <div class="x_panel tile overflow_hidden">
                      <div class="x_title">
                          <h2>Contracts By Location</h2>
                          <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                              <li><a onclick="expandChart1(20);"><i class="fa fa-arrows-h"></i></a></li>
                              <li><a onclick="exportChart('contracts_by_location');"><i class="fa fa-cloud-download"></i></a></li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                          <canvas id="contracts_by_location" height="180px"></canvas>
                      </div>
                  </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_20">
                  <div class="x_panel tile overflow_hidden">
                      <div class="x_title">
                          <h2>Contracts By Location</h2>
                          <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                              <li><a onclick="collapseChart(20);"><i class="fa fa-arrows-h"></i></a></li>
                              <li><a onclick="exportChart('contracts_by_location');"><i class="fa fa-cloud-download"></i></a></li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                          <table class="tile_info" style="margin-left: 0px;">
                              <?php
                              $idx = 0;
                              foreach ($contracts_by_location as $dataset_idx=>$a_location)
                              {
                                  if ($dataset_idx === 'total') continue;
                                  $idx += 1;
                                  switch ($idx)
                                  {
                                      case 1  : $color = "rgba(220,220,220,0.5)"; break;
                                      case 2  : $color = "rgba(151,220,187,0.5)"; break;
                                      case 3  : $color = "rgba(187,220,151,0.5)"; break;
                                      case 4  : $color = "rgba(220,151,187,0.5)"; break;
                                      case 5  : $color = "rgba(220,187,151,0.5)"; break;
                                      case 6  : $color = "rgba(187,151,220,0.5)"; break;
                                      case 7  : $color = "rgba(120,151,187,0.5)"; break;
                                      case 8  : $color = "rgba(151,120,187,0.5)"; break;
                                      case 9  : $color = "rgba(187,120,151,0.5)"; break;
                                      case 10 : $color = "rgba(151,187,120,0.5)"; break;
                                      default : $color = "rgba(151,187,205,0.5)"; break;
                                  }
                                  ?>

                                  <tr>
                                      <td style="width: 5%;"><i class="fa fa-square" style="color: <?php echo $color; ?>!important;"></i></td>
                                      <td style="vertical-align: middle;">
                                          <?php echo addslashes($a_location['location_name']); ?> - <?php echo $a_location['total'] ; ?>
                                      </td>
                                  </tr>

                                  <?php
                              }
                              ?>
                          </table>
                      </div>
                  </div>
              </div>

          </div>
          <div class="clearfix"><br /></div>
          <?php } ?>
</div>