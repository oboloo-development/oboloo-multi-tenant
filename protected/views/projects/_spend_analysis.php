<div class="col-md-12">
<?php if(count($orders)>0) {?>
          <table id="order_table" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                  <th>Action</th>
                  <th>Order ID</th>
                  <th>Supplier</th>
                  <th>Date</th>
                  <th>Amount in Tool Currency</th>
                  <th>Amount in Order Currency</th>
                  <th>User</th>
                  <th style="width: 140px;">Status</th>
              </tr>
              </thead>

              <tbody>

              <?php
          
                  foreach ($orders as $order) { ?>
                      <tr>
                          <td>
                              <a href="<?php echo AppUrl::bicesUrl('orders/edit/' . $order['order_id']); ?>">
                                  <button type="button" class="btn btn-sm btn-success">View</button>
                              </a>
                          </td>
                          <td style="text-align: right;"><?php echo $order['order_id']; ?></td>
                          <td>
                              <a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a>
                          </td>
                          <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                          <td style="text-align: right;">
                              <nobr>
                                  <?php
                                  switch ($order['currency']) {
                                      case 'GBP' :
                                          $currency = '&#163;';
                                          break;
                                      case 'USD' :
                                          $currency = '$';
                                          break;
                                      case 'CAD' :
                                          $currency = '$';
                                          break;
                                      case 'CHF' :
                                          $currency = '&#8355;';
                                          break;
                                      case 'EUR' :
                                          $currency = '&#8364;';
                                          break;
                                      case 'JPY' :
                                          $currency = '&#165;';
                                          break;
                                      case 'INR' :
                                          $currency = '&#x20B9;';
                                          break;
                                      default :
                                          $currency = '&#163;';
                                          break;
                                  }
                                  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($order['total_price'], 2); ?>
                              </nobr>
                          </td>
                          <td style="text-align: right;">
                              <nobr>
                                  <?php
                                  echo $currency . ' ' . number_format($order['calc_price'], 2);
                                  ?>
                              </nobr>
                          </td>


                          <td>
                              <a href="<?php echo AppUrl::bicesUrl('users/edit/' . $order['user_id']); ?>"><?php echo $order['user']; ?></a>
                          </td>
                          <td>
                              <?php
                              if (empty($order['order_status'])) $order['order_status'] = 'Pending';
                              $status_style = "";

                              if ($order['order_status'] == 'Paid' || $order['order_status'] == 'Closed' || $order['order_status'] == 'Received')
                                  $status_style = ' style="border: 1px solid green !important; background-color: green !important; color: #fff !important;" ';
                              if ($order['order_status'] == 'Declined')
                                  $status_style = ' style="border: 1px solid #990000 !important; background-color: #990000 !important; color: #fff !important;" ';
                              if ($order['order_status'] == 'More Info Needed')
                                  $status_style = ' style="border: 1px solid #CC3333 !important; background-color: #CC3333 !important; color: #fff !important;" ';
                              if ($order['order_status'] == 'Pending' || $order['order_status'] == 'Ordered - PO Not Sent To Supplier'
                                  || $order['order_status'] == 'Submitted' || $order['order_status'] == 'Ordered - PO Sent To Supplier'
                              )
                                  $status_style = ' style="border: 1px solid orange !important; background-color: orange !important; color: #fff !important;" ';
                              if (!empty($order['description'])) {
                                  $toolTip = $order['description'];
                              } else {
                                  $toolTip = 'N/A';
                              }

                              ?>
                              <button class="btn btn-sm btn-success status"
                                      title="<?php echo $toolTip; ?>" <?php echo $status_style; ?>>
                                  <?php echo $order['order_status']; ?>
                              </button>
                          </td>
                      </tr>

                  <?php } ?>

              </tbody>

          </table>

          <?php } if(count($orders)>0) { ?>
          <div class="clearfix"> </div>
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_11">
                  <div class="x_panel tile overflow_hidden">
                      <div class="x_title">
                          <h2>Project Spend with Order</h2>
                          <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                              <li><a onclick="expandChart1(11);"><i class="fa fa-arrows-h"></i></a></li>
                              <li><a onclick="exportChart('project_combo_chart');"><i class="fa fa-cloud-download"></i></a></li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content" style="width: 100%">
                          <canvas id="project_combo_chart"></canvas>
                      </div>
                  </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_11">
                  <div class="x_panel tile overflow_hidden">
                      <div class="x_title">
                          <h2>Project Spend with Order</h2>
                          <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
                              <li><a onclick="collapseChart(11);"><i class="fa fa-arrows-h"></i></a></li>
                              <li><a onclick="exportChart('vendors_by_month');"><i class="fa fa-cloud-download"></i></a></li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                          <table class="tile_info" style="margin-left: 0px;">
                              <thead>
                              <th>Action</th>
                              <th>Order Date</th>
                              <th>Total</th>
                              </thead>
                              <?php
                              $idx = 0;
                              foreach ($projects_combo as $project_index => $combo)
                              {

                                  if ($project_index === 'total') continue;
                                  $idx += 1;
                                  switch ($idx)
                                  {
                                      case 1  : $color = 'blue'; break;
                                      case 2  : $color = 'purple'; break;
                                      case 3  : $color = 'green'; break;
                                      case 4  : $color = 'turquoise'; break;
                                      case 5  : $color = 'red'; break;
                                      case 6  : $color = 'antique-white'; break;
                                      case 7  : $color = 'aqua-marine'; break;
                                      case 8  : $color = 'bisque'; break;
                                      case 9  : $color = 'khaki'; break;
                                      case 10  : $color = 'blue'; break;
                                      case 11  : $color = 'purple'; break;
                                      case 12  : $color = 'green'; break;
                                      case 13  : $color = 'turquoise'; break;
                                      case 14  : $color = 'red'; break;
                                      case 15  : $color = 'antique-white'; break;
                                      case 16  : $color = 'aqua-marine'; break;
                                      case 17  : $color = 'bisque'; break;
                                      case 18  : $color = 'khaki'; break;
                                      case 19  : $color = 'blue'; break;
                                      case 20  : $color = 'purple'; break;
                                      case 21  : $color = 'green'; break;
                                      case 22  : $color = 'turquoise'; break;
                                      case 23  : $color = 'red'; break;
                                      case 24  : $color = 'antique-white'; break;
                                      case 25  : $color = 'aqua-marine'; break;
                                      case 26  : $color = 'bisque'; break;
                                      case 27  : $color = 'khaki'; break;
                                      case 28  : $color = 'blue'; break;
                                      case 29  : $color = 'purple'; break;
                                      case 30  : $color = 'green'; break;
                                      case 31  : $color = 'turquoise'; break;
                                      case 32  : $color = 'red'; break;
                                      case 33  : $color = 'antique-white'; break;
                                      case 34  : $color = 'aqua-marine'; break;
                                      case 35  : $color = 'bisque'; break;
                                      case 36  : $color = 'khaki'; break;
                                      default : $color = 'salmon'; break;
                                  }

                                  ?>


                                  <tr>
                                      <td style="width: 4%;"><i class="fa fa-square <?php echo $color; ?>" style="margin-left: 10px;"></i></td>
                                      <td style="width: 39%;vertical-align: middle;"><?php echo date("F j, Y", strtotime($combo['order_date'])); ?></td>
                                      <td style="width: 32%;vertical-align: middle;"> <?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format($combo['total_price'],2); ?></td>
                                  </tr>
                                  <?php

                              }
                              ?>
                          </table>
                      </div>
                  </div>
              </div>

          </div>
          <div class="clearfix"><br /></div>
          <?php } ?>
</div>