<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Projects</h3>
        </div>

        <div class="span6 pull-right">
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bs-example-modal-lg" data-project-id="0" data-project-name="">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Project
            </button>
        </div>

        <div class="clearfix"> </div>
    </div>

    <table id="project_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Action</th>
          <th>ID</th>
          <th>Name</th>
          <th>Locations</th>
          <th>Departments</th>

        </tr>
      </thead>

      <tbody>

          <?php foreach ($projects as $project) { ?>

              <tr id="department_<?php echo $project['project_id']; ?>">
                    <td>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg"
                                data-project-id="<?php echo $project['project_id']; ?>" data-project-name="<?php echo $project['project_name']; ?>">
                            Edit
                        </button>
                    </td>
                  <td><?php echo $project['project_id']; ?></td>
                    <td><?php echo $project['project_name']; ?></td>
                    <td><?php echo $project['project_locations']; ?></td>
                    <td><?php echo $project['project_departments']; ?></td>

              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>

<div id="project_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add New Project</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <div class="col-md-8 col-sm-8 col-xs-16">
                <input type="text" class="form-control has-feedback-left" name="project_name" id="project_name" placeholder="Project Name" value="" />
                <span class="fa fa-institution form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>
        <div class="clearfix"> <br /><br /> </div>
        <div class="form-group">
            <div class="col-md-8 col-sm-8 col-xs-16">
                <select multiple="multiple" name="locations[]" id="locations" class="form-control select2_multiple">
                    <?php foreach ($locations as $location) { ?>
                        <option value="<?php echo $location['location_id']; ?>">
                            <?php echo $location['location_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
          <div class="clearfix"> <br /><br /> </div>
          <div class="form-group">
              <div class="col-md-8 col-sm-8 col-xs-16">
                  <select multiple="multiple" name="departments[]" id="departments" class="form-control select2_multiple">
                      <?php foreach ($departments as $department) { ?>
                          <option value="<?php echo $department['department_id']; ?>">
                              <?php echo $department['department_name']; ?>
                          </option>
                      <?php } ?>
                  </select>
              </div>
          </div>
        <div class="clearfix"> </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="project_id" id="project_id" value="0" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="saveDepartmentButton" type="button" class="btn btn-primary" onclick="saveProject();">
            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading_icon" style="display: none;"></span>
            <span>Save Project</span>
        </button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

var project_locations = new Array();
<?php
foreach ($projects as $project_id => $project_data) { ?>
project_locations[<?php echo $project_id; ?>] = '<?php if (isset($project_data['locations'])) echo implode(",", $project_data['locations']); ?>';
<?php } ?>

var project_departments = new Array();
<?php
foreach ($projects as $project_id => $project_data) { ?>
project_departments[<?php echo $project_id; ?>] = '<?php if (isset($project_data['departments'])) echo implode(",", $project_data['departments']); ?>';
<?php } ?>

$(document).ready( function() {
    $("#locations").select2({placeholder: 'Select Locations'});
    $("#departments").select2({placeholder: 'Select Departments'});

//    $('#project_table').dataTable({
//        "columnDefs": [{
//            "targets": 0,
//            "width": "12%",
//            "orderable": false
//        },
//        {
//            "targets": 5,
//            "visible": false,
//            "searchable": false
//        }],
//        "createdRow": function(row, data, dataIndex) {
//            $(row).attr('id', 'project_' + data[3]);
//        },
//        "order": []
//    });
})

$("#project_modal").on('show.bs.modal', function (e) {
    var project_id = $(e.relatedTarget).data('project-id');
    var project_name = $(e.relatedTarget).data('project-name');
    var current_project_locations = '';
    var current_project_departments = '';

    if (project_id == 0)
    {
        $('#myModalLabel').html('Add New Project');
        $('#project_name').val('');
        $('#locations').select2().val(null).trigger('change');
        $('#departments').select2().val(null).trigger('change');
    }
    else
    {
        $('#myModalLabel').html('Edit Project');
        $('#project_name').val(project_name);
        
        current_project_locations = project_locations[project_id];
        console.log(current_project_locations);
	    var location_ids = new Array();
	    if (current_project_locations.indexOf(",") >= 0)
	    	location_ids = current_project_locations.split(',');
	    else if ($.trim(current_project_locations) != "0")
	    	location_ids.push(current_project_locations);
	    
        $('#locations').select2().val(null).trigger('change');
        if (location_ids.length > 0)
            $('#locations').select2().val(location_ids).trigger('change');


        current_project_departments = project_departments[project_id];
        var department_ids = new Array();
        if (current_project_departments.indexOf(",") >= 0)
            department_ids = current_project_departments.split(',');
        else if ($.trim(current_project_departments) != "0")
            department_ids.push(current_project_departments);

        $('#departments').select2().val(null).trigger('change');
        if (department_ids.length > 0)
            $('#departments').select2().val(department_ids).trigger('change');

    }
    $('#project_id').val(project_id);
});

function saveProject()
{
    var project_name = $.trim($('#project_name').val());
    var selected_locations = $('#locations').val();
    if (selected_locations == null) selected_locations = '';
    else selected_locations = selected_locations.toString();
    var selected_departments = $('#departments').val();
    if (selected_departments == null) selected_departments = '';
    else selected_departments = selected_departments.toString();

    if (project_name != '')
    {
        $('#loading_icon').show();
        var saved_project_id = 0;
        var new_locations_text = "";
        var new_departments_text = "";

        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('projects/save'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: {
                project_id: $('#project_id').val(),
                project_name: project_name,
          		locations: selected_locations,
          		departments: selected_departments,
            },
            success: function(new_project_data) {
          		saved_project_id = new_project_data.new_project_id;
          		new_locations_text = new_project_data.new_locations_text;
                new_departments_text = new_project_data.new_departments_text;
            }
        });

        if (saved_project_id == 0) saved_project_id = $('#project_id').val();
        var project_actions_html = '';
        project_actions_html = '<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg"';
        project_actions_html += ' data-project-id="' + saved_project_id + '" data-project-name="' + $.trim($('#project_name').val()) + '">Edit</button>';

        project_locations[saved_project_id] = selected_locations;
        project_departments[saved_project_id] = selected_departments;
        var table = $('#project_table').DataTable();
        table.row('#project_' + saved_project_id).remove().draw();

        $('#project_id').val(0);
        $('#project_name').val('');

        table.row.add([project_actions_html,saved_project_id, project_name, new_locations_text,new_departments_text ]).draw().node();
        $('#loading_icon').hide();
        $('#project_modal').modal('hide');
    }
}

</script>
