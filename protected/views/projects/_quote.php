<div class="col-md-12">
 <?php if(count($quotes)>0) {?>
          <table id="quote_table" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
              <tr>
                  <th>Action</th>
                  <th>Quote ID</th>
                  <th>Name</th>
                  <th>Opening Date</th>
                  <th>Closing Date</th>
                  <th>User</th>
                  <th>Status</th>
              </tr>
              </thead>

              <tbody>

              <?php
              foreach ($quotes as $quote) {
                  ?>
                  <tr>
                      <td>
                          <a href="<?php echo AppUrl::bicesUrl('quotes/edit/' . $quote['quote_id']); ?>">
                              <button type="button" class="btn btn-sm btn-success">View</button>
                          </a>
                      </td>
                      <td style="text-align: right;"><?php echo $quote['quote_id']; ?></td>
                      <td><?php echo $quote['quote_name']; ?></td>
                      <td><?php echo date("F j, Y H:iA", strtotime($quote['opening_date'])); ?></td>
                      <td><?php echo date("F j, Y H:iA", strtotime($quote['closing_date'])); ?></td>
                      <td> <a href="<?php echo AppUrl::bicesUrl('users/edit/'.$quote['user_id']); ?>"><?php echo $quote['full_name']; ?></a></td>
                      <td>
                          <?php
                          if (strtotime($quote['closing_date']) <= time()) echo 'Complete<br />';

                          if ($quote['submit_count'] <= 0)
                          {
                              if (strtotime($quote['closing_date']) > time()) echo 'Awaiting Quotes';
                          }
                          else
                          {
                              if ($quote['invite_count'] == 1)
                                  echo $quote['submit_count'] . ' of ' . $quote['invite_count'] . ' Quote received';
                              else
                                  echo $quote['submit_count'] . ' of ' . $quote['invite_count'] . ' Quotes received';
                          }
                          ?>
                      </td>
                  </tr>

              <?php } ?>

              </tbody>

          </table>
        <?php } ?>
</div>