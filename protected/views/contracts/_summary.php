<?php $contract_id = $contract['contract_id'];?>
<div class="clearfix"></div><br />
<div class="col-md-12">
<div class="x_title" style="border-bottom:none"><h4 class="heading">Contract Summary</h4></div>
<!-- <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($contract['vendor_id']) && !empty($contract['vendor_id'])) echo $contract['vendor_id']; else echo '0'; ?>" /> -->
<input type="hidden" name="form_summary" id="form_summary" value="1" />
</div>
<div class="clearfix"></div>
<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12 valid">
		<label class="control-label">Contract Title <span style="color: #a94442;">*</span></label>
		<input required type="text" class="form-control notranslate" name="contract_title"
			id="contract_title"
			<?php if (isset($contract['contract_title']) && !empty($contract['contract_title'])) echo 'value="' . $contract['contract_title'] . '"'; else echo 'placeholder="Contract Title"'; ?>>
		<input type="hidden" name="contractId" class="notranslate" id="contractId"
			value="<?php if (isset($contract_id) && !empty($contract_id)) echo $contract_id; else echo '0'; ?>" />
	</div>
</div>

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12 valid">
		<label class="control-label">Reference # <span style="color: #a94442;">*</span></label>
		<input required type="text" class="form-control notranslate" name="contract_reference"
			   id="contract_reference"
			<?php if (isset($contract['contract_reference']) && !empty($contract['contract_reference'])) echo 'value="' . $contract['contract_reference'] . '"'; else echo 'placeholder="Reference #"'; ?>>
	</div>
</div>

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-6">
		<label class="control-label">Contract Type</label>
		<select name="contract_type" id="contract_type"
			class="form-control notranslate">
			<option value="">Select Contract Type</option>
	  	  <?php
	  foreach ($contract_types as $contract_type) { ?>
	  	  		<option value="<?php echo $contract_type['value']; ?>"
				<?php if (isset($contract['contract_type']) && $contract['contract_type'] == $contract_type['value']) echo ' selected="SELECTED" '; ?>>
	  	  			<?php echo $contract_type['value']; ?>
	  	  		</option>
	  	  <?php } ?>
	  </select>
	</div>

	<!-- <div class="col-md-3 col-sm-3 col-xs-6">
		<label class="control-label">Contract Status <span style="color: #a94442;">*</span></label>
		<select name="contract_status" id="contract_status"
			class="form-control">
			<option value="">Select Contract Status</option>
	  	  <?php foreach ($contract_statuses as $contract_status) { ?>
	  	  		<option value="<?php echo $contract_status['code']; ?>"
				<?php if (isset($contract['contract_status']) && $contract['contract_status'] == $contract_status['code']) echo ' selected="SELECTED" '; ?>>
	  	  			<?php echo $contract_status['value']; ?>
	  	  		</option>
	  	  <?php } ?>
	  </select>
	</div> -->
</div>


<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-6 valid">
		<label class="control-label">Linked Locations <span style="color: #a94442;">*</span></label>
		<select required name="location_id[]" id="location_id" class="form-control notranslate"
			onchange="loadDepartmentsForSingleLocation(0);" multiple>
			<option value="">Select Locations</option>
			<?php foreach ($locations as $location) { ?>
				<option
						value="<?php echo $location['location_id']; ?>"
						<?php if (!empty($contract_locationIDs) && in_array($location['location_id'], $contract_locationIDs)) echo ' selected="SELECTED" '; ?>>
					<?php echo $location['location_name']; ?>
				</option>
			<?php } ?>
  		</select>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-6 valid">
		<label class="control-label">Linked Departments <span style="color: #a94442;">*</span></label>
		<select required name="department_id[]" id="department_id"
			class="form-control notranslate" multiple>
		</select>
	</div>
</div>

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12 valid">
		<label class="control-label">Contract Category <span style="color: #a94442;">*</span></label>
		<select required name="contract_category_id" id="contract_category_id"
			class="form-control notranslate" onchange="loadSubcategories(0);">
			<option value="">Select Contract Category</option>
		<?php foreach ($categories as $category) { ?>
			<option value="<?php echo $category['id']; ?>"
				<?php if (isset($contract['contract_category_id']) && $contract['contract_category_id'] == $category['id']) echo ' selected="SELECTED" '; ?>>
				<?php echo $category['value']; ?>
			</option>
		<?php } ?>
	</select>
	</div>
</div>

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12 valid">
		<label class="control-label">Contract Sub Category <span style="color: #a94442;">*</span></label>
		<select required name="contract_subcategory_id" id="contract_subcategory_id"
			class="form-control notranslate">
			<option value="">Select Contract Sub Category</option>
		</select>
	</div>
</div>
<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-6 date-input valid">
	  <label class="control-label">Supplier <!-- <?php echo $contract['vendor_id'];?> --></label>
	  <select  type="text" class="form-control vendor_id select_vendor_multiple notranslate"  name="vendor_id" id="vendor_id" style="  border-radius: 10px;border: 1px solid #4d90fe;">
	  	<option value="" ></option>
	    <?php foreach($vendor_list as $vendorInfo){
    			if($vendorInfo['vendor_id']==$contract['vendor_id']){
    				$selected="selected='selected'";
    			}else{
    				$selected="";
    			}
	    	?>
	    	<option value="<?php echo $vendorInfo['vendor_id'];?>" <?php echo $selected;?>><?php echo $vendorInfo['vendor_name'];?></option>

	    <?php } ?>

	</select>
	
    </div>
</div>

<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Contract Start Date</label>
		<input type="text" class="form-control contract_start_date notranslate"
			name="contract_start_date" id="contract_start_date"
			<?php if (isset($contract['contract_start_date']) && !empty($contract['contract_start_date']) && $contract['contract_start_date'] !="0000-00-00") echo 'value="' . date(FunctionManager::dateFormat(), strtotime($contract['contract_start_date'])) . '"'; else echo 'placeholder="Contract Start Date"'; ?>>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
	<label class="control-label">Contract End Date</label>
		<input type="text" class="form-control contract_end_date notranslate"
			name="contract_end_date" id="contract_end_date"
			<?php if (isset($contract['contract_end_date']) && !empty($contract['contract_end_date'])  && $contract['contract_end_date'] !="0000-00-00") echo 'value="' . date(FunctionManager::dateFormat(), strtotime($contract['contract_end_date'])) . '"'; else echo 'placeholder="Contract End Date"'; ?>>
	</div>
</div>

<div class="form-group">

	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Notice Period Date</label>
		<input type="text" class="form-control notice-period notranslate" autocomplete="off" name="break_clause"
			id="break_clause"
			<?php if (isset($contract['break_clause']) && $contract['break_clause'] !="0000-00-00 00:00:00") echo 'value="' . date(FunctionManager::dateFormat(), strtotime($contract['break_clause'])) . '"'; else echo 'placeholder="Notice Period Date"'; ?>>
	</div>

	<div class="col-md-3 col-sm-3 col-xs-6">
		<label class="control-label">Termination Terms</label>
		<input type="text" class="form-control termination_terms notranslate" name="termination_terms"
			id="termination_terms"
			<?php if (isset($contract['termination_terms']) && !empty($contract['termination_terms'])) echo 'value="' . $contract['termination_terms'] . '"'; else echo 'placeholder="Termination Terms"'; ?>>
	</div>
</div>
<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<label class="control-label">Description</label>
		<textarea class="form-control notranslate" name="contract_description"
			id="contract_description" rows="4"
			<?php if (!isset($contract['contract_description']) || empty($contract['contract_description'])) echo 'placeholder="Contract description"'; ?>><?php if (isset($contract['contract_description']) && !empty($contract['contract_description'])) echo $contract['contract_description']; ?></textarea>
	</div>
</div>


<div class="clearfix">
	<br />
</div>

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="hidden" name="contract_id" id="contract_id" class="notranslate"
			value="<?php if (isset($contract_id)) echo $contract_id; ?>" /> <input
			type="hidden" name="form_submitted" id="form_submitted" value="1"  class="notranslate" />
	</div>
</div>
<?php $this->renderPartial('_submit',array('contract_id'=>$contract_id,'contract'=>$contract));?>


<!-- <script>
	 $(".select_vendor_multiple").select2({
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"
      });
</script> -->