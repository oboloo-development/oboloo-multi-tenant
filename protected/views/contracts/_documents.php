<?php
$disabledArchiveContract = '';
$disabled = FunctionManager::sandbox();
// this disabled variable targer the archive and un-archive checkbox in the list.
if (strtolower($disabledArchive) == 'yes') {
  $disabledArchiveContract = 'disabled';
} else {
  $disabledArchiveContract = '';
}
$fliter = 'cd.contract_id=' . $contract_id; 
$fliterSupplier = 'crd.contract_id=' . $contract_id; 
if($filter_document_type){ 
  $fliter .= ' and cd.document_type ='. $filter_document_type; 
  $fliterSupplier .= ' and crd.contract_type=' . $filter_document_type; 
}
if(!empty($flter_document_status)){
  if($flter_document_status == "archive")
    $status = 1;
  else if($flter_document_status == "unarchive")
    $status = 0;
    $fliter .= ' and cd.archive ='. $status; 
    $fliterSupplier .= ' and crd.status="'.$status.'"'; 
}
if($fltr_doc_contract_supplier_status){
  $fliter .= ' and cd.uploaded_by_type ="'. $fltr_doc_contract_supplier_status.'" '; 
  $fliterSupplier .= ' and crd.status="'.$fltr_doc_contract_supplier_status.'"'; 
}

$modalId = 1;
$editForm = '';
$userPer = new User;
$approveDocument = $userPer->checkPermission(Yii::app()->session['user_id'], $location_id, $department_id, "contract_document");

$getContractDocument = CommonFunction::getContractDocumentTable($fliter, $fliterSupplier);
$documentDetailReader = Yii::app()->db->createCommand($getContractDocument)->queryAll();

$sql = "select id,name from contract_document_type where created_type ='General' and soft_deleted=0 order by name asc";
$reader = Yii::app()->db->createCommand($sql)->query()->readAll();

?>
  <div class="col-md-9 pl-0 mb-20">
    <form method="POST" action="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=document'); ?>">
      <div class="row">
        <div class="col-md-4">
          <select name="filter_document_type" id="document_type" class="form-control notranslate">
            <option value="">Select Document Type</option>
            <?php foreach ($reader as $documentTpye) { ?>
              <option value="<?php echo $documentTpye['id']; ?>" <?php if (isset($filter_document_type) && $filter_document_type == $documentTpye['id']) echo ' selected="SELECTED" '; ?>>
                <?php echo $documentTpye['name']; ?>
              </option>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-3">
          <select name="flter_document_status" id="flter_document_status" class="form-control">
            <option value="">Archived Status</option>
            <option value="archive" <?= !empty($flter_document_status) && $flter_document_status == "archive" ? 'selected="SELECTED"':''; ?>>Archived</option>
            <option value="unarchive" <?= !empty($flter_document_status) && $flter_document_status == "unarchive" ? 'selected="SELECTED"':''; ?>>Active</option>
          </select>
        </div>
        <div class="col-md-3">
          <select name="fltr_doc_contract_supplier_status" id="fltr_doc_contract_supplier_status" class="form-control">
            <option value="">Status</option>
            <option value="Vendor"  <?= !empty($fltr_doc_contract_supplier_status) && $fltr_doc_contract_supplier_status == "Vendor" ? 'selected="SELECTED"':''; ?>>Uploaded by supplier</option>
            <option value="Client"  <?= !empty($fltr_doc_contract_supplier_status) && $fltr_doc_contract_supplier_status == "Client" ? 'selected="SELECTED"':''; ?>>Uploaded Internally</option>
            <option value="Pending" <?= !empty($fltr_doc_contract_supplier_status) && $fltr_doc_contract_supplier_status  == "Pending"? 'selected="SELECTED"':''; ?>>Awaiting Upload From Supplier</option>
          </select>
        </div>
        <div class="col-md-2">
          <button class="btn btn-primary pull-right" type="submit" style="margin-top: 0px !important;">Apply Filters</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-3 mb-20">
    <form method="POST" action="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract_id . '?tab=document'); ?>">
      <input type="hidden" name="filter_document_type" value=""  />
      <input type="hidden" name="flter_document_status" value="" />
      <input type="hidden" name="fltr_doc_contract_supplier_status" value="" />
      <button class="btn btn-info search-quote" type="submit" style="border-color: #46b8da;">Clear Filters</button>
    </form>
  </div>
<table class="table table-striped table-bordered contract_document_type" style="width: 100%;">
  <thead>
    <tr>
      <th style="min-width: 10%">Document Type</th>
      <th style="width: 20%">Document Title</th>
      <th style="width: 8%">Approved</th>
      <th style="width: 10%">Approved By</th>
      <th style="width: 11%">Approved Date</th>
      <th style="width: 13%">Expiry Date</th>
      <th style="width: 12%">Uploaded Date</th>
      <th style="width: 6%">Archive</th>
      <th style="width: 8%">Status</th>
      <th style="width: 8%">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($documentDetailReader as $detailKey => $detailValue) {
      $modalIDExp = "edit_document" . $modalId;
      $modalId++;
    ?>
      <tr>
        <td><span class="btn btn-success doc-btn-round notranslate" style=" font-size:10px !important; <?php echo FunctionManager::contractDocumentBgColor($detailValue['document_type']); ?>"><?php echo FunctionManager::contractDocumentTable($detailValue['document_type']); ?></span></td>
        <td style='word-break:break-all' class="notranslate"><?php echo $detailValue['document_title']; ?></td>
        <td>
          <?php if (!empty($detailValue) && isset($detailValue['pendiing_documents']) && $detailValue['pendiing_documents'] > 0 
            &&  !empty($approveDocument) && $approveDocument["contract_document"] == "yes"
            &&  $detailValue["type"] == "main_document") { ?>
            <input type="checkbox" id="approve_1" class="approve_check_btn_<?php echo $detailValue['id']; ?>" onclick="approveDocument(<?php echo $detailValue['id']; ?>,event);" <?php echo $disabled; ?>  />
          <?php } else if($detailValue["type"] == "main_document"){ ?>
            <input type="checkbox" checked disabled />
          <?php } ?>
        </td>
        <td>
         <?php 
          if($detailValue["type"] == "main_document"){
            echo  $detailValue['approved_by_name'];
          } ?>
        </td>
        <td><?php 
          if($detailValue["type"] == "main_document"){ 
            if($detailValue['approved_datetime'] != '0000-00-00' || $detailValue['approved_datetime'] != '01/01/1970'){
              echo date(FunctionManager::dateFormat(), strtotime($detailValue['approved_datetime']));
            }else{
              echo '';
            }
          } ?>
        </td>
        <td><?php
          if($detailValue["type"] == "main_document"){
            echo  $detailValue['expiry_date'] != '0000-00-00' ? date(FunctionManager::dateFormat(), strtotime($detailValue['expiry_date'])) : 'No Expiry Date Entered, please update'; ?> 
            &nbsp;&nbsp;&nbsp;
            <?php if (FunctionManager::checkEnvironment(true)) {
              if ($disabledArchive != 'yes') { ?>
                <button <?php echo $disabled; ?> class="btn btn-success btn-xs" onclick="$('#<?php echo $modalIDExp; ?>').modal('show');return false">Edit</button>
            <?php }
            } } ?>
        </td>
        <td><?php echo date(FunctionManager::dateFormat(), strtotime($detailValue['date_created'])); ?></td>
        <td>
          <?php
          if (empty($detailValue['archive']) &&  $detailValue["type"] == "main_document") { ?>
            <input type="checkbox" class="archive_check archive_check_btn_<?php echo $detailKey; ?>" onclick="documentArchive(<?php echo $detailValue['document_type']; ?>,event,<?php echo $detailValue['id']; ?>);" <?php echo $disabled; ?>>
          <?php } else if($detailValue["type"] == "main_document"){ ?>
            <input type="checkbox" class=" archive_check archive_check_btn_<?php echo $detailKey; ?>" onclick="documentUnArchive(<?php echo $detailValue['id']; ?>,event);" <?php echo $disabled; ?> checked />
          <?php } ?>
        </td>
        <td>
          <?php
          if (!empty($detailValue['uploaded_by_type']) && $detailValue['uploaded_by_type'] == 'Upload By Supplier') { 
            echo 'Awaitng upload by supplier';
          }else if (!empty($detailValue['uploaded_by_type']) && $detailValue['uploaded_by_type'] == 'Vendor') { 
            echo 'Uploaded by supplier';
          }else if(!empty($detailValue['uploaded_by_type']) && $detailValue['uploaded_by_type'] == 'Client'){
            echo 'Uploaded Internally';
          } ?>
        </td>
        <td>
          <?php if (!empty($detailValue['contract_id']) && $detailValue["type"] == "main_document") { ?>
            <a href="<?php echo Yii::app()->createAbsoluteUrl('contracts/documentDownload', array('id' => $detailValue['id']));  ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
          <?php } ?>
        </td>
      </tr>
    <?php

      $formID = "edit_document_form" . $detailValue['id'];
      $formEditFunc = "editSingleDocument('" . $formID . "','" . $modalIDExp . "')";
      $expiryField = "<label class='control-label'>Expiry Date</label><input type='text' class='form-control expiry_date_1' name='expiry_date_" . $formID . "' id='expiry_date_" . $formID . "' value='" . date(FunctionManager::dateFormat(), strtotime($detailValue['expiry_date'])) . "' placeholder='Expiry Date'/>";
      $idField = "<input type='hidden' name='document_id_" . $formID . "' id='document_id_" . $formID . "' value='" . $detailValue['id'] . "' />";
      $idField .= "<input type='hidden' name='contract_id_" . $formID . "' id='contract_id_" . $formID . "' value='" . $detailValue['contract_id'] . "' />";
      $editForm .=  '<div id="' . $modalIDExp . '" class="modal fade" role="dialog"><div class="modal-dialog" style="margin-top:125px;"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Edit ' . FunctionManager::contractDocument($detailValue['document_type']) . '-' . $detailValue['document_title'] . '</h4></div><div class="clearfix"></div>
      <form id="' . $formID . '" action="" method="post">
      <div class="modal-body">
        ' . $expiryField . $idField . '
      </div>
      </form><div class="clearfix"></div>
      <div class="modal-footer">

      <button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button>
      <button  type="button" class="btn btn-primary btn sm" onclick="' . $formEditFunc . '" >Edit</button></div></div></div></div>';
      echo $editForm;
    } ?>

  </tbody>
</table>

<script type="text/javascript">
  $(document).ready(function() {
    $("#document_type").select2();
    $("#document_type").select2();
    $("#fltr_doc_contract_supplier_status").select2();
    $("#flter_document_status").select2();
    $('.contract_document_type').dataTable({});
  });
</script>