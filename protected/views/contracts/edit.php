<?php $savingsCurrency = "You can add more currencies under the configurations menu";?>
 <?php $disabled = FunctionManager::sandbox(); 
$disabledArchive = CommonFunction::disabledArchive('contracts', $contract_id);

$sql="select vendor_id,vendor_name from vendors where active=1 and member_type=0 and (vendor_archive is null  or vendor_archive='') and quick_created not in('Yes and Quick Sourcing Evaluation','Yes')  ORDER by vendor_name ASC";
$vendor_list = Yii::app()->db->createCommand($sql)->query()->readAll(); ?>
<div class="right_col" role="main">

	<div class="row-fluid tile_count">
          <?php $alert=Yii::app()->user->getFlash('success');
          if(!empty($alert)) { ?>
          <div class="clearfix"></div>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $alert;?></div>
        <?php } ?>
		<div class="span6 pull-left">
			<!-- <h3> 
                <?php
                if (isset($contract_id) && $contract_id) {
                    echo 'Edit Contract';
                } else
                    echo 'Add Contract';
                ?>
            </h3> -->
            <h4>
            <?php
            $avgScore = '';
            if(10*$contract['avg_score']<=60 && 10*$contract['avg_score']>0){
                  $avgScore = "<span class='btn btn-primary' style='background: #F0DFDF !important;border-color: #F0DFDF  !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$contract['avg_score'])."%</span>";
            }else if(10*$contract['avg_score']>60 and 10*$contract['avg_score']<=80){
              $avgScore = "<span class='btn btn-primary' style='background: #FDEC90 !important;border-color: #FDEC90  !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$contract['avg_score'])."%</span>";
            }else if(10*$contract['avg_score']<=0){
              $avgScore = "<span class='btn btn-primary' style='background: #DCDCDC !important;border-color: #DCDCDC  !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$contract['avg_score'])."%</span>";
            }else{
              $avgScore = "<span class='btn btn-primary' style='background: #DDF1D5 !important;border-color: #DDF1D5  !important;border-radius: 10px;color:#2d9ca2 !important'>".number_format(10*$contract['avg_score'])."%</span>";
            }
                
            if(isset($contract['contract_reference'])){?>
            	<br /><strong class="notranslate">Contract Number</strong>&nbsp;:&nbsp;<span class="title-text notranslate"><?php echo $contract['contract_reference'];?></span>
            <?php }
            if(isset($contract['contract_title'])){?>
            	<br /><br /><strong class="notranslate">Contract Title</strong>&nbsp;:&nbsp;<span class="title-text notranslate"><?php echo $contract['contract_title'];?></span>
            <?php 
             }
            if(isset($contract['vendor_contact_name'])){?>
            	<br /><br /><strong class="notranslate">Supplier Name</strong>&nbsp;:&nbsp;<span class="title-text notranslate"><?php echo $contract['vendor'];?></span>
            <?php }if(isset($contract['vendor_contact_name'])){?>
            	<br /><br /><strong class="notranslate">Supplier Contact Name</strong>&nbsp;:&nbsp;<span class="title-text notranslate"><?php echo $contract['vendor_contact_name'];?></span>
            <?php } 
             if(!empty($contract['vendor_email'])){?>
            	<br /><br /><strong class="notranslate">Supplier Email</strong>&nbsp;&nbsp;:&nbsp;<span class="title-text notranslate"><?php echo $contract['vendor_email'];?></span>
            <?php } ?>
            <?php if(!empty($contract['vendor_phone'])){?>
                <br /><br /><strong class="notranslate">Supplier Phone</strong>&nbsp;&nbsp;:<span class="title-text"> <span class="title-text notranslate"><?php echo $contract['vendor_phone'];?></span>
            <?php } 
            ?>
            <br /><br /><strong class="notranslate">Average Performance Score : <?php echo $avgScore;?></strong><br /><br /> 
           </h4>
		</div>

		<div class="span6 pull-right">

            <?php if(strtolower($contract['contract_archive']) =='yes'){ ?>
                <a href="<?php echo AppUrl::bicesUrl('contracts/archiveList'); ?>">
                <button type="button" class="btn btn-info" style="">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                    Return to Archive Contract List
                </button></a>
                <a href="<?php echo AppUrl::bicesUrl('contracts/List'); ?>">
                <button type="button" class="btn btn-info" style="">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                    Return To Active Contracts
                </button>
                </a>
            <?php }else{?>
			<a href="<?php echo AppUrl::bicesUrl('contracts/list'); ?>">
				<button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;border-color: #F79820;">
					<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
					Return To Contract List
				</button>
			</a>
        <?php } ?>
		</div>


		<div class="clearfix"></div>
	</div>

	<?php if(Yii::app()->user->hasFlash('success')) { ?>
    <?php echo Yii::app()->user->getFlash('success');?>
<?php }

	 $this->renderPartial("_detail_statistic",array('contractStatistics'=>$contractStatistics));?>
	<div class="row tile_count" role="tabpanel"
		data-example-id="togglable-tabs">

	<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
    <li role="presentation" class="<?php echo empty($_GET['tab']) || $_GET['tab']=='summary'?'active':''?>">
        <a class="has-feedback-left pull-right" href="#tab_contract_summary" id="vendor-tab" role="tab" data-toggle="tab" aria-expanded="true">
           Contract Summary
        </a>
    </li>
    <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='finance'?'active':''?>">
        <a href="#tab_financial" role="tab" id="financial-tab" data-toggle="tab" aria-expanded="false">
            Financials
        </a>
    </li>
    <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='timeline'?'active':''?>">
        <a href="#tab_timeline" role="tab" id="timeline-tab" data-toggle="tab" aria-expanded="false">
            Timeline
        </a>
    </li>
    <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='people'?'active':''?>">
        <a href="#tab_people" role="tab" id="people-tab" data-toggle="tab" aria-expanded="false">
            People
        </a>
    </li>
    <?php if(true){?>
    <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='document'?'active':''?>">
        <a href="#tab_document" role="tab" id="document-tab" data-toggle="tab" aria-expanded="false">
            Documents
        </a>
    </li>
    <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='saving'?'active':''?>">
        <a href="#tab_saving" role="tab" id="saving-tab" data-toggle="tab" aria-expanded="false">
            Savings
        </a>
    </li>
  <?php } ?>
    <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='comments'?'active':''?>">
        <a href="#tab_comments" role="tab" id="comments-tab" data-toggle="tab" aria-expanded="false">
            Comments
        </a>
    </li>
    <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='notification'?'active':''?>">
        <a href="#tab_additional_details" role="tab" id="additional-details-tab" data-toggle="tab" aria-expanded="false">
            Notifications
        </a>
    </li>
    <?php if(!empty($quote)){?>
    <li role="presentation" class="" >
        <a href="#scoring_criteria_supplier_comparision" role="tab" id="supplier-evaluation" data-toggle="tab" aria-expanded="false">Scoring Criteria & Supplier Comparison</a>
    </li>
   <?php } ?>
   <!-- <li role="presentation" class="<?php //echo !empty($_GET['tab']) && $_GET['tab']=='supplier-comparision'?'active':''?>">
        <a href="#tab_content10" role="tab" id="supplier-comparision" data-toggle="tab" aria-expanded="false">
            Supplier Comparison
        </a>
      </li> -->
	</ul>

<form id="contract_form" class="form-horizontal form-label-left input_mask"
		enctype="multipart/form-data" method="post"
		action="<?php echo AppUrl::bicesUrl('contracts/edit'); ?>">
  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade <?php echo empty($_GET['tab']) || $_GET['tab']=='summary'?'in active':''?>" id="tab_contract_summary" aria-labelledby="vendor-tab">
      <?php $this->renderPartial('_summary',
      array('contract'=>$contract,'locations'=>$locations,'categories'=>$categories,
      'contract_types'=>$contract_types,'vendor_list'=>$vendor_list, 'contract_locationIDs' => $contract_locationIDs));?>
    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab']=='finance'?'in active':''?>" id="tab_financial" aria-labelledby="quote-tab" style="padding: 15px;">
      <?php $this->renderPartial('_financial',array('contract'=>$contract,'currency_rate'=>$currency_rate,'savingsCurrency'=>$savingsCurrency));?>
      <div class="clearfix"><br /></div>
    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) &&  $_GET['tab']=='timeline'?'in active':''?>" id="tab_timeline" aria-labelledby="quote-tab" style="padding: 15px;">
      <?php $this->renderPartial('_timeline',array('contract_id'=>$contract_id,'contract'=>$contract));?>
      <div class="clearfix"><br /></div>
    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) &&  $_GET['tab']=='people'?'in active':''?>" id="tab_people" aria-labelledby="quote-tab" style="padding: 15px;">
      <?php $this->renderPartial('_people',array('contract_id'=>$contract_id,'contract'=>$contract,'vendor_list'=>$vendor_list));?>
      <div class="clearfix"><br /></div>
    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab']=='comments'?'in active':''?>" id="tab_comments" aria-labelledby="quote-tab" style="padding: 15px;">
      <?php $this->renderPartial('_comments',array('contract_id'=>$contract_id,'contract'=>$contract,'contractCommHistory'=>$contractCommHistory));?>
      <div class="clearfix"><br /></div>
    </div>
    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) &&  $_GET['tab']=='notification'?'in active':''?>" id="tab_additional_details" aria-labelledby="quote-tab" style="padding: 15px;">
      <?php $this->renderPartial('_additional_details',array('contract'=>$contract,'contract_types'=>$contract_types,'contract_statuses'=>$contract_statuses,'notifications'=>$notifications));?>
      <div class="clearfix"><br /></div>
    </div>
    <?php if(!empty($quote)){?>
    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) &&  $_GET['tab']=='notification'?'in active':''?>" id="scoring_criteria_supplier_comparision" aria-labelledby="quote-tab" style="padding: 15px;">
      <?php $this->renderPartial('_scoring_criteria',array('quote'=>$quote,'scoringCriteria'=>$scoringCriteria,'quoteVendors'=>$quoteVendors,'quote_stacked_bar_graph'=>$quote_stacked_bar_graph,'quoteAnswerVendor'=>$quoteAnswerVendor));?>
      <div class="clearfix"><br /></div>
    </div>
    <?php } ?>
<!-- <div role="tabpanel" class="tab-pane fade" id="tab_content10" aria-labelledby="supplier-tab" style="padding: 15px;">
    <?php // $this->renderPartial('_supplier_comparision',array('quickComparison'=>$quickComparison,'vendor_id'=>$vendor_id,));?>
</div> -->
  <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab']=='document'?'in active':''?>" id="tab_document" aria-labelledby="quote-tab" style="padding: 15px;">
    <?php $this->renderPartial('documents',array('disabledArchive' => $disabledArchive, 'contract_id'=>$contract_id,'location_id'=>$contract['location_id'],
    'department_id'=>$contract['department_id'],'documentList'=>$documentList,
    'documenArchivetList'=>$documenArchivetList,'vendor_id'=>$contract['vendor_id'],
    'filter_document_type' => $filter_document_type,
    'flter_document_status' => $flter_document_status,
    'fltr_doc_contract_supplier_status' => $fltr_doc_contract_supplier_status));?>
    <div class="clearfix"><br /></div>
   </div>

   <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab']=='saving'?'in active':''?>" id="tab_saving" aria-labelledby="quote-tab" style="padding: 15px;">
      <?php $this->renderPartial('contract_saving',array('savings'=> $savings));?>
      <div class="clearfix"><br /></div>
   </div>
</div>
</form>
<?php $this->renderPartial('../shared_component/_log',array('logTitle' => 'Contract', 'logHistory'=>$contractLogs)); ?>
</div>
</div>

<!-- Contract Document  Modal -->
<div class="modal fade" id="contract_document_model" style="width: 1000px; margin-left: 150px;" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content" style="width: 1000px;">
			<div class="modal-header alert-info" style="width: 1000px;">
				<button type="button" class="close" data-dismiss="modal" style="width: 10px;float: none;">&times;</button>
				<h4 class="modal-title">Contract Document</h4>
			</div>
			<div class="clearfix"> </div>
			<div>
				<iframe id="content" style="width: 800px; min-height: 630px;" src=""></iframe>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  var contract_departmentIDs = <?php echo json_encode($contract_departmentIDs); ?>;
  for (var i = 0; i < contract_departmentIDs.length; i++) {
    contract_departmentIDs[i] = parseInt(contract_departmentIDs[i]);
  }

$(document).ready( function() {
  $(".processing_oboloo").css("display", "none");

  $("#location_id").select2({
    placeholder: "Select Locations",
    allowClear: true,
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });

  $("#department_id").select2({
    placeholder: "Select Departments",
    allowClear: true,
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });
    
  $('#order_file_1').bind('change', function() {
    var MAX_FILE_SIZE = 101 * 1024 * 1024;
    if(this.files[0].size > MAX_FILE_SIZE){
     alert("Maximum File Size of 100MB");
     this.value = "";
    }    
  });

  $(".termination_terms").change(function(){
    $(".termination_terms").val($(this).val());
  });

  $(".select_vendor_multiple").select2({
    placeholder: "Select Supplier",
    allowClear: true,
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });

  $(".select_internal").select2({
    placeholder: "Select Internal User",
    allowClear: true,
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });
  

	<?php if (isset($contract['contract_subcategory_id']) && !empty($contract['contract_subcategory_id'])) { ?>
		loadSubcategories(<?php echo $contract['contract_subcategory_id']; ?>);
	<?php } ?>
	
	function createDateRangePicker()
	{
    $('.note_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'}).on('dp.change', function (e) { $(this).val($(this).val());  }); 
	}

  $( "#contract_form" ).validate( {
    rules: {
      location_id: "required",
      department_id: "required",
		contract_title: "required",
      contract_status: "required"
    },
    messages: {
      location_id: "Location is required",
      department_id: "Department is required",
      contract_title: "Contract title is required",
			//contract_status: "Contract Status is required"
    },
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] ){
				$( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
			}

		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] ){
				$( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
			}

		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
			//$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error" );
			//$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });

    $('.contract_start_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'}).on('dp.change', function (e) { $('.contract_start_date').val($(this).val());  });

    $('.contract_end_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'}).on('dp.change', function (e) { $('.contract_end_date').val($(this).val());  });

    $('.notice-period').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'}).on('dp.change', function (e) { $('.notice-period').val($(this).val());  });

    $('.expiry_date_1').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'});

	createDateRangePicker();
   
    $('.vendor_name').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
        onSelect: function(suggestion) { $('#vendor_id').val(suggestion.data);vendorName($(this)); },
        onSearchComplete: function(query, suggestions) {
         if (!suggestions.length) $('#vendor_id').val(0); 
     	}
    });

    $('.vendor_id').on('change',function(){
    	vendorName = $(this).val();
    	$('.vendor_id [value='+vendorName+']').attr('selected', 'true');
    });

    $('#contract_title').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('contracts/getContractsTitle'); ?>',
        onSelect: function(suggestion) { $('#contractId').val(suggestion.data); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#contractId').val(0); }
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field",
        "onCreate": function() { createDateRangePicker(); }
    });

});

function loadContractFile(file_idx, file_name)
{
	$('#contract_document_model').modal('show');
	$("#content").attr("src", "<?php echo Yii::app()->baseUrl ?>/contracts/loadFile/id/"+file_idx+"/file/"+file_name);

}

function deleteContractFile(file_idx, file_name)
{
	$('#delete_contract_file_link_' + file_idx).confirmation({
	  title: "Are you sure you want to delete the attached file?",
	  singleton: true,
	  placement: 'right',
	  popout: true,
	  onConfirm: function() {
	  	$('#existing_file_id_' + file_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('contracts/deleteFile/'); ?>",
            data: { contract_id: $('#contract_id').val(), file_name: file_name }
        });
	  },
	  onCancel: function() {  }
	});
}

function loadCurrencyRate()
{
	var currency_id = $('#currency_id').val();
	$.ajax({
		type: "POST", data: { currency_id: currency_id }, dataType: "json",
		url: BICES.Options.baseurl + '/orders/getRates',
		success: function(options) {
			$('#currency_rate').val(options);
		},
		error: function() { $('#currency_rate').html(1); }
	});
}

function loadSubcategories(input_subcategory_id)
{
	var category_id = $('#contract_category_id').val();
	
	if (category_id == 0)
		$('#contract_subcategory_id').html('<option value="">Select Contract Sub Category</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { category_id: category_id,subcategory_id:input_subcategory_id }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="">Select Contract Sub Category</option>';
	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	        	$('#contract_subcategory_id').html(options_html);
	        	if (input_subcategory_id != 0) $('#contract_subcategory_id').val(input_subcategory_id); 
	        },
	        error: function() { $('#contract_subcategory_id').html('<option value="">Select Contract Sub Category</option>'); }
	    });
	}
}
  loadDepartmentsForSingleLocation(0);
function loadDepartmentsForSingleLocation(department_id)
{
	var location_id = $('#location_id').val();
	var single = 1;
  if (location_id == 0)
      $('#department_id').html('');
  else 
	{
		$.ajax({
			type: "POST", data: { location_id: location_id }, dataType: "json",
			url: BICES.Options.baseurl + '/locations/getDepartments',
			success: function(options) {
          var options_html = '';
          for (var i = 0; i < options.length; i++) {
            options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
          }
          $('#department_id').html(options_html);

          if (contract_departmentIDs.length > 0) $('#department_id').val(contract_departmentIDs);
        },
			error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
		});
	}
}

function addDocument()
{
	var total_documents = $('#total_documents').val();
	total_documents = parseInt(total_documents);
	total_documents = total_documents + 1;
	$('#total_documents').val(total_documents);

	var new_document_html = $('#new_document_code').html();
	new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
	$('#total_documents').before(new_document_html);
}

function deleteDocument(docidx)
{
	var display_document_count = 0;
	$("div[id^='document_area']").each(function () {
		if ($(this).is(':visible')) display_document_count += 1;
	});

	if (display_document_count <= 1) alert("You must have at least one document field in the contract form");
	else
	{
		if (confirm("Are you sure you want to delete this document?"))
		{
			$('#delete_document_flag_' + docidx).val(1);
			$('#document_area_' + docidx).hide();
			$('#end_document_area_' + docidx).hide();
		}
	}
}
$(".spinnerContainer").hide();
function uploadDocument(uploadBtnIDX,e)
{  
  e.preventDefault();
  var uploadedObj = $('#order_file_'+uploadBtnIDX);
  var uploadedObjDesc = $('#file_desc_'+uploadBtnIDX); 
  var uploadedDocType = $('#document_type_'+uploadBtnIDX);
  var uploadedDocStatus = $('#document_status_'+uploadBtnIDX);
  var uploadedDocExpiryDate = $('#expiry_date_'+uploadBtnIDX);

  var file_data = $('#order_file_'+uploadBtnIDX).prop('files')[0];
  var field_data = $('#file_desc_'+uploadBtnIDX).val();
  var field_type = $('#document_type_'+uploadBtnIDX).val();
  if($('#document_status_'+uploadBtnIDX).is(':checked')){
    var field_status = 'Approved';
  }else{
    var field_status = 'Pending';
  }
  var expiry_date = $('#expiry_date_'+uploadBtnIDX).val();

  if(typeof file_data === "undefined" || file_data==""){
    $.alert({title: 'Required!',content: 'Document is required',});
   }else if(field_data==""){
    $.alert({title: 'Required!',content: 'Document Title is required',});
   }else if(field_type==""){
    $.alert({title: 'Required!',content: 'Document Type is required',});
    }else if(expiry_date==""){
    $.alert({title: 'Required!',content: 'Expiry Date is required',});
   }else{

  var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('file_description', field_data);
    form_data.append('document_type',field_type);
    form_data.append('contract_id',$('#contract_id').val());
    form_data.append('document_status',field_status);
    form_data.append('expiry_date',expiry_date);                             
    $.ajax({
        url: BICES.Options.baseurl + '/contracts/uploadDocument', // point to server-side PHP script 
        dataType: 'json',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
         xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if(evt.lengthComputable) {
                var percentComplete = Math.round((evt.loaded / evt.total) * 100);
                $("#progressBar").width(percentComplete + "%");
                $("#progressBar").text(percentComplete + "%");  // Set the percentage text
              }
            }, false);
            return xhr;
          },
          beforeSend: function() {
            $('.spinnerContainer').show(); 
            $("#progressBar").width("0%");
            $("#progressBar").text("0%");  // Initialize with 0%
            $("#btn_upload_1").prop('disabled',true);
          },
          success: function(response){
            window.location.assign(response.url); 
            uploadedObj.val(null);
            uploadedObjDesc.val(null);
            uploadedDocType.val(null);
            uploadedDocStatus.prop('checked', false);
            uploadedDocExpiryDate.val(null);
          },
          complete:function() {
            $(".spinnerContainer").hide();
            $("#btn_upload_1").prop('disabled',false);
          }
     });
    }
}

function approveDocument(docIDs,e)
{
  e.preventDefault();
   $.confirm({
    title: false,
    content: '<span style="font-size:13px">Are you sure you want to approve the document?</span>',
    buttons: {
      Yes: {
        text: "Yes",
        btnClass: 'btn-blue',
        action: function(){
          $.ajax({
              type: 'POST',
              dataType: 'json', // what to expect back from the PHP script, if anything
              url: BICES.Options.baseurl + '/contracts/approveDocument', // point to server-side PHP script 
              data: {document_ids:docIDs,contract_id:$('#contract_id').val()},                         
              success: function(response){
                window.location.assign(response.url);         
              }
          });
        }
      },
    No: {
        text: "No",
        btnClass: 'btn-red',
        action: function(){}
        },          
    }});
}

function documentArchive(documentType,e,docID)
{     //docID.hide();
  e.preventDefault();
   $.confirm({
    title: false,
    content: '<span style="font-size:13px">Are you sure you want to archive the document?</span>',
    buttons: {
      Yes: {
        text: "Yes",
        btnClass: 'btn-blue',
        action: function(){
          $.ajax({
            type: "POST",
            dataType: 'json',
            url: "<?php echo AppUrl::bicesUrl('contracts/documentArchive'); ?>",
            data: { document_ids:docID,contract_id:$('#contract_id').val() },
            success: function(response){
              window.location.assign(response.url);
            }
          });

        }
      },
  No: {
      text: "No",
      btnClass: 'btn-red',
      action: function(){
              
      }
      },          
  }});
}

function documentUnArchive(docID,e)
{   
  e.preventDefault();
   $.confirm({
            title: false,
            content: '<span style="font-size:13px">Are you sure you want to unarchive this document?</span>',
            buttons: {
                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                $.ajax({
                                 type: "POST",
                                 dataType: 'json',
                                 url: "<?php echo AppUrl::bicesUrl('contracts/documentUnArchive'); ?>",
                                 data: { document_ids:docID,contract_id:$('#contract_id').val() },
                                 success: function(response){
                                   window.location.assign(response.url);
                                 }
                                });
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },          
            }
    });
}

function deleteDocumentNew(documentID)
{
  var docID = documentID;
   $.confirm({
            title: false,
            content: '<span style="font-size:13px">Are you sure you want to delete the document?</span>',
            buttons: {
                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo AppUrl::bicesUrl('contracts/deleteDocument/'); ?>",
                                    data: { documentID: docID },
                                    success: function(uploaded_response){
                                      $("#document_list_cont").html(uploaded_response);

                                    }
                                });
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },          
            }});
}

function deleteDocumentPending(requestID)
{
  var requestID = requestID;
  var contract_id = $('#contract_id').val();
    $.confirm({
      title: false,
      content: '<spam style="font-size:13px">Are you sure you want to delete this  Requested Refreshed Contract Document?</span>',
      buttons: {
                Yes: {
                      text: "Yes",
                      btnClass: 'btn-blue',
                      action: function(){
                          $.ajax({
                              type: "POST",
                              url: "<?php echo AppUrl::bicesUrl('contracts/deleteDocumentPending'); ?>",
                              data: { request_id: requestID,contract_id:contract_id },
                              success: function(uploaded_response){
                                loadingRequestDocument(contract_id);
                              }
                          });
                      }
                    },
                No: {
                      text: "No",
                      btnClass: 'btn-red',
                      action: function(){
                              
                      }
                    },
      }});
}

function deleteDocumentOther(documentID)
{
  var docID = documentID;
  $('#delete_document_' + documentID).confirmation({
    title: "Are you sure you want to delete the document?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('contracts/deleteDocumentOther/'); ?>",
            data: { documentID: docID },
            success: function(uploaded_response){
              $("#document_list_cont").html(uploaded_response);
            }
        });
    },
    onCancel: function() {  }
  });
  
}

var today = new Date(); 
function editSingleDocument(formID,modalID)
{   
  var modalID = modalID;
  var formID = formID;
  var expiry_date = $("#expiry_date_"+formID).val();
  var contract_id = $("#contract_id_"+formID).val();
  var document_id = $("#document_id_"+formID).val();
  $.ajax({
    url: BICES.Options.baseurl + '/contracts/editDocument', // point to server-side PHP script 
    dataType: 'json',  // what to expect back from the PHP script, if anything
    data: {contract_id:contract_id,document_id:document_id,expiry_date:expiry_date},//$('#'+formID).serialize(),                         
    type: 'post',
    success: function(response){
      window.location.assign(response.url);         
    }
  });
}
</script>
<style type="text/css">
.h4, h4 {font-size: 14px;}
.modal{ overflow-y: auto !important;}
.modal-open
{
  overflow:auto !important;
  overflow-x:hidden !important;
  padding-right: 0 !important;
}
.title-text { color: #555 !important }
</style>