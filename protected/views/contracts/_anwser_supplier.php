
<div class="modal fade" id="anwser_supplier_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width: 70%;"><div class="modal-content" style="width: 90%; margin:auto; word-wrap: break-word; padding: 0 10px;"> 

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Supplier Questionnaire Answers</h4>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 valid">
    
    <table id="anwser_supplier_table" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
        <tr>
          <th>Scoring Criteria</th>
          <th>Question</th>
          <th>Supplier</th>
          <th>Answer</th>
        </tr>
      </thead>
       <tbody>
        <?php 
          foreach ($quoteAnswerVendor as $value) 
          {?>
            <tr>
              <td  style="word-wrap: break-word;word-break: break-all;"><?php echo $value['scoring_title'];?></td>
              <td style="word-wrap: break-word;word-break: break-all;"><?php echo $value['question'];?></td>
              <td><?php echo $value['vendor_name'];?></td>
              <td style="word-wrap: break-word;word-break: break-all;"><?php echo $value['qans'];?></td>
            </tr>
        <?php } ?>
      </tbody>
  </table>
</div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div></div></div></div>

  <script type="text/javascript">
     $('#anwser_supplier_table').dataTable({
       "pageLength": 50,
        "columnDefs": [ 
          { "targets": 0, "width": "6%", "orderable": false },        
        ],
        "order": []
    });

  </script>