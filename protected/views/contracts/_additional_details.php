<?php $contract_id = $contract['contract_id'];?>
<!-- <div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<label class="control-label">Project</label>
		<select name="project_id" id="project_id" class="form-control">
			<option value="">Select Project</option>
			<?php foreach ($projects as $project) { ?>
				<option value="<?php echo $project['project_id']; ?>"
					<?php if (isset($contract['project_id']) && $project['project_id'] == $contract['project_id']) echo ' selected="SELECTED" '; ?>>
					<?php echo $project['project_name']; ?>
				</option>
			<?php } ?>
		</select>
	</div>
</div> -->
<div class="clearfix"></div>
<div class="x_title" style="border-bottom:none"><h4 class="heading">Notifications</h4>
        <h4 class="subheading"><br />Create custom notifications for this contract which are received directly in your inbox.<br /></h4></div>
<div class="clearfix"></div><br />
<form id="contract_form_financeial" class="form-horizontal form-label-left input_mask"
		enctype="multipart/form-data" method="post"
		action="<?php echo AppUrl::bicesUrl('contracts/edit'); ?>">

<input type="hidden" name="contractId" class="notranslate" id="contractId" value="<?php if (isset($contract_id) && !empty($contract_id)) echo $contract_id; else echo '0'; ?>" />

<input type="hidden" name="contract_id" class="notranslate" id="contract_id" value="<?php if (isset($contract_id)) echo $contract_id; ?>" /> 
<input type="hidden" name="form_submitted" class="notranslate" id="form_submitted" value="1" />
<input type="hidden" name="form_notificatioin" class="notranslate" id="form_notificatioin" value="1" />

<?php $note_count = 0; foreach ($notifications as $notification) { $note_count += 1; ?>
	
	<div class="form-group">
	<div class="col-md-4 col-sm-4 col-xs-6">
		<input type="text" name="notification_update[<?php echo $notification['id']?>]" class="form-control notranslate"
			value="<?php echo $notification['note']; ?>"
			placeholder="Notification description" />
	</div>
	<div class="col-md-2 col-sm-2 col-xs-6">
		<input type="text" name="notification_date_update[<?php echo $notification['id']?>]"
			class="form-control note_date notranslate"
			value="<?php echo date(FunctionManager::dateFormat(), strtotime($notification['date'])); ?>"
			placeholder="Date To Notify" />
	</div>

	<div class="col-md-2">
		<a onclick="$(this).parent().parent().remove(); return false;"
			class="btn btn-link" style="background-color: #de6154; color: #fff;"> <span><i class="fa fa-trash hidden-xs" style=""></i></span>
		</a>
	</div>
</div>
	
<?php } ?>
<div>
<div id="additional-field-model">
	<div class="dinamic-fields">
	<div class="form-group">
		<div class="col-md-4 col-sm-4 col-xs-6">
			<input type="text" name="notification[]" class="form-control notranslate"
				value="" placeholder="Notification description" />
		</div>
		<div class="col-md-2 col-sm-2 col-xs-6">
			<input type="text" name="notification_date[]"
				class="form-control note_date notranslate" value="<?php echo date(FunctionManager::dateFormat()); ?>"
				placeholder="Date To Notify" />
		</div>

		<div class="col-md-2">
			<a href="javascript:void(0);"
				class="btn btn-danger remove-this-field " > Remove Notification
			</a> <a href="javascript:void(0);"
				class="btn btn-link create-new-field" style=" background-color: #f0ad4e !important; border-color: #f0ad4e !important; color: #fff;"> Add new notification 
			</a>
		</div>
	</div>
	</div></div>
</div>
<br />
 <?php $this->renderPartial('_submit',array('contract_id'=>$contract_id,'contract'=>$contract));?>

</form>