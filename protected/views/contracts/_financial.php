<?php $contract_id = $contract['contract_id'];?>
<div class="clearfix"></div>
<div class="form-group">
	<div class="x_title" style="border-bottom:none"><h4 class="heading">Financial Information</h4></div>
</div>

<input type="hidden" name="contractId" class="notranslate" id="contractId" value="<?php if (isset($contract_id) && !empty($contract_id)) echo $contract_id; else echo '0'; ?>" />

<input type="hidden" name="contract_id" class="notranslate" id="contract_id" value="<?php if (isset($contract_id)) echo $contract_id; ?>" /> 
<input type="hidden" name="form_submitted" class="notranslate" id="form_submitted" value="1" />
<input type="hidden" name="form_financial" class="notranslate" id="form_financial" value="1" />


<div class="clearfix"></div>
<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12 valid">
		<label class="control-label">Currency</label>
		<select name="currency_id" id="currency_id" class="form-control notranslate" onchange="loadCurrencyRate();">
			<?php foreach ($currency_rate as $rate) { ?>
				<option value="<?php echo $rate['id']; ?>"
					<?php if(isset($contract['currency_id']) && $rate['id'] == $contract['currency_id']) echo ' selected="SELECTED" '; ?>>
					<?php echo $rate['currency']; ?>
				</option>
			<?php } ?>
		</select>
	</div>

	<!-- <div class="col-md-3 col-sm-3 col-xs-6 valid">
		<label class="control-label">Currency Rate At Time Of Contract Creation</label>
		<input readonly type="text" class="form-control" name="currency_rate" id="currency_rate"
			<?php if (isset($contract['currency_rate']) && !empty($contract['currency_rate'])) { echo 'value="' . $contract['currency_rate'] . '"'; } else { echo 'value="1"'; } ?> >

	</div> -->

</div>

<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-12 date-input">
		<label class="control-label">Estimated Total Contract Value</label>
		<input type="text" class="form-control notranslate"
			name="contract_value" id="contract_value"	
			<?php if (isset($contract['contract_value']) && !empty($contract['contract_value'])) echo 'value="' . $contract['contract_value'] . '"'; else echo 'placeholder="Estimated Total Contract Value"'; ?>>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 date-input">
		<label class="control-label">Estimated Annual Contract Value</label>
		<input type="text" class="form-control notranslate"
			   name="contract_budget" id="contract_budget"
			   
			<?php if (isset($contract['contract_budget']) && !empty($contract['contract_budget'])) echo 'value="' . $contract['contract_budget'] . '"'; else echo 'placeholder="Estimated Annual Contract Value"'; ?>>
	</div>
</div>

<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-12 date-input">
		<label class="control-label">Original Contract Value</label>
		<input type="text" class="form-control notranslate"
			name="original_budget" id="original_budget"
			
			<?php if (isset($contract['original_budget']) && !empty($contract['original_budget'])) echo 'value="' . $contract['original_budget'] . '"'; else echo 'placeholder="Original Contract Value"'; ?>>

	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 date-input">
		<label class="control-label">Original Contract Annual Value</label>
		<input type="text" class="form-control notranslate"
			name="original_annual_budget" id="original_annual_budget"
			
			<?php if (isset($contract['original_annual_budget']) && !empty($contract['original_annual_budget'])) echo 'value="' . $contract['original_annual_budget'] . '"'; else echo 'placeholder="Original Contract Annual Value"'; ?>>
	</div>
</div>

<div class="form-group">

	<div class="col-md-3 col-sm-3 col-xs-12 date-input">
		<label class="control-label">PO Number</label>
		<input type="text" class="form-control notranslate"
			name="po_number" id="po_number" 
			<?php if (isset($contract['po_number']) && !empty($contract['po_number'])) echo 'value="' . $contract['po_number'] . '"'; else echo 'placeholder="PO Number"'; ?>>
	</div>

	<div class="col-md-3 col-sm-3 col-xs-12 date-input">
		<label class="control-label">Cost Centre</label>
		<input type="text" class="form-control notranslate"
			name="cost_centre" id="cost_centre" 
			<?php if (isset($contract['cost_centre']) && !empty($contract['cost_centre'])) echo 'value="' . $contract['cost_centre'] . '"'; else echo 'placeholder="Cost Centre"'; ?>>
	</div>
</div> 


<div class="clearfix"></div><br />

<?php $this->renderPartial('_submit',array('contract_id'=>$contract_id,'contract'=>$contract));?>
			