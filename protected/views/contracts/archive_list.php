<?php
$user_currency_symbol = Yii::app()->session['user_currency_symbol'];?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
         <h3>Archived Contracts</h3>
        </div>

        <div class="span6 pull-right">
         <a href="<?php echo AppUrl::bicesUrl('contracts/list'); ?>">
          <button type="button" class="btn btn-info">
           <span class="glyphicon glyphicon-list mr-2" aria-hidden="true"></span> Return To Active Contracts
          </button>
         </a>
        </div>

        <div class="clearfix"> </div>
        <?php if(Yii::app()->user->hasFlash('success')) { 
         	echo Yii::app()->user->getFlash('success');
        } ?>
    </div>
    <div class="clearfix"><br /></div>
	<form class="form-horizontal" id="contract_list_form" name="contract_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('contracts/list'); ?>">
		
			<div class="col-md-2 col-sm-2 col-xs-12 location">
				<div class="form-group">
				<select name="location_id[]" id="location_id" class="form-control select_location_multiple border-select"  searchable="Search here.."  
				onchange="loadDepartmentsArchive(this);" >
					<option value="0">All Locations</option>
					<?php
					$i = 0 ;
					foreach ($locations as $location) {
						if(isset($location_id[$i])){?>
						 <option value="<?php echo $location['location_id']; ?>"
						  <?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
						  <?php echo $location['location_name']; ?>
						 </option>
						<?php } else { ?>
						 <option value="<?php echo $location['location_id']; ?>">
						  <?php echo $location['location_name']; ?>
						 </option>
						<?php }
						$i++;
					} ?>
				</select>
			</div></div>
			<div class="col-md-2 col-sm-2 col-xs-12 department">
				<div class="form-group">
				<select name="department_id[]" id="department_id" class="form-control select_department_multiple border-select"  >
					 <option value="">All Departments</option>
					<?php if(!empty($department_info))
                        foreach($department_info as $dept_value){?>
                        <option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
                    <?php } ?>
                    
				</select> 
			</div></div>
		
			<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="form-group">
			
				<select name="category_id" id="category_id" class="form-control select_category_multiple border-select" onchange="loadSubcategories(0);"  searchable="Search here..">
					<option value="0">All Categories</option>
					<?php foreach ($categories as $category) { ?>
						<option value="<?php echo $category['id']; ?>"
								<?php if (isset($category_id) && $category_id == $category['id']) echo ' selected="SELECTED" '; ?>>
							<?php echo $category['value']; ?>
						</option>
					<?php } ?>
				</select>
			</div></div>
			<div class="col-md-2 col-sm-2 col-xs-12">
				<div class="form-group">
				<select name="subcategory_id" id="subcategory_id" class="form-control select_subcategory_multiple border-select">
					<option value="0">All Subcategories</option>
				</select></div>
			</div>
			
			<div class="col-md-3 col-sm-3 col-xs-12 search-contract text-left" >
				<button class="btn btn-info search-quote" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
				<button class="btn btn-primary " onclick="loadContracts(); return false;" style="margin-right: -7px">Apply filters</button>
			</div>
			
		</form>
		<div class="clearfix"></div><br />
		<div class="clearfix"></div>
    <div class="table-responsive">
    <table id="contract_table" class="table table-striped table-bordered contract-table" style="width: 100%;">
      <thead>
        <tr>
          <th style="width: 6% "> </th>
          <th  style="width: 14%">Title</th>
          <th style="width: 13% ">Category</th>
          <!-- <th>Location</th> -->
          <th class="th-center managedBy" style="width: 11%;">Managed By</th>
          <th style="width: 10%; text-align: center;" >Supplier</th>
          <th class="th-center" style="width: 7%">Currency</th>
          <th class="th-center" style="width: 10%">Estimated Value</th>
          <th class="th-center" style="width: 9%">Start Date</th>
          <th class="th-center" style="width: 11 %">Notice Period Date</th>
          <th class="th-center" style="width: 9%">End Date</th>
		  <!-- <th>Amount in Tool Currency</th>
		  <th>Estimated Total Contract Value</th> -->
        </tr>
      </thead>
      <tbody>
      </tbody>
  </table>
</div>
</div>
<style>
	 
	.btn .caret {
		float: right;
		margin-top: 10px;
	}
 
	.location  .multiselect {
		width: 138%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
		min-width: 135px;
	}
	/*.select2-container .select2-selection--multiple {border-radius: 10px !important;border: 1px solid #4d90fe !important;}*/
	.btn .caret {
		float: right;
		margin-top: 10px;
	}
	.select2-selection__arrow { display: none !important; }
</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>
<!-- <link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"> -->
<!-- <script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script> -->

<script type="text/javascript">


function select2function(className,lableTitle){
  var lableTitle = lableTitle;
  $("."+className).select2({
      placeholder: lableTitle,
     /* allowClear: true*/
    });
}

function loadContracts()
{
	$('#contract_table').DataTable().ajax.reload();
}
function clearFilter() {
  $("option:selected").removeAttr("selected");
  $('.select_location_multiple').trigger("change");
  $('.select_department_multiple').trigger("change");
  $('.select_category_multiple').trigger("change");
  $('.select_subcategory_multiple').trigger("change");
  $('#contract_table').DataTable().ajax.reload();
}


$(document).ready(function(){

  	select2function('select_location_multiple','All Locations');
  	select2function('select_department_multiple','All Departments');
  	select2function('select_category_multiple','All Categories');
  	select2function('select_subcategory_multiple','All Sub Categories');
  	select2function('select_status_multiple','All Status');
  	$('#contract_table').dataTable({
        "columnDefs": [ {
            //"targets": -1,
            "targets": 0,
            
        		"ordering": false,
        		"info":     false
            /*"width": "6%",*/
        } ],
        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
            	for (var i=1; i<=6; i++)
                	$('td', row).eq(i).css('text-decoration', 'line-through');
        },        
        "order": [[ 1,"asc" ]],
		    "pageLength": 50,        
        "processing": true,
        "serverSide": true,
        "ajax": {
        	"url": "<?php echo AppUrl::bicesUrl('contracts/listArchiveAjax'); ?>",
        	"type": "POST",
        	data: function ( input_data ) {
        		input_data.location_id = $('#location_id').val();
        		input_data.department_id = $('#department_id').val();
        		input_data.category_id = $('#category_id').val();
                input_data.subcategory_id = $('#subcategory_id').val();
                //input_data.contract_status = $('#contract_status').val();
  			}    
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          if(aData[1].search("<span class='deactivated_vendor'></span>")>0){
            $(nRow).addClass('deactivated_record');
            /*$(nRow).css("background-color", "#ccc");*/
          }
           for (var i=3; i<=9; i++){
            $('td:eq('+i+')', nRow).addClass('td-center');
        	}
        	 for (var i=3; i==3; i++){
            $('td:eq('+i+')', nRow).addClass('managedBy');
        	}
         }
    });

  // $(".select_location").select2({
  //   placeholder: "Select Location",
  //   allowClear: true,
  //   containerCssClass: "notranslate",
  //   dropdownCssClass: "notranslate"
  // });

  //  $(".select_department").select2({
  //   placeholder: "Select Department",
  //   allowClear: true,
  //   containerCssClass: "notranslate",
  //   dropdownCssClass: "notranslate"
  // });

  //  $(".select_category").select2({
  //   placeholder: "Select Category",
  //   allowClear: true,
  //   containerCssClass: "notranslate",
  //   dropdownCssClass: "notranslate"
  // });

  //  $(".select_sub_category").select2({
  //   placeholder: "Select Category",
  //   allowClear: true,
  //   containerCssClass: "notranslate",
  //   dropdownCssClass: "notranslate"
  // });

  //  $(".select_status").select2({
  //   placeholder: "Select Status",
  //   allowClear: true,
  //   containerCssClass: "notranslate",
  //   dropdownCssClass: "notranslate"
  // });

});

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions2(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Departments',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }

    function getOptions3(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Statuses',
            numberDisplayed: 1,
            maxHeight: 400,
            width: 400,
        }
    }

/*
	$('#location_id').multiselect(getOptions(true));
	$('#department_id').multiselect(getOptions2(true));
	$('#contract_status').multiselect(getOptions3(true));*/

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );



function loadDepartmentsArchive(sel)
{ 
  var location_id = sel.value;
  var single = 1;
    $.ajax({
      type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
      url: BICES.Options.baseurl + '/locations/getDepartments',
      success: function(options) {
        var options_html = '<option value="">Select Department</option>';
        for (var i=0; i<options.length; i++)
          options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
        $('#department_id').html(options_html);
      },
      error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
    });
    //$(".select_department_multiple").select2();
    $(".select_department_multiple").trigger("change");
}

$(document).ready( function() {

	<?php if (isset($category_id) && !empty($category_id)) { ?>

		<?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>
			loadSubcategories(<?php echo $subcategory_id; ?>);
		<?php } else { ?>
			loadSubcategories(0);
		<?php } ?>

	<?php } ?>

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			//loadDepartmentsArchive(<?php //echo $department_id; ?>);
		<?php } else { ?>
			loadDepartmentsArchive(0);
		<?php } ?>

	<?php } ?>
    
});

function loadSubcategories(subcategory_id)
{
	var category_id = $('#category_id').val();
	
 
	    $.ajax({
	        type: "POST", data: { category_id: category_id }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="0">All Subcategories</option>';
	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	        	$('#subcategory_id').html(options_html);
	        	if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id); 
	        },
	        error: function() { $('#subcategory_id').html('<option value="0">All Subcategories</option>'); }
	    });
	$(".select_sub_category").trigger("change");
}
</script>


<style>
	.btn .caret {
		float: right;
		margin-top: 10px;
	}
 
	.location  .multiselect {
		width: 138%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
		min-width: 135px;
	}
	/*.select2-container .select2-selection--multiple {border-radius: 10px !important;border: 1px solid #4d90fe !important;}*/
	.btn .caret {
		float: right;
		margin-top: 10px;
	}
	td{font-weight: normal;}
	.select2-selection__arrow { display: none !important; }
	.location .multiselect {width: 138%;}
.department .multiselect {width: 112%; display: none;}
</style>