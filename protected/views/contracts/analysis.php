<?php
	$status_labels = "";
	$status_data = "";
	$max_data = 0;
	foreach ($contract_statuses as $total_contracts)
	{
		if ($status_labels == "") $status_labels = "'" . $total_contracts['status'] . "'";
		else $status_labels = $status_labels . ", '" . $total_contracts['status'] . "'";
		
		if ($status_data == "") $status_data = $total_contracts['total'];
		else $status_data = $status_data . "," . $total_contracts['total'];
		
		if ($max_data < $total_contracts['total']) $max_data = $total_contracts['total'];
	}
	
	if ($max_data == 0) $max_data = 1;
	else if ($max_data == 1) $max_data = 2;
	else if ($max_data < 5) $max_data = 5;
	else if ($max_data < 10) $max_data = 10;
	else if ($max_data < 20) $max_data = 20;
	else if ($max_data < 50) $max_data = 50;
	else if ($max_data < 100) $max_data = 100;
	else if ($max_data < 200) $max_data = 200;
	else if ($max_data < 500) $max_data = 500;
	else if ($max_data < 1000) $max_data = 1000;

	$category_wise_contract_data = $contracts_by_category;
	$contracts_by_category = array();
	foreach ($category_wise_contract_data as $category_wise_contract_row)
	{
		$category_wise_contract_row['full_category_name'] = $category_wise_contract_row['category_name'];
		$current_category_name = $category_wise_contract_row['category_name']; 
		if (strlen($current_category_name) >= 7) 
		{
			$abbr_category_name = "";
			$current_category_name = ucwords($current_category_name);
			foreach (explode(" ", $current_category_name) as $current_category_word)
			{
				if ($abbr_category_name == "") $abbr_category_name = substr($current_category_word, 0, 1);
				else $abbr_category_name = $abbr_category_name . " " . substr($current_category_word, 0, 1);
			}
			$current_category_name = $abbr_category_name;
		}
		$category_wise_contract_row['category_name'] = $current_category_name;
		$contracts_by_category[] = $category_wise_contract_row;
	}

?>
          <div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Contract Analysis</h3>
        </div>
        <div class="clearfix"> </div>
    </div>


          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Active Contracts Value</span>
              <div class="count"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['contract']['active_total'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-list"></i> Active Contracts Count</span>
              <div class="count"><?php echo number_format($metrics['contract']['active_count'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="color: red !important;">
              <span class="count_top"><i class="fa fa-money"></i> Expire in 30 Days</span>
              <div class="count"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['contract']['expire_current_total'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="color: red !important;">
              <span class="count_top"><i class="fa fa-list"></i>  Expire in 30 Days</span>
              <div class="count"><?php echo number_format($metrics['contract']['expire_current_count'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="color: orange !important;">
              <span class="count_top"><i class="fa fa-money"></i> Expire in 3 Months</span>
              <div class="count"><?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrics['contract']['expire_later_total'], 0); ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="color: orange !important;">
              <span class="count_top"><i class="fa fa-list"></i>  Expire in 3 Months</span>
              <div class="count"><?php echo number_format($metrics['contract']['expire_later_count'], 0); ?></div>
            </div>
          </div>


		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-24">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Contracts By Category</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('contracts_by_category');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="contracts_by_category" width="940px"></canvas>
			              	</div>
			           	</div>
			        	</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>



				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Contracts</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('top_contracts');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="top_contracts" height="170px"></canvas>
			              	</div>
			           	</div>
			        	</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Contracts By Status</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('contracts_by_status');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
			                  <table class="" style="width:100%">
			                    <tbody>
			                    <tr>
			                      <td style="vertical-align: top;">
			                      	<canvas id="contracts_by_status" height="230px"></canvas>
			                      </td>
			                      <td style="vertical-align: top;">
					            		<table class="tile_info">
						           			<?php
						           				$idx = 0;
						           				foreach (explode(",", $status_labels) as $a_status)
												{
													$a_status = str_replace("'", "", $a_status);
													$a_status = rtrim(ltrim($a_status));
													$idx += 1;
																	
													switch ($idx)
													{
														case 1  : $color = "#F7464A"; break;
														case 2  : $color = "#46BFBD"; break;
														case 3  : $color = "#FDB45C"; break;
														case 4  : $color = "#949FB1"; break;
														case 5  : $color = "#4D5360"; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
						           			?>
						
						                         		<tr>
						                            		<td>
						                              			<i class="fa fa-square" style="color: <?php echo $color; ?> !important;"></i>
						                              			<?php 
						                              				echo $a_status;
																	if (isset($contract_statuses[$a_status]))
																		echo ' - (' .  $contract_statuses[$a_status]['total'] . ')';
						                              			?>
						                            		</td>
						                          		</tr>
							                        
						                    <?php 
												}
									        ?>					            			
										</table>
								  </td>
								  </tr>
								</tbody>
								</table>                        		
			              	</div>
			           	</div>
			        	</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Contracts By Location</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('contracts_by_location');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="contracts_by_location" width="460px" height="230px"></canvas>
			              	</div>
			           	</div>
			        	</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Expiring Contracts</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="exportChart('expiring_contracts');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
				           		<canvas id="expiring_contracts" width="460px" height="230px"></canvas>
			              	</div>
			           	</div>
			        	</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

</div>

<script type="text/javascript">
$(document).ready(function() {

	var top_contracts_chart_config = {
		type: 'pie',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ 
				<?php $i = 1; foreach ($top_contracts as $a_contract) { ?>
					"<?php echo addslashes($a_contract['dim_1']); ?>"<?php if ($i != count($top_contracts)) echo ','; ?>
				<?php $i += 1; } ?> 
			],
			datasets: [

					{			
						backgroundColor: [
							<?php
								$i = 1; 	
								foreach ($top_contracts as $a_contract)
								{
									if ($i == 1) $backgroundColor = "rgba(220,220,220,0.5)";
									else if ($i == 2) $backgroundColor = "rgba(151,220,187,0.5)";
									else if ($i == 3) $backgroundColor = "rgba(187,220,151,0.5)";
									else if ($i == 4) $backgroundColor = "rgba(220,151,187,0.5)";
									else if ($i == 5) $backgroundColor = "rgba(220,187,151,0.5)";
									else if ($i == 6) $backgroundColor = "rgba(187,151,220,0.5)";
									else if ($i == 7) $backgroundColor = "rgba(120,151,187,0.5)";
									else if ($i == 8) $backgroundColor = "rgba(151,120,187,0.5)";
									else if ($i == 9) $backgroundColor = "rgba(187,120,151,0.5)";
									else if ($i == 10) $backgroundColor = "rgba(151,187,120,0.5)";
									else $backgroundColor = "rgba(151,187,205,0.5)";
									
									echo "'" . $backgroundColor . "'";
									if ($i != count($top_contracts)) echo ',';
									$i += 1;
								}
							?>
						], 
						
						data: [
							<?php 
								$i = 1; 	
								foreach ($top_contracts as $a_contract)
								{
									echo round($a_contract['total'], 2);
									if ($i != count($top_contracts)) echo ',';
									$i += 1;
								}
							?>
						
						]
					}
				]
		},
		options: { 
			responsive: true,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						var prefix = data.labels[tooltipItem.index];
					    if (parseInt(value) >= 1000){
					    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					    } else {
					    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
					    }
					}						
				}
			}
		}
	};

    var top_contracts_chart = new Chart($('#top_contracts'), top_contracts_chart_config);


    var contracts_polar_config = {
		type: 'polarArea',
        data: {
            datasets: [
            	{
                	data: [ <?php echo $status_data; ?> ],
                	backgroundColor: [ "#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360" ],
                	label: 'Contracts By Status' // for legend
            	}
            ],
            labels: [ <?php echo $status_labels; ?> ]
        },
        options: {
            responsive: true,
			legend: {
                position: 'right'
            },
            scale: {
              ticks: {
                beginAtZero: true,
                max: <?php echo $max_data; ?>
              },
              reverse: false
            }            
        }
    };
    
    var contracts_polar = new Chart($('#contracts_by_status'), contracts_polar_config);


	var location_contracts_chart_config = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ 
				<?php $i = 1; foreach ($contracts_by_location as $a_contract) { ?>
					"<?php echo addslashes($a_contract['location_name']); ?>"<?php if ($i != count($contracts_by_location)) echo ','; ?>
				<?php $i += 1; } ?> 
			],
			datasets: [

					{			
						backgroundColor: [
							<?php
								$i = 1; 	
								foreach ($contracts_by_location as $a_contract)
								{
									if ($i == 1) $backgroundColor = "rgba(220,220,220,0.5)";
									else if ($i == 2) $backgroundColor = "rgba(151,220,187,0.5)";
									else if ($i == 3) $backgroundColor = "rgba(187,220,151,0.5)";
									else if ($i == 4) $backgroundColor = "rgba(220,151,187,0.5)";
									else if ($i == 5) $backgroundColor = "rgba(220,187,151,0.5)";
									else if ($i == 6) $backgroundColor = "rgba(187,151,220,0.5)";
									else if ($i == 7) $backgroundColor = "rgba(120,151,187,0.5)";
									else if ($i == 8) $backgroundColor = "rgba(151,120,187,0.5)";
									else if ($i == 9) $backgroundColor = "rgba(187,120,151,0.5)";
									else if ($i == 10) $backgroundColor = "rgba(151,187,120,0.5)";
									else $backgroundColor = "rgba(151,187,205,0.5)";
									
									echo "'" . $backgroundColor . "'";
									if ($i != count($contracts_by_location)) echo ',';
									$i += 1;
								}
							?>
						], 
						
						data: [
							<?php 
								$i = 1; 	
								foreach ($contracts_by_location as $a_contract)
								{
									echo round($a_contract['total'], 2);
									if ($i != count($contracts_by_location)) echo ',';
									$i += 1;
								}
							?>
						
						]
					}
				]
		},
		options: { 
			responsive: true, 
            scales: { yAxes: [ { ticks: { min: 0, stepSize: 1 } } ] } 
		}
	};

    var location_contracts_chart = new Chart($('#contracts_by_location'), location_contracts_chart_config);


	var category_contracts_chart_config = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ 
				<?php $i = 1; foreach ($contracts_by_category as $a_contract) { ?>
					"<?php echo addslashes($a_contract['category_name']); ?>"<?php if ($i != count($contracts_by_category)) echo ','; ?>
				<?php $i += 1; } ?> 
			],
			full_labels: [ 
				<?php $i = 1; foreach ($contracts_by_category as $a_contract) { ?>
					"<?php echo addslashes($a_contract['full_category_name']); ?>"<?php if ($i != count($contracts_by_category)) echo ','; ?>
				<?php $i += 1; } ?> 
			],
			datasets: [

					{			
						backgroundColor: [
							<?php
								$i = 1; 	
								foreach ($contracts_by_category as $a_contract)
								{
									if ($i == 1) $backgroundColor = "rgba(220,220,220,0.5)";
									else if ($i == 2) $backgroundColor = "rgba(151,220,187,0.5)";
									else if ($i == 3) $backgroundColor = "rgba(187,220,151,0.5)";
									else if ($i == 4) $backgroundColor = "rgba(220,151,187,0.5)";
									else if ($i == 5) $backgroundColor = "rgba(220,187,151,0.5)";
									else if ($i == 6) $backgroundColor = "rgba(187,151,220,0.5)";
									else if ($i == 7) $backgroundColor = "rgba(120,151,187,0.5)";
									else if ($i == 8) $backgroundColor = "rgba(151,120,187,0.5)";
									else if ($i == 9) $backgroundColor = "rgba(187,120,151,0.5)";
									else if ($i == 10) $backgroundColor = "rgba(151,187,120,0.5)";
									else $backgroundColor = "rgba(151,187,205,0.5)";
									
									echo "'" . $backgroundColor . "'";
									if ($i != count($contracts_by_category)) echo ',';
									$i += 1;
								}
							?>
						], 
						
						data: [
							<?php 
								$i = 1; 	
								foreach ($contracts_by_category as $a_contract)
								{
									echo round($a_contract['total'], 2);
									if ($i != count($contracts_by_category)) echo ',';
									$i += 1;
								}
							?>
						
						]
					}
				]
		},
		options: { 
			responsive: true, 
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						var category_name = data.full_labels[tooltipItem.index];						
					    if (category_name.length >= 7) return ['(' + category_name + ')', value];
					    else return value;
					}						
				}
			},			
            scales: { yAxes: [ { ticks: { min: 0, stepSize: 1 } } ] } 
		}
	};

    var category_contracts_chart = new Chart($('#contracts_by_category'), category_contracts_chart_config);



	var expiring_contract_data = {
		labels: [
			<?php $c = 1; foreach ($expiring_contracts as $expiring_contract_row) { ?>
				
				'<?php echo addslashes($expiring_contract_row['month_name']); ?>' <?php if ($c != count($expiring_contracts)) echo ','; ?> 
				
			<?php $c += 1; } ?>
		],
		
		datasets: [
		
			{
                type: 'line',
                label: 'Contract Count',
                borderColor: 'green',
                borderWidth: 2,
                fill: false,
                yAxisID: 'E1',
                data: [

						<?php $c = 1; foreach ($expiring_contracts as $expiring_contract_row) { ?>
							
							<?php echo $expiring_contract_row['total']; ?> 
							<?php if ($c != count($expiring_contracts)) echo ','; ?> 
							
						<?php $c += 1; } ?>

                ]				
			},
			
			{
                type: 'bar',
                label: 'Contract Value',
                backgroundColor: 'lightblue',
                fill: false,
                yAxisID: 'E2',
                data: [
                
						<?php $c = 1; foreach ($expiring_contracts as $expiring_contract_row) { ?>
							
							<?php echo round($expiring_contract_row['value'], 2); ?> 
							<?php if ($c != count($expiring_contracts)) echo ','; ?> 
							
						<?php $c += 1; } ?>
						
                ]				
			}
		
		]
	};	
	
    var expiring_contract_chart = 
    	new Chart($('#expiring_contracts'), 
    			{
                	type: 'bar',
                	data: expiring_contract_data,
                	options: { 
                		scaleUse2Y: true, 
                		responsive: true, 
						tooltips: {
							callbacks: {
								label: function(tooltipItem, data) {
									var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									if (tooltipItem.datasetIndex == 0) return value;
									else
									{
									    if (parseInt(value) >= 1000){
									    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									    } else {
									    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
									    }
									}
								}						
							}
						},
                		scales: { 
                			xAxes: [{ ticks: { maxRotation: 90, minRotation: 90 } }],
                			yAxes: [
                						{ 
                							id: 'E2', type: 'linear', position: 'left', display: true, 
											ticks: { 
		            							min: 0, 
												callback: function(value, index, values) {
	              									if (parseInt(value) >= 1000) {
	                									return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              									} else {
	                									return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              									}	            	
		            							}
		            						}                									 
                						}, 
                						{ id: 'E1', type: 'linear', position: 'right', display: true, ticks: { min: 0, stepSize: 1 }, gridLines: { drawOnChartArea: false } }
                				] 
                		} 
                	}
            	}
        );
	
	
});	

function exportChart(canvas_id)
{
	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id;
}

</script>
