<div class="form-group">
	<div class="x_title" style="border-bottom:none"><h4 class="heading">Timelines</h4></div>
</div>
<input type="hidden" name="contractId" class="notranslate" id="contractId" value="<?php if (isset($contract_id) && !empty($contract_id)) echo $contract_id; else echo '0'; ?>" />

<input type="hidden" name="contract_id" class="notranslate" id="contract_id" value="<?php if (isset($contract_id)) echo $contract_id; ?>" /> 

<input type="hidden" name="form_submitted" class="notranslate" id="form_submitted" value="1" />
<input type="hidden" name="form_timeline" class="notranslate" id="form_timeline" value="1" />
<div class="clearfix"></div>

<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Contract Start Date</label>
		<input type="text" class="form-control contract_start_date notranslate"
			name="contract_start_date" id="contract_start_date"
			<?php if (isset($contract['contract_start_date']) && !empty($contract['contract_start_date']) && $contract['contract_start_date'] !="0000-00-00") echo 'value="' . date(FunctionManager::dateFormat(), strtotime($contract['contract_start_date'])) . '"'; else echo 'placeholder="Contract Start Date"'; ?>>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
	<label class="control-label">Contract End Date <span style="color: #a94442;">*</span></label>
		<input type="text" class="form-control contract_end_date notranslate"
			name="contract_end_date" id="contract_end_date"
			<?php if (isset($contract['contract_end_date']) && !empty($contract['contract_end_date']) && $contract['contract_end_date'] !="0000-00-00") echo 'value="' . date(FunctionManager::dateFormat(), strtotime($contract['contract_end_date'])) . '"'; else echo 'placeholder="Contract End Date"'; ?> autocomplete="off" required>
	</div>
	<!-- <div class="col-md-2 col-sm-2 col-xs-4 date-input">
	<label class="control-label">Original End Date</label>
		<input type="text" class="form-control has-feedback-left original_end_date"
			name="original_end_date" id="original_end_date"
			<?php if (isset($contract['original_end_date']) && $contract['original_end_date'] !="0000-00-00") echo 'value="' . date(FunctionManager::dateFormat(), strtotime($contract['original_end_date'])) . '"'; else echo 'placeholder="Original End Date"'; ?> autocomplete="off">
		<span class="fa fa-calendar-o form-control-feedback left"
			aria-hidden="true"></span>
	</div> -->
</div>

<div class="form-group">

	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Notice Period Date</label>
		<input type="text" class="form-control notice-period notranslate" autocomplete="off" name="break_clause"
			id="break_clause"
			<?php if (isset($contract['break_clause']) && $contract['break_clause'] !="0000-00-00 00:00:00") echo 'value="' . date(FunctionManager::dateFormat(), strtotime($contract['break_clause'])) . '"'; else echo 'placeholder="Notice Period Date"'; ?>>
	</div>

	<div class="col-md-3 col-sm-3 col-xs-6">
		<label class="control-label">Termination Terms</label>
		<input type="text" class="form-control termination_terms notranslate" name="termination_terms"
			id="termination_terms"
			<?php if (isset($contract['termination_terms']) && !empty($contract['termination_terms'])) echo 'value="' . $contract['termination_terms'] . '"'; else echo 'placeholder="Termination Terms"'; ?>>
	</div>
</div>

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<label class="control-label">Termination Comments</label>
		<textarea class="form-control notranslate" name="termination_comments"
			id="termination_comments" rows="4"
			<?php if (!isset($contract['termination_comments']) || empty($contract['termination_comments'])) echo 'placeholder="Termination Comments"'; ?>><?php if (isset($contract['termination_comments']) && !empty($contract['termination_comments'])) echo $contract['termination_comments']; ?></textarea>
	</div>
</div>

<div class="form-group">
	<div class="label-adjust"><label class="control-label">Extension Options (in Months)</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control notranslate"
			name="extension_options" id="extension_options"
			<?php if (isset($contract['extension_options']) && !empty($contract['extension_options'])) echo 'value="' . $contract['extension_options'] . '"'; else echo 'placeholder="Extension Options (in Months)"'; ?>>
	</div>
</div>

<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<label class="control-label">Extension Comments</label>
		<textarea class="form-control notranslate" name="extension_comments"
			id="extension_comments" rows="4"
			<?php if (!isset($contract['extension_comments']) || empty($contract['extension_comments'])) echo 'placeholder="Extension Comments"'; ?>><?php if (isset($contract['extension_comments']) && !empty($contract['extension_comments'])) echo $contract['extension_comments']; ?></textarea>
	</div>
</div>

 <div class="clearfix"></div><br />
<?php $this->renderPartial('_submit',array('contract_id'=>$contract_id,'contract'=>$contract));?>