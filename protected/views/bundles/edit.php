<div class="right_col" role="main">

	<div class="row-fluid tile_count">
		<div class="span6 pull-left">
			<h3>
                <?php
                if (isset($bundle_id) && $bundle_id) {
                    echo 'Edit Bundle';
                    if (isset($bundle) && is_array($bundle) && isset($bundle['bundle_name']) && ! empty($bundle['bundle_name']))
                        echo ' - ' . $bundle['bundle_name'];
                } else
                    echo 'Add Bundle';
                ?>
            </h3>
		</div>

		<div class="span6 pull-right">
			<a href="<?php echo AppUrl::bicesUrl('bundles/list'); ?>">
				<button type="button" class="btn btn-default">
					<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
					Bundle List
				</button>
			</a>
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="row tile_count">
		<form id="bundle_form"
			class="form-horizontal form-label-left input_mask" method="post"
			action="<?php echo AppUrl::bicesUrl('bundles/edit'); ?>">
			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-8 date-input valid">
					<label class="control-label">Bundle Name <span style="color: #a94442;">*</span></label>
					<input required type="text" class="form-control has-feedback-left"
						name="bundle_name" id="bundle_name"
						<?php if (isset($bundle['bundle_name']) && !empty($bundle['bundle_name'])) echo 'value="' . $bundle['bundle_name'] . '"'; else echo 'placeholder="Bundle Name"'; ?>>
					<span class="fa fa-institution form-control-feedback left"
						aria-hidden="true"></span>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-4 date-input valid">
					<label class="control-label">Price <span style="color: #a94442;">*</span></label>
					<input required type="text"
						class="form-control has-feedback-left text-right" name="price"
						id="price"
						<?php if (isset($bundle['price']) && !empty($bundle['price'])) echo 'value="' . $bundle['price'] . '"'; else echo 'placeholder="Price"'; ?>>
					<span class="fa fa-money form-control-feedback left"
						aria-hidden="true"></span>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<label class="control-label">Description</label>
					<textarea class="form-control" name="description" id="description"
						rows="4"
						<?php if (!isset($bundle['description']) || empty($bundle['description'])) echo ' placeholder="Bundle Description" '; ?>><?php if (isset($bundle['description']) && !empty($bundle['description'])) echo $bundle['description']; ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-8">
					<h4>Products in the bundle</h4>
				</div>

		  <?php if (isset($bundle_id) && $bundle_id && $bundle['active'] == 0) { ?>
	          <div class="col-md-2 col-sm-2 col-xs-4">
				  <label class="control-label">Status</label>
					<select name="active" id="active" class="form-control">
						<option value="0">Inactive</option>
						<option value="1">Active</option>
					</select>
				</div>
	      <?php } ?>
      </div>

      <?php if (isset($bundle['product_details']) && is_array($bundle['product_details']) && count($bundle['product_details'])) { ?>

          <?php foreach ($bundle['product_details'] as $bundle_product) { ?>

              <div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-8 date-input valid">
					<label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
					<input required type="text" class="form-control has-feedback-left"
						name="product_name[]"
						<?php if (isset($bundle_product['product_name']) && !empty($bundle_product['product_name'])) echo 'value="' . $bundle_product['product_name'] . '"'; ?>>
					<input type="hidden" name="product_id[]"
						value="<?php echo $bundle_product['product_id']; ?>" /> <span
						class="fa fa-shopping-basket form-control-feedback left"
						aria-hidden="true"></span>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-4 date-input valid">
					<label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
					<input required type="text"
						class="form-control has-feedback-left text-right"
						name="quantity[]"
						<?php if (isset($bundle_product['quantity']) && !empty($bundle_product['quantity'])) echo 'value="' . $bundle_product['quantity'] . '"'; else echo 'placeholder="Quantity"'; ?>>
					<span class="fa fa-database form-control-feedback left"
						aria-hidden="true"></span>
				</div>

				<div class="col-md-2" style="margin-top: 30px;">
					<a onclick="$(this).parent().parent().remove(); return false;"
						class="btn btn-link"> <span class="fa fa-remove hidden-xs"></span>
					</a>
				</div>
			</div>

          <?php } ?>

      <?php } ?>

      <fieldset id="additional-field-model">
				<div class="form-group">
					<div class="col-md-4 col-sm-4 col-xs-8 date-input valid">
						<label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
						<input required type="text" class="form-control has-feedback-left"
							name="product_name[]" placeholder="Product Name" /> <input
							type="hidden" name="product_id[]" value="0" /> <span
							class="fa fa-shopping-basket form-control-feedback left"
							aria-hidden="true"></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-4 date-input valid">
						<label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
						<input required type="text"
							class="form-control has-feedback-left text-right"
							name="quantity[]" placeholder="Quantity" /> <span
							class="fa fa-database form-control-feedback left"
							aria-hidden="true"></span>
					</div>

					<div class="col-md-2" style="margin-top: 30px;">
						<a href="javascript:void(0);"
							class="btn btn-link remove-this-field"> <span
							class="fa fa-remove hidden-xs"></span>
						</a> <a href="javascript:void(0);"
							class="btn btn-link create-new-field"> <span
							class="fa fa-plus hidden-xs"></span>
						</a>
					</div>
				</div>
			</fieldset>

			<div class="form-group">
				<div class="clearfix">
					<br />
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="hidden" name="bundle_id" id="bundle_id"
						value="<?php if (isset($bundle_id)) echo $bundle_id; ?>" /> <input
						type="hidden" name="form_submitted" id="form_submitted" value="1" />

				</div>
			</div>

		</form>
	</div>

	<a href="javascript:void(0);" onclick="$('#bundle_form').submit();">
		<button type="button" class="btn btn-primary">
        <?php if (isset($bundle_id) && $bundle_id) echo 'Edit Bundle'; else echo 'Add Bundle'; ?>
    </button>
	</a>

<?php if (isset($bundle_id) && $bundle_id && $bundle['active'] == 1) { ?>
    <button type="button" class="btn btn-warning"
		onclick="$('#delete_confirm_modal').modal('show');">Change Status</button>
<?php } ?>

<?php if (isset($bundle_id) && $bundle_id) { ?>
        <button type="button" class="btn btn-danger"
		onclick="$('#delete_modal').modal('show');">Delete Bundle</button>
    <?php } ?>

<div class="modal fade" id="delete_confirm_modal" role="dialog"
		data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header alert-info">
					<h4 class="modal-title">De-Activate Bundle Confirmation</h4>
				</div>
				<div class="modal-body">
					<p>Are you sure you want to de-activate this bundle?</p>
				</div>
				<div class="modal-footer">
					<button id="no_delete" type="button"
						class="alert-success btn btn-default" data-dismiss="modal">NO</button>
					<button id="yes_delete" type="button"
						class="alert-danger btn btn-default" data-dismiss="modal">YES</button>
				</div>
			</div>
		</div>
</div>
	
<div class="modal fade" id="delete_modal" role="dialog"
	data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header alert-info">
				<h4 class="modal-title">Delete Bundle Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this bundle?</p>
			</div>
			<div class="modal-footer">
				<button id="no_delete" type="button"
					class="alert-success btn btn-default" data-dismiss="modal">NO</button>
				<button id="yes_delete" type="button"
					class="alert-danger btn btn-default" data-dismiss="modal">YES</button>
			</div>
		</div>
	</div>
</div>
	



</div>


<script type="text/javascript">
$(document).ready( function() {
    $( "#bundle_form" ).validate( {
        rules: {
            bundle_name: "required",
            price: "required"
        },
        messages: {
            bundle_name: "Bundle Name is required",
            price: "Price is a required field"
        },
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
			//$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error" );
			//$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field"
    });

    createProductAutocompleteSearch();

    $('body').on('click', "input[name^='product_name']", function() {
        createProductAutocompleteSearch();
    });

	$('#delete_confirm_modal .modal-footer button').on('click', function(event) {
	  var button = event.target; // The clicked button
	
	  $(this).closest('.modal').one('hidden.bs.modal', function() {
	      if (button.id == 'yes_delete') changeItemStatus();
      });
   });

	$('#delete_modal .modal-footer button').on('click', function(event) {
		  var button = event.target; // The clicked button
		
		  $(this).closest('.modal').one('hidden.bs.modal', function() {
		      if (button.id == 'yes_delete') deleteStatus();
	      });
	 });
    
});


function createProductAutocompleteSearch()
{
    $("input[name^='product_name']").devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('bundles/getProducts'); ?>',
        onSelect: function(suggestion) { $(this).parent().find('input[type=hidden]').val(suggestion.data); }
    });
}

function deleteStatus()
{
    $.ajax({
        type: "POST",
        data: { delete_item_id: $('#bundle_id').val()},
        url: "<?php echo AppUrl::bicesUrl('bundles/deleteItemStatus/'); ?>",
        success: function() { location = "<?php echo AppUrl::bicesUrl('bundles/list/'); ?>"; },
    });
}


function changeItemStatus()
{
    $.ajax({
        type: "POST", data: { delete_item_id: $('#bundle_id').val(), active_flag: 0 },
        url: "<?php echo AppUrl::bicesUrl('bundles/updateItemStatus/'); ?>",
        success: function() { location = "<?php echo AppUrl::bicesUrl('bundles/edit/'); ?>" + $('#bundle_id').val(); }
    });
}

</script>
