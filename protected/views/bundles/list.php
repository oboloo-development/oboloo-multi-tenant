<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Bundles</h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('bundles/edit/0'); ?>">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Bundle
                </button>
            </a>

            <a href="<?php echo AppUrl::bicesUrl('bundles/export'); ?>">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export CSV
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

    <table id="bundle_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th> </th>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Products</th>
        </tr>
      </thead>

      <tbody>

          <?php 
          	  foreach ($bundles as $bundle) 
          	  { 
          	  	  $text_decoration = ' style="text-decoration: none;" ';
				  if ($bundle['active'] == 0) $text_decoration = ' style="text-decoration: line-through;" ';
          ?>

              <tr>
                    <td>
                        <a href="<?php echo AppUrl::bicesUrl('bundles/edit/' . $bundle['bundle_id']); ?>">
							<button class="btn btn-sm btn-success">Edit</button>
                        </a>
                    </td>
                    <td <?php echo $text_decoration; ?>><?php echo $bundle['bundle_name']; ?></td>
                    <td <?php echo $text_decoration; ?>><?php echo $bundle['description']; ?></td>
                    <td <?php echo $text_decoration; ?>>
                    	<div style="text-align: right;">
	                        <?php
	                            if (!empty($bundle['price']))
	                                echo Yii::app()->session['user_currency_symbol']. number_format(floatval($bundle['price']), 2);
	                        ?>
                        </div>
                    </td>
                    <td <?php echo $text_decoration; ?>>
                        <?php
                            if (isset($bundle['product_details']) && is_array($bundle['product_details']))
                                foreach ($bundle['product_details'] as $bundle_product_data)
                                    echo $bundle_product_data['product_name'] . '<br />';
                        ?>
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>


<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">De-Activate Bundle Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to de-activate this bundle: <span id="delete_name" style="font-weight: bold;"></span>?</p>
          <input type="hidden" name="delete_item_id" id="delete_item_id" value="0" />
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          	  NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          	  YES
          </button>
        </div>
      </div>
	</div>
</div>


<script type="text/javascript">
$(document).ready( function() {
    $('#bundle_table').dataTable({
        "columnDefs": [ {
            "targets": 0,
            "width": "6%",
            "orderable": false
        } ],
        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
            	for (var i=1; i<=4; i++)
                	$('td', row).eq(i).css('text-decoration', 'line-through');
        },        
        "order": []
    });

	$('#delete_confirm_modal .modal-footer button').on('click', function(event) {
	  var button = event.target; // The clicked button
	
	  $(this).closest('.modal').one('hidden.bs.modal', function() {
	      if (button.id == 'yes_delete') changeItemStatus();
      });
   });
    
})


function deleteBundle(item_id)
{
   	$('#delete_item_id').val(0);
   	$('#delete_name').text('');

	var item_name = "";
	var col_index = 0;
	$('#delete_icon_' + item_id).parent().parent().parent().find('td').each(function() {
		col_index += 1;
		if (col_index == 2) item_name = $(this).text();
	});

	$('#delete_item_id').val(item_id);
	if ($('#delete_icon_' + item_id).hasClass('glyphicon-ok')) changeItemStatus();
	else
	{
		$('#delete_item_id').val(item_id);
		$('#delete_name').text(item_name);
		$('#delete_confirm_modal').modal('show');
	}	
}

function changeItemStatus()
{
   	var delete_item_id = $('#delete_item_id').val();
   	var active_flag = 0;
	    	
   	if ($('#delete_icon_' + delete_item_id).hasClass('glyphicon-remove'))
   	{
   		active_flag = 0;
   		$('#delete_icon_' + delete_item_id).removeClass('glyphicon-remove');
   		$('#delete_icon_' + delete_item_id).addClass('glyphicon-ok');
   	}
   	else
   	{
   		active_flag = 1;
   		$('#delete_icon_' + delete_item_id).removeClass('glyphicon-ok');
   		$('#delete_icon_' + delete_item_id).addClass('glyphicon-remove');
  	}
	    	
    $.ajax({
        type: "POST", data: { delete_item_id: delete_item_id, active_flag: active_flag },
        url: "<?php echo AppUrl::bicesUrl('bundles/updateItemStatus/'); ?>"
    });
    
	var col_index = 0;
	$('#delete_icon_' + delete_item_id).parent().parent().parent().find('td').each(function() {
		col_index += 1;
		if (col_index != 1)
		{
			if (active_flag == 0) $(this).css("text-decoration", "line-through");
			else $(this).css("text-decoration", "none");
		}
	});
}
</script>
