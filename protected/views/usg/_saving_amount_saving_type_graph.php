<div class="col-lg-6 col-md-6 col-xs-12 p-0">
    <div class="x_panel ">
    <div class="x_title">
      <h2>Savings Amount By Savings Type</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div id="saving_amount_by_saving_type"></div>
    </div>
  </div>
</div>

<div class="col-lg-6 col-md-6 col-xs-12  pr-0  mbl-pl-0">
    <div class="x_panel ">
    <div class="x_title">
      <h2>Forecasted & Realised Savings Amount Total</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div id="planned_and_realised_savings_by_saving_type"></div>
    </div>
  </div>
</div>

<!-- // Start: Savings Amount By Savings Type  -->
<script  type="text/javascript">
  // convert if amount is greater then 1000000 => show into in the million
  const numberFormatter = function(value) {
    let val = Math.abs(value);
     if (val >= 1) {
      val = (val / 1000000).toFixed(2);
     }
    return val;
  };

    var options = {
      colors : ['#29d179', '#ccc8c8'],
      series: [{
      name: 'Realized',
      data: [<?php echo $savingAmountByType['cost_reduction_realized'];   ?>,
             <?php echo $savingAmountByType['cost_avoidance_realized'];   ?>,
             <?php echo $savingAmountByType['cost_containment_realized']; ?>]
            }, {
      name: 'Forecasted',
      data: [<?php echo $savingAmountByType['cost_reduction_forecasted'];   ?>,
             <?php echo $savingAmountByType['cost_avoidance_forecasted'];   ?>,
             <?php echo $savingAmountByType['cost_containment_forecasted']; ?>]
        }],
          chart: {
          type: 'bar',
          height: 350,
          stacked: true,
        },
        plotOptions: {
          bar: {
            horizontal: false,
            dataLabels: {
              enabled: false,
              formatter: numberFormatter,
            }
          },
        },
        dataLabels: {enabled: false,},
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
          categories: ["Cost Reduction", "Cost Avoidance", "Cost Containment"],
        },
        yaxis: {
          labels: {
            formatter: numberFormatter
          },
          title: {
            text: "Millions",
            style: {
             fontSize: '10px',
             fontFamily: 'Poppins !important',
             fontWeight: 'normal',
             cssClass: 'apexcharts-xaxis-label',
            },
          },
        },
        tooltip: {
          y: {
            formatter: numberFormatter 
          }
        },
        fill: { opacity: 1 },
        legend: {
          position: 'bottom',
          horizontalAlign: 'center',
          offsetX: 20
        }
      };
    var chart = new ApexCharts(document.querySelector("#saving_amount_by_saving_type"), options);
    chart.render();
    // /* End  : Savings Amount By Savings Type  */

   
   var options = {
      colors : ['#29d179', '#ccc8c8'],
      series: [{
      name: 'Realized',
      data: [<?= $planRealizeSavingType['realized_total']; ?>]
            }, {
      name: 'Forecasted',
      data: [<?= $planRealizeSavingType['planned_total']; ?>]
        }],
          chart: {
          type: 'bar',
          height: 350,
          stacked: true,
        },
        plotOptions: {
          bar: {
            horizontal: false,
            dataLabels: {
              enabled: false,
              formatter: numberFormatter,
            }
          },
        },
        dataLabels: {enabled: false,},
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
          categories: ["", ""],
        },
        yaxis: {
          labels: {
            formatter: numberFormatter
          },
          title: {
            text: "Millions",
            style: {
             fontSize: '10px',
             fontFamily: 'Poppins !important',
             fontWeight: 'normal',
             cssClass: 'apexcharts-xaxis-label',
            },
          },
        },
        tooltip: {
          y: {
            formatter: numberFormatter 
          }
        },
        fill: { opacity: 1 },
        legend: {
          position: 'bottom',
          horizontalAlign: 'center',
          offsetX: 20
        }
      };
    var chart = new ApexCharts(document.querySelector("#planned_and_realised_savings_by_saving_type"), options);
    chart.render();
    // /* End  : Savings Amount By Savings Type  */
  </script>