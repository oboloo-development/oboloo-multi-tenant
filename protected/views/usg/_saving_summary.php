<?php 
$dateFormate =FunctionManager::dateFormat();
$userDepartment = FunctionManager::userSettings(Yii::app()->session['user_id'],'department_id');
$userLocation  = FunctionManager::userSettings(Yii::app()->session['user_id'],'location_id');
$materialType  = "select * from material_types where soft_deleted=0";
$materialReader= Yii::app()->db->createCommand($materialType)->query()->readAll();

$sql="select vendor_id,vendor_name from vendors where active=1 and member_type=0 and (vendor_archive is null  or vendor_archive='') and quick_created not in('Yes and Quick Sourcing Evaluation','Yes')  ORDER by vendor_name ASC";
$vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
$sql="select contract_id,contract_title from contracts where contract_archive='' or contract_archive is null";
$contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
$disabled    = FunctionManager::sandbox();

$sql = "SELECT sum(base_line_spend) as baseline FROM saving_milestone  where saving_id=".$saving['id'];
$mileStoneReader = Yii::app()->db->createCommand($sql)->queryRow();

if(!empty($mileStoneReader['baseline'])) $baseline = $mileStoneReader['baseline'];
else $baseline = 1;

$subcategoryUrl =  AppUrl::bicesUrl('products/getSubCategories');
$categoryUrl    =  AppUrl::bicesUrl('savingsUsg/getCategories');

$sql="select id,`value` as title from saving_country where soft_deleted='0' ORDER by value ASC";
$countryReader = Yii::app()->db->createCommand($sql)->query()->readAll();
?>

<form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" 
  action="<?php echo AppUrl::bicesUrl('strategicSourcing/edit/'.$saving['id']); ?>">
  <input type="hidden" name="update_saving" id="update_saving" value="1">
  <input type="hidden" name="redirect_url" id="redirect_url" value="edit">
  <input type="hidden" name="saving_id" id="saving_id" value="<?php echo $saving['id'];?>">
  <div class="form-group">
   <div class="col-md-6 col-sm-6 col-xs-12"> <br />
    <h4 class="heading">Saving Summary</h4>
   </div>
  </div>
  <div class="form-group"><br />
    <div class="col-md-6 col-sm-6 col-xs-6 valid">
     <label class="control-label">Savings Name <span style="color: #a94442;">*</span></label>
     <input required type="text" class="form-control notranslate" name="title" id="title"placeholder="Savings Name" value="<?php echo $saving['title'];?>"required autocomlete="off">
    </div>
    <div class="clearfix"></div><br />

    <div class="col-md-6 col-sm-6 col-xs-6 valid">
     <label class="control-label">Savings Description</label>
     <textarea class="form-control notranslate" name="notes" id="notes" rows="4" placeholder="Savings Notes"><?php echo $saving['notes'];?></textarea>
    </div>
       
   </div>
    <div class="form-group"> 
        <div class="col-md-3 col-sm-3 col-xs-6">
         <label class="control-label">Supplier Name</label><input type="text" class="form-control notranslate" name="oracle_supplier_name" value="<?php echo $saving['oracle_supplier_name']; ?>" 
         style="  border-radius: 10px;border: 1px solid #4d90fe;">
        </div>
       <div class="col-md-3 col-sm-3 col-xs-6">
          <label class="control-label">Oracle Supplier Number</label>
          <input type="text" class="form-control notranslate" name="oracle_supplier_number" id="oracle_supplier_number" value="<?php echo $saving['oracle_supplier_number']; ?>" style="  border-radius: 10px;border: 1px solid #4d90fe;">
       </div>
    </div>

    <div class="form-group">
      <div class="col-md-3 col-sm-3 col-xs-6 valid">
       <label class="control-label">Savings Status</label>
       <select name="status" id="status" class="form-control notranslate">
          <option value="">Select Savings Status</option>
          <?php foreach ($saving_status as $status) { ?>
          <option value="<?php echo $status['id']; ?>" <?php if($status['id']==$saving['status']){?> selected <?php } ?>><?php echo $status['value']; ?></option>
          <?php } ?>
      </select>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6">
        <label class="control-label">Savings Type
          <span style="color: #a94442;">*</span>
        </label>
        <select name="saving_type" id="saving_type" class="form-control notranslate" required>
            <option value="">Select</option>
            <?php 
            $saving_type = FunctionManager::savingType();
            foreach ($saving_type as $key=>$type) { ?>
            <option value="<?php echo $key; ?>" <?php echo $saving['saving_type']==$key?'selected="selected"':''?>><?php echo $type; ?></option>
            <?php } ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-3 col-sm-3 col-xs-12">
      <label class="control-label">Savings Area</label>
      <select name="saving_area" id="saving_area" class="form-control notranslate">
        <option value="">Select</option>
        <?php foreach($savingsArea as $savingArea){?>
        <option value="<?php echo $savingArea['id'];?>" <?php echo $saving['saving_area']==$savingArea['id']?'selected="selected"':''?>><?php echo $savingArea['value'];?></option>
        <?php } ?>  
      </select>
      </div>
   
       <div class="col-md-3 col-sm-3 col-xs-12">
          <label class="control-label">Material Type<span style="color: #a94442;">*</span></label>
          <select name="material_type_id" id="modal_material_type_id" onchange="loadCategories('<?php echo $categoryUrl;?>',this.value);"
           class="form-control notranslate"  required>
           <option value="">Select Material</option>
            <?php foreach ($materialReader as $material) { ?>
              <option value="<?php echo $material['id']; ?>" <?php if($material['id'] == $saving['material_type_id']){?> selected <?php } ?>>
                <?php echo $material['value']; ?>
              </option>
            <?php } ?>
          </select>
        </div>
    <div><div class="clearfix"></div>
   <div class="form-group">
      <div class="col-md-3 col-sm-3 col-xs-6">
        <label class="control-label">Primary Purchasing Category <span style="color: #a94442;">*</span></label>
        <select name="category_id" id="modal_category_id"
          class="form-control notranslate" onchange="loadSubcategories('<?php echo $subcategoryUrl;?>',this.value);" required>
          <option value="">Select Primary Purchasing Category</option>
        <?php foreach ($categories as $category) { ?>
          <option value="<?php echo $category['id']; ?>" <?php if($category['id']==$saving['category_id']){?> selected <?php } ?>>
            <?php echo $category['value']; ?>
          </option>
        <?php } ?>
      </select>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6">
        <label class="control-label">Secondary Purchasing Category</label>
        <select name="subcategory_id" id="subcategory_id"
          class="form-control notranslate">
          <option value="0">Select Secondary Purchasing Category</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <div class="col-md-3 col-sm-3 col-xs-6">
       <label class="control-label"> Operating Unit<span style="color: #a94442;">*</span></label>
        <select required name="department_id" id="department_id" 
        onchange="loadDeptForMultiLocations(this.value)" class="form-control notranslate">
         <option value="">Select Department</option>
        </select>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6">
        <label class="control-label">Savings Oracle Location<span style="color: #a94442;">*</span></label>
        <select required name="location_id" id="location_id" class="form-control dept_multipule_location">
         <option value="">Select Location</option>
        </select>
      </div>
     </div>
    <div class="form-group">
      <div class="col-md-3 col-sm-3 col-xs-6">
       <label class="control-label">Business Unit</label>
       <select name="business_unit" id="business_unit" class="form-control notranslate">
        <option value="">Select</option>
        <?php foreach($savingsBussinessUnit as $savingBussinessUnit){?>
         <option value="<?php echo $savingBussinessUnit['id'];?>"<?php echo $saving['business_unit']==$savingBussinessUnit['id']?'selected="selected"':''?>><?php echo $savingBussinessUnit['value'];?></option>
          <?php } ?>  
        </select>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-6">
       <label class="control-label">Savings Location Country</label>
       <select name="country" id="country" class="form-control notranslate">
          <option value="">Select</option>
           <?php foreach($countryReader as $countValue){?>
                  <option value="<?php echo $countValue['title'];?>" <?php echo $saving['country']==$countValue['title']?'selected="selected"':''?>><?php echo $countValue['title'];?></option>
           <?php } ?>
       </select>
      </div>
  </div>
   <div class="form-group"> 
      <div class="col-md-6 col-sm-6 col-xs-6  date-input">
        <label class="control-label">Savings Currency <span style="color: #a94442;">*</span></label>
      <select name="currency_id" id="currency_id" class="form-control notranslate" required>
      <?php foreach ($currency_rate as $rate) { ?>
      <option value="<?php echo $rate['id']; ?>" <?php if($rate['id']==$saving['currency_id']){?> selected <?php } ?>>
      <?php echo $rate['currency']; ?>
      </option>
      <?php } ?>
      </select>
    </div>
  </div>
  <div class="form-group">
   <div class="col-md-3 col-sm-3 col-xs-6">
    <label class="control-label">Savings Resulted from a Special Event?</label>
    <select name="special_event" id="special_event" class="form-control notranslate">
          <option value="">Select</option>
          <option value="Yes" <?php echo $saving['special_event']=='Yes'?'selected="selected"':''?> >Yes</option>
          <option value="No" <?php echo $saving['special_event']=='No'?'selected="selected"':''?> >No</option>  
     </select>
   </div>
   <div class="col-md-3 col-sm-3 col-xs-6">
    <label class="control-label">Event Name & Details</label>
    <input type="text" class="form-control notranslate" name="event_name" id="event_name" value="<?php echo $saving['event_name']; ?>" style="  border-radius: 10px;border: 1px solid #4d90fe;">
    </div>
   </div>

   <!--New Or Incremental Savings  -->
   <div class="form-group">
          <!-- <div class="col-md-3 col-sm-3 col-xs-6">
          <label class="control-label">New Or Incremental Savings</label>
           <select name="new_or_incremental_savings" id="new_or_incremental_savings" class="form-control notranslate">
            <option value="">Select</option>
            <?php /*$newOrIncrements = FunctionManager::newOrIncrementalSavings(); 
                foreach($newOrIncrements as $key=> $newOrIncrement){?>
                    <option value="<?= $key ?>" <?php echo $saving['new_or_incremental_savings']==$key?'selected="selected"':''?>><?= $newOrIncrement ?></option>
             <?php } */?>
           </select>
          </div>  -->
     
     <div class="col-md-6 col-sm-6 col-xs-6">
      <label class="control-label">Archived?</label>
       <select name="new_archived" id="new_archived" class="form-control notranslate">
        <option value="">Select</option>
        <option value="1" <?= $saving['new_archived']==1?'selected="selected"':''?>>Yes</option>
        <option value="0" <?= $saving['new_archived']==0?'selected="selected"':''?>>No</option>
       </select>
      </div>
    </div>
    <?php $readonly=''; if(Yii::app()->session['user_type'] != 4) $readonly = 'readonly';  ?>
    <div class="form-group">
     <div class="col-md-8 col-sm-8 col-xs-12 mt-18">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 p-0">
        <label><input type="checkbox" class=" notranslate" name="current_budget" id="current_budget" value="1"
          <?php if($saving['current_budget'] == 1)  echo 'checked="checked"'; ?> <?= $readonly ?> /> In Current Budget</label>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
         <label ><input type="checkbox" class=" notranslate" name="initially_verified" id="initially_verified" value="1"
           <?php if($saving['initially_verified'] == 1)  echo 'checked="checked"'; ?> <?= $readonly ?> /> Pre-Savings Validation
         </label>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
         <label><input type="checkbox" class=" notranslate" name="verified" id="verified" value="1"
          <?php if($saving['verified'] == 1)  echo 'checked="checked"'; ?> <?= $readonly ?> />Post-Savings Validation</label>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <label><input type="checkbox" class=" notranslate" name="supported_by_px" id="supported_by_px" value="1"
          <?php if($saving['supported_by_px'] == 1)  echo 'checked="checked"'; ?> <?= $readonly ?> />Supported by PX</label>
      </div>
     </div>
    </div>
    <div class="form-group">  
      <div class="col-md-6 col-sm-6 col-xs-6">
        <label class="control-label">Total Planned Savings</label>
         <div class="input-group">
          <span class="input-group-addon fieldGroupReadOnly"><?php echo $currencyReader['currency_symbol']; ?></span>
         <input type="number" class="form-control fieldLeftRadius notranslate" name="total_project_saving" id="total_project_saving" placeholder="Total Project Savings"  value="<?php echo $saving['total_project_saving']?number_format($saving['total_project_saving'],0,'.',''):'';?>" readonly /> 
        </div>
      </div>
      <div class="clearfix"></div>
   </div>
  <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-6">
        <label class="control-label">Total Realized Savings</label>
        <div class="input-group">
        <span class="input-group-addon fieldGroupReadOnly"><?php echo $currencyReader['currency_symbol']; ?></span>
        <input type="number" class="form-control notranslate fieldLeftRadius" name="realised_saving" id="realised_saving" placeholder="Total Realised Savings" readonly value="<?php echo $saving['realised_saving']? number_format($saving['realised_saving'],0,'.',''):'';?>" />
        </div> 
      </div>
    </div>
  <div class="form-group">
    <!-- <div class="col-md-3 col-xs-6">
      <label class="control-label">Savings Start Date</label>
      <input type="text" readonly class="form-control notranslate"  name="start_date" id="start_date" placeholder="Savings Start Date" value="<?php echo $saving['start_date'] != '0000-00-00 00:00:00'?date($dateFormate, strtotime($saving['start_date'])):'';?>" />
    </div> -->
    <div class="col-md-2 col-xs-6">
      <label class="control-label">Savings Due Date</label>
      <input type="text" readonly class="form-control notranslate" name="saving_due_date" id="saving_due_date" placeholder="Savings Due Date" value="<?php echo $saving['due_date'] != '0000-00-00 00:00:00'?date($dateFormate, strtotime($saving['due_date'])):'';?>"/> 
    </div>
 
    <div class="col-md-2 col-xs-6">
      <label class="control-label">Created On</label>
      <input type="text" readonly class="form-control notranslate" name="created_at" id="created_at" placeholder="Created On" value="<?php echo $saving['created_at'] != '0000-00-00 00:00:00'?date($dateFormate, strtotime($saving['created_at'])):'';?>" /> 
    </div>
    <div class="col-md-2 col-xs-6">
      <label class="control-label">Created By</label>
      <input type="text" readonly class="form-control notranslate" name="user_name" id="user_name" placeholder="Created By" value="<?php echo $saving['user_name'];?>"  /> 
    </div>
     <div class="clearfix"></div>
  </div><br />
  <div class="col-md-6">
    <button type="submit" class="btn btn-primary btn-functionality submit-btn" style="background-color: #1abb9c !important; border-color: #1abb9c !important" <?php echo $disabled; ?>>Save Savings Record</button>
      <?php if($saving['status']=="1"){?>
     <button type="button" class="btn btn-default" data-toggle="modal" data-target="#validateSavingsModal" style="background-color: #F79820;color: #fff; border-color:#F79820">
              <span class="glyphicon-glyphicon-info-sign" aria-hidden="true"></span>  Validate Savings</button>
      <?php } ?>
  </div>
</div></div>
</form>



