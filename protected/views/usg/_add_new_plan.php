 <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Add New Month</h4>
      </div>

        <form action="<?php echo $this->createUrl('PlannedStore'); ?>" method="POST">
         <div class="modal-body"> 
         <input type="hidden" name="planned_saving_id" value="<?php echo $saving_id; ?>" />
           <div class="form-group">
             <label>Month </label>
             <input type="date" name="planned_month" class="form-control  notranslate" required />
           </div>
           <div class="form-group">
             <label>Planned Savings Amount</label>
             <input type="text" name="planned_amount" class="form-control notranslate" value="" required />
           </div>
         </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button  type="submit" class="btn btn sm green-btn" >Save</button>
      </div>
      </form>
    </div>
  </div>

<div class="clearfix"><br/></div>