<?php
if(FunctionManager::savingSpecific()){
  $milestoneDueDate  = "Savings Expiration Date";
  $milestoneBaseline ='<input type="hidden" class="form-control notranslate" name="base_line_spend" id="base_line_spend" value="0"/>';
  $milestoneCostAvoidance = ' <input type="hidden" class="form-control notranslate" name="cost_avoidance" id="cost_avoidance" value="0" />';
  $milestoneCostReduction = "Total Planned Savings ";
  $milestoneCostReductionClass = "col-md-6 col-sm-6 col-xs-12";
  $savingDate = "Savings Start Date <span style='color: #a94442; margin-left: -2px;'>*</span>";
  $toolTipReduction = '';
}else{
  $milestoneDueDate  = "Due Date ";
  $savingDate = 'Savings Due Date <span style="color: #a94442; margin-left: -2px;">*</span>';
  $milestoneBaseline ='<div class="col-md-4 col-sm-4 col-xs-12">
        <label class="control-label">Baseline Spend <span style="color: #a94442;">*</span></label>
        <input type="number" class="form-control notranslate" name="base_line_spend" id="base_line_spend" placeholder="Baseline Spend" required/> 
      </div>';
  $milestoneCostAvoidance ='<div class="col-md-4 col-sm-4 col-xs-12" style="padding-right: 6px;">
        <label class="control-label">Projected Cost Avoidance <span style="color: #a94442; margin-left: -1px;">*</span></label>
        <input type="number" class="form-control notranslate" name="cost_avoidance" id="cost_avoidance" placeholder="Projected Cost Avoidance" required/> 
      </div>';
  $milestoneCostReduction = "Projected Cost Reduction ";
  $milestoneCostReductionClass = "col-md-4 col-sm-4 col-xs-12";
  $toolTipReduction = '';

}
$disabled = FunctionManager::sandbox(); ?>
<div class="modal fade" id="add_milestone" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style="margin-top: 138px; width: 630px;"> 

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
            Add Saving Financials
        </h4>
    </div>


    <div id="" class="alert alert-success ajax_success" role="alert"></div>
    <form id="add_milestone_form" class="form-horizontal col-md-12 form-label-left input_mask" enctype="multipart/form-data" method="post" action="#">
        <div class="col-md-12 col-sm-12 col-xs-12">
        <label class="control-label">Milestone Name <span style="color: #a94442;">*</span></label>
      <input type="text" class="form-control notranslate" name="milestone_title" id="milestone_title" placeholder="Milestone Name" required /> 
      </div>
  
        <div class="clearfix"></div><br />
        <div class="col-md-12 col-sm-12 col-xs-12" id="amountFields">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
        <label class="control-label"><?php echo $milestoneDueDate; ?><span style="color: #a94442;">*</span></label>
        <input type="text" class="form-control notranslate" name="due_date" id="due_date"  required readonly /> 
        </div><div class="clearfix"></div><br />
        <?php echo $milestoneBaseline; ?>
        <?php echo $milestoneCostAvoidance; ?>

        <?php if(!FunctionManager::savingSpecific()){?>
          <div class="<?php echo $milestoneCostReductionClass;?>">
          <label class="control-label"><?php echo $milestoneCostReduction; ?><span style="color: #a94442; margin-left: -2px;">*</span></label>
          <input type="number" class="form-control notranslate" name="cost_reduction" id="cost_reduction" placeholder="<?php echo $milestoneCostReduction; ?>" required/> 
        </div>
        <?php } ?>
  
    <input type="hidden" name="saving_id"  value="<?php echo $saving_id;?>" />
    <div class="clearfix"></div><br />
  <div class="modal-footer">
    <button type="submit" class="btn btn-success" <?php echo $disabled; ?> style="background-color: #1abb9c !important; border-color: #1abb9c !important"  >
          Add Saving Financials
    </button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div></form><div class="clearfix"></div></div></div></div>
  