<?php $dateFormate =FunctionManager::dateFormat();
$sql="select * from currency_rates where id=".$saving['currency_id'];
$currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
$initiativeOwner = new User();
$initiativeOwnerName = $initiativeOwner->getOne(array('user_id' => $saving['user_id']));
?>
<div class="right_col" role="main">
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
        <h4>
         <?php 
         if(!empty($saving['title'])){?>
          <br /><br /><strong class="notranslate">
          Savings Initiative Name: </strong> <span class="title-text notranslate"> <?php echo $saving['title'];?></span>
         <?php if(!empty($saving['user_id'])){?>
          <br /><br /><strong class="notranslate">Initiative Owner: </strong> <span class="title-text notranslate"> 
          <?php echo $initiativeOwnerName['full_name'] ?></span>
         <?php }
          if(!empty($saving['saving_type'])){
            $saving_type = FunctionManager::savingType($saving['saving_type']);?>
            <br /> <br /><strong class="notranslate">Savings Type: </strong> <span class="title-text notranslate">
            <?php echo $saving_type;?></span>
         <?php }
         if(!empty($saving['id'])){?>
            <br /> <br /><strong class="notranslate">Total Forecasted: </strong> <span class="title-text notranslate"> 
            <?= Yii::app()->session['user_currency_symbol'] .' '. number_format(FunctionManager::TotalForecasted($saving['id'])); ?></span>
         <?php }  
          if(!empty($saving['approver_status'])){
                $supStatus = $saving['approver_status'];
                if($supStatus=="Approved"){
                    $color="#DDF1D5";
                }else if($supStatus=="Sent For Approval"){
                    $color="#d5dbf1";
                }else if($supStatus=="Rejected"){
                    $color="#F0DFDF";
                }else {
                    $color="#DCDCDC";
                }?>
           <br /><br /><strong class="notranslate">Savings Status: </strong> <span class="title-text notranslate"> <span class="btn btn-primary" style="background: <?php echo $color;?> !important;border-color: <?php echo $color;?>  !important;border-radius: 10px;color:#2d9ca2 !important"><?php echo $saving['approver_status'];?></span></span>
          <?php } 
             if(!empty($saving['validate_approver_status'])){
                    $supStatus = $saving['validate_approver_status'];
                    if($supStatus=="Approved"){
                        $color="#DDF1D5";
                    }else if($supStatus=="Sent For Approval"){
                        $color="#d5dbf1";
                    }else if($supStatus=="Rejected"){
                        $color="#F0DFDF";
                    }else {
                        $color="#DCDCDC";
                    }?>
              <br /><br /><strong class="notranslate">Validate Savings Status: </strong> <span class="title-text notranslate"> <span class="btn btn-primary" style="background: <?php echo $color;?> !important;border-color: <?php echo $color;?>  !important;border-radius: 10px;color:#2d9ca2 !important"><?php echo $supStatus;?></span></span>
            <?php }} ?>

             
          </h4>

        </div>
        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('home'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;border-color: #F79820;">
                    <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Return To Savings List
                </button>
            </a>
             <div class="clearfix"></div><br />
             <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#mysupplierStatusModal" style="background-color: #F79820;color: #fff; border-color:#F79820">
              <span class="glyphicon-glyphicon-info-sign" aria-hidden="true"></span>  Change Savings Status</button>
        </div>
        

        <?php $savingCreated=Yii::app()->user->getFlash('saving_message');
          if(!empty($savingCreated)) { ?>
           <div class="clearfix"></div><br /><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $savingCreated;?></div>
        <?php } ?>
        <div class="clearfix"> </div><br />
      <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title"><h2>Savings Amount By Savings Type</h2><div class="clearfix"></div></div>
        <div class="x_content">
        <div id="saving_column_stacked_chart"></div>
        </div>
      </div>
      </div>
        
        <div class="col-md-6">
        </div>
        <div class="clearfix"> </div>
        <?php if(!empty(Yii::app()->user->getFlash('success'))):?>
            <div class="alert alert-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->hasFlash('error')):?>
            <div class="alert alert-danger">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
    </div>

<div class="row tile_count supplier-mbl-vw-tab" role="tabpanel" data-example-id="togglable-tabs">
      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="<?php echo empty($_GET['tab']) || $_GET['tab'] !='financial'?'active':'';?>">
            <a class="has-feedback-left pull-right" href="#tab_content1" id="quote-tab" role="tab" data-toggle="tab" aria-expanded="true">
                Savings Summary
            </a>
        </li>
        <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] =='financial'?'active':'';?>">
            <a href="#tab_content2" role="tab" id="vendor-tab" data-toggle="tab" aria-expanded="false">
                Savings Financials
            </a>
        </li>
        <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] =='documents'?'active':'';?>">
            <a href="#tab_content3" role="tab" id="vendor-tab" data-toggle="tab" aria-expanded="false">
                Attachments
            </a>
        </li>
      </ul>

    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane <?php echo empty($_GET['tab']) || $_GET['tab'] !='financial'?'fade active in':'';?>" id="tab_content1" aria-labelledby="quote-tab">
      <?php $this->renderPartial('/usg/_saving_summary',array('saving'=>$saving,'saving_status'=>$saving_status,'currency_rate'=>$currency_rate,'categories'=>$categories,'locations'=>$locations,'currencyReader'=>$currencyReader,'savingsArea'=>$savingsArea,'savingsBussinessUnit'=>$savingsBussinessUnit));?>
 
    </div>
    <div role="tabpanel" class="tab-pane <?php echo !empty($_GET['tab']) && $_GET['tab'] =='financial'?'fade active in':'';?>" id="tab_content2" aria-labelledby="vendor-tab">
       <?php  $this->renderPartial('/usg/_milestone',array('saving'=>$saving,'currencyReader'=>$currencyReader));?>
    </div>
    <div role="tabpanel" class="tab-pane <?php echo !empty($_GET['tab']) && $_GET['tab'] =='documents'?'fade active in':'';?>" id="tab_content3" aria-labelledby="vendor-tab">
       <?php  $this->renderPartial('/usg/_saving_documents',array('saving_id'=>$saving['id']));?>
    </div>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
   </div>
       <?php if(FunctionManager::savingSpecific()){
       $this->renderPartial('/savingsUsg/_log',array('savingLogs'=>$savingLogs));
       } ?>

  <?php $this->renderPartial('/savingsUsg/_savings_modals',array('saving'=>$saving,'saving_status'=>$saving_status, 'sourc_usg' => 'sourc_usg'));?>
</div>
<style type="text/css">
  .h4, h4 { font-size: 14px;}
  td,th {word-break:break-all !important;  word-wrap: break-word !important;}
  #milestone_table td:last-child {text-align: center;}
  .apexcharts-toolbar { display: none !important; },
  .form-control {padding-left: 6px !important;}
  .select2-container .select2-selection--single {height: 33px !important;}
  .fieldLeftRadius { border-top-left-radius: 0px !important;border-bottom-left-radius: 0px !important;}
  .fieldGroupReadOnly {background-color: #d8d4d4;border-radius: 10px;    border-top-right-radius: 0px;border-bottom-right-radius: 0px;}
</style>

<?php $subcategoryUrl=  AppUrl::bicesUrl('products/getSubCategories');
      $categoryUrl   =  AppUrl::bicesUrl('savingsUsg/getCategories');
      $departmentUrl =  AppUrl::bicesUrl('/savingsUsg/getUsgDepartments');
      $milestoneUrl  =  AppUrl::bicesUrl('/savingsUsg/listMilestone');
 ?>
 <script type="text/javascript">
    var milestoneurl =  "<?php echo AppUrl::bicesUrl('/savingsUsg/listMilestone');?>";
    var saving_id =  "<?php echo $saving['id'];?>";
    var addMilestoneUrl = "<?php echo AppUrl::bicesUrl('/savingsUsg/addSavingMilestone');?>";
    var dateFormat =  "<?php echo FunctionManager::dateFormatJS();?>";
    var deleteMilestoneUrl =  "<?php echo AppUrl::bicesUrl('savingsUsg/deleteSavingMilestone'); ?>";
    var deletePlaneUrl =  "<?php echo AppUrl::bicesUrl('savingsUsg/deletePlan'); ?>";
    var savingCalculationFieldsUrl =  "<?php echo AppUrl::bicesUrl('savingsUsg/savingCalculationFields'); ?>";
    var savingChartUrl =  "<?php echo AppUrl::bicesUrl('savingsUsg/savingChart1'); ?>";
    var savingChartUrl2 =  "<?php echo AppUrl::bicesUrl('savingsUsg/savingChart2');?>";
    var departmentUrl =  "<?php echo $departmentUrl; ?>";
    
 </script>
<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/custom_savings.js'); ?>"></script>
<script type="text/javascript">
  loadSubcategories("<?php echo $subcategoryUrl;?>","department_id_new","<?php echo $saving['subcategory_id'];?>");
  loadCategories("<?php echo $categoryUrl;?>","<?php echo  $saving['material_type_id'];?>","<?php echo $saving['category_id'];?>");

  loadDepartments("<?php echo $departmentUrl;?>","<?php echo $saving['location_id'];?>","<?php echo $saving['department_id'];?>")
  $(document).ready(function(){

    $('.generaldate').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>',
    useCurrent: false,
    debug:true
  });


    // dynamically create duration field
  $("#field_value,#milestone_due_date,#cost_reduction").on("change keyup blur",function(){
  var duration = $('#field_value').val();
  var start_date = $('#milestone_due_date').val();
  var cost_reduction = $('#cost_reduction').val();
  if(parseFloat(duration)>0 && parseFloat(cost_reduction)>0){
   $.ajax({
          type: "POST", 
          data: { duration: duration,start_date,start_date,cost_reduction:cost_reduction }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('savingsUsg/durationValue/'); ?>",
          success: function(results) {
           $('#amountFields').html(results.durationList);
           $('#due_date').val(results.endDate);
          }
  });
  }
  });


    // Start: saving milestone load
   $('#milestone_table').dataTable({
      "paging":false,
      "ordering":false,
      "info":false,
      "searching":false,
      "autoWidth":true ,
      "responsive":true,
      "columnDefs": [
          {"className": "dt-center", "targets": [ 2,3,4,5,6 ]}
         ],
       
        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
              for (var i=1; i<=6; i++)
                  $('td', row).eq(i).css('text-decoration', 'line-through');
        },        
        //"order": [[ 1,"asc" ]],
    //    "pageLength": 50,        
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": milestoneurl,
          "type": "POST",
          data: function ( input_data ) {
            input_data.saving_id = "<?php echo $saving['id'];?>";
            /*input_data.preferred_flag = $('#preferred_flag').val();
            input_data.industry_id = $('#industry_id').val();
            input_data.subindustry_id = $('#subindustry_id').val();
              input_data.supplier_status = $('#supplier_status').val();*/
        } 
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          if(aData[1].search("<span class='deactivated_vendor'></span>")>0){
            $(nRow).addClass('deactivated_record');
           
          }
         }
    });
    $('#milestone_table').DataTable().ajax.reload();
// End: saving milestone load  
  });


function loadgeneraldate(){
  $('.generaldate').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> HH:mm:ss',
    useCurrent: false,
    debug:true
  });
 }


function loadUpdateSavingChart1(){
  location.reload();
}

function editMilestone(ele)
{     
        var formObj = ele;
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('savingsUsg/editMilestone'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: $(formObj).serialize(),
             beforeSend: function() {
            $(".processing_oboloo").show();
            $(".ajax_success").html('');
            $(".ajax_success").hide();
            
           },
            success: function(project_data) {
              $(".ajax_success").show();
              $(".ajax_success").html('Milestone updated successfully');
              $('#milestone_table').DataTable().ajax.reload();
              $(".ajax_success").delay(1000*2).fadeOut();
              loadUpdateSavingChart1();
              loadUpdateSavingChart2();
              
            }
        });

        
        proModal = $(formObj).attr('id').replace ( /[^\d.]/g, '' );
        $('#savingedit'+proModal).modal('hide');
        
}

function milestoneComplete(ele)
{
        var formObj = ele;
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('savingsUsg/milestoneComplete'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: $(formObj).serialize(),
             beforeSend: function() {
            $(".processing_oboloo").show();
            $(".ajax_success").html('');
            $(".ajax_success").hide();
            
           },
            success: function(project_data) {
              $(".ajax_success").show();
              $(".ajax_success").html('Milestone completed successfully');
              
            }
        });
        proModal = $(formObj).attr('id').replace ( /[^\d.]/g, '' );
        $('#savingmilestonecomplete'+proModal).modal('hide');
        $('#milestone_table').DataTable().ajax.reload();
        $(".ajax_success").delay(1000*2).fadeOut();
        loadUpdateSavingChart1();
        loadUpdateSavingChart2();
}

$(document).ready(function(){
  var options = {

          series: [{
          name: 'Projected Savings',
          type: 'area',
          data: [<?php echo implode(",",$project_saving_chart1);?>]
        }, {
          name: 'Realised Savings',
          type: 'line',
          data: [<?php echo implode(",",$realised_saving_chart1);?>]
        }],
          chart: {
          fontFamily: 'Poppins !important',
          id: 'id_saving_area_chart',
          height: 270,
          type: 'line',
        },
        stroke: {
          curve: 'smooth'
        },
        fill: {
          type:'solid',
          opacity: [0.35, 1],
        },
        labels: [<?php echo "'".implode("','",$milestone_chart1)."'";?>],
        markers: {
          size: 0
        },
        dataLabels: {enabled: false,},
         xaxis: {
          labels : {
            style: {
             /* colors: '#2d9ca2',*/
              fontSize: '12px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
          },
          }
        },
        yaxis: [
       
          {
            title: {
              text: '',
               style: {
                fontSize:  '12px',
                fontFamily:  'Poppins !important',
                fontWeight:  'normal',
                /*color:'#2d9ca2'*/
              },


            },
            labels : {
            style: {
             /* colors: '#2d9ca2',*/
              fontSize: '12px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
          },
          }
           
          },

         /* {
          //  opposite: true,
            title: {
              text: 'Realised',
              style: {
                fontSize:  '12px',
                fontFamily:  'Poppins !important',
                fontWeight:  'normal',
                
              },
            },
            labels : {
            style: {
              
              fontSize: '12px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
          },
          }
          },*/

        ],
        tooltip: {
          shared: true,
          intersect: false,
         /* y: {
            formatter: function (y) {
              if(typeof y !== "undefined") {
                return  y.toFixed(0) + " points";
              }
              return y;
            }
          }*/
        },
        legend: {
          position: 'bottom',
          offsetX: 0,
          labels: {
          colors: ['#5bc0de', '#29d179'],
          useSeriesColors: ['#5bc0de', '#29d179']
          },
        },
        };
       /* var saving_area_chart = new ApexCharts(document.querySelector("#saving_area_chart"), options);
        saving_area_chart.render();*/
 // End: line and area chart

 // Start: column stacked chart
 colorstype:['#5bc0de', '#29d179', '#9C27B0'];
   var options = {
          series: [{
          name: 'Total Planned Savings',
          data: [null,<?php echo $planned_total;?>, null]
                },
          {name: 'Total Realized Savings',
          data: [null,<?php echo $realized_total;?>, null]
            }],
          chart: {
          fontFamily: 'Poppins !important',
          id: 'id_saving_bar_chart',
          type: 'bar',
          height: 270,
          stacked: false,
          toolbar: {
            show: true
          },
          zoom: {
            enabled: true
          }
        },
        dataLabels: {enabled: false,},
        colors: ['#5bc0de', '#29d179'],
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        plotOptions: {
          bar: {
            horizontal: false,
            distributed: false, // this line is mandatory
            columnWidth: '40%',
          },
        },
        xaxis: {
          //type: 'datetime',
          categories: ['Total Planned Savings', 'Total Realised Savings'],
          labels : {
            show: false,
            style: {
              colors: ['#5bc0de', '#29d179'],
              fontSize: '12px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
          },
          }
        },
        yaxis: {
           labels : {
            style: {
              colors: '#2d9ca2',
              fontSize: '12px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
          },
          }
        },
        legend: {
          position: 'bottom',
          offsetX: 0,
        },
        fill: {
          opacity: 1
        }
        };

        var chart = new ApexCharts(document.querySelector("#saving_column_stacked_chart"), options);
        chart.render();
         
 // End: column stacked chart

 });


function loadCategories(categoryUrl,material_type_id,category_id)
{

  var categoryUrl = categoryUrl
  var category_id = category_id;
  var material_type_id = material_type_id;
      if (material_type_id == 0)
        $('#modal_category_id').html('<option value="0">Select Category</option>');
      else
      { 
          $.ajax({
              type: "POST", data: { category_id: category_id, material_type_id:material_type_id }, dataType: "json",
              url: categoryUrl,
              success: function(options) {

                var options_html = '<option value="0">Select Category</option>';

                for (var i=0; i<options.suggestions.length; i++)
                  options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
                $('#modal_category_id').html(options_html);
                if (category_id != 0) $('#modal_category_id').val(category_id); 

              },
              error: function() { $('#modal_category_id').html('<option value="0">Select Category</option>'); }
          });
      }
}
$(document).ready(function() {
 loadDeptForMultiLocations("<?php echo $saving['location_id'];?>");
});

function loadDeptForMultiLocations(locationID) {
   
     var dept_id = $('#department_id_new').val();
     var location_ID = locationID;
    
      if (location_ID == 0)
        $('#dept_multipule_location').html('<option value="0">Select Locations</option>');
      else
      { 
        $.ajax({
            type: "POST", data: { dept_id: dept_id }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getLocations',
            success: function(options) {
              console.log(options);
                var options_html = '<option value="">Select Locations</option>';
                for (var i=0; i<options.length; i++)

                    options_html += '<option value="' + options[i].location_id + '">' + options[i].location_name + '</option>';
                $('.dept_multipule_location').html(options_html);
                 if (location_ID != 0) $('.dept_multipule_location').val(location_ID); 
               
            },
            error: function() { $('.dept_multipule_location').html('<option value="">Oracle Location</option>'); }
        });
    }
}


</script>
<style type="text/css">
  th.dt-center, td.dt-center {text-align: center;}
  th {word-break:break-all  !important;word-wrap:break-word !important;}
  .ui-datepicker {
    width: 17em !important; /*what ever width you want*/
  }
  .bootstrap-datetimepicker-widget.dropdown-menu{
    z-index:99999999 !important;
    width: 50% !important;
  }
</style>