<div class="col-md-12">
  <div class="pull-left " style="width: 85%">
    <h4 class="heading">Saving Financials</h4>
  </div>
  <button type="button" class="btn btn-default " data-toggle="modal" data-target="#plan_create_modal" 
     style="background-color: #F79820;color: #fff; border-color:#F79820">
      Add New Month
  </button>
</div>
<div class="clearfix"></div><br /><br />
<?php $this->renderPartial('/usg/_view_milestone',array('saving_id'=>$vendor_id));?>
<div class="clearfix"></div>
<div id="" class="alert alert-success ajax_success" role="alert"></div>
 <div class="clearfix"></div>
<div class="col-md-12 col-xs-12">
<?php
  $saving_id = $saving['id'];
  $sql = " SELECT * from saving_milestone where saving_id = ".$saving_id;
  $milestoneRead = Yii::app()->db->createCommand($sql)->query()->readAll(); 

  $sql = " SELECT milestone_field.*,milestone_field.field_name
         AS  order_by_date, cr.currency_symbol from milestone_field
    left join savings s on milestone_field.saving_id = s.id
    left join currency_rates as cr on s.currency_id = cr.id
   
   where milestone_field.saving_id =".$saving_id." ORDER BY order_by_date ASC ";
  $milestoneFieldRead = Yii::app()->db->createCommand($sql)->query()->readAll();

  if(!empty($milestoneFieldRead)){ ?>
  <div class="table-responsive table-container">
  <table id="milestone_planning_table" class="table table-striped table-bordered dt-responsive display nowrap" cellspacing="0" style="width: 100%;">
      <thead>
        <tr>
          <th style="text-align: left;">Month-Year</th>
          <th style="text-align: left;">Planned Savings</th>
          <th style="text-align: left;">Total Realized Savings</th>
          <th style="text-align: left; width: 30%;">Completion Comments</th>
          <th style="text-align: center;width:18%">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($milestoneFieldRead as $fValue){
            $plannModel = 'plannmodel_'.$fValue['id'];
            $completeModel = 'complete_'.$fValue['id'];
            $planModelID = "plan_".$fValue['id'];
            $calculation = "milestonePlanningCalculation('".$planModelID."','0')";
            $totalRealised = round($fValue['total_realised_savings']);
          ?>
            <tr>
             <td><?php echo  date("M-y",strtotime($fValue['field_name']));?></td>
             <td><?php echo $fValue['currency_symbol'].number_format(round($fValue['field_value']),2);?></td>
             <td><?php echo $totalRealised ? $fValue['currency_symbol'].number_format($totalRealised, 2) :"";?></td>
             <td><?php echo !empty($fValue['comments'])?$fValue['comments']:'';?></td>
             <td style="text-align: center;">
              <a href="#"  data-toggle="modal" data-target="#<?php echo $plannModel ?>"><i class="fa fa-edit" style="font-size:18px"></i></a>

              <?php 
              if (FunctionManager::checkEnvironment(true) && Yii::app()->session['user_type']=="4") {?>
               <a style='cursor: pointer; padding: 5px;' onclick="deletePlanning(<?php echo $fValue['id'];?>)"><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a>
              <?php }
              if(strtolower(trim($fValue['status'])) !="completed"){?>
                <a data-toggle="modal" data-target="#<?php echo $completeModel ?>" class="btn btn-primary submit-btn" style="text-decoration:none;font-size:10px;" >Mark As Complete</a>
              <?php }else{?>
                <a data-toggle="modal" data-target="#<?php echo $completeModel ?>" class="btn" style="background:#6cce47;border-color:#6cce47;color:#fff;font-size:10px;text-decoration: none;" >
                Completed 
                </a>
              <?php } ?>
             </td>
            </tr>
            
         <div class="modal fade" id="<?php echo $plannModel ?>" role="dialog">
         <div class="modal-dialog">
         <div class="modal-content">
         <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title" style="text-align:left;font-size: 20px">Planned Savings</h4>
          </div>
          <form action="<?php echo $this->createUrl('plannedEdit'); ?>" method="POST">
          <div class="modal-body">
          <input type="hidden" name="edit_planned_id" value="<?php echo $fValue['id']; ?>" />
          <input type="hidden" name="edit_planned_saving_id" value="<?php echo $fValue['saving_id']; ?>" />
          <input type="hidden" name="edit_planned_milestone_id" value="<?php echo $fValue['milestone_id']; ?>" />
          <div class="form-group">
           <label>Month</label>
           <input type="text" name="edit_planned_month" class="form-control  notranslate" onpaste="return false;" 
            onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false" autocomplete=off 
            value="<?php echo date(FunctionManager::dateFormat(),strtotime($fValue['field_name'])); ?>" readonly />
          </div>
          <div class="form-group">
            <label>Amount</label>
            <input type="text" name="edit_planned_amount" class="form-control notranslate" 
            value="<?php echo round($fValue['field_value']); ?>" />
          </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button  type="submit" class="btn btn sm green-btn" >Save</button>
      </div>
      </form>
    </div>
  </div>
</div>

    <!--Start: Mark as Complete Model -->
    <div class="modal fade" id="<?php echo $completeModel ?>" role="dialog">
      <div class="modal-dialog">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align:left;font-size: 20px">Planned Savings</h4>
        </div>

          <form action="<?php echo $this->createUrl('markCompleteEdit'); ?>" method="POST">
          <div class="modal-body">
          <input type="hidden" name="edit_planned_id" value="<?php echo $fValue['id']; ?>" />
          <input type="hidden" name="edit_planned_saving_id" value="<?php echo $fValue['saving_id']; ?>" />
          <input type="hidden" name="edit_planned_milestone_id" value="<?php echo $fValue['milestone_id']; ?>" />
          <div class="row">
          <div class="col-md-12">
            <div class="form-group">
            <label>Planned Savings <span style="color: #a94442;">*</span></label>
             <input type="text" name="edit_planned_amount" class="form-control notranslate" value="<?php echo round($fValue['field_value']); ?>" required <?php echo strtolower(trim($fValue['status'])) =="completed" && Yii::app()->session['user_type'] != 4?"readonly":"" ?>/>
            </div>
            <div class="form-group">
              <label class="control-label">Total Realized Savings <span style="color: #a94442;">*</span><!-- Realised Cost Reduction --></label><br />
                <input type="number" class="form-control notranslate" name="complete_edit_realised_cost_reduction" id="complete_edit_realised_cost_reduction<?php echo $planModelID;?>" value="<?php
                echo $totalRealised>0?round($totalRealised):'0';//$mValue['realised_cost_reduction']>0?round($mValue['realised_cost_reduction']):'0';?>"  style="width:98%;"  onkeyup="<?php echo $calculation; ?>"  onchange="<?php echo $calculation; ?>" required <?php echo strtolower(trim($fValue['status'])) =="completed"  && Yii::app()->session['user_type'] != 4?"readonly":"" ?>/> 
            </div>
            <div class="form-group">
              <label class="control-label">Comments</label>
              <textarea class="form-control notranslate" name="complete_edit_comment" id="notes" rows="4"></textarea>
            </div>
            <!-- Start : milestone table  -->
            <?php $sql = "SELECT * from milestone_field_comments where milestone_field_id=".$fValue['id'];
                  $commentHistory = Yii::app()->db->createCommand($sql)->query()->readAll(); 
            if(!empty($commentHistory)){?>
            <br /> <br />
            <div class="div-odd div-even-head">
              <div class="col-md-2">User</div>
              <div class="col-md-8">Comments</div>
              <div class="col-md-2">Date/Time</div>
              <div class="clearfix"></div>
            </div>

            <?php $ctr=2; 
            foreach($commentHistory as $commentValue){?>
             <div class="<?php echo $ctr%2=='0'?'div-even':'div-odd';?>">
              <div class="col-md-2"><?php echo $commentValue['user_name']; ?></div>
              <div class="col-md-8 notranslate"><?php echo $commentValue['comment']; ?></div>
              <div class="col-md-2"><?php echo date(FunctionManager::dateFormat(), strtotime($commentValue['datetime'])); ?></div>
              <div class="clearfix"></div>
             </div>
            <?php $ctr++; } ?>
            <?php } ?>
          <!-- End : milestone table  -->

          <input type="hidden" class="form-control notranslate" name="complete_edit_realised_cost_avoidance" id="complete_edit_realised_cost_avoidance<?php echo $planModelID;?>"  value="<?php echo $mValue['realised_cost_avoidance']>0?round($mValue['realised_cost_avoidance']):'0';?>" />

        <input type="hidden" class="form-control notranslate" name="complete_edit_realised_saving" id="complete_edit_realised_saving<?php echo $planModelID;?>"  value="<?php echo $totalRealised>0?round($totalRealised):'0'; ?>"  style="width:98%" readonly  />

          </div>
          </div>
            <div class="modal-footer">
             <button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Cancel</button>
              <button  type="submit" class="btn btn-primary">
              <?php echo strtolower(trim($fValue['status'])) =="completed"?"Save":"Mark As Complete" ?></button>
            </div>
          </form>
         </div>
         </div>
        </div>
        <!--End: Mark as Complete Model -->
        <?php } ?>
      </tbody>
     </table>
   </div>
 <br /><br />
<?php } //} ?>
</div> 


<!-- Modal -->
<div class="clearfix"></div><br />

<div class="modal" id="plan_create_modal" data-backdrop="static">
    <?php $this->renderPartial('/usg/_add_new_plan',array('saving_id'=>$saving['id']));?>
</div>