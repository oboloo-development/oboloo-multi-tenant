
<div class="panel panel-metrix usg_bg_banner_title">
  <div class="panel-body text-center ">YTD by Savings Status</div>
</div>
<div class="clearfix"></div>
<div class="ytd_tile_stats_count_ajax"></div>


<script>
   function countStatusByAmountAjax(){
    var filterColumns = {};
    filterColumns.department_id   = "<?= $dataArrFilter['departmentIdFilter']; ?>";
    filterColumns.initiative_owner= "<?= $dataArrFilter['initiativeOwnerFilter'] ?>";
    filterColumns.savings_area    = "<?= $dataArrFilter['savingsAreaFilter']; ?>";
    filterColumns.savings_type    = "<?= $dataArrFilter['savingsTypeFilter']; ?>";
    filterColumns.business_unit   = "<?= $dataArrFilter['businessUnitFilter']; ?>";
    filterColumns.filter_by_year  = "<?= $filter_by_year ?>";
    
     $.ajax({
      type: "POST",
      data: filterColumns,
      dataType: "json",
      url: "<?php echo AppUrl::bicesUrl('USG/strategicSourcing/countStatusByAmountAjax'); ?>",
      success: function(options) {
        $('.ytd_tile_stats_count_ajax').html(options);
      },
      error: function(err) {
        console.log(err);
      }
    });
  }
  setTimeout(countStatusByAmountAjax(), 12000);
</script>

<style type="text/css">
.ytd-metrice {background-color: #ed95a4;}
/*.ytd_tile_stats_count{width: 220px !important; border-radius: 7px; margin-right: 0.6rem; padding: 25px 5px !important; font-size: 16px !important;  margin-bottom: 10px !important}*/

@media only screen and (max-width: 768px){
  .ytd_tile_stats_count_ajax{
  display: grid;
  grid-template-columns: auto !important;
  grid-gap: 10px; margin-top: 20px;
}
.ytd_tile_stats_count{
  width: calc(100% / 6 - 20px);
  text-align: center; padding: 25px 5px !important;
  font-size: 30px; border-radius: 7px; font-size: 16px
}
}
.ytd_tile_stats_count_ajax{
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  grid-gap: 10px;
}
.ytd_tile_stats_count{
  text-align: center; padding: 25px 5px !important;
  font-size: 30px; border-radius: 7px; font-size: 16px
}

</style>