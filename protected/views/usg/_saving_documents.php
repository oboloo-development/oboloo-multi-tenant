<div class="col-md-12"><br />
 <div id="file_upload_alert" class="alert alert-success" role="alert"></div>
 <img class="processing_oboloo" src="<?php echo AppUrl::bicesUrl('images/icons/processin_oboloo.gif'); ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">
  <div class="pull-left"><h4 class="heading">Documents</h4></div>
  <div class="clearfix"></div>
  <div class="row mt-26">
  <form id="saving_document_form" onsubmit="return false; ">
      <input type="hidden" name="saving_id"  class="saving_id" value="<?= $saving_id; ?>">
      <div class="col-md-4">
        <div class="form-group">
          <label for="">Title <span class="error_color">*</span></label>
          <input type="text" name="doc_title" placeholder="Title" class="form-control doc_title" required />
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="">Document <span class="error_color">*</span></label>
          <input type="file" name="doc_file"  placeholder="Upload File" class="form-control doc_file" required />
        </div>
      </div>
      <div class="col-md-4">
        <button type="buttom" class="btn btn-primary mt-18 submit_btn">Upload</button>
      </div>
   </form>
  </div>

  <div id="document_list_cont " style="margin-top: 50px">
    <div class="pull-left" ><h4 class="heading" style="margin-bottom: 10px;">Uploaded Documents</h4></div>
    <table id="saving_document" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
        <tr>
          <th>Document Title</th>
          <th>Document</th>  
          <th class="text-center">Action</th>
       </tr>
      </thead>
      <tbody id="saving_document_list"></tbody>
    </table>
</div>
</div>
<!--  this page js is render in usg.js -->

