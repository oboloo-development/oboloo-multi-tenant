<script type="text/javascript">
  // convert if amount is greater then 1000000 => show into in the million
  let labelFormatter = function(value) {
    let val = Math.abs(value);
     if (val >= 1) {
      val = (val / 1000000).toFixed(2);
     }
    return val;
  };
  
  var options = {
          series: [
          {
            name: 'Total',
            data: [
              {
                x: 'Avoidance',
                y: <?= $areaAndType['Direct Materials'][2]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['dm_cost_avoidance'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
              {
                x: 'Containment',
                y: <?= $areaAndType['Direct Materials'][3]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['dm_cost_containment'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
              {
                x: 'Reduction',
                y: <?= $areaAndType['Direct Materials'][1]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['dm_cost_reduction'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
            ]
          }
        ],
          chart: {
          height: 350,
          type: 'bar'
        },
        plotOptions: {
          bar: {
            columnWidth: '40%'
          }
        },
        colors: ['#29d179'],
        dataLabels: {
          enabled: false
        },
        yaxis: {
         title: {
          text: 'Millions',
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        },
        labels: {
          formatter: labelFormatter,
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            
          },
          
        },
      },
        legend: {
          show: true,
          showForSingleSeries: true,
          customLegendItems: ['Total', 'Goals'],
          markers: {
            fillColors: ['#29d179', '#57585a']
          }
        }
        };
  var chart = new ApexCharts(document.querySelector("#saving_cat1"), options).render(); 
  /*End: Saving Type Direct Materials*/

  /*Start: Saving Type Indirect Materials*/
  var options = {
          series: [
          {
            name: 'Total',
            data: [
              {
                x: 'Avoidance',
                y: <?= $areaAndType['Indirect Materials'][2]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['im_cost_avoidance'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
              {
                x: 'Containment',
                y: <?= $areaAndType['Indirect Materials'][3]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['im_cost_containment'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
              {
                x: 'Reduction',
                y: <?= $areaAndType['Indirect Materials'][1]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['im_cost_reduction'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
            ]
          }
        ],
          chart: {
          height: 350,
          type: 'bar'
        },
        plotOptions: {
          bar: {
            columnWidth: '40%'
          }
        },
        colors: ['#29d179'],
        dataLabels: {
          enabled: false
        },
        yaxis: {
         title: {
          text: 'Millions',
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label'
          },
        },
        labels: {
          formatter: labelFormatter,
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            
          },
          
        },
      },
        legend: {
          show: true,
          showForSingleSeries: true,
          customLegendItems: ['Total', 'Goals'],
          markers: {
            fillColors: ['#29d179', '#57585a']
          }
        }
    };

     var chart = new ApexCharts(document.querySelector("#saving_cat3"), options).render();
    /*End: Saving Type Indirect Materials*/


    /*Start: Saving Type Capital/Mobile*/
  
     var options = {
          series: [
          {
            name: 'Total',
            data: [
              {
                x: 'Avoidance',
                y: <?= $areaAndType['Capital/Mobile'][2]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['cm_cost_avoidance'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
              {
                x: 'Containment',
                y: <?= $areaAndType['Capital/Mobile'][3]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['cm_cost_containment'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
              {
                x: 'Reduction',
                y: <?= $areaAndType['Capital/Mobile'][1]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['cm_cost_reduction'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
            ]
          }
        ],
          chart: {
          height: 350,
          type: 'bar'
        },
        plotOptions: {
          bar: {
            columnWidth: '40%'
          }
        },
        colors: ['#29d179'],
        dataLabels: {
          enabled: false
        },
        yaxis: {
         title: {
          text: 'Millions',
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
          },
        },
        labels: {
          formatter: labelFormatter,
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            
          },
          
        },
      },
        legend: {
          show: true,
          showForSingleSeries: true,
          customLegendItems: ['Total', 'Goals'],
          markers: {
            fillColors: ['#29d179', '#57585a']
          }
        }
        };

     var chart = new ApexCharts(document.querySelector("#saving_cat4"), options).render();
    
    /*End: Saving Type Capital/Mobile*/

     /*Start: Saving Type Energy*/
   
     var options = {
          series: [
          {
            name: 'Total',
            data: [
              {
                x: 'Avoidance',
                y: <?= $areaAndType['Energy'][2]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['e_cost_avoidance'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
              {
                x: 'Containment',
                y: <?= $areaAndType['Energy'][3]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['e_cost_containment'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
              {
                x: 'Reduction',
                y: <?= $areaAndType['Energy'][1]; ?>,
                goals: [
                  {
                    name: 'Goals',
                    value: <?= $savingDeptType['e_cost_reduction'] ?>,
                    strokeHeight: 5,
                    strokeColor: '#57585a'
                  }
                ]
              },
            ]
          }
        ],
          chart: {
          height: 350,
          type: 'bar'
        },
        plotOptions: {
          bar: {
            columnWidth: '40%'
          }
        },
        colors: ['#29d179'],
        dataLabels: {
          enabled: false
        },
        yaxis: {
         title: {
          text: 'Millions',
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
          },
        },
        labels: {
          formatter: labelFormatter,
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            
          },
          
        },
      },
        legend: {
          show: true,
          showForSingleSeries: true,
          customLegendItems: ['Total', 'Goals'],
          markers: {
            fillColors: ['#29d179', '#57585a']
          }
        }
        };

     var chart = new ApexCharts(document.querySelector("#saving_cat2"), options).render();
 
     /*End: Saving Type Energy*/

  </script>