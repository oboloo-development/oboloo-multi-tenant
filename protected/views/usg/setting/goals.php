<div class="right_col" role="main">
    <div class="tile_count">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <h3>Goals</h3><br />
            <h5 class="subheading">Please enter goals for the selected year by department. All values are in USD.</h5>
        </div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<form id="savings_due_date" method="post" class="pull-right" action="" autocomplete="off">
			 <div class=" form-inline" style="margin-right: -4px; margin-top: -5px; position: relative">
				<span style="font-size: 11px; font-weight: bold; margin-right: 8px;">Year filters</span>
				<select type="text" class="form-control notranslate saving_from_year" name="filter_by_year" id="saving_from_year" placeholder="From Date">
				 <option value="" >Select Year</option>
				  <?php $years = ['2015','2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030']; 
				  foreach($years as $year) {?>
				   <option value="<?= $year; ?>" 
				   <?php $flashYear = Yii::app()->user->getFlash('filter_by_year');
					if(!empty($filter_by_year) && $filter_by_year === $year ||
					  !empty($flashYear) && $flashYear === $flashYear) echo "selected" ?>><?= $year; ?>
				 </option>
				 <?php } ?>
				</select>
				<a href="<?php echo AppUrl::bicesUrl('goals'); ?>" class="btn btn-info search-goals" style="border-color: #46b8da;margin-left: 7px;">Clear Search</a>
				<button type="submit" class="btn btn-primary search-goals">Search</button>
			 </div>
			</form >
		</div>
      <div class="clearfix"><br><br></div>
      <?php 
			 $goalCreated=Yii::app()->user->getFlash('success');
		    if(!empty($goalCreated)) { ?>
		     <div class="alert alert-success mt-26">
		     	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		     	<?= $goalCreated;?>
		     </div>
		  <?php }?>
    </div>
    <form id="savings_due_date" method="post" action="<?php echo AppUrl::bicesUrl('USG/strategicSourcing/saveGoals'); ?>" autocomplete="off">
	   <div class="col-12 col-sm-12 col-lg-12 table-responsive">
	   	<input type="hidden" name="filter_by_year" value="<?= $filter_by_year ?>" />
	    <table id="saving_amount_table" class="table table-striped table-bordered savings-table" style="width: 145%;">
	      <thead>
	        <tr>
	          <th class="th-center">Department Name</th>
	          <th class="th-center">Cost Avoidance ($)</th>
	          <th class="th-center">Cost Reduction ($)</th>
	          <th class="th-center">Cost Containment ($)</th>
	          <th class="th-center">Total ($)</th>
	        </tr>
	      </thead>
	      
		    <?php if(!empty($departmentName)){ 
	    	  foreach($departmentName as $department){?>
	    	   <tr class="row_<?= $department['id'] ?>">
	    	   	<input type="hidden" name="depart_id[]" value="<?= $department['id'] ?>" />

	    	   	 <?php $goalHis = CommonFunction::GetGoals($department['id'], $filter_by_year);

	    	   	  if(!empty($goalHis)){
		    	    foreach($goalHis as $goal){ ?>
					     <td class="department_name"><?= $department['value'] ?></td> 
					     <td>

					     	<div class="input-group">
					     		<span class="input-group-addon">$</span>
									<input type="number" class="form-control td-input" id="cost_avoidance_<?= $department['id'] ?>" name="cost_avoidance[<?= $department['id'] ?>]" value="<?= $goal['cost_avoidance'] ?>" required >
									</div>
				     	 </td> 
					     <td>
					     	 	<div class="input-group">
					     		<span class="input-group-addon">$</span>
						     	 <input type="number"
							     	 class="form-control td-input" id="cost_reduction_<?= $department['id'] ?>" name="cost_reduction[<?= $department['id'] ?>]"
							     	 value="<?= $goal['cost_reduction'] ?>" required/>
							    </div>
					     </td> 
					     <td>
					     	<div class="input-group">
					     	 <span class="input-group-addon">$</span>
					     	 <input type="number" class="form-control td-input" id="cost_containment_<?= $department['id'] ?>" name="cost_containment[<?= $department['id'] ?>]" value="<?= $goal['cost_containment'] ?>" required/>
					     	</div>
					     </td> 
					     <td>
							<b>
							 <div class="input-group">
							  <span class="input-group-addon">$</span>
								<input type="number" class="form-control row-total" id="row_total_<?= $department['id'] ?>" name="row_total[<?= $department['id'] ?>]" readonly required/>
							 </div>
							</b>
					     </td>
				    </tr>
			    <?php }}else{?>
				     <td class="department_name"><?= $department['value'] ?></td> 
				     <td>
				     	 <div class="input-group">
					     	<span class="input-group-addon">$</span>
				     	 <input type="number" class="form-control td-input" id="cost_avoidance_<?= $department['id'] ?>" name="cost_avoidance[<?= $department['id'] ?>]"  required />
				     	</div>
				     </td> 
				     <td>
				     	<div class="input-group">
					     <span class="input-group-addon">$</span>
				     	<input type="number"
				     	 class="form-control td-input" id="cost_reduction_<?= $department['id'] ?>" name="cost_reduction[<?= $department['id'] ?>]" required/>
				     	</div>
				     </td> 
				     <td>
				     	<div class="input-group">
					     <span class="input-group-addon">$</span>
				     	<input type="number" class="form-control td-input" id="cost_containment_<?= $department['id'] ?>" name="cost_containment[<?= $department['id'] ?>]"  required/>
				      </div>
				     </td> 
				     <td>
				     	<b>
				     	<div class="input-group">
					     <span class="input-group-addon">$</span>
				     	 <input type="number" class="form-control row-total" id="row_total_<?= $department['id'] ?>" name="row_total[<?= $department['id'] ?>]" readonly required/>
				     	</div>
				     	</b>
				     </td>
				    </tr>
		  	<?php }}} ?>
	     	<tr>
	     		<td style="padding-top:18px;"><b>Total</b></td>
	     		<td> <b>	
	     			  <div class="input-group">
					     <span class="input-group-addon">$</span>
					     <input type="number" class="form-control column" id="total_cost_avoidance_" readonly   name="total_cost_avoidance[]" />
					   </div>
					</b></td>
	     		<td> <b>
	     			<div class="input-group">
					     <span class="input-group-addon">$</span>
					    <input type="number" class="form-control column" id="total_cost_reduction_"  readonly  name="total_cost_reduction[]" />
					  </div></b></td>
	     		<td> <b>
	     			<div class="input-group">
					     <span class="input-group-addon">$</span>
					     <input type="number" class="form-control column" id="total_cost_containment_" readonly name="total_cost_containment[]" />
					  </div></b></td>
	     		<td> <b>
	     			<div class="input-group">
					    <span class="input-group-addon">$</span>
	     			  <input type="number" class="form-control column" id="total_cost" name="total_cost[]" readonly />
	     			</div></b></td>
	     	</tr>

	     </tbody>
	    </table>
 
  	</div>
  			<button type="submit" class="btn btn-success pull-right">Save</button>
  	</form>
</div>

<script>
$(document).ready(function(){

		$(".td-input").keyup(function(){
		  obj = $(this).closest('tr');
		  /************ Start Rows ***************/
		  var total = 0;
		  $(obj.find('.td-input')).each(function(){
         currentValue = parseFloat($(this).val());
         if(!isNaN(currentValue)){
         		total=total+currentValue;
       		}       		
 				});

		    obj.find('.row-total').val(total);
		     /************ End Rows ***************/

		    /************ Columns ***************/
		    currentID = $(this).attr("id");
       	currentID = currentID.replace(/[0-9]/g, '');
       	var totalColumn=0;
       	$("input[id^='"+currentID+"']").each(function(){
   					currentValue = parseFloat($(this).val());
		        if(!isNaN(currentValue)){
		         	totalColumn=totalColumn+currentValue;
		       	}
 				});
 				$("#total_"+currentID).val(totalColumn);
 				//console.log(currentID);
 				costAvoidance   = parseFloat($("#total_cost_avoidance_").val());
 				costReduction   = parseFloat($("#total_cost_reduction_").val());
 				costContainment = parseFloat($("#total_cost_containment_").val());
 				if(isNaN(costAvoidance)){ costAvoidance = 0;}
 				if(isNaN(costReduction)){ costReduction = 0;}
 				if(isNaN(costContainment)){ costContainment = 0;}

		    totalColumn = costAvoidance+costReduction+costContainment;
		    $("#total_cost").val(totalColumn);
		    /************ End Columns ***************/
    });
    $(".td-input").trigger("keyup");

   // cut, copy, paste is not allowed 
   $(".td-input").on("cut copy paste",function(e) {
      e.preventDefault();
      alert('copy paste not allowed');
   });
});
</script>
<style>
 .department_name{padding-top: 19px !important; font-weight: 700;}
 .table-responsove{min-width: 146px !important;}
 .td-input, .row-total, .column{border-top-left-radius : 0px !important; border-bottom-left-radius : 0px !important; border-top-right-radius : 5px !important; border-bottom-right-radius :5px !important;}
 .input-group-addon{background-color: #ffffff !important;}
 @media only screen and (max-width: 768px){ #savings_due_date{margin-top: 20px;} .search-goals{margin-top: 10px;}}
</style>