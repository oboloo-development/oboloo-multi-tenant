<div class="panel panel-primary panel-metrix usg_bg_banner_title">
  <div class="panel-body text-center">Monthly Forecast & Comparables</div>
</div>
<?php 
  $currencySymbol = Yii::app()->session['user_currency_symbol'];
  $cat1 = $areaPlannedRealized[4];
  $cat2 = $areaPlannedRealized[3];
  $cat3 = $areaPlannedRealized[5];
  $cat4 = $areaPlannedRealized[6];
  $cat1Date =$cat2Date = $cat3Date =$cat4Date =array();
  $cat1Plan =  $cat1Real =$cat2Plan =  $cat2Real =$cat3Plan =  $cat3Real = $cat4Plan =  $cat4Real = array();

  foreach($cat1 as $key=>$value){
    $cat1Date[] = $key;
    $cat1Plan[] = $value['planned'];
    $cat1Real[] = $value['realized'];
  }
  foreach($cat2 as $key=>$value){
    $cat3Date[] = $key;
    $cat3Plan[] = $value['planned'];
    $cat3Real[] = $value['realized'];
  }
  foreach($cat4 as $key=>$value){
    $cat2Date[] = $key;
    $cat2Plan[] = $value['planned'];
    $cat2Real[] = $value['realized'];
  }
  foreach($cat3 as $key=>$value){
    $cat4Date[] = $key;
    $cat4Plan[] = $value['planned'];
    $cat4Real[] = $value['realized'];
  }
?>

<div class="table-responsive">
 <table id="saving_amount_table" class="table table-striped table-bordered savings-table" style="width: 100%;">
 <thead>
        <tr>
          <th class="th-center">Savings</th>
           <?php foreach($cat1Date as $key=>$value){?>
            <th class="th-center"><?php echo $value ;?></th>
           <?php } ?>
        </tr>
        </thead>
          <tr>
            <td align="left">Realized</td>
            <?php foreach($cat1Date as $key=>$value){
              $realized = $cat1Real[$key]+$cat2Real[$key]+$cat3Real[$key]+$cat4Real[$key];?>
              <td align="right" class="<?= $realized < 0 ? 'text-red' :''; ?>"><?php echo $currencySymbol.number_format($realized);?></td>           
            <?php } ?>
         </tr>
         <tr>
          <td align="left">Planned</td>
          <?php foreach($cat1Date as $key=>$value){
            $planned  = $cat1Plan[$key]+$cat2Plan[$key]+$cat3Plan[$key]+$cat4Plan[$key];?>
            <td align="right" class="<?= $planned < 0 ? 'text-red' :''; ?>"><?php echo $currencySymbol.number_format($planned);?></td> 
          <?php } ?>
         </tr>
         <tr>
          <td align="left">Variance (R-P)</td>
          <?php foreach($cat1Date as $key=>$value){
            $planned  = $cat1Plan[$key]+$cat2Plan[$key]+$cat3Plan[$key]+$cat4Plan[$key];
            $realized = $cat1Real[$key]+$cat2Real[$key]+$cat3Real[$key]+$cat4Real[$key];
            ?>
          <td align="right" class="<?= ($realized-$planned) < 0 ? 'text-red' :''; ?>">
            <?php echo $currencySymbol.number_format($realized-$planned);?></td><?php } ?>
         </tr>
         <tr>
          <td align="left">Variance % (R/P)</td>
          <?php foreach($cat1Date as $key=>$value){
            $realized = $cat1Real[$key]+$cat2Real[$key]+$cat3Real[$key]+$cat4Real[$key];
            $planned  = $cat1Plan[$key]+$cat2Plan[$key]+$cat3Plan[$key]+$cat4Plan[$key]; ?>
            
          <td align="right" class="<?= ($planned > 0 ? ($realized/$planned) *100 : 0) < 0 ? 'text-red' :''; ?>">
          <?php echo number_format($planned > 0 ? ($realized/$planned) *100:0);?>%</td>
         <?php } ?>
         </tr>
  </table></div>
<div class="clearfix"></div>
<!-- area-categ-line -->
<div class="">
  <div class="usg_panel col-md-6 col-lg-6 col-sm-12 col-xs-12">
    <div class="x_title"><h2>Direct Materials - Planned vs. Realized Savings</h2><div class="clearfix"></div></div>
    <div class="x_content"><div id="area_graph_1"></div></div></div>
  <div class="usg_panel col-md-6 col-lg-6 col-sm-12 col-xs-12 grap-right">
    <div class="x_title"><h2>Indirect Materials - Planned vs. Realized Savings</h2><div class="clearfix"></div></div>
    <div class="x_content"><div id="area_graph_3"></div></div></div>
  <div class="usg_panel mb-0 col-md-6 col-lg-6 col-sm-12 col-xs-12 ">
    <div class="x_title"><h2>Capital - Planned vs. Realized Savings</h2><div class="clearfix"></div></div>
    <div class="x_content"><div id="area_graph_4"></div></div><div class="clearfix"></div></div>
  <div class="usg_panel mb-0 col-md-6 col-lg-6 col-sm-12 col-xs-12 grap-right ">
    <div class="x_title"><h2>Energy - Planned vs. Realized Savings</h2><div class="clearfix"></div></div>
    <div class="x_content"><div id="area_graph_2"></div></div></div>
</div>
<script type="text/javascript">

  function catLineGraph(charID,title1,title2,exp){
    var seriesL =exp;
    var cat =[];
    var plan =exp.plan;
    var real =exp.real;
    var seriesDate =exp.catdate;
    seriesDate = seriesDate.map((each)=>{return each.split("-");});
    var title1 = title1;
    var title2 = title2;
    let seriesPlan = plan.map((each)=>{return JSON.parse(each)});
    
    let seriesReal = real.map((each)=>{return JSON.parse(each)});
    var catDate = exp.catdate;
    var charID = charID;
    var options = {
          series: [{name:title1,data:seriesPlan},{name:title2,data:seriesReal}],
          chart: {
            fontFamily: 'Poppins !important',
            height: 350,
            width: "100%",
            type: 'line',
            toolbar: {show: false}
          },
        colors: ['#5bc0de', '#29d179'],
        dataLabels: {enabled: false,  
        formatter: labelFormatter
        },
        stroke: {curve: 'smooth'},
        xaxis: {categories: seriesDate,   

        

        },
        yaxis: {
          title: {text: 'Millions',style: {fontSize: '10px',fontFamily: 'Poppins !important',fontWeight: 'normal',cssClass: 'apexcharts-xaxis-label',
              },},
          labels: {
           formatter: labelFormatter
          }
        },
        legend: {position: 'bottom',horizontalAlign: 'center',},
        };
        var chart = new ApexCharts(document.querySelector("#"+charID), options);
        chart.render();
   }
//catLineGraph("area_graph_1","Planned Savings","Realized Savings",<?php // echo json_encode(array('plan'=>implode(",", $cat1Plan),'catdate'=>implode(",", $cat1Real),'real'=>'"'.implode('","', $cat1Date).'"'))//?>);
 catLineGraph("area_graph_1","Planned Savings","Realized Savings",<?php echo json_encode(array('plan'=>$cat1Plan,'catdate'=>$cat1Date,'real'=>$cat1Real))?>);
 catLineGraph("area_graph_2","Planned Savings","Realized Savings",<?php echo json_encode(array('plan'=>$cat2Plan,'catdate'=>$cat2Date,'real'=>$cat2Real))?>);
  catLineGraph("area_graph_3","Planned Savings","Realized Savings",<?php echo json_encode(array('plan'=>$cat3Plan,'catdate'=>$cat3Date,'real'=>$cat3Real))?>);
  catLineGraph("area_graph_4","Planned Savings","Realized Savings",<?php echo json_encode(array('plan'=>$cat4Plan,'catdate'=>$cat4Date,'real'=>$cat4Real))?>);
</script>