<?php 

$domainUrl = Yii::app()->getBaseUrl(true);
$dateFormate = FunctionManager::dateFormat();

$reportFromDate = !empty($_POST['report_from_date']) ? $_POST['report_from_date'] : '';
$reportToDate = !empty($_POST['report_to_date']) ? $_POST['report_to_date'] : "";
$reportLocation = !empty($_POST['report_of_location']) ? $_POST['report_of_location'] : $reportLocation = "";

$report = new UsgReport();
$savingAmountByType = $report->savingAmountBySavingTypePhase2($filter_by_year, $dataArrFilter);
$savingDeptPlannedRelized = $report->savingDeptPlannedRelizedYealyGoal($filter_by_year);
$planRealizeSavingType = $report->plannedAndRealisedSavingsBySavingTypePhase2($filter_by_year, $dataArrFilter);
$areaAndType = $report->savingAreaAndTypePhase2($filter_by_year , $dataArrFilter);
$savingDeptType = $report->getSavingDeptarmentType($filter_by_year);

if (isset($_POST['report_from_date']) && isset($_POST['report_to_date'])) { $monthStart = $_POST['report_from_date']; }
$areaPlannedRealized = $report->areaPlannedRealizedGraphPhase2($filter_by_year, $dataArrFilter);
$currencySymbol = Yii::app()->session['user_currency_symbol'];
$query = "SELECT * FROM departments  ORDER BY department_name";
$getDepartment = Yii::app()->db->createCommand($query)->query()->readAll();
?>

<div class="right_col" role="main">
  <div class="row-fluid tile_count mb-0">
    <div class="col-lg-12 col-md-12 col-xs-12 text-right">
      <div class="col-md-12 col-xs-12 pull-right" style="margin-top: 10px; margin-bottom: 14px;">
        <?php if(Yii::app()->session['user_type']=="4") { ?>
         <a href="<?php echo AppUrl::bicesUrl('USG/StrategicSourcing/ExportCSV'); ?>"><button class="btn btn-primary">Export CSV</button></a>
        <?php } ?>
        <button type="button" class="btn btn-warning click-modal-poup hidden-xs" onclick="addSavingModal(event);" style="background-color: #f59118 !important; border-color: #f59118 !important; margin-right: -2px;">
          <span class="glyphicon glyphicon-plus mr-2" aria-hidden="true"></span> Create Savings Record
        </button> 
      </div> 
      <div class="clearfax"></div>
      <div class="col-md-9 col-xs-3" style="padding-top: 1%; font-weight: bold;"> Currency</div>
       <div class="col-md-3 col-xs-9 pull-right">
         <form action="<?php echo AppUrl::bicesUrl('home'); ?>" method="post" name="currency_dashboard_form"
          id="currency_dashboard_form">
          <?php  $sql = "SELECT * FROM `currency_rates` where  rate>0 and status='0'order by currency asc"; 
           $currencyRates = Yii::app()->db->createCommand($sql)->queryAll(); ?>
            <select name="user_currency" id="user_currency"
              class="form-control border-select "
              onchange="changeCurrency();">
              <?php foreach($currencyRates as $rateValue){?>
              <option value="<?php echo $rateValue['currency'];?>" 
                <?php if (isset(Yii::app()->session['user_currency']) &&
                 Yii::app()->session['user_currency'] == $rateValue['currency'])
                  echo ' selected="SELECTED" '; ?>>
                  <?php echo $rateValue['currency'];?></option>
              <?php } ?>
            </select>
        <input name="current_page_url" id="current_page_url" type="hidden"
        value="<?php echo Yii::app()->request->requestUri; ?>" />
          </form></div>  
    </div>

    <div class="span6 pull-left col-md-6">
      <h3 class="heading-title">Strategic Sourcing Annual Savings</h3>
    </div>
     <div class="clearfix"><br></div>
   

    <form id="" method="post" class="saving-highlight " action="<?php echo AppUrl::bicesUrl('home'); ?>" autocomplete="off">
      <div class="form_filter_container">
       <div class="input_filter_col form-group">
            <select type="text" class="form-control notranslate saving_from_year" name="filter_by_year" id="saving_from_year" placeholder="From Date" style="padding-top:10px">
              <option value="" >Select Year</option>
              <?php $years = ['2015','2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030']; 
              foreach($years as $year) {?>
                <option value="<?= $year; ?>" <?php if (!empty($year) && $year == $filter_by_year) echo ' selected="SELECTED" '; ?>><?= $year; ?></option>
              <?php } ?>
            </select>
        </div>
        <div class="input_filter_col form-group">
          <select name="department_id" id="department_id" 
            class="form-control select_department_multiple border-select" searchable="Search here..">
            <option value="">Operating Units</option>
            <?php if (!empty($getDepartment))
              foreach ($getDepartment as $dept) { ?>
              <option value="<?= $dept['department_id']; ?>" 
                <?php if ($dept['department_id'] == $dataArrFilter['departmentIdFilter']) echo ' selected="SELECTED" '; ?>><?= $dept['department_name']; ?></option>
            <?php } ?>
          </select>
        </div>
       <div class="input_filter_col">
          <div class="form-group">
            <select name="savings_area" id="savings_area" class="form-control  area_multiple border-select">
              <option value="0">Savings Areas</option>
              <?php foreach ($savingsArea as $savingArea) { ?>
                <option value="<?php echo $savingArea['id']; ?>" <?php if ($savingArea['id'] == $dataArrFilter['savingsAreaFilter']) echo ' selected="SELECTED" '; ?>><?php echo $savingArea['value']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="input_filter_col">
          <div class="form-group">
            <select name="savings_type" id="savings_type" class="form-control type_multiple border-select">
              <option value="0">Savings Types</option>
              <?php foreach ($saving_types = FunctionManager::savingType() as $key => $saving_typess) { ?>
                <option value="<?php echo $key; ?>" 
                  <?php if ($key == $dataArrFilter['savingsTypeFilter']) echo ' selected="SELECTED" '; ?>><?php echo $saving_typess; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="input_filter_col">
          <div class="form-group">
            <select name="business_unit" id="business_unit" class="form-control business_unit_multiple border-select">
              <option value="0">Business Units</option>
              <?php foreach ($savingsBussinessUnit as $savingBussinessUnit) { ?>
                <option value="<?php echo $savingBussinessUnit['id']; ?>"
                  <?php if ($savingBussinessUnit['id'] == $dataArrFilter['businessUnitFilter']) echo ' selected="SELECTED" '; ?>><?php echo $savingBussinessUnit['value']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="input_filter_col">
          <div class="form-group">
            <select name="initiative_owner" id="initiative_owner" class="form-control owner_multiple border-select">
              <option value="0">Initiative Owners</option>
              <?php foreach ($get_all_user as $user) { ?>
                <option value="<?php echo $user['user_id']; ?>"
                  <?php if ($user['user_id'] == $dataArrFilter['initiativeOwnerFilter']) echo ' selected="SELECTED" '; ?>><?php echo $user['full_name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      <div class="input_filter_col">
       <a href="<?php echo AppUrl::bicesUrl('home'); ?>" class="btn btn-info ">Clear Search</a>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
      <!-- End: search filters -->
    </div>
      <!-- End: New Select drop down -->
    </form>

  </div>
  <div class="clearfix"> </div>
  <?php $savingCreated = Yii::app()->user->getFlash('saving_message');
  if (!empty($savingCreated)) { ?>
    <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">
    &times;</button><?php echo $savingCreated; ?></div>
  <?php } ?>
  
  <!-- Start: First Matrixs -->
  <div class="row-fluid tile_count m-0">
   <div class="panel panel-metrix usg_bg_banner_title">
     <div class="panel-body text-center">Executive Level Summary</div>
   </div>
   <div class="col-lg-12 col-md-12 col-xs-12 text-center">
    <div class="lds-dual-ring"></div>
   </div>
   <div class="append_metrix"></div>
  </div>
  <!-- Start: First Matrixs -->

  <div class="clearfix"> </div>
<!-- Start : Total YE Forecast -->
  <div class="panel panel-metrix usg_bg_banner_title">
    <div class="panel-body text-center">Total YE Forecast</div>
  </div>
  
  <div class="row tile_count m-0">
    <?php  
    $data = ["savingAmountByType"       => $savingAmountByType,
             "planRealizeSavingType"    => $planRealizeSavingType, 
             "savingDeptPlannedRelized" => $savingDeptPlannedRelized];
    $this->renderPartial('/usg/_saving_amount_saving_type_graph', $data); ?>
  </div>
  
  <div class="clear-fix"></div>
  <!-- load and appended saving_amount_table_ajax to this id-->
  <div class="table-responsive saving_amount_table_ajax" style="overflow: hidden;"></div>
  <div class="panel panel-metrix usg_bg_banner_title">
    <div class="panel-body text-center">Department Level Summary</div>
  </div>
  <div class="clear-fix"></div>
  <div class="row row-xs-graps" style="zoom:85%; margin-left: -11px !important; margin-right: -17px !important;">
    <!-- Start: Saving Type Direct Materials -->
    <div class="x_panel area-categ-graph">
      <div class="x_title">
        <h2>Direct Materials Realized</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="saving_cat1"></div>
      </div>
    </div>
    <div class="x_panel area-categ-graph">
      <div class="x_title">
        <h2>Indirect Materials Realized</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="saving_cat3"></div>
      </div>
    </div>
    <div class="x_panel area-categ-graph">
      <div class="x_title">
        <h2>Capital/Mobile Realized</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="saving_cat4"></div>
      </div>
    </div>
    <div class="x_panel area-categ-graph">
      <div class="x_title">
        <h2>Energy Realized</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="saving_cat2"></div>
      </div>
    </div>

    <?php $this->renderPartial('/usg/_dept_level_summary_and_goals_graph', 
    array('areaAndType' => $areaAndType, 'savingDeptType' => $savingDeptType)); ?>
  </div>
  <div class="table-responsive departmentLevelSummaryAjax"></div>
  <!-- END : Department Level Summary -->
  <?php $this->renderPartial('/usg/_area_graph', array("areaPlannedRealized" => $areaPlannedRealized)); ?>
 <div class="clearfix"></div>
 <!-- START: YTD Forecast by status -->
 <?php $this->renderPartial('/usg/YTD_forecast_by_status_metrix', 
 array("dataArrFilter" => $dataArrFilter, 'filter_by_year' => $filter_by_year, 'statusCount' => $statusCount)); ?>
  <!-- END: YTD Forecast by status -->
   <div class="clearfix"></div>
  <div class="panel panel-metrix usg_bg_banner_title">
   <div class="panel-body text-center">Record Vault</div>
  </div>

  <div class="clearfix"></div>
  <div class="row tile_count m-0">
    <div class="">
    <div class="table-responsive " id="savings_phase2_table" >
      <table id="savings_table" class="table table-striped table-bordered savings-table m-0" style="width: 100%;">
        <thead>
          <tr>
            <th> </th>
            <th style="width:max-content; text-align: center;">Saving ID</th>
            <th class="text-center">Savings Initiative Name</th>
            <!-- <th>Start Date</th> -->
            <th class="text-center">Savings Start Date </th>
            <th class="text-center">Savings Due Date</th>
            <th class="text-center">Initiative Owner</th>
            <th class="text-center">Currency</th>
            <th class="text-center">Savings Type</th>
            <th class="text-center">Planned Savings</th>
            <th class="text-center">Realized Savings</th>
            <!-- <th class="th-center">Realized Savings %</th> -->
            <th class="text-center">Status</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>
    </div>
    <!-- </div> -->

  </div>
  </div>

</div>
<div class="usg_footer">
 <h1 >Managed by Procurement Excellence</h1>
</div>

<script type="text/javascript">
  colors = ['red', '#66DA26', '#546E7A', '#E91E63', '#FF9800', '#2E93fA', '#2196F3', 
            '#3a62ba', '#7b9333', '#344189', '#', '#', '#', '#', '#'];


  function init(){ getMetrixAjax(); }
  function loadSavings() {
    $('#savings_table').DataTable().ajax.reload();
    init();
  }


  function select2function(className, lableTitle) {
    var lableTitle = lableTitle;
    $("." + className).select2({ });
  }
  $(document).ready(function() {
    select2function('select_location_multiple', 'All Locations');
    select2function('select_department_multiple', 'All Operating Units');
    select2function('select_category_multiple', 'All Categories');
    select2function('select_subcategory_multiple', 'All Sub Categories');
    select2function('select_status_multiple', 'All Savings Statuses');

    select2function('owner_multiple', 'All Initiative Owners');
    select2function('archive_multiple', 'Archived');
    select2function('area_multiple', 'All Savings Areas');
    select2function('type_multiple', 'All Savings Types');
    select2function('business_unit_multiple', 'All Business Units');
    select2function('location_country_multiple', 'All Savings Location Countries');
    //select2function('incremental_savings_multiple','All New Or Incremental Savings');
  });

  <?php if (isset($category_id) && !empty($category_id)) { ?>
   <?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>
    <?php } else { ?>
    <?php } ?>
  <?php } ?>

  function loadSubcategories(subcategory_id) {
    var category_id = $('#category_id').val();

    $.ajax({
      type: "POST",
      data: {
        category_id: category_id
      },
      dataType: "json",
      url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
      success: function(options) {

        var options_html = '<option value="0">All Subcategories</option>';
        for (var i = 0; i < options.suggestions.length; i++)
          options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
        $('#subcategory_id').html(options_html);
        if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id);

        $('#subcategory_id').trigger('change');
      },
      error: function() {
        $('#subcategory_id').html('<option value="0">All Subcategories</option>');
      }
    });
  }
  jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function(s) {
      return Date.parse(s);
    },

    "sort-month-year-asc": function(a, b) {
      return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "sort-month-year-desc": function(a, b) {
      return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
  });
  $.fn.singleDatePicker = function() {
    $(this).on("apply.daterangepicker", function(e, picker) {
      picker.element.val(picker.startDate.format('DD/MM/YYYY'));
    });
    return $(this).daterangepicker({
      singleDatePicker: true,
      singleClasses: "picker_3",
      autoUpdateInput: false,
      locale: {
        format: 'DD/MM/YYYY'
      }
    });
  };

  $(document).ready(function() {
    $('#from_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>'
    });
    $('#from_date_to').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>'
    });
    $('#from_date_to').on('apply.daterangepicker', function(ev, picker) {
      $('#from_date_to').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS(); ?>'));
    });
    $('#from_date').on('apply.daterangepicker', function(ev, picker) {
      $('#from_date').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS(); ?>'));
    });
  });
  function addSavingModal(e) {
    e.preventDefault();
    $.ajax({
      datatype: 'html',
      url: "<?php echo AppUrl::bicesUrl('savingsUsg/createByModal'); ?>",
      type: "POST",
      data: {
        modalWind: 1,
        strSouAnnSav : 'usg'
      },
      success: function(mesg) {
        $('#create_saving_cont').html(mesg);
        $('#create_saving').modal('show');
      }
    });
  }
  // Start: line and area chart
   $('document').ready(function() {
    /*Start: Saving Type Direct Materials*/

    var options = {
      series: [{
        name: 'Planned Savings',
        data: [<?php echo $areaAndType['Direct Materials'][2]; ?>, <?php echo $areaAndType['Direct Materials'][3]; ?>, <?php echo $areaAndType['Direct Materials'][1]; ?>]
      }, ],
      chart: {
        type: 'bar',
        height: 380
      },
      plotOptions: {
        bar: {
          barHeight: '100%',
          distributed: true,
          //horizontal: true,
          dataLabels: {
            position: 'bottom'
          },
        }
      },
      colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
        '#f48024', '#69d2e7'
      ],
      dataLabels: {
        enabled: true,
        textAnchor: 'start',
        style: {
          colors: ['#fff']
        },
        formatter: function(val, opt) {
          return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
        },
        offsetX: 0,
        dropShadow: {
          enabled: true
        }
      },
      stroke: {
        width: 1,
        colors: ['#fff']
      },
      xaxis: {
        categories: [
          ["Cost", "Avoidance"],
          ["Cost", "Containment"],
          ["Cost", "Reduction"]
        ]
      },
      yaxis: {
        labels: {
          show: false
        }
      },
      title: {
        text: 'Custom DataLabels',
        align: 'center',
        floating: true
      },
      subtitle: {
        text: 'Category Names as DataLabels inside bars',
        align: 'center',
      },
      tooltip: {
        theme: 'dark',
        x: {
          show: false
        },
        y: {
          title: {
            formatter: function() {
              return ''
            }
          }
        }
      }
    };
});
</script>

<?php $this->renderPartial('/savingsUsg/create_saving'); ?>
<?php if (!empty($_GET['from']) && $_GET['from'] == 'information') { ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('.click-modal-poup').trigger('click');
    });
  </script>
<?php } ?>

<script >
 $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();    
   });
  function tableLoad(){
      $('#savings_table').dataTable({
      "columnDefs": [{
        //"targets": -1,
        "targets": 0,

        "ordering": false,
        "info": false
      }],

      "createdRow": function(row, data, index) {
        if (data[0].indexOf('glyphicon glyphicon-ok') >= 0)
          for (var i = 1; i <= 6; i++)
            $('td', row).eq(i).css('text-decoration', 'line-through');
      },
      "order": [
        [1, "desc"]
      ],
      "pageLength": 50,
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo AppUrl::bicesUrl('USG/strategicSourcing/listAjax'); ?>",
        "type": "POST",
        data: function(input_data) {
          input_data.location_id = $('#location_id').val();
          input_data.department_id    = "<?= $dataArrFilter['departmentIdFilter']; ?>";
          input_data.initiative_owner = "<?= $dataArrFilter['initiativeOwnerFilter'] ?>";
          input_data.savings_area     = "<?= $dataArrFilter['savingsAreaFilter']; ?>";
          input_data.savings_type     = "<?= $dataArrFilter['savingsTypeFilter']; ?>";
          input_data.business_unit    = "<?= $dataArrFilter['businessUnitFilter']; ?>";
          input_data.filter_by_year   = "<?= $filter_by_year ?>";
          input_data.category_id = $('#category_id').val();
          input_data.subcategory_id = $('#subcategory_id').val();
          input_data.status   = $('#status').val();
          input_data.due_date = $('#from_date').val();
          input_data.due_date_to = $('#from_date_to').val();
         
          input_data.savings_location_country = $('#savings_location_country').val();
          input_data.new_or_incremental_savings = $('#new_or_incremental_savings').val();
          input_data.new_archived = $('#new_archived').val();

        }
      },
      "oLanguage": {
        "sProcessing": "<h1>Please wait ... retrieving data</h1>"
      },
      "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (aData[1].search("<span class='deactivated_vendor'></span>") > 0) {
          $(nRow).addClass('deactivated_record');
          /*$(nRow).css("background-color", "#ccc");*/
        }
        for (var i = 3; i <= 9; i++) {
          $('td:eq(' + i + ')', nRow).addClass('td-center');
        }
      }
    });


  }
  function savingAmountTableAjaxLoad(){
   let filterData = allFliterFields();
   $.ajax({
      type: "POST",
      data: filterData,
      dataType: "json",
      url: "<?php echo AppUrl::bicesUrl('USG/strategicSourcing/savingAmountTable'); ?>",
      success: function(options) {
        $('.saving_amount_table_ajax').html(options);
      },
      error: function(err) {}
    });

    $.ajax({
        type: "POST",
        data: filterData,
        dataType: "json",
      url: "<?php echo AppUrl::bicesUrl('USG/strategicSourcing/savingAmountTableAjax'); ?>",
        success: function(options) {
        $('.departmentLevelSummaryAjax').html(options);
        },
        error: function(err) {
        }
      });
    }
    
$(document).ready(function() {
    $('#saving_table').dataTable({
      "columnDefs": [{
          "targets": 0,
          "width": "6%",
          "orderable": false
        },
        
      ],
      "order": []
    });
});

setTimeout(savingAmountTableAjaxLoad(), 12000);
setTimeout(tableLoad(), 15000);

function getMetrixAjax(){
  $(".lds-dual-ring").css('display', 'none');  
    let filterData = allFliterFields();
    $.ajax({
      type: "POST",
      data: filterData,
      dataType: "json",
      cache: false,
      url: "<?php echo AppUrl::bicesUrl('USG/strategicSourcing/getMetrixAjax'); ?>",
      beforeSend: function() { $(".lds-dual-ring").css("display", "inline-block") }, 
      success: function(options) {
       $('.append_metrix').html(options);
      },
      error: function(err) { console.log(err); }
    }).done(function(){
       $(".lds-dual-ring").css('display', 'none');
  });
}  

getMetrixAjax();
function allFliterFields(){
  var saveData = {};
  saveData.location_id     = $('#location_id').val();
  saveData.department_id   = "<?= $dataArrFilter['departmentIdFilter']; ?>";
  saveData.initiative_owner= "<?= $dataArrFilter['initiativeOwnerFilter']; ?>";
  saveData.savings_area    = "<?= $dataArrFilter['savingsAreaFilter']; ?>";
  saveData.savings_type    = "<?= $dataArrFilter['savingsTypeFilter']; ?>";
  saveData.business_unit   = "<?= $dataArrFilter['businessUnitFilter']; ?>";
  saveData.filter_by_year  = "<?= $filter_by_year ?>";
  return saveData;
}
</script>


<style type="text/css">
 .multiselect-selected-text { float: left; margin-left: 0px;}
 .btn .caret {float: right; margin-top: 10px;}
 .department .dropdown-toggle { min-width: 112%;}
 .apexcharts-xaxis-label:nth-last-child(2) {  transform: translateX(-2px) !important;}
 .btn-status { background: red !important;  border-color: 1px red;}
 .btn-status:hover { background: red !important;  border-color: 1px red; }

 .location .multiselect { width: 138%;}
 .department .multiselect { width: 112%;}
 .multiselect-container>li>a>label.checkbox,
 .multiselect-container>li>a>label.radio { color: #777;}
 #quote_table_filter label { font-weight: 500; }
 #quote_table_filter label input { font-weight: normal;}
 body {background: #ebecf6; }
 .apexcharts-toolbar { display: none !important; }
 .apexcharts-xaxis-label:nth-last-child(2) { transform: translateX(-20px) }
 
 
 .dropDwonSelectSaveing .border-select { padding-right: 3px; padding-right: 3px;}
 .ui-datepicker-calendar { display: none;}
 .panel-metrix { background: #337ab7; margin-left: 0px; margin-right: 0px; }
 .tile_count .tile_stats_count { margin-bottom: 10px; border-bottom: 0;padding: 22px 0px; margin-left: 0px !important; }
 .contract-metrice { font-size: 18px !important;}
 .executive_level_summary_metrix{ margin-bottom: 10px; border-bottom: 0; padding: 22px 0px; border-radius: 7px; background: #d54f4e !important;}
 .ytd_tile_stats_count{background: #d54f4e !important;}
  @media screen and (max-width: 768px) {
   .dropDwonSelectSaveing { width: 100% !important; min-width: 100% !important; margin-left: 0px !important;}
   .search_usg_mobile_vw{padding: 0px 16px !important;}
   .oracle_location{margin-top: 0px !important;}
   .mb-10{margin-bottom: 10px;}
   .cont-metrice{width: 100%; min-width: 100%; margin: 5px 0px !important;}
   .executive_level_summary_metrix{ margin: 5px 0px !important;}
   .search-contract{margin-left: 10px !important;}
  }

  @media (min-width:1025px) { .oracle_location {margin-left: -9px;}}
  @media only screen and (min-width: 992px) { .oracle_location { margin-left: -9px;} }
  @media only screen and (max-width: 490px) { .oracle_location { margin-left: 0px; margin-top: 10px;}}
  @media only screen and (max-width: 768px) { .search_usg_mobile_vw {padding: 0px !important; }}

  
  @media only screen and (max-width: 768px){ 
   .tile_count .tile_stats_count { margin-left: 0px !important; }
   .summary_metrix{ padding-left: 0px !important; padding-right: 0px !important;}
   .heading-title{font-size: 16px !important; padding: 20px 0px;}
   .row-xs-graps{
    margin-left: -11px !important;
    margin-right: -4px !important;}
  }

  @media only screen and (min-width: 769px and max-width:961px){
   .summary_metrix{padding-left: 0px !important; padding-right: 0px !important; }
   .executive_level_summary_metrix{ padding: 0px !important; }
  }
</style>