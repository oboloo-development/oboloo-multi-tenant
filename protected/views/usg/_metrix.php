<?php 
    $toGoal = 0;
    if($metrixArr['metrix_1'] > 0 && $metrixArr['metrix_2'] > 0){
      $toGoal = ($metrixArr['metrix_1'] / $metrixArr['metrix_2']) * 100; 
    }else{
      $metrix1 = $metrix2 = 1;
      if($metrixArr['metrix_1'] < 0.00 || $metrixArr['metrix_1'] > 0.00){
        $metrix1 = $metrixArr['metrix_1']; 
      }
      if($metrixArr['metrix_2'] < 0.00 || $metrixArr['metrix_2'] > 0.00){
        $metrix2 = $metrixArr['metrix_2'];
      }
      
      $toGoal = ($metrix1 / $metrix2)* 100;
    } 
?>


  
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pl-0 summary_metrix">
    <h4 class="text-center contract-metrice  tile_stats1  executive_level_summary_metrix">Total Forecasted<br>
    <?= Yii::app()->session['user_currency_symbol'] .' '. number_format($metrixArr['metrix_2']) ?></h4>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 summary_metrix" >
      <h4 class="text-center contract-metrice tile_stats2 executive_level_summary_metrix">% To Goal</br>
     <?= number_format($toGoal, 1) ?>% </h4>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 summary_metrix">
      <h4 class="text-center contract-metrice tile_stats3 executive_level_summary_metrix">Total Annual Goal<br />
      <?= Yii::app()->session['user_currency_symbol'] .' '. number_format($metrixArr['totalGoalCurrentYear']) ?></h4>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 mr-0 pr-0 summary_metrix">
      <h4 class="text-center contract-metrice tile_stats4 executive_level_summary_metrix">Monthly Run Rate </br>
      <?= Yii::app()->session['user_currency_symbol'] .' '. number_format( $metrixArr['metrix_1'] / 12) ?></h4>
    </div>
    
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 pl-0 summary_metrix">
    <h4 class="text-center contract-metrice tile_stats1 executive_level_summary_metrix">Carry-Over Savings<br> 
    <?= Yii::app()->session['user_currency_symbol'] .' '. number_format( $metrixArr['totalCarryoverSavings']) ?></h4>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 summary_metrix">
    <h4 class="text-center contract-metrice tile_stats2 executive_level_summary_metrix">YTD New Savings </br>
    <?= Yii::app()->session['user_currency_symbol'] .' '. number_format( $metrixArr['metrix_1']) ?></h4>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 summary_metrix">
    <h4 class="text-center contract-metrice tile_stats3 executive_level_summary_metrix">Remaining Planned Savings<br />
    <?= Yii::app()->session['user_currency_symbol'] .' '. number_format( $metrixArr['totalRemainPlanned']) ?></h4>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 mr-0 pr-0 summary_metrix">
    <h4 class="text-center contract-metrice tile_stats4 executive_level_summary_metrix">New Year Incremental Savings</br>
    <?= Yii::app()->session['user_currency_symbol'] .' '. number_format( $metrixArr['totalIncrementalSavings']) ?></h4>
  </div>
  
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 pl-0 summary_metrix">
      <h4 class="text-center contract-metrice tile_stats1 executive_level_summary_metrix">Cost Avoidance<br>
      <?= Yii::app()->session['user_currency_symbol'] .' '. number_format($savingByTypeMetrx['cost_avoidance_total']) ?></h4>
      </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 summary_metrix" >
      <h4 class="text-center contract-metrice tile_stats2 executive_level_summary_metrix">Cost Containment</br>
      <?= Yii::app()->session['user_currency_symbol'] .' '. number_format($savingByTypeMetrx['cost_containment_total']) ?></h4>
    </div>
  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 summary_metrix">
  <h4 class="text-center contract-metrice tile_stats3 executive_level_summary_metrix"> Cost Reduction<br />
  <?= Yii::app()->session['user_currency_symbol'] .' '. number_format($savingByTypeMetrx['cost_reduction_total']) ?></h4>
  </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ml-0 mr-0 pr-0 summary_metrix">
      <h4 class="text-center contract-metrice tile_stats4 executive_level_summary_metrix">Impact Against the Budget</br>
      <?= Yii::app()->session['user_currency_symbol'] .' '. number_format( $metrixArr['totalInCurrentBudgetSavings']) ?></h4>
    </div>

<div class="clearfix"></div>
<style>
 .tile_count .tile_stats_count { margin-bottom: 10px; border-bottom: 0;padding: 22px 0px; margin-left: 0px !important; }
 .contract-metrice { font-size: 16px !important;}
 .executive_level_summary_metrix{ margin-bottom: 10px; border-bottom: 0; padding: 22px 0px; border-radius: 7px;}
 @media only screen and (max-width: 768px){ 
  .tile_count .tile_stats_count { margin-left: 0px !important; }
  .summary_metrix{ padding-left: 0px !important; padding-right: 0px !important;} }
 @media only screen and (min-width: 769px and max-width:961px){
  .summary_metrix{padding-left: 0px !important; padding-right: 0px !important; }
  .executive_level_summary_metrix{ padding: 0px !important; }

 @media only screen and (min-width: 1300px){ .executive_level_summary_metrix { width: 329px !important;} }
</style>