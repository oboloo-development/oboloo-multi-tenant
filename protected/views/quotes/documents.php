<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_title" style="border-bottom:none"><h4 class="heading">Communications</h4></div>
  </div>
<div class="form-group" id="document_area_1">

  <div class="clearfix"></div>
  <div id="file_upload_alert" class="alert alert-success" role="alert"></div>
  <img class="processing_oboloo" src="<?php echo AppUrl::bicesUrl('images/icons/processin_oboloo.gif'); ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">

    <div class="col-md-3 col-sm-3 col-xs-12">
        <label class="control-label">Document <span style="color: #a94442;">*</span></label>
        <input class="form-control notranslate" type="file" name="order_file_1" id="order_file_1" />
        <span class="text-danger" style="font-size: 10px;">Maximum File Size of 2MB</span>
    </div>
   <div class="col-md-3 col-sm-3 col-xs-12">
        <label class="control-label">Document Title <span style="color: #a94442;">*</span></label>
        <input type="text" class="form-control notranslate" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" /> 
    </div>
     <div class="col-md-5 col-sm-5 col-xs-12">
      <label class="control-label">Document Type <span style="color: #a94442;">*</span></label>
      <?php $documentsTitle = FunctionManager::quoteCommunicationDocument();?>
      <select class="form-control notranslate" name="document_type_1" id="document_type_1">
        <option value="">Select Document Type</option>
        <?php foreach($documentsTitle as $key=>$value){
          echo "<pre>"; print_r($value); die(); ?>
        <option value="<?php echo $key?>"><?php echo $value;?></option>
        <?php } ?>
      </select>
    </div>
    
   <!--  <div class="col-md-5 col-sm-5 col-xs-12">
      <label class="control-label" style="padding-top: 14%;">&nbsp;
      <input type="checkbox" name="document_status_1" id="document_status_1" / >Approve
      </label>
    </div> -->
    
    <div class="col-md-1 col-sm-1 col-xs-12 text-right"><br />
    <button id="btn_upload_1" class="btn btn-info submit-btn" onclick="uploadDocument(1,event);" style="margin-top: 8px;">Upload</button>
</div>
</div><div class="clearfix"></div><br /><br />
<div id="document_list_cont">
  <?php 
  $this->renderPartial('_documents',array('quote_id'=>$quote_id,'documentList'=>$documentList));?>
</div>
           


