<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-left: 0px;">
  <form class="form-horizontal form-label-left input_mask">
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea class="form-control" readonly="readonly" placeholder="Notes/comments for the quote" name="notes" id="notes"><?php if (isset($quote['notes']) && !empty($quote['notes'])) echo $quote['notes']; ?></textarea>
          </div>
	  </div>

      <?php
        $existing_items_found = false;
        if (isset($quote_details) && is_array($quote_details) && count($quote_details))
            $existing_items_found = true;
      ?>

      <?php
        $total_price = 0;
        if (isset($quote_details) && is_array($quote_details) && count($quote_details))
        {
      ?>

          <?php
            foreach ($quote_details as $quote_detail)
            {
                if (!isset($quote_detail['quantity']) || empty($quote_detail['quantity'])) $quote_detail['quantity'] = 1;
                if (!isset($quote_detail['unit_price']) || empty($quote_detail['unit_price'])) $quote_detail['unit_price'] = 0;
          ?>

                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-3 date-input">
                        <label class="control-label">Product Name</label>
                        <input type="text" class="form-control has-feedback-left" name="product_name[]" readonly="readonly"
                            <?php if (isset($quote_detail['product_name']) && !empty($quote_detail['product_name'])) echo 'value="' . $quote_detail['product_name'] . '"'; ?> >
                        <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <label class="control-label">Quantity</label>
                        <input style="text-align: right;" type="text" class="qty form-control" name="quantity[]" readonly="readonly"
                            <?php if (isset($quote_detail['quantity']) && !empty($quote_detail['quantity'])) echo 'value="' . $quote_detail['quantity'] . '"'; ?> >
                    </div>

                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <label class="control-label">UOM</label>
                        <input style="text-align: right;" type="text" class="form-control" name="uom[]" readonly="readonly"
                            <?php if (isset($quote_detail['uom']) && !empty($quote_detail['uom'])) echo 'value="' . $quote_detail['uom'] . '"'; else echo 'Units'; ?> >
                    </div>

                   <!--  <div class="col-md-1 col-sm-1 col-xs-4">
                        <label class="control-label">Unit Price</label>
                        <input style="text-align: right;" type="text" class="form-control" name="price[]" readonly="readonly" placeholder="Current Unit Price (If Applicable)"
                            <?php if (isset($quote_detail['price']) && !empty($quote_detail['price'])) echo 'value="' . $quote_detail['price'] . '"'; ?> >
                    </div> -->


                </div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label class="control-label">Product Notes</label>
                        <input type="text" class="form-control" name="vendor_product_notes[]" placeholder="Product Notes"
                            <?php if (isset($quote_detail['vendor_product_notes']) && !empty($quote_detail['vendor_product_notes'])) echo 'value="' . $quote_detail['vendor_product_notes'] . '"'; ?> >
                    </div>
                </div>

                <div class="form-group">

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label class="control-label">New Unit Price</label>
                        <input  type="text" class="price_calc product_unit_price form-control" id="unit_price"  name="unit_price[]" placeholder="Price Per Unit"
                            <?php if (isset($quote_detail['unit_price']) && !empty($quote_detail['unit_price'])) echo 'value="' . $quote_detail['unit_price'] . '"'; ?> >
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label class="control-label">Tax Rate %</label>
                        <input  type="text" class="tax_calc product_tax_rate form-control" id="tax_rate" data-value="<?php echo $quote_detail['quantity']; ?>"  name="product_tax_rate[]"  placeholder="Tax Rate %"
                            <?php if (isset($quote_detail['product_tax_rate']) && !empty($quote_detail['product_tax_rate'])) echo 'value="' . $quote_detail['product_tax_rate'] . '"'; ?> >
                    </div>

                </div>

                <div class="form-group">

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label class="control-label">Shipping</label>
                        <input   type="text" class="shipping_calc product_shipping_charges form-control" id="shipping_charges" name="product_shipping_charges[]" placeholder="Shipping Charges"
                            <?php if (isset($quote_detail['product_shipping']) && !empty($quote_detail['product_shipping'])) echo 'value="' . $quote_detail['product_shipping'] . '"'; ?> >
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label class="control-label">Other Charges</label>
                        <input type="text" class="other_calc product_other_charges form-control" id="other_charges" name="product_other_charges[]" placeholder="Other Charges"
                            <?php if (isset($quote_detail['product_other_charges']) && !empty($quote_detail['product_other_charges'])) echo 'value="' . $quote_detail['product_other_charges'] . '"'; ?>>
                    </div>

                </div>

                <?php
                if (isset($quote_detail['unit_price']) && !empty($quote_detail['unit_price'])){

                    $total_unit_price = $quote_detail['unit_price']*$quote_detail['quantity'];
                    $product_tax_rate_price = ($quote_detail['product_tax_rate']/100)*$total_unit_price;

                    $product_total_price = $total_unit_price+$product_tax_rate_price+$quote_detail['product_shipping']+$quote_detail['product_other_charges'];
                }

                ?>


                <div class="form-group">

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        &nbsp;
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <label class="control-label">Product Total Price</label>
                        <input style="text-align: right;" type="text" class="form-control product_total_price"  id="product_total_price" name="product_total_price[]" placeholder="Product Total Price" <?php if (isset($product_total_price) && !empty($product_total_price)) echo 'value="' . $product_total_price . '"'; ?> >
                    </div>


                </div>

                <div class="clearfix"> <br /> </div>

                <?php
                $total_price += $quote_detail['unit_price'];
            }
        }
      ?>

    <div class="clearfix"> </div>
   <div style="float: right; width:75%;">
    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-8">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4 text-right">
      		Tax Amount
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4">
            <input type="text" class="price_calc form-control text-right" name="tax_rate" id="tax_rate" readonly="readonly"
                <?php if (isset($quote['tax_rate']) && !empty($quote['tax_rate'])) echo 'value="' . $quote['tax_rate'] . '"'; else echo 'placeholder="Tax Rate"'; ?> >
      	</div>
    </div>

    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-8">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4 text-right">
      		Shipping Charges
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4">
            <input type="text" class="price_calc form-control text-right" name="shipping" id="shipping" readonly="readonly"
                <?php if (isset($quote['shipping']) && !empty($quote['shipping'])) echo 'value="' . $quote['shipping'] . '"'; else echo 'placeholder="Shipping"'; ?> >
      	</div>
    </div>

    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-8">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4 text-right">
      		Other Charges
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4">
            <input type="text" class="price_calc form-control text-right" name="other_charges" id="other_charges" readonly="readonly"
                <?php if (isset($quote['other_charges']) && !empty($quote['other_charges'])) echo 'value="' . $quote['other_charges'] . '"'; else echo 'placeholder="Other Charges"'; ?> >
      	</div>
    </div>

    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-8">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4 text-right">
      		Discount
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4">
            <input type="text" class="price_calc form-control text-right" name="discount" id="discount" readonly="readonly"
                <?php if (isset($quote['discount']) && !empty($quote['discount'])) echo 'value="' . $quote['discount'] . '"'; else echo 'placeholder="Discount"'; ?> >
      	</div>
    </div>

    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-8">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4 text-right">
      		Total Price
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-4">
            <input type="text" class="form-control text-right" name="total_price" id="total_price" readonly="readonly"
                <?php if (isset($quote['total_price']) && !empty($quote['total_price'])) echo 'value="' . $quote['total_price'] . '"'; else echo 'placeholder="Total Price"'; ?> >
      	</div>
    </div>
   </div>

    <div class="clearfix"> <br /> </div>
</form>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-6">
            <h5>Vendor Documents</h5>
        </div>
    </div>
    <div class="clearfix"> </div>


    <?php
    // integer starts at 0 before counting
    $your_existing_files = array();
    $upload_dir = 'uploads/quotes/';
    if ($quote_id)
    {
        if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
        if (!is_dir('uploads/quotes/' . $quote_id))
            mkdir('uploads/quotes/' . $quote_id);
        if (!is_dir('uploads/quotes/' . $quote_id . '/' . $vendor_id))
            mkdir('uploads/quotes/' . $quote_id . '/' . $vendor_id);
        $upload_dir = 'uploads/quotes/' . $quote_id . '/' . $vendor_id;
        if ($handle = opendir($upload_dir)) {
            while (($uploaded_file = readdir($handle)) !== false){
                if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file))
                    $your_existing_files[] = $uploaded_file;
            }
        }
    }
    ?>

    <?php
    if (is_array($your_existing_files) && count($your_existing_files)) { ?>

        <div class="col-md-6 col-sm-6 col-xs-6">
            <table class="table">
                <?php $file_idx = 1; foreach ($your_existing_files as $uploaded_file) { ?>
                    <tr id="existing_file_id_<?php echo $file_idx; ?>">
                        <td>
                            <a href="<?php echo AppUrl::bicesUrl('uploads/quotes/' . $quote_id . '/' .$vendor_id .'/'. $uploaded_file); ?>" target="quote_file" style="color: #fff;">
                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true" style="color: #000;"></span>
                            </a>
                            <?php if (!isset($quote['submit_status']) || $quote['submit_status'] == 0) { ?>
                                &nbsp;
                                <a id="delete_quote_file_link_<?php echo $file_idx; ?>" style="cursor: pointer; color: #fff;"
                                   onclick="deleteQuoteFile(<?php echo $file_idx; ?>, '<?php echo $uploaded_file; ?>');">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </a>
                            <?php } ?>
                        </td>
                        <td>
                            <?php echo $uploaded_file; ?>
                        </td>
                    </tr>
                    <?php $file_idx += 1; } ?>
            </table>
        </div>

        <div class="clearfix"> </div>

    <?php } ?>

    <div role="main">
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h5>Quote Questtionnaire</h5>
            </div>
        </div>
        <div class="clearfix"> </div><br/>
        <div class="form-group">
            <?php
            $i = 1;
            if (isset($quote_questions) && is_array($quote_questions))
            {
                foreach ($quote_questions as $quote_question)
                {
                    $quoteAnswers = new QuoteAnswer();
                    $quoteVendorAnswers = new QuoteVendorAnswer();
                    $quoteAnswers = $quoteAnswers->getQuoteAnswers($quote_question['id']);
                    $quoteVendorAnswers = $quoteVendorAnswers->getQuoteVendorAnswers($quote_question['quote_id'],$quote_question['id'],$vendor_id);

                    ?>


                    <div class="col-md-5 col-sm-5 col-xs-10" style="margin-bottom: -12px;">

                        <input type="text" class="form-control" name="question_<?php echo $i; ?>" readonly="readonly" value="<?php echo $quote_question['question']; ?>"><br/>

                    </div>

                    <div class="clearfix"></div>

                    <?php  $j=1;
                    foreach ($quoteAnswers as $quoteAnswer)
                    { ?>

                        <div class="col-md-3 col-sm-3 col-xs-6" id="free_text_<?php echo $i ?>" <?php if($quote_question['question_type']=='yes_or_no' || $quote_question['question_type']=='multiple_choice') { ?> style="display: none;" <?php } ?> >

                            <div style="float: left;width: 200px;margin-right: 10px;">
                                <textarea type="text" class="form-control" placeholder="Answer" name="free_text_answer_<?php echo $i; ?>"  id="free_text_answer_<?php echo $i; ?>" style="height: 100px;width: 430px;"><?php echo $quoteVendorAnswers['free_text']; ?></textarea>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-3 col-sm-3 col-xs-6" id="yes_no_area_<?php echo $i ?>" <?php if($quote_question['question_type']=='free_text' || $quote_question['question_type']=='multiple_choice') { ?> style="display: none;" <?php } ?> >

                            <select id="yes_or_no_<?php echo $i ?>" name="yes_or_no_<?php echo $i ?>" class="form-control">
                                <option value="">Select Yes or No</option>
                                <option <?php if (isset($quoteVendorAnswers['yes_no']) && $quoteVendorAnswers['yes_no'] == 'yes') echo ' selected="SELECTED" '; ?> value="yes">Yes</option>
                                <option <?php if (isset($quoteVendorAnswers['yes_no']) && $quoteVendorAnswers['yes_no'] == 'no') echo ' selected="SELECTED" '; ?> value="no">No</option>
                            </select>

                        </div>
                        <div class="clearfix"></div>
                        <?php if($j==1) { ?>
                        <div class="col-md-6 col-sm-6 col-xs-12" id="answer_area_<?php echo $i; ?>_<?php echo $j; ?>" <?php if($quote_question['question_type']=='free_text' || $quote_question['question_type']=='yes_or_no') { ?> style="display: none;" <?php } ?>>

                            <select id="quote_answer_<?php echo $i; ?>" name="quote_answer_<?php echo $i; ?>" class="form-control" style="width: 430px;">
                                <option value="">Select Multiple Choice Answer</option>
                                <?php
                                foreach ($quoteAnswers as $quoteAnswer)
                                { ?>
                                    <option <?php if (isset($quoteVendorAnswers['quote_answer']) && $quoteVendorAnswers['quote_answer'] == $quoteAnswer['id']) echo ' selected="SELECTED" '; ?> value="<?php echo $quoteAnswer['id']; ?>"><?php echo $quoteAnswer['answer']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix"></div><br/>
                    <?php }
                        $j++;
                    }

                    $i++;
                }
            } ?>
        </div>
    </div>

</div>
