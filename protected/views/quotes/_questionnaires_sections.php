<div class="row-fluid tile_count">
  <div class="col-md-12">
    <h4 class="heading mt-15">Scoring Analysis</h4>
    <h4 class="subheading">The overall weighted scores by supplier and questionnaire</h4><br />
  </div>
  <div class="col-md-12">
    <?php $this->renderPartial('_scoring_criteria',array('quote'=>$quote));?>
  </div>
  <div class="clearfix"> </div>
  <div class="col-md-12">
   <h4 class="heading"><br />Supplier Questionnaire Responses</h4>
   <h4 class="subheading"><br>Expand the below tables to see the answers given by suppliers against each questionnaire</h4>
  <div class="table-responsive">
    <?php
    $quoteQuestionnaire = new QuestionnaireForm();
    $quoteQuestionnaire = $quoteQuestionnaire->getQuoteQuestions($quote_id);
    $ctr = 0;
    foreach ($quoteQuestionnaire as $quoteForm) {
      $ctr += 1;
      $parentRow = $quoteForm['id'] . '_' . $quoteForm['question_form_id'] . '_' . $ctr; ?>
      <table class="table table-striped mt-26 quote_user_scoring" style="width: 100%">
        <thead>
          <tr data-node="treetable-<?= $parentRow; ?>" class="parent_row">
            <td style="width:40%"><b><?= $quoteForm['question_form_name'] ?></b></td>
            <td style="width:40%"><b>Supplier Answers</b></td>
            <td style="width:20%"><b>(Weighted Overall Score = <?= $quoteForm['question_scorer'] ?> %)</td>
            <td style="width:10%"></td>
          </tr>
        </thead>
        <tbody>
        <?php if (!empty($quoteForm['id'])) {
        $resQuestAndAnswer = CommonFunction::getVendorQuestion($quote_id, $quoteForm['question_form_id']);
        if (!empty($resQuestAndAnswer)) {
          foreach ($resQuestAndAnswer as $questAndAnswer) {?>
            <tr data-pnode="treetable-parent-<?= $parentRow; ?>" style="background-color: rgba(77, 144, 254, 0.47);">
              <td style="width:40%"><b><?= str_replace('"', '',$questAndAnswer['questions']); ?></b></td>
              <td style="width:40%"></td>
              <td style="width:10%"></td>
              <td style="width:10%"></td>
            </tr>
            <?php if ($questAndAnswer['answer'] != "") {
              $data = [
                'questions' => $questAndAnswer['questions'], 'quoteID' => $quote_id,
                'questionFormId' => $quoteForm['question_form_id'], 
                'vendorID' => $questAndAnswer['vend_id']
              ];
              $vendorRender = CommonFunction::getVendorNameAndAnswers($data);
            
              foreach ($vendorRender as $vValue) { ?>
                <tr data-pnode="treetable-parent-<?= $parentRow; ?>" style="background: rgba(77, 144, 254, 0.01) !important;">
                  <td class="pt-15" style="width:40%; font-weight: 600;"><?= $vValue['vendor_name']; ?></td>
                  <td class="pt-15 notranslate" style="width:40%; font-weight: 600;">
                   <?php
                   if($vValue['que_ans_type'] == 'select'){
                      $values = explode(',', $vValue['vend_answer']); 
                      $displayVal = implode('<br>', $values);
                      echo "<p class='p-0 notranslate'>".$displayVal."</p>";
                   } 
                   else{ echo "<p class='p-0 notranslate'>".str_replace('"', '',$vValue['vend_answer'])."</p>"; } ?>
                   </td>
                  <td style="width:10%;">
                  <div class="input-group m-0">
                   <div class="input-group-addon" style="border-top-left-radius: 10px !important;  border-bottom-left-radius: 10px !important; background-color: #ddd7d7;"><b>%</b></div>
                    <input type="number" class="form-control" value="<?= number_format($vValue['total_score_answer'] / $vValue['all_scorers'],2); ?>" readonly
                    style="border-top-left-radius: 0px !important; border-bottom-left-radius: 0px !important;" />
                   </div>
                  </td>
                  <td style="width:10%"></td>
                </tr>
            <?php }
            }
          }
        } }  ?>
        </tbody>
      </table>
    <?php } ?>
  </div>
</div>
</div>