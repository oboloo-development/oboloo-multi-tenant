  <?php
    $deadline = "";
    if (isset($quote_id) && !empty($quote_id)) {
      $sql = "select contribute_deadline from quote_user where quote_id=" . $quote_id . " group by quote_id ";
      $res = Yii::app()->db->createCommand($sql)->queryRow();
      if(date('Y', strtotime($res['contribute_deadline'])) != "1970"){
        $deadline = date("Y-m-d H:i:s", strtotime($res['contribute_deadline']));
      }
  } 
  ?>                      

<div class="modal fade" id="completeContribution" tabindex="-1" role="dialog" aria-labelledby="completeContribution" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <form method="POST" action="<?php echo AppUrl::bicesUrl('quotes/completeContribution'); ?>" autocomplete="off"  >
         <input type="hidden" name="quote_id" value="<?= $quote_id ?>" />
         <input type="hidden" name="user_id" value="<?= $user_id ?>" />
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Complete Contribution</h4>
          </div>
          <div class="modal-body p-5">
            <h5 class="panel-group" style="line-height: 20px;">Are you sure you want to finish contributing to this sourcing activity?</br> You will not be able to make any further changes.</h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            <button type="submit" class="btn btn-primary">Yes</button>
          </div>
         </div>
        </form>
      </div>
</div>

<div class="modal fade" id="showAllContributors" tabindex="-1" role="dialog" aria-labelledby="showAllContributors" aria-hidden="true">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Manage Contributors</h4>
          </div>
          <div class="clearfix"> <br /></div>
          <div class="clearfix"> </div>
          <div class="modal-body p-5">
          <div class="col-md-12">
            <button type="button" class="btn sunshine pull-right mt-15 mb-45" id="updateContributionDeadlineshow">Change Contribution Deadline</button>
            <button type="button" class="btn submit-btn1 pull-right mt-15 mb-45 text-white" id="addContribution">Add New Contributor</button>
          </div>

            <!-- START: Update contributions date -->
            <div id="updateContributionDeadlineOpen" >
             <div class="panel panel-default">
              <div class="panel-body">
               <form method="POST" action="<?= AppUrl::bicesUrl('quotes/updateContributionDeadline'); ?>" >
                  <input type="hidden" name="quote_id" value="<?= $quote_id ?>" />
                  <div class="col-lg-6 col-md-6 col-xs-12 form-group px-3 py-2 text-primary">
                      <label class="text-black">Please select a deadline (UTC) for contributions below <span class="text-danger">*</span></label>
                      <div class="form-group"></br />
                        <input type="datetime-local" class="form-control" name="contribute_deadline" id="contribute_deadline" value="<?= $deadline ?>">
                        <span id="error-msg" style="color: red;"></span>
                      </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-xs-12 form-group px-3 py-2 text-primary "></br />
                      <button type="submit" class="btn submit-btn1 mt-15 text-white">Save</button>
                  </div>
               </form>
              <div class="col-md-12">
                <button type="button" class="btn btn-danger pull-right mt-15" id="updateContributionDeadlineClose">Cancel</button>
              </div>
              </div>
             </div>
            </div>
            <!-- END: Update contributions date -->

            <!-- START: Invite Internal Contributors -->
            <div id="addContributionOpen" >
             <div class="panel panel-default">
              <div class="panel-body">
                <form class="internal_contributors_form" method="post" action="<?php echo AppUrl::bicesUrl('quotes/addMoreContributors'); ?>">
                <input type="hidden" name="quote_id" value="<?= $quote_id ?>" />
                  <div class="row p-5">
                    <div class="col-lg-12 col-xs-12">
                    <h5 class="heading">Invite Internal Contributors</h5><br/>
                      <div class="row m-0 ">
                          <div class="col-lg-9 col-xs-12">
                            <label class="form-label">Select Users<span class="text-danger">*</span></label><br />
                            <?php
                            $query = "select user_id from quote_user where quote_id=".$quote_id;
                            $contrubeArr = Yii::app()->db->createCommand($query)->queryAll();
                            $existContrube = [];
                            foreach($contrubeArr as $Vuser){
                              $existContrube[] = $Vuser['user_id'];
                            }
                            $contrubeimplode = implode(',', $existContrube);
                            if(!empty($contrubeimplode)){
                              $sql = "select user_id,full_name from users  where 1 and user_id not in(2,3," . $contrubeimplode . ") order by full_name asc ";
                            }else{
                              $sql = "select user_id,full_name from users  where 1 and user_id not in(2,3) order by full_name asc ";
                            }
		                        $users = Yii::app()->db->createCommand($sql)->query()->readAll();
                            foreach($users as $user){?>
                               <input type="checkbox" name="quote_internal_user[]" value="<?= $user['user_id'] ?>" id="user_<?= $user['user_id'] ?>" class="userCheckbox" >
                               <label for="<?= $user['user_id'] ?>" style="margin-left: 10px; font-weight: 100;"><?= $user['full_name'] ?></label><br>
                            <?php } ?>
                          </div>
                      </div>
                      <?php
                      if(empty($deadline)){ ?> 
                        <br />
                        <br />
                          <div class="col-lg-6 col-md-6 col-xs-12 form-group px-3 py-2 text-primary">
                              <label class="text-black">Please select a deadline (UTC) for contributions below <span class="text-danger">*</span></label>
                              <div class="form-group"></br />
                                <input type="datetime-local" class="form-control" name="contribute_deadline" id="contribute_deadline" required />
                                <span id="error-msg" style="color: red;"></span>
                              </div>
                          </div>
                      <?php } ?>
                      <div class="col-lg-12 col-md-12 col-xs-12 form-group px-3 py-2 text-primary mt-15">
                      <button class="btn submit-btn1 btn-sm pull-right contributor_submit text-white disabled"  type="submit"> Save</button>
                      </div></br>
                    </div>
                  </div>
                </form>
                <div class="col-md-12">
                 <button type="button" class="btn btn-danger pull-right mt-15" id="removeContribution">Cancel</button>
                </div>
              </div>
             </div>
             
            </div>
            <!-- END: Invite Internal Contributors -->
            <div class="clearfix"></div>
                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Contributor Name</th>
                        <th></th>
                        <th class="text-center">Status</th>
                      </tr>
                    </thead>
                    <tbody id="contributor_table"></tbody>
                  </table>
                </div>
             </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
         </div>
      </div>
</div>


