<div class="custom-scoring-remove">  
<?php $scoreCtr = 1;
foreach($maxQuestion as $key => $customScore){ ?>
    
    <div class="custom-scoring-all col-md-12 custom_scoring_<?= $scoreCtr ?>" id="custom_scoring_<?= $scoreCtr ?>">
      <div class="form-group">
        <div class="col-md-8 col-sm-8 col-xs-12" style="padding-right: 21px; padding-left: 0px;">
          <input type="text" class="form-control custom_scoring_<?= $scoreCtr; ?> notranslate" id="custom_title_<?= $scoreCtr; ?>" name="custom_title[<?= $scoreCtr ?>]" 
         value="<?= $customScore['scoring_title'] ?> " style="padding:5px;">
        </div>
        <div class="col-md-3 col-sm-5 col-xs-6" style="width: 100px !important; padding-left: 0px; padding-right: 16px;"> 
         <input type="text" name="custom_percentage[<?= $scoreCtr ?>]" value="<?= $customScore['score'] ?>"
         class="form-control  text-center notranslate score-criteria" 
         onkeypress="return isNumberKey(event)" onchange="maxScoring(this.value)" required style="padding:5px;">
        </div>
        <div class="col-md-3 col-sm-5 col-xs-6" style="width: 100px !important; padding-left: 0px; padding-right: 16px;"> 
            <a href="javascript:void(0)" class="btn btn-danger" 
             onclick="removeCustomScoreContainer(<?= $scoreCtr ?>)">Delete</a>
        </div>
        <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
           <h4>
            <div class="pull-left" style="font-size: 14px; padding-top: 5px;">
             <a style="cursor: pointer; text-decoration: underline;" 
                onclick="addCustomQuestionnaire(<?= $scoreCtr ?>)"> Add Questionnaire</a>
                Related To This Scoring criteria <?= $scoreCtr ?>
            </div> 
           </h4>
          </div>
        </div>
      </div>
    <div class="question_custom_score_cont_<?= $scoreCtr; ?>">
     <?php $getCustomQuestion = CommonFunction::getCustomQuestion($customScore['vendor_scoring_id'], $quote_id);
          $questionCtr = 1;
          foreach($getCustomQuestion as $question){ 
            $questionsID =  $scoreCtr.'_'.$questionCtr; ?>
            <div class="form-group" id="question_area_custom_<?= $questionsID ?>">
                <div class="col-md-8 col-sm-8 col-xs-10">
                    <label class="control-label">Question for the supplier submitting quote</label>
                    <input type="text" class="form-control quote_question_custom_<?= $scoreCtr ?> notranslate" placeholder="Question for the supplier submitting quote" name="quote_question_custom_<?= $questionsID ?>" id="quote_question_custom_<?=$questionsID ?>" 
                    value="<?= $question['question']; ?>" />
                </div>
                <div class="col-md-4 col-sm-4 col-xs-2" style="margin-top: 33px;">
                    <a class="col-md-4" onclick="removeCustomQuestion(<?= $scoreCtr; ?>, <?= $questionCtr; ?>);" 
                     title="Click here to delete this question" 
                     style="cursor: pointer;"><span class="fa fa-minus fa-2x"></span>
                    </a>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-8 col-sm-8 col-xs-4">
                 <label class="control-label">Question Type</label>
                  <select id="question_type_custom_<?= $questionsID ?>" name="question_type_custom_<?= $questionsID ?>" class="form-control notranslate" onchange="selectQuestionTypeCustom(this.value, <?= $scoreCtr; ?>, <?= $questionCtr; ?>);">
                   <option value="free_text" <?php echo $question['question_type'] == 'free_text' ? 'selected' : ''; ?>>Free Text</option>
                   <option value="yes_or_no" <?php echo $question['question_type'] == 'yes_or_no' ? 'selected' : ''; ?>>Yes or No</option>
                   <option value="multiple_choice" <?php echo $question['question_type'] == 'multiple_choice' ? 'selected' : ''; ?>>Multiple Choice</option>
                 </select>
                 </div>
                <div class="clearfix"> </div>
                <!-- this answer_section_custom_id for to append new answer to this question -->
                <div id="answer_section_custom_id_<?= $questionsID ?>">
                <?php  $getCustomAnswer = CommonFunction::getDefaultAnswer($question['id'], $quote_id);
                $answerCtr = 1;
                foreach($getCustomAnswer as $customAnswer){
                $answer = $scoreCtr.'_'.$questionCtr.'_'.$answerCtr; ?>
                  <div id="answer_section_custom_<?= $answer ?>" >
                    <div class="form-group notranslate" id="answer_area_custom_<?= $answer ?>">
                     <div class="col-md-2 col-sm-2 col-xs-2" style="margin-top: 25px;">
                        <a class="col-md-4 hidden_first_delete_option" 
                        onclick="deleteAnswerOfCustomQuestion(<?= $scoreCtr ?>, <?= $questionCtr; ?>, <?= $answerCtr ?>)"
                          title="Click here to delete this answer" style="cursor: pointer;"><span class="fa fa-minus fa-2x"></span>
                        </a>&nbsp;
                        <a class="col-md-2 text-left" onclick="addQuestionAnswerCustom(<?= $scoreCtr ?> , <?= $questionCtr?>);" title="Click here to add another answer"  style="cursor: pointer;"> <span class="fa fa-plus fa-2x"></span>
                        </a>
                      </div>
                      <div class="col-md-10 col-sm-10 col-xs-12">
                        <div>&nbsp;</div>
                          <div style="float: left;width: 200px;margin-right: 10px;">
                           <input type="text" class="form-control notranslate quote_answer_custom_answer_<?= $scoreCtr ?>_<?= $questionCtr ?> quote_answer_custom_<?= $answer ?>" 
                           id="quote_answer_custom_<?= $answer ?>"; 
                           placeholder="Answer" name="quote_answer_custom_<?= $answer ?>" value="<?= $customAnswer['answer']; ?>" />
                          </div>
                      </div>
                    </div>
                   <div class="clearfix"></div>  
              </div>
              <div class="clearfix"> </div>
             <?php $answerCtr++; } ?>
             </div>
            </div><div class="clearfix"> </div>
           <?php $questionCtr++; } ?>
          </div><div class="clearfix"> </div>
         </div>
        <?php  $scoreCtr++; } ?>
 </div>
 <div class="clearfix"> </div>
