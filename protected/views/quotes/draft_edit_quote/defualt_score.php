            <div id="default_scoring_container">
              <input type="hidden" name="default_score_flag" id="default_score_flag" value="0" />
              <div class="row">
                <div class="col-md-8 col-sm-6 col-xs-6" style="padding-left: 30px;">
                  <h5><b>Title</b></h5>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <h5><b>Score (%)</b></h5>
                </div>
                <div class="clearfix"><br></div>
              </div>
              <?php $i = 1;
              foreach ($defaultScoringCriteria as $value) {?>
                <div class="form-group">
                  <div class="col-md-8 col-sm-6 col-xs-6">
                    <input type="text" class="form-control notranslate" name="score_title[<?php echo $value['id']; ?>]" readonly="readonly" value="<?php echo $value['value']; ?>" style="padding:5px;">
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-6" style="width: 100px !important;">
                    <input type="text" name="score_percentage[<?php echo $value['id']; ?>]" class="form-control text-center notranslate score-criteria" placeholder="Enter Scoring Percentage" onkeypress="return isNumberKey(event)" value="<?php echo $value['score']; ?>" onchange="maxScoring(this.value)" required style="padding:5px;">
                  </div>

                </div>
                <div class="form-group">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <h4>
                      <div class="pull-left" style="font-size: 12px !important; padding-top: 5px;">
                        <a style="cursor: pointer; text-decoration: underline;" onclick="addDefaultQuestion(<?= $value['id']; ?>)">
                        Add Questionnaire</a>
                        Related To This Scoring criteria
                      </div>
                    </h4>
                  </div>
                </div>

                 <?php $getDefaultQuestion = CommonFunction::getDefaultQuestion($value['id'], $quote_id);;
                if(!empty($getDefaultQuestion)) {
                  $questionCtr = 1;
                  foreach($getDefaultQuestion as $question){ ?>
                    <div id="new_question_code_<?= $value['id']; ?>_<?= $questionCtr; ?>" class="new_question_code">
                        <div class="form-group" id="question_area_<?= $value['id']; ?>_<?= $questionCtr; ?>">
                          <div class="col-md-8 col-sm-8 col-xs-10">
                            <input type="text" class="form-control quote_question_<?= $value['id']; ?> notranslate" placeholder="Question for the supplier submitting quote" name="quote_question_<?= $value['id']; ?>_<?= $questionCtr; ?>" value="<?=$question['question']?>" />
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-2" style="margin-top:7px;">
                            <a class="col-md-4 col-sm-4 col-xs-2" 
                            onclick="deleteDefaultQuestion(<?= $value['id']; ?>,<?= $questionCtr; ?>);" title="Click here to delete this question" style="cursor: pointer;">
                              <span class="fa fa-minus fa-2x"></span>
                            </a>
                          </div>
                          <div class="clearfix"> </div>
                          <div class="col-md-8 col-sm-8 col-xs-6">
                            <label class="control-label">Question Type</label>
                            <select id="question_type_<?= $value['id']; ?>_<?= $questionCtr; ?>" 
                            name="question_type_<?= $value['id']; ?>_<?= $questionCtr; ?>"
                            onchange="selectDefaultType(this.value, <?= $value['id']; ?>, <?= $questionCtr; ?>)" 
                            class="form-control notranslate" >
                              <option value="free_text" <?= $question['question_type'] == 'free_text' ? 'selected' : ''; ?>>Free Text</option>
                              <option value="yes_or_no" <?= $question['question_type'] == 'yes_or_no' ? 'selected' : ''; ?>>Yes or No</option>
                              <option value="multiple_choice" <?= $question['question_type'] == 'multiple_choice' ? 'selected' : ''; ?>>Multiple Choice</option>
                            </select>
                          </div>
                          <div class="clearfix"> </div>
                        </div>
                        <div  id="add_new_answer_area_<?= $value['id']; ?>_<?= $questionCtr; ?>"  style="margin-bottom: 27px;">
                        <?php $getDefaultAnswer = CommonFunction::getDefaultAnswer($question['id'], $quote_id);
                        $answerCtr=1;
                        foreach($getDefaultAnswer as $answer){ ?>
                          <div id="new_answer_area_<?= $value['id']; ?>_<?= $questionCtr; ?>_<?= $answerCtr ?>">
                            <div class="form-group" id="answer_area_ans_<?= $value['id']; ?>_<?= $questionCtr; ?>_<?= $answerCtr ?>">
                              <div class="col-md-2 col-sm-2 col-xs-2" style="margin-top: 25px;">
                                <a class="col-md-4" 
                                onclick="removeDefAnswer(<?= $value['id']; ?>,<?= $questionCtr; ?>,<?= $answerCtr ?>);" title="Click here to delete this answer" style="cursor: pointer;">
                                  <span class="fa fa-minus fa-2x"></span>
                                </a>
                                &nbsp;
                                <a class="col-md-2 text-left" 
                                onclick="addDefAnswer(<?= $value['id']; ?>,<?= $questionCtr; ?>,<?= $answerCtr ?>);" title="Click here to add another answer" style="cursor: pointer;">
                                  <span class="fa fa-plus fa-2x"></span>
                                </a>
                              </div>
                              <div class="col-md-10 col-sm-10 col-xs-12">
                                <div>&nbsp;</div>
                                <div style="float: left;width: 200px;margin-right: 10px;">
                                  <input type="text" class="form-control notranslate" placeholder="Answer" 
                                  name="quote_answer_<?= $value['id']; ?>_<?= $questionCtr; ?>_<?= $answerCtr ?>" 
                                  id="quote_answer_<?= $value['id']; ?>_<?= $questionCtr; ?>_<?= $answerCtr; ?>" 
                                  value="<?= $answer['answer']; ?>" />
                                </div>
                                <div class="clearfix"></div>
                              </div>
                              </div>
                            </div>
                          
                        <?php $answerCtr++;} ?>
                      </div></div>
                    <?php  $questionCtr++;} } ?>

                  <!-- New Questions -->
                <div id="new_question_area_<?= $value['id']; ?>"></div>
                <?php  }?>
            </div>

            
<script>   
var queId = ansId = 0;
function addDefaultQuestion(scoreId){
    var qExp = $('.quote_question_'+scoreId).last().attr('name');
     if(qExp != undefined){
      qExp=qExp.split('_');
     if(parseInt(qExp[3])>0){
       queId=parseInt(qExp[3])+1;
      }else{
        queId = 1;
      }}else{
        queId = 1;
      }
    var newQuestion = addQuestions(scoreId,queId);
    $(`#new_question_area_${scoreId}`).append(newQuestion);
  }
function addQuestions(questionId, queId){
    return `
      <div id="new_question_code_${questionId}_${queId}" class="new_question_code">
      <div class="form-group" id="question_area_${questionId}_${queId}">
        <div class="col-md-8 col-sm-8 col-xs-10">
          <input type="text" class="form-control quote_question_${questionId} notranslate" placeholder="Question for the supplier submitting quote"  name="quote_question_${questionId}_${queId}" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-2" style="margin-top:7px;">
          <a class="col-md-4 col-sm-4 col-xs-2" onclick="deleteDefaultQuestion(${questionId},${queId});" title="Click here to delete this question" style="cursor: pointer;">
            <span class="fa fa-minus fa-2x"></span>
          </a>
        </div>
        <div class="clearfix"> </div>
        <div class="col-md-8 col-sm-8 col-xs-6">
          <label class="control-label">Question Type</label>
          <select id="question_type_${questionId}_${queId}" onchange="selectDefaultType(${this.value}, ${questionId}, ${queId})" 
          name="question_type_${questionId}_${queId}" class="form-control notranslate" >
            <option value="free_text">Free Text</option>
            <option value="yes_or_no">Yes or No</option>
            <option value="multiple_choice">Multiple Choice</option>
          </select>
        </div>
        <div class="clearfix"> </div>
      </div>
     <div id="new_answer_area_${questionId}_${queId}"></div>
    </div>`;
  };
function selectDefaultType(selectObject, scoreID, queId){
  console.log(selectObject, scoreID, queId)
    ansId += 1; 
    var question_type = selectObject || $('#question_type_'+scoreID+'_'+queId).val();
    var questionAnswers = questionAnswer(question_type, scoreID, queId, ansId);
    $(`#new_answer_area_${scoreID}_${queId}`).html(questionAnswers);
      
  }
function questionAnswer(question_type, scoreId, queId, ansId){
    const questionsAnswersID = scoreId+'_'+queId+'_'+ansId;
    if(question_type === 'multiple_choice'){
      return `<div class="form-group" id="answer_area_ans_${questionsAnswersID}" >
                    <div class="col-md-2 col-sm-2 col-xs-2" style="margin-top: 25px;">
                      <a class="col-md-4" onclick="removeDefAnswer(${scoreId},${queId},${ansId});" title="Click here to delete this answer" style="cursor: pointer;">
                        <span class="fa fa-minus fa-2x"></span>
                      </a>
                      &nbsp;
                      <a class="col-md-2 text-left" onclick="addDefAnswer(${scoreId},${queId},${ansId});" title="Click here to add another answer" style="cursor: pointer;">
                        <span class="fa fa-plus fa-2x"></span>
                      </a>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                      <div>&nbsp;</div>
                      <div style="float: left;width: 200px;margin-right: 10px;">
                        <input type="text" class="form-control notranslate" placeholder="Answer" name="quote_answer_${questionsAnswersID}" id="quote_answer_${questionsAnswersID}" value="" />
                      </div>
                      <div class="clearfix"></div>
                    </div>
                    <div class="clearfix" id="add_new_answer_area_${scoreId}_${queId}" style="margin-bottom: 27px;"> <br />
              </div>`;
    }else if(question_type === 'free_text'){
      return ` <div id="new_answer_area_${questionsAnswersID}">
                  <div class="form-group" id="answer_area_${questionsAnswersID}" >
                  <input type="hidden" class="form-control notranslate" placeholder="Answer" name="quote_answer_${questionsAnswersID}" id="quote_answer_${questionsAnswersID}" value="free_text" /></div>
              </div>`;
    }else if(question_type === 'yes_or_no'){
      return `<div id="new_answer_area_${questionsAnswersID}">
                  <div class="form-group" id="answer_area_${questionsAnswersID}" > 
                  <input type="hidden" class="form-control notranslate" placeholder="Answer" name="quote_answer_${questionsAnswersID}" id="quote_answer_${questionsAnswersID}" value="yes_or_no" /></div>
              </div>`;
    }
  }
function deleteDefaultQuestion(questionId, queId){
    $(`#new_question_code_${questionId}_${queId}`).remove();
  }
function addDefAnswer(questionId,queId,ansId){
    ansId += 1
    var addMoreAns = addMoreAnswerInput(questionId, queId, ansId);
    $(`#add_new_answer_area_${questionId}_${queId}`).append(addMoreAns);
  }
function removeDefAnswer(questionId,queId,ansId){
    var addMoreAns = $(`#answer_area_ans_${questionId}_${queId}_${ansId}`).remove();
  }
function addMoreAnswerInput(questionId,queId,ansId) {
  const questionsAnswersID = questionId+'_'+queId+'_'+ansId;
  return `<div class="form-group" id="answer_area_ans_${questionsAnswersID}" >
            <div class="col-md-2 col-sm-2 col-xs-2" style="margin-top: 25px;">
              <a class="col-md-4" onclick="removeDefAnswer(${questionId},${queId},${ansId});" title="Click here to delete this answer" style="cursor: pointer;">
                <span class="fa fa-minus fa-2x"></span>
              </a>
              &nbsp;
              <a class="col-md-2 text-left" onclick="addDefAnswer(${questionId},${queId},${ansId});" title="Click here to add another answer" style="cursor: pointer;">
                <span class="fa fa-plus fa-2x"></span>
              </a>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
              <div>&nbsp;</div>
              <div style="float: left;width: 200px;margin-right: 10px;">
                <input type="text" class="form-control notranslate" placeholder="Answer" name="quote_answer_${questionsAnswersID}" id="quote_answer_${questionsAnswersID}" value="" />
              </div>
              <div class="clearfix"></div>
          </div>`;
 }
</script>