<?php
$dateFormate = FunctionManager::dateFormat();
$currencySymbol = FunctionManager::currencySymbol('', $quote['quote_currency']);
$disabled = FunctionManager::sandbox();
$vendorCtr = 1;
$userID = Yii::app()->session['user_id'];
$vendExist = 0;
$sql = "select * from quote_vendors qv left join vendors v on v.vendor_id = qv.vendor_id where qv.quote_id=" . $quote['quote_id'];
$vendorExistReader = Yii::app()->db->createCommand($sql)->query()->readAll();
!empty($vendorExistReader)  ? $vendExist = 1 : $vendExist = 0;

$sqlRes = 'select * from quote_user where open_for_scoring=0 and user_id=' . $userID . ' and quote_id=' . $quote['quote_id'];
$contributeUser = Yii::app()->db->createCommand($sqlRes)->queryRow();

$sqlRes = 'select count(id) as total_contributions from quote_user where open_for_scoring=0 and contribute_status !=1 and quote_id=' . $quote['quote_id'];
$allContributers = Yii::app()->db->createCommand($sqlRes)->queryRow();
// $quote['user_id'] means creator of this course
$conditions = [
  'creator' => $quote['user_id'] === $userID,
  'draft'   => $quote['quote_status'] == 'Draft',
  'checkExprie' => date("Y-m-d H:i:s") <= FunctionManager::CheckContriddlineExprie($quote_id, $userID),
  'contributeUser' => $contributeUser['quote_id'],
  'contributorCompleted' => $contributeUser['contribute_status'] == "1" && $quote['user_id'] != $userID,
  'contributorTotal' => $allContributers['total_contributions'] >0 && $quote['user_id'] == $userID,
  'checkContributorExprie' => ( date("Y-m-d H:i:s") < FunctionManager::CheckContriddlineExprie($quote_id, $userID) && $quote['user_id'] != $userID)
]; 
?>
<div class="right_col" role="main">
  <div class="row-fluid tile_count">
    <div class="clearfix"> </div>
    <?php $supplierAdded = Yii::app()->user->getFlash('success');
    if (!empty($supplierAdded)) { ?>
      <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?= $supplierAdded; ?></div>
    <?php } ?>
    <div class="clearfix"> </div>
    <?php 
    if($conditions['contributorCompleted']) {?>
      <div class="alert alert-success text-center">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong class="font-1rem">Contribution Complete</strong>
      </div>
    <?php }  ?>
    <div class="span6 pull-left">
      <?php if ($conditions['checkContributorExprie'] && FunctionManager::getCrtburelineDate($quote['quote_id'], $userID) != '01/01/1970 00:00AM') { ?>
        <h4 class="details_heading">
          <b>Contribution Deadline date</b> :
          <?php
          if (isset($quote['quote_id']) && !empty($quote['quote_id'])) echo FunctionManager::getCrtburelineDate($quote['quote_id'], $userID); ?>
        </h4>
      <?php } ?>
    </div>
    <div class="span6 pull-right span6-btn-vw">

      <!-- // START: publish button only be made visible to the esourcing creator, not to contributors -->
      <?php if ($conditions['creator'] && $conditions['draft']) { ?>
        <button type="button" onclick="publishQuote(<?= $vendExist ?>)" class="btn btn-success btn-functionality submit-btn1" <?php echo $disabled; ?>> Publish</button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inviteSuppliers" <?php echo $disabled; ?>>Add Suppliers</button>
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#showAllContributors" <?php echo $disabled; ?>>Contributors</button>

        <!-- This button only show to contribute -->
      <?php }
      if ($conditions['checkExprie'] && ($contributeUser['contribute_status'] !== '1' && !$conditions['creator'])) { ?>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#completeContribution" <?php echo $disabled; ?>>Complete Contribution</button>
      <?php } ?>
      <!-- // END: publish button only be made visible to the esourcing creator, not to contributors -->

      <a href="<?php echo AppUrl::bicesUrl('quotes/list'); ?>">
        <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;border-color: #F79820;">
          <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Return to eSourcing List
        </button>
      </a>
    </div>
    <div class="clearfix"> </div>
    <div class="row tile_count supplier-mbl-vw-tab" role="tabpanel" data-example-id="togglable-tabs">
      <div id="myTabContent" class="tab-content">
          <input type="hidden" name="quote_id" id="quote_id" value="<?= $quote_id ?>">
          <!-- START: Quote Details -->
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <h3 class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pl-0">Quote Details</h3><br /><br />
                <ul class="list-unstyled">
                  <li class="my14">
                    <strong class="me-1">Title:</strong>
                    <span><?= !empty($quote['quote_name']) ? $quote['quote_name'] : '' ?></span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Description/Notes:</strong>
                    <span><?= !empty($quote['quote_desc']) ? $quote['quote_desc'] : '' ?></span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Quote Currency:</strong>
                    <span class="label label-success">
                      <?php
                      foreach ($currency_rate as $rate) {
                        if (!empty($quote['quote_currency']) && $quote['quote_currency'] == $rate['currency']) {
                          echo $rate['currency'];
                        }
                      }
                      ?>
                    </span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Location:</strong>
                    <span>
                      <?php
                      foreach ($locations as $location) {
                        if (isset($quote['location_id']) && $location['location_id'] == $quote['location_id']) {
                          echo $location['location_name'];
                        }
                      }
                      ?>
                    </span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Department:</strong>
                    <span class="selected_department_name"></span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Category:</strong>
                    <span>
                      <?php
                      foreach ($categories as $category) {
                        if (isset($quote['category_id']) && $quote['category_id'] == $category['id']) {
                          echo $category['value'];
                        }
                      }
                      ?>
                    </span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Sub Category:</strong>
                    <span id="selectedSubcategory"></span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Spend Type:</strong>
                    
                    <span>
                      <?php
                      $spendType = isset($quote['spend_type']) ? $quote['spend_type'] : '';
                      if ($spendType == 'Goods') {
                        echo "Goods";
                      } elseif ($spendType == 'Services') {
                        echo "Services";
                      } elseif ($spendType == 'Goods & Services') {
                        echo "Goods & Services";
                      } ?>
                    </span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Opening Date & Time (UTC):</strong>
                    <span><?php if (isset($quote['opening_date']) && !empty($quote['opening_date'])) echo date($dateFormate . " H:iA", strtotime($quote['opening_date'])); ?></span>
                  </li>
                  <li class="my14">
                    <strong class="me-1">Closing Date & Time (UTC):</strong>
                    <span><?php if (isset($quote['closing_date']) && !empty($quote['closing_date'])) echo date($dateFormate . " H:iA", strtotime($quote['closing_date'])); ?></span>
                  </li>
                </ul>
                <?php if ($conditions['checkContributorExprie'] || $conditions['creator']) { 
                   if(!$conditions['contributorCompleted'] || $conditions['creator']) {?>
                <div class="col-12 text-center">
                  <button type="button" class="btn btn-success submit-btn1" data-toggle="modal" data-target="#quoteDetailEditModal">Edit</button>
                </div>
                <?php } }?>
              </div>
            </div>
          </div>
          <!-- END: Quote Details -->

          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <!-- Start: Quote Items List  -->
            <div class="panel panel-default" style="height: 380px;">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h3 class="pl-0">Quote Items</h3>
                  </div>
                  <?php if ($conditions['checkContributorExprie'] || $conditions['creator']) {
                     if(!$conditions['contributorCompleted'] || $conditions['creator']) {?> 
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <button type="button" class="btn btn-success submit-btn1 pull-right" data-toggle="modal" data-target="#myQuoteItemsEditModal">Edit</button>
                      </div>
                  <?php }} ?>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-borderd mt-15" id="quote_items_table" style="width: 100%;">
                      <thead>
                        <tr>
                          <th>Product Name</th>
                          <th>Product Code</th>
                          <th>Unit of Measure</th>
                          <th>Quantity</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- END: Quote Items  -->
          </div>
          <div class="clearfix"></div>
          <div class="col-lg-12 col-md-12 col-xs-12">
              <!-- START: Quote Documents  -->
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h3 class="pl-0">Quote Documents</h3>
                  </div>
                  <?php if ($conditions['checkContributorExprie'] || $conditions['creator']) { 
                    if(!$conditions['contributorCompleted'] || $conditions['creator']) { ?>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <button type="button" class="btn btn-success submit-btn1 pull-right munis-m10" data-toggle="modal" data-target="#myQuoteDocumentEditModal">Edit</button>
                      </div>
                  <?php }} ?>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
                    <div id="document_list">
                      <table class="table table-striped table-bordered ' style='width: 100%;">
                        <thead>
                          <tr>
                            <th>File</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id='tbody'></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END: Quote Documents  -->
          </div>
          <!-- Start: Quote Questionnaire List  -->
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <h3 class="pl-0">Questionnaire List 
                <?php if ($conditions['checkContributorExprie'] || $conditions['creator']) {
                      if(!$conditions['contributorCompleted'] || $conditions['creator']) {?>
                  <button type="button" class="btn btn-success submit-btn1 pull-right munis-m10" data-toggle="modal" data-target="#myQuoteQuestionnaireEditModal">Edit</button>
                <?php } } ?>
                </h3>
                <?php $quoteQuestionnaire = Yii::app()->db->createCommand("SELECT id, question_form_name as name,question_scorer, quote_id FROM 
                  quote_questionnaire WHERE quote_id=" . $quote["quote_id"])->queryAll(); ?>
                <table class="table mt-15">
                  <thead>
                    <tr>
                      <th>Questionnaire Name</th>
                      <th>Score (%)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($quoteQuestionnaire as $questionnaire) { ?>
                      <tr>
                        <td><?= $questionnaire['name'] ?></td>
                        <td><?= $questionnaire['question_scorer'] ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- End: Quote Questionnaire List  -->

          <!-- Start: Quote Comments List  -->
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
            <div class="panel panel-default">
              <div class="panel-body">
                <h3 class="pl-0">Comments 
                <?php if ($conditions['checkContributorExprie'] || $conditions['creator']) { 
                     if(!$conditions['contributorCompleted'] || $conditions['creator']) {?>
                  <button type="button" class="btn btn-success submit-btn1 pull-right munis-m10" data-toggle="modal" data-target="#myQuoteCommentEditModal">Edit</button>
                <?php }} ?>
                </h3>
                <!-- START: Comment and history of each user -->
                <?php $this->renderPartial('_comments', array('quote_id' => $quote_id, 'quoteCommHistory' => $quoteCommHistory)); ?>
                <!-- END: Comment and history of each user -->
              </div>
            </div>
          </div>
          <!-- End: Quote Comments List  -->

          <!-- /////////////////////////////////////////// All Edit modal of this page /////////////////////////////////// -->

          <!-- START: Quote Details Edit Modal -->
          <div class="modal fade" id="quoteDetailEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Quote Details</h4>
                </div>
                <div class="modal-body">
                  <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/quoteSaveAsDraft'); ?>">
                    <input type="hidden" name="quote_id" id="quote_id" value="<?= $quote_id ?>">
                    <div class="col-md-12 col-sm-12 col-xs-12 date-input valid">
                      <div class="form-group">
                       <label class="control-label">Title <span style="color: #a94442;">*</span></label>
                       <input required type="text" class="form-control notranslate" name="quote_name" id="quote_name" 
                       <?php if (isset($quote['quote_name']) && !empty($quote['quote_name'])) echo 'value="' . $quote['quote_name'] . '"';
                            else echo 'placeholder="Quote Name"'; ?>>
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="form-group">
                      <label class="control-label">Description/Notes</label>
                      <textarea class="form-control notranslate" name="quote_desc" id="quote_desc" <?php if (!isset($quote['quote_desc']) || empty($quote['quote_desc'])) echo 'placeholder="Quote description/notes"'; ?> style="height:200px"><?php if (isset($quote['quote_desc']) && !empty($quote['quote_desc'])) echo $quote['quote_desc']; ?></textarea>
                     </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                      <div class="form-group">
                       <label class="control-label">Quote Currency </label>
                        <select name="quote_currency" id="quote_currency" class="form-control notranslate" required>
                          <?php foreach ($currency_rate as $rate) { ?>
                            <option value="<?php echo $rate['currency']; ?>" <?php if (!empty($quote['quote_currency']) && $quote['quote_currency'] == $rate['currency']) echo ' selected="SELECTED" '; ?>>
                              <?php echo $rate['currency']; ?>
                            </option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <?php
                    $vendors_to_show = 1;
                    if (isset($quote_vendors) && is_array($quote_vendors) && count($quote_vendors))
                      $vendors_to_show = count($quote_vendors);
                    ?>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 valid">
                        <label class="control-label">Location <span style="color: #a94442;">*</span></label>
                        <select required name="location_id" id="location_id" class="form-control notranslate" onchange="loadDepartmentsForSingleLocation(0);">
                          <option value="">Select Location</option>
                          <?php foreach ($locations as $location) { ?>
                            <option value="<?php echo $location['location_id']; ?>" <?php if (isset($quote['location_id']) && $location['location_id'] == $quote['location_id']) echo ' selected="SELECTED" '; ?>>
                              <?php echo $location['location_name']; ?>
                            </option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 valid">
                        <label class="control-label">Department <span style="color: #a94442;">*</span></label>
                        <select required name="department_id" id="department_id" class="form-control notranslate">
                          <option value="">Select Department</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">Category</label>
                        <select name="category_id" id="category_id" class="form-control notranslate" onchange="loadSubcategories(0);">
                          <option value="0">Select Category</option>
                          <?php foreach ($categories as $category) { ?>
                            <option value="<?php echo $category['id']; ?>" <?php if (isset($quote['category_id']) && $quote['category_id'] == $category['id']) echo ' selected="SELECTED" '; ?>>
                              <?php echo $category['value']; ?>
                            </option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">Sub Category</label>
                        <select name="subcategory_id" id="subcategory_id" class="form-control notranslate">
                          <option value="0">Select Sub Category</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 valid">
                      <div class="form-group">
                        <label class="control-label">Spend Type</label>
                        <select name="spend_type" id="spend_type" class="form-control notranslate">
                          <option value="">Select Spend Type</option>
                          <option <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                          <option <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
                          <option <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Goods & Services') echo ' selected="SELECTED" '; ?> value="Goods & Services">Goods & Services</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
                        <label class="control-label">Opening Date & Time (UTC)<span style="color: #a94442;">*</span></label>
                        <input required type="text" class="form-control  notranslate" name="opening_date" id="opening_date" <?php if (isset($quote['opening_date']) && !empty($quote['opening_date'])) echo 'value="' . date($dateFormate . " H:iA", strtotime($quote['opening_date'])) . '"';
                                                                                                                            else echo 'placeholder="Opening Date"'; ?>>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
                        <label class="control-label">Closing Date & Time (UTC)<span style="color: #a94442;">*</span></label>
                        <input required type="text" class="form-control notranslate" name="closing_date" id="closing_date" <?php if (isset($quote['closing_date']) && !empty($quote['closing_date'])) echo 'value="' . date($dateFormate . " H:iA", strtotime($quote['closing_date'])) . '"';
                                                                                                                            else echo 'placeholder="Closing Date"'; ?>>
                      </div>
                    </div>
                    
                  </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
              </div>
            </div>
          </div>
          <!-- END : Quote Details Edit Modal -->

          <!-- START: Quote Items Edit Modal -->
          <div class="modal fade" id="myQuoteItemsEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
            <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/quoteSaveAsDraft'); ?>">
                <input type="hidden" name="quote_id" id="quote_id" value="<?= $quote_id ?>">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Quote Items</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12  mt-50">
                      <div id="import_showhide" class="btn btn-sm btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Import Products Via Template</div>
                      <div id="import_hide" class="btn btn-sm btn-danger pull-right"><span class="glyphicon glyphicon-minus"></span> Import Products Via Template</div>
                    </div>
                  </div>

                  <?php
                  $total_price = 0;
                  $tax_amount = 0;
                  ?>
                  <div class="clearfix"></div>

                  <!-- Start: Product Section -->
                  <div id="product_import_area">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <label class="control-label">Import Products</label>
                      <input class="form-control notranslate" type="file" name="product_import" id="product_import" />
                      <span class="text-danger" style="font-size: 8px;">Maximum File Size of 2MB</span>
                      <span id="product_import_error" style="color: red;"></span>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-6"><br />
                      <button id="btn_product_import" class="btn btn-sm btn-success" onclick="importDraftProduct(1,event);" style="margin-top: 11px;" <?php echo $disabled; ?>>Upload</button>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 q_download_temp"><br />
                      <a href="<?php echo $this->createAbsoluteUrl('quotes/productSample'); ?>" class="btn btn-sm btn-primary" style="margin-top: 11px;" <?php echo $disabled; ?>>Download Template</a>
                    </div>
                    <div class="clearfix"></div><br /><br />
                  </div>

                  <div id="here_is_where_you_add_new_items_to_this_quote">
                    <div>
                      <?php $this->renderPartial('draft_edit_quote/_product', array('quote_products' => $quote_products)); ?>
                    </div>
                    <div id="product_list_cont"></div>
                    <div class="clearfix"> </div>
                  </div>
                  <!-- END: Product Section -->

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
            </div>
          </div>
          <!-- END : Quote Items Edit Modal -->

          <!-- START: Quote Document Edit Modal -->
          <div class="modal fade" id="myQuoteDocumentEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Quote Documents</h4>
                </div>

                <!-- Start: Supporting Document Area -->
                <div class="clearfix"> <br /> </div>
                <div class="clearfix"><br /></div>
                <div class="form-group">
                  <h3 class="col-md-6 pull-left" style="font-size: 16px !important;margin-top:32px; text-decoration: underline;">Quote Documents</h3>
                  <div class="col-md-6 pull-right text-right" style="margin-top:32px;">
                    <div id="supporting_document_showhide" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-plus"></span> Attach Supporting Documents</div>
                    <div id="supporting_document_hide" class="btn btn-sm btn-danger"> <span class="glyphicon glyphicon-minus"></span> Attach Supporting Documents</div>
                  </div>
                  <div class="clearfix"></div>
                  <div id="supporting_document_area" style="margin-top: 26px">
                    <div id="file_upload_alert" class="alert alert-success" role="alert"></div>
                    <img class="processing_oboloo" src="<?php echo AppUrl::bicesUrl('images/icons/processin_oboloo.gif'); ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">

                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label">Document</label>
                      <input class="form-control notranslate" type="file" name="order_file_1" id="order_file_1" />
                      <span class="text-danger" style="font-size: 8px;">Maximum File Size of 25MB</span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <label class="control-label">File/Document Description</label>
                      <input type="text" class="form-control notranslate" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" />
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-12 text-right">
                      <button id="btn_upload_1" class="btn btn-sm btn-success" onclick="uploadDocumentDraft(1,<?= $quote_id; ?>,event);" style="margin-top: 18px;" <?php echo $disabled; ?>>Upload</button>
                    </div>
                    <div class="clearfix"></div><br />
                  </div>
                </div>
                <!-- END: Supporting Document Area -->

                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- END : Quote Items Edit Modal -->

          <!-- START: Quote Details Edit Modal -->
          <div class="modal fade" id="myQuoteQuestionnaireEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Quote Scoring Criteria</h4>
                </div>
                <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/quoteSaveAsDraft'); ?>">
                 <input type="hidden" name="quote_id" id="quote_id" value="<?= $quote_id ?>">
                 <div class="modal-body p-0">
                  <h3 class="pull-left" style="font-size: 16px !important; margin-bottom: 10px;margin-top:9px;margin-left: 8px; text-decoration: underline;">Questionnaires & Weighted Scoring</h3>
                  <div class="clearfix"></div><br />
                  <h4 class="col-md-12 subheading">
                    Do you want to add questionnaires to this sourcing activity? </br></br>
                    Suppliers can answer the questionnaires that you include below at the same time as responding to the other areas of your sourcing activity. Put a percentage next to each questionnaire to assign your weighted scoring. 
                    </br></br>
                    You can create your own questionnaires within the eSourcing Questionnaires page.
                  </h4>
                  <div class="clearfix"></div><br />

                  <div class="col-md-12 text-right mb-45">
                    <button type="button" class="btn btn-danger remove_questionaire_section">Remove Questionnaire Section</button>
                    <?php if (empty($quoteQuestionnaire)) { ?>
                      <button type="button" class="btn btn-warning add_questionaire_section">Add Questionnaire Section</button>
                    <?php } ?>
                  </div>
                  
                  <div class="col-md-12 mb-45">
                    <div class="questionaire_section mb-45">
                      <div class="form-group col-md-8">
                        <label>Questionaire</label>
                        <select class="form-control" id="quote_questionaire"></select>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-10 mb-45 questionaire_list">
                    <div class="row">
                      <div class="col-md-6">
                        <h5 style="margin-bottom: 20px; margin-left: 10px;">Questionnaire Name</h5>
                      </div>
                      <div class="col-md-2">
                        <h5 style="margin-bottom: 20px;">Score (%)</h5>
                      </div>
                      <div class="col-md-10">
                        <ul id="questionaire_sortable" class="add_new_questionire" style="list-style: none;">
                          <?php
                          $quoteQuestionnaire = Yii::app()->db->createCommand("SELECT id, question_form_name as name,question_scorer, quote_id FROM 
                          quote_questionnaire WHERE quote_id=" . $quote["quote_id"]." order by question_form_name ")->queryAll();
                          if (!empty($quoteQuestionnaire)) {
                            $quotesQuestionListArr = [];
                            foreach ($quoteQuestionnaire as $quotesQuestionList) {
                              $quotesQuestionListArr[] = $quotesQuestionList['id']; ?>
                              <div class="removeItem">
                                <li class="ui-state-default select_form_list notranslate" style="padding: 12px; font-size: 12px;">
                                  <div class="row">
                                    <div class="col-md-7" style="padding: 8px 20px"><?= $quotesQuestionList['name']; ?></div>
                                    <div class="col-md-2"><input type="number" class="form-control score-criteria text-center" value="<?= $quotesQuestionList['question_scorer']; ?>" style="border-color: #3b4fb4 !important;" name="question_scorer[]" onkeypress="return isNumberKey(event)" onchange="maxScoring(this.value)"></div>
                                    <div class="col-md-3" style="padding: 8px 20px"> <span class="btn-link pull-right removeThisRow_<?= $quotesQuestionList['id']; ?>" id="removeThisRow" onclick="removeThisRow(<?= $quotesQuestionList['id']; ?>)" style="cursor: pointer;">Remove</span></div>
                                  </div>
                                  <input type="hidden" name="question_form_id[]" value="<?= $quotesQuestionList['id']; ?>" />
                                  <input type="hidden" name="question_form_name[]" value="<?= $quotesQuestionList['name']; ?>" />
                                </li>
                              </div>
                          <?php }
                          } 
                            $exitQuestionnireArray = json_encode($quotesQuestionListArr);
                          ?>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-12 mt-15">
                      <div class="pull-right">
                        <div class="scoring_alert" style="color: red;"></div><br />
                        <div class="scoring_percentage_alert "></div>
                        <div class="scoring_alert2 pull-right" style='color: red;'></div>
                      </div>
                    </div>
                  </div>

                </div>
                <!-- END: custom-scoring -->
                <div class="clearfix"><br /></div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary  queestionnaire_submit">Submit</button>
                </div>
               </form>
              </div>
            </div>
          </div>
          <!-- END : Quote Questionnare Edit Modal -->

          <!-- START: Quote Comments Edit Modal -->
          <div class="modal fade" id="myQuoteCommentEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Comments</h4>
                </div>
                <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/quoteSaveAsDraft'); ?>">
                 <input type="hidden" name="quote_id" id="quote_id" value="<?= $quote_id ?>">
                 <div class="col-xs-12 pt-15 pb-30">
                  <h4 style="padding-bottom: 15px;" class="subheading">Please leave any comments for other contributors here. These will not be made visible to any suppliers responding to this sourcing activity.</h4>
                  <input type="hidden" class="notranslate" name="form_comments" id="form_comments" value="1" />
                  <label class="control-label notranslate">Comments</label>
                  <textarea class="form-control notranslate" name="quote_comments" id="quote_id" rows="4" placeholder="Additional Comments"></textarea>
                 </div>
                 <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                 </div>
                </form>
              </div>
            </div>
          </div>
          <!-- END : Quote Comments Edit Modal -->
    
        <!-- Start: Quote Log -->
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pl-0" style="margin-left: -3px;">
          <div class="panel panel-default">
            <div class="panel-body">
              <h3 class="pl-0">Change Log</h3>
              <table class="table mt-15">
                <thead>
                  <tr>
                    <th>User</th>
                    <th>Date</th>
                    <th>Comments</th>
                  </tr>
                </thead>
                <?php 
                if (!empty($scoringLog)) {
                  foreach ($scoringLog as $log) { ?>
                    <tr>
                      <td><?php echo $log['user']; ?></td>
                      <td><?php echo date(FunctionManager::dateFormat(), strtotime($log['created_datetime'])); ?></td>
                      <td><?php echo $log['comment']; ?></td>
                    </tr>
                  <?php }
                } else { ?>
                  <tr>
                    <td colspan="4">No Record Found</td>
                  </tr>
                <?php } ?>
              </table>
            </div>
          </div>
        </div>
        <!-- End: Quote Log  -->
      </div>
    </div>
  </div>
</div>
<?php $this->renderPartial("draft_edit_quote/_complete_contribution", ['quote_id' => $quote_id, 'user_id' => $userID]); ?>
<!-- Start: Modal Supplier -->
<div class="modal fade" id="inviteSuppliers">
  <div class="modal-dialog" style="width: 1100px;">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Suppliers</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Supplier modal body -->
      <div class="modal-body">
        <div class="tab scoring_supplier_tab">
          <h3 class="col-md-6" style="font-size: 16px !important; margin-bottom: 10px;margin-top:9px;text-decoration: underline;">Invite Suppliers</h3>
          <form method="post" action="<?php echo AppUrl::bicesUrl('quotes/quoteVendors'); ?>" autocomplete="off">
            <input type="hidden" name="quote_id" value="<?= $quote['quote_id']; ?>" />
            <?php $sql = "select vendor_id,vendor_name from vendors where active=1  and member_type=0 and (vendor_archive is null or vendor_archive=''  or LOWER(vendor_archive)='null') and quick_created not in('Yes and Quick Sourcing Evaluation','Yes') ORDER by vendor_name ASC";
            $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll(); ?>

            <div class="form-group tab_w_auto_new_modal">
              <div class="col-md-12 supplier_invite_container mb-45" style="padding-left: 0px;">

                <?php
                if (!empty($vendorExistReader) && count($vendorExistReader) > 0) { ?>
                  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                    <div class=" pull-right btn btn-success" id="add_supplier_contianer" title="Add another supplier">Add Another Supplier</div>
                  </div>
                  <?php foreach ($vendorExistReader as $vendorExist) { ?>
                    <!-- Start : Already exsit supplier showing here -->
                    <div class="col-md-12 supplier_invite_container" style="padding-left: 0px;">
                      <div class="vendor_new_<?= $vendorCtr ?>">
                        <div class="clearfix"></div>
                        <div class="col-md-5 col-sm-5 col-xs-12 date-input valid">
                          <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                          <select id="vendor_new_<?= $vendorCtr ?>" class="form-control select_vendor_multiple notranslate vendor_new_<?= $vendorCtr ?> vendor_new_vendor_name_<?= $vendorCtr ?>" onChange="supplierSelect(this.value,<?= $vendorCtr; ?>)" name="vendor_name_new[<?= $vendorCtr; ?>]" style="border-radius: 10px;border: 1px solid #4d90fe;">
                            <option value=""></option>
                            <option value="NEW_QUOTE_SUPPLIER" style="margin-left:20px;"><a data-toggle="modal" href="#myModalQuoteVendor" class="btn btn-primary"><b>+ Add New</b></a>
                              <?php foreach ($vendorReader as $value) { ?>
                            <option value="<?php echo $value['vendor_id']; ?>" <?php if ($vendorExist['vendor_id'] == $value['vendor_id']) echo "selected='selected'"; ?>><?php echo $value['vendor_name']; ?></option>
                          <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
                          <label class="control-label">Supplier Contact <span style="color: #a94442;">*</span></label>
                          <select class="form-control notranslate vendor_contact_new_<?= $vendorCtr ?> vendor_new_<?= $vendorCtr ?> vendor_contact_new_select" name="vendor_contact_new[<?= $vendorCtr ?>][]" multiple>
                          </select>
                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-12">
                          <div class="pt-15" id="remove-supplier" title="Remove Supplier" onclick="removeSupplier('<?= $vendorCtr ?>');">Remove Supplier</div>
                        </div>
                      </div>

                      <div class="clearfix"></div><br><br>
                    </div>

                    <!-- END : Already exsit supplier showing here -->
                  <?php $vendorCtr += 1;
                  }
                } else { ?>
                  <!-- Start: Add New supplier show -->
                  <div class="col-md-11 supplier_invite_container" style="padding-left: 0px;">
                    <div class="vendor_new_<?= $vendorCtr ?>">
                      <div class="clearfix"></div>
                      <div class="col-md-5 col-sm-5 col-xs-12 date-input valid">
                        <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                        <select class="form-control select_vendor_multiple notranslate vendor_new_<?= $vendorCtr ?> vendor_new_vendor_name_<?= $vendorCtr ?>" onChange="supplierSelect(this.value,<?= $vendorCtr; ?>)" name="vendor_name_new[<?= $vendorCtr; ?>]" style="border-radius: 10px;border: 1px solid #4d90fe;">
                          <option value=""></option>
                          <option value="NEW_QUOTE_SUPPLIER" style="margin-left:20px;"><a data-toggle="modal" href="#myModalQuoteVendor" class="btn btn-primary"><b>+ Add New</b></a>
                            <?php foreach ($vendorReader as $value) { ?>
                          <option value="<?php echo $value['vendor_id']; ?>"><?php echo $value['vendor_name']; ?></option>
                        <?php } ?>
                        </select>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
                        <label class="control-label">Supplier Contact <span style="color: #a94442;">*</span></label>
                        <select class="form-control notranslate vendor_contact_new_<?= $vendorCtr ?> vendor_new_<?= $vendorCtr ?> vendor_contact_new_select" name="vendor_contact_new[<?= $vendorCtr; ?>][]" onChange="contactSelect(this.value,<?= $vendorCtr; ?>)" multiple>
                        </select>
                      </div>

                      <div class="col-md-1 col-sm-1 col-xs-12">
                        <div class="remove-supplier" id="remove-supplier" title="Remove Supplier" onclick="removeSupplier('<?= $vendorCtr ?>');">Remove Supplier</div>
                      </div>
                      <div class="clearfix"></div><br><br>
                    </div>

                  </div>
                  <div class="col-md-1 col-sm-4 col-xs-12 text-right">
                    <div class=" pull-right btn btn-success" id="add_supplier_contianer" title="Add another supplier" style="margin-top: 14px !important;">Add Another Supplier</div>
                  </div>
                  <!-- END: Add New supplier show -->
                <?php } ?>


                <div id="more_supplier_container"></div>

                <div class="clearfix"></div><br />
                <div class="col-md-8 col-sm-8 col-xs-12 ">
                  <span id="hide_new_supplier" class="" title="Remove Quick Add Vendor fields">Remove Supplier Not In Oboloo</span>
                  <span class="btn btn-success" id="new_supplier" title="Quick Add Vendor">Add New Supplier Not In Oboloo</span>
                </div>
                <div class="clearfix"></div>
                <div class="new_supplier_con col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                      <input type="text" class="form-control notranslate" name="new_vendor_name" id="new_vendor_name" placeholder="Supplier Name">
                      <span class="new_vendor_err" style="display: none; font-size: 8px;">Supplier Name requried.</span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 quick_vendor_field">
                      <label class="control-label">Email Address <span style="color: #a94442;">*</span></label>
                      <input type="text" class="form-control  notranslate" name="new_vendor_emails" id="new_vendor_emails" placeholder="Email Address">
                      <span class="emails_error_1" style="display: none; font-size: 8px;"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 date-input quick_vendor_field">
                      <label class="control-label ">Confirm Email Address <span style="color: #a94442;">*</span></label>
                      <input type="text" class="form-control notranslate" name="confm_emails" id="confm_emails" placeholder="Email Address">
                      <span class="emails_error" style="display: none; font-size: 8px;">Email Address and Confirm Email Address are not matching.</span>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 quick_vendor_field"><label class="control-label">Contact Person <span style="color: #a94442;">*</span></label><input type="text" class="form-control notranslate" name="new_vendor_contact" id="new_vendor_contact" value="" placeholder="Contact Person">
                      <span class="new_vendor_cont_err" style="display: none; font-size: 8px;">Contact Person requried</span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 quick_vendor_field"><label class="control-label">Address Line 1</label><input type="text" class="form-control notranslate" name="new_vendor_address" id="new_vendor_address" value="" placeholder="Address Line 1"></div>
                    <div class="col-md-6 col-sm-6 col-xs-6 quick_vendor_field"><label class="control-label">Address Line 2</label><input type="text" class="form-control notranslate" name="new_vendor_address2" id="new_vendor_address2" value="" placeholder="Address Line 2"></div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">City</label><input type="text" class="form-control notranslate" name="new_vendor_city" id="new_vendor_city" value="" placeholder="City"></div>
                    <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">State</label><input type="text" class="form-control notranslate" name="new_vendor_state" id="new_vendor_state" value="" placeholder="State"></div>
                    <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">Post Code/Zip Code</label><input type="text" class="form-control notranslate" name="new_vendor_zip" id="new_vendor_zip" value="" placeholder="Post Code/Zip Code"></div>
                    <div class="form-group quick_vendor_field">
                      <span class='quick_msg pull-right' style='color: #a94442;'></span><br />
                      <span class="btn btn-primary btn-sm pull-right" id="save_new_supplier" title="Save Quick Vendor" <?php echo $disabled; ?>>Save Quick Vendor</span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div><br>
                <div id="quick_more_supplier_container"></div>

                <?php $vendors_to_show = 1; ?>
                <?php for ($vendor_idx = 1; $vendor_idx <= $vendors_to_show; $vendor_idx++) { ?>
                <?php } ?>
                <input type="hidden" name="vendor_count" class="notranslate" id="vendor_count" value="<?php echo $vendors_to_show; ?>" />
              </div>
              <div class="clearfix"> <br /> </div>
            </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- END: Modal Supplier -->
<?php if(date("Y-m-d H:i:s") > FunctionManager::CheckContriddlineExprie($quote_id, $userID) && $quote['user_id'] != $userID) {  ?>
 <script>
  $(document).ready(function(){
    $('.remove-document_draft').addClass("disabled");
  });
 </script>
<?php } else {?>
  <script>
  $(document).ready(function(){
    $('.remove-document_draft').removeClass("disabled");
  });
 </script>
<?php } 

if($contributeUser['contribute_status'] == "1" && $quote['user_id'] != $userID) {?>
      <script>
        $(document).ready(function(){
          $('.remove-document_draft').prop('disabled', true);
        });
      </script>
<?php } else { ?>
  <script>
  $(document).ready(function(){
    $('.remove-document_draft').prop('disabled', false);
  });
 </script>
<?php } ?>

<?php $updatedContributeDeadline = Yii::app()->user->getFlash('updatedContributeDeadline');
  if (!empty($updatedContributeDeadline)) { ?>
    <script>
      $(document).ready(function(){
        $('#showAllContributors').modal('show');
      });
    </script>
<?php } ?>

<?php $this->renderPartial("_create_quote_supplier"); ?>
<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::jsUrl('bootstrap-duallistbox.css'); ?>" />
<script type="text/javascript" src="<?php echo AppUrl::jsUrl('jquery.bootstrap-duallistbox.js'); ?>"></script>
<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/quote_scoring_criteria_script.js'); ?>"></script>
<script type="text/javascript">
  
  $(document).ready(function() {
    setTimeout(function() {
      $(".vendor_contact_new_select").select2({
        placeholder: "Select Contact",
        allowClear: true,
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"
      });
    }, 100);
    $("#quote_question_select_2").select2({
      placeholder: "Select Questioninare",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });

    <?php if (isset($quote['location_id']) && !empty($quote['location_id'])) { ?>

      <?php if (isset($quote['department_id']) && !empty($quote['department_id'])) { ?>
        loadDepartmentsForSingleLocation(<?php echo $quote['department_id']; ?>);
      <?php } else { ?>
        loadDepartmentsForSingleLocation(0);
      <?php } ?>
    <?php } ?>
    <?php if (isset($quote['subcategory_id']) && !empty($quote['subcategory_id'])) { ?>
      loadSubcategories(<?php echo $quote['subcategory_id']; ?>);
      loadSubcategoriesContract(<?php echo $quote['subcategory_id']; ?>);
    <?php } ?>
    $('#opening_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?> HH:mm:ss',
    });

    $('#closing_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?> HH:mm:ss',
    });


    $('#quote_start_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?> HH:mm:ss'
    });
    $('#qoute_end_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?> HH:mm:ss'
    });
    $('#needed_date').daterangepicker({
      singleDatePicker: true,
      singleClasses: "picker_3",
      timePicker: true,
      timePickerIncrement: 10,
      locale: {
        format: '<?php echo FunctionManager::dateFormatJS(); ?> h:mm A'
      }
    });
    $(".select2").select2({
      placeholder: 'Invite Suppliers',
      minimumInputLength: 1,
      ajax: {
        url: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
        dataType: 'json',
        quietMillis: 50,
        type: "GET",
        data: function(term) {
          return {
            query: term
          };
        },
        processResults: function(data) {
          $('#vendor_name_select2_1').trigger('change.select2');
          return {
            results: data
          };
        }
      }
    });

    $('[class*="vendor_new_"]').trigger('change');

  });

  function loadDepartmentsForSingleLocation(department_id) {
    var location_id = $('#location_id').val();
    var single = 1;
    if (location_id == 0)
      $('#department_id').html('<option value="">Select Department</option>');
    else {
      $.ajax({
        type: "POST",
        data: {
          location_id: location_id,
          single: single
        },
        dataType: "json",
        url: BICES.Options.baseurl + '/locations/getDepartments',
        success: function(options) {
          var options_html = '<option value="">Select Department</option>';
          for (var i = 0; i < options.length; i++)
            options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
          $('#department_id').html(options_html);
          if (department_id != 0) $('#department_id').val(department_id);
            // Set the selected department name in the created element
          $('.selected_department_name').text($('#department_id option:selected').text());
        },
        error: function() {
          $('#department_id').html('<option value="">Select Department</option>');
        }
      });
    
    }
  }

  function loadSubcategories(input_subcategory_id) {
    var category_id = $('#category_id').val();
    if (category_id == 0)
      $('#subcategory_id').html('<option value="0">Select Sub Category</option>');
    else {
      $.ajax({
        type: "POST",
        data: {
          category_id: category_id,
          subcategory_id: input_subcategory_id
        },
        dataType: "json",
        url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
        success: function(options) {
          var options_html = '<option value="0">Select Sub Category</option>';
          for (var i = 0; i < options.suggestions.length; i++)
            options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
          $('#subcategory_id').html(options_html);
          if (input_subcategory_id != 0) $('#subcategory_id').val(input_subcategory_id);
          var selectedSubcategory = $('#subcategory_id option:selected').text();
          $('#selectedSubcategory').text(selectedSubcategory);
        },
        error: function() {
          $('#subcategory_id').html('<option value="0">Select Sub Category</option>');
        }
      });
    }
  }

  function loadSubcategoriesContract(input_subcategory_id) {
    var category_id = $('#contract_category_id').val();
    if (category_id == 0)
      $('#contract_subcategory_id').html('<option value="0">Select Sub Category</option>');
    else {
      $.ajax({
        type: "POST",
        data: {
          category_id: category_id
        },
        dataType: "json",
        url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
        success: function(options) {
          var options_html = '<option value="0">Select  Sub Category</option>';
          for (var i = 0; i < options.suggestions.length; i++)
            options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
          $('#contract_subcategory_id').html(options_html);
          if (input_subcategory_id != 0) $('#contract_subcategory_id').val(input_subcategory_id);
        },
        error: function() {
          $('#contract_subcategory_id').html('<option value="0">Select Sub Category</option>');
        }
      });
    }
  }

  $(document).ready(function() {
    $("#import_hide").hide();
    $("#supporting_document_hide").hide();
    $("#product_import_area").hide();
    $("#supporting_document_area").hide();
    $("#scoring_criteria_hide").hide();
    $("#default_scoring_container").hide();

    // Start: Import product
    $("#import_showhide").click(function() {
      $("#product_import_area").show();
      $("#import_showhide").hide();
      $("#import_hide").show();
    });
    $("#import_hide").click(function() {
      $("#product_import_area").hide();
      $("#import_showhide").show();
      $("#import_hide").hide();
    });
  });
  $("#supporting_document_showhide").click(function() {
    $("#supporting_document_area").show();
    $("#supporting_document_showhide").hide();
    $("#supporting_document_hide").show();
  });
  $("#supporting_document_hide").click(function() {
    $("#supporting_document_area").hide();
    $("#supporting_document_showhide").show();
    $("#supporting_document_hide").hide();
  });

  function importDraftProduct(uploadBtnIDX, e) {
    e.preventDefault();
    var uploadedObj = $('#product_import');

    $(document).ready(function() {
      $('#product_import').bind('change', function() {
        if (this.files[0].size > 2097152) {
          alert("Maximum File Size of 2MB");
          this.value = "";
        }
      });
    });

    var file_data = $('#product_import').prop('files')[0];
    fileName = uploadedObj[0].files[0]['name']
    if (fileName.split('.').pop() != "csv") {
      $("#product_import_error").html("Only csv file is allowed");
      return false;
    } else {
      $("#product_import_error").html("");
    }

    var form_data = new FormData();
    form_data.append('file', file_data);
    $.ajax({
      url: BICES.Options.baseurl + '/quotes/productImport', // point to server-side PHP script 
      dataType: 'text', // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(uploaded_response) {
        $("#product_list_cont").html(uploaded_response);
        uploadedObj.val(null);
        $("#product_import_area").hide();
        $("#import_showhide").show();
        $("#import_hide").hide();

      }
    });


  }
  // END: Import product

  $('#new_vendor_code').hide();
  $(".remove-supplier").hide();
  $(".new_supplier_con").hide();
  $("#hide_new_supplier").hide();

  $("#new_supplier").click(function() {
    $(".new_supplier_con").show();
    $("#hide_new_supplier").show();
    $(".quick_vendor_field").show();
  });

  $("#hide_new_supplier").click(function() {
    $(".new_supplier_con").hide();
    $("#new_vendor_name").val("");
    $("#new_vendor_emails").val("");
    $("#hide_new_supplier").hide();
    $(".btn-functionality").remove('disabled');
    $(".btn-functionality").prop('disabled', false);
  });

  $('.add_questionaire_section').click(function() {
    $('.questionaire_section').show();
    $('.add_questionaire_section').hide();
    $('.remove_questionaire_section').show();
  });

  $('.remove_questionaire_section').click(function() {
    $('.questionaire_section').hide();
    $('.add_questionaire_section').show();
    $('.remove_questionaire_section').hide();
  });

  $("#questionaire_sortable").sortable({
    placeholder: "ui-state-highlight"
  });
  $("#questionaire_sortable").disableSelection();
  var questionaireArr = [];
  var exitQuestionnireArray = <?= $exitQuestionnireArray ?>;
   if(Array.isArray(exitQuestionnireArray) && exitQuestionnireArray.length > 0){
    exitQuestionnireArray.forEach(item => questionaireArr.push(item));
   }
  //  $('#quote_questionaire').change(function() {
  //   var optionText = $(this).find("option:selected").text();
  //   var optionVal = $(this).find("option:selected").val();
  //   if (optionVal != '' && optionVal != 0) {
  //     $('.questionaire_list').show();
  //     $('.add_new_questionire').append(
  //       `<div class="removeItem ">
  //          <li class="ui-state-default select_form_list notranslate" style="padding:12px; font-size:12px;"> 
  //           <div class="row">
  //            <div class="col-md-7" style="padding: 8px 20px">${optionText}</div>
  //            <div class="col-md-2 valid"><input type="number" class="form-control score-criteria text-center" name="question_scorer[]" onkeypress="return isNumberKey(event)" onchange="maxScoring(this.value)" required /></div>
  //            <div class="col-md-3" style="padding: 8px 20px"> <span class="btn-link pull-right removeThisRow_${optionVal}" id="removeThisRow" onclick="removeThisRow(${optionVal})" style="cursor: pointer;">Remove</span></div>
  //           </div>
  //           <input type="hidden" name="question_form_id[]" id="remove_questionire_array" value="${optionVal}" />
  //           <input type="hidden" name="question_form_name[]" value="${optionText}" /></li>
  //         </div>`);
  //       questionaireArr.push(optionVal);
  //     $('#quote_questionaire').val("");
      
  //   }
  //   questionaire();
  //  });
    $('#quote_questionaire').change(function() {
    var optionText = $(this).find("option:selected").text();
    var optionVal = $(this).find("option:selected").val();
    if (optionVal != '' && optionVal != 0) {
      $('.questionaire_list').show();

      var newItem = $(`<div class="removeItem">
                      <li class="ui-state-default select_form_list notranslate " style="padding:12px; font-size:12px;"> 
                        <div class="row">
                          <div class="col-md-7" style="padding: 8px 20px">${optionText}</div>
                          <div class="col-md-2 valid"><input type="number" class="form-control score-criteria text-center" name="question_scorer[]" onkeypress="return isNumberKey(event)" onchange="maxScoring(this.value)" required /></div>
                          <div class="col-md-3" style="padding: 8px 20px"> <span class="btn-link pull-right removeThisRow_${optionVal}" id="removeThisRow" onclick="removeThisRow(${optionVal})" style="cursor: pointer;">Remove</span></div>
                        </div>
                        <input type="hidden" name="question_form_id[]" id="remove_questionire_array" value="${optionVal}" />
                        <input type="hidden" name="question_form_name[]" value="${optionText}" /></li>
                    </div>`);

      var items = $('.add_new_questionire .removeItem').toArray();
      items.push(newItem);
      items.sort(function(a, b) {
        var textA = $(a).find('.col-md-7').text();
        var textB = $(b).find('.col-md-7').text();
        return textA.localeCompare(textB);
      });

      $('.add_new_questionire').empty().append(items);
      questionaireArr.push(optionVal);
      $('#quote_questionaire').val("");
    }
    questionaire();
  });

  // Make sure this is placed before the event listener
function questionaire() {
  $.ajax({
    type: "POST",
    data: {
      questionaire: questionaireArr
    },
    dataType: "html",
    url: BICES.Options.baseurl + '/quotes/questionaireDropdown',
    success: function(options) {
      try {
        var optionsObj = JSON.parse(options);
        $("#quote_questionaire").html(optionsObj.option);
      } catch (error) {
        console.error("Error parsing JSON: ", error);
      }
    },
    error: function(xhr, status, error) {
      console.error("AJAX Error: ", error);
    }
  });
}
questionaire();

function removeThisRow(optionID){
  // when remove questionaire from list also remove this questionaire ID from array questionaireArr
  var optionID = String(optionID);
  var index = questionaireArr.indexOf(optionID);
  if(index !== -1) {
    questionaireArr.splice(index, 1);
    questionaire();
  }

  $('.next-page').attr('disabled',false);
  $(".scoring_percentage_alert").html('');
  $('.removeThisRow_'+optionID).parent().parent().parent().parent().remove();
  $('.score-criteria').trigger('keyup');
}

  // END  : show questionire dropdown 
$("#save_new_supplier").click(function() {
  var email = $("#new_vendor_emails").val();
  var conf_email = $("#confm_emails").val();
  var new_vendor_name = $("#new_vendor_name").val();
  var new_vendor_emails = $("#new_vendor_emails").val();
  var new_vendor_contact_name = $("#new_vendor_contact").val();
  var new_vendor_address = $("#new_vendor_address").val();
  var new_vendor_address2 = $("#new_vendor_address2").val();
  var new_vendor_city = $("#new_vendor_city").val();
  var new_vendor_state = $("#new_vendor_state").val();
  var new_vendor_zip = $("#new_vendor_zip").val();

  if (email != conf_email) {
    $(".emails_error").text("Email Address and Confirm Email Address are not matching.");
    $(".emails_error").css('color', 'red').show();
    return false;
  } else {
    $(".emails_error").css('color', 'red').hide();
  }

  if (!validate(email)) {
    $(".emails_error_1").css('color', 'red').show();
    $(".emails_error_1").text("Please enter valid email ");
    return false;
  } else {
    $(".emails_error_1").css('color', 'red').hide();
  }

  if (!validate(conf_email)) {
    $(".emails_error").css('color', 'red').show();
    $(".emails_error").text("Please enter valid email ");
    return false;
  } else {
    $(".emails_error").css('color', 'red').hide();
  }

  if (new_vendor_name == "") {
    $(".new_vendor_err").css('color', 'red').show();
    return false;
  }

  if (new_vendor_contact_name == "") {
    $(".new_vendor_cont_err").css('color', 'red').show();
    return false;
  }

    if (new_vendor_emails != '') {
      $(this).prop('disabled', true);
      $.ajax({
        type: "POST",
        data: {
          vendor_name: new_vendor_name,
          vendor_emails: new_vendor_emails,
          new_vendor_contact_name: new_vendor_contact_name,
          new_vendor_address: new_vendor_address,
          new_vendor_address2: new_vendor_address2,
          new_vendor_city: new_vendor_city,
          new_vendor_state: new_vendor_state,
          new_vendor_zip: new_vendor_zip
        },
        url: BICES.Options.baseurl + '/vendors/quickAddVendor',
        success: function(options) {
          if (options == 1) {
            $(".quick_msg").html("Email is already taken.");
            alert("Supplier with this Email is already taken. Please try another.");
          } else if (options != 1) {
            $(".quick_msg").html("New Supplier added successfully.");
            alert("Supplier added successfully.");
            //dropdownVendors("vendor_id");

            $('.vendor_new_QIX').closest('invalid');
            $('.supplier_select_supplier_email').removeClass('invalid');
            $('.vendor_contact_new_select').removeClass('invalid');
            //
            total_vendors = $('#total_vendors').val();
            total_vendors = parseInt(total_vendors);
            total_vendors = total_vendors + 1;
            $('#total_vendors').val(total_vendors);

            $(".select_vendor_multiple").select2("destroy");
            $(".vendor_contact_new_select").select2("destroy");

            // supplierCon = $(".supplier_invite_container").last().html();
            // supplierCon = supplierCon.replace(/QIX/g, total_vendors);
            vendorCtr = Number(vendorCtr) + 1;
            $("#quick_more_supplier_container").append(addMoreSupplierToContainer(vendorCtr));

            $("#quick_more_supplier_container .remove-supplier").show();
            $(".vendor_new_vendor_name_" + vendorCtr).removeClass('supplier_invite_container');
            $(".vendor_new_vendor_name_" + vendorCtr).html("<option value='" + options + "'>" + $("#new_vendor_name").val() + "</option>");
            contactNew = $("#new_vendor_contact").val();
            emailNew = $("#new_vendor_emails").val();
            $(".vendor_contact_new_" + vendorCtr).html('<option selected val="' + contactNew + '">' + contactNew + '<option>');
            $(".vendor_email_new_" + vendorCtr).html('<option selected val="' + emailNew + '">' + emailNew + '<option>');
            //$('.vendor_email_new_'+total_vendors+' option[value="'+emailNew+'"]');

            $(".select_vendor_multiple").select2({
              placeholder: "Select Supplier",
              allowClear: true,
              containerCssClass: "notranslate",
              dropdownCssClass: "notranslate"
            });

            setTimeout(function() {
              $(".vendor_contact_new_select").select2({
                placeholder: "Select Contact",
                allowClear: true,
                containerCssClass: "notranslate",
                dropdownCssClass: "notranslate"
              });
            }, 100);

            $("#hide_new_supplier").trigger("click");
            $(".quick_vendor_field").hide();
            $("#quick_vendor_name");
            $("#new_vendor_emails").val('');
            $("#confm_emails").val('');
            $("#new_vendor_contact").val('');
            $("#new_vendor_address").val('');
            $("#new_vendor_address2").val('');
            $("#new_vendor_city").val('');
            $("#new_vendor_state").val('');
            $("#new_vendor_zip").val('');

          } else if (options == 3) {
            $(".quick_msg").html("Problem occured, try again.");
            alert("Problem occured, try again.");
          }
          $("#save_new_supplier").prop('disabled', false);
        }


      });

    } else {
      $(".quick_msg").before("Email is required");
    }
    $(".quick_msg").delay(5000).fadeOut(800);
  });

  $("#myModalQuoteVendor").hide('modal');
  $(document).ready(() => {

    $(".select_vendor_multiple").select2({
      placeholder: "Select Supplier",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    }).on('select2:close', function() {
      var el = $(this);
      if (el.val() === "NEW_QUOTE_SUPPLIER") {
        $(".tab_w_auto_new_modal").css("height", '550px');
        $("#myModalQuoteVendor").show('modal');
      }
    });
  });

  function supplierSelect(vendor_id, serialNum) {
    var vendor_id = vendor_id;
    var serialNum = serialNum;

    if (serialNum == 'QIX') {
      var el = $(".vendor_new_vendor_name_QIX");
      var el_for_later = ".vendor_new_vendor_name_QIX";
    } else {
      var el = $(".vendor_new_vendor_name_" + serialNum);
      var el_for_later = ".vendor_new_vendor_name_" + serialNum;
    }

    if (el.val() === "NEW_QUOTE_SUPPLIER") {
      $(".tab_w_auto_new_modal").css("height", '550px');
      $("#myModalQuoteVendor").show('modal');
      $("#vendor_class_number").val(el_for_later);
    } else {
      $.ajax({
        dataType: 'json',
        type: "post",
        url: "<?php echo AppUrl::bicesUrl('vendors/getVendorDetail'); ?>",
        data: {
          vendor_id: vendor_id,
          quote_id: "<?= $quote['quote_id'] ?>",
        },
        success: function(data_option) {
          if (serialNum == 'QIX') {
            $(".vendor_contact_new_QIX").html(data_option.contact_name);
            $(".vendor_email_new_QIX").val(data_option.emails);
            contactSelect($(".vendor_contact_new_" + serialNum).val(), serialNum)
          } else {

            $(".vendor_contact_new_" + serialNum).html(data_option.contact_name);
            $(".vendor_email_new_" + serialNum).val(data_option.emails);
            contactSelect($(".vendor_contact_new_" + serialNum).val(), serialNum)
          }
        }
      });
    }
  }

  function contactSelect(selectElement, serialNum) {

    var selectorName = '';
    if (serialNum == 'QIX') {
      var vendor_id = $(".vendor_new_vendor_name_QIX ").val();
      var selectorName = selectElement;
    } else {
      var vendor_id = $(".vendor_new_vendor_name_" + serialNum).val();
      var selectorName = selectElement;
    }

    // var contactName = event.target.value;
    var serialNum = serialNum;
    $.ajax({
      dataType: 'json',
      type: "post",
      url: "<?php echo AppUrl::bicesUrl('vendors/getVendorContactEmail'); ?>",
      data: {
        vendor_id: vendor_id,
        contact_name: selectorName,
        quote_id: "<?= $quote['quote_id'] ?>"
      },
      success: function(data_option) {
        if (serialNum == 'QIX') {
          $(".vendor_email_new_QIX").html(data_option.emails);
        } else {
          $(".vendor_email_new_" + serialNum).html(data_option.emails);
        }
      }
    });
    // event.preventDefault();
  }

  $(".remove-supplier").hide();
  var vendorCtr = "<?= $vendorCtr ?>";
  $("#add_supplier_contianer").click(function() {
    $(".select_vendor_multiple").select2("destroy");
    var total_vendors = $('#total_vendors').val();
    total_vendors = parseInt(total_vendors);
    total_vendors = total_vendors + 1;
    $('#total_vendors').val(total_vendors);

    // var supplierCon = $(".supplier_invite_container").last().html();
    // supplierCon = supplierCon.replace(/QIX/g, total_vendors);
    vendorCtr = Number(vendorCtr) + 1;

    $("#more_supplier_container").append(addMoreSupplierToContainer(vendorCtr));

    $(".vendor_contact_new_" + vendorCtr).select2({
      placeholder: "Select Contact",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });

    // Start: removed selected vendor
    $('.select_vendor_multiple option:selected').each(function() {
      if (parseInt($(this).attr('value')) > 0)
        $(".vendor_new_" + vendorCtr + " option[value='" + $(this).attr('value') + "']").remove();
    });

    // End: removed selected vendor
    $("#more_supplier_container .remove-supplier").show();
    $(".select_vendor_multiple").select2({
      placeholder: "Select Supplier",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });
  });

function removeSupplier(idnumber) {
  $.confirm({
    title: false,
    content: '<spam style="font-size:11px">Do you want to remove this record?</span>',
    buttons: {
      Yes: {
        text: "Yes",
        btnClass: 'btn-blue',
        action: function() {
          $(".vendor_new_" + idnumber).remove();
        }
      },
      No: {
        text: "No",
        btnClass: 'btn-red',
        action: function() {}
      },
    }
  });

}

function addMoreSupplierToContainer(vendorCtr) {
  return `  <div class="vendor_new_${vendorCtr}">
    <div class="col-md-5 col-sm-5 col-xs-12 date-input valid">
      <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
      <select class="form-control select_vendor_multiple notranslate vendor_new_${vendorCtr} vendor_new_vendor_name_${vendorCtr}"     
        onChange="supplierSelect(this.value,${vendorCtr})" name="vendor_name_new[${vendorCtr}]"  style="border-radius: 10px;border: 1px solid #4d90fe;">
        <option value=""></option>
        <option value="NEW_QUOTE_SUPPLIER" style="margin-left:20px;"><a data-toggle="modal" 
          href="#myModalQuoteVendor" class="btn btn-primary"><b>+ Add New</b></a>
          <?php foreach ($vendorReader as $value) { ?>
        <option value="<?php echo $value['vendor_id']; ?>"><?php echo $value['vendor_name']; ?></option>
      <?php } ?>
      </select>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
      <label class="control-label">Supplier Contact <span style="color: #a94442;">*</span></label>
      <select class="form-control notranslate vendor_contact_new_${vendorCtr} vendor_new_${vendorCtr} vendor_contact_new_select"
      name="vendor_contact_new[${vendorCtr}][]" onChange="contactSelect(this.value,${vendorCtr})" multiple></select>
    </div>

    <div class="col-md-1 col-sm-1 col-xs-12">
      <div class="remove-supplier" id="remove-supplier" title="Remove Supplier" style="padding-top:14px;" onclick="removeSupplier(${vendorCtr});">Remove Supplier</div>
    </div>
    <div class="clearfix"></div><br><br>
  </div>`;
}

function publishQuote(vendExist) {
  message = '';
  if (vendExist === 1) {
    message = `<p>Are you sure you want to publish this sourcing activity to suppliers?</p>
                  <p>You will not be able to edit this activity again unless you reopen it to suppliers at a later date.</p>`;
    publishAlert(vendExist, message)
  } else {
    message = 'You must invite at least one supplier before you can publish this sourcing activity';
    publishAlert(vendExist, message)
  }
}

const publishAlert = (vendExist, message) => {
  if (vendExist === 1) {
    $.confirm({
      title: false,
      content: message,
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              type: "POST",
              data: {
                quote_id: $('#quote_id').val()
              },
              dataType: "json",
              url: "<?php echo AppUrl::bicesUrl('quotes/changeQuoteStatusAndUpdate'); ?>",
              success: function(options) {
                if (options.status == 'pushlished') {
                  location.reload();
                }
              },
              error: function() {

              }
            });
          }
        },
        No: {
          text: "Close",
          btnClass: 'btn-red',
          action: function() {}
        },
      }
    });
  } else {
    $.confirm({
      title: false,
      content: message,
      buttons: {
        No: {
          text: "Close",
          btnClass: 'btn-red',
          action: function() {}
        },
      }
    });
  }
}
</script>

<style>
  .table-condensed {width: 224px; padding: 0 0 4px 0 !important;}
  .daterangepicker_input {text-align: center;}
  h3 {  font-size: 16px !important;  }
  .quote_draft_case_hide{display: none;}
</style>