<div class="col-md-8" >
 <div class="x_panel tile" style="box-shadow: none;">
 <div class="x_title">
  <h2 class="pl-22">Supplier Weighted Score By Questionnaire (%)</h2>
  <div class="clearfix"></div>
 </div>
 <div class="clearfix"></div>
  <div class="x_content" >
   <div id="quote_scoring_chart"></div>
  </div>
 </div>
</div>
<div class="col-md-4">
 <div class="x_panel tile" style="box-shadow: none;  height: 442px !important;">
 <div class="x_title">
  <h2 class="pl-22">% of Users Who Have Completed Scoring</h2>
  <div class="clearfix"></div>
 </div>
 <div class="clearfix"></div>
  <div class="x_content" >
   <div id="quote_scoring_chart_by_status"></div>
  </div>
 </div>
</div>
<?php

// START: This code for left side graph, it's show score of supplier answer
$quoteQuestionniareName = [];
$quoteFormName = Yii::app()->db->createCommand("SELECT qv.* FROM quote_questionnaire qv
WHERE qv.quote_id = " . $quoteID)->queryAll();
foreach ($quoteVendorQuestionnaire as $item) {
  if(!in_array($quoteQuestionniareName, $item['question_form_name']))
 $quoteQuestionniareName[] =  $item['question_form_name'] . " (" . $item['question_scorer'] . "%)";
}

$quoteVendorQuestionnaire = Yii::app()->db->createCommand("SELECT qv.*, q_vend.vendor_id,q_vend.vendor_name, qv.question_form_id as question_form_id 
FROM quote_vendors  q_vend
INNER JOIN quote_questionnaire qv ON  q_vend.quote_id = qv.quote_id
WHERE qv.quote_id=". $quoteID)->queryAll();

$vendorData = [];
foreach ($quoteVendorQuestionnaire as $item) {
    $vendorId = $item['vendor_id'];
    $vendor_name = $item['vendor_name'];
    $score = CommonFunction::SupplierScore($vendorId, $quoteID, $item['question_form_id']);
    if (!isset($vendorData[$vendorId])) {
        $vendorData[$vendorId] = [
            'name' => [],
            'data' => [],
        ];
    }

    $vendorData[$vendorId]['name'][] = $vendor_name;
    $vendorData[$vendorId]['data'][] = number_format($score,2);
}

$data = array_values($vendorData);
foreach ($data as &$subArray) {
  $subArray['name'] = array_unique($subArray['name']);
}
// START: This code for left side graph, it's show score of supplier answer

$scoringTotalQuestion = 0;
$scoringTotalByOpened = Yii::app()->db->createCommand("SELECT count(id) AS open
FROM quote_scorer_questions_form
WHERE status = 1 and quote_id=" . $quoteID)->queryRow();

$scoringTotalByCompleted = Yii::app()->db->createCommand("SELECT count(id) AS completed
FROM quote_scorer_questions_form
WHERE status = 2 and quote_id=" . $quoteID)->queryRow();

$scoringTotalQuest = Yii::app()->db->createCommand(" select count(id) as totalQuestion FROM quote_scorer_questions_form
WHERE quote_id = ".$quoteID." group by  question_id ")->queryAll();
foreach($scoringTotalQuest as $scoringTotalQuestVal){
  $scoringTotalQuestion += $scoringTotalQuestVal['totalQuestion'];
}

$scoringTotalByStatus = [ 
  'open'  => is_nan($scoringTotalByOpened['open'] / $scoringTotalQuestion) ?0:$scoringTotalByOpened['open'] / $scoringTotalQuestion,
  'completed' => is_nan($scoringTotalByCompleted['completed'] / $scoringTotalQuestion) ?0 :$scoringTotalByCompleted['completed'] / $scoringTotalQuestion
];
?>
<script>
$(document).ready(function(){
var usersData = <?= json_encode($data); ?>;
var options = {
  series:usersData,
      chart: {
      type: 'bar',
      height: 350,
      stacked: false,
      toolbar: {
        show: false,
      },        
  },
    plotOptions: {
      bar: {
        horizontal: true,
        dataLabels: {
          total: {
            enabled: true,
            offsetX: 0,
            style: {
              fontSize: '13px',
              fontWeight: 900
            }
          }
        }
      },
    },
    stroke: {
      width: 1,
      colors: ['#fff']
    },
    xaxis: {
      categories: [<?= "'".implode("','",$quoteQuestionniareName)."'"; ?>],
      labels: {
        formatter: function (val) {
          return val.toFixed(2)
        }
      }
    },
    yaxis: {
      title: {
        text: undefined
      },
      labels: {
        maxWidth: 250
      }
    },
    tooltip: {
      y: {
        formatter: function (val) {
          return val.toFixed(2)
        }
      }
    },
    fill: {
      opacity: 1
    },
    legend: {
      position: 'top',
      horizontalAlign: 'left',
      offsetX: 40
    }
    };
    var chart = new ApexCharts(document.querySelector("#quote_scoring_chart"), options);
    chart.render();

    // Set a fixed width for the y-axis labels using CSS
    var yAxisLabels = document.querySelectorAll('.apexcharts-text .apexcharts-yaxis-label');
    yAxisLabels.forEach(function(label) {
      label.style.width = '200px'; // Replace '80px' with your desired width
    });


    var scoringTotalByStatus = <?= json_encode($scoringTotalByStatus); ?>;
     var options = {
          series: [Number(scoringTotalByStatus.open), Number(scoringTotalByStatus.completed)],
          labels: ['Not Complete','Completed'],
          chart: {
          type: 'donut',
          height:500,
        },
        tooltip: {enabled: false },
        responsive: [{
          breakpoint: 900,
          options: {
            chart: {
              width: 200
            },
          }
        }],
         plotOptions: {
          pie: { dataLabels: {  enabled: false  } }
         },
        };

        var chart = new ApexCharts(document.querySelector("#quote_scoring_chart_by_status"), options);
        chart.render();
});
</script>