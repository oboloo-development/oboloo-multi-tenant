
<?php
$sql = " select * from quotes where quote_id=". $quote_id;
$resuilt = Yii::app()->db->createCommand($sql)->queryRow();

$scorerStatusClosed = 0;
$sql = " select status from quote_scorer_questions_form where status=3 and quote_id=".$quote_id;
$resultRes = Yii::app()->db->createCommand($sql)->queryAll();
foreach($resultRes as $resultVal){
  $scorerStatusClosed = 3; 
}

if($scorerStatusClosed == 3)
  $closedScorer = 'disabled';
else
 $closedScorer = '';
?>
<div class="row-fluid tile_count">
  <div class="col-lg-6 col-md-6 col-xs-6 col-xs-12">
    <h4 class="heading">Quote Scorers</h4>
    <h4 class="subheading"><br>Internal scorers and their progress</h4><br /><br />
  </div>
  <div class="col-lg-6 col-md-6 col-xs-6 col-xs-12">
    <?php if($resuilt['user_id'] == Yii::app()->session['user_id'] && $scorerStatusClosed !=3) { ?> 
     <button class="btn btn-info pull-right" data-toggle="modal" data-target="#creatorAddScorerModal">+ Add Scorers</button>
    <?php } ?>
  </div>
  <div class="clearfix"> </div>
  <div class="alert alert-success ">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     <span class="success-alert"></span>
  </div>
  <div class="clearfix"> </div>
  <div class="col-md-12"> 
   <table class="table table-striped table-bordered quote-table " style="width: 100%; margin-bottom:0px !important">
    <thead>
      <tr>
        <th style="width: 10%;"></th>
        <th style="width: 10%;">Username</th>
        <th style="width: 30%;">Questionnaire Section</th>
        <th style="width: 20%;" class="text-center">Scoring Completed</th>
        <th style="width: 10%;" class="text-center">Status</th>
        <?php if(!empty($creator_id) && $creator_id == Yii::app()->session['user_id']) { ?>
        <th style="width: 10%;" class="text-center"></th>
        <?php } ?>
      </tr>
    </thead>
   </table>
   <?php 
   $quoteQuiteScorer = new QuoteScorerUser();
   $quoteQuestionnaire = new QuestionnaireForm();
   $quoteQuiteScorer = $quoteQuiteScorer->getQuoteScorerQuestions($quote_id); 
   $quoteQuestionnaire = $quoteQuestionnaire->getQuoteFormQuestions($quote_id);

   if(!empty($quoteQuiteScorer) && $quoteQuiteScorer >0){ 
    foreach ($quoteQuiteScorer as $quoteScorerValue) {
     $qUserSldForm = new QuoteScorerForm(); ?>
      <form id="scoringForm_<?= $quoteScorerValue['id'] ?>" class="scoringForm">
      <input type="hidden" name="quote_id" value="<?= $quote_id ?>" />
      <input type="hidden" name="scoring_row_id" value="<?= $quoteScorerValue['id'] ?>" />
      <input type="hidden" name="user_id" value="<?= $quoteScorerValue['user_id'] ?>" />
      <input type="hidden" name="user_name" value="<?= $quoteScorerValue['user_name'] ?>" />
      <table id="scoring_table" class="table-bordered quote-table table-striped" style="width: 100%">
        <tbody>
          <tr>
            <td style="width: 10%;">
              <?php if ($quoteScorerValue['status'] == 0 && $creator_id == Yii::app()->session['user_id'] ) { ?>
                <div class="creatorScoringBtn_<?= $quoteScorerValue['id'] ?>">
                <button type="submit" class="btn green-btn openScoring_<?= $quoteScorerValue['id'] ?>" <?= $closedScorer ?>>Open Scoring</button>
                </div>
              <?php } else if ($quoteScorerValue['status'] == 0) { ?>
                <button type="button" class="btn green-btn" disabled>Open Scoring </button>
              <?php } else { ?>
                <?php if(!empty($creator_id) && $creator_id == Yii::app()->session['user_id']){ ?>
                 <div class="creatorScoringBtn_<?= $quoteScorerValue['id'] ?>">
                  <a class="btn btn-primary" style="text-decoration: none" target="_blank" href="<?= AppUrl::bicesUrl('quotes/myscoringdetail/' . $quote_id . '?scorer=' . $quoteScorerValue['user_id']) ?>">View Scoring</a>
                 </div>
                <?php } else{ ?>
                  <button type="button" class="btn btn-primary">View Scoring</button>
                <?php } ?>
              <?php } ?>
            </td>
            <td style="width: 10%;"><?= $quoteScorerValue['user_name']; ?></td>
            <td style="width: 30%;">
              <select name="quote_questionId[]" multiple="multiple" class="form-control quote_question_select_2" required>
              <?php foreach ($quoteQuestionnaire as $questionsForm) : ?>
                <option value="<?= $questionsForm['question_form_id']; ?>" 
                <?= $qUserSldForm->getSelectedQuestionForm((int)$quote_id, (int)$quoteScorerValue['user_id'], $questionsForm['question_form_id']) > 0 ? 'selected="selected"' : '' ?>>
                  <?= $questionsForm['question_form_name']; ?>
                </option>
              <?php endforeach; ?>
              </select>
            </td>
            <td style="width: 20%;" class="text-center">
              <?= // it 2 means completed questionnare
                  CommonFunction::scorerAllQustionnaires($quoteScorerValue['user_id'], $quote_id, 2)
                  ."/". // it 0 means all completed questionnare
                  CommonFunction::scorerAllQustionnaires($quoteScorerValue['user_id'], $quote_id, 0) 
              ?>
            </td>
            <td style="width: 10%;" class="text-center">
            <?php
             $sql = " select status from quote_scorer_questions_form where scoring_row_id=".$quoteScorerValue['id']." and quote_id=".$quoteScorerValue['quote_id']." group by scoring_row_id ";
             $result = Yii::app()->db->createCommand($sql)->queryRow();
            //echo "<pre>"; print_r($sql); echo "</pre>"; exit;
              if(empty($result['status'])){ ?>   
                <button type="button" class="btn btn-pink scoringStatus_<?= $quoteScorerValue['id'] ?>">Not Open</button>
              <?php } else if($result['status'] == 1){ ?>
                <button type="button" class="btn sunshine scoringStatus_<?= $quoteScorerValue['id'] ?>">Open</button>
              <?php } else if($result['status'] == 2){ ?>
                <button type="button" class="btn green-btn scoringStatus_<?= $quoteScorerValue['id'] ?>">Complete</button>
              <?php } else if($result['status'] == 3){?>
                <button type="button" class="btn btn-danger scoringStatus_<?= $quoteScorerValue['id'] ?>">Close</button>
              <?php } ?>
            </td>
            <?php if(!empty($creator_id) && $creator_id == Yii::app()->session['user_id']) { ?>
             <td style="width: 10%; text-align: right;">
             <div class="dropdown">
              <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-h" style="font-size: 15px;"></i>
              </button>
              <?php
                $parameterData = [
                    'quoteID' => base64_encode($quote_id),
                    'id' =>  base64_encode($quoteScorerValue['id']),
                    'userID' =>  base64_encode($quoteScorerValue['user_id'])
                ];
              ?>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?= $this->createUrl('quotes/reopenScoring',$parameterData); ?>">Reopen Scoring</a>
                <a class="dropdown-item" href="<?= $this->createUrl('quotes/closeScoring',$parameterData); ?>">Close Scoring</a>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#closeScoringFoAllScorers">Close Scoring For All Scorers</a>
              </div>
            </div>
            <div class="modal fade" id="closeScoringFoAllScorers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" style="width: 45% !important;">
                <div class="modal-content">
                  <div class="modal-header p-5">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                  <h4 style="padding-bottom: 20px;">Are you sure you want to close scoring for all users?</h4>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <a class="btn btn-primary" href="<?= $this->createUrl('quotes/closeScoringForAll',$parameterData); ?>">Confirm</a>
                  </div>
                </div>
              </div>
            </div>
            
              </div>
             </td>
            <?php } ?>
          </tr>
        </tbody>
      </table>
    </form>
   <?php } 
   } else{?>
    <table id="scoring_table" class="table-bordered quote-table table-striped" style="width: 100%">
      <thead>
        <tr>
          <th style="width: 100%;" class="text-center">No data available in table.</th>
        </tr>
      </thead>
    </table>
   <?php } ?>
  </div>
<div class="clearfix"></div>

<!-- Modal -->
  <div class="modal fade" id="creatorAddScorerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 50%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Add Scorers</h4>
        </div>
        <div class="modal-body">
          <div class="row m-0 ">
            <form id="addScorerUsers"> <? /* action="<?= AppUrl::bicesUrl('quotes/addQuoteUserForScoring'); ?>" method="POST" */ ?> 
              <input type="hidden" name="quote_id" value="<?= $quote_id ?>" />
              <div class="clone_for_more_users row m-0">
                <div class="col-lg-9 col-xs-12 ">
                  <div class="form-group">
                   <label class="form-label">Select Users<span class="text-danger">*</span></label>
                   <select class="form-control quote_internal_user_id quote_scorer_user" name="quote_internal_scorer_user[]" required onChange="if(this.value !== undefined) selectQuoteInternalUser(this.value);" ></select>
                  </div>
                </div>
              </div>
              <div class="new_scorer_row"></div>

              <div class="col-lg-12 col-md-12 col-xs-12 form-group px-3 py-2 text-primary mt-15">
                <button class="btn btn-primary btn-sm add-more-users pull-right" onclick="addMoreUsers()" type="button"><i class="fa fa-plus" aria-hidden="true"></i> Add User</button>
              </div></br>

              <div class="pull-right mt-50">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save" />
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>

<script>
  var selectedScorerIds = [];
  $.ajax({
    type: "POST",
    dataType: "html",
    data: { quoteID: "<?= $quote_id ?>"  },
    url: BICES.Options.baseurl + '/quotes/quoteInternalUser',
    success: function(options) {
      var option = JSON.parse(options);
      $(`.quote_scorer_user`).html(option);
    },
  });

  function selectQuoteInternalUser(value) {
    if (value !== undefined && value !== 'Select') {
      var lowercaseValue = value.toLowerCase();
      selectedScorerIds.push(lowercaseValue);
    } else {
      console.log("myString is undefined");
    }
  }

  var str = 1;
  //   Add new users in select option 
  function addMoreUsers() {
    str += 1;
    $('.new_scorer_row').append(`
      <div class="clone_for_more_users row  m-0 mt-15">
       <div class="form-group">
        <div class="col-lg-9 col-xs-12">
         <select class="form-control quote_internal_user_id quote_scorer_user_more_${ str }" name="quote_internal_scorer_user[]" onChange="if(this.value !== undefined) selectQuoteInternalUser(this.value);" required></select>
        </div>
       </div>
       <div class="col-lg-2 col-xs-12 text-primary removeuser_${str}">
          <button class="btn btn-danger btn-sm removeuser" onclick="removeQuoteScorerUser(${ str })" type="button"><i class="fa fa-remove"></i></button>
        </div>
      </div>
    `);

    // selected user IDs
    $('.add-more-users').show();
    $.ajax({
      type: "POST",
      data: {
        selectedUserIds: selectedScorerIds,
        quoteID: "<?= $quote_id ?>"
      },
      dataType: "html",
      url: BICES.Options.baseurl + '/quotes/quoteInternalUser',
      success: function(options) {
        var option = JSON.parse(options);
        $(`.quote_scorer_user_more_${str}`).html(option);
        // if only remain select in the option 
        if (option.toLowerCase() === '<option>Select</option>') {
          $('.add-more-users').hide();
        }
      },
    });
  }

  // remove QuoteInternalUser this selected quoteUserID
  function removeQuoteScorerUser(quoteUserID) {
    let UserID = $(`.quote_scorer_user_more_${quoteUserID}`).val();
    // Find the index of the value to remove
    $(`.removeuser_${quoteUserID}`).parents(`.clone_for_more_users`).remove();
    $('.add-more-users').show();
  }

  $.ajax({
    type: "POST",
    data: {
      row_number: 1,
      quoteID: "<?= $quote_id ?>"
    },
    dataType: "html",
    url: BICES.Options.baseurl + '/quotes/quoteInternalUser',
    success: function(options) {
      var option = JSON.parse(options);
      $('.quote_internal_user').html(option);
    }
  });

  $(document).ready(function(){
    $('.alert-success').hide();
    var form = $(`.scoringForm`);
    form.submit(function(event) {
      event.preventDefault();
      var currentFormId = this.id;
      const partID = currentFormId.split("_")[1];
      var openScoringSubmit = $(`.openScoring_${Number(partID)}`);
      var scoringStatus = $(`.scoringStatus_${Number(partID)}`);
      var creatorScoringBtn = $(`.creatorScoringBtn_${Number(partID)}`);
      var data = $(this).serialize();
    
      $.ajax({
        url: BICES.Options.baseurl +'/quotes/userOpenForScoring',
        type: 'POST',
        data: data,
        success: function(response) {
          const user = JSON.parse(response);
          openScoringSubmit.text('View Scoring');
          openScoringSubmit.addClass('btn-primary');
          openScoringSubmit.prop('disabled', true);
          scoringStatus.addClass('sunshine');
          window.location.href = user.creatorURL
          scoringStatus.text('Open');
          $('.success-alert').text(`${user.username} added for Open Scoring successfully.`);
          $('.alert-success').show();
          setTimeout(function() {
            $('.alert-success').hide();
          }, 5000);
          
        },
        error: function(error) {
          alert('There was an error submitting your form. Please try again later.');
        }
      });
    });
  });

  $(document).ready(function() {
  // Handler for the submit event of the form
   $("#addScorerUsers").submit(function(event) {
    // Prevent the form from submitting normally
    event.preventDefault();
    var checkRequired = true;
    // Get the data from the form
    var data = $(this).serialize();
    var quote_form_qutestion = $(".quote_internal_user_id");
    quote_form_qutestion.each(function(){
      if($(this).val() == 'Select' || $(this).val() == ''){
        $(this).closest(".form-group").addClass("has-error");
        $(this).after("<span class='help-block text-danger'>This field is required.</span>");
        checkRequired = false;
      }else {
        $(this).closest(".form-group").removeClass("has-error");
        $(".help-block").hide();
      }
     });

    if(checkRequired){
      // Use AJAX to post the data to the server
     $.ajax({
      url: BICES.Options.baseurl + '/quotes/addQuoteUserForScoring',
      type: "POST",
      data: data,
      success: function(response) {
        var option = JSON.parse(response);
        window.location.href = option.redirectURL;
        
      }
     });
    }
  }); 
 });
</script>
<style> 
.btn-group {  width: 100%;  }
.multiselect{border-radius: 30px !important;}
</style>