<div role="main">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3 class="pull-left"><?php echo 'Create Quick Sourcing Evaluation';?></h3>
  <div class="pull-right" style="text-align:center;padding-right: 21px;padding-top: 3px;">
  <span class="step"></span>
</div></div>

    

        <div class="clearfix"> </div>
        <?php if(Yii::app()->user->hasFlash('success')):?>
        <div class="tile_count">
            <div class="alert alert-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>  </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->hasFlash('error')):?>
        <div class="tile_count">
            <div class="alert alert-danger">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>  </div>
        <?php endif; ?>
  

<div class="tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">
    <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/edit'); ?>" autocomplete="off">

      
   
     <div class="tab"> <h3 class="col-md-12 col-sm-12 col-xs-12" style="font-size: 16px !important;">Quote Scoring Criteria</h3> 

      <div class="col-md-8 col-sm-8 col-xs-8 date-input valid">
        <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
        <select  type="text" class="vendor_name_field form-control has-feedback-left"
          name="vendor_name_1" id="vendor_name_1" style="  border-radius: 10px;border: 1px solid #4d90fe;">
      </select>
      <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
      </div>
      <div class="clearfix"></div><br />
     <div class="table-responsive">
      <table class="table">
      <tr>
        <th>Scoring Criteria</th>
        <th>Scoring Percentage</th>
      </tr>
      <tbody>
    <?php $i=1;$scoringCriteriaArr=array();
    foreach($scoringCriteria as $value){
      $scoringCriteriaArr[$value['id']] = $value['value'];?>
      <tr>
      <th><input type="text" class="form-control" disabled="disabled" value="<?php echo $value['value'];?>" style="padding:5px;"></th>
      <th><input type="text" name="score_percentage[<?php echo $value['id'];?>]" class="form-control score-criteria" placeholder="Enter Scoring Percentage" onkeypress="return isNumberKey(event)" onchange="maxScoring(this.value)" required style="padding:5px;"><div class="scoring_alert"  style='color: red;'></div></th>
    </tr>
    <?php } ?>
    
  </tbody></table></div>
  </div>
    
<div>
  <div style="float:right; margin:0 5px;" id="submitBtnCont">
    <button type="button" class="btn btn-info" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
    <button type="button" class="btn btn-success" id="nextBtn" onclick="nextPrev(1)">Next</button>
    <?php if(in_array(Yii::app()->session['user_type'],array(4,3))) { ?>
    <!--  <div class="pull-right">
        <div class="scoring_alert"  style='color: red;'></div>
         <div class="scoring_alert2"  style='color: red;position: absolute;bottom: 96px;'></div>
            <a href="javascript:void(0);" onclick="createQuoteAndValidation()" class="sub_btns">
                <button type="button" class="btn btn-primary btn-functionality">
                    Create Quote
                </button>
            </a>
              <a href="<?php echo $this->createUrl('quotes/listSourcingEvaluation'); ?>" onClick="return confirm('Are you want to cancel?')"  class="sub_btns">
                        <button type="button" class="btn btn-danger btn-functionality">
                           Cancel Quote
                        </button>
                    </a>
    <div class="clearfix"> <br /><br /> </div>
</div> -->
<?php }?>
  </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
</form>
</div>
      <div class="clearfix"> </div>
     </div>
</div>

    <style type="text/css">
        canvas {
            display: block;
            max-width: 800px;
            margin: 60px auto;
        }
    </style>
<script type="text/javascript">
function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
}
function dropdownVendors(obj){
    var testingobj = obj;
    var options_html="";
     $.ajax({
            type: "POST", data: { }, dataType: "json",
            url: BICES.Options.baseurl + '/orders/getVendors',
            success: function(options) {
              console.log(options.suggestions);
                var options_html = '<option value="">Select Supplier</option>';
                for (var i=0; i<options.suggestions.length; i++)
                    options_html += '<option value="' + options.suggestions[i].data + '">' +  options.suggestions[i].value + '</option>';
                     $("#"+testingobj).append(options_html);
               
               
            }
           
        });
}

</script>

<script type="text/javascript">
  var scoring_criteria_arr_inital = <?php echo json_encode($scoringCriteriaArr); ?>;
  var scoring_criteria_arr = <?php echo json_encode($scoringCriteriaArr); ?>;
  var vendor_dropdown_arr = [];
  var vendor_dropdown_arr_id = [];


</script>

<script type="text/javascript">



function setScoringCriterArr(scoring_criteria){
 
}


function makeTable(container, scoring_arr,vendor_arr,vendor_arr_id) {
  var vendor_arr = vendor_arr;
    var table = $("<table/>").addClass('table');
    var i=0;
    var j=0;

    $.each(scoring_arr, function(rowIndex, r) {
        var row = $("<tr/>");
         if(i == 0){
         row.append($("<th/>").text("Scoring Criteria"));
         row.append($("<th/>").text("Scoring Percentage"));

        del=1;
        
         $.each(vendor_arr, function(vendorID, vendorName) { 
row.append($("<td class='"+del+"'/>").html(vendorName.vendorID+"<a style='cursor: pointer; padding: 5px;'  onclick='deleteScoringVendor("+del+")'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a>"));
        del++;
             });
        }
        table.append(row);
        var row = $("<tr/>");
        row.append($("<td/>").text(r));
        row.append($("<td/>").html('<input type="text" name="score_percentage["'+rowIndex+'"]"  class="form-control" placeholder="Enter Scoring Percentage" onkeypress="return isNumberKey(event)" required style="padding:5px;">'));
        var optionExp=""; 
        for (m = 1; m <=vendor_arr_id.length; ++m) {
           optionExp=optionExp+'<option value="'+m+'">'+m+'</option>';
        }
         del=1;
        $.each(vendor_arr_id, function(vendorIndex, vendorInfo) { 
          row.append($("<td class='"+del+"'/>").html('<select name="score_criteria["'+rowIndex+'"]["'+vendorInfo.vendor+'"]>'+ optionExp+'</select>'));
          del++;
             });
          
           i++;
       /* $.each(r, function(colIndex, c) { 

        
            
             $.each(vendor_arr, function(vendorID, vendorName) { 
              row.append($("<td/>").text(vendorName));
             });
        });*/
        table.append(row);
    });
    
    return container.html(table);
}

$(document).ready(function(){
$("#vendor_name_1").change(function(){
   vendorID = $("#vendor_name_1 option:selected ").val();
   vendorName = $("#vendor_name_1 option:selected ").text();
   vendor_dropdown_arr.push({vendorID : vendorName});
   vendor_dropdown_arr_id.push({vendor : vendorID});
  
    
   /* $.each( , function(key, value) {
          vendor_dropdown_arr.push({key : value});
  });*/

   makeTable($('.table-responsive'), scoring_criteria_arr,vendor_dropdown_arr,vendor_dropdown_arr_id);
});

});

$(document).ready(function() {
   
    var cityTable = makeTable($('.table-responsive'), vendor_dropdown_arr,vendor_dropdown_arr);
});
</script>

<script type="text/javascript">
dropdownVendors("vendor_name_1");


</script>


<script type="text/javascript">
function deleteVendor(vendor_idx)
{
  var display_vendor_count = 0;
    $("div[id^='vendor_area']").each(function () {
      if ($(this).is(':visible')) display_vendor_count += 1;
  });
  
  if (display_vendor_count <= 1) alert("You must have at least one vendor field in the quote form");
  else
  {
    if (confirm("Are you sure you want to delete this vendor?"))
    {
      $('#delete_vendor_flag_' + vendor_idx).val(1);
      $('#vendor_area_' + vendor_idx).hide();
      $('#added_vendors_' + vendor_idx).hide();
    }
  }
}

function displayVendorDetails(vendor_idx_rownum,obj)
{
  var vendor_idx = $('#vendor_name_'+vendor_idx_rownum).val();
  if($('#added_vendors_' + vendor_idx).is(':visible')) $('#added_vendors_' + vendor_idx).hide();
  else
  { 
    var display_vendor_id = $('#display_vendor_id_' + vendor_idx).val();
    var my_vendor_id = $('#vendor_id_' + vendor_idx).val();
    if (true) 
    {
      $('#added_vendors_' + vendor_idx).html('');
      populateVendorDetails(vendor_idx_rownum,vendor_idx);
    }
    $('#added_vendors_' + vendor_idx_rownum).show();
  }
}
var currentTab = 0; // Current tab is set to be the first tab (0)
 // Display the current tab
$(".sub_btns").hide();
showTab(currentTab);
function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
    $("#total_price_cont").hide();
  } else {
    document.getElementById("prevBtn").style.display = "inline";
    $("#total_price_cont").show();
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    $("#nextBtn").hide();
    $(".sub_btns").show();

  } else {
    $("#nextBtn").show();
    $(".sub_btns").hide();
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("order_form").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

var requiredArr = ['location_id','department_id_new','currency_id','quote_name','opening_date','closing_date','product_name_1','quantity_1','quote_currency'];

function requiredArrCheck(field_id) {
  return age >= field_id;
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].querySelectorAll("select,input");
  // A loop that checks every input field in the current tab:
 for (i = 0; i < y.length; i++) { 
    // If a field is empty... 
    if ((y[i].value == "" || y[i].value == 0) && (requiredArr.includes(y[i].id) || requiredArr.includes(y[i].class) ) ) {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}


$(".scoring_alert").hide();
$(".scoring_alert2").hide();
function maxScoring(currentFieldValue=''){
  var totalScore = 0;
  $(".scoring_alert").hide();
  $('.score-criteria').each(function(index, currentElement) {
    totalScore=totalScore+parseFloat($(this).val());
    if(totalScore>100){
      $(this).val('');
      $(this).closest('div').find('.scoring_alert').show();
      $(this).closest('div').find('.scoring_alert').html("By Entering this value "+currentFieldValue+" scoring criteria exceeds 100, please, try another");
       return false;
    }
  });

 if(currentFieldValue ==''){
    totalScore = 0;
    $('.score-criteria').each(function(index, currentElement) {
      totalScore=totalScore+parseFloat($(this).val());
    });

    if(totalScore<100){
      $('.scoring_alert2').show();
      $('.scoring_alert2').html("Scoring Criteria must add up to 100%");
       /*alert("Scoring Criteria must add up to 100%");*/
       return false;
    }
    
  }
   return true;
}

function deleteScoringVendor(c){
 $("."+c).hide();
}

function createQuoteAndValidation(){
  if(maxScoring()){
    $('#quote_form').submit();
    $(".btn-functionality").attr('disabled','disabled');
    $(".btn-functionality").prop('disabled',true);
  }

  //(this).prop('disabled',true);
}
</script>
<style type="text/css">
.btn-file {
    padding:6px !important;
}
@media (max-width: @screen-xs-min) {
  .modal-xs { width: @modal-sm; }
}
#save_new_supplier {
  margin-top: 10px;
}


/*
{
  /** width: 100%;
      height: 100%;
      padding: 0;
      margin:0;/**/
   /* width: 90%;
    height: 100%;
    padding: 0;
    margin: 0 auto;
}
.modal-content {    
      height: 100%;
      border-radius: 0;
      color:white;
      overflow:auto;
}
label { color:#73879C; }*/

/* Mark input boxes that gets an error on validation: */
/*#order_form {
  background-color: #ffffff;
  margin: 100px auto;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}*/

/* Style the input fields */


/* Mark input boxes that gets an error on validation: */
.invalid {
  background-color: #ffdddd !important;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none; 
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

/* Mark the active step: */
.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}

.form-control-feedback-new {
    margin-top: 31px !important;
}
</style>
