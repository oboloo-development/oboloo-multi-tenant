<div class="row">
  <div class="col-md-6 quote_draft_case_hide">
      <form method="post" action="<?php echo AppUrl::bicesUrl('quotes/quoteComment'); ?>">
          <input type="hidden" name="quote_id" id="quote_id" value="<?= $quote_id ?>">
          <div class="col-xs-12 pt-15 pb-30">
            <input type="hidden" class="notranslate" name="quote_comments" id="quote_comments" value="1" />
            <label class="control-label notranslate">Comments</label>
            <textarea class="form-control notranslate" name="quote_comments" rows="4" placeholder="Comments"></textarea>
            <button type="submit" class="btn btn-primary mt-15">Save</button>
          </div>
      </form>
  </div>
</div>

<table id="table_scoring_criteria" class="table mt-15">
  <tbody>
    <tr>
      <th>User</th>
      <th>Comments</th>
      <th>Date/Time</th>
    </tr>
    <?php
    foreach ($quoteCommHistory as $valueCommHistory) { ?>
      <tr>
        <td><?php echo $valueCommHistory['user']; ?></td>
        <td class="notranslate"><?php echo $valueCommHistory['comment']; ?></td>
        <td><?php echo $valueCommHistory['datetime']; ?></td>
      </tr>
    <?php } ?>


  </tbody>
</table>

</div>
</div>
<div class="clearfix"></div><br />