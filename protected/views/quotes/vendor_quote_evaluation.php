<?php
$currencySymbol = FunctionManager::currencySymbol('', $quote['quote_currency']);?>

<div class="row m-0">
    <div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_8">
        <div class="x_panel tile overflow_hidden">
            <div class="x_title">
               <h4 class="details_heading">Total Product Cost Comparison</h4>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="text-align: center;">
                <div id="chart" height="140px"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_8">
        <div class="x_panel tile overflow_hidden">
            <div class="x_title">
                <h2 class="details_heading">Top Products</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="tile_info" style="margin-left: 0px;">
                    <?php

                    $idx = 0;
                    foreach ($quote_stacked_bar_graph['products'] as $dataset_idx => $a_dataset)
                    {
                        if ($dataset_idx === 'total') continue;
                        $idx += 1;
                        switch ($idx)
                        {
                            case 1  : $color = 'salmon'; break;
                            case 2  : $color = 'purple'; break;
                            case 3  : $color = 'green'; break;
                            case 4  : $color = 'blue'; break;
                            case 5  : $color = 'red'; break;
                            case 6  : $color = 'antique-white'; break;
                            case 7  : $color = 'aqua-marine'; break;
                            case 8  : $color = 'bisque'; break;
                            case 9  : $color = 'khaki'; break;
                            default : $color = 'turquoise'; break;
                        }
                        ?>
                        <tr>
                            <td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
                            <td style="vertical-align: middle;">
                                <?php echo $a_dataset['product_name']; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format($a_dataset['product_price'], 2); ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row m-0">
     <div class="col-md-12 col-sm-12 col-xs-12" id="table_area_10">
                     <div class="">
                         <table id="table_Supplier_Analysis" class="table table-striped table-bordered " style="width: 100%;">
                             <?php $totalProductPrice = 0;
                             $heatMapSeries = ''; 
                             $vendor = array_unique($quote_stacked_bar_graph['vendor_name']);
                             $idx = 0; 
                             echo "<tr><th>Product/Service Name</th>";
                             $vendorProduct = $quote_stacked_bar_graph['vendorProduct'];
                             asort($vendorProduct);
                             foreach ($vendor as $vendor_name)
                             { ?><th><?php echo $vendor_name;?></th><?php }
                              echo "</tr>";

                            foreach ($vendorProduct as $product_name=>$vendorInfo){?>
                             <tr>
                              <th><strong><?php echo $product_name;$heatMapSeries.="{name:'".$product_name."',data:["; ?></strong></th>
                              <?php foreach ($vendor as $vendor_name){?>
                                    <td><?php 
                                    $productPrice = $vendorProduct[$product_name][$vendor_name];
                                    $productPrice = number_format($productPrice,0,".","");
                                    echo $currencySymbol.$productPrice;
                                    $heatMapSeries.=$productPrice.",";
                                    ?></td>
                            <?php } $heatMapSeries.="]},"; ?>
                            </tr>
                            <?php } ?> 
                            <tr>
                              <td><strong>Total</strong></td>
                              <?php 
                                  $supplierSums = array();
                                  foreach ($vendorProduct as $product_name => $vendorInfo) {
                                      foreach ($vendorInfo as $supplier => $value) {
                                          if (isset($supplierSums[$supplier])) {
                                              $supplierSums[$supplier] += $value;
                                          } else {
                                              $supplierSums[$supplier] = $value;
                                          }
                                      } ?>
                                  <?php }?>
                                  <?php foreach ($supplierSums as $supplier => $sum) { ?>
                                    <td><strong><?= $currencySymbol.$supplierSums[$supplier]; ?></strong></td>
                                  <?php } ?>
                            </tr>
                         </table>
                     </div>

             </div>
</div>
<script>
    var colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];
        var options = {
             chart: {
                height: 350,
                type: 'bar',
                stacked: true,
                toolbar: {
            tools: {
                download: false
            },
          }
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '10%',
                    //endingShape: 'rounded'  
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [<?php echo $quote_stacked_bar_graph['datasets']; ?>],
            xaxis: {
                categories: [<?php echo "'".implode("','",explode(',',$quote_stacked_bar_graph['vendors']))."'"; ?>],
                labels: {
                    style: {
                        colors: colors,
                        fontSize: '12px',
                    }
                }
            },
             /*yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },*/
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return /*"$ " +*/ val /*+ " thousands"*/
                    }
                }
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#chart"),
            options
        );

        chart.render();
        
    var options = {
      chart: {
        height: 350,
        type: 'line',
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      series: [<?php echo $quote_answer_line_graph['vendor_score']?>],
      title: {
        text: '',
        align: 'left'
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      xaxis: {
        categories: [<?php echo $quote_answer_line_graph['answers']; ?>],
      },
      markers: {size: 5,hover: {size: 6},}
    }

    var chart = new ApexCharts(
      document.querySelector("#myChart"),
      options
    );
    chart.render();

     var options = {
      chart: {
        height: 200,
        type: 'heatmap',
      },
      stroke: {
        width: 0
      },
      plotOptions: {
        heatmap: {
          radius: 30,
          enableShades: false,
          shadeIntensity: 3,
          reverseNegativeShade: true,
          distributed: true,
          colorScale: {
          },

        }
      },
      dataLabels: {
        enabled: true,
        style: {
          colors: ['#fff']
        }
      },
      series: [<?php echo $heatMapSeries;?>],

      xaxis: {
        categories: [<?php echo "'".implode("','",$vendor)."'";?>],
        type: 'category',
      },
      
    }

    var chart = new ApexCharts(
      document.querySelector("#chartheatmap"),
      options
    );

    chart.render();


    $('#table_Supplier_Analysis').dataTable({
       "pageLength": 50,
        "columnDefs": [ 
          { "targets": 0, "width": "6%", "orderable": false },        
        ],
        "order": []
    });
</script>