<div class="modal fade" id="re_open_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style="width: 60%; margin:auto;"> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-size: 20px;"><b>Re-open Quote</b></h4>
         <p>Reopening a quote allows your suppliers to edit their submission and add more documents. You can choose to reopen it to specific suppliers or all of them.</p>
        <p>You can reopen a quote as many times as you like and you can see historic submissions under the Submission History tab</p>
    </div>
    <form id="reopen_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/reopenQuote'); ?>" novalidate>    
        <div class="form-group">
           <div class="col-md-12 col-md-12 col-xs-12 date-input valid">
                <label class="control-label">Supplier <span style="color: #a94442;">*</span></label><br /><br />
                 <select class="form-control notranslate reopen-dualbox" name="reopen_vendor[]" multiple="multiple" id="reopen_vendor" required="required">
  
                 <?php foreach($quote_vendors as $value){ ?>
                    <option value="<?php echo $value['vendor_id'];?>">
                       <?php echo $value['vendor_name'];?></option>
                  <?php } ?>
                </select>
                <span class="reopen-vendor-error reopen-erro"></span>
            </div>
        </div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
              <label class="control-label">Opening Date (UTC)<span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control notranslate" name="quote_start_date" id="quote_start_date"
                    <?php if (isset($quote['opening_date']) && !empty($quote['opening_date'])) echo 'value="' . date($dateFormate." H:iA", strtotime($quote['opening_date'])) . '"'; else echo 'placeholder="Opening Date"'; ?> >
              
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
              <label class="control-label">Closing Date  (UTC)<span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control notranslate" name="qoute_end_date" id="qoute_end_date" <?php if (isset($quote['closing_date']) && !empty($quote['closing_date'])) echo 'value="' . date($dateFormate." H:iA", strtotime($quote['closing_date'])) . '"'; else echo 'placeholder="Closing Date"'; ?> >
               
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label">Description/Notes <span style="color: #a94442;">*</span></label>
              <textarea class="form-control notranslate" name="reopen_notes" id="reopen_notes" rows="4" placeholder="description/notes" required="required"></textarea>
               <span class="reopen-desc-error reopen-erro"></span>
          </div>
        </div>
    <input type="hidden" class="notranslate" name="quote_id"  value="<?php echo $quote_id;?>" />
  <div class="modal-footer">
    <?php $disabled = FunctionManager::sandbox(); ?>
    <button type="submit" class="btn btn-success submit-btn1"  <?php echo $disabled; ?>>Submit</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <?php 
      $sql = "SELECT qrh.quote_start_date,qrh.qoute_end_date,qrh.created_at,qrh.created_by_name,v.vendor_name FROM quote_reopen_history qrh INNER JOIN vendors v on qrh.vendor_id=v.vendor_id where qrh.quote_id=".$quote_id;
      $reopnReader = Yii::app()->db->createCommand($sql)->query()->readAll();
      if(!empty($reopnReader)){?>
          <br /><br />
          <table class="table table-striped table-bordered" style="width: 100%; text-align: left">
          <thead>
            <tr>
              <th>Supplier</th>
              <th>Opening Date</th>
              <th>Closing Date</th>
              <th>User</th>
              <th>Re-Open Date</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($reopnReader as $value){ ?>
              <tr>
                <td><?php echo $value['vendor_name'];?></td>
                <td><?php if ($value['quote_start_date'] !="0000-00-00 00:00:00") 
                        echo date(FunctionManager::dateFormat()." H:iA", strtotime($value['quote_start_date']));
                      ?></td>
                <td><?php if ($value['qoute_end_date'] !="0000-00-00 00:00:00") 
                        echo date(FunctionManager::dateFormat()." H:iA", strtotime($value['qoute_end_date']));
                      ?></td>
                <td><?php echo $value['created_by_name'];?></td>
                <td><?php if ($value['created_at'] !="0000-00-00 00:00:00") 
                            echo date(FunctionManager::dateFormat()." H:iA", strtotime($value['created_at']));
                    ?></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } ?>
  </div></form></div></div></div>