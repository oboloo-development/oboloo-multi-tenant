
<div class="modal fade" id="award_supplier_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style="width: 60%; margin:auto;"> 

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-size: 20px;"><b>Award Supplier</b></h4>
        <p><b>Please note: </b>oboloo does not notify awarded or unawarded suppliers. Please ensure that you contact suppliers included within this sourcing event to notify them if they have or haven't been awarded.</p>
      </div>

    <form id="order_invoice_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/awardSupplier'); ?>">    
        <div class="form-group">
           <div class="col-md-6 col-sm-6 col-xs-10 date-input valid">
                <label class="control-label">Supplier<span style="color: #a94442;">*</span></label>
                 <select class="form-control notranslate" name="award_vendor" id="award_vendor" required="required">
                   <option value="">Select</option>
                 <?php foreach($quote_vendors as $value){ ?>
                    <option value="<?php echo $value['vendor_id'];?>">
                       <?php echo $value['vendor_name'];?></option>
                  <?php } ?>
                </select>
            </div>
     </div>
    <input type="hidden" name="quote_id"  value="<?php echo $quote_id;?>" />
  <div class="modal-footer">
    <?php $disabled = FunctionManager::sandbox(); ?>
    <button type="submit" class="btn btn-success submit-btn"  <?php echo $disabled; ?>>Award Supplier</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div></form></div></div></div>