<div class="modal fade" id="contract_supplier_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style="width: 60%; margin:auto;">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"> &nbsp;Create Contract</h4>
      </div>

    <form id="order_invoice_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/createContract'); ?>">    
       <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12 valid">
          <label class="control-label">Contract Title <span style="color: #a94442;">*</span></label>
          <input required type="text" class="form-control" name="contract_title"
            id="contract_title"
            <?php if (isset($quote['quote_name']) && !empty($quote['quote_name'])) echo 'value="' . $quote['quote_name'] . '"'; else echo 'placeholder="Contract Title"'; ?>>
          
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12 valid">
          <label class="control-label">Supplier <span style="color: #a94442;">*</span></label>
          <input required type="text" class="form-control" name=""
            id=""
            <?php if (isset($quote['awarded_vendor_name']) && !empty($quote['awarded_vendor_name'])) echo 'value="' . $quote['awarded_vendor_name'] . '"'; else echo 'placeholder="Supplier"'; ?> readonly>
            <input type="hidden" name="vendor_id" id="vendor_id" <?php if (isset($quote['awarded_vendor_id']) && !empty($quote['awarded_vendor_id'])) echo 'value="' . $quote['awarded_vendor_id'] . '"'; ?>>
        </div>
      </div>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6 valid">
              <label class="control-label">Location <span style="color: #a94442;">*</span></label>
        <select required name="location_id" id="location_id_contract" class="form-control" onchange="loadDepartmentsForSingleLocationContract(0);">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($quote['location_id']) && $location['location_id'] == $quote['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-6 valid">
              <label class="control-label">Department <span style="color: #a94442;">*</span></label>
              <select required name="department_id" id="department_id_contract" class="form-control">
                  <option value="">Select Department</option>
              </select>
          </div>
      </div>
   
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <label class="control-label">Category</label>
          <select name="contract_category_id" id="contract_category_id"
            class="form-control" onchange="loadSubcategoriesContract(0);">
            <option value="0">Select Category</option>
          <?php foreach ($categories as $category) { ?>
            <option value="<?php echo $category['id']; ?>"
              <?php if (isset($quote['category_id']) && $quote['category_id'] == $category['id']) echo ' selected="SELECTED" '; ?>>
              <?php echo $category['value']; ?>
            </option>
          <?php } ?>
        </select>
        </div>
        <div class="col-md-6 col-sm-6  col-xs-12">
          <label class="control-label">Sub Category</label>
          <select name="contract_subcategory_id" id="contract_subcategory_id"
            class="form-control">
            <option value="0">Select Sub Category</option>
          </select>
        </div>
      </div>

    <div class="form-group">
  <div class="col-md-12 col-sm-12 col-xs-12 valid">
    <label class="control-label">Currency Rate </label>
    <select name="currency_id" id="currency_id" class="form-control notranslate" >

    <?php foreach ($currency_rate as $rate) { ?>
      <option value="<?php echo $rate['id']; ?>" <?php if (!empty($quote['quote_currency']) && $quote['quote_currency']== $rate['currency']) echo ' selected="SELECTED" '; ?>>
      <?php echo $rate['currency']; ?>
      </option>
      <?php } ?>
    </select>
  </div>
  </div>

    <input type="hidden" name="quote_id"  value="<?php echo $quote_id;?>" />
  <div class="modal-footer">
    <button type="submit" class="btn btn-success submit-btn">Create Contract</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div></form></div></div></div>

  <script type="text/javascript">
    function loadDepartmentsForSingleLocationContract(department_id)
   {
    var location_id = $('#location_id_contract').val();
    var single = 1;

    if (location_id == 0)
        $('#department_id_contract').html('<option value="">Select Department</option>');
    else
    {
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getDepartments',
            success: function(options) {
                var options_html = '<option value="">Select Department</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                $('#department_id_contract').html(options_html);
                if (department_id_contract != 0) $('#department_id_contract').val(department_id);
            },
            error: function() { $('#department_id_contract').html('<option value="">Select Department</option>'); }
        });
    }
}

loadDepartmentsForSingleLocationContract(<?php echo $quote['department_id']; ?>);

  </script>