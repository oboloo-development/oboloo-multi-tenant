<?php 
$quoteVendorName = Yii::app()->db->createCommand("SELECT qv.quote_id,qv.vendor_id,qv.vendor_name FROM quote_vendors qv
WHERE qv.quote_id = ".$quote['quote_id']." AND submit_status = 1 ")->queryAll();

$quoteVendorQuestionnaire = Yii::app()->db->createCommand("SELECT qv.*, qvq.* FROM quote_questionnaire qv
 inner join quote_vendor_questionnaire qvq on qvq.quote_id = qv.quote_id and qv.question_form_id = qvq.question_form_id
  WHERE qv.quote_id=".$quote['quote_id']." 
  group by qv.question_form_name order by qv.question_order asc ")->queryAll();
?>
<div class="table-responsive">
  <?php /*
  <!--START: Supplier Team Table comming through Ajax -->
  <div class="col-lg-6 col-md-6 col-xs-6 col-xs-12 pl-0">
   <h4 class="subheading">Supplier Team</h4>
   <div class="clearfix"></br></div>
   <?php
   $sql = " SELECT vendor_id, vendor_name FROM quote_vendors WHERE quote_id=". $quote['quote_id'];
   $resuilt = Yii::app()->db->createCommand($sql)->queryAll();
   ?>
   <table class="table table-striped" id="scoring_team" style="width:100%">
    <thead>
      <tr>
        <th>Supplier name</th>
        <th class="text-center">Questionnaires Completed</th>
      </tr>
    </thead> 
     <tbody>
      <?php foreach($resuilt as $value){ ?>
       <tr>
        <td><?= $value['vendor_name']; ?></td>
        <td class="text-center"><?= CommonFunction::supplierTotalQuestionnaireByStatus($quote['quote_id'], $value['vendor_id'], 'Completed'); ?> / <?= CommonFunction::supplierTotalQuestionnaireByStatus($quote['quote_id'], $value['vendor_id'], 'all'); ?></td>
       </tr>
      <?php } ?>
     </tbody>  
   </table>
  </div>
  <div class="col-lg-6 col-md-6 col-xs-6 col-xs-12"></div>
  <div class="clearfix"></br></div>
  <!--END: Supplier Team Table comming through Ajax -->
  */ ?>
  <div class="row pl-0">
  <!-- START page render partial _scoring_criteria_graphs -->
   <?php $this->renderPartial(
    '_scoring_criteria_graphs', 
    ['quoteVendorName' => $quoteVendorName ,
     'quoteVendorQuestionnaire' => $quoteVendorQuestionnaire,
     'quoteID' => $quote['quote_id']
     ]); ?>
  <!-- END page render partial _scoring_criteria_graphs -->
  </div>
  <br>

  <table class="table table-striped" style="width: 100%">
    <thead>
      <tr>
        <th width="20%"><?= 'Questionnaire Name'; ?></th>
        <?php foreach ($quoteVendorName as $quoteVendQues) { ?>
          <th><?= $quoteVendQues['vendor_name']; ?></th>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
    <?php $overallScoreArr = [];  
    foreach($quoteVendorQuestionnaire as $vQuestionnaire){ ?>
     <tr>
      <td width="20%"><?= $vQuestionnaire['question_form_name'] . " ( " . $vQuestionnaire['question_scorer'] . " % )"; ?></td>
      <?php foreach ($quoteVendorName as $quoteVendQues) { 
      $supplierScore = CommonFunction::SupplierScore($quoteVendQues['vendor_id'], $quote['quote_id'],$vQuestionnaire['question_form_id']);
      $overallScoreArr[$quoteVendQues['vendor_id']] = $overallScoreArr[$quoteVendQues['vendor_id']]+$supplierScore;
      ?>
       <td width="10%"><?= number_format($supplierScore,2)." %"; ?></td>
      <?php } ?> 
     </tr>
    <?php } ?>
     <tr>
      <td width="20%"><b>Overall Score</b></td>
      <?php foreach ($overallScoreArr as $overallScore) { ?>
       <td width="10%"><b><?= number_format($overallScore,2)." %"; ?></b></td>
      <?php } ?> 
     </tr>
    </tbody>
  </table>
</div>
