<div class="right_col" role="main">
<br /><br />
<div class="col-md-2 col-sm-2 col-xs-2">	
    <img class="img-responsive" src="<?php echo AppUrl::bicesUrl('images/app_logo.png'); ?>" />
</div>
<div class="col-md-10 col-sm-10 col-xs-10">
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3 style="color: #fff;">
            	Submit Quote Details
            </h3>
        </div>
        <div class="clearfix"> </div>
    </div>

<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs" style="color: #fff;">
  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background: none !important;">
    <li role="presentation" class="active">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="quote-tab" role="tab" data-toggle="tab" aria-expanded="true">
            Quote Details
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content2" role="tab" id="document-tab" data-toggle="tab" aria-expanded="false">
            Documents
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content3" role="tab" id="question-tab" data-toggle="tab" aria-expanded="false">
            Questionnaire
        </a>
    </li>
  </ul>
  <div class="clearfix"> </div>

  <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/submitQuoteDetails'); ?>">

  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">

		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<h5>Quote Information</h5>
			</div>
		</div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
			  <label class="control-label">Quote Name</label>
              <input type="text" class="form-control has-feedback-left" name="quote_name" id="quote_name" disabled="disabled"
                    <?php if (isset($quote['quote_name']) && !empty($quote['quote_name'])) echo 'value="' . $quote['quote_name'] . '"'; else echo 'placeholder="Quote Name"'; ?> >
              <span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
          </div>
	  </div>

      <div class="form-group">
          <div class="col-md-2 col-sm-2 col-xs-2 date-input">
			  <label class="control-label">Contact Name</label>
              <input type="text" class="form-control has-feedback-left" name="full_name" id="full_name" disabled="disabled"
                    <?php if (isset($quote['full_name']) && !empty($quote['full_name'])) echo 'value="' . $quote['full_name'] . '"'; else echo 'placeholder="Quote Created By"'; ?> >
              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-2 date-input">
			  <label class="control-label">Contact Phone</label>
              <input type="text" class="form-control has-feedback-left" name="contact_phone" id="contact_phone" disabled="disabled"
                    <?php if (isset($quote['contact_phone']) && !empty($quote['contact_phone'])) echo 'value="' . $quote['contact_phone'] . '"'; else echo 'placeholder="Contact Phone"'; ?> >
              <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-2 date-input">
			  <label class="control-label">Contact Email</label>
              <input type="text" class="form-control has-feedback-left" name="contact_email" id="contact_email" disabled="disabled"
                    <?php if (isset($quote['contact_email']) && !empty($quote['contact_email'])) echo 'value="' . $quote['contact_email'] . '"'; else echo 'placeholder="Contact Email"'; ?> >
              <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
          </div>
	  </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6">
			  <label class="control-label">Vendor Name</label>
              <input type="text" class="form-control" name="vendor_name" id="vendor_name" disabled="disabled"
                    <?php if (isset($vendor['vendor_name']) && !empty($vendor['vendor_name'])) echo 'value="' . $vendor['vendor_name'] . '"'; else echo 'placeholder="Vendor Name"'; ?> >
          </div>
	  </div>

		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-6 date-input">
				<label class="control-label">Quote Currency</label>
				<input type="text" class="form-control has-feedback-left" name="quote_currency" id="quote_currency" disabled="disabled"
					<?php if (isset($quote['quote_currency']) && !empty($quote['quote_currency'])) echo 'value="' . $quote['quote_currency'] . '"'; else echo 'placeholder="Quote Currency"'; ?> >
				<span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
			</div>
		</div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6">
			  <label class="control-label">Description/Notes</label>
              <textarea class="form-control" placeholder="Notes/comments for the quote" name="notes" id="notes"><?php if (isset($quote['quote_desc']) && !empty($quote['quote_desc'])) echo $quote['quote_desc']; ?></textarea>
          </div>
	  </div>

      <?php
        $existing_items_found = false;
        if (isset($quote_details) && is_array($quote_details) && count($quote_details))
            $existing_items_found = true;
      ?>

      <?php
	    $i=1;
        $total_price = 0;
        if (isset($quote_details) && is_array($quote_details) && count($quote_details))
        {
      ?>

          <?php

            foreach ($quote_details as $quote_detail)
            {
                if (!isset($quote_detail['quantity']) || empty($quote_detail['quantity'])) $quote_detail['quantity'] = 1;
                if (!isset($quote_detail['unit_price']) || empty($quote_detail['unit_price'])) $quote_detail['unit_price'] = 0;
          ?>

              <div class="form-group">
                  <div class="col-md-3 col-sm-3 col-xs-3 date-input">
					  <label class="control-label">Product Name</label>
                      <input type="text" class="form-control has-feedback-left" name="product_name[]" readonly="readonly"
                            <?php if (isset($quote_detail['product_name']) && !empty($quote_detail['product_name'])) echo 'value="' . $quote_detail['product_name'] . '"'; ?> >
                      <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-1 col-sm-1 col-xs-1">
					  <label class="control-label">Quantity</label>
                      <input style="text-align: right;" type="text" class="qty form-control" name="quantity[]" readonly="readonly"
                            <?php if (isset($quote_detail['quantity']) && !empty($quote_detail['quantity'])) echo 'value="' . $quote_detail['quantity'] . '"'; ?> >
                  </div>

                  <div class="col-md-1 col-sm-1 col-xs-1">
					  <label class="control-label">UOM</label>
                      <input style="text-align: right;" type="text" class="form-control" name="uom[]" readonly="readonly"
                            <?php if (isset($quote_detail['uom']) && !empty($quote_detail['uom'])) echo 'value="' . $quote_detail['uom'] . '"'; else echo 'Units'; ?> >
                  </div>

                  <div class="col-md-1 col-sm-1 col-xs-1">
					  <label class="control-label">Unit Price</label>
                      <input style="text-align: right;" type="text" class="form-control" name="price[]" readonly="readonly" placeholder="Current Unit Price (If Applicable)"
                            <?php if (isset($quote_detail['price']) && !empty($quote_detail['price'])) echo 'value="' . $quote_detail['price'] . '"'; ?> >
                  </div>


              </div>

              <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-6">
					  <label class="control-label">Product Notes</label>
                      <input type="text" class="form-control" name="vendor_product_notes[]" placeholder="Product Notes"
                            <?php if (isset($quote_detail['vendor_product_notes']) && !empty($quote_detail['vendor_product_notes'])) echo 'value="' . $quote_detail['vendor_product_notes'] . '"'; ?> >
                  </div>
              </div>

				<div class="form-group">

					<div class="col-md-3 col-sm-3 col-xs-3">
						<label class="control-label">New Unit Price</label>
						<input onblur="calculateProductPrice(<?php echo $i; ?>)" onkeyup="calculateProductPrice(<?php echo $i; ?>)" type="text" class="price_calc product_unit_price form-control unit_<?php echo $i; ?>" id="unit_price_<?php echo $i; ?>" data-value="<?php echo $quote_detail['quantity']; ?>" name="unit_price[]" placeholder="Price Per Unit"
							<?php if (isset($quote_detail['unit_price']) && !empty($quote_detail['unit_price'])) echo 'value="' . $quote_detail['unit_price'] . '"'; ?> >
					</div>

					<div class="col-md-3 col-sm-3 col-xs-3">
						<label class="control-label">Tax Rate %</label>
						<input onblur="calculateProductPrice(<?php echo $i; ?>)" onkeyup="calculateProductPrice(<?php echo $i; ?>)" type="text" class="tax_calc product_tax_rate form-control unit_<?php echo $i; ?>" id="tax_rate_<?php echo $i; ?>" data-value="<?php echo $quote_detail['quantity']; ?>" data-id="<?php echo $i; ?>" name="product_tax_rate[]"  placeholder="Tax Rate %"
							<?php if (isset($quote_detail['product_tax_rate']) && !empty($quote_detail['product_tax_rate'])) echo 'value="' . $quote_detail['product_tax_rate'] . '"'; ?> >
					</div>

				</div>

				<div class="form-group">

					<div class="col-md-3 col-sm-3 col-xs-3">
						<label class="control-label">Shipping</label>
						<input onblur="calculateProductPrice(<?php echo $i; ?>)" onkeyup="calculateProductPrice(<?php echo $i; ?>)" type="text" class="shipping_calc product_shipping_charges form-control unit_<?php echo $i; ?>" id="shipping_charges_<?php echo $i; ?>" name="product_shipping_charges[]" placeholder="Shipping Charges"
							<?php if (isset($quote_detail['product_shipping']) && !empty($quote_detail['product_shipping'])) echo 'value="' . $quote_detail['product_shipping'] . '"'; ?> >
					</div>

					<div class="col-md-3 col-sm-3 col-xs-3">
						<label class="control-label">Other Charges</label>
						<input onblur="calculateProductPrice(<?php echo $i; ?>)" onkeyup="calculateProductPrice(<?php echo $i; ?>)"  type="text" class="other_calc product_other_charges form-control unit_<?php echo $i; ?>" id="other_charges_<?php echo $i; ?>" name="product_other_charges[]" placeholder="Other Charges"
							<?php if (isset($quote_detail['product_other_charges']) && !empty($quote_detail['product_other_charges'])) echo 'value="' . $quote_detail['product_other_charges'] . '"'; ?>>
					</div>

				</div>

				<?php
				if (isset($quote_detail['unit_price']) && !empty($quote_detail['unit_price'])){

					$total_unit_price = $quote_detail['unit_price']*$quote_detail['quantity'];
					$product_tax_rate_price = ($quote_detail['product_tax_rate']/100)*$total_unit_price;

					$product_total_price = $total_unit_price+$product_tax_rate_price+$quote_detail['product_shipping']+$quote_detail['product_other_charges'];
				}

				?>


				<div class="form-group">

					<div class="col-md-3 col-sm-3 col-xs-3">
						&nbsp;
					</div>

					<div class="col-md-3 col-sm-3 col-xs-3">
						<label class="control-label">Product Total Price</label>
						<input style="text-align: right;" type="text" class="form-control product_total_price_<?php echo $i; ?>"  id="product_total_price_<?php echo $i; ?>" name="product_total_price[]" placeholder="Product Total Price" <?php if (isset($product_total_price) && !empty($product_total_price)) echo 'value="' . $product_total_price . '"'; ?> >
					</div>


				</div>
              
              <div class="clearfix"> <br /> </div>

          <?php $i++;
				$total_price += $quote_detail['unit_price'];
			} ?>

      <?php
            }
      ?>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" name="submit_quote_id" id="submit_quote_id" value="<?php if (isset($submit_quote_id)) echo $submit_quote_id; ?>" />
            <input type="hidden" name="quote_id" id="quote_id" value="<?php if (isset($quote_id)) echo $quote_id; ?>" />
            <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($vendor_id)) echo $vendor_id; ?>" />
            <input type="hidden" name="quote_vendor_id" id="quote_vendor_id" value="<?php if (isset($quote_vendor_id)) echo $quote_vendor_id; ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
        </div>
    </div>
    <div style="float: left; width:100%;margin-left: -180px;">
    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-4">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2 text-right">
      		Tax Amount
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2">
            <input type="text" class="price_calc form-control text-right" name="tax_rate" id="total_tax_rate"
                <?php if (isset($quote['tax_rate']) && !empty($quote['tax_rate'])) echo 'value="' . $quote['tax_rate'] . '"'; else echo 'placeholder="Tax Amount"'; ?> >
      	</div>
    </div>

    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-4">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2 text-right">
      		Shipping Charges
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2">
            <input type="text" class="price_calc form-control text-right" name="shipping" id="total_shipping"
                <?php if (isset($quote['shipping']) && !empty($quote['shipping'])) echo 'value="' . $quote['shipping'] . '"'; else echo 'placeholder="Shipping"'; ?> >
      	</div>
    </div>

    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-4">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2 text-right">
      		Other Charges
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2">
            <input type="text" class="price_calc form-control text-right" name="other_charges" id="total_other_charges"
                <?php if (isset($quote['other_charges']) && !empty($quote['other_charges'])) echo 'value="' . $quote['other_charges'] . '"'; else echo 'placeholder="Other Charges"'; ?> >
      	</div>
    </div>

    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-4">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2 text-right">
      		Discount
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2">
            <input type="text" class="price_calc form-control text-right" name="discount" id="discount"
                <?php if (isset($quote['discount']) && !empty($quote['discount'])) echo 'value="' . $quote['discount'] . '"'; else echo 'placeholder="Discount"'; ?> >
      	</div>
    </div>

    <div class="form-group">
      	<div class="col-md-4 col-sm-4 col-xs-4">
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2 text-right">
      		Total Price
      	</div>

      	<div class="col-md-2 col-sm-2 col-xs-2">
            <input type="text" class="form-control text-right" name="total_price" id="total_price" readonly="readonly"
                <?php if (isset($quote['total_price']) && !empty($quote['total_price'])) echo 'value="' . $quote['total_price'] . '"'; else echo 'placeholder="Total Price"'; ?> >
      	</div>
    </div>
	</div>
    </div>

	  <div class="clearfix"> </div>
    
    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="document-tab">
		<?php 
		    // integer starts at 0 before counting
		    $existing_files = array(); 
		    $upload_dir = 'uploads/quotes/';
			if ($quote_id)
			{
				if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
				if (!is_dir('uploads/quotes/' . $quote_id)) 
					mkdir('uploads/quotes/' . $quote_id);
				$upload_dir = 'uploads/quotes/' . $quote_id . '/';
			    if ($handle = opendir($upload_dir)) {
			        while (($uploaded_file = readdir($handle)) !== false){
			            if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file)) 
			                $existing_files[] = $uploaded_file;
			        }
			    }
			}
		?>

		      <div class="form-group">
		          <div class="col-md-6 col-sm-6 col-xs-6">
		              <h5>Quote Documents</h5>
		          </div>
		      </div>
		

		<?php if (is_array($existing_files) && count($existing_files)) { ?>
		      
	          <div class="col-md-6 col-sm-6 col-xs-6">
		      <table class="table">
		      <?php $file_idx = 1; foreach ($existing_files as $uploaded_file) { ?>
		      	<tr>
		      		<td>
                        <a href="<?php echo AppUrl::bicesUrl('uploads/quotes/' . $quote_id . '/' . $uploaded_file); ?>" target="quote_file">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
		      		</td>
		      		<td>
		      			<?php echo $uploaded_file; ?>
		      		</td>
		      		<td>
		      			<?php
		      				$file_description = "";
		      				if (isset($quote_files) && is_array($quote_files))
		      				{
								foreach ($quote_files as $a_quote_file)
								{
									if ($a_quote_file['file_name'] == $uploaded_file)
									{
										$file_description = $a_quote_file['description'];
										break;
									}
								}
							}
							echo $file_description;
		      			?>
		      		</td>
		      	</tr>
		      <?php $file_idx += 1; } ?>
		      </table>
		      </div>

		      <div class="clearfix"> <br /> </div>
		
        <?php } ?>    	

		<?php 
		    // integer starts at 0 before counting
		    $your_existing_files = array(); 
		    $upload_dir = 'uploads/quotes/';
			if ($quote_id)
			{
				if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
				if (!is_dir('uploads/quotes/' . $quote_id)) 
					mkdir('uploads/quotes/' . $quote_id);
				if (!is_dir('uploads/quotes/' . $quote_id . '/' . $vendor_id))
				    mkdir('uploads/quotes/' . $quote_id . '/' . $vendor_id);
				$upload_dir = 'uploads/quotes/' . $quote_id . '/' . $vendor_id;
			    if ($handle = opendir($upload_dir)) {
			        while (($uploaded_file = readdir($handle)) !== false){
			            if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file)) 
			                $your_existing_files[] = $uploaded_file;
			        }
			    }
			}
		?>

		      <div class="form-group">
		          <div class="col-md-6 col-sm-6 col-xs-6">
		          	<h5>
                      	Quote Documents
						<?php if (!isset($quote['submit_status']) || $quote['submit_status'] == 0) { ?>                      	
                          <div class="pull-right" style="font-size: 70%; padding-top: 5px;">
                              <a style="cursor: pointer; text-decoration: underline;"
                                    onclick="$('#here_is_where_you_add_documents_to_this_quote').toggle();">
                                  Click here</a>
                              To Add Documents for this Quote
                          </div>
                        <?php } ?>
                    </h5>
		          </div>
		      </div>
		

		<?php if (is_array($your_existing_files) && count($your_existing_files)) { ?>
		      
	          <div class="col-md-6 col-sm-6 col-xs-6">
		      <table class="table">
		      <?php $file_idx = 1; foreach ($your_existing_files as $uploaded_file) { ?>
		      	<tr id="existing_file_id_<?php echo $file_idx; ?>">
		      		<td>
                        <a href="<?php echo AppUrl::bicesUrl('uploads/quotes/' . $quote_id . '/' . $uploaded_file); ?>" target="quote_file">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
						<?php if (!isset($quote['submit_status']) || $quote['submit_status'] == 0) { ?>
	                        &nbsp;
							<a id="delete_quote_file_link_<?php echo $file_idx; ?>" style="cursor: pointer; color: #fff;" 
									onclick="deleteQuoteFile(<?php echo $file_idx; ?>, '<?php echo $uploaded_file; ?>');">
							   <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</a>                        
						<?php } ?>
		      		</td>
		      		<td>
		      			<?php echo $uploaded_file; ?>
		      		</td>
		      	</tr>
		      <?php $file_idx += 1; } ?>
		      </table>
		      </div>

		      <div class="clearfix"> <br /> </div>
		
        <?php } ?>    	

        <div id="here_is_where_you_add_documents_to_this_quote" style="display: none;">
		<?php if (!isset($quote['submit_status']) || $quote['submit_status'] == 0) { ?>
		    <div class="form-group" id="document_area_1">
		    	<div class="col-md-5 col-sm-5 col-xs-5">
		    		<input class="form-control" type="file" name="quote_file_1" id="quote_file_1" />
		    	</div>
		    	<div class="col-md-1 col-sm-1 col-xs-1">
		          	 <a onclick="deleteDocument(1);" title="Click here to delete this document" style="cursor: pointer; color: #fff;">
		          	 	<span class="fa fa-minus fa-2x"></span>
		          	 </a>
		          	 &nbsp;
		          	 <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer; color: #fff;">
		          	 	<span class="fa fa-plus fa-2x"></span>
		          	 </a>
		    	</div>
		    </div>
			<input type="hidden" name="delete_document_flag_1" id="delete_document_flag_1" value="0" />
			<div class="clearfix" id="end_document_area_1"> <br /> </div>
        <?php } ?>
        
        <input type="hidden" name="total_documents" id="total_documents" value="1" />
	    </div>

    </div>

    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="question-tab">
		<div class="right_col" role="main">
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h5>Quote Questionnaire</h5>
				</div>
			</div>

			<div class="form-group">
				<?php
				$i = 1;
				if (isset($quote_questions) && is_array($quote_questions))
				{
					foreach ($quote_questions as $quote_question)
					{
						$quoteAnswers = new QuoteAnswer();
						$quoteVendorAnswers = new QuoteVendorAnswer();
						$quoteAnswers = $quoteAnswers->getQuoteAnswers($quote_question['id']);
						$quoteVendorAnswers = $quoteVendorAnswers->getQuoteVendorAnswers($quote_question['quote_id'],$quote_question['id'],$vendor_id);

						?>


						<div class="col-md-5 col-sm-5 col-xs-10" style="margin-bottom: 6px;">

								<input style="color: #000;" type="hidden" name="total_questions" id="total_questions" value="<?php echo count($quote_questions); ?>"><br/>
								<input style="color: #000;" type="hidden" name="question_type_<?php echo $i ?>" id="question_type_<?php echo $i ?>" value="<?php echo $quote_question['question_type'] ?>">
								<input style="color: #000;" type="hidden" name="question_id_<?php echo $i ?>" id="question_id_<?php echo $i ?>" value="<?php echo $quote_question['id'] ?>">
								<input type="text" class="form-control" name="question_<?php echo $i; ?>" readonly="readonly" value="<?php echo $quote_question['question']; ?>">

						</div>

						<div class="clearfix"></div>

						<?php  $j=1;
						foreach ($quoteAnswers as $quoteAnswer)
						{ ?>

							<div class="col-md-3 col-sm-3 col-xs-6" id="free_text_<?php echo $i ?>" <?php if($quote_question['question_type']=='yes_or_no' || $quote_question['question_type']=='multiple_choice') { ?> style="display: none;" <?php } ?> >

								<div style="float: left;width: 200px;margin-right: 10px;">
									<textarea type="text" class="form-control" placeholder="Answer" name="free_text_answer_<?php echo $i; ?>"  id="free_text_answer_<?php echo $i; ?>" style="height: 100px;width: 430px;"><?php echo $quoteVendorAnswers['free_text']; ?></textarea>
								</div>
							</div>

							<div class="clearfix"></div>
							<div class="col-md-3 col-sm-3 col-xs-6" id="yes_no_area_<?php echo $i ?>" <?php if($quote_question['question_type']=='free_text' || $quote_question['question_type']=='multiple_choice') { ?> style="display: none;" <?php } ?> >

									<select id="yes_or_no_<?php echo $i ?>" name="yes_or_no_<?php echo $i ?>" class="form-control">
										<option value="">Select Yes or No</option>
										<option <?php if (isset($quoteVendorAnswers['yes_no']) && $quoteVendorAnswers['yes_no'] == 'yes') echo ' selected="SELECTED" '; ?> value="yes">Yes</option>
										<option <?php if (isset($quoteVendorAnswers['yes_no']) && $quoteVendorAnswers['yes_no'] == 'no') echo ' selected="SELECTED" '; ?> value="no">No</option>
									</select>

							</div>
							<div class="clearfix"></div>
				          <?php if($j==1) { ?>
							<div class="col-md-6 col-sm-6 col-xs-12" id="answer_area_<?php echo $i; ?>_<?php echo $j; ?>" <?php if($quote_question['question_type']=='free_text' || $quote_question['question_type']=='yes_or_no') { ?> style="display: none;" <?php } ?>>

									<select id="quote_answer_<?php echo $i; ?>" name="quote_answer_<?php echo $i; ?>" class="form-control" style="width: 430px;">
										<option value="">Select Multiple Choice Answer</option>
										<?php
										foreach ($quoteAnswers as $quoteAnswer)
										{ ?>
										<option <?php if (isset($quoteVendorAnswers['quote_answer']) && $quoteVendorAnswers['quote_answer'] == $quoteAnswer['id']) echo ' selected="SELECTED" '; ?> value="<?php echo $quoteAnswer['id']; ?>"><?php echo $quoteAnswer['answer']; ?></option>
										<?php } ?>
									</select>
							</div>
							   <?php }
							$j++;
						}

						$i++;
					}
				} ?>
			</div>
		</div>


	</div>


	<div class="clearfix"> <br /><hr /<br /> </div>
	
	<?php if (isset($quote['submit_status']) && $quote['submit_status'] == 1) { ?>
		<div class="checkbox">
			<div class="col-md-10 col-md-offset-1">
				<label for="submitted_already" style="font-weight: bold; font-size: 115%;">
					Since you have already submitted this quote, you cannot make any changes to it.
				</label>
			</div>
		</div>
	    <div class="clearfix"> <br /> </div>
	<?php } else { ?>
		<div class="checkbox">
			<div class="col-md-10 col-md-offset-1">
				<input type="checkbox" class="flat" name="submit_quote" id="submit_quote" value="1" style="margin-bottom: 15px !important;" />
				<label for="submit_quote">Check this box if you want to submit the quote. You will not be able to change it later. <br />If you want to just save it and come back later to finish it, leave this unchecked.</label>
			</div>
		</div>
	    <div class="clearfix"> <br /><br /> </div>
	
		<div class="col-md-10">
			<a href="javascript:void(0);" onclick="$('#quote_form').submit();">
				<button type="button" class="btn btn-primary">
					Save Quote
				</button>
			</a>
	    <a href="javascript:void(0);" onclick="submitQuote();">
	        <button type="button" class="btn btn-primary">
	        	 Submit Quote
			</button>
	    </a>
	    </div>
	<?php } ?>
    <div class="clearfix"> <br /> </div>
</div>
    
</form>
</div>
</div>
</div>

<div id="new_document_code" style="display: none;">
	<div class="form-group" id="document_area_DOCIDX">
		<div class="col-md-5 col-sm-5 col-xs-5">
		    <input class="form-control" type="file" name="quote_file_DOCIDX" id="quote_file_DOCIDX" />
		</div>
		<div class="col-md-1 col-sm-1 col-xs-1">
		    <a onclick="deleteDocument(DOCIDX);" title="Click here to delete this document" style="cursor: pointer; color: #fff;">
		        <span class="fa fa-minus fa-2x"></span>
		    </a>
		    <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer; color: #fff;">
		        <span class="fa fa-plus fa-2x"></span>
		    </a>
		</div>
	</div>
	<input type="hidden" name="delete_document_flag_DOCIDX" id="delete_document_flag_DOCIDX" value="0" />
	<div class="clearfix" id="end_document_area_DOCIDX"> <br /> </div>
</div>



<script type="text/javascript">
	$(document).ready(function() {
		$('.price_calc').on('keyup', function() { calculateTotalPrice(); });
		$('.price_calc').on('blur', function() { calculateTotalPrice(); });

		$('.tax_calc').on('keyup', function() { calculateTotalPrice(); });
		$('.tax_calc').on('blur', function() { calculateTotalPrice(); });

		$('.shipping_calc').on('keyup', function() { calculateTotalPrice(); });
		$('.shipping_calc').on('blur', function() { calculateTotalPrice(); });

		$('.other_calc').on('keyup', function() { calculateTotalPrice(); });
		$('.other_calc').on('blur', function() { calculateTotalPrice(); });
	});

	function calculateProductPrice(id){
		//alert(id);
		var total_single_product_price = 0;
		$('.unit_'+id).each(function() {
			var single_product_price = $.trim($(this).val());
			var eid = this.id;
			if(eid == 'unit_price_'+id){
				var qty = $(this).data("value");
				if (single_product_price != "") total_single_product_price += parseFloat(single_product_price*qty);
			}else{
				if(eid == 'tax_rate_'+id){
					var qty = $(this).data("value");
					if (single_product_price != ""){
						var product_unit_price = $("#unit_price_"+id).val();
						var single_tax_product_price = parseFloat((single_product_price / 100.0)*qty) * parseFloat(product_unit_price);
						total_single_product_price += parseFloat(single_tax_product_price);
					}
				}else{
					if (single_product_price != "") total_single_product_price += parseFloat(single_product_price);
				}

			}

		});
		$('.product_total_price_'+id).val(total_single_product_price.toFixed(2));
	}
	
	function calculateTotalPrice()
	{
		var total_product_price = 0;
		var product_price = 0;
		var total_product_rate = 0;
		var product_tax = 0;
		var total_product_shipping_charges = 0;
		var product_shipping_charges = 0;
		var total_product_other_charges = 0;
		var product_other_charges = 0;


		$('.product_unit_price').each(function() {
			product_price = $.trim($(this).val());
			var qty = $(this).data("value");
			if (product_price != "") total_product_price += parseFloat(product_price*qty);
		});

		$('.product_tax_rate').each(function() {
			product_tax = $.trim($(this).val());
			var idx = $(this).data("id");
			var qty = $(this).data("value");
			if (product_tax != "")
			{
				var product_unit_price = $("#unit_price_"+idx).val();
				product_tax = parseFloat((product_tax / 100.0)*qty) * parseFloat(product_unit_price);
				total_product_rate += product_tax;
			}

		});

		$('.product_shipping_charges').each(function() {
			product_shipping_charges = $.trim($(this).val());
			if (product_shipping_charges != "") total_product_shipping_charges += parseFloat(product_shipping_charges);
		});

		$('.product_other_charges').each(function() {
			product_other_charges = $.trim($(this).val());
			if (product_other_charges != "") total_product_other_charges += parseFloat(product_other_charges);
		});

		//console.log(total_single_product_price);
		
		var tax_rate = $.trim($('#tolal_tax_rate').val());
		if (tax_rate == "") tax_rate = 0; else tax_rate = parseFloat(tax_rate);
		
		var shipping = $.trim($('#tolal_shipping').val());
		if (shipping == "") shipping = 0; else shipping = parseFloat(shipping);
		
		var other_charges = $.trim($('#tolal_other_charges').val());
		if (other_charges == "") other_charges = 0; else other_charges = parseFloat(other_charges);
		
		var discount = $.trim($('#discount').val());
		if (discount == "") discount = 0; else discount = parseFloat(discount);
				
		var total_price = 0;
		total_price = parseFloat(total_product_price);

		total_price = total_price + total_product_rate;
		total_price = total_price + total_product_shipping_charges;
		total_price = total_price + total_product_other_charges;
		total_price = total_price - discount;

		$('#total_tax_rate').val(total_product_rate.toFixed(2));
		$('#total_shipping').val(total_product_shipping_charges.toFixed(2));
		$('#total_other_charges').val(total_product_other_charges.toFixed(2));
		$('#total_price').val(total_price.toFixed(2));
	}

	function submitQuote()
	{
		if($("#submit_quote").prop('checked') == true){
			$('#quote_form').submit();
		} else {
			alert('Please check the check box above');
		}


	}

	function deleteQuoteFile(file_idx, file_name)
	{
		$('#delete_quote_file_link_' + file_idx).confirmation({
		  title: "Are you sure you want to delete the attached file?",
		  singleton: true,
		  placement: 'right',
		  popout: true,
		  onConfirm: function() {
		  	$('#existing_file_id_' + file_idx).remove();
	        $.ajax({
	            type: "POST",
	            url: "<?php echo AppUrl::bicesUrl('quotes/deleteVendorFile/'); ?>",
	            data: { quote_id: $('#quote_id').val(), vendor_id: $('#vendor_id').val(), file_name: file_name }
	        });
		  },
		  onCancel: function() {  }
		});
		
	}

	function addDocument()
	{
		var total_documents = $('#total_documents').val();
		total_documents = parseInt(total_documents);
		total_documents = total_documents + 1;
		$('#total_documents').val(total_documents);
		
		var new_document_html = $('#new_document_code').html();
		new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
		$('#total_documents').before(new_document_html);
	}

	function deleteDocument(docidx)
	{
		var display_document_count = 0;
	    $("div[id^='document_area']").each(function () {
	    	if ($(this).is(':visible')) display_document_count += 1;
		});

		if (display_document_count <= 1) alert("You must have at least one document field in the quote form");
		else
		{
			if (confirm("Are you sure you want to delete this document?"))
			{
				$('#delete_document_flag_' + docidx).val(1);
				$('#document_area_' + docidx).hide();
				$('#end_document_area_' + docidx).hide();
			}
		}
	}

	
</script>
