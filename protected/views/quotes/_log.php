<div class="clearfix"> <br /> </div>
	<div class="form-group">
		<div class="col-md-4 col-sm-4 col-xs-8">
			<h4>Change Log</h4>
		</div>
	</div>
	<div class="clearfix"> <br /> </div>
	<div class="col-md-7 col-sm-7 col-xs-12">
		<table class="table">
			<thead><tr><th>User</th><th>Date</th><th>Comments</th></tr></thead>
			<?php
			if(!empty($quoteLogs)) {
				foreach ($quoteLogs as $scoringLog) {?>
                 <tr>
                  <td><?php echo $scoringLog['user']; ?></td>
                  <td><?php echo date(FunctionManager::dateFormat(), strtotime($scoringLog['created_datetime'])); ?></td>
                  <td><?php echo $scoringLog['comment']; ?></td>
                 </tr>
		  <?php }
           }else{?>
				<tr><td colspan="4">No Record Found</td></tr>
			<?php } ?>
		</table>
	</div>