<?php

$modalId = 1;
$editForm = '';
$userPer = new User;

foreach($documentList as $key=>$value) {
  if(!empty($quote_id)){
    $sql = " SELECT * from quote_communication where quote_id=$quote_id and  document_type=".$value['document_type']." order by id DESC";
    $documentDetailReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $documentType = $value['document_type'];
  }else{
    $documentType = $key;
    $documentDetailReader = $value;
  }
  $tableID = "doctype_".$documentType;
  ?>
<table id="<?php echo $tableID;?>" class="table table-striped table-bordered" style="width: 100%;">
<thead>
<tr><th colspan="9"><?php echo FunctionManager::quoteCommunicationDocument($documentType);?></th></tr>
<tr>
    <th style="min-width: 30%">Document Type</th>
    <th  style="width: 21%">Document Title</th>
    <th style="width: 8%">Approved</th>
    <th style="width: 10%">Approved By</th>
    <th style="width: 12%">Approved Date</th>
    <th style="width: 12%">Uploaded Date</th>
    <th style="width: 8%">Action</th>
</tr>
</thead>
<tbody>
  <?php 
  foreach($documentDetailReader as $detailKey=>$detailValue){
    $modalIDExp = "edit_document".$modalId;
    $modalId++;

    ?>
  <tr>
     <td  style='word-break:break-all;word-wrap:break-word; max-width: 30% '><span class="btn btn-success doc-btn-round" style="<?php echo FunctionManager::quoteCommunicationDocumentBgColor($documentType);?>;"><?php echo FunctionManager::quoteCommunicationDocument($documentType);?></span></td>
    <td style='word-break:break-all'><?php echo $detailValue['document_title'];?></td>
   <!--  <td style='word-break:break-all'><?php echo $detailValue['document_file'];?></td> -->
    <td><input type="checkbox" <?php if(!empty($detailValue) && strtolower($detailValue['status'])=='pending'){?> name="status_<?php echo !empty($detailValue) && !empty($detailValue['quote_id'])?$value['document_type']:$key;?>[]" <?php } ?> <?php echo !empty($detailValue) && strtolower($detailValue['status'])=='approved'?'checked="checked" disabled="disabled" ':"";?> value="<?php echo $detailValue['id'];?>" /></td>
    <td><?php echo  $detailValue['approved_by_name'];?></td>
    <td><?php echo  !empty($detailValue['approved_datetime'])?date(FunctionManager::dateFormat(), strtotime($detailValue['approved_datetime'])):'';?></td>
    <td><?php echo date(FunctionManager::dateFormat(),strtotime($detailValue['date_created']));?></td>
    <td>
      <?php if(!empty($detailValue['quote_id'])){?>
       <a href="<?php echo Yii::app()->createAbsoluteUrl('quotes/documentDownload',array('id'=>$detailValue['id']));  ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
      <?php } if(/*strtolower($detailValue['status'])=='pending' && */ !empty($detailValue['id'])){?>
      <a style="cursor: pointer; padding: 5px;" id="delete_document_<?php echo $detailValue['id'];?>" onclick="deleteDocumentNew(<?php echo $detailValue['id']; ?>)">
         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
      </a><?php 
      }else if(empty($detailValue['quote_id'])){?>
        <a style="cursor: pointer; padding: 5px;" id="delete_document_<?php echo $key.'_'.$detailKey;?>" onclick="deleteDocumentOther('<?php echo $key.'_'.$detailKey; ?>')">
         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
      </a>
      <?php } ?>
    </td>
  </tr>
<?php 
} 
if(!empty($value) && isset($value['pendiing_documents']) && $value['pendiing_documents']>0){?>
<tr><td></td><td></td><td><button id="approve_1" class="btn btn-success  btn-xs" onclick="approveDocument(<?php echo $value['document_type'];?>,event);">Approve</button></td><td></td><td></td><td></td><td></td></tr>
<?php } ?>
</tbody>
</table>
<?php }  ?>

<script>
$(document).ready(function(){

  $("#file_upload_alert").hide();
  $(".processing_oboloo").hide();

 $('.expiry_date_1').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'});

    $('#order_file_1').bind('change', function() {
     if(this.files[0].size > 2097152){
        alert("Maximum File Size of 2MB");
        this.value = "";
       }    
    });
});
 
function uploadDocument(uploadBtnIDX,e)
{  
  e.preventDefault();
  var uploadedObj = $('#order_file_'+uploadBtnIDX);
  var uploadedObjDesc = $('#file_desc_'+uploadBtnIDX); 
  var uploadedDocType = $('#document_type_'+uploadBtnIDX);
  var uploadedDocStatus = $('#document_status_'+uploadBtnIDX);

  var file_data = $('#order_file_'+uploadBtnIDX).prop('files')[0];
  var field_data = $('#file_desc_'+uploadBtnIDX).val();
  var field_type = $('#document_type_'+uploadBtnIDX).val();
  if($('#document_status_'+uploadBtnIDX).is(':checked')){
    var field_status = 'Approved';
  }else{
    var field_status = 'Pending';
  }
  if(typeof file_data === "undefined" || file_data==""){
    $.alert({title: 'Required!',content: 'Document is required',});
   }else if(field_data==""){
    $.alert({title: 'Required!',content: 'Document Title is required',});
   }else if(field_type==""){
    $.alert({title: 'Required!',content: 'Document Type is required',});
  }else{
  var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('file_description', field_data);
    form_data.append('document_type',field_type);
    form_data.append('quote_id',$('#quote_id').val());
    form_data.append('document_status',field_status);                         
    $.ajax({
        url: BICES.Options.baseurl + '/quotes/uploadDocument', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',

         beforeSend: function() {
          $("#file_upload_alert").html('');
          $("#file_upload_alert").hide();
          $(".processing_oboloo").show();
          $("#btn_upload_1").prop('disabled',true);
         },
        success: function(uploaded_response){
          $("#document_list_cont").html(uploaded_response);
          uploadedObj.val(null);
          uploadedObjDesc.val(null);
          uploadedDocType.val(null);
          uploadedDocStatus.prop('checked', false);

          $("#file_upload_alert").show();
          $("#file_upload_alert").html('File uploaded successfully');
       
          $(".processing_oboloo").hide();
          $("#btn_upload_1").prop('disabled',false);
           // display response from the PHP script, if any
        }
     });
  }
}

function approveDocument(documentType,e)
{  
  e.preventDefault();
  var arrayIDs=[];
  var documentTypeID = documentType;
  inputField = $("input[name^='status_']");
  inputField.each(function(index){
    fieldName = $(this).attr('name');
    if(fieldName.indexOf('status_'+documentTypeID) !== -1 && $(this).is(':checked'))
      arrayIDs.push($(this).val());
  });

  $.ajax({
        url: BICES.Options.baseurl + '/quotes/approveDocument', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        data: {document_ids:arrayIDs,quote_id:$('#quote_id').val()},                         
        type: 'post',
        success: function(uploaded_response){
          $("#document_list_cont").html(uploaded_response);          
            // display response from the PHP script, if any
        }
     });
}


function deleteDocumentNew(documentID)
{
  var docID = documentID;
   $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Are you sure you want to delete the document?</span>',
            buttons: {
                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo AppUrl::bicesUrl('quotes/deleteDocument'); ?>",
                                    data: { documentID: docID },
                                    success: function(uploaded_response){
                                      $("#document_list_cont").html(uploaded_response);
                                    }
                                });
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },          
            }});
}
  
</script>