<?php
$dateFormate =FunctionManager::dateFormat();
$currencySymbol = FunctionManager::currencySymbol('', $quote['quote_currency']);
$disabled = FunctionManager::sandbox(); 
?>
<div class="right_col" role="main">
    <div class="row-fluid tile_count">
       <div class="clearfix"></div>
        <?php $qouteAwarded=Yii::app()->user->getFlash('success');
          if(!empty($qouteAwarded)) { ?>
          <div class="alert alert-success qoute-alert-mbl-vw"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $qouteAwarded;?></div>
        <?php } ?>
        <?php
        if(!empty($quote['awarded_vendor_id'])){?>
            <h3><div class="alert-success-quote text-center notranslate qoute-alert-mbl-vw" style="display:block !important;"><?php echo $quote['awarded_vendor_name'];?>  Awarded</divf></h3><br />
        <?php } ?>
        <div class="clearfix"></div>
        <div class="span6 pull-left">
          <h4 class="details_heading">
           <?php $tool_currency = Yii::app()->session['user_currency'];
            if (isset($quote_id) && $quote_id){
             echo "";
             if (isset($quote) && is_array($quote) && isset($quote['quote_id']) && !empty($quote['quote_id']))
              echo '#' . $quote['quote_id'].(!empty($quote['quote_name'])?"-".$quote['quote_name']:'');
            }else echo 'Publish eSourcing Activity';?>
          </h4>
        </div>

        <div class="span6 pull-right span6-btn-vw">
          <?php if(empty($quote['awarded_vendor_id'])){?>
            <?php if ((!isset($quote['closing_date']) || strtotime($quote['closing_date']) > time()) 
            && strtolower($quote['quote_status']) !='cancelled' ) { ?>
            <a href="<?php echo $this->createUrl('quotes/changeQuote',array('id'=>isset($quote_id)?$quote_id:'')); ?>" onClick="return confirm('Are you sure you want to cancel this activity? This cannot be undone')">
              <button type="button" class="btn btn-danger"  <?php echo $disabled; ?>>Cancel eSourcing Activity</button>
            </a>
        <?php }
          if($total_reopen<=4){?>
           <button type="button" class="btn btn-primary submit-btn" onclick="$('#re_open_modal').modal('show');">
                <span class="fa fa-pencil mr-2"></span> Re-open eSourcing Activity
            </button>
            <?php  
            $this->renderPartial('_re_open',array('quote_vendors'=>$quote_vendors,'quote_id'=>$quote_id,'dateFormate'=>$dateFormate,'quote'=>$quote));
          }
          }
          if(empty($quote['awarded_vendor_id'])){?>
           <button type="button" class="btn btn-success submit-btn1" onclick="$('#award_supplier_modal').modal('show');">
                <span class="fa fa-users  mr-2"></span> Award Supplier
            </button>
            <?php  $this->renderPartial('_award_supplier',array('quote_vendors'=>$quote_vendors,'quote_id'=>$quote_id));
           }
           if(!empty($quote['awarded_vendor_id']) && empty($quote['awarded_saving_id'])){?>
            <button type="button" class="btn btn-warning click-modal-poup " onclick="addSavingModal(event);" style="background-color: #357ae8 !important; border-color: #357ae8 !important;">
                <span class="fa fa-tags mr-2" aria-hidden="true"></span> Create Savings Record
            </button> 
            <?php } 

            if(!empty($quote['awarded_saving_id'])){?>
             <a href="<?php echo $this->createUrl('savings/edit/'.$quote['awarded_saving_id']); ?>" class="btn btn-success saving-view-btn" ><span class="fa fa-tags"></span> &nbsp;View Saving</a>
            <?php } 

            if(!empty($quote['awarded_vendor_id']) && empty($quote['awarded_contract_id'])){?>
              <?php if (strtotime($quote['closing_date']) > time() && strtolower($quote['quote_status']) !='cancelled') { ?>
            <?php }?>
            <button type="button" class="btn btn-default bg-green" onclick="$('#contract_supplier_modal').modal('show');"  >
                <span class="fa fa-handshake-o"></span>&nbsp;Create Contract
            </button>

            <?php  $this->renderPartial('_contract_supplier',array('quote'=>$quote,'quote_id'=>$quote_id,'categories'=>$categories,'locations'=>$locations,'currency_rate'=>$currency_rate));
            }
            if(!empty($quote['awarded_contract_id'])){?>
              <?php if ((!isset($quote['closing_date']) || strtotime($quote['closing_date']) > time()) && strtolower($quote['quote_status']) !='cancelled' ) { ?>
               <a href="<?php echo $this->createUrl('quotes/changeQuote',array('id'=>isset($quote_id)?$quote_id:'')); ?>" onClick="return confirm('Are you want to cancel?')">
                        <button type="button" class="btn btn-danger">
                           Cancel RFP
                        </button>
                    </a>
            <?php } ?>
            <a href="<?php echo $this->createUrl('contracts/edit/'.$quote['awarded_contract_id']); ?>" class="btn btn-success" ><span class="fa fa-handshake-o"></span> &nbsp;Awarded Contract</a>
            <?php } ?>

            <a href="<?php echo AppUrl::bicesUrl('quotes/list'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;border-color: #F79820;">
                    <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Return to eSourcing List
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
        <?php if(!empty(Yii::app()->user->getFlash('success'))):?>
            <div class="alert alert-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->hasFlash('error')):?>
            <div class="alert alert-danger">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
    </div>

<div class="row tile_count supplier-mbl-vw-tab" role="tabpanel" data-example-id="togglable-tabs">
    <?php if (isset($quote_id) && $quote_id) { ?>
      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="<?php echo empty($_GET['tab']) || !in_array($_GET['tab'],array('scoring'))?'active':'';?>">
            <a class="has-feedback-left pull-right" href="#tab_content1" id="quote-tab" role="tab" data-toggle="tab" aria-expanded="true">
                Quote Details
            </a>
        </li>
        <li role="presentation" class="">
            <a href="#tab_content2" role="tab" id="vendor-tab" data-toggle="tab" aria-expanded="false">
              Product Financials
            </a>
        </li>
        <?php if(!empty($quote_vendors_questionnaire)){?>
         <li role="presentation" >
          <a href="#tab_content6" role="tab" id="quote-scoring" class="" data-toggle="tab" aria-expanded="false">
            Scoring Analysis 
          </a>
         </li>
        <?php }
        if(!empty($checkQuestionnireOfQuote) && count($checkQuestionnireOfQuote['counter']) > 0){ ?>
        <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='scoring'?'active':'';?>" >
          <a href="#tab_content10" role="tab" id="quote-scorers" 
            class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='quote-scoring'?'active':''?>" data-toggle="tab" aria-expanded="false">
              Scorers
          </a>
        </li>
        <?php } ?>
        <li role="presentation" class="" >
          <a href="#tab_content8" role="tab" id="quote-scoring"
          class="<?php echo !empty($_GET['tab']) && $_GET['tab']=='comments'?'active':''?>"
            data-toggle="tab" aria-expanded="false">
              Comments
          </a>
        </li>
        <li role="presentation" class="">
          <a href="#tab_content5" role="tab" id="vendor-tab" data-toggle="tab" aria-expanded="false">
              Received Submissions History
          </a>
        </li>
      </ul>
  <?php } ?>

    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade <?php echo empty($_GET['tab']) || !in_array($_GET['tab'],array('scoring'))?'active in':'';?>" id="tab_content1" aria-labelledby="quote-tab">
    <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/edit'); ?>">
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 "> <br />
              <h4 class="pull-left details_heading">Quote Details<br /></h4>
          </div>
      </div>

      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
          <label class="control-label">Title <span style="color: #a94442;">*</span></label>
          <input required type="text" class="form-control notranslate" name="quote_name" id="quote_name"
          <?php if (isset($quote['quote_name']) && !empty($quote['quote_name'])) echo 'value="' . $quote['quote_name'] . '"'; else echo 'placeholder="Quote Name"'; ?> >
        </div>
    </div>

      <div class="form-group">
        <div class="col-md-6 col-sm- col-xs-12">
          <label class="control-label">Description/Notes</label>
          <textarea class="form-control notranslate" name="quote_desc" id="quote_desc" <?php if (!isset($quote['quote_desc']) || empty($quote['quote_desc'])) echo 'placeholder="Quote description/notes"'; ?> style="height:200px"><?php if (isset($quote['quote_desc']) && !empty($quote['quote_desc'])) echo $quote['quote_desc']; ?></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="col-md-6 col-sm- col-xs-12 date-input">
          <label class="control-label">Quote Currency</label>
          <select name="quote_currency" id="quote_currency" class="form-control notranslate" required>
          <?php foreach ($currency_rate as $rate) { ?>
          <option value="<?php echo $rate['currency']; ?>" <?php if (!empty($quote['quote_currency']) && $quote['quote_currency']== $rate['currency']) echo ' selected="SELECTED" '; ?>>
          <?php echo $rate['currency']; ?>
          </option>
          <?php } ?>
        </select>
        </div>
      </div>

    <?php
        $vendors_to_show = 1;
        if (isset($quote_vendors) && is_array($quote_vendors) && count($quote_vendors))
          $vendors_to_show = count($quote_vendors);
    ?>

    <input type="hidden" class="notranslate" name="vendor_count" id="vendor_count" value="<?php echo $vendors_to_show; ?>" />
        
      <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-12 valid">
          <label class="control-label">Location <span style="color: #a94442;">*</span></label>
          <select required name="location_id" id="location_id" class="form-control notranslate" onchange="loadDepartmentsForSingleLocation(0);">
          <option value="">Select Location</option>
          <?php foreach ($locations as $location) { ?>
            <option value="<?php echo $location['location_id']; ?>"
              <?php if (isset($quote['location_id']) && $location['location_id'] == $quote['location_id']) echo ' selected="SELECTED" '; ?>>
              <?php echo $location['location_name']; ?>
            </option>
          <?php } ?>
          </select>
        </div>

       <div class="col-md-3 col-sm-3 col-xs-12 valid">
        <label class="control-label">Department <span style="color: #a94442;">*</span></label>
        <select required name="department_id" id="department_id" class="form-control notranslate">
            <option value="">Select Department</option>
        </select>
       </div>
      </div>
      <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-12">
          <label class="control-label">Category</label>
          <select name="category_id" id="category_id"
            class="form-control notranslate" onchange="loadSubcategories(0);">
            <option value="0">Select Category</option>
            <?php foreach ($categories as $category) { ?>
             <option value="<?php echo $category['id']; ?>"
              <?php if (isset($quote['category_id']) && $quote['category_id'] == $category['id']) echo ' selected="SELECTED" '; ?>>
              <?php echo $category['value']; ?>
             </option>
            <?php } ?>
        </select>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <label class="control-label">Sub Category</label>
          <select name="subcategory_id" id="subcategory_id"
            class="form-control notranslate">
            <option value="0">Select Sub Category</option>
          </select>
        </div>
      </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 valid">
                <label class="control-label">Spend Type</label>
                <select name="spend_type" id="spend_type" class="form-control notranslate">
                    <option value="">Select Spend Type</option>
                    <option  <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                    <option  <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
                    <option  <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Goods & Services') echo ' selected="SELECTED" '; ?> value="Goods & Services">Goods & Services</option>
                </select>
            </div>
        </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input valid">
              <label class="control-label">Opening Date & Time (UTC)<span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control  notranslate" name="opening_date" id="opening_date"
                    <?php if (isset($quote['opening_date']) && !empty($quote['opening_date'])) echo 'value="' . date($dateFormate." H:iA", strtotime($quote['opening_date'])) . '"'; else echo 'placeholder="Opening Date"'; ?> >
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input valid">
              <label class="control-label">Closing Date & Time (UTC)<span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control notranslate" name="closing_date" id="closing_date"
                    <?php if (isset($quote['closing_date']) && !empty($quote['closing_date'])) echo 'value="' . date($dateFormate." H:iA", strtotime($quote['closing_date'])) . '"'; else echo 'placeholder="Closing Date"'; ?> >
          </div>
    </div>
       


  <!--     <div class="form-group">
          <?php
            $record_user_id = 0;
      if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];
      if (isset($quote['user_id']) && !empty($quote['user_id'])) $record_user_id = $quote['user_id'];  
          ?>
        <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Created by  By</label>
              <select class="form-control" name="user_id" id="user_id">
               <?php
               
                   foreach ($users as $user) {
                      if($user['user_id'] !=0){
                    ?>
                      <option value="<?php echo $user['user_id']; ?>"
                                <?php if ($record_user_id == $user['user_id']) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $user['full_name']; ?>
                      </option>
                  <?php }} ?>
              </select>
          </div></div> -->
            <div class="clearfix"> <br /> </div>
            <?php if(!empty($quoteVendors)) { ?>
             <h4 class="details_heading">Invited Suppliers</h4>
            <br>
            <?php foreach ($quoteVendors as $quoteVendor) { ?>
            <div class="col-md-2 col-sm-2 col-xs-6 date-input valid">
              <label class="control-label">Supplier Name</label>
              <input  class="form-control  notranslate" name="" id="" value="<?php echo $quoteVendor['vendor_name']; ?>" disabled>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6 date-input valid">
              <label class="control-label">Supplier Contact Name</label>
              <input required type="text" class="form-control  notranslate" name="" id="" value="<?php echo $quoteVendor['contact_name']; ?>" disabled>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6 date-input valid">
              <label class="control-label">Supplier Email</label>
              <input required type="text" class="form-control  notranslate" name="" id="" value="<?php echo $quoteVendor['emails']; ?>" disabled>
          </div>
          <div class="clearfix"></div><br>
            <?php } } ?>

          <div class="clearfix"></div>
          <h4 class=" details_heading">Cost Savings</h4>
          <div class="col-md-3 col-sm-3 col-xs-4">
          <div class="form-group">
          <label class="control-label">Cost Savings (including tax)</label>
          <input  class="form-control  notranslate" name="" id="" value="<?php echo $costSaving;?>" disabled></div>
          </div>


    <br><br>
<div class="clearfix"></div>
   
</form>
<?php
if (isset($quote_id) && $quote_id) {
    if(Yii::app()->session['user_id']==$quote['user_id'] || in_array(Yii::app()->session['user_type'],array(4))) { ?>
<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
       <?php if ((!isset($quote['closing_date']) || strtotime($quote['closing_date']) > time()) && strtolower($quote['quote_status']) !='cancelled' ) { ?>

      <?php }else { ?>
          <div class="alert alert-warning fade in" role="alert" style="margin-top: 20px;">
              <span style="font-size: 120%;">This quote has already been closed and hence cannot be edited.</span>
          </div>
      <?php } ?>
    </div>
    <div class="clearfix"> <br /><br /> </div>
</div>
<?php }
} else { ?>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php if (!isset($quote['closing_date']) || strtotime($quote['closing_date']) > time()) { ?>
                <?php } else { ?>
                    <div class="alert alert-warning fade in" role="alert" style="margin-top: 20px;">
                        <span style="font-size: 120%;">This quote has already been closed and hence cannot be edited.</span>
                    </div>
                <?php } ?>
            </div>
            <div class="clearfix"> <br /><br /> </div>
        </div>
<?php } ?>
</div>
      <div class="clearfix"> </div>
      <div role="tabpanel" class="tab-pane" id="tab_content2" aria-labelledby="vendor-tab">
            <?php $this->renderPartial('__financial_summary', array('quote_vendors' => $quote_vendors, 'quote_id' => $quote_id));?>
        <div class="col-md-12"><br />
        <h4 style="padding: 10px;" class="details_heading mt-26">Supplier Detailed Response</h4>
        <h6 style="padding: 10px;" >Itemised product financials by supplier<br /><br /></h6>
            <?php
              $infoModal='';
              $i=1;$vendorCtr = 1;
              $previousVendor='';
              $total_price=$product_total_price=$total_amount_without_tax=0;
              foreach ($quote_details as $quote_detail){
              if($vendorCtr==1){?>
               <table class="table table-striped table-bordered" style="width: 100%">
                <thead>
                  <tr>
                    <th></th>
                    <th>Supplier</th>
                    <th>Supplier Response Date (UTC)</th>
                    <th>Product Name</th>
                    <th>Product Code</th>
                    <th>Unit of Measure</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Total Amount (Exc Tax)</th>
                    <th>Total Amount (Inc Tax)</th>
                  </tr>
                </thead>
                <tbody>

              <?php } $vendorCtr++;if (true){
                    $total_unit_price = $quote_detail['unit_price']*$quote_detail['quantity'];
                    $product_tax_rate_price = ($quote_detail['product_tax_rate']/100)*$total_unit_price;
                    $product_total_price = $total_unit_price+$product_tax_rate_price+$quote_detail['product_shipping']+$quote_detail['product_other_charges'];
                }
                $total_price += $product_total_price;

                 $amount_without_tax = $product_total_price - $product_tax_rate_price;
                 $total_amount_without_tax = $total_amount_without_tax+$amount_without_tax;

                $popUpID = "productmodal".$quote_detail['id'].$i;
                $i++;

                if(empty($previousVendor))
                  $previousVendor = $quote_detail['vendor_id'];
                if($previousVendor != $quote_detail['vendor_id']){
                  ?>
                   <tr><td colspan="8" align="left">Total</td>
                     <td><?php echo $currencySymbol.($total_amount_without_tax-$amount_without_tax);
                          $total_amount_without_tax = $amount_without_tax;?></td>

                    <td><?php echo $currencySymbol.($total_price-$product_total_price);
                         $total_price=$product_total_price;?></td></tr>
                </tbody>
                </table>
                <table class="table table-striped table-bordered" style="width: 100%">
                <thead>
                  <tr>
                    <th></th>
                    <th>Supplier</th>
                    <th>Supplier Response Date (UTC)</th>
                    <th>Product Name</th>
                    <th>Product Code</th>
                    <th>Unit of Measure</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Total Amount (Exc Tax)</th>
                    <th>Total Amount (Inc Tax)</th>
                  </tr>
                </thead>
                <tbody>
                   
                <?php }
                  $previousVendor = $quote_detail['vendor_id'];
                 ?>
                  <tr>
                    <td><a data-toggle="modal" data-target="#<?php echo $popUpID;?>" href="#" 
                      style="text-decoration:none;color: #357ae8;"> 
                      <span class="glyphicon glyphicon-glyphicon glyphicon-eye-open" aria-hidden="true" style="color: #357ae8;"></span> &nbsp;View</a>
                    </td>
                    <td class="notranslate"><?php echo $quote_detail['vendor_name'];?></td>
                    <td><?php
                    if (!empty($quote_detail['vendor_repsond_date']) && $quote_detail['vendor_repsond_date'] !="0000-00-00 00:00:00") 
                           echo date(FunctionManager::dateFormat(), strtotime($quote_detail['vendor_repsond_date'])).' '.date("h:i:s A",strtotime($quote_detail['vendor_repsond_date'])); ?></td>
                    <td><?php echo $quote_detail['product_name'];?></td>
                    <td><?php echo $quote_detail['product_code'];?></td>
                    <td><?php echo $quote_detail['uom']; ?></td>
                    <td><?php echo $currencySymbol.$quote_detail['unit_price']; ?></td>
                    <td><?php echo $quote_detail['quantity']; ?></td>
                    <td><?php echo !empty($amount_without_tax)?$currencySymbol.($amount_without_tax):''; ?></td>
                    <td><?php echo !empty($product_total_price)?$currencySymbol.($product_total_price):''; ?></td>
                  </tr>
                <?php 


                $infoModal .= '<div id="'.$popUpID.'" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title notranslate">'.$quote_detail['vendor_name'].'</h4>
                                    </div>
                                    <div class="modal-body">
                                    <h4>Product Detail<br /><br /></h4>
                                     <table class="table table-striped table-bordered" style="width: 100%">
          <thead>
            <tr>
              <th>Product Name</th>
              <th>Product Code</th>
              <th>Unit of Measure</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>
                  <tr>
                    <td>'.$quote_detail['product_name'].'</td>
                    <td>'.$quote_detail['product_code'].'</td>
                    <td>'.$quote_detail['uom'].'</td>
                    <td>'.$quote_detail['quantity'].'</td>
                  </tr>
                </tbody></table><br />

          <h4>Product Supplier Detail<br /><br /></h4>
          <table class="table table-striped table-bordered" style="width: 100%">
          <thead>
            <tr>
              <th>Product Notes</th>
              <th>Unit Price</th>
              <th>Tax Rate %</th>
              <th>Tax Amount</th>
              <th>Shipping</th>
              <th>Other Charges</th>
              <th>Total Price</th>
            </tr>
          </thead>
          <tbody>
              <tr>
                 <td>'.$quote_detail['vendor_product_notes'].'</td>
                <td>'.$currencySymbol.$quote_detail['unit_price'].'</td>
                <td>'.$quote_detail['product_tax_rate'].'</td>
                <td>'.$currencySymbol.$product_tax_rate_price.'</td>
                <td>'.$quote_detail['product_shipping'].'</td>
                <td>'.$quote_detail['product_other_charges'].'</td>  
                <td>'.$currencySymbol.$product_total_price.'</td>
              </tr>
              </tbody></table>';
               $vendor_id = $quote_detail['vendor_id'];
               //  $your_existing_files = array();
               //  $upload_dir = 'uploads/quotes/';
               //  if ($quote_id)
               //  {
               //      if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
               //      if (!is_dir('uploads/quotes/' . $quote_id))
               //          mkdir('uploads/quotes/' . $quote_id);
               //      if (!is_dir('uploads/quotes/' . $quote_id . '/' . $vendor_id))
               //          mkdir('uploads/quotes/' . $quote_id . '/' . $vendor_id);
               //      $upload_dir = 'uploads/quotes/' . $quote_id . '/' . $vendor_id;
               //      if ($handle = opendir($upload_dir)) {
               //          while (($uploaded_file = readdir($handle)) !== false){
               //              if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file))
               //                  $your_quite_files[] = $uploaded_file;
               //          }
               //      }
               //  }


        $sql = "SELECT * FROM quote_files WHERE quote_id = ".$quote_id;
        $quite_files = Yii::app()->db->createCommand($sql)->query()->readAll();
          
        if (!empty($quite_files)) {
           $infoModal .= '<h4><br />Quote Documents<br /><br /></h4>
              <table class="table table-striped table-bordered" style="width: 100%;">';
              $file_idx = 1; 
              foreach ($quite_files as $uploaded_file) {
                  $infoModal .= '<tr id="existing_file_id_$file_idx">
                        <td>
                            <a href="'.AppUrl::bicesUrl('uploads/quotes/' . $quote_id . '/'. $uploaded_file['file_name']).'" target="quote_file" style="text-decoration:none;color: #2d9ca2;"> 
                                <span class="glyphicon glyphicon-glyphicon glyphicon-download" aria-hidden="true" style="color: #2d9ca2;"></span> Download 
                            </a>';
                        $infoModal .= '</td>
                         <td>
                             '.substr($uploaded_file['file_name'], strpos($uploaded_file['file_name'], "_") + 1).'
                         </td>
                        <td class="notranslate">'.$uploaded_file['description'].'</td>
                    </tr>';
                    $file_idx += 1; }
                  $infoModal .= '</table><div class="clearfix"></div>';
          }

        if (!empty($vendor_id)) {
          $sql = "SELECT * FROM quote_vendor_files WHERE quote_id = ".$quote_id." and vendor_id = ".$vendor_id." ORDER BY id";
          $your_quite_files = Yii::app()->db->createCommand($sql)->query()->readAll();  
        }
        if (!empty($your_quite_files)) {
           $infoModal .= '<h4><br />Supplier Documents<br /><br /></h4>
              <table class="table table-striped table-bordered" style="width: 100%;">';
              $file_idx = 1; 
              foreach ($your_quite_files as $uploaded_file) {
                  $infoModal .= '<tr id="existing_file_id_$file_idx">
                        <td>
                            <a href="'.AppUrl::bicesUrl('uploads/quotes/' . $quote_id . '/' .$vendor_id .'/'. $uploaded_file['file_name']).'" target="quote_file" style="text-decoration:none;color: #2d9ca2;"> 
                                <span class="glyphicon glyphicon-glyphicon glyphicon-download" aria-hidden="true" style="color: #2d9ca2;"></span> Download 
                            </a>';
                          /*  if (!isset($quote['submit_status']) || $quote['submit_status'] == 0) {
                               $infoModal .= ' &nbsp;
                                <a id="delete_quote_file_link_$file_idx" style="cursor: pointer; color: red;"
                                   onclick="deleteQuoteFile('.$file_idx.', '.$uploaded_file.');">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </a>';
                             }  */
                        $infoModal .= '</td>
                         <td>
                             '.substr($uploaded_file['file_name'], strpos($uploaded_file['file_name'], "_") + 1).'
                         </td>
                        <td class="notranslate">'.$uploaded_file['file_description'].'</td>
                    </tr>';
                    $file_idx += 1; }
                  $infoModal .= '</table><div class="clearfix"></div>';
            }

          if(!empty($quoteAnswerVendor)){
               $infoModal .= '<h4><br />Supplier Questionnaire Answers<br /><br /></h4>
              <table class="table table-striped table-bordered" style="width: 100%;">
      <thead>
        <tr>
          <th>Scoring Criteria</th>
          <th>Question</th>
          <th>Answer</th>
        </tr>
      </thead>
       <tbody>';
          foreach ($quoteAnswerVendor as $value) {
            if($value['vendor_id']==$quote_detail['vendor_id']){
           $infoModal .= '<tr>
              <td  style="word-wrap: break-word;word-break: break-all;">'.$value['scoring_title'].'</td>
              <td style="word-wrap: break-word;word-break: break-all;">'.$value['question'].'</td>
              <td style="word-wrap: break-word;word-break: break-all;">'.$value['qans'].'</td>
            </tr>';
         }  }
       $infoModal .= '</tbody></table>';};
        $infoModal .= '</div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>';
              }
              
            if(!empty($total_price)){?>
             <tr><td colspan="8" align="left">Total</td>
              <td><?php echo $currencySymbol.$total_amount_without_tax ;?></td>
              <td><?php echo $currencySymbol.$total_price;?></td>
             </tr>    
            <?php } ?>
            </tbody>
          </table>
          </div>
          <?php echo $infoModal;?>
    </div>
      <div class="clearfix"> </div>
      <div role="tabpanel" class="tab-pane" id="tab_content4" aria-labelledby="communication">
        <div class="col-md-12"><br />
          <?php //$this->renderPartial('documents',array('quote_id'=>$quote_id,'documentList'=>$documentList));?>
          <div class="clearfix"><br /></div> 
      </div>
      </div>

      <div role="tabpanel" class="tab-pane" id="tab_content5" aria-labelledby="communication">
          <?php $this->renderPartial('_submission_history',array('quote_details_history'=>$quote_details_history,'quote_details_history_new'=>$quote_details_history_new,'total_reopen'=>$total_reopen,'reopenIds'=>$reopenIds,'currencySymbol'=>$currencySymbol));?>
      </div>
      <?php if(!empty($quote_vendors_questionnaire)){?>
      <div role="tabpanel" class="tab-pane " id="tab_content6" aria-labelledby="quote_scoring">
          <?php $this->renderPartial('_questionnaires_sections',array('quote' => $quote, 'quote_id'=>$quote_id));?>
      </div>
      <?php } ?>
      <div class="clearfix"><br /></div>
      <div role="tabpanel" class="tab-pane <?php echo !empty($_GET['tab']) && $_GET['tab']=='scoring'?'active':'';?>" id="tab_content10" aria-labelledby="scorers">
          <?php $this->renderPartial('_quote_scoring',array('quote_id' => $quote_id, 'creator_id' => $quote['user_id'])); ?>
      </div>
      <div role="tabpanel" class="tab-pane" id="tab_content8" aria-labelledby="communication">
          <!-- START: Comment and history of each user -->
        <?php $this->renderPartial('_comments',array('quote_id'=>$quote_id,'quoteCommHistory'=>$quoteCommHistory));?>
      </div>
    

    <div id="new_question_code" style="display: none;">
        <div class="form-group" id="question_area_QIDX">
            <div class="col-md-5 col-sm-5 col-xs-10">
                <input type="text" class="form-control notranslate" placeholder="Question for the supplier submitting quote"
                       name="quote_question_QIDX" id="quote_question_QIDX"  />
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2" style="margin-top:7px;">
                <a onclick="deleteQuestion(QIDX);" title="Click here to delete this question" style="cursor: pointer;">
                    <span class="fa fa-minus fa-2x"></span>
                </a>
                <a onclick="addQuestion();" title="Click here to add another question" style="cursor: pointer;">
                    <span class="fa fa-plus fa-2x"></span>
                </a>
            </div>
            <div class="clearfix">  </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <label class="control-label">Question Type</label>
                <select id="question_type_QIDX" name="question_type_QIDX" class="form-control notranslate" onchange="selectType(QIDX);">
                    <option value="free_text">Free Text</option>
                    <option value="yes_or_no">Yes or No</option>
                    <option value="multiple_choice">Multiple Choice</option>
                </select>
            </div>

            <div class="clearfix"> </div>
            <div class="col-md-3 col-sm-3 col-xs-6" style="display: none;" id="yes_no_area_QIDX">
                <div>&nbsp;</div>

                <table style="width: 200px;">
                    <tr>
                        <td><b>Yes =</b></td>
                        <td><input type="radio" value="Correct" name="yes_QIDX" >  Correct</td>
                        <td><input type="radio" value="Incorrect" name="yes_QIDX" >  Incorrect</td>
                    </tr>
                    <tr>
                        <td><b>No =</b></td>
                        <td><input type="radio" value="Correct" name="no_QIDX" > Correct</td>
                        <td><input type="radio" value="Incorrect" name="no_QIDX" > Incorrect</td>
                    </tr>
                </table>

            </div>
            <div class="clearfix"> </div>
            <div id="answer_section_QIDX">
            <?php for($i=1; $i<=10;$i++) {  ?>

            <div class="form-group" id="answer_area_QIDX_<?php echo $i; ?>" style="display: none;">
                <div class="col-md-1 col-sm-1 col-xs-2" style="margin-top: 25px;">
                    <a onclick="deleteAnswer(<?php echo $i; ?>,QIDX);" title="Click here to delete this answer" style="cursor: pointer;">
                        <span class="fa fa-minus fa-2x"></span>
                    </a>
                    &nbsp;
                    <a onclick="addAnswer(QIDX);" title="Click here to add another answer" style="cursor: pointer;">
                        <span class="fa fa-plus fa-2x"></span>
                    </a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div>&nbsp;</div>
                    <div style="float: left;width: 200px;margin-right: 10px;">
                        <input type="text" class="form-control notranslate" placeholder="Answer" name="quote_answer_QIDX_<?php echo $i; ?>" id="quote_answer_QIDX_<?php echo $i; ?>" value="" />
                    </div>

                    <div style="float: left; width:60px;">
                        <div>Excellent</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 15px;"><input type="radio" class="notranslate" value="Excellent" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                    <div style="float: left; width:50px;">
                        <div>Good</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 8px;"><input type="radio" class="notranslate" value="Good" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                    <div style="float: left; width:50px;">
                        <div>Okay</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 8px;"><input class="notranslate" type="radio" value="Okay" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                    <div style="float: left; width:40px;">
                        <div>Bad</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 4px;"><input class="notranslate" type="radio" value="Bad" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                    <div style="float: left; width:60px;">
                        <div>Very Bad</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 15px;"><input type="radio" class="notranslate" value="Very Bad" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                </div>
                <input type="hidden" class="notranslate" name="delete_answer_flag_<?php echo $i; ?>" id="delete_answer_flag_<?php echo $i; ?>" value="0" />
                <div class="clearfix" id="end_answer_area_<?php echo $i; ?>" style="margin-bottom: 27px;"> <br /> </div>

            </div>
             <?php } ?>
            <input type="hidden" class="notranslate" name="total_answers_QIDX" id="total_answers_QIDX" value="1" />
            </div>
        </div>
        <input type="hidden" name="delete_question_flag_QIDX" class="notranslate" id="delete_question_flag_QIDX" value="0" />
        <div class="clearfix" id="end_question_area_QIDX"> <br /> </div>
        <div class="clearfix"> </div>
    </div>

<div id="new_vendor_code">
  <div class="form-group" id="vendor_area_VENDOR_IDX">
      <div class="col-md-4 col-sm-4 col-xs-8">

        <select required type="text" class="vendor_name_field form-control has-feedback-left notranslate" 
                    name="vendor_name_VENDOR_IDX" id="vendor_name_VENDOR_IDX" style="  border-radius: 10px;
    border: 1px solid #4d90fe;"
                 >
        </select>

         <!--  <input type="text" class="vendor_name_field form-control has-feedback-left" name="vendor_name_VENDOR_IDX" id="vendor_name_VENDOR_IDX" />
          <input id="vendor_id_VENDOR_IDX" type="hidden" name="vendor_id_VENDOR_IDX" class="vendor_id" value="0" /> -->
          <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-4">
    <!--  <div class="col-md-4 col-sm-4 col-xs-8">
          <a onclick="displayVendorDetails(VENDOR_IDX,this);" title="Click here to update contact details for selected suppliers" style="cursor: pointer;">
              <span class="fa fa-edit fa-2x"></span>
            </a>
        </div> -->
        <div class="col-md-4 col-sm-4 col-xs-8">
          <a onclick="deleteVendor(VENDOR_IDX);" title="Click here to delete this supplier" style="cursor: pointer;">
              <span class="fa fa-minus fa-2x"></span>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-8">
          <a onclick="addVendor();" title="Click here to invite more suppliers for this quote" style="cursor: pointer;">
                <span class="fa fa-plus fa-2x"></span>
            </a>
        </div>
      </div>
  </div>

  <div class="form-group" id="added_vendors_VENDOR_IDX" style="display: none;">
  </div>
  <input type="hidden" class="notranslate" name="delete_vendor_flag_VENDOR_IDX" id="delete_vendor_flag_VENDOR_IDX" value="0" /> 
</div>

<?php $this->renderPartial('/savings/create_saving');?>
<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::jsUrl('bootstrap-duallistbox.css'); ?>" />
      <script type="text/javascript"
          src="<?php echo AppUrl::jsUrl('jquery.bootstrap-duallistbox.js'); ?>"></script>

<script type="text/javascript">
$(document).ready( function() {
  setTimeout(function() {
    $("#evalution_vendor_id").select2({
      placeholder: "Select Supplier",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });
  }, 100);
  $(".select_vendor_multiple").select2();
 var demo2 = $('.reopen-dualbox').bootstrapDualListbox({
  preserveSelectionOnMove: 'moved',
  moveOnSelect: true,
  //nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
});

 $('[id*="bootstrap-duallistbox-selected-list_"]').addClass("notranslate");
 $('[id*="bootstrap-duallistbox-nonselected-list_reopen"]').addClass("notranslate");

 $('#reopen_form').submit(function(){
  reopenVendorID = $('[id*="bootstrap-duallistbox-selected-list_"]').html();
  reopenNotes = $('#reopen_notes').val();
  if(reopenVendorID=="" || reopenVendorID==null){
   $('[id*="bootstrap-duallistbox-selected-list_"]').addClass('invalid');
   $('.reopen-vendor-error').html('Supplier Selection is required');
  }else{
     $('[id*="bootstrap-duallistbox-selected-list_"]').removeClass('invalid');
     $('.reopen-vendor-error').html('');
  }
  if(reopenNotes=="" || reopenNotes==null){
   $('#reopen_notes').addClass('invalid');
   $('.reopen-desc-error').html('Description/Notes is required');
    return false;
  }else{
    $('#reopen_notes').removeClass('invalid');
    $('.reopen-desc-error').html('');
  }
  
 });


   $('#new_vendor_code').hide();
  <?php if (isset($quote['location_id']) && !empty($quote['location_id'])) { ?>

    <?php if (isset($quote['department_id']) && !empty($quote['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $quote['department_id']; ?>);
    //loadDepartmentsForSingleLocationContract(<?php echo $quote['department_id']; ?>);
    <?php } else { ?>
    loadDepartmentsForSingleLocation(0);
    //loadDepartmentsForSingleLocationContract(0);
    <?php } ?>
  <?php } ?>

    <?php if (isset($quote['subcategory_id']) && !empty($quote['subcategory_id'])) { ?>
    loadSubcategories(<?php echo $quote['subcategory_id']; ?>);
    loadSubcategoriesContract(<?php echo $quote['subcategory_id']; ?>);
  <?php } ?>

   
  

  $('#manager_name').devbridgeAutocomplete({
    serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
    onSelect: function(suggestion) { $('#manager_user_id').val(suggestion.data); },
    onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#manager_user_id').val(0); }
  });

  $('#procurement_lead_name').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
        onSelect: function(suggestion) { $('#procurement_lead_user_id').val(suggestion.data); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#procurement_lead_user_id').val(0); }
    });

  $('#commercial_lead_name').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
        onSelect: function(suggestion) { $('#commercial_lead_user_id').val(suggestion.data); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#commercial_lead_user_id').val(0); }
    });


    $('#quote_form').on('submit', function() {
        $('#quote_status').removeAttr('disabled');
    });

    $( "#quote_form" ).validate( {
        rules: {
            quote_name: "required",
            location_id: "required",
            department_id: "required"
        },
        messages: {
            quote_name: "Quote name is required",
            location_id: "Location is required",
            department_id: "Department is required"
        },
      submitHandler: function(form) {
        var product_id = 0;
        $('.product_quantity').each(function() {
          if ($.trim($(this).val()) == "") {
              product_id = $(this).closest('.form-group').find('.product_id').val();
              if (product_id == '0') $(this).val('0');
            else $(this).val('1');
          }
        });
        form.submit();
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      element.parents( ".col-sm-6" ).addClass( "has-feedback" );

      if ( element.prop( "type" ) === "checkbox" )
        error.insertAfter( element.parent( "label" ) );
      else error.insertAfter( element );

      if ( !element.next( "span" )[ 0 ] )
        $( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
    },
    success: function ( label, element ) {
      if ( !$( element ).next( "span" )[ 0 ] )
        $( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
      //$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
    },
    unhighlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error" );
      //$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
    }
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field"
    });
 
  $('#opening_date').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
      timePicker: true,
      timePickerIncrement: 10,
      locale: {
    format: '<?php echo FunctionManager::dateFormatJS();?> h:mm A'
    }
  });

  $('#closing_date').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
      timePicker: true,
      timePickerIncrement: 10,
      locale: {
    format: '<?php echo FunctionManager::dateFormatJS();?> h:mm A'
    }
  });

 $('#quote_start_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> HH:mm:ss'});
  $('#qoute_end_date').datetimepicker({format: '<?php echo FunctionManager::dateFormatJS();?> HH:mm:ss'});

    $('#needed_date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_3",
        timePicker: true,
        timePickerIncrement: 10,
        locale: {
            format: '<?php echo FunctionManager::dateFormatJS();?> h:mm A'
        }
    });

    /*
    $('body').on('click', "input[name^='product_name']", function() {
        createProductAutocompleteSearch();
    });
    */

    $('body').on('click', "input[name^='vendor_name_']", function() {
       // createVendorAutocompleteSearch();
    });
    <?php //echo AppUrl::bicesUrl('orders/getVendors'); if you uncomment the placed this line in line number 1364 under url ?>

   $(".select2").select2({
      placeholder: 'Invite Suppliers', 
      minimumInputLength: 1,
     /* border-radius: '10px',
      border: '1px solid #4d90fe',*/
      ajax: {
        url: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
        dataType: 'json', quietMillis: 50, type: "GET",
      data: function (term) { return { query: term }; },
      processResults: function (data) { 
        $('#vendor_name_select2_1').trigger('change.select2');
        return { results: data }; }     
      }
    });

  <?php if (isset($quote_vendors) && is_array($quote_vendors) && count($quote_vendors)) { ?>
    <?php for ($vendor_idx=1; $vendor_idx<=count($quote_vendors); $vendor_idx++) { ?>
      populateVendorDetails(<?php echo $vendor_idx; ?>,<?php echo $quote_vendors[$vendor_idx-1]['vendor_id'];?>);
    <?php } ?>
  <?php } ?>


});

function dropdownVendors(obj){
    var testingobj = obj;
    var options_html="";
     $.ajax({
            type: "POST", data: { }, dataType: "json",
            url: BICES.Options.baseurl + '/orders/getVendors',
            success: function(options) {
             
                var options_html = '<option value="">Select Vendor</option>';
                for (var i=0; i<options.suggestions.length; i++)
                    options_html += '<option value="' + options.suggestions[i].data + '">' +  options.suggestions[i].value + '</option>';
                     $("#"+testingobj).append(options_html);
               
               
            }
           
        });
    
   }

function populateVendorDetails(vendor_idx_rownum,vendor_idx)
{  
    $.ajax({
        type: "POST", data: { vendor_ids: vendor_idx,quote_id:"<?php echo !empty($quote['quote_id'])?$quote['quote_id']:0;?>" }, dataType: 'json',
        url: "<?php echo AppUrl::bicesUrl('quotes/getVendorDetails/'); ?>",
        error: function() { $('#added_vendors_' + vendor_idx_rownum).html('<div class="clearfix"> <br /> </div>Cannot load selected supplier details ... please try again !!!') },
        success: function(data) {
      var vendor_html = '<div class="clearfix"> <br /> </div>';
      var current_vendor_html = "";
      
      for (var i=0; i<data.length; i++)
      {
        current_vendor_html = "";
        current_vendor_html += '<div class="form-group">';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Supplier Name </label>';
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_name_' + data[i].vendor_id + '" value="' + data[i].vendor_name + '" placeholder="Supplier Name">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Contact Person</label>';
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_contact_' + data[i].vendor_id + '" value="' + data[i].contact_name + '" placeholder="Contact Person">';
        current_vendor_html += '</div>';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="form-group">';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Address Line 1</label>';
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_address_1_' + data[i].vendor_id + '" value="' + data[i].address_1 + '" placeholder="Address Line 1">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Address Line 2</label>';
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_address_2_' + data[i].vendor_id + '" value="' + data[i].address_2 + '" placeholder="Address Line 2">';
        current_vendor_html += '</div>';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="form-group">';
        current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">City</label>';
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_city_' + data[i].vendor_id + '" value="' + data[i].city + '" placeholder="City">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">State</label>';
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_state_' + data[i].vendor_id + '" value="' + data[i].state + '" placeholder="State">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">Zip Code</label>';
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_zip_' + data[i].vendor_id + '" value="' + data[i].zip + '" placeholder="Zip Code">';
        current_vendor_html += '</div>';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="form-group">';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Email Address</label>'; 
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_email_' + data[i].vendor_id + '" value="' + data[i].emails + '" placeholder="Email Address">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Phone Number</label>';
        current_vendor_html += '<input type="text" class="form-control notranslate" name="vendor_phone_' + data[i].vendor_id + '" value="' + data[i].phone_1 + '" placeholder="Phone Number">';
        current_vendor_html += '</div>';
        current_vendor_html += '</div>';
        current_vendor_html += '<input type="hidden" class="notranslate" name="display_vendor_id_' + vendor_idx + '" id="display_vendor_id_' + vendor_idx_rownum + '" value="' + data[i].vendor_id + '">';
        
        vendor_html = vendor_html + current_vendor_html;
        vendor_html = vendor_html + '<div class="clearfix"><br /></div>';
      }

      $('#added_vendors_' + vendor_idx_rownum).html(vendor_html);
    }
    });
}

function createProductAutocompleteSearch()
{
    var product_type = '';
    $("input[name^='product_name']").each(function () {
        product_type = $(this).closest('.form-group').find('.product_type').val();
        $(this).devbridgeAutocomplete({
            serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getProducts'); ?>',
            params: { product_type: 'Product' },
            onSelect: function(suggestion) {
                $(this).closest('.form-group').find('.product_id').val(suggestion.data.id);
                if ($.trim($(this).closest('.form-group').find('.price_of_product').val()) == ''
                    || $.trim($(this).closest('.form-group').find('.price_of_product').val()) == '0')
                  $(this).closest('.form-group').find('.price_of_product').val(suggestion.data.price);
                if ($.trim($(this).closest('.form-group').find('.product_quantity').val()) == '')
                  $(this).closest('.form-group').find('.product_quantity').val(1);
            }
        });
    });
}

function createVendorAutocompleteSearch()
{
  var my_id = "";
    $("input[name^='vendor_name_']").each(function () {
      my_id = $(this).attr('id');
      if (my_id != 'vendor_name_VENDOR_IDX')
      {
          $(this).devbridgeAutocomplete({
              serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
              onSelect: function(suggestion) {
                  $(this).closest('.form-group').find('.vendor_id').val(suggestion.data);
              }
          });
    }
    });
}

function displayVendorQuote(quote_id)
{
  var vendor_id = $('#quote_vendor_id').val();
  $('#vendor_quote_area').html('');
  
  if (vendor_id != 0)
  {
    $('#vendor_quote_area').html('Please wait ... retrieveing selected supplier quote ... ');
      $.ajax({
      url: '<?php echo AppUrl::bicesUrl('quotes/getVendorQuote'); ?>', 
      data: { quote_id: quote_id, vendor_id: vendor_id }, type: "POST",
      success: function(quote_data) { $('#vendor_quote_area').html(quote_data) }, 
      error: function() { 
        $('#vendor_quote_area').html('ERROR ! Cannot retrieve selected supplier quote. Please try later ... '); 
      }   
    });
  }
}

function displaySupplierEvaluation(quote_id, state){
  var vendor_id = $("#evalution_vendor_id").val();
  $('#vendor_evaluation_area').html('');
  if (vendor_id != 0 && state === 'change'){
    displaySupplierEvaluationAjax(vendor_id, quote_id);
  }else if(state === 'default'){
    displaySupplierEvaluationAjax(vendor_id, quote_id);
  }
}

function displaySupplierEvaluationAjax(vendor_id, quote_id){
  $('#vendor_evaluation_area').html('Please wait ... retrieveing selected supplier quote ... ');
    $.ajax({
     url: '<?php echo AppUrl::bicesUrl('quotes/getVendorEvaluation'); ?>',
     data: { quote_id: quote_id, vendor_id: vendor_id }, type: "POST",
      success: function(quote_data) { $('#vendor_evaluation_area').html(quote_data) 
     },
     error: function() {
      $('#vendor_evaluation_area').html('ERROR ! Cannot retrieve selected supplier quote. Please try later ... ');
     }
  });
}

displaySupplierEvaluation(<?php echo $quote_id; ?>, 'default');
$(document).on('change', '#evalution_vendor_id', function() {
    var quote_id = <?php echo $quote_id; ?>;
    displaySupplierEvaluation(quote_id , 'change');
});



function loadDepartmentsForSingleLocation(department_id)
{
    var location_id = $('#location_id').val();
    var single = 1;

    if (location_id == 0)
        $('#department_id').html('<option value="">Select Department</option>');
    else
    {
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getDepartments',
            success: function(options) {
                var options_html = '<option value="">Select Department</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                $('#department_id').html(options_html);
                if (department_id != 0) $('#department_id').val(department_id);
            },
            error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
        });
    }
}

function loadSubcategories(input_subcategory_id)
{
  var category_id = $('#category_id').val();
  
  if (category_id == 0)
    $('#subcategory_id').html('<option value="0">Select Sub Category</option>');
  else
  {
      $.ajax({
          type: "POST", data: { category_id: category_id,subcategory_id:input_subcategory_id}, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
          success: function(options) {
            var options_html = '<option value="0">Select  Sub Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#subcategory_id').html(options_html);
            if (input_subcategory_id != 0) $('#subcategory_id').val(input_subcategory_id); 
          },
          error: function() { $('#subcategory_id').html('<option value="0">Select Sub Category</option>'); }
      });
  }
}
function loadSubcategoriesContract(input_subcategory_id)
{
  var category_id = $('#contract_category_id').val();
  
  if (category_id == 0)
    $('#contract_subcategory_id').html('<option value="0">Select Sub Category</option>');
  else
  {
      $.ajax({
          type: "POST", data: { category_id: category_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
          success: function(options) {
            var options_html = '<option value="0">Select  Sub Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#contract_subcategory_id').html(options_html);
            if (input_subcategory_id != 0) $('#contract_subcategory_id').val(input_subcategory_id); 
          },
          error: function() { $('#contract_subcategory_id').html('<option value="0">Select Sub Category</option>'); }
      });
  }
}

function deleteQuoteFile(file_idx, file_name)
{
  $('#delete_quote_file_link_' + file_idx).confirmation({
    title: "Are you sure you want to delete the attached file?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
      $('#existing_file_id_' + file_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('quotes/deleteFile/'); ?>",
            data: { quote_id: $('#quote_id').val(), file_name: file_name }
        });
    },
    onCancel: function() {  }
  }); 
}

function addDocument()
{
  var total_documents = $('#total_documents').val();
  total_documents = parseInt(total_documents);
  total_documents = total_documents + 1;
  $('#total_documents').val(total_documents);
  
  var new_document_html = $('#new_document_code').html();
  new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
  $('#total_documents').before(new_document_html);
}

function deleteDocument(docidx)
{
    var display_document_count = 0;
    $("div[id^='document_area']").each(function () {
        if ($(this).is(':visible')) display_document_count += 1;
    });

    if (display_document_count <= 1) alert("You must have at least one document field in the quote form");
    else
    {
        if (confirm("Are you sure you want to delete this document?"))
        {
            $('#delete_document_flag_' + docidx).val(1);
            $('#document_area_' + docidx).hide();
            $('#end_document_area_' + docidx).hide();
        }
    }
}

function addQuestion()
{
    var total_questions = $('#total_questions').val();
    total_questions = parseInt(total_questions);
    total_questions = total_questions + 1;
    $('#total_questions').val(total_questions);

    var new_question_html = $('#new_question_code').html();
    new_question_html = new_question_html.replace(/QIDX/g, total_questions);
    $('#total_questions').before(new_question_html);
}
function deleteQuestion(qidx)
{
    var display_question_count = 0;
    $("div[id^='question_area']").each(function () {
        if ($(this).is(':visible')) display_question_count += 1;
    });

    if (display_question_count <= 1) alert("You must have at least one question field in the quote form");
    else
    {
        if (confirm("Are you sure you want to delete this question?"))
        {
            $('#delete_question_flag_' + qidx).val(1);
            $('#question_area_' + qidx).hide();
            $('#end_question_area_' + qidx).hide();
        }
    }
}
function addAnswer(index)
{

    var new_index = $('#total_answers_'+index).val();
    new_index = parseInt(new_index);
    new_index = new_index + 1;
    $('#answer_area_' +index+'_'+ new_index).show();
    $('#total_answers_'+index).val(new_index);
}
function deleteAnswer(ansidx,index)
{
    var display_answer_count = 0;
    $("div[id^='answer_area']").each(function () {
        if ($(this).is(':visible')) display_answer_count += 1;
    });

    if (display_answer_count <= 1) alert("You must have at least one answer field in the quote form");
    else
    {
        if (confirm("Are you sure you want to delete this answer?"))
        {
            var new_index = $('#total_answers_'+index).val();
            new_index = parseInt(new_index);
            new_index = new_index - 1;
            $('#total_answers_'+index).val(new_index);
            $('#delete_answer_flag_' + ansidx).val(1);
            $('#answer_area_' + index+'_'+ansidx).hide();
            $('#end_answer_area_' + ansidx).hide();

        }
    }
}

function addVendor()
{
  var vendor_count = $('#vendor_count').val();
  vendor_count = parseInt(vendor_count);
  vendor_count = vendor_count + 1;
  $('#vendor_count').val(vendor_count);
  
  var new_vendor_html = $('#new_vendor_code').html();
  new_vendor_html = new_vendor_html.replace(/VENDOR_IDX/g, vendor_count);
  $('#vendor_count').before(new_vendor_html);
  dropdownVendors("vendor_name_"+vendor_count);
}
</script>
<?php for ($vendor_idx=1; $vendor_idx<=$vendors_to_show; $vendor_idx++) { ?>
<script type="text/javascript">
dropdownVendors("vendor_name_<?php echo $vendor_idx;?>");
</script>
<?php } ?>

<script type="text/javascript">

function deleteVendor(vendor_idx)
{
  var display_vendor_count = 0;
    $("div[id^='vendor_area']").each(function () {
      if ($(this).is(':visible')) display_vendor_count += 1;
  });
  
  if (display_vendor_count <= 1) alert("You must have at least one vendor field in the quote form");
  else
  {
    if (confirm("Are you sure you want to delete this vendor?"))
    {
      $('#delete_vendor_flag_' + vendor_idx).val(1);
      $('#vendor_area_' + vendor_idx).hide();
      $('#added_vendors_' + vendor_idx).hide();
    }
  }
}

function displayVendorDetails(vendor_idx_rownum,obj)
{

  var vendor_idx = $('#vendor_name_'+vendor_idx_rownum).val();
   
  if($('#added_vendors_' + vendor_idx).is(':visible')) $('#added_vendors_' + vendor_idx).hide();
  else
  { 
    var display_vendor_id = $('#display_vendor_id_' + vendor_idx).val();
    var my_vendor_id = $('#vendor_id_' + vendor_idx).val();
    if (true) 
    {
      $('#added_vendors_' + vendor_idx).html('');
      populateVendorDetails(vendor_idx_rownum,vendor_idx);
    }
    $('#added_vendors_' + vendor_idx_rownum).show();
  }
}

function selectType(index){

    var qurstionType = $('#question_type_'+index).val();

    if(qurstionType=='yes_or_no'){
        $('#yes_no_area_'+index).show();
        $('#answer_area_'+index+'_1').hide();
        $('#answer_section_'+index).hide();
    } else {
        if(qurstionType=='multiple_choice'){
            $('#yes_no_area_'+index).hide();
            $('#answer_area_'+index+'_1').show();
            $('#answer_section_'+index).show();
        } else {
            $('#yes_no_area_'+index).hide();
            $('#answer_area_'+index+'_1').hide();
            $('#answer_section_'+index).hide();

        }
    }


}

function expandChart(display_index)
{
    var show_index = display_index;
    var hide_index = 0;
    if (display_index % 2 != 0) hide_index = show_index - 1;
    else hide_index = show_index + 1;


    if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
    else
    {
        $('#chart_area_' + hide_index).hide(1000);
        $('#table_area_' + hide_index).hide(1000);

        $('#chart_area_' + show_index).show(1000);
        $('#table_area_' + show_index).show(1000);
    }
}

function collapseChart(display_index)
{
    var show_index = display_index;
    var hide_index = 0;
    if (display_index % 2 != 0) hide_index = show_index - 1;
    else hide_index = show_index + 1;

    $('#table_area_' + show_index).hide(1000);
    $('#table_area_' + hide_index).hide(1000);

    if (!$('#chart_area_' + show_index).is(':visible'))
        $('#chart_area_' + show_index).show(2000);
    if (!$('#chart_area_' + hide_index).is(':visible'))
        $('#chart_area_' + hide_index).show(2000);
}

var colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];
        var options = {
             chart: {
                height: 350,
                type: 'bar',
                stacked: true,
                toolbar: {
                  show: false
                },
           
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '10%',
                    //endingShape: 'rounded'  
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [<?php echo $quote_stacked_bar_graph['datasets']; ?>],
            xaxis: {
                categories: [<?php echo "'".implode("','",explode(',',$quote_stacked_bar_graph['vendors']))."'"; ?>],
                labels: {
                    style: {
                        colors: colors,
                        fontSize: '12px',
                    }
                }
            },
            
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return /*"$ " +*/ val /*+ " thousands"*/
                    }
                }
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#chart"),
            options
        );
      chart.render();

    var options = {
      chart: {
        height: 200,
        type: 'heatmap',
          toolbar: {
             tools: {
                download: false
            }
          },
      },
      stroke: {
        width: 0
      },
      plotOptions: {
        heatmap: {
          radius: 30,
          enableShades: false,
          shadeIntensity: 3,
          reverseNegativeShade: true,
          distributed: true,
          colorScale: {},

        }
      },
      dataLabels: {
        enabled: true,
        style: {
          colors: ['#fff']
        }
      },
      series: [<?php echo $heatMapSeries;?>],

      xaxis: {
        categories: [<?php echo "'".implode("','",$vendor)."'";?>],
        type: 'category',
      },
      
     /* title: {
        text: 'Currency '+<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']);?>;
      },*/

    }

    var chart = new ApexCharts(
      document.querySelector("#chartheatmap"),
      options
    );

    chart.render();


$('#table_Supplier_Analysis').dataTable({
       "pageLength": 50,
        "columnDefs": [ 
          { "targets": 0, "width": "6%", "orderable": false },        
        ],
        "order": []
    });

function addSavingModal(e){
  e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo $this->createUrl('savings/createByModal',array('quote_id'=>$quote_id,'cost_savings'=>round($quote['cost_savings']),'vendor_id'=>$quote['awarded_vendor_id'],'location_id'=>$quote['location_id'],'department_id'=>$quote['department_id'],'contract_category_id'=>$quote['category_id'],'contract_subcategory_id'=>$quote['subcategory_id'],'currency_id'=>$quote['currency_id'],'quote-name'=>$quote['quote_name'])); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_saving_cont').html(mesg);
            $('#create_saving').modal('show');   
        }
    });
}

$(document).ready(function() {
  $('#questionaire_table').dataTable({
    "columnDefs": [ {} ],
    "createdRow": function ( row, data, index ) {
      if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 ){
        for (var i=1; i<=6; i++)
          $('td', row).eq(i).css('text-decoration', 'line-through');
      }
    },        
    "order": [[ 1,"desc" ]],
    "pageLength": 50       
  });
});
</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
<script>
  $(function() {
        $('.quote_question_select_2').multiselect();
        $(".quote_user_scoring").treeFy({
          treeColumn: 1,
          expanderTemplate:'<span class="treetable-expander"></span>',
          indentTemplate:'<span class="treetable-indent"></span>',
          expanderExpandedClass:'fa fa-angle-down',
          expanderCollapsedClass:'fa fa-angle-right',
          initStatusClass:'treetable-collapsed'
        });
        // Auto expand the first element on initialization
        $(".quote_user_scoring .treetable-expander:first").trigger('click');
  });
</script>
<style type="text/css">
.reopen-vendor-error { float: right; }
.reopen-erro { color: #ca2b2b; }
.invalid {  background-color: #ffdddd !important;}
.moveall,.removeall { height: 28px;}
.apexcharts-tooltip-title {
  font-family: popin;  font-size: 12px;  background: #12A79C !important;
  color: #fff;  font-family:Poppins !important;}
div.dataTables_filter input {  margin-left: 0.5em !important;  padding: 0 !important;}
.tile_count {margin-bottom: 10px;  margin-top: 10px;}
.apexcharts-yaxis-title,.apexcharts-xaxis-title { font-family: Poppins !important; }
.select2-hidden-accessible  {  border-radius: 10px !important;  border: 1px solid #4d90fe !important;}
.modal.in .modal-dialog { width: 70%; }
canvas { display: block; max-width: 800px; margin: 60px auto; }
.saving-view-btn{ background: #357ae8 !important; border: 1px solid #357ae8 !important; }
.treetable-expander, .treetable-indent{width:0px; height: 0px;}
.fa-angle-down, .fa-angle-right{width:16px;}
</style>