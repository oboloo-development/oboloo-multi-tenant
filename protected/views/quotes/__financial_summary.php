     <div class="col-md-12 col-sm-12 col-xs-12" style="display:block !important;">
       <div class="clearfix"></div><br />
        <div class="form-group">
        <h4 style="padding: 10px;" class="supplier-comp-tab-h3 details_heading">Financial Analysis</h4>
        <h6 style="padding: 10px;" >Product financials from the latest received supplier submissions<br /><br /></h6>
        <br />
          
        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 20px;">
         <?php if (isset($quote_vendors) && is_array($quote_vendors) && count($quote_vendors)){ ?>
          <select name="evalution_vendor_id[]" class="form-control notranslate" id="evalution_vendor_id" multiple >
           <?php foreach ($quote_vendors as $quote_vendor){?>
            <option  value="<?php echo $quote_vendor['vendor_id']; ?>" > <?php echo $quote_vendor['vendor_name']; ?></option>
           <?php } ?> 
          </select>
         <?php } ?>
        </div>
      </div>

        <div class="clearfix"></div>
        <div id="vendor_evaluation_area">
          <div class="row m-0">
           
            <div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_8">
              <div class="x_panel tile overflow_hidden">
                <div class="x_title">
                 <h4 class="details_heading">Total Product Cost Comparison</h4>
                 <div class="clearfix"></div>
                </div>
                <div class="x_content" style="text-align: center;">
                 <div id="chart" height="110px"></div>
                </div>
              </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_8">
              <div class="x_panel tile overflow_hidden">
                <div class="x_title">
                    <h2>Total Product Cost Comparison</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <table class="tile_info" style="margin-left: 0px;">
                <?php
                    $idx = 0;
                    foreach ($quote_stacked_bar_graph['products'] as $dataset_idx => $a_dataset)
                    {
                    if($dataset_idx === 'total') continue;
                        $idx += 1;
                        switch ($idx)
                        {
                            case 1  : $color = 'salmon'; break;
                            case 2  : $color = 'purple'; break;
                            case 3  : $color = 'green'; break;
                            case 4  : $color = 'blue'; break;
                            case 5  : $color = 'red'; break;
                            case 6  : $color = 'antique-white'; break;
                            case 7  : $color = 'aqua-marine'; break;
                            case 8  : $color = 'bisque'; break;
                            case 9  : $color = 'khaki'; break;
                            default : $color = 'turquoise'; break;
                        } ?>
                        <tr>
                            <td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
                            <td style="vertical-align: middle;">
                                <?php echo $a_dataset['product_name']; ?> - <?php echo /*Yii::app()->session['user_currency_symbol'] . '' .*/ number_format($a_dataset['product_price'], 2); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </table>
                </div>
               </div>
             </div>
             <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_9">
                 <div class="x_panel tile overflow_hidden">
                     <div class="x_title">
                         <h2>Total Product Cost Comparison</h2>
                         <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                         <table class="tile_info" style="margin-left: 0px;">
                            <?php
                             $idx = 0;
                             foreach ($quote_answer_line_graph['data'] as $dataset_idx => $a_dataset)
                             {
                                 if ($dataset_idx === 'total') continue;
                                 $idx += 1;
                                 switch ($idx)
                                 {
                                     case 1  : $color = 'salmon'; break;
                                     case 2  : $color = 'purple'; break;
                                     case 3  : $color = 'green'; break;
                                     case 4  : $color = 'blue'; break;
                                     case 5  : $color = 'red'; break;
                                     case 6  : $color = 'antique-white'; break;
                                     case 7  : $color = 'aqua-marine'; break;
                                     case 8  : $color = 'bisque'; break;
                                     case 9  : $color = 'khaki'; break;
                                     default : $color = 'turquoise'; break;
                                 }
                                 ?>
                                 <tr>
                                     <td style="vertical-align: middle;">
                                        <?php echo $a_dataset['question']; ?> ?
                                     </td>
                                 </tr>
                                 <tr>
                                     <td>
                                         <?php
                                         if($a_dataset['question_type']=='free_text'){
                                             $answer = $a_dataset['free_text'];
                                         }elseif($a_dataset['question_type']=='yes_or_no'){
                                             $answer = $a_dataset['yes_no'];
                                         }else{
                                             //$quote = Quote();
                                             //$answer = $quote->getAnswer($a_dataset['quote_answer']);
                                             $answer = $a_dataset['quote_answer'];
                                         }
                                         ?>
                                         <table class="tile_info" style="margin-left: 0px;">
                                             <tr> s
                                                 <td style="width: 1%;">
                                                     <i  style="margin-left:5px;background-color:<?php echo $color; ?>;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
                                                 </td>
                                                 <td >
                                                     <?php echo $answer; ?>
                                                 </td>
                                             </tr>
                                             <tr><td colspan="2" style="height: 10px;">&nbsp;</td></tr>
                                         </table>
                                     </td>
                                 </tr>
                                 <?php
                             }
                             ?>
                         </table>
                     </div>
                 </div>
             </div>

         </div>
         <div class="row m-0">
              
             <div class="col-md-12 col-sm-12 col-xs-12" id="table_area_10">
                <div class="">
                    <table class="table table-striped table-bordered" style="width: 100%; " id="simpleTable1">
                        <?php
                        $heatMapSeries = '';
                        $vendor = array_unique($quote_stacked_bar_graph['vendor_name']);
                        $vendorProduct = $quote_stacked_bar_graph['vendorProduct'];
                        asort($vendorProduct);
                        $idx = 0; 
                        if(!empty($vendorProduct)){
                        echo "<tr><th>Product Name</th>";
                        foreach ($vendor as $vendor_name)
                        { ?><th class="notranslate"><?php echo $vendor_name;?></th><?php }
                        echo "</tr>";
                        }

                      foreach ($vendorProduct as $product_name=>$vendorInfo){?>
                        <tr><th><?php echo $product_name;$heatMapSeries.="{name:'".$product_name."',data:["; ?></th>
                        <?php foreach ($vendor as $vendor_name){?>
                              <th>

                                  <?php 
                              $productPrice = $vendorProduct[$product_name][$vendor_name];
                              $productPrice = number_format($productPrice*FunctionManager::currencyRate($tool_currency),0,".","");

                              echo /*html_entity_decode(Yii::app()->session['user_currency_symbol']).*/$currencySymbol.$productPrice;
                              $heatMapSeries.=$productPrice.",";
                              ?></th>
                      <?php } $heatMapSeries.="]},"; echo "</tr>";}?>

                    </table>
                </div>
             </div>
           </div>
         </div>
        </div> 