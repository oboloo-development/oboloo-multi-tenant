<div>

	<div class="col-md-3" style="width: 25% !important; margin-left: 36% !important;">
			<br /><br />
          <img class="img-responsive" src="<?php echo AppUrl::bicesUrl('images/app_logo.png'); ?>" />
          <div class="clearfix"> </div>
	</div>
    <div class="clearfix"> </div>
	
  <div class="login_wrapper" style="margin-top: 0% !important; max-width: 400px !important;">
    <div class="animate form login_form">
      <section class="login_content">
          <form novalidate id="quote_submit_form" name="quote_submit_form" method="post" action="<?php echo AppUrl::bicesUrl('quotes/submitQuote'); ?>">
          <h1>Enter Quote Invitation Reference</h1>
          <div class="item form-group">
            <input id="reference" name="reference" type="text" class="form-control" placeholder="Invitation Reference" value="<?php if (isset($invitation_reference)) echo $invitation_reference; ?>"/>
          </div>
          <div class="clearfix"></div>
          <div style="width: 350px;">
            <button class="btn btn-default submit" onclick="$('#quote_submit_form').submit();">Retrieve Invitation</button>
          </div>
          <input type="hidden" name="form_submitted" id="form_submitted" value="1" />

          <div class="clearfix"></div>

            <div class="clearfix"></div>
            <br />

          </div>
        </form>
      </section>
    </div>

    <?php if (isset($error) && $error == 1) { ?>
        <div id="message_area" style="color: orange; text-align: center; font-size: 120%;">
        	<br />
            Log in Failed - Tender has not started yet or login not recognised.
            <br /><br />
        </div>
    <?php } ?>

    <div id="status_area">
    </div>
    <div class="clearfix"></div>


  </div>
</div>

