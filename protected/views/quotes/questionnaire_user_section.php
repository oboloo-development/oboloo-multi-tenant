<div class="right_col" role="main">
 <div class="form-group">
  <div class="col-md-6 col-sm-6 col-xs-12">
    <!-- <h4>Quote Questionnair </h4> -->
   <div>&nbsp;</div>
  </div>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <a href="<?php echo AppUrl::bicesUrl('quotes/list'); ?>">
     <button type="button" class="btn btn-default pull-right" style="background-color: #F79820;color: #fff;border-color: #F79820;">
       <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Return to eSourcing List
     </button>
    </a>
 </div>
 <div class="clearfix">&nbsp;</div>

  <h4>
   <?php if(isset($questionnaireuser['user_id'])){?>
     <br><strong class="notranslate">User Name</strong>&nbsp;:&nbsp;
     <span class="title-text notranslate"><?= $questionnaireuser['full_name']; ?></span>
   <?php } if(!empty($questionnaireuser['quote_internal_user_role'])) {?>
     <br><br><strong class="notranslate">Role</strong>&nbsp;:&nbsp;<span class="title-text notranslate">
     <?= CommonFunction::getQuoteParticipateUser($questionnaireuser['quote_internal_user_role']); ?></span>
   <?php } ?>
  </h4>
<br><br>
 </div>




 <!-- table quote users -->
         
 <?php if(!empty($questionnaireuserList)){ ?>
 <div class="table-responsive">
  <table class="table table-striped">
      <thead>
          <tr>
              <th>Username</th>
              <th>Role</th>
          </tr>
      </thead>
       <tbody>
          <?php foreach($questionnaireuserList as $usersList) { ?>
          <tr>
            <td><?= $usersList['full_name']; ?></td>
            <td><?= CommonFunction::getQuoteParticipateUser($usersList['quote_internal_user_role']); ?></td>
          </tr>
          <?php }  ?>
      </tbody>
  </table>  
 </div>
<?php } else{ ?>
    <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        No record found</div>
<?php } ?>
</div>







