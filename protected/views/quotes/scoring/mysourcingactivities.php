<div class="right_col" role="main">

    <div class="row-fluid tile_count">
      <div class="span6 pull-left">
       <h3>My Sourcing Activities</h3><br />
       <h6 class="subheading">Below are a list of eSourcing activities that you have either created or been asked contribute to before they go live to suppliers.</h6>
      </div>

      <div class="clearfix"> </div>
        <?php if(Yii::app()->user->hasFlash('success')):?>
         <div class="alert alert-success">
          <?php echo Yii::app()->user->getFlash('success'); ?>
         </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->hasFlash('error')):?>
            <div class="alert alert-danger">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
      </div>

    <div class="row tile_count m-0">
     <div class="table-responsive">
      <table id="quote_table" class="table table-striped table-bordered quote-table " style="width: 100%">
       <thead>
        <tr>
          <th style="width:6%;" class="th-center">Contribution Status</th>
          <th style="width:5%">ID</th>
          <th style="width:16%">Title</th>
          <th style="width:20%;" class="text-center">Contribution Deadline (UTC)</th>
          <th style="width:15%" class="th-center">Opening Date (UTC)</th>
          <th style="width:15%" class="th-center">Closing Date (UTC)</th>
          <th style="width:15%" class="th-center">Created By</th>
          <th style="width:8%" class="text-center">Awarded<br />Supplier</th>
          <th style="width:5%" class="th-center">Currency</th>
          <th style="width:7%" class="th-center">Cost Savings</th>
          <th style="width:5%" class="th-center">Status</th>
        </tr>
       </thead>
      <tbody>
      </tbody>
     </table>
    </div>
    </div>
</div>
<style>
  .th-center{text-align: center; text-wrap: nowrap;}
</style>
<script>
$('#quote_table').dataTable({
    "columnDefs": [ {
      "targets": 0,
    } ],

    "createdRow": function ( row, data, index ) {
     if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 ){
       for (var i=1; i<=6; i++)
        $('td', row).eq(i).css('text-decoration', 'line-through');
     }
    },        
    "order": [[ 1,"desc" ]],
    "pageLength": 50,        
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?php echo AppUrl::bicesUrl('quotes/sourcingActivitiesListAjax'); ?>",
      "type": "POST",
      data: function ( input_data ) {
        input_data.from_date = $('#from_date').val();
        input_data.to_date = $('#to_date').val();
        input_data.location_id = $('#location_id').val();
        input_data.department_id = $('#department_id').val();
        input_data.category_id = $('#category_id').val();
        input_data.subcategory_id = $('#subcategory_id').val();
    }    
    },
    "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      if(aData[1].search("<span class='deactivated_vendor'></span>")>0){
        $(nRow).addClass('deactivated_record');
        /*$(nRow).css("background-color", "#ccc");*/
      }
      for (var i=7; i<=10; i++){
        $('td:eq('+i+')', nRow).addClass('td-center');
      }
      for (var i=8; i==8; i++){
        $('td:eq('+i+')', nRow).addClass('table-currency');
      }
     }
});
</script>