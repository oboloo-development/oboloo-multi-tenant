<div class="right_col" role="main">
  <div class="row-fluid tile_count">
    <div class="span6 pull-left mb-45">
      <h3 class="">Score Supplier Responses</h3>
      <h5 class="subheading pt-15" style="line-height: 20px;">Below you can score the responses that each supplier has given against each questionnaire in the sourcing activity. </br>
       You can score a suppliers answer 1-5 (1 being a low score and 5 the highest).
      </h5>
    </div>
    <div class="clearfix"> </div>
    <?php
    $loginUserID = Yii::app()->session['user_id'];
    $scorerUSerID= !empty($_GET['scorer']) ? $_GET['scorer'] : $loginUserID;
    $sql = " select status from quote_scorer_questions_form where user_id=".$scorerUSerID." and quote_id=".$quote_id." group by scoring_row_id ";
    $scorerStatus = Yii::app()->db->createCommand($sql)->queryRow();
    
     if(!empty($scorerStatus) && $scorerStatus['status'] == 2){ ?>
        <div class="alert alert-success text-center">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Scoring Complete
        </div>
    <?php }
    if(!empty($scorerStatus) && $scorerStatus['status'] == 3){ ?>
      <div class="alert alert-danger text-center">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Scoring Closed
      </div>
    <?php } ?>

    <div class="clearfix"> </div>
    <?php
    $ctr = 0;
    $scoreStatus = "";
    $scorerFlage = '';
    $loginUserID = Yii::app()->session['user_id'];
    if (empty($_GET['scorer']) || (int)$_GET['scorer'] == $loginUserID) {
      $data = CommonFunction::getScorerQuestionnaire($loginUserID, $quote_id);
    }

    if(!empty($_GET['scorer']) && (int)$_GET['scorer'] != $loginUserID){
      $scorerID = $_GET['scorer'];
       $scorerFlage ='disabled';
      $data = CommonFunction::getScorerQuestionnaire($scorerID, $quote_id);
    }

    foreach ($data as $quoteForm) {
        $ctr += 1;
        $parentRow = $quoteForm['id'] . '_' . $quoteForm['question_form_id'] . '_' . $ctr; ?>
        <form id="form_submitted" action="<?php echo $this->createUrl('quotes/myscoringdetailSave'); ?>" method="post">
          <input type="hidden" name="quote_id" value="<?= $quote_id; ?>" />
          <input type="hidden" name="created_by_id" value="<?= $quoteForm['creator_id']; ?>" />
          <input type="hidden" name="form_id" value="<?= $quoteForm['id']; ?>" />
          <input type="hidden" name="question_form_id" value="<?= $quoteForm['question_form_id']; ?>" />
          <input type="hidden" name="questionniare_score" value="<?= $quoteForm['question_scorer']; ?>" />
          <table class="table table-striped quote_user_scoring" style="width: 100%">
            <thead>
              <tr data-node="treetable-<?= $parentRow; ?>" class="parent_row">
                <td style="width:40%" class="pt-15"><b><?= $quoteForm['question_form_name'] ?></b></td>
                <td style="width:40%" class="pt-15"><b>Supplier Answers</b></td>
                <td style="width:20%"><b>Score (Weighted Score = <?= $quoteForm['question_scorer'] ?> %) <span style="color: #a94442;">*</span></b></td>
              </tr>
            </thead>
            <tbody>
              <?php if (!empty($quoteForm['id'])) {
                $resQuestAndAnswer =  CommonFunction::getVendorQuestion($quote_id, $quoteForm['question_form_id']);
                if (!empty($resQuestAndAnswer)) {
                  foreach ($resQuestAndAnswer as $questAndAnswer) { ?>
                    <tr data-pnode="treetable-parent-<?= $parentRow; ?>" style="background-color: #4d90fe78;">
                      <td style="width:40%"><b><?= str_replace('"', '',$questAndAnswer['questions']); ?></b></td>
                      <td style="width:40%;"></td>
                      <td style="width:20%"></td>
                    </tr>
                    <?php if ($questAndAnswer['answer'] != "") {
                      $data = ['questions' => $questAndAnswer['questions'], 'quoteID' => $quote_id,
                               'questionFormId' => $quoteForm['question_form_id'],
                               'vendorID' => $questAndAnswer['vend_id']
                              ];
                      $vendorRender = CommonFunction::getVendorNameAndAnswers($data);
                      foreach ($vendorRender as $vValue) { ?>
                        <tr data-pnode="treetable-parent-<?= $parentRow; ?>" style="background: #4d90fe03 !important;">
                          <td style="width:40%; padding-top: 15px; font-weight: 600;"><?= $vValue['vendor_name']; ?></td>
                          <td class="notranslate" style="width:40%; padding-top: 15px;">
                          <?php
                              if($vValue['que_ans_type'] == 'select'){
                                  $values = explode(',', $vValue['vend_answer']); 
                                  $displayVal = implode('<br>', $values);
                                  echo "<p class='p-0 notranslate'>".$displayVal."</p>";
                              } 
                              else{ echo "<p class='p-0 notranslate'>".str_replace('"', '',$vValue['vend_answer'])."</p>"; } 
                          ?>
                          <td style="width:20% ">
                            <div class="form-group m-0">
                              <?php $scorerUserID = !empty($_GET['scorer']) ? $_GET['scorer'] : $loginUserID;
                              $sql = 'select * from user_quote_ans_score where quote_id ="' . $quote_id . '" and (user_id='. $scorerUserID .' ) and vendor_question_answer_id="' . $vValue['vqa_id'] . '"';
                              $selectedScore = Yii::app()->db->createCommand($sql)->queryRow();
                              ?>
                              <select class="form-control" name="scorer_score[<?php echo $vValue['vqa_id']; ?>]" required <?= !empty($selectedScore['score'])  && $selectedScore['status'] == "2" ? "disabled" : "" ?>>
                                <option value="">Select Score</option>
                                <?php for ($i = 1; $i <= FunctionManager::scoreValueMaximum(); $i++) { ?>
                                  <option value="<?php echo $i; ?>" <?php echo !empty($selectedScore['score'])  && $selectedScore['score'] == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </td>
                        </tr>
                  <?php }
                    }
                  } ?>
                  <tr data-pnode="treetable-parent-<?= $parentRow; ?>">
                    <td></td>
                    <td></td>
                    <td style="width: 15%;">
                      <div class="pull-right" >
                        <?php if (!empty($selectedScore['score']) && $selectedScore['status'] == 2) {
                          $scoreStatus = "disabled";
                        } else {
                          $scoreStatus = "";
                        } ?>
                        <input type="submit" class="btn green-btn submit_scoring" name="submit_score" value="Submit Scoring" <?= $scoreStatus; ?> <?= $scorerFlage; ?> />
                        <input type="submit" class="btn btn-primary submit_scoring" name="save_score" value="Save" <?= $scoreStatus; ?> <?= $scorerFlage; ?> />
                      </div>
                    </td>
                  </tr>
          <?php }
              }  ?>
</tbody>
</table>
</form>
<?php } ?>
</div>
</div>
<script>
  $(document).ready(function() {
    $(".quote_user_scoring").treeFy({
      treeColumn: 1,
      expanderTemplate: '<span class="treetable-expander"></span>',
      indentTemplate: '<span class="treetable-indent"></span>',
      expanderExpandedClass: 'fa fa-angle-down',
      expanderCollapsedClass: 'fa fa-angle-right',
      initStatusClass: 'treetable-collapsed'
    });
  });
</script>
<style>
.left_col{height: 1500px !important;}
.treetable-expander, .treetable-indent{width:0px; height: 0px;}
.fa-angle-down, .fa-angle-right{width:16px;}
</style>