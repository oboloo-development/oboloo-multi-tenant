<?php 
if(empty($fileReader)){ ?>
 <fieldset id="additional-field-model">
  <div class="form-group">
      <div class="col-md-4 col-sm-4 col-xs-4 date-input valid">
          <label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
          <input required type="text" class="form-control notranslate" value="" name="product_name[]" id="product_name_1" />
          <!-- <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span> -->
      </div>

      <div class="col-md-2 col-sm-2 col-xs-6 ">
          <label class="control-label">Product Code</label>
          <input type="text" class="form-control notranslate" value="" name="product_code[]" id="product_code_1" placeholder="Product Code" />
         
      </div>
       <div class="col-md-2 col-sm-2 col-xs-6">
          <label class="control-label">Unit of Measure </label>
        <input type="text" class="form-control notranslate" name="uom[]" placeholder="Unit of Measure" />
      </div>
      <div class="col-md-2 col-sm-2 col-xs-2 valid">
          <label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
          <input required style="text-align: right;" type="number" class="product_quantity form-control notranslate" name="quantity[]" placeholder="Quantity" id="quantity_1" />
      </div>
      <!-- <div class="col-md-3 col-sm-3 col-xs-3">
          <label class="control-label">Current Unit Price</label>
          <input type="text" class="price_of_product form-control" name="current_price[]" placeholder="Current Unit Price" />
      </div> -->
      <div class="col-md-2" style="margin-top: 22px;text-align: right;">
          <a href="javascript:void(0);" class="btn btn-link remove-this-field" style="background-color: rgb(226 69 69); border-color:rgb(226 69 69); color: #fff;">
              Remove Item
          </a>
          <a href="javascript:void(0);" class="btn btn-link create-new-field" style="background-color: #1abb9c;color: #fff; border-color: #1abb9c;">
              Add Item
          </a>
      </div>
  </div>
</fieldset>
<?php }else if(!empty($fileReader)){?>
  <div id="dinamic-fields">
	<?php $row = 0;
	while (($data = fgetcsv($fileReader,1000, ",")) !== FALSE) {
	if($row>0){?>

	<fieldset id="additional-field-model" class="dinamic-field">
     <div class="form-group">
      <div class="col-md-4 col-sm-4 col-xs-4  date-input valid">
          <label class="control-label notranslate">Product Name <span style="color: #a94442;">*</span></label>
          <input required type="text"  class="form-control notranslate" value="<?php echo $data[0];?>" name="product_name[]" id="product_name_<?php echo $row;?>" />
          <!-- <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span> -->
      </div>
      <div class="col-md-2 col-sm-2 col-xs-6">
          <label class="control-label">Product Code</label>
          <input type="text" class="form-control notranslate" value="<?php echo $data[1];?>" name="product_code[]" id="product_code_<?php echo $row;?>" />
         
      </div>
       <div class="col-md-2 col-sm-2 col-xs-6">
          <label class="control-label">Unit of Measure</label>
        <input type="text" class="form-control notranslate" name="uom[]" placeholder="Unit of Measure" value="<?php echo $data[2];?>" id="uom_<?php echo $row;?>"/>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-2 valid">
          <label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
          <input required style="text-align: right;" type="text" class="product_quantity form-control notranslate" name="quantity[]" placeholder="Quantity" value="<?php //echo $data[3];?>" id="quantity_<?php echo $row;?>" />
      </div>
      <!-- <div class="col-md-3 col-sm-3 col-xs-3">
          <label class="control-label">Current Unit Price</label>
          <input type="text" class="price_of_product form-control" name="current_price[]" placeholder="Current Unit Price" />
      </div> -->
      <div class="col-md-1" style="margin-top: 30px;">
          <a href="javascript:void(0);" class="btn btn-link remove-this-field">
              <span class="fa fa-remove hidden-xs"></span>
          </a>
          <a href="javascript:void(0);" class="btn btn-link create-new-field">
              <span class="fa fa-plus hidden-xs"></span>
          </a>
      </div>
  </div>
</fieldset>
<?php }$row++;} echo "</div>";}?>

<script type="text/javascript">
    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field"
    });
</script>