<?php // tooltip title 
$toolTipText = MessageManager::tooltipText();
$disabled = FunctionManager::sandbox();

?>
<div role="main">
  <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <span class="step" style="display:none;"></span>

    <h3 class="pull-left"><?php echo 'Create eSourcing Request'; ?></h3>
    <a href="https://oboloo.com/support-how-to-create-a-quote/" class="btn btn-warning tutorial pull-right ml-30" target="_blank" style="margin-top: -2px;">Tutorial</a>
    <div class="clearfix"> </div>
    <div class="" style="padding-right: 21px;padding-top: 3px;">
      <!-- <span class="step col-md-offset-1">1 
        <span style="color: #555;position: absolute;top: 77px;left: 28px;font-size: 15px;"> Quote Users</span></span> -->
      <span class="step col-md-offset-2" style="background-color:#f4f5fd;">1</span>
      <!-- <span style="color: #f4f5fd;position: absolute;top: 77px;left: 12%;font-size: 15px;"> Quote Description</span></span> -->
      <span class="step col-md-offset-3" style="background-color:#f4f5fd;">2</span>
      <!-- <span style="color: #f4f5fd;position: absolute;top: 77px; left: 31%; font-size: 15px;"> Scoring Criteria & Questioinnaires </span></span> -->
      <span class="step col-md-offset-3" style="background-color:#f4f5fd;">3</span>
      <!-- <span style="color: #f4f5fd;position: absolute;top: 77px; left: 65%; font-size: 15px;"> Invite Suppliers </span> -->
      <!-- </span> -->
    </div>
    <?php
    $userDepartment = FunctionManager::userSettings(Yii::app()->session['user_id'], 'department_id');
    $userLocation = FunctionManager::userSettings(Yii::app()->session['user_id'], 'location_id');
    ?>

  </div>
  <div class="clearfix"> </div>
  <?php if (!empty(Yii::app()->user->getFlash('success'))) : ?>
    <div class="tile_count">
      <div class="alert alert-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
      </div>
    </div>
  <?php endif; ?>
  <?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="tile_count">
      <div class="alert alert-danger">
        <?php echo Yii::app()->user->getFlash('error'); ?>
      </div>
    </div>
  <?php endif; ?>
  <div class="tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <div id="myTabContent" class="tab-content">
      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">
        <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/edit'); ?>" autocomplete="off">
          <!-- START: NEW TAB -->
          <!-- <div class="tab">
            <h3 class="col-md-12 col-sm-12 col-xs-12 quote-details-title">Quote Users<br /><br /></h3>
             <?php // $this->renderPartial('internal_user_participate_quote_activity'); 
              ?>
          </div> -->
          <!--END: New Tab-->
          <input type="hidden" class="quote_internal_user_class" name="quote_internal_users" />
          <input type="hidden" class="contributions_datetime" name="contribute_deadline" value="" />
          <div class="tab">
            <h3 class="col-md-12 col-sm-12 col-xs-12 quote-details-title">Quote Description<br /><br /></h3>
            <input type="hidden" class="notranslate" name="quote_send_to" id="quote_send_to" value="">
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 valid">
                <label class="control-label">Title <span class="error_color">*</span></label>
                <input required type="text" class="form-control  notranslate" name="quote_name" id="quote_name" <?php if (isset($quote['quote_name']) && !empty($quote['quote_name'])) echo 'value="' . $quote['quote_name'] . '"';
                                                                                                                else echo 'placeholder="Title"'; ?> required autocomlete="off">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 valid">
                <label class="control-label">Description/Notes <span class="error_color">*</span></label>
                <textarea class="form-control notranslate" name="quote_desc" id="quote_desc" placeholder="Quote description/notes" style="height: 200px;"></textarea>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
              <h3 class="col-md-12 col-sm-12 col-xs-12" style="font-size: 16px !important; margin-bottom: 6px;margin-top:20px; text-decoration: underline;">Quote Details</h3>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-6 valid">
                <label class="control-label">Location <span style="color: #a94442;">*</span></label>
                <select required name="location_id" id="location_id" class="form-control notranslate" onchange="loadDepartmentsForSingleLocation(this);">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                    <option value="<?php echo $location['location_id']; ?>" <?php if ($location['location_id'] == $userLocation) echo ' selected="SELECTED" '; ?>>
                      <?php echo $location['location_name']; ?>
                    </option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-6 valid">
                <label class="control-label">Department <span style="color: #a94442;">*</span></label>
                <select required name="department_id" id="department_id_new" class="form-control notranslate">
                  <option value="">Select Department</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Category</label>
                <select name="category_id" id="modal_category_id" class="form-control notranslate" onchange="loadSubcategories(0);">
                  <option value="0">Select Category</option>
                  <?php foreach ($categories as $category) { ?>
                    <option value="<?php echo $category['id']; ?>" <?php if (isset($contract['contract_category_id']) && $contract['contract_category_id'] == $category['id']) echo ' selected="SELECTED" '; ?>>
                      <?php echo $category['value']; ?>
                    </option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Sub Category</label>
                <select name="subcategory_id" id="subcategory_id_new" class="form-control notranslate">
                  <option value="0">Select Sub Category</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-6   date-input">
                <label class="control-label">Quote Currency <span style="color: #a94442;">*</span></label>
                <select name="quote_currency" id="quote_currency" class="form-control notranslate" required>
                  <?php foreach ($currency_rate as $rate) { ?>
                    <option value="<?php echo $rate['currency']; ?>" <?php if (isset(Yii::app()->session['user_currency_dropdown']) && Yii::app()->session['user_currency_dropdown'] == $rate['currency']) echo ' selected="SELECTED" '; ?>>
                      <?php echo $rate['currency']; ?>
                    </option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-6 valid">
                <label class="control-label">Spend Type</label>
                <select name="spend_type" id="spend_type" class="form-control notranslate">
                  <option value="">Select Spend Type</option>
                  <option <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                  <option <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>

                  <option <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Goods & Services') echo ' selected="SELECTED" '; ?> value="Goods & Services">Goods & Services</option>

                </select>
              </div>


            </div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-6 date-input valid">
                <label class="control-label">Opening Date & Time (UTC)<span style="color: #a94442;">*</span></label>
                <input required type="text" class="form-control notranslate" name="opening_date" id="opening_date" <?php if (isset($quote['opening_date']) && !empty($quote['opening_date'])) echo 'value="' . date("d/m/Y H:iA", strtotime($quote['opening_date'])) . '"';
                                                                                                                    else echo 'placeholder="Opening Date"'; ?> autoclose>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 date-input valid">
                <label class="control-label">Closing Date & Time (UTC)<span style="color: #a94442;">*</span></label>
                <input required type="text" class="form-control notranslate" name="closing_date" id="closing_date" <?php if (isset($quote['closing_date']) && !empty($quote['closing_date'])) echo 'value="' . date("d/m/Y H:iA", strtotime($quote['closing_date'])) . '"';
                                                                                                                    else echo 'placeholder="Closing Date"'; ?>>
              </div>
            </div>

            <div class="form-group">
              <h3 class="col-md-6 pull-left quote_items_title">Quote Items</h3>

              <div class="col-md-6 pull-right text-right mt-50">
                <div id="import_showhide" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> Import Products Via Template</div>
                <div id="import_hide" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-minus"></span> Import Products Via Template</div>
              </div>
            </div>
            <?php
            $total_price = 0;
            $tax_amount = 0;
            ?>
            <div class="clearfix"></div>
            <div id="product_import_area">

              <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Import Products</label>
                <input class="form-control notranslate" type="file" name="product_import" id="product_import" />
                <span class="text-danger" style="font-size: 8px;">Maximum File Size of 2MB</span>
                <span id="product_import_error" style="color: red;"></span>
              </div>


              <div class="col-md-2 col-sm-2 col-xs-6"><br />
                <button id="btn_product_import" class="btn btn-sm btn-success" onclick="importProduct(1,event);" style="margin-top: 11px;" <?php echo $disabled; ?>>Upload</button>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6 q_download_temp"><br />
                <a href="<?php echo $this->createAbsoluteUrl('quotes/productSample'); ?>" class="btn btn-sm btn-primary" style="margin-top: 11px;" <?php echo $disabled; ?>>Download Template</a>
              </div>

              <div class="clearfix"></div><br /><br />
            </div>

            <div id="here_is_where_you_add_new_items_to_this_quote">
              <div id="product_list_cont">
                <?php $this->renderPartial('_product', array('fileReader' => '')); ?>
              </div>
              <div class="clearfix"> </div>
            </div>

            <div class="clearfix"><br /></div>
            <div class="form-group">
              <h3 class="col-md-6 pull-left" style="font-size: 16px !important;margin-top:32px; text-decoration: underline;">Quote Documents</h3>

              <div class="col-md-6 pull-right text-right" style="margin-top:32px;">
                <div id="supporting_document_showhide" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-plus"></span> Attach Supporting Documents</div>
                <div id="supporting_document_hide" class="btn btn-sm btn-danger"> <span class="glyphicon glyphicon-minus"></span> Attach Supporting Documents</div>

              </div>
              <div class="clearfix"></div>
              <div id="supporting_document_area" style="margin-top: 26px">
                <div id="file_upload_alert" class="alert alert-success" role="alert"></div>
                <img class="processing_oboloo" src="<?php echo AppUrl::bicesUrl('images/icons/processin_oboloo.gif'); ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">

                <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Document</label>
                  <input class="form-control notranslate" type="file" name="order_file_1" id="order_file_1" />
                  <span class="text-danger" style="font-size: 8px;">Maximum File Size of 25MB</span>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <label class="control-label">File/Document Description</label>
                  <input type="text" class="form-control notranslate" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" />
                </div>

                <div class="col-md-1 col-sm-1 col-xs-12 text-right">
                  <button id="btn_upload_1" class="btn btn-sm btn-success" onclick="uploadDocument(1,event);" style="margin-top: 31px;" <?php echo $disabled; ?>>Upload</button>
                </div>
                <div class="clearfix"></div><br />
                <div id="document_list_cont"></div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="hidden" class="notranslate" name="quote_id" id="quote_id" value="<?php if (isset($quote_id)) echo $quote_id; ?>" />
                <input type="hidden" class="notranslate" name="quote_status" id="quote_status" value="" />
                <input type="hidden" class="notranslate" name="created_by_user_id" id="created_by_user_id" value="<?php if (isset(Yii::app()->session['user_id'])) echo Yii::app()->session['user_id'];
                                                                                                                  else echo '0'; ?>" />
                <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
              </div>
            </div>

            <div class="col-md-12 col-sm-12  col-xs-12">
              <div class="form-group">
                <!-- <div class="label-adjust"><label class="control-label">Quote Created By</label></div>-->
                <input type="hidden" class="form-control  notranslate" id="created_by_name" name="created_by_name" readonly <?php if (isset(Yii::app()->session['full_name']) && !empty(Yii::app()->session['full_name'])) echo 'value="' . Yii::app()->session['full_name'] . '"';
                                                                                                                            else echo 'placeholder="' . Yii::app()->session['full_name'] . '"'; ?>>
                <input type="hidden" id="created_by_user_id" class="notranslate" name="created_by_user_id" <?php if (isset(Yii::app()->session['user_id']) && !empty(Yii::app()->session['user_id'])) echo 'value="' . Yii::app()->session['user_id'] . '"'; ?>>


                <input type="hidden" id="user_id" name="user_id" class="notranslate" <?php if (isset(Yii::app()->session['user_id']) && !empty(Yii::app()->session['user_id'])) echo 'value="' . Yii::app()->session['user_id'] . '"'; ?>>

              </div>
            </div>

            <input type="hidden" name="manager_name" id="" class="notranslate" <?php if (!empty($users['position'])) echo 'value="' . $users['position'] . '"';
                                                                                else echo 'placeholder="Position/Title"'; ?>>

            <input type="hidden" class="notranslate" name="manager_user_id" id="manager_user_id" <?php if (isset($quote['manager_user_id']) && !empty($quote['manager_user_id'])) echo 'value="' . $quote['manager_user_id'] . '"'; ?> />



            <input type="hidden" class="form-control has-feedback-left notranslate" name="contact_email2" id="contact_email2" <?php if (!empty($users['email'])) echo 'value="' . $users['email'] . '"';
                                                                                                                              else echo 'placeholder="Contact Email Address"'; ?>>


            <input type="hidden" class="form-control has-feedback-left notranslate" name="contact_phone2" id="contact_phone2" <?php if (!empty($users['contact_number'])) echo 'value="' . $users['contact_number'] . '"';
                                                                                                                              else echo 'placeholder="Contact Phone Address"'; ?>>




            <input type="hidden" class="form-control notranslate" name="procurement_lead_name" id="procurement_lead_name" <?php if (isset($quote['procurement_lead_name']) && !empty($quote['procurement_lead_name'])) echo 'value="' . $quote['procurement_lead_name'] . '"';
                                                                                                                          else echo 'placeholder="Position/Title"'; ?>>
            <input type="hidden" name="procurement_lead_user_id" class="notranslate" id="procurement_lead_user_id" <?php if (isset($quote['procurement_lead_user_id']) && !empty($quote['procurement_lead_user_id'])) echo 'value="' . $quote['procurement_lead_user_id'] . '"'; ?> />



            <input type="hidden" class="form-control has-feedback-left notranslate" name="contact_email" id="contact_email" <?php if (isset($quote['contact_email']) && !empty($quote['contact_email'])) echo 'value="' . $quote['contact_email'] . '"';
                                                                                                                            else echo 'placeholder="Contact Email Address"'; ?>>
            <input type="hidden" class="form-control has-feedback-left notranslate" name="contact_phone" id="contact_phone" <?php if (isset($quote['contact_phone']) && !empty($quote['contact_phone'])) echo 'value="' . $quote['contact_phone'] . '"';
                                                                                                                            else echo 'placeholder="Contact Phone Number"'; ?>>
          </div>
          <div class="tab socoring_and_question_tab">
            <h3 class="pull-left" style="font-size: 16px !important; margin-bottom: 10px;margin-top:9px;margin-left: 8px; text-decoration: underline;">Questionnaires & Weighted Scoring</h3>
            <div class="clearfix"></div><br />
            <h4 class="col-md-12 subheading">
              Do you want to add questionnaires to this sourcing activity? </br></br>
              Suppliers can answer the questionnaires that you include below at the same time as responding to the other areas of your sourcing activity. Put a percentage next to each questionnaire to assign your weighted scoring.
              </br></br>
              You can create your own questionnaires within the eSourcing Questionnaires page.
            </h4>
            <div class="clearfix"></div><br />

            <div class="col-md-12 text-right mb-45">
              <button type="button" class="btn btn-danger remove_questionaire_section">Remove Questionnaire Section</button>
              <button type="button" class="btn btn-warning add_questionaire_section">Add Questionnaire Section</button>
            </div>
            <div class="col-md-12 mb-45">
              <div class="questionaire_section  mb-45">
                <div class="form-group col-md-8">
                  <label>Questionaire</label>
                  <select class="form-control" id="quote_questionaire">
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-12 mb-45">
              <div class="col-md-8 mb-45 questionaire_list">
                <div class="row">
                  <div class="col-md-7">
                    <h5 style="margin-bottom: 20px; margin-left: 10px;">Questionnaire Name</h5>
                  </div>
                  <div class="col-md-2">
                    <h5 style="margin-bottom: 20px;">Score (%)</h5>
                  </div>
                </div>
                <ul id="questionaire_sortable" class="add_new_questionire"></ul>
              </div>
              <div class="col-md-11 col-sm-6 col-xs-6 text-center">
                <div class="scoring_alert" style="color: red;"></div><br />
                <div class="scoring_percentage_alert " style="margin-left: -29px;"></div>
                <div class="scoring_alert2 pull-right" style='color: red;'></div>
              </div>
            </div>
          </div>
          <div class="tab scoring_supplier_tab">
            <h3 class="col-md-6" style="font-size: 16px !important; margin-bottom: 10px;margin-top:9px;text-decoration: underline;">Invite Suppliers</h3>

            <?php $sql = "select vendor_id,vendor_name from vendors where active=1  and member_type=0 and (vendor_archive is null or vendor_archive=''  or LOWER(vendor_archive)='null') and quick_created not in('Yes and Quick Sourcing Evaluation','Yes') ORDER by vendor_name ASC";
            $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll(); ?>
            <input type="hidden" name="total_vendors_new" id="total_vendors" value="1">
            <div class="form-group tab_w_auto_new_modal">
              <?php $ctr_vend = 0; ?>
              <div class="col-md-10 supplier_invite_container" style="padding-left: 0px;">
                <div class="vendor_new_QIX">
                  <div class="clearfix"></div>
                  <div class="col-md-5 col-sm-5 col-xs-12 date-input valid">
                    <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                    <select class="form-control select_vendor_multiple notranslate vendor_new_QIX vendor_new_vendor_name_QIX" onChange="supplierSelect(this.value,'QIX')" name="vendor_name_new[QIX]" style="  border-radius: 10px;border: 1px solid #4d90fe;">
                      <option value=""></option>
                      <option value="NEW_QUOTE_SUPPLIER" style="margin-left:20px;"><a data-toggle="modal" href="#myModalQuoteVendor" class="btn btn-primary"><b>+ Add New</b></a>
                        <?php foreach ($vendorReader as $value) { ?>
                      <option value="<?php echo $value['vendor_id']; ?>"><?php echo $value['vendor_name']; ?></option>
                    <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
                    <label class="control-label">Supplier Contact <span style="color: #a94442;">*</span></label><select class="form-control notranslate vendor_contact_new_QIX vendor_new_QIX vendor_contact_new_select" name="vendor_contact_new[QIX][]" multiple>
                    </select>
                  </div>
                  <input type="hidden" name="total_vendors_count" value="1" />
                  <div class="col-md-1 col-sm-1 col-xs-12">
                    <div class="remove-supplier" id="remove-supplier" title=" Remove Supplier" onclick="removeSupplier(QIX);">Remove Supplier</div>
                  </div>
                  <div class="clearfix"></div><br><br>
                </div>

              </div>
              <div class="col-md-2 col-sm-4 col-xs-12 text-right">
                <div class=" pull-right btn btn-success" id="add_supplier_contianer" title=" Add another supplier">Add Another Supplier</div>
              </div>

              <!-- Start: Add Another Supplier  -->
              <div class="col-md-10" style="padding-left: 0px;">
                <div id="more_supplier_container"></div>
              </div>
              <!-- END: Add Another Supplier  -->
            </div>

            <div class="clearfix"> <br /> </div>
            <?php $existing_items_found = false; ?>
          </div>
          <div style="float:right; margin:0 5px;" id="submitBtnCont">
            <div class="pull-right">
              <button type="button" class="btn btn-info" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
              <button type="button" class="btn btn-success next-page" id="nextBtn" onclick="nextPrev(1)">Next</button>
              <a href="javascript:void(0);" onclick="createQuoteAndValidation('choosen_supplier', 'publish')" class="sub_btns create_esourcing" <?php echo $disabled; ?>>
                <button type="button" <?php echo $disabled; ?> class="btn btn-success btn-functionality submit-btn1 create_esourcing" id="create_esourcing" <?php echo $disabled; ?>>
                  Publish To Suppliers
                </button>
              </a>
              <a href="javascript:void(0);" onclick="createQuoteAndValidation('choosen_supplier','')" class="sub_btns save_as_draft" <?php echo $disabled; ?>>
                <button type="button" <?php echo $disabled; ?> class="btn btn-success btn-functionality submit-btn1 save_as_draft" id="save_as_draft" <?php echo $disabled; ?>>
                  Save as Draft
                </button>
              </a>
            </div>
            <div class="clearfix"></div>
          </div>
      </div>
      <!-- Circles which indicates the steps of the form: -->
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="addtional_btns" tabindex="-1" role="dialog" style="z-index: 999">
  <div class="modal-dialog" role="document" style="position: relative;  top: 42%; transform: translateY(-50%);">
    <div class="modal-content" style="max-width: 538px; margin: auto;">
      <div class="modal-body text-center">
        Are you happy for oboloo to share your contact details with an independent Procurement Consultant who can assist you with your sourcing activites?
        <br /> <br />
        Please note that any work rates are agreed directly between you and the consulting company outside of oboloo
        <br /> <br />
        <a href="javascript:void(0);" id="consultant" onclick="sendQuoteToIndependent('consultant')">
          <button type="button" class="btn btn-info btn-functionality"> Submit </button></a>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="new_document_code" style="display: none;">
  <div class="form-group" id="document_area_DOCIDX">
    <div class="col-md-8 col-sm-8 col-xs-8">
      <input class="form-control notranslate" type="file" name="quote_file_DOCIDX" id="quote_file_DOCIDX" />
    </div>
    <div class="col-md-4 col-sm-4 col-xs-4">
      <a onclick="deleteDocument(DOCIDX);" title="Click here to delete this document" style="cursor: pointer;">
        <span class="fa fa-minus fa-2x"></span>
      </a>
      <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
        <span class="fa fa-plus fa-2x"></span>
      </a>
    </div>
    <div class="clearfix"> </div>
    <div class="col-md-8 col-sm-8 col-xs-8">
      <label class="control-label">File/Document</label>
      <input class="form-control notranslate" type="text" name="file_desc_DOCIDX" id="file_desc_DOCIDX" placeholder="File/Document Description" />
    </div>
  </div>
  <input type="hidden" class="notranslate" name="delete_document_flag_DOCIDX" id="delete_document_flag_DOCIDX" value="0" />
  <div class="clearfix" id="end_document_area_DOCIDX"> <br /> </div>
</div>

<div id="new_vendor_code">
  <div class="form-group" id="vendor_area_VENDOR_IDX">
    <div class="col-md-8 col-sm-8 col-xs-8">
      <select required class="vendor_name_field form-control select_vendor_multiple notranslate" name="vendor_name_VENDOR_IDX" id="vendor_name_VENDOR_IDX" style="border-radius: 10px; border: 1px solid #4d90fe;">
      </select>
    </div>
    <div class="col-md-2 col-sm-2 col-xs-4">

      <div class="col-md-4 col-sm-4 col-xs-8">
        <a onclick="deleteVendor(VENDOR_IDX);" title="Click here to delete this supplier" style="cursor: pointer;">
          <span class="fa fa-minus fa-2x"></span>
        </a>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-8">
        <a onclick="addVendor();" title="Click here to invite more suppliers for this quote" style="cursor: pointer;">
          <span class="fa fa-plus fa-2x"></span>
        </a>
      </div>
    </div>
  </div>

  <div class="form-group" id="added_vendors_VENDOR_IDX" style="display: none;">
  </div>
  <input type="hidden" class="notranslate" name="delete_vendor_flag_VENDOR_IDX" id="delete_vendor_flag_VENDOR_IDX" value="0" />
</div>

<style type="text/css">
  canvas {
    display: block;
    max-width: 800px;
    margin: 60px auto;
  }
</style>
<?php echo $this->renderPartial("_create_quote_supplier");

$sql = "select user_id,full_name from users where admin_flag != 1 order by full_name asc ";
$quoteInternalUser = Yii::app()->db->createCommand($sql)->query()->readAll();
?>

<script type="text/javascript">
  $(document).ready(function() {
    $('.next-page').attr('disabled', false);
    $(document).on('keyup', '.score-criteria', function() {
      var totalEnteredScore = 0;

      $('.score-criteria').each(function(index, currentElement) {
        score = parseFloat($(this).val());
        if (!isNaN(score)) {
          totalEnteredScore = totalEnteredScore + score;
        }


      });

      $(".scoring_percentage_alert").html(totalEnteredScore + "/100%");
      if (totalEnteredScore > 100) {
        $('.next-page').attr('disabled', 'disabled')
        $('.next-page').attr('disabled', true);
      } else if (totalEnteredScore < 100) {
        $('.next-page').attr('disabled', 'disabled')
        $('.next-page').attr('disabled', true);
      } else {
        $('.next-page').attr('disabled', '')
        $('.next-page').attr('disabled', false);
      }
    });


    $('.create_esourcing').on('click', function() {
      $('#quote_status').val('Pending');
    });

    $('.save_as_draft').on('click', function() {
      $('#quote_status').val('Draft');
    });

    $('[data-toggle="tooltip"]').tooltip();
    $("#import_hide").hide();
    $("#supporting_document_hide").hide();
    $("#product_import_area").hide();
    $("#supporting_document_area").hide();
    $("#scoring_criteria_hide").hide();
    $("#default_scoring_container").hide();

    $("#import_showhide").click(function() {
      $("#product_import_area").show();
      $("#import_showhide").hide();
      $("#import_hide").show();
    });

    $("#import_hide").click(function() {
      $("#product_import_area").hide();
      $("#import_showhide").show();
      $("#import_hide").hide();
    });

    $("#supporting_document_showhide").click(function() {
      $("#supporting_document_area").show();
      $("#supporting_document_showhide").hide();
      $("#supporting_document_hide").show();
    });

    $("#supporting_document_hide").click(function() {
      $("#supporting_document_area").hide();
      $("#supporting_document_showhide").show();
      $("#supporting_document_hide").hide();
    });

    dropdownVendors("vendor_name_1");
    $('#opening_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?> HH:mm:ss',
    });
    $('#closing_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?> HH:mm:ss'
    });


    $('#addtional_btns').on('hidden.bs.modal', function() {
      $('#create_quote').modal('show');
    });
    $("#myModalQuoteVendor").hide('modal');
    $(".select_vendor_multiple").select2({
      placeholder: "Select Supplier",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    }).on('select2:close', function() {
      var el = $(this);
      if (el.val() === "NEW_QUOTE_SUPPLIER") {
        $(".tab_w_auto_new_modal").css("height", '550px');
        $("#myModalQuoteVendor").show('modal');
      }
    });

    $(".vendor_contact_new_select").select2({
      placeholder: "  Select Contacts",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    })

    $(".heading_custom_score_cont").hide();
    $("#number_custom_score").click(function() {
      var numLoading = 1;
      $.ajax({
        type: "POST",
        data: {
          row_number: numLoading
        },
        dataType: "html",
        url: BICES.Options.baseurl + '/quotes/customSocreAndQuestion',
        success: function(options) {
          $("#number_custom_score_cont").append(options);
          $(".heading_custom_score_cont").show();
        },
      });
    });

    $('#new_vendor_code').hide();
    $('#manager_name').devbridgeAutocomplete({
      serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
      onSelect: function(suggestion) {
        $('#manager_user_id').val(suggestion.data);
      },
      onSearchComplete: function(query, suggestions) {
        if (!suggestions.length) $('#manager_user_id').val(0);
      }
    });

    /* $('#procurement_lead_name').devbridgeAutocomplete({
           serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
           onSelect: function(suggestion) { $('#procurement_lead_user_id').val(suggestion.data); },
           onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#procurement_lead_user_id').val(0); }
       });*/

    $('#commercial_lead_name').devbridgeAutocomplete({
      serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
      onSelect: function(suggestion) {
        $('#commercial_lead_user_id').val(suggestion.data);
        $('#procurement_lead_name').val(suggestion.position);
        $('#contact_email').val(suggestion.contact_email);
        $('#contact_phone').val(suggestion.contact_phone);
      },
      onSearchComplete: function(query, suggestions) {
        if (!suggestions.length) $('#commercial_lead_user_id').val(0);
      }
    });

    $('#quote_form').on('submit', function() {
      $('#quote_status').removeAttr('disabled');
    });

    $("#quote_form").validate({
      rules: {
        quote_name: "required",
        //quote_desc: "required",
        location_id: "required",
        department_id_new: "required",
        //vendor_name_1: "required"
      },
      messages: {
        quote_name: "Quote name is required",
        location_id: "Location is required",
        //quote_desc: "Description/Notes is required",
        department_id_new: "Department is required",
        //vendor_name_1: "Supplier Name is required"
      },
      submitHandler: function(form) {
        var product_id = 0;
        $('.product_quantity').each(function() {
          if ($.trim($(this).val()) == "") {
            product_id = $(this).closest('.form-group').find('.product_id').val();
            if (product_id == '0') $(this).val('0');
            else $(this).val('1');
          }
        });
        form.submit();
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("help-block");
        element.parents(".col-sm-6").addClass("has-feedback");

        if (element.prop("type") === "checkbox")
          error.insertAfter(element.parent("label"));
        else error.insertAfter(element);

        if (!element.next("span")[0])
          $("<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>").insertAfter(element);
      },
      success: function(label, element) {
        if (!$(element).next("span")[0])
          $("<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>").insertAfter($(element));
      },
      highlight: function(element, errorClass, validClass) {
        $(element).parents(".valid").addClass("has-error").removeClass("has-success");
        //$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parents(".valid").addClass("has-success").removeClass("has-error");
        //$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
      }
    });


    $('body').on('click', "input[name^='vendor_name_']", function() {
      //createVendorAutocompleteSearch();
    });
    <?php //echo AppUrl::bicesUrl('orders/getVendors'); if you uncomment the placed this line in line number 1364 under url 
    ?>

    <?php if (isset($quote_vendors) && is_array($quote_vendors) && count($quote_vendors)) { ?>
      <?php for ($vendor_idx = 1; $vendor_idx <= count($quote_vendors); $vendor_idx++) { ?>
        populateVendorDetails(<?php echo $vendor_idx; ?>);
      <?php } ?>
    <?php } ?>
  });

  function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 &&
      (charCode < 48 || charCode > 57))
      return false;

    return true;
  }

  function populateVendorDetails(vendor_idx_rownum, vendor_idx) {
    $.ajax({
      type: "POST",
      data: {
        vendor_ids: vendor_idx
      },
      dataType: 'json',
      url: "<?php echo AppUrl::bicesUrl('quotes/getVendorDetails/'); ?>",
      error: function() {
        $('#added_vendors_' + vendor_idx_rownum).html('<div class="clearfix"> <br /> </div>Cannot load selected supplier details ... please try again !!!')
      },
      success: function(data) {
        var vendor_html = '<div class="clearfix"> <br /> </div>';
        var current_vendor_html = "";

        for (var i = 0; i < data.length; i++) {
          current_vendor_html = "";
          current_vendor_html += '<div class="form-group">';
          current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Supplier Name </label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_name_' + data[i].vendor_id + '" value="' + data[i].vendor_name + '" placeholder="Supplier Name">';
          current_vendor_html += '</div>';
          current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Contact Person</label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_contact_' + data[i].vendor_id + '" value="' + data[i].contact_name + '" placeholder="Contact Person">';
          current_vendor_html += '</div>';
          current_vendor_html += '</div>';
          current_vendor_html += '<div class="form-group">';
          current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Address Line 1</label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_address_1_' + data[i].vendor_id + '" value="' + data[i].address_1 + '" placeholder="Address Line 1">';
          current_vendor_html += '</div>';
          current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Address Line 2</label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_address_2_' + data[i].vendor_id + '" value="' + data[i].address_2 + '" placeholder="Address Line 2">';
          current_vendor_html += '</div>';
          current_vendor_html += '</div>';
          current_vendor_html += '<div class="form-group">';
          current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">City</label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_city_' + data[i].vendor_id + '" value="' + data[i].city + '" placeholder="City">';
          current_vendor_html += '</div>';
          current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">State</label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_state_' + data[i].vendor_id + '" value="' + data[i].state + '" placeholder="State">';
          current_vendor_html += '</div>';
          current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">Zip Code</label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_zip_' + data[i].vendor_id + '" value="' + data[i].zip + '" placeholder="Zip Code">';
          current_vendor_html += '</div>';
          current_vendor_html += '</div>';
          current_vendor_html += '<div class="form-group">';
          current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Email Address</label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_email_' + data[i].vendor_id + '" value="' + data[i].emails + '" placeholder="Email Address">';
          current_vendor_html += '</div>';
          current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Phone Number</label>';
          current_vendor_html += '<input type="text" class="form-control" name="vendor_phone_' + data[i].vendor_id + '" value="' + data[i].phone_1 + '" placeholder="Phone Number">';
          current_vendor_html += '</div>';
          current_vendor_html += '</div>';
          current_vendor_html += '<input type="hidden" name="display_vendor_id_' + vendor_idx + '" id="display_vendor_id_' + vendor_idx_rownum + '" value="' + data[i].vendor_id + '">';

          vendor_html = vendor_html + current_vendor_html;
          vendor_html = vendor_html + '<div class="clearfix"><br /></div>';
        }

        $('#added_vendors_' + vendor_idx_rownum).html(vendor_html);
      }
    });
  }

  function createProductAutocompleteSearch() {
    var product_type = '';
    $("input[name^='product_name']").each(function() {
      product_type = $(this).closest('.form-group').find('.product_type').val();
      $(this).devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getProducts'); ?>',
        params: {
          product_type: 'Product'
        },
        onSelect: function(suggestion) {
          $(this).closest('.form-group').find('.product_id').val(suggestion.data.id);
          if ($.trim($(this).closest('.form-group').find('.price_of_product').val()) == '' ||
            $.trim($(this).closest('.form-group').find('.price_of_product').val()) == '0')
            $(this).closest('.form-group').find('.price_of_product').val(suggestion.data.price);
          if ($.trim($(this).closest('.form-group').find('.product_quantity').val()) == '')
            $(this).closest('.form-group').find('.product_quantity').val(1);
        }
      });
    });
  }

  function createVendorAutocompleteSearch() {
    var my_id = "";
    $("input[name^='vendor_name_']").each(function() {
      my_id = $(this).attr('id');
      if (my_id != 'vendor_name_VENDOR_IDX') {
        $(this).devbridgeAutocomplete({
          serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
          onSelect: function(suggestion) {
            $(this).closest('.form-group').find('.vendor_id').val(suggestion.data);
          }
        });
      }
    });
  }

  function selectDepartment(dpartmentID) {
    $('#department_id_new').prop('selectedIndex', dpartmentID);
  }

  function loadDepartments(location_id) {
    var location_id = location_id;
    var single = 1;


    $.ajax({
      type: "POST",
      data: {
        location_id: location_id,
        single: single
      },
      dataType: "json",
      url: BICES.Options.baseurl + '/locations/getDepartments',
      success: function(options) {
        var options_html = '<option value="">Select Department</option>';
        for (var i = 0; i < options.length; i++)
          options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
        $('#department_id_new').html(options_html);
        selectDepartment('<?php echo $userDepartment; ?>');
      },
      error: function() {
        $('#department_id_new').html('<option value="">Select Department</option>');
      }
    });
  }

  function loadDepartmentsForSingleLocation(sel) {
    var location_id = sel.value;
    var single = 1;
    loadDepartments(location_id);
  }

  function loadSubcategories(input_subcategory_id) {
    var category_id = $('#modal_category_id option:selected').val();

    if (category_id == 0)
      $('#subcategory_id_new').html('<option value="0">Select Sub Category</option>');
    else {
      $.ajax({
        type: "POST",
        data: {
          category_id: category_id
        },
        dataType: "json",
        url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
        success: function(options) {
          var options_html = '<option value="0">Select  Sub Category</option>';
          for (var i = 0; i < options.suggestions.length; i++)
            options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
          $('#subcategory_id_new').html(options_html);
          if (input_subcategory_id != 0) $('#subcategory_id_new').val(input_subcategory_id);
        },
        error: function() {
          $('#subcategory_id_new').html('<option value="0">Select Sub Category</option>');
        }
      });
    }
  }

  function deleteQuoteFile(file_idx, file_name) {
    $('#delete_quote_file_link_' + file_idx).confirmation({
      title: "Are you sure you want to delete the attached file?",
      singleton: true,
      placement: 'right',
      popout: true,
      onConfirm: function() {
        $('#existing_file_id_' + file_idx).remove();
        $.ajax({
          type: "POST",
          url: "<?php echo AppUrl::bicesUrl('quotes/deleteFile/'); ?>",
          data: {
            quote_id: $('#quote_id').val(),
            file_name: file_name
          }
        });
      },
      onCancel: function() {}
    });
  }

  function addDocument() {
    var total_documents = $('#total_documents').val();
    total_documents = parseInt(total_documents);
    total_documents = total_documents + 1;
    $('#total_documents').val(total_documents);

    var new_document_html = $('#new_document_code').html();
    new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
    $('#total_documents').before(new_document_html);
  }

  function deleteDocument(docidx) {
    var display_document_count = 0;
    $("div[id^='document_area']").each(function() {
      if ($(this).is(':visible')) display_document_count += 1;
    });

    if (display_document_count <= 1) alert("You must have at least one document field in the quote form");
    else {
      if (confirm("Are you sure you want to delete this document?")) {
        $('#delete_document_flag_' + docidx).val(1);
        $('#document_area_' + docidx).hide();
        $('#end_document_area_' + docidx).hide();
      }
    }
  }

  function dropdownVendors(obj) {
    var testingobj = obj;
    var options_html = "";
    $.ajax({
      type: "POST",
      data: {},
      dataType: "json",
      url: BICES.Options.baseurl + '/orders/getVendors',
      success: function(options) {

        var options_html = '<option value="">Select Supplier</option>';
        for (var i = 0; i < options.suggestions.length; i++)
          options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
        $("#" + testingobj).html(options_html);


      }

    });

  }

  function addVendor() {
    var vendor_count = $('#vendor_count').val();
    vendor_count = parseInt(vendor_count);
    vendor_count = vendor_count + 1;
    $('#vendor_count').val(vendor_count);

    var new_vendor_html = $('#new_vendor_code').html();
    new_vendor_html = new_vendor_html.replace(/VENDOR_IDX/g, vendor_count);
    $('#vendor_count').before(new_vendor_html);
    dropdownVendors("vendor_name_" + vendor_count);
  }
</script>
<?php if ($userLocation) { ?>
  <script type="text/javascript">
    loadDepartments("<?php echo $userLocation; ?>");
  </script>
<?php } ?>

<script type="text/javascript">
  function deleteVendor(vendor_idx) {
    var display_vendor_count = 0;
    $("div[id^='vendor_area']").each(function() {
      if ($(this).is(':visible')) display_vendor_count += 1;
    });

    if (display_vendor_count <= 1) alert("You must have at least one vendor field in the quote form");
    else {
      if (confirm("Are you sure you want to delete this vendor?")) {
        $('#delete_vendor_flag_' + vendor_idx).val(1);
        $('#vendor_area_' + vendor_idx).hide();
        $('#added_vendors_' + vendor_idx).hide();
      }
    }
  }

  function displayVendorDetails(vendor_idx_rownum, obj) {

    var vendor_idx = $('#vendor_name_' + vendor_idx_rownum).val();

    if ($('#added_vendors_' + vendor_idx).is(':visible')) $('#added_vendors_' + vendor_idx).hide();
    else {
      var display_vendor_id = $('#display_vendor_id_' + vendor_idx).val();
      var my_vendor_id = $('#vendor_id_' + vendor_idx).val();
      if (true) {
        $('#added_vendors_' + vendor_idx).html('');
        populateVendorDetails(vendor_idx_rownum, vendor_idx);
      }
      $('#added_vendors_' + vendor_idx_rownum).show();
    }
  }


  var currentTab = 0; // Current tab is set to be the first tab (0)
  showTab(currentTab); // Display the current tab
  $(".sub_btns").hide();

  function showTab(n) {
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
      document.getElementById("prevBtn").style.display = "none";
      $("#total_price_cont").hide();
      $(".sub_btns2").show();
    } else {
      document.getElementById("prevBtn").style.display = "inline";
      $("#total_price_cont").show();
      $(".sub_btns2").hide();
    }
    if (n == (x.length - 1)) {
      document.getElementById("nextBtn").innerHTML = "Submit";
      $("#nextBtn").hide();
      $(".sub_btns").show();
    } else {
      $("#nextBtn").show();
      $(".sub_btns").hide();
      document.getElementById("nextBtn").innerHTML = "Next";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
  }

  function stakeholdersPopup() {
    $.confirm({
      title: false,
      content: '<spam style="font-size:11px">Would you like to add internal stakeholders to contribute to this sourcing activity before it is published to suppliers?</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            contributorsPopup();
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {
            // $("#nextBtn").trigger('click');
          }
        },

      }
    });
  }

  function questionValidation() {
    var questionScorers = $("input[name='question_scorer[]']");
    var isValid = true;
    questionScorers.each(function() {
      var value = $(this).val().trim();
      if (value === "") {
        $(this).addClass('invalid');
        isValid = false;
      } else {
        $(this).removeClass('invalid');
      }
    });
    return isValid;
  }

  function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && currentTab == 1 && !questionValidation()) return false;
    else if (n == 1 && currentTab == 1 && questionValidation()) stakeholdersPopup();

    if (n == 1 && !validateForm()) return false;
    if (currentTab == 0) {
      $.confirm({
        title: false,
        content: '<spam style="font-size:11px">Would you like to add questionnaires or scoring criteria?</span>',
        buttons: {

          Yes: {
            text: "Yes",
            btnClass: 'btn-blue',
            action: function() {
              show_hide_score = 1;
            }
          },
          No: {
            text: "No",
            btnClass: 'btn-red',
            action: function() {
              $("#nextBtn").trigger('click');
              show_hide_score = 0;
            }
          },
        }
      });

    } else if (currentTab == 2) {
      if (show_hide_score == 0) {
        n = -1;
      }
    }

    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:

    currentTab = currentTab + n;

    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
      //...the form gets submitted:
      document.getElementById("order_form").submit();
      return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);

  }

  var requiredArr = ['location_id', 'department_id_new', 'currency_id', 'quote_name', 'opening_date', 'closing_date', 'product_name_1', 'quantity_1', 'quote_currency', /*'estimated_value',*/ 'vendor_name_1', 'quote_desc'];


  function requiredArrCheck(field_id) {
    return age >= field_id;
  }

  function validateForm() {

    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].querySelectorAll("select,input,textarea");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
      // If a field is empty... 

      if (y[i].id == "vendor_name_1") {
        //quick_vendor_field
        var vandor_name_valid = $('#vendor_name_1').val();
        var vandor_name_quick = $('#new_vendor_name').val();
        if ((vandor_name_valid == null || vandor_name_valid == '') && (vandor_name_quick == null || vandor_name_quick == '')) {
          $('.select2-selection').addClass('invalid');
          valid = false;
        } else {
          $('.select2-selection').removeClass('invalid');
        }
      }
      if ((y[i].value == "" || y[i].value == 0) && y[i].id != "vendor_name_1" && (requiredArr.includes(y[i].id) || requiredArr.includes(y[i].class))) {
        // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false:
        valid = false;
      } else {
        $('#' + y[i].id).removeClass('invalid');
      }

    }

    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
      document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
  }

  function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
  }

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  function validate(email) {
    //alert(email);
    var result = $("#emails_error");
    result.text("");

    if (validateEmail(email)) {
      return true;
    } else {
      return false;
    }

  }

  $(".scoring_alert").hide();
  $(".scoring_alert2").hide();


  function createQuoteAndValidation(btntype = '', publish = '') {
    $("#quote_send_to").val(btntype);
    var error_supplier = 0;



    //  var vendorEmailNameValid = vendorContactNameValid = vendorNameValid = false;
    $('select[class*="vendor_new_vendor_name_"]').each(function(i, obj) {
      vendorName = $(this).val();

      if (vendorName != '' || vendorName != null || typeof vendorName !== "undefined") {
        $(this).removeClass('invalid');
        error_supplier = 0;
      }

      if (vendorName == '' || vendorName == null || typeof vendorName === "undefined") {
        $(this).next('.select2-container').find('.select2-selection--single').addClass('invalid');
        error_supplier = 1;
      }

    });

    $('select[class*="vendor_contact_new_"]').each(function(i, obj) {
      vendorContactName = $(this).val();

      if (vendorContactName != '' || vendorContactName != null || typeof vendorContactName !== "undefined") {
        $(this).removeClass('invalid');
        error_supplier = 0;
      }

      if (vendorContactName == '' || vendorContactName == null || typeof vendorContactName === "undefined") {
        $(this).addClass('invalid');
        error_supplier = 1;
      }
    });

    $('select[class*="vendor_email_new_"]').each(function(i, obj) {
      vendorEmailName = $(this).val();

      if (vendorEmailName != '' || vendorEmailName != null || typeof vendorEmailName !== "undefined") {
        $(this).removeClass('invalid');
        error_supplier = 0;
      }

      if (vendorEmailName == '' || vendorEmailName == null || typeof vendorEmailName === "undefined") {
        $(this).addClass('invalid');
        error_supplier = 1;
      }

    });


    if (error_supplier == 0 && publish != '' && publish == 'publish') {
      publishToSuppliers();
    } else {
      $('#quote_form').submit();
      $(".btn-functionality").attr('disabled', 'disabled');
      $(".btn-functionality").prop('disabled', true);
    }

    //(this).prop('disabled',true);
  }

  function sendQuoteToIndependent(btntype = '') {
    $(".btn-functionality").attr('disabled', 'disabled');
    $("#quote_send_to").val(btntype);
    $.ajax({
      type: "POST",
      data: {},
      url: BICES.Options.baseurl + '/quotes/sendToindependent',
      success: function(options) {
        location.reload();
      }
    });

    return false;

  }

  function importProduct(uploadBtnIDX, e) {
    e.preventDefault();
    var uploadedObj = $('#product_import');

    $(document).ready(function() {
      $('#product_import').bind('change', function() {
        if (this.files[0].size > 2097152) {
          alert("Maximum File Size of 2MB");
          this.value = "";
        }
      });
    });

    var file_data = $('#product_import').prop('files')[0];
    fileName = uploadedObj[0].files[0]['name']
    if (fileName.split('.').pop() != "csv") {
      $("#product_import_error").html("Only csv file is allowed");
      return false;
    } else {
      $("#product_import_error").html("");
    }

    var form_data = new FormData();
    form_data.append('file', file_data);
    $.ajax({
      url: BICES.Options.baseurl + '/quotes/productImport', // point to server-side PHP script 
      dataType: 'text', // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(uploaded_response) {
        $("#product_list_cont").html(uploaded_response);
        uploadedObj.val(null);
        $("#product_import_area").hide();
        $("#import_showhide").show();
        $("#import_hide").hide();

      }
    });

  }

  $("#file_upload_alert").hide();
  $(".processing_oboloo").hide();

  function uploadDocument(uploadBtnIDX, e) {

    e.preventDefault();
    var uploadedObj = $('#order_file_' + uploadBtnIDX);
    var uploadedObjDesc = $('#file_desc_' + uploadBtnIDX);


    var file_data = $('#order_file_' + uploadBtnIDX).prop('files')[0];
    var field_data = $('#file_desc_' + uploadBtnIDX).val();


    if (typeof file_data !== 'undefined') {

      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('file_description', field_data);
      $.ajax({
        url: BICES.Options.baseurl + '/quotes/uploadDocumentModal', // point to server-side PHP script 
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        beforeSend: function() {
          $(".processing_oboloo").show();
          $("#file_upload_alert").html('');
          $("#file_upload_alert").hide();
          $(".btn-functionality").attr('disabled', 'disabled');
          $(".btn-functionality").prop('disabled', true);
          $("#btn_upload_1").prop('disabled', true);
        },
        success: function(uploaded_response) {

          if (uploaded_response.uploaded == 1) {
            $("#file_upload_alert").show();
            $("#file_upload_alert").html('File uploaded successfully');
          } else {

            $("#file_upload_alert").html('Problem occured while uploading file, try again');
            $("#file_upload_alert").show();
          }

          $(".processing_oboloo").hide();
          $(".btn-functionality").prop('disabled', false);
          $("#btn_upload_1").prop('disabled', false);

          $("#document_list_cont").html(uploaded_response.uploaded_list);

          uploadedObj.val(null);
          uploadedObjDesc.val(null);
        }
      });
    } else {
      $("#file_upload_alert").show();
      $("#file_upload_alert").html('Document is required');
    }

    $("#file_upload_alert").delay(1000 * 5).fadeOut();
  }

  function deleteDocumentModal(documentKey) {
    var documentKey = documentKey;
    $("#file_upload_alert").hide();

    $.confirm({
      title: false,
      content: '<spam style="font-size:11px">Are you sure want to delete this file/document</span>',
      buttons: {

        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              dataType: 'json',
              type: "post",
              url: "<?php echo AppUrl::bicesUrl('quotes/deleteDocumentModal'); ?>",
              data: {
                documentKey: documentKey
              },
              success: function(uploaded_response) {
                if (uploaded_response.uploaded == 1) {
                  $("#file_upload_alert").show();
                  $("#file_upload_alert").html('File deleted successfully');
                } else {
                  $("#file_upload_alert").show();
                  $("#file_upload_alert").html('Problem occured while deleting file, try again');
                }
                $("#document_list_cont").html(uploaded_response.uploaded_list);
              }
            });
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });

    $("#file_upload_alert").delay(1000 * 5).fadeOut();
  }

  $(document).ready(function() {

    $('#order_file_1').bind('change', function() {
      if (this.files[0].size > 1024 * 1024 * 25) {
        alert("Maximum File Size of 25MB");
        this.value = "";
      }
    });
  });

  function removeSupplier(idnumber) {
    $.confirm({
      title: false,
      content: '<spam style="font-size:11px">Do you want to remove this record?</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $(".vendor_new_" + idnumber).remove();
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {}
        },
      }
    });

  }
  $(document).ready(function() {
    $('#quote_file_1').bind('change', function() {
      if (this.files[0].size > 2097152) {
        alert("Maximum File Size of 2MB");
        this.value = "";
      }
    });

    $(".remove-supplier").hide();
    counter = 0;
    $("#add_supplier_contianer").click(function() {

      $(".select_vendor_multiple").select2("destroy");
      $(".vendor_contact_new_select").select2("destroy");
      var total_vendors = $('#total_vendors').val();
      total_vendors = parseInt(total_vendors);
      total_vendors = total_vendors + 1;
      $('#total_vendors').val(total_vendors);

      var supplierCon = $(".supplier_invite_container").last().html();
      supplierCon = supplierCon.replace(/QIX/g, total_vendors);

      $("#more_supplier_container").append(supplierCon);
      // Start: removed selected vendor
      $('.select_vendor_multiple option:selected').each(function() {
        if (parseInt($(this).attr('value')) > 0)
          $(".vendor_new_" + total_vendors + " option[value='" + $(this).attr('value') + "']").remove();
      });
      // End: removed selected vendor

      $("#more_supplier_container .remove-supplier").show();
      $(".select_vendor_multiple").select2({
        placeholder: "Select Supplier",
        allowClear: true,
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"
      });

      $(".vendor_contact_new_select").select2({
        placeholder: "Select Contacts",
        allowClear: true,
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"
      });
    });
  });

  function supplierSelect(vendor_id, serialNum) {
    var vendor_id = vendor_id;
    var serialNum = serialNum;

    if (serialNum == 'QIX') {
      var el = $(".vendor_new_vendor_name_QIX");
      var el_for_later = ".vendor_new_vendor_name_QIX";
    } else {
      var el = $(".vendor_new_vendor_name_" + serialNum);
      var el_for_later = ".vendor_new_vendor_name_" + serialNum;
    }

    if (el.val() === "NEW_QUOTE_SUPPLIER") {
      $(".tab_w_auto_new_modal").css("height", '550px');
      $("#myModalQuoteVendor").show('modal');
      $("#vendor_class_number").val(el_for_later);
    } else {
      $.ajax({
        dataType: 'json',
        type: "post",
        url: "<?php echo AppUrl::bicesUrl('vendors/getVendorDetail'); ?>",
        data: {
          vendor_id: vendor_id
        },
        success: function(data_option) {
          if (serialNum == 'QIX') {
            $(".vendor_contact_new_QIX").html(data_option.contact_name);
          } else {
            $(".vendor_contact_new_" + serialNum).html(data_option.contact_name);
          }
        }
      });
    }
  }

  function contactSelect(contactName, serialNum) {
    if (serialNum == 'QIX') {
      var vendor_id = $(".vendor_new_vendor_name_QIX ").val();
    } else {
      var vendor_id = $(".vendor_new_vendor_name_" + serialNum).val();
    }
    var serialNum = serialNum;
    var contactName = contactName;
    $.ajax({
      dataType: 'json',
      type: "post",
      url: "<?php echo AppUrl::bicesUrl('vendors/getVendorContactEmail'); ?>",
      data: {
        vendor_id: vendor_id,
        contact_name: contactName
      },
      success: function(data_option) {
        if (serialNum == 'QIX') {
          $(".vendor_email_new_QIX").html(data_option.emails);
        } else {
          $(".vendor_email_new_" + serialNum).html(data_option.emails);
        }
      }
    });
  }

  // START: show questionire dropdown
  $('.questionaire_section').hide();
  $('.remove_questionaire_section').hide();

  $('.add_questionaire_section').click(function() {
    $('.questionaire_section').show();
    $('.add_questionaire_section').hide();
    $('.remove_questionaire_section').show();
  });

  $('.remove_questionaire_section').click(function() {
    $('.questionaire_section').hide();
    $('.add_questionaire_section').show();
    $('.remove_questionaire_section').hide();
  });

  $("#questionaire_sortable").sortable({
    placeholder: "ui-state-highlight"
  });
  $("#questionaire_sortable").disableSelection();
  $('.questionaire_list').hide();
  var questionaireArr = [];

  $('#quote_questionaire').change(function() {
    var optionText = $(this).find("option:selected").text();
    var optionVal = $(this).find("option:selected").val();
    if (optionVal != '' && optionVal != 0) {
      $('.questionaire_list').show();

      var newItem = $(`<div class="removeItem">
                      <li class="ui-state-default select_form_list notranslate "> 
                        <div class="row">
                          <div class="col-md-7" style="padding: 8px 20px">${optionText}</div>
                          <div class="col-md-2 valid"><input type="number" class="form-control score-criteria text-center" name="question_scorer[]" onkeypress="return isNumberKey(event)" onchange="maxScoring(this.value)" required /></div>
                          <div class="col-md-3" style="padding: 8px 20px"> <span class="btn-link pull-right removeThisRow_${optionVal}" id="removeThisRow" onclick="removeThisRow(${optionVal})" style="cursor: pointer;">Remove</span></div>
                        </div>
                        <input type="hidden" name="question_form_id[]" id="remove_questionire_array" value="${optionVal}" />
                        <input type="hidden" name="question_form_name[]" value="${optionText}" /></li>
                    </div>`);

      var items = $('.add_new_questionire .removeItem').toArray();
      items.push(newItem);
      items.sort(function(a, b) {
        var textA = $(a).find('.col-md-7').text();
        var textB = $(b).find('.col-md-7').text();
        return textA.localeCompare(textB);
      });

      $('.add_new_questionire').empty().append(items);
      questionaireArr.push(optionVal);
      $('#quote_questionaire').val("");
    }
    questionaire();
  });

  questionaire();

  function questionaire() {
    $.ajax({
      type: "POST",
      data: {
        questionaire: questionaireArr
      },
      dataType: "html",
      url: BICES.Options.baseurl + '/quotes/questionaireDropdown',
      success: function(options) {
        var options = JSON.parse(options);
        $("#quote_questionaire").html(options.option);
      },
    });
  }

  function removeThisRow(optionID) {
    // when remove questionaire from list also remove this questionaire ID from array questionaireArr
    var optionID = String(optionID);
    var index = questionaireArr.indexOf(optionID);
    if (index !== -1) {
      questionaireArr.splice(index, 1);
      questionaire();
    }

    $('.next-page').attr('disabled', false);
    $(".scoring_percentage_alert").html('');
    $('.removeThisRow_' + optionID).parent().parent().parent().parent().remove();
    $('.score-criteria').trigger('keyup');
  }

  // END  : show questionire dropdown 
  function publishToSuppliers() {
    $.confirm({
      title: false,
      content: `<spam style="font-size:11px;">Are you sure you want to publish this sourcing activity to suppliers? <br/><br/> You will not be able to edit this activity again unless you reopen it to suppliers at a later date.<br/><br/>
        Any internal contributors that you have selected to provide additional information to this sourcing activity will be unable to make any further changes.</span>`,
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $('#quote_form').submit();
            $(".btn-functionality").attr('disabled', 'disabled');
            $(".btn-functionality").prop('disabled', true);
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {}
        },
      }
    });
  }

  $(".scoring_alert").hide();
  $(".scoring_alert2").hide();

  function maxScoring(currentFieldValue = '') {
    var totalScore = 0;
    $('.scoring_alert').hide();
    $('.score-criteria').each(function(index, currentElement) {
      totalScore = totalScore + parseFloat($(this).val());

      if (totalScore > 100) {
        $(this).val('');
        $('.scoring_alert').show();
        $('.scoring_alert').html("By entering this value " + currentFieldValue + ", the scoring criteria exceeds 100. Please try another value.");
        $(".next-page").attr('disabled', 'disabled');
        $(".next-page").prop('disabled', true);
        return false;
      }
    });

    if (currentFieldValue == '') {
      totalScore = 0;
      $('.score-criteria').each(function(index, currentElement) {
        totalScore = totalScore + parseFloat($(this).val());
      });

      if (totalScore < 100) {
        $('.scoring_alert').show();
        $('.scoring_alert').html("Scoring Criteria must add up to 100%");
        $(".next-page").attr('disabled', 'disabled');
        return false;
      }

      if (totalScore == 100) {
        $(".next-page").attr('disabled', '');
        $(".next-page").prop('disabled', false);
      }
    }
    return true;
  }
</script>
<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/modules/quotes/quote_user.js'); ?>"></script>

<style type="text/css">
  .tab { display: none;}
  .invalid { background-color: #ffdddd !important; }
  #questionaire_sortable { list-style-type: none; margin: 0;  padding: 0; font-weight: 700; }
  #questionaire_sortable li {
    margin: 0 5px 5px 5px;
    padding: 5px; font-size: 1.2em;
    height: 1.5em; background: #fff;
  }

  html>body #questionaire_sortable li {  height: 3.7em; line-height: 1.5em;}
  .ui-state-highlight { height: 3.7em; line-height: 1.5em;  }
  .disabled { cursor: not-allowed; opacity: 0.5; pointer-events: none; }
  .not-allowed-element { cursor: not-allowed; }
</style>