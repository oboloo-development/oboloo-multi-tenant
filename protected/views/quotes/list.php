<?php $quote_statuses = array('Pending', 'Submitted', 'Closed', 'Cancelled'); 
 $quteLocName = $quteCount = '';
  foreach ($location_quote_count as $value) { 
    $quteLocName .= '"'.$value['location_name'].'",';
    $quteCount   .= intval($value['total_quotes']).',';
  }
 $quteLocName = rtrim($quteLocName,",");
 $quteCount = rtrim($quteCount,","); 
?>
<div class="right_col" role="main">
    <div class="row-fluid tile_count"> <div class="clearfix"> </div><br />
    <?php $quoteCreated=Yii::app()->user->getFlash('success');
      if(!empty($quoteCreated)) { ?>
      <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $quoteCreated;?></div>
    <?php } ?>
    <div class="clearfix"></div>
      <div class="span6 col-md-10 ">
        <h3>eSourcing</h3>
      </div>
      <div class="col-md-2 pull-right text-right hidden-xs" >
        <button type="`button" class="btn btn-warning click-modal-poup  quote-tutorial " onclick="addQuoteModal(event);">
          <span c lass="glyphicon glyphicon-plus" aria-hidden="true"></span>Create eSourcing Activity
        </button>
      </div>
    <div class="clearfix"> </div>
    <div class="clearfix"></div>

    <div class="row-fluid tile_count">
      <div class="container">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats1 in_draft_metx mb-0">
          <h4  class="text-center contract-metrice"><?= $inDraftMetric; ?> <br>In Draft</h4>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats2 in_progress_metx mb-0">
          <h4  class="text-center contract-metrice"><?= $inProgressMetric; ?> <br>In Progress</h4>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats3 completed_metx mb-0">
          <h4  class="text-center contract-metrice"><?= $completedMetric; ?><br> 
            Completed</h4>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats4 awarded_metx mb-0" >
          <h4  class="text-center contract-metrice"><?= $awardedMetric; ?> <br>Awarded</h4>
        </div>
      </div>
    </div>
    <div class="clearfix"> </div><br />
   
    <div class="col-md-6 col-sm-6 col-xs-12 tile_count pl-0" style="margin-top: 0px !important; ">
     <div class="x_panel notification_quotes" id="notification_scroll" style="height: 658px !important;">
      <div class="x_title">
        <h2>Recent eSourcing Notifications </h2>
        <h2 class="pull-right"><a href="<?php echo AppUrl::bicesUrl('notifications/list'); ?>" class="btn btn-success expiring-contract-btn">All Notifications</a></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="dashboard-widget-content">
          <ul class="list-unstyled timeline widget">
          <?php foreach ($notifications as $notification) { ?>
            <li>
              <div class="block">
                <div class="block_content">
                  <h2 class="title">
                     <div class="row">
                     <div class="col-md-2">
                      <?php 
                      //if(strtolower($notification['notification_type']) ==  'quote'){
                      $btnStyle="background-color: #a0e7a0;border-color: #a0e7a0;font-size:8px;";
                      $btnText = "eSourcing";
                      ?>
                      <button class="btn btn-success" style="<?php echo $btnStyle;?>"><?php echo $btnText; ?></button>
                      </div>
                      <div class="col-md-10">
                      <?php  
                      // echo "<pre>"; print_r($notification); exit; 
                      $str = $notification['notification_text'];
                      $wordsNumber = 1000;
                      echo FunctionManager::getNWordsFromString($str,$wordsNumber); ?>
                      <div class="byline">
                      <span><?php echo Notification::getTimeAgo($notification['notification_date']); ?></span>
                      </div>
                      </div>
                      </div>
                     <!-- End :row -->
                    </h2>
                </div>
              </div>
            </li>
          <?php } ?>
          </ul></div></div></div></div>
  <div class="col-md-6 col-sm-6 col-xs-12 quote-graphs pr-0">
  <div class="col-md-12 col-sm-12 col-xs-12 p-0">
  <div class="x_panel tile ">
    <div class="x_title">
      <h2>eSourcing Activities By Location</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content"><div id="full_scoring_by_location_count"></div></div>
  </div></div>
  <div class="col-md-12 col-sm-12 col-xs-12 p-0">
    <div class="x_panel tile ">
      <div class="x_title">
        <h2>Cost Savings by Year - (Baseline Currency)</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content"><div id="cost_saving_by_year_baseline"></div><br /></div>
    </div></div>
  </div>
    </div>
    <div class="clearfix"></div><br />
	<form class="form-horizontal" id="quote_list_form" name="quote_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('quotes/list'); ?>">
    <div class="form-group">
          <div class="col-md-2 col-sm-2 col-xs-6 " style="min-width: 120px;">
              <input type="text" class="form-control border-select" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date(FunctionManager::dateFormat(), strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> autocomplete="off">
          </div>
      
          <div class="col-md-2 col-sm-2 col-xs-6 " style="min-width: 120px;">
            <input type="text" class="form-control border-select" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date(FunctionManager::dateFormat(), strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> autocomplete="off"  >
          </div>

      <div class="col-md-2 col-sm-2 col-xs-6  location">
        <select name="location_id[]" id="location_id"  class="form-control " onchange="loadNewDepartments(0);">
          <option value="">All Locations</option>
          <?php
          $i = 0 ;
          foreach ($locations as $location) {
            if(isset($location_id[$i])){
              ?>
              <option value="<?php echo $location['location_id']; ?>"
                <?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
                <?php echo $location['location_name']; ?>
              </option>
            <?php } else { ?>
              <option value="<?php echo $location['location_id']; ?>">
                <?php echo $location['location_name']; ?>
              </option>
            <?php } ?>
            <?php
            $i++;
          } ?>
        </select>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-6">
        <select name="department_id[]" id="department_id" class="form-control " >
          <option value="">All Departments</option>
          <?php if(!empty($department_info))
            foreach($department_info as $dept_value){?>
            <option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
          <?php } ?>
        </select>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-6 ">
      <div class="form-group">
        <select name="category_id[]" id="category_id" class="form-control border-select "  onchange="loadSubcategories(0);">
          <option value="">All Categories</option>
          <?php foreach ($categories as $category) { ?>
            <option value="<?php echo $category['id']; ?>"
                <?php if (!empty($category_id) && in_array($category['id'],$category_id)) echo ' selected="SELECTED" '; ?>>
              <?php echo $category['value']; ?>
            </option>
          <?php } ?>
        </select>
      </div></div>
      <div class="col-md-2 col-sm-2 col-xs-6 ">
        <div class="form-group">
        <select name="subcategory_id[]" id="subcategory_id" class="form-control border-select ">
          <option value="">All Subcategories</option>
          <?php if(!empty($subcategory)) foreach ($subcategory as $sub) { ?>
            <option value="<?php echo $category['id']; ?>"
                <?php if (!empty($subcategory_id) && in_array($sub['id'],$subcategory_id)) echo ' selected="SELECTED" '; ?>>
              <?php echo $sub['value']; ?>
            </option>
          <?php } ?>
        </select></div>
      </div>
      <!-- <div class="col-md-2 status">
        <select name="quote_status[]" id="quote_status" multiple class="form-control">
          <?php
          $i = 0 ;
          foreach ($quote_statuses as $quote_status_display) {
            if(isset($quote_status[$i])){?>
              <option value="<?php echo $quote_status_display; ?>"
                <?php if ($quote_status[$i] == $quote_status_display) echo ' selected="SELECTED" '; ?>>
                <?php echo $quote_status_display; ?>
              </option>
            <?php } else { ?>
              <option value="<?php echo $quote_status_display; ?>">
                <?php echo $quote_status_display; ?>
              </option>
            <?php }
            $i++;
          } ?>
        </select>
      </div> -->
      <div class="col-md-6  pull-right text-right search-quote">
         <button class="btn btn-info search-quote" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
        <button class="btn btn-primary search-quote" onclick="loadQuotes(); return false;">Apply Filters</button>
       
      </div>
      <div class="clearfix"> </div>
    </div>
  </form>
<div class="clearfix"> </div>
<div class="table-responsive" >
<table id="quote_table" class="table table-striped table-bordered quote-table " style="width: 100%">
      <thead>
        <tr>
          <th style="width:6%"> </th>
          <th style="width:5%">ID</th>
          <th style="width:12%">Title</th>
          <th style="width:26%">Category</th>
          <th style="width:15%" class="th-center">Opening Date (UTC)</th>
          <th style="width:15%" class="th-center">Closing Date (UTC)</th>
          <th style="width:10%" class="th-center">Created By</th>
          <!-- <th style="width:8%" class="th-center">Estimated Value</th> -->
          <th style="width:8%" class="th-center">Awarded <br> Supplier</th>
          <th style="width:5%" class="th-center">Currency</th>
          <th style="width:7%" class="th-center">Cost Savings</th>
          <!--<th style="width:8%">User Profile</th> -->
          <!-- <th style="width:8%">User</th> -->
          <th style="width:5%" class="th-center">Status</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
  </table>
</div>

</div>
<style>
.metrics{margin-top:-56px; margin-bottom: 24px;}
.location  .multiselect {width: 138%;  }
.status  .multiselect {width: 100%;}
.multiselect-selected-text{float:left; margin-left: 0px;}
.btn .caret {float: right; margin-top: 10px;}
.department .dropdown-toggle{ min-width: 112%; }
.table-user-img {margin-right: 5px; }
.th-center{text-wrap: nowrap; text-align: center !important;}
</style>

<!-- <link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script> -->
<script src="https://unpkg.com/@popperjs/core@^2.0.0"></script>
<script type="text/javascript">
 colors = ['#008ffb','#00e396', '#feb019', '#E91E63', '#FF9800','#5fed64','#59f7ea', '#5385f5', '#cbf257', '#f7ae77','#','#','#','#','#'];
$(document).ready(function(){
  $('.ui-datepicker').addClass('notranslate');
  select2function('select_department_multiple','All Departments');
  select2function('select_location_multiple','All Locations');
  select2function('select_category_multiple','All Categories');
  select2function('select_subcategory_multiple','All Sub Categories');
});

function select2function(className,lableTitle){
  var lableTitle = lableTitle;
  $("."+className).select2({
    placeholder: lableTitle,
    allowClear: true
  });
}

  <?php if (isset($category_id) && !empty($category_id)) { ?>
    <?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>
    <?php } else { ?>
    <?php } ?>
  <?php } ?>

function createLocationChart()
{  
  var quote_id = $("#quote_id").val();
  $.ajax({
    url: '<?php echo AppUrl::bicesUrl('quotes/listScoreDataChart'); ?>',
    type: 'POST', dataType: 'json',
    data: {},
    success: function(chart_data) {
    var score_values = chart_data.score_values;
    var score_labels = chart_data.score_labels;
    var pie_score_series = [];
    var pie_score_label  = chart_data.pie_score_label;
    for (i = 0; i < chart_data.pie_score_series.length; i++) {
          pie_score_series.push(parseInt(chart_data.pie_score_series[i]));
      }
    
      ApexCharts.exec('scoringPieChartlist', 'updateOptions', {
        labels:  pie_score_label,
      }, false, true);
      ApexCharts.exec('scoringPieChartlist', 'updateSeries', pie_score_series,true);

    // END: Pie Chart
       }
      });

  }
  createLocationChart();


  function loadSubcategories(subcategory_id)
  {
    var category_id = $('#category_id').val();
      $.ajax({
          type: "POST", data: { category_id: category_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
          success: function(options) { 
            var options_html = '<option value="">All Subcategories</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#subcategory_id').html(options_html);
            if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id); 
              $('#subcategory_id').trigger('change');
          },
          error: function() { $('#subcategory_id').html('<option value="">All Subcategories</option>'); }
      });
  }

function loadNewDepartments(department_id)
{
  var location_id = $('#location_id').val();
  
  if (location_id == 0)
    $('#department_id').html('<option value="">All Departments</option>');
  else
  {
      $.ajax({
          type: "POST", data: { location_id: location_id }, dataType: "json",
          url: BICES.Options.baseurl + '/locations/getDepartments',
          success: function(options) {
            var options_html = '<option value="0">All Departments</option>';
            for (var i=0; i<options.length; i++)
              options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
            $('#department_id').html(options_html);
            $("#department_id").trigger('change');
            if (department_id != 0) $('#department_id').val(department_id); 
          },
          error: function() { $('#department_id').html('<option value="">All Departments</option>'); }
      });
  }
}

  function getOptions(isFilter) {
    return {
      enableFiltering: isFilter,
      enableCaseInsensitiveFiltering: isFilter,
      filterPlaceholder: 'Search ...',
      nonSelectedText: 'All Locations',
      numberDisplayed: 1,
      maxHeight: 400,
    }
  }

  function getOptions1(isFilter) {
    return {
      enableFiltering: isFilter,
      enableCaseInsensitiveFiltering: isFilter,
      filterPlaceholder: 'Search ...',
      nonSelectedText: 'All Status Values',
      numberDisplayed: 1,
      maxHeight: 400,
    }
  }
  function getOptionsCategory(isFilter) {
    return {
      enableFiltering: isFilter,
      enableCaseInsensitiveFiltering: isFilter,
      filterPlaceholder: 'Search ...',
      nonSelectedText: 'All Categories',
      numberDisplayed: 1,
      maxHeight: 400,
    }
  }
  function getOptionsSubCategory(isFilter) {
    return {
      enableFiltering: isFilter,
      enableCaseInsensitiveFiltering: isFilter,
      filterPlaceholder: 'Search ...',
      nonSelectedText: 'All Subcategories',
      numberDisplayed: 1,
      maxHeight: 400,
    }
  }

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );

function loadQuotes() {
  $('#quote_table').DataTable().ajax.reload();
}

function clearFilter() {
  $('#from_date').val("");
  $('#to_date').val("");

  $("option:selected").removeAttr("selected");
  $('#location_id option[value=""]').attr('selected','selected');
  //$('#department_id option[value=""]').attr('selected','selected');
  $('#category_id option[value=""]').attr('selected','selected');
  //$('#subcategory_id option[value=""]').attr('selected','selected');

  $('#location_id').trigger("change");
  //$('#department_id').trigger("change");
  $('#category_id').trigger("change");
  //$('#subcategory_id').trigger("change");

  $('#quote_table').DataTable().ajax.reload();
}

$(document).ready(function(){

 $.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
  locale: {
      format: 'DD/MM/YYYY'
  }
  });
};

  $('#quote_table').dataTable({
    "columnDefs": [ {
        "targets": 0,
    } ],

    "createdRow": function ( row, data, index ) {
        if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 ){
          for (var i=1; i<=6; i++)
              $('td', row).eq(i).css('text-decoration', 'line-through');
        }

    },        
    "order": [[ 1,"desc" ]],
    "pageLength": 50,        
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?php echo AppUrl::bicesUrl('quotes/listAjax'); ?>",
      "type": "POST",
      data: function ( input_data ) {
        input_data.from_date = $('#from_date').val();
        input_data.to_date    = $('#to_date').val();
        input_data.location_id = $('#location_id').val();
        input_data.department_id = $('#department_id').val();
        input_data.category_id   = $('#category_id').val();
        input_data.subcategory_id = $('#subcategory_id').val();
      }    
    },
    "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      if(aData[1].search("<span class='deactivated_vendor'></span>")>0){
        $(nRow).addClass('deactivated_record');
        /*$(nRow).css("background-color", "#ccc");*/
      }
      for (var i=7; i<=10; i++){
        $('td:eq('+i+')', nRow).addClass('td-center');
      }
      for (var i=8; i==8; i++){
        $('td:eq('+i+')', nRow).addClass('table-currency');
      }
      }
});
 var userType = <?= json_encode(Yii::app()->session['user_type']) ?>;
 var baseUrl = <?= json_encode(Yii::app()->getBaseUrl(true)) ?>;
if (userType == 4 && baseUrl.indexOf("sandbox") === -1) {
    $('.dataTables_filter').append('<a href="<?= AppUrl::bicesUrl('quotes/exportCSV') ?>"><button class="btn btn-yellow pull-right" style="margin-left: 10px;">Export CSV</button></a>');
}

  $('#from_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'});

  $('#to_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'});

  $('#to_date').on('apply.daterangepicker', function(ev, picker) {
    $('#to_date').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS();?>')); 
  });

  $('#from_date').on('apply.daterangepicker', function(ev, picker) {
    $('#from_date').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS();?>')); 
  });

  <?php if (isset($location_id) && !empty($location_id)) { ?>

    <?php if (isset($department_id) && !empty($department_id)) { ?>
        //loadDepartments(<?php //echo $department_id; ?>);
    <?php } else { ?>
      loadNewDepartments(0);
    <?php } ?>

  <?php } ?>
    
});
function addQuoteModal(e){
  e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('quotes/createByModal'); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_quote_cont').html(mesg);
            $('#create_quote').modal('show');   
        }
    });
}

var options = {
      series: [{ name: "",
      data: [<?php echo  $quteCount;?>]
        }],
        chart: {
        fontFamily: 'Poppins !important',
        download: false, 
        toolbar: {
          show: false
        },
        height: 239,
        type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: '',
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [<?php echo  $quteLocName;?>],
          labels: {
            style: {
              colors: "#000000",
              fontSize: '9px'
            }
          }
        },
        yaxis: {
        labels: {
          formatter: function(val) {
            return parseInt(val)
          }
        }
      }
    };

    var chart = new ApexCharts(document.querySelector("#full_scoring_by_location_count"), options);
    chart.render();
     
    var dataArr = <?php echo json_encode($baseLinetotalByYear); ?>;
    var baselineTotalPriceArr = dataArr.map(item => item.total_cost);
    var yearArr = dataArr.map(item => item.year);
    var options = {
          series: [{ name:"",
          data: baselineTotalPriceArr
        }],
          chart: {
          type: 'bar',
          height: 212,
          toolbar: {show: false},
        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: false,
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories:yearArr,
        },
        yaxis: {
          labels: {
            formatter: function(val) {
              const formatter = new Intl.NumberFormat();
              const formattedNumber = formatter.format(val);
              const currencySymbol = '<?= Yii::app()->session['user_currency_symbol'] ?>';
              return currencySymbol + formattedNumber;
            }
          }
        }
        };
        var chart = new ApexCharts(document.querySelector("#cost_saving_by_year_baseline"), options);
        chart.render();
  
</script>
<?php $this->renderPartial('/quotes/create_quote');?>
<?php if(!empty($_GET['from']) && $_GET['from']=='information'){?>
<script type="text/javascript">
  $(document).ready(function(){
    $('.click-modal-poup').trigger('click');
  });
</script>
<?php } ?>
<?php if(!empty($_GET['tour']) && $_GET['tour']=='eSourcingTour'){?>
<script type="text/javascript">
  $(document).ready(function(){
    tour.start();
  });
</script>
<?php } ?>

<style type="text/css">
.table-currency { padding-right:28px !important; }
.location .multiselect {width: 138%;}
.department .multiselect {width: 112%;}
.apexcharts-legend { justify-content: center !important; }

.multiselect-container>li>a>label.checkbox, .multiselect-container>li>a>label.radio {color: #777;}
#quote_table_filter label{font-weight: 500;}
#quote_table_filter label input{font-weight: normal;}
#quote_table_filter {width:auto !important; }
@media only screen and (max-width: 490px)  {
    #quote_list_form .col-xs-6 { margin-top:20px; }
    #quote_list_form .search-quote { margin-top:10px; }
}


</style>

