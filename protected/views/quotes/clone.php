<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>
                <?php
                $tool_currency = Yii::app()->session['user_currency'];
                    if (isset($quote_id) && $quote_id)
                    {
                        echo 'Clone Quote';
                        if (isset($quote) && is_array($quote) && isset($quote['quote_id']) && !empty($quote['quote_id']))
                            echo ' - #' . $quote['quote_id'];
                    }
                    else echo 'Publish Quote';
                ?>
            </h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('quotes/list'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;border-color: #F79820;">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Quote List
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
        <?php if(Yii::app()->user->hasFlash('success')):?>
            <div class="alert alert-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->hasFlash('error')):?>
            <div class="alert alert-danger">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
    </div>

<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <?php if (isset($quote_id) && $quote_id) { ?>
      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active">
            <a class="has-feedback-left pull-right" href="#tab_content1" id="quote-tab" role="tab" data-toggle="tab" aria-expanded="true">
                Quote Details
            </a>
        </li>
      </ul>
  <?php } ?>

    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">
    <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('quotes/clone'); ?>">
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>Quote Information</h4>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
              <label class="control-label">Quote Name <span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control has-feedback-left" name="quote_name" id="quote_name"
                    <?php if (isset($quote['quote_name']) && !empty($quote['quote_name'])) echo 'value="' . $quote['quote_name'] . '"'; else echo 'placeholder="Quote Name"'; ?> >
              <span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
          </div>
    </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Description/Notes</label>
              <textarea class="form-control" name="quote_desc" id="quote_desc" rows="4" <?php if (!isset($quote['quote_desc']) || empty($quote['quote_desc'])) echo 'placeholder="Quote description/notes"'; ?>><?php if (isset($quote['quote_desc']) && !empty($quote['quote_desc'])) echo $quote['quote_desc']; ?></textarea>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Quote Currency</label>
              <input type="text" class="form-control" name="quote_currency" id="quote_currency"
                  <?php if (isset($quote['quote_currency']) && !empty($quote['quote_currency'])) echo 'value="' . $quote['quote_currency'] . '"'; else echo 'placeholder="Quote Currency"'; ?> >
          </div>
      </div>

      <div class="form-group">
        <div class="label-adjust"><label class="control-label">Quote Owner</label></div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control has-feedback-left" id="created_by_name" name="created_by_name" readonly
            <?php if (isset(Yii::app()->session['full_name']) && !empty(Yii::app()->session['full_name'])) echo 'value="' . Yii::app()->session['full_name'] . '"'; else echo 'placeholder="'.Yii::app()->session['full_name'].'"'; ?>>
              <input type="hidden" id="created_by_user_id" name="created_by_user_id" <?php if (isset(Yii::app()->session['user_id']) && !empty(Yii::app()->session['user_id'])) echo 'value="' . Yii::app()->session['user_id'] . '"'; ?>>
          <span class="fa fa-user form-control-feedback left"
            aria-hidden="true"></span>
        </div>
      </div>
       <div class="form-group">
        <div class="label-adjust"><label class="control-label">Position/Title</label></div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control"
            name="manager_name" id=""
            <?php if (isset($quote['manager_name']) && !empty($quote['manager_name'])) echo 'value="' . $quote['manager_name'] . '"'; else echo 'placeholder="Position/Title"'; ?>>
        
        </div>
      </div>

        <div class="form-group">
        <div class="label-adjust"><label class="control-label">Secondary Owner</label></div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control has-feedback-left"
            name="commercial_lead_name" id="commercial_lead_name"
            <?php if (isset($quote['commercial_lead_name']) && !empty($quote['commercial_lead_name'])) echo 'value="' . $quote['commercial_lead_name'] . '"'; else echo 'placeholder="Commercial Lead"'; ?>>
          <input type="hidden" name="commercial_lead_user_id" id="commercial_lead_user_id" <?php if (isset($quote['commercial_lead_user_id']) && !empty($quote['commercial_lead_user_id'])) echo 'value="' . $quote['commercial_lead_user_id'] . '"'; ?> />
          <span class="fa fa-user-circle-o form-control-feedback left"
            aria-hidden="true"></span>
        </div>
      </div>

      <div class="form-group">
        <div class="label-adjust"><label class="control-label">Position/Title</label></div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control"
            name="procurement_lead_name" id=""
            <?php if (isset($quote['procurement_lead_name']) && !empty($quote['procurement_lead_name'])) echo 'value="' . $quote['procurement_lead_name'] . '"'; else echo 'placeholder="Position/Title"'; ?>>
         
        </div>
      </div>



    <?php
        $vendors_to_show = 1;
        if (isset($quote_vendors) && is_array($quote_vendors) && count($quote_vendors))
          $vendors_to_show = count($quote_vendors);
    ?>

    <?php for ($vendor_idx=1; $vendor_idx<=$vendors_to_show; $vendor_idx++) { ?>
       <div class="col-md-4 col-sm-4 col-xs-10 date-input valid">
                <label class="control-label">Vendor Name<span style="color: #a94442;">*</span></label>
                <select required type="text" class="vendor_name_field form-control has-feedback-left"
                    name="vendor_name_<?php echo $vendor_idx; ?>" id="vendor_name_<?php echo $vendor_idx; ?>" style="  border-radius: 10px;
    border: 1px solid #4d90fe;">

                  <?php if (isset($quote_vendors[$vendor_idx - 1]['vendor_name']) && !empty($quote_vendors[$vendor_idx - 1]['vendor_name'])){ ?>
                    <option value="<?php echo $quote_vendors[$vendor_idx - 1]['vendor_id'];?>" selected><?php echo  $quote_vendors[$vendor_idx - 1]['vendor_name'];?></option>
                  <?php } ?>

                </select>
                 <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-4" style="margin-top: 30px;">
             <div class="col-md-4 col-sm-4 col-xs-10">
                 <a onclick="deleteVendor(<?php echo $vendor_idx; ?>);" title="Click here to delete this supplier" style="cursor: pointer;">
                  <span class="fa fa-minus fa-2x"></span>
                 </a>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-10">
                 <a onclick="addVendor();" title="Click here to invite more suppliers for this quote" style="cursor: pointer;">
                  <span class="fa fa-plus fa-2x"></span>
                 </a>
              </div>
            </div><div class="clearfix"></div>


        <div class="form-group" id="added_vendors_<?php echo $vendor_idx; ?>" style="display: none;">
      </div>
      <input type="hidden" name="delete_vendor_flag_<?php echo $vendor_idx; ?>" id="delete_vendor_flag_<?php echo $vendor_idx; ?>" value="0" />
    <?php } ?>
    <input type="hidden" name="vendor_count" id="vendor_count" value="<?php echo $vendors_to_show; ?>" />
        
      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Location <span style="color: #a94442;">*</span></label>
        <select required name="location_id" id="location_id" class="form-control" onchange="loadDepartmentsForSingleLocation(0);">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($quote['location_id']) && $location['location_id'] == $quote['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Department <span style="color: #a94442;">*</span></label>
              <select required name="department_id" id="department_id" class="form-control">
                  <option value="">Select Department</option>
              </select>
          </div>
      </div>

        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6 valid">
                <label class="control-label">Project</label>
                <select name="project_id" id="project_id" class="form-control">
                    <option value="">Select Project</option>
                    <?php foreach ($projects as $project) { ?>
                        <option value="<?php echo $project['project_id']; ?>"
                            <?php if (isset($quote['project_id']) && $project['project_id'] == $quote['project_id']) echo ' selected="SELECTED" '; ?>>
                            <?php echo $project['project_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6 valid">
                <label class="control-label">Spend Type</label>
                <select name="spend_type" id="spend_type" class="form-control">
                    <option value="">Select Spend Type</option>
                    <option  <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                    <option  <?php if (isset($quote['spend_type']) && $quote['spend_type'] == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
                </select>
            </div>
        </div>

      <div class="form-group">
          <div class="col-md-2 col-sm-2 col-xs-2 date-input valid">
              <label class="control-label">Opening Date <span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control has-feedback-left" name="opening_date" id="opening_date"
                    <?php if (isset($quote['opening_date']) && !empty($quote['opening_date'])) echo 'value="' . date("d/m/Y H:iA", strtotime($quote['opening_date'])) . '"'; else echo 'placeholder="Opening Date"'; ?> >
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-2 date-input valid">
              <label class="control-label">Closing Date <span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control has-feedback-left" name="closing_date" id="closing_date"
                    <?php if (isset($quote['closing_date']) && !empty($quote['closing_date'])) echo 'value="' . date("d/m/Y H:iA", strtotime($quote['closing_date'])) . '"'; else echo 'placeholder="Closing Date"'; ?> >
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-2 date-input">
              <label class="control-label">Date Needed By</label>
              <input type="text" class="form-control has-feedback-left" name="needed_date" id="needed_date"
                  <?php if (isset($quote['needed_date']) && !empty($quote['needed_date'])) echo 'value="' . date("d/m/Y H:iA", strtotime($quote['needed_date'])) . '"'; else echo 'placeholder="Date Needed By"'; ?> >
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
    </div>


      <div class="form-group">
          <?php
            $record_user_id = 0;
      if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];
      if (isset($quote['user_id']) && !empty($quote['user_id'])) $record_user_id = $quote['user_id'];  
          ?>
          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Full Name</label>
              <select class="form-control" name="user_id" id="user_id">
                  <?php foreach ($users as $user) { ?>
                      <option value="<?php echo $user['user_id']; ?>"
                                <?php if ($record_user_id == $user['user_id']) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $user['full_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Contact Phone Number</label>
              <input type="text" class="form-control has-feedback-left" name="contact_phone" id="contact_phone"
                    <?php if (isset($quote['contact_phone']) && !empty($quote['contact_phone'])) echo 'value="' . $quote['contact_phone'] . '"'; else echo 'placeholder="Contact Phone Number"'; ?> >
              <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Contact Email Address</label>
              <input type="text" class="form-control has-feedback-left" name="contact_email" id="contact_email"
                    <?php if (isset($quote['contact_email']) && !empty($quote['contact_email'])) echo 'value="' . $quote['contact_email'] . '"'; else echo 'placeholder="Contact Email Address"'; ?> >
              <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
          </div>
    </div>

      <div class="clearfix"> <br /> </div>

      <?php
        $existing_items_found = false;
        if (isset($quote_details) && is_array($quote_details) && count($quote_details))
            $existing_items_found = true;
      ?>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>
                  Quote Items
              <?php if (!isset($quote['quote_status']) || $quote['quote_status'] == 'Pending') { ?>
                    <?php if ($existing_items_found) { ?>                   
                        <div class="pull-right" style="font-size: 70%; padding-top: 5px;">
                            <a style="cursor: pointer; text-decoration: underline;"
                                  onclick="$('#here_is_where_you_add_new_items_to_this_quote').show();">
                                Click here</a>
                            To Add More Items To this Quote
                        </div>
                    <?php } ?>
                  <?php } ?>
              </h4>
          </div>
      </div>

      <?php
        $total_price = 0;
        if (isset($quote_details) && is_array($quote_details) && count($quote_details))
        {
      ?>

          <?php
            foreach ($quote_details as $quote_detail)
            {
                if (!isset($quote_detail['quantity']) || empty($quote_detail['quantity'])) $quote_detail['quantity'] = 1;
                if (!isset($quote_detail['unit_price']) || empty($quote_detail['unit_price'])) $quote_detail['unit_price'] = 0;
          ?>

              <div class="form-group">
                  <div class="col-md-2 col-sm-2 col-xs-4 date-input valid">
                      <label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
                      <input required type="text" class="form-control has-feedback-left" name="product_name[]"
                            <?php if (isset($quote_detail['product_name']) && !empty($quote_detail['product_name'])) echo 'value="' . $quote_detail['product_name'] . '"'; ?> >
                      <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-1 col-sm-1 col-xs-3 valid">
                      <label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
                      <input required style="text-align: right;" type="text" class="product_quantity form-control" name="quantity[]"
                            <?php if (isset($quote_detail['quantity']) && !empty($quote_detail['quantity'])) echo 'value="' . $quote_detail['quantity'] . '"'; ?> >
                  </div>
                   <div class="col-md-1 col-sm-1 col-xs-1">
                  <label class="control-label">Product Code</label>
                  <input required type="text" class="form-control" <?php if (isset($quote_detail['product_code']) && !empty($quote_detail['product_code'])) echo 'value="' . $quote_detail['product_code'] . '"'; ?>  name="product_code[]" id="product_code_1" />
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-3">
                      <label class="control-label">Unit of Measure</label>
                    <input type="text" class="form-control" name="uom[]" placeholder="UOM"
                            <?php if (isset($quote_detail['uom']) && !empty($quote_detail['uom'])) echo 'value="' . $quote_detail['uom'] . '"'; ?> >
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Current Unit Price</label>
                      <input type="text" class="price_of_product form-control" name="current_price[]" placeholder="Current Unit Price (If Applicable)"
              <?php if (isset($quote_detail['price']) && !empty($quote_detail['price'])) echo 'value="' . $quote_detail['price'] . '"'; ?> />
                  </div>

                  <div class="col-md-2" style="margin-top: 30px;">
                <?php if (!isset($quote['quote_status']) || $quote['quote_status'] == 'Pending') { ?>
                        <a onclick="$(this).parent().parent().remove(); return false;" class="btn btn-link">
                            <span class="fa fa-remove hidden-xs"></span>
                        </a>
                    <?php } ?>
                  </div>
              </div>

          <?php $total_price += $quote_detail['unit_price']; } ?>

      <?php
            }

            $tax_amount = 0;
            $tax_rate = 20;
            if (isset($quote['tax_rate']) && $quote['tax_rate'] && $total_price != 0)
            {
                $tax_rate = $quote['tax_rate'];
                $tax_amount = ($quote['tax_rate'] * $total_price) / 100.0;
                $total_price += $tax_amount;
            }

            if (isset($quote['ship_amount']) && $quote['ship_amount'])
                $total_price += $quote['ship_amount'];
      ?>

      <div id="here_is_where_you_add_new_items_to_this_quote"
            <?php if ($existing_items_found) echo ' style="display: none;" '; ?>>
      <fieldset id="additional-field-model">
          <div class="form-group">
              <div class="col-md- col-sm-2 col-xs-4 date-input valid">
                  <label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
                  <input required type="text" class="form-control has-feedback-left" value="" name="product_name[]" />
                  <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div class="col-md-1 col-sm-1 col-xs-2 valid">
                  <label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
                  <input required style="text-align: right;" type="text" class="product_quantity form-control" name="quantity[]" placeholder="Quantity" />
              </div>

              <div class="col-md-1 col-sm-1 col-xs-2">
                  <label class="control-label">UOM</label>
                <input type="text" class="form-control" name="uom[]" placeholder="UOM" />
              </div>

              <div class="col-md-2 col-sm-2 col-xs-4">
                  <label class="control-label">Current Unit Price</label>
                  <input type="text" class="price_of_product form-control" name="current_price[]" placeholder="Current Unit Price" />
              </div>

              <div class="col-md-2" style="margin-top: 30px;">
                  <a href="javascript:void(0);" class="btn btn-link remove-this-field">
                      <span class="fa fa-remove hidden-xs"></span>
                  </a>
                  <a href="javascript:void(0);" class="btn btn-link create-new-field">
                      <span class="fa fa-plus hidden-xs"></span>
                  </a>
              </div>
          </div>
      </fieldset>
      <div class="clearfix"> </div>
      </div>
      <div class="clearfix"> <br /> </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>
                Quote Documents
                  <div class="pull-right" style="font-size: 70%; padding-top: 5px;">
                      <a style="cursor: pointer; text-decoration: underline;"
                            onclick="$('#here_is_where_you_add_documents_to_this_quote').toggle();">
                          Click here</a>
                      To Add Documents for this Quote
                  </div>
              </h4>
          </div>
      </div>

        <?php
        // integer starts at 0 before counting
        $existing_files = array();
        $upload_dir = 'uploads/quotes/';
        if ($quote_id)
        {
            if (!is_dir('uploads/quotes')) mkdir('uploads/quotes');
            if (!is_dir('uploads/quotes/' . $quote_id))
                mkdir('uploads/quotes/' . $quote_id);
            $upload_dir = 'uploads/quotes/' . $quote_id . '/';
            if ($handle = opendir($upload_dir)) {
                while (($uploaded_file = readdir($handle)) !== false){
                    if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file))
                        $existing_files[] = $uploaded_file;
                }
            }
        }
        ?>

        <?php if (is_array($existing_files) && count($existing_files)) { ?>

            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h5>Existing Documents</h5>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <table class="table">
                    <?php $file_idx = 1; foreach ($existing_files as $uploaded_file) { ?>
                        <tr id="existing_file_id_<?php echo $file_idx; ?>">
                            <td>
                                <a href="<?php echo AppUrl::bicesUrl('uploads/quotes/' . $quote_id . '/' . $uploaded_file); ?>" target="quote_file">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                </a>
                                <?php if (!isset($quote['quote_status']) || $quote['quote_status'] == 'Pending') { ?>
                                    &nbsp;
                                    <a id="delete_quote_file_link_<?php echo $file_idx; ?>" style="cursor: pointer;"
                                       onclick="deleteQuoteFile(<?php echo $file_idx; ?>, '<?php echo $uploaded_file; ?>');">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </a>
                                <?php } ?>
                            </td>
                            <td>
                                <?php echo $uploaded_file; ?>
                            </td>
                            <td>
                                <?php
                                $file_description = "";
                                if (isset($quote_files) && is_array($quote_files))
                                {
                                    foreach ($quote_files as $a_quote_file)
                                    {
                                        if ($a_quote_file['file_name'] == $uploaded_file)
                                        {
                                            $file_description = $a_quote_file['description'];
                                            break;
                                        }
                                    }
                                }
                                echo $file_description;
                                ?>
                            </td>
                        </tr>
                        <?php $file_idx += 1; } ?>
                </table>
            </div>

            <div class="clearfix"> <br /> </div>

        <?php } ?>


        <div id="here_is_where_you_add_documents_to_this_quote" style="display: none;">
        <?php if (!isset($quote['quote_status']) || $quote['quote_status'] == 'Pending') { ?>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <h5>Upload Documents</h5>
            </div>
        </div>

        <div class="form-group" id="document_area_1">
          <div class="col-md-5 col-sm-5 col-xs-10">
            <input class="form-control" type="file" name="quote_file_1" id="quote_file_1" />
          </div>
          <div class="col-md-1 col-sm-1 col-xs-2">
                 <a onclick="deleteDocument(1);" title="Click here to delete this document" style="cursor: pointer;">
                  <span class="fa fa-minus fa-2x"></span>
                 </a>
                 &nbsp;
                 <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
                  <span class="fa fa-plus fa-2x"></span>
                 </a>
          </div>
          <div class="clearfix"> </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">File/Document Description</label>
            <input class="form-control" type="text" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" />
          </div>
        </div>
      <input type="hidden" name="delete_document_flag_1" id="delete_document_flag_1" value="0" />
      <div class="clearfix" id="end_document_area_1"> <br /> </div>
        <?php } ?>
        
        <input type="hidden" name="total_documents" id="total_documents" value="1" />
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>
                  Questionnaire
              <?php if (!isset($quote['quote_status']) || $quote['quote_status'] == 'Pending') { ?>
                      <div class="pull-right" style="font-size: 70%; padding-top: 5px;">
                          <a style="cursor: pointer; text-decoration: underline;"
                                onclick="$('#here_is_where_you_add_questions_to_this_quote').show();">
                              Click here</a>
                          To Add Questionnaire for this Quote
                      </div>
                  <?php } ?>
              </h4>
          </div>
      </div>

        <div id="here_is_where_you_add_questions_to_this_quote_edit">
            <!-- <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h5>Quote Questtionnaire</h5>
                </div>
            </div> -->
            <div class="clearfix"> </div><br/>
            <?php
            $i = 1;
            if (isset($quote_questions) && is_array($quote_questions))
            {
                foreach ($quote_questions as $quote_question)
                {
                    $quoteAnswers = new QuoteAnswer();
                    $quoteAnswers = $quoteAnswers->getQuoteAnswers($quote_question['id']);
                    ?>

                    <div class="form-group">
                        <div class="col-md-5 col-sm-5 col-xs-10">
                            <input type="text" class="form-control" placeholder="Question for the supplier submitting quote"
                                   name="quote_quesion_<?php echo $i ?>" id="quote_question_<?php echo $i ?>" value="<?php echo $quote_question['question'] ?>"/>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-2" style="margin-top: 8px;">
                            <a onclick="deleteQuestion(<?php echo $i ?>);" title="Click here to delete this question" style="cursor: pointer;">
                                <span class="fa fa-minus fa-2x"></span>
                            </a>
                            &nbsp;
                            <a onclick="addQuestion();" title="Click here to add another questions" style="cursor: pointer;">
                                <span class="fa fa-plus fa-2x"></span>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <label class="control-label">Question Type</label>
                            <select id="question_type_<?php echo $i ?>" name="question_type_<?php echo $i ?>" class="form-control" onchange="selectType(<?php echo $i ?>);">
                                <option <?php if($quote_question['question_type']=='free_text') { ?> selected="selected" <?php } ?> value="free_text">Free Text</option>
                                <option <?php if($quote_question['question_type']=='yes_or_no') { ?> selected="selected" <?php } ?> value="yes_or_no">Yes or No</option>
                                <option <?php if($quote_question['question_type']=='multiple_choice') { ?> selected="selected" <?php } ?> value="multiple_choice">Multiple Choice</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>

                        <?php  $j=1;
                        foreach ($quoteAnswers as $quoteAnswer)
                        { ?>

                            <div class="col-md-3 col-sm-3 col-xs-6" id="yes_no_area_<?php echo $i ?>" <?php if($quote_question['question_type']=='free_text' || $quote_question['question_type']=='multiple_choice') { ?> style="display: none;" <?php } ?> >
                                <div>&nbsp;</div>
                                <table style="width: 200px;">
                                    <tr>
                                        <td><b>Yes =</b></td>
                                        <td><input type="radio" <?php if($quoteAnswer['yes']=='Correct') { ?> checked="checked" <?php } ?>  value="Correct" name="yes_<?php echo $i ?>">  Correct</td>
                                        <td><input type="radio" <?php if($quoteAnswer['yes']=='Incorrect') { ?> checked="checked" <?php } ?> value="Incorrect" name="yes_<?php echo $i ?>">  Incorrect</td>
                                    </tr>
                                    <tr>
                                        <td><b>No =</b></td>
                                        <td><input type="radio" <?php if($quoteAnswer['no']=='Correct') { ?> checked="checked" <?php } ?> value="Correct" name="no_<?php echo $i ?>">  Correct</td>
                                        <td><input type="radio" <?php if($quoteAnswer['no']=='Incorrect') { ?> checked="checked" <?php } ?> value="Incorrect" name="no_<?php echo $i ?>">  Incorrect</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                            <div id="answer_section_1">
                                <div class="form-group" id="answer_area_<?php echo $i; ?>_<?php echo $j; ?>" <?php if($quote_question['question_type']=='free_text' || $quote_question['question_type']=='yes_or_no') { ?> style="display: none;" <?php } ?>>
                                    <div class="col-md-1 col-sm-1 col-xs-2" style="margin-top: 25px;">
                                        <a onclick="deleteAnswer(<?php echo $i; ?>,1);" title="Click here to delete this answer"
                                           style="cursor: pointer;">
                                            <span class="fa fa-minus fa-2x"></span>
                                        </a>
                                        &nbsp;
                                        <a onclick="addAnswer(<?php echo $i; ?>);" title="Click here to add another answer"
                                           style="cursor: pointer;">
                                            <span class="fa fa-plus fa-2x"></span>
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div>&nbsp;</div>
                                        <div style="float: left;width: 200px;margin-right: 10px;">
                                            <input type="text" class="form-control" placeholder="Answer"
                                                   name="quote_answer_1_<?php echo $i; ?>" id="quote_answer_<?php echo $i; ?>_<?php echo $j; ?>"
                                                   value="<?php echo $quoteAnswer['answer']; ?>"/>
                                        </div>

                                        <div style="float: left; width:60px;">
                                            <div>Excellent</div>
                                            <div class="clearfix"></div>
                                            <div style="margin-left: 15px;"><input type="radio" value="Excellent" <?php if($quoteAnswer['excellent']=='Excellent') { ?> checked="checked" <?php } ?>
                                                                                   name="answer_<?php echo $i; ?>_<?php echo $j; ?>"></div>
                                        </div>

                                        <div style="float: left; width:50px;">
                                            <div>Good</div>
                                            <div class="clearfix"></div>
                                            <div style="margin-left: 8px;"><input type="radio" value="Good" <?php if($quoteAnswer['good']=='Good') { ?> checked="checked" <?php } ?>
                                                                                  name="answer_<?php echo $i; ?>_<?php echo $j; ?>"></div>
                                        </div>

                                        <div style="float: left; width:50px;">
                                            <div>Okay</div>
                                            <div class="clearfix"></div>
                                            <div style="margin-left: 8px;"><input type="radio" value="Okay" <?php if($quoteAnswer['okay']=='Okay') { ?> checked="checked" <?php } ?>
                                                                                  name="answer_<?php echo $i; ?>_<?php echo $j; ?>"></div>
                                        </div>

                                        <div style="float: left; width:40px;">
                                            <div>Bad</div>
                                            <div class="clearfix"></div>
                                            <div style="margin-left: 4px;"><input type="radio" value="Bad" <?php if($quoteAnswer['bad']=='Bad') { ?> checked="checked" <?php } ?>
                                                                                  name="answer_<?php echo $i; ?>_<?php echo $j; ?>"></div>
                                        </div>

                                        <div style="float: left; width:60px;">
                                            <div>Very Bad</div>
                                            <div class="clearfix"></div>
                                            <div style="margin-left: 15px;"><input type="radio" value="Very Bad" <?php if($quoteAnswer['very_bad']=='Very Bad') { ?> checked="checked" <?php } ?>
                                                                                   name="answer_<?php echo $i; ?>_<?php echo $j; ?>"></div>
                                        </div>

                                    </div>
                                    <input type="hidden" name="delete_answer_flag_<?php echo $j; ?>"
                                           id="delete_answer_flag_<?php echo $j; ?>" value="0"/>

                                    <div class="clearfix" id="end_answer_area_<?php echo $j; ?>" style="margin-bottom:15px;"><br/>
                                    </div>
                                </div>

                                <input type="hidden" name="total_answers_<?php echo $i; ?>" id="total_answers_<?php echo $i; ?>" value="<?php echo count($quoteAnswers); ?>"/>
                            </div>
                            <input type="hidden" name="delete_question_flag_<?php echo $i; ?>" id="delete_question_flag_<?php echo $i; ?>" value="0"/>

                            <div class="clearfix" id="end_question_area_<?php echo $i; ?>" style="margin-bottom:-10px;"><br/></div>
                            <input type="hidden" name="total_questions" id="total_questions" value="<?php echo count($quote_questions); ?>"/>
                            <?php
                            $j++;
                        } ?>
                    </div>
                    <?php $i++; }
            } ?>
        </div>

      <div id="here_is_where_you_add_questions_to_this_quote" style="display: none;">


          <div class="form-group" id="question_area_1">
              <div class="col-md-5 col-sm-5 col-xs-10">
                  <label class="control-label">Question for the supplier submitting quote</label>
                  <input type="text" class="form-control" placeholder="Question for the supplier submitting quote" name="quote_question_1" id="quote_question_1" value="" />
              </div>
              <div class="col-md-1 col-sm-1 col-xs-2" style="margin-top: 33px;">
                  <a onclick="deleteQuestion(1);" title="Click here to delete this question" style="cursor: pointer;">
                      <span class="fa fa-minus fa-2x"></span>
                  </a>
                  &nbsp;
                  <a onclick="addQuestion();" title="Click here to add another questions" style="cursor: pointer;">
                      <span class="fa fa-plus fa-2x"></span>
                  </a>
              </div>
              <div class="clearfix">  </div>
              <div class="col-md-2 col-sm-2 col-xs-4">
                  <label class="control-label">Question Type</label>
                  <select id="question_type_1" name="question_type_1" class="form-control" onchange="selectType(1);">
                      <option value="free_text">Free Text</option>
                      <option value="yes_or_no">Yes or No</option>
                      <option value="multiple_choice">Multiple Choice</option>
                  </select>
              </div>

              <div class="clearfix"> </div>
              <div class="col-md-3 col-sm-3 col-xs-6" style="display: none;" id="yes_no_area_1">
                  <div>&nbsp;</div>
                  <table style="width: 200px;">
                      <tr>
                          <td><b>Yes =</b></td>
                          <td><input type="radio" value="Correct" name="yes_1" >  Correct</td>
                          <td><input type="radio" value="Incorrect" name="yes_1" >  Incorrect</td>
                      </tr>
                      <tr>
                          <td><b>No =</b></td>
                          <td><input type="radio" value="Correct" name="no_1" > Correct</td>
                          <td><input type="radio" value="Incorrect" name="no_1" > Incorrect</td>
                      </tr>
                  </table>
              </div>
              <div class="clearfix"> </div>
              <div id="answer_section_1">
              <?php for($i=1; $i<=10;$i++) {  ?>
              <div class="form-group" id="answer_area_1_<?php echo $i; ?>" style="display: none;">
                  <div class="col-md-1 col-sm-1 col-xs-2" style="margin-top: 25px;">
                      <a onclick="deleteAnswer(<?php echo $i; ?>,1);" title="Click here to delete this answer" style="cursor: pointer;">
                          <span class="fa fa-minus fa-2x"></span>
                      </a>
                      &nbsp;
                      <a onclick="addAnswer(1);" title="Click here to add another answer" style="cursor: pointer;">
                          <span class="fa fa-plus fa-2x"></span>
                      </a>
                  </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div>&nbsp;</div>
                  <div style="float: left;width: 200px;margin-right: 10px;">
                      <input type="text" class="form-control" placeholder="Answer" name="quote_answer_1_<?php echo $i; ?>" id="quote_answer_1_<?php echo $i; ?>" value="" />
                  </div>

                  <div style="float: left; width:60px;">
                      <div>Excellent</div>
                      <div class="clearfix">  </div>
                      <div style="margin-left: 15px;"><input type="radio" value="Excellent" name="answer_1_<?php echo $i; ?>" > </div>
                  </div>

                  <div style="float: left; width:50px;">
                      <div>Good</div>
                      <div class="clearfix">  </div>
                      <div style="margin-left: 8px;"><input type="radio" value="Good" name="answer_1_<?php echo $i; ?>" > </div>
                  </div>

                  <div style="float: left; width:50px;">
                      <div>Okay</div>
                      <div class="clearfix">  </div>
                      <div style="margin-left: 8px;"><input type="radio" value="Okay" name="answer_1_<?php echo $i; ?>" > </div>
                  </div>

                  <div style="float: left; width:40px;">
                      <div>Bad</div>
                      <div class="clearfix">  </div>
                      <div style="margin-left: 4px;"><input type="radio" value="Bad" name="answer_1_<?php echo $i; ?>" > </div>
                  </div>

                  <div style="float: left; width:60px;">
                      <div>Very Bad</div>
                      <div class="clearfix">  </div>
                      <div style="margin-left: 15px;"><input type="radio" value="Very Bad" name="answer_1_<?php echo $i; ?>" > </div>
                  </div>

                </div>
                  <input type="hidden" name="delete_answer_flag_<?php echo $i; ?>" id="delete_answer_flag_<?php echo $i; ?>" value="0" />
                  <div class="clearfix" id="end_answer_area_<?php echo $i; ?>" style="margin-bottom:15px;"> <br /> </div>
                </div>

              <?php } ?>
              <input type="hidden" name="total_answers_1" id="total_answers_1" value="1" />
              </div>
              <input type="hidden" name="delete_question_flag_1" id="delete_question_flag_1" value="0" />
              <div class="clearfix" id="end_question_area_1" style="margin-bottom:15px;"> <br /> </div>
              <input type="hidden" name="total_questions" id="total_questions" value="1" />
          </div>

          <div class="clearfix">  </div>

      </div>
                     
    <div class="clearfix"> <br /> </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" name="quote_id" id="quote_id" value="<?php //if (isset($quote_id)) echo $quote_id; ?>" />
            <input type="hidden" name="quote_status" id="quote_status" value="Pending" />
            <input type="hidden" name="created_by_user_id" id="created_by_user_id" value="<?php if (isset(Yii::app()->session['user_id'])) echo Yii::app()->session['user_id']; else echo '0'; ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
        </div>
    </div>

    <!--<div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="checkbox">
        <div class="col-md-10 col-md-offset-1">
          <input type="checkbox" class="form-control" name="invite_suppliers" id="invite_suppliers" value="1" />
          <label for="invite_suppliers" style="padding-left: 5px !important; margin-bottom: 6px !important;">
            Publish/Edit Quote and Send to Above Suppliers
          </label>
        </div>
      </div>
          </div>
    </div>-->

</form>
<?php if(!empty($quote_id)) { ?>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <a href="javascript:void(0);" onclick="$('#quote_form').submit();"><button type="button" class="btn btn-primary">Publish Quote</button></a>
            </div>
            <div class="clearfix"> <br /><br /> </div>
        </div>
<?php } ?>
</div>
<div class="clearfix"> </div></div></div>
<div id="new_document_code" style="display: none;">
  <div class="form-group" id="document_area_DOCIDX">
    <div class="col-md-5 col-sm-5 col-xs-10">
        <input class="form-control" type="file" name="quote_file_DOCIDX" id="quote_file_DOCIDX" />
    </div>
    <div class="col-md-1 col-sm-1 col-xs-2">
        <a onclick="deleteDocument(DOCIDX);" title="Click here to delete this document" style="cursor: pointer;">
            <span class="fa fa-minus fa-2x"></span>
        </a>
        <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
            <span class="fa fa-plus fa-2x"></span>
        </a>
    </div>
    <div class="clearfix"> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
            <label class="control-label">File/Document</label>
      <input class="form-control" type="text" name="file_desc_DOCIDX" id="file_desc_DOCIDX" placeholder="File/Document Description" />
    </div>
  </div>
  <input type="hidden" name="delete_document_flag_DOCIDX" id="delete_document_flag_DOCIDX" value="0" />
  <div class="clearfix" id="end_document_area_DOCIDX"> <br /> </div>
</div>

    <div id="new_question_code" style="display: none;">
        <div class="form-group" id="question_area_QIDX">
            <div class="col-md-5 col-sm-5 col-xs-10">
                <input type="text" class="form-control" placeholder="Question for the supplier submitting quote"
                       name="quote_question_QIDX" id="quote_question_QIDX"  />
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2" style="margin-top:7px;">
                <a onclick="deleteQuestion(QIDX);" title="Click here to delete this question" style="cursor: pointer;">
                    <span class="fa fa-minus fa-2x"></span>
                </a>
                <a onclick="addQuestion();" title="Click here to add another question" style="cursor: pointer;">
                    <span class="fa fa-plus fa-2x"></span>
                </a>
            </div>
            <div class="clearfix">  </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <label class="control-label">Question Type</label>
                <select id="question_type_QIDX" name="question_type_QIDX" class="form-control" onchange="selectType(QIDX);">
                    <option value="free_text">Free Text</option>
                    <option value="yes_or_no">Yes or No</option>
                    <option value="multiple_choice">Multiple Choice</option>
                </select>
            </div>

            <div class="clearfix"> </div>
            <div class="col-md-3 col-sm-3 col-xs-6" style="display: none;" id="yes_no_area_QIDX">
                <div>&nbsp;</div>

                <table style="width: 200px;">
                    <tr>
                        <td><b>Yes =</b></td>
                        <td><input type="radio" value="Correct" name="yes_QIDX" >  Correct</td>
                        <td><input type="radio" value="Incorrect" name="yes_QIDX" >  Incorrect</td>
                    </tr>
                    <tr>
                        <td><b>No =</b></td>
                        <td><input type="radio" value="Correct" name="no_QIDX" > Correct</td>
                        <td><input type="radio" value="Incorrect" name="no_QIDX" > Incorrect</td>
                    </tr>
                </table>

            </div>
            <div class="clearfix"> </div>
            <div id="answer_section_QIDX">
            <?php for($i=1; $i<=10;$i++) {  ?>

            <div class="form-group" id="answer_area_QIDX_<?php echo $i; ?>" style="display: none;">
                <div class="col-md-1 col-sm-1 col-xs-2" style="margin-top: 25px;">
                    <a onclick="deleteAnswer(<?php echo $i; ?>,QIDX);" title="Click here to delete this answer" style="cursor: pointer;">
                        <span class="fa fa-minus fa-2x"></span>
                    </a>
                    &nbsp;
                    <a onclick="addAnswer(QIDX);" title="Click here to add another answer" style="cursor: pointer;">
                        <span class="fa fa-plus fa-2x"></span>
                    </a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div>&nbsp;</div>
                    <div style="float: left;width: 200px;margin-right: 10px;">
                        <input type="text" class="form-control" placeholder="Answer" name="quote_answer_QIDX_<?php echo $i; ?>" id="quote_answer_QIDX_<?php echo $i; ?>" value="" />
                    </div>

                    <div style="float: left; width:60px;">
                        <div>Excellent</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 15px;"><input type="radio" value="Excellent" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                    <div style="float: left; width:50px;">
                        <div>Good</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 8px;"><input type="radio" value="Good" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                    <div style="float: left; width:50px;">
                        <div>Okay</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 8px;"><input type="radio" value="Okay" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                    <div style="float: left; width:40px;">
                        <div>Bad</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 4px;"><input type="radio" value="Bad" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                    <div style="float: left; width:60px;">
                        <div>Very Bad</div>
                        <div class="clearfix">  </div>
                        <div style="margin-left: 15px;"><input type="radio" value="Very Bad" name="answer_QIDX_<?php echo $i; ?>" > </div>
                    </div>

                </div>
                <input type="hidden" name="delete_answer_flag_<?php echo $i; ?>" id="delete_answer_flag_<?php echo $i; ?>" value="0" />
                <div class="clearfix" id="end_answer_area_<?php echo $i; ?>" style="margin-bottom: 27px;"> <br /> </div>

            </div>
             <?php } ?>
            <input type="hidden" name="total_answers_QIDX" id="total_answers_QIDX" value="1" />
            </div>
        </div>
        <input type="hidden" name="delete_question_flag_QIDX" id="delete_question_flag_QIDX" value="0" />
        <div class="clearfix" id="end_question_area_QIDX"> <br /> </div>
        <div class="clearfix"> </div>
    </div>

<div id="new_vendor_code" style="display: none;"> <br />
  <div class="form-group" id="vendor_area_VENDOR_IDX">
      <div class="col-md-4 col-sm-4 col-xs-8">
           <select required type="text" class="vendor_name_field form-control has-feedback-left" 
                    name="vendor_name_VENDOR_IDX" id="vendor_name_VENDOR_IDX" style="  border-radius: 10px;
    border: 1px solid #4d90fe;"
                 >
        </select>
          <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-4">
      
        <div class="col-md-4 col-sm-4 col-xs-8">
          <a onclick="deleteVendor(VENDOR_IDX);" title="Click here to delete this supplier" style="cursor: pointer;">
              <span class="fa fa-minus fa-2x"></span>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-8">
          <a onclick="addVendor();" title="Click here to invite more suppliers for this quote" style="cursor: pointer;">
                <span class="fa fa-plus fa-2x"></span>
            </a>
        </div>
      </div>
  </div>

  <div class="form-group" id="added_vendors_VENDOR_IDX" style="display: none;">
  </div>
  <input type="hidden" name="delete_vendor_flag_VENDOR_IDX" id="delete_vendor_flag_VENDOR_IDX" value="0" /> 
</div>

    <style type="text/css">
        canvas {
            display: block;
            max-width: 800px;
            margin: 60px auto;
        }
    </style>
<script type="text/javascript">
$(document).ready( function() {

   $('#new_vendor_code').hide();
  <?php if (isset($quote['location_id']) && !empty($quote['location_id'])) { ?>

    <?php if (isset($quote['department_id']) && !empty($quote['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $quote['department_id']; ?>);
    <?php } else { ?>
    loadDepartmentsForSingleLocation(0);
    <?php } ?>

  <?php } ?>

  $('#manager_name').devbridgeAutocomplete({
    serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
    onSelect: function(suggestion) { $('#manager_user_id').val(suggestion.data); },
    onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#manager_user_id').val(0); }
  });

  $('#procurement_lead_name').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
        onSelect: function(suggestion) { $('#procurement_lead_user_id').val(suggestion.data); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#procurement_lead_user_id').val(0); }
    });

  $('#commercial_lead_name').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('users/getUsers'); ?>',
        onSelect: function(suggestion) { $('#commercial_lead_user_id').val(suggestion.data); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#commercial_lead_user_id').val(0); }
    });


    $('#quote_form').on('submit', function() {
        $('#quote_status').removeAttr('disabled');
    });

    $( "#quote_form" ).validate( {
        rules: {
            quote_name: "required",
            location_id: "required",
            department_id: "required"
        },
        messages: {
            quote_name: "Quote name is required",
            location_id: "Location is required",
            department_id: "Department is required"
        },
      submitHandler: function(form) {
        var product_id = 0;
        $('.product_quantity').each(function() {
          if ($.trim($(this).val()) == "") {
              product_id = $(this).closest('.form-group').find('.product_id').val();
              if (product_id == '0') $(this).val('0');
            else $(this).val('1');
          }
        });
        form.submit();
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      element.parents( ".col-sm-6" ).addClass( "has-feedback" );

      if ( element.prop( "type" ) === "checkbox" )
        error.insertAfter( element.parent( "label" ) );
      else error.insertAfter( element );

      if ( !element.next( "span" )[ 0 ] )
        $( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
    },
    success: function ( label, element ) {
      if ( !$( element ).next( "span" )[ 0 ] )
        $( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
      //$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
    },
    unhighlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error" );
      //$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
    }
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field"
    });

  $('#opening_date').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
      timePicker: true,
      timePickerIncrement: 10,
      locale: {
    format: 'DD/MM/YYYY h:mm A'
    }
  });

  $('#closing_date').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
      timePicker: true,
      timePickerIncrement: 10,
      locale: {
    format: 'DD/MM/YYYY h:mm A'
    }
  });

    $('#needed_date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_3",
        timePicker: true,
        timePickerIncrement: 10,
        locale: {
            format: 'DD/MM/YYYY h:mm A'
        }
    });

    /*
    $('body').on('click', "input[name^='product_name']", function() {
        createProductAutocompleteSearch();
    });
    */

    $('body').on('click', "input[name^='vendor_name_']", function() {
       // createVendorAutocompleteSearch();
    });
    <?php //echo AppUrl::bicesUrl('orders/getVendors'); if you uncomment the placed this line in line number 1364 under url ?>
  /*
    $("#vendor_id").select2({
      placeholder: 'Invite Suppliers', minimumInputLength: 1,
      ajax: {
        url: '',
        dataType: 'json', quietMillis: 50, type: "GET",
      data: function (term) { return { query: term }; },
      processResults: function (data) { return { results: data }; }     
      }
    });
    $("#vendor_id").on('change', function() { populateVendorDetails(); });
  */


   $(".select2").select2({
      placeholder: 'Invite Suppliers', 
      minimumInputLength: 1,
     /* border-radius: '10px',
      border: '1px solid #4d90fe',*/
      ajax: {
        url: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
        dataType: 'json', quietMillis: 50, type: "GET",
      data: function (term) { return { query: term }; },
      processResults: function (data) { 
        $('#vendor_name_select2_1').trigger('change.select2');
        return { results: data }; }     
      }
    });


   
    



  <?php if (isset($quote_vendors) && is_array($quote_vendors) && count($quote_vendors)) { ?>
    <?php for ($vendor_idx=1; $vendor_idx<=count($quote_vendors); $vendor_idx++) { ?>
      populateVendorDetails(<?php echo $vendor_idx; ?>,<?php echo $quote_vendors[$vendor_idx-1]['vendor_id'];?>);
    <?php } ?>
  <?php } ?>


});


function dropdownVendors(obj){
    var testingobj = obj;
    var options_html="";
     $.ajax({
            type: "POST", data: { }, dataType: "json",
            url: BICES.Options.baseurl + '/orders/getVendors',
            success: function(options) {
              
                var options_html = '<option value="">Select Vendor</option>';
                for (var i=0; i<options.suggestions.length; i++)
                    options_html += '<option value="' + options.suggestions[i].data + '">' +  options.suggestions[i].value + '</option>';
                     $("#"+testingobj).append(options_html);
               
               
            }
           
        });
    
   }

function populateVendorDetails(vendor_idx_rownum,vendor_idx)
{  
    $.ajax({
        type: "POST", data: { vendor_ids: vendor_idx,quote_id:"<?php echo !empty($quote['quote_id'])?$quote['quote_id']:0;?>" }, dataType: 'json',
        url: "<?php echo AppUrl::bicesUrl('quotes/getVendorDetails/'); ?>",
        error: function() { $('#added_vendors_' + vendor_idx_rownum).html('<div class="clearfix"> <br /> </div>Cannot load selected supplier details ... please try again !!!') },
        success: function(data) {
      var vendor_html = '<div class="clearfix"> <br /> </div>';
      var current_vendor_html = "";
      
      for (var i=0; i<data.length; i++)
      {
        current_vendor_html = "";
        current_vendor_html += '<div class="form-group">';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Supplier Name </label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_name_' + data[i].vendor_id + '" value="' + data[i].vendor_name + '" placeholder="Supplier Name">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Contact Person</label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_contact_' + data[i].vendor_id + '" value="' + data[i].contact_name + '" placeholder="Contact Person">';
        current_vendor_html += '</div>';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="form-group">';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Address Line 1</label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_address_1_' + data[i].vendor_id + '" value="' + data[i].address_1 + '" placeholder="Address Line 1">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Address Line 2</label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_address_2_' + data[i].vendor_id + '" value="' + data[i].address_2 + '" placeholder="Address Line 2">';
        current_vendor_html += '</div>';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="form-group">';
        current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">City</label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_city_' + data[i].vendor_id + '" value="' + data[i].city + '" placeholder="City">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">State</label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_state_' + data[i].vendor_id + '" value="' + data[i].state + '" placeholder="State">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-4 col-sm-4 col-xs-4"><label class="control-label">Zip Code</label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_zip_' + data[i].vendor_id + '" value="' + data[i].zip + '" placeholder="Zip Code">';
        current_vendor_html += '</div>';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="form-group">';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Email Address</label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_email_' + data[i].vendor_id + '" value="' + data[i].emails + '" placeholder="Email Address">';
        current_vendor_html += '</div>';
        current_vendor_html += '<div class="col-md-6 col-sm-6 col-xs-6"><label class="control-label">Phone Number</label>';
        current_vendor_html += '<input type="text" class="form-control" name="vendor_phone_' + data[i].vendor_id + '" value="' + data[i].phone_1 + '" placeholder="Phone Number">';
        current_vendor_html += '</div>';
        current_vendor_html += '</div>';
        current_vendor_html += '<input type="hidden" name="display_vendor_id_' + vendor_idx + '" id="display_vendor_id_' + vendor_idx_rownum + '" value="' + data[i].vendor_id + '">';
        
        vendor_html = vendor_html + current_vendor_html;
        vendor_html = vendor_html + '<div class="clearfix"><br /></div>';
      }

      $('#added_vendors_' + vendor_idx_rownum).html(vendor_html);
    }
    });
}

function createProductAutocompleteSearch()
{
    var product_type = '';
    $("input[name^='product_name']").each(function () {
        product_type = $(this).closest('.form-group').find('.product_type').val();
        $(this).devbridgeAutocomplete({
            serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getProducts'); ?>',
            params: { product_type: 'Product' },
            onSelect: function(suggestion) {
                $(this).closest('.form-group').find('.product_id').val(suggestion.data.id);
                if ($.trim($(this).closest('.form-group').find('.price_of_product').val()) == ''
                    || $.trim($(this).closest('.form-group').find('.price_of_product').val()) == '0')
                  $(this).closest('.form-group').find('.price_of_product').val(suggestion.data.price);
                if ($.trim($(this).closest('.form-group').find('.product_quantity').val()) == '')
                  $(this).closest('.form-group').find('.product_quantity').val(1);
            }
        });
    });
}

function createVendorAutocompleteSearch()
{
  var my_id = "";
    $("input[name^='vendor_name_']").each(function () {
      my_id = $(this).attr('id');
      if (my_id != 'vendor_name_VENDOR_IDX')
      {
          $(this).devbridgeAutocomplete({
              serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
              onSelect: function(suggestion) {
                  $(this).closest('.form-group').find('.vendor_id').val(suggestion.data);
              }
          });
    }
    });
}

function displayVendorQuote(quote_id)
{
  var vendor_id = $('#quote_vendor_id').val();
  $('#vendor_quote_area').html('');
  
  if (vendor_id != 0)
  {
    $('#vendor_quote_area').html('Please wait ... retrieveing selected supplier quote ... ');
      $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/getVendorQuote'); ?>', 
      data: { quote_id: quote_id, vendor_id: vendor_id }, type: "POST",
      success: function(quote_data) { $('#vendor_quote_area').html(quote_data) }, 
      error: function() { 
        $('#vendor_quote_area').html('ERROR ! Cannot retrieve selected supplier quote. Please try later ... '); 
      }   
      });
  }
}
function displaySupplierEvaluation(quote_id)
{   
//var vendor_id = $('#evalution_vendor_id').val();

    var vendor_id = [];
    $.each($("input[name^='evalution_vendor_id']:checked"), function(){            
      vendor_id.push($(this).val());
    }); 
    $('#vendor_evaluation_area').html('');

    if (vendor_id != 0)
    {
        $('#vendor_evaluation_area').html('Please wait ... retrieveing selected supplier quote ... ');
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('quotes/getVendorEvaluation'); ?>',
            data: { quote_id: quote_id, vendor_id: vendor_id }, type: "POST",
            success: function(quote_data) { $('#vendor_evaluation_area').html(quote_data) },
            error: function() {
                $('#vendor_evaluation_area').html('ERROR ! Cannot retrieve selected supplier quote. Please try later ... ');
            }
        });
    }
}

function loadDepartmentsForSingleLocation(department_id)
{
    var location_id = $('#location_id').val();
    var single = 1;

    if (location_id == 0)
        $('#department_id').html('<option value="">Select Department</option>');
    else
    {
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getDepartments',
            success: function(options) {
                var options_html = '<option value="">Select Department</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                $('#department_id').html(options_html);
                if (department_id != 0) $('#department_id').val(department_id);
            },
            error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
        });
    }
}

function deleteQuoteFile(file_idx, file_name)
{
  $('#delete_quote_file_link_' + file_idx).confirmation({
    title: "Are you sure you want to delete the attached file?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
      $('#existing_file_id_' + file_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('quotes/deleteFile/'); ?>",
            data: { quote_id: $('#quote_id').val(), file_name: file_name }
        });
    },
    onCancel: function() {  }
  }); 
}

function addDocument()
{
  var total_documents = $('#total_documents').val();
  total_documents = parseInt(total_documents);
  total_documents = total_documents + 1;
  $('#total_documents').val(total_documents);
  
  var new_document_html = $('#new_document_code').html();
  new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
  $('#total_documents').before(new_document_html);
}

function deleteDocument(docidx)
{
    var display_document_count = 0;
    $("div[id^='document_area']").each(function () {
        if ($(this).is(':visible')) display_document_count += 1;
    });

    if (display_document_count <= 1) alert("You must have at least one document field in the quote form");
    else
    {
        if (confirm("Are you sure you want to delete this document?"))
        {
            $('#delete_document_flag_' + docidx).val(1);
            $('#document_area_' + docidx).hide();
            $('#end_document_area_' + docidx).hide();
        }
    }
}

function addQuestion()
{
    var total_questions = $('#total_questions').val();
    total_questions = parseInt(total_questions);
    total_questions = total_questions + 1;
    $('#total_questions').val(total_questions);

    var new_question_html = $('#new_question_code').html();
    new_question_html = new_question_html.replace(/QIDX/g, total_questions);
    $('#total_questions').before(new_question_html);
}



function deleteQuestion(qidx)
{
    var display_question_count = 0;
    $("div[id^='question_area']").each(function () {
        if ($(this).is(':visible')) display_question_count += 1;
    });

    if (display_question_count <= 1) alert("You must have at least one question field in the quote form");
    else
    {
        if (confirm("Are you sure you want to delete this question?"))
        {
            $('#delete_question_flag_' + qidx).val(1);
            $('#question_area_' + qidx).hide();
            $('#end_question_area_' + qidx).hide();
        }
    }
}
function addAnswer(index)
{

    var new_index = $('#total_answers_'+index).val();
    new_index = parseInt(new_index);
    new_index = new_index + 1;
    $('#answer_area_' +index+'_'+ new_index).show();
    $('#total_answers_'+index).val(new_index);
}
function deleteAnswer(ansidx,index)
{
    var display_answer_count = 0;
    $("div[id^='answer_area']").each(function () {
        if ($(this).is(':visible')) display_answer_count += 1;
    });

    if (display_answer_count <= 1) alert("You must have at least one answer field in the quote form");
    else
    {
        if (confirm("Are you sure you want to delete this answer?"))
        {
            var new_index = $('#total_answers_'+index).val();
            new_index = parseInt(new_index);
            new_index = new_index - 1;
            $('#total_answers_'+index).val(new_index);
            $('#delete_answer_flag_' + ansidx).val(1);
            $('#answer_area_' + index+'_'+ansidx).hide();
            $('#end_answer_area_' + ansidx).hide();

        }
    }
}

function addVendor()
{
  var vendor_count = $('#vendor_count').val();
  vendor_count = parseInt(vendor_count);
  vendor_count = vendor_count + 1;
  $('#vendor_count').val(vendor_count);
  
  var new_vendor_html = $('#new_vendor_code').html();
  new_vendor_html = new_vendor_html.replace(/VENDOR_IDX/g, vendor_count);
  $('#vendor_count').before(new_vendor_html);
  dropdownVendors("vendor_name_"+vendor_count);
}
</script>
<?php for ($vendor_idx=1; $vendor_idx<=$vendors_to_show; $vendor_idx++) { ?>
<script type="text/javascript">
dropdownVendors("vendor_name_<?php echo $vendor_idx;?>");
</script>
<?php } ?>

<script type="text/javascript">

function deleteVendor(vendor_idx)
{
  var display_vendor_count = 0;
    $("div[id^='vendor_area']").each(function () {
      if ($(this).is(':visible')) display_vendor_count += 1;
  });
  
  if (display_vendor_count <= 1) alert("You must have at least one vendor field in the quote form");
  else
  {
    if (confirm("Are you sure you want to delete this vendor?"))
    {
      $('#delete_vendor_flag_' + vendor_idx).val(1);
      $('#vendor_area_' + vendor_idx).hide();
      $('#added_vendors_' + vendor_idx).hide();
    }
  }
}

function displayVendorDetails(vendor_idx_rownum,obj)
{

  var vendor_idx = $('#vendor_name_'+vendor_idx_rownum).val();
   
  if($('#added_vendors_' + vendor_idx).is(':visible')) $('#added_vendors_' + vendor_idx).hide();
  else
  { 
    var display_vendor_id = $('#display_vendor_id_' + vendor_idx).val();
    var my_vendor_id = $('#vendor_id_' + vendor_idx).val();
    if (true) 
    {
      $('#added_vendors_' + vendor_idx).html('');
      populateVendorDetails(vendor_idx_rownum,vendor_idx);
    }
    $('#added_vendors_' + vendor_idx_rownum).show();
  }
}

function selectType(index){

    var qurstionType = $('#question_type_'+index).val();

    if(qurstionType=='yes_or_no'){
        $('#yes_no_area_'+index).show();
        $('#answer_area_'+index+'_1').hide();
        $('#answer_section_'+index).hide();
    } else {
        if(qurstionType=='multiple_choice'){
            $('#yes_no_area_'+index).hide();
            $('#answer_area_'+index+'_1').show();
            $('#answer_section_'+index).show();
        } else {
            $('#yes_no_area_'+index).hide();
            $('#answer_area_'+index+'_1').hide();
            $('#answer_section_'+index).hide();

        }
    }


}

function expandChart(display_index)
{
    var show_index = display_index;
    var hide_index = 0;
    if (display_index % 2 != 0) hide_index = show_index - 1;
    else hide_index = show_index + 1;


    if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
    else
    {
        $('#chart_area_' + hide_index).hide(1000);
        $('#table_area_' + hide_index).hide(1000);

        $('#chart_area_' + show_index).show(1000);
        $('#table_area_' + show_index).show(1000);
    }
}

function collapseChart(display_index)
{
    var show_index = display_index;
    var hide_index = 0;
    if (display_index % 2 != 0) hide_index = show_index - 1;
    else hide_index = show_index + 1;

    $('#table_area_' + show_index).hide(1000);
    $('#table_area_' + hide_index).hide(1000);

    if (!$('#chart_area_' + show_index).is(':visible'))
        $('#chart_area_' + show_index).show(2000);
    if (!$('#chart_area_' + hide_index).is(':visible'))
        $('#chart_area_' + hide_index).show(2000);
}


function exportChart(quote_id,type_id)
{
    location = '<?php echo AppUrl::bicesUrl('quotes/exportChart/?quote_id='); ?>' + quote_id+'&type_id='+type_id;
}

/*var ctx = document.getElementById('chart');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["<?php echo $quote_stacked_bar_graph['vendors']; ?>"],
    datasets: [{data:[<?php echo $quote_stacked_bar_graph['datasets']; ?>]}]
  },
  options: {
    scales: {
      xAxes: [{ stacked: false }],
      yAxes: [{ stacked: true }]
    }
  }
});*/
 var colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];
        var options = {
             chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '45%',
                    //endingShape: 'rounded'  
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [<?php echo $quote_stacked_bar_graph['datasets']; ?>],
            xaxis: {
                categories: [<?php echo "'".implode("','",explode(',',$quote_stacked_bar_graph['vendors']))."'"; ?>],
                labels: {
                    style: {
                        colors: colors,
                        fontSize: '12px',
                    }
                }
            },
             /*yaxis: {
                title: {
                    text: '$ (thousands)'
                }
            },*/
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return /*"$ " +*/ val /*+ " thousands"*/
                    }
                }
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#chart"),
            options
        );

        chart.render();

/*var options = {
      chart: {
        height: 350,
        type: 'line',
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      series: [<?php echo $quote_answer_line_graph['vendor_score']?>],
      title: {
        text: '',
        align: 'left'
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      xaxis: {
        categories: [<?php echo $quote_answer_line_graph['answers']; ?>],
      },
      markers: {size: 5,hover: {size: 6},}
    }

    var chart = new ApexCharts(
      document.querySelector("#myChart"),
      options
    );

    chart.render();*/




    var options = {
      chart: {
        height: 200,
        type: 'heatmap',
      },
      stroke: {
        width: 0
      },
      plotOptions: {
        heatmap: {
          radius: 30,
          enableShades: false,
          shadeIntensity: 3,
          reverseNegativeShade: true,
          distributed: true,
          colorScale: {
            /*ranges: [{
                from: 0,
                to: 100,
                color: undefined
              },
              {
                from: 100,
                to: 10000,
                color: undefined
              },
            ],*/
          },

        }
      },
      dataLabels: {
        enabled: true,
        style: {
          colors: ['#fff']
        }
      },
      series: [<?php echo $heatMapSeries;?>],

      xaxis: {
        categories: [<?php echo "'".implode("','",$vendor)."'";?>],
        type: 'category',
      },
      
     /* title: {
        text: 'Currency '+<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']);?>;
      },*/

    }

    var chart = new ApexCharts(
      document.querySelector("#chartheatmap"),
      options
    );

    chart.render();

/*var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [<?php echo $quote_answer_line_graph['answers']; ?>],
        datasets: [{
            label: "% of Answers",
            // backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0,<?php echo $quote_answer_line_graph['scores']?>],
        }]
    },

    // Configuration options go here
     options: {
        scales: {
            xAxes: [{
                ticks: {
                    min: '0'
                }
            }]
        }
    }
});*/


</script>
<style type="text/css">
.apexcharts-tooltip-title {
    font-family: popin;
    font-size: 12px;
    background: #12A79C !important;
    color: #fff;
    font-family:Poppins !important;
  }
  div.dataTables_filter input {
    margin-left: 0.5em !important;
    padding: 0 !important;
}

.tile_count {
    margin-bottom: 10px;
    margin-top: 10px;
}
.apexcharts-yaxis-title,.apexcharts-xaxis-title { font-family: Poppins !important; }
.select2-hidden-accessible  {
    border-radius: 10px !important;
    border: 1px solid #4d90fe !important;
}

.modal.in .modal-dialog { width: 70%; }

</style>

