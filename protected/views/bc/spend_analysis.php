<?php
//echo "<pre>";
//print_r($spend_stats);
//echo "</pre>";
//die;
/*echo '<pre>';
print_r($metrics);
exit;*/
?>

<div class="row tile_count">
            <?php
            $sparklineData = '';
            if(count($order_spend_stats)>0)
            {

                foreach ($order_spend_stats as $order)
                {
                $sparklineData .= $order['total_price'].',';
                }
            }
            $sparklineData1 = '';
            if(count($order_spend_approve_stats)>0)
            {

                foreach ($order_spend_approve_stats as $order)
                {
                    $sparklineData1 .= $order['total_price'].',';
                }
            }
            $sparklineData2 = '';
            if(count($order_spend_vendor_stats)>0)
            {
            foreach ($order_spend_vendor_stats as $vendor)
                {
                    $sparklineData2 .= $vendor['total'].',';
                }

            }
            $sparklineData3 = '';
            if(count($order_spend_paid_stats)>0)
            {

            foreach ($order_spend_paid_stats as $order)
                {
                    $sparklineData3 .= $order['total_price'].',';
                }
            }
            ?>

    <div class="">
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#total_year_spend_modal').modal('show');"><div id="spark1"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#total_year_spend_modal').modal('show');"><div id="spark2"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#approved_spend_modal').modal('show');"><div id="spark3"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#approved_spend_modal').modal('show');"><div id="spark4"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#supplier_spend_modal').modal('show');"><div id="spark5"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#paid_spend_modal').modal('show');"><div id="spark6"></div></a>
        </div>
    </div>
</div>

		<div class="clearfix"><br /></div>
            <?php
            	for ($display_index = 1; $display_index <= 5; $display_index++)
            	{
            		$top_datasets = array();
					$canvas_id = "";

					if ($display_index == 1)
					{
						$top_datasets = $current_year;
		            	$canvas_id = 'year_to_date';
					}
					else if ($display_index == 2)
					{
	            		$top_datasets = $top_vendors;
		            	$canvas_id = 'spend_vendors';
					}
					/*
					else if ($display_index == 3)
					{
						$top_datasets = $top_departments;
		            	$canvas_id = 'spend_departments';
					}
					*/
					else if ($display_index == 3)
					{
						$top_datasets = $top_locations;
		            	$canvas_id = 'spend_locations';
					}
					else if ($display_index == 4)
					{
						$top_datasets = $year_over_year;
		            	$canvas_id = 'year_over_year';
					}
					else if ($display_index == 5)
					{
						$top_datasets = $top_contracts;
		            	$canvas_id = 'spend_contracts';
					}

					$canvas_header = str_replace('Spend ', 'Top ', ucwords(str_replace("_", " ", $canvas_id)));
					if ($display_index == 3) $canvas_header .= ' (Current Year)';
					$canvas_header = str_replace(' Over ', ' On ', $canvas_header);
					$canvas_header = str_replace(' Vendors', ' Suppliers', $canvas_header);
					if ($canvas_id == 'year_to_date') $canvas_header = 'Monthly Spend';

            ?>

		            <?php if ($display_index % 2 == 0) echo '<div class="row">'; ?>

						<?php if ($display_index != 1) { ?>
							<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_<?php echo $display_index; ?>">
						<?php } else { ?>
							<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_<?php echo $display_index; ?>">
						<?php  } ?>
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2><?php echo $canvas_header; ?></h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
				                  <?php if ($display_index != 1) { ?>
				                    <li><a onclick="expandChart(<?php echo $display_index; ?>);"><i class="fa fa-arrows-h"></i></a></li>
				                  <?php  } else{ ?>
									  <li><a onclick="expandChart1(<?php echo $display_index; ?>);"><i class="fa fa-arrows-h"></i></a></li>
								  <?php } ?>
				                  <li><a onclick="exportChart('<?php echo $canvas_id; ?>');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
	                      		<?php if ($canvas_id == 'year_to_date') { ?>
	                      			<canvas id="<?php echo $canvas_id; ?>" width="920px" height="200px"></canvas>
	                      		<?php } else { ?>
	                        		<canvas id="<?php echo $canvas_id; ?>" height="140px"></canvas>
	                        	<?php } ?>
	                        </div>
	                      </div>
	                   </div>

						<?php //if ($display_index != 1) { ?>
						<div <?php if ($display_index != 1) { ?> class="col-md-6 col-sm-6 col-xs-12" <?php } else { ?> class="col-md-12 col-sm-12 col-xs-12" <?php } ?> style="display: none;" id="table_area_<?php echo $display_index; ?>">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2><?php echo $canvas_header; ?></h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(<?php echo $display_index; ?>);"><i class="fa fa-arrows-h"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
					            			<?php
					            				$idx = 0;
					            				foreach ($top_datasets as $dataset_idx => $a_dataset)
												{
													if ($dataset_idx === 'total') continue;
													$idx += 1;
													switch ($idx)
													{
														case 1  : $color = 'blue'; break;
														case 2  : $color = 'purple'; break;
														case 3  : $color = 'green'; break;
														case 4  : $color = 'turquoise'; break;
														case 5  : $color = 'red'; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														case 10  : $color = 'blue'; break;
														case 11  : $color = 'purple'; break;
														case 12  : $color = 'green'; break;
														case 13  : $color = 'turquoise'; break;
														case 14  : $color = 'red'; break;
														case 15  : $color = 'antique-white'; break;
														case 16  : $color = 'aqua-marine'; break;
														case 17  : $color = 'bisque'; break;
														case 18  : $color = 'khaki'; break;
														case 19  : $color = 'blue'; break;
														case 20  : $color = 'purple'; break;
														case 21  : $color = 'green'; break;
														case 22  : $color = 'turquoise'; break;
														case 23  : $color = 'red'; break;
														case 24  : $color = 'antique-white'; break;
														case 25  : $color = 'aqua-marine'; break;
														case 26  : $color = 'bisque'; break;
														case 27  : $color = 'khaki'; break;
														case 28  : $color = 'blue'; break;
														case 29  : $color = 'purple'; break;
														case 30  : $color = 'green'; break;
														case 31  : $color = 'turquoise'; break;
														case 32  : $color = 'red'; break;
														case 33  : $color = 'antique-white'; break;
														case 34  : $color = 'aqua-marine'; break;
														case 35  : $color = 'bisque'; break;
														case 36  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
					            			?>
					                          		<tr>
					                            		<td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
					                            		<td style="vertical-align: middle;">
					                              			<?php echo $a_dataset['dim_1']; ?> - <?php echo number_format($a_dataset['percent'], 2) . '%'; ?>
					                            		</td>
					                          		</tr>
					                        <?php
					                        	}
					                        ?>
					                        </table>
			              </div>
			           </div>
			        </div>
			        <?php // } ?>

		            <?php if ($display_index % 2 == 1) echo '</div><div class="clearfix"> </div><div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>'; ?>

			<?php } ?>








<script>

                                    window.Apex = {
                                        stroke: {
                                            width: 3
                                        },
                                        markers: {
                                            size: 0
                                        },
                                        tooltip: {
                                            fixed: {
                                                enabled: true,
                                            }
                                        }
                                    };

                                    var randomizeArray = function (arg) {
                                        var array = arg.slice();
                                        var currentIndex = array.length,
                                            temporaryValue, randomIndex;

                                        while (0 !== currentIndex) {

                                            randomIndex = Math.floor(Math.random() * currentIndex);
                                            currentIndex -= 1;

                                            temporaryValue = array[currentIndex];
                                            array[currentIndex] = array[randomIndex];
                                            array[randomIndex] = temporaryValue;
                                        }

                                        return array;
                                    }
                                    // data for the sparklines that appear below header area
                                    var sparklineData = [47, 45, 54, 38, 56, 24, 65, 31, 37, 39, 62, 51, 35, 41, 35, 27, 93, 53, 61, 27, 54, 43, 19, 46];

                                    var spark1 = {
                                        chart: {
                                            type: 'area',
                                            height: 160,
                                            sparkline: {
                                                enabled: true
                                            },
                                        },
                                        stroke: {
                                            curve: 'straight'
                                        },
                                        fill: {
                                            opacity: 0.3,
                                        },
                                        series: [{
                                            data: randomizeArray([<?php echo rtrim($sparklineData); ?>])
                                        }],
                                        yaxis: {
                                            min: 0
                                        },
                                        title: {
                                            text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($metrics['spend']['spend_total'], 0); ?>',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '24px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        },
                                        subtitle: {
                                            text: 'Total Year Spend',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '14px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        }
                                    }

                                    var spark2 = {
                                        chart: {
                                            type: 'area',
                                            height: 160,
                                            sparkline: {
                                                enabled: true
                                            },
                                        },
                                        stroke: {
                                            curve: 'straight'
                                        },
                                        fill: {
                                            opacity: 0.3,
                                        },
                                        series: [{
                                            data: randomizeArray([<?php echo rtrim($sparklineData); ?>])
                                        }],
                                        yaxis: {
                                            min: 0
                                        },

                                        title: {
                                            text: '<?php echo number_format($metrics['spend']['spend_count'], 0); ?>',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '24px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        },
                                        subtitle: {
                                            text: 'Total Year Orders',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '14px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        }
                                    }

                                    var spark3 = {
                                        chart: {
                                            type: 'area',
                                            height: 160,
                                            sparkline: {
                                                enabled: true
                                            },
                                        },
                                        stroke: {
                                            curve: 'straight'
                                        },
                                        fill: {
                                            opacity: 0.3
                                        },
                                        series: [{
                                            data: randomizeArray([<?php echo rtrim($sparklineData1); ?>])
                                        }],
                                        xaxis: {
                                            crosshairs: {
                                                width: 1
                                            },
                                        },
                                        yaxis: {
                                            min: 0
                                        },

                                        title: {
                                            text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($metrics['spend']['spend_total_pending'], 0); ?>',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '24px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        },
                                        subtitle: {
                                            text: 'Spend To Approve',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '14px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        }
                                    }

                                    var spark4 = {
                                        chart: {
                                            type: 'area',
                                            height: 160,
                                            sparkline: {
                                                enabled: true
                                            },
                                        },
                                        stroke: {
                                            curve: 'straight'
                                        },
                                        fill: {
                                            opacity: 0.3
                                        },
                                        series: [{
                                            data: randomizeArray([<?php echo rtrim($sparklineData1); ?>])
                                        }],
                                        xaxis: {
                                            crosshairs: {
                                                width: 1
                                            },
                                        },
                                        yaxis: {
                                            min: 0
                                        },

                                        title: {
                                            text: '<?php echo  number_format($metrics['spend']['spend_count_pending'], 0); ?>',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '24px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        },
                                        subtitle: {
                                            text: 'Orders To Approve',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '14px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        }
                                    }

                                    var spark5 = {
                                        chart: {
                                            type: 'area',
                                            height: 160,
                                            sparkline: {
                                                enabled: true
                                            },
                                        },
                                        stroke: {
                                            curve: 'straight'
                                        },
                                        fill: {
                                            opacity: 0.3
                                        },
                                        series: [{
                                            data: randomizeArray([<?php echo rtrim($sparklineData2); ?>])
                                        }],
                                        xaxis: {
                                            crosshairs: {
                                                width: 1
                                            },
                                        },
                                        yaxis: {
                                            min: 0
                                        },
                                        title: {
                                            text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($metrics['vendor']['top_vendors_total'], 0); ?>',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '24px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        },
                                        subtitle: {
                                            text: 'Top 10 Supplier Spend',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '14px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        }
                                    }

                                    var spark6 = {
                                        chart: {
                                            type: 'area',
                                            height: 160,
                                            sparkline: {
                                                enabled: true
                                            },
                                        },
                                        stroke: {
                                            curve: 'straight'
                                        },
                                        fill: {
                                            opacity: 0.3
                                        },
                                        series: [{
                                            data: randomizeArray([<?php echo rtrim($sparklineData3); ?>])
                                        }],
                                        xaxis: {
                                            crosshairs: {
                                                width: 1
                                            },
                                        },
                                        yaxis: {
                                            min: 0
                                        },
                                        title: {
                                            text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($metrics['spend']['to_be_paid_count'], 0); ?>',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '24px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        },
                                        subtitle: {
                                            text: 'Orders To Be Paid',
                                            offsetX: 0,
                                            style: {
                                                fontSize: '14px',
                                                cssClass: 'apexcharts-yaxis-title'
                                            }
                                        }
                                    }

                                    var spark1 = new ApexCharts(document.querySelector("#spark1"), spark1);
                                    spark1.render();
                                    var spark2 = new ApexCharts(document.querySelector("#spark2"), spark2);
                                    spark2.render();
                                    var spark3 = new ApexCharts(document.querySelector("#spark3"), spark3);
                                    spark3.render();
                                    var spark4 = new ApexCharts(document.querySelector("#spark4"), spark4);
                                    spark4.render();
                                    var spark5 = new ApexCharts(document.querySelector("#spark5"), spark5);
                                    spark5.render();
                                    var spark6 = new ApexCharts(document.querySelector("#spark6"), spark6);
                                    spark6.render();

                                </script>