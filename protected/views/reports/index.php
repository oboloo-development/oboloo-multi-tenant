<?php 
	$dimension_names = array(); 
	$order_statuses = array( 'Pending', 'Approved', 'Cancelled','Closed','Submitted', 'Received', 'Paid', 'Invoice Received', 'Declined', 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier');
?>

<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Reports</h3>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>

	<form class="form-horizontal" role="form" method="post" name="report_form" id="report_form">
    <div class="form-group">
    	<div class="col-md-6 col-sm-6 col-xs-12">
        	<h4>Filters</h4>
		</div>
	</div>
    <div class="clearfix"> </div>

    	<div class="form-group">
    		<div class="col-md-9">
            <input type="text" class="form-control" name="vendor_name" id="vendor_name"
                    <?php if (isset($vendor_name) && !empty($vendor_name)) echo 'value="' . $vendor_name . '"'; else echo 'placeholder="Supplier Name"'; ?> >
            <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($vendor_id) && !empty($vendor_id)) echo $vendor_id; else echo '0'; ?>" />
			</div>
	    </div>

		<div class="form-group">
			<div class="col-md-3 location">
				<select name="location_id[]" id="location_id"  class="form-control" multiple searchable="Search here.."  onchange="loadDepartments(0);">
					<?php
					$i = 0 ;
					foreach ($locations as $location) {
						if(isset($location_id[$i])){
							?>
							<option value="<?php echo $location['location_id']; ?>"
								<?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
								<?php echo $location['location_name']; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $location['location_id']; ?>">
								<?php echo $location['location_name']; ?>
							</option>
						<?php } ?>
						<?php
						$i++;
					} ?>
				</select>
			</div>
			<div class="col-md-3">
				<select name="department_id" id="department_id" class="form-control">
					<option value="0">All Departments</option>
				</select>
			</div>
			<div class="col-md-3 status">

				<select name="order_status[]" id="order_status" multiple class="form-control">
					<?php
					$i = 0 ;
					foreach ($order_statuses as $order_status_display) {
						if ($order_status_display != 'Canceled' && $order_status_display != 'Declined') {
							if (isset($order_status[$i])) {
								?>
								<option value="<?php echo $order_status_display; ?>"
									<?php if ($order_status[$i] == $order_status_display) echo ' selected="SELECTED" '; ?>>
									<?php echo $order_status_display; ?>
								</option>
							<?php } else { ?>
								<option value="<?php echo $order_status_display; ?>">
									<?php echo $order_status_display; ?>
								</option>
							<?php }
							$i++;
						}
					}?>
				</select>
			</div>
		</div>

    <div class="form-group">
	    <div class="col-md-3 col-sm-3 col-xs-6">
	    	<input type="text" class="report_date_picker form-control" name="from_date" placeholder="Report From Date"
	        	id="from_date" <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . $from_date . '"'; ?> />
		</div>
	
	    <div class="col-md-3 col-sm-3 col-xs-6">
	    	<input type="text" class="report_date_picker form-control" name="to_date" placeholder="Report To Date"
	        	id="to_date" <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . $to_date . '"'; ?> />
		</div>
	    
	    <div class="col-md-3 col-sm-3 col-xs-6">
	    	<select class="form-control" name="report_type" id="report_type">
	    		<option value="A" <?php if (isset($report_type) && $report_type == 'A') echo ' selected="SELECTED "'; ?>>
	    			All
	    		</option>
	    		<option value="O" <?php if (isset($report_type) && $report_type == 'O') echo ' selected="SELECTED "'; ?>>
	    			Procurement
	    		</option>
	    		<option value="E" <?php if (isset($report_type) && $report_type == 'E') echo ' selected="SELECTED "'; ?>>
	    			Travel &amp; Expenses
	    		</option>
	    	</select>
		</div>

	    <div class="col-md-3 col-sm-3 col-xs-6">
	    	<button class="btn btn-default btn-primary" onclick="$('#report_form').submit();">
	    		Run Report
	    	</button>
		</div>
	</div>
    <div class="clearfix"> </div><br />

    <div class="form-group">
    	<div class="col-md-6 col-sm-6 col-xs-12">
        	<h4>Dimensions</h4>
		</div>
	</div>
    <div class="clearfix"> </div>

    <div class="form-group">
    	<?php $total_dimensions = 0; for ($i=1; $i<=4; $i++) { $var_name = 'dim_' . $i; ?>
		    <div class="col-md-3 col-sm-3 col-xs-6">
		    	<select name="dim_<?php echo $i; ?>" id="dim_<?php echo $i; ?>" class="form-control">
		    		<option value="">Run Report For</option>
		    		<option value="L" <?php if (isset($$var_name) && $$var_name == 'L') echo ' selected="SELECTED"; '; ?>>
		    			Locations
		    		</option>
		    		<option value="D" <?php if (isset($$var_name) && $$var_name == 'D') echo ' selected="SELECTED"; '; ?>>
		    			Departments
		    		</option>
		    		<option value="V" <?php if (isset($$var_name) && $$var_name == 'V') echo ' selected="SELECTED"; '; ?>>
		    			Suppliers
		    		</option>
		    		<option value="C" <?php if (isset($$var_name) && $$var_name == 'C') echo ' selected="SELECTED"; '; ?>>
		    			Categories
		    		</option>
		    		<option value="R" <?php if (isset($$var_name) && $$var_name == 'R') echo ' selected="SELECTED"; '; ?>>
		    			Subcategories
		    		</option>
		    		<option value="I" <?php if (isset($$var_name) && $$var_name == 'I') echo ' selected="SELECTED"; '; ?>>
		    			Industries
		    		</option>
		    		<option value="S" <?php if (isset($$var_name) && $$var_name == 'S') echo ' selected="SELECTED"; '; ?>>
		    			Subindustries
		    		</option>
		    		<option value="M" <?php if (isset($$var_name) && $$var_name == 'M') echo ' selected="SELECTED"; '; ?>>
		    			Months
		    		</option>
		    		<option value="Y" <?php if (isset($$var_name) && $$var_name == 'Y') echo ' selected="SELECTED"; '; ?>>
		    			Years
		    		</option>
		    	</select>
			</div>	
		<?php if (isset($$var_name) && !empty($$var_name)) $total_dimensions += 1; } ?>
	</div>
    <div class="clearfix"> </div>
    
	<input type="hidden" name="form_submitted" id="form_submitted" value="1" />
	</form>
    <div class="clearfix"> <br /> </div>

	<?php $selected_dimensions_ids = $selected_dimensions = $stacked_datasets = array(); if (isset($report_data) && is_array($report_data) && count($report_data)) { ?>

    <div class="form-group">
    	<div class="col-md-6 col-sm-6 col-xs-12">
        	<h4>Report Results</h4>
		</div>
	</div>
    <div class="clearfix"> <br /> </div>

	<div class="row">
    <table id="report_data_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <?php 
          	for ($i=1; $i<=4; $i++) 
          	{
          		$var_name = 'dim_' . $i;
				if (isset($$var_name) && !empty($$var_name))
				{ 
          ?>
          			<th>
          				<?php
          					switch ($$var_name)
							{
								case 'L' : $dimension_names[$i] = 'Location'; echo 'Location'; break;
								case 'D' : $dimension_names[$i] = 'Department'; echo 'Department'; break;
								case 'V' : $dimension_names[$i] = 'Vendor'; echo 'Supplier'; break;
								case 'C' : $dimension_names[$i] = 'Category'; echo 'Category Name'; break;
								case 'R' : $dimension_names[$i] = 'Subcategory'; echo 'Sub Category'; break;
								case 'I' : $dimension_names[$i] = 'Industry'; echo 'Industry Name'; break;
								case 'S' : $dimension_names[$i] = 'Subindustry'; echo 'Sub Industry'; break;
								case 'M' : $dimension_names[$i] = 'Month'; echo 'Month'; break;
								case 'Y' : $dimension_names[$i] = 'Year'; echo 'Year'; break;
								default  : $dimension_names[$i] = 'Unknown'; echo 'Unknown'; break;
							}
          				?> 
          			</th>
          <?php 
				}
			} 
		  ?>
          
          <th style="text-align: right;">Total Spent</th>
        </tr>
      </thead>

      <tbody>
      		<?php
      		foreach ($report_data as $idx => $report_row) { if ($idx === 'total') continue; ?>
      			
      			<tr>
			          <?php 
			          	$dataset_key = "";
			          	for ($i=1; $i<=4; $i++) 
			          	{
			          		$var_name = 'dim_' . $i;
							if (isset($$var_name) && !empty($$var_name))
							{
								echo '<td>';
								if (isset($report_row['dim_' . $i]) && !empty($report_row['dim_' . $i])) echo $report_row['dim_' . $i];
								echo '</td>';
								
								if (!isset($selected_dimensions[$i])) $selected_dimensions[$i] = array();
								if (!in_array($report_row['dim_' . $i], $selected_dimensions[$i]))
									 $selected_dimensions[$i][$report_row['dim_' . $i . '_id']] = $report_row['dim_' . $i];

								if (isset($report_row['dim_' . $i]) && !empty($report_row['dim_' . $i]))
								{
									if ($dataset_key == "") $dataset_key = $report_row['dim_' . $i];
									else $dataset_key = $dataset_key . "|" . $report_row['dim_' . $i]; 
								} 
							}
						} 
			          ?>
			          
			          <td style="text-align: right;">
			          	  <nobr>
				          	  <?php
					          	  	$stacked_datasets[$dataset_key] = 0; 
					          	  	if (isset($report_row['total'])) 
					          	  	{
					          	  		echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($report_row['total'], 2);
										$stacked_datasets[$dataset_key] = $report_row['total'];
									} 
				          	  ?>
			          	  </nobr>
			          </td>
      			</tr>
      			
      		<?php } ?>
	  </tbody>
    </table>
    <div class="clearfix"> </div>
    <?php 
	    	if ($total_dimensions != 2)
			{
	    		for ($i=1; $i<=4; $i++) 
	    		{
	    			$sort_this_dimension = true;
	    			$var_name = 'dim_' . $i;
					if (isset($$var_name) && $$var_name == 'M') 
						$sort_this_dimension = false;
					
	    			if ($sort_this_dimension && isset($selected_dimensions[$i])) asort($selected_dimensions[$i]);
				} 
			}
		}

	?>
	</div>

	<?php if (isset($report_data) && is_array($report_data) && (count($report_data) > '1')) { ?>

	    <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>
	
	    <?php if ($total_dimensions != 2) { ?>

	    <div class="form-group">
	    	<div class="col-md-6 col-sm-6 col-xs-12">
	        	<h4>Report Charts</h4>
			</div>
		</div>
	    <div class="clearfix"> <br /><br /><br /><br /> </div>
		
		<div class="row">
	       	<div class="col-md-4">
	       		<canvas id="report_chart" height="300px"></canvas>
			    <div id="chartjs-tooltip"></div>
	       	</div>		

	       	<div class="col-md-8">
				<?php
					$idx = 0; 
					foreach ($report_data as $dataset_idx => $a_dataset) 
					{ 
						$idx += 1;
						switch ($idx)
						{
							case 1  : $color = 'blue'; break;
							case 2  : $color = 'purple'; break;
							case 3  : $color = 'green'; break;
							case 4  : $color = 'turquoise'; break;
							case 5  : $color = 'red'; break;
							case 6  : $color = 'antique-white'; break;
							case 7  : $color = 'aqua-marine'; break;
							case 8  : $color = 'bisque'; break;
							case 9  : $color = 'khaki'; break;
							default : $color = 'salmon'; break;
						}
						
						if ($dataset_idx === 'total') $color = 'aero';

				?>
				
							<div class="col-md-12">
								<div class="col-md-4">
									<table style="width: 100%;">
									<tr>
									<td style="padding-left: 5px; text-align: right;">
									<i class="fa fa-lg fa-square <?php echo $color; ?>"></i>
									<span style="font-size: 120%;">
										<?php 
			          						for ($i=1; $i<=4; $i++) 
			          						{
			          							$var_name = 'dim_' . $i;
												if (isset($$var_name) && !empty($$var_name))
												{
													if (isset($a_dataset['dim_' . $i]) && !empty($a_dataset['dim_' . $i]))
													{
														if ($i != 1) echo ' - ';
														echo $a_dataset['dim_' . $i];
													} 
												}
											} 
										?>
									</span>
									</td>
									</tr>
									</table>
								</div>
								<div class="col-md-6">
				                    <div class="progress">
				                        <div class="progress-bar bg-green" role="progressbar" 
				                        	aria-valuenow="<?php echo number_format($a_dataset['percent'], 0); ?>" aria-valuemin="0" aria-valuemax="100" 
				                        		style="width: <?php echo number_format($a_dataset['percent'], 2); ?>%;">
			                        	</div>
		                        	</div>						
								</div>
								<div class="col-md-2 text-right" style="font-size: 130%;">
									<?php echo Yii::app()->session['user_currency_symbol'] . number_format($a_dataset['total'], 2); ?>
								</div>
							</div>
							<div class="clearfix"></div>

				<?php 
					} 
				?>
	       	</div>		
		</div>
	
	    <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>
			<div class="clearfix"> <br /> </div>
	    
	    <?php } else { ?>
			<div class="row tile_count">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel tile overflow_hidden">
					<div class="x_title">
						<h2>Stacked Charts (<?php echo $dimension_names[1] . ' Data ' . $dimension_names[2] . ' Wise'; ?>)</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<canvas id="stacked_chart" height="150px"></canvas>
					</div>
				</div>
			</div>

			    <div class="form-group">
			    	<div class="col-md-6 col-sm-6 col-xs-12">
			        	<h4>Pivot Charts</h4>
					</div>
			    	<div class="col-md-offset-5 col-md-1" style="float: right;">
			        	<button class="btn btn-primary" id="createPivot" name="createPivot" onclick="createPivotChart();">
			        		Chart It
			        	</button>
					</div>
				</div>
				<div class="clearfix"> <br /> </div>
			    <div class="form-group">
			    	<div class="col-md-6">
			    		<label>Select <?php echo $dimension_names[1]; ?></label>
		                <select multiple="multiple" name="dimension_1[]" id="dimension_1" class="form-control select2_multiple">
		                    <?php if(isset($selected_dimensions[1]) && !empty($selected_dimensions[1])){
		                    foreach ($selected_dimensions[1] as $sel_dim_1_id => $sel_dim_1) { ?>
		                        <option value="<?php echo $sel_dim_1_id; ?>">
		                            <?php echo $sel_dim_1; ?>
		                        </option>
		                    <?php 
		                          }
		                    }
		                    ?>
		                </select>
					</div>
			    	<div class="col-md-6">
			    		<label>Select <?php echo $dimension_names[2]; ?></label>
		                <select multiple="multiple" name="dimension_2[]" id="dimension_2" class="form-control select2_multiple">
		                    <?php if(isset($selected_dimensions[2]) && !empty($selected_dimensions[2])){
		                    foreach ($selected_dimensions[2] as $sel_dim_2_id => $sel_dim_2) { ?>
		                        <option value="<?php echo $sel_dim_2_id; ?>">
		                            <?php echo $sel_dim_2; ?>
		                        </option>
		                    <?php
                                 } 
		                    }
		                    ?>
		                </select>
					</div>
					<div class="clearfix"></div>
		    	</div>

			    <div class="clearfix"> <br /> </div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel tile overflow_hidden">
						<div class="x_title">
							<h2>Pivot Chart</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<canvas id="pivot_chart" height="150px"></canvas>
						</div>
					</div>
				</div>
		    </div>
	    	
	    <?php } ?>

	<?php } ?>

</div>
<style>
	.location  .multiselect {
	width: 208%;
	}
	.status  .multiselect {
		width: 198%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}

	.btn .caret {
		float: right;
		margin-top: 10px;

	}
</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions1(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Status Values',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}
	$('#location_id').multiselect(getOptions(true));
	$('#order_status').multiselect(getOptions1(true));


jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s.replace(' 2', ' 1, 2'));
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );

$(document).ready( function() {

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			loadDepartments(<?php echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>

    $('#vendor_name').devbridgeAutocomplete({
        serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
        onSelect: function(suggestion) { $('#vendor_id').val(suggestion.data); },
        onInvalidateSelection: function() { $('#vendor_id').val(0); },
        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#vendor_id').val(0); }
    });
	    
	$('.report_date_picker').daterangepicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$('#report_data_table').dataTable( 
		{ 
			"paging": false, "searching": false,
			columnDefs: [ 
				{ targets: [ 0 ], orderData: [ 0, 1 ] },
        		{ targets: [ 1 ], orderData: [ 1, 0 ] }
        		<?php 
        			if (in_array('Month', $dimension_names)) 
        			{
        				$dimension_index = 0;
						foreach ($dimension_names as $a_dimension_index => $a_selected_dimension)
						{
							if ($a_selected_dimension == 'Month')
							{
								$dimension_index = $a_dimension_index;
								break;
							}
						} 
						
						if ($dimension_index) 
						{
							$dimension_index -= 1;
        		?>
        					,{ "type": "sort-month-year", targets: <?php echo $dimension_index; ?> }
        		<?php 
						}
					} 
				?>        		 
        	],
			buttons: [ 
				{ extend: 'excelHtml5', filename: 'Report Data Export', "className": 'dt-button btn btn-success' },
				{ extend: 'pdfHtml5', filename: 'Report Data Export', "className": 'dt-button btn btn-danger' }
			],
			dom: 'frtipB'        	
		});
	
	$('#dimension_1').select2();
	$('#dimension_2').select2();

	<?php if ($total_dimensions != 2) { ?>

	var report_labels = [];
	var report_data = [];
	var report_colors = [];

	<?php 
		$i = 0; 
		foreach ($report_data as $idx => $a_report) 
		{
			if ($idx === 'total') continue; 

			$dimension_name = "";
			for ($j=1; $j<=4; $j++) 
			{
				$var_name = 'dim_' . $j;
				if (isset($$var_name) && !empty($$var_name))
				{
					if (isset($a_report['dim_' . $j]) && !empty($a_report['dim_' . $j]))
					{
						if ($j != 1) $dimension_name .= ' - ';
						$dimension_name .= $a_report['dim_' . $j];
					} 
				}
			} 

	?>
	
			report_labels[<?php echo $i; ?>] = "<?php echo addslashes($dimension_name); ?>";
			report_data[<?php echo $i; ?>] = <?php echo round($a_report['total'], 2); ?>;
	
	<?php 
			$i += 1; 

			$color = '';
			switch ($i)
			{
				case 1  : $color = '#3498DB'; break;
				case 2  : $color = '#9B59B6'; break;
				case 3  : $color = '#1ABB9C'; break;
				case 4  : $color = '#00CED1'; break;
				case 5  : $color = '#E74C3C'; break;
				case 6  : $color = '#FAEBD7'; break;
				case 7  : $color = '#7FFFD4'; break;
				case 8  : $color = '#FFE4C4'; break;
				case 9  : $color = '#BDB76B'; break;
				default : $color = '#FFA07A'; break;
			}
			
			echo 'report_colors[' . ($i - 1) . '] = "' . $color . '";';

		} 
	?>

		<?php if (isset($report_data) && is_array($report_data) && count($report_data)) { ?>
			createPieChart('report_chart', report_data, report_labels, report_colors);
		<?php } ?>
	
	<?php } else { ?>
		var stacked_chart_settings = {
			type: 'bar',
			tooltipFillColor: "rgba(51, 51, 51, 0.55)",
			data: {
				labels: [ 
							<?php $l = 1;
							if(isset($selected_dimensions[1]) && !empty($selected_dimensions[1])){
							foreach ($selected_dimensions[1] as $label) { ?>
								'<?php echo addslashes($label) ?>' <?php if ($l != count($selected_dimensions[1])) echo ','; ?>
							<?php $l += 1; } 
                            }
                        ?> 
					],
				datasets: [ 
				
						<?php
							$l = 1;
							if(isset($selected_dimensions[2]) && !empty($selected_dimensions[2])) {
							foreach ($selected_dimensions[2] as $label)
							{
								if ($l % 10 == 1) $backgroundColor = "rgba(220,220,220,0.5)";
								else if ($l % 10 == 2) $backgroundColor = "rgba(251,220,187,0.5)";
								else if ($l % 10 == 3) $backgroundColor = "rgba(187,220,151,0.5)";
								else if ($l % 10 == 4) $backgroundColor = "rgba(220,251,187,0.5)";
								else if ($l % 10 == 5) $backgroundColor = "rgba(220,187,151,0.5)";
								else if ($l % 10 == 6) $backgroundColor = "rgba(187,151,220,0.5)";
								else if ($l % 10 == 7) $backgroundColor = "rgba(120,151,187,0.5)";
								else if ($l % 10 == 8) $backgroundColor = "rgba(151,120,187,0.5)";
								else if ($l % 10 == 9) $backgroundColor = "rgba(187,120,151,0.5)";
								else if ($l % 10 == 0) $backgroundColor = "rgba(151,187,120,0.5)";
								else $backgroundColor = "rgba(151,187,205,0.5)";
						?>
						
								{
									label: '<?php echo addslashes($label); ?>',
									backgroundColor: "<?php echo $backgroundColor; ?>",
									data: [
										<?php
											$d = 1; 
											foreach ($selected_dimensions[1] as $main_label)
											{
												$dataset_key = $main_label . "|" . $label;
												$current_data = 0;
												if (isset($stacked_datasets[$dataset_key]))
													$current_data = round($stacked_datasets[$dataset_key], 2);
										?>
										
												<?php echo $current_data; ?> <?php if ($d != count($selected_dimensions[1])) echo ','; ?>
										
										<?php
												$d += 1;
											}
										?>
									]
								} <?php if ($l != count($selected_dimensions[2])) echo ','; ?>
								
						<?php
								$l += 1;
							}
                        }
						?>
				
					]
			},
			options: { 
				responsive: true,
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							var prefix = data.datasets[tooltipItem.datasetIndex].label;				
						    if (parseInt(value) >= 1000){
						    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						    } else {
						    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
						    }
						}						
					}
				},
	            scales: {
	            	xAxes: [{ stacked: true }],
	            	yAxes: [{ stacked: true, 
	            				ticks: 
	            					{ 
	            						beginAtZero: true, 
										callback: function(value, index, values) {
              								if (parseInt(value) >= 1000) {
                								return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              								} else {
                								return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
              								}	            	
	            						}
	            					} 
	            			}]
	            } 
			}
		};		
		
		var stacked_chart = new Chart($('#stacked_chart'), stacked_chart_settings);	
	<?php } ?>
	
});


function createPieChart(div_id, data_values, data_labels, data_colors)
{
		var chart_doughnut_settings = {
				type: 'doughnut',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: data_labels,
					datasets: [{
						data: data_values,
						backgroundColor: data_colors
					}]
				},
				options: { 
					legend: false, 
					responsive: true,
					tooltips: {
						callbacks: {
							label: function(tooltipItem, data) {
								var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								var prefix = data.labels[tooltipItem.index];
									
							    if (parseInt(value) >= 1000){
							    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							    } else {
							    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
							    }
							}						
						}
					}
				}
			}
		
			var chart_doughnut = new Chart( $('#' + div_id), chart_doughnut_settings);
		
}	

var pivot_chart_created = false;
var pivot_chart = false;

function createPivotChart()
{
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('reports/getPivotChartData'); ?>',
        type: 'POST', dataType: 'json', 
        data: { 
        		from_date: $('#from_date').val(), to_date: $('#to_date').val(),
        		dim_1: $('#dim_1').val(), dim_2: $('#dim_2').val(),
        		dimension_1: $('#dimension_1').val(), dimension_2: $('#dimension_2').val()
        	},
        success: function(pivot_chart_data) {
        	
        	var chart_data = new Object();
			var backgroundColor = "rgba(151,187,205,0.5)";
			
			for (var i=0; i<pivot_chart_data.secondary_labels.length; i++)
			{
				if (i == 1) backgroundColor = "rgba(220,220,220,0.5)";
				else if (i == 2) backgroundColor = "rgba(151,220,187,0.5)";
				else if (i == 3) backgroundColor = "rgba(187,220,151,0.5)";
				else if (i == 4) backgroundColor = "rgba(220,151,187,0.5)";
				else if (i == 5) backgroundColor = "rgba(220,187,151,0.5)";
				else if (i == 6) backgroundColor = "rgba(187,151,220,0.5)";
				else if (i == 7) backgroundColor = "rgba(120,151,187,0.5)";
				else if (i == 8) backgroundColor = "rgba(151,120,187,0.5)";
				else if (i == 9) backgroundColor = "rgba(187,120,151,0.5)";
				else if (i == 10) backgroundColor = "rgba(151,187,120,0.5)";
				else backgroundColor = "rgba(151,187,205,0.5)";

				chart_data[i] = new Object();
				chart_data[i].label = pivot_chart_data.secondary_labels[i];
				chart_data[i].data = pivot_chart_data.data[i];
				chart_data[i].backgroundColor = backgroundColor;
			}
			
			var pivot_chart_settings = {
				type: 'bar',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: pivot_chart_data.main_labels,
					datasets: chart_data
				},
				options: { 
					responsive: true,
					tooltips: {
						callbacks: {
							label: function(tooltipItem, data) {
								var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								var prefix = data.datasets[tooltipItem.datasetIndex].label;				
							    if (parseInt(value) >= 1000){
							    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							    } else {
							    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
							    }
							}						
						}
					},
		            scales: {
		            	yAxes: [{ ticks: 
		            				{ 
		            					beginAtZero: true, 
										callback: function(value, index, values) {
	              							if (parseInt(value) >= 1000) {
	                							return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              							} else {
	                							return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              							}	            	
		            					}
		            				} 
		            		}]
					}
				}
			};
			
			if (pivot_chart_created == true) pivot_chart.destroy();
			pivot_chart = new Chart($('#pivot_chart'), pivot_chart_settings);
			pivot_chart_created = true;		
		}
	});
}

</script>
