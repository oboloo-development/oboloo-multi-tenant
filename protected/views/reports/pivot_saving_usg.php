<?php $order_statuses = array( 'Pending', 'Approved', 'Canceled', 'Closed', 'Submitted', 'Paid', 'Invoice Received', 'Declined', 'Ordered - PO Sent To Supplier');

  $sql = 'SELECT * FROM saving_status' ;
  $savingStatus = Yii::app()->db->createCommand($sql)->query()->readAll();
  $savingsCurrency = "You can add more currencies under the configurations menu";
  $currency_rate = new CurrencyRate();
  $currency_rate = $currency_rate->getAllCurrencyRate();
?>
<?php  
    $userObj = new User();
    $usersList = $userObj->getAll(array('status' => 1,  'admin_flag' => 0, 'ORDER' => 'full_name', 'limit' => 100)); 
?>


<div class="right_col" role="main">
    <div class="">
        <div class="span6 pull-left">
            <h3>Report Builder</h3>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div> <br /><br />

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <h4 class="subheading">Filters<br /><br /></h4>
        </div>
    </div>
    <div class="clearfix"> </div>
    <div class="form-group">
            <div class="col-md-9">
             <label for="User Name">All Users</label>
              <select name="user_id" id="user_id" class="form-control select_internal notranslate">
                    <option value="">All Users</option>
                  <?php foreach($usersList as $value){?>
                    <option value="<?php echo $value['user_id'];?>" <?php if (isset($user_id) && $user_id ==$value['user_id']){?> selected <?php } ?>><?php echo $value['full_name'];?></option>
                  <?php }?>
              </select>
            </div>
        </div>
        <div class="clearfix"> </div><br />
        <div class="form-group">
            <div class="col-md-3">
                 <label for="Location"> Savings Oracle Location</label>
                <select name="location_id" id="location_id" class="form-control select_location_multiple" onchange="loadDepartments(0);">
                    <option value="0">All Savings Oracle Locations</option>
                    <?php foreach ($locations as $location) { ?>
                        <option value="<?php echo $location['location_id']; ?>"
                                <?php if (isset($location_id) && $location_id == $location['location_id']) echo ' selected="SELECTED" '; ?>>
                            <?php echo $location['location_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-3">
                 <label for="Department">Operating Unit</label>
                <select name="department_id" id="department_id" class="form-control select_department_multiple">
                    <option value="0">All Operating Units</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="Status"> All Status Values</label>
                <select name="order_status" id="order_status" class="form-control select_status_multiple">
                    <option value="">All Status Values</option>
                    <?php foreach ($savingStatus as $saving_status_display) { ?>
                        <option value="<?php echo $saving_status_display['id']; ?>">
                            <?php echo $saving_status_display['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    <div class="clearfix"> </div><br />
                   
    <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label for="Report From Date">Report From Date</label>
            <input type="text" class="form-control" name="from_date" placeholder="Report From Date"
                id="from_date" <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . $from_date . '"'; ?> />
        </div>
    
        <div class="col-md-3 col-sm-3 col-xs-6">
             <label for="Report To Date">Report To Date</label>
            <input type="text" class="form-control" name="to_date" placeholder="Report To Date"
                id="to_date" <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . $to_date . '"'; ?> />
        </div>
    <div class="col-md-3 col-sm-3 col-xs-6">
      <label for="Select Currency">Select Currency</label>
    <select name="currency_id" id="currency_id" class="form-control select_currency_multiple notranslate">
    <option value="">Select Currency</option>
    <?php foreach ($currency_rate as $rate) { ?>
    <option value="<?php echo $rate['id']; ?>" <?php if (isset(Yii::app()->session['user_currency_dropdown']) && Yii::app()->session['user_currency_dropdown'] == $rate['currency']) echo ' selected="SELECTED" '; ?>>
    <?php echo $rate['currency']; ?>
    </option>
    <?php } ?>
    </select>
  </div>
 
    </div>
    <div class="clearfix"> </div> <br />
     <div class="col-md-9 text-right">
        <!-- <a href="https://oboloo.com/rbuildertutorial" class="btn btn-success view-btn" target="_blank" style="border-radius: 30px !important;">Tutorial</a> -->
        <button class="btn btn-info" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
        <button class="btn btn-default btn-primary" id="run_report" onclick="getPivotData();" style="background-color: #1abb9c !important; border-color: #1abb9c !important;">
                Run Report
            </button>
    </div> <div class="clearfix"> </div> <br />
    <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>
    <div class="row">
        <!-- <div class="col-md-12">
            <div id="output" style="margin: 10px; border: 1px solid #c7c7c7;"></div>
        </div> -->
    <div class="clearfix"></div>
    <div class="col-md-12">
     <div id="wdr-component"></div>
   </div>
       <div class="clearfix"></div>
   <div class="clearfix"></div><br />
   <div class="col-md-12">
    <!-- <div id="highchartsContainer"></div> -->
   </div></div></div></div>
<div class="clearfix"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"></script>
<script type="text/javascript">
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre" : function ( s ) { return Date.parse(s.replace(' 2', ' 1, 2')); },
    "sort-month-year-asc" : function ( a, b ) {  return ((a < b) ? -1 : ((a > b) ? 1 : 0)); },
    "sort-month-year-desc": function ( a, b ) { return ((a < b) ? 1 : ((a > b) ?  -1 : 0)); }
} );

    $(document).ready(function() {

     $('#from_date').datetimepicker({ format: '<?php echo FunctionManager::dateFormatJS();?> '});
     $('#to_date').datetimepicker({ format: '<?php echo FunctionManager::dateFormatJS();?> '});
        <?php if (isset($location_id) && !empty($location_id)) { ?>
            <?php if (isset($department_id) && !empty($department_id)) { ?>
                loadDepartments(<?php echo $department_id; ?>);
            <?php } else { ?>
                loadDepartments(0);
            <?php } ?>
        <?php } ?>
/*
        $('#vendor_name').devbridgeAutocomplete({
            serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
            onSelect: function(suggestion) { $('#vendor_id').val(suggestion.data); },
            onInvalidateSelection: function() { $('#vendor_id').val(0); },
            onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#vendor_id').val(0); }
        });*/
            
        $('.report_date_picker').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_3",
          locale: {
            format: 'DD/MM/YYYY'
          }
        });
        getPivotData();
    });

function dateFormat(date){
    const d = new Date(date);
    // const date = new Date(2009, 10, 10);  // 2009-11-10
    // var options = { year: "numeric", month: "long"};
    // return d.toLocaleDateString("en", options);
    var momentObj = moment(d);
    var momentString = momentObj.format('YYYY/MM');
    return momentString;
}

function getPivotData()
    {
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('reports/getPivotSavingData'.$customSubDomain); ?>',
            type: 'POST', dataType: 'json', 
            data: {
                location_id  : $('#location_id').val(),
                department_id: $('#department_id').val(), 
                vendor_id    : $('#vendor_id').val(),
                order_status : $('#order_status').val(),
                from_date    : $('#from_date').val(), 
                to_date      : $('#to_date').val(),
                currency_id  : $('#currency_id').val(),
                user_id      : $('#user_id').val()
            },
            success: function(pivot_data) {
              
            var pivot_datap = pivot_data.data; 
            for (let i = 0; i < pivot_datap.length; i++) {
                pivot_datap[i].month = dateFormat(pivot_datap[i].month);
            }
           
            $(document).ready(function(){
              setInterval(function(){
              $('.wdr-sort-btns').addClass('notranslate');
                  let firstAZbtn = $('.wdr-sort-btn');
                  firstAZbtn.addClass('notranslate');
               });
            });
           
            var wdr_pivot_data = pivot_data;
            var pivot = new WebDataRocks({
            container: "#wdr-component",
            toolbar: true,
            report: {
                "dataSource": {
                "dataSourceType": "json",
                "data": wdr_pivot_data.data
                },
            slice: {
                rows: [
                /*{
                    uniqueName: "location"
                },*/
                {
                 "Operating Unit" : {
                    type: "string",
                 },
                },
                {
                  "Category" : {
                    type : "string"
                  },
                },
                {   
                 "month" : {
                   type : "date string"
                 },
                }
                ,{
                  uniqueName: "Measures" 
                },
                {
                 "Username" : {
                    type: "string",
                  },
                }
                ],
                columns: [
                {
                  uniqueName : "month"
                },
                {
                  "Savings Oracle Location" : {
                   type: "number",
                  },
                },
                {
                 "Sub Category" : {
                   type: "number",
                 },
                },
                {
                 "Realised Savings" : {
                   type: "number",
                  },
                },
                {
                  "Forecasted Savings"  : {
                   type: "number",
                  },
                },
                {
                  "Cost Reduction - Realized" : {
                   type: "number",
                  },
                },
                {
                  "Cost Reduction - Forecasted" : {
                    type: "number",
                  },
                },
                {
                  "Cost Avoidance - Realized" : {
                    type: "number",
                  },
                },
                {
                  "Cost Avoidance - Forecasted" : {
                    type: "number",
                  },
                },
                {
                  "Cost Containment - Realized" : {
                    type: "number",
                  },
                },
                {
                  "Cost Containment - Forecasted" : {
                   type: "number",
                  },
                },
                {
                  "Direct Materials" : {
                    type: "number",
                  },
                },
                {
                  "Indirect Materials" : {
                    type: "number",
                  },
                },
                {
                  "Capital/Mobile" : {
                    type: "number",
                  },
                },
                {
                  "Energy" : {
                    type: "number",
                  },
                },
                {
                 "Saving Initiative ID" : {
                   type:'number'
                 }
                },
                {
                 "Saving Initiative Name" : {
                    type: "string",
                  },
                }
                ],

                measures: [
                {
                  uniqueName:"Realised Savings"
                },
                {
                  uniqueName: "Planned Savings"
                },
                {
                  uniqueName:"Forecasted Savings"
                },
                {
                  "Cost Reduction - Realized" : {
                   type:'number'
                  }
                },
                {
                 "Cost Reduction - Forecasted" : {
                   type:'number'
                 }
                },
                {
                 "Cost Avoidance - Realized" : {
                   type:'number'
                  }
                },
                {
                 "Cost Avoidance - Forecasted" : {
                   type:'number'
                 }
                },
                {
                 "Cost Containment - Realized" : {
                  type:'number'
                 }
                },
                {
                 "Cost Containment - Forecasted" : {
                   type:'number'
                 }
                },           
                {
                 "Direct Materials" : {
                   type:'number'
                 }
                },
                {
                 "Indirect Materials" : {
                   type:'number'
                 }
                },
                {
                 "Capital/Mobile" : {
                  type:'number'
                 }
                },
                {
                 "Energy" : {
                   type:'number'
                 }
                },
                
                
            /* {
                    uniqueName: "total"
                }*//*,{
                    uniqueName: "project_total"
                }*/]
        },
       
    },
    reportcomplete: function() {
        pivot.off("reportcomplete");
       // createChart();
    }
});

 /*Highcharts.theme = {
    colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', 
             '#FF9655', '#FFF263', '#6AF9C4'],
    chart: {
        backgroundColor: {
            linearGradient: [0, 0, 500, 500],
            stops: [
                [0, '#fff'],
                [1, '#fff']
            ]
        },
    },

    title: {
        enabled: false
    },
    subtitle: {
        enabled: false
    },
    yAxis: {
         labels: {
                enabled: false
            },
            title: {
                text: ''
            },
       // min: 0,
     
    
    },
    legend: {
       enabled: false
    }
};
*/
// Apply the theme
//Highcharts.setOptions(Highcharts.theme);

/*function createChart() { 
    pivot.highcharts.getData({
        type: "column"
    }, function(data) {
        Highcharts.chart('highchartsContainer', data)
    }, function(data) {
        Highcharts.chart('highchartsContainer', data)
    });

}*/

        }
      });
    }

function clearFilter() {
  $('#from_date').val("");
  $('#to_date').val("");
  $("option:selected").removeAttr("selected");
  //$('#vendor_name').val("");
  //$('#vendor_name').trigger("change");
  $('.select_location_multiple').trigger("change");
  $('.select_department_multiple').trigger("change");
  $('.select_status_multiple').trigger("change");
  $('.select_currency_multiple').trigger("change");
  $('#run_report').trigger("click");
}
function select2function(className,lableTitle){
  var lableTitle = lableTitle;
  $("."+className).select2({
     // placeholder: lableTitle,
     /* allowClear: true*/
    });
}
$(document).ready(function(){
  select2function('select_location_multiple','All Locations');
  select2function('select_department_multiple','All Departments');
  select2function('select_status_multiple','All Statuses');
  select2function('select_currency_multiple','All currencies');
});


  
</script>
<link href="<?php echo AppUrl::jsUrl('webdatarocks/theme/teal/webdatarocks.min.css'); ?>" rel="stylesheet" />
<script src="<?php echo AppUrl::jsUrl('webdatarocks/webdatarocks.toolbar.min.js'); ?>"></script>
<script src="<?php echo AppUrl::jsUrl('webdatarocks/webdatarocks.js'); ?>"></script>
<!-- <script src="//code.highcharts.com/4.2.2/highcharts.js"></script>
<script src="//code.highcharts.com/4.2.2/highcharts-more.js"></script>
<script src="//cdn.webdatarocks.com/latest/webdatarocks.highcharts.js"></script> -->
<style>
 tspan.wdr-ui-element { display: none; }
 #wdr-pivot-view > span > a {display: none;}
 #wdr-tab-connect, #wdr-tab-open, #wdr-tab-save{display: none !important;}
 #wdr-tab-export {padding-left: 10px !important;}
 .wdr-sheet-selection-canvas { display: none !important;}
 .daterangepicker .calendar.single .calendar-table { width: auto !important;}
</style>
<script type="text/javascript">
//  webdatarocks.on("cellclick", function () { webdatarocks.removeSelection(); });
//  webdatarocks.on("celldoubleclick", function() { webdatarocks.removeSelection(); });
</script>