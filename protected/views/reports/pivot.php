<?php $order_statuses = array( 'Pending', 'Approved', 'Canceled', 'Closed', 'Submitted', 'Paid', 'Invoice Received', 'Declined', 'Ordered - PO Sent To Supplier'); ?>


<link href="<?php echo AppUrl::jsUrl('webdatarocks/theme/teal/webdatarocks.min.css'); ?>" rel="stylesheet" />
<script src="<?php echo AppUrl::jsUrl('webdatarocks/webdatarocks.toolbar.min.js'); ?>"></script>
<script src="<?php echo AppUrl::jsUrl('webdatarocks/webdatarocks.js'); ?>"></script>
<script src="//code.highcharts.com/4.2.2/highcharts.js"></script>
<script src="//code.highcharts.com/4.2.2/highcharts-more.js"></script>
<script src="//cdn.webdatarocks.com/latest/webdatarocks.highcharts.js"></script>

<div class="right_col" role="main">
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Pivot Report</h3>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>

    <div class="form-group">
    	<div class="col-md-6 col-sm-6 col-xs-12">
        	<h4>Filters</h4>
		</div>
	</div>
    <div class="clearfix"> </div>

    	<div class="form-group">
    		<div class="col-md-9">
            <input type="text" class="form-control" name="vendor_name" id="vendor_name"
                    <?php if (isset($vendor_name) && !empty($vendor_name)) echo 'value="' . $vendor_name . '"'; else echo 'placeholder="Supplier Name"'; ?> >
            <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($vendor_id) && !empty($vendor_id)) echo $vendor_id; else echo '0'; ?>" />
			</div>
	    </div>
	    <div class="clearfix"> </div><br />

		<div class="form-group">
			<div class="col-md-3">
				<select name="location_id" id="location_id" class="form-control" onchange="loadDepartments(0);">
					<option value="0">All Locations</option>
					<?php foreach ($locations as $location) { ?>
						<option value="<?php echo $location['location_id']; ?>"
								<?php if (isset($location_id) && $location_id == $location['location_id']) echo ' selected="SELECTED" '; ?>>
							<?php echo $location['location_name']; ?>
						</option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-3">
				<select name="department_id" id="department_id" class="form-control">
					<option value="0">All Departments</option>
				</select>
			</div>
			<div class="col-md-3">
				<select name="order_status" id="order_status" class="form-control">
					<option value="">All Status Values</option>
					<?php foreach ($order_statuses as $order_status_display) { ?>
						<option value="<?php echo $order_status_display; ?>"
								<?php if (isset($order_status) && $order_status == $order_status_display) echo ' selected="SELECTED" '; ?>>
							<?php echo $order_status_display; ?>
						</option>
					<?php } ?>
				</select>
			</div>
		</div>
    <div class="clearfix"> </div><br />

    <div class="form-group">
	    <div class="col-md-3 col-sm-3 col-xs-6">
	    	<input type="text" class="report_date_picker form-control" name="from_date" placeholder="Report From Date"
	        	id="from_date" <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . $from_date . '"'; ?> />
		</div>
	
	    <div class="col-md-3 col-sm-3 col-xs-6">
	    	<input type="text" class="report_date_picker form-control" name="to_date" placeholder="Report To Date"
	        	id="to_date" <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . $to_date . '"'; ?> />
		</div>
	    
	    <div class="col-md-3 col-sm-3 col-xs-6">
	    	<button class="btn btn-default btn-primary" onclick="getPivotData();">
	    		Run Report
	    	</button>
		</div>
	</div>
    <div class="clearfix"> </div> <br />

    <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

	<div class="row">
		<!-- <div class="col-md-12">
			<div id="output" style="margin: 10px; border: 1px solid #c7c7c7;"></div>
		</div> -->
    <div class="clearfix"></div>
    <div class="col-md-12">
    <table>
    <tr>
        <td style="width: 800px;">
            <div id="wdr-component"></div>
        </td>
        <td>
            <div id="highchartsContainer" style="width:500px;height:400px;"></div>
        </td>
    </tr>
</table></div></div></div>
<div class="clearfix"></div>

<script type="text/javascript">
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s.replace(' 2', ' 1, 2'));
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );

	$(document).ready(function() {

		<?php if (isset($location_id) && !empty($location_id)) { ?>
	
			<?php if (isset($department_id) && !empty($department_id)) { ?>
				loadDepartments(<?php echo $department_id; ?>);
			<?php } else { ?>
				loadDepartments(0);
			<?php } ?>
	
		<?php } ?>

	    $('#vendor_name').devbridgeAutocomplete({
	        serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
	        onSelect: function(suggestion) { $('#vendor_id').val(suggestion.data); },
	        onInvalidateSelection: function() { $('#vendor_id').val(0); },
	        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#vendor_id').val(0); }
	    });
		    
		$('.report_date_picker').daterangepicker({
		  singleDatePicker: true,
		  singleClasses: "picker_3",
		  locale: {
			format: 'DD/MM/YYYY'
		  }
		});
	
		getPivotData();

	});
	
	function getPivotData()
	{
	    $.ajax({
	        url: '<?php echo AppUrl::bicesUrl('reports/getPivotData'); ?>',
	        type: 'POST', dataType: 'json', 
	        data: {
	        	location_id: $('#location_id').val(),
	        	department_id: $('#department_id').val(), 
	        	vendor_id: $('#vendor_id').val(),
	        	order_status: $('#order_status').val(),
	        	from_date: $('#from_date').val(), 
	        	to_date: $('#to_date').val() 
	        },
	        success: function(pivot_data) {

			/*	var pivot_dataset = new Array();
				var sorted_timeframes = new Array();
				
				for (var i=0; i<pivot_data.data.length; i++)
				{
					var pivot_data_object = new Object();
					pivot_data_object.location = pivot_data.data[i].location;
					pivot_data_object.department = pivot_data.data[i].department;
					pivot_data_object.vendor = pivot_data.data[i].vendor;
					pivot_data_object.category = pivot_data.data[i].category;
					pivot_data_object.timeframe = pivot_data.data[i].timeframe;
					pivot_data_object.total = pivot_data.data[i].total;
					pivot_dataset[i] = pivot_data_object;
				}
				
				sorted_timeframes = pivot_data.sorted_timeframes;
	
		    	var renderers = $.extend(
	    	    	$.pivotUtilities.renderers,
	        	    $.pivotUtilities.c3_renderers,
	            	$.pivotUtilities.d3_renderers,
	            	$.pivotUtilities.export_renderers
	         	);

				$("#output").pivotUI(
						pivot_dataset, 
						{
							rows: ["location", "department"], 
							cols: ["vendor", "category", "timeframe"], 
							vals: ["total"], aggregatorName: "Sum", 
		                    sorters: { "timeframe": $.pivotUtilities.sortAs(sorted_timeframes) },
							renderers: renderers 
						}, 
					true);*/
			/*	var pivot = new WebDataRocks({
       					container: "#wdr-component",
        				toolbar: true,
        				report: {
            				dataSource: {
                				filename: "https://cdn.webdatarocks.com/data/data.csv"
            				}
        				}

    				});*/
    	var wdr_pivot_data = pivot_data;

    	var pivot = new WebDataRocks({
	    container: "#wdr-component",
	    toolbar: true,
	    report: {
	        "dataSource": {
	            "dataSourceType": "json",
	            "data": wdr_pivot_data.data
	        },
        slice: {
            rows: [{
                uniqueName: "location"
            },
            {
                uniqueName: "department"
            }
            , {
                uniqueName: "Measures"
            }],
            columns: [{
                uniqueName: "vendor"
            },

            {
                uniqueName: "category"
            },

            {
                uniqueName: "timeframe"
            }],
            measures: [{
                uniqueName: "total"
            }]
        }
    },
    reportcomplete: function() {
        pivot.off("reportcomplete");
        createChart();
    }
});

function createChart() { 
    pivot.highcharts.getData({
        type: "areaspline"
    }, function(data) {
        Highcharts.chart('highchartsContainer', data)
    }, function(data) {
        Highcharts.chart('highchartsContainer', data)
    });

}

function getJSONData() {
    return [{"location":"Amsterdam","department":"Finance","vendor":"360 Data Limited","category":"Food & Drink","timeframe":"Jun 2019","total":40916.36},{"location":"London","department":"Procurement","vendor":"360 Data Limited","category":"Hotel","timeframe":"Jun 2019","total":1220},{"location":"London","department":"Procurement","vendor":"360 Data Limited","category":"Food","timeframe":"Jun 2019","total":120},{"location":"London","department":"Procurement","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Jun 2019","total":3100},{"location":"London","department":"Procurement","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Sep 2018","total":15.45},{"location":"London","department":"HR","vendor":"360 Data Limited","category":"Hotel","timeframe":"Jun 2019","total":1220},{"location":"London","department":"HR","vendor":"360 Data Limited","category":"Food","timeframe":"Jun 2019","total":120},{"location":"London","department":"HR","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Jun 2019","total":3100},{"location":"London","department":"HR","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Sep 2018","total":15.45},{"location":"London","department":"Finance","vendor":"360 Data Limited","category":"Hotel","timeframe":"Jun 2019","total":4880},{"location":"London","department":"Finance","vendor":"360 Data Limited","category":"Food","timeframe":"Jun 2019","total":480},{"location":"London","department":"Finance","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Jun 2019","total":12400},{"location":"London","department":"Finance","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Sep 2018","total":61.8},{"location":"Paris Office","department":"Sales","vendor":"360 Data Limited","category":"Hotel","timeframe":"Jun 2019","total":1220},{"location":"Paris Office","department":"Sales","vendor":"360 Data Limited","category":"Food","timeframe":"Jun 2019","total":120},{"location":"Paris Office","department":"Sales","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Jun 2019","total":3100},{"location":"Paris Office","department":"Sales","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Sep 2018","total":15.45},{"location":"New York V","department":"Procurement","vendor":"360 Data Limited","category":"Hotel","timeframe":"Jun 2019","total":1220},{"location":"New York V","department":"Procurement","vendor":"360 Data Limited","category":"Food","timeframe":"Jun 2019","total":120},{"location":"New York V","department":"Procurement","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Jun 2019","total":3100},{"location":"New York V","department":"Procurement","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Sep 2018","total":15.45},{"location":"New York V","department":"IT","vendor":"360 Data Limited","category":"Hotel","timeframe":"Jun 2019","total":1220},{"location":"New York V","department":"IT","vendor":"360 Data Limited","category":"Food","timeframe":"Jun 2019","total":120},{"location":"New York V","department":"IT","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Jun 2019","total":3100},{"location":"New York V","department":"IT","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Sep 2018","total":15.45},{"location":"Amsterdam","department":"Finance","vendor":"360 Data Limited","category":"Hotel","timeframe":"Jun 2019","total":4880},{"location":"Amsterdam","department":"Finance","vendor":"360 Data Limited","category":"Food","timeframe":"Jun 2019","total":480},{"location":"Amsterdam","department":"Finance","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Jun 2019","total":12400},{"location":"Amsterdam","department":"Finance","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Sep 2018","total":61.8},{"location":"Frankfurt","department":"Commercial","vendor":"360 Data Limited","category":"Hotel","timeframe":"Jun 2019","total":1220},{"location":"Frankfurt","department":"Commercial","vendor":"360 Data Limited","category":"Food","timeframe":"Jun 2019","total":120},{"location":"Frankfurt","department":"Commercial","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Jun 2019","total":3100},{"location":"Frankfurt","department":"Commercial","vendor":"360 Data Limited","category":"Rental Car","timeframe":"Sep 2018","total":15.45}]
}
			}
		});
	}
</script>
