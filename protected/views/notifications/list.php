<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left ">
            <h3>Notifications</h3>
        </div>
        <div class="clearfix"> </div>
    </div>

	<form class="form-horizontal mt-26" id="notification_list_form" name="notification_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('notifications/list'); ?>">
		<div class="form-group">
          <div class="col-md-2">
              <input type="text" class="form-control" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date("d/m/Y", strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> autocomplete="off">
          </div>
			
          <div class="col-md-2">
              <input type="text" class="form-control" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date("d/m/Y", strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> autocomplete="off">
          </div>

          <div class="col-md-2">
              <select name="notification_filter" class="form-control" id="notification_filter">
                <option value=""> Notification type</option>
                <option value="sourcing" <?= !empty($notification_type) && $notification_type == 'sourcing' ? 'selected':''; ?>>Sourcing</option>
                <option value="contract" <?= !empty($notification_type) && $notification_type == 'contract' ? 'selected':''; ?>>Contract</option>
                <option value="supplier" <?= !empty($notification_type) && $notification_type == 'supplier' ? 'selected':''; ?>>Supplier</option>
                <option value="savings"  <?= !empty($notification_type) && $notification_type == 'savings'  ? 'selected':''; ?>>Savings</option>
              </select>    
          </div>

			<div class="col-md-3">
				<button class="btn btn-primary" onclick="$('#notification_list_form').submit();">Search Notifications</button>
			</div>

			<div class="col-md-2">
			</div>
          <?php $disabled = FunctionManager::sandbox(); ?>
            <div class="col-md-3 text-right">
				<button class="btn btn-danger" onclick="deleteNotifications(); return false;"  <?php echo $disabled; ?>>Delete Selected</button>
            </div>
		</div>
	</form>
<div class="clearfix"> <br /><br /> </div>


    <table id="notifications_table" class="table table-striped table-bordered" style="wdith:100%;">
      <thead>
        <tr>
          <th>ID</th>
          <th>Date</th>
          <th>Notification</th>
          <th><input type="checkbox"  id="checkboxforAll" class="form-control" value="" title="Check All"> Delete</th>

        </tr>


        
      </thead>

      <tbody>

          <?php /* $str = '<a href="http://dev.spend-365.com/orders/edit/73">Order ID #73</a> has been declined by Rubin Wilson';
                $start = strpos($str,"<a");
                $end   = strpos($str,"</a>");
                echo substr_replace($str,'',$start,$end);
                echo "<br />";

                $str = 'More information has been requested by Rubin Wilson for <a href="http://dev.spend-365.com/orders/edit/71">Order ID #71</a>';
                $start = strpos($str,"<a");
                $end   = strpos($str,"</a>");
                echo substr_replace($str,'',$start,$end);*/

                
                ?>
          <?php 

          $notificationLabelArr = array();
          foreach ($notifications as $notification) { 
               /* $str = $notification['notification_text'];
                $start = strpos($str,"<a");
                $end   = strpos($str,"</a>");
                $parsedStr = substr_replace($str,'',$start,$end);
                $notificationLabelArr[$parsedStr] = $parsedStr;*/
            ?>

              <tr>
                    <td><?php echo $notification['id']; ?></td>
                    <td><?php echo date("F j, Y h:iA", strtotime($notification['notification_date'])); ?></td>
                    <td>
                    <?php 
                      if(strtolower($notification['notification_type']) == 'contract'){
                        $btnStyle="background-color: #fdaaaa;border-color: #fdaaaa;font-size:10px;margin-top: 14px;";
                        $btnText = "Contract";
                      }
                      elseif(strtolower($notification['notification_type']) ==  'supplier status' || strtolower($notification['notification_type'])=='vendor'){ 
                      $btnStyle="background-color: #fec98f;border-color: #fec98f;font-size:10px;margin-top: 14px;";
                      $btnText = "Supplier";
                      }elseif(strtolower($notification['notification_type']) ==  'saving milestone'){
                      $btnStyle="background-color: #98ddfc;border-color: #98ddfc;font-size:10px;margin-top: 14px;";
                      $btnText = "Savings";
                      }elseif(strtolower($notification['notification_type']) ==  'quote' || strtolower($notification['notification_type']) ==  'quote and not home'){ 
                      $btnStyle="background-color: #a0e7a0;border-color: #a0e7a0;font-size:10px;margin-top: 14px;";
                      $btnText = "eSourcing";
                       }else{ 
                        $btnStyle="";
                        $btnText = "Other";
                        } ?>
                         <button class="btn btn-success" style="<?php echo $btnStyle; ?>"><?php echo $btnText;?></button>
                    <?php echo $notification['notification_text']; ?>
                  </td>
                    <td><input class="delete_notification_checkbox allcheckbox" type="checkbox" name="delete_notification_id[]" value="<?php echo $notification['id']; ?>" /></td>
              </tr>

          <?php } ?>

      </tbody>

  </table>
  <?php //echo "<pre>"; print_r($notificationLabelArr);exit;?>

</div>


<div class="modal fade" id="delete_notifications_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Delete Notifications</h4>
        </div>
        <div class="modal-body">
        	<p>Are you sure you want to delete the selected notifications? This action cannot be reversed.</p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-info btn btn-default" data-dismiss="modal">
          	  No
          </button>
          <button id="yes_delete" type="button" class="alert-success btn btn-default">
          	  Yes
          </button>
        </div>
      </div>
	</div>
</div>


<div class="modal fade" id="nothing_selected_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Delete Notifications</h4>
        </div>
        <div class="modal-body">
          <p>You must select at least one notification to delete.</p>
        </div>
        <div class="modal-footer">
          <button id="ok_invoice_message" type="button" class="alert-info btn btn-default" data-dismiss="modal">
          	  Ok
          </button>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s.replace('AM', ':00').replace('PM', ':00'));
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );


$.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
	locale: {
	    format: 'DD/MM/YYYY'
	}
  });
};

$(document).ready( function() {
    $('#notifications_table').dataTable({
        "columnDefs": [ 
        	{ "targets": 0, "orderable": false },
			{ "type": "sort-month-year", targets: 1 }         
        ],
        "order": []
    });

	// $('#from_date').singleDatePicker({
	//   singleDatePicker: true,
	//   singleClasses: "picker_3",
	//   locale: {
	// 	format: 'DD/MM/YYYY'
	//   }
	// });

  $('#from_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'});
  $('#to_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?>'});

	// $('#to_date').singleDatePicker({
	//   singleDatePicker: true,
	//   singleClasses: "picker_3",
	//   locale: {
	// 	format: 'DD/MM/YYYY'
	//   }
	// });

	$('#delete_notifications_modal .modal-footer button').on('click', function(event) {
	    var button = event.target; // The clicked button
	    if (button.id == 'yes_delete') {
		  	var selected_notifications_for_delete = "";
		  	$('.delete_notification_checkbox').each(function() {
				if ($(this).is(':checked'))
				{
					if (selected_notifications_for_delete == "")
						selected_notifications_for_delete = $(this).val();
					else
						selected_notifications_for_delete = selected_notifications_for_delete + ',' + $(this).val();
				}
		  	});
		  	window.location = '<?php echo AppUrl::bicesUrl('notifications/deleteNotifications/?notification_ids_to_delete=') ?>' + selected_notifications_for_delete;
	    }
	    else $('.delete_notification_checkbox').each(function() { $(this).removeAttr('checked'); });
   });

});

function deleteNotifications()
{
	var selected_notifications_for_delete = 0;
	$('.delete_notification_checkbox').each(function() {
		if ($(this).is(':checked')) selected_notifications_for_delete = selected_notifications_for_delete + 1;
	});
	
	if (selected_notifications_for_delete == 0) $('#nothing_selected_modal').modal('show');
	else $('#delete_notifications_modal').modal('show');
}

$("#checkboxforAll" ).change(function() {
  if($(this).is(':checked')){
    $(".allcheckbox").prop('checked', true);
  }else{
     $(".allcheckbox").prop('checked', false);
  }
});
</script>
<style>
  .form-horizontal .form-group{
    margin-right: -15px;
    margin-left:-15px;
}
</style>