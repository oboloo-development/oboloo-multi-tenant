<?php $invoice_statuses = array( 'Pending', 'Approved', 'Canceled', 'Closed', 'Submitted', 'Paid', 'Invoice Received', 'Declined', 'Ordered - PO Sent To Supplier'); ?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Invoices</h3>
        </div>

        <div class="clearfix"> </div>
    </div>

	<form class="form-horizontal" id="invoice_list_form" name="invoice_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('invoices/list'); ?>">
		<div class="form-group">
          <div class="col-md-2">
              <input type="text" class="form-control" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date("d/m/Y", strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> >
          </div>
			
          <div class="col-md-2">
              <input type="text" class="form-control" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date("d/m/Y", strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> >
          </div>

			<div class="col-md-2 location">
				<select name="location_id[]" id="location_id" class="form-control" multiple onchange="loadDepartments(0);">
					<?php 
					$i = 0 ;
					foreach ($locations as $location) { 

						if(isset($location_id[$i])){
                        ?>
						<option value="<?php echo $location['location_id']; ?>"
								<?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
							<?php echo $location['location_name']; ?>
						</option>
                        <?php } else { ?>
                            <option value="<?php echo $location['location_id']; ?>">
                                <?php echo $location['location_name']; ?>
                            </option>
                        <?php } ?>
					<?php
                    $i++;}?>
				</select>
			</div>

			<div class="col-md-2 department">
				<select name="department_id[]" id="department_id" class="form-control" multiple searchable="Search here..">
					<option value="0">All Departments</option>
					<?php if(!empty($department_info))
                        foreach($department_info as $dept_value){?>
                        <option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
                    <?php } ?>
				</select>
			</div>
			<div class="col-md-2 status">
				<select name="order_status[]" id="order_status" multiple class="form-control">
					<?php
					$i = 0 ;
					foreach ($invoice_statuses as $invoice_status_display) {
						if(isset($order_status[$i])){
							?>
							<option value="<?php echo $invoice_status_display; ?>"
								<?php if ($order_status[$i] == $invoice_status_display) echo ' selected="SELECTED" '; ?>>
								<?php echo $invoice_status_display; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $invoice_status_display;?>">
								<?php echo $invoice_status_display; ?>
							</option>
						<?php }
						$i++;
					} ?>
				</select>
			</div>

			<div class="col-md-2">
				<button class="btn btn-primary" onclick="$('#invoice_list_form').submit();">Search Invoices</button>
			</div>
		</div>
	</form>
<div class="clearfix"> </div>
<div class="row tile_count"> </div>

    <table id="invoice_table" class="table table-striped table-bordered">
      <thead>
        <tr>
		  <th> </th>
		  <th>Invoice Number</th>
          <th>Order ID</th>
          <th>Invoice Date</th>
          <th>Supplier</th>
          <th>Order Status</th>
          <th>Invoice Files</th>
        </tr>
      </thead>

      <tbody>

          <?php $order = new Order();
            foreach ($invoices as $invoice) { ?>
              <tr>
                    <td style="text-align: right;">
                    	<a style="text-decoration: underline;" target="_procurement_order"
                    			href="<?php echo AppUrl::bicesUrl('orders/edit/' . $invoice['order_id']); ?>"> <button class="btn btn-sm btn-success">View</button>
                    	</a>
                    </td>
                    <td><?php echo $invoice['inv_number']; ?></td>
				    <td><?php echo $invoice['order_id']; ?></td>
				    <td><?php echo date("F j, Y", strtotime($invoice['inv_created_at']));?></td>
				    
                    <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $invoice['vendor_id']); ?>"><?php echo $invoice['vendor']; ?></a></td>
                    <td>
                    	<?php
                    		if (empty($invoice['order_status'])) $invoice['order_status'] = 'Pending';
                    		$status_style = "";
							
							if ($invoice['order_status'] == 'Paid' || $invoice['order_status'] == 'Closed' || $invoice['order_status'] == 'Received')
								$status_style = ' style="border: 1px solid green !important; background-color: green !important; color: #fff !important;" ';
							if ($invoice['order_status'] == 'Declined')
								$status_style = ' style="border: 1px solid #990000 !important; background-color: #990000 !important; color: #fff !important;" ';								
							if ($invoice['order_status'] == 'More Info Needed')
								$status_style = ' style="border: 1px solid #CC3333 !important; background-color: #CC3333 !important; color: #fff !important;" ';								
							if ($invoice['order_status'] == 'Pending' || $invoice['order_status'] == 'Ordered - PO Not Sent To Supplier' 
										|| $invoice['order_status'] == 'Submitted' || $invoice['order_status'] == 'Ordered - PO Sent To Supplier')
								$status_style = ' style="border: 1px solid orange !important; background-color: orange !important; color: #fff !important;" ';

							if($invoice['order_status'] == 'Pending') {
								$toolTip = "Invoice has been created by " . $invoice['user'] . " however not submitted for approval.";
							}
							elseif($invoice['order_status'] == 'Submitted'){
								$toolTip = "Invoice has been submitted  by ".$invoice['user']." and is awaiting approval.";
							}
							elseif($invoice['order_status'] == 'More Info Needed'){
								$toolTip = "Approver has requested more information from the order creator.";
							}
							elseif($invoice['order_status'] == 'Declined'){
								$toolTip = "Approver has declined the order. Please read approver notes.";
							}
							elseif($invoice['order_status'] == 'Approved'){
								$toolTip = "Invoice has been approved and awaiting Purchase Order creation.";
							}
							elseif($invoice['order_status'] == 'Ordered - PO Not Sent To Supplier'){
								$toolTip = "Purchase Order created but not sent to supplier.";
							}
							elseif($invoice['order_status'] == 'Ordered - PO Sent To Supplier'){
								$toolTip = "Purchase Order created and sent to supplier.";
							}
							elseif($invoice['order_status'] == 'Invoice Received'){
								$toolTip = "Invoice has been received by supplier and awaiting payment.";
							}
							elseif($invoice['order_status'] == 'Paid'){
								$toolTip = "Invoice has been paid .";
							}
							elseif($invoice['order_status'] == 'Received'){
								$toolTip = "Order has been received.";
							}
							elseif($invoice['order_status'] == 'Cancelled'){
								$toolTip = "Invoice has been cancelled.";
							}
							elseif($invoice['order_status'] == 'Closed'){
								$toolTip = "Invoice has been closed.";
							}
							else{
								$toolTip = 'N/A';
							}
						?>
                    	<button class="btn btn-sm btn-success status" title="<?php echo $toolTip; ?>" <?php echo $status_style; ?>>
                    		<?php echo $invoice['order_status']; ?>
                    	</button>
                    </td>
                    <td>
						<?php
							if(!empty($invoice['inv_id'])){
							$order_invoice = $order->orderConsolidateInvoice($invoice['inv_number'],$invoice['order_id']);
							foreach ($order_invoice as $uploaded_file) {
								$invoice_idx = $uploaded_file['id'];
								$showFileName = substr($uploaded_file['file_name'], strpos($uploaded_file['file_name'], "_") + 1);
								echo '<a href="'.Yii::app()->createAbsoluteUrl('orders/invoiceDownload',array('invoice-id'=>$invoice_idx,'order-id'=>$uploaded_file['order_id'])).'" target="order_invoice">'.$showFileName.'</a><br />';
							} }
						?>                   	
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>
<style>
	.ui-tooltip {
		width: 200px;
		text-align: center;
		box-shadow: none;
		padding: 0;
	}
	.ui-tooltip-content {
		position: relative;
		padding: 0.5em;
	}
	.ui-tooltip-content::after, .ui-tooltip-content::before {
		content: "";
		position: absolute;
		border-style: solid;
		display: block;
		left: 90px;
	}
	.bottom .ui-tooltip-content::before {
		bottom: -10px;
		border-color: #AAA transparent;
		border-width: 10px 10px 0;
	}
	.bottom .ui-tooltip-content::after {
		bottom: -7px;
		border-color: white transparent;
		border-width: 10px 10px 0;
	}
	.top .ui-tooltip-content::before {
		top: -10px;
		border-color: #AAA transparent;
		border-width: 0 10px 10px;
	}
	.top .ui-tooltip-content::after {
		top: -7px;
		border-color: white transparent;
		border-width: 0 10px 10px;
	}

	.location  .multiselect {
		width: 138%;
	}
	.status  .multiselect {
		width: 100%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}

	.btn .caret {
		float: right;
		margin-top: 10px;

	}
    .department .dropdown-toggle{
        min-width: 112%;
    }

</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}


	 function getOptions2(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Departments',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }

	function getOptions1(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Status Values',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}
	$('#location_id').multiselect(getOptions(true));
	$('#department_id').multiselect(getOptions2(true));
	$('#order_status').multiselect(getOptions1(true));

    $.fn.singleDatePicker = function() {
    $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
    });
    return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
	locale: {
	    format: 'DD/MM/YYYY'
	}
  });
};

$(document).ready( function() {
    $('#invoice_table').dataTable({
        "columnDefs": [ 
            { "targets": 0, "width": "6%", "orderable": false },
            { "type": "date", targets: 3}         
        ],
        "order": [[3,'desc']],
        "language": {"search": "Search:"}
    });

	$('#from_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$('#to_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$( ".status" ).tooltip({
		position: {
			my: "center bottom",
			at: "center top-10",
			collision: "flip",
			using: function( position, feedback ) {
				$( this ).addClass( feedback.vertical )
					.css( position );
			}
		}
	});

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			//loadDepartments(<?php //echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>
	
});
</script>
