<?php $user_id = Yii::app()->session['user_id'];
  $history = new SavingApprovalHistory();
  $historyRecord = $history->getHistroy($saving['id']);
  $validateHistory = new ValidateSavingApprovalHistory();
  $validateHistoryRecord = $validateHistory->getHistroy($saving['id']);
  $validateDate =   $validateHistory->getApprovedDate($saving['id']);
  $savingObj = new SavingUsg();
?>
<div id="mysupplierStatusModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Savings Status</h4>
      </div>
      <div class="modal-body" style="padding-bottom:20px;">
       <?php if(!empty($sourc_usg) && $sourc_usg == 'sourc_usg') { ?> 
        <form action="<?php echo AppUrl::bicesUrl('strategicSourcing/savingApprove'); ?>" method="post">
       <?php } else{?>
        <form action="<?php echo AppUrl::bicesUrl('savingsUsg/savingApprove'); ?>" method="post">
       <?php } ?>
           <label>Approver <span style="color: #a94442;">*</span> </label>
            <?php  $sql = " SELECT user_id,full_name FROM `users` where admin_flag !=1 order by full_name asc";
            $getUsers = Yii::app()->db->createCommand($sql)->queryAll(); 
             ?>
            <div class="form-group">
            <select  class="form-control select_user notranslate" name="approver_id" id="approver_id" style="border-radius: 10px;border: 1px solid #4d90fe;" required>
              <option value="">Select User</option>
                <?php foreach ($getUsers as $all_user) { ?>
                <option value="<?php echo $all_user['user_id'];?>" <?php 
                   if (!empty($saving['approver_id']) && $saving['approver_id'] == $all_user['user_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $all_user['full_name']; ?>
                </option>
                <?php } ?>
           </select>
          </div>
          <div class="form-group">
           <label>Status <span style="color: #a94442;">*</span> </label>
           <?php 
       
           $onChange = '';
           if($saving['approver_id'] != $user_id){
               $onChange = 'onchange="checkSavingApprover(this.value)"';
              
           }?>
            <select name="status" id="status" class="form-control notranslate" <?php echo $onChange;?> required="required">
              <option value="">Select Status</option>
                <option value="In Review" <?php if($saving['approver_status'] =='In Review') echo 'selected="selected"'; ?>>In Review</option>
                <option value="Sent For Approval" <?php if($saving['approver_status'] =='Sent For Approval') echo 'selected="selected"'; ?>>Sent For Approval</option>
                <option value="Approved" <?php if($saving['approver_status'] =='Approved') echo 'selected="selected"'; ?>>Approved</option>
                <option value="Rejected" <?php if($saving['approver_status'] =='Rejected') echo 'selected="selected"'; ?>>Rejected</option>
          </select>
            
          </div>
          <div class="form-group" style=" margin-top: 20px;">
            <label>Comment</label>
            <textarea class="form-control notranslate" name="approver_comment"><?php echo !empty($vendor['approver_comment'])?$vendor['approver_comment']:'';?></textarea>
          </div>
          <input type="hidden" name="saving_id" value="<?php echo $saving['id']; ?>">
          <div class="form-group" style=" margin-top: 20px;">
          <label>Date</label>
          <input type="text" class="form-control" value="<?php echo
              !empty($vendor['approver_datetime'])?date(FunctionManager::dateFormat().' H:i:s',strtotime($vendor['approver_datetime'])):date('d/m/Y H:i:s');?>" readonly />
           <input type="hidden" class="form-control" name="approver_datetime" value="<?php echo
              !empty($vendor['approver_datetime'])?date('Y-m-d H:i:s',strtotime($vendor['approver_datetime'])):date('Y-m-d H:i:s');?>" readonly />
          </div><br>
          <div class="col-md-12">
              <?php 
              $history = new SavingApprovalHistory();
              $historyRecord = $history->getHistroy($saving['id']);
              if(!empty($historyRecord)) { ?>
          <table id="" class="table table-striped table-bordered" style="width:100%;">
           <thead>
            <tr>
               <th style="width:50%">Comments</th>
               <th style="width:20%">User Name</th>
               <th style="width:30%">Date and time</th>
            </tr>
             </thead>
             <tbody>

            <?php foreach ($historyRecord as $value) { ?>
            <tr>
               <td><?php echo $value['approver_comment']; ?></td>
               <td class="notranslate"><?php echo $value['user_name']; ?></td>
               <td><?php echo
              !empty($value['created_at'])?date(FunctionManager::dateFormat().' H:i:s',strtotime($value['created_at'])):date('d/m/Y H:i:s');?></td>
            </tr>
          <?php } ?>
            </tbody>
          </table>
        <?php } ?>
          </div>
          <div class="form-group" id="select_send_for_approvel_only" style=" margin-top: 20px;">
            <button type="submit" class="btn btn-primary">Save</button>
             
          </div>
          </form>
        </div>


      </div>
      
    </div>
  </div>

  <div id="validateSavingsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Validate Savings Status</h4>
      </div>
      <div class="modal-body" style="padding-bottom:20px;">

        <form action="<?php echo AppUrl::bicesUrl('savingsUsg/savingValidate'); ?>" method="post">
           <label>Approver <span style="color: #a94442;">*</span> </label>
            <?php  $sql = " SELECT user_id,full_name FROM `users` where admin_flag !=1 order by full_name asc";
            $getUsers = Yii::app()->db->createCommand($sql)->queryAll(); 
             ?>
            <div class="form-group">
            <select  class="form-control select_user notranslate" name="validate_approver_id" id="validate_approver_id" style="border-radius: 10px;border: 1px solid #4d90fe;" required>
              <option value="">Select User</option>
                <?php foreach ($getUsers as $all_user) { ?>
                <option value="<?php echo $all_user['user_id'];?>" <?php 
                   if (!empty($saving['validate_approver_id']) && $saving['validate_approver_id'] == $all_user['user_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $all_user['full_name']; ?>
                </option>
                <?php } ?>
           </select>
          </div>

          <?php 
           $onChange = '';
           if($saving['validate_approver_id'] != $user_id){
               $onChange = 'onchange="checkSavingValidateApprover(this.value)"';
              
            }?>
          <div class="form-group">
           <label>Status <span style="color: #a94442;">*</span> </label>
           
            <select name="validate_approver_status" id="validate_approver_status" class="form-control notranslate" onchange="checkSavingValidateApprover(this.value, <?php echo $user_id;?>)" required="required">
              <option value="">Select Status</option>
                <option value="Sent For Approval" <?php if($saving['validate_approver_status'] =='Sent For Approval') echo 'selected="selected"'; ?>>Sent For Approval</option>
                <option value="Approved" <?php if($saving['validate_approver_status'] =='Approved') echo 'selected="selected"'; ?>>Approved</option>
                <option value="Rejected" <?php if($saving['validate_approver_status'] =='Rejected') echo 'selected="selected"'; ?>>Rejected</option>
          </select>
            
          </div>
          <div class="form-group" style=" margin-top: 20px;">
            <label>Comment</label>
            <textarea class="form-control notranslate" name="approver_comment"></textarea>
          </div>
          <input type="hidden" name="saving_id" value="<?php echo $saving['id']; ?>">
          <div class="form-group" style=" margin-top: 20px;">
          <label>Date</label>
          <input type="text" class="form-control" value="<?php echo date('d/m/Y H:i:s');?>" readonly />

           <input type="hidden" class="form-control" name="approver_datetime" value="<?php date('Y-m-d H:i:s');?>" readonly />
          </div><br>
          <div class="col-md-12">
              <?php 
             
              if(!empty($validateHistoryRecord)) { ?>
          <table id="" class="table table-striped table-bordered" style="width:100%;">
           <thead>
            <tr>
               <th style="width:50%">Comments</th>
               <th style="width:20%">User Name</th>
               <th style="width:30%">Date and time</th>
            </tr>
             </thead>
             <tbody>

            <?php foreach ($validateHistoryRecord as $value) { ?>
            <tr>
               <td><?php echo $value['approver_comment']; ?></td>
               <td class="notranslate"><?php echo $value['user_name']; ?></td>
               <td><?php echo 
              !empty($value['created_at'])?date(FunctionManager::dateFormat().' H:i:s',strtotime($value['created_at'])):date('d/m/Y H:i:s');?></td>
            </tr>
          <?php } ?>
            </tbody>
          </table>
        <?php } ?>
          </div>
          <div class="form-group" id="validate_select_send_for_approvel_only" style=" margin-top: 20px;">
            <button type="submit" class="btn btn-primary">Save</button>
             
          </div> 
          </form>
        </div>


      </div>
      
    </div>
  </div>
 
<?php  if($saving['approver_id'] != $user_id){?>
<script type="text/javascript">
  $(document).ready(function(){
    $('#select_send_for_approvel_only').hide();
     /* $('#approver_status').on('change ',function() {

        if (this.value == 'Sent For Approval') {
            $('#select_send_for_approvel_only').show();
        } else if (this.value != 'Sent For Approval') {
            $('#select_send_for_approvel_only').hide();
        }
      });*/
  });
</script>
<?php } ?>

<?php  if($saving['approver_id'] != $user_id){?>
<script type="text/javascript">
  $(document).ready(function(){
    $('#select_send_for_approvel_only').hide();
     /* $('#approver_status').on('change ',function() {

        if (this.value == 'Sent For Approval') {
            $('#select_send_for_approvel_only').show();
        } else if (this.value != 'Sent For Approval') {
            $('#select_send_for_approvel_only').hide();
        }
      });*/
  });
</script>
<?php } ?>
<?php  if($saving['validate_approver_id'] != $user_id){?>
<script type="text/javascript">
  $(document).ready(function(){
    $('#validate_select_send_for_approvel_only').hide();
  });
</script>
<?php } ?>
<script type="text/javascript">
  
  $(document).ready(function(){
    $("#validate_approver_status").trigger("change");
    $("#validate_approver_id").change(function(){
      $("#validate_approver_status").trigger("change");
    });
  });
 
  function checkSavingApprover(approvalstatus){
   if (approvalstatus == 'Sent For Approval' || approvalstatus == 'In Review') {
        $('#select_send_for_approvel_only').show();
    } else if (approvalstatus != 'Sent For Approval') {
        $('#select_send_for_approvel_only').hide();
    }
 }
 function checkSavingValidateApprover(approvalstatus,userID){
    var approverid = $("#validate_approver_id option:selected").val();
   if (approvalstatus == 'Sent For Approval' || approvalstatus == 'In Review') {
        $('#validate_select_send_for_approvel_only').show();
    } else if (approvalstatus == 'Approved' && approverid ==userID) {
        $('#validate_select_send_for_approvel_only').show();
    }else if (approvalstatus == 'Rejected' && approverid ==userID) {
        $('#validate_select_send_for_approvel_only').show();
    }else {
        $('#validate_select_send_for_approvel_only').hide();
    }
 }
</script>