<?php $domainUrl = Yii::app()->getBaseUrl(true);
$metrix1Title = "Realized Savings";
$metrix2Title = "<br> Forecasted Savings";
$metrix3Title = "Planned Savings";
$metrix4Title = "  <br>Total Incremental Savings";
$mPadd        = "";

$dateFormate = FunctionManager::dateFormat();
if(!empty($_POST['report_from_date'])) {
  $reportFromDate = $_POST['report_from_date'];
}else {
  $reportFromDate = "";
}

if(!empty($_POST['report_to_date'])) {
  $reportToDate = $_POST['report_to_date'];
}else {
  $reportToDate = "";
}

if(!empty($_POST['report_of_location'])) {
  $reportLocation = $_POST['report_of_location'];
}else {
  $reportLocation = "";
}

$report = new UsgReport();
$statusCount = $report->savingCountSatus($chartDateFrom, $chartDateTo);
$areaCount = $report->savingCountArea();
$areaAndType = $report->savingAreaAndType($chartDateFrom, $chartDateTo);
$savingAmountByType = $report->savingAmountBySavingType($chartDateFrom, $chartDateTo);

$amountArea = $report->savingAmountByArea($chartDateFrom, $chartDateTo);
if(isset($_POST['report_from_date']) && isset($_POST['report_to_date'])) {
  if(FunctionManager::dateFormat() == "d/m/Y") {
    $monthStart = date("Y-m-01", strtotime(strtr($_POST['report_from_date'], '/', '-')));
    $monthEnd = date("Y-m-t", strtotime(strtr($_POST['report_to_date'], '/', '-')));
  }else{
    $monthStart = date("Y-m-01", strtotime($_POST['report_from_date']));
    $monthEnd = date("Y-m-t", strtotime($_POST['report_to_date']));
  }
}else{
  $monthStart = date("Y-m-01");
  $monthEnd = date("Y-m-t", strtotime("+11 months"));
}
$areaPlannedRealized = $report->areaPlannedRealizedGraph($monthStart, $monthEnd);
$currencySymbol = Yii::app()->session['user_currency_symbol'];

$statusCountAll = $statusCountActive = $statusCountCompleted = $statusCountPlanned = 0;
foreach ($statusCount as $statusVal) {

  if(!empty($statusVal['All'])) {
    $statusCountAll += count($statusVal['All']);
  }

  if(!empty($statusVal['Planned'])) {
    $statusCountPlanned += count($statusVal['Planned']);
  }

  if(!empty($statusVal['Completed'])) {
    $statusCountCompleted += count($statusVal['Completed']);
  }

  if(!empty($statusVal['Active'])) {
    $statusCountActive += count($statusVal['Active']);
  }
}

$Direct_Materials = $Energy = $Indirect_Materials = $Capital_Mobile = 0;
foreach ($areaCount as $areaVal) {

  if (!empty($areaVal['Direct_Materials'])) {
    $Direct_Materials += count($areaVal['Direct_Materials']);
  }

  if (!empty($areaVal['Energy'])) {
    $Energy += count($areaVal['Energy']);
  }

  if (!empty($areaVal['Indirect_Materials'])) {
    $Indirect_Materials += count($areaVal['Indirect_Materials']);
  }
  if (!empty($areaVal['Capital_Mobile'])) {
    $Capital_Mobile += count($areaVal['Capital_Mobile']);
  }
}

$query = "SELECT * FROM departments  ORDER BY department_name";
$getDepartment = Yii::app()->db->createCommand($query)->query()->readAll();

?>

<div class="right_col" role="main">
  <!-- <div class="quote-tutorial"></div> -->
  <div class="col-md-5" style=""></div>
  <div class="col-md-3 highlight-create" style=""></div>
  <div class="col-md-4" style=""></div>

  <div class="row-fluid tile_count">
    <div class="span6 pull-left col-md-5">
      <h3>Savings Management</h3>
    </div>


    <div class="span6 pull-right saving-range">
      <div class="clearfix"></div>
      <div class="saving-range-currency-highlight"></div>
      <div class="clearfix"><br></div>
      <!-- // From to Date To  -->
      <div class="pull-right">
        <form id="savings_due_date" method="post" class="saving-highlight hidden-xs" action="<?php echo AppUrl::bicesUrl('savingslist'); ?>" autocomplete="off">
          <div class=" form-inline" style="margin-right: -4px; margin-top: -5px; position: relative">
            <span style="font-size: 11px; font-weight: bold; margin-right: 8px;">Date Range</span>
            <input type="text" class="form-control notranslate saving_from_date" name="report_from_date" id="saving_from_date" placeholder="From Date" value="<?php echo $reportFromDate; ?>" />
            <input type="text" class="form-control notranslate saving_to_date " name="report_to_date" placeholder="To Date" value="<?php echo $reportToDate; ?>" />
            <a href="<?php echo $this->createUrl('/savingslist'); ?>" class="btn btn-info search-quote" style="border-color: #46b8da;margin-left: 7px;">Clear Search</a>
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
        </form>
      </div>
      <div class="clearfix"><br></div>
    </div>
  </div>
  <div class="clearfix"> </div>
  <?php $savingCreated = Yii::app()->user->getFlash('saving_message');
  if (!empty($savingCreated)) { ?>
    <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $savingCreated; ?></div>
  <?php } ?>

  <div class="row-fluid tile_count">
    <div class="container">
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats1 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice">
          <?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrix_1); ?> <br><?php echo $metrix1Title; ?>
        </h4>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats2 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?>">
          <?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrix_2 - $metrix_1); ?><br>
          <?php echo $metrix3Title; ?>
        </h4>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats3 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?>">
          <?php echo Yii::app()->session['user_currency_symbol'] . number_format($metrix_2); ?>
          <?php echo $metrix2Title; ?>  
        </h4>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats4 mb-0">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?>">
          <?php echo number_format($metrix_4, 0); ?>
          <?php echo $metrix4Title; ?>
        </h4>
      </div>
    </div>
  </div>
   <div class="row-fluid tile_count">
    <?php $this->renderPartial('/savingsUsg/_saving_amount_saving_type_graph', array("savingAmountByType" => $savingAmountByType)); ?>
  </div>
  <div class="panel panel-metrix usg_bg_banner_title">
    <div class="panel-body text-center">Savings Initiatives</div>
  </div>
  <div class="row-fluid tile_count">
    <div class="container">
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats1 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice"><?php echo  $statusCountAll; ?> <br>Total Savings Initiatives</h4>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats2 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?>">
          <?php echo $statusCountPlanned; ?><br />
          Planned Initiatives
        </h4>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats3 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?>">
          <?php echo $statusCountCompleted; ?><br>Completed
        </h4>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats4 mb-0">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?>">
          <?php echo $statusCountActive; ?><br />Active
        </h4>
      </div>
    </div>
  </div>

  <div class="panel panel-metrix usg_bg_banner_title">
    <div class="panel-body text-center">Savings By Category</div>
  </div>
  <div class="row-fluid tile_count">
    <div class="container">
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats1 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice"><?php echo  $Direct_Materials; ?> <br>Direct Materials</h4>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats1 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?>">
          <?php echo $Indirect_Materials; ?><br>
          Indirect Materials
        </h4>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats4 mb-0">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?> ">
          <?php echo $Capital_Mobile; ?><br />
          Capital/Mobile
        </h4>
      </div>

      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats4 mb-0" style="margin-right: 20px;">
        <h4 class="text-center contract-metrice <?php echo $mPadd; ?>"><?php echo $Energy; ?><br />Energy
        </h4>
      </div>
    </div>
  </div>

  <div class="clear-fix"></div>
  <div class="row" style="zoom:85%; margin-left: -13px !important; margin-right: -17px !important;">
    <!-- Start: Saving Type Direct Materials -->
    <div class="mb-0 x_panel area-categ-graph ">
      <div class="x_title">
        <h2>Direct Materials Planned Savings</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="saving_cat1"></div>
      </div>
    </div>
    <div class="mb-0 x_panel area-categ-graph">
      <div class="x_title">
        <h2>Indirect Materials Planned Savings</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="saving_cat3"></div>
      </div>
    </div>
    <div class="mb-0 x_panel area-categ-graph">
      <div class="x_title">
        <h2>Capital/Mobile Planned Savings</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="saving_cat4"></div>
      </div>
    </div>
    <div class="mb-0 x_panel area-categ-graph">
      <div class="x_title">
        <h2>Energy Planned Savings</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="saving_cat2"></div>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table id="saving_amount_table" class="table table-striped table-bordered savings-table" style="width: 100%;margin-top: 20px;">
      <thead>
        <tr>
          <th class="th-center">Savings Area</th>
          <th class="th-center">Realized Savings</th>
          <th class="th-center">Planned Savings</th>
          <th class="th-center">Forecasted Savings</th>
          <th class="th-center">Allocation By Group</th>
        </tr>
      </thead>
      <?php
      $dPlannig = !empty($amountArea['P_Direct_Materials']) ? $amountArea['P_Direct_Materials'] : 0;
      $iPlannig = !empty($amountArea['P_Indirect_Materials']) ? $amountArea['P_Indirect_Materials'] : 0;
      $ePlannig = !empty($amountArea['P_Energy']) ? $amountArea['P_Energy'] : 0;
      $mPlannig = !empty($amountArea['P_Capital_Mobile']) ? $amountArea['P_Capital_Mobile'] : 0;

       $class = 'class="text-red"';
       // row 1 when any number occur subtract symbol, these value auto color change to red
       $row1Col1 = $amountArea['R_Direct_Materials'] < 0 ? $class : '';
       $row1Col2 = ($amountArea['P_Direct_Materials'] - $amountArea['R_Direct_Materials']) < 0 ? $class : '';
       $row1Col3 = $amountArea['P_Direct_Materials'] < 0 ? $class : '';
       $row1Col4 = ($dPlannig / $amountArea['grand_total'] * 100)  < 0  ? $class : '';
       // row 2
       $row2Col1 = $amountArea['R_Indirect_Materials'] < 0 ? $class : '';
       $row2Col2 = ($amountArea['P_Indirect_Materials'] - $amountArea['R_Indirect_Materials']) < 0 ? $class : '';
       $row2Col3 = $amountArea['P_Indirect_Materials'] < 0 ? $class : '';
       $row2Col4 = ($iPlannig / $amountArea['grand_total'] * 100)  < 0  ? $class : '';
       // row 3
       $row3Col1 = $amountArea['R_Capital_Mobile'] < 0 ? $class : '';
       $row3Col2 = ($amountArea['P_Capital_Mobile'] - $amountArea['R_Capital_Mobile']) < 0 ? $class : '';
       $row3Col3 = $amountArea['P_Capital_Mobile'] < 0 ? $class : '';
       $row3Col4 = ($mPlannig / $amountArea['grand_total'] * 100)  < 0  ? $class : '';
       // row 4
       $row4Col1  = $amountArea['R_Energy'] < 0 ? $class : '';
       $row4Col2 = ($amountArea['P_Energy'] - $amountArea['R_Energy']) < 0 ? $class : '';
       $row4Col3 = $amountArea['P_Energy'] < 0 ? $class : '';
       $row4Col4 = ($ePlannig / $amountArea['grand_total'] * 100)  < 0  ? $class : '';

       $tRealized = $amountArea['R_Indirect_Materials'] + $amountArea['R_Capital_Mobile'] + $amountArea['R_Energy'] + $amountArea['R_Direct_Materials'];
       // row 5
       $row5Col1 = $tRealized < 0 ? $class : '';
       $row5Col2 = ($eoyi + $eoye + $eoyd + $eoym) < 0 ? $class : '';
       $row5Col3 = ($amountArea['P_Indirect_Materials'] + $amountArea['P_Capital_Mobile'] + 
                   $amountArea['P_Energy'] + $amountArea['P_Direct_Materials']) < 0 ? $class : '';
       $row5Col4 = (($dPlannig + $iPlannig + $ePlannig + $mPlannig) / $amountArea['grand_total'] * 100)  < 0  ? $class : '';
       ?>
       <tr>
       <tr>
        <td>Direct Materials</td>
        <td align="right" <?= $row1Col1 ?>><?php echo $currencySymbol . number_format($amountArea['R_Direct_Materials']); ?></td>
        <td align="right" <?= $row1Col2 ?>>
          <?php $eoyd = $amountArea['P_Direct_Materials'] - $amountArea['R_Direct_Materials'];
                echo $currencySymbol . number_format($eoyd); ?>
        </td>
        <td align="right" <?= $row1Col3 ?>><?php echo $currencySymbol . number_format($amountArea['P_Direct_Materials']); ?></td>
        <td align="right" <?= $row1Col4 ?>><?php echo $amountArea['grand_total'] > 0 && $dPlannig > 0 ? 
                              number_format($dPlannig / $amountArea['grand_total'] * 100) . '%' : ''; ?></td>
       </tr>
       <tr>
        <td>Indirect Materials</td>
        <td align="right" <?= $row2Col1 ?>><?php echo $currencySymbol . number_format($amountArea['R_Indirect_Materials']); ?></td>
        <td align="right" <?= $row2Col2 ?>><?php
                          $eoyi = $amountArea['P_Indirect_Materials'] - $amountArea['R_Indirect_Materials'];
                          echo $currencySymbol . number_format($eoyi); ?></td>
        <td align="right" <?= $row2Col3 ?>><?php echo $currencySymbol . number_format($amountArea['P_Indirect_Materials']); ?></td>
        <td align="right" <?= $row2Col4 ?>><?php echo $amountArea['grand_total'] > 0 && $iPlannig > 0 ? number_format($iPlannig / $amountArea['grand_total'] * 100) . '%' : ''; ?></td>
       </tr>
       <tr>
        <td>Capital/Mobile</td>
        <td align="right" <?= $row3Col1 ?>><?php echo $currencySymbol . number_format($amountArea['R_Capital_Mobile']); ?></td>
        <td align="right" <?= $row3Col2 ?>><?php $eoym = $amountArea['P_Capital_Mobile'] - $amountArea['R_Capital_Mobile'];
                          echo $currencySymbol . number_format($eoym); ?></td>
        <td align="right" <?= $row3Col3 ?>><?php echo $currencySymbol . number_format($amountArea['P_Capital_Mobile']); ?></td>
        <td align="right" <?= $row3Col4 ?>><?php echo $amountArea['grand_total'] > 0 && $mPlannig > 0 ? number_format($mPlannig / $amountArea['grand_total'] * 100) . '%' : ''; ?></td>
       </tr>
       <tr>
        <td>Energy</td>
        <td align="right" <?= $row4Col1 ?>><?php echo $currencySymbol . number_format($amountArea['R_Energy']); ?></td>
        <td align="right" <?= $row4Col2 ?>>
          <?php
            $eoye = $amountArea['P_Energy'] - $amountArea['R_Energy'];
            echo $currencySymbol . number_format($eoye); ?></td>
        <td align="right" <?= $row4Col3 ?>><?php echo $currencySymbol . number_format($amountArea['P_Energy']); ?></td>
        <td align="right" <?= $row4Col4 ?>><?php echo $amountArea['grand_total'] > 0 && $ePlannig > 0 ? number_format($ePlannig / $amountArea['grand_total'] * 100) . '%' : ''; ?></td>
       </tr>
       <tr style="font-weight: 700;">
        <td>Grand Total</td>
        <td align="right" <?= $row5Col1 ?>><?php echo $currencySymbol . number_format($tRealized); ?></td>
        <td align="right" <?= $row5Col2 ?>><?php echo $currencySymbol . number_format($eoyi + $eoye + $eoyd + $eoym); ?></td>
        <td align="right" <?= $row5Col3 ?>><?php
                          $tPlanned = $amountArea['P_Indirect_Materials'] + $amountArea['P_Capital_Mobile'] + $amountArea['P_Energy'] + $amountArea['P_Direct_Materials'];
                          echo $currencySymbol . number_format($tPlanned); ?></td>
        <td align="right" <?= $row5Col4 ?>><?php echo $amountArea['grand_total'] > 0 ? number_format(($dPlannig + $iPlannig + $ePlannig + $mPlannig) / $amountArea['grand_total'] * 100) . '%' : ''; ?></td>
       </tr>
      </tr>
      <tbody>
      </tbody>
    </table>
  </div>
  <?php $this->renderPartial('/savingsUsg/_area_graph', array("areaPlannedRealized" => $areaPlannedRealized)); ?>

  <!-- End: Saving Type Direct Materials -->
  <div class="clearfix"></div>
  <div class=" tile_count">
   <div class=""> 
    <form class="form-horizontal" id="contract_list_form" name="contract_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('savings/list'); ?>">
     <div class="saving_filter1_container">
      <div class="form-group">
        <input type="text" class="form-control border-select" name="due_date" id="from_date" <?php if (isset($due_date) && !empty($due_date)) echo 'value="' . date(FunctionManager::dateFormat(), strtotime($due_date)) . '"';
          else echo 'placeholder="Start Date"'; ?> autocomplete="off" />
      </div>
      <div class="form-group">
        <input type="text" class="form-control border-select" name="due_date_to" id="from_date_to" <?php if (isset($due_date_to) && !empty($due_date_to)) echo 'value="' . date(FunctionManager::dateFormat(), strtotime($due_date_to)) . '"';
          else echo 'placeholder="Due Date"'; ?> autocomplete="off">
      </div>
      <div class="form-group">
        <select name="status" id="status" class="form-control select_status_multiple notranslate">
          <option value="">All Saving Status</option>
          <?php foreach ($saving_status as $status) { ?>
            <option value="<?php echo $status['id']; ?>" <?php if (!empty($saving_status_id) && in_array($status['id'], $saving_status_id)) echo ' selected="SELECTED" '; ?>><?php echo $status['value']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <select name="department_id[]" onchange="loadDeptForMultiLocations(0);" id="department_id" class="form-control select_department_multiple border-select" searchable="Search here..">
          <option value="">All Operating Units</option>
          <?php if (!empty($getDepartment))
            foreach ($getDepartment as $dept) { ?>
            <option value="<?= $dept['department_id']; ?>"><?= $dept['department_name']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <select name="location_id" id="location_id" class="form-control select_location_multiple border-select dept_multipule_location" searchable="Search here..">
          <option value="0">All Savings Oracle Locations</option>
        </select>
      </div>
      <div class="form-group">
        <select name="category_id" id="category_id" class="form-control select_category_multiple border-select" onchange="loadSubcategories(0);" searchable="Search here..">
          <option value="0">All Categories</option>
          <?php foreach ($categories as $category) { ?>
            <option value="<?php echo $category['id']; ?>" <?php if (isset($category_id) && $category_id == $category['id']) echo ' selected="SELECTED" '; ?>>
              <?php echo $category['value']; ?>
            </option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <select name="subcategory_id" id="subcategory_id" class="form-control select_subcategory_multiple border-select">
          <option value="0">All Subcategories</option>
        </select>
      </div>
     </div>
     <div class="saving_filter1_container">
      <div class="form-group">
        <select name="initiative_owner" id="initiative_owner" class="form-control owner_multiple border-select">
          <option value="0">All Initiative Owners</option>
          <?php foreach ($get_all_user as $user) { ?>
            <option value="<?php echo $user['user_id']; ?>"><?php echo $user['full_name']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <select name="new_archived" id="new_archived" class="form-control archive_multiple border-select notranslate">
          <option value="">Archived</option>
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      <div class="form-group">
        <select name="savings_area" id="savings_area" class="form-control  area_multiple border-select">
          <option value="0">All Savings Areas</option>
          <?php foreach ($savingsArea as $savingArea) { ?>
            <option value="<?php echo $savingArea['id']; ?>"><?php echo $savingArea['value']; ?></option>
          <?php } ?>
        </select>
      </div>          
      <div class="form-group">
          <select name="savings_type" id="savings_type" class="form-control type_multiple border-select">
            <option value="0">All Savings Types</option>
            <?php foreach ($saving_types = FunctionManager::savingType() as $key => $saving_typess) { ?>
              <option value="<?php echo $key; ?>"><?php echo $saving_typess; ?></option>
            <?php } ?>
          </select>
      </div>       
      <div class="form-group">
        <select name="business_unit" id="business_unit" class="form-control business_unit_multiple border-select">
          <option value="0">All Business Units</option>
          <?php foreach ($savingsBussinessUnit as $savingBussinessUnit) { ?>
            <option value="<?php echo $savingBussinessUnit['id']; ?>"><?php echo $savingBussinessUnit['value']; ?></option>
          <?php } ?>
        </select>
      </div>     
      <div class="form-group">
       <?php $sql = "select id,`value` as title from saving_country where soft_deleted='0' ORDER by value ASC";
          $countryReader = Yii::app()->db->createCommand($sql)->query()->readAll(); ?>
       <select name="savings_location_country" id="savings_location_country" class="form-control location_country_multiple border-select">
        <option value="0">All Savings Location Countries</option>
         <?php foreach ($countryReader as $countValue) { ?>
         <option value="<?php echo $countValue['title']; ?>"><?php echo $countValue['title']; ?></option>
        <?php } ?>
       </select>
      </div>
      <div class="form-group">
         <button class="btn btn-info search-quote" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters
         </button>
         <button class="btn btn-primary " onclick="loadSavings(); return false;">Apply filters</button>
      </div>
     </div>
      <!-- End: New Select drop down -->
    </form>
    <!-- End: search filters -->
    <div class="table-responsive" style="padding:0px 2px;">
      <table id="savings_table" class="table table-striped table-bordered savings-table" style="width: 100%;margin-top: 20px;">
        <thead>
          <tr>
            <th> </th>
            <th style="width:max-content; text-align: center;">Saving ID</th>
            <th class="th-center">Savings Initiative Name</th>
            <!-- <th>Start Date</th> -->
            <th class="th-center">Savings Start Date </th>
            <th class="th-center">Savings Due Date</th>
            <th class="th-center">Initiative Owner</th>
            <th class="th-center">Currency</th>
            <th class="th-center">Savings Type</th>
            <th class="th-center">Planned Savings</th>
            <th class="th-center">Realized Savings</th>
            <!-- <th class="th-center">Realized Savings %</th> -->
            <th class="th-center">Status</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>
    </div>
   </div>
  </div>
 </div>
 <div class="panel panel-metrix usg_bg_banner_title">
  <div class="panel-body text-center">Managed by Procurement Excellence</div>
 </div>
<script type="text/javascript">
  colors = ['red', '#66DA26', '#546E7A', '#E91E63', '#FF9800', '#2E93fA', '#2196F3', '#3a62ba', '#7b9333', '#344189', '#', '#', '#', '#', '#'];

  // convert if amount is greater then 1000000 => show into in the million
  var labelFormatter = function(value) {
    let val = Math.abs(value);
     if (val >= 1) {
      val = (val / 1000000).toFixed(2);
     }
    return val;
    };

  function loadDeptForMultiLocations(LocationID) {
    var dept_id = $('#department_id').val();

    $.ajax({
      type: "POST",
      data: {
        dept_id: dept_id
      },
      dataType: "json",
      url: BICES.Options.baseurl + '/locations/getLocations',
      success: function(options) {
        console.log(options);
        var options_html = '<option value="">Savings Oracle Locations</option>';
        for (var i = 0; i < options.length; i++)
          options_html += '<option value="' + options[i].location_id + '">' + options[i].location_name + '</option>';
        $('.dept_multipule_location').html(options_html);

      },
      error: function() {
        $('.dept_multipule_location').html('<option value="">Savings Oracle Locations</option>');
      }
    });
  }
  function loadSavings() {
    $('#savings_table').DataTable().ajax.reload();
  }
  function clearFilter() {
    $('#from_date').val("");
    $('#from_date_to').val("");
    $("option:selected").removeAttr("selected");
    $('.select_location_multiple').trigger("change");
    $('.select_department_multiple').trigger("change");
    $('.select_category_multiple').trigger("change");
    $('.select_subcategory_multiple').trigger("change");
    $('.select_status_multiple').trigger("change");
    //$('.incremental_savings_multiple').trigger("change");
    $('.location_country_multiple').trigger("change");
    $('.business_unit_multiple').trigger("change");
    $('.type_multiple').trigger("change");
    $('.area_multiple').trigger("change");
    $('.archive_multiple').trigger("change");
    $('.owner_multiple').trigger("change");

    $('#savings_table').DataTable().ajax.reload();
  }
  function select2function(className, lableTitle) {
    var lableTitle = lableTitle;
    $("." + className).select2({
      // placeholder: lableTitle,
      /* allowClear: true*/
    });
  }
  $(document).ready(function() {
    select2function('select_location_multiple', 'All Locations');
    select2function('select_department_multiple', 'All Operating Units');
    select2function('select_category_multiple', 'All Categories');
    select2function('select_subcategory_multiple', 'All Sub Categories');
    select2function('select_status_multiple', 'All Savings Statuses');

    select2function('owner_multiple', 'All Initiative Owners');
    select2function('archive_multiple', 'Archived');
    select2function('area_multiple', 'All Savings Areas');
    select2function('type_multiple', 'All Savings Types');
    select2function('business_unit_multiple', 'All Business Units');
    select2function('location_country_multiple', 'All Savings Location Countries');
    //select2function('incremental_savings_multiple','All New Or Incremental Savings');
  });

  <?php if (isset($category_id) && !empty($category_id)) { ?>

    <?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>

    <?php } else { ?>
    <?php } ?>

  <?php } ?>

  function loadSubcategories(subcategory_id) {
    var category_id = $('#category_id').val();

    $.ajax({
      type: "POST",
      data: {
        category_id: category_id
      },
      dataType: "json",
      url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
      success: function(options) {

        var options_html = '<option value="0">All Subcategories</option>';
        for (var i = 0; i < options.suggestions.length; i++)
          options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
        $('#subcategory_id').html(options_html);
        if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id);

        $('#subcategory_id').trigger('change');
      },
      error: function() {
        $('#subcategory_id').html('<option value="0">All Subcategories</option>');
      }
    });


  }
  jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function(s) {
      return Date.parse(s);
    },

    "sort-month-year-asc": function(a, b) {
      return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "sort-month-year-desc": function(a, b) {
      return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
  });
  $.fn.singleDatePicker = function() {
    $(this).on("apply.daterangepicker", function(e, picker) {
      picker.element.val(picker.startDate.format('DD/MM/YYYY'));
    });
    return $(this).daterangepicker({
      singleDatePicker: true,
      singleClasses: "picker_3",
      autoUpdateInput: false,
      locale: {
        format: 'DD/MM/YYYY'
      }
    });
  };
  $(document).ready(function() {
    $('#saving_table').dataTable({
      "columnDefs": [{
          "targets": 0,
          "width": "6%",
          "orderable": false
        },
        /*  { "type": "sort-month-year", targets: 3 },         
          { "type": "sort-month-year", targets: 4 } */
      ],
      "order": []
    });




    $('#from_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>'
    });

    $('#from_date_to').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>'
    });


    $('#from_date_to').on('apply.daterangepicker', function(ev, picker) {
      $('#from_date_to').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS(); ?>'));
    });

    $('#from_date').on('apply.daterangepicker', function(ev, picker) {
      $('#from_date').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS(); ?>'));
    });

    $('.saving_to_date, .saving_from_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>',
    });

  });

  function addSavingModal(e) {
    e.preventDefault();
    $.ajax({
      datatype: 'html',
      url: "<?php echo AppUrl::bicesUrl('savingsUsg/createByModal'); ?>",
      type: "POST",
      data: {
        modalWind: 1
      },
      success: function(mesg) {
        $('#create_saving_cont').html(mesg);
        $('#create_saving').modal('show');
      }
    });
  }

  // Start: line and area chart
  $('document').ready(function() {
    /*Start: Saving Type Direct Materials*/

    var options = {
      series: [{
        name: 'Planned Savings',
        data: [<?php echo $areaAndType['Direct Materials'][2]; ?>, <?php echo $areaAndType['Direct Materials'][3]; ?>, <?php echo $areaAndType['Direct Materials'][1]; ?>]
      }, ],
      chart: {
        type: 'bar',
        height: 380
      },
      plotOptions: {
        bar: {
          barHeight: '100%',
          distributed: true,
          dataLabels: {
            position: 'bottom'
          },
        }
      },
      colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
        '#f48024', '#69d2e7'
      ],
      dataLabels: {
        enabled: true,
        textAnchor: 'start',
        style: {
          colors: ['#fff']
        },
        formatter: labelFormatter,
        offsetX: 0,
        dropShadow: {
          enabled: true
        }
      },
      stroke: {
        width: 1,
        colors: ['#fff']
      },
      xaxis: {
        categories: [
          ["Cost", "Avoidance"],
          ["Cost", "Containment"],
          ["Cost", "Reduction"]
        ]
      },
      yaxis: {
        labels: {
          show: false
        }
      },
      title: {
        text: 'Custom DataLabels',
        align: 'center',
        floating: true
      },
      subtitle: {
        text: 'Category Names as DataLabels inside bars',
        align: 'center',
      },
      tooltip: {
        theme: 'dark',
        x: {
          show: false
        },
        y: {
          title: {
            formatter: labelFormatter,
          }
        }
      }
    };

    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();

    var options = {
      series: [{
        name: 'Planned Savings',
        data: [<?php echo $areaAndType['Direct Materials'][2]; ?>, <?php echo $areaAndType['Direct Materials'][3]; ?>, <?php echo $areaAndType['Direct Materials'][1]; ?>]
      }, ],
      chart: {
        type: 'bar',
        height: 320,
        width: "100%"
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '25%',
          endingShape: 'rounded',
        },
      },
      colors: ['#48d6a8', '#48d6a8', '#48d6a8'],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
      },
      xaxis: {
        categories: ["Avoidance", "Containment", "Reduction"],
      },
      yaxis: {
        //tickAmount: 1000,
        title: {
          text: 'Millions',
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label'
          },
        },
        labels: {
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label'
          },
        }
      },
      legend: {
        position: 'bottom',
        offsetX: 0,
        labels: {
          colors: ['#efa65f', '#48d6a8'],
          useSeriesColors: ['#efa65f', '#48d6a8'],
        },
      },
    };
    var chart = new ApexCharts(document.querySelector("#saving_cat1"), options);
    chart.render();
    /*End: Saving Type Direct Materials*/

    /*Start: Saving Type Energy*/
    var options = {
      series: [{
        name: 'Planned Savings',
        data: [<?php echo $areaAndType['Energy'][2]; ?>, <?php echo $areaAndType['Energy'][3]; ?>, <?php echo $areaAndType['Energy'][1]; ?>]
      }, ],
      chart: {
        type: 'bar',
        height: 320,
        width: "100%"
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '25%',
          endingShape: 'rounded',
        },
      },
      colors: ['#efa65f', '#efa65f', '#efa65f'],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
      },
      xaxis: {
        categories: ["Avoidance", "Containment", "Reduction"],
      },
      yaxis: {
        //tickAmount: 1000,
        title: {
          text: 'Millions',
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        },
        labels: {
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        }
      },
      legend: {
        position: 'bottom',
        offsetX: 0,
        labels: {
          colors: ['#efa65f', '#efa65f'],
          useSeriesColors: ['#efa65f', '#efa65f'],
        },
      },
    };
    var chart = new ApexCharts(document.querySelector("#saving_cat2"), options);
    chart.render();
    /*End: Saving Type Energy*/

    /*Start: Saving Type Indirect Materials*/
    var options = {
      series: [{
        name: 'Planned Savings',
        data: [<?php echo $areaAndType['Indirect Materials'][2]; ?>, <?php echo $areaAndType['Indirect Materials'][3]; ?>, <?php echo $areaAndType['Indirect Materials'][1]; ?>]
      }, ],
      chart: {
        type: 'bar',
        height: 320,
        width: "100%"
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '25%',
          endingShape: 'rounded',
        },
      },
      colors: ['#48d6a8', '#48d6a8', '#48d6a8'],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
      },
      xaxis: {
        categories: ["Avoidance", "Containment", "Reduction"],
      },
      yaxis: {
        //tickAmount: 1000,
        title: {
          text: 'Millions',
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        },
        labels: {
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        }
      },
      legend: {
        position: 'bottom',
        offsetX: 0,
        labels: {
          colors: ['#48d6a8', '#48d6a8'],
          useSeriesColors: ['#48d6a8', '#48d6a8'],
        },
      },
    };
    var chart = new ApexCharts(document.querySelector("#saving_cat3"), options);
    chart.render();
    /*End: Saving Type Indirect Materials*/

    /*Start: Saving Type Capital/Mobile*/
    var options = {
      series: [{
        name: 'Planned Savings',
        data: [<?php echo $areaAndType['Capital/Mobile'][2]; ?>, <?php echo $areaAndType['Capital/Mobile'][3]; ?>, <?php echo $areaAndType['Capital/Mobile'][1]; ?>]
      }, ],
      chart: {
        type: 'bar',
        height: 320,
        width: "100%"
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '25%',
          endingShape: 'rounded',
        },
      },
      colors: ['#efa65f', '#efa65f', '#efa65f'],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
      },
      xaxis: {
        categories: ["Avoidance", "Containment", "Reduction"],
      },
      yaxis: {
        //tickAmount: 1000,
        title: {
          text: 'Millions',
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        },
        labels: {
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        }
      },
      legend: {
        position: 'bottom',
        offsetX: 0,
        labels: {
          colors: ['#efa65f', '#48d6a8'],
          useSeriesColors: ['#efa65f', '#48d6a8'],
        },
      },
    };
    var chart = new ApexCharts(document.querySelector("#saving_cat4"), options);
    chart.render();
    /*End: Saving Type Capital/Mobile*/

  
  // $(document).ready(function(){
  //       $(".apexcharts-text, .apexcharts-xaxis-label, .apexcharts-xaxis-label").on("click", function(){
  //         alert("hello");
  //       })
  // });
});

function tableUsgSavingPhase1TableLoad() {
  $(document).ready(function() {
    $('#savings_table').dataTable({
      "columnDefs": [{
        //"targets": -1,
        "targets": 0,

        "ordering": false,
        "info": false
        /*"width": "6%",*/
      }],

      "createdRow": function(row, data, index) {
        if (data[0].indexOf('glyphicon glyphicon-ok') >= 0)
          for (var i = 1; i <= 6; i++)
            $('td', row).eq(i).css('text-decoration', 'line-through');
      },
      "order": [
        [1, "desc"]
      ],
      "pageLength": 50,
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo AppUrl::bicesUrl('savingsUsg/listAjax'); ?>",
        "type": "POST",
        data: function(input_data) {
          input_data.location_id = $('#location_id').val();
          input_data.department_id = $('#department_id').val();
          input_data.category_id = $('#category_id').val();
          input_data.subcategory_id = $('#subcategory_id').val();
          input_data.status = $('#status').val();
          input_data.due_date = $('#from_date').val();
          input_data.due_date_to = $('#from_date_to').val();
          input_data.initiative_owner = $('#initiative_owner').val();
          input_data.savings_area = $('#savings_area').val();
          input_data.savings_type = $('#savings_type').val();
          input_data.business_unit = $('#business_unit').val();
          input_data.savings_location_country = $('#savings_location_country').val();
          input_data.new_or_incremental_savings = $('#new_or_incremental_savings').val();
          input_data.new_archived = $('#new_archived').val();

          // console.log($('#new_or_incremental_savings').val());
        }
      },
      "oLanguage": {
        "sProcessing": "<h1>Please wait ... retrieving data</h1>"
      },
      "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (aData[1].search("<span class='deactivated_vendor'></span>") > 0) {
          $(nRow).addClass('deactivated_record');
          /*$(nRow).css("background-color", "#ccc");*/
        }
        for (var i = 3; i <= 9; i++) {
          $('td:eq(' + i + ')', nRow).addClass('td-center');
        }
      }
    });
  });
}
setTimeout(tableUsgSavingPhase1TableLoad(), 15000);
</script>

<?php $this->renderPartial('/savingsUsg/create_saving'); ?>
<?php if (!empty($_GET['from']) && $_GET['from'] == 'information') { ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('.click-modal-poup').trigger('click');
    });
  </script>
<?php } ?>

<script type="text/javascript">
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>


<style type="text/css">
 body { background: #ebecf6; }
 .apexcharts-toolbar { display: none !important;}
 .apexcharts-xaxis-label:nth-last-child(2) { transform: translateX(-20px) }

 .btn .caret { float: right; margin-top: 10px;}
 .apexcharts-xaxis-label:nth-last-child(2) { transform: translateX(-2px) !important; }
 .btn-status { background: red !important; border-color: 1px red; }
 .btn-status:hover { background: red !important; border-color: 1px red;}

  @media screen and (max-width: 768px) {
    .mb-10{margin-bottom: 10px;}
    .cont-metrice{width: 100%; min-width: 100%; margin: 5px 0px !important;}
    .panel-metrix{ margin: 0px !important;}
    .search-contract{margin-left: 10px !important;}
  }

</style>