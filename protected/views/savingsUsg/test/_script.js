
    function loadDefaultScoring() {
     $("#default_scoring_container").show();
     $("#scoring_criteria_show").hide();
     $("#scoring_criteria_hide").show();
     $.ajax({
      type: "POST",
      data: {},
      dataType: "json",
      url: BICES.Options.baseurl + '/quotes/scoreDefaultValue',
      success: function(options) {
        $.each(options.score, function(k, v) {
          $('input[name="score_percentage[' + k + ']"').val(v);
          $('input[name="score_percentage[' + k + ']"').trigger('keyup');
          maxScoring();
        });
      }

    });

  }

    $("#scoring_criteria_hide").click(function() {
      $("#default_scoring_container").hide();
      $("#scoring_criteria_show").show();
      $("#scoring_criteria_hide").hide();
      $('.score-criteria').each(function() {
        $(this).val('');
      });
      $(".score-criteria").trigger("keyup");
    });

    function maxScoring(currentFieldValue = '') {
    var totalScore = 0;
    var scoreExist = 0;
    $(".scoring_alert").hide();
    $('.score-criteria').each(function(index, currentElement) {
      totalScore = totalScore + parseFloat($(this).val());
      scoreExist = 1;
    });

    if (currentFieldValue == '') {
      totalScore = 0;
      $('.score-criteria').each(function(index, currentElement) {
        totalScore = totalScore + parseFloat($(this).val());
      });
    }

    if (scoreExist && totalScore < 100) {
      $('.scoring_alert').show();
      $('.scoring_alert').html("Scoring Criteria must add up to 100%");
      return false;
    } else if (totalScore > 100) {
      $('.scoring_alert').show();
      $('.scoring_alert').html("Sourcing criteria must not exceed 100%");
      return false;
    }
    return true;
  }

  function addQuestion(scr) {
    var total_questions = $('#total_questions_' + scr).val();
    total_questions = parseInt(total_questions);
    total_questions = total_questions + 1;
    $('#total_questions_' + scr).val(total_questions);

    var new_question_html = $('#new_question_code' + scr).html();

    new_question_html = new_question_html.replace(/QIDX/g, total_questions);

    $('#total_questions_' + scr).before(new_question_html);
  }



  function addQuestionCustom(scr) {
    var total_questions = $('#total_questions_custom_' + scr).val();
    total_questions = parseInt(total_questions);
    total_questions = total_questions + 1;
    $('#total_questions_custom_' + scr).val(total_questions);

    var new_question_html = $('#new_question_code_custom' + scr).html();

    new_question_html = new_question_html.replace(/QIDX/g, total_questions);

    $('#total_questions_custom_' + scr).before(new_question_html);
  }





  function deleteQuestion(qidx) {
    var display_question_count = 0;
    $("div[id^='question_area']").each(function() {
      if ($(this).is(':visible')) display_question_count += 1;
    });

    if (display_question_count < 1) alert("You must have at least one question field in the quote form");
    else {
      if (confirm("Are you sure you want to delete this question?")) {
        $('#delete_question_flag_' + qidx).val(1);
        $('#question_area_' + qidx).hide();
        $('#answer_section_' + qidx).hide();
        $('#end_question_area_' + qidx).hide();
      }
    }
  }

  function deleteQuestionCustom(qidx) {
    var display_question_count = 0;
    $("div[id^='question_area_custom']").each(function() {
      if ($(this).is(':visible')) display_question_count += 1;
    });

    if (display_question_count < 1) alert("You must have at least one question field in the quote form");
    else {
      if (confirm("Are you sure you want to delete this question?")) {
        $('#delete_question_flag_' + qidx).val(1);
        $('#answer_section_custom' + qidx).hide();
        $('#question_area_custom_' + qidx).hide();
        $('#end_question_area_custom_' + qidx).hide();
      }
    }
  }


  function showQuestion(qidx) {
    $('#here_is_where_you_add_questions_to_this_quote' + qidx).show();
    $('#question_area_1' + qidx).show();
    /* $('#here_is_where_you_add_questions_to_this_quote'+qidx+' [id*="question_area_"]').show();;
     $('#here_is_where_you_add_questions_to_this_quote'+qidx+' [id*="end_question_area_"]').show();*/

  }

  function showQuestionCustom(qidx) {
    $('#here_is_where_you_add_questions_to_this_quote_custom' + qidx).show();
    $('#question_area_custom_1' + qidx).show();
    /* $('#here_is_where_you_add_questions_to_this_quote'+qidx+' [id*="question_area_"]').show();;
     $('#here_is_where_you_add_questions_to_this_quote'+qidx+' [id*="end_question_area_"]').show();*/

  }

  function addAnswer(index) {
    var new_index = $('#total_answers_' + index).val();
    new_index = parseInt(new_index);
    new_index = new_index + 1;
    $('#answer_area_' + index + '_' + new_index).show();
    $('#total_answers_' + index).val(new_index);
  }

  function addAnswerCustom(index) {
    var new_index = $('#total_answers_custom_' + index).val();

    /*if(typeof new_index === "undefined"){
      new_index = 1;
      alert("hereerere");
    }*/

    new_index = parseInt(new_index);
    new_index = new_index + 1;
    $('#answer_area_custom_' + index + '_' + new_index).show();
    $('#total_answers_custom_' + index).val(new_index);

    /*alert(new_index+" *** "+index);*/
  }

  function deleteAnswer(ansidx, index) {
    var display_answer_count = 0;
    $("div[id^='answer_area']").each(function() {
      if ($(this).is(':visible')) display_answer_count += 1;
    });

    if (display_answer_count <= 1) alert("You must have at least one answer field in the quote form");
    else {
      if (confirm("Are you sure you want to delete this answer?")) {
        var new_index = $('#total_answers_' + index).val();
        new_index = parseInt(new_index);
        new_index = new_index - 1;
        $('#total_answers_' + index).val(new_index);
        $('#delete_answer_flag_' + ansidx).val(1);
        $('#answer_area_' + index + '_' + ansidx).hide();
        $('#end_answer_area_' + ansidx).hide();

      }
    }
  }

  function deleteAnswerCustom(ansidx, index) {
    var display_answer_count = 0;
    $("div[id^='answer_area_custom']").each(function() {
      if ($(this).is(':visible')) display_answer_count += 1;
    });

    if (display_answer_count <= 1) alert("You must have at least one answer field in the quote form");
    else {
      if (confirm("Are you sure you want to delete this answer?")) {
        var new_index = $('#total_answers_custom_' + index).val();
        new_index = parseInt(new_index);
        new_index = new_index - 1;
        $('#total_answers_custom_' + index).val(new_index);
        $('#delete_answer_flag_' + ansidx).val(1);
        $('#answer_area_custom_' + index + '_' + ansidx).hide();
        $('#end_answer_area_custom_' + ansidx).hide();

      }
    }
  }