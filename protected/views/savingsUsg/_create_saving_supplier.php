<?php 
 $industry = new Industry();
 $industries = $industry->getAll(array('soft_deleted' => '0','ORDER' => 'value'));
 $toolTipText = MessageManager::tooltipText();
?>
<div class="modal" id="mySavingSupplierModal" data-backdrop="static">
     <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Supplier</h4>
          <button type="button" class="close" onclick="closeSavingSupplierModal();" aria-hidden="true">×</button>
        </div><div class="container"></div>
        <div class="modal-body">
         
      <form id="saving_vendor_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('vendors/createByModal'); ?>">
         <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                  <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate" name="vendor_name" id="vendor_name"
                      <?php if (isset($vendor['vendor_name']) && !empty($vendor['vendor_name'])) echo 'value="' . $vendor['vendor_name'] . '"'; else echo 'placeholder="Supplier Name"'; ?>  required="required"  >
            </div>
              </div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                  <label class="control-label">Contact Name <span style="color: #a94442;">*</span></label>
                  <input type="text" class="form-control contact_name notranslate" name="contact_name" id="contact_name"  aria-invalid="false" required>
              </div>
          </div>
           <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
            <label class="control-label">Company Assigned Supplier ID</label>
            <input type="text" class="form-control notranslate" name="external_id" id="external_id" placeholder="Company Assigned Supplier ID" >
          </div></div>
          

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                <label class="control-label">Email Address <span style="color: #a94442;">*</span> - Please check that this is correct for supplier correspondance  </label>
                <input type="text" class="form-control notranslate" name="emails" id="emails" required="required" placeholder="Email Address" >
            </div>
        </div>

         <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                <label class="control-label ">Confirm Email Address <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control emails_error notranslate" name="confm_emails" 
                id="confrim_email" required="required" placeholder="Email Address" >
                <span class="emails_error" style="display: none;">Email Address and Confirm Email Address are not matching.</span>
            </div>
        </div>
          <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label">Supplier Industry <span style="color: #a94442;">*</span> <i class="fa  fa-info-circle" data-toggle="tooltip" data-placement="right" title="<?php echo $toolTipText['supplierIndustry']; ?>"> </i></label>
             <select name="industry_id" id="industry_id" class="form-control notranslate" onchange="loadSubindustriesContractSupplier(this);" required="required">
              <option value="">Select Supplier Industry</option>
              <?php foreach ($industries as $industry) { ?>
              <option value="<?php echo $industry['id']; ?>">
              <?php echo $industry['value']; ?>
              </option>
              <?php } ?>
            </select>
          </div>
          </div>

        <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
          <label class="control-label">Supplier Sub Industry <span id="requiredIndi" style="color: #a94442;">*</span> <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="<?php echo $toolTipText['supplierSubIndustry']; ?>"></i></label>
          <select name="subindustry_id" id="subindustry_id_modal" class="form-control notranslate" required="required">
          <option value="0">Select Supplier Sub Industry</option>
          </select>
         </div>
       </div>
       <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="checkbox">
                      <label><input type="checkbox" class="flat notranslate" name="preferred_flag" id="preferred_flag" value="1"></label>
                      <label style="padding: 0;margin-top: 10px;display: inline-block;"><div class="clearfix"></div><br />Check this box to indicate that this is a preferred supplier</label>
                  </div>
              </div>
          </div>

     <div class="alert alert-success vendor_alert"><strong>Success!</strong> Supplier submitted successfully.</div>
       <div class="col-md-12 col-sm-12 col-xs-12 text-right">
        <div class="modal-footer">
          <a href="#" onclick="closeSavingSupplierModal();" aria-hidden="true" class="btn">Close</a>
          <input type="submit" style="background-color: #1abb9c !important; border-color: #1abb9c !important;" id="add_vendor_btn"  class="btn btn-primary submit-btn notranslate" value="Add Supplier">
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
function closeSavingSupplierModal(){
  $('#mySavingSupplierModal').hide();
  $(".modal-backdrop").first().css("display","none"); 
}
$("#requiredIndi").hide();
$(document).ready(function(){

  $('#saving_vendor_form').submit(function () { 
  var email = $("#emails").val();
  var confrim_email = $("#confrim_email").val();
  // alert(confrim_email);
  if(email != confrim_email) {
    $(".emails_error").css('color', 'red').show();
    return false;
  }
  $(".vendor_alert").show();
  $.ajax({
      url: '<?php echo AppUrl::bicesUrl('vendors/createByModal'); ?>',
      type: 'POST', async: false, dataType: 'json',
      data: $("#saving_vendor_form").serialize(),
      success: function(info) { 
        $(".vendor_alert").html(info.alert).show().delay(2000).fadeOut();
        setTimeout(function(){
          if (info.dupplicate==0){
          $(".supplier_select").append('<option value="'+info.vendor_id+'" selected>'+info.vendor_name+'</option>');
          closeSavingSupplierModal();
          
          $('#saving_vendor_form')[0].reset();
          }
        }, 3000);
        
      }
  });
  return false;
 });
});

$(".vendor_alert").hide();
function loadSubindustriesContractSupplier(obj){
  var industry_id = obj.value;
  if (industry_id==""){
    $('#subindustry_id_modal').html('<option value="">Select Supplier Sub Industry</option>');
    $('#subindustry_id_modal').removeAttr("required");
    $("#requiredIndi").hide();
  }else
  {
  $.ajax({
      type: "POST", data: { industry_id: industry_id }, dataType: "json",
      url: "<?php echo AppUrl::bicesUrl('vendors/getSubIndustries/'); ?>",
      success: function(options) {
        var options_html = '<option value="">Select Supplier Sub Industry</option>';

        if(options.suggestions.length){
          $('#subindustry_id_modal'). attr("required", "required");
          $("#requiredIndi").show();
        }else{
           $('#subindustry_id_modal').removeAttr("required");
           $("#requiredIndi").hide();
        }

        for (var i=0; i<options.suggestions.length; i++){
          options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
        }
      $("#subindustry_id_modal").html(options_html);      
      },
    
  });
  }
}
</script>