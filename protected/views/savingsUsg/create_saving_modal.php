<?php

 $disabled    = FunctionManager::sandbox();

  $milestoneTitle = '';
  $savingMilestoneDueDate = '<div class="col-md-12 col-sm-12 col-xs-12">
        <label class="control-label">Start Date<span class="text-danger" style="font-size:12px;">*</span></label>
        <input type="text" class="form-control notranslate" name="milestone_saving_due_date" id="milestone_saving_due_date" placeholder="Start Date" /> 
      </div>
      <p class="milestonedueDate_req" style="font-size:12px; color:red; padding-left: 12px;">Start Date is required!</p>';
  $dueDateLabel = 'Savings Expiration Date';
  $mileStonebaseLine ='';

  $mileStoneprojectCostAvoidance = '';

  $projectedCostReduction = "Total Planned Savings";
  $projectedCostReductionClass = "col-md-6 col-sm-6 col-xs-12";
  $jsAddMileStoneFunc = "saveMileStoneCustom()";
  $milestoneDuration = '<div class="col-md-6 col-sm-6 col-xs-12">
      <label class="control-label">Savings Duration (in Months)   
      <span class="text-danger" style="font-size:12px;">*</span></label>
      <!-- <input type="number" class="form-control notranslate" name="field_value" id="field_value" placeholder="Savings Duration (in Months)" />  -->
      <select class="form-control notranslate" placeholder="Savings Duration (in Months)" name="field_value" id="field_value">
        <option>Select</option>';
        for($i = 1; $i<=36; $i++){
          $milestoneDuration .= '<option>'.$i.'</option>';
        }
      $milestoneDuration .= '</select>
      <p class="savings_duration_req" style="font-size:12px; color:red;">Savings Duration (in Months) required</p></div>
      <div class="col-md-6 col-sm-6 col-xs-12" id="amountFields">
      </div>';
    $totalProjectPercent  = '';
    $totalProjectClass = "col-md-12 col-sm-12 col-xs-12";
?>
<div role="main">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h3 class="pull-left">Add New Saving Record - Savings Summary</h3> 
<div class="pull-right" style="padding-right: 21px;padding-top: 3px;">
  <span class="step"></span>
</div><br /><br />
<?php 
$userDepartment = FunctionManager::userSettings(Yii::app()->session['user_id'],'department_id');
$userLocation = FunctionManager::userSettings(Yii::app()->session['user_id'],'location_id');
$sql="select vendor_id,vendor_name from vendors where active=1 and member_type=0 and (vendor_archive is null  or vendor_archive='') and quick_created not in('Yes and Quick Sourcing Evaluation','Yes')  ORDER by vendor_name ASC";
$vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
$sql="select contract_id,contract_title from contracts where contract_archive='' or contract_archive is null ORDER by contract_title ASC";
$contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
$sql="select id,`value` as title from saving_country where soft_deleted='0' ORDER by value ASC";
$countryReader = Yii::app()->db->createCommand($sql)->query()->readAll();
?>
</div>
  <div class="clearfix"> </div>
  <?php if(!empty(Yii::app()->user->getFlash('success'))):?>
  <div class="tile_count">
      <div class="alert alert-success">
          <?php echo Yii::app()->user->getFlash('success'); ?>
      </div>  </div>
  <?php endif; ?>
  <?php if(Yii::app()->user->hasFlash('error')):?>
  <div class="tile_count">
      <div class="alert alert-danger">
          <?php echo Yii::app()->user->getFlash('error'); ?>
      </div>  </div>
  <?php endif; ?>
  <div class="tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">
    
    <?php if(!empty($openModalOnstrSouAnnSav) && $openModalOnstrSouAnnSav === 'usg'){ ?>
      <form id="saving_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('USG/StrategicSourcing/edit'); ?>" autocomplete="off">
    <?php } else { ?>
      <form id="saving_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('savingsUsg/edit'); ?>" autocomplete="off">
    <?php } ?>
       <div class="tab">
        <input type="hidden" class="notranslate" name="redirect_url" id="redirect_url" value="">
        <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 valid">
              <label class="control-label">Savings Name <span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control notranslate" name="title" id="title" placeholder="Saving Title"  required autocomlete="off">
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 valid">

          <label class="control-label">Savings Description</label>
          <textarea class="form-control notranslate" name="notes" id="notes" rows="4" placeholder="Savings Description"></textarea>
        </div>

         <div class="col-md-6 col-sm-6  col-xs-6 date-input valid">
           <label class="control-label">Supplier Name</label>
           <input type="text" class="form-control notranslate" name="oracle_supplier_name" style="  border-radius: 10px;border: 1px solid #4d90fe;">
        </div>

        <div class="col-md-6 col-sm-6  col-xs-6 date-input valid">
          <label class="control-label">Oracle Supplier Number</label>
          <input type="text" class="form-control notranslate" name="oracle_supplier_number" id="oracle_supplier_number" style="  border-radius: 10px;border: 1px solid #4d90fe;">
        </div>

        <div class="col-md-6 col-sm-6 col-xs-6 valid">
          <label class="control-label">Savings Status <span style="color: #a94442;">*</span></label>
          <select name="status" id="status" class="form-control notranslate" required>
            <option value="">Select Savings Status</option>
            <?php foreach ($saving_status as $status) { ?>
            <option value="<?php echo $status['id']; ?>"><?php echo $status['value']; ?></option>
            <?php } ?>
          </select>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-6 valid">
          <label class="control-label">Savings Type
          <span style="color: #a94442;">*</span>
          </label>
          <select name="saving_type" id="saving_type" class="form-control notranslate" required>
              <option value="">Select</option>
              <?php 
              $saving_type = FunctionManager::savingType();
              foreach ($saving_type as $key=>$type) { ?>
              <option value="<?php echo $key; ?>"><?php echo $type; ?></option>
              <?php } ?>   
          </select>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-6 valid">
          <label class="control-label">Savings Area</label>
          <select name="saving_area" id="saving_area" class="form-control notranslate">
            <option value="">Select</option>
            <?php foreach($savingsArea as $savingArea){?>
            <option value="<?php echo $savingArea['id'];?>"><?php echo $savingArea['value'];?></option>
            <?php } ?>  
          </select>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <label class="control-label">Material Type<span style="color: #a94442;">*</span></label>
          <select name="material_type_id" id="modal_material_type_id" onchange="loadCategories(0);"
           class="form-control notranslate"  required>
           <option value="">Select Material</option>
            <?php foreach ($material_type as $material) { ?>
              <option value="<?php echo $material['id']; ?>">
                <?php echo $material['value']; ?>
              </option>
            <?php } ?>
          </select>
        </div>

      <div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-6"></div>
      <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">Primary Purchasing Category<span style="color: #a94442;">*</span> </label>
        <select name="category_id" id="modal_category_id"
          class="form-control notranslate" onchange="loadSubcategories(0);" required>
          <option value="0">Select Category</option>
      </select>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">Secondary Purchasing Category</label>
        <select name="subcategory_id" id="subcategory_id_new"
          class="form-control notranslate">
          <option value="0">Select Sub Category</option>
        </select>
      </div>
    </div>
    <?php
      $query = "SELECT * FROM departments  ORDER BY department_name";
      $getDepartment = Yii::app()->db->createCommand($query)->query()->readAll();
    ?>
     <div class="col-md-6 col-sm-6 col-xs-6 valid">
            <label class="control-label">Operating Unit<span style="color: #a94442;">*</span></label>
            <select required name="department_id" id="department_id_new" onchange="loadDeptForMultiLocations(0);" class="form-control notranslate">
                <option value="">Select Operating Unit</option>
                <?php foreach($getDepartment as $dept) { ?>
                  <option value="<?= $dept['department_id']; ?>"><?= $dept['department_name']; ?></option>
                <?php } ?>
            </select>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6 valid">
       <label class="control-label">Savings Oracle Location<span style="color: #a94442;">*</span></label>
       <select required name="location_id" id="location_id" class="form-control notranslate dept_multipule_location">
            <option value="">Select Location</option>
       </select>
       </div>

        <div class="col-md-6 col-sm-6 col-xs-6 valid">
          <label class="control-label">Business Unit</label>
          <select name="business_unit" id="business_unit" class="form-control notranslate">
            <option value="">Select</option>
            <?php foreach($savingsBussinessUnit as $savingBussinessUnit){?>
            <option value="<?php echo $savingBussinessUnit['id'];?>"><?php echo $savingBussinessUnit['value'];?></option>
            <?php } ?>  
          </select>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-6 valid">
          <label class="control-label">Savings Location Country</label>
          <select name="country" id="country" class="form-control notranslate">
              <option value="">Select Savings Location Country</option>
              <?php foreach($countryReader as $countValue){?>
                <option value="<?php echo $countValue['title'];?>"><?php echo $countValue['title'];?></option>
              <?php } ?>
          </select>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 date-input">
          <label class="control-label">Savings Currency <span style="color: #a94442;">*</span></label>
          <select name="currency_id" id="currency_id" class="form-control notranslate" required>
          <?php foreach ($currency_rate as $rate) { ?>
          <option value="<?php echo $rate['id']; ?>" <?php if (isset(Yii::app()->session['user_currency_dropdown']) && Yii::app()->session['user_currency_dropdown'] == $rate['currency']) echo ' selected="SELECTED" '; ?>>
          <?php echo $rate['currency']; ?>
          </option>
          <?php } ?>
          </select>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-6">
          <label class="control-label">Savings Resulted From A Special Event?</label>
          <select name="special_event" id="special_event" class="form-control notranslate">
            <option value="">Select</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
          </select>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
           <label class="control-label">Event Name & Details</label>
            <input type="text" class="form-control notranslate" name="event_name" id="event_name" placeholder="Event Name & Details" style="border-radius: 10px;border: 1px solid #4d90fe;">
        </div>


<div class="clearfix"><br /></div>
  <div class="clearfix"><br /></div>
    <div class="form-group">
      <?php  if(FunctionManager::savingSpecific()){?>
      <div class="col-md-6 mt-10">          
        <p id="supporting_document_showhide" class="btn btn-sm btn-success submit-btn"> 
          <span class="glyphicon glyphicon-plus"></span>
         Add Financials
        </p>
        <div id="supporting_document_hide" class="btn btn-sm btn-danger"> 
          <span class="glyphicon glyphicon-minus"></span>
          Remove Financials
        </div>
      </div><div class="clearfix"></div>
      <div id="supporting_document_area" style="margin-top: 26px">
      <div id="ajax_success" class="alert alert-success" role="alert"></div>
      <img class="processing_oboloo" src="<?php echo AppUrl::bicesUrl('images/icons/processin_oboloo.gif'); ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">
        <?php
         echo $savingMilestoneDueDate;
         echo $milestoneTitle;
         // echo  $milestoneStartDate;
        ?>
        <div class="clearfix"></div><br />
      <?php echo $mileStonebaseLine; ?>
       
      <div class="<?php echo $projectedCostReductionClass; ?> ">
        <label class="control-label"><?php echo $projectedCostReduction; ?> <span style="color:red">*</span></label>
        <input type="number" class="form-control notranslate" name="cost_reduction" id="cost_reduction" placeholder="<?php echo $projectedCostReduction; ?>" /> 
        <p class="planned_req text-danger" style="font-size: 12px;">Total Planned Savings is required</p>
      </div>
       <?php echo  $milestoneDuration; ?>

       <?php echo $mileStoneprojectCostAvoidance; ?>
     
      <div class="clearfix"></div><br/>

      <div class="col-md-6 col-sm-6 col-xs-12">
      <label class="control-label"><?php echo $dueDateLabel; ?> </label>
      <input type="text" class="form-control notranslate" name="due_date" id="due_date" readonly /> 
      </div>
      <div class="clearfix"></div>
      <div class="col-md-2 col-sm-2 col-xs-12 pull-right text-right">
      <div id="add_milestone_btn"  <?php echo $disabled; ?> class="btn btn-sm btn-success submit-btn" onclick="<?php echo $jsAddMileStoneFunc;?>" style="margin-top: 31px;background: #48d6a8 !important;border-color: #48d6a8 !important;">
          Save Financials
       </div>
      </div>
       </div>
     <?php } else {?>
      <div class="col-md-6">          
        <p id="supporting_document_showhide" class="btn btn-sm btn-success submit-btn"> <span class="glyphicon glyphicon-plus"></span>
          Add Milestone 
         </p>
        <div id="supporting_document_hide" class="btn btn-sm btn-danger"> <span class="glyphicon glyphicon-minus"></span>
          Remove milestone 
        </div>
      </div><div class="clearfix"></div>
      <div id="supporting_document_area" style="margin-top: 26px">
      <div id="ajax_success" class="alert alert-success" role="alert"></div>
      <img class="processing_oboloo" src="<?php echo AppUrl::bicesUrl('images/icons/processin_oboloo.gif'); ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">
        <?php
         echo $savingMilestoneDueDate;
         echo $milestoneTitle;
         // echo  $milestoneStartDate;
        ?>
        <div class="clearfix"></div><br />
        <div class="col-md-6 col-sm-6 col-xs-12">
      <label class="control-label"><?php echo $dueDateLabel;?> </label>
      <input type="text" class="form-control notranslate" name="due_date" id="due_date" /> 
      </div>
      <?php echo $mileStonebaseLine;?>
         <div class="clearfix"></div><br />
      <div class="<?php echo $projectedCostReductionClass; ?>">
        <label class="control-label">Projected Cost Reduction </label>
        <input type="number" class="form-control notranslate" name="cost_reduction" id="cost_reduction" placeholder="Projected Cost Reduction" /> 
      </div>
       

       <?php echo $mileStoneprojectCostAvoidance;?>
     
      <div class="clearfix"></div><br/>
     
      <div class="clearfix"></div>
      <div class="col-md-2 col-sm-2 col-xs-12 pull-right text-right">
      <div id="add_milestone_btn"  <?php echo $disabled; ?> class="btn btn-sm btn-success submit-btn" onclick="<?php echo $jsAddMileStoneFunc;?>" style="margin-top: 31px;background: #48d6a8 !important;border-color: #48d6a8 !important;">
          Save Milestone 
       </div>
      </div>
       </div>
     <?php } ?>
     
    </div>
<div class="clearfix"><br /></div>
  <div class="clearfix"></div><br />
     <div id="milestone_list_cont"></div>
  <div class="clearfix"></div><br /><br />
  <div class="form-group">
   <div class="clearfix"></div><br />
    <div class="<?php echo $totalProjectClass;?>">
      <label class="control-label">Total Project Savings </label>
      <input type="number" class="form-control notranslate" name="total_project_saving" id="total_project_saving" placeholder="Total Project Savings" readonly /> 
    </div>
    <?php echo $totalProjectPercent;?>
     <div class="clearfix"></div>
  </div>
  </div>
  <!-- Start: checkbox -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-18">
    <div class="pull-right">
       <label>
       <input type="checkbox" class=" notranslate" name="current_budget" id="current_budget" value="1">In Current Budget</label></br>
       <label>
        <?php $checkBox = !empty($openModalOnstrSouAnnSav) && $openModalOnstrSouAnnSav === 'usg' ? $openModalOnstrSouAnnSav  : '';
          $initially_verified = $checkBox === 'usg' ? 'Pre-Savings Validation'  : 'Initially Verified';
          $verified = $checkBox === 'usg' ? 'Post-Savings Validation' : 'Verified';
         ?>
       <input type="checkbox" class=" notranslate" name="initially_verified" id="initially_verified" value="1"><?= $initially_verified ?></label></br>
       <label><input type="checkbox" class=" notranslate" name="verified" id="verified" value="1"><?= $verified ?></label></br>
       <label><input type="checkbox" class=" notranslate" name="supported_by_px" id="supported_by_px" value="1">Supported by PX</label>
     
     <div id="submitBtnCont" >
      <!-- Start: Approver -->
      <label class="pull-right"><input type="checkbox" class="notranslate" name="approver_id" id="approver_user_id" value="1">Are planned savings above policy limit?</label>
      <div class="clearfix"></div><br />
      
      <div id="show_approver_user" style="margin-bottom: 18px;text-align: left;">
          <label>Select Approver</label>
          <?php  $sql = "SELECT user_id,full_name FROM `users` where admin_flag !=1 order by full_name asc";
            $getUsers = Yii::app()->db->createCommand($sql)->queryAll(); ?>
          <select  class="form-control select_user_contract notranslate" name="approver_id" id="approver_id" style="border-radius: 10px;border: 1px solid #4d90fe;">
            <option value="">Select Approver</option>
              <?php foreach ($getUsers as $all_user) { ?>
              <option value="<?php echo $all_user['user_id']; ?>">
                <?php echo $all_user['full_name']; ?>
              </option>
              <?php } ?>
         </select>
        </div>

           
        <div class="clearfix"></div>
          <!-- End: Approver -->

      
       <button type="hidden" class="btn btn-info" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
       <button type="hidden" class="btn btn-success" id="nextBtn" onclick="nextPrev(1)">Next</button>
       <div class="scoring_alert2"  style='color: red;position: absolute;bottom: 96px;'></div>
      <div class="clearfix"></div>
  </div>
 
  </div>
  <!-- END: checkbox -->
  <?php if(!empty($openModalOnstrSouAnnSav) && $openModalOnstrSouAnnSav === 'usg'){?>
   <div class="col-lg-12 col-md-12 mt-18">
    <button type="submit" <?php echo $disabled; ?>  class="btn btn-primary btn-functionality pull-right">Add Savings Record</button>
    <a href="<?php echo $this->createUrl('/home'); ?>" onClick="return confirm('Are you want to cancel?')"  class="sub_btns pull-right">
      <button type="button" class="btn btn-danger btn-functionality ">Cancel Savings Record</button>
    </a>
   </div>
  <?php } else { ?>
   <div class="col-lg-12 col-md-12 mt-18">
    <button type="submit" <?php echo $disabled; ?>  class="btn btn-primary btn-functionality pull-right">Add Savings Record</button>
    <a href="<?php echo $this->createUrl('/savingslist'); ?>" onClick="return confirm('Are you want to cancel?')"  class="sub_btns pull-right">
      <button type="button" class="btn btn-danger btn-functionality ">Cancel Savings Record</button>
    </a>
  </div>
  <?php } ?>  
  
<div class="clearfix"></div>
<div class="form-group">
<div class="col-md-4 col-xs-12">
  <label class="control-label">Savings Due Date</label>
  <input type="text" readonly class="form-control notranslate" name="saving_due_date" id="saving_due_date" placeholder="Savings Due Date" /> 
</div>
<div class="col-md-4 col-xs-12">
  <label class="control-label">Created On</label>
  <input type="text" readonly class="form-control notranslate" name="created_at" id="created_at" placeholder="Created On" value="<?php echo date(FunctionManager::dateFormat());?>" /> 
</div>
<div class="col-md-4 col-xs-12">
  <label class="control-label">Created By</label>
  <input type="text" readonly class="form-control notranslate" name="user_name" id="user_name" placeholder="Created By" value="<?php echo Yii::app()->session['full_name'];?>" /> 
</div>
 <div class="clearfix"></div>
</div>
</div>
</form>
</div></div>
</div>
<?php echo $this->renderPartial("_create_saving_supplier"); ?>

<script type="text/javascript">
$(document).ready( function() {
 $("#supporting_document_area").hide();
 $("#supporting_document_hide").hide();
 $('#show_approver_user').hide();

  $("#supporting_document_showhide").click(function(){
  $("#supporting_document_area").show();
  $("#supporting_document_showhide").hide();
  $("#supporting_document_hide").show();
  $(".show-remove-btn-alert-hide").hide();
});

$("#supporting_document_hide").click(function(){
  $("#supporting_document_area").hide();
  $("#supporting_document_showhide").show();
  $("#supporting_document_hide").hide();
  $(".show-remove-btn-alert-hide").show();
});

$('#approver_user_id').on('click', function () {
    if ($(this).prop('checked')) {
        $('#show_approver_user').fadeIn();
    } else {
        $('#show_approver_user').hide();
    }
});

  $('#due_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> '});
  $('#milestone_saving_due_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> '});
  
  $('#created_at').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> '});
  $('#saving_due_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> '});
  // $('#start_date').datetimepicker({
  //   format: '<?php echo FunctionManager::dateFormatJS();?>'});
  
  $(".select_vendor_multiple").select2({
         placeholder: "Select Supplier",
         allowClear: true,
        containerCssClass: "notranslate",
       dropdownCssClass: "notranslate"
      }).on('select2:close', function() {
        var el = $(this);
      if(el.val()==="NEW_SUPPLIER_SAVING") {
        $("#mySavingSupplierModal").show('modal');
      }
      });

  
  $("#saving_form").submit(function(){
    var redirectUrl = $("#redirect_url").val();
    if(redirectUrl==null || redirectUrl==''){
     
      $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Would you like to complete the additional details for this saving? (Milestones/timelines etc)</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-success',
                            action: function(){
                              $("#redirect_url").val('edit');
                              $("#saving_form").submit();
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-info',
                            action: function(){
                                $("#redirect_url").val('list');
                                $("#saving_form").submit();
                            }
                          },
            }});
  
       return false;
    }
    
  });
});

function createSavingAndValidation(){

  return false;
}
function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
}



function loadDeptForMultiLocations(LocationID) {
     var dept_id = $('#department_id_new').val();

        $.ajax({
            type: "POST", data: { dept_id: dept_id }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getLocations',
            success: function(options) {
              console.log(options);
                var options_html = '<option value="">Select Locations</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].location_id + '">' + options[i].location_name + '</option>';
                $('.dept_multipule_location').html(options_html);
               
            },
          error: function() { $('.dept_multipule_location').html('<option value="">Oracle Location</option>'); }
      });
}



function selectDepartment(dpartmentID ){
  $('#department_id_new').prop('selectedIndex', dpartmentID);
}

function loadDepartments(location_id)
{
    var location_id = location_id;
    var single = 1;

        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/savingsUsg/getUsgDepartments',
            success: function(options) {
              
                var options_html = '<option value="">Select Operating Unit</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                $('#department_id_new').html(options_html);
                selectDepartment('<?php echo $userDepartment;?>');
            },
            error: function() { $('#department_id_new').html('<option value="">Operating Unit</option>'); }
        });
}

function loadDepartmentsForSingleLocation(sel)
{
    var location_id = sel.value;
    var single = 1;
    loadDepartments(location_id);
}

 $(document).ready(function(){

  $("#field_value,#milestone_saving_due_date,#cost_reduction").on("change keyup blur",function(){
  var duration = $('#field_value').val();
  var start_date = $('#milestone_saving_due_date').val();
  var cost_reduction = $('#cost_reduction').val();
  if(parseFloat(duration)>0 && parseFloat(cost_reduction)>0){
   $.ajax({
          type: "POST", 
          data: { duration: duration,start_date,start_date,cost_reduction:cost_reduction }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('savingsUsg/durationValue/'); ?>",
          success: function(results) {
           $('#amountFields').html(results.durationList);
           $('#due_date').val(results.endDate);
           //$('#total_project_saving_perc').val(results.totalPlanned);

          }
  });
  }
  });
 });

/*function loadFieldValueChange(field_value){
  var duration = $('#field_value').val();
  var start_date = $('#start_date').val();
  var cost_reduction = $('#cost_reduction').val();
   $.ajax({
          type: "POST", 
          data: { duration: duration,start_date,start_date,cost_reduction:cost_reduction }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('savingsUsg/durationValue/'); ?>",
          success: function(results) {
           $('#amountFields').html(results.durationList);
          }
  });
}*/

function loadSubcategories(input_subcategory_id)
{
  var category_id = $('#modal_category_id option:selected').val();
  
  if (category_id == 0)
    $('#subcategory_id_new').html('<option value="0">Select Sub Category</option>');
  else
  { 
      $.ajax({
          type: "POST", data: { category_id: category_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
          success: function(options) {
            var options_html = '<option value="0">Select  Sub Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#subcategory_id_new').html(options_html);
            if (input_subcategory_id != 0) $('#subcategory_id_new').val(input_subcategory_id); 
          },
          error: function() { $('#subcategory_id_new').html('<option value="0">Select Sub Category</option>'); }
      });
  }
}

function loadCategories(input_category_id)
{
  var material_type_id = $('#modal_material_type_id option:selected').val();
  if (material_type_id == 0)
    $('#modal_category_id').html('<option value="0">Select Category</option>');
  else
  { 
      $.ajax({
          type: "POST", data: { material_type_id: material_type_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('savingsUsg/getCategories'); ?>",
          success: function(options) {

            var options_html = '<option value="0">Select Category</option>';

            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#modal_category_id').html(options_html);
            if (input_category_id != 0) $('#modal_category_id').val(input_category_id); 

          },
          error: function() { $('#modal_category_id').html('<option value="0">Select Category</option>'); }
      });
  }
}
</script>

<?php if ($userLocation) { ?>
  <script type="text/javascript">loadDepartments("<?php echo $userLocation;?>");</script>
  <?php } ?>

<script type="text/javascript">

var show_hide_score=1;
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab
function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";

  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    $("#nextBtn").hide();
  } else {
    $("#nextBtn").show();
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:


  if (n == 1 && !validateForm()) return false;
  if(currentTab==0){

       $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Would you like to add questionnaires or scoring criteria?</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                   show_hide_score = 1;
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                  $("#nextBtn").trigger('click');
                                   show_hide_score = 0;
                            }
                          },
                
                
                
            }
        });

   
      /*if(!confirm('Would you like to add questionnaires or scoring criteria?')){
       n=2;
       show_hide_score = 0;
     }else{
      show_hide_score = 1;
     }*/
  }else if(currentTab==2){
    if(show_hide_score==0){
       n=-2;
    }
  }
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("order_form").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

var requiredArr = ['title','modal_category_id','subcategory_id_new','currency_id'/*,'notes'*/];


function requiredArrCheck(field_id) {
  return age >= field_id;
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].querySelectorAll("select,input,textarea");
  // A loop that checks every input field in the current tab:
 for (i = 0; i < y.length; i++) { 
    // If a field is empty... 
 
    if ((y[i].value == "" || y[i].value == 0) && (requiredArr.includes(y[i].id) || requiredArr.includes(y[i].class) ) ) {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }else {
       $('#'+ y[i].id).removeClass('invalid');
    }
  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}



$(".new_supplier_con").hide();
$("#hide_new_supplier").hide();
$("#new_supplier").click(function(){
  $(".new_supplier_con").show();
  $("#hide_new_supplier").show();
    $(".quick_vendor_field").show();
  
});
$("#hide_new_supplier").click(function(){
  $(".new_supplier_con").hide();
  $("#new_vendor_name").val("");
  $("#new_vendor_emails").val("");
  $("#hide_new_supplier").hide();
});



$("#save_new_supplier").click(function(){
      
      var email = $("#new_vendor_emails").val();
      var conf_email = $("#confm_emails").val();
      var new_vendor_name = $("#new_vendor_name").val();
      var new_vendor_emails = $("#new_vendor_emails").val();
      var new_vendor_contact_name = $("#new_vendor_contact").val();     
      var new_vendor_address = $("#new_vendor_address").val();
      var new_vendor_address2 = $("#new_vendor_address2").val();
      var new_vendor_city = $("#new_vendor_city").val();
      var new_vendor_state = $("#new_vendor_state").val();
      var new_vendor_zip = $("#new_vendor_zip").val();

    if(email != conf_email) {
      $(".emails_error").css('color', 'red').show();
      return false;
      }

    if (new_vendor_name =="") {
      $(".new_vendor_err").css('color', 'red').show();
      return false;
    }
 
    if(new_vendor_contact_name ==""){
      $(".new_vendor_cont_err").css('color', 'red').show();
      return false;
    }

    if(new_vendor_emails  != ''){
     $(this).prop('disabled',true);
       $.ajax({
            type: "POST", 
            data: { vendor_name:new_vendor_name,vendor_emails:new_vendor_emails,new_vendor_contact_name:new_vendor_contact_name,new_vendor_address:new_vendor_address,new_vendor_address2:new_vendor_address2,new_vendor_city:new_vendor_city,new_vendor_state:new_vendor_state,new_vendor_zip:new_vendor_zip},
            url: BICES.Options.baseurl + '/vendors/quickAddVendor',
            success: function(options) {
                if(options==1){
                $(".quick_msg").html("Email is already taken.");
                 alert("Supplier with this Email is already taken. Please try another.");
               }else if(options !=1){
                $(".quick_msg").html("New Supplier added successfully.");
                 alert("Supplier added successfully.");
                 /*$("#hide_new_supplier").trigger("click");*/
                  //
                  $(".quick_vendor_field").hide();

                  
                  $("#quick_vendor_name");
                  $("#new_vendor_emails").val('');
                  $("#confm_emails").val('');
                  $("#new_vendor_contact").val('');
                  $("#new_vendor_address").val('');
                  $("#new_vendor_address2").val('');
                  $("#new_vendor_city").val('');
                  $("#new_vendor_state").val('');
                  $("#new_vendor_zip").val('');

               }else if(options==3){
                $(".quick_msg").html("Problem occured, try again.");
                 alert("Problem occured, try again.");
               }
              $("#save_new_supplier").prop('disabled',false);
            
           
            }

           
        });
      
    }else{
      $(".quick_msg").before("Email is required");
    }

    $(".quick_msg").delay(5000).fadeOut(800);

});




$("#ajax_success").hide();
$(".processing_oboloo").hide();
$(".planned_req").hide();
$(".milestonedueDate_req").hide();
$(".savings_duration_req").hide();
function saveMileStone(){ 

  var mileStone = $("#milestone_title").val();  
  var dueDate = $("#due_date").val();
  var milestonedueDate = $("#milestone_saving_due_date").val();
  var baseLine = $("#base_line_spend").val();
  var costReduction = $("#cost_reduction").val();
  var costAvoidance = $("#cost_avoidance").val();
 
    $.ajax({
        url: BICES.Options.baseurl + '/savingsUsg/addMileStone', // point to server-side PHP script 
        dataType: 'json',  // what to expect back from the PHP script, if anything
        data: {title:mileStone,due_date:dueDate,miletone_due_date:milestonedueDate,base_line_spend:baseLine,cost_reduction:costReduction,cost_avoidance:costAvoidance},
        type: 'post',
         beforeSend: function() {
          $(".processing_oboloo").show();
          $("#ajax_success").html('');
          $("#ajax_success").hide();
          $(".btn-functionality").attr('disabled','disabled');
          $(".btn-functionality").prop('disabled',true);
          $("#add_milestone_btn").prop('disabled',true);
          $("#nextBtn").prop('disabled',true);
         },
        success: function(uploaded_response){

          if(uploaded_response.mesg==1){
            $("#ajax_success").show();
            $("#ajax_success").html('Milestone saved successfully');
          }else{
            $("#ajax_success").html('Problem occured while saving Milestone, try again');
            $("#ajax_success").show();
          }
          $(".processing_oboloo").hide();
          $(".btn-functionality").prop('disabled',false);
          $("#add_milestone_btn").prop('disabled',false);
          $("#nextBtn").prop('disabled',false);
          $("#milestone_list_cont").html(uploaded_response.record_list);

          $("#total_project_saving").val(uploaded_response.total_project_saving);
          $("#total_project_saving_perc").val(uploaded_response.total_project_saving_perc);
          $("#saving_due_date").val(uploaded_response.due_date);
          
          $("#milestone_title").val('');
          $("#due_date").val('');
          $("#milestone_saving_due_date").val('');
          $("#base_line_spend").val('');
          $("#cost_reduction").val('');
          $("#cost_avoidance").val('');
          

        }
     });
   
    $("#ajax_success").delay(1000*5).fadeOut();
}

function saveMileStoneCustom(){ 

  var mileStone = $("#milestone_title").val();  
  var dueDate = $("#due_date").val();
  var milestonedueDate = $("#milestone_saving_due_date").val();
  // var mileStoneStartDate = $("#start_date").val();
  var baseLine = $("#base_line_spend").val();
  var costReduction = $("#cost_reduction").val();
  var costAvoidance = $("#cost_avoidance").val();
  var savingDuration=$("#field_value").val();
  var durationMontly= new Array();
  console.log(savingDuration);
  if(milestonedueDate == '' || milestonedueDate == 'undefined'){
    $(".milestonedueDate_req").show();
    return false;
  }else{
    $(".milestonedueDate_req").hide();
  }

  if(costReduction == '' || costReduction == 'undefined'){
    $(".planned_req").show();
    return false;
  }else{
    $(".planned_req").hide();
  }

  if(savingDuration == '' || savingDuration == 'Select'){
     $(".savings_duration_req").show();
      return false;
  }else{
    $(".savings_duration_req").hide();
  }

  $('.milestone-plann-duration').each(function(){
      mothly  = {
        'fieldName' : $(this).attr("name"),
        'fieldValue' : $(this).val()
      };
      durationMontly.push(mothly);
  });

  $.ajax({
        url: BICES.Options.baseurl + '/savingsUsg/addMileStone', // point to server-side PHP script 
        dataType: 'json',  // what to expect back from the PHP script, if anything
        data: {title:mileStone,due_date:dueDate,miletone_due_date:milestonedueDate,base_line_spend:baseLine,cost_reduction:costReduction,cost_avoidance:costAvoidance,saving_duration:savingDuration, duration_monthly: durationMontly},
        type: 'post',
         beforeSend: function() {
          $(".processing_oboloo").show();
          $("#ajax_success").html('');
          $("#ajax_success").hide();
          $(".btn-functionality").attr('disabled','disabled');
          $(".btn-functionality").prop('disabled',true);
          $("#add_milestone_btn").prop('disabled',true);
          $("#nextBtn").prop('disabled',true);
         },
        success: function(uploaded_response){

          if(uploaded_response.mesg==1){
            $("#ajax_success").show();
            $("#ajax_success").html('Milestone saved successfully');
          }else{
            $("#ajax_success").html('Problem occured while saving Milestone, try again');
            $("#ajax_success").show();
          }
          $(".processing_oboloo").hide();
          $(".btn-functionality").prop('disabled',false);
          $("#add_milestone_btn").prop('disabled',false);
          $("#nextBtn").prop('disabled',false);
          $("#milestone_list_cont").html(uploaded_response.record_list);

          $("#total_project_saving").val(uploaded_response.total_project_saving);
          $("#total_project_saving_perc").val(uploaded_response.total_project_saving_perc);
          $("#saving_due_date").val(uploaded_response.due_date);
          
          $("#milestone_title").val('');
          $("#due_date").val('');
          $("#milestone_saving_due_date").val('');
          $("#base_line_spend").val('');
          $("#cost_reduction").val('');
          $("#cost_avoidance").val('');
          // $("#start_date").val('');
          $("#field_value").val('');
          $("#amountFields").html('');
        }
     });
   
    $("#ajax_success").delay(1000*5).fadeOut();
}


function deleteMilestone(recordKey)
{
  var recordKey = recordKey;
  $("#ajax_success").hide();
  $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Are you sure want to delete this file/document</span>',
            buttons: {
               Yes: {
                      text: "Yes",
                      btnClass: 'btn-blue',
                      action: function(){
                          $.ajax({
                              dataType: 'json',  
                              type: "post",
                              url: "<?php echo AppUrl::bicesUrl('savingsUsg/deleteMilestone'); ?>",
                              data: { recordKey: recordKey },
                              success: function(uploaded_response){
                                if(uploaded_response.mesg==1){
                                  $("#ajax_success").show();
                                  $("#ajax_success").html('File deleted successfully');
                                }else{
                                 $("#ajax_success").show();
                                 $("#ajax_success").html('Problem occured while deleting record, try again');
                                }
                                 $("#milestone_list_cont").html(uploaded_response.record_list);
                                 $("#total_project_saving").val(uploaded_response.total_project_saving);
                                 $("#total_project_saving_perc").val(uploaded_response.total_project_saving_perc);
                                 $("#saving_due_date").val(uploaded_response.due_date);
                              }
                          });
                      }
                    },
                      No: {
                        text: "No",
                        btnClass: 'btn-red',
                        action: function(){}
                      },
            }});
 
      $("#ajax_success").delay(1000*5).fadeOut();
}


function supplierSelect(vendor_id,serialNum){
  var vendor_id = vendor_id;
  var serialNum = serialNum;

  var obj ='';
  if(serialNum=='QIX'){ 
    obj = $(".vendor_new_vendor_name_QIX");
  }else{
      obj = $(".vendor_new_vendor_name_"+serialNum);
  }
  $(obj).next('.select2-container').find('.select2-selection--single').removeClass('invalid');

   $.ajax({
          dataType:'json',  
          type: "post",
          url: "<?php echo AppUrl::bicesUrl('vendors/getVendorDetail'); ?>",
          data: { vendor_id: vendor_id },
          success: function(data_option){
          if(serialNum=='QIX'){
             $(".vendor_contact_new_QIX").html(data_option.contact_name);
             //$(".vendor_email_new_QIX").val(data_option.emails);
          }else{
           $(".vendor_contact_new_"+serialNum).html(data_option.contact_name);
           //$(".vendor_email_new_"+serialNum).val(data_option.emails);

          }
          }
        });
  }

 $(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();  
  });

</script>


<!-- Modals -->
<div class="modal fade" id="product_modal" tabindex="-1" role="dialog" style="z-index: 999">
  <div class="modal-dialog" role="document" style="
    position: relative;
    top: 41%;
    transform: translateY(-50%);
">
    <div class="modal-content" style="max-width: 538px; margin: auto;">
      <div class="modal-body">

        <div class="new_supplier_con col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label">Supplier Name</label>
            <input type="text" class="form-control  notranslate" name="quick_vendor_name" id="quick_vendor_name" placeholder="Supplier Name">
        
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <span class='quick_msg pull-right' style='color: #a94442;'></span><br />
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" title="Close Quick Supplier" style="margin-top: 8px;">Close</button>
          <span class="btn btn-primary btn-sm pull-right" id="save_new_supplier" title="Save Quick Supplier">Add Quick Supplier</span>
          <div class="clearfix"></div><br />
          <div class="text-right">New suppliers added via quick sourcing activities are not added to the supplier management module</div>

        </div>

        <div class="clearfix"></div>
        </div>
      </div>
      
        
      </div>
    </div>
  </div>
</div>

<style type="text/css">
.btn-file {
 padding:6px !important;
}
@media (max-width: @screen-xs-min) {
 .modal-xs { width: @modal-sm; }
}
#save_new_supplier { margin-top: 10px;}
#number_custom_score {cursor: pointer; padding-right: 10px;}
canvas { display: block; max-width: 800px; margin: 60px auto; }
/* Mark input boxes that gets an error on validation: */
.invalid {background-color: #ffdddd !important;}
/* Hide all steps by default: */
.tab { display: none;}

/* Make circles that indicate the steps of the form: */
.step { height: 20px; width: 20px; background-color: #bbbbbb;
  border: none; border-radius: 50%; display: inline-block;
  opacity: 0.5; display: inline-block; padding-left: 7px; color: #fff;
}

/* Mark the active step: */
.step.active { opacity: 1; background-color: #2d9ca2;}

/* Mark the steps that are finished and valid: */
.step.finish { background-color: #2d9ca2;}
.form-control-feedback-new { margin-top: 31px !important; }
.ui-datepicker { width:auto; }
.select2-container { -webkit-box-shadow: none; box-shadow: none; }
.step.active { background-color:none !important }
.step {  height: 0px; width: 0px; }
.help-block { color: #f40707; }
</style>