<div class="clearfix"> <br /> </div>
	<div class="form-group">
		<div class="col-md-4 col-sm-4 col-xs-8">
			<h4>Saving Log</h4>
		</div>
	</div>
	<div class="clearfix"> <br /> </div>
	<div class="col-md-7 col-sm-7 col-xs-12">
		<table class="table">
			<thead><tr><th>User</th><th>Date</th><th style="width: 70%;">Comments</th></tr></thead>
			<?php
			if(count($savingLogs)) {
				foreach ($savingLogs as $savingLog) {
					?>
					<tr>
						<td><?php echo $savingLog['user']; ?></td>
						<td><?php echo date(FunctionManager::dateFormat(), strtotime($savingLog['created_datetime'])); ?></td>
						
						<td><?php echo $savingLog['comment']; ?></td>
					</tr>
					<?php
				}
			}else{
			?>
				<tr><td colspan="4">No Record Found</td></tr>
			<?php } ?>
		</table>
	</div>