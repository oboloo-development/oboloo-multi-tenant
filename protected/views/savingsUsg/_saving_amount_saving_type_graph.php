<div class="x_panel mb-0">
  <div class="x_title">
    <h2>Savings Amount By Savings Type</h2>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <div id="saving_amount_by_saving_type"></div>
  </div>
</div>


<!-- // Start: Savings Amount By Savings Type  -->
<script  type="text/javascript">

// convert if amount is greater then 1000000 > show into million
var labelFormatter = function(value) {
    var val = Math.abs(value);
     if (val >= 1) {
      val = (val / 1000000).toFixed(2);
     }
    return val;
    };

    var options = {
      series: [{
        name: 'Realized',
        data: [<?php echo $savingAmountByType['cost_reduction_realized']; ?>, <?php echo $savingAmountByType['cost_avoidance_realized']; ?>, <?php echo $savingAmountByType['cost_containment_realized']; ?>]
      }, {
        name: 'Forecasted',
        data: [<?php echo $savingAmountByType['cost_reduction_forecasted']; ?>,<?php echo $savingAmountByType['cost_avoidance_forecasted']; ?>,<?php echo $savingAmountByType['cost_containment_forecasted']; ?>]
      }],
      chart: {
        type: 'bar',
        height: 350
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '30%',
          endingShape: 'rounded'
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['#48d6a8', '#efa65f'],
      },
      xaxis: {
        categories: ["Cost Reduction", "Cost Avoidance", "Cost Containment"],
      },
      
      yaxis: {
         title: {
          text: 'Millions',
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        },
        labels: {
          formatter: labelFormatter,
          style: {
            fontSize: '10px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            
          },
          
        },
        formatter: function (value) {
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        }
      },
      colors: ['#48d6a8','#efa65f'],
    };
    
    var chart = new ApexCharts(document.querySelector("#saving_amount_by_saving_type"), options);
    chart.render();
    // /* End  : Savings Amount By Savings Type  */
  
   

  </script>