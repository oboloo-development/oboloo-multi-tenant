<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Accounts</h3>
        </div>

        <div class="span6 pull-right">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg"
                    data-departments="" data-account-id="0" data-account-name="" data-account-code="" data-parent-account-id="0" data-account-type-id="0">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Account
            </button>
        </div>

        <div class="clearfix"> </div>
    </div>

    <table id="account_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th> </th>
          <th>Account Type</th>
          <th>Account Code</th>
          <th>Account Name</th>
          <th>Parent Account</th>
          <th>ID</th>
        </tr>
      </thead>

      <tbody>

          <?php foreach ($accounts as $account) { ?>

              <tr id="account_<?php echo $account['account_id']; ?>">
                    <td>
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg"
                                data-account-id="<?php echo $account['account_id']; ?>" data-account-name="<?php echo $account['account_name']; ?>"
                                    data-account-code="<?php echo $account['account_code']; ?>"
                                        data-departments="<?php echo $account['departments']; ?>"
                                            data-parent-account-id="<?php echo $account['parent_account_id']; ?>"
                                                data-account-type-id="<?php echo $account['account_type_id']; ?>">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        &nbsp;
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </td>
                    <td><?php echo $account['account_type']; ?></td>
                    <td><?php echo $account['account_code']; ?></td>
                    <td><?php echo $account['account_name']; ?></td>
                    <td><?php echo $account['parent_account_name']; ?></td>
                    <td><?php echo $account['account_id']; ?></td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>

<div id="account_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add New Account</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control has-feedback-left" name="account_name" id="account_name" placeholder="Account Name" value="" />
                <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>
        <div class="clearfix"> </div>

        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <input type="text" class="form-control" name="account_code" id="account_code" placeholder="Account Code" />
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <select name="account_type_id" id="account_type_id" class="form-control">
                    <option value="0">Select Account Type</option>
                    <?php foreach ($account_types as $account_type) { ?>
                        <option value="<?php echo $account_type['id']; ?>">
                            <?php echo $account_type['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="clearfix"> </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="parent_account_id" id="parent_account_id" class="form-control">
                    <option value="0">Select Parent Account</option>
                    <?php foreach ($parent_accounts as $parent_account) { ?>
                        <option value="<?php echo $parent_account['account_id']; ?>">
                            <?php echo $parent_account['account_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="clearfix"> </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select multiple="multiple" name="departments[]" id="departments" class="form-control select2_multiple">
                    <?php foreach ($departments as $department) { ?>
                        <option value="<?php echo $department['department_id']; ?>">
                            <?php echo $department['department_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="clearfix"> </div>
        </form>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="account_id" id="account_id" value="0" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="saveAccountButton" type="button" class="btn btn-primary" onclick="saveAccount();">
            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading_icon" style="display: none;"></span>
            <span>Save Account</span>
        </button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready( function() {
    $("#departments").select2({placeholder: 'Select Departments'});

    $('#account_table').dataTable({
        "columnDefs": [{
            "targets": 0,
            "width": "12%",
            "orderable": false
        },
        {
            "targets": 5,
            "visible": false,
            "searchable": false
        }],
        "createdRow": function(row, data, dataIndex) {
            $(row).attr('id', 'account_' + data[4]);
        },
        "order": []
    });
})

$("#account_modal").on('show.bs.modal', function (e) {
    var account_id = $(e.relatedTarget).data('account-id');
    var account_name = $(e.relatedTarget).data('account-name');
    var account_code = $(e.relatedTarget).data('account-code');
    var account_type_id = $(e.relatedTarget).data('account-type-id');
    var parent_account_id = $(e.relatedTarget).data('parent-account-id');

    var departments = $(e.relatedTarget).data('departments') + '';
    var department_ids = new Array();
    if (departments.indexOf(",") >= 0) department_ids = departments.split(',');
    else if ($.trim(departments) != "") department_ids.push(departments);

    if (account_id == 0)
    {
        $('#account_name').val('');
        $('#account_type_id').val(0);
        $('#account_code').val('');
        $('#departments').select2().val(null).trigger('change');
        $('#parent_account_id').val(0);
        $('#myModalLabel').html('Add New Account');
    }
    else
    {
        $('#account_name').val(account_name);
        $('#account_code').val(account_code);
        $('#account_type_id').val(account_type_id);
        if (department_ids.length > 0)
            $('#departments').select2().val(department_ids).trigger('change');
        else
            $('#departments').select2().val(null).trigger('change');
        $('#parent_account_id').val(parent_account_id);
        $('#myModalLabel').html('Edit Account');
    }

    $('#account_id').val(account_id);
});

function saveAccount()
{
    var account_name = $.trim($('#account_name').val());
    var account_code = $.trim($('#account_code').val());
    var account_type = $("#account_type_id option:selected").text();

    if (account_name != '')
    {
        $('#loading_icon').show();
        var saved_account_id = 0;

        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('accounts/save'); ?>',
            type: 'POST', async: false,
            data: {
                    account_id: $('#account_id').val(),
                    account_name: account_name,
                    account_type_id: $('#account_type_id').val(),
                    parent_account_id: $('#parent_account_id').val(),
                    account_code: $('#account_code').val(),
                    departments: $('#departments').val()
              },
              success: function(new_account_id) { saved_account_id = new_account_id; }
        });

        if ($('#account_id').val() == 0)
        {
            $.ajax({
                url: '<?php echo AppUrl::bicesUrl('accounts/getParentAccounts'); ?>',
                type: 'POST', dataType: 'json',
                success: function(parent_accounts) {
                    var option_html = "";
                    for (var i=0; i<parent_accounts.length; i++) {
                        option_html += '<option value="' + parent_accounts[i].account_id + '">';
                        option_html += parent_accounts[i].account_name + '</option>';
                    }
                    $('#parent_account_id').html(option_html);
                }
            });
        }

        if (saved_account_id == 0) saved_account_id = $('#account_id').val();
        var account_actions_html = '';
        account_actions_html = '<button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg"';
        account_actions_html += ' data-departments="' + $.trim($('#departments').val()) + '" data-parent-account-id="' + $('#parent_account_id').val() + '"';
        account_actions_html += ' data-account-type-id="' + $('#account_type_id').val() + '" data-account-code="' + $.trim($('#account_code').val()) + '"';
        account_actions_html += ' data-account-id="' + saved_account_id + '" data-account-name="' + $.trim($('#account_name').val()) + '">';
        account_actions_html += ' <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
        account_actions_html += ' </button> &nbsp; <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>';

        var table = $('#account_table').DataTable();
        table.row('#account_' + saved_account_id).remove().draw();

        $('#account_id').val(0);
        $('#account_name').val('');
        $('#account_type_id').val(0);
        $('#account_code').val('');
        $('#departments').val('');

        table.row.add([account_actions_html, account_type, account_code, account_name, saved_account_id]).draw().node();
        $('#loading_icon').hide();
        $('#account_modal').modal('hide');
    }
}

</script>
