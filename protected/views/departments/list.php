
<div class="right_col" role="main">
    <input type="hidden" id="department_label" value="<?php 
        if(strpos(Yii::app()->getBaseUrl(true),"usg")){echo "Operating Unit";
        } else {echo "Departments";} ?> ">
    <div class="row-fluid tile_count">
        <?php $alert=Yii::app()->user->getFlash('success');
          if(!empty($alert)) { ?>
          <div class="clearfix"></div>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $alert;?></div>
        <?php } ?>
        <?php $disabled = FunctionManager::sandbox(); ?>
        <div class="span6 pull-left">
            <h3> 
                <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                    Operating Unit
                <?php } else { ?> 
                    Departments
                <?php } ?></h3>
            <h4 class="subheading"><br />Add your organisation's 
                <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                 Operating Unit
                <?php } else { ?> 
                 Department
                <?php } ?> here and link them to locations<br /><br /></h4>
        </div>

        <div class="span6 pull-right">
            <button type="button" class="btn btn-warning adddepartment" data-toggle="modal" data-target=".bs-example-modal-lg" data-department-id="0" data-department-name="">
                <span class="glyphicon glyphicon-plus mr-2" aria-hidden="true"></span> Add 
                <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                Operating Unit
                <?php } else { ?> 
                Department
                <?php } ?>
            </button>
        </div>

        <div class="clearfix"> </div>
    </div>

    <table id="department_table" class="table table-striped table-bordered"  style="width: 100%">
      <thead>
        <tr>
          <th> </th>
          <th>Name</th>
          <th>Locations</th>
          <th>ID</th>
        </tr>
      </thead>

      <tbody>

          <?php
           foreach ($departments as $department) { ?>

              <tr id="department_<?php echo $department['department_id']; ?>">
                    <td>
                        <button type="button" class="btn btn-success btn-sm view-btn notranslate" data-toggle="modal" data-target=".bs-example-modal-lg"
                                data-department-id="<?php echo $department['department_id']; ?>" data-department-name="<?php echo $department['department_name']; ?>">
                            Edit
                        </button>
                    </td>
                    <td class="notranslate"><?php echo $department['department_name']; ?></td>
                    <td><?php echo $department['department_locations']; ?></td>
                    <td><?php echo $department['department_id']; ?></td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>

<div id="department_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add New Departments</h4>
      </div>
      <form action="" onsubmit="saveDepartment();">
      <div class="modal-body">
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-6 valid" style="padding-top: 8px;">
                <label class="control-label">
                     <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                    Operating Unit
                    <?php } else { ?> 
                    Department
                    <?php } ?>
                    <sup style="color: #ff0000;">*</sup></label>
                  <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                     <input type="text" class="form-control department_name notranslate" name="department_name" id="department_name" placeholder="Operating Unit"  value="" required="required" />
                    <?php } else { ?> 
                     <input type="text" class="form-control department_name notranslate" name="department_name" id="department_name" placeholder="Department Name"  value="" required="required" />
                    <?php } ?>
            </div>
        </div>

            <div class="col-md-6 col-sm-6 col-xs-6">
                <label class="control-label">Location <sup style="color: #ff0000;">*</sup></label>
                <select multiple="multiple" name="locations[]" id="locations" class="form-control select2_multiple notranslate" required="required" >
                    <?php foreach ($locations as $location) { ?>
                        <option value="<?php echo $location['location_id']; ?>"  <?php if(!empty($_GET['location_id']) && $location['location_id']==$_GET['location_id']){ echo 'selected="selected"';}?>>
                            <?php echo $location['location_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"> </div>
       
        
      </div>
      <div class="modal-footer">
        <input type="hidden" name="department_id" id="department_id" value="0" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="saveDepartmentButton" <?php echo $disabled; ?> type="submit" class="btn btn-primary submit-btn1">
            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading_icon" style="display: none;"></span>
             <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
             <span>Save Operating Unit</span>
            <?php } else { ?> 
            <span>Save Department</span>
            <?php } ?>
            
        </button>
      </div>
  </form>
    </div>
  </div>
</div>

<script type="text/javascript">

var department_locations = new Array();
<?php foreach ($departments as $department_id => $department_data) { ?>
	department_locations[<?php echo $department_id; ?>] = '<?php if (isset($department_data['locations'])) echo implode(",", $department_data['locations']); ?>';
<?php } ?>

$(document).ready( function() {
    $("#locations").select2({placeholder: 'Select Locations',
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"});

    $('#department_table').dataTable({
        "columnDefs": [{
            "targets": 0,
            "width": "12%",
            "orderable": false
        },
        {
            "targets": 3,
            "visible": false,
            "searchable": false
        }],
        "createdRow": function(row, data, dataIndex) {
            $(row).attr('id', 'department_' + data[3]);
        },
        "order": []
    });
})

$("#department_modal").on('show.bs.modal', function (e) {
    var department_id = $(e.relatedTarget).data('department-id');
    var department_name = $(e.relatedTarget).data('department-name');

    if (department_id == 0)
    {
        $('#myModalLabel').html('Add New '+($("#department_label").val()));
        $('#department_name').val('');
        $('#locations').select2({
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"}).val(null).trigger('change');
    }
    else
    {
        $('#myModalLabel').html('Edit '+($("#department_label").val()));
        $('#department_name').val(department_name);
        
        var current_department_locations = department_locations[department_id];
	    var location_ids = new Array();
	    if (current_department_locations.indexOf(",") >= 0) 
	    	location_ids = current_department_locations.split(',');
	    else if ($.trim(current_department_locations) != "0") 
	    	location_ids.push(current_department_locations);
	    
        $('#locations').select2({
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"}).val(null).trigger('change');
        if (location_ids.length > 0)
            $('#locations').select2({
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"}).val(location_ids).trigger('change');
    }
    $('#department_id').val(department_id);
});

function saveDepartment()
{
    var department_name = $.trim($('#department_name').val());
    var selected_locations = $('#locations').val();

    if (department_name == '') {
     // $('.department_name').css('border-color', 'red');  
     // alert("kjlkjlk");
    }

    if (selected_locations == null) selected_locations = '';
    else selected_locations = selected_locations.toString();

    

    if (department_name != '')
    {

        $('#loading_icon').show();
        var saved_department_id = 0;
        var new_locations_text = "";

        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('departments/save'); ?>', 
            type: 'POST', async: false, dataType: 'json',
            data: { 
          		department_id: $('#department_id').val(), 
          		department_name: department_name,
          		locations: selected_locations,
            },
            success: function(new_department_data) { 
          		saved_department_id = new_department_data.new_department_id; 
          		new_locations_text = new_department_data.new_locations_text; 
            }
        });

        if (saved_department_id == 0) saved_department_id = $('#department_id').val();
        var department_actions_html = '';
        department_actions_html = '<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg"';
        department_actions_html += ' data-department-id="' + saved_department_id + '" data-department-name="' + $.trim($('#department_name').val()) + '">Edit</button>';

		department_locations[saved_department_id] = selected_locations;
        var table = $('#department_table').DataTable();
        table.row('#department_' + saved_department_id).remove().draw();

        $('#department_id').val(0);
        $('#department_name').val('');

        table.row.add([department_actions_html, department_name, new_locations_text, saved_department_id]).draw().node();
        $('#loading_icon').hide();
        $('#department_modal').modal('hide');
    }
}

</script>

<?php if(!empty($_GET['location_id'])){?>
  <script type="text/javascript">
    $(document).ready(function() {
        $('.adddepartment').trigger('click');
        $('#locations').val('<?php echo $_GET['location_id'];?>').trigger('change');
    });
</script>
<?php } ?>
