<?php $expense_statuses = array( 'Pending', 'Approved', 'Cancelled', 'Closed', 'Submitted', 'Paid', 'Declined'); ?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Expenses</h3>
        </div>

        <div class="span6 pull-right">
        	 <button type="button" class="btn btn-warning" onclick="addExpenseModal(event);">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Expense
            </button>

            <!-- <a href="<?php echo AppUrl::bicesUrl('expenses/edit/0'); ?>">
                <button type="button" class="btn btn-warning">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Expense
                </button>
            </a> -->

            <a href="<?php echo AppUrl::bicesUrl('expenses/export'); ?>">
                <button type="button" class="btn btn-success">
                    <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export CSV
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>

    </div>

	<form class="form-horizontal" id="expense_list_form" name="expense_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('expenses/list'); ?>">
		<div class="form-group">
          <div class="col-md-2">
              <input type="text" class="form-control" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date("d/m/Y", strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> >
          </div>
			
          <div class="col-md-2">
              <input type="text" class="form-control" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date("d/m/Y", strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> >
          </div>
			<div class="col-md-2 location">
				<select name="location_id[]" id="location_id"  class="form-control" multiple searchable="Search here.."  onchange="loadDepartments(0);">
					<?php
					$i = 0 ;
					foreach ($locations as $location) {
						if(isset($location_id[$i])){
							?>
							<option value="<?php echo $location['location_id']; ?>"
								<?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
								<?php echo $location['location_name']; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $location['location_id']; ?>">
								<?php echo $location['location_name']; ?>
							</option>
						<?php } ?>
						<?php
						$i++;
					} ?>
				</select>
			</div>
			<div class="col-md-2 department">
				<select name="department_id[]" id="department_id" class="form-control" multiple searchable="Search here..">
					<option value="0">All Departments</option>
					<?php if(!empty($department_info))
                        foreach($department_info as $dept_value){?>
                        <option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
                    <?php } ?>
				</select>
			</div>
			<div class="col-md-2 status">
				<select name="expense_status[]" id="expense_status" multiple class="form-control">
					<?php
					$i = 0 ;
					foreach ($expense_statuses as $expense_status_display) {
						if(isset($expense_status[$i])){
							?>
							<option value="<?php echo $expense_status_display; ?>"
								<?php if ($expense_status[$i] == $expense_status_display) echo ' selected="SELECTED" '; ?>>
								<?php echo $expense_status_display; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $expense_status_display; ?>">
								<?php echo $expense_status_display; ?>
							</option>
						<?php }
						$i++;
					} ?>
				</select>
			</div>

			<div class="col-md-2">
				<button class="btn btn-primary" onclick="$('#expense_list_form').submit();">Search Travel Expenses</button>
			</div>
		</div>
	</form>
<div class="clearfix"> </div>
<div class="row tile_count"> </div>
<div class="col-md-12">
	<?php if(Yii::app()->user->hasFlash('success')):?>
	    <div class="alert alert-success">
	        <?php echo Yii::app()->user->getFlash('success'); ?>
	    </div>
	<?php endif; ?></div>


    <table id="expense_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Action</th>
          <th>ID</th>
          <th>Name</th>
          <th>Date</th>
<!--          <th>Description</th>-->
		  <th>Amount in Tool Currency</th>
		  <th>Amount in Order Currency</th>
          <th>User</th>
          <th style="width: 100px;">Status</th>
        </tr>
      </thead>

      <tbody>

          <?php foreach ($expenses as $expense) { ?>
              <tr>
                    <td>
                        <a href="<?php echo AppUrl::bicesUrl('expenses/edit/' . $expense['expense_id']); ?>">
                            <button class="btn btn-sm btn-success">View/Edit</button>
                        </a>
                    </td>
                    <td style="text-align: right;"><?php echo $expense['expense_id']; ?></td>
                    <td><?php echo $expense['expense_name']; ?></td>
                    <td><?php echo date("F j, Y", strtotime($expense['expense_date'])); ?></td>
<!--                    <td>--><?php //echo $expense['description']; ?><!--</td>-->

					  <td style="text-align: right;">
						  <nobr>
							  <?php
							  /*switch ($expense['currency']) {
								  case 'GBP' : $currency = '&#163;';
									  break;
								  case 'USD' : $currency = '$';
									  break;
								  case 'CAD' : $currency = '$';
									  break;
								  case 'CHF' : $currency = '&#8355;';
									  break;
								  case 'EUR' : $currency = '&#8364;';
									  break;
								  case 'JPY' : $currency = '&#165;';
									  break;
								  case 'INR' : $currency = '&#x20B9;';
									  break;
								  default :  $currency = '&#163;';
									  break;
							  }*/
							   $currency = $expense['currency_symbol'];
							  echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($expense['calc_func_expenses_gb'], 2); ?>
						  </nobr>
					  </td>
					  <td style="text-align: right;">
						  <nobr>
							  <?php
							  echo $currency . ' ' . number_format($expense['calc_func_expenses'], 2);
							  ?>
						  </nobr>
					  </td>
                    <td><a href="<?php echo AppUrl::bicesUrl('users/edit/' . $expense['user_id']); ?>"><?php echo $expense['user_name']; ?></a></td>
                    <td>
                    	<?php
                    		if (empty($expense['expense_status'])) $expense['expense_status'] = 'Pending';
                    		$status_style = "";
							
							if ($expense['expense_status'] == 'Paid' || $expense['expense_status'] == 'Closed' || $expense['expense_status'] == 'Received')
								$status_style = ' style="border: 1px solid green !important; background-color: green !important; color: #fff !important;" ';
							if ($expense['expense_status'] == 'Declined')
								$status_style = ' style="border: 1px solid #990000 !important; background-color: #990000 !important; color: #fff !important;" ';								
							if ($expense['expense_status'] == 'More Info Needed')
								$status_style = ' style="border: 1px solid #CC3333 !important; background-color: #CC3333 !important; color: #fff !important;" ';								
							if ($expense['expense_status'] == 'Pending' || $expense['expense_status'] == 'Ordered - PO Not Sent To Supplier' || $expense['expense_status'] == 'Ordered' 
										|| $expense['expense_status'] == 'Submitted' || $expense['expense_status'] == 'Ordered - PO Sent To Supplier')
								$status_style = ' style="border: 1px solid orange !important; background-color: orange !important; color: #fff !important;" ';
						    if ($expense['expense_status'] == 'Cancelled')
							$status_style = ' style="border: 1px solid grey !important; background-color: grey !important; color: #fff !important;" ';


						    if($expense['expense_status'] == 'Pending') {
								$toolTip = "Travel and Expense has been created by " . $expense['user'] . " however not submitted for approval.";
							}
							elseif($expense['expense_status'] == 'Submitted'){
								$toolTip = "Travel and Expense has been submitted  by ".$expense['user']." and is awaiting approval.";
							}
							elseif($expense['expense_status'] == 'More Info Needed'){
								$toolTip = "Approver has requested more information from the travel and expense creator.";
							}
							elseif($expense['expense_status'] == 'Declined'){
								$toolTip = "Approver has declined the travel and expense. Please read approver notes.";
							}
							elseif($expense['expense_status'] == 'Approved'){
								$toolTip = "Travel and Expense has been approved.";
							}

							elseif($expense['expense_status'] == 'Paid'){
								$toolTip = "Travel and Expense has been paid .";
							}
							elseif($expense['expense_status'] == 'Received'){
								$toolTip = "Travel and Expense has been received.";
							}
							elseif($expense['expense_status'] == 'Ordered'){
								$toolTip = "Travel and Expense has been ordered.";
							}
							elseif($expense['expense_status'] == 'Closed'){
								$toolTip = "Travel and Expense has been closed.";
							}
							else{
								$toolTip = 'N/A';
							}
						?>
                    	<button class="btn btn-sm btn-success status" title="<?php echo $toolTip; ?>" <?php echo $status_style; ?>>
                    		<?php echo $expense['expense_status']; ?>
                    	</button>
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>
<style>
	.ui-tooltip {
		width: 200px;
		text-align: center;
		box-shadow: none;
		padding: 0;
	}
	.ui-tooltip-content {
		position: relative;
		padding: 0.5em;
	}
	.ui-tooltip-content::after, .ui-tooltip-content::before {
		content: "";
		position: absolute;
		border-style: solid;
		display: block;
		left: 90px;
	}
	.bottom .ui-tooltip-content::before {
		bottom: -10px;
		border-color: #AAA transparent;
		border-width: 10px 10px 0;
	}
	.bottom .ui-tooltip-content::after {
		bottom: -7px;
		border-color: white transparent;
		border-width: 10px 10px 0;
	}
	.top .ui-tooltip-content::before {
		top: -10px;
		border-color: #AAA transparent;
		border-width: 0 10px 10px;
	}
	.top .ui-tooltip-content::after {
		top: -7px;
		border-color: white transparent;
		border-width: 0 10px 10px;
	}

	.location  .multiselect {
		width: 138%;
	}
	.status  .multiselect {
		width: 100%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}

	.btn .caret {
		float: right;
		margin-top: 10px;

	}
  .department  .multiselect {
		width: 112%;
	}
</style>

<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions2(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Departments',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }


	function getOptions1(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Status Values',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	$('#location_id').multiselect(getOptions(true));
	 $('#department_id').multiselect(getOptions2(true));
	$('#expense_status').multiselect(getOptions1(true));

$.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    autoUpdateInput: false,
    singleClasses: "picker_3",
	locale: {
	    format: 'DD/MM/YYYY'
	}
  });
};

$(document).ready( function() {
    $('#expense_table').dataTable({
        "columnDefs": [ 
            { "targets": 0, "width": "6%", "orderable": false },
            { "type": "date", targets: 3}            
        ],
        "order": [[3,'desc']],
        "language": {"search": "General Search:"}
    });

	$('#from_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$('#to_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$( ".status" ).tooltip({
		position: {
			my: "center bottom",
			at: "center top-10",
			collision: "flip",
			using: function( position, feedback ) {
				$( this ).addClass( feedback.vertical )
					.css( position );
			}
		}
	});

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			//loadDepartments(<?php //echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>
		
});

</script>
