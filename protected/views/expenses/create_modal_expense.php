<?php $record_user_id = Yii::app()->session['user_id'];?>
<div class="" role="main">

  <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3 class="pull-left"><?php echo 'Add Expense';?></h3>
  <div class="pull-right" style="text-align:center;padding-right: 21px;padding-top: 3px;">
  <span class="step"></span>
  <span class="step"></span>
</div></div>
<div class="clearfix"> </div>
  <div class="col-md-12">
  <?php if(Yii::app()->user->hasFlash('error')):?>
      <div class="alert alert-danger">
          <?php echo Yii::app()->user->getFlash('error'); ?>
      </div>
  <?php endif; ?></div>
  <div id="alert-info" class="alert alert-info alert-dismissable" style="display: none;">
      <a style="margin-top: 2px;" onclick="$('#alert-info').hide();" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
      <strong>This expense was not submitted since no route was found for approving the same.</strong>
  </div>
<div class="tile_count" role="tabpanel" data-example-id="togglable-tabs">
  <form id="expense_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('expenses/edit'); ?>">

    <div class="tab"> <h3 class="col-md-12 col-sm-12 col-xs-12" style="font-size: 16px !important;">Expense Information</h3>
    <input type="hidden" name="user_id" value="<?php echo !empty($expense['user_id'])?$expense['user_id']:Yii::app()->session['user_id'];?>">
    
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Location <span style="color: #a94442;">*</span></label>
        <select required name="location_id" id="location_id" class="form-control" onchange="loadDepartmentsForSingleLocation(this);">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($expense['location_id']) && $location['location_id'] == $expense['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Department <span style="color: #a94442;">*</span></label>
              <select required name="department_id"  id="department_id_new" class="form-control">
                  <option value="">Select Department</option>
              </select>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6 valid">
              <label class="control-label">Project</label>
              <select name="project_id" id="project_id" class="form-control">
                  <option value="">Select Project</option>
                  <?php foreach ($projects as $project) { ?>
                      <option value="<?php echo $project['project_id']; ?>"
                          <?php if (isset($expense['project_id']) && $project['project_id'] == $expense['project_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $project['project_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
           <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Expense title e.g. trip name <span style="color: #a94442;">*</span></label>
              <input required type="text" class="form-control" name="expense_name" id="expense_name"
                    <?php if (isset($expense['expense_name']) && !empty($expense['expense_name'])) echo 'value="' . $expense['expense_name'] . '"'; else echo 'placeholder="Expense title e.g. trip name"'; ?> >
          </div>

          <!-- <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Spend Type</label>
              <select name="spend_type" id="spend_type" class="form-control">
                  <option value="">Select Spend Type</option>
                  <option  <?php if (isset($expense['spend_type']) && $expense['spend_type'] == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                  <option  <?php if (isset($expense['spend_type']) && $expense['spend_type'] == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
              </select>
          </div> -->
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Currency Rate <span style="color: #a94442;">*</span></label>
              <select name="currency_id" id="currency_id" class="form-control" onchange="loadCurrencyRate();" required="required">
                <option value="">Select Currency</option>
                  <?php foreach ($currency_rate as $rate) { ?>
                      <option value="<?php echo $rate['id']; ?>"
                          <?php if (isset($expense['currency_id']) && $rate['id'] == $expense['currency_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $rate['currency']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Currency Rate</label>
              <input readonly type="text" class="form-control" name="currency_rate" id="currency_rate"
                  <?php if (isset($expense['currency_rate']) && !empty($expense['currency_rate'])) { echo 'value="' . $expense['currency_rate'] . '"'; } else { echo 'value="1"'; } ?> >

          </div>

      </div>


      
      <div class="form-group">
          <?php

            $orderObj = new Order();
          if (isset($expense['location_id'])) { $location_id = $expense['location_id']; } else {$location_id = 0;}
          if (isset($expense['department_id'])) { $department_id = $expense['department_id']; } else {$department_id = 0;}
          $approve = $orderObj->approveStatus($location_id,$department_id);
            if($approve['travel_expense']==1){
                $applicable_expense_statuses = array('Pending', 'Approved', 'Closed', 'Declined', 'Submitted');
            } else {
                $applicable_expense_statuses = array('Pending','Closed', 'Declined', 'Submitted');
            }

          
      if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];
      if (isset($expense['user_id']) && !empty($expense['user_id'])) $record_user_id = $expense['user_id'];

            $applicable_expense_statuses = $applicable_expense_statuses;
            if (!isset($expense['expense_status']) || $expense['expense_status'] == 'Pending'  || $expense['expense_status'] == 'More Info Needed')
      {
        if (!isset($expense['expense_status']))
                  $applicable_expense_statuses = array('Pending', 'Submitted');
                  else $applicable_expense_statuses = array($expense['expense_status'], 'Submitted','Declined');
      }

      if(!empty($expense['location_id'])){
          $userPer = new User;
          $appOrder = $userPer->checkPermission(Yii::app()->session['user_id'],$expense['location_id'],$expense['department_id'],"approve_travel_expense");
          if(isset($appOrder) && $appOrder["approve_travel_expense"]=="yes" && in_array(Yii::app()->session['user_type'],array(1,3,4))){
             $applicable_expense_statuses = array('Pending', 'Approved', 'Closed', 'Declined', 'Submitted');
          }else{
            if(strtolower($expense['expense_status'])=='submitted'){
              $applicable_expense_statuses = array('Submitted');
            }else{
              $applicable_expense_statuses = array($expense['expense_status'],'Pending', 'Submitted');
            }

          }

      }else{
          $applicable_expense_statuses = array('Pending', 'Submitted');
        }

        $class="col-md-12 col-sm-12 col-xs-12";
        //$class="col-md-3 col-sm-3 col-xs-6";
          ?>
           <div class="form-group">
          <div class="<?php echo $class?>">
              <label class="control-label">User Name</label>
              <select class="form-control" disabled="disabled">
                  <?php foreach ($users as $user) { 


                    ?>
                      <option value="<?php echo $user['user_id']; ?>"
                                <?php if ($record_user_id == $user['user_id']) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $user['full_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div></div> 
          <?php /*<div class="<?php echo $class;?>">
              <label class="control-label">Status</label>
              <select class="form-control" name="expense_status" id="expense_status"
                        <?php if (isset($expense['expense_status']) && $expense['expense_status'] != 'Pending' && $expense['expense_status'] != 'More Info Needed' && (!isset($appOrder) || $appOrder["approve_travel_expense"]!="yes")) echo ' disabled="disabled" '; ?>>
                  <?php foreach ($applicable_expense_statuses as $expense_status) { ?>
                      <option value="<?php echo $expense_status; ?>"
                                <?php if (isset($expense['expense_status']) && $expense['expense_status'] == $expense_status) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $expense_status; ?>
                      </option>
                  <?php } ?>
              </select>
          </div> */?>

          <?php if (!empty($expense['expense_status'])){?>
          <div class="form-group">
            <div class="<?php echo $class;?>">
               <label class="control-label">Status</label>

          <?php } else {?> <div><div><?php } if (!empty($expense['expense_status'])){?>
            <input type="text" name="expense_status" class="form-control" id="expense_status" readonly="readonly" value="<?php echo $expense['expense_status']?>" readonly="readonly"/>
          <?php } else if (empty($expense_id)){?>
            <input type="hidden" name="expense_status" id="expense_status_new" />
          <?php }else {?> 
            <div class="form-group"> 
            <div class="<?php echo $class;?>">
            <select class="form-control" name="expense_status" id="expense_status"
                        <?php if (isset($expense['expense_status']) && $expense['expense_status'] != 'Pending' && $expense['expense_status'] != 'More Info Needed' && (!isset($appOrder) || $appOrder["approve_travel_expense"]!="yes")) echo ' disabled="disabled" '; ?>>
                  <?php foreach ($applicable_expense_statuses as $expense_status) { ?>
                      <option value="<?php echo $expense_status; ?>"
                                <?php if (isset($expense['expense_status']) && $expense['expense_status'] == $expense_status) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $expense_status; ?>
                      </option>
                  <?php } ?>
              </select>
          <?php } ?></div></div>
      </div>
      <div class="clearfix"> </div>

      <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label">Expense description/notes</label>
              <textarea class="form-control" name="description" id="description" rows="4" <?php if (!isset($expense['description']) || empty($expense['description'])) echo 'placeholder="Expense description/notes/comments if any ..."'; ?>><?php if (isset($expense['description']) && !empty($expense['description'])) echo $expense['description']; ?></textarea>
          </div>
      </div>
      <div class="clearfix"> <br /> </div>

</div>
    <!--
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <a style="text-decoration: underline; cursor: pointer;" onclick="$('#add_vendor_modal').modal('show');">
            Click here</a> to add a new vendor not found via the database search in expense details below.
          </div>
      </div>
      <div class="clearfix"> <br /> </div>
    -->
    
      <?php
        $existing_items_found = false;
        if (isset($expense_details) && is_array($expense_details) && count($expense_details))
            $existing_items_found = true;
      ?>

    <div class="tab"> <h3 class="col-md-12 col-sm-12 col-xs-12" style="font-size: 16px !important;">Expense Details  </h3>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>
                  
                  <?php if ($existing_items_found && in_array($expense['expense_status'],array('Pending','More Info Needed'))) { ?>
                      <div class="pull-right" style="font-size: 70%; padding-top: 5px;">
                          <a style="cursor: pointer; text-decoration: underline;"
                                onclick="$('#here_is_where_you_add_new_items_to_this_expense').show();">
                              Click here</a>
                          To Add More Expenses To This Request
                      </div>
                  <?php } ?>
              </h4>
          </div>
      </div>

      <?php
        $total_expenses = 0;

        if (isset($expense_details) && is_array($expense_details) && count($expense_details))
        {
      ?>

          <?php
            foreach ($expense_details as $expense_detail)
            {
                if (!isset($expense_detail['expense_price']) || empty($expense_detail['expense_price'])) 
                  $expense_detail['expense_price'] = 0;
                 
          ?>

              <div class="form-group">
                  <div class="col-md-4 col-sm-4 col-xs-12" valid>
                      <label class="control-label">Expense Type <span style="color: #a94442;">*</span></label>
                      <select class="form-control" name="expense_type_id[]" onchange="createDateRangePicker();" required="required">
                        <option value="">Expense Type</option>
                        <?php foreach ($expense_types as $expense_type) { ?>
                          <option value="<?php echo $expense_type['id']; ?>"
                              <?php if (isset($expense_detail['expense_type_id']) && $expense_detail['expense_type_id'] == $expense_type['id']) echo ' selected="SELECTED" '; ?>>
                            <?php echo $expense_type['value']; ?>
                          </option>
                        <?php } ?>
                      </select>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label">Expense Date</label>
                      <input type="text" class="<?php if(in_array($expense['expense_status'],array('Pending','More Info Needed'))){ echo 'expense_date_picker'; }?> form-control" name="expense_date[]"
                            <?php if (isset($expense_detail['expense_date']) && !empty($expense_detail['expense_date'])) echo 'value="' . date("d/m/Y", strtotime($expense_detail['expense_date'])) . '"'; ?> readonly>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label">Amount</label>
                      <input style="text-align: right;" type="text" class="expense_price price_calc form-control" name="expense_price[]"
                            <?php if (isset($expense_detail['expense_price']) && !empty($expense_detail['expense_price'])) echo 'value="' . number_format($expense_detail['expense_price'], 2) . '"'; ?> <?php if(!in_array($expense['expense_status'],array('Pending','More Info Needed'))){ echo "readonly";}?>>
                  </div>
                  <div class="clearfix"> <br /><br /> </div>

          <?php
              $current_price = $current_tax = 0;
            if (isset($expense_detail['expense_price'])) $current_price = $expense_detail['expense_price'];
            if (isset($expense_detail['tax_rate'])) $current_tax = $expense_detail['tax_rate'];
            $current_tax_amount = ($current_tax * $current_price) / 100.0; 
          ?>
                  <div class="col-md-2 col-sm-2 col-xs-4" style="padding: 8px; font-size: 120%;">
                      <label class="control-label">&nbsp;</label>
                      % Tax Rate Applied Is
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Tax Rate</label>
                      <input style="text-align: right;" type="text" class="tax_rate price_calc form-control" name="tax_rate[]"
                            <?php if (isset($expense_detail['tax_rate']) && !empty($expense_detail['tax_rate'])) echo 'value="' . number_format($expense_detail['tax_rate'], 2) . '"'; ?> <?php if(!in_array($expense['expense_status'],array('Pending','More Info Needed'))){ echo "readonly";}?> />
                </div>
                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Tax Amount</label>
                      <input style="text-align: right;" type="text" class="tax_amount form-control" name="tax_amount[]" 
                          readonly="readonly" <?php echo 'value="' . number_format($current_tax_amount, 2) . '"'; ?> />
                </div>
                  <div class="clearfix"> <br /><br /> </div>

                  <div class="col-md-6 col-sm-6 col-xs-12">
                <a style="text-decoration: underline; cursor: pointer;" onclick="$('#add_vendor_modal').modal('show');">
                Click here</a> to add a new vendor not found via the database search in expense details below.
                </div>
                  <div class="clearfix"> <br /> </div>

                  <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                      <label class="control-label">Vendor Name</label>
                      <input type="text" class="form-control has-feedback-left" name="vendor_name[]"
                            <?php if (isset($expense_detail['vendor_name']) && !empty($expense_detail['vendor_name'])) echo 'value="' . $expense_detail['vendor_name'] . '"'; ?> />
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      <input class="vendor_id" type="hidden" name="vendor_id[]" value="<?php echo $expense_detail['vendor_id']; ?>" />
                  </div>
                  <div class="clearfix"> <br /><br /> </div>

                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <label class="control-label">Expense Notes</label>
                      <input type="text" class="form-control" name="expense_notes[]"
                            <?php if (isset($expense_detail['expense_notes']) && !empty($expense_detail['expense_notes'])) echo 'value="' . $expense_detail['expense_notes'] . '"'; ?> >
                  </div>

                  <div class="col-md-2">
                      <a onclick="$(this).parent().parent().remove(); calculateTotalPrice(); return false;" class="btn btn-link">
                          <span class="fa fa-remove hidden-xs"></span>
                      </a>
                  </div>
              </div>

          <?php $total_expenses += $expense_detail['expense_price'] + $current_tax_amount; } ?>

      <?php
            }
      ?>

      <div id="here_is_where_you_add_new_items_to_this_expense"
            <?php if ($existing_items_found) echo ' style="display: none;" '; ?>>
      <fieldset id="additional-field-model">
          <div class="form-group">
              <div class="col-md-4 col-sm-4 col-xs-12 valid">
                  <label class="control-label">Expense Type <span style="color: #a94442;">*</span></label>
                 <select class="form-control" name="expense_type_id[]" onchange="createDateRangePicker();" required="required">
                    <option  value="">Expense Type</option>
                    <?php foreach ($expense_types as $expense_type) { ?>
                      <option value="<?php echo $expense_type['id']; ?>">
                        <?php echo $expense_type['value']; ?>
                      </option>
                    <?php } ?>
                  </select>
              </div>

              <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Expense Date</label>
                  <input type="text" class="expense_date_picker form-control" name="expense_date[]" placeholder="Date" />
              </div>

              <div class="col-md-4 col-sm-4 col-xs-12 valid">
                  <label class="control-label">Expense Price <span style="color: #a94442;">*</span></label>
                  <input required style="text-align: right;" type="text" class="expense_price price_calc form-control" name="expense_price[]" placeholder="Amount" />
              </div>
              <div class="clearfix"> <br /><br /> </div>

              <div class="col-md-4 col-sm-4 col-xs-12" style="padding: 33px 8px 8px 8px; font-size: 120%;">

                  % Tax Rate Applied Is
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Tax Rate</label>
                  <input style="text-align: right;" type="text" class="tax_rate price_calc form-control" name="tax_rate[]" />
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                  <label class="control-label">Tax Amount</label>
                  <input style="text-align: right;" type="text" class="tax_amount form-control" readonly="readonly" name="tax_amount[]" />
              </div>
              <div class="clearfix"> <br /><br /> </div>

              <div class="col-md-6 col-sm-6 col-xs-12">
              <a style="text-decoration: underline; cursor: pointer;" onclick="$('#add_vendor_modal').modal('show');">
              Click here</a> to add a new vendor not found via the database search in expense details below.
              </div>
              <div class="clearfix"> <br /> </div>

              <div class="col-md-5 col-sm-5 col-xs-10 date-input valid">
                  <label class="control-label">Supplier <span style="color: #a94442;">*</span></label>
                  <input required type="text" class="form-control has-feedback-left" name="vendor_name[]" placeholder="Supplier" />
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  <input class="vendor_id" type="hidden" name="vendor_id[]" value="0" />
              </div>
             

              <div class="col-md-5 col-sm-6 col-xs-10">
                  <label class="control-label">Notes</label>
                  <input type="text" class="form-control" name="expense_notes[]" placeholder="Notes about this expense" />
              </div>

              <div class="col-md-2" style="margin-top: 20px;">
                  <label class="control-label">&nbsp;</label>
                  <a href="javascript:void(0);" class="btn btn-link remove-this-field">
                      <span class="fa fa-remove hidden-xs"></span>
                  </a>
                  <a href="javascript:void(0);" class="btn btn-link create-new-field">
                      <span class="fa fa-plus hidden-xs"></span>
                  </a>
              </div>
          </div>
      </fieldset>
      <div class="clearfix"> </div>
      </div>
      <div class="clearfix"> <br /> </div>
      

<!--      <div class="form-group">-->
<!--          <div class="col-md-5 col-sm-5 col-xs-10">-->
<!--              <h4>Upload Receipts and Invoices</h4>-->
<!--          </div>-->
<!--          <div class="col-md-1 col-sm-1 col-xs-2">-->
<!--            --><?php //if (isset($expense_id) && $expense_id) { ?>
<!--                <button class="btn btn-sm btn-primary" onclick="uploadReceipts(); return false;">Upload</button>-->
<!--            --><?php //} ?>
<!--          </div>          -->
<!--      </div>     -->
    <?php 
    $uploadCondArr = array('pending','more info needed');
    if(empty($expense_id) || (!empty($expense_id) && in_array(strtolower(trim($expense['expense_status'])),$uploadCondArr))){?>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h5>Upload Documents</h5>
          </div>
      </div>
      <div class="form-group" id="document_area_1">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label">File/Document</label>
              <input class="form-control" type="file" name="expense_file_1" id="expense_file_1" />
          </div>
           <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label">File/Document Description</label>
              <textarea class="form-control" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" rows="3" /></textarea> 
          </div>
        <!--   <div class="col-md-3 col-sm-3 col-xs-2"><br /><br />
              <a onclick="deleteDocument(1);" title="Click here to delete this document" style="cursor: pointer;">
                  <span class="fa fa-minus fa-2x"></span>
              </a>
              &nbsp;
              <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
                  <span class="fa fa-plus fa-2x"></span>
              </a>
          </div> -->
          <div class="clearfix"> </div>
         
          <div class="col-md-12 col-sm-12 col-xs-12 text-right">
              <button id="expense_upload_1" class="btn btn-info upload-doc" onclick="uploadDocument(1,event);" style="margin-top:4%">Upload</button>
          </div>
      </div><?php } ?>
      <input type="hidden" name="delete_document_flag_1" id="delete_document_flag_1" value="0" />
      <div class="clearfix" id="end_document_area_1"> <br /> </div>
      <input type="hidden" name="total_documents" id="total_documents" value="1" />
      <div class="clearfix"> <br /> </div>
      <div id="uploaded_file_list" class="col-md-6 col-sm-6 col-xs-12"></div>
      <div id="new_document_code">
          <div class="form-group" id="document_area_DOCIDX">
              <div class="col-md-5 col-sm-5 col-xs-10">
                  <input class="form-control" type="file" name="expense_file_DOCIDX" id="expense_file_DOCIDX" />
              </div>
              <div class="col-md-1 col-sm-1 col-xs-2">

                  <a onclick="deleteDocument(DOCIDX);" title="Click here to delete this document" style="cursor: pointer;">
                      <span class="fa fa-minus fa-2x"></span>
                  </a>
                  <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
                      <span class="fa fa-plus fa-2x"></span>
                  </a>
              </div>
              <div class="clearfix"> </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                  <label class="control-label">File/Document Description</label>
                  <textarea class="form-control"  class="form-control" name="file_desc_DOCIDX" id="file_desc_DOCIDX" placeholder="File/Document Description" rows="3" ></textarea>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
              <button id="expense_upload_DOCIDX" class="btn btn-info upload-doc" onclick="uploadDocument(DOCIDX,event);">Upload</button></div>
          </div>
          <input type="hidden" name="delete_document_flag_DOCIDX" id="delete_document_flag_DOCIDX" value="0" />
          <div class="clearfix" id="end_document_area_DOCIDX"> <br /> </div>
      </div>
      <div class="clearfix"> </div>

    <?php 
        // integer starts at 0 before counting
        $existing_files = array(); 
        $upload_dir = 'uploads/expenses/';
      if ($expense_id)
      {
        if (!is_dir('uploads/expenses')) mkdir('uploads/expenses');
        if (!is_dir('uploads/expenses/' . $expense_id)) 
          mkdir('uploads/expenses/' . $expense_id);
        $upload_dir = 'uploads/expenses/' . $expense_id . '/';
          if ($handle = opendir($upload_dir)) {
              while (($uploaded_file = readdir($handle)) !== false){
                  if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file)) 
                      $existing_files[] = $uploaded_file;
              }
          }
      }
    ?>
    
    <?php if (is_array($existing_files) && count($existing_files)) { ?>

          <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <h5>Existing Receipts/Files</h5>
              </div>
          </div>
          
            <div class="col-md-6 col-sm-6 col-xs-12">
          <table class="table">
          <?php $file_idx = 1; foreach ($existing_files as $uploaded_file) {
            $sql = 'select * from expense_files where expense_id='.$expense_id.' and file_name="'.$uploaded_file.'"';
            $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
           ?>
            <tr id="existing_file_id_<?php echo $file_idx; ?>">
              <td>
                        <a href="<?php echo AppUrl::bicesUrl('uploads/expenses/' . $expense_id . '/' . $uploaded_file); ?>" target="expense_file">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
                        &nbsp;
            <a id="delete_expense_file_link_<?php echo $file_idx; ?>" style="cursor: pointer;" 
                onclick="deleteExpenseFile(<?php echo $file_idx; ?>, '<?php echo $uploaded_file; ?>');">
               <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a>                        
                       </a>
              </td>
              <td>
                <?php echo $uploaded_file; ?>
              </td>
              <td>
                <?php echo !empty($fileReader['description'])?$fileReader['description']:'';?>
              </td>
            </tr>
          <?php $file_idx += 1; } ?>
          </table>
          </div>
               
          <div class="clearfix"> <br /> </div>
    
        <?php } ?>
        
<!--    <div class="form-group">-->
<!--        <div class="col-md-6 col-sm-6 col-xs-12">-->
<!--            <input multiple="multiple" class="file" name="file[]" id="file" type="file" />-->
<!--      </div>-->
<!--    </div>-->
    <div class="clearfix"> </div>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" name="total_expenses" id="total_expenses" value="<?php echo $total_expenses; ?>" />
            <input type="hidden" name="expense_id" id="expense_id" value="<?php if (isset($expense_id)) echo $expense_id; ?>" />
            <input type="hidden" name="created_by_user_id" id="created_by_user_id" value="<?php if (isset(Yii::app()->session['user_id'])) echo Yii::app()->session['user_id']; else echo '0'; ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
            <input type="hidden" name="files_only_upload" id="files_only_upload" value="0" />
            <input type="hidden" name="user_name" id="user_name" value="<?php echo Yii::app()->session['full_name'];?>" />
            <input type="hidden" name="user_email" id="user_email" value="<?php echo Yii::app()->session['current_user_email'];?>" />
        </div>
    </div>
</div>
</form>
</div>



<?php if (isset($approvals) && is_array($approvals) && count($approvals)) { ?>
      <div class="clearfix"> <br /> </div>
      <div class="form-group">
          <div class="col-md-4 col-sm-4 col-xs-8">
              <h4>Approval Routing</h4>  
          </div>
          <?php if (in_array(Yii::app()->session['user_type'],array(1,3,4)) && isset($appOrder) && $appOrder["approve_travel_expense"]=="yes" ) { ?>            
              <?php 
                  $last_apporval_id = 0;
                  foreach ($approvals as $an_approval)
            if ($an_approval['user_id'] == Yii::app()->session['user_id']) 
                      $last_apporval_id = $an_approval['id'];  
              ?>
            <div class="col-md-2 col-sm-2 col-xs-4" style="text-align: right;">
              <?php if ($last_apporval_id) { ?>
                <a href="<?php echo AppUrl::bicesUrl('routing/edit/' . $last_apporval_id); ?>">
                    <button class="btn btn-sm btn-primary">
                      Approve Expense
                    </button>
                </a>
             <?php } ?>
            </div>
        <?php } ?>
      </div>     
      <div class="clearfix"> <br /> </div>
      
      <div class="col-md-6 col-sm-6 col-xs-12">
      <table class="table">
          <thead><tr><th>User</th><th>Date</th><th>Status</th><th>Comments</th></tr></thead>
         <?php foreach ($approvals as $an_approval) { ?>
              <tr>
                <td><?php echo $an_approval['user']; ?></td>
                <td><?php echo date("F j, Y", strtotime($an_approval['routing_date'])); ?></td>
                <td>
                  <?php 
                  if (true) echo $an_approval['routing_status'];
                  ; 
                  ?>
                </td>
                <td><?php echo $an_approval['routing_comments']; ?></td>
              </tr>
          <?php 
        } ?>
      </table>
    </div>      
      
      <div class="clearfix"> </div>
<?php } ?>

<div class="form-group" id="total_price_cont_exp">
    <div class="col-md-10 col-sm-10 col-xs-10 text-center">
        <h3>Total Price
        <span id="total_expenses_display" style="display: inline-block;">
            <?php

               if (!empty($expense['currency_id'])) {
                  $symbol = FunctionManager::currencySymbol($expense['currency_id']);
                }else{
                   $symbol = Yii::app()->session['user_currency_symbol'];
                }
             echo $symbol . number_format($total_expenses, 2); ?>
        </span>
    </div>
    
    <div class="clearfix"> </div>
</div>


<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php 

        if (isset($expense['expense_status'])  && strtolower($expense['expense_status']) == 'approved' && (isset($appOrder) && $appOrder["approve_travel_expense"] ="yes")) { ?>
          <a href="javascript:void(0);" onclick="
          if(confirm('Are you sure to Cancel?')){
            $('#expense_status_edit').val('Cancelled');$('#expense_form_edit').submit();
          }"><input type="submit" class="btn btn-danger pull-right" value="Cancel" name="expense_status"></button></a>

        <?php }else if (isset($expense['expense_status'])  && (!isset($appOrder) || $appOrder["approve_travel_expense"] !="yes") && $expense['expense_status'] != 'Pending' && $expense['expense_status'] != 'More Info Needed') { ?>
            <div class="alert alert-warning fade in" role="alert" style="margin-top: 20px;">
                <span style="font-size: 120%;">This expense has already been submitted and hence cannot be edited.</span>
            </div>
        <?php } else { ?>

              <?php 
              if(isset($expense['expense_status']) && in_array(strtolower(trim($expense['expense_status'])),array('pending','more info needed'))){
                ?> <br />
                <a href="javascript:void(0);" onclick="$('#expense_status').val('Submitted');$('#expense_form').submit();"><input type="submit" class="btn btn-success  pull-right" value="Submit for Approval" name="expense_status"></button></a>
                    <?php 
              }
              else if (empty($expense_id)){
                      ?> <!-- <a href="javascript:void(0);" onclick="$('#expense_form').submit();"><button type="button" class="btn btn-primary">Edit expense </button></a> --><?php
                    /*} else {*/?><br />
                      <!--  <a href="javascript:void(0);" onclick="$('#expense_status').val('Pending');$('#expense_form').submit();"><input type="submit" class="btn btn-primary" value="Save Expense and Not Submit" name="expense_status" /></a>

                       <a href="javascript:void(0);" onclick="$('#expense_status').val('Submitted');$('#expense_form').submit();"><input type="submit" class="btn btn-success" value="Submit for Approval" name="expense_status"></button></a> -->
                    <?php }


                     ?>
               
         
        <?php } ?>
    </div>
    <div class="clearfix"></div> <div id="loader_expense" class="text-center"><img style="width: 43px;height: 43px;" src="<?php echo AppUrl::bicesUrl('images/ajax-loader.gif'); ?>"></div>
</div>
 <div style="overflow:auto;">
  <div style="float:right; margin:0 5px;" id="submitBtnCont">
    <button type="button" class="btn btn-info" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
    <button type="button" class="btn btn-success" id="nextBtn" onclick="nextPrev(1)">Next</button>

     

    <a href="javascript:void(0);" class="sub_btns" onclick="sbtFormExpense(event,'Pending');"><input type="submit" class="btn btn-primary" value="Save Expense and Not Submit" name="expense_status" /></a>

    <a href="javascript:void(0);" class="sub_btns" onclick="sbtFormExpense(event,'Submitted');"><input type="submit" class="btn btn-success" value="Submit for Approval" name="expense_status"></button></a>
  </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->

<?php  
   if (!empty($expense['currency_id'])) {
      $symbol = FunctionManager::currencySymbol($expense['currency_id']);
    }else{
       $symbol = Yii::app()->session['user_currency_symbol'];
    }

 ?>

<input type="hidden" name="user_currency_symbol" id="user_currency_symbol" value="<?php $symbol;?>" />

<!-- Status Change form -->
<form id="expense_form_edit" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('expenses/changeStatus'); ?>">

  <input type="hidden" name="expense_status" id="expense_status_edit" />
  <input type="hidden" name="expense_id" value="<?php echo !empty($expense['expense_id'])?$expense['expense_id']:'';?>" />
</div>

</form>

<div class="modal-footer">    
<div class="form-group">
  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
<!-- END: Status Change form -->
<script type="text/javascript">
$(document).ready( function() {

$("#new_document_code").show();
$("#new_document_code").hide();

  <?php if (isset($expense['location_id']) && !empty($expense['location_id'])) { ?>

    <?php if (isset($expense['department_id']) && !empty($expense['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $expense['department_id']; ?>);
    <?php } else { ?>
    loadDepartmentsForSingleLocation(0);
    <?php } ?>

  <?php } ?>
  
  <?php 
    if (isset(Yii::app()->session['expense_status_changed_id']) && Yii::app()->session['expense_status_changed_id']) 
    { 
  ?>
  
      $('#alert-info').show();
      setTimeout(function() { $("#alert-info").hide(); }, 7000);
  
  <?php 
      Yii::app()->session['expense_status_changed_id'] = 0;
      unset(Yii::app()->session['expense_status_changed_id']);
    } 
  ?>    

    $('#expense_form').on('submit', function() {
        $('#expense_status').removeAttr('disabled');
    });

    $( "#expense_form" ).validate( {
        rules: {
            expense_name: "required",
            location_id: "required",
            department_id: "required"
        },
        messages: {
            expense_name: "Please enter title for expense request",
            location_id: "Location is required",
            department_id: "Department is required"
        },
      submitHandler: function(form) {
        $('.tax_rate').each(function() {
          if ($.trim($(this).val()) == "") $(this).val('0.00'); 
        });
        form.submit();
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      element.parents( ".col-sm-6" ).addClass( "has-feedback" );

      if ( element.prop( "type" ) === "checkbox" )
        error.insertAfter( element.parent( "label" ) );
      else error.insertAfter( element );

      if ( !element.next( "span" )[ 0 ] )
        $( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
    },
    success: function ( label, element ) {
      if ( !$( element ).next( "span" )[ 0 ] )
        $( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
      //$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
    },
    unhighlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error" );
      //$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
    }
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field",
        "onRemove":  function() { calculateTotalPrice(); },
        "onCreate": function() { createDateRangePicker(); }
    });

    $('.price_calc').blur(function() { calculateTotalPrice(); });

    $('body').on('blur', "input[name^='expense_price']", function() {
        calculateTotalPrice();
    });

    $('body').on('blur', "input[name^='tax_rate']", function() {
        calculateTotalPrice();
    });

    createVendorAutocompleteSearch();

    $('body').on('click', "input[name^='vendor_name']", function() {
        createVendorAutocompleteSearch();
    });
    
  $('.expense_date_picker').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    locale: {
    format: 'DD/MM/YYYY'
    }
  });
   
    $("#file").fileinput();
});

function createDateRangePicker()
{
  $('.expense_date_picker').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    locale: {
    format: 'DD/MM/YYYY'
    }
  });
}

function loadCurrencyRate()
{
    var currency_id = $('#currency_id').val();

    $.ajax({
        type: "POST", data: { currency_id: currency_id }, dataType: "json",
        url: BICES.Options.baseurl + '/orders/getCurrencyRates',
        success: function(options) {
            $('#currency_rate').val(options.rate);
            $('#user_currency_symbol').val(options.currency_symbol);
            calculateTotalPrice();
        },
        error: function() { $('#currency_rate').html(1); }
    });
}


function deleteExpenseFile(file_idx, file_name)
{
  $('#delete_expense_file_link_' + file_idx).confirmation({
    title: "Are you sure you want to delete the attached file?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
      $('#existing_file_id_' + file_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('expenses/deleteFile/'); ?>",
            data: { expense_id: $('#expense_id').val(), file_name: file_name }
        });
    },
    onCancel: function() {  }
  });
  
}

function createVendorAutocompleteSearch()
{
    $("input[name^='vendor_name']").each(function () {
        $(this).devbridgeAutocomplete({
            serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
            onSelect: function(suggestion) {
                $(this).closest('.form-group').find('.vendor_id').val(suggestion.data);
            },
          onSearchComplete: function(query, suggestions) { 
            if (!suggestions.length) $(this).closest('.form-group').find('.vendor_id').val(0); 
          }
        });
    });
}


function calculateTotalPrice()
{
    var total_expenses = 0;
    var price = 0;
    var tax_rate = 0;
    var price_with_tax = 0;
    var tax_amount = 0;

    $('.expense_price').each(function() {
        price = $(this).val();
        if ($.trim(price) == '' || !$.isNumeric(price)) price = 0;

        tax_rate = $(this).closest('.form-group').find('.tax_rate').val();
        if ($.trim(tax_rate) == '' || !$.isNumeric(tax_rate)) tax_rate = 0;
        
        if (price == 0) tax_amount = 0;
        else tax_amount = (tax_rate * price) / 100.0;
        $(this).closest('.form-group').find('.tax_amount').val(tax_amount.toFixed(2));
        
        price_with_tax = parseFloat(price) + parseFloat(tax_amount);
        total_expenses = parseFloat(total_expenses) + parseFloat(price_with_tax);
    });
    
    $('#total_expenses').val(total_expenses.toFixed(2));
    $('#total_expenses_display').html($('#user_currency_symbol').val() + total_expenses.toFixed(2));
}


function uploadReceipts()
{
  $('#files_only_upload').val(1);
  $('#expense_form').submit();  
}
function loadDepartmentsForSingleLocation(sel)
{
     var location_id = sel.value;
    var single = 1;
      $.ajax({
          type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
          url: BICES.Options.baseurl + '/locations/getDepartments',
          success: function(options) {
              var options_html = '<option value="" selected>Select Department</option>';
              for (var i=0; i<options.length; i++)
                  options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
              $('#department_id_new').html(options_html);
              //if (department_id != 0) $('#department_id_new').val(department_id);
          },
          error: function() { $('#department_id_new').html('<option value="">Select Department</option>'); }
      });

}

function addDocument()
{
    var total_documents = $('#total_documents').val();
    total_documents = parseInt(total_documents);
    total_documents = total_documents + 1;
    $('#total_documents').val(total_documents);

    var new_document_html = $('#new_document_code').html();
    new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
    $('#total_documents').before(new_document_html);
}
//expense_file_1 
function uploadDocument(uploadBtnIDX,e)
{  
  e.preventDefault();
  
  var uploadedObj = $('#expense_file_'+uploadBtnIDX);
  var uploadedObjDesc = $('#file_desc_'+uploadBtnIDX);
  
  var file_data = $('#expense_file_'+uploadBtnIDX).prop('files')[0];
  var field_data = $('#file_desc_'+uploadBtnIDX).val();   
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('file_description', field_data);
    form_data.append('expense_id',$('#expense_id').val());                           
    $.ajax({
        url: BICES.Options.baseurl + '/expenses/uploadDocument', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(uploaded_response){
          $("#uploaded_file_list").html(uploaded_response);
          uploadedObj.val(null);
          uploadedObjDesc.val(null);
            // display response from the PHP script, if any
        }
     });
}

function deleteSessionDocument(IDX)
{  
    var arrIDX = IDX;                 
    $.ajax({
        url: BICES.Options.baseurl + '/expenses/deleteSessionDocument', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        data: {IDX:arrIDX},                         
        type: 'post',
        success: function(uploaded_response){
         $("#uploaded_file_list").html(uploaded_response);
            // display response from the PHP script, if any
        }
     });
}




function deleteDocument(docidx)
{
    var display_document_count = 0;
    $("div[id^='document_area']").each(function () {
        if ($(this).is(':visible')) display_document_count += 1;
    });

    if (display_document_count <= 1) alert("You must have at least one document field in the expense form");
    else
    {
        if (confirm("Are you sure you want to delete this document?"))
        {
            $('#delete_document_flag_' + docidx).val(1);
            $('#document_area_' + docidx).hide();
            $('#end_document_area_' + docidx).hide();
        }
    }
}

$('#loader_expense').hide();

function sbtFormExpense(e,orderStatus){
  e.preventDefault();
  $('#expense_status_new').val(orderStatus);
  var form_data = $("#expense_form");                                            
    $.ajax({
        url: BICES.Options.baseurl + '/expenses/edit', // point to server-side PHP script 
        //dataType: 'text',  // what to expect back from the PHP script, if anything
        data: form_data.serialize(),                         
        type: 'post',
        beforeSend: function() {
        $('#loader_expense').show();
        $('#submitBtnCont').hide()},
        success: function(formResponse){
           $('#loader_expense').hide();
           $('#submitBtnCont').show()
           location.reload();
        }
     });
}

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab
$(".sub_btns").hide();
function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
    $("#total_price_cont_exp").hide();
    
  } else {
    document.getElementById("prevBtn").style.display = "inline";
    $("#total_price_cont_exp").show();
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    $("#nextBtn").hide();
    $(".sub_btns").show();
  } else {
    $("#nextBtn").show();
    $(".sub_btns").hide();
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("order_form").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

var requiredArr = ['location_id','department_id_new','currency_id','order_date','price_calc'];


function requiredArrCheck(field_id) {
  return age >= field_id;
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].querySelectorAll("select,input");
  // A loop that checks every input field in the current tab:
 for (i = 0; i < y.length; i++) { 
    // If a field is empty... 
    if ((y[i].value == "" || y[i].value == 0) && (requiredArr.includes(y[i].id) || requiredArr.includes(y[i].class) ) ) {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}
</script>
<style type="text/css">
.btn-file {
    padding:6px !important;
}
@media (max-width: @screen-xs-min) {
  .modal-xs { width: @modal-sm; }
}
/*
{
  /** width: 100%;
      height: 100%;
      padding: 0;
      margin:0;/**/
   /* width: 90%;
    height: 100%;
    padding: 0;
    margin: 0 auto;
}
.modal-content {    
      height: 100%;
      border-radius: 0;
      color:white;
      overflow:auto;
}
label { color:#73879C; }*/

/* Mark input boxes that gets an error on validation: */
/*#order_form {
  background-color: #ffffff;
  margin: 100px auto;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}*/

/* Style the input fields */


/* Mark input boxes that gets an error on validation: */
.invalid {
  background-color: #ffdddd !important;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none; 
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

/* Mark the active step: */
.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}
</style>


