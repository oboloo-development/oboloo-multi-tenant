<?php
// integer starts at 0 before counting
$existing_files = array(); 
$upload_dir = 'uploads/expenses/';
if ($expense_id && is_dir('uploads/expenses/' . $expense_id)){
  $upload_dir = 'uploads/expenses/' . $expense_id . '/';
  if ($handle = opendir($upload_dir)) {
    while (($uploaded_file = readdir($handle)) !== false){
    if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file)) 
      $existing_files[] = $uploaded_file;
    }
  }
 }
 if (is_array($existing_files) && count($existing_files)) { ?>
  <div class="form-group"><div class="col-md-6 col-sm-6 col-xs-12"><h5>Existing Receipts/Files</h5></div></div>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <table class="table">
      <?php $file_idx = 1; foreach ($existing_files as $uploaded_file) {
            $sql = 'select * from expense_files where expense_id='.$expense_id.' and file_name="'.$uploaded_file.'"';
            $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
            if(!empty($fileReader)){?>
            ?>
            <tr id="existing_file_id_<?php echo $file_idx; ?>">
              <td><a href="<?php echo AppUrl::bicesUrl('uploads/expenses/' . $expense_id . '/' . $uploaded_file); ?>" target="expense_file"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><?php echo $uploaded_file; ?></td>
              <td><?php echo !empty($fileReader['description'])?$fileReader['description']:'';?></td>
            </tr>
          <?php $file_idx += 1; }} ?></table></div><div class="clearfix"> <br /> </div>
  <?php } ?>