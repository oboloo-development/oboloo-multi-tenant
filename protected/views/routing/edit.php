<?php
	$route_approval_type = 'O';
	$route_approval_order_id = 0;
  $shipAmount= $taxAmount=0;
	$route_type_display = 'Order Approval';
  $orderStatuArr = array('Submitted','More Info Needed');
	if (isset($complete_route_data) && is_array($complete_route_data))
	{
		if (isset($complete_route_data['route_data']) && is_array($complete_route_data['route_data']))
		{
			if (isset($complete_route_data['route_data']['approval_type'])
					&& $complete_route_data['route_data']['approval_type'] == 'E')
			{
				$route_type_display = 'Expense Approval';		
				$route_approval_type = 'E';				
			}
		}

		if (isset($complete_route_data['order_data']) && is_array($complete_route_data['order_data']))
		{
			if ($route_approval_type == 'O')
			{
				if (isset($complete_route_data['order_data']['order_id'])
						&& !empty($complete_route_data['order_data']['order_id']))
				{
					$route_type_display .= ' - ID #' . $complete_route_data['order_data']['order_id'];
					$route_approval_order_id = $complete_route_data['order_data']['order_id'];
				}
			}
			else
			{
				if (isset($complete_route_data['order_data']['expense_id'])
						&& !empty($complete_route_data['order_data']['expense_id']))
				{
					$route_type_display .= ' - ID #' . $complete_route_data['order_data']['expense_id'];
					$route_approval_order_id = $complete_route_data['order_data']['expense_id'];
				}
			}
		}
	}
  if(!empty($complete_route_data['route_data']['order_id'])){
    $orderID = $complete_route_data['route_data']['order_id'];
  }else{
    $orderID=0;
  }
  $currencySymbol=FunctionManager::orderCurrSymbol($orderID);

?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>
            	<?php echo $route_type_display; ?>
            </h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('routing/list'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;border-color:#F79820">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Approvals List
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

<?php
    $userPer = new User;
    $order_data = $route_data = $order_details = array();
    if (isset($complete_route_data) && is_array($complete_route_data))
    {
        if (isset($complete_route_data['order_data'])) $order_data = $complete_route_data['order_data'];
        if (isset($complete_route_data['route_data'])) $route_data = $complete_route_data['route_data'];
        if (isset($complete_route_data['order_details'])) $order_details = $complete_route_data['order_details'];

    }

    if($route_approval_type == 'O') {
      if(isset(Yii::app()->session['user_id']) && !empty($order_data['location_id']) && !empty($order_data['department_id'])){
        $appOrder = $userPer->checkPermission(Yii::app()->session['user_id'],$order_data['location_id'],$order_data['department_id'],"approve_order");
        if(isset($appOrder) && $appOrder["approve_order"]=="yes" && in_array(Yii::app()->session['user_type'],array(1,3,4))){
          $approve="yes";
        }
      }
      $orderStatus = $order_data['order_status'];

    }else if($route_approval_type == 'E') {
      if(isset(Yii::app()->session['user_id']) && !empty($order_data['location_id']) && !empty($order_data['department_id'])){
        $appOrder = $userPer->checkPermission(Yii::app()->session['user_id'],$order_data['location_id'],$order_data['department_id'],"approve_travel_expense");
        if(isset($appOrder) && $appOrder["approve_travel_expense"]=="yes" && in_array(Yii::app()->session['user_type'],array(1,3,4))){
          $approve="yes";
        }
      }
      $orderStatus = $order_data['expense_status'];
    }


?>
<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs">
  <form id="routing_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('routing/edit'); ?>">
      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Location</label>
              <input type="text" class="form-control has-feedback-left" name="location_name" id="location_name" readonly="readonly"
                    <?php if (isset($order_data['location_name']) && !empty($order_data['location_name'])) echo 'value="' . $order_data['location_name'] . '"'; else echo 'placeholder="Location Name"'; ?> >
              <span class="fa fa-institution form-control-feedback left" aria-hidden="true"></span>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Department</label>
              <input type="text" class="form-control has-feedback-left" name="department_name" id="department_name" readonly="readonly"
                    <?php if (isset($order_data['department_name']) && !empty($order_data['department_name'])) echo 'value="' . $order_data['department_name'] . '"'; else echo 'placeholder="Department Name"'; ?> >
              <span class="fa fa-building-o form-control-feedback left" aria-hidden="true"></span>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Order Date</label>
              <input type="text" class="form-control has-feedback-left" name="order_date" id="order_date" readonly="readonly"
                    <?php if (isset($order_data['order_date']) && !empty($order_data['order_date'])) echo 'value="' . date("d/m/Y", strtotime($order_data['order_date'])) . '"'; else echo 'placeholder="Order Date"'; ?> >
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
            <?php $totalPrice=FunctionManager::orderTotalPrice($complete_route_data['route_data']['order_id']);?>
              <label class="control-label">Total Price</label>
              <input type="text" style="text-align: right;" class="form-control has-feedback-left" name="total_price" id="total_price" readonly="readonly"
                    <?php if (isset($totalPrice) && !empty($totalPrice)) echo 'value="' .$totalPrice . '"'; else echo 'placeholder="Total Price"'; ?> >
              <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
          </div>
      </div>
      <div class="clearfix"> </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Supplier Name</label>
          	  <?php if ($route_approval_type == 'O') { ?>
	              <input type="text" class="form-control has-feedback-left" name="vendor_name" id="vendor_name" readonly="readonly"
	                    <?php if (isset($order_data['vendor_name']) && !empty($order_data['vendor_name'])) echo 'value="' . $order_data['vendor_name'] . '"'; else echo 'placeholder="Supplier Name"'; ?> >
	              <span class="fa fa-user-o form-control-feedback left" aria-hidden="true"></span>
              <?php } else { ?>
                  <label class="control-label">Expense Name</label>
	              <input type="text" class="form-control" name="expense_name" id="expense_name" readonly="readonly"
	                    <?php if (isset($order_data['expense_name']) && !empty($order_data['expense_name'])) echo 'value="' . $order_data['expense_name'] . '"'; else echo 'placeholder="Expense Name"'; ?> >
              <?php } ?>
          </div>

        <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Status</label>
               <input type="text" class="form-control" name="routing_status" id="routing_status" readonly="readonly"
                      <?php if (!empty($route_data['routing_status'])) echo 'value="' . $route_data['routing_status'] . '"'; else  'value="Pending"'; ?> >

             <!--  <select name="routing_status" id="routing_status" class="form-control">
                  <?php foreach (array('Pending', 'Approved', 'Declined', 'Information') as $routing_status) { ?>
                        <option value="<?php echo $routing_status; ?>"
                                <?php if (isset($route_data['routing_status']) && $route_data['routing_status'] == $routing_status) echo ' selected="SELECTED" '; ?>>
                            <?php
                            	if ($routing_status != 'Information') echo $routing_status;
								else echo 'More Info Needed'; 
                            ?>
                        </option>
                  <?php } ?>
              </select> -->
          </div>
      </div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">User Name</label>
        <input type="text" class="form-control"  value="<?php echo $order_data['user_name'];?>" readonly>
      </div>
    </div>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Routing Comments</label>
              <textarea name="routing_comments" id="routing_comments" rows="4" class="form-control"  <?php if(!in_array($orderStatus,$orderStatuArr)){ echo "readonly"; }?>><?php if (isset($route_data['routing_comments']) && !empty($route_data['routing_comments'])) echo $route_data['routing_comments']; ?></textarea>
          </div>
      </div>

      <div class="clearfix"> <br /> </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4><?php if ($route_approval_type == 'O') echo 'Order Details'; else echo 'Expense Details'; ?></h4>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Description</label>
              <textarea name="overall_description" id="overall_description" readonly="readonly" rows="4" class="form-control"><?php if (isset($order_data['description']) && !empty($order_data['description'])) echo $order_data['description']; ?></textarea>
          </div>
      </div>

      <div class="clearfix"> 
      	<br />
      	<div class="col-md-6 col-sm-6 col-xs-12">
      		<h3 style="font-size: 18px !important;">Line Items</label></h3>
      	</div> 
      </div>

      <?php $total_price = 0; if (isset($order_details) && is_array($order_details) && count($order_details)) { ?>

          <?php
            foreach ($order_details as $order_detail)
            {
                if (!isset($order_detail['quantity']) || empty($order_detail['quantity'])) $order_detail['quantity'] = 1;
                if (!isset($order_detail['calc_unit_price']) || empty($order_detail['calc_unit_price'])) $order_detail['calc_unit_price'] = 0;
          ?>

			  <?php if ($route_approval_type == 'O') { ?>
              <div class="form-group">
                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Product Type</label>
                      <select class="form-control product_type" name="product_type[]" disabled="disabled">
                          <option value="Bundle"
                                <?php if (isset($order_detail['product_type']) && $order_detail['product_type'] == 'Bundle') echo ' selected="SELECTED" '; ?>>
                              Bundle
                          </option>
                          <option value="Product"
                                <?php if (isset($order_detail['product_type']) && $order_detail['product_type'] == 'Product') echo ' selected="SELECTED" '; ?>>
                              Product
                          </option>
                      </select>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-8 date-input">
                      <label class="control-label">Product Name</label>
                      <input type="text" class="form-control has-feedback-left price_calc" name="product_name[]" disabled="disabled"
                            <?php if (isset($order_detail['product_name']) && !empty($order_detail['product_name'])) echo 'value="' . $order_detail['product_name'] . '"'; ?> >
                      <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
                  </div>
                  <div class="clearfix"> <br /><br /> </div>

                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Quantity</label>
                      <input style="text-align: right;" type="text" class="product_quantity form-control price_calc" name="quantity[]" disabled="disabled"
                            <?php if (isset($order_detail['quantity']) && !empty($order_detail['quantity'])) echo 'value="' . $order_detail['quantity'] . '"'; ?> >
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4 display_price informational_messages_area" style="padding: 8px; font-size: 120%;">
                      <label class="control-label">&nbsp;</label>
                      <?php
                            if (isset($order_detail['calc_unit_price']) && !empty($order_detail['calc_unit_price'])
                                    && isset($order_detail['quantity']) && !empty($order_detail['quantity']))
                            {
                                echo '@ ';
                                echo number_format($order_detail['calc_unit_price'] / $order_detail['quantity'], 2);
                                echo ' / Each';
                            }
                            if(!empty($order_detail['calc_tax'])){
                              $taxAmount += $order_detail['calc_tax'];
                            }
                            if(!empty($order_detail['shipping_cost'])){
                              $shipAmount += $order_detail['shipping_cost'];
                            }

                       ?>
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Unit Price</label>
                      <input style="text-align: right;" type="text" class="unit_price form-control" name="unit_price[]" readonly="readonly"
                            <?php if (isset($order_detail['calc_unit_price']) && !empty($order_detail['calc_unit_price'])) echo 'value="' . $order_detail['calc_unit_price'] . '"'; ?> >
                      <input type="hidden" class="unit_price_hidden" name="unit_price_hidden[]"
                              <?php if (isset($order_detail['calc_unit_price']) && !empty($order_detail['calc_unit_price']) && isset($order_detail['quantity']) && !empty($order_detail['quantity'])) echo 'value="' . $order_detail['calc_unit_price'] / $order_detail['quantity'] . '"'; ?> >
                  </div>
              </div>
              <?php } ?>

			  <?php if ($route_approval_type == 'E') { ?>
              <div class="form-group">
                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Expense Type</label>
                      <select class="form-control" name="expense_type_id[]" disabled="disabled">
                      	<option value="0">Expense Type</option>
                      	<?php foreach ($expense_types as $expense_type) { ?>
                      		<option value="<?php echo $expense_type['id']; ?>"
                      				<?php if (isset($order_detail['expense_type_id']) && $order_detail['expense_type_id'] == $expense_type['id']) echo ' selected="SELECTED" '; ?>>
                      			<?php echo $expense_type['value']; ?>
                      		</option>
                      	<?php } ?>
                      </select>
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Expense Date</label>
                      <input type="text" class="expense_date_picker form-control" name="expense_date[]" readonly="readonly"
                            <?php if (isset($order_detail['expense_date']) && !empty($order_detail['expense_date'])) echo 'value="' . date("d/m/Y", strtotime($order_detail['expense_date'])) . '"'; ?> >
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Expense Price</label>
                      <input style="text-align: right;" type="text" class="expense_price price_calc form-control" name="expense_price[]"  readonly="readonly"
                            <?php if (isset($order_detail['expense_price']) && !empty($order_detail['expense_price'])) echo 'value="' . number_format($order_detail['expense_price'], 2) . '"'; ?> >
                  </div>
                  <div class="clearfix"> <br /><br /> </div>

				  <?php
				  		$current_price = $current_tax = 0;
						if (isset($order_detail['expense_price'])) $current_price = $order_detail['expense_price'];
						if (isset($order_detail['tax_rate'])) $current_tax = $order_detail['tax_rate'];
						$current_tax_amount = ($current_tax * $current_price) / 100.0; 
            $totalPrice = $current_price+$current_tax_amount;
            $taxAmount = $current_tax_amount;
				  ?>
                  <div class="col-md-2 col-sm-2 col-xs-4" style="margin-top: 26px;text-align: right; font-size: 120%;">
                  		% Tax Rate Applied Is
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-4">
                    <label class="control-label">Tax Rate</label>
                      <input style="text-align: right;" type="text" class="tax_rate price_calc form-control" name="tax_rate[]" readonly="readonly"
                            <?php if (isset($order_detail['tax_rate']) && !empty($order_detail['tax_rate'])) echo 'value="' . number_format($order_detail['tax_rate'], 2) . '"'; ?> />
	              </div>
                  <div class="col-md-2 col-sm-2 col-xs-4">
                    <label class="control-label">Tax Amount</label>
                      <input style="text-align: right;" type="text" class="tax_amount form-control" name="tax_amount[]" 
                      		readonly="readonly" <?php echo 'value="' . number_format($current_tax_amount, 2) . '"'; ?> />
	              </div>
                  <div class="clearfix"> <br /><br /> </div>

                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">Supplier Name</label>
                      <input type="text" class="form-control has-feedback-left" name="vendor_name[]" readonly="readonly"
                            <?php if (isset($order_detail['vendor_name']) && !empty($order_detail['vendor_name'])) echo 'value="' . $order_detail['vendor_name'] . '"'; ?> />
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true" style="margin-top: 32px;"></span>
                      <input class="vendor_id" type="hidden" name="vendor_id[]" value="<?php echo $order_detail['vendor_id']; ?>" />
                  </div>
                  <div class="clearfix"> <br /><br /> </div>

                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">Expense Notes</label>
                      <input type="text" class="form-control" name="expense_notes[]" readonly="readonly"
                            <?php if (isset($order_detail['expense_notes']) && !empty($order_detail['expense_notes'])) echo 'value="' . $order_detail['expense_notes'] . '"'; ?> >
                  </div>
                  </div>
			  <?php } ?>

          <?php 
          
          if ($route_approval_type == 'O') $total_price += $order_detail['calc_unit_price'];
					else $total_price += $order_detail['expense_price'] + $current_tax_amount; 

				} 
		  ?>

      <?php } ?>

      <div class="clearfix"> </div>      
      <div class="form-group">
          <div class="col-md-offset-3 col-md-3 col-sm-3 col-xs-6" style="text-align: right;">
              <h4><?php echo 'Item Total: ' . $currencySymbol . ' ' . number_format($total_price, 2); ?></h4>
          </div>
      </div>

	  <?php
     if (!empty($shipAmount)) { ?>
	      <div class="clearfix"> </div>      
	      <div class="form-group">
	          <div class="col-md-offset-3 col-md-3 col-sm-3 col-xs-6" style="text-align: right;">
	              <h4><?php echo 'Shipping: ' . $currencySymbol . ' ' . number_format($shipAmount, 2); ?></h4>
	          </div>
	      </div>
	  <?php } ?>
	  
	  <?php if (isset($taxAmount)) { ?>
	      <div class="clearfix"> </div>      
	      <div class="form-group">
	          <div class="col-md-offset-3 col-md-3 col-sm-3 col-xs-6" style="text-align: right;">
	              <h4><?php echo 'Tax: ' . $currencySymbol . ' ' . number_format($taxAmount, 2); ?></h4>
	          </div>
	      </div>
	  <?php } ?>
	  
      <div class="clearfix"> </div>      
      <div class="form-group">
          <div class="col-md-offset-3 col-md-3 col-sm-3 col-xs-6" style="text-align: right;">
              <h4><?php echo 'Grand Total: ' . $currencySymbol . ' ' . number_format($totalPrice, 2); ?></h4>
          </div>
      </div>

      <div class="clearfix"> </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" name="id" id="id" value="<?php if (isset($route_data['id'])) echo $route_data['id']; ?>" />
            <input type="hidden" name="order_id" id="order_id" value="<?php echo $route_approval_order_id; ?>" />
            <input type="hidden" name="user_id" id="user_id" value="<?php if (isset(Yii::app()->session['user_id'])) echo Yii::app()->session['user_id']; else echo '0'; ?>" />
            <input type="hidden" name="routing_date" id="routing_date" value="<?php echo date("d/m/Y"); ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
            <input type="hidden" name="approval_type" id="approval_type" value="<?php echo $route_approval_type; ?>" />
        </div>
    </div>
   <?php if($route_approval_type == 'O'){
        $this->renderPartial('_documents_order',array('order_id'=>$orderID));
      }else if($route_approval_type == 'E'){
        $this->renderPartial('_documents_expense',array('expense_id'=>$orderID));
      }?>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
     <?php if(!empty($approve) && in_array($orderStatus,$orderStatuArr)){ ?>
      <input type="submit" name="routing_status" value ="Approve" class="btn btn-success" />
      <input type="submit" name="routing_status" value ="Decline" class="btn btn-danger" />
      <input type="submit" name="routing_status" value="More Information Needed" class="btn btn-success" style="    background-color: #F79820;color: #fff;border: none;" />
    <?php } ?>
    </div>
    <div class="clearfix"> <br /><br /> </div>
</div>
  </form>
<!--     <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <a href="javascript:void(0);" onclick="$('#routing_form').submit();">
            <button type="button" class="btn btn-primary">
                Save Approval
            </button>
        </a>
    </div>
    <div class="clearfix"> <br /><br /> </div>
</div> -->
</div>



</div>

<script type="text/javascript">
$(document).ready(function() {
	$('#order_date').daterangepicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});	
});
</script>
