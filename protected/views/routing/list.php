<?php $order_statuses = FunctionManager::routingStatus();?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Order Approvals</h3>
        </div>
        <div class="clearfix"> </div>
    </div>

    <form class="form-horizontal" id="order_list_form" name="order_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('routing/list'); ?>">
        <div class="form-group">
          <div class="col-md-2" style="width:11%">
              <input type="text" class="form-control" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . $from_date . '"'; else echo 'placeholder="From Date"'; ?> >
          </div>
            
          <div class="col-md-2" style="width:11%">
              <input type="text" class="form-control" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . $to_date . '"'; else echo 'placeholder="To Date"'; ?> >
          </div>

            <div class="col-md-2 location">
                <select name="location_id[]" id="location_id"  class="form-control" multiple searchable="Search here.."  onchange="loadDepartments(0);">
                    <?php
                    $i = 0 ;
                    foreach ($locations as $location) {
                        if(isset($location_id[$i])){
                        ?>
                        <option value="<?php echo $location['location_id']; ?>"
                                <?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
                            <?php echo $location['location_name']; ?>
                        </option>
                        <?php } else { ?>
                            <option value="<?php echo $location['location_id']; ?>">
                                <?php echo $location['location_name']; ?>
                            </option>
                        <?php } ?>
                    <?php
                    $i++;
                    } ?>
                </select>
            </div>

            <div class="col-md-2 department">
                <select name="department_id[]" id="department_id" class="form-control" multiple searchable="Search here..">
                    <option value="0">All Departments</option>
                    <?php if(!empty($department_info))
                        foreach($department_info as $dept_value){?>
                        <option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
                    <?php } ?>
                    
                </select>
            </div>
            <div class="col-md-2 status">
                <select name="order_status[]" id="order_status" multiple class="form-control">
                    <?php
                    $i = 0 ;
                    foreach ($order_statuses as $order_status_display) {
                        ?>
                        <option value="<?php echo $order_status_display; ?>"
                                <?php if (isset($order_status) && in_array($order_status_display,$order_status)) { echo ' selected="SELECTED" ';} ?>>
                            <?php echo $order_status_display; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-2">
                <select name="spend_type" id="spend_type" class="form-control">
                    <option value="">Spend Type</option>
                    <option  <?php if (isset($spend_type) && $spend_type == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                    <option  <?php if (isset($spend_type) && $spend_type == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
                </select>
            </div>
            <div class="col-md-1">
                <button class="btn btn-primary" onclick="$('#order_list_form').submit();">Search</button>
            </div>
        </div>
    </form>
    <br /><br />
    <table id="routing_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th> </th>
          <th>ID</th>
          <th>Date</th>
          <th>Location</th>
          <th>Department</th>
          <th>Approver Comments</th>
          <th>Status</th>
          <th>Total Price</th>
        </tr>
      </thead>

      <tbody>

          <?php foreach ($order_approvals as $approval) { ?>

              <tr>
                    <td>
                        <a href="<?php echo AppUrl::bicesUrl('routing/edit/' . $approval['id']); ?>">
                            <button class="btn btn-sm btn-success">View/Approve</button>
                        </a>
                    </td>
                    <td>
                    	<?php if ($approval['approval_type'] == 'O') { ?>
	                    	<a style="text-decoration: underline;" target="_procurement_order"
	                    			href="<?php echo AppUrl::bicesUrl('orders/edit/' . $approval['order_id']); ?>">
	                    		<?php echo 'Order #' . $approval['order_id']; ?>
	                    	</a>
                    	<?php } else { ?>
	                    	<a style="text-decoration: underline;" target="_procurement_expense"
	                    			href="<?php echo AppUrl::bicesUrl('expenses/edit/' . $approval['order_id']); ?>">
	                    		<?php echo 'Expense #' . $approval['order_id']; ?>
	                    	</a>
                    	<?php } ?>
                    </td>
                    <td><?php echo date("F j, Y", strtotime($approval['order_date'])); ?></td>
                    <td><?php echo $approval['location_name']; ?></td>
                    <td><?php echo $approval['department_name']; ?></td>
                    <td><?php echo $approval['routing_comments']; ?></td>
                    <td>
                    	<?php
                            $status_style = "";
                            if(!empty($approval['other_status']) && strtolower($approval['other_status'])=="approved"){
                                  $toolTip = "Order routing has been approved.";
                            }else {
                    		if (empty($approval['routing_status'])) $approval['routing_status'] = 'Pending';
                    		
							
							if ($approval['routing_status'] == 'Paid' || $approval['routing_status'] == 'Closed' || $approval['routing_status'] == 'Received')
								$status_style = ' style="border: 1px solid green !important; background-color: green !important; color: #fff !important;" ';
							if ($approval['routing_status'] == 'Declined')
								$status_style = ' style="border: 1px solid #990000 !important; background-color: #990000 !important; color: #fff !important;" ';								
							if ($approval['routing_status'] == 'More Info Needed' || $approval['routing_status'] == 'Information')
								$status_style = ' style="border: 1px solid #CC3333 !important; background-color: #CC3333 !important; color: #fff !important;" ';								
							if ($approval['routing_status'] == 'Pending' || $approval['routing_status'] == 'Ordered - PO Not Sent To Supplier' 
										|| $approval['routing_status'] == 'Submitted' || $approval['routing_status'] == 'Ordered - PO Sent To Supplier')
								$status_style = ' style="border: 1px solid orange !important; background-color: orange !important; color: #fff !important;" ';

                        if($approval['routing_status'] == 'Pending') {
                            $toolTip = "Order routing has been created by " . $approval['full_name'] . " however has not been approved.";
                        }
                        elseif($approval['routing_status'] == 'Declined'){
                            $toolTip = "Approver has declined the order routing. Please read approver notes.";
                        }
                        elseif($approval['routing_status'] == 'Approved'){
                            $toolTip = "Order routing has been approved.";
                        }
                        elseif($approval['routing_status'] == 'More Info Needed'){
                            $toolTip = "Order routing has requested more information from the order creator.";
                        }

                        elseif($approval['routing_status'] == 'Informaion'){
                            $toolTip = "Order routing need more information.";
                        } else{
                            $toolTip = 'N/A';
                        }
                        }
                        ?>
	                    	<button class="btn btn-sm btn-success status" title="<?php echo $toolTip; ?>" <?php echo $status_style; ?>>
		                    	<?php
		                    		if ($approval['routing_status'] != 'Information') {
                                        if(!empty($approval['other_status']) && strtolower($approval['other_status'])=="approved"){
                                            echo $approval['other_status']; 
                                        }else{
                                             echo $approval['routing_status']; 
                                        }
                                    }
									else echo 'More Info Needed';
		                    	?>
	                    	</button>                    	
                    </td>
                    <td style="text-align: right;">
                    	<nobr>
                    		<?php echo Yii::app()->session['user_currency_symbol'] . ' ' . $approval['func_total_price']; ?>
                    	</nobr>
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>
<style>
    .ui-tooltip {
        width: 200px;
        text-align: center;
        box-shadow: none;
        padding: 0;
    }
    .ui-tooltip-content {
        position: relative;
        padding: 0.5em;
    }
    .ui-tooltip-content::after, .ui-tooltip-content::before {
        content: "";
        position: absolute;
        border-style: solid;
        display: block;
        left: 90px;
    }
    .bottom .ui-tooltip-content::before {
        bottom: -10px;
        border-color: #AAA transparent;
        border-width: 10px 10px 0;
    }
    .bottom .ui-tooltip-content::after {
        bottom: -7px;
        border-color: white transparent;
        border-width: 10px 10px 0;
    }
    .top .ui-tooltip-content::before {
        top: -10px;
        border-color: #AAA transparent;
        border-width: 0 10px 10px;
    }
    .top .ui-tooltip-content::after {
        top: -7px;
        border-color: white transparent;
        border-width: 0 10px 10px;
    }
    .department .dropdown-toggle{
        width: 112%;
    }
    .location .dropdown-toggle{
         width: 138%;
    }
    .status .dropdown-toggle{
       width: 139%;
    }
</style>

<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

    function getOptions(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Locations',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }

    function getOptions2(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Departments',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }


    function getOptions1(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Statuses',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }
    $('#location_id').multiselect(getOptions(true));
    $('#department_id').multiselect(getOptions2(true));
    $('#order_status').multiselect(getOptions1(true));

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );


$.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
    locale: {
        format: 'DD/MM/YYYY'
    }
  });
};

$(document).ready( function() {
    $('#routing_table').dataTable({
        "columnDefs": [ 
            { "targets": 0, "width": "6%", "orderable": false },
            { "type": "date", targets: 2 }         
        ],
        "order": [[ 2, "desc" ]],
        "language": {"search": "General Search:"}
    });

    //$(".input-sm").attr("placeholder","General Search");

    $('#from_date').singleDatePicker({
      singleDatePicker: true,
      singleClasses: "picker_3",
      locale: {
        format: 'DD/MM/YYYY'
      }
    });

    $('#to_date').singleDatePicker({
      singleDatePicker: true,
      singleClasses: "picker_3",
      locale: {
        format: 'DD/MM/YYYY'
      }
    });

    $( ".status" ).tooltip({
        position: {
            my: "center bottom",
            at: "center top-10",
            collision: "flip",
            using: function( position, feedback ) {
                $( this ).addClass( feedback.vertical )
                    .css( position );
            }
        }
    });

    <?php if (isset($location_id) && !empty($location_id)) { ?>

        <?php if (isset($department_id) && !empty($department_id)) { ?>
            //loadDepartments(<?php //echo $department_id; ?>);
        <?php } else { ?>
            loadDepartments(0);
        <?php } ?>

    <?php } ?>
        
});
</script>

