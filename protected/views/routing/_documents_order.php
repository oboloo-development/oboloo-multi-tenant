<?php 
  // integer starts at 0 before counting
  $existing_files = array(); 
  $upload_dir = 'uploads/orders/';
  if ($order_id && is_dir('uploads/orders/' . $order_id)){
        $upload_dir = 'uploads/orders/' . $order_id . '/';
          if ($handle = opendir($upload_dir)) {
              while (($uploaded_file = readdir($handle)) !== false){
                  if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file)) 
                      $existing_files[] = $uploaded_file;
              }
          }
  }
  if (is_array($existing_files) && count($existing_files)) { ?>
  <div class="form-group"><div class="col-md-6 col-sm-6 col-xs-12"><h5>Existing Receipts/Files</h5></div></div>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <table class="table">
      <?php $file_idx = 1; 
          $orderObject = new Order();
          foreach ($existing_files as $uploaded_file) {
            $sql = 'select * from order_files where order_id='.$order_id.' and file_name="'.$uploaded_file.'"';
            $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
            if(!empty($fileReader)){
            ?>
            <tr id="existing_file_id_<?php echo $file_idx; ?>">
              <td><a href="<?php echo AppUrl::bicesUrl('uploads/orders/' . $order_id . '/' . $uploaded_file); ?>" target="order_file" ><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                &nbsp;</td>
                <td><?php echo $uploaded_file; ?></td>
                <td><?php echo !empty($fileReader['description'])?$fileReader['description']:'';?></td>
            </tr>
            <?php $file_idx += 1; }} ?>
          </table></div><div class="clearfix"> <br /> </div>
  <?php } ?>