<?php
  $toolTipText = MessageManager::tooltipText();
  $roleTool = 'This gives users different access rights';
  $roleLogTool = 'Only active suppliers can log into oboloo';
	$username_value = "";
  
	if (isset(Yii::app()->session['duplicate_username'])) $username_value = Yii::app()->session['duplicate_username'];
	else if (isset($user) && isset($user['username'])) $username_value = $user['username'];

	$email_value = "";
	if (isset(Yii::app()->session['duplicate_email'])) $email_value = Yii::app()->session['duplicate_email'];
	else if (isset($user) && isset($user['email'])) $email_value = $user['email'];

  if(strpos(Yii::app()->getBaseUrl(true),"multi-local") ){
      $disabled = "";
      $db = SubscriptionManager::getDatabaseForSubdomain('');
  }else{
      $db = getDatabaseForSubdomain(APP_SUBDOMAIN_URL);
      $disabled = FunctionManager::sandbox();
  }

  
 
/*
$connection=new CDbConnection('mysql:host=localhost;dbname='.APP_FOREMAN_DB,APP_FOREMAN_USER,APP_FOREMAN_PASS);
$connection->active=true;
 
$command = $connection->createCommand('SELECT * FROM foreman_clients order by id desc');
$posts = $command->queryAll();
echo "<pre>";print_r($posts);exit;
*/
?>

<div class="right_col" role="main">

    <div class="row-fluid tile_count">
      <?php $alert=Yii::app()->user->getFlash('success');
      $msg = Yii::app()->user->getFlash('danger');
          if(!empty($alert)) { ?>
          <div class="clearfix"></div>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $alert;?></div>
        <?php } ?>
        <div class="span6 pull-left">
            <h3>
                <?php
                    if (isset($user_id) && $user_id)
                    {
                        echo 'Save User';
                        if (isset($user) && is_array($user) && isset($user['full_name']) && !empty($user['full_name']))
                            echo ' - ' . $user['full_name'];
                    }
                    else echo 'Add User';
                ?>
            </h3>
        </div>

        <div class="span6 pull-right">
          <a href="<?php echo AppUrl::bicesUrl('usersUsg/list'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;">
                    <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> User List
                </button>
            </a>
            <br>
             <a href="https://oboloo.com/support-hub/#Userroles" target="_blank">
                <button type="button" class="btn btn-default user_role_type" style="background-color: #5bc0de;color: #fff;">
                  User Role Types
                </button>
            </a>

            <?php if(!empty($user['user_id']) && Yii::app()->session['user_id'] !=$user['user_id']){?>
           <!--  <a href="<?php echo AppUrl::bicesUrl('usersUsg/delete/' . $user['user_id']); ?>" onclick="return confirm('Are you sure want to delete');">
                <button type="button" class="btn btn-danger" style="color: #fff;">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete User
                </button>
            </a> -->
          <?php } ?>
        </div>

        <div class="clearfix"> </div>
    </div>

<?php
	if (isset(Yii::app()->session['duplicate_username']) && !empty(Yii::app()->session['duplicate_username']))
		echo '<div style="color: red;">Username ' . Yii::app()->session['duplicate_username'] . ' already exists</div>';
	if (isset(Yii::app()->session['duplicate_email']) && !empty(Yii::app()->session['duplicate_email']))
		echo '<div style="color: red;">Email ' . Yii::app()->session['duplicate_email'] . ' already exists</div>';
	if (isset(Yii::app()->session['bad_password']) && !empty(Yii::app()->session['bad_password']))
		echo '<div style="color: red;">' . Yii::app()->session['bad_password'] . '</div>';

	Yii::app()->session['duplicate_username'] = "";
	unset(Yii::app()->session['duplicate_username']);
			
	Yii::app()->session['duplicate_email'] = "";
	unset(Yii::app()->session['duplicate_email']);	
	
	Yii::app()->session['bad_password'] = "";
	unset(Yii::app()->session['bad_password']);			
?>

<div class="row tile_count">
   <div class="col-md-6 col-xs-12">
  <form id="user_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('usersUsg/edit'); ?>">
      <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12 date-input">
              <label class="control-label">Full Name <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="full_name" id="full_name"
              <?php if (isset($user['full_name']) && !empty($user['full_name'])) echo 'value="' . $user['full_name'] . '"'; else echo 'placeholder="Full Name"'; ?> >
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">Email Address <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="email" id="email"
                      <?php if (isset($user['email']) && !empty($user['email'])) echo 'value="' . $email_value . '"'; else echo 'placeholder="Email Address"'; ?>  >
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">Logon Username <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="username" id="username"
                      <?php if (isset($user['username']) && !empty($user['username']) && !empty($user_id)) echo 'value="' . $username_value . '"';else if(empty($user_id)) {?>  <?php echo 'placeholder="Logon Username"';} else echo 'placeholder="Logon Username"'; ?> required>
           
          </div>
      </div>

     <!--  <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6">
              <input type="password" class="form-control" name="password" id="password" placeholder="Password"  autocomplete="off" >
              <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6">
              <input type="password" class="form-control " name="confirm_password" id="confirm_password" placeholder="Confirm Password" >
              <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
          </div>
      </div> -->

      <div class="form-group">
        <?php if(FunctionManager::superUserCheck()==1 && !empty($user['user_type_id']) && $user['user_type_id']==4){?>
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label" >User Role <span style="color: #a94442;">*</span> <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="<?php echo $toolTipText['roleTool']; ?>"></i></label>
              <input type="text" name="user_type_id" class="form-control notranslate user_role" value="<?php echo FunctionManager::getUserType($user['user_type_id']);?>" readonly />
              <input type="hidden" name="user_type_id" class="form-control notranslate" value="<?php echo $user['user_type_id'];?>" />
          </div>
        <?php }else{?>
         
           <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Position <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control  notranslate" name="position" id="position"
                    <?php if (isset($user['position']) && !empty($user['position'])) echo 'value="' . $user['position'] . '"'; else echo 'placeholder="Position"'; ?> >
            
          </div>
        <?php } ?>
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">User Role <span style="color: #a94442;">*</span><!-- <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="<?php echo $toolTipText['roleLogTool']; ?>"></i> --></label>
              <select name="user_type_id" id="user_type_id" class="form-control notranslate user_type" required>
                  <option value="">Select User Role</option>
                  <?php foreach ($user_types as $user_type) { ?>
                      <option value="<?php echo $user_type['id']; ?>"
                            <?php if (isset($user['user_type_id']) && $user['user_type_id'] == $user_type['id']) echo 'selected="SELECTED"'; ?>>
                          <?php echo $user_type['value']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">Location <span style="color: #a94442;">*</span></label>
				<select name="location_id" id="location_id" class="form-control notranslate" onchange="loadDepartmentsForSingleLocation(0);">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($user['location_id']) && $location['location_id'] == $user['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">Department <span style="color: #a94442;">*</span></label>
              <select name="department_id" id="department_id" class="form-control notranslate">
                  <option value="">Select Department</option>
              </select>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">Contact Number</label>
              <input type="number" class="form-control notranslate" name="contact_number" id="contact_number"
                      <?php if (isset($user['contact_number']) && !empty($user['contact_number'])) echo 'value="' . $user['contact_number'] . '"'; else echo 'placeholder="Contact Number"'; ?> >
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
            <label class="control-label">Manager</label>
              <select name="manager_id" id="manager_id" class="form-control notranslate">
                  <option value="">Select Manager</option>
                  <?php foreach ($manager as $value) { ?>
                      <option value="<?php echo $value['user_id']; ?>"
                            <?php if (isset($value['user_id']) && $value['user_id']== $user['manager_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $value['full_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          
      </div>

    <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="hidden" class="notranslate" name="user_id" id="user_id" value="<?php if (isset($user_id)) echo $user_id; ?>" />
    <input type="hidden" class="notranslate" name="user_type" id="user_type" value="<?php if (!empty($user_id)) { echo $user['user_type_id']; } ?>" />
    <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
     <?php /*if(!empty($user_id) && Yii::app()->session['user_type']==4 && (in_array($user['user_type_id'],array(3,1,4)))){*/
      /*if(true){?>
    <div id="related_orders" style="width: 725px;">
            <table id="order_table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th colspan="7"> <h3>Approval Levels</h3><br />
                      <h4 class="subheading">Manage what this user can approve by each location/department<br /></h4></th>
                </tr>
                <tr>
                    <th>Location</th>
                    <th>Department</th>
                    <!-- <th style="text-align: center;">Vendor Documents</th>
 -->                    <th style="text-align: center;">Approve Contract Documents</th>
                    <!-- <th style="text-align: center;">Approve Orders</th> -->
                    <!-- <th style="text-align: center;">Approve Travel & Expenses</th> -->
                    <!-- <th style="text-align: center;">Approve Invoices</th> -->
                    <!-- <th style="text-align: center;">Amend Budgets</th> -->
                    <th style="text-align: center;">View Contractus</th>
                    <th style="text-align: center;">Edit Contracts</th>
                    
                </tr>
                </thead>

                <tbody>
                <?php 
               $userPer = new User;
                foreach($locations_departments as $locValue) {
                   $locID=$locValue['location_id'];
                   $deptID=$locValue['department_id'];
                   
                   $vendorDocument = $userPer->checkPermission($user_id,$locID,$deptID,"vendor_document");
                   $contractDocument = $userPer->checkPermission($user_id,$locID,$deptID,"contract_document");
                   $appOrder = $userPer->checkPermission($user_id,$locID,$deptID,"approve_order");
                   $appExpens = $userPer->checkPermission($user_id,$locID,$deptID,"approve_travel_expense");
                   $appInvoice  = $userPer->checkPermission($user_id,$locID,$deptID,"approve_invoice");
                   $amendBudget  = $userPer->checkPermission($user_id,$locID,$deptID,"amend_budget");
                   $contractView  = $userPer->checkPermission($user_id,$locID,$deptID,"contract_view");
                   $contractEdit  = $userPer->checkPermission($user_id,$locID,$deptID,"contract_edit");
                  ?>
                   
                <tr>
                    <td><?php echo $locValue['location_name'];?></td>
                    <td><?php echo $locValue['department_name'];?></td>

                   <!--  <td style="text-align: center;"><input type="checkbox" class="flat" name="locationDepartment[<?php echo $locID;?>][<?php echo $deptID;?>][vendor_document]" <?php if(!empty($vendorDocument["vendor_document"]) && $vendorDocument["vendor_document"]=="yes") { ?> checked="checked" <?php } ?> /></td> -->
                    <td style="text-align: center;">

                      <input type="checkbox" class=" apply_permission"   name="locationDepartment[<?php echo $locID;?>][<?php echo $deptID;?>][contract_document]"  <?php if(!empty($contractDocument["contract_document"]) && $contractDocument["contract_document"]=="yes") { ?> checked="checked" <?php } ?>/></td>

                   <!--  <td style="text-align: center;"><input type="checkbox" class="flat" name="locationDepartment[<?php echo $locID;?>][<?php echo $deptID;?>][approve_order]" <?php if(!empty($appOrder["approve_order"]) && $appOrder["approve_order"]=="yes") { ?> checked="checked" <?php } ?> /></td> -->
                    <!-- <td style="text-align: center;"><input type="checkbox" class="flat" name="locationDepartment[<?php echo $locID;?>][<?php echo $deptID;?>][approve_travel_expense]"  <?php if(!empty($appExpens["approve_travel_expense"]) && $appExpens["approve_travel_expense"]=="yes") { ?> checked="checked" <?php } ?>/></td> -->
                    <!-- <td style="text-align: center;"><input type="checkbox" class="flat" name="locationDepartment[<?php echo $locID;?>][<?php echo $deptID;?>][approve_invoice]"  <?php if(!empty($appInvoice["approve_invoice"]) && $appInvoice["approve_invoice"]=="yes") { ?> checked="checked" <?php } ?>/></td> -->
                    <!-- <td style="text-align: center;"><input type="checkbox" class="flat" name="locationDepartment[<?php echo $locID;?>][<?php echo $deptID;?>][amend_budget]" <?php if(!empty($amendBudget["amend_budget"]) && $amendBudget["amend_budget"]=="yes") { ?> checked="checked" <?php } ?>/></td> -->
                    <td style="text-align: center;"><input type="checkbox" class="apply_permission"  name="locationDepartment[<?php echo $locID;?>][<?php echo $deptID;?>][contract_view]" <?php if(!empty($contractView["contract_view"]) && $contractView["contract_view"]=="yes") { ?> checked="checked" <?php } ?>/></td>
                    <td style="text-align: center;"><input type="checkbox" class="apply_permission" name="locationDepartment[<?php echo $locID;?>][<?php echo $deptID;?>][contract_edit]" <?php if(!empty($contractEdit["contract_edit"]) && $contractEdit["contract_edit"]=="yes") { ?> checked="checked" <?php } ?>/></td>
                    </tr>
               <?php } ?>
                </tbody>

            </table>
        </div>
    <?php } */?>
  </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="checkbox" style="margin-left: -19px;">
        <input type="hidden" class="flat notranslate" name="status" id="status" value="1"
          <?php if (!empty($user['status']) || empty($user_id)) echo 'checked="checked"'; ?>> 
       </div>
      <div class="clearfix"></div><br />
    <!--     Deactivated users are unable to log into oboloo or receive notifications but are still charged a licence fee. To remove this licence completely please delete this user' TO When you untick the above checkbox this licence will expire on the next billing cycle. -->
        <br /><br />
    <?php if(!empty($user_id) && $db['is_trial']=="1"){ $disabled = FunctionManager::sandbox(); ?>
       <a  href="javascript:void(0);"  onclick="$('#user_form').submit();" <?php echo $disabled; ?>>
        <button type="button" <?php echo $disabled; ?> class="btn btn-success"  style="background-color: #1abb9c !important; border-color: #1abb9c !important">
            <?php if (isset($user_id) && $user_id) echo 'Save User'; else echo 'Add User'; ?>
        </button>
    </a>
    <?php }else if($db['is_trial']=="0" && ($user['status_payment']==1 or $user['status']==0)){

        if($user['status_payment']==1){
         $poupTitle="Are you sure you want to reactive this user? Your organisation will be charged from today for an additional licence using your organisations stored payment method. The licence will be charged on a pro rata basis (how many days you have until your organisations next billing date). Please confirm";
        }else{
           $poupTitle="Are you sure you want to add a new user? Your organisation will be charged from today for an additional licence using your organisations stored payment method. The licence will be charged on a pro rata basis (how many days you have until your organisations next billing date). Please confirm";
        }
    ?>
    <a href="javascript:void(0);" <?php echo $disabled; ?> onclick="checkUser('<?php echo $poupTitle;?>');" >
        <button type="button" class="btn btn-success"  style="background-color: #1abb9c !important; border-color: #1abb9c !important" <?php echo $disabled; ?>>
            <?php if (isset($user_id) && $user_id) echo 'Save User'; else echo 'Add User'; ?>
        </button>
    </a>
    <?php } else {?>
    <a href="javascript:void(0);" onclick="$('#user_form').submit();" <?php echo $disabled; ?> >
        <button type="button" class="btn btn-success"  style="background-color: #1abb9c !important; border-color: #1abb9c !important" <?php echo $disabled; ?>>
            <?php if (isset($user_id) && $user_id) echo 'Save User'; else echo 'Add User'; ?>
        </button>
    </a>
    <?php } ?>
   
  </div>  
    </div>
  </form>


  <!-- Start: Button User Deactivate -->
  <?php if(!empty($user['status']) && $user['status'] != 0 && $user['status'] == 1){ ?>
    <form id="deactived_form" method="post" action="<?php echo AppUrl::bicesUrl('usersUsg/userDeactivate'); ?>" style="margin-left: 130px;">
      <input type="hidden" name="user_status" value="<?php echo $user['user_id']; ?>"  />
       <a href="javascript:void(0);" <?php echo $disabled; ?> onclick="checkDeactivateUser();" >
        <button type="button" <?php echo $disabled; ?> class="btn btn-danger" style="margin-top: -49px; width: 139px; position: absolute;">Deactivate User</button>
      </a>
    </form>
  <!-- End: Button User Deactivate -->

  <?php }  else if(!empty($user['user_id']) && $user['user_id'] != 0 && $db['is_trial'] == 1){ ?> 
     <!-- Start: Button User Activation -->
     <form id="actived_form" method="post" action="<?php echo AppUrl::bicesUrl('usersUsg/userActivate'); ?>" style="margin-left: 130px;">
      
       <input type="hidden" name="user_status" value="<?php echo $user['user_id']; ?>"  />
       <a href="javascript:void(0);" <?php echo $disabled; ?>  >
        <button type="submit" name="submit" <?php echo $disabled; ?> class="btn btn-success" style="margin-top: -49px; width: 139px; position: absolute;">Activate User</button>
      </a>
    </form>
    <?php } else if(!empty($user['user_id']) && isset($user['user_id']) && $user['user_id'] != 0  && $db['is_trial'] == 0){?>
       <!-- Start: Button User Activation -->
     <form id="actived_form" method="post" action="<?php echo AppUrl::bicesUrl('usersUsg/userActivate'); ?>" style="margin-left: 130px;">
      <input type="hidden" name="user_status" value="<?php echo $user['user_id']; ?>"  />
      <a href="javascript:void(0);" <?php echo $disabled; ?> onclick="checkActivateUser();" >
        <button type="button" <?php echo $disabled; ?> class="btn btn-success" style="margin-top: -49px; width: 139px; position: absolute;">Activate User</button>
      </a>
    </form>
    <?php } ?>
    <!-- End: Button User Activation -->
</div>
<!-- <div class="col-md-6 col-xs-12"> <br /><br /><br />
<strong>Super Users</strong> <br /><br />Have control of the entire oboloo software including admin rights. This include:<br /><br />
• Managing user accounts <br />
• Configuring drop down options <br />
• Adding currencies <br />
• Adding locations & departments <br />
• Company settings (system date formats, default currency etc) <br />
• Editing approval levels <br /><br /><br />


<strong>Regular Users</strong> <br /><br /> Have no admin rights. Regular users are able to create and edit Suppliers, Contracts and Sourcing activities. They can also approve contract documents (if a Super User has given them access rights to do so)
  </div> -->
</div>
</div>
<?php /*if(empty($user['user_id'])){ ?>
   <!-- Modal -->
    <div class="modal fade" id="userPermissionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <h4 class="subheading text-center" style="margin: 20px;">Do you want this user to have full approval access under contract management? These settings can be changed later</h4>
          <div class="modal-body user_modal_body">
           <form id="FormFilters" >
            <div class="row text-center" style="margin-left:0px">
                  <label class="user_container">Yes
                  <input type="checkbox" id="checkAll">
                  <span class="checkmark"></span>
                  </label>
                  <label class="user_container">No
                  <input type="checkbox" id="closePupop">
                  <span class="checkmark"></span>
                  </label>
              <div class="modal-footer" style="padding:0px; border: 0px;">
                    <!-- <button type="button" class="btn btn-red" data-dismiss="modal" style="background: red; color: white;">Cancel</button> -->
                    <!-- <button type="button" class="btn btn-green checkAllPermission" style="background: green; color: white;">Ok</button> -->
                  </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  <?php } */?>
<script type="text/javascript">
   /*window.onload = function(e) {
     $('#userPermissionModal').modal('show'); 
   };*/

 //   $("#checkAll").click(function () {
 //     //$('#yes_permission').attr('checked', true);
 //    $('input:checkbox').not(this).prop('checked', this.checked);
 //     //$("#yes_permission").prop('checked', true);
      
 // });

     /*$(document).ready(function(){

     $('#checkAll').on('change',function(){
         //$('.yes_permission').prop('checked',$(this).prop('checked'));
         if($(this).is(':checked')){
            $(".apply_permission").prop('checked', true);
          }else{
             $(".apply_permission").prop('checked', false);
          }
          $('#userPermissionModal').modal('hide');
      });

      
      $('#closePupop').on('change',function(){
        $('#userPermissionModal').modal('hide');
      });

    })*/
     
   // $("#checkAll").on("click", function () {
   //      $("input:checkbox[class=yes_permission]:checked").each(function () {
   //          alert("Id: 234234");
   //      });
   //       $('#userPermissionModal').modal('hide');
   //  });
   

    function loadDepartmentsForSingleLocation(department_id)
    {
        var location_id = $('#location_id').val();
        var single = 1;

        if (location_id == 0)
            $('#department_id').html('<option value="">Select Department</option>');
        else
        {
            $.ajax({
                type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
                url: BICES.Options.baseurl + '/locations/getDepartments',
                success: function(options) {
                    var options_html = '<option value="">Select Department</option>';
                    for (var i=0; i<options.length; i++)
                        options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                    $('#department_id').html(options_html);
                    if (department_id != 0) $('#department_id').val(department_id);
                },
                error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
            });
        }
    }

$(document).ready( function() {

	<?php if (isset($user['location_id']) && !empty($user['location_id'])) { ?>
	<?php if (isset($user['department_id']) && !empty($user['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $user['department_id']; ?>);
	<?php } else { ?>
    loadDepartmentsForSingleLocation(0);
	<?php } ?>

	<?php } ?>

    $( "#user_form" ).validate( {
        rules: {
            full_name: "required",
            username: "required",
            email: { required: true, /*email: true*/ },
            position: "required",
            location_id: "required",
            department_id: "required",
            user_role: "required",
            user_type: {required: true},
           /* <?php if (!isset($user_id) || empty($user_id)) { ?>
	            password: "required",
	        <?php } ?>
            confirm_password: { equalTo: "#password" }*/
        },
        messages: {
            full_name: "Full Name is required",
            username: "Username is required",
            email: "Enter valid email",
            position: "Position is required",
            location_id: "Location is required",
            department_id: "Department is required",
            user_role: "User Role is required",
            user_type: "Select User Role is required",
        },
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-error" ).removeClass( "has-success" );
			$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-success" ).removeClass( "has-error" );
			$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });
    
});

function checkUser(popupTitle) {
   var popupTitle = popupTitle;
   $.confirm({
            title: false,
            content: '<spam style="font-size:13px">'+popupTitle+'</span>',
            buttons: {
                      Yes: {
                            text: "confirm ",
                            btnClass: 'btn-green',
                            action: function(){
                               $('#user_form').submit();
                            }
                          },
                      No: {
                            text: "Cancel",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },          
            }});
}

  function checkDeactivateUser(){
     $.confirm({
            title: false,
            content: '<spam style="font-size:13px">This will remove this licence from your organisations oboloo account on your next billing date. You will still be able to use the licence until this date. You can also move this licence across to a different user until it is removed by changing the email address associated to it. Please confirm</span>',
            buttons: {
                      Yes: {
                            text: "confirm ",
                            btnClass: 'btn-green',
                            action: function(){
                               $('#deactived_form').submit();
                            }
                          },
                      No: {
                            text: "Cancel",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },          
            }});
  }

   function checkActivateUser(){
     $.confirm({
            title: false,
            content: '<spam style="font-size:13px"> Are you sure you want to reactive this user? Your organisation will be charged from today for an additional licence using your organisations stored payment method. The licence will be charged on a pro rata basis (how many days you have until your organisations next billing date). Please confirm</span>',
            buttons: {
                      Yes: {
                            text: "confirm ",
                            btnClass: 'btn-green',
                            action: function(){
                               $('#actived_form').submit();
                            }
                          },
                      No: {
                            text: "Cancel",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },          
            }});
  }

$('#email').bind('input', function(){
    this.value=$(this).val().trim();
}); 

</script>
<style type="text/css">
  input[type=checkbox]{ width: 20px; border: none;  height: 20px;}
  .user_role_type:hover{border-color:#5bc0de !important;}
</style>