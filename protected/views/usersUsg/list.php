 Add User
<div class="right_col" role="main">
<div class="col-md-12">
    <div class="tile_count">
        <div class="span6 pull-left">
            <h3>Users</h3>
             <!-- <p>To add a new user please contact your Account Manager or hello@oboloo.com</p> -->
        </div>


        <?php //if(Yii::app()->session['admin_flag']==1){?>
        <div class="span6 pull-right">
           <!--  <a href="<?php echo AppUrl::bicesUrl('usersUsg/edit/0'); ?>">
                <button type="button" class="btn btn-warning">
                    <span class="glyphicon glyphicon-plus mr-2" aria-hidden="true"></span> Add User
                </button>
            </a> -->
            <br>
          <!--   <a href="https://oboloo.com/support-hub/#Userroles">
                <button type="button" class="btn btn-default user_role_type" style="background-color: #5bc0de;color: #fff;">
                  User Role Types
                </button>
            </a> -->
        </div>
      <?php // }?>
        <div class="clearfix"> </div>
    </div>
    <?php 
    $message = Yii::app()->user->getFlash('success');
    $msg = Yii::app()->user->getFlash('danger');
    if(!empty($message) ): ?>
    <div class="alert alert-success">
         <strong>Success!</strong> <?php echo $message; ?>
    </div>
    <?php endif; 
    if(!empty($msg) ): ?>
    <div class="alert alert-success">
         <strong>User</strong> <?php echo $msg; ?>
    </div>
    <?php endif; ?>
    <table id="user_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th> </th>
          <th>Name</th>
          <th>Type</th>
          <th>Email</th>
          <th>Position</th>
          <th>Username</th>
          <th>Status</th>
        </tr>
      </thead>

      <tbody>

          <?php foreach ($users as $user) { ?>

              <tr>
                    <td>
                        <a href="<?php echo AppUrl::bicesUrl('usersUsg/edit/' . $user['user_id']); ?>">
                            <button class="btn btn-success btn-sm view-btn">Edit</button>
                        </a>
                    </td>
                    <td class="notranslate"><?php echo $user['full_name']; ?></td>
                    <td><?php echo $user['user_type']; ?></td>
                    <td class="notranslate"><?php echo $user['email']; ?></td>
                    <td><?php echo $user['position']; ?></td>
                    <td class="notranslate"><?php echo $user['username']; ?></td>
                    <td><?php echo 
$user['status'] != 0 ? 'Active': ($user['status'] == 0 && $user['soft_deleted'] == 0?'Licence Active, Being Removed On '.SubscriptionManager::getNextBilling():'Inactive'); ?></td>
                    
              </tr>

          <?php } ?>

      </tbody>

  </table>
</div>

</div>

<script type="text/javascript">
$(document).ready( function() {
    $('#user_table').dataTable({
        "columnDefs": [ {
            "targets": 0,
            //"width": "6%",
            "orderable": false
        } ],
        "order": []
    });
})
</script>
<style>
.user_role_type:hover{border-color:#5bc0de !important;}
</style>
