<?php if(!empty($documentListPendingRequest)){?>
<div class="x_title pull-left" style="border-bottom:none;width: 71%"><h4 class="heading"> Requested Refreshed Pending Documents</h4></div>
<?php
$previousType='';
foreach($documentListPendingRequest as $key=>$value) {
  $documentType = $value['type_id'];
  
if($previousType != $documentType){
    $tableID = "doctype_request".$documentType;
  if(!empty($previousType)){
  ?>
</tbody>
</table><br />
<?php } ?>
<table id="<?php echo $tableID;?>" class="table table-striped table-bordered" style="width: 100%;">
<thead>
<tr><th colspan="9"><?php echo FunctionManager::vendorDocumentOnboard($documentType);?></th></tr>
<tr>
    <th style="min-width: 50%">Document Type</th>
    <th style="width: 15%">Supplier Contact Name</th>
    <th style="width: 15%">Supplier Contact Email</th>
    <th style="width: 12%">Date Created</th>
    <th style="width: 8%">Action</th>
</tr>
</thead>
<tbody>
  <?php } 
      $supplierContactName = unserialize($value['supplier_contact_name']);
      $supplierContactEmail = unserialize($value['supplier_contact_email']);
      $disabled = FunctionManager::sandbox();
    ?>
  <tr>
    <td><span class="btn btn-success doc-btn-round" style="<?php echo FunctionManager::vendorDocumentBgColorOnboard($documentType);?>"><?php echo FunctionManager::vendorDocumentOnboard($documentType);?></span></td>
    <td><?php echo $supplierContactName[0];?></td>
    <td><?php echo $supplierContactEmail[0];?></td>
    <td><?php echo date(FunctionManager::dateFormat(),strtotime($value['created_at']));?></td>
    <td><a style="cursor: pointer; padding: 5px;" <?php echo $disabled; ?> id="delete_document_request<?php echo $value['request_id'];?>" onclick="deleteDocumentPendingOnboard(<?php echo $value['request_id']; ?>)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
      </a>
    </td>
  </tr>
<?php   $previousType = $documentType; }
  if(!empty($previousType)){?>
    </tbody>
    </table><br /><br />
  <?php } } ?>

