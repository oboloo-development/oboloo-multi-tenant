<div class="pull-left">
  <a href="javascript:void(0)" onclick="$('#supplier_contract_modal').modal('show');" class="btn btn-warning " >Add New Contact For Supplier</a>
</div>
<div class="clearfix"><br /></div>
  <table id="supplier_contract_table" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
      <tr>
        <th>Contact Name</th>
        <th>Contact Email</th>
        <th>Position</th>
        <th>Phone Number</th>
        <th>Location</th>
        <th>Created By</th>
        <th>Date & Time</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
      </tbody>
  </table>
  <div class="clearfix"> </div>

  <script type="text/javascript">
    $(document).ready(function(){
        $("#supplier_contract_alert").hide();
        $("#supplier_contract_form").submit(function(e){
         e.preventDefault();
          $.ajax({
          type: "POST", data: $('#supplier_contract_form').serialize(), dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('vendors/createSupplierContract'); ?>",
          beforeSend: function() {
             $("#new_contract_submit").attr("disabled","disabled");
          },
          success: function(options) {
            $("#new_contract_submit").removeAttr("disabled");
            if(options.code ==1){
              $('#supplier_contract_table').DataTable().ajax.reload();
              $("#supplier_contract_alert").show().html("Suppplier Contact added successfully.");
              setTimeout(function() {
                  $("#supplier_contract_alert").hide('blind', {}, 1000)
              }, 5000);
              $(".resetform").val('');
            }else if(options.code ==2){
              $("#supplier_contract_alert").show().html('Suppplier with the same <b>Contact Email</b> is already taken');
              setTimeout(function() {
                  $("#supplier_contract_alert").hide('blind', {}, 1000)
              }, 5000);
            }
          },
      });
        });
        $('#supplier_contract_table').dataTable({
        "columnDefs": [ {
            "targets": -1,
            "width": "6%",
           // "orderable": false
        } ],
        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
              for (var i=1; i<=6; i++)
                  $('td', row).eq(i).css('text-decoration', 'line-through');
        },        
        "order": [],
        "pageLength": 50,        
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?php echo AppUrl::bicesUrl('vendors/getSupplierContract'); ?>",
          "type": "POST",
          data: function ( input_data ) {
           input_data.vendor_id = '<?php echo $vendor_id;?>';
        } 
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          if(aData[1].search("<span class='deactivated_vendor'></span>")>0){
            $(nRow).addClass('deactivated_record');
            /*$(nRow).css("background-color", "#ccc");*/
          }
         }
    });
    });

    function deleteSupplierContact(ID){
      var contactID = ID;
      $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Would you like to delete this supplier contact?</span>',
            buttons: {
              Yes: {
                    text: "Yes",
                    btnClass: 'btn-blue',
                    action: function(){
                      $.ajax({
                          type: "POST", data: {id:contactID}, dataType: "json",
                          url: "<?php echo AppUrl::bicesUrl('vendors/deleteSupplierContract'); ?>",
                          success: function(options) {
                            $('#supplier_contract_table').DataTable().ajax.reload();
                          },
                      });
                      }
                  },
              No: {
                    text: "No",
                    btnClass: 'btn-red',
                  },
            }
        });
    }
  </script>