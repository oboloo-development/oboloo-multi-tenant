<?php
$disabledArchiveVendor = '';
// this disabled variable targer the archive and un-archive checkbox in the list.
if (strtolower($disabledArchive) == 'yes') {
  $disabledArchiveVendor = 'disabled';
} else {
  $disabledArchiveVendor = '';
} ?>
<div class="col-md-12 col-sm-12 col-xs-12 p-0">

  <div class="pull-left" style="width: 71%">
    <h4 class="heading">Documents</h4>
    <div class="clearfix"></div><br />
    <div class="col-md-6 col-sm-6 col-xs-16">
      <div id="owner_list_cont"></div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-6 col-sm-6 col-xs-12 p-0">
    <h4 class="heading ownerpermission" style="font-size:15px;">Upload A Document On Behalf Of A Supplier</h4>
  </div>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <?php if (strtolower($disabledArchive) != 'yes') { ?>
      <button type="button" class="btn request-cntrt-css ownerpermission pull-right" onclick="$('#request_contract_document_modal').modal('show');">
        Request A New Document From Supplier
      </button>
      <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#uploadContractDocumentType">Upload Document</button>
    <?php } ?>
  </div>
  

  <div class="clearfix"></div><br /><br />
  
  <div id="document_list_cont">
    <?php
    $this->renderPartial('_documents', array(
      'documentList' => $documentList, 'documenArchivetList' => $documenArchivetList,
      'vendor_id' => $vendor_id, 'disabledArchive' => $disabledArchive,
      'filter_document_type' => $filter_document_type,
      'flter_document_status' => $flter_document_status,
      'fltr_doc_contract_supplier_status' => $fltr_doc_contract_supplier_status
    )); ?>
  </div>
<?php $this->renderPartial('_vendor_document', array('vendor_id' => $vendor_id)); ?>
  <script>
    $(document).ready(function() {
      $('#order_file_1').bind('change', function() {
        var MAX_FILE_SIZE = 101 * 1024 * 1024;
        if (this.files[0].size > MAX_FILE_SIZE) {
          alert("Maximum File Size of 100MB");
          this.value = "";
        }
      });
    });
  </script>
  <style>
    .request-cntrt-css {
      background-color: #F79820 !important;
      color: #fff !important;
      border-color: #F79820 !important;
    }

    .request-cntrt-css:hover {
      background-color: #F79820 !important;
      color: #fff !important;
      border-color: #F79820 !important;
    }
  </style>
</div>
<div id="uploadContractDocumentType" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 1000px !important;">
    <!-- Modal content-->
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Document</h4>
      </div>
      <div class="modal-body">
        <div class="clearfix"></div><br />
            <div class="spinnerContainer" class="text-center ">
              <div class="progress">
                <div id="progressBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                </div>
              </div>
            </div>
            <div class="form-group" id="document_area_1" style="margin-bottom: 0px;">
              <div class="col-md-3 col-sm-3 col-xs-12">
                <label class="control-label">Document <span style="color: #a94442;">*</span></label>
                <input class="form-control" type="file" name="order_file_1" id="order_file_1" />
                <span class="text-danger" style="font-size: 10px;">Maximum File Size of 100MB</span>

              </div>
              <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 0px;">
                <label class="control-label">Document Title <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" />
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 0px;">
                <label class="control-label">Document Type <span style="color: #a94442;">*</span></label>
                <?php $documentsTitle = FunctionManager::vendorDocument(); ?>
                <select class="form-control" name="document_type_1" id="document_type_1">
                  <option value="">Select Document Type</option>
                  <?php foreach ($documentsTitle as $key => $value) { ?>
                    <option value="<?php echo $key ?>"><?php echo $value; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12" style="margin-bottom: 0px;">
                <label class="control-label">Expiry Date <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control expiry_date_1" name="expiry_date_1" id="expiry_date_1" placeholder="Expiry Date">
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12 text-right"><br />
                <?php $disabled = FunctionManager::sandbox(); ?>
                <button id="btn_upload_1" class="btn btn-info submit-btn" 
                <?php echo $disabled; echo $disabledArchiveVendor; ?> 
                  onclick="uploadDocument(1,event);" style="margin-top: 8px;">Upload</button>
              </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>