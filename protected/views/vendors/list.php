<div class="right_col" role="main">
  <!-- <div class="vendor-tutorial"></div> -->
  <div class="row-fluid tile_count">
    <div class="span6 pull-left col-md-6 col-xs-12">
      <h3>Supplier Management</h3>
    </div>
    <div class=" col-md-6 col-xs-12 span6 text-right">
      <div class="clearfix"></div>

      <button type="button" class="btn btn-warning click-modal-poup supplier-btn-tour hidden-xs" onclick="addVendorModal(event);" style="">
        <span class="glyphicon glyphicon-plus mr-1" aria-hidden="true"></span> Add Supplier
      </button>
      <a href="<?php echo AppUrl::bicesUrl('vendors/archiveList'); ?>" class="mbl-vw-btn">
        <button type="button" class="btn btn-info mbl-vw-btn">
          <span class="glyphicon glyphicon-list mr-2" aria-hidden="true"></span> Archived Suppliers
        </button>
      </a>
      <div class="clearfix"></div>
      <!--   <a href="<?php echo AppUrl::bicesUrl('vendors/edit/0'); ?>">
                <button type="button" class="btn btn-warning">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Supplier ( OLD )
                </button>
            </a> -->

      <?php if (isset(Yii::app()->session['admin_flag']) && Yii::app()->session['admin_flag'] == 1) { ?>

        <!-- <a href="<?php /*echo AppUrl::bicesUrl('vendors/import'); */ ?>">
	                <button type="button" class="btn btn-default">
	                    <span class="glyphicon glyphicon-import" aria-hidden="true"></span> Upload CSV
	                </button>
	            </a>-->

      <?php } ?>
    </div>
    <div class="clearfix"> </div>
    <?php if (!empty(Yii::app()->user->hasFlash('success'))) { ?>
      <?php echo Yii::app()->user->getFlash('success'); ?>
    <?php } ?>

    <div class="clearfix"> </div>
    <div class="alert alert-success vendor_alert" style="display: none;"><strong>Success!</strong> Supplier submitted successfully.</div>


    <!--START: supplier Metrice  -->
    <div class="clearfix"> </div>
    <div class="row-fluid">
      <div class="tile_count mbl_view_graphs">
        <div class="col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats1 " style="background-color: #48d6a8 !important;">
          <h4 class="contract-metrice supplier-metrice" onclick="$('#number_of_preferred_supplier').modal('show');">
            <?php echo number_format($vendorMetric['preferredVendors']); ?><br />
            Preferred Suppliers</h4>
        </div>

        <div class="col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats2" style="background-color: #efa65f !important">
          <h4 class="contract-metrice supplier-metrice" onclick="$('#number_Of_Suppliers_With_Contract').modal('show');">
            <?php echo number_format($vendorMetric['vendorUnderContract']); ?><br />
            Suppliers With A Contract</h4>
        </div>

        <div class="col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats3" style="background-color: #5bc0de !important">
          <h4 class="contract-metrice supplier-metrice" onclick="$('#active_Documents_Need_Approval').modal('show');">
            <?php echo number_format($vendorStatistics['documentPending']); ?><br />
            Documents Needing Approval</h4>
        </div>

        <div class="col-md-3 col-sm-4 col-xs-12 tile_stats_count cont-metrice tile_stats4 " style="background-color: #f7778c !important">
          <h4 class="contract-metrice supplier-metrice " onclick="$('#documents_Have_Expired').modal('show');"><?php echo number_format($vendorStatistics['documentExpired']); ?><br />
            Documents That Have Expired</h4>
        </div>
      </div>
    </div>
    <!--END: supplier Matrix  -->

    <div class="supplier-graphs">
      <?php $this->renderPartial("_list_statistic", array('vendorStatistics' => $vendorStatistics, 'vendors_by_category' => $vendors_by_category, 'top_contracts' => $top_contracts, 'performance_avg' => $performance_avg, 'notifications' => $notifications)); ?>
    </div>

    <div class="row-fluid tile_count">
      <form name="vendor_list_form" id="vendor_list_form" class="form-horizontal row" role="form">
        <div class="saving_filter_container">
          <div class="saving_filter_input">
            <select name="location_id" id="location_id" class="form-control select_industry_multiple border-select" onchange="loadDepartmentsForSingleLocation(0);">
              <option value="">Select Locations</option>
              <?php foreach ($locations as $location) { ?>
                <option value="<?php echo $location['location_id']; ?>" <?php if (isset($location_id) && $location_id == $location['location_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $location['location_name']; ?>
                </option>
              <?php } ?>
            </select>
          </div>
          <div class="saving_filter_input">
            <select name="department_id" id="department_id" class="form-control select_subindustry_multiple border-select">
              <option value="">Select Departments</option>
            </select>
          </div>

          <div class="saving_filter_input">
            <select name="industry_id" id="industry_id" class="form-control select_industry_multiple border-select" onchange="loadSubindustries();">
              <option value="">All Industries</option>
              <?php foreach ($industries as $industry) { ?>
                <option value="<?php echo $industry['id']; ?>" <?php if (isset($industry_id) && $industry_id == $industry['id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $industry['value']; ?>
                </option>
              <?php } ?>
            </select>
          </div>
          <div class="saving_filter_input">
            <select name="subindustry_id" id="subindustry_id" class="form-control select_subindustry_multiple border-select">
              <option value="">All Subindustries</option>
            </select>
          </div>

          <div class="saving_filter_input">
            <select name="supplier_status" id="supplier_status" class="form-control select_status_multiple border-select">
              <option value="">All Suppliers &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</option>
              <option value="1">Active Suppliers</option>
              <option value="0">Deactivated Suppliers</option>
            </select>
          </div>
          <div class="saving_filter_input">
            <select name="preferred_flag" id="preferred_flag" class="form-control select_type_multiple border-select">
              <option value="-1">Select Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</option>
              <option value="1" <?php if (isset($preferred_flag) && $preferred_flag == 1) echo ' selected="SELECTED" '; ?>>Preferred</option>
              <option value="0" <?php if (isset($preferred_flag) && $preferred_flag == 0) echo ' selected="SELECTED" '; ?>>Non-Preferred</option>
            </select>
          </div>
          <div class="saving_filter_input">
            <div style="display: grid;">
              <button class="btn btn-info search-quote" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
              <button class="btn btn-primary " onclick="loadVendors(); return false;" style="margin-top: 0px !important;">Apply Filters</button>
            </div>
          </div>
        </div>
      </form>
    </div>

    <table id="vendor_table" class="table table-striped table-bordered suppliers-table" style="width: 100%">
      <thead>
        <tr>
          <th style="width: 6%;" tabindex="-1" class="nosort"> </th>
          <th class="notranslate" style="width: 16%;">Name</th>
          <th style="width: 15%;">Industry</th>
          <th style="width: 15%;">Sub Industry</th>
          <th style="width: 14%;text-align: center;">Documents Expired</th>
          <th style="width: 16%;text-align: center;">Documents to Approve</th>
          <!--  
          <th>Contact Name</th>
          <th>Phone</th>
           -->
          <th style="width: 11%;text-align: center;">Performance Score</th>
          <th style="width: 4%;text-align: center;">Preferred</th>
          <th style="width:4%;text-align: center;">Contract</th>
          <th style="width:4%;text-align: center;">Sustainability</th>

          <th style="width:2%;text-align: center;">Risk</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>

  <div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">De-Activate Supplier Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to de-activate this vendor: <span id="delete_name" style="font-weight: bold;"></span>?</p>
          <input type="hidden" name="delete_item_id" id="delete_item_id" value="0" />
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
            NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
            YES
          </button>
        </div>
      </div>
    </div>
  </div>

  <!--Start:Number of preferred supplier -->
  <div class="modal fade" id="number_of_preferred_supplier" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size: 18px;">Number Of Preferred Suppliers</h4>
        </div>
        <div class="clearfix"> </div>
        <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
              <table id="preferred_supplier_value" class="table table-striped table-bordered preferred_supplier_value" style="width: 100%;">
                <thead>
                  <tr>
                    <th>Supplier Name</th>
                    <th>Industry</th>
                    <th>Sub Industry</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($vendorMetric['preVerndorList'] as $modelVendorList) { ?>
                    <tr>
                      <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $modelVendorList['vendor_id']); ?>" style="text-decoration: none;"><?php echo $modelVendorList['vendor_name']; ?></a></td>
                      <td><?php echo $modelVendorList['industry']; ?></td>
                      <td><?php echo $modelVendorList['subindustry']; ?></td>
                    </tr>
                  <?php  } ?>
                </tbody>

              </table>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
  <!--End:Number of preferred supplier-->

  <!--Start:Number Of Suppliers With A Contract -->
  <div class="modal fade" id="number_Of_Suppliers_With_Contract" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size: 18px;">Number Of Suppliers With A Contract</h4>
        </div>
        <div class="clearfix"> </div>
        <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
              <table id="suppliers_With_Contract" class="table table-striped table-bordered contractWithsupplier" style="width: 100%;">
                <thead>
                  <tr>
                    <th>Supplier Name</th>
                    <th>Industry</th>
                    <th>Sub Industry</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($vendorMetric['vendorUnderContractList'] as $modelVendorList) { ?>
                    <tr>
                      <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $modelVendorList['vendor_id']); ?>" style="text-decoration: none;"><?php echo $modelVendorList['vendor_name']; ?></a></td>
                      <td><?php echo $modelVendorList['industry']; ?></td>
                      <td><?php echo $modelVendorList['subindustry']; ?></td>
                    </tr>
                  <?php  } ?>
                </tbody>

              </table>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
  <!--End:Number Of Suppliers With A Contract-->

  <!--Start:Number Of Active Documents That Need Approval -->
  <div class="modal fade" id="active_Documents_Need_Approval" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size: 18px;">Number Of Active Documents That Need Approval</h4>
        </div>
        <div class="clearfix"> </div>
        <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

          <div class="tab-content">
            <div role="tabpanel" class="tab-panel active" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
              <table id="document_need_approval" class="table table-striped table-bordered need_approval_table" style="width: 100%;">
                <thead>
                  <tr>
                    <th>Supplier Name</th>
                    <th>Document Title</th>
                    <th>Expiry Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($vendorStatistics['documentNeedApproval'] as $modelVendorList) { ?>
                    <tr>
                      <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $modelVendorList['vendor_id']); ?>" style="text-decoration: none;"><?php echo $modelVendorList['vendor_name']; ?></a></td>
                      <td><?php echo $modelVendorList['document_title']; ?></td>
                      <td data-sort='<?php echo date("Y-m-d", strtotime($modelVendorList['expiry_date'])); ?>'><?php echo date(FunctionManager::dateFormat(), strtotime($modelVendorList['expiry_date'])); ?></td>
                    </tr>
                  <?php  } ?>
                </tbody>

              </table>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
  <!--End:Number Of Active Documents That Need Approval-->

  <!--Start:Number Of Documents That Have Expired -->
  <div class="modal fade" id="documents_Have_Expired" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size: 18px;">Number Of Documents That Have Expired</h4>
        </div>
        <div class="clearfix"> </div>
        <div class="row1 tile_count" role="tabpanel" data-example-id="togglable-tabs" style="margin-top: -10px;">

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" aria-labelledby="order-spend-paid-tab" style="padding: 15px;">
              <table id="document_expired_modal" class="table table-striped table-bordered contractExpired_table" style="width: 100%;">
                <thead>
                  <tr>
                    <th>Supplier Name</th>
                    <th>Document Title</th>
                    <th>Expiry Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($vendorStatistics['documentExpiredList'] as $modelVendorList) { ?>
                    <tr>
                      <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $modelVendorList['vendor_id']); ?>" style="text-decoration: none;"><?php echo $modelVendorList['vendor_name']; ?></a></td>
                      <td><?php echo $modelVendorList['document_title']; ?></td>
                      <td data-sort='<?php echo date("Y-m-d", strtotime($modelVendorList['expiry_date'])); ?>'><?php echo date(FunctionManager::dateFormat(), strtotime($modelVendorList['expiry_date']));
                                                                                                              ?></td>
                    </tr>
                  <?php  } ?>
                </tbody>

              </table>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
  <!--End:Number Of Documents That Have Expired-->

  <script type="text/javascript">
    $(document).ready(function() {
      $('#vendor_table').dataTable({
        "columnDefs": [{
          "targets": [6, 7],
          "className": "text-right",

        }],

        "createdRow": function(row, data, index) {
          if (data[0].indexOf('glyphicon glyphicon-ok') >= 0)
            for (var i = 1; i <= 6; i++)
              $('td', row).eq(i).css('text-decoration', 'line-through');
        },
        "order": [
          [1, "asc"]
        ],
        "pageLength": 50,
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?php echo AppUrl::bicesUrl('vendors/list'); ?>",
          "type": "POST",
          data: function(input_data) {
            input_data.preferred_flag = $('#preferred_flag').val();
            input_data.industry_id = $('#industry_id').val();
            input_data.subindustry_id = $('#subindustry_id').val();
            input_data.supplier_status = $('#supplier_status').val();
            input_data.location_id = $('#location_id').val();
            input_data.department_id = $('#department_id').val();
          }
        },
        "oLanguage": {
          "sProcessing": "<h1>Please wait ... retrieving data</h1>"
        },
        "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          if (aData[1].search("<span class='deactivated_vendor'></span>") > 0) {
            $(nRow).addClass('deactivated_record');
            /*$(nRow).css("background-color", "#ccc");*/
          }
        }
      });

      $('#delete_confirm_modal .modal-footer button').on('click', function(event) {
        var button = event.target; // The clicked button

        $(this).closest('.modal').one('hidden.bs.modal', function() {
          if (button.id == 'yes_delete') changeItemStatus();
        });
      });

      var userType = <?= json_encode(Yii::app()->session['user_type']) ?>;
      var baseUrl = <?= json_encode(Yii::app()->getBaseUrl(true)) ?>;
      if (userType == 4 && baseUrl.indexOf("sandbox") === -1) {
        $('#vendor_table_filter').append('<a href="<?php echo AppUrl::bicesUrl('vendors/export'); ?>"><button class="btn btn-yellow pull-right" style="margin-left: 10px;">Export CSV</button></a>');
      }
    });

    function loadVendors() {
      $('#vendor_table').DataTable().ajax.reload();
    }

    function clearFilter() {
      $("option:selected").removeAttr("selected");
      $('.select_industry_multiple').trigger("change");
      $('.select_subindustry_multiple').trigger("change");
      $('.select_type_multiple').trigger("change");
      $('.select_status_multiple').trigger("change");
      $('#vendor_table').DataTable().ajax.reload();
    }

    function select2function(className, lableTitle) {
      var lableTitle = lableTitle;
      $("." + className).select2({
        // placeholder: lableTitle,
        /* allowClear: true*/
      });
    }
    $(document).ready(function() {
      select2function('select_industry_multiple', 'All Locations');
      select2function('select_subindustry_multiple', 'All Departments');
      select2function('select_type_multiple', 'All Categories');
      select2function('select_status_multiple', 'All Sub Categories');
    });

function loadSubindustries(input_subindustry_id)
{
	var industry_id = $('#industry_id').val();
  var input_subindustry_id = input_subindustry_id;
	
	if (industry_id==""){
		$('#subindustry_id').html('<option value="">Select Supplier Sub Industry</option>');
    $('#subindustry_id').removeAttr("required");
    $("#requiredIndi").hide();
	}else
	{
	    $.ajax({
	        type: "POST", data: { industry_id: industry_id, subindustry_id:input_subindustry_id}, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('vendors/getSubIndustries/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="">Select Supplier Sub Industry</option>';
            if(options.suggestions.length){
              $('#subindustry_id'). attr("required", "required");
              $("#requiredIndi").show();
            }else{
               $('#subindustry_id').removeAttr("required");
               $("#requiredIndi").hide();
            }

	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	        	$('#subindustry_id').html(options_html);
	        	if (input_subindustry_id != 0) $('#subindustry_id').val(input_subindustry_id); 
	        },
	        error: function() { $('#subindustry_id').html('<option value="">Select Supplier Sub Industry</option>'); }
	    });
	}
}

    function deleteVendor(item_id) {
      $('#delete_item_id').val(0);
      $('#delete_name').text('');

      var item_name = "";
      var col_index = 0;
      $('#delete_icon_' + item_id).parent().parent().parent().find('td').each(function() {
        col_index += 1;
        if (col_index == 2) item_name = $(this).text();
      });

      $('#delete_item_id').val(item_id);
      if ($('#delete_icon_' + item_id).hasClass('glyphicon-ok')) changeItemStatus();
      else {
        $('#delete_item_id').val(item_id);
        $('#delete_name').text(item_name);
        $('#delete_confirm_modal').modal('show');
      }
    }

    function changeItemStatus() {
      var delete_item_id = $('#delete_item_id').val();
      var active_flag = 0;

      if ($('#delete_icon_' + delete_item_id).hasClass('glyphicon-remove')) {
        active_flag = 0;
        $('#delete_icon_' + delete_item_id).removeClass('glyphicon-remove');
        $('#delete_icon_' + delete_item_id).addClass('glyphicon-ok');
      } else {
        active_flag = 1;
        $('#delete_icon_' + delete_item_id).removeClass('glyphicon-ok');
        $('#delete_icon_' + delete_item_id).addClass('glyphicon-remove');
      }

      $.ajax({
        type: "POST",
        data: {
          delete_item_id: delete_item_id,
          active_flag: active_flag
        },
        url: "<?php echo AppUrl::bicesUrl('vendors/updateItemStatus/'); ?>"
      });

      var col_index = 0;
      $('#delete_icon_' + delete_item_id).parent().parent().parent().find('td').each(function() {
        col_index += 1;
        if (col_index != 1) {
          if (active_flag == 0) $(this).css("text-decoration", "line-through");
          else $(this).css("text-decoration", "none");
        }
      });
    }
    $(".vendor_alert").hide();

    function addVendorModal(e) {
      e.preventDefault();
      $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('vendors/createByModal'); ?>",
        type: "POST",
        data: {
          modalWind: 1
        },
        success: function(mesg) {
          $('#create_vendor_cont').html(mesg);
          $('#create_vendor').modal('show');
        }
      });
    }
  </script>
  <?php $this->renderPartial('/vendors/create_vendor'); ?>
  <?php if (!empty($_GET['from']) && $_GET['from'] == 'information') { ?>
    <script type="text/javascript">
      $(document).ready(function() {
        $('.click-modal-poup').trigger('click');
      });
      $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
      });
    </script>
  <?php } ?>
  <!-- End : Product Tour of Supplier on sandbox -->

  <style type="text/css">
    .location .multiselect { width: 138%; }
    .department .multiselect{ width: 112%; }
    .deactivated_record { background-color: #ccc !important}
    #preferred_supplier_value_wrapper .dataTables_filter,
    #suppliers_With_Contract_wrapper .dataTables_filter,
    #document_need_approval_wrapper .dataTables_filter,
    #document_expired_modal_wrapper .dataTables_filter { display: block;  width: auto; float: none; }
    table tr td:nth-child(8) { text-align: center !important; }
    #vendor_table_filter{width:auto !important; }

    @media (min-width:1025px) {
      .supplier-type {
        width: max-content
      }
    }

    @media only screen and (min-width: 992px) {
      .supplier-type {
        width: max-content
      }
    }

    @media only screen and (max-width: 490px) {
      .supplier-type {
        width: auto;
        margin-top: 20px;
      }

      .search-supplier,
      #vendor_list_form .col-xs-6 {
        margin-top: 20px;
      }
    }

    #vendor_table {
      -webkit-overflow-scrolling: auto;
    }
  </style>
  <script>
    $(document).ready(function() {
      $('.preferred_supplier_value').dataTable();

      $('.contractExpired_table').dataTable({
        order: [
          [2, 'desc']
        ],
      });

      $('.contractWithsupplier').dataTable();

      $('#document_need_approval').dataTable({
        order: [
          [2, 'desc']
        ],
      });
    });

    function loadDepartmentsForSingleLocation(department_id) {
      var location_id = $('#location_id').val();
      if (location_id == 0)
        $('#department_id').html('<option value="0">Select Departments</option>');
      else {
        $.ajax({
          type: "POST",
          data: {
            location_id: location_id
          },
          dataType: "json",
          url: BICES.Options.baseurl + '/locations/getDepartments',
          success: function(options) {
            var options_html = '<option value="0">Select Departments</option>';
            for (var i = 0; i < options.length; i++) {
              options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
            }
            $('#department_id').html(options_html);
            if (department_id != 0) $('#department_id').val(department_id);
          },
          error: function() {
            $('#department_id').html('<option value="0">Select Departments</option>');
          }
        });
      }
    }
  </script>