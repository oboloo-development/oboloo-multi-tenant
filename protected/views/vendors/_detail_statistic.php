<div class="row tile_count">
  <div class="col-md-4 col-sm-4 col-xs-12 hidden-xs"><div id="daysuntilexpiry"></div></div>
  <div class="col-md-4 col-sm-4 col-xs-12 hidden-xs"><div id="approved_document"></div></div>
  <div class="col-md-4 col-sm-4 col-xs-12 hidden-xs"><div id="completed_field"></div></div>
</div>
<script type="text/javascript">

  var options = {
  chart: {
    height: 260,
    type: "radialBar",
    
    sparkline: {
            enabled: true
          }
  },

  series: [<?php echo $vendorStatistics['documentExpiredPercentage']; /*$vendorStatistics['respondedQuotesPercentage'];*/?>],
  labels: ['Documents Expired'],
  colors: ["#f7778c"],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "65%",
        background:'#fff'
       
      },
       startAngle: -100,
       endAngle: 100,
      track: {
        strokeWidth: 60,
        strokeLinecap:'Square',
        dropShadow: {
          enabled: true,
           top:0,
          left:0,
          blur:0,
          opacity:0.5
          
        }
      },
      dataLabels: {
        name: {
         offsetY: -20,
          color: "#f7778c",
          fontSize: "12px",
          fontFamily: 'Poppins',
        },
        value: {
          color: "#f7778c",
          fontSize: "12px",
          show: true,
          offsetY: -60,
          fontFamily: 'Poppins',
          formatter: function (val) {
            //return '';
            return val+"%";
          }
        }
      }
    }
  },
  
  stroke: {
    lineCap: "Square"
  },
  
};

var chart = new ApexCharts(document.querySelector("#daysuntilexpiry"), options);

chart.render();

var options = {
  chart: {
    height: 260,
    
    type: "radialBar",
     sparkline: {
            enabled: true
          }
  },

  series: [<?php echo $vendorStatistics['documentApprovedPercentage'];?>],
  labels: [['Documents Approved']],
  colors: ["#48d6a8"],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
         size: "65%",
        background:'#fff'
       
      },
       startAngle: -100,
       endAngle: 100,
      track: {
        strokeWidth: 60,
        strokeLinecap:'Square',
        //background: "#3b4fb4",
        dropShadow: {
          enabled: true,
          top:0,
          left:0,
          blur:0,
          opacity:0.5
          
        }
      },
      dataLabels: {
        name: {
         offsetY: -20,
          color: "#48d6a8",
          fontSize: "12px",
          fontFamily: 'Poppins',
        },
        value: {
          color: "#48d6a8",
          fontSize: "12px",
          show: true,
          offsetY: -60,
          fontFamily: 'Poppins',
          formatter: function (val) {
            //return '';
            return val+"%"
          }
        }
      }
    }
  },
  
  stroke: {
    lineCap: "Square"
  },
  
};

var chart = new ApexCharts(document.querySelector("#approved_document"), options);

chart.render();

var options = {
  chart: {
   height: 260,
    type: "radialBar",
     sparkline: {
            enabled: true
          }
  },

  series: [<?php echo $vendorStatistics['fieldsCompletedPercentage'];?>],
  labels: ['Fields Populated'],
  colors: ["#3b4fb4"],
  plotOptions: {
    radialBar: {
    hollow: {
        
        size: "65%",
        background:'#fff',
        margin: 0,
       
      },
       
       startAngle: -100,
       endAngle: 100,
      track: {
      strokeWidth: 60,
        dropShadow: {
          enabled: true,
          top:0,
          left:0,
          blur:0,
          opacity:0.5
          
        }
      },
      dataLabels: {
        name: {
          offsetY: -20,
          color: "#3b4fb4",
          fontSize: "12px",
          fontFamily: 'Poppins',
          
        },
        value: {
          color: "#3b4fb4",
          fontSize: "12px",
          show: true,
          offsetY: -60,
          fontFamily: 'Poppins',
          formatter: function (val) {
            return val+"%";
          }
        }
      }
    }
  },
  
 /* stroke: {
    lineCap: "round"
  },*/
  
};

var chart = new ApexCharts(document.querySelector("#completed_field"), options);

chart.render();
</script>

<style type="text/css">
 .apexcharts-track {

 }
</style>