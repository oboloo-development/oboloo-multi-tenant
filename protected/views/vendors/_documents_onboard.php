<?php $disabled = FunctionManager::sandbox();
  if(!empty($documentList)){?><div class="x_title pull-left" style="border-bottom:none;width: 71%"><h4 class="heading">Uploaded Documents</h4></div>
<?php } 
      $modalId = 1;
      $editForm = '';
      foreach($documentList as $key=>$value) {
        if(!empty($vendor_id)){
          $sql = " SELECT * from vendor_documents_onboard where archive !=1 and vendor_id=$vendor_id and  document_type=".$value['document_type']." order by id DESC";
          $documentDetailReader = Yii::app()->db->createCommand($sql)->query()->readAll();
          $documentType = $value['document_type'];
        }else{
           $documentType = $key;
          $documentDetailReader = $value;
        }
        $tableID = "doctype_onboard_".$documentType;
        ?>
      <table id="<?php echo $tableID;?>" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
      <tr><th colspan="10"><?php echo FunctionManager::vendorDocumentOnboard($documentType);?></th></tr>
      <tr>
          <th style="width: 15%">Document Type</th>
          <th  style="width: 17%">Document Title</th>
          <th style="width: 15%">Document</th>
          <th style="width: 8%">Approved</th>
          <th style="width: 10%">Approved By</th>
          <th style="width: 10%">Approved Date</th>
          <th style="width: 11%">Expiry Date</th>
          <th style="width: 8%">Uploaded Date</th>
          <th style="width: 6%">Archive</th>
          <th style="width: 6%">Action</th>
      </tr>
      </thead>
      <tbody>
        <?php foreach($documentDetailReader as $detailKey=>$detailValue){
          $modalIDExp = "edit_document".$modalId;
          $modalId++;
          ?>
        <tr>
          <td><span class="btn btn-success doc-btn-round" style="<?php echo FunctionManager::vendorDocumentBgColorOnboard($documentType);?>"><?php echo FunctionManager::vendorDocumentOnboard($documentType);?></span></td>
          <td style='word-break:break-all'  class="notranslate"><?php echo $detailValue['document_title'];?></td>
          <td style='word-break:break-all'><?php echo $detailValue['document_file'];?></td>

          <td><input type="checkbox" class="approve_check_<?php echo $tableID;?>"  <?php if(strtolower($detailValue['status'])=='pending'){?> name="status_<?php echo !empty($detailValue['vendor_id'])?$value['document_type']:$key;?>[]" <?php } ?> <?php echo strtolower($detailValue['status'])=='approved'?'checked="checked" disabled="disabled" ':'';?> value="<?php echo $detailValue['id'];?>" /></td>

          <td><?php echo  $detailValue['approved_by_name'];?></td>
          <td><?php echo  isset($detailValue['approved_datetime']) && $detailValue['approved_datetime'] !='0000-00-00 00:00:00'?date(FunctionManager::dateFormat(), strtotime($detailValue['approved_datetime'])):'';?></td>
          <td><?php echo  $detailValue['expiry_date'] !='0000-00-00'?date(FunctionManager::dateFormat(), strtotime($detailValue['expiry_date'])):'No Expiry Date Entered, please update';?> &nbsp;&nbsp;&nbsp;
             <?php if (FunctionManager::checkEnvironment(true)) { ?>
              <button class="btn btn-danger btn-xs ownerpermission" onclick="$('#<?php echo $modalIDExp;?>').modal('show');return false">Edit</i></button>
              <?php }?>
          </td>  
          <td><?php echo date(FunctionManager::dateFormat(),strtotime($detailValue['date_created']));?></td>
          <td><input type="checkbox" class="archive_check_<?php echo $tableID;?>" <?php if(!empty($detailValue) && strtolower($detailValue['archive'])==0){?> name="archive_<?php echo !empty($detailValue) && !empty($detailValue['vendor_id'])?$value['archive']:$key;?>[]" <?php } ?> <?php echo !empty($detailValue) && strtolower($detailValue['archive'])== 1?'checked="checked" disabled="disabled"':''; ?>  value="<?php echo $detailValue['id'];?>" /></td>
          <td>
           
            <?php if(!empty($detailValue['vendor_id'])){?>
             <a href="<?php echo Yii::app()->createAbsoluteUrl('vendors/documentDownloadOnboard',array('id'=>$detailValue['id']));  ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
            <?php } /*if(/*strtolower($detailValue['status'])=='pending' &&  !empty($detailValue['id'])){?>
            <a style="cursor: pointer; padding: 5px;" id="delete_document_<?php echo $detailValue['id'];?>" onclick="deleteDocument(<?php echo $detailValue['id']; ?>)" class="ownerpermission">
               <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a><?php 
            }else if(empty($detailValue['vendor_id'])){?>
              <a style="cursor: pointer; padding: 5px;" id="delete_document_<?php echo $key.'_'.$detailKey;?>" onclick="deleteDocumentOther('<?php echo $key.'_'.$detailKey; ?>')"  class="ownerpermission">
               <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a>
            <?php } */?>
          </td>
        </tr>
      <?php 
          $formID = "edit_document_form_onboard".$detailValue['id'];
          $formEditFunc = "editSingleDocumentOnboard('".$formID."','".$modalIDExp."')";
          $expiryField = "<label class='control-label'>Expiry Date</label><input type='text' class='form-control expiry_date_1 notranslate' name='expiry_date_".$formID."' id='expiry_date_".$formID."' value='".date(FunctionManager::dateFormat(), strtotime($detailValue['expiry_date']))."' placeholder='Expiry Date'/>";

          $idField = "<input type='hidden' name='document_id_".$formID."' id='document_id_".$formID."' value='".$detailValue['id']."' />";
          $idField .= "<input type='hidden' name='vendor_id_".$formID."' id='vendor_id_".$formID."' value='".$detailValue['vendor_id']."' />";

          $editForm .=  '<div id="'.$modalIDExp.'" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Edit '.FunctionManager::vendorDocumentOnboard($documentType).'-'.$detailValue['document_title'].'</h4></div><div class="clearfix"></div>
          <form id="'.$formID.'" action="" method="post">
                  <div class="modal-body">
                    
                    '.$expiryField.$idField.'
                 </div>
                 </form><div class="clearfix"></div>
                  <div class="modal-footer">
            
                    <button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button>
                    <button  type="button" class="btn btn-primary btn sm" onclick="'.$formEditFunc.'">Edit</button></div></div></div></div>';
                    echo $editForm;

    } ?>
    <?php if (FunctionManager::checkEnvironment(true)) { ?>
     <tr style="width: 100%;">
        <td></td><td></td><td></td><td>
        <?php if(isset($value['pendiing_documents']) && $value['pendiing_documents']>0){?>
         <button id="approve_1_onboard" class="btn btn-success approve_btn btn-xs approve_check_btn_<?php echo $tableID;?>"  onclick="approveDocumentOnboard(<?php echo $value['document_type'];?>,event);">Approve</button>
        <?php }?>
        </td><td></td><td></td><td></td><td></td>
        <td>
         <button  class="btn btn-success  btn-xs archive_check archive_check_btn_<?php echo $tableID;?>" onclick="documentArchiveOnboard(<?php echo $value['archive'];?>,event);" <?php //echo $disabled; ?> style="display: none;">Archive</button></td><td>     
         </td>
      </tr>
      <?php } ?>
     </tbody>
     </table>

     <?php } ?>

   <?php  
    if(!empty($documenArchivetList)){?>

 <div class="x_title pull-left" style="border-bottom:none;width: 71%"><h4 class="heading"><h4 class="heading">Archived Documents</h4></div><div class="clearfix"></div>

    <?php foreach($documenArchivetList as $key=>$value) {
      if(!empty($vendor_id)){    
      $sql = " SELECT * from vendor_documents_onboard where archive !=0 and vendor_id=$vendor_id and  document_type=".$value['document_type']." order by id DESC";
      $documentDetailReader = Yii::app()->db->createCommand($sql)->query()->readAll();
      $documentType = $value['document_type'];
      }else{
           $documentType = $key;
          $documentDetailReader = $value;
        }
        $tableID = "doctype_onboard_".$documentType;
      ?>

      <table id="<?php echo $tableID;?>" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
      <tr><th colspan="10"><?php echo FunctionManager::vendorDocumentOnboard($documentType); ?></th></tr>
      <tr>
          <th style="min-width: 15%">Document Type</th>
          <th  style="width: 21%">Document Title</th>
          <th  style="width: 15%">Document</th>
          <th style="width: 11%">Expiry Date</th>
          <th style="width: 12%">Archived Date</th>
          <th style="width: 10%">Uploaded Date</th>
          <th style="width: 10%" class="notranslate">Unarchive</th>
          <th style="width: 10%">Action</th>
      </tr>
      </thead>
      <tbody>

      <?php foreach($documentDetailReader as $detailKey=>$detailValue){?>
       
         <tr>
           <td><span class="btn btn-success doc-btn-round" style="<?php echo FunctionManager::vendorDocumentBgColorOnboard($documentType);?>"><?php echo FunctionManager::vendorDocumentOnboard($documentType);?></span></td>
            <td style='word-break:break-all'  class="notranslate"><?php echo $detailValue['document_title'];?></td>
            <td style='word-break:break-all'><?php echo $detailValue['document_file'];?></td>
            <td><?php echo  $detailValue['expiry_date'] !='0000-00-00'?date(FunctionManager::dateFormat(), strtotime($detailValue['expiry_date'])):'No Expiry Date Entered, please update';?>
            </td>
            <td><?php echo date(FunctionManager::dateFormat(),strtotime($detailValue['date_created']));?></td>
           <td><?php echo date(FunctionManager::dateFormat(),strtotime($detailValue['archive_datetime']));?></td>
           <td><input type="checkbox" class="un-archive unarchive_check_<?php echo $tableID;?>"  name="archive_activate_<?php echo $value['archive']?>[]" value="<?php echo $detailValue['id'];?>" /></td>
            <td>
             
              <?php if(!empty($detailValue['vendor_id'])){?>
               <a href="<?php echo Yii::app()->createAbsoluteUrl('vendors/documentDownloadOnboard',array('id'=>$detailValue['id']));  ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
              <?php }  ?>
            </td>
        </tr>
      <?php } ?>
      <?php if (FunctionManager::checkEnvironment(true)) { ?>
      <tr style="width: 100%;">
     <td></td><td></td><td></td><td></td><td></td><td></td>
     <td>
       <button  class="btn btn-success  btn-xs unarchive_check unarchive_check_btn_<?php echo $tableID;?>" onclick="documentUnArchiveOnboard(<?php echo $value['archive'];?>,event);" <?php //echo $disabled; ?>>Unarchive</button>     
     </td><td></td>
   </tr>
 <?php } ?>
  </table>
<?php } } ?>

<br /><br /><br /><br /><br /><br />
<script type="text/javascript">
$(document).ready( function() {
    /* Start: Archive button show/hide */           
    $('[class*="archive_check_btn_doctype_onboard"]').hide();

    var checked_ctr=0;
    $('[class*="archive_check_doctype_onboard_"]').change(function(){
        currenctID = $(this).attr("class");
        docType=currenctID.split("_");
        className="archive_check_btn_doctype_onboard_"+docType[4];
        //alert(checked_ctr);
        checked_ctr = $('input:checkbox.'+currenctID+':checked').length;
         // alert(checked_ctr);
        if($(this).prop('checked')==true && checked_ctr > 0){
            $('[class*="'+className+'"]').show();
            
        }else {
             $('[class*="'+className+'"]').hide();
        }
    });
    
     /* Start: Un-Archive button show/hide */  
     $('[class*="unarchive_check_btn_doctype_onboard"]').hide();

     var checkedUnarchive_ctr = 0;
     $('[class*="unarchive_check_doctype_onboard_"]').change(function(){
     currenctUnarchiveID = $(this).attr("class");
     docTypeUn=currenctUnarchiveID.split("_");
     
     classNameUnarchive="unarchive_check_btn_doctype_onboard_"+docTypeUn[4];
     // alert(classNameUnarchive);
     checkedUnarchive_ctr =  $('input:checkbox.'+currenctUnarchiveID+':checked').length;
     //alert(checkedUnarchive_ctr);
     if($(this).is(":checked")){
            $('[class*="'+classNameUnarchive+'"]').show();
        }else {
             $('[class*="'+classNameUnarchive+'"]').hide();
        }
     });
     /* End: Un-Archive button show/hide */  

     /* Start: Approve button show/hide */         
    $('[class*="approve_check_btn_doctype_onboard"]').hide();

        var checked_ctr=0;
        $('[class*="approve_check_doctype_onboard_"]').change(function(){
            currenctApproveID = $(this).attr("class");
            docAprroveType=currenctApproveID.split("_");
            classApproveName="approve_check_btn_doctype_onboard_"+docAprroveType[4];
            // alert(classApproveName);
            checkedApprove_ctr = $('input:checkbox.'+currenctApproveID+':checked').length;
             // alert(checked_ctr);
            if($(this).prop('checked')==true && checkedApprove_ctr > 0){
                $('[class*="'+classApproveName+'"]').show();
            }else {
                 $('[class*="'+classApproveName+'"]').hide();
            }
        });
    
     /* End: Approve button show/hide */  
  });
</script>
