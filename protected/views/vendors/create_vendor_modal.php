<?php 
  $disabled = FunctionManager::sandbox();
  $domainUrl = Yii::app()->getBaseUrl(true);
?>
<div role="main">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 6px;"><span aria-hidden="true">&times;</span></button><h3 class="pull-left"><?php echo 'Add Supplier - Supplier Details';?></h3>
   <a href="https://oboloo.com/support-adding-a-supplier/" class="btn btn-warning tutorial pull-right" target="_blank" style="margin-left: 30px;">Tutorial</a> 
  <div class="pull-right" style="text-align:center;padding-right: 21px;padding-top: 3px;">
  <span class="step" style="display: none;"></span>

</div></div>
  <div class="tile_count" role="tabpanel"
    data-example-id="togglable-tabs">
    <form id="vendor_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('vendors/createByModal'); ?>">
     <div class="tab"> 
         <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                  <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate" name="vendor_name" id="vendor_name" onkeypress="return omit_special_char(event)" 
                      <?php if (isset($vendor['vendor_name']) && !empty($vendor['vendor_name'])) echo 'value="' . $vendor['vendor_name'] . '"'; else echo 'placeholder="Supplier Name"'; ?>  required="required"  >
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 date-input">
              <label class="control-label">Description/Notes<span class="error_color">*</span></label>
              <textarea class="form-control notranslate" name="vendor_desc" id="vendor_desc" rows="4" placeholder="Supplier description/notes" required="required"  /></textarea>
          </div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                  <label class="control-label">Contact Name <span style="color: #a94442;">*</span></label>
                  <input type="text" class="form-control contact_name notranslate" name="contact_name" id="contact_name"  aria-invalid="false" onkeypress="return omit_special_char(event)"  required>
              </div>
          </div>
           <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
            <label class="control-label">Company Assigned Supplier ID</label>
            <input type="text" class="form-control notranslate" name="external_id" id="external_id" placeholder="Company Assigned Supplier ID" >
          </div></div>
          

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                <label class="control-label">Email Address <span style="color: #a94442;">*</span> - Please check that this is correct for supplier correspondance  </label>
                <input type="text" class="form-control notranslate" name="emails"  id="emails" required="required" placeholder="Email Address"  >
            </div>
        </div>

         <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                <label class="control-label ">Confirm Email Address <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control emails_error notranslate" name="confm_emails" id="confm_emails" required="required" placeholder="Email Address" >
                <span class="emails_error" style="display: none;">Email Address and Confirm Email Address are not matching.</span>
            </div>

        </div>

       <!--  <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                <label class="control-label">Supplier Status  <span style="color: #a94442;">*</span></label>
                <select name="profile_status" id="profile_status" class="form-control" required="required">
                <option value="Active">Active</option>
                <option value="Inactive">Inactive</option>
                <option value="Under Review">Under Review</option>
              </select>
            </div>
        </div> -->

          <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <label class="control-label">Supplier Industry <span style="color: #a94442;">*</span></label>
        <select name="industry_id" id="industry_id" class="form-control notranslate" onchange="loadSubindustries(this);" required="required">
          <option value="">Select Supplier Industry</option>
          <?php foreach ($industries as $industry) { ?>
            <option value="<?php echo $industry['id']; ?>">
              <?php echo $industry['value']; ?>
            </option>
          <?php } ?>
        </select>
              </div>
          </div>

        <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
          <label class="control-label">Supplier Sub Industry <span id="requiredIndi" style="color: #a94442;">*</span></label>
          <select name="subindustry_id" id="subindustry_id_modal" class="form-control notranslate" required="required">
          <option value="0">Select Supplier Sub Industry</option>
          </select>
         </div>
       </div>
       <div class="form-group">
         <?php // if(strpos($domainUrl,"bondoporaja") || strpos($domainUrl,"sandbox") || strpos($domainUrl,"james") || strpos($domainUrl,"adam61") || strpos($domainUrl,"multi-local")){?>
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="checkbox">
                      <label><input type="checkbox" class="flat notranslate" name="approver_id" id="approver_user_id" value="1"></label>
                      <label style="padding: 0;margin-top: 10px;display: inline-block;"><div class="clearfix"></div><br />Does someone in your organisation need to approve this supplier?</label>
                  </div><br />
                  <div id="show_approver_user">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <label>Select User</label>
                      <?php  $sql = " SELECT user_id,full_name FROM `users` where admin_flag !=1";
                      $getUsers = Yii::app()->db->createCommand($sql)->queryAll(); 
                       ?>
                      <select  class="form-control select_user_contract notranslate" name="approver_id" id="approver_id" style="border-radius: 10px;border: 1px solid #4d90fe;">
                        <option value="">Select Approver</option>
                          <?php foreach ($getUsers as $all_user) { ?>
                          <option value="<?php echo $all_user['user_id']; ?>">
                            <?php echo $all_user['full_name']; ?>
                          </option>
                          <?php } ?>
                     </select>
                    </div>
                  </div>
              </div>
            <?php // } ?>
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="checkbox">
                      <label><input type="checkbox" class="flat notranslate" name="preferred_flag" id="preferred_flag" value="1"></label>
                      <label style="padding: 0;margin-top: 10px;display: inline-block;"><div class="clearfix"></div><br />Check this box to indicate that this is a preferred supplier</label>
                  </div>
                  <!--  <div class="checkbox">
                       <label  class="pull-left"><input type="checkbox" class="flat notranslate" name="active" id="active" value="1"></label>
                      <label style="padding: 5px;float: left;width: 95%;">Are you sure you want to activate this supplier?</label>

                  </div> -->
              </div>
          </div>
     </div>
     <div class="alert alert-success vendor_alert"><strong>Success!</strong> Supplier submitted successfully.</div>
       <div class="col-md-12 col-sm-12 col-xs-12 text-right"><br />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" style="background-color: #1abb9c !important; border-color: #1abb9c !important;" id="add_vendor_btn"  class="btn btn-primary submit-btn notranslate" value="Add Supplier"  <?php echo $disabled; ?>>
        <div class="clearfix"></div>
    </form>
  </div>
  <div class="clearfix"> <br /> </div>

</div>

<div class="modal fade" id="vendor_addtional_field" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="
    top: 40%;
    transform: translateY(-44%);
    right: 10%;
">
    <div class="modal-content pull-right" style="max-width: 538px; margin: auto;">
      <!-- <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body " style="padding: 29px;">
        <p style="font-size: 14px;">Would you like to complete the additional fields for this supplier?</p>
      </div>
      <div class="pull-right">
        <a href="#" id="vendor_detail_link" class="btn btn-success" style="border-radius: 4px;">Yes</a>
        <button type="button" class="btn btn-info" data-dismiss="modal" style="border-radius: 4px;">No</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$("#requiredIndi").hide();
$('#show_approver_user').hide();
 $(document).ready(function(){
   $('#approver_user_id').on('click', function () {
        if ($(this).prop('checked')) {
            $('#show_approver_user').fadeIn();
        } else {
            $('#show_approver_user').hide();
        }
    });
   $('[data-toggle="tooltip"]').tooltip();  
 });
 
function loadSubindustries(obj)
{
  var industry_id = obj.value;
  if (industry_id==""){
    $('#subindustry_id_modal').html('<option value="">Select Supplier Sub Industry</option>');
    $('#subindustry_id_modal').removeAttr("required");
    $("#requiredIndi").hide();
  }else
  {
      $.ajax({
          type: "POST", data: { industry_id: industry_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('vendors/getSubIndustries/'); ?>",
          success: function(options) {
            var options_html = '<option value="">Select Supplier Sub Industry</option>';

            if(options.suggestions.length){
              $('#subindustry_id_modal'). attr("required", "required");
              $("#requiredIndi").show();
            }else{
               $('#subindustry_id_modal').removeAttr("required");
               $("#requiredIndi").hide();
            }

            for (var i=0; i<options.suggestions.length; i++){
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            }
          $("#subindustry_id_modal").html(options_html);      
          },
        
      });
  }
}

$(".vendor_alert").hide();

$('#vendor_form').submit(function () { 
  var email = $("#emails").val();
    var confm_emails = $("#confm_emails").val();
    
    
    if(email != confm_emails) {
        // alert('Email Not Matching!');
      $(".emails_error").css('color', 'red').show();
       return false;
    }
  $(".vendor_alert").show();
  
  $.ajax({
      url: '<?php echo AppUrl::bicesUrl('vendors/createByModal'); ?>',
      type: 'POST', async: false, dataType: 'json',
      data: $("#vendor_form").serialize(),
      success: function(info) {
           
          $(".vendor_alert").html(info.alert).show().delay(50000).fadeOut();

          if(info.dupplicate == 0){
            $("#vendor_detail_link").attr('href','<?php echo $this->createUrl('vendors/edit');?>/'+info.vendor_id);
            $('#vendor_addtional_field').modal('show');
          }
      }
  });

  $('#vendor_form').trigger("reset");
  $('#vendor_table').DataTable().ajax.reload();
  return false;

});


$('#emails, #confm_emails').bind('input', function(){
    this.value=$(this).val().trim();
}); 
 
</script>

