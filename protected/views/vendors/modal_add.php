<div id="add_vendor_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Add New Supplier</h4>
        </div>
        <div class="modal-body">
		<form id="vendor_form" class="form-horizontal form-label-left input_mask">
          <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control has-feedback-left" name="new_vendor_name" id="new_vendor_name" placeholder="Supplier Name" />
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
              </div>

	          <div class="col-md-6 col-sm-6 col-xs-12">
	              <input type="text" class="form-control has-feedback-left" name="contact_name" id="contact_name" placeholder="Contact Name" />
	              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
	          </div>
          </div>

          <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-24">
				<select name="industry_id" id="industry_id" class="form-control" onchange="loadSubindustries();" required="required">
					<option value="">Select Supplier Industry</option>
					<?php foreach ($industries as $industry) { ?>
						<option value="<?php echo $industry['id']; ?>">
							<?php echo $industry['value']; ?>
						</option>
					<?php } ?>
				</select>
              </div>
          </div>

          <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-24">
				<select name="subindustry_id" id="subindustry_id" class="form-control">
					<option value="">Select Supplier Sub Industry</option>
				</select>
              </div>
          </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="payment_term_id" id="payment_term_id" class="form-control">
                    <option value="0">Select Payment Term</option>
                    <?php foreach ($payment_terms as $payment_term) { ?>
                        <option value="<?php echo $payment_term['id']; ?>">
                            <?php echo $payment_term['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="shipping_method_id" id="shipping_method_id" class="form-control">
                    <option value="0">Select Shipping Method</option>
                    <?php foreach ($shipping_methods as $shipping_method) { ?>
                        <option value="<?php echo $shipping_method['id']; ?>">
                            <?php echo $shipping_method['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>

		</form>
        </div>
		<div class="clearfix"> <br /> </div>

        <div class="col-md-1 col-sm-1 col-xs-2">
        </div>
        <div class="col-md-10 col-sm-10 col-xs-20">
			<div id="alert-warning" class="alert alert-warning alert-dismissable" style="display: none;">
			    <a style="margin-top: 2px;" onclick="$('#alert-warning').hide();" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
			    <strong class="errorMessage">ERROR! Supplier name is mandatory.</strong>
			</div>
	
			<div id="alert-error" class="alert alert-error alert-dismissable" style="display: none;">
			    <a style="margin-top: 2px;" onclick="$('#alert-error').hide();" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
			    <strong>ERROR! Could not save vendor to database.</strong>
			</div>
	
			<div id="alert-info" class="alert alert-info alert-dismissable" style="display: none;">
			    <a style="margin-top: 2px;" onclick="$('#alert-info').hide();" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
			    <strong>Please wait! Supplier is being added to database.</strong>
			</div>
		</div>
		<div class="clearfix"> <br /> </div>
		
        <div class="modal-footer">
          <button id="cancel_vendor_button" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          	  Cancel
          </button>
          <button id="add_vendor_button" type="button" class="btn-primary btn btn-default" onclick="saveVendorData();">
          	  Save Supplier
          </button>
        </div>
		<div class="clearfix"> <br /> </div>
		
      </div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#new_vendor_name').devbridgeAutocomplete({
	        serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>'
	    });	
	});
	
    function saveVendorData()
    {   
    	if ($.trim($('#new_vendor_name').val()) == "")
    	{ 
			$('#alert-info').hide();
			$('#alert-error').hide();
			$('#alert-warning').show();
			$('.errorMessage').html("ERROR! Supplier name is mandatory.");
			setTimeout(function() { $("#alert-warning").hide(); }, 4000);
    	}else if ($.trim($('#industry_id').val()) == "")
    	{  
			$('#alert-info').hide();
			$('#alert-error').hide();
			$('#alert-warning').show();
			$('.errorMessage').html("ERROR! Supplier Industry is mandatory.");
			setTimeout(function() { $("#alert-warning").hide(); }, 4000);
    	}else if ($.trim($('#subindustry_id').val()) == "")
    	{  
			$('#alert-info').hide();
			$('#alert-error').hide();
			$('#alert-warning').show();
			$('.errorMessage').html("ERROR! Supplier Sub Industry is mandatory.");
			setTimeout(function() { $("#alert-warning").hide(); }, 4000);
    	}
    	else
    	{
			$('#alert-error').hide();
			$('#alert-warning').hide();
			$('#alert-info').show();
			
	        $.ajax({
	            type: "POST",
	            url: "<?php echo AppUrl::bicesUrl('vendors/addNew/'); ?>",
	            data: { 
	            	vendor_name: $.trim($('#new_vendor_name').val()), 
	            	contact_name: $.trim($('#contact_name').val()), 
	            	industry_id: $('#industry_id').val(), 
	            	subindustry_id: $('#subindustry_id').val(), 
	            	payment_term_id: $('#payment_term_id').val(), 
	            	shipping_method_id: $('#shipping_method_id').val() 
	            },
	            success: function() {
	            	$('#alert-info').hide();
	            	$('#add_vendor_modal').modal('hide');
	            },
	            error: function() {
	            	$('#alert-info').hide();
	            	$('#alert-error').show();
					setTimeout(function() { $("#alert-error").hide(); }, 4000);
	            }
	        });
    	}
    }

	function loadSubindustries()
	{
		var industry_id = $('#industry_id').val();
		
		if (industry_id == 0)
			$('#subindustry_id').html('<option value="0">Select Supplier Sub Industry</option>');
		else
		{
		    $.ajax({
		        type: "POST", data: { industry_id: industry_id }, dataType: "json",
		        url: "<?php echo AppUrl::bicesUrl('vendors/getSubIndustries/'); ?>",
		        success: function(options) {

		        	if(options.suggestions.length){
		              $('#subindustry_id'). attr("required", "required");
		            }else{
		               $('#subindustry_id').removeAttr("required");
		            }
		        	var options_html = '<option value="">Select Supplier Sub Industry</option>';
		        	for (var i=0; i<options.suggestions.length; i++)
		        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
		        	$('#subindustry_id').html(options_html);
		        },
		        error: function() { $('#subindustry_id').html('<option value="">Select Supplier Sub Industry</option>'); }
		    });
		}
	}
</script>
