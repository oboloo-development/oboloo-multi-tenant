<?php
foreach ($allScorecards as $key => $scorecard) { ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= $scorecard['name'] ?></h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
                <div class="col-md-12 p-0">
                    <div class="row">
                        <div class="col-md-9 ">
                            <label style="font-size: 13px; padding-left: 5px;">Category/Area</label>
                        </div>
                        <div class="col-md-3 text-center">
                            <label style="font-size: 13px;">Score (%)</label>
                        </div>
                    </div>
                </div>
            </div>
         <?php  foreach ($scorecard['scoreCardLists'] as $key => $list) { ?>
            <div class="form-group mt-15">
                <div class="col-md-12 col-sm-12 col-xs-12 p-0">
                    <div style="float: left; width: 80%;">
                        <input type="text" class="form-control notranslate score_name" name="score_name[<?= $scorecard['card_id'] ?>][]" value="<?= $list['value']; ?>" readonly />
                    </div>
                    <div style="float: left;width: 20%;">
                        <input type="number" autocomplete="off" class="form-control text-center score score_1 notranslate" name="defualt_score[<?= $scorecard['card_id'] ?>][]" value="<?= $list['score']; ?>" min="0" step="0.01" readonly />
                    </div>
                </div>
            </div>

            <?php 
            $vendorScoringQuestion = new vendorScoringQuestion();
            $vendorScoringQuestion = $vendorScoringQuestion->getAll([ 'scorecard_id' => $list['scorecard_id'], 'vendor_scoring_id' => $list['id'],'order' => 'id asc']);       
            foreach($vendorScoringQuestion as $getQuestion) {?>
              <div style="width: 100%; display: flex" class="mt7  scoreCardQuestion_<?= $list['scorecard_id'] ?>_<?= $list['id'] ?>">
                <input type="text" autocomplete="off" class="form-control question " name="questions[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" placeholder="Type New Criteria Here" value="<?= $getQuestion['question'] ?>" style="flex:3" readonly />
                <div  style="flex: 1">
                 <select name="score_evaluation[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" class="form-control scores-validation-question" required>
                    <option value="0">Score Supplier</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                 </select>
                <span class="error-message" style="color: red;"></span>
                </div>
                <textarea name="question_notes[<?= $scorecard['card_id'] ?>][<?= $key ?>][]" class="form-control score score_1 notranslate" style="flex:2" placeholder="Type Comments"></textarea>
              </div> 
            <?php } 
        } ?>
        </div>
    </div>
<?php } ?>

