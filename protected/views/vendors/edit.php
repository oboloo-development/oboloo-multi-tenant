<style type="text/css">
  .h4,
  h4 {
    font-size: 14px;
  }

  .modal {
    overflow-y: auto !important;
  }

  .modal-open {
    overflow: auto !important;
    overflow-x: hidden !important;
    padding-right: 0 !important;
  }

  ul.bar_tabs>li a {
    padding: 10px 14px !important;
  }

  .venor_performance>thead>tr>th {
    vertical-align: top;
  }

  ul.bar_tabs {
    padding-left: 5px !important;
  }

  ul.bar_tabs>li {
    margin-left: 6px !important;
  }
</style>
<?php $domainUrl = Yii::app()->getBaseUrl(true);
$disabledArchive = CommonFunction::disabledArchive('vendors', $vendor['vendor_id']);
$location_ids = array_column($vendor_location, 'location_id');
$department_ids = array_column($vendor_department, 'department_id');
?>
<div class="right_col" role="main">
  <div class="row-fluid tile_count">
    <div class="span6 pull-left">
      <?php
      if (isset($vendor_id) && $vendor_id) {
      } else //echo 'Add Supplier';
      ?>
      <h4 class="notranslate">
        <?php if ($vendor['net_score'] > 0) {
        $netScore = "<span class='btn btn-primary' style='background: #DDF1D5 !important;border-color: #DDF1D5  !important;border-radius: 10px;color:#2d9ca2 !important'>" . number_format($vendor['net_score']) . "</span>";
      } else if ($vendor['net_score'] == 0) {
        $netScore = "<span class='btn btn-primary' style='background: #FDEC90 !important;border-color: #FDEC90  !important;border-radius: 10px;color:#2d9ca2 !important'>0</span>";
      } else {
        $netScore = "<span class='btn btn-primary' style='background: #F0DFDF !important;border-color: #F0DFDF  !important;border-radius: 10px;color:#2d9ca2 !important'>" . number_format($vendor['net_score']) . "</span>";
      }

      $avgScore = '';
      if (10 * $avg_score['avg_score'] <= 60 && 10 * $avg_score['avg_score'] > 0) {
        $avgScore = "<span class='btn btn-primary' style='background: #F0DFDF !important;border-color: #F0DFDF  !important;border-radius: 10px;color:#2d9ca2 !important'>" . number_format(10 * $avg_score['avg_score']) . "%</span>";
      } else if (10 * $avg_score['avg_score'] > 60 and 10 * $avg_score['avg_score'] <= 80) {
        $avgScore = "<span class='btn btn-primary' style='background: #FDEC90 !important;border-color: #FDEC90  !important;border-radius: 10px;color:#2d9ca2 !important'>" . number_format(10 * $avg_score['avg_score']) . "%</span>";
      } else if (10 * $avg_score['avg_score'] <= 0) {
        $avgScore = "<span class='btn btn-primary' style='background: #DCDCDC !important;border-color: #DCDCDC  !important;border-radius: 10px;color:#2d9ca2 !important'>" . number_format(10 * $avg_score['avg_score']) . "%</span>";
      } else {
        $avgScore = "<span class='btn btn-primary' style='background: #DDF1D5 !important;border-color: #DDF1D5  !important;border-radius: 10px;color:#2d9ca2 !important'>" . number_format(10 * $avg_score['avg_score']) . "%</span>";
      }
        ?>

        <?php
        if (!empty($vendor['vendor_name'])) { ?>
          <br /><strong class="notranslate">Supplier Name: </strong> <span class="title-text notranslate"><?php echo $vendor['vendor_name']; ?></span>
        <?php }
        if (!empty($vendor['contact_name'])) { ?>
          <br /><br /><strong class="notranslate">Contact Name: </strong> <span class="title-text notranslate"> <?php echo $vendor['contact_name']; ?></span>
        <?php }
        if (!empty($vendor['emails'])) { ?>
          <br /><br /><strong class="notranslate">Email: </strong> <span class="title-text notranslate"> <?php echo $vendor['emails']; ?></span>
        <?php } ?>
        <?php if (!empty($vendor['phone_1'])) { ?>
          <br /> <br /><strong class="notranslate">Contact Phone Number: </strong><span class="title-text notranslate"> <?php echo $vendor['phone_1']; ?></span>
        <?php }
        if (!empty($vendor['phone_2'])) { ?>
          <br /><br />
          <strong class="notranslate">Contact Mobile Number: </strong> <span class="title-text"><?php echo $vendor['phone_2']; ?></span>
        <?php }
        if (!empty($vendor['website'])) { ?>
          <br /><br /><strong class="notranslate">Company Website: </strong> <span class="title-text notranslate"><a href="<?php echo $vendor['website']; ?>" target="_blank"><?php echo $vendor['website']; ?></a></span>
        <?php } ?>
        <br /><br /><strong class="notranslate">Average Performance Score: <?php echo $avgScore; ?></strong>
        <?php if (!empty($vendor['approver_status'])) {
          $supStatus = $vendor['approver_status'];
          if ($supStatus == "Approved") {
            $color = "#DDF1D5";
          } else if ($supStatus == "Sent For Approval") {
            $color = "#d5dbf1";
          } else if ($supStatus == "Rejected") {
            $color = "#F0DFDF";
          } else {
            $color = "#DCDCDC";
          }
        ?>
          <br /><br /><strong class="notranslate">Supplier Status: </strong> <span class="title-text notranslate"> <span class="btn btn-primary" style="background: <?php echo $color; ?> !important;border-color: <?php echo $color; ?>  !important;border-radius: 10px;color:#2d9ca2 !important"><?php echo $vendor['approver_status']; ?></span></span>
        <?php } ?>
      </h4>

    </div>

    <div class="span6 pull-right">
      <a href="<?php echo AppUrl::bicesUrl('vendors/list'); ?>">
        <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff; border-color:#F79820">
          <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Return to Active Supplier List
        </button>
      </a>
      <br />
      <?php if (strpos($domainUrl, "bondoporaja") || strpos($domainUrl, "sandbox") || strpos($domainUrl, "james") || strpos($domainUrl, "adam61") || strpos($domainUrl, "multi-local")) { ?>
        <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#mysupplierStatusModal" style="background-color: #F79820;color: #fff; border-color:#F79820">Change Supplier Status</button>
      <?php } ?>
    </div>

    <div class="clearfix"> </div>
    <?php $successMesg = Yii::app()->user->getFlash('success');
    if (Yii::app()->user->hasFlash('error')) : ?>
      <div class="alert alert-danger" role="alert">
        <?php echo Yii::app()->user->getFlash('error'); ?>
      </div>
    <?php elseif (!empty($successMesg)) : ?>
      <div class="alert alert-success" role="alert">
        <?php echo $successMesg; ?>
      </div>
    <?php endif; ?>
  </div>

  <?php $this->renderPartial("_detail_statistic", array('vendorStatistics' => $vendorStatistics)); ?>

  <div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
      <li role="presentation" class="<?php echo empty($_GET['tab']) || !in_array($_GET['tab'], array('document', 'scorecard')) ? 'active' : ''; ?>">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="vendor-tab" role="tab" data-toggle="tab" aria-expanded="true">
          Supplier Details
        </a>
      </li>
      <li role="presentation" class="">
        <a href="#tab_content2" role="tab" id="contact-info-tab" data-toggle="tab" aria-expanded="false">
          <span style="text-transform: capitalize;">Contact Info</span>
        </a>
      </li>

      <?php if (true || !empty($vendor_id)) { ?>
        <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] == 'document' ? 'active' : '' ?>">
          <a href="#tab_content8" role="tab" id="document-tab" data-toggle="tab" aria-expanded="false">
            General Documents
          </a>
        </li>
        <?php if (strpos($domainUrl, "sandbox") || strpos($domainUrl, "james") || strpos($domainUrl, "adam61") || strpos($domainUrl, "multi-local")) { ?>
      <?php }
      } ?>
      <li role="presentation" class="">
        <a href="#tab_content6" role="tab" id="contract-tab" data-toggle="tab" aria-expanded="false">Contracts</a>
      </li>
      <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab']  == 'vendor-performance' ? 'active' : '' ?>">
        <a href="#tab_content3" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] == 'vendor-performance' ? 'active' : '' ?>" role="tab" id="comments-tab" data-toggle="tab" aria-expanded="false">Performance</a>
      </li>
      <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] == 'vendor-risk' ? 'active' : '' ?>">
        <a href="#tab_risk" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] == 'vendor-risk' ? 'active' : '' ?>" role="tab" id="risk-tab" data-toggle="tab" aria-expanded="false">
          Risk
        </a>
      </li>

      <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] == 'vendor-sustainability' ? 'active' : '' ?>">
        <a href="#tab_sustainability" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] == 'vendor-sustainability' ? 'active' : '' ?>" role="tab" id="sustainability-tab" data-toggle="tab" aria-expanded="false">
          Sustainability
        </a>
      </li>

      <li role="presentation" class="">
        <a href="#tab_content7" role="tab" id="quote-tab" data-toggle="tab" aria-expanded="false">
          eSourcing
        </a>
      </li>
      <li role="presentation" class="">
        <a href="#tab_content12" role="tab" id="quote-tab" data-toggle="tab" aria-expanded="false">
          Savings
        </a>
      </li>
      <!-- <li role="presentation" class="<?php // echo !empty($_GET['tab']) && $_GET['tab']=='supplier-comparision'?'active':''
                                          ?>">
        <a href="#tab_content10" role="tab" id="supplier-comparision" data-toggle="tab" aria-expanded="false">
            Supplier Comparisons
        </a>
      </li> -->

      <li role="presentation" class="">
        <a href="#tab_content9" role="tab" id="owner-tab" data-toggle="tab" aria-expanded="false">
          Notifications
        </a>
      </li>
      
      <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] == 'scorecard' ? 'active' : '' ?>">
        <a href="#tab_content10" role="tab" id="scorecard-tab" data-toggle="tab" aria-expanded="false">
          Scorecard
        </a>
      </li>

      <!-- <li role="presentation" class="">
        <a href="#tab_content11" role="tab" id="supplier_contacts-tab" data-toggle="tab" aria-expanded="false">
            Contacts
        </a>
    </li> -->
      <!-- <li role="presentation" class="">
        <a href="#tab_content4" role="tab" id="payment-tab" data-toggle="tab" aria-expanded="false">
             Payments
        </a>
    </li> -->
    </ul>
    <form id="vendor_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('vendors/edit'); ?>">

      <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($vendor_id)) echo $vendor_id; ?>" />
      <input type="hidden" name="form_submitted" id="form_submitted" value="1" />

      <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade <?php echo empty($_GET['tab']) ? 'active' : '' ?> in" id="tab_content1" aria-labelledby="vendor-tab">
          <h4 class=" col-md-12 heading page-title">Supplier Details</h4>
          <div class="form-group">
            <?php if (isset($vendor_id) && $vendor_id && $vendor['active'] == 0) { ?>
              <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate" name="vendor_name" onkeypress="return omit_special_char(event)" id="vendor_name" <?php if (isset($vendor['vendor_name']) && !empty($vendor['vendor_name'])) echo 'value="' . $vendor['vendor_name'] . '"';
                                                                                                                                                      else echo 'placeholder="Supplier Name"'; ?> required="required">
              </div>
              <!--  <div class="col-md-2 col-sm-2 col-xs-4">
                      <label class="control-label">Status</label>
		          	  <select name="active" id="active" class="form-control">
		          	  	   <option value="0">Inactive</option>
		          	  	   <option value="1">Active</option>
		          	  </select>
		          </div> -->
            <?php } else { ?>
              <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate" name="vendor_name" id="vendor_name" <?php if (isset($vendor['vendor_name']) && !empty($vendor['vendor_name'])) echo 'value="' . $vendor['vendor_name'] . '"';
                                                                                                        else echo 'placeholder="Supplier Name"'; ?> required="required">
              </div>
            <?php } ?>
            <div class="clearfix"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Description/Notes <span class="error_color">*</span></label>
                <textarea class="form-control notranslate" name="vendor_desc" id="vendor_desc" rows="4" <?php if (!isset($vendor['vendor_desc']) || empty($vendor['vendor_desc'])) echo 'placeholder="Supplier Description/Notes"'; ?>><?php if (isset($vendor['vendor_desc']) && !empty($vendor['vendor_desc'])) echo $vendor['vendor_desc']; ?></textarea>
              </div>
            </div>

            <!-- <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Contact Name</label> -->
            <input type="hidden" class="form-control contact_name notranslate" name="contact_name" id="contact_name" <?php if (isset($vendor['contact_name']) && !empty($vendor['contact_name'])) echo 'value="' .
                                                                                                                        $vendor['contact_name'] . '"';
                                                                                                                      else echo 'placeholder="Contact Name"'; ?>>
            <!--</div>
            </div> -->

            <!-- <div class="form-group">
             <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Email Address  <span style="color: #a94442;">*</span></label> -->
            <input type="hidden" class="form-control notranslate" name="emails" id="emails" required="required" <?php if (isset($vendor['emails']) && !empty($vendor['emails'])) {
                                                                                                                  echo 'value="' . $vendor['emails'] . '"';
                                                                                                                  echo 'readonly';
                                                                                                                } else echo 'placeholder="Email Address"'; ?>>
            <!--</div>
            </div> -->

            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-6 date-input">
                <label class="control-label">Phone Number</label>
                <input type="number" class="form-control phone_1 notranslate" name="phone_1" id="phone_1" <?php if (isset($vendor['phone_1']) && !empty($vendor['phone_1'])) echo 'value="' . $vendor['phone_1'] . '"';
                                                                                                          else echo 'placeholder="Phone Number"'; ?>>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Company Assigned Supplier ID</label>
                <input type="text" class="form-control notranslate" name="external_id" id="external_id" <?php if (isset($vendor['external_id']) && !empty($vendor['external_id'])) echo 'value="' . $vendor['external_id'] . '"';
                                                                                                        else echo 'placeholder="Company Assigned Supplier ID"'; ?>>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-12">
              <label class="control-label">Linked Locations <span style="color: #a94442;">*</span></label>
              <select required name="location_id[]" id="location_id" class="form-control notranslate" onchange="loadDepartmentsForSingleLocation(0);" multiple>
                <option value="">Select Locations</option>
                <?php
                foreach ($locations as $location) { ?>
                  <option value="<?php echo $location['location_id']; ?>" <?php if (!empty($location_ids) && in_array($location['location_id'], $location_ids)) {
                                                                            echo ' selected="SELECTED" ';
                                                                          } ?>>
                    <?php echo $location['location_name']; ?>
                  </option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <label class="control-label">Linked Departments <span style="color: #a94442;">*</span></label>
              <select name="department_id[]" id="department_id" class="form-control notranslateselect_department_multiple" multiple="multiple" required>
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Supplier Industry <span style="color: #a94442;">*</span></label>
              <select name="industry_id" id="industry_id" class="form-control notranslate" onchange="loadSubindustries(0);" required="required">
                <option value="">Select Supplier Industry</option>
                <?php foreach ($industries as $industry) { ?>
                  <option value="<?php echo $industry['id']; ?>" <?php if (isset($vendor['industry_id']) && $vendor['industry_id'] == $industry['id']) echo ' selected="SELECTED" '; ?>>
                    <?php echo $industry['value']; ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Supplier Sub Industry <span id="requiredIndi" style="color: #a94442;">*</span></label>
              <select name="subindustry_id" id="subindustry_id" class="form-control notranslate" required="required">
                <option value="0">Select Supplier Sub Industry</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Company Number</label>
              <input type="number" class="form-control notranslate" name="number" id="number" <?php if (isset($vendor['number']) && !empty($vendor['number'])) echo 'value="' . $vendor['number'] . '"';
                                                                                              else echo 'placeholder="Company Number"'; ?>>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Tax Number</label>
              <input type="number" class="form-control notranslate" name="tax_number" id="tax_number" <?php if (isset($vendor['tax_number']) && !empty($vendor['tax_number'])) echo 'value="' . $vendor['tax_number'] . '"';
                                                                                                      else echo 'placeholder="Tax Number"'; ?>>
            </div>
          </div>


          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Payment Term</label>
              <select name="payment_term_id" id="payment_term_id" class="form-control notranslate">
                <option value="0">Select Payment Term</option>
                <?php foreach ($payment_terms as $payment_term) { ?>
                  <option value="<?php echo $payment_term['id']; ?>" <?php if (isset($vendor['payment_term_id']) && $vendor['payment_term_id'] == $payment_term['id']) echo ' selected="SELECTED"'; ?>>
                    <?php echo $payment_term['value']; ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Shipping Term</label>
              <select name="shipping_term_id" id="shipping_term_id" class="form-control notranslate">
                <option value="0">Select Shipping Term</option>
                <?php foreach ($shipping_terms as $shipping_term) { ?>
                  <option value="<?php echo $shipping_term['id']; ?>" <?php if (isset($vendor['shipping_term_id']) && $vendor['shipping_term_id'] == $shipping_term['id']) echo ' selected="SELECTED"'; ?>>
                    <?php echo $shipping_term['value']; ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Shipping Method</label>
              <select name="shipping_method_id" id="shipping_method_id" class="form-control notranslate">
                <option value="0">Select Shipping Method</option>
                <?php foreach ($shipping_methods as $shipping_method) { ?>
                  <option value="<?php echo $shipping_method['id']; ?>" <?php if (isset($vendor['shipping_method_id']) && $vendor['shipping_method_id'] == $shipping_method['id']) echo ' selected="SELECTED"'; ?>>
                    <?php echo $shipping_method['value']; ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="flat notranslate" name="preferred_flag" id="preferred_flag" value="1" <?php if (isset($vendor['preferred_flag']) && $vendor['preferred_flag']) echo 'checked="checked"'; ?>>
                  <span style="font-weight: 600;"> Check this box to indicate that this is a preferred supplier</span>
                </label>
              </div>
            </div>
          </div>

          <?php
          $this->renderPartial('_submit', array('vendor_id' => $vendor_id, 'vendor' => $vendor, 'vendorLogs' => $vendorLogs)); ?>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="contact-info-tab">
          <br>
          <h4 class="col-md-12 heading">Contact Info</h4>
          <h4 class="col-md-12 subheading"><br>Find the contact information for this supplier here. You can also add more contacts for this supplier at the bottom of this page <br /> <br /></h4>
          <!-- <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Contact Name </label>
                <input type="text" class="form-control contact_name notranslate" name="contact_name" onkeypress="return omit_special_char(event)" id="contact_name" pattern="\S(.*\S)?"
                <?php // if (isset($vendor['contact_name']) && !empty($vendor['contact_name'])) echo 'value="' . $vendor['contact_name'] . '"'; else echo 'placeholder="Contact Name"'; 
                ?> >
            </div>
        </div> -->
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Company Website</label>
              <input type="url" class="form-control notranslate" onChange="checkUrl()" name="website" id="website" <?php if (isset($vendor['website']) && !empty($vendor['website'])) echo 'value="' . $vendor['website'] . '"';
                                                                                                                    else echo 'placeholder="Company Website"'; ?>>
            </div>
          </div>
          <!-- <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
                <label class="control-label">Email Address  <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate" name="emails" id="emails" required="required" <?php if (isset($vendor['emails']) && !empty($vendor['emails'])) {
                                                                                                                    echo 'value="' . $vendor['emails'] . '"';
                                                                                                                    echo 'readonly';
                                                                                                                  } else echo 'placeholder="Email Address"'; ?> >
            </div>
        </div> -->

          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Address Line 1</label>
              <input type="text" class="form-control notranslate" name="address_1" id="address_1" <?php if (isset($vendor['address_1']) && !empty($vendor['address_1'])) echo 'value="' . $vendor['address_1'] . '"';
                                                                                                  else echo 'placeholder="Address Line 1"'; ?>>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Address Line 2</label>
              <input type="text" class="form-control notranslate" name="address_2" id="address_2" <?php if (isset($vendor['address_2']) && !empty($vendor['address_2'])) echo 'value="' . $vendor['address_2'] . '"';
                                                                                                  else echo 'placeholder="Address Line 2"'; ?>>
            </div>
          </div>


          <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">City</label>
              <input type="text" class="form-control notranslate" name="city" id="city" <?php if (isset($vendor['city']) && !empty($vendor['city'])) echo 'value="' . $vendor['city'] . '"';
                                                                                        else echo 'placeholder="City"'; ?>>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">County/State</label>
              <input type="text" class="form-control notranslate" name="state" id="state" <?php if (isset($vendor['state']) && !empty($vendor['state'])) echo 'value="' . $vendor['state'] . '"';
                                                                                          else echo 'placeholder="County/State"'; ?>>
            </div>
          </div>
          <?php $countries = FunctionManager::countryList(); ?>
          <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Country</label>
              <select class="form-control select_vendor_multiple notranslate" name="country" id="country">
                <option value="">Select</option>
                <?php foreach ($countries as $key => $value) { ?>
                  <option value="<?php echo $value; ?>" <?php if (!empty($vendor['country']) && $vendor['country'] == $value) {
                                                          echo "selected='selected'";
                                                        } ?>><?php echo $value; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Zip/Postal Code</label>
              <input type="text" class="form-control notranslate" name="zip" id="zip" <?php if (isset($vendor['zip']) && !empty($vendor['zip'])) echo 'value="' . $vendor['zip'] . '"';
                                                                                      else echo 'placeholder="Zip/Postal Code"'; ?>>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">Phone Number</label>
              <input type="number" class="form-control phone_1 notranslate" name="phone_1" id="phone_1" <?php if (isset($vendor['phone_1']) && !empty($vendor['phone_1'])) echo 'value="' . $vendor['phone_1'] . '"';
                                                                                                        else echo 'placeholder="Phone Number"'; ?>>
            </div>
          </div>

          <div class="form-group" style="display: none;">
            <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Fax Number</label>
              <input type="text" class="form-control notranslate" name="fax" id="fax" <?php if (isset($vendor['fax']) && !empty($vendor['fax'])) echo 'value="' . $vendor['fax'] . '"';
                                                                                      else echo 'placeholder="Fax Number"'; ?>>
            </div>
          </div>
          <br /><br />
          <div class="col-md-12"><?php $this->renderPartial('_supplier_contracts', array('quickComparison' => $quickComparison, 'vendor_id' => $vendor_id)); ?></div>
          <div class="clearfix"></div><br /><br />
          <?php $this->renderPartial('_submit', array('vendor_id' => $vendor_id, 'vendor' => $vendor)); ?>
        </div>
        <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab'] == 'vendor-performance' ? 'in active' : '' ?>" id="tab_content3" aria-labelledby="comments-tab" style="padding: 15px;">
          <?php $this->renderPartial('_supplier_performance_list', array('vendor_id' => $vendor_id, 'vendorPerformance' => $vendorPerformance)); ?>
        </div>

        <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab'] == 'vendor-risk' ? 'in active' : '' ?>" id="tab_risk" aria-labelledby="risk-tab" style="padding: 15px;">
          <?php $this->renderPartial('_supplier_risk_list', array('vendor_id' => $vendor_id, 'vendorRisk' => $vendorRisk)); ?>
        </div>

        <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab'] == 'vendor-sustainability' ? 'in active' : '' ?>" id="tab_sustainability" aria-labelledby="sustainability-tab" style="padding: 15px;">
          <?php $this->renderPartial('_supplier_sustainability_list', array('vendor_archive' => $vendor['vendor_archive'], 'vendor_id' => $vendor_id, 'vendorSustainability' => $vendorSustainability)); ?>
        </div>


        <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="contract-tab" style="padding: 15px;">
          <h4 class="heading">Contracts <br /><br /></h4>

          <table id="contract_table" class="table table-striped table-bordered" style="width: 100%;">
            <thead>
              <tr>
                <th>Action</th>
                <th>Supplier</th>
                <th>Reference</th>
                <th>Title</th>
                <th>Start Date</th>
                <th>End Date</th>
                <!--  <th>Amount in Tool Currency</th>
                  <th>Amount in Order Currency</th> -->
              </tr>
            </thead>

            <tbody>

              <?php if (!empty($contracts) && count($contracts) > 0) {
                foreach ($contracts as $contract) {
                  $class = '';
                  $endDate = date("Y-m-d", strtotime($contract['contract_end_date']));

                  $afterMonth = date("Y-m-d", strtotime("+1 month"));
                  $after3Month = date("Y-m-d", strtotime("+3 months"));

                  if ($endDate < date("Y-m-d")) {
                    $color = '#c1bfbf';
                    $expiresTip = 'Passed Contract End Date';
                    $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="top" title="' . $expiresTip . '" aria-hidden="true" style="color:' . $color . '"></i></span>';
                  } else {
                    if ($endDate >=  date("Y-m-d") && $endDate <= $afterMonth) {
                      $color = '#f7778c';
                      $expiresTip = 'End Date Within 30 Days';
                      $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="top" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                    } else if ($endDate >=  date("Y-m-d") && $endDate <= $after3Month) {
                      $color = '#fdec90';
                      $expiresTip = 'End Date Within 3 Months';
                      $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="top" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                    } else {
                      $color = '#00bc9d';
                      $expiresTip = 'End Date Over 3 Months';
                      $icon = '<span style="padding-left:5px"><i class="fa fa-circle" data-toggle="tooltip" data-placement="top" title="' . $expiresTip . '"  aria-hidden="true" style="color:' . $color . '"></i></span>';
                    }
                  }
              ?>
                  <tr <?php //if (!empty($class)) echo $class; 
                      ?>>
                    <td>
                      <a href="<?php echo AppUrl::bicesUrl('contracts/edit/' . $contract['contract_id']); ?>">
                        <button type="button" class="btn btn-sm btn-success view-btn">View</button>
                      </a>
                    </td>
                    <td class="notranslate"><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $contract['vendor_id']); ?>"><?php echo $contract['vendor']; ?></a></td>
                    <td><?php echo $contract['contract_reference']; ?></td>
                    <td><?php echo $contract['contract_title']; ?></td>
                    <td><?php echo date(FunctionManager::dateFormat(), strtotime($contract['contract_start_date'])); ?></td>
                    <td><?php echo date(FunctionManager::dateFormat(), strtotime($contract['contract_end_date'])); ?> <?php echo $icon; ?></td>
                    <!--   <td style="text-align: right;">
                          <nobr>
                              <?php
                              switch ($contract['currency']) {
                                case 'GBP':
                                  $currency = '&#163;';
                                  break;
                                case 'USD':
                                  $currency = '$';
                                  break;
                                case 'CAD':
                                  $currency = '$';
                                  break;
                                case 'CHF':
                                  $currency = '&#8355;';
                                  break;
                                case 'EUR':
                                  $currency = '&#8364;';
                                  break;
                                case 'JPY':
                                  $currency = '&#165;';
                                  break;
                                case 'INR':
                                  $currency = '&#x20B9;';
                                  break;
                                default:
                                  $currency = '&#163;';
                                  break;
                              }
                              echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($contract['contract_value_GB'], 2); ?>
                          </nobr>
                      </td> -->
                    <!--  <td style="text-align: right;">
                          <nobr>
                              <?php
                              echo $currency . ' ' . number_format($contract['contract_value'], 2);
                              ?>
                          </nobr>
                      </td> -->
                  </tr>

              <?php }
              } ?>
            </tbody>

          </table>


        </div>

        <div role="tabpanel" class="tab-pane fade" id="tab_content7" aria-labelledby="quote-tab" style="padding: 15px;">

          <h4 class="heading">eSourcing Activities</h4><br />
          <?php if (!empty($quotes) && count($quotes) > 0) { ?>
            <table id="quote_table" class="table table-striped table-bordered" style="width: 100%;">
              <thead>
                <tr>
                  <th>Action</th>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Opening Date</th>
                  <th>Closing Date</th>
                  <th>User</th>
                  <th>Awarded Supplier</th>
                  <th>Status</th>
                </tr>
              </thead>

              <tbody>

                <?php
                foreach ($quotes as $quote) {
                ?>
                  <tr>
                    <td>
                      <a href="<?php echo AppUrl::bicesUrl('quotes/edit/' . $quote['quote_id']); ?>">
                        <button type="button" class="btn btn-sm btn-primary view-btn">View</button>
                      </a>
                    </td>
                    <td><?php echo $quote['quote_id']; ?></td>
                    <td><?php echo $quote['quote_name']; ?></td>
                    <td><?php echo date(FunctionManager::dateFormat() . " H:iA", strtotime($quote['opening_date'])); ?></td>
                    <td><?php echo date(FunctionManager::dateFormat() . " H:iA", strtotime($quote['closing_date'])); ?></td>
                    <td> <a href="<?php echo AppUrl::bicesUrl('users/edit/' . $quote['user_id']); ?>"><?php echo $quote['full_name']; ?></a></td>
                    <td class="notranslate"><?php echo !empty($quote['awarded_vendor_name']) ? $quote['awarded_vendor_name'] : ''; ?></td>
                    <td>
                      <?php
                      if (strtotime($quote['closing_date']) <= time()) echo 'Complete<br />';

                      if ($quote['submit_count'] <= 0) {
                        if (strtotime($quote['closing_date']) > time()) echo 'Awaiting Quotes';
                      } else {
                        if ($quote['invite_count'] == 1)
                          echo $quote['submit_count'] . ' of ' . $quote['invite_count'] . ' Quote received';
                        else
                          echo $quote['submit_count'] . ' of ' . $quote['invite_count'] . ' Quotes received';
                      }
                      ?>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          <?php } ?>
          <div class="clearfix"><br /></div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content12" aria-labelledby="quote-tab" style="padding: 15px;">
          <h4 class="heading">Savings</h4><br />
          <?php $this->renderPartial('_savings_tab', array('savings' => $savings)); ?>
          <div class="clearfix"><br /></div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content9" aria-labelledby="supplier-tab" style="padding: 15px;">
          <?php $this->renderPartial('_owners', array('owners' => $owners, 'vendor_id' => $vendor_id)); ?>
        </div>

        <!-- <div role="tabpanel" class="tab-pane fade" id="tab_content10" aria-labelledby="supplier-tab" style="padding: 15px;">
        <?php // $this->renderPartial('_supplier_comparision',array('quickComparison'=>$quickComparison,'vendor_id'=>$vendor_id,));
        ?>
      </div> -->
    </form>

    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab'] == 'document' ? 'in active' : '' ?>" id="tab_content8" aria-labelledby="quote-tab" style="padding: 13px;">
      <?php
      $this->renderPartial('documents', array(
        'disabledArchive' => $disabledArchive, 'vendor_id' => $vendor_id, 'documentList' => $documentList,
        'documenArchivetList' => $documenArchivetList, 'owners' => $owners, 'vendor_archive' => $vendor['vendor_archive'],
        'filter_document_type' => $filter_document_type,
        'flter_document_status' => $flter_document_status,
        'fltr_doc_contract_supplier_status' => $fltr_doc_contract_supplier_status
      ));
      ?>
      <div class="clearfix"><br /></div>
    </div>

    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab'] == 'document_onboard' ? 'in active' : '' ?>" id="tab_content_onboard" aria-labelledby="quote-tab-onboard" style="padding: 15px;">
      <?php $this->renderPartial('documents_oboard', array('vendor_id' => $vendor_id, 'documentListOnboard' => $documentListOnboard, 'documenArchivetListOnboard' => $documenArchivetListOnboard, 'owners' => $owners, 'vendorApprovelLogs' => $vendorApprovelLogs)); ?>
      <div class="clearfix"><br /></div>
    </div>

    <div role="tabpanel" class="tab-pane fade <?php echo !empty($_GET['tab']) && $_GET['tab'] == 'scorecard' ? 'in active' : '' ?>" id="tab_content10" aria-labelledby="scorecard-tab" style="padding: 13px;">
      <?php $this->renderPartial('_scorecard', array('vendor_id' => $vendor_id)); ?>
    </div>

  </div>
  <!--  <div role="tabpanel" class="tab-pane fade" id="tab_content11" aria-labelledby="supplier-tab" style="padding: 15px;">
        <?php //$this->renderPartial('_supplier_contracts',array('quickComparison'=>$quickComparison,'vendor_id'=>$vendor_id,));
        ?>
      </div> -->

</div>
<?php $this->renderPartial('../shared_component/_log', array('logTitle' => 'Change', 'logHistory' => $vendorLogs)); ?>
</div>

<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header alert-info">
        <h4 class="modal-title">Deactivate Supplier Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Deactivating this supplier will not allow users to invite them to sourcing activites or create contracts or savings records for them</p>
      </div>
      <div class="modal-footer">
        <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          NO
        </button>
        <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          YES
        </button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="activate_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header alert-info">
        <h4 class="modal-title">Activate Supplier Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to activate this supplier?</p>
      </div>
      <div class="modal-footer">
        <button id="no_activate" type="button" class="alert-success btn btn-default" data-dismiss="modal">NO</button>
        <button id="yes_activate" type="button" class="alert-danger btn btn-default" data-dismiss="modal">YES</button>
      </div>
    </div>
  </div>
</div>
<?php $this->renderPartial('_supplier_performance', array('vendor_id' => $vendor_id)); ?>
<?php $this->renderPartial('_supplier_risk', array('vendor_id' => $vendor_id)); ?>
<?php $this->renderPartial('_supplier_sustainability', array('vendor_id' => $vendor_id)); ?>
<?php $this->renderPartial('_supplier_modals', array('vendor_id' => $vendor_id)); ?>
<?php $this->renderPartial("_supplier_approvel_status_modal", array('vendor' => $vendor, 'vendor_approval_history' => $vendor_approval_history)); ?>
<script type="text/javascript">
  var department_ids = <?php echo json_encode($department_ids); ?>;
  var vendor_combo_data = {
    labels: [
      <?php $c = 1;
      foreach ($vendors_combo as $vendor_combo_row) { ?> '<?php echo date("M j", strtotime($vendor_combo_row['order_date'])); ?>'
        <?php if ($c != count($vendors_combo)) echo ',';
        ?>
      <?php $c += 1;
      } ?>
    ],

    full_labels: [
      <?php $c = 1;
      foreach ($vendors_combo as $vendor_combo_row) { ?> '<?php echo date("F j", strtotime($vendor_combo_row['order_date'])); ?>'
        <?php if ($c != count($vendors_combo)) echo ','; ?>
      <?php $c += 1;
      } ?>
    ],

    datasets: [
      {
        type: 'bar',
        label: 'Amount Spent',
        backgroundColor: 'lightgreen',
        fill: false,
        yAxisID: 'Y2',
        data: [
          <?php $c = 1;
          foreach ($vendors_combo as $vendor_combo_row) { ?>
            <?php echo round($vendor_combo_row['total_price'], 2); ?>
            <?php if ($c != count($vendors_combo)) echo ','; ?>
          <?php $c += 1;
          } ?>
        ]
      }
    ]
  };
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $(".processing_oboloo").css("display", "none");
    /*$("#vendor_name").select2({
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });
    */
    $("#location_id").select2({
      placeholder: "Select Locations",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });

    $("#department_id").select2({
      placeholder: "Select Departments",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });


    $(".select_search").select2({
      placeholder: "Select",
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });
    $("#request_document_form_alert").hide();
    $("#request_contact_form").submit(function(e) {
      e.preventDefault();
      var form = $(this);
      var url = form.attr('action');
      $.ajax({
        dataType: "json",
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
        success: function(submitted_response) {
          if (submitted_response.msg == 1) {
            $("#request_document_form_alert").show();
            $("#request_document_form_alert").html('Request Contract Document submitted successfully.');
          } else {
            $("#request_document_form_alert").html('Problem occured while processing Request Contract Document, try again');
            $("#request_document_form_alert").show();
          }
          $(".select_search").val('').trigger('change');
          $(".vendor_new_QIX").val('');
          $("#request_document_note").val('');


        }
      });
      $("#request_document_form_alert").delay(1000 * 5).fadeOut();
    });


    $('.expiry_date_1').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>'
    });


    $('#owners').select2({
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    }).val(null).trigger('change');
    $("#owners").select2({
      placeholder: 'Select Owner',
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
    });

    <?php if (isset($vendor['subindustry_id']) && !empty($vendor['subindustry_id'])) { ?>
      loadSubindustries(<?php echo $vendor['subindustry_id']; ?>);
    <?php } ?>

    $('#order_table').dataTable({
      "columnDefs": [{
          "targets": 0,
          "width": "6%",
          "orderable": false
        },
        {
          "type": "sort-month-year",
          targets: 3
        }
      ],
      "order": []
    });

    $('#contract_table').dataTable({
      "columnDefs": [{
          "targets": 0,
          "width": "6%",
          "orderable": false
        },
        {
          "type": "sort-month-year",
          targets: 3
        }
      ],
      "order": []
    });

    $('#quote_table').dataTable({
      "columnDefs": [{
          "targets": 0,
          "width": "6%",
          "orderable": false
        },
        {
          "type": "sort-month-year",
          targets: 3
        }
      ],
      "order": []
    });
    $('#saving_table').dataTable({
      "columnDefs": [{
          "targets": 0,
          "width": "6%",
          "orderable": false
        },
        {
          "type": "sort-month-year",
          targets: 3
        }
      ],
      "order": []
    });

    $('#vendor_risk_table').dataTable({
      "columnDefs": [{
          "targets": 0,
          /*"width": "6%" , */ "orderable": false
        },
        {
          "type": "sort-month-year",
          targets: 3
        }
      ],
      "order": []
    });

    $('#vendor_peformance_table').dataTable();
    $('#vendor_risk_table').dataTable();

    $(".status").tooltip({
      position: {
        my: "center bottom",
        at: "center top-10",
        collision: "flip",
        using: function(position, feedback) {
          $(this).addClass(feedback.vertical)
            .css(position);
        }
      }
    });



    $('#additional-field-model').duplicateElement({
      "class_remove": ".remove-this-field",
      "class_create": ".create-new-field"
    });

    $('#delete_confirm_modal .modal-footer button').on('click', function(event) {
      var button = event.target; // The clicked button

      $(this).closest('.modal').one('hidden.bs.modal', function() {
        if (button.id == 'yes_delete') changeItemStatus(0);
      });
    });

    $('#activate_confirm_modal .modal-footer button').on('click', function(event) {
      var button = event.target; // The clicked button

      $(this).closest('.modal').one('hidden.bs.modal', function() {
        if (button.id == 'yes_activate') changeItemStatus(1);
      });
    });



  });

  $("#requiredIndi").hide();

  function loadSubindustries(input_subindustry_id) {
    var industry_id = $('#industry_id').val();
    var input_subindustry_id = input_subindustry_id;

    if (industry_id == "") {
      $('#subindustry_id').html('<option value="">Select Supplier Sub Industry</option>');
      $('#subindustry_id').removeAttr("required");
      $("#requiredIndi").hide();
    } else {
      $.ajax({
        type: "POST",
        data: {
          industry_id: industry_id,
          subindustry_id: input_subindustry_id
        },
        dataType: "json",
        url: "<?php echo AppUrl::bicesUrl('vendors/getSubIndustries/'); ?>",
        success: function(options) {
          var options_html = '<option value="">Select Supplier Sub Industry</option>';

          if (options.suggestions.length) {
            $('#subindustry_id').attr("required", "required");
            $("#requiredIndi").show();
          } else {
            $('#subindustry_id').removeAttr("required");
            $("#requiredIndi").hide();
          }

          for (var i = 0; i < options.suggestions.length; i++)
            options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
          $('#subindustry_id').html(options_html);
          if (input_subindustry_id != 0) $('#subindustry_id').val(input_subindustry_id);
        },
        error: function() {
          $('#subindustry_id').html('<option value="">Select Supplier Sub Industry</option>');
        }
      });
    }
  }

  function changeItemStatus(active_flag) {
    var active_flag = active_flag;
    $.ajax({
      type: "POST",
      data: {
        delete_item_id: $('#vendor_id').val(),
        active_flag: active_flag
      },
      url: "<?php echo AppUrl::bicesUrl('vendors/updateItemStatus/'); ?>",
      success: function() {
        location = "<?php echo AppUrl::bicesUrl('vendors/edit/'); ?>" + $('#vendor_id').val();
      }
    });
  }

  function expandChart1(display_index) {
    var show_index = display_index;

    if ($('#table_area_' + show_index).is(':visible')) collapseChart(display_index);
    else {
      $('#chart_area_' + show_index).hide(1000);
      $('#table_area_' + show_index).show(1000);
    }
  }

  function checkUrl() {
    var str = $("#website").val();

    if (str) {
      validUrl = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.?\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[?6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1?,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00?a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u?00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
      if (!validUrl.test(str)) {
        $.alert({
          title: 'Incorrect Website Format',
          content: 'Please add https:// before the website name',
        });
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  function collapseChart(display_index) {
    var show_index = display_index;
    var hide_index = 0;
    if (display_index % 2 != 0) hide_index = show_index - 1;
    else hide_index = show_index + 1;

    $('#table_area_' + show_index).hide(1000);
    $('#table_area_' + hide_index).hide(1000);

    if (!$('#chart_area_' + show_index).is(':visible'))
      $('#chart_area_' + show_index).show(2000);
    if (!$('#chart_area_' + hide_index).is(':visible'))
      $('#chart_area_' + hide_index).show(2000);
  }

  $('.spinnerContainer').hide();

  function uploadDocument(uploadBtnIDX, e) {
    e.preventDefault();
    var uploadedObj = $('#order_file_' + uploadBtnIDX);
    var uploadedObjDesc = $('#file_desc_' + uploadBtnIDX);
    var uploadedDocType = $('#document_type_' + uploadBtnIDX);
    var uploadedDocStatus = $('#document_status_' + uploadBtnIDX);
    var uploadedDocExpiryDate = $('#expiry_date_' + uploadBtnIDX);

    var file_data = $('#order_file_' + uploadBtnIDX).prop('files')[0];
    var field_data = $('#file_desc_' + uploadBtnIDX).val();
    var field_type = $('#document_type_' + uploadBtnIDX).val();
    if ($('#document_status_' + uploadBtnIDX).is(':checked')) {
      var field_status = 'Approved';
    } else {
      var field_status = 'Pending';
    }
    var expiry_date = $('#expiry_date_' + uploadBtnIDX).val();

    if (typeof file_data === "undefined" || file_data == "") {
      $.alert({
        title: 'Required!',
        content: 'Document is required',
      });
    } else if (field_data == "") {
      $.alert({
        title: 'Required!',
        content: 'Document Title is required',
      });
    } else if (field_type == "") {
      $.alert({
        title: 'Required!',
        content: 'Document Type is required',
      });
    } else {

      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('file_description', field_data);
      form_data.append('document_type', field_type);
      form_data.append('vendor_id', $('#vendor_id').val());
      form_data.append('document_status', field_status);
      form_data.append('expiry_date', expiry_date);

      $.ajax({
        url: BICES.Options.baseurl + '/vendors/uploadDocument', // point to server-side PHP script 
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        xhr: function() {
          var xhr = new window.XMLHttpRequest();
          xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = Math.round((evt.loaded / evt.total) * 100);
              $("#progressBar").width(percentComplete + "%");
              $("#progressBar").text(percentComplete + "%"); // Set the percentage text
            }
          }, false);
          return xhr;
        },
        beforeSend: function() {
          $('.spinnerContainer').show();
          $("#progressBar").width("0%");
          $("#progressBar").text("0%"); // Initialize with 0%
          $("#btn_upload_1").prop('disabled', true);
        },
        success: function(uploaded_response) {
          window.location.assign(uploaded_response.url);
          uploadedObj.val(null);
          uploadedObjDesc.val(null);
          uploadedDocType.val(null);
          uploadedDocStatus.prop('checked', false);
          uploadedDocExpiryDate.val(null);
          $("#btn_upload_1").prop('disabled', false);
          // display response from the PHP script, if any
        },
        complete: function() {
          $(".spinnerContainer").hide();
        }
      });
    }
  }

  function uploadDocumentOnboard(uploadBtnIDX, e) {
    e.preventDefault();
    // alert("SDddddddddd"+ uploadBtnIDX);
    var uploadedObj = $('#order_file_onboard_' + uploadBtnIDX);
    var uploadedObjDesc = $('#file_desc_onboard_' + uploadBtnIDX);
    var uploadedDocType = $('#document_type_onboard_' + uploadBtnIDX);
    var uploadedDocStatus = $('#document_status_onboard_' + uploadBtnIDX);
    var uploadedDocExpiryDate = $('#expiry_date_onboard_' + uploadBtnIDX);

    var file_data = $('#order_file_onboard_' + uploadBtnIDX).prop('files')[0];
    var field_data = $('#file_desc_onboard_' + uploadBtnIDX).val();
    var field_type = $('#document_type_onboard_' + uploadBtnIDX).val();
    if ($('#document_status_onboard_' + uploadBtnIDX).is(':checked')) {
      var field_status = 'Approved';
    } else {
      var field_status = 'Pending';
    }
    var expiry_date = $('#expiry_date_onboard_' + uploadBtnIDX).val();

    if (typeof file_data === "undefined" || file_data == "") {
      $.alert({
        title: 'Required!',
        content: 'Document is required',
      });
    } else if (field_data == "") {
      $.alert({
        title: 'Required!',
        content: 'Document Title is required',
      });
    } else if (field_type == "") {
      $.alert({
        title: 'Required!',
        content: 'Document Type is required',
      });
    } else if (expiry_date == "") {
      $.alert({
        title: 'Required!',
        content: 'Expiry Date is required',
      });
    } else {

      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('file_description', field_data);
      form_data.append('document_type', field_type);
      form_data.append('vendor_id', $('#vendor_id').val());
      form_data.append('document_status', field_status);
      form_data.append('expiry_date', expiry_date);
      $.ajax({
        url: BICES.Options.baseurl + '/vendors/uploadDocumentOnboard', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        beforeSend: function() {
          $(".processing_oboloo").show();
          // $("#btn_upload_1").prop('disabled',true);
        },
        success: function(uploaded_response) {
          $("#document_list_cont_onboard").html(uploaded_response);
          uploadedObj.val(null);
          uploadedObjDesc.val(null);
          uploadedDocType.val(null);
          uploadedDocStatus.prop('checked', false);
          uploadedDocExpiryDate.val(null);
          $(".processing_oboloo").css("display", "none");
          // display response from the PHP script, if any
        }
      });
    }
  }

  function editSingleDocument(formID, modalID) {
    var modalID = modalID;
    var formID = formID;
    var expiry_date = $("#expiry_date_" + formID).val();
    var vendor_id = $("#vendor_id_" + formID).val();
    var document_id = $("#document_id_" + formID).val();
    $.ajax({
      url: BICES.Options.baseurl + '/vendors/editDocument', // point to server-side PHP script 
      dataType: 'json', // what to expect back from the PHP script, if anything
      data: {
        vendor_id: vendor_id,
        document_id: document_id,
        expiry_date: expiry_date
      }, //$('#'+formID).serialize(),                         
      type: 'post',
      success: function(response) {
        window.location.assign(response.url);
      }
    });

  }

  function editSingleDocumentOnboard(formID, modalID) {
    var modalID = modalID;
    var formID = formID;
    var expiry_date = $("#expiry_date_" + formID).val();
    var vendor_id = $("#vendor_id_" + formID).val();
    var document_id = $("#document_id_" + formID).val();
    $.ajax({
      url: BICES.Options.baseurl + '/vendors/editDocumentOnboard', // point to server-side PHP script 
      dataType: 'text', // what to expect back from the PHP script, if anything
      data: {
        vendor_id: vendor_id,
        document_id: document_id,
        expiry_date: expiry_date
      }, //$('#'+formID).serialize(),                         
      type: 'post',
      success: function(uploaded_response) {
        $('#' + modalID).modal('hide');
        $('.modal-backdrop').remove();
        $("#document_list_cont_onboard").html(uploaded_response);

      }
    });

  }

  function approveDocument(docID, e) {
    e.preventDefault();
    $.confirm({
      title: false,
      content: '<span style="font-size:13px">Are you sure you want to approve this document?</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              url: BICES.Options.baseurl + '/vendors/approveDocument', // point to server-side PHP script 
              dataType: 'json', // what to expect back from the PHP script, if anything
              data: {
                document_ids: docID,
                vendor_id: $('#vendor_id').val()
              },
              type: 'post',
              success: function(response) {
                window.location.assign(response.url);
              }
            });
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });
  }

  function approveDocumentOnboard(documentType, e) {
    e.preventDefault();
    var arrayIDs = [];
    var documentTypeID = documentType;
    inputField = $("input[name^='status_']");
    inputField.each(function(index) {
      fieldName = $(this).attr('name');
      if (fieldName.indexOf('status_' + documentTypeID) !== -1 && $(this).is(':checked'))
        arrayIDs.push($(this).val());
    });

    $.ajax({
      url: BICES.Options.baseurl + '/vendors/approveDocumentOnboard', // point to server-side PHP script 
      dataType: 'text', // what to expect back from the PHP script, if anything
      data: {
        document_ids: arrayIDs,
        vendor_id: $('#vendor_id').val()
      },
      type: 'post',
      success: function(uploaded_response) {
        $("#document_list_cont_onboard").html(uploaded_response);
        // display response from the PHP script, if any
      }
    });
  }

  function documentArchive(documentType, e, docID) {
    e.preventDefault();
    $.confirm({
      title: false,
      content: '<span style="font-size:13px">Are you sure you want to archive the document?</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              type: "POST",
              dataType: 'json', // what to expect back from the PHP script, if anything
              url: "<?php echo AppUrl::bicesUrl('vendors/documentArchive'); ?>",
              data: {
                document_ids: docID,
                vendor_id: $('#vendor_id').val()
              },
              success: function(response) {
                window.location.assign(response.url);
              }
            });
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });
  }

  function documentUnArchive(docID, e) {
    e.preventDefault();
    $.confirm({
      title: false,
      content: '<span style="font-size:13px">Are you sure you want to unarchive this document?</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              type: "POST",
              dataType: 'json', // what to expect back from the PHP script, if anything
              url: "<?php echo AppUrl::bicesUrl('vendors/documentUnArchive'); ?>",
              data: {
                document_ids: docID,
                vendor_id: $('#vendor_id').val()
              },
              success: function(response) {
                window.location.assign(response.url);
              }
            });
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });
  }

  function documentArchiveOnboard(documentType, e) { //docID.hide();

    e.preventDefault();

    var arrayIDs = [];
    var documentTypeID = documentType;
    inputField = $("input[name^='archive_']");
    // alert(documentID);
    inputField.each(function(index) {
      fieldName = $(this).attr('name');
      if (fieldName.indexOf('archive_' + documentTypeID) !== -1 && $(this).is(':checked'))
        arrayIDs.push($(this).val());
    });

    $.confirm({
      title: false,
      content: '<span style="font-size:13px">Are you sure you want to archive the document?</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              type: "POST",
              url: "<?php echo AppUrl::bicesUrl('vendors/documentArchiveOnboard'); ?>",
              data: {
                document_ids: arrayIDs,
                vendor_id: $('#vendor_id').val()
              },
              success: function(uploaded_response) {
                $("#document_list_cont_onboard").html(uploaded_response);
              }
            });
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });
  }

  function documentUnArchiveOnboard(documentType, e) {
    e.preventDefault();
    var arrayIDs = [];
    var documentTypeID = documentType;
    inputField = $("input[name^='archive_activate_']");
    inputField.each(function(index) {
      fieldName = $(this).attr('name');
      if (fieldName.indexOf('archive_activate_' + documentTypeID) !== -1 && $(this).is(':checked'))
        arrayIDs.push($(this).val());
    });
    $.confirm({
      title: false,
      content: '<span style="font-size:13px">Are you sure you want to unarchive this document?</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              type: "POST",
              url: "<?php echo AppUrl::bicesUrl('vendors/documentUnArchiveOnboard'); ?>",
              data: {
                document_ids: arrayIDs,
                vendor_id: $('#vendor_id').val()
              },
              success: function(uploaded_response) {
                $("#document_list_cont_onboard").html(uploaded_response);
              }
            });
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });
  }

  function deleteDocument(documentID) {
    var docID = documentID;
    $.confirm({
      title: false,
      content: '<spam style="font-size:13px">Are you sure want to delete this file/document</span>',
      buttons: {
        Yes: {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              type: "POST",
              url: "<?php echo AppUrl::bicesUrl('vendors/deleteDocument/'); ?>",
              data: {
                documentID: docID
              },
              success: function(uploaded_response) {
                $("#document_list_cont").html(uploaded_response);
              }
            });
          }
        },
        No: {
          text: "No",
          btnClass: 'btn-red',
          action: function() {}
        },
      }
    });
  }

  function deleteDocumentOther(documentID) {
    var docID = documentID;
    $('#delete_document_' + documentID).confirmation({
      title: "Are you sure you want to delete the document?",
      singleton: true,
      placement: 'right',
      popout: true,
      onConfirm: function() {
        $.ajax({
          type: "POST",
          url: "<?php echo AppUrl::bicesUrl('vendors/deleteDocumentOther/'); ?>",
          data: {
            documentID: docID
          },
          success: function(uploaded_response) {
            $("#document_list_cont").html(uploaded_response);
          }
        });
      },
      onCancel: function() {}
    });
  }

  $(".contact_name").change(function() {
    $(".contact_name").val($(this).val());
  });

  $(".phone_1").change(function() {
    $(".phone_1").val($(this).val());
  });

  $(".phone_2").change(function() {
    $(".phone_2").val($(this).val());
  });

  $('#document-tab').click(function() {
    checkOwnerDoucment()
  });

  function checkOwnerDoucment() {}

  loadDepartmentsForSingleLocation();

  function loadDepartmentsForSingleLocation(department_id) {
    var location_id = $('#location_id').val();
    if (location_id == 0)
      $('#department_id').html('');
    else {
      $.ajax({
        type: "POST",
        data: {
          location_id: location_id
        },
        dataType: "json",
        url: BICES.Options.baseurl + '/locations/getDepartments',
        success: function(options) {
          var options_html = '';
          for (var i = 0; i < options.length; i++) {
            options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
          }
          $('#department_id').html(options_html);
          if (department_ids.length > 0) $('#department_id').val(department_ids);
        },
        error: function() {
          $('#department_id').html('');
        }
      });
    }
  }
  
</script>
<style>
  .select2-search__field { padding: 2px 13px !important; }
</style>