 <?php $disabled = FunctionManager::sandbox(); ?>
<div class="modal fade" id="add_supplier_sustainability_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style=""> 

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Supplier Sustainability Record</h4>
         
      </div>
    <form id="order_invoice_form" class="form-horizontal col-md-12 form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('vendors/supplierSustainability'); ?>">    
        <div class="form-group">
           <div class="col-md-12">
            <h4 class="subheading" style="padding: 10px 0px 10px 0px;">Scoring system - 1= High, 2 = Medium & 3 = Low Sustainability Efforts</h4>
                <label class="control-label">Score <span style="color: #a94442;">*</span></label>
                <select class="form-control notranslate" name="sustainability_score" id="sustainability_score" required="required">
                  <option value="">Select</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                </select>
            </div>
          <div class="col-md-12">
                <label class="control-label">Please give additional details of your sustainability score <span style="color: #a94442;">*</span></label>
                 <textarea class="form-control notranslate" name="sustainability_score_reason" id="sustainability_score_reason" required="required"></textarea>
            </div>

     </div>
    <input type="hidden" name="sustainability_vendor_id"  value="<?php echo $vendor_id;?>" />
  <div class="modal-footer"><?php $disabled = FunctionManager::sandbox(); ?>
    <button type="submit" class="btn btn-success" <?php echo $disabled; ?>>Submit Supplier Sustainability</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div></form><div class="clearfix"></div></div></div></div>