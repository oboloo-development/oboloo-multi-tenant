<div class="col-md-12 col-sm-12 col-xs-12 p-0">
  <div class="x_title p-0" style="border-bottom:none"><h4 class="heading">Supplier Comparisons</h4>
   <h4 class="subheading"><br />Find all Supplier Comparisons Evaluations that include this supplier here.<br /></h4>
  </div>
  <br />
<?php if(!empty($quickComparison)){?>
<table id="quote_table" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th></th>
      <th>Supplier</th>
      <th>Best Score Supplier</th>
      <th>User name</th>
      <th>Created at</th>
    </tr>
  </thead>
  <tbody>
   <?php foreach ($quickComparison as $value) { 
      $modelMainID = $value['id'];
      $sql = "select group_concat(distinct vendor_name) as name from vendors inner join vendor_quick_scoring qs on vendors.vendor_id=qs.vendor_id where main_quick_scoring_id=".$modelMainID." order by vendor_name asc";
      $vendorReader = Yii::app()->db->createCommand($sql)->queryRow();
      $createdAt = $value['created_at'];
      $sql = "select group_concat(vendor_name) as name  from main_quick_scoring qs inner join vendors ven on FIND_IN_SET(ven.vendor_id,qs.best_supplier_id) where id=".$modelMainID."  order by name asc";
      $bestVendorReader = Yii::app()->db->createCommand($sql)->queryRow(); ?>
      <tr>
        <td><a href="<?php echo $this->createUrl('vendorEvaluation/edit',array('score_record'=>$modelMainID)); ?>" class="btn btn-sm btn-success view-btn" style="text-decoration: none;">View/Edit</a> </td>
        <td class="notranslate"><?php echo $vendorReader['name'];?></td>
        <td class="notranslate"><?php echo $bestVendorReader['name'];?></td>
        <td><?php echo $value['created_by_name'];?></td>
        <td><?php echo date("F j, Y H:i A", strtotime($value['created_at'])); ?></td>
      </tr>
   <?php } ?>
  </tbody>
</table>
<?php } ?>
</div>

