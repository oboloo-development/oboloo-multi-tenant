 <?php $disabled = FunctionManager::sandbox(); ?> 
<div class="modal fade" id="supplier_contract_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style=""> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add New Contact For Supplier</h4>
         
      </div>
    <div class="alert alert-success text-center" style="width: 100%; margin-top: 20px; display: none;" role="alert" id="supplier_contract_alert"></div>
    <form method="post" action="" id="supplier_contract_form">
      <input type="hidden" name="supplier_contact_id" value="<?php echo $vendor_id;?>" />
      <div class="form-group">
        <div class="col-md-6">
        <label>Contact Name <span style="color: #a94442;">*</span></label>
        <input type="text" name="contact_name" pattern="\S(.*\S)?" class="form-control resetform" onkeypress="return omit_special_char(event)" required>
        </div><div class="col-md-6">
        <label>Contact Email <span style="color: #a94442;">*</span></label>
        <input type="email" name="contact_email" class="form-control resetform" id="venodr_contact_email" required>
      </div>
        <div class="col-md-6 mt11">
             <label>Position<span style="color: #a94442;">*</span></label>
             <input type="text" name="contact_position" class="form-control resetform" id="venodr_contact_position" required>
           </div>
           <div class="col-md-6 mt11">
             <label>Phone Number <span style="color: #a94442;">*</span></label>
             <input type="tel" name="contact_phone_no" class="form-control resetform" id="venodr_contact_phone_no" required>
           </div>
           <div class="col-md-6 mt-15">
             <label>Location <span style="color: #a94442;">*</span></label>
             <input type="text" name="contact_location" class="form-control resetform" id="venodr_contact_location" required>
           </div>
    </div><div class="clearfix"></div><br /><br />
    <div class="modal-footer">
    <button type="submit" id="new_contract_submit" class="btn green-btn"  <?php echo $disabled; ?>>Save</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>

    </form>
    </div></div>
</div>

<div class="modal fade" id="website_url" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Activate Supplier Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Website url should of correct format</p>
        </div>
        <div class="modal-footer">
          <button id="no_activate" type="button" class="alert-success btn btn-default" data-dismiss="modal">
              Close
          </button>
         
        </div>
      </div>
  </div>
</div>

<style type="text/css">
  .modal-title { font-size: 20px !important }
</style>
