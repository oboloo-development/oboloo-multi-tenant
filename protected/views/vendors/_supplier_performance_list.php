<div class="pull-left" style="width: 71%">
<h4 class="heading">Supplier Performance</h4>
<h4 class="subheading"><br>Score the supplier here. This can be based on your own relationship with the supplier or how you perceive their overall performance. oboloo will then calculate an average performance score for this supplier. </h4>
</div>
<?php 
$disabledArchive = CommonFunction::disabledArchive('vendors', $vendor_id);
if(strtolower($disabledArchive) !='yes') { ?>
 <button type="button" class="btn btn-success pull-right" onclick="$('#add_supplier_performance_modal').modal('show');">
                <span class="glyphicon glyphicon-modal-window mr-2"></span> Add Supplier Performance Record
            </button><div class="clearfix"></div><br /><br />
  <?php } ?>
<?php if(count($vendorPerformance)>0) {?>
<div class="col-md-12">
<table id="vendor_peformance_table" class="table table-striped table-bordered" style="width: 100%;">
    <thead>
    <tr>
        <th style="width:20%">Title</th>
        <th style="width:30%">Details</th>
        <th style="width:30%">Reasons For Score</th>
        <th style="width: 6%;">0-10</th>
        <th style="width: 8%;">User</th>
        <th style="width: 8%;">Date & Time</th>
    </tr>
    </thead>

    <tbody>

    <?php
    foreach ($vendorPerformance as $value) {
        ?>
        <tr>
            <td class="notranslate"  style='word-break:break-all'><?php echo $value['title']; ?></td>
            <td style='word-break:break-all'><?php echo $value['description']; ?></td>
            <td style='word-break:break-all'><?php echo $value['score_reason']; ?></td>
            <td class="notranslate" style='word-break:break-all'><?php echo $value['score_value']; ?></td>
            <td class="notranslate" style='word-break:break-all'> <a href="<?php echo AppUrl::bicesUrl('users/edit/'.$value['created_by_id']); ?>"><?php echo $value['created_by_name']; ?></a></td>
            <td><?php echo date(FunctionManager::dateFormat(), strtotime($value['created_at'])); ?></td>
         
        </tr>

    <?php } ?>

    </tbody>

</table>
</div>
<?php } ?>
<div class="clearfix"><br /></div>
