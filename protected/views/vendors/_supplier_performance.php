
<div class="modal fade" id="add_supplier_performance_modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style=""> 

    <div class="modal-header">
        <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Supplier Performance Record</h4>
        
      </div>
    <form id="order_invoice_form" class="form-horizontal col-md-12 form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('vendors/supplierPerformance'); ?>">    
        <div class="form-group">
           <div class="col-md-12">
                <label class="control-label">Title <span style="color: #a94442;">*</span></label>
                 <input type="text"  class="form-control notranslate" name="title" id="title" required="required" />
            </div><div class="clearfix"></div><br />

          <div class="col-md-12">
                <label class="control-label">Details <span style="color: #a94442;">*</span></label>
                 <textarea class="form-control notranslate" name="description" id="description" required="required"></textarea>
            </div><div class="clearfix"></div><br />
            <table class="venor_performance table col-md-12 text-center">
               <thead>
              <tr>
                <th style="background: #F0DFDF;text-align: center;">0</th>
                <th style="background: #F0DFDF;text-align: center;">1</th>
                <th style="background: #F0DFDF;text-align: center;">2</th>
                <th style="background: #F0DFDF;text-align: center;">3</th>
                <th style="background: #F0DFDF;text-align: center;">4</th>
                <th style="background: #F0DFDF;text-align: center;">5</th>
                <th style="background: #F0DFDF;text-align: center;">6</th>

                <th style="background: #FDEC90;text-align: center;">7</th>
                <th style="background: #FDEC90;text-align: center;">8</th>

                <th  style="background: #DDF1D5;text-align: center;">9</th>
                <th style="background: #DDF1D5;text-align: center;">10</th>
              </tr>
               </thead>
               <tbody>
                <tr>
                  <td><input type="radio" class="notranslate" name="score" value="0" required></td>
                   <td><input type="radio" class="notranslate" name="score" value="1"></td>
                  <td><input type="radio" class="notranslate" name="score" value="2"></td>
                  <td><input type="radio" class="notranslate" name="score" value="3"></td>
                  <td><input type="radio" class="notranslate" name="score" value="4"></td>
                  <td><input type="radio" class="notranslate" name="score" value="5"></td>
                  <td><input type="radio" class="notranslate" name="score" value="6"></td>
                  <td><input type="radio" class="notranslate" name="score" value="7"></td>
                  <td><input type="radio" class="notranslate" name="score" value="8"></td>
                  <td><input type="radio" class="notranslate" name="score" value="9"></td>
                  <td><input type="radio" class="notranslate" name="score" value="10"></td>
                </tr>
               </tbody>
            </table>


            <div class="clearfix"></div><br />

          <div class="col-md-12">
                <label class="control-label">Please, Explain The Reasons For Your Score <span style="color: #a94442;">*</span></label>
                 <textarea class="form-control notranslate" name="score_reason" id="score_reason" required="required"></textarea>
            </div>

     </div>
    <input type="hidden" name="vendor_id"  value="<?php echo $vendor_id;?>" />
  <div class="modal-footer"> <?php $disabled = FunctionManager::sandbox(); ?>
    <button type="submit" class="btn btn-success"  <?php echo $disabled; ?>>Submit Supplier Performance</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div></form><div class="clearfix"></div></div></div></div>