<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3 class="notranslate">
            <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
            Savings Oracle Location
            <?php } else { ?> 
            Locations
            <?php } ?>
            </h3>
             <h4 class="subheading notranslate"><br />Add your organisation's 
                <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                savings oracle location
                <?php } else { ?> 
                Location
                <?php } ?>
                 here.<br /><br /></h4>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('locations/edit/0'); ?>">
                <button type="button" class="btn btn-warning">
                    <span class="glyphicon glyphicon-plus mr-2" aria-hidden="true"></span> Add 
                    <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                    Savings Oracle Location
                    <?php } else { ?> 
                    Location
                    <?php } ?>
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

    <table id="location_table" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
        <tr>
          <th> </th>
          <th>Name</th>
          <th>Contact</th>
          <!-- <th>Email</th> -->
          <th>Phone</th>
          <!-- <th>Website</th> -->
          <th>Address</th>
        </tr>
      </thead>

      <tbody>

          <?php foreach ($locations as $location) { ?>

              <tr>
                    <td>
                        <a href="<?php echo AppUrl::bicesUrl('locations/edit/' . $location['location_id']); ?>">
                            <button class="btn btn-sm btn-success view-btn">Edit</button>
                        </a>
                    </td>
                    <td><?php echo $location['location_name']; ?></td>
                    <td><?php echo $location['contact_person']; ?></td>
                    <!-- <td class="notranslate"><?php //echo $location['email']; ?></td> -->
                    <td><?php echo $location['phone']; ?></td>
                    <!-- <td><?php //echo $location['website']; ?></td> -->
                    <td>
                        <?php
                            echo $location['loc_address'];
                            if (!empty($location['loc_city'])) echo ', ' . $location['loc_city'];

                            if (!empty($location['loc_state']) || !empty($location['loc_zip_code']) || !empty($location['loc_country']))
                            {
                                echo '<br />';
                                echo $location['loc_state'];
                                if (empty($location['loc_state'])) echo $location['loc_zip_code'];
                                else echo ' ' . $location['loc_zip_code'];
                                if (empty($location['loc_state']) && empty($location['loc_zip_code']))
                                    echo $location['loc_country'];
                                else echo ' ' . $location['loc_country'];
                            }
                        ?>
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>

<script type="text/javascript">
$(document).ready( function() {
    $('#location_table').dataTable({
        "columnDefs": [ {
            "targets": 0,
            "width": "6%",
            "orderable": false
        } ],
        "order": []
    });
})
</script>
