<?php if(!empty($_GET['newloc'])){?>
  <div class="modal fade" id="addtional_field" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="">
    <div class="modal-content" style="max-width: 538px; margin: auto;">
      <div class="modal-body loc__modal_dpt">
        <p>Would you like to add a department to this location?</p>
      </div>
      <div class="modal-footer">
        <a href="<?php echo $this->createUrl('departments/list');?>"  class="btn btn-primary">Yes</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript">
  $('#addtional_field').modal('show');
</script>

<?php } ?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
      <?php $alert=Yii::app()->user->getFlash('success');
          if(!empty($alert)) { ?>
          <div class="clearfix"></div>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $alert;?></div>
        <?php } ?>
        <div class="span6 pull-left">
            <h3>
                <?php
                    if (isset($location_id) && $location_id)
                    {
                        if(strpos(Yii::app()->getBaseUrl(true),"usg")){
                      echo 'Edit Savings Oracle Location';
                       }else { 
                      echo 'Edit Location';
                       }
                        // echo 'Edit Location';
                        if (isset($location) && is_array($location) && isset($location['location_name']) && !empty($location['location_name']))
                            echo ' - ' . $location['location_name'];
                    }
                    else{
                    if(strpos(Yii::app()->getBaseUrl(true),"usg")){
                    echo 'Add Savings Oracle Location';
                     }else { 
                    echo 'Add Location';
                     } 
                    }
                ?>
            </h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('locations/list'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;">
                    <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> 
                    <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                    Savings Oracle Location List
                    <?php } else { ?> 
                    Location List
                    <?php } ?>
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

<div class="row tile_count">
  <form id="location_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('locations/edit'); ?>">
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label"> 
                 <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                    Savings Oracle Location Name
                    <?php } else { ?> 
                    Location Name
                    <?php } ?>
                <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control  notranslate" name="location_name" id="location_name"
                    <?php if (isset($location['location_name']) && !empty($location['location_name'])) echo 'value="' . $location['location_name'] . '"'; else echo 'placeholder="Location Name"'; ?> >
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">
              <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                Savings Oracle Location Contract
                <?php } else { ?> 
                Location Contact
                <?php } ?>
                <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control  notranslate" name="contact_person" id="contact_person"
                    <?php if (isset($location['contact_person']) && !empty($location['contact_person'])) echo 'value="' . $location['contact_person'] . '"'; else echo 'placeholder="Location Contact"'; ?> >
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Phone Number <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="phone" id="phone"
                      <?php if (isset($location['phone']) && !empty($location['phone'])) echo 'value="' . $location['phone'] . '"'; else echo 'placeholder="Phone Number"'; ?> >
          </div>
          <!-- <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Fax Number <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="fax" id="fax"
                      <?php //if (isset($location['fax']) && !empty($location['fax'])) echo 'value="' . $location['fax'] . '"'; else echo 'placeholder="Fax Number"'; ?> >
          </div> -->
      </div>

      <div class="form-group">
         <!--  <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Email Address <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="email" id="email"
                      <?php //if (isset($location['email']) && !empty($location['email'])) echo 'value="' . $location['email'] . '"'; else echo 'placeholder="Email Address"'; ?> >
          </div> -->
        <!--   <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Website URL <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="website" id="website"
                      <?php //if (isset($location['website']) && !empty($location['website'])) echo 'value="' . $location['website'] . '"'; else echo 'placeholder="Website URL"'; ?> >
          </div> -->
      </div>

      <div class="form-group" style="    margin-top: 25px;">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h5 style="font-weight: 700;">  
                   <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
                    Savings Oracle Location Address
                    <?php } else { ?> 
                    Location Address
                    <?php } ?></h5>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Address Line 1 <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="loc_label" id="loc_label"
                    <?php if (isset($location['loc_label']) && !empty($location['loc_label'])) echo 'value="' . $location['loc_label'] . '"'; else echo 'placeholder="Address Line 1"'; ?> >
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Address Line 2 <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="loc_address" id="loc_address"
                    <?php if (isset($location['loc_address']) && !empty($location['loc_address'])) echo 'value="' . $location['loc_address'] . '"'; else echo 'placeholder="Address Line 2"'; ?> >
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">City <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="loc_city" id="loc_city"
                    <?php if (isset($location['loc_city']) && !empty($location['loc_city'])) echo 'value="' . $location['loc_city'] . '"'; else echo 'placeholder="City"'; ?> >
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">State/County <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="loc_state" id="loc_state"
                    <?php if (isset($location['loc_state']) && !empty($location['loc_state'])) echo 'value="' . $location['loc_state'] . '"'; else echo 'placeholder="State/County"'; ?> >
          </div>
      </div>

      <div class="form-group">
          <?php $countries = FunctionManager::countryList();?>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Country <span style="color: #a94442;">*</span></label>
             
              <select class="form-control select_vendor_multiple notranslate" name="loc_country" id="loc_country">
                 <option value="">Select</option>
                <?php foreach($countries as $key => $value) {?>
               <option value="<?php echo $value;?>" <?php if(!empty($location['loc_country']) && $location['loc_country']==$value) { echo "selected='selected'";}?>><?php echo $value;?></option>
             <?php } ?>
              </select>
             <!--  <input type="text"  > -->
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Zip/Postal Code <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="loc_zip_code" id="loc_zip_code"
                    <?php if (isset($location['loc_zip_code']) && !empty($location['loc_zip_code'])) echo 'value="' . $location['loc_zip_code'] . '"'; else echo 'placeholder="Zip/Postal Code"'; ?> >
          </div>
      </div>

      <!-- <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h5>
                  <div class="pull-left">
                      Shipping Address
                  </div>

                  <div class="pull-right">
                      <small>
                          <input type="checkbox" class="flat" name="same_as_loc_addr"
                            id="same_as_loc_addr" onclick="copyLocationAddressAsShipping();" />
                          Same as location address
                      </small>
                  </div>
              </h5>
          </div>
      </div>
 -->
     <!--  <div class="form-group">
          <div class="col-md-2 col-sm-2 col-xs-4 date-input">
              <label class="control-label">Label <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control has-feedback-left" name="ship_label" id="ship_label"
                    <?php if (isset($location['ship_label']) && !empty($location['ship_label'])) echo 'value="' . $location['ship_label'] . '"'; else echo 'placeholder="Label"'; ?> >
              <span class="fa fa-sticky-note-o form-control-feedback left" aria-hidden="true"></span>
          </div>

          <div class="col-md-4 col-sm-4 col-xs-8 date-input">
              <label class="control-label">Shipping Address <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control has-feedback-left" name="ship_address" id="ship_address"
                    <?php if (isset($location['ship_address']) && !empty($location['ship_address'])) echo 'value="' . $location['ship_address'] . '"'; else echo 'placeholder="Shipping Address"'; ?> >
              <span class="fa fa-address-card-o form-control-feedback left" aria-hidden="true"></span>
          </div>
      </div>
 -->
      <!-- <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">City <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control" name="ship_city" id="ship_city"
                    <?php if (isset($location['ship_city']) && !empty($location['ship_city'])) echo 'value="' . $location['ship_city'] . '"'; else echo 'placeholder="City"'; ?> >
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">State <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control" name="ship_state" id="ship_state"
                    <?php if (isset($location['ship_state']) && !empty($location['ship_state'])) echo 'value="' . $location['ship_state'] . '"'; else echo 'placeholder="State"'; ?> >
          </div>
      </div>
 -->
     <!--  <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Country <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control" name="ship_country" id="ship_country"
                    <?php if (isset($location['ship_country']) && !empty($location['ship_country'])) echo 'value="' . $location['ship_country'] . '"'; else echo 'placeholder="Country"'; ?> >
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Zip/Postal Code <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control" name="ship_zip_code" id="ship_zip_code"
                    <?php if (isset($location['ship_zip_code']) && !empty($location['ship_zip_code'])) echo 'value="' . $location['ship_zip_code'] . '"'; else echo 'placeholder="Zip/Postal Code"'; ?> >
          </div>
      </div> -->

    <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="hidden" name="location_id" class="notranslate" id="location_id" value="<?php if (isset($location_id)) echo $location_id; ?>" />
    <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
     <?php $disabled = FunctionManager::sandbox(); ?>
      <?php if(strpos(Yii::app()->getBaseUrl(true),"usg")){?>
            <a href="javascript:void(0);"  onclick="$('#location_form').submit();">
              <button type="button" <?php echo $disabled; ?> class="btn btn-primary submit-btn1">
              <?php if (isset($location_id) && $location_id) echo 'Save Oracle Location'; else echo 'Save Oracle Location'; ?>
              </button>
              </a>
            <?php } else { ?> 
              <a href="javascript:void(0);"  onclick="$('#location_form').submit();">
              <button type="button" <?php echo $disabled; ?> class="btn btn-primary submit-btn1">
              <?php if (isset($location_id) && $location_id) echo 'Save Location'; else echo 'Save Location'; ?>
              </button>
              </a>
            <?php } ?>

    </div>
    </div>

  </form>
</div>
</div>


<script type="text/javascript">
$(document).ready( function() {

  $(".select_vendor_multiple").select2({
      placeholder: "Select Country",
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
      //allowClear: true
    });

    $( "#location_form" ).validate( {
        rules: {
            location_name: "required",
            loc_country: "required",
            // email: { required: true,/* email: true */}
        },
        messages: {
            location_name: "Location Name is required",
            email: "Please enter a valid email address",
            loc_country: "Location Country needs to be specified"
        },
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-error" ).removeClass( "has-success" );
			$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-success" ).removeClass( "has-error" );
			$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field"
    });
});

$('input').on('ifChecked', function(event){
    var fields = new Array('address', 'city', 'state', 'zip_code', 'country', 'label');
	for (var i=0; i<fields.length; i++)
        $('#ship_' + fields[i]).val($('#loc_' + fields[i]).val());
});

</script>