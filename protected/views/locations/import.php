<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Import Locations</h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('locations/list'); ?>">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Location List
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

    <div class="x_content">
      <p>Select one or more CSV files to import locations. They will be automatically imported.</p>
      <p> </p>
      <p>The file should not have any header and must have fields in the following order: </p>
      <p> </p>
      <p>
          Location Name, Contact Person Name, Email, Phone, Fax, Website,
          Location Label, Location Address, City, State, Zip/Postal Code, Country,
          Shipping Label, Shipping Address, City, State, Zip/Postal Code, Country
      </p>

      <div class="row tile_count">
          <div class="clearfix"> </div>
      </div>

      <form name="import_form" id="import_form" action="<?php echo AppUrl::bicesUrl('locations/importData'); ?> " method="post" enctype="multipart/form-data" class="dropzone">
          <div class="fallback">
              <input name="file[]" type="file" multiple />
          </div>
      </form>

    </div>

</div>
