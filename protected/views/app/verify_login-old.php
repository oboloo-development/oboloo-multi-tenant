<script type="text/javascript" src="<?php echo AppUrl::jsUrl('app.js'); ?>"></script>
<div>

	<div class="col-md-3" style="width: 24% !important; margin-left: 38% !important;">
			<br /><br />
          <img class="img-responsive" src="<?php echo AppUrl::bicesUrl('images/app_logo.png'); ?>" />
          <div class="clearfix"> </div>
	</div>
    <div class="clearfix"> </div>
	
  <a class="hiddenanchor" id="signup"></a>
  <a class="hiddenanchor" id="signin"></a>

  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
        

          <form novalidate id="login_form" name="login_form" method="post" action="<?php echo AppUrl::bicesUrl('app/verifyLogin'); ?>">
         
           <?php if(!empty($error_message)){?>
            <div class="form-group">
              <div class="alert alert-danger" role="alert"><?php echo $error_message;?></div>
            </div>
           <?php } ?>
          

          <?php if(empty($user_data['google_secret_code'])){?>
          <div class="form-group">
              <img src="<?php echo $qr_code; ?>">
            </div>

            <p style="font-size: 14px; padding-bottom: 12px;">Welcome to oboloo's Two Factor Authentication security login. Please download the Google Authenticator app and scan the above QR code. Once scanned please enter the code from your phone into the field above.</p>

            <input type="hidden" name="google_secret_code" id="google_secret_code" value="<?php echo $secret;?>" />
          <?php } ?>
          <div class="clearfix"></div>
          <div class="form-group">
            <input required="required" name="code" type="text" class="form-control" placeholder="Code" required="required" />
          </div>

           <p style="font-size: 14px; padding-bottom: 12px;">Please enter your two factor authenticator code from the Google Authenticator app. A code has also been sent to your email inbox</p>

          <div class="login_btns">
            <button class="btn btn-default submit  logocolorbtn" onclick="return checkLogin();">Submit Code</button><div class="clearfix"></div>

          </div>
          <input type="hidden" name="action_login" id="action_login" value="1" />

          <div class="clearfix"></div>

          
        </form>
      </section>
    </div>

 

 

    <div id="status_area">
    </div>
    <div class="clearfix"></div>


  </div>
</div>
<style type="text/css">
  .login_content {

    width: auto !important;
</style>
