<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Manage Drop Down Values</h3>
            <h4 class="subheading"><br>Configure oboloo's drop down options here</h4>
        </div>
        <div class="clearfix"> </div>
    </div>

<?php
	$data_types = array(
		//'account_type' 		=> 'Account Types',
		'category'			=> 'Category',
		'subcategory'		=> 'Sub Category',
		// 'contract_type'		=> 'Contract Type',
		//'contract_status'	=> 'Contract Status',
		//'project_status'	=> 'Project Status',
		//'expense_type'		=> 'Expense Type',
		'industry'			=> 'Industry',
		'subindustry'		=> 'Sub Industry',
		//'supplier_scoring_criteria'  => 'Supplier Scoring Criteria',
		'payment_term'		=> 'Payment Term',
		//'payment_type'		=> 'Payment Type',
		'shipping_method'	=> 'Shipping Method',
		'shipping_term'		=> 'Shipping Term',
		'saving_status'		=> 'Saving Status',
	);
?>

<div class="tile_count">
  <form id="settings_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('app/settings'); ?>">
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
          	<select name="data_type" id="data_type" class="form-control notranslate" onchange="$('#data_type_change').val(1); $('#form_submitted').val(0); $('#settings_form').submit();">
          		<option value="">Select Drop Down To Manage Values</option>

          		<?php foreach ($data_types as $a_data_type => $a_data_type_display) { ?>
          				<option value="<?php echo $a_data_type; ?>"
          						<?php if (isset($data_type) && $data_type == $a_data_type) echo ' selected="SELECTED" '; ?>>
          					<?php echo $a_data_type_display; ?>
          				</option>
          		<?php } ?>
          	</select>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-4">
				<?php if (isset($data_type) && !empty($data_type)) { ?>
					<?php if($data_type =='category') {?>
          			<a style="text-decoration: underline; cursor: pointer;" onclick="addRow(); return 
						false;">Add New Category</a>
					<?php }else if($data_type =='subcategory'){ ?>
						<a style="text-decoration: underline; cursor: pointer;" onclick="addRow(); return false;">Add New Sub Category</a>
					<?php }else if($data_type =='industry'){ ?>
						<a style="text-decoration: underline; cursor: pointer;" onclick="addRow(); return false;">Add New Industry</a>
					<?php }else if($data_type =='subindustry'){ ?>
						<a style="text-decoration: underline; cursor: pointer;" onclick="addRow(); return false;">Add New Sub Industry</a>
					<?php }else if($data_type =='payment_term'){ ?>
						<a style="text-decoration: underline; cursor: pointer;" onclick="addRow(); return false;">Add New Payment Term</a>
					<?php }else if($data_type =='shipping_method'){ ?>
						<a style="text-decoration: underline; cursor: pointer;" onclick="addRow(); return false;">Add New Shipping Method</a>
					<?php }else if($data_type =='shipping_term'){ ?>
						<a style="text-decoration: underline; cursor: pointer;" onclick="addRow(); return false;">Add New Shipping Term</a>
					<?php }else if($data_type =='saving_status'){ ?>
						<a class="btn  btn-primary" onclick="addRow(); return false;">Add New Saving Status</a>
					<?php }else { ?>
						<a style="text-decoration: underline; cursor: pointer;" onclick="addRow(); return false;">Add New Value</a>
					<?php }?>
					
          		<?php } ?>
          </div>

         <!--  <div class="col-md-2 col-sm-2 col-xs-4">
				<?php if (isset($data_type) && !empty($data_type)) { ?>
					
					<?php if ($data_type == 'category' || $data_type == 'industry' || $data_type == 'subcategory' || $data_type == 'subindustry') { ?>


				            <a href="<?php echo AppUrl::bicesUrl('app/export/?data_type=' . $data_type); ?>">
				                <button type="button" class="btn btn-default">
				                    <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export CSV
				                </button>
				            </a>


					<?php } ?>
					
					
          		<?php } ?>
          </div> -->
      </div>
      <div class="clearfix"> </div>
      
      <?php if (isset($additional_data_filters) && is_array($additional_data_filters) && count($additional_data_filters)) { ?>
      	
      		<div class="clearfix"> <br /><br /> </div>
	        <div class="form-group">
    	    <div class="col-md-6 col-sm-6 col-xs-12">
      		<select name="additional_data_filter" id="additional_data_filter" class="form-control notranslate" onchange="$('#form_submitted').val(0); $('#settings_form').submit();">
      				<option value="">Select Additional Data Filter For Drop Down Values</option>
      				<?php foreach ($additional_data_filters as $filter_id => $filter_value) { ?>
      						<option value="<?php echo $filter_id; ?>"
      								<?php if (isset($additional_data_filter) && $additional_data_filter == $filter_id) echo ' selected="SELECTED" '; ?>>
      							<?php echo $filter_value; ?>
      						</option>
      				<?php } ?>
      		</select>
      		</div>
      		</div>
      		
      <?php } ?>

      <div class="row tile_count"><br /><hr style="border: 1px solid #c7c7c7;" /><br /></div>

	  <div id="datatype_values">
	  <?php $total_rows = 0; foreach ($data as $row) { $total_rows += 1; ?>
	      <div class="form-group" id="data_row_<?php echo $total_rows; ?>">
	          <div class="col-md-6 col-sm-6 col-xs-12">
	              <?php if($data_type=='contract_status') { ?>
					 <div style="float: left; width: 83%;">
						 <input type="text" class="form-control notranslate" name="value_<?php echo $total_rows; ?>" id="value_<?php echo $total_rows; ?>"
						  <?php if (isset($row['value']) && !empty($row['value'])) echo 'value="' . $row['value'] . '"'; else echo 'placeholder="Dropdown Option Value"'; ?> />
				     </div>
					  <div style="float: left;width: 2%">&nbsp;</div>
				  <div style="float: left;width: 15%;">
					  <input type="text" style="width: 60px;" class="form-control notranslate" name="statusCode_<?php echo $total_rows; ?>" id="statusCode__<?php echo $total_rows; ?>"
						  <?php if (isset($row['code'])) echo 'value="' . $row['code'] . '"'; else echo '0'; ?> />
				  </div>
				  <?php } else { ?>
					  <input type="text" class="form-control notranslate" name="value_<?php echo $total_rows; ?>" id="value_<?php echo $total_rows; ?>"
						  <?php if (isset($row['value']) && !empty($row['value'])) echo 'value="' . $row['value'] . '"'; else echo 'placeholder="Dropdown Option Value"'; ?> />

				  <?php } ?>
	              <input type="hidden" class="notranslate" name="id_<?php echo $total_rows; ?>" id="id_<?php echo $total_rows; ?>" 
	                    <?php if (isset($row['id']) && !empty($row['id'])) echo 'value="' . $row['id'] . '"'; else echo '0'; ?> />
	              <input type="hidden" class="notranslate" name="deleted_flag_<?php echo $total_rows; ?>" id="deleted_flag_<?php echo $total_rows; ?>" value="0" /> 
	          </div>
	          <div class="col-md-2 col-sm-2 col-xs-4">
	          		<a style="text-decoration: underline; cursor: pointer;" onclick="deleteRow(<?php echo $total_rows; ?>); return false;">Delete</a>
	          </div>
	      </div>
	  <?php } ?>
	  </div>
    <div class="clearfix"> </div>
	  
    <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
    <input type="hidden" class="notranslate" name="data_type_change" id="data_type_change" value="0" />

     <input type="hidden" class="notranslate" name="total_rows" id="total_rows" value="<?php echo $total_rows; ?>" />
   

	<?php if (isset($data_type) && !empty($data_type)) { ?>
	    <a href="javascript:void(0);" onclick="$('#settings_form').submit();">
	        <button type="button" class="btn btn-primary submit-btn">
	            Save Data
	        </button>
	    </a>
	<?php } ?>
    </div>
    </div>

  </form>
</div>
</div>

<div id="new_row_code" style="display: none;">
	      <div class="form-group" id="data_row_ROWIDX">
	          <div class="col-md-6 col-sm-6 col-xs-12">
				  <?php if($data_type=='contract_status') { ?>
					  <div style="float: left; width: 83%;">
						  <input type="text" class="form-control notranslate" name="value_ROWIDX" id="value_ROWIDX" placeholder="Dropdown Option Value" />
					  </div>
					  <div style="float: left;width: 2%">&nbsp;</div>
					  <div style="float: left;width: 15%;">
						  <input type="text" style="width: 60px;"  class="form-control notranslate" name="statusCode_ROWIDX" id="statusCode_ROWIDX" placeholder="Code" />
					  </div>
				  <?php } else if($data_type=='supplier_scoring_criteria') { ?>
					  <div style="float: left; width: 80%;">
						  <input type="text" class="form-control notranslate" name="value_ROWIDX" id="value_ROWIDX" placeholder="Dropdown Option Value" />
					  </div>
					  <div style="float: left;width: 2%">&nbsp;</div>
					  <!-- <div style="float: left;width: 18%;">
						  <input type="text"   class="form-control" name="score_ROWIDX" id="score_ROWIDX" placeholder="Scoring" />
					  </div> -->
				  <?php } else { ?>
	              <input type="text" class="form-control notranslate" name="value_ROWIDX" id="value_ROWIDX" placeholder="Dropdown Option Value" />
	              <?php } ?>
				  <input type="hidden" class="notranslate" name="id_ROWIDX" id="id_ROWIDX" value="0" />
	              <input type="hidden" class="notranslate" name="deleted_flag_ROWIDX" id="deleted_flag_ROWIDX" value="0" />
	          </div>
	          <div class="col-md-2 col-sm-2 col-xs-4">
	          		<a style="text-decoration: underline; cursor: pointer;" onclick="deleteRow(ROWIDX); return false;">Delete</a>
	          </div>
	      </div>
</div>

<script type="text/javascript">
function deleteRow(row_idx)
{
	if (confirm('Are you sure you want to delete this row? This action cannot be reversed.'))
	{
		$('#deleted_flag_' + row_idx).val(1);
		$('#data_row_' + row_idx).hide();
	}
}

function addRow()
{
	var total_rows = $('#total_rows').val();
	total_rows = parseInt(total_rows) + 1;
	$('#total_rows').val(total_rows);
	
	var new_row_html = $('#new_row_code').html();
	new_row_html = new_row_html.replace(/ROWIDX/g, total_rows);
	$('#datatype_values').prepend(new_row_html);
}
</script>
