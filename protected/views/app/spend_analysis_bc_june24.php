<?php
$tool_currency = Yii::app()->session['user_currency'];
?>

<div class="row tile_count">
            <?php
            $sparklineData = '';
            // START: Spartk1
            $spark1Exp = '';
            error_reporting(0);
            $orderSpendTotalAmount =0; $monthlyData = array();
            if(count($order_spend_stats)>0)
            {
                foreach ($order_spend_stats as $order)
                {
                    $monthlyData[$order['month_year']] = $monthlyData[$order['month_year']]+$order['total_amount'];
                    
                }

                foreach ($monthlyData as $key=>$orderValue)
                {
                    $orderSpendTotalAmount += $orderValue;
                    $spark1Exp .= '{ x: "'.$key.'",y: '.$orderValue.'},';
                }
            }
            // END: Spartk1

            // START: Spark2 Order count
            $spark2Exp= '';
            $totalOrders=0;
            if(!empty($order_count))
            {
                foreach ($order_count as $order)
                {
                    $totalOrders += $order['total_orders'];
                    $spark2Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_orders'].'},';
                }
            }
            // END: Spartk2 Order count


            $spark3Exp = '';
            $totalToApprovOrders =0;
            if(count($order_spend_approve_stats)>0)
            {

                foreach ($order_spend_approve_stats as $order)
                {
                    $totalToApprovOrders += $order['total_amount'];
                    $spark3Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_amount'].'},';
                }
            }
            // END: Spartk3 Order to approve count

            // START: Spark4 Order count
            $spark4Exp= '';
            $totalPendingOrders=0;
            if(!empty($order_pending_count))
            {
                foreach ($order_pending_count as $order)
                {
                    $totalPendingOrders += $order['total_orders'];
                    $spark4Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_orders'].'},';
                }
            }
            // END: Spartk4 Order count

            $sparklineData2 = '';
            if(count($order_spend_vendor_stats)>0)
            {
            foreach ($order_spend_vendor_stats as $vendor)
                {
                    $sparklineData2 .= $vendor['total'].',';
                }

            }
            
            // START: Spark5 Order count
            $spark5Exp= '';
            $totalVendorAmount=0;
            if(!empty($order_spend_vendor_stats))
            {
                foreach ($order_spend_vendor_stats as $vendor)
                {
                    $totalVendorAmount += $vendor['total'];
                    $spark5Exp .= '{ x: "'.(!empty($vendor['vendor_name'])?$vendor['vendor_name']:$vendor['vendor_id']).'",y: '.$vendor['total'].'},';
                }
            }
            // END: Spartk5 Order count


            // START: Spark6 Order count
            $spark6Exp= '';
            $totalPaiedAmount=0;
            if(!empty($order_spend_paid_stats))
            {
                foreach ($order_spend_paid_stats as $order)
                {
                    $totalPaiedAmount += $order['total_amount'];
                    $spark6Exp .= '{ x: "Order #'.$order['order_id'].'",y: '.$order['total_amount'].'},';
                }
            }
            // END: Spartk6 Order count
            ?>

    <div class="">
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#total_year_spend_modal').modal('show');"><div id="spark1"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#total_year_ordercount_modal').modal('show');"><div id="spark2"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#approved_spend_modal').modal('show');"><div id="spark3"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#approved_ordertoapprove_modal').modal('show');"><div id="spark4"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#supplier_spend_modal').modal('show');"><div id="spark5"></div></a>
        </div>
        <div class="col-md-2">
            <a style="cursor: pointer;" onclick="$('#paid_spend_modal').modal('show');"><div id="spark6"></div></a>
        </div>
    </div>
</div>

		<div class="clearfix"><br /></div>
            <?php
            	for ($display_index = 1; $display_index <= 5; $display_index++)
            	{
            		$top_datasets = array();
					$canvas_id = "";

					if ($display_index == 1)
					{
						$top_datasets = $current_year;
		            	$canvas_id = 'year_to_date';
					}
					else if ($display_index == 2)
					{
	            		$top_datasets = $top_vendors;
		            	$canvas_id = 'spend_vendors';
					}
					/*
					else if ($display_index == 3)
					{
						$top_datasets = $top_departments;
		            	$canvas_id = 'spend_departments';
					}
					*/
					else if ($display_index == 3)
					{
						$top_datasets = $top_locations;
		            	$canvas_id = 'spend_locations';
					}
					else if ($display_index == 4)
					{
						$top_datasets = $year_over_year;
		            	$canvas_id = 'year_over_year';
					}
					else if ($display_index == 5)
					{
						$top_datasets = $top_contracts;
		            	$canvas_id = 'spend_contracts';
					}

					$canvas_header = str_replace('Spend ', 'Top ', ucwords(str_replace("_", " ", $canvas_id)));
					if ($display_index == 3) $canvas_header .= ' (Current Year)';
					$canvas_header = str_replace(' Over ', ' On ', $canvas_header);
					$canvas_header = str_replace(' Vendors', ' Suppliers', $canvas_header);
					if ($canvas_id == 'year_to_date') $canvas_header = 'Monthly Spend';

            ?>

		            <?php if ($display_index % 2 == 0) echo '<div class="row">'; ?>

						<?php if ($display_index != 1) { ?>
							<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_<?php echo $display_index; ?>">
						<?php } else { ?>
							<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_<?php echo $display_index; ?>">
						<?php  } ?>
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2><?php echo $canvas_header; ?></h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
				                  <?php if ($display_index != 1) { ?>
				                    <li><a onclick="expandChart(<?php echo $display_index; ?>);"><i class="fa fa-arrows-h"></i></a></li>
				                  <?php  } else{ ?>
									  <li><a onclick="expandChart1(<?php echo $display_index; ?>);"><i class="fa fa-arrows-h"></i></a></li>
								  <?php } ?>
				                  <li><a onclick="exportChart('<?php echo $canvas_id; ?>');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
	                      		<?php if ($canvas_id == 'year_to_date') { ?>
	                      			<canvas id="<?php echo $canvas_id; ?>" width="920px" height="200px"></canvas>
	                      		<?php } else { ?>
	                        		<canvas id="<?php echo $canvas_id; ?>" height="140px"></canvas>
	                        	<?php } ?>
	                        </div>
	                      </div>
	                   </div>

						<?php //if ($display_index != 1) { ?>
						<div <?php if ($display_index != 1) { ?> class="col-md-6 col-sm-6 col-xs-12" <?php } else { ?> class="col-md-12 col-sm-12 col-xs-12" <?php } ?> style="display: none;" id="table_area_<?php echo $display_index; ?>">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2><?php echo $canvas_header; ?></h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(<?php echo $display_index; ?>);"><i class="fa fa-arrows-h"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
					            			<?php
					            				$idx = 0;
					            				foreach ($top_datasets as $dataset_idx => $a_dataset)
												{
													if ($dataset_idx === 'total') continue;
													$idx += 1;
													switch ($idx)
													{
														case 1  : $color = 'blue'; break;
														case 2  : $color = 'purple'; break;
														case 3  : $color = 'green'; break;
														case 4  : $color = 'turquoise'; break;
														case 5  : $color = 'red'; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														case 10  : $color = 'blue'; break;
														case 11  : $color = 'purple'; break;
														case 12  : $color = 'green'; break;
														case 13  : $color = 'turquoise'; break;
														case 14  : $color = 'red'; break;
														case 15  : $color = 'antique-white'; break;
														case 16  : $color = 'aqua-marine'; break;
														case 17  : $color = 'bisque'; break;
														case 18  : $color = 'khaki'; break;
														case 19  : $color = 'blue'; break;
														case 20  : $color = 'purple'; break;
														case 21  : $color = 'green'; break;
														case 22  : $color = 'turquoise'; break;
														case 23  : $color = 'red'; break;
														case 24  : $color = 'antique-white'; break;
														case 25  : $color = 'aqua-marine'; break;
														case 26  : $color = 'bisque'; break;
														case 27  : $color = 'khaki'; break;
														case 28  : $color = 'blue'; break;
														case 29  : $color = 'purple'; break;
														case 30  : $color = 'green'; break;
														case 31  : $color = 'turquoise'; break;
														case 32  : $color = 'red'; break;
														case 33  : $color = 'antique-white'; break;
														case 34  : $color = 'aqua-marine'; break;
														case 35  : $color = 'bisque'; break;
														case 36  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
					            			?>
					                          		<tr>
					                            		<td style="width: 5%;"><i class="fa fa-square <?php echo $color; ?>"></i></td>
					                            		<td style="vertical-align: middle;">
					                              			<?php echo $a_dataset['dim_1']; ?> - <?php echo number_format($a_dataset['percent'], 2) . '%'; ?>
					                            		</td>
					                          		</tr>
					                        <?php
					                        	}
					                        ?>
					                        </table>
			              </div>
			           </div>
			        </div>
			        <?php // } ?>

		            <?php if ($display_index % 2 == 1) echo '</div><div class="clearfix"> </div><div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>'; ?>

			<?php } ?>








<script>
window.Apex = {
    stroke: {
        width: 3
    },
    markers: {
        size: 0
    },
    tooltip: {
        fixed: {
            enabled: true,
        }
    }
};

var randomizeArray = function (arg) {
    var array = arg.slice();
    var currentIndex = array.length,
        temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}
// data for the sparklines that appear below header area
/*var sparklineData = [1000, 2000, 5000, ];*/
var spark1 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: "Spend",
            data: [<?php echo $spark1Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($orderSpendTotalAmount, 2); ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Total Year Spend',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}

var spark2 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: {
        curve: 'straight'
    },
    fill: {
        opacity: 0.3,
    },
    series: [{
            name: "Orders",
            data: [<?php echo $spark2Exp;?>]
        },],
    yaxis: {
        min: 0
    },

    title: {
        text: '<?php echo number_format($totalOrders, 2); ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Total Year Orders',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}

var spark3 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: "Total Spend",
            data: [<?php echo $spark3Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalToApprovOrders, 2); ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Spend To Approve',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}

var spark4 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: {
        curve: 'straight'
    },
    fill: {
        opacity: 0.3,
    },
    series: [{
            name: "Orders",
            data: [<?php echo $spark4Exp;?>]
        },],
    yaxis: {
        min: 0
    },

    title: {
        text: '<?php echo number_format($totalPendingOrders, 2); ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Orders To Approve',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}


   var spark5 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: "Spend",
            data: [<?php echo $spark5Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalVendorAmount, 2); ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Top 10 Suppliers Spend',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}

    var spark6 = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: {
        curve: 'straight'
    },
    fill: {
        opacity: 0.3,
    },
    series: [{
            name: "Orders",
            data: [<?php echo $spark6Exp;?>]
        },],
    yaxis: {
        min: 0
    },

    title: {
        text: '<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalPaiedAmount, 2); ?>',
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: 'Orders To Be Paid',
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}


    var spark1 = new ApexCharts(document.querySelector("#spark1"), spark1);
    spark1.render();
    var spark2 = new ApexCharts(document.querySelector("#spark2"), spark2);
    spark2.render();
    var spark3 = new ApexCharts(document.querySelector("#spark3"), spark3);
    spark3.render();
    var spark4 = new ApexCharts(document.querySelector("#spark4"), spark4);
    spark4.render();
    var spark5 = new ApexCharts(document.querySelector("#spark5"), spark5);
    spark5.render();
    var spark6 = new ApexCharts(document.querySelector("#spark6"), spark6);
    spark6.render();

</script>
