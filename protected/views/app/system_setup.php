<?php
  $username_value = "";
  if (isset(Yii::app()->session['duplicate_username'])) $username_value = Yii::app()->session['duplicate_username'];
  else if (isset($user) && isset($user['username'])) $username_value = $user['username'];

  $email_value = "";
  if (isset(Yii::app()->session['duplicate_email'])) $email_value = Yii::app()->session['duplicate_email'];
  else if (isset($user) && isset($user['email'])) $email_value = $user['email'];

  $currency_rate = new CurrencyRate();
  $currency_rate= $currency_rate->getAllCurrencyRate();

  $record_user_id = 0;
  if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];?>
<?php //$countries = FunctionManager::countryList();
  $sql = "SELECT country from currency_rates";
  $countries = Yii::app()->db->createCommand($sql)->queryAll();

  $industry = new Industry();
  $industries = $industry->getAll(array('ORDER' => 'value'));

?>
<div class="" role="main">
     <div class="container">
        <!-- <div class="panel panel-primary row" style="margin: 0px;"> -->
         <!--  <div class="col-md-3 col-xs-12">
            <div class="logo-img pull-right">
              <a href="<?php echo AppUrl::bicesUrl('app/indexSetup'); ?>"> <img style="width: 200px; height: 100px;"
                src="<?php echo AppUrl::bicesUrl('images/dashboard_logo.png'); ?>"
                class="img-responsive" />
              </a>
            </div>
          </div>
           <div class="col-md-6 col-xs-12"> -->
            <!-- <h1 class="panel-body text-center" style="margin-bottom: 0px;padding: 35px 0px 35px 65px;color: #fff;font-weight: 600;" >Welcome to oboloo! Let's set up your account</h1> -->
        </div>
      </div>
      </div>

    <div class="row-fluid tile_count">
      <div class="container_stetup">


      <div class="col-xs-12">
        <div class="system_setup" >
       
       <?php  $alert = Yii::app()->user->getFlash('success');
       if(!empty($alert)){?>
            <div class="alert alert-success">
                <?php echo $alert; ?>
            </div>
        <?php } ?>

          <div class="panel panel-default">
             
              <div class="panel-body">
              <div class="pull-left">Progress</div><div class="pull-right progress-bar-number">0%</div>
              <div class="clearfix"></div>
              <div class="progress">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
              aria-valuemin="0" aria-valuemax="100" style="width:0%">
                
              </div>
            </div>
            </div>
          </div>

         <div class="row">
          <div class="col-xs-12">
           <div class="panel panel-default" style=" margin-bottom: 0px; padding: 20px 0px 20px 0px;">
            <div class="panel-body">
               <h1 class="panel-body text-center" style="margin-bottom: 0px;padding: 35px 0px 35px 65px;color: #000;font-weight: 600;" >Welcome to oboloo</h1>
              <div class="col-xs-1">
                <span class="fa fa-check check"></span>
              </div>
              <div class="col-xs-9">
                <h2 class="panel-body">Let's get started <?php echo Yii::app()->session['full_name'];?></h2>
                <h5 class="panel-body">Complete the tasks below to get your account set up</h5>
                <h5 class="panel-body">This takes less than 5 minutes</h5>
                <h5 class="panel-body">Please note that any information you enter now can be easily changed later in oboloo</h5>
              </div>
            </div>
            </div>

            <!-- <div class="panel panel-default" style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
            <div class="panel-body" >
              <div class="col-xs-1">
                <span class="fa fa-check check-create"></span>
              </div>
              <div class="col-xs-8">
                <h4 style="text-decoration: line-through;">Create your account</h4>
              </div>
            </div>
           </div> -->

           <!-- supplier -->
           <!-- <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
            <div class="panel-body" >
              <div class="col-xs-1">
                <span class="fa fa-check check-supplier"></span>
              </div>
              <div class="col-xs-8">
                <h4 class="supplier-title">Add a supplier</h4>
                <p>Add your first supplier into oboloo to get you started</p>
              </div>
              <div class="col-xs-3 text-center">
                <button type="button" class="btn-profile supplier"  data-toggle="modal" data-target="#supplier_modal">
                  Add Supplier
                </button>
              </div>
            </div>
           </div> -->
            <!-- // profile panel -->
           <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
            <div class="panel-body" >
              <div class="col-xs-1">
                <span class="fa fa-check check-profile"></span>
              </div>
              <div class="col-xs-8">
                <h4 class="line-through">Your profile</h4>
                <p>Please confirm your details and choose your preferred language</p>
              </div>
              <div class="col-xs-3 text-center">
                <button type="button" class="btn-profile  profile"  data-toggle="modal" data-target="#profile_modal">
                  Complete Profile
                </button>
              </div>
            </div>
           </div>

            <!-- // profileCompany Settings panel -->
           <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
            <div class="panel-body" >
              <div class="col-xs-1">
                <span class="fa fa-check check-company"></span>
              </div>
              <div class="col-xs-8">
                <h4 class="company-title">Company settings</h4>
                <p>Add your company name, base currency & date format</p>
              </div>
              <div class="col-xs-3 text-center">
                <button type="button" class="btn-profile company"  data-toggle="modal" data-target="#company_modal">Complete Company</button>    
              </div>
            </div>
           </div>
          

            <!-- Add Locations -->
          <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
            <div class="panel-body" >
              <div class="col-xs-1">
                <span class="fa fa-check check-location"></span>
              </div>
              <div class="col-xs-8">
                <h4 class="location-title">Add a location</h4>
                <p>Add a company Location to get you started. You can add more Locations in Configurations after set up</p>
              </div>
              <div class="col-xs-3 text-center">

                 <button type="button" class="btn-profile  locations"  data-toggle="modal" data-target="#location_modal">
                  Add Location
                </button>
              </div>
            </div>
           </div>

           <!-- Add department -->
            <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
            <div class="panel-body" >
              <div class="col-xs-1">
                <span class="fa fa-check check-department"></span>
              </div>
              <div class="col-xs-8">
                <h4 class="department-title">Add a department</h4>
                <p>Add a company Department to get you started. You can add more Departments in Configurations after set up</p>
              </div>
              <div class="col-xs-3 text-center">
                <button type="button" class="btn-profile department"  data-toggle="modal" data-target="#department_modal">
                  Add Department
                </button>
              </div>
            </div>
           </div>

           
            <!-- Add currency -->
           <!--  <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
            <div class="panel-body" >
              <div class="col-xs-1">
                <span class="fa fa-check check-supplier"></span>
              </div>
              <div class="col-xs-8">
                <h4 class="supplier-title">Add supplier</h4>
                <p>Here you can add additional currencies your organisation uses that are different from your base currency</p>
              </div>
              <div class="col-xs-3 pull-right">
                <button type="button" class="btn-profile pull-right supplier"  data-toggle="modal" data-target="#supplier_modal">
                  Create Supplier
                </button>
              </div>
            </div>
           </div> -->
           <!--  <div class="text-center">
              <p>click <a href="<?php //echo AppUrl::bicesUrl('home'); ?>" style="color: blue; font-size: 14px; margin-top: 10; margin-bottom: 10px;">here</a> to skip setup and go to your dashborad</p>
            </div> -->


          </div>
         </div>    
        </div>   
       </div>
      </div>
     </div>
    </div>  

    <!-- //////////////////////////////////////////////// 
                     profile  model
        ///////////////////////////////////////////////////-->
  <div id="profile_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Profile Setup</h4>
      </div>
      <div class="modal-body">
       <form id="profile_form" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask" method="post" action="">
        <div class="form-group">
          <div class="col-md-12 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Full Name <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="full_name" id="full_name"
                    <?php if (isset($user['full_name']) && !empty($user['full_name'])) echo 'value="' . $user['full_name'] . '"'; else echo 'placeholder="Full Name"'; ?> >
          </div>
      </div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Email Address <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="email" id="email"
                      <?php if (isset($user['email']) && !empty($user['email'])) echo 'value="' . $email_value . '"'; else echo 'placeholder="Email Address"'; ?> >
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Logon Username <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="username" id="username"
                      <?php if (isset($user['username'])) echo 'value="' . $username_value . '"';?> readonly  >
           
          </div>
      </div>

     <div class="form-group">
      <!-- <div class="col-md-6 col-sm-6 col-xs-12 date-input">
        <label class="control-label">Position</label>
        <input type="text" class="form-control notranslate" name="position" id="position"
                    <?php if (isset($user['position']) && !empty($user['position'])) echo 'value="' . $user['position'] . '"'; else echo 'placeholder="Position"'; ?> >
      </div> -->
        <div class="col-md-6 col-sm-6 col-xs-12 date-input">
        <label class="control-label">Contact Number</label>
        <input type="text" class="form-control notranslate" name="contact_number" id="contact_number"
          <?php if (isset($user['contact_number']) && !empty($user['contact_number'])) echo 'value="' . $user['contact_number'] . '"'; else echo 'placeholder="Contact Number"'; ?> >
        </div>
       
        
        <div class="col-md-6 col-sm-6 col-xs-12 date-input">
            <label class="control-label">Select Language</label>

            <?php $languages = array(
                           ''=>'Select Language',
                          'en'=>'English',
                          'ar'=>'Arabicلعربية',
                          'zh-CN'=>'简体中文',
                          'zh-TW'=>'中國傳統的',
                          'nl'=>'Nederlands',
                          'fr'=>'Français',
                          'de'=>'Deutsche',
                          'hi'=>'हिन्दी',
                          'it'=>'Italiana',
                          'ja'=>'日本人',
                          'ko'=>'한국어',
                          'pl'=>'Polskie',
                          'pt'=>'Português',
                          'ru'=>'русский',
                          'es'=>'Español',
                          'th'=>'ไทย',
            )?>
            <select name="language_code" id="language_code" class="form-control select2_multiple notranslate" required="required">
                  <?php foreach ($languages as $code=>$language) { ?>
                      <option value="<?php echo $code; ?>" <?php echo  !empty($user['language_code']) && $user['language_code']==$code?"selected='selected'":""?>>
                          <?php echo $language; ?>
                      </option>
                  <?php } ?>
              </select>
         
        </div>

       <!--   <div class="col-md-3 col-sm-3 col-xs-6">
            <label class="control-label">Profile Picture</label>
            <input type="file" name="profile_img"  class="notranslate" style="border: none" id="profile_img"><br />
            <a href="<?php echo $this->createUrl('app/deleteProfileImg');?>" style="text-decoration: underline;color: #3b4fb4;" onclick="return confirm('Are you sure want to delete profile picture');">Delete Profile Picture</a>
            <div class="clearfix"></div>
          </div> -->

      </div>

    <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
    <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
    <input type="submit" name="" class="btn btn-success submit-btn1 pull-right" value="Save Profile" />
    
    </div>
    </div>

  </form>
      </div>
    </div>
  </div>
</div>
    <!-- /////////////////////////////
            end profile model 
      ///////////////////////////////////-->
<div class="clearfix"></div>

   <!-- /////////////////////////////////
         company  model
         ////////////////////////////////-->
<div class="row">
       
 <div id="company_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Company Setup</h4>
      </div>
      <div class="modal-body">
        <form id="company_form" enctype="multipart/form-data" class="form-horizontal input_mask" method="post" action="<?php echo AppUrl::bicesUrl('app/companyDetails'); ?>">
        <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12 required-cont">
          <label class="control-label">Company Name <span style="color: #a94442;">*</span></label>
           <input type="text" class="form-control notranslate" name="company_name" id="company_name" required>
        </div>
       <div class="clearfix"></div><br />
          <div class="col-md-6 col-sm-6 col-xs-12 required-cont">
              <label class="control-label">Select Base Currency <span style="color: #a94442;">*</span></label>
              <select name="currency_id" id="currency_id" class="form-control notranslate" required="required">
              <option value="">Select Currency</option>
                <?php foreach ($currency_rate as $rate) { ?>
                <option value="<?php echo $rate['currency']; ?>"
                  <?php if (isset($company_details['currency_id']) && $rate['currency'] == $company_details['currency_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $rate['currency']; ?>
                </option>
              <?php } ?>
            </select>
          </div> 
          <div class="col-md-6 col-sm-6 col-xs-12 required-cont">
              <label class="control-label">Select Date Format <span style="color: #a94442;">*</span></label>
              <?php $dformat = array(
                            ''=>'Select Date Format',
                            'DD/MM/YYYY'=>'DD/MM/YYYY',
                            'MM/DD/YYYY'=>'MM/DD/YYYY',
                            )?>
              <select name="default_date_format" id="default_date_format" class="form-control select2_multiple notranslate" required>
                    <?php foreach ($dformat as $key=>$value) { ?>
                        <option value="<?php echo $key; ?>" >
                            <?php echo $value; ?>
                        </option>
                    <?php } ?>
                </select>
           
          </div>
         </div>
          <div class="clearfix"></div>
          <input type="hidden" name="default_time_format" value="h" />

      <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-12">
    <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />

    

    <input type="submit" name="" class="btn btn-success submit-btn1 pull-right" value="Save Company" />

    </div>
    </div>

  </form>
      </div>
    </div>
  </div>
</div>
    
 </div>
    <!-- end company model -->


   <!-- /////////////////////////////////
         Locaton  model
         ////////////////////////////////-->
<div id="location_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Locations Setup </h4>
      </div>
      <div class="modal-body">

         <form id="location_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('locations/edit'); ?>">
        <div class="col-md-12 col-sm-12 col-xs-12 date-input">
          <div class="form-group">
            <label class="control-label">Location Name <span style="color: #a94442;">*</span></label>
            <input type="text" class="form-control  notranslate" name="location_name" id="location_name" >
          </div>
      </div>

      <div class="form-group">
          <?php $countries = FunctionManager::countryList();?>
          <div class="col-md-12 col-sm-12 col-xs-12 date-input">
              <label class="control-label">Country <span style="color: #a94442;">*</span></label>
             
              <select class="form-control select_vendor_multiple notranslate" name="loc_country" id="loc_country">
                 <option value="">Select</option>
                <?php foreach($countries as $key => $value) {?>
               <option value="<?php echo $value;?>"><?php echo $value;?></option>
             <?php } ?>
              </select>
             <!--  <input type="text"  > -->
          </div>
      </div>
      
    <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
    <input type="hidden" name="location_id" class="notranslate" id="location_id" value="<?php if (isset($location_id)) echo $location_id; ?>" />
    <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />

    <input type="submit" name="" class="btn btn-success submit-btn1 pull-right" value="Save Location" />
    </div>
    </div>

  </form>
      </div>
    </div>
  </div>
</div>


<!-- /////////////////////////////////
         department  model 
      ////////////////////////////////-->
 <div id="department_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Department Setup </h4>
      </div>
      <div class="modal-body">
        <form action="" id="department_form">
      <div class="modal-body">
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-6 valid" style="padding-top: 8px;">
                <label class="control-label">Department <sup style="color: #ff0000;">*</sup></label>
                <input type="text" class="form-control department_name notranslate" name="department_name" id="department_name" placeholder="Department Name" value="" required="required" />
            </div>
        </div>

           <div class="col-md-6 col-sm-6 col-xs-6">
                <label class="control-label">Location <sup style="color: #ff0000;">*</sup></label>
                <select multiple="multiple" name="locations[]" id="locations" class="form-control select2_multiple notranslate load-location" required="required" >
                    <?php foreach ($locations as $location) { ?>
                        <option value="<?php echo $location['location_id']; ?>"  <?php if(!empty($_GET['location_id']) && $location['location_id']==$_GET['location_id']){ echo 'selected="selected"';}?>>
                            <?php echo $location['location_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="clearfix"> </div>
       
        
      </div>
      <div class="modal-footer">
        <input type="hidden" name="department_id" id="department_id" value="0" />
         <input type="submit" name="" class="btn btn-success submit-btn1" value="Save Department" />
      </div>
  </form>
      </div>
    </div>
  </div>
</div>
 

</div></div></div>
<!-- /////////////////////////////////
         currency  model 
      ////////////////////////////////-->



  <div id="supplier_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    // Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Supplier Setup </h4>
      </div>
      <div class="modal-body">
         <form id="vendor_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="">
     <div class="tab"> 
         <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                  <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate" name="vendor_name" id="vendor_name"
                        required="required"  >
            </div>
              </div>
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                  <label class="control-label">Contact Name <span style="color: #a94442;">*</span></label>
                  <input type="text" class="form-control contact_name notranslate" name="contact_name" id="contact_name"  aria-invalid="false" required>
              </div>
          </div>
         <!--   <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
            <label class="control-label">Company Assigned Supplier ID</label>
            <input type="text" class="form-control notranslate" name="external_id" id="external_id" placeholder="Company Assigned Supplier ID" >
          </div></div> -->
          

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                <label class="control-label">Email Address <span style="color: #a94442;">*</span> - Please check that this is correct for supplier correspondance  </label>
                <input type="text" class="form-control notranslate" name="emails" id="emails" required="required" placeholder="Email Address" >
            </div>
        </div>

         <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 date-input">
                <label class="control-label ">Confirm Email Address <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control emails_error notranslate" name="confm_emails" id="confm_emails" required="required" placeholder="Email Address" >
                <span class="emails_error" style="display: none;">Email Address and Confirm Email Address are not matching.</span>
            </div>

        </div>

          <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <label class="control-label">Supplier Industry <span style="color: #a94442;">*</span> - These are industries within oboloo used to categorise your suppliers. These can be changed later under configurations </label>
        <select name="industry_id" id="industry_id" class="form-control notranslate" onchange="loadSubindustries(this);" required="required">
          <option value="">Select Supplier Industry</option>
          <?php foreach ($industries as $industry) { ?>
            <option value="<?php echo $industry['id']; ?>">
              <?php echo $industry['value']; ?>
            </option>
          <?php } ?>
        </select>
              </div>
          </div>

        <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
          <label class="control-label">Supplier Sub Industry <span id="requiredIndi" style="color: #a94442;">*</span></label>
          <select name="subindustry_id" id="subindustry_id_modal" class="form-control notranslate" required="required">
          <option value="0">Select Supplier Sub Industry</option>
          </select>
         </div>
       </div>
       <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="checkbox">
                      <label><input type="checkbox" class="flat notranslate" name="preferred_flag" id="preferred_flag" value="1"></label>
                      <label style="padding: 0;margin-top: 10px;display: inline-block;"><div class="clearfix"></div><br />Check this box to indicate that this is a preferred Supplier</label>
                  </div>
                  <!--  <div class="checkbox">
                       <label  class="pull-left"><input type="checkbox" class="flat notranslate" name="active" id="active" value="1"></label>
                      <label style="padding: 5px;float: left;width: 95%;">Are you sure you want to activate this supplier?</label>

                  </div> -->
              </div>
          </div>
     </div>
     <div class="alert alert-success vendor_alert"><strong>Success!</strong> Supplier submitted successfully.</div>
       <div class="col-md-12 col-sm-12 col-xs-12 text-right"><br />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" style="background-color: #1abb9c !important; border-color: #1abb9c !important;" id="add_vendor_btn"  class="btn btn-primary submit-btn notranslate" value="Add Supplier">
        <div class="clearfix"></div>
    </form>
      </div>
      </div>
    </div>
  </div>
<div class="modal fade" id="vendor_addtional_field" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="
    position: relative;
    top: 50%;
    transform: translateY(-50%);
">
    <div class="modal-content" style="max-width: 538px; margin: auto;">
      <!-- <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <p>Would you like to complete the additional fields for this supplier?</p>
      </div>
      <div class="modal-footer">
        <a href="#" id="vendor_detail_link" class="btn btn-primary">Yes</a>
        <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>



<style type="text/css">
body{
 background: #ebecf6;
}
</style>

<?php if(!empty($_GET['location_id'])){?>
  <script type="text/javascript">
    $(document).ready(function() {
        $('.adddepartment').trigger('click');
        $('#locations').val('<?php echo $_GET['location_id'];?>').trigger('change');
    });
</script>
<?php } ?>


<script type="text/javascript">

function checkuserSetup(){
  var appSetup = 0;
   $.ajax({
        url: '<?php echo $this->createUrl('setup/checkupSetup'); ?>',
        type: 'POST',
        data: {},
        dataType: "json",
        success: function (data) {
           var appSetup = 0;

          if(data.profile==1){
            $(".check-profile").css('color','#1abb9c');
            // $(".line-through").css('text-decoration', 'line-through');
              $(".profile").attr("disabled", true);
            $(".profile").css('cursor', 'not-allowed');
            $("#profile_modal").modal('hide');
            appSetup=appSetup+1;
          }

          if(data.company==1){
           
            $(".check-company").css('color','#1abb9c');
            // $(".company-title").css('text-decoration', 'line-through');
            $(".company").attr("disabled", true);
            $(".company").css('cursor', 'not-allowed');
             appSetup=appSetup+1;
          }
          if(data.location==1){
            $("#location_modal").modal('hide');
            $(".check-location").css('color','#1abb9c');
            // $(".location-title").css('text-decoration', 'line-through');
            $(".locations").attr("disabled", true);
            $(".locations").css('cursor', 'not-allowed');
             appSetup=appSetup+1;
          }

          if(data.department==1){

            $("#department_modal").hide();
            $(".check-department").css('color','#1abb9c');
            $(".modal-backdrop").hide();
            // $(".department-title").css('text-decoration', 'line-through');
            $(".department").attr("disabled", true);
            $(".department").css('cursor', 'not-allowed');
             appSetup=appSetup+1;

          }

         
           

         if(appSetup>=4){
          window.location.href = "<?php echo $this->createUrl('app/SetupUser');?>";
         }
         for(i=1; i<=appSetup; i++){
          $(".progress-bar").css('width',i*25+"%");
          $(".progress-bar-number").html(i*25+"%");
         }
        },
        cache: false,
        contentType: false,
        processData: false
    });

   
}
function loadLocation()
{ 
  $.ajax({
      type: "POST", data: { }, dataType: "json",
      url: BICES.Options.baseurl + '/setup/getLocations',
      success: function(options) {
        $(".load-location").html(options.location);
      },
      error: function() { }
  });
}

$('document').ready(function() {
  checkuserSetup();
$(".select2_multiple").select2({
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"});

  $("#profile_form").submit(function(e) {
    e.preventDefault();    

    var cnv = $('#language_code').val();
    if (!$.trim(cnv)) {  

        return false;
    }
    var formData = new FormData(this);
    $.ajax({
        url: '<?php echo $this->createUrl('setup/profile'); ?>',
        type: 'POST',
        data: formData,
        dataType: "json",
        success: function (data) {
           checkuserSetup();
           location.reload(true);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$("#company_form").submit('.submit-btn1', function() {
   
    var cnv = $('#company_name').val();
    if (!$.trim(cnv)) {   
        return false;
    }

    var cnv = $('#currency_id').val();
    if (!$.trim(cnv)) {   
        return false;
    }

    var cnv = $('#default_date_format').val();
    if (!$.trim(cnv)) {   
        return false;
    }


    var formData = new FormData(this);
    $.ajax({
        url: '<?php echo $this->createUrl('setup/company'); ?>',
        type: 'POST',
        data: formData,
        dataType: "json",
        success: function (data) {
           checkuserSetup();
            $("#company_modal").modal('hide');
        },
        cache: false,
        contentType: false,
        processData: false
     });

     return false;
   });

  $("#location_form").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
        url: '<?php echo $this->createUrl('setup/location'); ?>',
        type: 'POST',
        data: formData,
        dataType: "json",
        success: function (data) {
           checkuserSetup();
           loadLocation();
        },
        cache: false,
        contentType: false,
        processData: false
     });  
   });

  $("#department_form").submit(function(e) {
    e.preventDefault();    
    var department_name = $.trim($('#department_name').val());
    var selected_locations = $('#locations').val();
     
    //var formData = new FormData(this);
    $.ajax({
        url: '<?php echo $this->createUrl('setup/deptartment'); ?>',
        type: 'POST',dataType: 'json',
          data: { 
              department_id: $('#department_id').val(), 
              department_name: department_name,
              locations: selected_locations,
            },
        success: function (data) {
           checkuserSetup();
        },
         
     });  
   });

   $("#currency_form").submit(function(e) {
    e.preventDefault();
     $('#loading_icon').show();
    currencyRate = parseFloat($('#new_rate').val());
    
    if(isNaN(currencyRate) || currencyRate<0.001){
      $(".ajax_success_modal").show();
      $(".ajax_success_modal").html("Currency Rate should be greater then 0");
      $(".ajax_success_modal").delay(1000*10).fadeOut();
           
    }else{  
     var formData = new FormData(this);
    $.ajax({
        url: '<?php echo $this->createUrl('setup/currency'); ?>',
        type: 'POST', async: false,
        data: formData,
        dataType: "json",
        success: function (data) {
           checkuserSetup();
        },
        cache: false,
        contentType: false,
        processData: false
     });  
    }
   });

    
  $("#country").change(function(){
       var country = $(this).val();
       var alreadyExisted = 0;
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('app/fillCurrencyField'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: {country:country},
            beforeSend: function() {
            $("#add_currency_name").prop('readonly',false);
            $("#new_currency").prop('readonly',false);
            $("#add_currency_symbol").prop('readonly',false);
            $("#new_rate").prop('readonly',false);
            $("#saveDepartmentButton").removeAttr("disabled");
            $(".ajax_success_modal").hide();
            $(".ajax_success_modal").html('');
            
            
           },
            success: function(data) {

                $("#add_currency_name").val(data.currency_name);
                if(data.currency_name !=''){
                   //alreadyExisted = 1;
                  $("#add_currency_name").prop('readonly',true);
                }

                $("#new_currency").val(data.currency_code);
                if(data.currency_code !=''){
                   //alreadyExisted = 1;
                  $("#new_currency").prop('readonly',true);
                 
                }

                $("#add_currency_symbol").val(data.currency_symbol);
                if(data.currency_symbol !=''){
                  // alreadyExisted = 1;
                  $("#add_currency_symbol").prop('readonly',true);
                 
                }

               $("#new_rate").val(data.rate);

                if(data.status =='0'){
                  alreadyExisted=1;
                  $("#new_rate").prop('readonly',true);
                }

                if(data.rate <=0){
                  alreadyExisted=1;
                  $("#new_rate").prop('readonly',false);
                }else if(alreadyExisted==1){
                $("#saveDepartmentButton").prop("disabled",true);
                msg = "Currency Already Exists";
                $(".ajax_success_modal").show();
                $(".ajax_success_modal").html(msg);
                $(".ajax_success_modal").delay(1000*10).fadeOut();  
              }
            }
        });
    });

  });

    function loadDepartmentsForSingleLocation(department_id)
    {
        var location_id = $('#location_id').val();
        var single = 1;

        if (location_id == 0)
            $('#department_id').html('<option value="">Select Department</option>');
        else
        {
            $.ajax({
                type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
                url: BICES.Options.baseurl + '/locations/getDepartments',
                success: function(options) {
                    var options_html = '<option value="">Select Department</option>';
                    for (var i=0; i<options.length; i++)
                        options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                    $('#department_id').html(options_html);
                    if (department_id != 0) $('#department_id').val(department_id);
                },
                error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
            });
        }
    }



  <?php if (isset($user['location_id']) && !empty($user['location_id'])) { ?>
  <?php if (isset($user['department_id']) && !empty($user['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $user['department_id']; ?>);
  <?php } else { ?>
    loadDepartmentsForSingleLocation(0);
  <?php } ?>

  <?php } ?>

  $(document).ready( function() {
    loadLocation();
     $("#locations").select2({placeholder: 'Select Locations',
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"});

   $(".select_vendor_multiple").select2({
      placeholder: "Select Country",
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
      //allowClear: true
    });
    $( "#company_form" ).validate( {
        rules: {
            company_name: "required",
            address_1: "required",
            currency_id: "required",
        },
        messages: {
            company_name: "Company Name is required",
            address_1:"Address 1 is required",
            currency_id:"currency is required",
        },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      element.parents( ".col-sm-6" ).addClass( "has-feedback" );
      element.parents( ".required-cont" ).addClass( "has-feedback" );
      

      if ( element.prop( "type" ) === "checkbox" )
        error.insertAfter( element.parent( "label" ) );
      else error.insertAfter( element );

      if ( !element.next( "span" )[ 0 ] )
        $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
    },
    success: function ( label, element ) {
      if ( !$( element ).next( "span" )[ 0 ] )
        $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".col-sm-6" ).addClass( "has-error" ).removeClass( "has-success" );
      $( element ).parents( ".required-cont" ).addClass( "has-error" ).removeClass( "has-success" );
      $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
    },
    unhighlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".col-sm-6" ).addClass( "has-success" ).removeClass( "has-error" );
      $( element ).parents( ".required-cont" ).addClass( "has-success" ).removeClass( "has-error" );
      $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
    }
    });
});


$("#requiredIndi").hide();
function loadSubindustries(obj)
{
  var industry_id = obj.value;
  if (industry_id==""){
    $('#subindustry_id_modal').html('<option value="">Select Supplier Sub Industry</option>');
    $('#subindustry_id_modal').removeAttr("required");
    $("#requiredIndi").hide();
  }else
  {
      $.ajax({
          type: "POST", data: { industry_id: industry_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('setup/getSubIndustries'); ?>",
          success: function(options) {
            var options_html = '<option value="">Select Supplier Sub Industry</option>';

            if(options.suggestions.length){
              $('#subindustry_id_modal'). attr("required", "required");
              $("#requiredIndi").show();
            }else{
               $('#subindustry_id_modal').removeAttr("required");
               $("#requiredIndi").hide();
            }

            for (var i=0; i<options.suggestions.length; i++){
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            }
          $("#subindustry_id_modal").html(options_html);      
          },
        
      });
  }
}

$(".vendor_alert").hide();

 $("#vendor_form").submit(function(e) {
    e.preventDefault();  
    var email = $("#emails").val();
    var confm_emails = $("#confm_emails").val();
    if(email != confm_emails) {
      $(".emails_error").css('color', 'red').show();
       return false;
    }
  $(".vendor_alert").show(); 

    $.ajax({
        url: '<?php echo $this->createUrl('setup/vendor'); ?>',
        type: 'POST', async: false, dataType: 'json',
        data: $("#vendor_form").serialize(),
        success: function (data) {
           checkuserSetup();
             // $(".vendor_alert").html(info.alert).show().delay(50000).fadeOut();
            if(info.dupplicate == 0){
              $('#vendor_form').trigger("reset");
            
          }
             
        }
      
     });  
   
   });

 $(document).ready(function(){

 });

</script>

<style>
  .container_stetup{    width: 960px; margin: auto;}
  .panel-primary {border-color: #357ae8;background: #357ae8;}

  .check{color: #fff;
    background-color: #ece4e4;
    padding: 8px;
    border-radius: 16px;
    margin-top: 6px;
  }
  .check-create{color: green;
    font-size: 17px;
  }
  .check-profile,.check-company,.check-location,.check-department,.check-currency, .check-supplier{
    font-size: 17px; color: #ece4e4;margin-top: 14px;
  }
  .btn-profile{  
    border-color: #1abb9c;
    background: #1abb9c;
    color: #fff;
    border: 0px;
    padding: 8px 25px 8px 25px;
    font-size: 15px;
    font-weight: 500;
    margin-top: 5px;
    border-radius: 30px;
  
    }
  .btn-profile:hover{
   border-color: #1abb9c;
    background: #1abb9c;
    color: #fff;
    border: 0px;
    border-radius: 30px;
    font-size: 15px;
    font-weight: 500;
}
.profile{
   padding: 8px 24px 8px 24px;
}
.company{
  padding: 8px 8px 8px 10px;
}

.locations{
     padding: 8px 34px 8px 34px;
}
.department{
    padding: 8px 23px 8px 23px;
}
.panel-default{border-color: #ebecf6; border-radius: 10px; box-shadow: 5px 15px 5px rgba(80,102,224,0.08);}
</style>