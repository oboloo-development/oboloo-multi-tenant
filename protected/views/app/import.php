<?php unset(Yii::app()->session['contract_import']);?>
 <?php $disabled = FunctionManager::sandbox(); ?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Import Data</h3><br />
            <h4 class="subheading">You can upload your data in bulk via one of the templates below. <b>Please make sure all data is correct before you upload as this cannot be reversed</b></h4>
        </div>
        <div class="clearfix"> </div>
    </div>

	<form name="template_form" id="template_form" class="horizontal-form" role="form" 
			method="post" enctype="multipart/form-data" action="<?php echo AppUrl::bicesUrl('app/import'); ?>">
	
		<div class="form-group">
			<div class="col-md-3 col-xs-12">
				<select name="template_type" id="template_type" class="form-control notranslate" required>
					<option value="">Select Template To Upload</option>
					<option value="Vendor"  <?php echo isset($_GET['type']) && $_GET['type']=='vendor'?'selected':''; ?>>Supplier Import Template</option>
					<!-- <option value="Product">Product Import Template</option> -->
					<!-- <option value="Location">Location Import Template</option> -->
					<!-- <option value="Order">Order Import Template</option>
					<option value="Expense">Expense Import Template</option> -->
                    <option value="Contract" <?php echo isset($_GET['type']) && $_GET['type']=='contract'?'selected':''; ?>>Contract Import Template</option>
				</select>
			</div>
			<div class="col-md-6 col-xs-12">
				<input type="file" class="form-control notranslate" name="template_file" id="template_file" accept=".csv" onchange="checkextension()" />
			</div>

			<div class="col-md-2">
				<input type="submit" class="btn btn-primary"  <?php echo $disabled; ?> id="form_sumit_btn" value="Upload Template"/>
			</div>

		</div>
	
		<input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
	
	</form>

	<div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3><br /><br />Templates</h3><br />
            <h4 class="subheading">Download and populate one of the below templates and upload it above. <strong>Only 100 records can be uploaded at one time.</strong><br /><br />
            <strong>Note: try uploading only one record on each of the templates first to make sure that you have formatted your data correctly</strong>
            </h4>
        </div>
        <div class="clearfix"> </div>
        <?php 

        $mesgalert=Yii::app()->user->getFlash('success');

        if(!empty($mesgalert)) { ?><br /><br />
			<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $mesgalert;?>
			</div>
			<?php }else if(Yii::app()->user->hasFlash('error')) {
			?><br /><br /><div class="alert alert-danger" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo Yii::app()->user->getFlash('error');?>
			</div>

			<?php };?> 
    </div>
    <br /><br />

	<?php
		$import_templates = array();
	    if ($handle = opendir('uploads/imports')) 
			while (($uploaded_template = readdir($handle)) !== false)
				if (!in_array($uploaded_template, array('.', '..','history'))) 
			    	$import_templates[] = $uploaded_template;
	?>

	<div class="tile_count">
		<?php if (count($import_templates) <= 0) echo '<h4>No templates found</h4>'; else { ?>
			<?php $i = 0; foreach ($import_templates as $a_template) {
				 list($template_name, $template_file) = explode("~", $a_template); ?>
				<div class="col-md-4"> <br /><br />
					<a href="<?php echo AppUrl::bicesUrl('uploads/imports/' . $a_template); ?>">
						<div class="text-center">
						<img src="<?php echo AppUrl::bicesUrl('images/excel.png'); ?>" width="100"  class="center"/>
						<br /><br />
						<h4><strong><?php echo $template_name; ?></strong></h4>
						<?php if($template_name == 'Contract'){
						echo '<p style="color: red; text-align:center">Please use only DD/MM/YYYY as the date format in the template otherwise records will not load correctly.</p>';
						} ?>
				
				        <?php if (isset(Yii::app()->session['admin_flag']) && Yii::app()->session['admin_flag'] == 1) { ?>

				        	<!-- <a style="cursor: pointer; text-decoration: underline;" 
				        			onclick="confirmDeleteTemplate('<?php echo $template_name; ?>');">
				        		Delete
				        	</a> -->
				        <?php } ?>
				        </div>
					</a>					
				</div>
			<?php $i += 1; if ($i % 3 == 0) echo '<div class="clearfix"><br /><br /></div>'; } ?>
		<?php } ?>
	</div>

    <br /><br />

</div>



<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Delete Import Template Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this <span id="delete_template_name" style="font-weight: bold;"></span> import template?</p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          	  NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          	  YES
          </button>
        </div>
      </div>
	</div>
</div>

</div>
<script type="text/javascript">
	function checkextension() {
  		var file = document.querySelector("#template_file");
 		 if ( /\.(csv)$/i.test(file.files[0].name) === false ) { alert("Please, select a CSV file, file other then CSV is not acceptal");
 		 	$("#form_sumit_btn").prop("disabled","disabled");
 		  }else{
 		  	$("#form_sumit_btn").removeAttr("disabled");
 		  }
		}

	function confirmDeleteTemplate(template_name)
	{
		$('#delete_template_name').text(template_name);
		$('#delete_confirm_modal').modal('show');
	}
	
	$(document).ready(function() {
		$('#delete_confirm_modal .modal-footer button').on('click', function(event) {
		  var button = event.target; // The clicked button
		  var template_name = $('#delete_template_name').text();
		
		  $(this).closest('.modal').one('hidden.bs.modal', function() {
		      if (button.id == 'yes_delete')
		      	location = '<?php echo AppUrl::bicesUrl('app/deleteTemplate/?template_type='); ?>' + template_name;
	      });
	   });		
	});
</script>

<style type="text/css">
	#form_sumit_btn{background-color: #1abb9c!important; border-color: #1abb9c!important; }
	.center { display: block; margin-left: auto; margin-right: auto; width: 30%;}
</style>