<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Import Templates</h3>
        </div>
        <div class="clearfix"> </div>
        <?php 

        $mesgalert=Yii::app()->user->getFlash('success');

        if(!empty($mesgalert)) { ?><br /><br />
			<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $mesgalert;?>
			</div>
			<?php }else if(Yii::app()->user->hasFlash('error')) {
			?><br /><br /><div class="alert alert-danger" >
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo Yii::app()->user->getFlash('error');?>
			</div>

			<?php };?> 
    </div>
    <br /><br />

	<?php
		$import_templates = array();
	    if ($handle = opendir('uploads/imports')) 
			while (($uploaded_template = readdir($handle)) !== false)
				if (!in_array($uploaded_template, array('.', '..','history'))) 
			    	$import_templates[] = $uploaded_template;
	?>

	<div class="tile_count">
		<?php if (count($import_templates) <= 0) echo '<h4>No templates found</h4>'; else { ?>
			<?php $i = 0; foreach ($import_templates as $a_template) { list($template_name, $template_file) = explode("~", $a_template); ?>
				<div class="col-md-4" style="text-align: center;"> <br /><br />
					<a href="<?php echo AppUrl::bicesUrl('uploads/imports/' . $a_template); ?>">
						<img src="<?php echo AppUrl::bicesUrl('images/excel.png'); ?>" width="100" />
						<br /><br />
						<h4><?php echo $template_name; ?></h4>
				        <?php if (isset(Yii::app()->session['admin_flag']) && Yii::app()->session['admin_flag'] == 1) { ?>
				        	<!-- <a style="cursor: pointer; text-decoration: underline;" 
				        			onclick="confirmDeleteTemplate('<?php echo $template_name; ?>');">
				        		Delete
				        	</a> -->
				        <?php } ?>
					</a>					
				</div>
			<?php $i += 1; if ($i % 3 == 0) echo '<div class="clearfix"><br /><br /></div>'; } ?>
		<?php } ?>
	</div>

    <br /><br />

</div>



<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Delete Import Template Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to delete this <span id="delete_template_name" style="font-weight: bold;"></span> import template?</p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          	  NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          	  YES
          </button>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
	function confirmDeleteTemplate(template_name)
	{
		$('#delete_template_name').text(template_name);
		$('#delete_confirm_modal').modal('show');
	}
	
	$(document).ready(function() {
		$('#delete_confirm_modal .modal-footer button').on('click', function(event) {
		  var button = event.target; // The clicked button
		  var template_name = $('#delete_template_name').text();
		
		  $(this).closest('.modal').one('hidden.bs.modal', function() {
		      if (button.id == 'yes_delete')
		      	location = '<?php echo AppUrl::bicesUrl('app/deleteTemplate/?template_type='); ?>' + template_name;
	      });
	   });		
	});
</script>
