<?php $disabled = FunctionManager::sandbox();
      $tollTip  = MessageManager::tooltipText();
?>
<div class="right_col" role="main">
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>
            	Edit Company Details
            </h3>
        </div>
        <div class="clearfix"> </div>
    </div>
<div class="row tile_count">
 <div class="clearfix"></div>
  <div class="clearfix"> <br/></div>
  <form id="company_form" enctype="multipart/form-data" class="form-horizontal input_mask" method="post" action="<?php echo AppUrl::bicesUrl('app/companyDetails'); ?>">
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">Company Name <span style="color: #a94442;">*</span></label>
      </div><div class="clearfix"></div>
          <div class="col-md-6 col-sm-6 col-xs-12 valid">
            
              <input type="text" class="form-control notranslate" name="company_name" id="company_name"
              <?php if (isset($company_details['company_name']) && !empty($company_details['company_name'])) echo 'value="' . $company_details['company_name'] . '"'; else echo 'placeholder="Company Name"'; ?> required>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Company Number <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="company_number" id="company_number"
              <?php if (isset($company_details['company_number']) && !empty($company_details['company_number'])) echo 'value="' . $company_details['company_number'] . '"'; else echo 'placeholder="Company Registration Number"'; ?>>
          </div>
          
          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Phone Number <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="phone_number" id="phone_number"
                  <?php if (isset($company_details['phone_number']) && !empty($company_details['phone_number'])) echo 'value="' . $company_details['phone_number'] . '"'; else echo 'placeholder="Company Phone Number"'; ?> >
          </div>
          </div>
          <!-- <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Company Fax</label>
              <input type="text" class="form-control" name="tax_number" id="tax_number"
              <?php if (isset($company_details['tax_number']) && !empty($company_details['tax_number'])) echo 'value="' . $company_details['tax_number'] . '"'; else echo 'placeholder="Company Tax ID Number"'; ?> >
          </div> -->

          <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input ">
              <label class="control-label">Address Line 1 <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="address_1" id="address_1"
                    <?php if (isset($company_details['address_1']) && !empty($company_details['address_1'])) echo 'value="' . $company_details['address_1'] . '"'; else echo 'placeholder="Address Line 1"'; ?> required>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 date-input ">
              <label class="control-label">Address Line 2 <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="address_2" id="address_2"
                    <?php if (isset($company_details['address_2']) && !empty($company_details['address_2'])) echo 'value="' . $company_details['address_2'] . '"'; else echo 'placeholder="Address Line 2"'; ?> required>
          </div>
      </div>

       <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input ">
              <label class="control-label">City <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="city" id="loc_city"
                    <?php if (isset($company_details['city']) && !empty($company_details['city'])) echo 'value="' . $company_details['city'] . '"'; else echo 'placeholder="City"'; ?> required>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">State/County <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="state" id="loc_state"
                    <?php if (isset($company_details['state']) && !empty($company_details['state'])) echo 'value="' . $company_details['state'] . '"'; else echo 'placeholder="State/County"'; ?> required>
          </div>
      </div>

      <div class="form-group">
          <?php $countries = FunctionManager::countryList();?>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Country <span style="color: #a94442;">*</span></label> 
              <select class="form-control select_vendor_multiple notranslate" name="country" id="loc_country" >
                 <option value="">Select</option>
                <?php foreach($countries as $key => $value) {?>
               <option value="<?php echo $value;?>" <?php if(!empty($company_details['country']) && $company_details['country']==$value) { echo "selected='selected'";}?>><?php echo $value;?></option>
             <?php } ?>
              </select>
             <span id="country_err" style="font-style: italic;">Country Name is required</span>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input ">
              <label class="control-label">Zip/Postal Code <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="zipcode" id="loc_zip_code"
                    <?php if (isset($company_details['zipcode']) && !empty($company_details['zipcode'])) echo 'value="' . $company_details['zipcode'] . '"'; else echo 'placeholder="Zip/Postal Code"'; ?> required>
          </div>
      </div>


   
        
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 ">
              <label class="control-label">Company Website <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control notranslate" name="company_website" id="company_website"
                  <?php if (isset($company_details['company_website']) && !empty($company_details['company_website'])) echo 'value="' . $company_details['company_website'] . '"'; else echo 'placeholder="Company Website"'; ?> required>
          </div>
      </div>

       <div class="form-group">
       <!--    <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Default Language</label>

              <?php $languages = array(
                            ''=>'Select Language',
                            'en'=>'English',
                            'ar'=>'Arabic',
                            'zh-CN'=>'Chinese (Simplified)',
                            'zh-TW'=>'Chinese (Traditional)',
                            'nl'=>'Dutch',
                            'fr'=>'French',
                            'de'=>'German',
                            'hi'=>'Hindi',
                            'it'=>'Italian',
                            'ja'=>'Japanese',
                            'pl'=>'Polish',
                            'pt'=>'Portuguese',
                            'ru'=>'Russian',
                            'es'=>'Spanish',
              )?>
              <select name="default_language_code" id="default_language_code" class="form-control select2_multiple">
                    <?php /* foreach ($languages as $code=>$language) { ?>
                        <option value="<?php echo $code; ?>" <?php echo $company_details['default_language_code']==$code?"selected='selected'":""?>>
                            <?php echo $language; ?>
                        </option>
                    <?php } */ ?>
                </select>
           
          </div> -->
        
          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label base_currency" >Select Base Currency <span style="color: #a94442;">*</span></label>
              <select name="currency_id" id="currency_id" class="form-control notranslate" required>
              <option value="">Select Currency</option>
              <?php foreach ($currency_rate as $rate) { ?>
                <option value="<?php echo $rate['currency']; ?>"
                  <?php if (isset($company_details['currency_id']) && $rate['currency'] == $company_details['currency_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $rate['currency']; ?>
                </option>
              <?php } ?>
            </select>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 ">
            
              <label class="control-label" >Select Date Format <span style="color: #a94442;">*</span></label>
              <?php $dformat = array(
                            ''=>'Select Date Format',
                            'DD/MM/YYYY'=>'DD/MM/YYYY',
                            'MM/DD/YYYY'=>'MM/DD/YYYY',
                            )?>
              <select name="default_date_format" id="default_date_format" class="form-control select2_multiple notranslate" required>
                    <?php foreach ($dformat as $key=>$value) { ?>
                        <option value="<?php echo $key; ?>" <?php echo isset($company_details['default_date_format']) && $company_details['default_date_format']==$key?"selected='selected'":""?>>
                            <?php echo $value; ?>
                        </option>
                    <?php } ?>
                </select>
           
          </div></div>
          <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-12 valid" style="margin-top:20px;">
              <?php /* $companyLogo = FunctionManager::dishboardLogo();
              <!-- <img  class="companyLogo"  src="<?php // echo AppUrl::bicesUrl($companyLogo); ?>" /> -->  */ ?>
              <label class="control-label">Company Logo</label>
              <input width="100" height="50" type="file"  name="company_logo" id="company_logo" class="company_logo form-control"/>
            </div>
          </div>
          
          <!-- <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Default Time Format</label>
              <?php $tformat = array(
                            ''=>'Select Time Format',
                            'h'=>'1 through 12',
                            'H'=>'00 through 23')?>
              <select name="default_time_format" id="default_time_format" class="form-control select2_multiple">
                    <?php /*foreach ($tformat as $tcode=>$value) { ?>
                        <option value="<?php echo $tcode; ?>" <?php echo $company_details['default_time_format']==$tcode?"selected='selected'":""?>>
                            <?php echo $value; ?>
                        </option>
                    <?php }*/ ?>
                </select>
           
          </div> -->
          <div class="clearfix"></div>
          <input type="hidden" name="default_time_format" value="h" />

			

      <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
  <div class="clearfix"></div>
    <a href="javascript:void(0);"  onclick="$('#company_form').submit(); return false;" <?php echo $disabled; ?>>
        <button type="button" class="btn btn-primary submit-btn1" <?php echo $disabled; ?>>
            Save Company
        </button>
    </a>
    </div>
    </div>

  </form>
</div>
</div>

  <script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript">
$(document).ready( function() {
 $("#country_err").hide();

 $("#company_form").on('submit', function(){
    var loc_country = $('#loc_country').val();
    if (loc_country.length == 0 ) {
        $("#country_err").show();
        return false;
    }

 });
   
   $(".select_vendor_multiple").select2({
      placeholder: "Select Country",
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
      //allowClear: true
    });
    $( "#company_form" ).validate( {
        rules: {
            company_name: "required",
            address_1: "required",
            

        },
        messages: {
            company_name: "Company Name is required",
            address_1:"Address 1 is required",
            
        },
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-error" ).removeClass( "has-success" );
			$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-success" ).removeClass( "has-error" );
			$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });
});
// base_currency

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();  

  $('[data-toggle="tooltip"]').hover(function(){
    $('.tooltip-inner').css('min-width', '400px');
    $('.tooltip-inner').css('color', 'black');
    $('.tooltip-inner').css('background', '#eee');
    $('.tooltip-inner').css('padding', '30px');
    $('.tooltip-inner').css('font-weight', '600');
    $('.tooltip-inner').css('font-size', '13px');
    $('.tooltip-inner').css('box-shadow', '10px 5px 10px 5px #888');
}); 
});

$(document).ready(function(){
  $('.dropdown-toggle').dropdown()
});
</script>
<style type="text/css">
  .tooltip .tooltip-arrow {border-top: 5px solid #eee !important;}
  /*.tooltip-inner{font-size: 16px !important;}*/
  .tooltip-inner{min-width:auto !important; color:#ffffff !important; background:black !important; padding:15px 30px;font-weight:100;box-shadow: 0px 8px 0px -8px #808080;}
  .companyLogo{box-shadow: 3px 0px 6px; border-radius: 60%;width: 106px !important; height: 100px !important;}
</style>
