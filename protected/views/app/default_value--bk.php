<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Manage Default Values</h3>
            <h4 class="subheading"><br>Add or remove your organisations default values here. These can be used as a benchmark when creating full or quick sourcing activities. Having default values allows your users to understand your organisations values and when creating sourcing activities. Score each of your values below with a percentage.</h4>
        </div>

        <div class="clearfix"> </div>
    </div>
     <div class="row">
    <div class="col-md-5 col-sm-5 col-xs-12">
      <div class="x_panel tile overflow_hidden"><div class="x_title"><h2>Current Default Values (%)</h2><div class="clearfix"></div></div><div class="x_content"><div id="scoring_pie_chart"></div><br /></div>
      </div>
    </div>
 

<?php
  $data_types = array(    
    'supplier_scoring_criteria'  => 'Supplier Scoring Criteria',
  );
?>

<div class="col-md-7 col-sm-7 col-xs-12">
  <div class="tile_count" style="margin-top: -3px;">
    <form id="settings_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('app/defaultValue'); ?>">

      <div id="datatype_values">

         <div class="form-group">
          <div class="col-md-10 col-sm-10 col-xs-10">
             <div style="float: left; width: 78%;margin-left: 5px;">
                <label style="font-size: 14px;">Title</label>
             </div>
             <div style="float: left;width: 2%">&nbsp;</div>
              <div style="float: left;width: 18%;margin-left: 5px;">
                <label style="font-size: 14px;">Score (%)</label>
              </div>       
          </div>    
         </div>


      <?php $total_rows = 0; foreach ($data as $row) { $total_rows += 1; ?>
          <div class="form-group" id="data_row_<?php echo $total_rows; ?>" >
              <div class="col-md-10 col-sm-10 col-xs-10">
             <div style="float: left; width: 80%;">
               <input type="text" class="form-control notranslate" name="value_<?php echo $total_rows; ?>" id="value_<?php echo $total_rows; ?>"
                <?php if (isset($row['value']) && !empty($row['value'])) echo 'value="' . $row['value'] . '"'; else echo 'placeholder="Default Value Title"'; ?> />
               </div>
              <div style="float: left;width: 2%">&nbsp;</div>
          <div style="float: left;width: 18%;">
              <input type="number" autocomplete="off" class="form-control score notranslate" name="score_<?php echo $total_rows; ?>" id="score__<?php echo $total_rows; ?>"
                <?php if (isset($row['score'])) echo 'value="' . number_format($row['score']) . '"'; else echo '0'; ?>  min="0" step="0.01" />
            </div>
                  <input type="hidden" name="id_<?php echo $total_rows; ?>" id="id_<?php echo $total_rows; ?>" 
                        <?php if (isset($row['id']) && !empty($row['id'])) echo 'value="' . $row['id'] . '"'; else echo '0'; ?> />
                  <input type="hidden" class="notranslate"  name="deleted_flag_<?php echo $total_rows; ?>" id="deleted_flag_<?php echo $total_rows; ?>" value="0" /> 
              </div>
              <div class="col-md-2 col-sm-2 col-xs-2">
                  <a class="btn btn-sm btn-primary" style="background: #f96969 !important;border:1px solid #f96969 !important;" onclick="deleteRow(<?php echo $total_rows; ?>); return false;">Delete</a>
              </div>
          </div>
      <?php } ?>
      </div>
      <div class="clearfix"> </div>
      
      <div class="form-group">
      <div class="clearfix"> <br /> </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="hidden" class="notranslate" name="form_submitted" id="form_submitted" value="1" />
      <input type="hidden" class="notranslate" name="data_type_change" id="data_type_change" value="0" />
      <input type="hidden" class="notranslate" name="total_rows" id="total_rows" value="<?php echo $total_rows; ?>" />
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-8 col-sm-6 col-xs-6">
          <div class="scoring_alert text-center" ></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
           <div class="scoring_percentage_alert text-right text-center-md"></div>
        </div>
      </div>
      
    <?php if (isset($data_type) && !empty($data_type)) { ?>
        <div class="btn">
        <a style="cursor: pointer;background: #f0ad4e !important; border: #f0ad4e  !important" onclick="addRow(); return false;" class="btn btn-primary">Add New Default Value</a>
        <a href="javascript:void(0);" onclick="$('#settings_form').submit();">
            <button type="button" class="btn btn-success submit-btn1 score_btn">
                Save Data
            </button>
        </a>
        </div>
    <?php } ?>
      </div>
      </div>

    </form>
  </div>
</div>
</div>

<div id="new_row_code">
  <div id="new_row_code_area">
        <div class="form-group" id="data_row_ROWIDX">
            <div class="col-md-6 col-sm-6 col-xs-12">
        
            <div style="float: left; width: 80%;">
              <input type="text" class="form-control notranslate" name="value_ROWIDX" id="value_ROWIDX" placeholder="Default Value Title" />
            </div>
            <div style="float: left;width: 2%">&nbsp;</div>
           <div style="float: left;width: 18%;">
              <input type="number" class="form-control score notranslate" name="score_ROWIDX" id="score_ROWIDX" placeholder="Scoring" min="0" value="0" step="0.01" required="required"/>
            </div>
          
          <input type="hidden" class="notranslate" name="id_ROWIDX" id="id_ROWIDX" value="0" />
                <input type="hidden" class="notranslate" name="deleted_flag_ROWIDX"  id="deleted_flag_ROWIDX" value="0" />
            </div>
            <div class="col-md-2 col-sm-2 col-xs-4">
                <a style="text-decoration: underline; cursor: pointer;" onclick="deleteRow(ROWIDX); return false;">Delete</a>
            </div>
        </div>
      </div>
</div>



<script type="text/javascript">
	$("#new_row_code").hide();
function deleteRow(row_idx)
{  
	if (confirm('Are you sure you want to delete this row? This action cannot be reversed.'))
	{
		$('#deleted_flag_' + row_idx).val(1);
		$('#data_row_' + row_idx).hide();
	}

}

function addRow()
{
	var total_rows = $('#total_rows').val();
	total_rows = parseInt(total_rows) + 1;
	$('#total_rows').val(total_rows);

	var new_row_html = $('#new_row_code_area').html();
	new_row_html = new_row_html.replace(/ROWIDX/g, total_rows);
	$('#datatype_values').append(new_row_html);
}

function loadScoring(){
	var sum = 0;
	$(".score").each(function(){ 
		sum += parseFloat($(this).val());
		if(sum > 100 || sum < 100){
			$('.score_btn').attr('disabled','disabled');
			$(".scoring_alert").html('<strong style="color:red">Default Values must add up to 100% </strong>');
		}else{
			$('.score_btn').removeAttr('disabled','disabled');
			$(".scoring_alert").html('');
		}
	});
  	 $(".scoring_percentage_alert").html('<strong>'+sum+'%/100%</strong>');
}
loadScoring();

$(document).on('keyup', ".score", function () {
    loadScoring();

});

/*$('.score').on('keyup' ,function(){
	loadScoring();
  });*/



createPieChart();

colors = ['#2d9ca2','#66DA26', '#546E7A', '#E91E63', '#FF9800','#2E93fA','#2196F3', '#3a62ba', '#7b9333', '#344189','#','#','#','#','#'];

function createPieChart()
{   

    var quote_id = $("#quote_id").val();
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/listScoreDataChart'); ?>',
        type: 'POST', dataType: 'json',
        data: {},
        success: function(chart_data) {
 /*      var score_values = chart_data.score_values;
       var score_labels = chart_data.score_labels;*/
       var pie_score_series = [];
       var pie_score_label  = chart_data.pie_score_label;
      for (i = 0; i < chart_data.pie_score_series.length; i++) {
          pie_score_series.push(parseFloat(chart_data.pie_score_series[i]));
      }
      
    // START: Pie Chart
    var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 200,
              width: 450,
              type: 'pie',
              id:"scoringPieChart"

            },
           // legend: {show: false},
            dataLabels: {enabled: false},
            labels: pie_score_label,
            series: pie_score_series,
            responsive: [{
               breakpoint: 380,
                options: {
                    chart: {
                       width: 'auto'
                        //height: 'auto',

                    },
                    legend: {
                        show: false
                    }
                }
            }],
            
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_pie_chart"),
            options
        );
         chart.render();

          ApexCharts.exec('scoringPieChart', 'updateOptions', {
           labels:  pie_score_label,
          }, false, true);
          ApexCharts.exec('scoringPieChart', 'updateSeries', pie_score_series,true);

    // END: Pie Chart
       }
      });

  }
</script>
