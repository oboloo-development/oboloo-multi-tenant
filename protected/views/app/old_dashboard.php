
          <!-- page content -->
          <div class="right_col" role="main">
            
            <?php 
            	for ($display_index = 1; $display_index <= 3; $display_index++) 
            	{
            		$top_datasets = array();
					$canvas_id = "";
					
					if ($display_index == 1)
					{
	            		$top_datasets = $top_vendors;
		            	$canvas_id = 'top_vendors';
					}
					else if ($display_index == 2) 
					{
						$top_datasets = $top_departments;
		            	$canvas_id = 'top_departments';
					}
					else if ($display_index == 3) 
					{
						$top_datasets = $current_year;
		            	$canvas_id = 'year_to_date';
					}
					
					$canvas_header = ucwords(str_replace("_", " ", $canvas_id));
            ?>
            
		            <h3><?php echo $canvas_header; ?></h3>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count">
		            	<div class="col-md-4 col-md-offset-2">
		            		<canvas id="<?php echo $canvas_id; ?>" height="300px"></canvas>
		            	</div>
		            	<div class="col-md-4">
		            		<table class="tile_info">
		            			<?php
		            				$idx = 0;
		            				foreach ($top_datasets as $dataset_idx => $a_dataset)
									{
										if ($dataset_idx === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}
		            			?>
		                          		<tr>
		                            		<td>
		                              			<i class="fa fa-square <?php echo $color; ?>"></i>
		                              			<?php echo $a_dataset['dim_1']; ?> - <?php echo number_format($a_dataset['percent'], 2) . '%'; ?>
		                            		</td>
		                          		</tr>
		                        <?php 
		                        	}
		                        ?>
		                        </table>
						</div>
					</div>
					<div class="clearfix"><br /></div>
					
					<div class="row">
						<div class="col-md-12">
							<?php foreach ($top_datasets as $dataset_idx => $a_dataset) { ?>
								<div class="col-md-12">
									<div class="col-md-4">
										<span style="font-size: 140%;"><?php echo $a_dataset['dim_1']; ?></span>
									</div>
									<div class="col-md-7">
					                    <div class="progress">
					                        <div class="progress-bar bg-green" role="progressbar" 
					                        	aria-valuenow="<?php echo number_format($a_dataset['percent'], 0); ?>" aria-valuemin="0" aria-valuemax="100" 
					                        		style="width: <?php echo number_format($a_dataset['percent'], 2); ?>%;">
				                        	</div>
			                        	</div>						
									</div>
									<div class="col-md-1" style="text-align: right;">
										<h4><?php echo '$' . number_format($a_dataset['total'], 2); ?></h4>
									</div>
								</div>
								<div class="clearfix"></div>
							<?php } ?>
						</div>					
		            </div>
		            <div class="clearfix"> </div>
		            
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>
			<?php } ?>            


<script type="text/javascript">
$(document).ready(function() {
	var vendor_labels = [];
	var vendor_data = [];
	var vendor_colors = [];

	var department_labels = [];
	var department_data = [];
	var department_colors = [];
	
	var month_labels = [];
	var month_data = [];
	var month_colors = [];
	
	
	<?php 
		$i = 0; 
		foreach ($top_vendors as $idx => $a_vendor) 
		{
			if ($idx === 'total') continue; 
	?>
	
			vendor_labels[<?php echo $i; ?>] = "<?php echo $a_vendor['dim_1']; ?>";
			vendor_data[<?php echo $i; ?>] = <?php echo round($a_vendor['total'], 2); ?>;
	
	<?php 
			$i += 1; 

			$color = '';
			switch ($i)
			{
				case 1  : $color = '#3498DB'; break;
				case 2  : $color = '#9B59B6'; break;
				case 3  : $color = '#1ABB9C'; break;
				case 4  : $color = '#00CED1'; break;
				case 5  : $color = '#E74C3C'; break;
				case 6  : $color = '#FAEBD7'; break;
				case 7  : $color = '#7FFFD4'; break;
				case 8  : $color = '#FFE4C4'; break;
				case 9  : $color = '#BDB76B'; break;
				default : $color = '#FFA07A'; break;
			}
			
			echo 'vendor_colors[' . ($i - 1) . '] = "' . $color . '";';

		} 
	?>


	<?php 
		$i = 0; 
		foreach ($current_year as $idx => $a_month) 
		{
			if ($idx === 'total') continue; 
	?>
	
			month_labels[<?php echo $i; ?>] = "<?php echo $a_month['dim_1']; ?>";
			month_data[<?php echo $i; ?>] = <?php echo round($a_month['total'], 2); ?>;
	
	<?php 
			$i += 1; 

			$color = '';
			switch ($i)
			{
				case 1  : $color = '#3498DB'; break;
				case 2  : $color = '#9B59B6'; break;
				case 3  : $color = '#1ABB9C'; break;
				case 4  : $color = '#00CED1'; break;
				case 5  : $color = '#E74C3C'; break;
				case 6  : $color = '#FAEBD7'; break;
				case 7  : $color = '#7FFFD4'; break;
				case 8  : $color = '#FFE4C4'; break;
				case 9  : $color = '#BDB76B'; break;
				default : $color = '#FFA07A'; break;
			}
			
			echo 'month_colors[' . ($i - 1) . '] = "' . $color . '";';

		} 
	?>

	<?php 
		$i = 0; 
		foreach ($top_departments as $idx => $a_department) 
		{
			if ($idx === 'total') continue; 
	?>
	
			department_labels[<?php echo $i; ?>] = "<?php echo $a_department['dim_1']; ?>";
			department_data[<?php echo $i; ?>] = <?php echo round($a_department['total'], 2); ?>;
	
	<?php 
			$i += 1; 

			$color = '';
			switch ($i)
			{
				case 1  : $color = '#3498DB'; break;
				case 2  : $color = '#9B59B6'; break;
				case 3  : $color = '#1ABB9C'; break;
				case 4  : $color = '#00CED1'; break;
				case 5  : $color = '#E74C3C'; break;
				case 6  : $color = '#FAEBD7'; break;
				case 7  : $color = '#7FFFD4'; break;
				case 8  : $color = '#FFE4C4'; break;
				case 9  : $color = '#BDB76B'; break;
				default : $color = '#FFA07A'; break;
			}
			
			echo 'department_colors[' . ($i - 1) . '] = "' . $color . '";';

		} 
	?>
	
	createChart('top_vendors', vendor_data, vendor_labels, vendor_colors);
	createChart('top_departments', department_data, department_labels, department_colors);
	createChart('year_to_date', month_data, month_labels, month_colors);
});

function createChart(div_id, data_values, data_labels, data_colors)
{
	var chart_type = 'doughnut';
	if (div_id == 'top_departments') chart_type = 'pie';
	if (div_id == 'year_to_date') chart_type = 'bar';
	
		var chart_doughnut_settings = {
				type: chart_type,
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: data_labels,
					datasets: [{
						data: data_values,
						backgroundColor: data_colors
					}]
				},
				options: { 
					legend: false, 
					responsive: true 
				}
			}
		
			var chart_doughnut = new Chart( $('#' + div_id), chart_doughnut_settings);
		
}	
</script>
