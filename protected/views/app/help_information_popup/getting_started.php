<div id="getting_start_module_modal" class="modal fade" role="dialog">
<div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content info_contant" style="background-color: #ffff !important;">
      <div class="modal-header info_header">
        <h1 class="heading-title">Help & Learning</h1>
        <h5 style="padding-bottom: 10px; font-size: 10px;"><a herf="#" class="home_redirect">Home ></a> Getting Started</h5>
        <h2 class="text-center sec-title">What Would You Like To Do?</h2>
      </div>
      <div class="modal-body" >
        <!-- Start Create a Sourcing Activity -->
        <div class="row info_row">
          <?php if(!FunctionManager::savingSpecific()) { ?>
          <div class="col-md-2">
            <a href="<?php echo $this->createUrl('quotes/list',array('from'=>'information'));  ?>" class="">
              <div class="img-svg">
               <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/trolley.png'); ?>"   class="img-responsive " />
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('quotes/list',array('from'=>'information'));  ?>" class="">Create a Sourcing Activity</a></h3>
            <p class="elementor-icon-box-description">Create eSourcing activities with questionnaires & evaluations</p>
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('quotes/list',array('from'=>'information'));  ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create a Sourcing Activity -->

        <!-- Start Create a Contract -->
        <div class="row info_row">
          <div class="col-md-2">
            <a href="<?php echo $this->createUrl('contracts/list',array('from'=>'information'));  ?>" class="">
              <div class="img-svg">
               <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/contract.png'); ?>"   class="img-responsive " />
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('contracts/list',array('from'=>'information'));  ?>" class="">Create a Contract</a></h3>
            <p class="elementor-icon-box-description">Create, upload & build your own contract repository</p>
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('contracts/list',array('from'=>'information'));  ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create a Contract -->

        <!-- Start Create a Supplier -->
        <div class="row info_row">
          <div class="col-md-2">
            <a href="#" class="<?php echo $this->createUrl('vendors/list',array('from'=>'information'));  ?>">
              <div class="img-svg">
                 <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/network.png'); ?>"   class="img-responsive " />
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('vendors/list',array('from'=>'information'));  ?>" class="">Create a Supplier</a></h3>
            <p class="elementor-icon-box-description">Create, upload & build your own Supplier repository</p>
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('vendors/list',array('from'=>'information'));  ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create a Supplier -->
        <?php }

          ///////////// 
          // If hide savingSpecific getting started in this case , reflect some css issue, like border issue,
          // If the domain is savingSpecific in case we pass css class empty. 
          // Also hide (Super Users Only) for savingSpecific in paragraph 
          ////////////
          $info_row ='info_row';
          if(FunctionManager::savingSpecific()){
            $info_row ='';
          }?>

        <!-- Start Create a Savings Record -->
        <!-- $info_row dynamic class name case wise  savingSpecific -->
        <div class="row <?= $info_row; ?>">
          <div class="col-md-2">
            <a href="<?php echo $this->createUrl('savings/list',array('from'=>'information'));  ?>" class="">
              <div class="img-svg">
                <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/bar-chart.png'); ?>"   class="img-responsive " />
              </div>
            </a>
          </div>
      
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('savings/list',array('from'=>'information'));  ?>" class="">Create a Savings Record</a></h3>
            <p class="elementor-icon-box-description">Put the values that are important to your company 
              <?php  //if($info_row != ""){ echo '(Super Users Only)'; } else{ echo ""; } ?> </p>
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('savings/list',array('from'=>'information'));  ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create a Savings Record -->

        <?php
        
         if(!FunctionManager::savingSpecific()) { ?>
        <!-- Start Create Your Default Values -->
        <div class="row info_row" style="border-bottom:0px !important;">
          <div class="col-md-2">
            <a href="<?php echo $this->createUrl('app/defaultValue'); ?>" class="">
              <div class="img-svg">
                <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/pie-chart.png'); ?>"   class="img-responsive " />
              </div></a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="<?php echo $this->createUrl('app/defaultValue'); ?>" class="">Create Your Default Values</a></h3>
            <p class="elementor-icon-box-description">Identify, track and collaborate on your company wide savings</p>
          </div>
          <div class="col-md-1">
            <p><a href="<?php echo $this->createUrl('app/defaultValue'); ?>" class=""><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Create Your Default Values -->
        <?php } ?> 
      </div>
     </div>
    </div>
  </div>

