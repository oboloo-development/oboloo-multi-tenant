<div id="module_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content info_contant" style="background-color: #ffff !important; ">
      <div class="modal-header info_header" >
        <h1 class="heading-title">Help & Learning</h1>
        <h5 style="padding-bottom: 10px; font-size: 10px;" class="redirect_home">Home</h5>
      </div>
      <?php
           // echo getCurrentSite('sandbox'); exit;
      ?>
      <div class="modal-body" >
        <!-- Start Getting Started -->
        <div class="row info_row">
          <div class="col-md-2">
            <a href="javascript:void(0)" class="get_start">
              <div class="img-svg">
               <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/rocket.jpg'); ?>"   class="img-responsive" />
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="javascript:void(0)" class="get_start">Getting Started</a></h3>
            <p class="elementor-icon-box-description">Get started quickly and start using oboloo's features immediately</p>
          </div>
          <div class="col-md-1">
            <p><a href="javascript:void(0)" class="get_start"><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Getting Started -->

         <!-- Start Help Centre -->
        <div class="row info_row">
          <div class="col-md-2">
            <a href="javascript:void(0)" class="help_centre">
              <div class="img-svg">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="" height="" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><path xmlns="http://www.w3.org/2000/svg" d="m32 0h384c17.671875 0 32 14.328125 32 32v288c0 17.671875-14.328125 32-32 32h-384c-17.671875 0-32-14.328125-32-32v-288c0-17.671875 14.328125-32 32-32zm0 0" fill="#3b4fb4" data-original="#448aff" style="" class=""></path><path xmlns="http://www.w3.org/2000/svg" d="m416 508.878906-148.59375-92.878906h-171.40625c-17.671875 0-32-14.328125-32-32v-288c0-17.671875 14.328125-32 32-32h384c17.671875 0 32 14.328125 32 32v288c0 17.671875-14.328125 32-32 32h-64zm0 0" fill="#82b1ff" data-original="#82b1ff" style="" class=""></path><g xmlns="http://www.w3.org/2000/svg" fill="#fff"><path d="m304 288h-32v-64h16c17.671875 0 32-14.328125 32-32s-14.328125-32-32-32-32 14.328125-32 32h-32c0-33.257812 25.476562-60.976562 58.617188-63.773438 33.140624-2.796874 62.902343 20.253907 68.476562 53.042969 5.578125 32.789063-14.890625 64.382813-47.09375 72.699219zm0 0" fill="#ffffff" data-original="#ffffff" style="" class=""></path><path d="m272 320h32v32h-32zm0 0" fill="#ffffff" data-original="#ffffff" style="" class=""></path><path d="m128 160h-32v-64h64v32h-32zm0 0" fill="#ffffff" data-original="#ffffff" style="" class=""></path><path d="m480 384h-64v-32h32v-32h32zm0 0" fill="#ffffff" data-original="#ffffff" style="" class=""></path></g></g></svg>
            </div></a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="javascript:void(0)" class="help_centre">Help Centre</a></h3>
            <p class="elementor-icon-box-description">User Guides, Tutorial Videos and support help</p>
          </div>
          <div class="col-md-1">
            <p><a href="javascript:void(0)" class="help_centre"><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End Help Centre -->
      <?php
         $subDomainRecord = '';
         if(!strpos(Yii::app()->getBaseUrl(true),"multi-local")) {
           if(empty(getCurrentDataSite())){
           $subDomainRecord = '';
           }else{
            $subDomainRecord = getCurrentDataSite();
           }
         }
      ?>
         <!-- Start Sandbox -->
         <form id="" enctype="multipart/form-data" target="_blank" class="form-horizontal" method="post" action="<?php echo FunctionManager::environmentUrl(); ?>">
          <?php $user_id = Yii::app()->session['user_id']; 
             $current_domain_name = FunctionManager::currentDomain();
             $userObj = new User();
             $user = $userObj->getUsers($user_id); 
          ?>
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
        <input type="hidden" name="username" value="<?php echo !empty($subDomainRecord) ? $subDomainRecord['username'] : ''; ?>">
        <input type="hidden" name="password" value="<?php echo !empty($subDomainRecord) ? $subDomainRecord['password'] : ''; ?>">
        <?php if(!strpos(Yii::app()->getBaseUrl(true),"multi-local")) {?>
        <input type="hidden" name="current_username" value="<?php echo !empty($user->username)?$user->username:'';?>">
        <input type="hidden" name="current_password" value="<?php echo !empty($user->password)?$user->password:'';?>">
        <?php } ?>
        <input type="hidden" name="action_login" value="1">
        <input type="hidden" name="user_domain_name" value="<?php echo $current_domain_name; ?>">
        <?php if(FunctionManager::checkEnvironment()){?>
          <input type="hidden" name="domain_request" value="live">
        <?php $environmentBtn="Sandbox View";
        }else{ 
          $environmentBtn="My Domain";
         }?>
          <div class="row info_row">
          <div class="col-md-2">
            <button type="submit"  class="sandbox_redirect">
             <div class="img-svg">
              <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/like.png'); ?>"   class="img-responsive" />
             </div>
           </button>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><button type="submit"  class="sandbox_redirect" target="_blank">Sandbox</button></h3>
            <p class="elementor-icon-box-description">Get to know your way around by checking out the sandbox version of oboloo </p>
          </div>
          <div class="col-md-1">
            <button type="submit"  class="sandbox_redirect" target="_blank">
             <span style="font-size: 24pt; color: #5A738E;">&gt;</span>
          </button>
          </div>
        </div>
    </form>  
        
        <!-- End Sandbox -->
        <?php if(strpos(Yii::app()->getBaseUrl(true),"sandbox")){ ?>
            <div class="row info_row">
          <div class="col-md-2">
            <a href="javascript:void(0)" class="product_tour_visiable_sandbox_doamin">
              <div class="img-svg">
                <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/like.png'); ?>"   class="img-responsive " />
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="javascript:void(0)" class="product_tour_visiable_sandbox_doamin">Product Tour</a></h3>
            <p class="elementor-icon-box-description">Learn your way around the oboloo platform</p>
          </div>
          <div class="col-md-1">
            <p><a href="javascript:void(0)" class="product_tour_visiable_sandbox_doamin"><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <?php } else{?>
           <!-- Start Product Tour -->
          <form id="" target="_blank" enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo FunctionManager::environmentUrl();?>">
          <?php  $user_id = Yii::app()->session['user_id'];  
         
               $current_domain_name = FunctionManager::currentDomain();
               $userObj = new User();
               $user = $userObj->getUsers($user_id);

          ?>
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
        <input type="hidden" name="username" value="<?php echo !empty($subDomainRecord) ? $subDomainRecord['username'] : ''; ?>">
        <input type="hidden" name="password" value="<?php echo !empty($subDomainRecord) ? $subDomainRecord['password'] : ''; ?>">
        <?php if(!strpos(Yii::app()->getBaseUrl(true),"multi-local")) {?>
        <input type="hidden" name="current_username" value="<?php echo $user['username'];?>">
        <input type="hidden" name="current_password" value="<?php echo $user['password'];?>">
        <?php } ?>
        <input type="hidden" name="action_login" value="1">
        <input type="hidden" name="user_domain_name" value="<?php echo $current_domain_name; ?>">
        <?php if(FunctionManager::checkEnvironment()){?>
          <input type="hidden" name="domain_request" value="live">
        <?php $environmentBtn="Sandbox View";
        }else{ 
          $environmentBtn="My Domain";
         } ?>
        <div class="row info_row">
          <div class="col-md-2">
            <button type="submit"  class="sandbox_redirect">
              <div class="img-svg">
               <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/directional-sign.png'); ?>"   class="img-responsive " />
            </div></button>
          </div>
          <div class="col-md-9"> 
            <h3 class="title-disc"><button type="submit"  class="sandbox_redirect">Product Tour</button></h3>
            <p class="elementor-icon-box-description">Learn your way around the oboloo platform</p>
          </div>
          <div class="col-md-1">
            <p><button type="submit"  class="sandbox_redirect"><span style="font-size: 24pt;">&gt;</span></button></p>  
          </div>
        </div>
      </form>
        <!-- End Product Tour -->
         <?php } ?>

         <!-- Start Raise a Ticket -->
        <div class="row info_row" style="border-bottom:0px !important;">
          <div class="col-md-2">
            <a href="https://oboloo.stonly.com/kb/guide/en/contact-oboloo-pMPbNpkPFT/Steps/1819416,1819418" >
              <div class="img-svg">
                <img src="<?php echo AppUrl::bicesUrl('images/icons/info-icon/customer-service.png'); ?>"   class="img-responsive " />  
              </div>
            </a>
          </div>
          <div class="col-md-9">
            <h3 class="title-disc"><a href="https://oboloo.stonly.com/kb/guide/en/contact-oboloo-pMPbNpkPFT/Steps/1819416,1819418">Contact Us Or Raise a Ticket</a></h3>
            <p class="elementor-icon-box-description">Email our support team for help</p>
          </div>
          <div class="col-md-1">
            <p><a href="https://oboloo.stonly.com/kb/guide/en/contact-oboloo-pMPbNpkPFT/Steps/1819416,1819418" ><span style="font-size: 24pt;">&gt;</span></a></p>  
          </div>
        </div>
        <!-- End raiseTicket -->
   
       </div>
      </div>
    </div>
  </div>

<?php echo  $this->renderPartial('/app/help_information_popup/getting_started');?>
<?php echo  $this->renderPartial('/app/help_information_popup/help_centre');?>
  
<?php 

$sql = "select * from  user_setup where user_id=".Yii::app()->session['user_id'];
    $setup = Yii::app()->db->createCommand($sql)->queryRow();

if(!empty($setup) && $setup['home'] !=1 ){
    $sql = "update user_setup set home=1 where user_id=".Yii::app()->session['user_id'];
    $setup = Yii::app()->db->createCommand($sql)->execute();

?>
<script type="text/javascript">
  $(document).ready(function(){
    $('.quick_module_modal').trigger('click');
  });
</script>
<?php } ?>

<script type="text/javascript">

</script>
