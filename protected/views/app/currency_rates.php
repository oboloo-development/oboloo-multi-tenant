<?php $disabled = FunctionManager::sandbox(); ?>
<?php $record_user_id = 0;
if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];?>
<?php //$countries = FunctionManager::countryList();
  $sql = "SELECT country from currency_rates";
  $countries = Yii::app()->db->createCommand($sql)->queryAll();
  
?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left pb-30 pt-3">
            <h3 class="pt-3">Currencies</h3>
            <h4 class="subheading pt-3"  style="padding: 10px 0px;">Base currency</h4>
        </div>

        <div class="span6 pull-right">
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bs-example-modal-lg" data-project-id="0" data-project-name="">
                <span class="glyphicon glyphicon-plus mr-2" aria-hidden="true"></span> Add Currency
            </button>
        </div>
        <div class="clearfix"> </div>
    </div>

    <div id="" class="alert alert-success ajax_success" role="alert"></div>


    <table id="" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
        <tr>     
          <th>Country</th>
          <th>Currency Name</th>
          <th>Currency Code</th>
          <th>Currency Symbol</th>
          <th>Currency Rate</th>
          <th>Last Updated</th>
          <th>Last Updated By</th>
          <th style="width: 8%"></th>
        </tr>
      </thead>
      <tbody>
                    
       <?php if(!empty($baseCurrency)){
           foreach ($baseCurrency as $proValue) 
            {?>
              <tr>
              
               <td><?php echo $proValue['country'];?></td>
               <td><?php echo $proValue['currency_name'];?></td>
               <td><?php echo $currency=$proValue['currency'];?></td>
               <td><?php echo $currencySymbol=$proValue['currency_symbol'];?></td>
               <td><?php echo $rate=$proValue['rate'];?></td>
               <td><?php echo $proValue['updated_at'] !="0000-00-00 00:00:00"?date(FunctionManager::dateFormat(), strtotime($proValue['updated_at'])):''; ?></td>
               <td><?php echo $userName = $proValue['user_name'];?></td>    
              <td><?php echo '<span><a href="'.AppUrl::bicesUrl('app/companyDetails').'" class="btn btn-sm btn-success view-btn" style="text-decoration:none">Base Currency</a></span>'; ?></td>
           </tr>
           <?php }
        }?>
         
      </tbody>
  </table>

  <div class="row-fluid tile_count">
        <div class="span6 pull-leftpt-3 mt-50">
            <h4 class="subheading pt-3">Additional currencies</h4>
        </div>
        <div class="clearfix"> </div>
    </div>

    <table id="currency_table" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
        <tr>
          
          <th>Country</th>
          <th>Currency Name</th>
          <th>Currency Code</th>
          <th>Currency Symbol</th>
          <th>Currency Rate</th>
          <th>Last Updated</th>
          <th>Last Updated By</th>
         <!--  <th>Base Currency</th> -->
         <th style="width: 12%"></th>
        </tr>
      </thead>
      <tbody>
      </tbody>
  </table>
</div>

<div id="add_currency_modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add New Currency Rate</h4>
        <div id="" class="alert alert-danger ajax_success_modal" role="alert"></div>
      </div>
      <div class="modal-body">
        <form action="" method="post" id="project_create_form">
        <div class="clearfix"></div>
        <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <?php $currencyTool="Pick a currency by country from the drop down" ?>
               <label class="control-label">Country <span style="color: #a94442;">*</span></label>
               <select class="form-control notranslate" name="country" id="country">
                 <option value="">Select</option>
                <?php foreach($countries as $key => $value) {?>
               <option value="<?php echo $value['country'];?>"><?php echo $value['country'];?></option>
             <?php } ?>
              </select>
                
               
            </div>
        </div>
        <div class="clearfix"></div><br />
        <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
               <label class="control-label" >Currency Name <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate" name="currency_name" id="add_currency_name"  value="" required="required" />
               
            </div>
        </div>
        <div class="clearfix"></div><br />
        <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
               <label class="control-label">Currency Code <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate" name="currency" id="new_currency" value="" required="required" />
               
            </div>
        </div>
        <div class="clearfix"></div><br>

         <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
               <label class="control-label">Currency Symbol <span style="color: #a94442;">*</span></label>
                <input type="text" class="form-control notranslate"  name="currency_symbol" id="add_currency_symbol" value="" required="required" />
               
            </div>
        </div>
         <div class="clearfix"></div><br>
        <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
               <label class="control-label">Exchange Rate <span style="color: #a94442;">*</span></label>
                <input type="number" class="form-control" step="0.01" min="0.000" max="0.000"  name="rate" id="new_rate" value="" required="required" />
            </div>
        </div> 
        </form>
      </div>
      <div class="modal-footer">
        <input type="hidden" class="notranslate" name="project_id" id="project_id" value="0" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="saveDepartmentButton" <?php echo $disabled; ?> type="button" class="btn btn-success submit-btn1" onclick="saveCurrencyRate();">
            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading_icon" style="display: none;"></span>
            <span>Add Currency</span>
        </button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  $(document).ready( function() {

    $(".ajax_success").hide();
    $(".ajax_success_modal").hide();
    $(".select_search").select2({
     /* placeholder: "Select Country",*/
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
      //allowClear: true
    });

    $(".no-sort").removeClass("sorting_asc");


    $("#country").change(function(){
       var country = $(this).val();
       var alreadyExisted = 0;
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('app/fillCurrencyField'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: {country:country},
            beforeSend: function() {
            $("#add_currency_name").prop('readonly',false);
            $("#new_currency").prop('readonly',false);
            $("#add_currency_symbol").prop('readonly',false);
            $("#new_rate").prop('readonly',false);
            $("#saveDepartmentButton").removeAttr("disabled");
            $(".ajax_success_modal").hide();
            $(".ajax_success_modal").html('');
            
            
           },
            success: function(data) {

                $("#add_currency_name").val(data.currency_name);
                if(data.currency_name !=''){
                   //alreadyExisted = 1;
                  $("#add_currency_name").prop('readonly',true);
                }

                $("#new_currency").val(data.currency_code);
                if(data.currency_code !=''){
                   //alreadyExisted = 1;
                  $("#new_currency").prop('readonly',true);
                 
                }

                $("#add_currency_symbol").val(data.currency_symbol);
                if(data.currency_symbol !=''){
                  // alreadyExisted = 1;
                  $("#add_currency_symbol").prop('readonly',true);
                 
                }

               $("#new_rate").val(data.rate);

                if(data.status =='0'){
                  alreadyExisted=1;
                  $("#new_rate").prop('readonly',true);
                }

                if(data.rate <=0){
                  alreadyExisted=1;
                  $("#new_rate").prop('readonly',false);
                }else if(alreadyExisted==1){
                $("#saveDepartmentButton").prop("disabled",true);
                msg = "Currency Already Exists";
                $(".ajax_success_modal").show();
                $(".ajax_success_modal").html(msg);
                $(".ajax_success_modal").delay(1000*10).fadeOut();  
              }
            }
        });
    });

// setTimeout(function(){  $('.baseCurrncy').parentsUntil('#currency_table tbody').css('background', '#ccc'); }, 100);

  $('#currency_table').dataTable({
        "columnDefs": [ {
            "targets": -1,
            //"width": "6%",
             "order": [[ 0, "asc" ]]
           // "orderable": false
        } ],
        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
              for (var i=1; i<=6; i++)
                  $('td', row).eq(i).css('text-decoration', 'line-through');
        },        
        "order": [[ 1, "asc" ]],
        "pageLength": 100,        
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?php echo AppUrl::bicesUrl('app/currencyRatesAjax'); ?>",
          "type": "POST",
          data: function ( input_data ) {
            input_data.search_para = input_data.search.value;
        } 
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" }
    });

  $('#base_currency_table').dataTable({
        "columnDefs": [ {
             "targets": "no-sort",
             "orderable": false,
              
        } ],
        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
              for (var i=1; i<=6; i++)
                  $('td', row).eq(i).css('text-decoration', 'line-through');
        },
        "searching": false, 
        "paging": false, 
        "info": false,
        "pageLength": 100,
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?php echo AppUrl::bicesUrl('app/baseCurrencyRatesAjax'); ?>",
          "type": "POST",
          data: function ( input_data ) {
            input_data.search_para = input_data.search.value;
        } 
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" }
    });

  $('#delete_confirm_modal .modal-footer button').on('click', function(event) {
    var button = event.target; // The clicked button
  
    $(this).closest('.modal').one('hidden.bs.modal', function() {
        if (button.id == 'yes_delete') changeItemStatus();
      });
   });

  $("#add_currency_modal" ).on('shown.bs.modal', function(){
      $(".ajax_success_modal").html('');
      $(".ajax_success_modal").hide();
     $("#project_create_form")[0].reset();
    });

});

function saveCurrencyRate()
{
    $('#loading_icon').show();
    currencyRate = parseFloat($('#new_rate').val());
    
    if(isNaN(currencyRate) || currencyRate<0.001){
      $(".ajax_success_modal").show();
      $(".ajax_success_modal").html("Currency Rate should be greater then 0");
      $(".ajax_success_modal").delay(1000*10).fadeOut();
           
    }else{
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('app/saveCurrency'); ?>',
        type: 'POST', async: false, dataType: 'json',
        data: $("#project_create_form").serialize(),
        beforeSend: function() {
          $(".ajax_success_modal").html('');
          $(".ajax_success_modal").hide();
         },
        success: function(data) {

         /* if(data.currency_id==0){
            //msg = "Currency Already Exists";
          }else{*/
            msg = "Currency saved successfully";
           
            $('#add_currency_modal').modal('hide');
            $('#currency_table').DataTable().ajax.reload();
          //}
          $(".ajax_success_modal").show();
          $(".ajax_success_modal").html(msg);
          $(".ajax_success_modal").delay(1000*10).fadeOut();
        }
    });
    }
}


function editCurrency(ele)
{
   
    $('#loading_icon').show();
    var formObj = ele;
    currencyRate = parseFloat($('#rate').val());
    if(isNaN(currencyRate) || currencyRate<0.001){
      msg = "Currency Rate should be greater then 0";
     $(".edit_msg_area").html('<div id="edit_msg_area" class="alert alert-danger" role="alert">'+msg+'</div>');
     $(".edit_msg_area").show().delay(1000).fadeOut();

           
    }else{
        $.ajax({
            url: '<?php echo AppUrl::bicesUrl('app/editCurrency'); ?>',
            type: 'POST', async: false, dataType: 'json',
            data: $(formObj).serialize(),
            beforeSend: function() {
            $(".ajax_success").html('');
            $(".ajax_success").hide();
          
            },
            success: function(project_data) {
              $(".edit_msg_area").html('<div id="edit_msg_area" class="alert alert-success" role="alert">'+project_data.msg+'</div>');
              $(".edit_msg_area").show().delay(500).fadeOut();

              msg = "Currency edited successfully";
              $(".ajax_success").show();
              $(".ajax_success").html(msg);


              //$('#project_table').DataTable().ajax.reload();
              //$('#project_table').DataTable().api().ajax.reload();
            }
        });

        
        proModal = $(formObj).attr('id').replace ( /[^\d.]/g, '' );
        $('#curr'+proModal).modal('hide');
        $('#currency_table').DataTable().ajax.reload();
      }
}

function deleteCurrency(key)
{
   var key = key;
    $.confirm({
    title: '',
    content: 'Do you want to delete currency?',
    buttons: {
        confirm: {
        text: "Yes",
        btnClass: 'btn btn-success',
        action: function () {
          $.ajax({
              url: '<?php echo AppUrl::bicesUrl('app/deleteCurrency'); ?>',
              type: 'POST', async: false, dataType: 'json',
              data: {id:key},
              beforeSend: function() {
              $(".ajax_success").html('');
              $(".ajax_success").hide();
            
              },
              success: function(data) {
               
                msg = data.msg;
                $(".ajax_success").show();
                $(".ajax_success").html(msg);
                $(".ajax_success").delay(1000*3).fadeOut();


                //$('#project_table').DataTable().ajax.reload();
                //$('#project_table').DataTable().api().ajax.reload();
              }
          });
          $('#currency_table').DataTable().ajax.reload();
        }},
        cancel:{ 
        text: "Cancel",
        btnClass: 'btn btn-danger',
        action: function () {
            
        },}
      
    }
  });
}

 
$(window).load(function() {
      $('.baseCurrncy').parentsUntil('#base_currency_table tbody').css('background', '#DDF1D5');
});

</script>

<style type="text/css">
.select2-selection--multiple {
    border: 1px solid #4d90fe  !important;
    outline: 0;
    border-radius: 10px !important;
}
#project_create_form .form-control,[id^="edit"] .form-control {
    width: 100% !important;
}

input[type=search] { width: 77% !important; }

</style>