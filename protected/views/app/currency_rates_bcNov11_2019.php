<div class="right_col" role="main">
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Manage Currency Rates</h3>
        </div>
        <div class="clearfix"> </div>
    </div>
<div class="row tile_count">
  <form id="rates_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('app/currencyRates'); ?>">

	  <?php foreach ($rates as $currency_rate) { ?>
	  	
	  	<div class="col-md-2" style="text-align: right;">
	  		<strong>
	  			<?php echo $currency_rate['currency']; ?>
	  		</strong>
	  	</div>
	  	
	  	<div class="col-md-2">
	  		<input type="text" class="form-control" name="<?php echo $currency_rate['currency']; ?>" style="text-align: right;"
	  				id="<?php echo $currency_rate['currency']; ?>_rate" value="<?php echo sprintf("%.5f", $currency_rate['rate']); ?>" />
	  	</div>

        <div class="col-md-2">
            <input type="text" class="form-control" name="symbol[<?php echo $currency_rate['currency']; ?>]" style="text-align: right;"
                    id="symbol_<?php echo $currency_rate['currency']; ?>_rate" value="<?php echo $currency_rate['currency_symbol']; ?>" />
        </div>


	  	<div class="col-md-8"></div>
		<div class="clearfix"> </div>
	  	
	  <?php } ?>
	  <div class="clearfix"> </div>
	  
    <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="hidden"  id="form_submitted" value="1" />

    <a href="javascript:void(0);" onclick="$('#rates_form').submit();">
        <button type="button" class="btn btn-primary">
            Save Rates
        </button>
    </a>
    </div>
    </div>

  </form>
</div>
</div>
