		<div class="clearfix"><br /></div>
            <?php 
            	for ($display_index = 1; $display_index <= 4; $display_index++) 
            	{
            		$top_datasets = array();
					$canvas_id = "";
					
					if ($display_index == 1)
					{
	            		$top_datasets = $top_vendors;
		            	$canvas_id = 'spend_vendors';
					}
					else if ($display_index == 2) 
					{
						$top_datasets = $top_departments;
		            	$canvas_id = 'spend_departments';
					}
					else if ($display_index == 3) 
					{
						$top_datasets = $current_year;
		            	$canvas_id = 'year_to_date';
					}
					else if ($display_index == 4) 
					{
						$top_datasets = $top_locations;
		            	$canvas_id = 'spend_locations';
					}
					
					$canvas_header = str_replace('Spend ', 'Top ', ucwords(str_replace("_", " ", $canvas_id)));
					if ($canvas_id == 'year_to_date') $canvas_header = 'Monthly Spend';
            ?>

		            <?php if ($display_index % 2 == 1) echo '<div class="row">'; ?>
	
						<div class="col-md-6 col-sm-6 col-xs-12">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2><?php echo $canvas_header; ?></h2>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
			                  <table class="" style="width:100%">
			                    <tbody>
			                    <tr>
			                      <td>
			                      		<?php if ($canvas_id == 'year_to_date') { ?>
			                      			<canvas id="<?php echo $canvas_id; ?>" width="460px" height="230px"></canvas>
			                      		<?php } else { ?>
			                        		<canvas id="<?php echo $canvas_id; ?>" height="230px"></canvas>
			                        	<?php } ?>
			                      </td>
			                      <td style="vertical-align: top;">
					            		<table class="tile_info">
					            			<?php
					            				$idx = 0;
					            				foreach ($top_datasets as $dataset_idx => $a_dataset)
												{
													if ($dataset_idx === 'total') continue;
													$idx += 1;
													switch ($idx)
													{
														case 1  : $color = 'blue'; break;
														case 2  : $color = 'purple'; break;
														case 3  : $color = 'green'; break;
														case 4  : $color = 'turquoise'; break;
														case 5  : $color = 'red'; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
					            			?>
					                          		<tr>
					                            		<td>
					                              			<i class="fa fa-square <?php echo $color; ?>"></i>
					                              			<?php echo $a_dataset['dim_1']; ?> - <?php echo number_format($a_dataset['percent'], 2) . '%'; ?>
					                            		</td>
					                          		</tr>
					                        <?php 
					                        	}
					                        ?>
					                        </table>
			                      	
			                      </td>
			                    </tr>
			                  </table>
			              </div>
			           </div>
			        </div>

		            <?php if ($display_index % 2 == 0) echo '</div><div class="clearfix"><br /></div><div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>'; ?>
		           
			<?php } ?>            
