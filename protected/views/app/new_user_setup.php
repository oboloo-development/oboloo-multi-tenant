<?php
  $username_value = "";
  if (isset(Yii::app()->session['duplicate_username'])) $username_value = Yii::app()->session['duplicate_username'];
  else if (isset($user) && isset($user['username'])) $username_value = $user['username'];

  $email_value = "";
  if (isset(Yii::app()->session['duplicate_email'])) $email_value = Yii::app()->session['duplicate_email'];
  else if (isset($user) && isset($user['email'])) $email_value = $user['email'];

  
  if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];?>

<div class="" role="main">
  
     

    <div class="row-fluid tile_count">
      <div class="container_stetup">

      <div class="col-lg-12 col-xs-12">
        <div class="system_setup" >
       
        <?php  $alert = Yii::app()->user->getFlash('success');
         if(!empty($alert)){?>
            <div class="alert alert-success">
                <?php echo $alert; ?>
            </div>
        <?php } ?>

         <div class="row">
          <div class="col-lg-12 col-xs-12">
           <div class="panel panel-default" style=" margin-bottom: 0px; padding: 20px 0px 20px 0px; border-radius: 30px;">
            <div class="panel-body" style="padding-bottom: 33px;">
               <h1 class="panel-body text-center heading1" style="margin-bottom: 0px;padding: 35px 0px 9px 65px; color: #000;font-weight: 600;" >You're almost there....</h1>
                <h4 class="panel-body text-center heading1" style="margin-bottom: 0px; color: #000;font-weight: 400;">Invite your team who can also enjoy the oboloo free trial</h4>
                <!-- //id="form_submit" -->
              <form   class="invite-team-form"  action="<?php echo $this->createUrl('users/userSetup');?>" method="post">
                <div class="new-user-row">
                <div class="row">
                   <div class="col-lg-4 col-xs-12">
                      <label for="fullname" class="form-label">Full Name <span class="text-danger">*</span></label>
                      <input type="text" class="form-control input-style" name="fullname[]"  id="fullname" required="">
                   </div>
                    <div class="col-lg-4 col-xs-12">
                      <label for="Email" class="form-label">Email Address <span class="text-danger">*</span></label>
                      <input type="email" class="form-control input-style" name="email[]"  id="email" required="">
                    </div>

                    <div class="col-lg-4 col-xs-12">
                      <div class="form-group">
                        <label for="User Role">User Role <span class="text-danger">*</span></label><br>
                        <select name="user_type_id[]" id="user_type_id" class="form-control e user_type input-style" required>
                          <option value="">Select User Role</option>
                          <?php foreach ($user_types as $user_type) { ?>
                          <option value="<?php echo $user_type['id']; ?>"
                          <?php if (isset($user['user_type_id']) && $user['user_type_id'] == $user_type['id']) echo 'selected="SELECTED"'; ?>>
                          <?php echo $user_type['value']; ?>
                          </option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class=" border form-group px-3 py-2 text-center text-primary add-more-skills">
                  <button class="btn btn-custom btn-sm pull-right add-social-link" type="button">Add another</button>
                </div><br><br>

            <!-- row -->

                <div class="pull-right btn-row" >
                  <button type="button" class="btn btn-info btn-md do_later">I'll do it later</button>
                 <button type="submit" class="btn btn-custom btn-md">Invite your team</button>
                </div>

             </form>

            </div>
          </div>
         </div>    

        </div>   
       </div>
      </div>
     </div>
    </div>  


              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabelAddUser">Do you want this user to have full approval access under contract management</h4>
                </div>
                <div class="modal-body">

                  <!-- my radio button yes or no option start-->
                  <form id="FormFilters" >
                    <div class="row">
                      <div class="col-md-3" style="text-align:center;">
                        <label class="checkbox ">
                          <input type="checkbox" class="checkbox-option" value="">
                          <span class="user_yes">Yes</span>
                        </label>
                      </div> <!-- col-md-4 -->

                      <div class="col-md-2">
                        <div class="checkbox">
                          <label><input type="checkbox" class="checkbox-option" value="">
                            <span class="user_yes">No</span>
                          </label>
                           </div> 
                        </div> <!-- col-md-4 -->


                    </div><!-- row-->
         
                    </form>
                  <!-- my radio button  yes or no option end-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-custom btn-md">Invite your team</button>
                  </div>
                </div>
              </div>
             </div>




<script type="text/javascript">
  $(document).ready(function(){
    $('.add-social-link').click(function(){
    $('.new-user-row').append('<div class="row"><div class="col-md-4 col-xs-12"><label for="fullname" class="form-label">Full Name <span class="text-danger">*</span></label><input type="text" class="form-control input-style" name="fullname[]"  id="fullname" required></div><div class="col-md-4 col-xs-12"><label for="Email" class="form-label">Email Address <span class="text-danger">*</span></label><input type="email" class="form-control input-style" name="email[]"  id="email" required></div><div class="col-md-4 col-xs-12"><div class="form-group"><label for="User Role">User Role <span class="text-danger">*</span></label> <select name="user_type_id[]" id="user_type_id" class="form-control e user_type input-style" required>'+
     '<option value="">Select User Role</option>'+
     '<?php foreach ($user_types as $user_type) { ?>'+
     '<option value="<?php echo $user_type['id']; ?>"'+
     '<?php if (isset($user['user_type_id']) && $user['user_type_id'] == $user_type['id']) echo 'selected="SELECTED"'; ?>>'+
     '<?php echo $user_type['value']; ?></option><?php } ?></select></div></div><span class="fa fa-minus-square remove text-danger"></div>');
    });
  });

   $(document).on('click', '.remove', function(){
    $(this).parent('.row').remove();
   });

   $('.do_later').click(function(){
      window.location.href = "<?php echo $this->createUrl('home');?>";
   });

        

// jquery ro see that are inputs are not blank
$('#form_submit').submit(function( event ) {
   //$('#myModal').modal('show');
   event.preventDefault();
});


</script>
<style type="text/css">
   body{background: #ebecf6;}
  .row{margin-left: 0px !important;}
  .container_stetup{width: 700px; margin: auto;}
  .panel-primary {border-color: #357ae8;background: #357ae8;}
  .panel-default{border-color: #ebecf6; border-radius: 10px; box-shadow: 5px 15px 5px rgba(80,102,224,0.08);}
  .invite-team-form{ margin: 8px 10px; padding-top: 27px;}
  .remove{font-size: 31px; margin: 10px 13px;}
  .btn-row{ margin-top: 50px;}
  .do_later{color: #000;background: #ffffff;border-color: #efe7e7;border-radius: 9px;}
  .do_later:hover{color: #000;background: #ffffff;border-color: #efe7e7;border-radius: 9px;}
  .btn-custom{border-radius: 9px; background: #4242c3; color: #fff;}
  .btn-custom:hover{border-radius: 9px; background: #4242c3 !important; color: #fff;}
  .btn-custom:focus{border-radius: 9px; background: #4242c3 !important; color: #fff;}
  /*.form-control{ padding: 20px 5px;}*/
  .input-style{    height: 44px;}
  .checkbox-option{bottom: 4px !important;}
  #myModalLabelAddUser{ font-size: 13px !important; font-weight: 600;}
  .checkbox input[type=checkbox]{ width: 25px !important; height: 22px !important; margin-right: 14px !important; position: absolute !important;}
  .user_yes{padding-left: 14px;  font-size: 19px;}

@media screen and (max-width: 768px) {
   .container_stetup{width: auto !important; margin: auto;}
   .heading1{padding: 10px 0px !important;}
   #myModalLabelAddUser{ font-size: 10px !important; font-weight: 600;}
  .checkbox input[type=checkbox]{ width: 25px !important; height: 22px !important; margin-right: 14px !important; position: absolute !important;}
  .user_yes{padding-left: 14px !important;  font-size: 19px !important;}
  .row{margin-left: 0px !important;}
}
</style>