<div id="module_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:0px;  margin-top: 20px;
    margin-bottom: 20px;">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: 22px; margin-right: 18px; color: #fff">&times;</button>
        <h3 class="modal-title text-center top-h3" >What would you like to do?</h3>
      </div>
      <div class="modal-body" style="padding-top:0px;">
     
     <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
        <div class="panel-body" >
        <div class="create-quote">
      <div class="col-xs-8">
        <h4 class="line-through">Create an eSourcing Activity</h4>
      </div>
          <div class="col-xs-4 pull-right">
            <div class="btn-right pull-right" style="margin-top: 7px;">
             <a href="<?php echo $this->createUrl('quotes/list',array('from'=>'information'));  ?>"  class="btn btn-primary quote"> Create a Quote</a>     
            <a href="https://oboloo.com/support-how-to-create-a-quote/" class="btn btn-warning tutorial" target="_blank"
              >Tutorial</a> 
            </div>
          </div>
         </div>
         </div>       
        </div>
        <!-- ///////////////////--- Contract start   ----///////////////////////// -->
        <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
        <div class="panel-body" >
        <div class="create-quote">
      <div class="col-xs-8">
        <h4 class="line-through">Create a Contract</h4>
      </div>
          <div class="col-xs-4 pull-right">
            <div class="btn-right pull-right" style="margin-top: 7px;">
              <a href="<?php echo $this->createUrl('contracts/list',array('from'=>'information'));  ?>"  class="btn btn-primary contract"   style="padding-right: 7px; padding-left: 7px;">Create a Contract</a>     
             <a href="https://oboloo.com/support-creating-a-contract/" class="btn btn-warning tutorial" target="_blank">Tutorial</a> 
            </div>
          </div>
         </div>
         </div>       
        </div>

         <!-- ///////////////////--- Vendor start   ----///////////////////////// -->
        <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
        <div class="panel-body" >
        <div class="create-quote">
      <div class="col-xs-8">
        <h4 class="line-through">Create a Supplier</h4>
      </div>
          <div class="col-xs-4 pull-right">
            <div class="btn-right pull-right" style="margin-top: 7px;">
              <a href="<?php echo $this->createUrl('vendors/list',array('from'=>'information'));  ?>"  class="btn btn-primary supplier"   style="padding-left: 7px; padding-right: 8px;">Create a Supplier</a>      
              <a href="https://oboloo.com/support-adding-a-supplier/" class="btn btn-warning tutorial" target="_blank">Tutorial</a>   
           </div>
          </div>
         </div>
         </div>       
        </div>

          <!-- ///////////////////--- Saving start   ----///////////////////////// -->
        <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
         <div class="panel-body" >
          <div class="create-quote">
           <div class="col-xs-8">
            <h4 class="line-through">Create a Savings Record</h4>
           </div>
           <div class="col-xs-4 pull-right">
            <div class="btn-right pull-right" style="margin-top: 7px;">
              <a href="<?php echo $this->createUrl('savings/list',array('from'=>'information'));  ?>"  class="btn btn-primary saving">Create a Savings</a>
              <a href="https://oboloo.com/support-adding-a-savings-record/" class="btn btn-warning tutorial" target="_blank">Tutorial</a> 
            </div>
          </div>
         </div>
         </div>       
        </div>

        <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
         <div class="panel-body" >
          <div class="create-quote">
           <div class="col-xs-7">
            <h4 class="line-through">Create Your Default Values - Super Users Only</h4>
           </div>
           <div class="col-xs-5 pull-right">
            <div class="btn-right pull-right" style="margin-top: 7px;">
              <!-- <a href="<?php //echo $this->createUrl('app/defaultValue'); ?>" class="btn btn-primary saving">Create default values</a> -->
               <a href="https://oboloo.com/support-default-values-2/" class="btn btn-warning tutorial" target="_blank">Tutorial</a> 
            </div>
          </div>
         </div>
         </div>       
        </div>

         <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
        <div class="panel-body" >
        <div class="create-quote">
          <div class="col-xs-8">
            <h4 class="line-through">Import Your Suppliers - Super Users Only</h4>
          </div>
          <div class="col-xs-4 pull-right">
            <div class="btn-right pull-right" style="margin-top: 7px;">
             <!-- <a href="<?php //echo $this->createUrl('app/import',array('type'=>'vendor'));  ?>"  class="btn btn-primary quote">Import suppliers</a> -->
             <a href="http://oboloo.com/importrecords" class="btn btn-warning tutorial" target="_blank">Tutorial</a> 
            </div>
         </div>
         </div>       
        </div>
      </div>
      <div class="panel panel-default " style="margin-bottom: 0px; padding: 8px 0px 8px 0px;">
        <div class="panel-body" >
        <div class="create-quote">
          <div class="col-xs-8">
            <h4 class="line-through">Import Your Contracts - Super Users Only</h4>
          </div>
          <div class="col-xs-4 pull-right">
            <div class="btn-right pull-right" style="margin-top: 7px;">
             <!-- <a href="<?php //echo $this->createUrl('app/import',array('type'=>'contract'));  ?>"  class="btn btn-primary quote">Import contracts</a> -->
             <a href="http://oboloo.com/importrecords" class="btn btn-warning tutorial" target="_blank">Tutorial</a> 
            </div>
         </div>
         </div>       
        </div>
      </div>

     </div>
      </div>
    </div>
  </div>


  <style type="">
    .quote, .contract, .supplier, .saving{ background: #1abb9c !important; border-color: #1abb9c !important;}
    .quote:hover , .contract:hover ,.supplier:hover , .saving:hover{background: #1abb9c !important; border-color: #1abb9c !important; }
    .top-h3{  
      padding-top: 20px;
      padding-bottom: 20px;
      border: 1px solid #357aeB !important;
     background-color: #357aeB;
     color: #fff !important;
     border-radius: 30px; 
     box-shadow: 05px 15px 5px rgba(80, 102, 224,0.08);
    }
.panel-body{padding: 0px !important;}
.panel-default{border-color: #ddd; margin-bottom: 5px !important; }
.tutorial{border-radius: 25px;  background-color: #4d90fe !important; border-color: #4d90fe; padding-left: 18px; padding-right: 18px; }
.tutorial:hover{border-radius: 25px;  background-color: #4d90fe !important; border-color: #4d90fe; padding-left: 18px; padding-right: 18px; }

.{padding-left: 7px;
    padding-right: 7px}

.line-through{ margin-top: 12px; font-size: 14px;}
  </style>

<?php 

$sql = "select * from  user_setup where user_id=".Yii::app()->session['user_id'];
    $setup = Yii::app()->db->createCommand($sql)->queryRow();



if(!empty($setup) && $setup['home'] !=1 ){
    $sql = "update user_setup set home=1 where user_id=".Yii::app()->session['user_id'];
    $setup = Yii::app()->db->createCommand($sql)->execute();

?>
<script type="text/javascript">
  $(document).ready(function(){
    $('.quick_module_modal').trigger('click');
  });
</script>
<?php } ?>
