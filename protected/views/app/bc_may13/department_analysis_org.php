<?php
	/*
    $all_categories = array();
	$category_datasets = array();
	
	$all_departments = array();
	$department_labels = "";
	
	foreach ($departments_by_category as $department_index => $data_points)
	{
		$current_dataset = "";
		$current_department_name = "";
		
		foreach ($data_points as $data_point)
		{
			if (!in_array($data_point['category'], $all_categories))
				$all_categories[] = $data_point['category'];

			if (!in_array($data_point['department_name'], $all_departments))
				$all_departments[] = $data_point['department_name'];

			if (!isset($category_datasets[$data_point['category']]))
				$category_datasets[$data_point['category']] = array();
			$category_datasets[$data_point['category']][$data_point['department_name']] = $data_point['total'];
		}
	}
	
	foreach ($all_departments as $current_department_name)
	{
		if ($department_labels == "") $department_labels = "'" . $current_department_name . "'";
		else $department_labels = $department_labels . ", '" . $current_department_name . "'";
	}
	
	$budget_list = $spend_list = "";
	foreach ($top_departments as $department_row)
	{
		if ($budget_list == "") $budget_list = "'" . round($department_row['budget'], 0) . "'";
		else $budget_list = $budget_list . ", '" . round($department_row['budget'], 0) . "'";

		if ($spend_list == "") $spend_list = "'" . round($department_row['total'], 0) . "'";
		else $spend_list = $spend_list . ", '" . round($department_row['total'], 0) . "'";
	}
	*/
?>

          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Budget</span>
				<a style="cursor: pointer;" onclick="$('#department_total_budget_modal').modal('show');"><div class="count" id="count_1"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Total Year Spend</span>
				<a style="cursor: pointer;" onclick="$('#department_total_budget_modal').modal('show');"><div class="count" id="count_2"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Pct Budget Left</span>
				<a style="cursor: pointer;" onclick="$('#department_left_budget_modal').modal('show');"><div class="count" id="count_3"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-list"></i> Avg Suppliers / Dept</span>
				<a style="cursor: pointer;" onclick="$('#department_avg_suppliers_modal').modal('show');"><div class="count" id="count_4"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-list"></i> Avg Suppliers / Ctg</span>
				<a style="cursor: pointer;" onclick="$('#department_avg_categories_modal').modal('show');"><div class="count" id="count_5"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="color: red !important;">
              <span class="count_top"><i class="fa fa-list"></i> Departments Over Budget</span>
				<a style="cursor: pointer;" onclick="$('#department_over_budget_modal').modal('show');"><div class="count" id="count_6"></div></a>
            </div>
          </div>


		<div class="clearfix"><br /></div>
		
			<div class="row">
                 <div class="col-md-offset-4 col-md-4">
                 		<select name="location_id" id="location_id" class="form-control" onchange="createLocationChart(1); loadDepartmentStats();">
                 			<?php foreach ($locations as $location) { ?>
                				<option value="<?php echo $location['location_id']; ?>"><?php echo $location['location_name']; ?></option>
                			<?php } ?>
                		</select>
                 </div>
			</div>
		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_34">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Department Budget vs Spend</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(34);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportDepartmentChart('budgets_by_department');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="budgets_by_department" style="position: relative; height:40vh; width:80vw"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_34">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Department Budgets vs Spent</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(34);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('budgets_by_department');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<tr>
										<thead>
										<th></th>
										<th>Department</th>
										<th style="text-align: center;">Budget</th>
										<th style="text-align: center;">Spent</th>
										</thead>
									</tr>
									<tbody id="department_budget_spent_table_ajax_tbody">
									</tbody>
								</table>
							</div>
						</div>
					</div>


				</div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_35">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Categories By Departments</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(35);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportDepartmentChart('vendors_by_departments');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content" >
                        		<canvas id="vendors_by_department" style="position: relative; height:40vh; width:80vw"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_35">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Categories By Departments</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(35);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportDepartmentChart('vendors_by_departments');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<tr>
										<thead>
										<th></th>
										<th>Department</th>
										<th>Category</th>
										<th style="text-align: center;">Total</th>
										</thead>
									</tr>
									<tbody id="department_categories_table_ajax_tbody">
									</tbody>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_36">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Departments Spend with Supplier Count</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(36);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportDepartmentChart('department_vendors_combo');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="department_vendors_combo" style="position: relative; height:40vh; width:80vw"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_36">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Departments Spend with Supplier Count</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(36);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportDepartmentChart('vendors_by_departments');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<tr>
										<thead>
										<th></th>
										<th>Department</th>
										<th style="text-align: center;">Count</th>
										<th style="text-align: center;">Total</th>

										</thead>
									</tr>
									<tbody id="department_spent_count_table_ajax_tbody">
									</tbody>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

				<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_48">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Departments</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(48);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportDepartmentChart('spend_departments');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="spend_departments" height="157px"></canvas>
			              	</div>
			           	</div>
			        	</div>

						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_48">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Top Departments</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(48);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportDepartmentChart('spend_departments');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
					            			<tr>
						            			<thead>
						            				<th></th>
						            				<th>Department</th>
						            				<th style="text-align: center;">% across Total</th>
						            				<th style="text-align: center;">Amount Spent</th>
						            			</thead>
					            			</tr>
					            			<tbody id="top_categories_table_ajax_tbody">
					                        </tbody>
				                        </table>
			              </div>
			           </div>
			         </div>

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_49">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Preferred vs. Non Preferred Suppliers</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart(49);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportDepartmentChart('supplier_type_area');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="supplier_type_area" height="157px"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_49">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Preferred vs. Non Preferred Suppliers</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(49);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportDepartmentChart('supplier_type_area');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<tr>
										<thead>
										<th></th>
										<th>Department</th>
										<th style="text-align: center;">Preferred</th>
										<th style="text-align: center;">Non Preferred</th>
										</thead>
									</tr>
									<tbody id="supplier_type_area_table_ajax_tbody">
									</tbody>
								</table>
							</div>
						</div>
					</div>

			        </div>

		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>


<input type="hidden" name="user_selected_currency_symbol" id="user_selected_currency_symbol"
		value="<?php echo Yii::app()->session['user_currency_symbol']; ?>" />


<script type="text/javascript">

var category_chart_data;
var category_chart_settings;
var category_chart;

var budget_chart_data;
var budget_chart_settings;
var budget_chart;

var vendor_chart_data;
var vendor_chart_settings;
var vendor_chart;

var td_chart;
var td_chart_settings;

var supplier_chart;
var supplier_chart_config;

function createLocationChart(changed_period)
{
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('departments/getDepartmentChartData'); ?>',
        type: 'POST', dataType: 'json',
        data: { location_id: $('#location_id').val(), user_selected_year: $('#user_selected_year').val() },
        success: function(department_chart_data) {

        	$('#count_1').html($('#user_selected_currency_symbol').val() + department_chart_data.top_metrics.total_budget);
        	$('#count_2').html($('#user_selected_currency_symbol').val() + department_chart_data.top_metrics.spend_total);
        	$('#count_3').html(department_chart_data.top_metrics.pct_budget_used + '%');
        	$('#count_4').html(department_chart_data.top_metrics.avg_per_department);
        	$('#count_5').html(department_chart_data.top_metrics.avg_per_category);
        	$('#count_6').html(department_chart_data.top_metrics.departments_over_budget);

			var data_points = new Array();
			var all_categories = new Array();
			var all_departments = new Array();
			var category_datasets = new Object();

			var category_name = "";
			var department_name = "";
			var category_data_found = false;
			var td_table_html_dc = "";
			var flag = 0;
            for (var i=0; i<department_chart_data.departments_by_category.length; i++) {

   	        	category_name = $.trim(department_chart_data.departments_by_category[i].category);
           		if (all_categories.indexOf(category_name) < 0) all_categories.push(category_name);

            	department_name = $.trim(department_chart_data.departments_by_category[i].department_name);
           		if (all_departments.indexOf(department_name) < 0) all_departments.push(department_name);

           		category_data_found = false;
           		for (var key in category_datasets)
           		{
           			if (key == category_name)
           			{
           				category_data_found = true;
           				break;
           			}

           		}

           		if (category_data_found == false) category_datasets[category_name] = new Object();
           		category_datasets[category_name][department_name] = department_chart_data.departments_by_category[i].total;

				if (i == 1) backgroundColor = "rgba(220,220,220,0.5)";
				else if (i == 2) backgroundColor = "rgba(151,220,187,0.5)";
				else if (i == 3) backgroundColor = "rgba(187,220,151,0.5)";
				else if (i == 4) backgroundColor = "rgba(220,151,187,0.5)";
				else if (i == 5) backgroundColor = "rgba(220,187,151,0.5)";
				else if (i == 6) backgroundColor = "rgba(187,151,220,0.5)";
				else if (i == 7) backgroundColor = "rgba(120,151,187,0.5)";
				else if (i == 8) backgroundColor = "rgba(151,120,187,0.5)";
				else if (i == 9) backgroundColor = "rgba(187,120,151,0.5)";
				else if (i == 10) backgroundColor = "rgba(151,187,120,0.5)";
				else backgroundColor = "rgba(151,187,205,0.5)";



				   if (flag != department_name){
					   flag =1;
				   }


                    if(flag==0){
						td_table_html_dc += '<tr>';
						td_table_html_dc += '<td style="vertical-align: middle;" colspan="3">' + $.trim(department_chart_data.departments_by_category[i].department_name) + '</td>';
						td_table_html_dc += '</tr>';
					}

					td_table_html_dc += '<tr>';
					td_table_html_dc += '<td style="width: 5%;"><i class="fa fa-square" style="color: ' + backgroundColor + ' !important;"></i></td>';
				    td_table_html_dc += '<td style="vertical-align: middle;">' + $.trim(department_chart_data.departments_by_category[i].department_name) + '</td>';

				    td_table_html_dc += '<td style="vertical-align: middle;">' + $.trim(department_chart_data.departments_by_category[i].category) + '</td>';
					td_table_html_dc += '<td style="vertical-align: middle; text-align: center;"><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + department_chart_data.departments_by_category[i].total + '</td>';
					td_table_html_dc += '</tr>';

                    flag = department_name;
           	}

			$('#department_categories_table_ajax_tbody').html(td_table_html_dc);

            var category_chart_datasets = new Array();
            var backgroundColor = "";


            for (var i=0; i<all_categories.length; i++) {

				if (i == 1) backgroundColor = "rgba(220,220,220,0.5)";
				else if (i == 2) backgroundColor = "rgba(151,220,187,0.5)";
				else if (i == 3) backgroundColor = "rgba(187,220,151,0.5)";
				else if (i == 4) backgroundColor = "rgba(220,151,187,0.5)";
				else if (i == 5) backgroundColor = "rgba(220,187,151,0.5)";
				else if (i == 6) backgroundColor = "rgba(187,151,220,0.5)";
				else if (i == 7) backgroundColor = "rgba(120,151,187,0.5)";
				else if (i == 8) backgroundColor = "rgba(151,120,187,0.5)";
				else if (i == 9) backgroundColor = "rgba(187,120,151,0.5)";
				else if (i == 10) backgroundColor = "rgba(151,187,120,0.5)";
				else backgroundColor = "rgba(151,187,205,0.5)";

				var current_category_data = new Array();
				var department_value = 0;

           		for (var j=0; j<all_departments.length; j++) {
           			department_value = 0;

           			for (var department_name in category_datasets[all_categories[i]])
           			{
           				if ($.trim(department_name) == $.trim(all_departments[j]))
           				{
           					department_value = category_datasets[all_categories[i]][department_name];
           					break;
           				}
           			}
           			current_category_data.push(department_value);
           		}

				category_chart_datasets[i] = new Object();
				category_chart_datasets[i].label = all_categories[i];
				category_chart_datasets[i].backgroundColor = backgroundColor;
           		category_chart_datasets[i].data = current_category_data;
			}


			category_chart_settings = {
				type: 'bar',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: all_departments,
					datasets: category_chart_datasets
				},
				options: {
					responsive: true,
					maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							label: function(tooltipItem, data) {
								var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								var prefix = data.datasets[tooltipItem.datasetIndex].label;
							    if (parseInt(value) >= 1000){
							    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							    } else {
							    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
							    }
							}
						}
					},
		            scales: {
		            	xAxes: [{ stacked: true }],
		            	yAxes: [{ stacked: true,
								  ticks: {
				            				min: 0,
											callback: function(value, index, values) {
			              						if (parseInt(value) >= 1000) {
			                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			              						} else {
			                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
			              						}
				            				}
				            		}
			            		}]
		            }
				}
			};

			if (changed_period == 1) category_chart.destroy();
			category_chart = new Chart($('#vendors_by_department'), category_chart_settings);

            var bc_chart_labels = new Array();
            var bc_budgets = new Array();
            var bc_spends = new Array();
			var td_table_html_bs = "";

			var td_chart_colors_bs = new Array();

			var maxValue = 0;

            for (var i=0; i<department_chart_data.budget_data.length; i++) {
            	bc_chart_labels[i] = department_chart_data.budget_data[i].department_name;
            	bc_budgets[i] = department_chart_data.budget_data[i].budget;
            	bc_spends[i] = department_chart_data.budget_data[i].spend;

            	if (department_chart_data.budget_data[i].spent > maxValue)
            		maxValue = department_chart_data.budget_data[i].spent;
            	if (department_chart_data.budget_data[i].budget > maxValue)
            		maxValue = department_chart_data.budget_data[i].budget;

				if (i == 1) td_chart_colors_bs[i] = '#3498DB';
				else if (i == 2) td_chart_colors_bs[i] = '#9B59B6';
				else if (i == 3) td_chart_colors_bs[i] = '#1ABB9C';
				else if (i == 4) td_chart_colors_bs[i] = '#00CED1';
				else if (i == 5) td_chart_colors_bs[i] = '#E74C3C';
				else if (i == 6) td_chart_colors_bs[i] = '#FAEBD7';
				else if (i == 7) td_chart_colors_bs[i] = '#7FFFD4';
				else if (i == 8) td_chart_colors_bs[i] = '#FFE4C4';
				else if (i == 9) td_chart_colors_bs[i] = '#BDB76B';
				else td_chart_colors_bs[i] = '#FFA07A';

				td_table_html_bs += '<tr>';
				td_table_html_bs += '<td style="width: 5%;"><i class="fa fa-square" style="color: ' + td_chart_colors_bs[i] + ' !important;"></i></td>';
				td_table_html_bs += '<td style="vertical-align: middle;">' + department_chart_data.budget_data[i].department_name + '</td>';
				td_table_html_bs += '<td style="text-align: center;vertical-align: middle;"><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + department_chart_data.budget_data[i].budget + '</td>';
				td_table_html_bs += '<td style="text-align: center;vertical-align: middle;"><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + department_chart_data.budget_data[i].spend + '</td>';
				td_table_html_bs += '</tr>';
            }

			$('#department_budget_spent_table_ajax_tbody').html(td_table_html_bs);

			budget_chart_settings = {
				type: 'bar',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: bc_chart_labels,
					datasets: [
							{ label : 'Budget', backgroundColor: "rgba(151,120,187,0.5)", data: bc_budgets },
							{ label : 'Spent', backgroundColor: "rgba(120,187,151,0.5)", data: bc_spends }
						]
				},
				options: {
					responsive: true,
					maintainAspectRatio: false,
					tooltips: {
						callbacks: {
							label: function(tooltipItem, data) {
								var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							    if (parseInt(value) >= 1000){
							    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							    } else {
							    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
							    }
							}
						}
					},
		            scales: {
		            	yAxes: [{ ticks: {
				            		    min: 0, max: 0,
										callback: function(value, index, values) {
			              					if (parseInt(value) >= 1000) {
			                					return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			              					} else {
			                					return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
			              					}
				            			}
				            		}
			            		}]
		            }
				}
			};

			if (changed_period == 1) budget_chart.destroy();
			if (maxValue != 0)  {
				var maxValue = parseInt(maxValue)+parseInt(10000);
				budget_chart_settings['options']['scales']['yAxes'][0]['ticks']['max']= maxValue;
			}
			else {

				if($('#location_id').val()==5 ) {
					if(department_chart_data.budget_data.length>0){
						var amsterdam = parseInt(department_chart_data.budget_data[0].spend)+parseInt(3000);
						budget_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'] = amsterdam;
					} else{

						budget_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'] = '';
					}

				} else if ($('#location_id').val()==1) {
					var madrid = parseInt(department_chart_data.budget_data[0].spend)+parseInt(3000);
					budget_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'] = madrid;
				}

			}
			budget_chart = new Chart($('#budgets_by_department'), budget_chart_settings);
//			if (changed_period == 1) budget_chart.destroy();
//			if (maxValue != 0) delete budget_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'];
//			else budget_chart_settings['options']['scales']['yAxes'][0]['ticks']['max'] = 55000;
//			budget_chart = new Chart($('#budgets_by_department'), budget_chart_settings);

			var td_table_html = "";
			var color = "";

            var td_chart_labels = new Array();
            var td_chart_colors = new Array();
            var td_chart_data = new Array();

            for (var i=0; i<department_chart_data.top_departments.length; i++) {

				if (i == 1) td_chart_colors[i] = '#3498DB';
				else if (i == 2) td_chart_colors[i] = '#9B59B6';
				else if (i == 3) td_chart_colors[i] = '#1ABB9C';
				else if (i == 4) td_chart_colors[i] = '#00CED1';
				else if (i == 5) td_chart_colors[i] = '#E74C3C';
				else if (i == 6) td_chart_colors[i] = '#FAEBD7';
				else if (i == 7) td_chart_colors[i] = '#7FFFD4';
				else if (i == 8) td_chart_colors[i] = '#FFE4C4';
				else if (i == 9) td_chart_colors[i] = '#BDB76B';
				else td_chart_colors[i] = '#FFA07A';

				td_chart_labels[i] = department_chart_data.top_departments[i].department_name;
				td_chart_data[i] = department_chart_data.top_departments[i].total.replace(/,/g, "");

				td_table_html += '<tr>';
				td_table_html += '<td style="width: 5%;"><i class="fa fa-square" style="color: ' + td_chart_colors[i] + ' !important;"></i></td>';
				td_table_html += '<td style="vertical-align: middle;">' + department_chart_data.top_departments[i].department_name + '</td>';
				td_table_html += '<td style=" text-align: center;vertical-align: middle;">' + department_chart_data.top_departments[i].percent + '%</td>';
				td_table_html += '<td style=" text-align: center;vertical-align: middle;"><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + department_chart_data.top_departments[i].total + '</td>';
				td_table_html += '</tr>';
			}

			$('#top_categories_table_ajax_tbody').html(td_table_html);

			td_chart_settings = {
				type: 'pie',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: td_chart_labels,
					datasets: [{
						data: td_chart_data,
						backgroundColor: td_chart_colors
					}]
				},
				options: {
							legend: false, responsive: true,

						    tooltips: {
								callbacks: {
									label: function(tooltipItem, data) {
										var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										var prefix = data.labels[tooltipItem.index];
									    if (parseInt(value) >= 1000){
									    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									    } else {
									    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
									    }
									}
								}
							}
				}
			};

			if (changed_period == 1) td_chart.destroy();
			td_chart = new Chart($('#spend_departments'), td_chart_settings);

            var vendor_chart_labels = new Array();
            var vendor_chart_counts = new Array();
            var vendor_chart_totals = new Array();
            var td_chart_colors_dsc = new Array();
            var td_table_html_dsc = "";

            for (var i=0; i<department_chart_data.department_vendors_combo.length; i++) {
            	vendor_chart_labels[i] = department_chart_data.department_vendors_combo[i].department_name;
            	vendor_chart_counts[i] = department_chart_data.department_vendors_combo[i].vendor_count;
            	vendor_chart_totals[i] = department_chart_data.department_vendors_combo[i].vendor_total;

				if (i == 1) td_chart_colors_dsc[i] = '#3498DB';
				else if (i == 2) td_chart_colors_dsc[i] = '#9B59B6';
				else if (i == 3) td_chart_colors_dsc[i] = '#1ABB9C';
				else if (i == 4) td_chart_colors_dsc[i] = '#00CED1';
				else if (i == 5) td_chart_colors_dsc[i] = '#E74C3C';
				else if (i == 6) td_chart_colors_dsc[i] = '#FAEBD7';
				else if (i == 7) td_chart_colors_dsc[i] = '#7FFFD4';
				else if (i == 8) td_chart_colors_dsc[i] = '#FFE4C4';
				else if (i == 9) td_chart_colors_dsc[i] = '#BDB76B';
				else td_chart_colors_dsc[i] = '#FFA07A';

				td_table_html_dsc += '<tr>';
				td_table_html_dsc += '<td style="width: 5%;"><i class="fa fa-square" style="color: ' + td_chart_colors_dsc[i] + ' !important;"></i></td>';
				td_table_html_dsc += '<td style="vertical-align: middle;">' + department_chart_data.department_vendors_combo[i].department_name + '</td>';
				td_table_html_dsc += '<td style="text-align: center; vertical-align: middle;">' + department_chart_data.department_vendors_combo[i].vendor_count + '</td>';
				td_table_html_dsc += '<td style="text-align: center; vertical-align: middle;"><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + department_chart_data.department_vendors_combo[i].vendor_total + '</td>';
				td_table_html_dsc += '</tr>';
			}

			$('#department_spent_count_table_ajax_tbody').html(td_table_html_dsc);

			vendor_chart_data = {
				labels: vendor_chart_labels,
				datasets: [

								{
					                type: 'line',
					                label: 'Supplier Count',
					                borderColor: 'purple',
					                borderWidth: 2,
					                fill: false,
					                yAxisID: 'A',
					                data: vendor_chart_counts
								},

								{
					                type: 'bar',
					                label: 'Amount Spent',
					                backgroundColor: 'lightgreen',
					                yAxisID: 'B',
					                data: vendor_chart_totals
								}

					]
				};

			if (changed_period == 1) vendor_chart.destroy();
		    vendor_chart =
		    	new Chart($('#department_vendors_combo'),
		    			{
		                	type: 'bar',
		                	data: vendor_chart_data,
		                	options: {
		                		scaleUse2Y: true,
		                		responsive: true,
								maintainAspectRatio: false,
								tooltips: {
									callbacks: {
										label: function(tooltipItem, data) {
											var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
											if (tooltipItem.datasetIndex == 0) return value;
											else
											{
											    if (parseInt(value) >= 1000){
											    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
											    } else {
											    	return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
											    }
											}
										}
									}
								},
		                		scales: {
		                			xAxes: [{ ticks: { maxRotation: 0, minRotation: 0 } }],
		                			yAxes: [
        		        						{
        		        							id: 'B', type: 'linear', position: 'left',
													ticks: {
		            									min: 0,
														callback: function(value, index, values) {
	              											if (parseInt(value) >= 1000) {
	                											return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              											} else {
	                											return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              											}
		            						   			}
		            						   		}
        		        						},
                								{ id: 'A', type: 'linear', position: 'right', ticks: { min: 0, stepSize: 1 } }
                							]
		                		}
		                	}
		            	}
		        );

			var supplier_chart_department_names = new Array();
			var supplier_chart_preferred_data = new Array();
			var supplier_chart_non_preferred_data = new Array();
			var td_chart_colors_dvt = new Array();
			var td_table_html_dvt = "";

			for (var i=0; i<department_chart_data.department_vendors_type.length; i++)
			{
				supplier_chart_department_names[i] = department_chart_data.department_vendors_type[i].department_name;
				supplier_chart_preferred_data[i] = department_chart_data.department_vendors_type[i].preferred;
				supplier_chart_non_preferred_data[i] = department_chart_data.department_vendors_type[i].non_preferred;

				if (i == 1) td_chart_colors_dvt[i] = '#3498DB';
				else if (i == 2) td_chart_colors_dvt[i] = '#9B59B6';
				else if (i == 3) td_chart_colors_dvt[i] = '#1ABB9C';
				else if (i == 4) td_chart_colors_dvt[i] = '#00CED1';
				else if (i == 5) td_chart_colors_dvt[i] = '#E74C3C';
				else if (i == 6) td_chart_colors_dvt[i] = '#FAEBD7';
				else if (i == 7) td_chart_colors_dvt[i] = '#7FFFD4';
				else if (i == 8) td_chart_colors_dvt[i] = '#FFE4C4';
				else if (i == 9) td_chart_colors_dvt[i] = '#BDB76B';
				else td_chart_colors_dvt[i] = '#FFA07A';

				td_table_html_dvt += '<tr>';
				td_table_html_dvt += '<td style="width: 5%;"><i class="fa fa-square" style="color: ' + td_chart_colors_dvt[i] + ' !important;"></i></td>';
				td_table_html_dvt += '<td style="vertical-align: middle;">' + department_chart_data.department_vendors_type[i].department_name + '</td>';
				td_table_html_dvt += '<td style="text-align: center; vertical-align: middle;">' + department_chart_data.department_vendors_type[i].preferred + '</td>';
				td_table_html_dvt += '<td style="text-align: center; vertical-align: middle;">' + department_chart_data.department_vendors_type[i].non_preferred + '</td>';
				td_table_html_dvt += '</tr>';
			}

			$('#supplier_type_area_table_ajax_tbody').html(td_table_html_dvt);

			supplier_chart_config = {
					type: 'bar',
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: supplier_chart_department_names,
						datasets: [
								{ label: 'Preferred', backgroundColor: "rgba(220,220,220,0.5)", data: supplier_chart_preferred_data },
								{ label: 'Non Preferred', backgroundColor: "rgba(151,187,205,0.5)", data: supplier_chart_non_preferred_data }
							]
					},
					options: {
						responsive: true,
			            scales: {
			            	xAxes: [{ stacked: true }],
			            	yAxes: [{ stacked: true }]
			            }
					}
				};

			if (changed_period == 1) supplier_chart.destroy();
			supplier_chart = new Chart($('#supplier_type_area'), supplier_chart_config);

        }
    });

}

function loadDepartmentStats(changed_period)
{
	$.ajax({
		url: '<?php echo AppUrl::bicesUrl('departments/getDepartmentModalData'); ?>',
		type: 'POST', dataType: 'json',
		data: {
			location_id: $('#location_id').val(), user_selected_year: $('#user_selected_year').val()
		},
		success: function(department_chart_data) {

			var td_table_html_bs = "";
			var td_table_html_bs_left = "";
			var td_table_html_bs_over = "";
			var td_table_html_bs_avg_suppliers = "";
			var td_table_html_bs_avg_categories = "";


			td_table_html_bs += '<table id="order_department-total-budget" class="table table-striped table-bordered" style="width: 100%;"> <thead> <tr> <th>Department</th><th>Budget</th> <th>Spent</th> </tr> </thead> <tbody>';

			for (var i=0; i<department_chart_data.budget_data.length; i++) {


				td_table_html_bs += '<tr>';
				td_table_html_bs += '<td>' + department_chart_data.budget_data[i].department_name + '</td>';
				td_table_html_bs += '<td><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + Math.round(department_chart_data.budget_data[i].budget) + '</td>';
				td_table_html_bs += '<td><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + Math.round(department_chart_data.budget_data[i].spend) + '</td>';
				td_table_html_bs += '</tr>';
			}
			td_table_html_bs += '</tbody></table>';


			td_table_html_bs_left += '<table id="order_department-total-budget" class="table table-striped table-bordered" style="width: 100%;"> <thead> <tr> <th>Department</th><th>Budget</th> <th>Spent</th> <th>Left</th> <th>Left %</th> </tr> </thead> <tbody>';

			for (var i=0; i<department_chart_data.budget_data.length; i++) {

				td_table_html_bs_left += '<tr>';
				td_table_html_bs_left += '<td>' + department_chart_data.budget_data[i].department_name + '</td>';
				td_table_html_bs_left += '<td><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + Math.round(department_chart_data.budget_data[i].budget) + '</td>';
				td_table_html_bs_left += '<td><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + Math.round(department_chart_data.budget_data[i].spend) + '</td>';
				td_table_html_bs_left += '<td><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + Math.round(department_chart_data.budget_data[i].budget - department_chart_data.budget_data[i].spend) + '</td>';
				if(department_chart_data.budget_data[i].budget - department_chart_data.budget_data[i].spend>0) {
					td_table_html_bs_left += '<td>' + Math.round(((department_chart_data.budget_data[i].budget - department_chart_data.budget_data[i].spend)*100)/department_chart_data.budget_data[i].budget) + '%</td>';
				}else {
					td_table_html_bs_left += '<td>0%</td>';
				}
				td_table_html_bs_left += '</tr>';
			}
			td_table_html_bs_left += '</tbody></table>';

			td_table_html_bs_over += '<table id="order_department-total-budget" class="table table-striped table-bordered" style="width: 100%;"> <thead> <tr> <th>Department</th><th>Budget</th> <th>Spent</th> </tr> </thead> <tbody>';

			for (var i=0; i<department_chart_data.budget_data.length; i++) {

                if(department_chart_data.budget_data[i].spend > department_chart_data.budget_data[i].budget){

					td_table_html_bs_over += '<tr>';
					td_table_html_bs_over += '<td>' + department_chart_data.budget_data[i].department_name + '</td>';
					td_table_html_bs_over += '<td><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + Math.round(department_chart_data.budget_data[i].budget) + '</td>';
					td_table_html_bs_over += '<td><?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + Math.round(department_chart_data.budget_data[i].spend) + '</td>';
					td_table_html_bs_over += '</tr>';
				} else {
					td_table_html_bs_over += '<tr>';
					td_table_html_bs_over += '<td colspan="3">None</td>';
					td_table_html_bs_over += '</tr>';
				}

			}
			td_table_html_bs_over += '</tbody></table>';

			td_table_html_bs_avg_suppliers += '<table id="order_department-total-budget" class="table table-striped table-bordered" style="width: 100%;"> <thead> <tr> <th>Department</th><th>Supplier</th> <th>Supplier Count</th> </tr> </thead> <tbody>';

			for (var i=0; i<department_chart_data.avg_suppliers.length; i++) {


					td_table_html_bs_avg_suppliers += '<tr>';
					td_table_html_bs_avg_suppliers += '<td>' + department_chart_data.avg_suppliers[i].department + '</td>';
					td_table_html_bs_avg_suppliers += '<td>' + department_chart_data.avg_suppliers[i].vendor + '</td>';
					td_table_html_bs_avg_suppliers += '<td style="text-align: center;">' + department_chart_data.avg_suppliers[i].vendor_count + '</td>';
					td_table_html_bs_avg_suppliers += '</tr>';


			}
			td_table_html_bs_avg_categories += '</tbody></table>';

			td_table_html_bs_avg_categories += '<table id="order_department-total-budget" class="table table-striped table-bordered" style="width: 100%;"> <thead> <tr> <th>Department</th><th>Supplier</th> <th>Category</th> <th>Supplier Count</th> </tr> </thead> <tbody>';

			for (var i=0; i<department_chart_data.avg_categories.length; i++) {


				td_table_html_bs_avg_categories += '<tr>';
				td_table_html_bs_avg_categories += '<td>' + department_chart_data.avg_categories[i].department + '</td>';
				td_table_html_bs_avg_categories += '<td>' + department_chart_data.avg_categories[i].vendor + '</td>';
				td_table_html_bs_avg_categories += '<td>' + department_chart_data.avg_categories[i].category + '</td>';
				td_table_html_bs_avg_categories += '<td style="text-align: center;">' + department_chart_data.avg_categories[i].vendor_count + '</td>';
				td_table_html_bs_avg_categories += '</tr>';


			}
			td_table_html_bs_avg_categories += '</tbody></table>';

			$('#department-total-budget').html(td_table_html_bs);
			$('#department-left-budget').html(td_table_html_bs_left);
			$('#department-over-budget').html(td_table_html_bs_over);
			$('#department-avg_suppliers').html(td_table_html_bs_avg_suppliers);
			$('#department-avg_categories').html(td_table_html_bs_avg_categories);

		}
	});

}

$(document).ready(function() {

	createLocationChart(0);
	loadDepartmentStats();

});

function exportDepartmentChart(canvas_id)
{
	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id + '&location_id=' + $('#location_id').val();
}


</script>
