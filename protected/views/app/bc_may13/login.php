<script type="text/javascript" src="<?php echo AppUrl::jsUrl('app.js'); ?>"></script>
<div>

	<div class="col-md-3" style="width: 24% !important; margin-left: 38% !important;">
			<br /><br />
          <img class="img-responsive" src="<?php echo AppUrl::bicesUrl('images/app_logo.png'); ?>" />
          <div class="clearfix"> </div>
	</div>
    <div class="clearfix"> </div>
	
  <a class="hiddenanchor" id="signup"></a>
  <a class="hiddenanchor" id="signin"></a>

  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
          <form novalidate id="login_form" name="login_form" method="post" action="<?php echo AppUrl::bicesUrl('login'); ?>">
          <h1>Login Form</h1>
          <div class="item form-group">
            <input id="username" name="username" type="text" class="form-control" placeholder="Username" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="item form-group">
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required="required" />
          </div>
          <div style="width: 350px;">
            <button class="btn btn-default submit" onclick="return checkLogin();">Log in</button>
            <a class="to_forgot" href="<?php echo AppUrl::bicesUrl('app/forgot'); ?>">Lost your password?</a>
            <a class="to_forgot" href="<?php echo AppUrl::bicesUrl('app/changePassword'); ?>">Change Password?</a>
          </div>
          <input type="hidden" name="action_login" id="action_login" value="1" />

          <div class="clearfix"></div>

          <div class="separator">
            <p class="change_link">
              <a href="#signup" class="to_register">New to site? Create Account</a>
            </p>

            <div class="clearfix"></div>
            <br />

          </div>
        </form>
      </section>
    </div>

    <div id="register" class="animate form registration_form">
      <section class="login_content">
          <form novalidate id="register_form" name="register_form" method="post" action="<?php echo AppUrl::bicesUrl('login'); ?>">
          <h1>Create Account</h1>
          <div class="item form-group">
            <input id="new_fullname" name="new_fullname" type="text" class="form-control" placeholder="Full Name" required="required" />
          </div>
          <div class="item form-group">
            <input id="new_username" name="new_username" type="text" class="form-control" placeholder="Username" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="item form-group">
            <input id="new_email" name="new_email" type="email" class="form-control" placeholder="Email" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="item form-group">
            <input id="new_password" name="new_password" type="password" class="form-control" placeholder="Password" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="form-group col-md-7">
            <select name="location_id" id="location_id" class="form-control">
            	<option value="0">Select Location</option>
            	<?php foreach ($locations as $location) { ?>
	            	<option value="<?php echo $location['location_id']; ?>"><?php echo $location['location_name']; ?></option>
            	<?php } ?>
            </select>
          </div>
          <div class="clearfix"></div>
          <div style="width: 350px;">
            <button class="btn btn-default submit" onclick="return checkRegistration();">Submit</button>
          </div>
          <input type="hidden" name="action_register" id="action_register" value="1" />

          <div class="clearfix"></div>

          <div class="separator">
            <p class="change_link">
              <a href="#signin" class="to_register">Already a member? Log in</a>
            </p>

            <div class="clearfix"></div>
            <br />

          </div>
        </form>
      </section>
    </div>

    <?php if (isset($register_success) && $register_success == 1) { ?>
        <div id="message_area">
            You have successfully registered. Please contact your administrator to activate your account.
        </div>
    <?php } ?>

    <?php if (isset($error) && $error == 1) { ?>
        <div id="message_area" style="color: red;">
            Invalid username/password combination specified.<br />Please try again.
            <br /><br />
        </div>
    <?php } ?>

    <?php if (isset($error) && $error == 2) { ?>
        <div id="message_area" style="color: red;">
            Your account has not been activated as yet.<br />Please contact your administrator.
            <br /><br />
        </div>
    <?php } ?>

    <div id="status_area">
    </div>
    <div class="clearfix"></div>


  </div>
</div>

<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Fix Registration Errors</h4>
        </div>
        <div class="modal-body">
          <p id="registration_errors"></p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          	  OK
          </button>
        </div>
      </div>
	</div>
</div>

