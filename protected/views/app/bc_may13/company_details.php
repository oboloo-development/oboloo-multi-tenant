<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>
            	Edit Company Details
            </h3>
        </div>
        <div class="clearfix"> </div>
    </div>

<div class="row tile_count">
  <form id="company_form" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('app/companyDetails'); ?>">
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">Company Name</label>
      </div><div class="clearfix"></div>
          <div class="col-md-6 col-sm-6 col-xs-12 valid">
            
              <input type="text" class="form-control has-feedback-left" name="company_name" id="company_name"
              <?php if (isset($company_details['company_name']) && !empty($company_details['company_name'])) echo 'value="' . $company_details['company_name'] . '"'; else echo 'placeholder="Company Name"'; ?> >
              <span class="fa fa-institution form-control-feedback left" aria-hidden="true"></span>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Company Number</label>
              <input type="text" class="form-control" name="company_number" id="company_number"
              <?php if (isset($company_details['company_number']) && !empty($company_details['company_number'])) echo 'value="' . $company_details['company_number'] . '"'; else echo 'placeholder="Company Registration Number"'; ?> >
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Company Fax</label>
              <input type="text" class="form-control" name="tax_number" id="tax_number"
              <?php if (isset($company_details['tax_number']) && !empty($company_details['tax_number'])) echo 'value="' . $company_details['tax_number'] . '"'; else echo 'placeholder="Company Tax ID Number"'; ?> >
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Phone Number</label>
              <input type="text" class="form-control" name="phone_number" id="phone_number"
                  <?php if (isset($company_details['phone_number']) && !empty($company_details['phone_number'])) echo 'value="' . $company_details['phone_number'] . '"'; else echo 'placeholder="Company Phone Number"'; ?> >
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6">
              <label class="control-label">Company Website</label>
              <input type="text" class="form-control" name="company_website" id="company_website"
                  <?php if (isset($company_details['company_website']) && !empty($company_details['company_website'])) echo 'value="' . $company_details['company_website'] . '"'; else echo 'placeholder="Company Website"'; ?> >
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Company Logo</label>
              <input class="file" name="file" id="file" type="file" />
          </div>
          <div class="clearfix"> <br /> </div>
      </div>

      <div class="form-group">
    <div class="clearfix"> <br /> </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="hidden" name="form_submitted" id="form_submitted" value="1" />

    <a href="javascript:void(0);" onclick="$('#company_form').submit();">
        <button type="button" class="btn btn-primary">
            Save Company
        </button>
    </a>
    </div>
    </div>

  </form>
</div>
</div>


<script type="text/javascript">
$(document).ready( function() {
    $( "#company_form" ).validate( {
        rules: {
            company_name: "required"
        },
        messages: {
            company_name: "Company Name is required"
        },
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-error" ).removeClass( "has-success" );
			$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-6" ).addClass( "has-success" ).removeClass( "has-error" );
			$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });
});

</script>
