<?php
	$tool_currency = Yii::app()->session['user_currency'];
	$sparklineData = '';
    // START: Spartk13
    $spark13Exp = '';
    error_reporting(0);
    $orderSpendTotalAmount =0; $monthlyData = array();
    if(count($order_spend_stats)>0)
    {
        foreach ($order_spend_stats as $order)
        {
            $monthlyData[$order['month_year']] = $monthlyData[$order['month_year']]+$order['total_amount'];
            
        }

        foreach ($monthlyData as $key=>$orderValue)
        {
            $orderSpendTotalAmount += $orderValue;
            $spark13Exp .= '{ x: "'.$key.'",y: '.$orderValue.'},';
        }
    }
    // END: Spartk13

    // START: Spark14 Order count
    $spark14Exp= '';
    $totalOrders=0;
    if(!empty($order_count))
    {
        foreach ($order_count as $order)
        {
            $totalOrders += $order['total_orders'];
            $spark14Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_orders'].'},';
        }
    }
    // END: Spartk14 Order count
    // START: Spark15 Order count
    $spark15Exp = '';
    $totalToApprovOrders =0;
    if(count($order_spend_approve_stats)>0)
    {
        foreach ($order_spend_approve_stats as $order)
        {
            $totalToApprovOrders += $order['total_amount'];
            $spark15Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_amount'].'},';
        }
        $spark15Exp = rtrim($spark15Exp,",");
    }
    // END: Spartk15 Order to approve count

    // START: Spark16 Order count
    $spark16Exp= '';
    $totalPendingOrders=0;
    if(!empty($order_pending_count))
    {
        foreach ($order_pending_count as $order)
        {
            $totalPendingOrders += $order['total_orders'];
            $spark16Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_orders'].'},';
        }
    }
    // END: Spartk16 Order count

    // START: Spark17 Order count
    $spark17Exp= '';
    $totalPaiedAmount=0;
    if(!empty($order_spend_paid_stats))
    {
        foreach ($order_spend_paid_stats as $order)
        {
            $totalPaiedAmount += $order['total_amount'];
            $spark17Exp .= '{ x: "Order #'.$order['order_id'].'",y: '.$order['total_amount'].'},';
        }
    }
    // END: Spartk17 Order count

    // START: Spark18 Order count
    $spark18Exp= '';
    $ordersToReceive=0;
    if(!empty($order_count))
    {
        foreach ($order_to_receive as $order)
        {
            $ordersToReceive += $order['total_orders'];
            $spark18Exp .= '{ x: "'.$order['month_year'].'",y: '.$order['total_orders'].'},';
        }
    }
    // END: Spartk18 Order count

	$department_labels = $order_data = $expense_data = "";
	foreach ($departments_by_spend_type as $data_points)
	{
		$current_department_name = "";
		$orders = $expenses = 0;
		
		foreach ($data_points as $data_point)
		{
			$current_department_name = $data_point['location_name'];
			if ($data_point['type'] == 'E') $expenses += round($data_point['total'], 2);
			else $orders += round($data_point['total'], 2);
		}

		if ($department_labels == "") $department_labels = "'" . $current_department_name . "'";
		else $department_labels = $department_labels . ", '" . $current_department_name . "'";

		if ($expense_data == "") $expense_data = $expenses;
		else $expense_data = $expense_data . ',' . $expenses;

		if ($order_data == "") $order_data = $orders;
		else $order_data = $order_data . ',' . $orders;
	}

	$status_labels = "";
	$status_data = "";
	$max_data = 0;
	foreach ($order_statuses as $order_status => $total_orders)
	{
		if ($status_labels == "") $status_labels = "'" . $order_status . "'";
		else $status_labels = $status_labels . ", '" . $order_status . "'";
		
		if ($status_data == "") $status_data = $total_orders;
		else $status_data = $status_data . "," . $total_orders;
		
		if ($max_data < $total_orders) $max_data = $total_orders;
	}
	
	if ($max_data == 0) $max_data = 1;
	else if ($max_data == 1) $max_data = 2;
	else if ($max_data < 5) $max_data = 5;
	else if ($max_data < 10) $max_data = 10;
	else if ($max_data < 20) $max_data = 20;
	else if ($max_data < 50) $max_data = 50;
	else if ($max_data < 100) $max_data = 100;
	else if ($max_data < 200) $max_data = 200;
	else if ($max_data < 500) $max_data = 500;
	else if ($max_data < 1000) $max_data = 1000;

?>

          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6">
            	 <a style="cursor: pointer;" onclick="$('#total_year_spend_modal').modal('show');"><div id="spark13"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
              <a style="cursor: pointer;" onclick="$('#total_year_ordercount_modal').modal('show');"><div id="spark14"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
              <a style="cursor: pointer;" onclick="$('#approved_spend_modal').modal('show');"><div id="spark15"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
              <a style="cursor: pointer;" onclick="$('#approved_ordertoapprove_modal').modal('show');"><div id="spark16"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
             <a style="cursor: pointer;" onclick="$('#paid_spend_modal').modal('show');"><div id="spark17"></div></a>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6">
            	<a style="cursor: pointer;" onclick="$('#received_spend_modal').modal('show');"><div id="spark18"></div></a>
             
            </div>
          </div>


		<div class="clearfix"><br /></div>

				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" id="chart_area_30">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Locations By Procurement Orders vs. Travel/Expenses</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart1(30);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('departments_by_spend_type');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="departments_by_spend_type" style="position: relative; height:40vh; width:80vw"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-12 col-sm-12 col-xs-12" style="display: none;" id="table_area_30">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Locations By Procurement Orders vs. Travel/Expenses</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(30);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('departments_by_spend_type');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php
									$idx = 0;
									foreach ($departments_by_spend_type as $data_index =>$data_points)
									{
										$current_department_name = "";
									    $orders = $expenses = 0;

										if ($data_index === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}

										?>

										<tr>
											<td style="font-weight: bold;">
												<i class="fa fa-square <?php echo $color; ?>" style="margin-left: 10px;margin-top:-4px;"></i>
												<?php echo $data_points[0]['location_name']; ?></td>

										</tr>

										<?php

										foreach ($data_points as $data_point) {
											$current_department_name = $data_point['location_name'];
											if ($data_point['type'] == 'E') $expenses += round($data_point['total'], 2);
											else $orders += round($data_point['total'], 2);
										}
											?>
											<tr>
												<td>
													<table class="tile_info" style="margin-left: 0px;">
														<tr>
															<td style="width: 5%;">
																<i  style="margin-left: 35px;background-color:#E1E1E1;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
															</td>
															<td >
																<?php echo 'Procurement Orders'; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' .$orders; ?>
															</td>
														</tr>
														<tr>
															<td style="width: 5%;">
																<i  style="margin-left: 35px;background-color:#B7D8E8;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
															</td>
															<td>
																<?php echo 'Travel/Expenses'; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' . $expenses; ?>
															</td>
														</tr>
														<tr><td colspan="2" style="height: 10px;">&nbsp;</td></tr>
													</table>
												</td>
											</tr>
											<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

				<div class="row">

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_32">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Preferred Vs. Non Preferred Suppliers Used</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								<li><a onclick="expandChart(32);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('vendors_by_type');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="vendors_by_type" height="140px"></canvas>
			              	</div>
			           	  </div>
			        	</div>

					    <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_32">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Preferred Vs. Non Preferred Suppliers Used</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(32);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('vendors_by_type');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
										<tr>
											<td style="width: 5%;"><i class="fa fa-square" style="color: #007613;"></i></td>
											<td style="vertical-align: middle;">
												Preffered - <?php echo round($preferred_vendors_used, 2); ?>
											</td>
										</tr>

									   <tr>
										<td style="width: 5%;"><i class="fa fa-square" style="color: #E4941B"></i></td>
										<td style="vertical-align: middle;">
											Non Preffered - <?php echo number_format($non_preferred_vendors_used, 2); ?>
										</td>
									   </tr>

								</table>
							</div>
						</div>
					</div>

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_33">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Invoices Paid vs. Unpaid</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart(33);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('invoices_paid_unpaid');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="invoices_paid_unpaid" height="140px"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_33">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Invoices Paid vs. Unpaid</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(33);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('vendors_by_type');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<tr>
										<td style="width: 5%;"><i class="fa fa-square" style="color: #59FF68;"></i></td>
										<td style="vertical-align: middle;">
											Paid - <?php echo $invoices_paid_unpaid['paid']; ?>
										</td>
									</tr>

									<tr>
										<td style="width: 5%;"><i class="fa fa-square red"></i></td>
										<td style="vertical-align: middle;">
											Unpaid - <?php echo $invoices_paid_unpaid['unpaid']; ?>
										</td>
									</tr>

								</table>
							</div>
						</div>
					</div>

		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

				<div class="row">

						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_28">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Spend by Location - Total vs. Future</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
								  <li><a onclick="expandChart(28);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('locations_invoice_status');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
                        		<canvas id="locations_invoice_status" width="340px"></canvas>
			              	</div>
			           	</div>
			        	</div>

					<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_28">
						<div class="x_panel tile overflow_hidden">
							<div class="x_title">
								<h2>Spend by Location - Total vs. Future</h2>
								<ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
									<li><a onclick="collapseChart(28);"><i class="fa fa-arrows-h"></i></a></li>
									<li><a onclick="exportChart('locations_invoice_status');"><i class="fa fa-cloud-download"></i></a></li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table class="tile_info" style="margin-left: 0px;">
									<?php

									$idx = 0;
									foreach ($location_purchases as $data_index =>$data_location_purchases)
									{

										if ($data_index === 'total') continue;
										$idx += 1;
										switch ($idx)
										{
											case 1  : $color = 'blue'; break;
											case 2  : $color = 'purple'; break;
											case 3  : $color = 'green'; break;
											case 4  : $color = 'turquoise'; break;
											case 5  : $color = 'red'; break;
											case 6  : $color = 'antique-white'; break;
											case 7  : $color = 'aqua-marine'; break;
											case 8  : $color = 'bisque'; break;
											case 9  : $color = 'khaki'; break;
											default : $color = 'salmon'; break;
										}

										?>

										<tr>
											<td style="font-weight: bold;">
												<i class="fa fa-square <?php echo $color; ?>" style="margin-left: 10px;margin-top:-4px;"></i>
												<?php echo $data_location_purchases['location_name']; ?></td>

										</tr>


										<tr>
											<td>
												<table class="tile_info" style="margin-left: 0px;">
													<tr>
														<td style="width: 5%;">
															<i  style="margin-left: 35px;background-color:#E1E1E1;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
														</td>
														<td >
															<?php echo 'Total'; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' .number_format($data_location_purchases['spend_total'], 2); ?>
														</td>
													</tr>
													<tr>
														<td style="width: 5%;">
															<i  style="margin-left: 35px;background-color:#B7D8E8;height: 10px;width: 10px;border-radius: 2px;">&nbsp;</i>
														</td>
														<td>
															<?php echo 'Future'; ?> - <?php echo Yii::app()->session['user_currency_symbol'] . '' . number_format($data_location_purchases['spend_future'], 2); ?>
														</td>
													</tr>
													<tr><td colspan="2" style="height: 10px;">&nbsp;</td></tr>
												</table>
											</td>
										</tr>
										<?php
									}
									?>
								</table>
							</div>
						</div>
					</div>


						<div class="col-md-6 col-sm-6 col-xs-12" id="chart_area_29">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Order Totals By Status</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="expandChart(29);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('orders_by_status');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
		                      	<canvas id="orders_by_status" width="340px"></canvas>
			              	</div>
			           	</div>
			        	</div>
						<div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" id="table_area_29">
			              <div class="x_panel tile overflow_hidden">
			                <div class="x_title">
			                  <h2>Order Totals By Status</h2>
			                  <ul class="nav navbar-right panel_toolbox" style="min-width: 20px !important;">
			                    <li><a onclick="collapseChart(29);"><i class="fa fa-arrows-h"></i></a></li>
			                    <li><a onclick="exportChart('orders_by_status');"><i class="fa fa-cloud-download"></i></a></li>
			                  </ul>
			                  <div class="clearfix"></div>
			                </div>
			                <div class="x_content">
					            		<table class="tile_info" style="margin-left: 0px;">
						           			<?php
						           				$idx = 0;
						           				foreach (explode(",", $status_labels) as $a_status)
												{
													$a_status = str_replace("'", "", $a_status);
													$a_status = rtrim(ltrim($a_status));
													$idx += 1;
																	
													switch ($idx)
													{
														case 1  : $color = "#F7464A"; break;
														case 2  : $color = "#46BFBD"; break;
														case 3  : $color = "#FDB45C"; break;
														case 4  : $color = "#949FB1"; break;
														case 5  : $color = "#4D5360"; break;
														case 6  : $color = 'antique-white'; break;
														case 7  : $color = 'aqua-marine'; break;
														case 8  : $color = 'bisque'; break;
														case 9  : $color = 'khaki'; break;
														default : $color = 'salmon'; break;
													}
						           			?>
						
						                         		<tr>
						                            		<td>
						                            			<nobr>
						                              			<i class="fa fa-square" style="color: <?php echo $color; ?> !important;"></i>
						                              			<?php 
						                              				echo $a_status;
																	if (isset($order_statuses[$a_status]))
																		echo ' - (' .  $order_statuses[$a_status] . ')';
						                              			?>
						                              			</nobr>
						                            		</td>
						                          		</tr>
							                        
						                    <?php 
												}
									        ?>					            			
										</table>
			              </div>
			           </div>
			           </div>


		            </div>
		            <div class="clearfix"><br /></div>
		            <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>

<script type="text/javascript">

	var ctx = document.getElementById('orders_by_status').getContext('2d');
	var chart = new Chart(ctx, {
		// The type of chart we want to create
		type: 'line',

		// The data for our dataset
		data: {
			labels: [<?php echo $status_labels; ?>],
			datasets: [{
				label: "Orders By Status",
				//backgroundColor: 'rgb(255, 99, 132)',
				borderColor: 'rgb(255, 99, 132)',
				data: [<?php echo $status_data; ?>],
			}]
		},

		// Configuration options go here
		options: {
			scales: {
				xAxes: [{ ticks: {  maxRotation: 90, minRotation: 90 } }]
			}
		}
	});
	

$(document).ready(function() {

	var chart_settings = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ <?php echo $department_labels; ?> ],
			datasets: [
					{ label: 'Procurement Orders', backgroundColor: "rgba(220,220,220,0.5)", data: [ <?php echo $order_data; ?> ] },
					{ label: 'Travel/Expenses', backgroundColor: "rgba(151,187,205,0.5)", data: [ <?php echo $expense_data; ?> ] }
				]
		},
		options: { 
			responsive: true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						var prefix = "";
						if (tooltipItem.datasetIndex == 0) prefix = 'Procurement Orders'; else prefix = 'Travel/Expenses';
						
					    if (parseInt(value) >= 1000){
					    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					    } else {
					    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
					    }
					}						
				}
			},
            scales: {
            	xAxes: [{ stacked: true, ticks: { autoSkip: false, maxRotation: 0, minRotation: 0 } }],
            	yAxes: [{ stacked: true, 
						  ticks: { 
		            				min: 0,
									callback: function(value, index, values) {
	              						if (parseInt(value) >= 1000) {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              						} else {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              						}	            	
		            				}
		            		}                									 
	            		}]
            }			 
		}
	};
	
	var chart = new Chart($('#departments_by_spend_type'), chart_settings);
	
    // var polar = new Chart($('#orders_by_status'), polar_config);	

    var vendor_type_config = {
        type: 'pie',
        data: {
            datasets: [{
                data: [ <?php echo round($preferred_vendors_used, 2); ?>, <?php echo round($non_preferred_vendors_used, 2); ?> ],
                backgroundColor: [ 'green', 'orange' ]
            }],
            labels: ["Preferred", "Non Preferred"]
        },
        options: { responsive: true }
    };

	var vendor_type_chart = new Chart($('#vendors_by_type'), vendor_type_config);

    var invoice_payment_config = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [ <?php echo $invoices_paid_unpaid['paid']; ?>, <?php echo $invoices_paid_unpaid['unpaid']; ?> ],
                backgroundColor: [ 'lightgreen', 'red' ]
            }],
            labels: ["Paid", "Unpaid"]
        },
        options: { responsive: true }
    };

	var invoices_payment_chart = new Chart($('#invoices_paid_unpaid'), invoice_payment_config);

	var location_spend_chart_config = {
		type: 'bar',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ 
				<?php $l = 0; foreach ($location_purchases as $location_data) { $l += 1; ?>
					
					'<?php echo addslashes($location_data['location_name']); ?>'
					<?php if ($l < count($location_purchases)) echo ','; ?>
					
				<?php } ?> 
			],
			datasets: [ 
					{ label: 'Paid Invoices', backgroundColor: "rgba(220,220,220,0.5)", 
						data: [ 
							<?php $l = 0; foreach ($location_purchases as $location_data) { $l += 1; ?>
								
								<?php echo round($location_data['spend_total'], 2); ?>
								<?php if ($l < count($location_purchases)) echo ','; ?>
								
							<?php } ?> 
						] 
					}, 
					
					{ label: 'Unpaid Invoices', backgroundColor: "rgba(151,187,205,0.5)", 
						data: [ 
							<?php $l = 0; foreach ($location_purchases as $location_data) { $l += 1; ?>
								
								<?php echo round($location_data['spend_future'], 2); ?>
								<?php if ($l < count($location_purchases)) echo ','; ?>
								
							<?php } ?> 
						] 
					} 
				]
		},
		options: { 
			responsive: true,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						var prefix = "";
						if (tooltipItem.datasetIndex == 0) prefix = 'Total'; else prefix = 'Future';
						
					    if (parseInt(value) >= 1000){
					    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					    } else {
					    	return prefix + ' <?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
					    }
					}						
				}
			},
            scales: {
            	xAxes: [{ stacked: true }],
            	yAxes: [{ stacked: true, 
						  ticks: { 
		            				min: 0, 
									callback: function(value, index, values) {
	              						if (parseInt(value) >= 1000) {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              						} else {
	                						return '<?php echo Yii::app()->session['user_currency_symbol_unicode']; ?>' + value;
	              						}	            	
		            				}
		            		}                									 
	            		}]
            }			 
		}
	};
	
	var location_spend_chart = new Chart($('#locations_invoice_status'), location_spend_chart_config);

     	
});	

 // Need to capture all these in one function later

   function apexMetriceSpendSpark13(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark13Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark13('spark13','Spend','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($orderSpendTotalAmount, 2); ?>','Total Year Spend',<?php echo $spark13Exp;?>);

   function apexMetriceSpendSpark14(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	var dataExp=dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark14Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark14('spark14','Orders','<?php echo number_format($totalOrders, 0);?>','Total Year Orders',<?php echo $spark14Exp;?>);

   function apexMetriceSpendSpark15(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark15Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark15('spark15','Total Spend','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalToApprovOrders, 2); ?>','Spend To Approve','<?php echo $spark15Exp;?>');

   function apexMetriceSpendSpark16(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark16Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark16('spark16','Orders','<?php echo number_format($totalPendingOrders, 0); ?>','Orders To Approve','<?php echo $spark16Exp;?>');

   function apexMetriceSpendSpark17(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark17Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark17('spark17','Orders','<?php echo html_entity_decode(Yii::app()->session['user_currency_symbol']).number_format($totalPaiedAmount,2); ?>','Orders To Be Paid','<?php echo $spark17Exp;?>');

function apexMetriceSpendSpark18(chartID,dataExpTitle,subtitleTotal,subTitle,dataExp){
  	var dataExpTitle=dataExpTitle;
  	dataExp= dataExp;
  	var subtitleTotal=subtitleTotal;
  	var subTitle=subTitle;
  	//console.log(dataExp);alert(dataExp);
  	var sparkSpend = {
    chart: {
        type: 'area',
        height: 160,
        sparkline: {
            enabled: true
        },
    },
    stroke: { curve: 'straight'},
    fill: {opacity: 0.3,},
    series: [{
            name: dataExpTitle,
            data: [<?php echo $spark18Exp;?>]
        },],
    yaxis: {min: 0},
    title: {
        text: subtitleTotal,
        offsetX: 0,
        style: {
            fontSize: '24px',
            cssClass: 'apexcharts-yaxis-title'
        }
    },
    subtitle: {
        text: subTitle,
        offsetX: 0,
        style: {
            fontSize: '14px',
            cssClass: 'apexcharts-yaxis-title'
        }
    }
}
 var sparkSpend = new ApexCharts(document.querySelector("#"+chartID), sparkSpend);
    sparkSpend.render();
  }

   apexMetriceSpendSpark18('spark18','Orders','<?php echo number_format($ordersToReceive, 0); ?>','Orders To Be Received','<?php echo $spark17Exp;?>');
</script>
