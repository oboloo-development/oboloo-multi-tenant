<div>
  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
          <form novalidate id="forgot_form" name="forgot_form" method="post" action="<?php echo AppUrl::bicesUrl('app/forgot'); ?>">
          <h1>Forgot Password</h1>
          <div class="item form-group">
            <input id="username" name="username" type="text" class="form-control" placeholder="Username" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="item form-group">
            <input id="email" name="email" type="text" class="form-control" placeholder="Email" required="required" />
          </div>
          <div class="clearfix"></div>
          <div style="width: 350px;">
            <a class="btn btn-default submit" onclick="$('#forgot_form').submit();" style="color: #333">Retrieve Password</a>
            <div style="margin-top: 15px; display: inline; float: right;">
            OR <a style="text-decoration: underline;" href="<?php echo AppUrl::bicesUrl('login'); ?>">Login Here</a>
           </div>
          </div>
			<input type="hidden" name="form_submitted" id="form_submitted" value="1" />
          <div class="clearfix"></div>

            <br />

          </div>
        </form>
      </section>
    </div>


    <?php if (isset($success) && $success == 1) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            Your randomly generated password has been emailed to you.
        </div>
    <?php } ?>

    <?php if (isset($error) && $error == 1) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            No such account was not found. Please enter correct username or email address.
            <br /><br />
        </div>
    <?php } ?>


    <div id="status_area">
    </div>
    <div class="clearfix"></div>


  </div>
</div>
