<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <h3>Approval Levels</h3>
    </div>

<?php 
	$display_type = 'Amount';
	if (isset($data_type) && $data_type == 'S') $display_type = 'Step';
?> 

<div class="row tile_count">
  <form id="approval_form" class="form-horizontal form-label-left input_mask" method="post" action="<?php echo AppUrl::bicesUrl('approvals/list'); ?>">

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6">
              <select name="approval_type" id="approval_type" class="form-control" onchange="$('.approvals_data_area').hide();">
              	  <option value="">Select Approval Type</option>
                  <option value="O" <?php if (isset($approval_type) && $approval_type == 'O') echo ' selected="SELECTED" '; ?>>
                  	  Procurement Orders
                  </option>
                  <option value="E" <?php if (isset($approval_type) && $approval_type == 'E') echo ' selected="SELECTED" '; ?>>
                  	  Travel &amp; Expenses
                  </option>
                  <!--
                  <option value="P" <?php if (isset($approval_type) && $approval_type == 'P') echo ' selected="SELECTED" '; ?>>
                  	  Invoices And Payments
                  </option>
                  -->
              </select>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-4">
              <select name="data_type" id="data_type" class="form-control" onchange="$('.approvals_data_area').hide();">
                  <option value="A" <?php if (isset($data_type) && $data_type == 'A') echo ' selected="SELECTED" '; ?>>
                  	  Amount Based
                  </option>
                  <option value="S" <?php if (isset($data_type) && $data_type == 'S') echo ' selected="SELECTED" '; ?>>
                  	  Step Based
                  </option>
              </select>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-4">
              <select name="condition_type" id="condition_type" class="form-control" onchange="$('.approvals_data_area').hide();">
                  <option value="A" <?php if (isset($condition_type) && $condition_type == 'A') echo ' selected="SELECTED" '; ?>>
                  	  ALL (AND)
                  </option>
                  <option value="O" <?php if (isset($condition_type) && $condition_type == 'O') echo ' selected="SELECTED" '; ?>>
                  	  ANY (OR)
                  </option>
              </select>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6">
              <select name="location_id" id="location_id" class="form-control" onchange="$('.approvals_data_area').hide(); loadDepartmentsForSingleLocation(0);">
                  <option value="0">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($location_id) && $location['location_id'] == $location_id) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6">
              <select name="department_id" id="department_id" class="form-control" onchange="$('.approvals_data_area').hide();">
                  <option value="0">Select Department</option>
              </select>
          </div>

          <div class="col-md-1 col-sm-1 col-xs-2">
          <a href="javascript:void(0);" onclick="loadApprovalData();">
              <button type="button" class="btn btn-primary">
                  Load
              </button>
          </a>
          </div>
      </div>

      <input type="hidden" name="form_submitted" id="form_submitted" value="2" />
      <div class="clearfix"> <br /> </div>

	  <div class="approvals_data_area">
      <?php if (isset($approvals) && is_array($approvals) && count($approvals)) { ?>

          <?php foreach ($approvals as $approval) { ?>

              <div class="form-group">
                  <div class="col-md-3 col-sm-3 col-xs-6">
                      <select name="user_id[]" class="form-control">
                          <option value="0">Select User</option>
                          <?php foreach ($users as $user) { ?>
                              <option value="<?php echo $user['user_id']; ?>"
                                      <?php if ($approval['user_id'] == $user['user_id']) echo ' selected="SELECTED" '; ?>>
                                  <?php echo $user['full_name']; ?>
                              </option>
                          <?php } ?>
                      </select>
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <input type="text" class="form-control text-right step-count" name="amount[]" value="<?php echo $approval['amount']; ?>" 
                      		placeholder="<?php echo $display_type; ?>" <?php if ($display_type == 'Step') echo ' readonly="readonly" '; ?>>
                  </div>

                  <div class="col-md-2">
                      <a onclick="$(this).parent().parent().remove();resetStep(); return false;" class="btn btn-link">
                          <span class="fa fa-remove hidden-xs"></span>
                      </a>
                  </div>
              </div>

          <?php } ?>

      <?php } ?>

      <?php if (isset($form_submitted) && $form_submitted) { ?>
      <fieldset id="additional-field-model">
      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6">
              <select name="user_id[]" class="form-control">
                  <option value="0">Select User</option>
                  <?php foreach ($users as $user) { ?>
                      <option value="<?php echo $user['user_id']; ?>">
                          <?php echo $user['full_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-4">
              <input type="text" class="form-control text-right step-count" name="amount[]" 
              		placeholder="<?php echo $display_type; ?>" <?php if ($display_type == 'Step') echo ' readonly="readonly" '; ?>>
          </div>

          <div class="col-md-2">
              <a href="javascript:void(0);" onclick="$(this).parent().parent().remove();resetStep();" class="btn btn-link remove-this-field">
                  <span class="fa fa-remove hidden-xs"></span>
              </a>
              <a href="javascript:void(0);" class="btn btn-link create-new-field">
                  <span class="fa fa-plus hidden-xs"></span>
              </a>
          </div>
      </div>
      </fieldset>
      <?php } ?>
      </div>

  </form>


  <div class="clearfix"><br /></div>
  <?php if (isset($form_submitted) && $form_submitted) { ?>
  <div class="form-group approvals_data_area">
  	<div class="col-md-3 col-sm-3 col-xs-6">
	  <a href="javascript:void(0);" onclick="$('#form_submitted').val(2); $('#approval_form').submit();">
	      <button type="button" class="btn btn-primary">
	          Save Approvals
	      </button>
	  </a>
	</div>
  </div>
  <?php } ?>


	
</div>
</div>

<div id="alert-error" class="alert alert-error alert-dismissable col-md-6 col-md-offset-3" style="display: none;">
	<div class="col-md-11">
    	<strong>ERROR! Please select approval type, a location and related department to define approval routing for.</strong>
    </div>
	<div class="col-md-1">
	    <a style="margin-top: 2px; margin-right: 15px;" onclick="$('#alert-error').hide();" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
	</div>
</div>

<script type="text/javascript">

    function loadDepartmentsForSingleLocation(department_id)
    {
        var location_id = $('#location_id').val();
        var single = 1;

        if (location_id == 0)
            $('#department_id').html('<option value="">Select Department</option>');
        else
        {
            $.ajax({
                type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
                url: BICES.Options.baseurl + '/locations/getDepartments',
                success: function(options) {
                    var options_html = '<option value="">Select Department</option>';
                    for (var i=0; i<options.length; i++)
                        options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                    $('#department_id').html(options_html);
                    if (department_id != 0) $('#department_id').val(department_id);
                },
                error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
            });
        }
    }

$(document).ready( function() {
    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field"
    });

	<?php if ($display_type == 'Step') { ?>
	    $('body').on('click', "select[name^='user_id']", function() {
	        reorderSteps();
	    });
	<?php } ?>

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			loadDepartments(<?php echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>

});

function resetStep(){
    var data_type = $('#data_type').val();
    if(data_type=='S'){
        reorderSteps();
    }
}

function reorderSteps()
{
	var i = 1;
	$('.step-count').each(function() {
		$(this).val(i);
		i += 1;
	});	
}

function loadApprovalData()
{
	var department_id = $('#department_id').val();
	var location_id = $('#location_id').val();
	var approval_type = $('#approval_type').val();

	$('#alert-error').hide();	
	if (location_id == 0 || department_id == 0 || approval_type == '')
	{
		$('#alert-error').show();
		setTimeout(function() { $("#alert-error").hide(); }, 4000);
		return false;
	}
	else
	{
		$('#form_submitted').val(1); 
		$('#approval_form').submit();
	}
}
</script>
