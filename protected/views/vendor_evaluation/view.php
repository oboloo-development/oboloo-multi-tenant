<?php $quote_statuses = array('Pending', 'Submitted', 'Closed', 'Cancelled'); ?>
<?php $disabled = FunctionManager::sandbox(); ?>
<div class="right_col" role="main">
<div class="clearfix"></div>
<div class="col-md-12">
    <div class="tile_count">
        <div class="col-md-6 col-sm-6 col-xs-12 pull-left">
            <h3>Quick Evaluations</h3>

            <?php if($mainQuickScoring['quote_name']){?>
             <h4  class="subheading"><br /><strong>Quote Name</strong>: <span class="title-text"><?php echo  $mainQuickScoring['quote_name'];?></span><br /><br />
             <strong>Quote Description</strong>: <span class="title-text"><?php echo  $mainQuickScoring['quote_desc'];?></span></h4>
           <?php } ?>
           <?php if(isset($categorySubcateReader['category_name'])){?>
              <h4  class="subheading"><br /><strong>Category</strong>: <span class="title-text"><?php echo $categorySubcateReader['category_name'];?>
                </span></h4>
              <h4  class="subheading"><br /><strong>Sub Category</strong>: <span class="title-text">
                <?php echo $categorySubcateReader['subcategory_name'];?>
                </span></h4>
           <?php } ?>
            
        </div>
        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('vendorEvaluation/list'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;border-color: #F79820;">
                    <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Return To Sourcing Evaluation List
                </button>
            </a>
        </div>
        
        <div class="clearfix"> </div><br />
         <?php if(Yii::app()->user->hasFlash('success')):?>
         <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo Yii::app()->user->getFlash('success'); ?></div>
          <?php endif; ?>
</div>
	
<div class="clearfix"> </div>

 

  <!-- START: Scoring Criteria Graph-->
  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel tile ">

      <div class="x_title">
        <h2>Scoring Criteria (%)</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content"><div id="scoring_pie_chart"></div><br /></div>
    </div></div>

  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel tile ">
      <div class="x_title">
        <h2>Supplier Criteria Scoring</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content"><div id="scoring_chart"></div></div>
    </div></div>
  <!-- End: Scoring Criteria Graph-->
  <div class="clearfix"></div><br /><br />
  <div class="msg_area"></div>
  <div id="scoring_list_table"> </div>
  <div class="clearfix"></div><br /><br />

  <form id="detail_description" method="POST">
    <div class="col-md-6 col-xs-12">
     <div id="detail_description_alert" class="alert alert-success" role="alert"></div>
      <label>Please briefly explain why you gave that score</label>
      <textarea cols="5" rows="3" id="detail_descr" class="form-control" name="detail_description"><?php echo isset($mainQuickScoring['detail_description'])? $mainQuickScoring['detail_description']:''; ?></textarea><br>
      <input type="Submit" name="" value="Save" class="btn btn-primary pull-right" <?php echo $disabled; ?>>
    </div>
  </form>

  
  <div id="edit_score" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Edit Scoring Criteria</h4></div>
                  <div class="modal-body">
                    <form id="edit_score_form" action="" method="post">
                   </form>
                    <div class="edit_msg_area"></div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button>
                    <button  type="button" class="btn btn sm green-btn" onclick="editScoring('edit_score_form');" <?php echo $disabled; ?>>Save</button></div></div></div></div></div>
                 <!--  <style>
                  .control-label {min-width: 22% !important;}
                  .form-control {width: 50%  !important;}
                  .form-group {margin-bottom: 14px !important; width: 100%;}
                  </style> -->


</div>

</div>

<style>
	.location  .multiselect {
		width: 138%;
	}
	.status  .multiselect {
		width: 100%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}

	.btn .caret {
		float: right;
		margin-top: 10px;

	}
    .department .dropdown-toggle{
        min-width: 112%;
    }
</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>
<style type="text/css">
    th,td {cellspacing:10px;}
</style>
<script type="text/javascript">
    function checkUniqScore(obj,rowNumber){
        rowArr = [];
        var obj = obj;
        var rowNumber = rowNumber;
        $('.score_'+rowNumber).each(function(index,element) {
          if(jQuery.inArray($(this).val(), rowArr) !== -1){
                currValue = $(this).val();
              
               $('option:selected',obj).prop("selected", false);
               $(obj).next('span').html("<b style='color:red'>This Score ("+currValue+") is already selected for the same criteria. Try the other</b>");
              }else{
                 $(obj).next('span').html("");
              }

               rowArr.push($(this).val()) 
        });
    }

    function checkUniqScoreEdit(obj,rowNumber){
      rowArr = [];
      var obj = obj;
      var rowNumber = rowNumber;

      $('.scoreedit_'+rowNumber).each(function(index,element) {
        if(jQuery.inArray($(this).val(), rowArr) !== -1){
              currValue = $(this).val();
            
             $('option:selected',obj).prop("selected", false);
             $(obj).next('span').html("<b style='color:red'>This Score ("+currValue+") is already selected for the same criteria. Try the other</b>");
            }else{
               $(obj).next('span').html("");
            }

             rowArr.push($(this).val()) 
      });
    }
function calculateVendScore(vendID){
    var vendTotalScore = 0;
   $('.vendor_'+vendID).each(function(index,element) {
        currValue = parseInt($(this).val());
        if(currValue>0){
            vendTotalScore=parseInt(vendTotalScore)+currValue;
        }
    });
   
   $('#vendor_'+vendID).val(vendTotalScore);
}
$('.msg_area').hide();
function saveScoringCriteria(e)
{   e.preventDefault();
    $('#loading_icon').show();

    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/saveScoringCriteria'); ?>',
        type: 'POST', async: false, dataType: 'json',
        data: $("#scoring_criteria_form").serialize(),
        success: function(data) {
            loadScoring();
            $(".msg_area").html('<div id="edit_msg_area" class="alert alert-success" role="alert">'+data.msg+'</div>');
            $(".msg_area").show().delay(5000).fadeOut();
        }
    });
    createLocationChart();
    checkScoringSubmission();

}




function loadScoring()
{  
     
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('vendorEvaluation/listScoring'); ?>',
        type: 'POST',
        data: {main_score_id:'<?php echo $mainScoreID;?>'},
        success: function(data) {
            $("#scoring_list_table").html(data);
        }
    });

}

function loadEditForm(e,score,main_scoring_id)
{  
    e.preventDefault();
    var scoreID = score;
    var mainscoringid = main_scoring_id;
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('vendorEvaluation/editScoring'); ?>',
        type: 'POST',
        data: {score_id:scoreID,main_scoring_id:mainscoringid},
        success: function(data) {
          $('#edit_score_form').html(data);
          $('#edit_score').modal('show'); 
           // $("#scoring_list_table").html(data);
        }
    });

}

function editScoring(formID)
{   
  var formID = formID;
  $('#loading_icon').show();
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('vendorEvaluation/saveEditScoring'); ?>',
        type: 'POST', async: false, dataType: 'json',
        data: $("#"+formID).serialize(),
        success: function(data) {

           $('#edit_score').modal('hide');
            loadScoring();
            $(".msg_area").html('<div id="edit_msg_area" class="alert alert-success" role="alert">'+data.msg+'</div>');
            $(".msg_area").show().delay(5000).fadeOut();
        }
    });
    createLocationChart();

}
  <?php $editID = $mainQuickScoring['id']?>
 $("#detail_description_alert").hide();
$('#detail_description').on('submit', function(){
    var formID = formID;
    var detail_descr = $('#detail_descr').val();
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('vendorEvaluation/saveDetailDesc'); ?>',
        type: 'POST', dataType: 'json',
        data: { main_score_id:'<?php echo $editID;?>', detail_dsc:detail_descr},
        beforeSend: function() {
         $("#detail_description_alert").show();
         $("#detail_description_alert").delay(1000*5).fadeOut();
        },
        success: function(data) {
           if(data.msg==1){
            $("#detail_description_alert").html("Saved successfully");
          } 
        }
    });
    return false;
});
loadScoring();

function createLocationChart()
{   

    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('vendorEvaluation/listScoreDataChart'); ?>',
        type: 'POST', dataType: 'json',
        data: { main_score_id:'<?php echo $mainScoreID;?>' },
        success: function(chart_data) {
       var score_values = chart_data.score_values;
       var score_labels = chart_data.score_labels;
       var pie_score_series = [];
       var pie_score_label  = chart_data.pie_score_label;
      for (i = 0; i < chart_data.pie_score_series.length; i++) {
          pie_score_series.push(parseFloat(chart_data.pie_score_series[i]));
      }
      
      /**/
          var options = {
            chart: {
                height: 267,
                type: 'bar',
               // stacked: true,
               // stackType: '100%',
                id:"apexChartVendDeptscoring",
                toolbar: {
                  tools: {
                      download: false
                  }
                }
            },
            plotOptions: {bar: {distributed: true}},
            responsive: [{
                breakpoint: 380,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            series: [{name:'Score',data: score_values}],
            xaxis: {
                categories: score_labels,

                /*labels: {
                //trim: true,
            show: true,
            minHeight: undefined,
              maxHeight: 70,
              rotate: -5,
         
            }*/},
            yaxis:{ 
            tickAmount: 10,
            min: 0,
            max: 100,
            labels: {
              formatter: function (value) {
                  valueL = value+"%";
                  return valueL;
                },
             }
      },

      dataLabels: {
        enabled: false,
        enabledOnSeries: false,
        formatter: function(value, { seriesIndex, dataPointIndex, w }) {
          return /*w.config.series[seriesIndex].name+": "+*/w.config.series[seriesIndex].data[dataPointIndex]+"%";
        },
       /* formatter: function (val, opts) {
          console.log(opts);
            return val;
        },*/
      },
      tooltip: {
          y: {
          
          },
          marker: {
          show: true,
          },
      },
            fill: {
                opacity: 1
            },
            legend: {
                show: false,
                position: 'bottom',
                offsetX: 0,
                offsetY: 0
            },
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_chart"),
            options
        );
        chart.render();

        chart.updateSeries([{
          name: 'Score',
        
          data: score_values
        }]);

        
        /* ApexCharts.exec('apexChartVendDeptscoring', 'updateOptions', {
            xaxis: {
              categories: score_labels,
            }
          }, true, true);
          ApexCharts.exec('apexChartVendDeptscoring', 'updateSeries', score_values,true);*/
    
    // START: Pie Chart
    var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 260,
              width: 520,
              type: 'pie',
              id:"scoringPieChart"

            },
           // legend: {show: false},
            dataLabels: {enabled: false},
            labels: pie_score_label,
            series: pie_score_series,
            responsive: [{
                breakpoint: 520,
                options: {
                    chart: {
                      width: 'auto'
                    },
                    legend: {
                        show: false
                    }
                }
            }],
          tooltip: {
            y: {
                formatter: function(value) {
                 return value+"%";
               },
                 
            },}
            
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_pie_chart"),
            options
        );
         chart.render();
          ApexCharts.exec('scoringPieChart', 'updateOptions', {
           labels:  pie_score_label,
          }, false, true);
          ApexCharts.exec('scoringPieChart', 'updateSeries', pie_score_series,true);

    // END: Pie Chart
       }
      });

  }
  createLocationChart();  
 
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 
        && (charCode < 48 || charCode > 57))
         return false;
      return true;
   }
       
    </SCRIPT>


