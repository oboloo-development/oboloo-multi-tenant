<?php $quote_statuses = array('Pending', 'Submitted', 'Closed', 'Cancelled'); ?>
<?php $topTenCategory = $topTenQuickEvaluation = array();
      foreach ($evaluationCateReader as $value) {
          $topTenCategory[]       =  $value['category_name'];
          $topTenQuickEvaluation[] = $value['quick_evaluations'];
        }
?>
<div class="right_col" role="main">
 <!-- <div class="quick-sourcing-evaluation-tutorial"></div> -->
  <div class="col-md-5" style="" ></div>
  <div class="col-md-3 highlight-create" style="" ></div>
  <div class="col-md-4" style="" ></div>
<div class="">
    <div class="row-fluid tile_count">

        <div class="col-md-10 span6 pull-left" style="word-break: break-all;word-wrap: break-word;">
            <h3>Quick Evaluations</h3>
            <h4  class="subheading"><br />Create a simple record of supplier comparison scores based on either your organisation’s default values or your own <br /><br /><strong class="top-h4-qck">Quick Evaluations are not sent to suppliers</strong>
            </h4>
        </div>

        <div class="col-md-2 span6 pull-right quick-tutorial hidden-xs">
          <button type="button" class="btn btn-warning   pull-right" onclick="addQuickSourcingEvaluationModal(event);">
                Create Quick Evaluation
            </button>
        </div>

        <div class="clearfix"> </div><br /><br />
         <?php
         $qouteAwarded=Yii::app()->user->getFlash('success');
          if(!empty($qouteAwarded)) {?>
         <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo Yii::app()->user->getFlash('success'); ?></div>
          <?php } ?>
        

    </div>

     <div class="col-md-6 col-sm-6 col-xs-12 quick-graphs" style="margin-bottom: 49px;">
        <div class="x_panel tile "><div class="x_title"><h2>Current Default Values (%)</h2><div class="clearfix"></div></div><div class="x_content"><div id="scoring_pie_chart"></div><br /></div>
        </div>
     </div>

      <div class="col-md-6 col-sm-6 col-xs-12  hidden-xs">
        <div class="x_panel tile" style="    margin-left: 15px;">
          <div class="x_title">
             <h2>Top Ten Categories By Number Of Evaluations</h2>
             <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div id="evaluattion_top_ten_by_category"></div>
          </div>
        </div>
      </div>
  
<div class="clearfix"> </div>
<form class="form-horizontal" id="quick_evaluation_search" name="" role="form" method="post" action="">
    
    <div class="col-md-3 col-sm-3 col-xs-12">
    <select name="vendor_id" id="vendor_id" class="form-control select_vendor_multiple border-select" onchange=""  >
      <option value="">All Suppliers</option>
    </select>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-12">
    <select name="category_id" id="category_id" class="form-control select_category_multiple border-select" onchange=""  >
      <option value="">All Categories</option>
    </select>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
    <select name="subcategory_id" id="subcategory_id" class="form-control select_subcategory_multiple border-select" onchange=""  >
      <option value="">All Sub Categories</option>
    </select>
    </div>
    <div class="col-md-3 col-xs-12 pull-right text-left search-quote">
          <button class="btn btn-info search-quote" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
        <button class="btn btn-primary search-quote" onclick="quickEvaluation(); return false;">Apply Filters</button>
   </div>
</form>
      
          

<table id="quick_table" class="table table-striped table-bordered quick-table" width="100%">
      <thead>
        <tr>
          <th style="width: 8%"></th>
          <th style="width: 15%">Quote Name</th>
          <th style="width: 20%">Description</th>
          <th style="width: 15%">Category</th>
          <th style="width: 14%;  text-align: center;">Suppliers Evaluated</th>
          <th style="width: 14%; text-align: center;">Highest Scoring Supplier</th>
          <th style="width: 8%; text-align: center; ">Created by</th>
          <th style="width: 12%; text-align: center;">Created on</th>
        </tr>
      </thead>
      <tbody>
       
      </tbody>

  </table>

</div>

</div>

<style>
  .location  .multiselect {
    width: 138%;
  }
  .status  .multiselect {
    width: 100%;
  }
  .multiselect-selected-text{
    float: left;
    margin-left: 0px;
  }

  .btn .caret {
    float: right;
    margin-top: 10px;

  }
    .department .dropdown-toggle{
        min-width: 112%;
    }
  .table-user-img {
    margin-right: 5px;
}
.location .multiselect {width: 138%;}
.department .multiselect {width: 112%;}
</style>

<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>
<style type="text/css">
    th,td {cellspacing:10px;}
</style>

<script type="text/javascript">


function addQuickSourcingEvaluationModal(e){
  e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('vendorEvaluation/createBySourcingEvaluationModal'); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_quick_sourcing_evaluation_cont').html(mesg);
            $('#create_quick_sourcing_evaluation').modal('show');
            
             
        }
    });
}
</script>
<?php $this->renderPartial('/vendor_evaluation/create_quick_sourcing_evaluation');

    // foreach ($evaluationCateReader as $eval_categorirs) {
    //   $eval_categorirs['category_name']; 
    //   $eval_categorirs['quick_evaluations']; 
    // }
?>

<script type="text/javascript">
function select2function(className,lableTitle){
  var lableTitle = lableTitle;
  $("."+className).select2({
     // placeholder: lableTitle,
     /* allowClear: true*/
    });
}

function clearFilter() {
  $("option:selected").removeAttr("selected");
  $('#vendor_id option[value=""]').attr('selected','selected');
  $('#category_id option[value=""]').attr('selected','selected');
  $('#subcategory_id option[value=""]').attr('selected','selected');

  $('#vendor_id').trigger("change");
  $('#category_id').trigger("change");
  $('#subcategory_id').trigger("change");

  $('#quick_table').DataTable().ajax.reload();
}

function loadSuppliers(vendor_id)
{
 $.ajax({
          type: "POST", data: { }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('dropdownAndSearch/getSupplier'); ?>",
          success: function(options) {
            var options_html = '<option value="0">Select Supplier</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#vendor_id').html(options_html);
            if (vendor_id != 0) $('#vendor_id').val(vendor_id);
          },
          error: function() { $('#vendor_id').html('<option value="0">Select Supplier</option>'); }
      });
}

function loadCategories(category_id)
{
 $.ajax({
          type: "POST", data: { }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('dropdownAndSearch/getCategory'); ?>",
          success: function(options) {
            var options_html = '<option value="0">Select Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#category_id').html(options_html);
            if (category_id != 0) $('#category_id').val(category_id);
             
          },
          error: function() { $('#category_id').html('<option value="0">Select Category</option>'); }
      });
 
}

function loadSubcategories(category_id,input_subcategory_id)
{
  var category_id = category_id;
  
  if (category_id == 0)
    $('#subcategory_id').html('<option value="0">Select Sub Category</option>');
  else
  {
      $.ajax({
          type: "POST", data: { category_id: category_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('dropdownAndSearch/getSubCategory/'); ?>",
          success: function(options) {
            var options_html = '<option value="0">Select Sub Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#subcategory_id').html(options_html);
            if (input_subcategory_id != 0) $('#subcategory_id').val(input_subcategory_id);
            
          },
          error: function() { $('#subcategory_id').html('<option value="0">Select Sub Category</option>'); }
      });
  }
  $('#subcategory_id').trigger('change');
}

function quickEvaluation()
{
  $('#quick_table').DataTable().ajax.reload();
}
$(document).ready( function() {
      $('#quick_table').dataTable({
        "columnDefs": [ {
        } ],

        "createdRow": function ( row, data, index ) {
            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 ){
              for (var i=1; i<=6; i++)
                  $('td', row).eq(i).css('text-decoration', 'line-through');
            }

        },        
        "order": [[ 1, "asc" ]],
        "pageLength": 50,        
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?php echo AppUrl::bicesUrl('vendorEvaluation/list'); ?>",
          "type": "POST",
          data: function ( input_data ) {
            input_data.vendor_id = $('#vendor_id').val();
            input_data.category_id = $('#category_id').val();
            input_data.subcategory_id  = $('#subindustry_id').val();
        } 
        },
        "oLanguage": { "sProcessing": "<h1>Please wait ... retrieving data</h1>" },
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          // if(aData[1].search("<span class='deactivated_vendor'></span>")>0){
            $(nRow).addClass('deactivated_record');
            /*$(nRow).css("background-color", "#ccc");*/
          // }
         }
    });

    select2function('select_vendor_multiple','All Suppliers');
    select2function('select_category_multiple','All Categories');
    select2function('select_subcategory_multiple','All Sub Categories');

    loadCategories("<?php echo !empty($_POST['category_id'])?$_POST['category_id']:0?>");
    $('#category_id').trigger('change');
    loadSuppliers("<?php echo !empty($_POST['vendor_id'])?$_POST['vendor_id']:0?>");
    $('#vendor_id').trigger('change');

    $("#category_id").change(function(){
        loadSubcategories($(this).val(),"<?php echo !empty($_POST['subcategory_id']) ? $_POST['subcategory_id'] : 0 ?>");
    });
});

createPieChart();
colors = ['#2d9ca2','#66DA26', '#546E7A', '#E91E63', '#FF9800','#2E93fA','#2196F3', '#3a62ba', '#7b9333', '#344189','#','#','#','#','#'];

function createPieChart()
{   
    var quote_id = $("#quote_id").val();
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/listScoreDataChart'); ?>',
        type: 'POST', dataType: 'json',
        data: {},
        success: function(chart_data) {
 /*      var score_values = chart_data.score_values;
       var score_labels = chart_data.score_labels;*/
       var pie_score_series = [];
       var pie_score_label  = chart_data.pie_score_label;
      for (i = 0; i < chart_data.pie_score_series.length; i++) {
          pie_score_series.push(parseFloat(chart_data.pie_score_series[i]));
      }
      
    // START: Pie Chart
    var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 200,
              width: 450,
              type: 'pie',
              id:"scoringPieChart"

            },
           // legend: {show: false},
            dataLabels: {enabled: false},
            labels: pie_score_label,
            series: pie_score_series,
            responsive: [{
               breakpoint: 380,
                options: {
                    chart: {
                       width: 'auto'
                        //height: 'auto',

                    },
                    legend: {
                        show: false
                    }
                }
            }],
           tooltip: {
            y: {
                formatter: function(value) {
                 return value+"%";
               },
                 
            },}
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_pie_chart"),
            options
        );
         chart.render();

          ApexCharts.exec('scoringPieChart', 'updateOptions', {
           labels:  pie_score_label,
          }, false, true);
          ApexCharts.exec('scoringPieChart', 'updateSeries', pie_score_series,true);

    // END: Pie Chart
       }
      });

  }
 
 

      /* Start: Top ten count by category */
  var colors = ['rgb(72, 214, 168)'];
   var options = {
          series: [{ name: "Top Ten Categories",
          data: [<?php echo implode(",",$topTenQuickEvaluation); ?>]
        }],
          chart: {
          fontFamily: 'Poppins !important',
          download: false, 
          toolbar: {
            show: false
          },
          height: 200,
          //width: 610,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '35%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [<?php echo '"'.implode('","',$topTenCategory).'"'; ?>],
          labels: {
            colors: ['rgb(72, 214, 168)'],
            style: {
               // colors: ['rgb(72, 214, 168)'],
              fontSize: '12px'
            },
            show:false,
                
              },
        },
        yaxis: {
            //show:false,
           // min:0,
            //tickAmount:2,
            labels: {
              color: colors,
                formatter: function (value) {
                  /*value = value.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                  valueL = value;*/
                  return value.toFixed(0)
                },
              },}
        };

        var chart = new ApexCharts(document.querySelector("#evaluattion_top_ten_by_category"), options);
        chart.render();
      /* End: Top ten count by category */
</script>