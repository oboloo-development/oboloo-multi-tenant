<div role="main">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3 class="pull-left"><?php echo 'Create Quick Evaluations';?></h3>
  <div class="pull-right" style="text-align:center;padding-right: 21px;padding-top: 3px;">
  <span class="step"></span>
</div></div>

        <div class="clearfix"> </div>
        <?php if(Yii::app()->user->hasFlash('success')):?>
        <div class="tile_count">
            <div class="alert alert-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>  </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->hasFlash('error')):?>
        <div class="tile_count">
            <div class="alert alert-danger">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>  </div>
        <?php endif; ?>
  

<div class="tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">
    <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('vendorEvaluation/list'); ?>" autocomplete="off">

      
   
     <div class="tab"> 
     <!--  <div class="pull-left">
        <h4 class="col-md-12 col-sm-12 col-xs-12 heading">Score Suppliers </h4>
        <h4 class="col-md-12 col-sm-12 col-xs-12 subheading">Highest Number = Best Scoring Supplier, Lowest Number = Lowest Scoring Supplier (Suppliers can have the same score)</h4>
      </div> -->

     
        <div class="form-group">
        <div class="col-md-8 col-sm-8 col-xs-8 valid">
            <label class="control-label">Quote Name <span style="color: #a94442;">*</span></label>
            <input required type="text" class="form-control  quote_name notranslate" name="quote_name" id="quote_name" placeholder="Quote Name" autocomlete="off" required>
            
        </div></div>

      <div class="form-group">
          <div class="col-md-8 col-sm-8 col-xs-8">
              <label class="control-label">Description/Notes <span style="color: #a94442;">*</span></label>
              <textarea class="form-control description_notes notranslate" name="quote_desc" id="quote_desc" rows="4" placeholder="Quote description/notes" required></textarea>
          </div>
      </div> 
<div class="col-md-8 col-sm-8 col-xs-8 date-input valid show_supplier_name">
</div>
      <div class="col-md-8 col-sm-8 col-xs-8 date-input valid">
        <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12" style="padding-left: 0px;">
        <label class="control-label">Category <span style="color: #a94442;">*</span></label>
        <select name="category_id" id="modal_category_id"
          class="form-control notranslate" onchange="loadSubcategories(0);" required>
          <option value="">Category</option>
        <?php foreach ($categories as $category) { ?>
          <option value="<?php echo $category['id']; ?>">
            <?php echo $category['value']; ?>
          </option>
        <?php } ?>
      </select>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12" style="padding-right: 0px;">
        <label class="control-label">Sub Category</label>
        <select name="subcategory_id" id="subcategory_id_new"
          class="form-control notranslate">
          <option value="0">Select Sub Category</option>
        </select>
      </div>
    </div>
    <div class="vendor_name_cont">
        <div style="float:left;margin-left: -9px;" class="col-md-10 col-sm-10 col-xs-12 use-vendor-cont">
        <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
        <select  type="text" class="vendor_name_field form-control select_vendor_multiple vendor_name_1 notranslate use-vendor" 
          name="vendor_name_1" id="vendor_name_1" style="  border-radius: 10px;border: 1px solid #4d90fe;width: 300px;" required>
      </select></div>
    </div> 
     
    <div class="clearfix"></div>
      <div id="vendorNewName"></div>

     <div class="pull-left"></div><br><br>  <!--  -->
      <button type="button" class="btn btn-danger" style="background-color: #f0ad4e !important; border-color:#f0ad4e !important;" id="addNewVendorButoon">Add new supplier</button>
       <button type="button" id="add_new_supplier_not" class="btn add_new_supplier_not btn-success sub_btns" onclick="supplierNotInOboloo()" >+ Add New Supplier Not In Oboloo</button>
       <div class="clearfix"></div> <br />
      <button type="button" class="btn btn-primary submit-btn addindefaultvalues" onclick="loadDefaultScoring();">
          <span class="glyphicon glyphicon-plus"></span> Add in default values
      </button>
      <button type="button" class="btn  btn-danger  deletedefaultvalues" onclick="removeDefaultScoring();">
          <span class="glyphicon glyphicon-minus"></span> Delete default values
      </button>

      <div  id="number_custom_score" class="btn submit-btn1" style="color: #fff; display: inline-block;"><span class="fa fa-plus fa-1x"></span> &nbsp;Add More Scoring Criteria</div>
      <div class="clearfix"></div> <br />
      </div>
      </div>
      <div class="pull-left score_info">
      <h4 class="col-md-12 col-sm-12 col-xs-12 heading">Score Suppliers</h4>
       <h4 class="col-md-12 col-sm-12 col-xs-12 subheading" style="margin-top: 12px;">Evaluate and score your suppliers againest each other using your organisations or your own values</h4><div class="clearfix"><br /><br /><br/></div>
        <h4 class="col-md-12 col-sm-12 col-xs-12 subheading" style="margin-top: 10px;"><i>Highest Number = Best Scoring Supplier, Lowest Number = Lowest Scoring Supplier (Suppliers can have the same score)</i></h4>
        <div class="clearfix"><br /><br /></div>
      </div>
      <div class="clearfix"></div><br />
     <div class="table-responsive">
      <!-- <table class="table">
      <tr>
        <th class="notranslate">Scoring Criteria</th>
        <th class="notranslate">Enter Scoring %</th>
      </tr>
      <tbody>
    <?php $i=1;$scoringCriteriaArr=array();
    foreach($scoringCriteria as $value){
      $scoringCriteriaArr[$value['id']] = $value['value'];?>
      <tr>
      <th><input type="text" class="form-control notranslate" disabled="disabled" value="<?php echo $value['value'];?>" style="padding:5px;"></th>
      <th><input type="text" name="score_percentage[<?php echo $value['id'];?>]" class="form-control score-criteria notranslate" placeholder="Enter Scoring Percentage" onkeypress="return isNumberKey(event)" onchange="maxScoring(this.value)" required style="padding:5px;"><div class="scoring_alert"  style='color: red;'></div></th>
    </tr>
    <?php } ?>
    
  </tbody></table>  -->
  </div> 
  <div class="custom-socring-table-cont">

  </div>

  <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
    <div class="scoring_percentage_alert"></div>
    </div><div class="clearfix"></div><br />
  
  <div style="float:right; margin:0 5px;" id="submitBtnCont">
    <button type="button" class="btn btn-info" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
    <button type="button" class="btn btn-success" id="nextBtn" onclick="nextPrev(1)">Next</button>
   
   <div class="pull-right">
        <div class="scoring_alert"  style='color: red;'></div>
         <div class="scoring_alert2"  style='color: red;position: absolute;bottom: 96px;'></div>
            <a href="javascript:void(0);" onclick="createQuoteAndValidation()" class="sub_btns">
                <button type="button" class="btn btn-success submit-btn1 btn-functionality">
                    Create Supplier Evaluation
                </button>
            </a>
              <a href="<?php echo $this->createUrl('vendorEvaluation/list'); ?>" onClick="return confirm('Are you want to cancel?')"  class="sub_btns">
                        <button type="button" class="btn btn-danger btn-functionality">
                           Cancel Supplier Evaluation
                        </button>
                    </a>
    <div class="clearfix"> <br /><br /> </div>
</div>

  </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
</form>
</div>
      <div class="clearfix"> </div>
     </div>
</div>

 


    <style type="text/css">
        canvas {
            display: block;
            max-width: 800px;
            margin: 60px auto;
        }
    </style>
<script type="text/javascript">

  var scoring_criteria_arr_inital = <?php echo json_encode($scoringCriteriaArr); ?>;
  var scoring_criteria_arr = <?php echo json_encode($scoringCriteriaArr); ?>;
  var vendor_dropdown_arr = [];
  var vendor_dropdown_arr_id = [];

  var scoring_criteria_custom_index = 1;

  $("#number_custom_score").hide();

  function loadSubcategories(input_subcategory_id)
{
  var category_id = $('#modal_category_id option:selected').val();
  
  if (category_id == 0)
    $('#subcategory_id_new').html('<option value="0">Select Sub Category</option>');
  else
  { 
      $.ajax({
          type: "POST", data: { category_id: category_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
          success: function(options) {
            var options_html = '<option value="0">Select  Sub Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#subcategory_id_new').html(options_html);
            if (input_subcategory_id != 0) $('#subcategory_id_new').val(input_subcategory_id); 
          },
          error: function() { $('#subcategory_id_new').html('<option value="0">Select Sub Category</option>'); }
      });
  }
}

function  supplierNotInOboloo(){
 $.confirm({
            //title: 'false',
            columnClass: 'col-md-5 col-md-offset-4',
            content: '<label class="control-label">Supplier Name</label><input type="text" class="form-control  notranslate" name="quick_vendor_name" id="quick_vendor_name" placeholder="Supplier Name">',
            buttons: {

                      Yes: {
                            text: "Add Quick Supplier",
                            btnClass: 'btn-blue',
                            action: function(){
                            var new_vendor_name = this.$content.find('#quick_vendor_name').val();
    var new_vendor_emails = '';
    var new_vendor_contact_name = $("#new_vendor_contact").val();
    var new_vendor_address = $("#new_vendor_address").val();
    var new_vendor_address2 = $("#new_vendor_address2").val();
    var new_vendor_city = $("#new_vendor_city").val();
    var new_vendor_state = $("#new_vendor_state").val();
    var new_vendor_zip = $("#new_vendor_zip").val();
    if(new_vendor_name  != ''){
     $(this).prop('disabled',true);
       $.ajax({
            type: "POST", 
            data: { vendor_name:new_vendor_name,vendor_emails:new_vendor_emails,new_vendor_contact_name:new_vendor_contact_name,new_vendor_address:new_vendor_address,new_vendor_address2:new_vendor_address2,new_vendor_city:new_vendor_city,new_vendor_state:new_vendor_state,new_vendor_zip:new_vendor_zip,created_by_activity:'Quikc Sourcing Evaluation'},
            url: BICES.Options.baseurl + '/vendors/quickAddVendor',
            success: function(options) {
               if(options==1){
                $(".quick_msg").html("Email is already taken.");
               }else if(options==2){
                $(".quick_msg").html("New Supplier added successfully.");
                
               }else if(options==3){
                $(".quick_msg").html("Problem occured, try again.");
               }
              $("#save_new_supplier").prop('disabled',false);

              $.alert('Supplier added successfully.');
               $('.score_info').show();
              $(".addindefaultvalues").show();

             //$(".show_supplier_name").append("<input type='text' class='form-control' readonly value='"+new_vendor_name+"' /><br />");

              if(options!=3 && options !=1){
                 

                $(".select_vendor_multiple ").select2("destroy");
  var supplierCon = $(".vendor_name_cont").last().html();
  supplierCon = supplierCon.replace('use-vendor','');
  removeFieldClass = 'wweweremove-vendor-field';
   classCount = $('.'+removeFieldClass).length;
   classCount = classCount+1;
   removeFieldClass2 = '  wweweremove-vendor-field'+classCount;
   removeFieldClassID = 'wweweremove-vendor-field'+classCount;
   removeFieldClassTesting = "'wweweremove-vendor-field"+classCount+"'";



   //supplierCon = $($('option:last', supplierCon).append($("<option value='"+ options+"' selected>"+new_vendor_name+"</option>"));

    
   supplierCon = '<div style="margin-top:10px !important;" id="'+removeFieldClassID+'" data="'+classCount+'" class="'+removeFieldClass+removeFieldClass2+'">'+supplierCon+'<div class="btn btn-danger btn-sm pull-left" style="margin-top: 29px;margin-left: 10px;" onclick="deleteScoringVendorField('+removeFieldClassTesting+','+classCount+')">Delete</div><div class="clearfix"></div></div>';

    
    
 
      $("#vendorNewName").append(supplierCon);
      $("#"+removeFieldClassID+ " .select_vendor_multiple ").append("<option value='"+ options+"' selected>"+new_vendor_name+"</option>");


    
     // Start: removed selected vendor
     $('.select_vendor_multiple option:selected').each(function() { 
       if(parseInt($(this).attr('value'))>0)
        $("select_vendor_multiple option[value='"+$(this).attr('value')+"']").remove();
      });
     // End: removed selected vendor

     // End: removed selected vendor

      //$("#vendorNewName").show();

      $(".select_vendor_multiple").select2({
          placeholder: "Select Supplier",
          containerCssClass: "notranslate",
          dropdownCssClass: "notranslate"
          });

      // Start: removed selected vendor
     $('.select_vendor_multiple').each(function() { 
       if(parseInt($(this).attr('value'))>0)
        $(this).val($(this).attr('value'));
       $(this).trigger("change");
      });
     // End: removed selected vendor
 
              }
               //$('#quick_supplier_modal').modal('hide');
            }

           
        });
      
    }else{
      $(".quick_msg").before("Email is required");
    }

    $(".quick_msg").delay(5000).fadeOut(800);
                            }
                          },
                      No: {
                            text: "Close",
                            btnClass: 'btn-red',
                            action: function(){
                                   
                            }
                          },
                      
                
                
            },title:'<div class="" style="font-size:12px;">New suppliers added via quick sourcing activities are not added to the supplier management module</div>'});
}
 

$(document).ready( function() {
  $(".addindefaultvalues").hide();
  $(".deletedefaultvalues").hide();
  $('#quick_supplier_modal').on('hidden.bs.modal', function () {
    $('#create_quick_sourcing_evaluation').modal('show');
    $("#quick_vendor_name").val('');
    $("#new_vendor_emails").val('');
    $("#new_vendor_contact").val('');
    $("#new_vendor_address").val('');
    $("#new_vendor_address2").val('');
    $("#new_vendor_city").val('');
    $("#new_vendor_state").val('');
    $("#new_vendor_zip").val('');
  });
  $(".select_vendor_multiple").select2({
    placeholder: "Select Supplier",
    allowClear: true,
    containerCssClass: "notranslate",
    dropdownCssClass: "notranslate"
  });

  $("#save_new_supplier").click(function(){
    var new_vendor_name = $("#quick_vendor_name").val();
    var new_vendor_emails = $("#new_vendor_emails").val();
    var new_vendor_contact_name = $("#new_vendor_contact").val();
    var new_vendor_address = $("#new_vendor_address").val();
    var new_vendor_address2 = $("#new_vendor_address2").val();
    var new_vendor_city = $("#new_vendor_city").val();
    var new_vendor_state = $("#new_vendor_state").val();
    var new_vendor_zip = $("#new_vendor_zip").val();
    if(new_vendor_emails  != ''){
     $(this).prop('disabled',true);
       $.ajax({
            type: "POST", 
            data: { vendor_name:new_vendor_name,vendor_emails:new_vendor_emails,new_vendor_contact_name:new_vendor_contact_name,new_vendor_address:new_vendor_address,new_vendor_address2:new_vendor_address2,new_vendor_city:new_vendor_city,new_vendor_state:new_vendor_state,new_vendor_zip:new_vendor_zip,created_by_activity:'Quikc Sourcing Evaluation'},
            url: BICES.Options.baseurl + '/vendors/quickAddVendor',
            success: function(options) {
               if(options==1){
                $(".quick_msg").html("Email is already taken.");
               }else if(options==2){
                $(".quick_msg").html("New Supplier added successfully.");
                
               }else if(options==3){
                $(".quick_msg").html("Problem occured, try again.");
               }
              $("#save_new_supplier").prop('disabled',false);

              alert("Supplier added successfully.");
              $(".addindefaultvalues").show();

             //$(".show_supplier_name").append("<input type='text' class='form-control' readonly value='"+new_vendor_name+"' /><br />");

               vendor_dropdown_arr.push({vendorID : new_vendor_name});
               vendor_dropdown_arr_id.push({vendor : options});

                //makeTable($('.table-responsive'), scoring_criteria_arr,vendor_dropdown_arr,vendor_dropdown_arr_id);

              dropdownVendors("vendor_name_1");
               //$('#quick_supplier_modal').modal('hide');
            }

           
        });
      
    }else{
      $(".quick_msg").before("Email is required");
    }

    $(".quick_msg").delay(5000).fadeOut(800);

});
});

function loadDefaultScoring(){
  $(".deletedefaultvalues").show();

  $(".select_vendor_multiple").trigger("change");
 $.ajax({
        type: "POST", data: { }, dataType: "json",
        url: BICES.Options.baseurl + '/quotes/scoreDefaultValue',
        success: function(options) {
        $.each(options.score, function(k, v) {
          $('input[name="score_percentage['+k+']"').val(v);
          $('input[name="score_percentage['+k+']"').trigger('keyup');
        });
          $(".addindefaultvalues").hide();
          $(".deletedefaultvalues").show();
        }
       
    });
    
}

function removeDefaultScoring(){
  $(".table-responsive").html('');
  scoringCriterShow();
  $(".addindefaultvalues").show();
  $(".deletedefaultvalues").hide();
}

function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
}
function dropdownVendors(obj){
    var testingobj = obj;
    var options_html="";
     $.ajax({
            type: "POST", data: { }, dataType: "json",
            url: BICES.Options.baseurl + '/orders/getVendors',
            success: function(options) {
             
                var options_html = '<option value="">Select Supplier</option>';
                for(var i=0; i<options.suggestions.length; i++)
                    options_html += '<option value="' + options.suggestions[i].data + '">' +  options.suggestions[i].value + '</option>';
                     $("."+testingobj).html(options_html);
               
               
            }
           
        });
}

function scoringCriterShow(){
  var totalEnteredScore = 0;
   $('.score-criteria').each(function(index, currentElement) {
    score = parseFloat($(this).val());
    if(!isNaN(score)){
     totalEnteredScore=totalEnteredScore+score;
    }
  });
   $(".scoring_percentage_alert").html("<strong>"+totalEnteredScore+"/100%</strong>");
};



function setScoringCriterArr(scoring_criteria){
 
}


function makeTable(container, scoring_arr,vendor_arr,vendor_arr_id) {
  var vendor_arr = vendor_arr;
    var table = $("<table/>").addClass('table');
    var i=0;
    var j=0;

    $.each(scoring_arr, function(rowIndex, r) {
        var row = $("<tr/>");
         if(i == 0){
         row.append($("<th class='notranslate' />").text("Scoring Criteria"));
         row.append($("<th class='notranslate' />").text("Scoring Percentage"));

        del=1;
        
         $.each(vendor_arr, function(vendorID, vendorName) { 

        
        

row.append($("<td  class='"+vendorID+" notranslate'/>").html(vendorName.vendorID+"<a style='cursor: pointer; padding: 5px;'></a>"));
  // <span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
  // onclick='deleteScoringVendor("+vendorID+")'
        del++;
             });
        }
        table.append(row);
        var row = $("<tr/>");
        row.append($("<td style='width:18%' />").text(r));
        row.append($("<td style='width:18%'/>").html('<input type="text" name="score_percentage['+rowIndex+']"  class="form-control score-criteria notranslate" placeholder="Enter Scoring Percentage" onkeypress="return isNumberKey(event)" onkeyup="scoringCriterShow()" required style="padding:5px;">'));
        var optionExp=""; 
        for (m = 1; m <=vendor_arr_id.length; ++m) {
           optionExp=optionExp+'<option value="'+m+'">'+m+'</option>';
        }
         del=1;
        $.each(vendor_arr_id, function(vendorIndex, vendorInfo) {
          row.append($("<td class='"+vendorIndex+"'/>").html('<select class="form-control notranslate" name="score_criteria['+rowIndex+']['+vendorInfo.vendor+']">'+ optionExp+'</select>'));
          del++;
             });
          
           i++;
       /* $.each(r, function(colIndex, c) { 

        
            
             $.each(vendor_arr, function(vendorID, vendorName) { 
              row.append($("<td/>").text(vendorName));
             });
        });*/
        table.append(row);
    });
    
    return container.html(table);
}

 

function removeCustomTableRow(className){
    $("."+className).remove();
    // alert("sdfdsf");
}




function updateVendorFieldIndex(){

  removeFieldClass = 'wweweremove-vendor-field';
  var i=1;
  $('.wweweremove-vendor-field').each(function(index, obj){

    htmlExpress = $(this).parent().html();
    var htmlExpressEle = $(this).parent();
    var eleClass= $(this).attr("id");
    var eleCount= $(this).attr("data");
    var eleCountReplacement= i;

    var htmlExpressOthere = $(this).parent().html();
    htmlExpress  = htmlExpress.replace("deleteScoringVendorField('"+$(this).attr("id")+"',"+$(this).attr("data")+")", "deleteScoringVendorField('"+$(this).attr("id")+"',"+i+")");
    //console.log(htmlExpress);
     htmlExpress  = htmlExpress .replace('data="'+$(this).attr("data")+'"','data="'+i+'"');
     console.log(htmlExpress);

     $.ajax({
          type: "POST", data: {htmlExpressOthere:htmlExpressOthere,eleClass:eleClass,eleCount:eleCount,eleCountReplacement:eleCountReplacement}, dataType: "html",
          url: BICES.Options.baseurl + '/vendorEvaluation/replaceExp',
          success: function(options) {
            htmlExpressEle.html(options);
          },
      });
   

   //$(this).parent().html(htmlExpress);


    /*  console.log("deleteScoringVendorField('"+$(this).attr("id")+"',"+$(this).attr("data")+")");

     console.log($(this).attr("id"));
     console.log($(this).attr("data"));*/

     i=i+1;
  });

  /*'<div col-md-6 date-input valid" style="margin-top:10px !important;" class="'+removeFieldClass+removeFieldClass2+'"><input style="width: 88% !important;padding: 7px !important;border-radius: 9px !important; margin-right: 9px;" type="text" class"form-control vendorNewName" readonly value="'+vendorName+'" /><div class="btn btn-danger btn-sm" onclick="deleteScoringVendorField('+removeFieldClassTesting+','+classCount+')">Delete</div></div>'*/
}

function deleteScoringVendorField(classVendorField,c){
  if(confirm("Are you sure want to remove the supplier?")){
   $("."+classVendorField+" .select_vendor_multiple option:selected").val('');
   $("."+classVendorField).remove();
   $(".select_vendor_multiple").trigger('change');
  }

}

function getSupplierSelectedIDs(obj){

}

 $('.score_info').hide();
$(document).ready(function(){
 var vendorArr = [];
 
$(document).on("change",'select',function(){
  
  if($(".deletedefaultvalues").is(":visible")){
    var loadDefaultValue  = 1;
  }else{
    var loadDefaultValue  = 0;
  }

  if($(this).hasClass('select_vendor_multiple')){
    vendorID = $(this).find('option:selected').val();
    vendorName = $(this).find('option:selected').text();
    if(parseInt(vendorID)>0 && $.inArray(vendorID, vendorArr) == -1){
        var vendorIDs ='';
       $('.select_vendor_multiple option:selected').each(function(index, elem) { 
           vendorIDs = vendorIDs+","+$(elem).val();
        }); 
    }
    if(vendorID !=''){
    $.ajax({
          type: "POST", data: {vendorIDs:vendorIDs,loadDefaultValue:loadDefaultValue}, dataType: "html",
          url: BICES.Options.baseurl + '/vendorEvaluation/setVendorScoreTable',
          success: function(options) {
           if($(".deletedefaultvalues").is(":visible")){
              $(".table-responsive").html(options);
            }
          },
      });

    $('.score_info').show();
    $("#number_custom_score").show();
    $(".addindefaultvalues").show();
    $(".custom-socring-table-cont").html("");
     scoring_criteria_custom_index = 1;
    }
  }
});
 
 $("#addNewVendorButoon").click(function(){
  
  $(".select_vendor_multiple ").select2("destroy");
  var supplierCon = $(".vendor_name_cont").last().html();
  supplierCon = supplierCon.replace('use-vendor','');
  removeFieldClass = 'wweweremove-vendor-field';
   classCount = $('.'+removeFieldClass).length;
   classCount = classCount+1;
   removeFieldClass2 = '  wweweremove-vendor-field'+classCount;
   removeFieldClassID = 'wweweremove-vendor-field'+classCount;
   removeFieldClassTesting = "'wweweremove-vendor-field"+classCount+"'";

    
   supplierCon = '<div style="margin-top:10px !important;" id="'+removeFieldClassID+'" data="'+classCount+'" class="'+removeFieldClass+removeFieldClass2+'">'+supplierCon+'<div class="btn btn-danger btn-sm pull-left" style="margin-top: 29px;margin-left: 10px;" onclick="deleteScoringVendorField('+removeFieldClassTesting+','+classCount+')">Delete</div><div class="clearfix"></div></div>';

   
 
      $("#vendorNewName").append(supplierCon);

    
     // Start: removed selected vendor
     $('.select_vendor_multiple option:selected').each(function() { 
       if(parseInt($(this).attr('value'))>0)
        $("select_vendor_multiple option[value='"+$(this).attr('value')+"']").remove();
      });
     // End: removed selected vendor

     // End: removed selected vendor

      //$("#vendorNewName").show();

      $(".select_vendor_multiple").select2({
          placeholder: "Select Supplier",
          containerCssClass: "notranslate",
          dropdownCssClass: "notranslate"
          });

      // Start: removed selected vendor
     $('.select_vendor_multiple').each(function() { 
       if(parseInt($(this).attr('value'))>0)
        $(this).val($(this).attr('value'));
       $(this).trigger("change");
      });
     // End: removed selected vendor
   
    });

/*$("#addNewVendorButoon").click(function(){

   makeTable($('.table-responsive'), scoring_criteria_arr,vendor_dropdown_arr,vendor_dropdown_arr_id);
   $('.custon-socring-table').html('');

   $('.score_info').show();

   $("#number_custom_score").show();
   $(".addindefaultvalues").show();
});*/

 $("#number_custom_score").click(function(){
    var scoreID = 0;
     var vendorIDs ='';
       $('.select_vendor_multiple option:selected').each(function(index, elem) { 
           vendorIDs = vendorIDs+","+$(elem).val();
        }); 



    
    $.ajax({
          type: "POST", data: {vendorIDs:vendorIDs,scoring_criteria_custom_index:scoring_criteria_custom_index}, dataType: "html",
          url: BICES.Options.baseurl + '/vendorEvaluation/getMaximSocreID',
          success: function(options) {
            if( scoring_criteria_custom_index==1){
              $('.custom-socring-table-cont').append(options);
            }else{
              $('#scoring-custom-table').append(options);
            }
            scoring_criteria_custom_index = scoring_criteria_custom_index+1;
           
          },
      });
     
     
  });

});

$(document).ready(function() {
   
    //var cityTable = makeTable($('.table-responsive'), vendor_dropdown_arr,vendor_dropdown_arr);
});
</script>

<script type="text/javascript">
dropdownVendors("vendor_name_1");

$(function(){
  $('.add_new_supplier_not').on('click', function(){
      // var quoteName = $('#quote_name');
      // alert("hello");
     
      $('#quote_name').prop('required',true);
     
    return false;
  });
});


</script>



<script type="text/javascript">
function deleteVendor(vendor_idx)
{
  var display_vendor_count = 0;
    $("div[id^='vendor_area']").each(function () {
      if ($(this).is(':visible')) display_vendor_count += 1;
  });
  
  if (display_vendor_count <= 1) alert("You must have at least one vendor field in the quote form");
  else
  {
    if (confirm("Are you sure you want to delete this vendor?"))
    {
      $('#delete_vendor_flag_' + vendor_idx).val(1);
      $('#vendor_area_' + vendor_idx).hide();
      $('#added_vendors_' + vendor_idx).hide();
    }
  }
}

function displayVendorDetails(vendor_idx_rownum,obj)
{
  var vendor_idx = $('#vendor_name_'+vendor_idx_rownum).val();
  if($('#added_vendors_' + vendor_idx).is(':visible')) $('#added_vendors_' + vendor_idx).hide();
  else
  { 
    var display_vendor_id = $('#display_vendor_id_' + vendor_idx).val();
    var my_vendor_id = $('#vendor_id_' + vendor_idx).val();
    if (true) 
    {
      $('#added_vendors_' + vendor_idx).html('');
      populateVendorDetails(vendor_idx_rownum,vendor_idx);
    }
    $('#added_vendors_' + vendor_idx_rownum).show();
  }
}
var currentTab = 0; // Current tab is set to be the first tab (0)
 // Display the current tab
$(".sub_btns").hide();
showTab(currentTab);
function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
    $("#total_price_cont").hide();
  } else {
    document.getElementById("prevBtn").style.display = "inline";
    $("#total_price_cont").show();
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    $("#nextBtn").hide();
    $(".sub_btns").show();

  } else {
    $("#nextBtn").show();
    $(".sub_btns").hide();
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("order_form").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

var requiredArr = ['location_id','department_id_new','currency_id','quote_name','opening_date','closing_date','product_name_1','quantity_1','quote_currency'];

function requiredArrCheck(field_id) {
  return age >= field_id;
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].querySelectorAll("score_per");
  // A loop that checks every input field in the current tab:
 for (i = 0; i < y.length; i++) { 
    // If a field is empty... 
    if ((y[i].value == "" || y[i].value == 0) && (requiredArr.includes(y[i].id) || requiredArr.includes(y[i].class) ) ) {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}


$(".scoring_alert").hide();
$(".scoring_alert2").hide();
function maxScoring(currentFieldValue=''){
  var totalScore = 0;
  $(".scoring_alert").hide();
  $('.score-criteria').each(function(index, currentElement) { 
    totalScore=totalScore+parseFloat($(this).val());

  });
  return totalScore;
}

function deleteScoringVendor(c){
if(confirm("Are you sure want to remove the vendor?")){
   vendor_dropdown_arr.splice(c, 1);
   vendor_dropdown_arr_id.splice(c, 1);
   $("."+c).hide();
  makeTable($('.table-responsive'), scoring_criteria_arr,vendor_dropdown_arr,vendor_dropdown_arr_id);
}
}





function testingDeleteVendor(c){
 alert(c);
  $('"'+c+'').trigger("click");
}


function createQuoteAndValidation(){
    
    var quote_desc = $("#quote_desc").val();
    var quote_name = $("#quote_name").val();

    if(quote_name ==""){
      $(".description_notes").css('border-color', '#4d90fe');
      $(".quote_name").css('border-color', 'red');
      $(".quote_name").focus();
       return false;
    }else if(quote_desc ==""){
       $(".quote_name").css('border-color', '#4d90fe');
      $(".description_notes").css('border-color', 'red');
      $(".description_notes").focus();
      return false;
    }else{
      $(".quote_name").css('border-color', '#4d90fe');
      $(".description_notes").css('border-color', '#4d90fe');
    }
  var totalScore = maxScoring();
  if(totalScore>100){
    $('.scoring_alert').show();
    $('.scoring_alert').html("Sourcing criteria must not exceed 100%");
  }else if(totalScore<100){
    $('.scoring_alert').show();
    $('.scoring_alert').html("Scoring Criteria must add up to 100%");
  }else if(totalScore==100){
    $('#quote_form').submit();
    $(".btn-functionality").attr('disabled','disabled');
    $(".btn-functionality").prop('disabled',true);
  }
  //(this).prop('disabled',true);
}
</script>
<style type="text/css">
.btn-file {
    padding:6px !important;
}
@media (max-width: @screen-xs-min) {
  .modal-xs { width: @modal-sm; }
}
#save_new_supplier {
  margin-top: 10px;
}
#number_custom_score {cursor: pointer; padding-right: 23px;}


/*
{
  /** width: 100%;
      height: 100%;
      padding: 0;
      margin:0;/**/
   /* width: 90%;
    height: 100%;
    padding: 0;
    margin: 0 auto;
}
.modal-content {    
      height: 100%;
      border-radius: 0;
      color:white;
      overflow:auto;
}
label { color:#73879C; }*/

/* Mark input boxes that gets an error on validation: */
/*#order_form {
  background-color: #ffffff;
  margin: 100px auto;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}*/

/* Style the input fields */


/* Mark input boxes that gets an error on validation: */
.invalid {
  background-color: #ffdddd !important;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none; 
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

/* Mark the active step: */
.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}

.form-control-feedback-new {
    margin-top: 31px !important;
}

.modal-body {
    position: relative;
    overflow-y: auto;
    max-height: 400px;
    padding: 15px;
}
  .remove,.remove:hover,.remove:focus{ text-decoration: none;margin-top: 10px;}

  input:read-only {
  background-color: 
  #827b7b1f;
}
</style>
