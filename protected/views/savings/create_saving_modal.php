<?php
 $disabled = FunctionManager::sandbox();
?>
<div role="main">
<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h3 class="pull-left">Add New Saving Record - Savings Summary</h3>
 <a href="https://oboloo.com/support-adding-a-savings-record/" class="btn btn-warning tutorial pull-right" target="_blank" style="margin-left: 30px;">Tutorial</a> 
<div class="pull-right" style="padding-right: 21px;padding-top: 3px;">
  <span class="step"></span>
</div><br /><br />
<?php 
$userDepartment = FunctionManager::userSettings(Yii::app()->session['user_id'],'department_id');
$userLocation = FunctionManager::userSettings(Yii::app()->session['user_id'],'location_id');
$sql="select vendor_id,vendor_name from vendors where active=1 and member_type=0 and (vendor_archive is null  or vendor_archive='') and quick_created not in('Yes and Quick Sourcing Evaluation','Yes')  ORDER by vendor_name ASC";
$vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
$sql="select contract_id,contract_title from contracts where contract_archive='' or contract_archive is null ORDER by contract_title ASC";
$contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
?>
</div>
  <div class="clearfix"> </div>
  <?php if(!empty(Yii::app()->user->getFlash('success'))):?>
  <div class="tile_count">
      <div class="alert alert-success">
          <?php echo Yii::app()->user->getFlash('success'); ?>
      </div>  </div>
  <?php endif; ?>
  <?php if(Yii::app()->user->hasFlash('error')):?>
  <div class="tile_count">
      <div class="alert alert-danger">
          <?php echo Yii::app()->user->getFlash('error'); ?>
      </div>  </div>
  <?php endif; ?>
  <div class="tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">
    <form id="saving_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('savings/edit'); ?>" autocomplete="off">
       <div class="tab">
      <?php if(!empty($_GET['quote_id'])){?>
      <input type="hidden" class="notranslate" name="quote_id" id="quote_id" value="<?php echo $_GET['quote_id'];?>" />
      <?php } ?>
      <input type="hidden" class="notranslate" name="redirect_url" id="redirect_url" value="">
      <div class="form-group">

        <div class="col-md-12 col-sm-12 col-xs-12 valid"> 
        <label class="control-label">Saving Title <span style="color: #a94442;">*</span> </label>
        <input required type="text" class="form-control notranslate" name="title" id="title" placeholder="Saving Title"  required autocomlete="off">
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12 valid"> 
          <label class="control-label">Description/Notes <span class="error_color">*</span></label>
          <textarea class="form-control notranslate" name="notes" id="notes" rows="4" placeholder="Saving description/notes" 
          required="required" ></textarea>
          </div>
      </div>
    <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-6 valid">
    <label class="control-label">Saving Status <span style="color: #a94442;">*</span></label>
    <select name="status" id="status" class="form-control notranslate" required>
        <option value="">Select Saving Status</option>
        <?php foreach ($saving_status as $status) { ?>
        <option value="<?php echo $status['id']; ?>"><?php echo $status['value']; ?></option>
        <?php } ?>
    </select>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12  date-input">
      <label class="control-label">Saving Currency <span style="color: #a94442;">*</span> </label>
    <select name="currency_id" id="currency_id" class="form-control notranslate" required>
    <?php foreach ($currency_rate as $rate) { ?>
    <option value="<?php echo $rate['id']; ?>" <?php if (isset(Yii::app()->session['user_currency_dropdown']) && Yii::app()->session['user_currency_dropdown'] == $rate['currency']) echo ' selected="SELECTED" '; ?>>
    <?php echo $rate['currency']; ?>
    </option>
    <?php } ?>
    </select>
  </div>
  </div>
   <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">Saving Category <span style="color: #a94442;">*</span> </label>
        <select name="category_id" id="modal_category_id"
          class="form-control notranslate" onchange="loadSubcategories(0);" required>
          <option value="">Select Category</option>
        <?php foreach ($categories as $category) { ?>
          <option value="<?php echo $category['id']; ?>">
            <?php echo $category['value']; ?>
          </option>
        <?php } ?>
      </select>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">Saving Sub Category</label>
        <select name="subcategory_id" id="subcategory_id_new"
          class="form-control notranslate">
          <option value="0">Select Sub Category</option>
        </select>
      </div>
    </div>
    <div class="form-group"> 
      <div class="col-md-6 col-sm-6 col-xs-6 valid">
      <label class="control-label">Location <span style="color: #a94442;">*</span></label>
      <select required name="location_id" id="location_id" class="form-control notranslate" onchange="loadDepartmentsForSingleLocation(this);">
                <option value="">Select Location</option>
                <?php foreach ($locations as $location) { ?>
                    <option value="<?php echo $location['location_id']; ?>"
                          <?php if ( $location['location_id'] == $userLocation) echo ' selected="SELECTED" '; ?>>
                        <?php echo $location['location_name']; ?>
                    </option>
                <?php } ?>
            </select>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-6 valid">
            <label class="control-label">Department <span style="color: #a94442;">*</span></label>
            <select required name="department_id" id="department_id_new" class="form-control notranslate">
              <option value="">Select Department</option>
            </select>
        </div>
    </div>
      <div class="form-group"> 
        <div class="supplier_invite_container">
        <div class="vendor_new_QIX">
         <div class="col-md-6 col-sm-6  col-xs-6 date-input valid">
        
        <label class="control-label">Supplier Name</label>
        <select class="form-control select_vendor_multiple notranslate vendor_new_QIX vendor_new_vendor_name_QIX supplier_select" name="vendor_id" style="  border-radius: 10px;border: 1px solid #4d90fe;">
          <option value=""></option>
          <option value="NEW_SUPPLIER_SAVING" style="margin-left:20px;"><a data-toggle="modal" href="#mySavingSupplierModal" class="btn btn-primary"><b>+ Add New</b></a></option>
          <?php foreach($vendorReader as $value){?>
          <option value="<?php echo $value['vendor_id'];?>"><?php echo $value['vendor_name'];?></option>
          <?php } ?>
        </select>

     <!--   <div class="clearfix"></div><br />
       <span  id="hide_new_supplier"  title="Remove Quick Add Vendor fields" style="margin-bottom: 20px;" class="btn btn-danger">Remove Supplier Not In oboloo</span>
        <span  id="new_supplier" title="Quick Add Vendor" class="btn btn-success">Add New Supplier Not In Oboloo</span>
          -->
        </div>

         <div class="col-md-6 col-sm-6  col-xs-6 date-input valid">

           <label class="control-label">Associated Contract</label>
            <select class="form-control select_vendor_multiple notranslate" name="contract_id" id="contract_id" style="  border-radius: 10px;border: 1px solid #4d90fe;">
             <option value=""></option>
            <?php foreach($contractReader as $value){?>
              <option value="<?php echo $value['contract_id'];?>"><?php echo $value['contract_title'];?></option>
            <?php } ?>
          </select>
         </div>
         
      <div class="clearfix"></div><br />
      <div class="new_supplier_con col-md-12 col-sm-12 col-xs-12">
      <div class="clearfix"></div><br />
      <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
            <input type="text" class="form-control notranslate" name="new_vendor_name" id="new_vendor_name" placeholder="Supplier Name">
            <span class="new_vendor_err" style="display: none; font-size: 10px;">Supplier Name requried.</span>
        </div>
        <div class="clearfix"></div><br />
        <div class="col-md-6 col-sm-6 col-xs-12 quick_vendor_field">
          <label class="control-label">Email Address <span style="color: #a94442;">*</span></label>
          <input type="text" class="form-control  notranslate" name="new_vendor_emails" id="new_vendor_emails"  placeholder="Email Address">
          
        <div class="col-md-6 col-sm-6 col-xs-12 date-input quick_vendor_field">
          <label class="control-label ">Confirm Email Address <span style="color: #a94442;">*</span></label>
          <input type="text" class="form-control emails_error notranslate" name="confm_emails" id="confm_emails" placeholder="Email Address" >
          <span class="emails_error" style="display: none; font-size: 10px;">Email Address and Confirm Email Address are not matching.</span>
        </div>
        <div class="clearfix"></div><br />
        <div class="col-md-12 col-sm-12 col-xs-12 quick_vendor_field"><label class="control-label">Contact Person <span style="color: #a94442;">*</span></label><input type="text" class="form-control notranslate" name="new_vendor_contact" id="new_vendor_contact" value="" placeholder="Contact Person">
          <span class="new_vendor_cont_err" style="display: none; font-size: 10px;">Contact Person requried</span></div>
        <div class="clearfix"></div><br />
        <div class="col-md-6 col-sm-6 col-xs-6 quick_vendor_field"><label class="control-label">Address Line 1</label><input type="text" class="form-control notranslate" name="new_vendor_address" id="new_vendor_address"  value="" placeholder="Address Line 1"></div>

        <div class="col-md-6 col-sm-6 col-xs-6 quick_vendor_field"><label class="control-label">Address Line 2</label><input type="text" class="form-control notranslate" name="new_vendor_address2" id="new_vendor_address2" value="" placeholder="Address Line 2"></div></div>
        <div class="clearfix"></div><br />
        <div class="form-group">
        <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">City</label><input type="text" class="form-control notranslate" name="new_vendor_city" id="new_vendor_city" value="" placeholder="City"></div>

        <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">State</label><input type="text" class="form-control notranslate" name="new_vendor_state" id="new_vendor_state" value="" placeholder="State"></div>

        <div class="col-md-4 col-sm-4 col-xs-4 quick_vendor_field"><label class="control-label">Post Code/Zip Code</label><input type="text" class="form-control notranslate" name="new_vendor_zip" id="new_vendor_zip" value="" placeholder="Post Code/Zip Code"></div>
        <div class="clearfix"></div><br />
        <div class="form-group quick_vendor_field">
          <span class='quick_msg pull-right' style='color: #a94442;'></span><br />
          <span class="btn btn-success btn-sm pull-right" id="save_new_supplier" title="Save Quick Vendor">Save Quick Vendor</span>
        </div>
        <div class="clearfix"></div>
        </div>
      </div><br></br />
        <div class="clearfix"></div><br /><br />
        </div>
      </div>
    </div>
  <div class="clearfix"><br /></div>
    <div class="form-group">
      <div class="col-md-6">          
        <p id="supporting_document_showhide" class="btn btn-sm btn-success submit-btn"> <span class="glyphicon glyphicon-plus"></span>
          Add Milestone 
        </p>
        <div id="supporting_document_hide" class="btn btn-sm btn-danger"> <span class="glyphicon glyphicon-minus"></span>
          Remove milestone 
        </div>
      </div><div class="clearfix"></div>
      <div id="supporting_document_area" style="margin-top: 26px">
      <div id="ajax_success" class="alert alert-success" role="alert"></div>
      <img class="processing_oboloo" src="<?php echo AppUrl::bicesUrl('images/icons/processin_oboloo.gif'); ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <label class="control-label">Milestone Name *</label>
          <input type="text" class="form-control notranslate" name="milestone_title" id="milestone_title" placeholder="Milestone Name" /> 
          <span class="text-danger required_msg"></span>
        </div>
        <div class="clearfix"></div><br />
        <div class="col-md-6 col-sm-6 col-xs-12">
      <label class="control-label">Due Date</label>
      <input type="text" class="form-control notranslate" name="due_date" id="due_date" /> 
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">Baseline Spend </label>
        <input type="number" class="form-control notranslate" name="base_line_spend" id="base_line_spend" placeholder="Baseline Spend" /> 
      </div>
         <div class="clearfix"></div><br />
      <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">Projected Cost Reduction</label>
        <input type="number" class="form-control notranslate" name="cost_reduction" id="cost_reduction" placeholder="Projected Cost Reduction" /> 
      </div>
       
      <div class="col-md-6 col-sm-6 col-xs-12">
       <label class="control-label">Projected Cost Avoidance </label>
       <input type="number" class="form-control notranslate" name="cost_avoidance" id="cost_avoidance" placeholder="Projected Cost Avoidance" /> 
      </div>
      <div class="clearfix"></div><br/>

     
      <div class="clearfix"></div>
      <div class="col-md-2 col-sm-2 col-xs-12 pull-right text-right">
      <div id="add_milestone_btn"  <?php echo $disabled; ?> class="btn btn-sm btn-success submit-btn" onclick="saveMileStone()" style="margin-top: 31px;background: #48d6a8 !important;border-color: #48d6a8 !important;">
          Save Milestone 
       </div>
      </div>
       </div>
    </div>

  </div>
  <div class="clearfix"></div><br />
     <div id="milestone_list_cont"></div>
  <div class="clearfix"></div><br /><br />
  <div class="form-group">
 <!--  <div class="col-md-12 col-sm-12 col-xs-12 valid">
    <label class="control-label">Saving Notes <span style="color: #a94442;">*</span></label>
    <textarea class="form-control notranslate" name="notes" id="notes" rows="4" placeholder="Savings Notes" required></textarea>
  </div> -->
   <div class="clearfix"></div><br />
    <div class="col-md-6 col-sm-6 col-xs-12">
      <label class="control-label">Total Project Savings </label>
      <input type="number" class="form-control notranslate" name="total_project_saving" id="total_project_saving" placeholder="Total Project Savings" readonly /> 
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
     <label class="control-label">Total Project Savings % </label>
     <input type="number" class="form-control notranslate" name="total_project_saving_perc" id="total_project_saving_perc" placeholder="Total Project Savings %" readonly />
    </div>
    <div class="clearfix"></div>
  </div>
  </div>
  <div style="float:right; margin:22px 5px;" id="submitBtnCont">
  <div class="pull-right">
    <button type="button" class="btn btn-info" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
    <button type="button" class="btn btn-success" id="nextBtn" onclick="nextPrev(1)">Next</button>
    <div class="scoring_alert2"  style='color: red;position: absolute;bottom: 96px;'></div>

      <!-- Start: Approver -->
          <div class="" style="margin-top: -37px;">
            <div class="checkbox">
                <label><input type="checkbox" class="flat notranslate" name="approver_id" id="approver_user_id" value="1"></label>
                <label style="padding: 0;margin-top: 10px;display: inline-block;"><div class="clearfix"></div><br />Does someone in your organisation need to approve this saving?
                </label>
            </div>
            <div class="clearfix"></div><br />
            <div id="show_approver_user" style="margin-bottom: 18px;text-align: left;">
                <label>Select Approver</label>
                <?php  $sql = " SELECT user_id,full_name FROM `users` where admin_flag !=1 order by full_name asc";
                $getUsers = Yii::app()->db->createCommand($sql)->queryAll(); 
                 ?>
                <select  class="form-control select_user_contract notranslate" name="approver_id" id="approver_id" style="border-radius: 10px;border: 1px solid #4d90fe;">
                  <option value="">Select Approver</option>
                    <?php foreach ($getUsers as $all_user) { ?>
                    <option value="<?php echo $all_user['user_id']; ?>">
                      <?php echo $all_user['full_name']; ?>
                    </option>
                    <?php } ?>
               </select>
              </div>
            </div>
            <div class="clearfix"></div>
           
          <!-- End: Approver -->

    <button type="submit" <?php echo $disabled; ?>  class="btn btn-primary btn-functionality "  >Add Savings Record</button>
    <a href="<?php echo $this->createUrl('savings/list'); ?>" onClick="return confirm('Are you want to cancel?')"  class="sub_btns"><button type="button" class="btn btn-danger btn-functionality ">Cancel Savings Record</button>
    </a>
  </div>
  <div class="clearfix"></div>
  </div>

<div class="clearfix"></div>
<div class="form-group">
<!-- <div class="col-md-3 col-xs-12">
  <label class="control-label">Savings Start Date</label>
  <input type="text" readonly class="form-control notranslate"  name="start_date" id="start_date" placeholder="Savings Start Date" />
</div> -->
<div class="col-md-4 col-xs-12">
  <label class="control-label">Savings Due Date</label>
  <input type="text" readonly class="form-control notranslate" name="saving_due_date" id="saving_due_date" placeholder="Savings Due Date" /> 
</div>
<div class="col-md-4 col-xs-12">
  <label class="control-label">Created On</label>
  <input type="text" readonly class="form-control notranslate" name="created_at" id="created_at" placeholder="Created On" value="<?php echo date(FunctionManager::dateFormat());?>" /> 
</div>
<div class="col-md-4 col-xs-12">
  <label class="control-label">Created By</label>
  <input type="text" readonly class="form-control notranslate" name="user_name" id="user_name" placeholder="Created By" value="<?php echo Yii::app()->session['full_name'];?>" /> 
</div>
 <div class="clearfix"></div>
</div>
</div>
</form>
</div></div>
</div>
<?php echo $this->renderPartial("_create_saving_supplier"); ?>
<style type="text/css">
  canvas { display: block; max-width: 800px; margin: 60px auto;}
</style>
<script type="text/javascript">
$(document).ready( function() {
 $("#supporting_document_area").hide();
 $("#supporting_document_hide").hide();
 $('#show_approver_user').hide();

  $("#supporting_document_showhide").click(function(){
  $("#supporting_document_area").show();
  $("#supporting_document_showhide").hide();
  $("#supporting_document_hide").show();
  $(".show-remove-btn-alert-hide").hide();
});

$("#supporting_document_hide").click(function(){
  $("#supporting_document_area").hide();
  $("#supporting_document_showhide").show();
  $("#supporting_document_hide").hide();
  $(".show-remove-btn-alert-hide").show();
});

$('#approver_user_id').on('click', function () {
  if($(this).prop('checked')) {
    $('#show_approver_user').fadeIn();
  }else{
    $('#show_approver_user').hide();
  }
});

  $('#due_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> '});
  $('#milestone_saving_due_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> '});
  
  $('#created_at').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> '});
  $('#saving_due_date').datetimepicker({
    format: '<?php echo FunctionManager::dateFormatJS();?> '});
  // $('#start_date').datetimepicker({
  //   format: '<?php echo FunctionManager::dateFormatJS();?>'});
  
  $(".select_vendor_multiple").select2({
       placeholder: "Select Supplier",
       allowClear: true,
       containerCssClass: "notranslate",
       dropdownCssClass: "notranslate"
      }).on('select2:close', function() {
        var el = $(this);
    if(el.val()==="NEW_SUPPLIER_SAVING") {
      $("#mySavingSupplierModal").show('modal');
    }
  });

 $(".select_associated_multiple").select2({
      placeholder: "Select Associated Contract",
      allowClear: true,
      containerCssClass: "notranslate",
      dropdownCssClass: "notranslate"
  });
  

  $("#saving_form").submit(function(){
    var redirectUrl = $("#redirect_url").val();
    if(redirectUrl == null || redirectUrl==''){
     
      $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Would you like to complete the additional details for this saving? (Milestones/timelines etc)</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-success',
                            action: function(){
                              $("#redirect_url").val('edit');
                              $("#saving_form").submit();
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-info',
                            action: function(){
                                $("#redirect_url").val('list');
                                $("#saving_form").submit();
                            }
                          },
                
                
                
            }});
  
       return false;
    }
    
  });
});

function createSavingAndValidation(){

  return false;
}
function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
}



function selectDepartment(dpartmentID ){
  $('#department_id_new').prop('selectedIndex', dpartmentID);
}
function loadDepartments(location_id)
{
    var location_id = location_id;
    var single = 1;

  
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getDepartments',
            success: function(options) {
                var options_html = '<option value="">Select</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                $('#department_id_new').html(options_html);
                selectDepartment('<?php echo $userDepartment;?>');
            },
            error: function() { $('#department_id_new').html('<option value="">Select</option>'); }
        });
}

function loadDepartmentsForSingleLocation(sel)
{
    var location_id = sel.value;
    var single = 1;
    loadDepartments(location_id);
}

 $(document).ready(function(){

  $("#field_value,#milestone_saving_due_date,#cost_reduction").on("change keyup blur",function(){
  var duration = $('#field_value').val();
  var start_date = $('#milestone_saving_due_date').val();
  var cost_reduction = $('#cost_reduction').val();
  if(parseFloat(duration)>0 && parseFloat(cost_reduction)>0){
   $.ajax({
          type: "POST", 
          data: { duration: duration,start_date,start_date,cost_reduction:cost_reduction }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('savings/durationValue/'); ?>",
          success: function(results) {
           $('#amountFields').html(results.durationList);
           $('#due_date').val(results.endDate);
           //$('#total_project_saving_perc').val(results.totalPlanned);

          }
  });
  }
  });
 });

/*function loadFieldValueChange(field_value){
  var duration = $('#field_value').val();
  var start_date = $('#start_date').val();
  var cost_reduction = $('#cost_reduction').val();
   $.ajax({
          type: "POST", 
          data: { duration: duration,start_date,start_date,cost_reduction:cost_reduction }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('savings/durationValue/'); ?>",
          success: function(results) {
           $('#amountFields').html(results.durationList);
          }
  });
}*/

function loadSubcategories(input_subcategory_id)
{
  var category_id = $('#modal_category_id option:selected').val();
  
  if (category_id == 0)
    $('#subcategory_id_new').html('<option value="0">Select Sub Category</option>');
  else
  { 
      $.ajax({
          type: "POST", data: { category_id: category_id }, dataType: "json",
          url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
          success: function(options) {
            var options_html = '<option value="0">Select  Sub Category</option>';
            for (var i=0; i<options.suggestions.length; i++)
              options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
            $('#subcategory_id_new').html(options_html);
            if (input_subcategory_id != 0) $('#subcategory_id_new').val(input_subcategory_id); 
          },
          error: function() { $('#subcategory_id_new').html('<option value="0">Select Sub Category</option>'); }
      });
  }
}
</script>

<?php if ($userLocation) { ?>
  <script type="text/javascript">loadDepartments("<?php echo $userLocation;?>");</script>
  <?php } ?>

<script type="text/javascript">

var show_hide_score=1;
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab
function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";

  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    $("#nextBtn").hide();
  } else {
    $("#nextBtn").show();
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:


  if (n == 1 && !validateForm()) return false;
  if(currentTab==0){

       $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Would you like to add questionnaires or scoring criteria?</span>',
            buttons: {

                      Yes: {
                            text: "Yes",
                            btnClass: 'btn-blue',
                            action: function(){
                                   show_hide_score = 1;
                            }
                          },
                      No: {
                            text: "No",
                            btnClass: 'btn-red',
                            action: function(){
                                  $("#nextBtn").trigger('click');
                                   show_hide_score = 0;
                            }
                          },
                
                
                
            }
        });

   
      /*if(!confirm('Would you like to add questionnaires or scoring criteria?')){
       n=2;
       show_hide_score = 0;
     }else{
      show_hide_score = 1;
     }*/
  }else if(currentTab==2){
    if(show_hide_score==0){
       n=-2;
    }
  }
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("order_form").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

var requiredArr = ['title','modal_category_id','subcategory_id_new','currency_id'/*,'notes'*/];


function requiredArrCheck(field_id) {
  return age >= field_id;
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].querySelectorAll("select,input,textarea");
  // A loop that checks every input field in the current tab:
 for (i = 0; i < y.length; i++) { 
    // If a field is empty... 
 
    if ((y[i].value == "" || y[i].value == 0) && (requiredArr.includes(y[i].id) || requiredArr.includes(y[i].class) ) ) {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }else {
       $('#'+ y[i].id).removeClass('invalid');
    }
  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}



$(".new_supplier_con").hide();
$("#hide_new_supplier").hide();
$("#new_supplier").click(function(){
  $(".new_supplier_con").show();
  $("#hide_new_supplier").show();
    $(".quick_vendor_field").show();
  
});
$("#hide_new_supplier").click(function(){
  $(".new_supplier_con").hide();
  $("#new_vendor_name").val("");
  $("#new_vendor_emails").val("");
  $("#hide_new_supplier").hide();
});



$("#save_new_supplier").click(function(){
      
      var email = $("#new_vendor_emails").val();
      var conf_email = $("#confm_emails").val();
      var new_vendor_name = $("#new_vendor_name").val();
      var new_vendor_emails = $("#new_vendor_emails").val();
      var new_vendor_contact_name = $("#new_vendor_contact").val();     
      var new_vendor_address = $("#new_vendor_address").val();
      var new_vendor_address2 = $("#new_vendor_address2").val();
      var new_vendor_city = $("#new_vendor_city").val();
      var new_vendor_state = $("#new_vendor_state").val();
      var new_vendor_zip = $("#new_vendor_zip").val();

    if(email != conf_email) {
      $(".emails_error").css('color', 'red').show();
      return false;
      }

    if (new_vendor_name =="") {
      $(".new_vendor_err").css('color', 'red').show();
      return false;
    }
 
    if(new_vendor_contact_name ==""){
      $(".new_vendor_cont_err").css('color', 'red').show();
      return false;
    }

    if(new_vendor_emails  != ''){
     $(this).prop('disabled',true);
       $.ajax({
            type: "POST", 
            data: { vendor_name:new_vendor_name,vendor_emails:new_vendor_emails,new_vendor_contact_name:new_vendor_contact_name,new_vendor_address:new_vendor_address,new_vendor_address2:new_vendor_address2,new_vendor_city:new_vendor_city,new_vendor_state:new_vendor_state,new_vendor_zip:new_vendor_zip},
            url: BICES.Options.baseurl + '/vendors/quickAddVendor',
            success: function(options) {
                if(options==1){
                $(".quick_msg").html("Email is already taken.");
                 alert("Supplier with this Email is already taken. Please try another.");
               }else if(options !=1){
                $(".quick_msg").html("New Supplier added successfully.");
                 alert("Supplier added successfully.");
                 /*$("#hide_new_supplier").trigger("click");*/
                  //
                  $(".quick_vendor_field").hide();

                  
                  $("#quick_vendor_name");
                  $("#new_vendor_emails").val('');
                  $("#confm_emails").val('');
                  $("#new_vendor_contact").val('');
                  $("#new_vendor_address").val('');
                  $("#new_vendor_address2").val('');
                  $("#new_vendor_city").val('');
                  $("#new_vendor_state").val('');
                  $("#new_vendor_zip").val('');

               }else if(options==3){
                $(".quick_msg").html("Problem occured, try again.");
                 alert("Problem occured, try again.");
               }
              $("#save_new_supplier").prop('disabled',false);
            
           
            }

           
        });
      
    }else{
      $(".quick_msg").before("Email is required");
    }

    $(".quick_msg").delay(5000).fadeOut(800);

});




$("#ajax_success").hide();
$(".processing_oboloo").hide();

function saveMileStone(){ 

  var mileStone = $("#milestone_title").val();  
  var dueDate = $("#due_date").val();
  var milestonedueDate = $("#milestone_saving_due_date").val();
  var baseLine = $("#base_line_spend").val();
  var costReduction = $("#cost_reduction").val();
  var costAvoidance = $("#cost_avoidance").val();
  
  if(mileStone == ""){
    $(".required_msg").text('Milestone Name is required.');
    return false;
  }else{
    $.ajax({
        url: BICES.Options.baseurl + '/savings/addMileStone', // point to server-side PHP script 
        dataType: 'json',  // what to expect back from the PHP script, if anything
        data: {title:mileStone,due_date:dueDate,miletone_due_date:milestonedueDate,base_line_spend:baseLine,cost_reduction:costReduction,cost_avoidance:costAvoidance},
        type: 'post',
         beforeSend: function() {
          $(".processing_oboloo").show();
          $("#ajax_success").html('');
          $("#ajax_success").hide();
          $(".btn-functionality").attr('disabled','disabled');
          $(".btn-functionality").prop('disabled',true);
          $("#add_milestone_btn").prop('disabled',true);
          $("#nextBtn").prop('disabled',true);
         },
        success: function(uploaded_response){

          if(uploaded_response.mesg==1){
            $("#ajax_success").show();
            $("#ajax_success").html('Milestone saved successfully');
          }else{
            $("#ajax_success").html('Problem occured while saving Milestone, try again');
            $("#ajax_success").show();
          }
          $(".processing_oboloo").hide();
          $(".btn-functionality").prop('disabled',false);
          $("#add_milestone_btn").prop('disabled',false);
          $("#nextBtn").prop('disabled',false);
          $("#milestone_list_cont").html(uploaded_response.record_list);

          $("#total_project_saving").val(uploaded_response.total_project_saving);
          $("#total_project_saving_perc").val(uploaded_response.total_project_saving_perc);
          $("#saving_due_date").val(uploaded_response.due_date);
          
          $("#milestone_title").val('');
          $("#due_date").val('');
          $("#milestone_saving_due_date").val('');
          $("#base_line_spend").val('');
          $("#cost_reduction").val('');
          $("#cost_avoidance").val('');
          
          $(".required_msg").text('');
        }
     });
   }
  $("#ajax_success").delay(1000*5).fadeOut();
}

function saveMileStoneCustom(){ 

  var mileStone = $("#milestone_title").val();  
  var dueDate = $("#due_date").val();
  var milestonedueDate = $("#milestone_saving_due_date").val();
  // var mileStoneStartDate = $("#start_date").val();
  var baseLine = $("#base_line_spend").val();
  var costReduction = $("#cost_reduction").val();
  var costAvoidance = $("#cost_avoidance").val();
  var savingDuration=$("#field_value").val();
  var durationMontly= new Array();
  $('.milestone-plann-duration').each(function(){
      mothly  = {
        'fieldName' : $(this).attr("name"),
        'fieldValue' : $(this).val()
      };
      durationMontly.push(mothly);
  });
  
  $.ajax({
        url: BICES.Options.baseurl + '/savings/addMileStone', // point to server-side PHP script 
        dataType: 'json',  // what to expect back from the PHP script, if anything
        data: {title:mileStone,due_date:dueDate,miletone_due_date:milestonedueDate,base_line_spend:baseLine,cost_reduction:costReduction,cost_avoidance:costAvoidance,saving_duration:savingDuration, duration_monthly: durationMontly},
        type: 'post',
         beforeSend: function() {
          $(".processing_oboloo").show();
          $("#ajax_success").html('');
          $("#ajax_success").hide();
          $(".btn-functionality").attr('disabled','disabled');
          $(".btn-functionality").prop('disabled',true);
          $("#add_milestone_btn").prop('disabled',true);
          $("#nextBtn").prop('disabled',true);
         },
        success: function(uploaded_response){

          if(uploaded_response.mesg==1){
            $("#ajax_success").show();
            $("#ajax_success").html('Milestone saved successfully');
          }else{
            $("#ajax_success").html('Problem occured while saving Milestone, try again');
            $("#ajax_success").show();
          }
          $(".processing_oboloo").hide();
          $(".btn-functionality").prop('disabled',false);
          $("#add_milestone_btn").prop('disabled',false);
          $("#nextBtn").prop('disabled',false);
          $("#milestone_list_cont").html(uploaded_response.record_list);

          $("#total_project_saving").val(uploaded_response.total_project_saving);
          $("#total_project_saving_perc").val(uploaded_response.total_project_saving_perc);
          $("#saving_due_date").val(uploaded_response.due_date);
          
          $("#milestone_title").val('');
          $("#due_date").val('');
          $("#milestone_saving_due_date").val('');
          $("#base_line_spend").val('');
          $("#cost_reduction").val('');
          $("#cost_avoidance").val('');
          // $("#start_date").val('');
          $("#field_value").val('');
          $("#amountFields").html('');
        }
     });

   
    $("#ajax_success").delay(1000*5).fadeOut();
}


function deleteMilestone(recordKey)
{
  var recordKey = recordKey;
  $("#ajax_success").hide();
  $.confirm({
            title: false,
            content: '<spam style="font-size:13px">Are you sure want to delete this file/document</span>',
            buttons: {
               Yes: {
                      text: "Yes",
                      btnClass: 'btn-blue',
                      action: function(){
                          $.ajax({
                              dataType: 'json',  
                              type: "post",
                              url: "<?php echo AppUrl::bicesUrl('savings/deleteMilestone'); ?>",
                              data: { recordKey: recordKey },
                              success: function(uploaded_response){
                                if(uploaded_response.mesg==1){
                                  $("#ajax_success").show();
                                  $("#ajax_success").html('File deleted successfully');
                                }else{
                                 $("#ajax_success").show();
                                 $("#ajax_success").html('Problem occured while deleting record, try again');
                                }
                                 $("#milestone_list_cont").html(uploaded_response.record_list);
                                 $("#total_project_saving").val(uploaded_response.total_project_saving);
                                 $("#total_project_saving_perc").val(uploaded_response.total_project_saving_perc);
                                 $("#saving_due_date").val(uploaded_response.due_date);
                              }
                          });
                      }
                    },
                      No: {
                        text: "No",
                        btnClass: 'btn-red',
                        action: function(){}
                      },
            }});
 
      $("#ajax_success").delay(1000*5).fadeOut();
}


function supplierSelect(vendor_id,serialNum){
  var vendor_id = vendor_id;
  var serialNum = serialNum;

  var obj ='';
  if(serialNum=='QIX'){ 
    obj = $(".vendor_new_vendor_name_QIX");
  }else{
      obj = $(".vendor_new_vendor_name_"+serialNum);
  }
  $(obj).next('.select2-container').find('.select2-selection--single').removeClass('invalid');

   $.ajax({
          dataType:'json',  
          type: "post",
          url: "<?php echo AppUrl::bicesUrl('vendors/getVendorDetail'); ?>",
          data: { vendor_id: vendor_id },
          success: function(data_option){
          if(serialNum=='QIX'){
             $(".vendor_contact_new_QIX").html(data_option.contact_name);
             //$(".vendor_email_new_QIX").val(data_option.emails);
          }else{
           $(".vendor_contact_new_"+serialNum).html(data_option.contact_name);
           //$(".vendor_email_new_"+serialNum).val(data_option.emails);

          }
          }
        });
  }
</script>
<!-- Start: eSourcing/Quotes Savings Record --> 
<?php if(!empty($_GET['quote_id'])){?>
  <script>
$(document).ready(function(){
$("#supporting_document_showhide").trigger("click");
$('#cost_reduction').val("<?php echo $_GET['cost_savings'];?>");
$('#title').val("<?php echo $_GET['quote-name'];?>");
$('#milestone_title').val("<?php echo $_GET['quote-name'];?>");
$('.vendor_new_vendor_name_QIX option[value="<?php echo $_GET['vendor_id'];?>"]').attr('selected','selected');
$('.vendor_new_vendor_name_QIX').trigger("change");

$('#location_id option[value="<?php echo $_GET['location_id'];?>"]').attr('selected','selected');

$('#currency_id option[value="<?php echo $_GET['currency_id'];?>"]').attr('selected','selected');

 

$('#modal_category_id option[value="<?php echo $_GET['contract_category_id'];?>"]').attr('selected','selected');
$('#modal_category_id').trigger("change");

 setTimeout(function() { 
    $('#department_id_new option[value="<?php echo $_GET['department_id'];?>"]').attr('selected','selected');
    $('#subcategory_id_new option[value="<?php echo $_GET['contract_subcategory_id'];?>"]').attr('selected','selected');
 }, 200);

  });
</script>
<?php } ?>
<!-- End: eSourcing/Quotes Savings Record --> 
<style type="text/css">
.btn-file {
 padding:6px !important;
}
@media (max-width: @screen-xs-min) {
 .modal-xs { width: @modal-sm; }
}
#save_new_supplier {
 margin-top: 10px;
}

#number_custom_score {cursor: pointer; padding-right: 10px;}


/* Mark input boxes that gets an error on validation: */
.invalid {
  background-color: #ffdddd !important;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 20px;
  width: 20px;
 /* margin: 0 2px;*/
  background-color: #bbbbbb;
  border: none; 
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
  display: inline-block;
  padding-left: 7px;
  color: #fff;

}

/* Mark the active step: */
.step.active {
  opacity: 1;
  background-color: #2d9ca2;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #2d9ca2;
}

.form-control-feedback-new {
    margin-top: 31px !important;
}
.ui-datepicker {
    width:auto;
}
.select2-container {
    /*border-bottom: 1px solid #4d90fe !important;*/
    -webkit-box-shadow: none;
    box-shadow: none;
}

.step.active {
    background-color:none !important
}
.step {
    height: 0px;
    width: 0px;
  }
.help-block {
    color: #f40707 ;
}
</style>

<!-- Modals -->
<div class="modal fade" id="product_modal" tabindex="-1" role="dialog" style="z-index: 999">
  <div class="modal-dialog" role="document" style="
    position: relative;
    top: 41%;
    transform: translateY(-50%);">
    <div class="modal-content" style="max-width: 538px; margin: auto;">
      <div class="modal-body">

        <div class="new_supplier_con col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label">Supplier Name</label>
            <input type="text" class="form-control  notranslate" name="quick_vendor_name" id="quick_vendor_name" placeholder="Supplier Name">
        
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <span class='quick_msg pull-right' style='color: #a94442;'></span><br />
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal" title="Close Quick Supplier" style="margin-top: 8px;">Close</button>
          <span class="btn btn-primary btn-sm pull-right" id="save_new_supplier" title="Save Quick Supplier">Add Quick Supplier</span>
          <div class="clearfix"></div><br />
          <div class="text-right">New suppliers added via quick sourcing activities are not added to the supplier management module</div>

        </div>

        <div class="clearfix"></div>
        </div>
      </div>
      
        
      </div>
    </div>
  </div>
</div>
