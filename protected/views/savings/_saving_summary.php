<?php 
$dateFormate =FunctionManager::dateFormat();
$userDepartment = FunctionManager::userSettings(Yii::app()->session['user_id'],'department_id');
$userLocation = FunctionManager::userSettings(Yii::app()->session['user_id'],'location_id');
$sql="select vendor_id,vendor_name from vendors where active=1 and member_type=0 and (vendor_archive is null  or vendor_archive='') and quick_created not in('Yes and Quick Sourcing Evaluation','Yes')  ORDER by vendor_name ASC";
       $vendorReader = Yii::app()->db->createCommand($sql)->query()->readAll();
$sql="select contract_id,contract_title from contracts where contract_archive='' or contract_archive is null";
$contractReader = Yii::app()->db->createCommand($sql)->query()->readAll();
$disabled    = FunctionManager::sandbox();
$userPer = new User;
$accessToSuperAdmin = $userPer->getOne(array('user_type_id' => 4, 'user_id' => Yii::app()->session['user_id']));
$editSaving ='';
if (!empty($accessToSuperAdmin) && $accessToSuperAdmin['user_type_id'] == 4){
  $editSaving = ["saving_edit"=> "yes"];
}else{
  $editSaving = $userPer->checkPermission(Yii::app()->session['user_id'],$saving['location_id'],$saving['department_id'],"saving_edit");
}

$sql = "SELECT sum(base_line_spend) as baseline FROM saving_milestone  where saving_id=".$saving['id'];
$mileStoneReader = Yii::app()->db->createCommand($sql)->queryRow();

if(!empty($mileStoneReader['baseline'])){
  $baseline = $mileStoneReader['baseline'];
}else{
  $baseline = 1;
}
$subcategoryUrl =  AppUrl::bicesUrl('products/getSubCategories');
$conditions = [
  'archeved' =>  strtolower($saving['saving_archive']) =='yes',
  'editAccess' =>  (!empty($editSaving) && $editSaving["saving_edit"]=="yes"),
  'canEditCeator' => $saving['user_id'] === Yii::app()->session['user_id']
];

?>

<form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('savings/edit'); ?>">
  <input type="hidden" name="update_saving" id="update_saving" value="1">
  <input type="hidden" name="redirect_url" id="redirect_url" value="edit">
  <input type="hidden" name="saving_id" id="saving_id" value="<?php echo $saving['id'];?>">
  <div class="form-group">
  <div class="col-md-6 col-sm-6 col-xs-12"> <br />
    <h4 class="heading">Saving Summary</h4>
  </div>
  </div>
  <div class="form-group"><br />
     <div class="col-md-8 col-sm-8 col-xs-12 valid">
      <label class="control-label">Saving Title <span style="color: #a94442;">*</span></label>
      <input required type="text" class="form-control notranslate" name="title" id="title"placeholder="Saving Title" value="<?php echo $saving['title'];?>"required autocomlete="off">
     </div>
     <div class="clearfix"></div><br />
     
     <div class="col-md-8 col-sm-8 col-xs-12 valid">
      <label class="control-label">Saving Description/Notes</label>
      <textarea class="form-control notranslate" name="notes" id="notes" rows="4" <?php if (!isset($saving['notes']) || empty($saving['notes'])) echo 'placeholder="Saving Description/Notes"'; ?>><?php if (isset($saving['notes']) && !empty($saving['notes'])) echo $saving['notes']; ?></textarea>
     </div>
    
    </div>
    <div class="form-group">
     <div class="col-md-4 col-sm-4 col-xs-12 valid">
      <label class="control-label">Saving Status</label>
      <select name="status" id="status" class="form-control notranslate">
       <option value="">Select Saving Status</option>
        <?php foreach ($saving_status as $status) { ?>
         <option value="<?php echo $status['id']; ?>" <?php if($status['id']==$saving['status']){?> selected <?php } ?>><?php echo $status['value']; ?></option>
        <?php } ?>
      </select>
     </div>
    <div class="col-md-4 col-sm-4 col-xs-12  date-input">
     <label class="control-label">Saving Currency <span style="color: #a94442;">*</span></label>
     <select name="currency_id" id="currency_id" class="form-control notranslate" required>
      <?php foreach ($currency_rate as $rate) { ?>
       <option value="<?php echo $rate['id']; ?>" <?php if($rate['id']==$saving['currency_id']){?> selected <?php } ?>>
        <?php echo $rate['currency']; ?>
       </option>
      <?php } ?>
     </select>
    </div>
  </div>
   <div class="form-group">
     <div class="col-md-4 col-sm-4 col-xs-12">
      <label class="control-label">Saving Category <span style="color: #a94442;">*</span></label>
      <select name="category_id" id="modal_category_id"
        class="form-control notranslate" onchange="loadSubcategories('<?php echo $subcategoryUrl;?>',this.value);" required>
        <option value="">Select Category</option>
        <?php foreach ($categories as $category) { ?>
          <option value="<?php echo $category['id']; ?>" <?php if($category['id']==$saving['category_id']){?> selected <?php } ?>>
            <?php echo $category['value']; ?>
          </option>
        <?php } ?>
      </select>
     </div>
     <div class="col-md-4 col-sm-4 col-xs-12">
       <label class="control-label">Saving Sub Category</label>
       <select name="subcategory_id" id="subcategory_id"
        class="form-control notranslate">
        <option value="0">Select Sub Category</option>
       </select>
     </div>
    </div>
    <div class="form-group"> 
      <div class="col-md-4 col-sm-4 col-xs-12">
        <label class="control-label">Location <span style="color: #a94442;">*</span></label>
        <select required name="location_id" id="location_id" class="form-control notranslate" onchange="loadDepartmentsForSingleLocation(this);">
          <option value="">Select Location</option>
          <?php foreach ($locations as $location) { ?>
              <option value="<?php echo $location['location_id']; ?>"
                    <?php if ( $location['location_id'] == $saving['location_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $location['location_name']; ?>
              </option>
          <?php } ?>
        </select>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
          <label class="control-label">Department <span style="color: #a94442;">*</span></label>
          <select required name="department_id" id="department_id" class="form-control notranslate">
              <option value="">Select Department</option>
          </select>
        </div>
    </div>
     <div class="form-group"> 
       <div class="col-md-4 col-sm-4 col-xs-12">
        <label class="control-label">Supplier Name</label><select class="form-control select_search notranslate supplier_select" name="vendor_id" style="  border-radius: 10px;border: 1px solid #4d90fe;">
           <option value=""></option>
          <?php foreach($vendorReader as $value){?>
            <option value="<?php echo $value['vendor_id'];?>" <?php if ( $value['vendor_id'] == $saving['vendor_id']) echo ' selected="SELECTED" '; ?>><?php echo $value['vendor_name'];?></option>
          <?php } ?>
        </select>
        </div>
       <div class="col-md-4 col-sm-4 col-xs-12">
        <label class="control-label">Associated Contract </label><select class="form-control select_search notranslate" name="contract_id" id="contract_id" style="  border-radius: 10px;border: 1px solid #4d90fe;">
           <option value=""></option>
          <?php foreach($contractReader as $value){?>
            <option value="<?php echo $value['contract_id'];?>" <?php if ( $value['contract_id'] == $saving['contract_id']) echo ' selected="SELECTED" '; ?>><?php echo $value['contract_title'];?></option>
          <?php } ?>
        </select>
       </div>
  </div>

  <div class="form-group"> 
      <div class="col-md-3 col-sm-3 col-xs-12">
        <label class="control-label">Total Baseline Spend</label>
        <div class="input-group">
          <span class="input-group-addon fieldGroupReadOnly"><?php echo $currencyReader['currency_symbol']; ?></span>
          <input type="number" class="form-control fieldLeftRadius notranslate" name="total_baseline_spend" id="total_baseline_spend" placeholder="Total Baseline Spend"  value="<?php echo !empty($totalBaseLineSpend['total_base_line_spend']) ? number_format($totalBaseLineSpend['total_base_line_spend'],0,'.',''):''; ?>" readonly /> 
        </div>
      </div> 
      <div class="col-md-3 col-sm-3 col-xs-6">
        <label class="control-label">Total Projected Savings</label>
        <div class="input-group">
          <span class="input-group-addon fieldGroupReadOnly"><?php echo $currencyReader['currency_symbol']; ?></span>
        <input type="number" class="form-control fieldLeftRadius notranslate" name="total_project_saving" id="total_project_saving" placeholder="Total Project Savings"  value="<?php echo $saving['total_project_saving']>0?number_format($saving['total_project_saving'],0,'.',''):'';?>" readonly /> 
      </div>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-6">
        <label class="control-label">Total Projected Savings %</label>
        <input type="number" class="form-control notranslate" name="total_project_saving_perc" id="total_project_saving_perc" placeholder="Total Projected Savings %" value="<?php echo ($saving['total_project_saving_perc'] !="0"?number_format($saving['total_project_saving']/$baseline*100,0,'.',''):'');?>" readonly/> 
      </div>
       <div class="clearfix"></div>
   </div>
  <div class="form-group">
     <div class="col-md-3 col-sm-3 col-xs-12">
        <label class="control-label">Total Baseline Spend Against Realised Savings</label>
        <div class="input-group">
          <span class="input-group-addon fieldGroupReadOnly"><?php echo $currencyReader['currency_symbol']; ?></span>
          <input type="number" class="form-control fieldLeftRadius notranslate" name="total_baseline_spend" id="total_baseline_spend" placeholder="Total Baseline Spend"  value="<?php echo !empty($totalBaselineSpendRealizedSaving['total_base_line_spend']) ? number_format($totalBaselineSpendRealizedSaving['total_base_line_spend'],0,'.',''):''; ?>" readonly /> 
        </div>
      </div>

      <div class="col-md-3 col-sm-3 col-xs-12">
        <label class="control-label">Total Realised Savings</label>
        <div class="input-group">
        <span class="input-group-addon fieldGroupReadOnly"><?php echo $currencyReader['currency_symbol']; ?></span>
        <input type="number" class="form-control notranslate fieldLeftRadius" name="realised_saving" id="realised_saving" placeholder="Total Realised Savings" readonly value="<?php echo $saving['realised_saving']>0? number_format($saving['realised_saving'],0,'.',''):'';?>" />
        </div> 
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12">
        <label class="control-label">Total Realised Savings %</label>
        <input type="number" class="form-control notranslate" name="realised_saving_perc" id="realised_saving_perc" placeholder="Total Realised Savings %" value="<?php echo ($saving['realised_saving_perc'] ? number_format($saving["realised_saving_perc"]* 100 ,0,'.',''):'' );?>" readonly/> 
      </div>
       <div class="clearfix"></div>
    </div>
  <div class="form-group">
    <!-- <div class="col-md-3 col-xs-6">
      <label class="control-label">Savings Start Date</label>
      <input type="text" readonly class="form-control notranslate"  name="start_date" id="start_date" placeholder="Savings Start Date" value="<?php echo $saving['start_date'] != '0000-00-00 00:00:00'?date($dateFormate, strtotime($saving['start_date'])):'';?>" />
    </div> -->
    <div class="col-md-3 col-sm-3 col-xs-12">
      <label class="control-label">Savings Due Date</label>
      <input type="text" readonly class="form-control notranslate" name="saving_due_date" id="saving_due_date" placeholder="Savings Due Date" value="<?php echo $saving['due_date'] != '0000-00-00 00:00:00'?date($dateFormate, strtotime($saving['due_date'])):'';?>"/> 
    </div>
 
    <div class="col-md-3 col-sm-3 col-xs-12">
      <label class="control-label">Created On</label>
      <input type="text" readonly class="form-control notranslate" name="created_at" id="created_at" placeholder="Created On" value="<?php echo $saving['created_at'] != '0000-00-00 00:00:00'?date($dateFormate, strtotime($saving['created_at'])):'';?>" /> 
    </div>
    <div class="col-md-2 col-sm-3 col-xs-12">
      <label class="control-label">Created By</label>
      <input type="text" readonly class="form-control notranslate" name="user_name" id="user_name" placeholder="Created By" value="<?php echo $saving['user_name'];?>"  /> 
    </div>
     <div class="clearfix"></div>
  </div><br />

  <div class="col-md-6">
 <?php 
  if(($conditions['archeved'] && $conditions['editAccess']) && ($conditions['canEditCeator'] || !empty($accessToSuperAdmin))){?>
   <a href="<?php echo $this->createUrl('savings/saveArhive',array('saving-id'=>$saving['id'],'archive-type'=>'un-archive'));?>" class="btn btn-primary" onclick="return confirm('Are you sure you want to Un-archive this Saving')" <?php echo $disabled; ?> />
        Unarchive Saving
      </a>
   <?php } else{ 
    if($conditions['editAccess'] && ($conditions['canEditCeator'] || $accessToSuperAdmin)) { ?>
    <button type="submit" class="btn btn-primary btn-functionality submit-btn" style="background-color: #1abb9c !important; border-color: #1abb9c !important" <?php echo $disabled; ?>>Save Savings Record</button>
    <a href="<?php echo $this->createUrl('savings/saveArhive',array('saving-id'=>$saving['id']));?>" style="background-color: #5bc0de !important; border-color: #5bc0de !important;" class="btn btn-danger" onclick="return confirm('Are you sure you want to archive this Saving')" <?php echo $disabled; ?> />
       Archive Saving
      </a>
   <?php }} ?>
  </div>
</form>



