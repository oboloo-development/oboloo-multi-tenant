<?php
$disabled = FunctionManager::sandbox();
$userPer = new User;
$accessToSuperAdmin = $userPer->getOne(array('user_type_id' => 4, 'user_id' => Yii::app()->session['user_id']));
?>
<div class="modal fade" id="add_milestone" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style="margin-top: 138px; width: 630px;"> 

    <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
     <h4 class="modal-title">Add Saving Milestone</h4>
    </div>

    <div id="" class="alert alert-success ajax_success" role="alert"></div>
    <form id="add_milestone_form" class="form-horizontal col-md-12 form-label-left input_mask" enctype="multipart/form-data" method="post" action="#">
      <div class="clearfix"></div><br />
    <div class="col-md-12 col-sm-12 col-xs-12">
      <label class="control-label">Milestone Title <span style="color: #a94442; margin-left: -2px;">*</span></label>
      <input type="text" class="form-control  notranslate" name="milestone_title" id="milestone_title" required /> 
    </div>
          
    <div class="clearfix"></div><br />
    <!-- <div class="col-md-12 col-sm-12 col-xs-12" id="amountFields"></div> -->
    <div class="col-md-12 col-sm-12 col-xs-12">
      <label class="control-label">Due Date<span style="color: #a94442;">*</span></label>
      <input type="text" class="form-control notranslate" name="due_date" id="due_date"  required /> 
    </div>

    <div class="clearfix"></div><br />
    <div class="col-md-4 col-sm-4 col-xs-12">
      <label class="control-label">Baseline Spend <span style="color: #a94442;">*</span> </label>
      <input type="number" class="form-control notranslate" name="base_line_spend" id="base_line_spend" placeholder="Baseline Spend" required/> 
    </div>
   
    <div class="col-md-4 col-sm-4 col-xs-12">
      <label class="control-label">Projected Cost Reduction <span style="color: #a94442; margin-left: -2px;">*</span></label>
      <input type="number" class="form-control notranslate" name="cost_reduction" id="cost_reduction" placeholder="Projected Cost Reduction" required/> 
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12" style="padding-right: 6px;">
      <label class="control-label">Projected Cost Avoidance <span style="color: #a94442; margin-left: -1px;">*</span></label>
      <input type="number" class="form-control notranslate" name="cost_avoidance" id="cost_avoidance" placeholder="Projected Cost Avoidance" required/> 
    </div>

    <div class="clearfix"></div><br />
    <div class="col-md-12 col-sm-12 col-xs-12">
      <label class="control-label">Milestone Notes</label>
      <textarea class="form-control notranslate" name="milestone_notes" id="milestone_notes" rows="4" placeholder="Savings Notes"></textarea>
    </div>

    <input type="hidden" name="saving_id"  value="<?php echo $saving_id;?>" />
    <div class="clearfix"></div><br />
    <div class="modal-footer">
      <?php if($userID === Yii::app()->session['user_id'] || $accessToSuperAdmin['user_id']){?>
      <button type="submit" class="btn btn-success" <?php echo $disabled; ?> style="background-color: #1abb9c !important; border-color: #1abb9c !important"  >Add Saving Milestone</button>
      <?php } ?>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</form>
<div class="clearfix"></div></div></div></div>
  