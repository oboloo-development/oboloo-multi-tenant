<?php $domainUrl = Yii::app()->getBaseUrl(true);

$mPadd = "";
$dateFormate = FunctionManager::dateFormat();
if (!empty($_POST['report_from_date'])) {
  $reportFromDate = $_POST['report_from_date'];
} else {
  $reportFromDate = "";
}

if (!empty($_POST['report_to_date'])) {
  $reportToDate = $_POST['report_to_date'];
} else {
  $reportToDate = "";
}

if (!empty($_POST['report_of_location'])) {
  $reportLocation = $_POST['report_of_location'];
} else {
  $reportLocation = "";
}

?>
<div class="right_col" role="main">
  <div class="col-md-5"></div>
  <div class="col-md-3"></div>
  <div class="col-md-4"></div>

  <div class="row-fluid tile_count">
    <div class="span6 pull-left col-md-5">
      <h3>Archived Savings</h3>
    </div>
    <div class="span6 pull-right">
      <a href="<?php echo AppUrl::bicesUrl('savings/list'); ?>">
        <button type="button" class="btn btn-info">
          <span class="glyphicon glyphicon-list mr-2" aria-hidden="true"></span> Return To Active Savings
        </button>
      </a>
    </div>

    <div class="clearfix"> </div>
    <?php if (Yii::app()->user->hasFlash('success')) {  ?>
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong><?= Yii::app()->user->getFlash('success'); ?></strong>
      </div>
    <?php } ?>
  </div>

  <div class="clearfix"> </div>
  <br /><br />
  <div class="clearfix"></div>

  <form class="form-horizontal" id="contract_list_form" name="contract_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('savings/list'); ?>">
    <div class="saving_filter_container">
      <div class="saving_filter_input">
        <input type="text" class="form-control border-select" name="due_date" id="from_date" <?php if (isset($due_date) && !empty($due_date)) echo 'value="' . date(FunctionManager::dateFormat(), strtotime($due_date)) . '"';
                                                                                              else echo 'placeholder="Due Date From"'; ?> autocomplete="off">
      </div>
      <div class="saving_filter_input">
        <input type="text" class="form-control border-select" name="due_date_to" id="from_date_to" <?php if (isset($due_date_to) && !empty($due_date_to)) echo 'value="' . date(FunctionManager::dateFormat(), strtotime($due_date_to)) . '"';
                                                                                                    else echo 'placeholder="Due Date To"'; ?> autocomplete="off">
      </div>
      <div class="saving_filter_input">
        <select name="status" id="status" class="form-control select_status_multiple notranslate">
          <option value="">Saving Status</option>
          <?php foreach ($saving_status as $status) { ?>
            <option value="<?php echo $status['id']; ?>" <?php if (!empty($saving_status_id) && in_array($status['id'], $saving_status_id)) echo ' selected="SELECTED" '; ?>><?php echo $status['value']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="saving_filter_input">
        <div class="">
          <select name="location_id[]" id="location_id" class="form-control select_location_multiple border-select" searchable="Search here.." onchange="loadDepartments(0);">
            <option value="0">All Locations</option>
            <?php
            $i = 0;
            foreach ($locations as $location) {
              if (isset($location_id[$i])) {  ?>
                <option value="<?php echo $location['location_id']; ?>" <?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
                  <?php echo $location['location_name']; ?>
                </option>
              <?php } else { ?>
                <option value="<?php echo $location['location_id']; ?>">
                  <?php echo $location['location_name']; ?>
                </option>
              <?php } ?>
            <?php
              $i++;
            } ?>
          </select>
        </div>
      </div>
      <div class="saving_filter_input">
        <div class="">
          <select name="department_id[]" id="department_id" class="form-control select_department_multiple border-select" searchable="Search here..">
            <option value="">All Departments</option>
            <?php if (!empty($department_info))
              foreach ($department_info as $dept_value) { ?>
              <option value="<?php echo $dept_value['department_id']; ?>" <?php if (in_array($dept_value['department_id'], $department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name']; ?></option>
            <?php } ?>

          </select>
        </div>
      </div>

      <div class="saving_filter_input">
        <div class="">
          <select name="category_id" id="category_id" class="form-control select_category_multiple border-select" onchange="loadSubcategories(0);" searchable="Search here..">
            <option value="0">All Categories</option>
            <?php foreach ($categories as $category) { ?>
              <option value="<?php echo $category['id']; ?>" <?php if (isset($category_id) && $category_id == $category['id']) echo ' selected="SELECTED" '; ?>>
                <?php echo $category['value']; ?>
              </option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="saving_filter_input">
        <div class="">
          <select name="subcategory_id" id="subcategory_id" class="form-control select_subcategory_multiple border-select">
            <option value="0">All Subcategories</option>
          </select>
        </div>
      </div>
      <div class="saving_filter_input">
        <div style="display: grid;">
          <button class="btn btn-info search-quote" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
          <button class="btn btn-primary " onclick="loadSavings(); return false;">Apply filters</button>
        </div>
      </div>
    </div>
  </form>

  <!-- End: search filters -->
  <div class="table-responsive">
    <table id="savings_table" class="table table-striped table-bordered savings-table" style="width: 100%;margin-top: 30px;">
      <thead>
        <tr>
          <th> </th>
          <th style="width: 9%;" class="">Saving ID</th>
          <th class="notranslate text-center" style="width: 12%">Saving Title</th>
          <!-- <th>Start Date</th> -->
          <th class="text-center" style="width: 10%">Created on </th>
          <th class="text-center" style="width: 9%">Savings Due date</th>
          <th class="text-center" style="width: 9%;">Created By</th>
          <th class="text-center">Currency</th>
          <th class="text-center">Baseline Spend</th>
          <th class="text-center">Projected Savings</th>
          <th class="text-center" style="width: 8%;">Projected Savings %</th>
          <th class="text-center">Realised Savings</th>
          <th class="text-center" style="width: 8%;">Realised Savings %</th>
          <th class="text-center">Status</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
  </div>
  <!-- </div> -->

</div>
</div>
<style>
  .table-currency {
    padding-right: 28px !important;
  }

  .location .multiselect {
    width: 138%;
  }

  .status .multiselect {
    width: 100%;
  }

  .multiselect-selected-text {
    float: left;
    margin-left: 0px;
  }

  .btn .caret {
    float: right;
    margin-top: 10px;

  }

  .department .dropdown-toggle {
    min-width: 112%;
  }

  .apexcharts-xaxis-label:nth-last-child(2) {
    transform: translateX(-2px) !important;
  }

  .btn-status {
    background: red !important;
    border-color: 1px red;
  }

  .btn-status:hover {
    background: red !important;
    border-color: 1px red;
  }
</style>


<script type="text/javascript">
  colors = ['red', '#66DA26', '#546E7A', '#E91E63', '#FF9800', '#2E93fA', '#2196F3', '#3a62ba', '#7b9333', '#344189', '#', '#', '#', '#', '#'];
  /*  
  $(document).ready(function(){
    select2function('select_location_multiple','All Locations');
    select2function('select_department_multiple','All Departments');
    select2function('select_category_multiple','All Categories');
    select2function('select_subcategory_multiple','All Sub Categories');
  });*/

  function loadSavings() {
    $('#savings_table').DataTable().ajax.reload();
  }

  function clearFilter() {
    $('#from_date').val("");
    $('#from_date_to').val("");
    $("option:selected").removeAttr("selected");
    $('.select_location_multiple').trigger("change");
    $('.select_department_multiple').trigger("change");
    $('.select_category_multiple').trigger("change");
    $('.select_subcategory_multiple').trigger("change");
    $('.select_status_multiple').trigger("change");
    $('#savings_table').DataTable().ajax.reload();
  }

  function select2function(className, lableTitle) {
    var lableTitle = lableTitle;
    $("." + className).select2({
      // placeholder: lableTitle,
      /* allowClear: true*/
    });
  }
  $(document).ready(function() {
    select2function('select_location_multiple', 'All Locations');
    select2function('select_department_multiple', 'All Departments');
    select2function('select_category_multiple', 'All Categories');
    select2function('select_subcategory_multiple', 'All Sub Categories');
    select2function('select_status_multiple', 'All Statuses');
    select2function('general-select2', '');
  });
  /*
  function select2function(className,lableTitle){
    var lableTitle = lableTitle;
    $("."+className).select2({
        placeholder: lableTitle,
        allowClear: true
      });
  }
  */
  <?php if (isset($category_id) && !empty($category_id)) { ?>

    <?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>

    <?php } else { ?>
    <?php } ?>

  <?php } ?>




  function loadSubcategories(subcategory_id) {
    var category_id = $('#category_id').val();

    $.ajax({
      type: "POST",
      data: {
        category_id: category_id
      },
      dataType: "json",
      url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
      success: function(options) {

        var options_html = '<option value="0">All Subcategories</option>';
        for (var i = 0; i < options.suggestions.length; i++)
          options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
        $('#subcategory_id').html(options_html);
        if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id);

        $('#subcategory_id').trigger('change');
      },
      error: function() {
        $('#subcategory_id').html('<option value="0">All Subcategories</option>');
      }
    });
  }



  jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function(s) {
      return Date.parse(s);
    },

    "sort-month-year-asc": function(a, b) {
      return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "sort-month-year-desc": function(a, b) {
      return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
  });


  $.fn.singleDatePicker = function() {
    $(this).on("apply.daterangepicker", function(e, picker) {
      picker.element.val(picker.startDate.format('DD/MM/YYYY'));
    });
    return $(this).daterangepicker({
      singleDatePicker: true,
      singleClasses: "picker_3",
      autoUpdateInput: false,
      locale: {
        format: 'DD/MM/YYYY'
      }
    });
  };

  $(document).ready(function() {
    $('#savings_table').dataTable({
      "columnDefs": [{
        //"targets": -1,
        "targets": 0,

        "ordering": false,
        "info": false
        /*"width": "6%",*/
      }],

      "createdRow": function(row, data, index) {
        if (data[0].indexOf('glyphicon glyphicon-ok') >= 0)
          for (var i = 1; i <= 6; i++)
            $('td', row).eq(i).css('text-decoration', 'line-through');
      },
      "order": [
        [1, "desc"]
      ],
      "pageLength": 50,
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo AppUrl::bicesUrl('savings/listArchiveAjax'); ?>",
        "type": "POST",
        data: function(input_data) {
          input_data.location_id = $('#location_id').val();
          input_data.department_id = $('#department_id').val();
          input_data.category_id = $('#category_id').val();
          input_data.subcategory_id = $('#subcategory_id').val();
          input_data.status = $('#status').val();
          input_data.due_date = $('#from_date').val();
          input_data.due_date_to = $('#from_date_to').val();
        }
      },
      "oLanguage": {
        "sProcessing": "<h1>Please wait ... retrieving data</h1>"
      },
      "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        if (aData[1].search("<span class='deactivated_vendor'></span>") > 0) {
          $(nRow).addClass('deactivated_record');
          /*$(nRow).css("background-color", "#ccc");*/
        }
        for (var i = 0; i <= 12; i++) {
          if (i != 2) {
            $('td:eq(' + i + ')', nRow).addClass('td-center');
          }
        }
        for (var i = 6; i == 6; i++) {
          $('td:eq(' + i + ')', nRow).addClass('table-currency');
        }
      }
    });
  });
  $(document).ready(function() {
    $('#saving_table').dataTable({
      "columnDefs": [{
          "targets": 0,
          "width": "6%",
          "orderable": false
        },
        /*  { "type": "sort-month-year", targets: 3 },         
          { "type": "sort-month-year", targets: 4 } */
      ],
      "order": []
    });




    $('#from_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>'
    });

    $('#from_date_to').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>'
    });

    /*  $('#to_date').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        singleClasses: "picker_3",
        locale: {
        format: '<?php //echo FunctionManager::dateFormatJS();
                  ?>'
        }
      });*/

    $('#from_date_to').on('apply.daterangepicker', function(ev, picker) {
      $('#from_date_to').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS(); ?>'));
    });

    $('#from_date').on('apply.daterangepicker', function(ev, picker) {
      $('#from_date').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS(); ?>'));
    });


    $('.saving_to_date, .saving_from_date').datetimepicker({
      format: '<?php echo FunctionManager::dateFormatJS(); ?>',
    });




    <?php if (isset($location_id) && !empty($location_id)) { ?>

      <?php if (isset($department_id) && !empty($department_id)) { ?>
        loadDepartments(<?php echo $department_id; ?>);
      <?php } else { ?>
        loadDepartments(0);
      <?php } ?>

    <?php } ?>

  });

  function addSavingModal(e) {
    e.preventDefault();
    $.ajax({
      datatype: 'html',
      url: "<?php echo AppUrl::bicesUrl('savings/createByModal'); ?>",
      type: "POST",
      data: {
        modalWind: 1
      },
      success: function(mesg) {
        $('#create_saving_cont').html(mesg);
        $('#create_saving').modal('show');
      }
    });
  }

  // Start: line and area chart
  $('document').ready(function() {

    var options = {

      series: [{
        name: 'Projected Savings',

        data: [<?php echo implode(",", $chart_1["project"]); ?>]
      }, {
        name: 'Realised Savings',

        data: [<?php echo implode(",", $chart_1["realised"]); ?>]
      }],
      chart: {
        type: 'bar',
        height: 350,
        width: '1100px',
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '40%',
          endingShape: 'rounded',

        },
      },
      colors: ['#efa65f', '#48d6a8'],
      dataLabels: {

        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,

      },
      xaxis: {
        categories: [<?php echo "'" . implode("','", $chart_1["department"]) . "'"; ?>],
      },
      yaxis: {
        //tickAmount: 1000,
        labels: {
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '12px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        }
      },
      legend: {
        position: 'bottom',
        offsetX: 0,
        labels: {
          colors: ['#efa65f', '#48d6a8'],
          useSeriesColors: ['#efa65f', '#48d6a8'],
        },
      },
    };

    var chart = new ApexCharts(document.querySelector("#realize_project_depart_chart"), options);
    chart.render();



    var options = {
      series: [{
        name: 'Projected Savings',
        data: [<?php echo "'" . implode("','", $chart_2["project"]) . "'"; ?>]
      }, {
        name: 'Realised Savings',
        data: [<?php echo "'" . implode("','", $chart_2["realised"]) . "'"; ?>]
      }],
      chart: {
        type: 'bar',
        height: 300,
        width: '1100px',
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '40%',
          endingShape: 'rounded'
        },
      },
      colors: ['#efa65f', '#48d6a8'],
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      xaxis: {
        categories: [<?php echo "'" . implode("','", $chart_2["category"]) . "'"; ?>],
        labels: {
          show: false,
        }
      },
      yaxis: {
        title: {
          text: '',
          style: {
            fontSize: '12px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            /*color:'#2d9ca2'*/
          },
        },
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        // y: {
        //   formatter: function (val) {
        //     return "$ " + val + " thousands"
        //   }
        // }
      },
      legend: {
        position: 'bottom',
        offsetX: 0,
        labels: {
          colors: ['#efa65f', '#48d6a8'],
          useSeriesColors: ['#efa65f', '#48d6a8'],
        },
      },

    };

    var chart = new ApexCharts(document.querySelector("#realize_project_cat_chart"), options);
    chart.render();


    // Start: column category stacked chart
    var options = {
      series: [{
        name: 'Projected Savings',
        // data: ['1200','1100','1600','1810','1000','4684','740'
        data: [<?php echo implode(',', $chart_3['project']); ?>]
      }, {
        name: 'Realised Savings',
        // data: ['800','153','0','0','20','0','600']
        data: [<?php echo implode(',', $chart_3['realised']); ?>]
      }],
      chart: {
        fontFamily: 'Poppins !important',
        type: 'bar',
        height: 300,
        width: '1100px',
        stacked: false,
        distributed: false,
        horizontal: true,
        toolbar: {
          show: false
        },
        zoom: {
          enabled: true
        }
      },
      colors: ['#efa65f', '#48d6a8'],
      dataLabels: {
        enabled: false,
      },
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            position: 'bottom',
            offsetX: -10,
            offsetY: 0
          }
        }
      }],
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '60%',
        },
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      xaxis: {

        //type: 'datetime',
        categories: [<?php echo "'" . implode("','", $chart_3['month']) . "'"; ?>],
        labels: {
          show: true,
          style: {
            //colors: ['#2d9ca2','#2d9ca2'],
            fontSize: '12px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
          },
        }
      },
      yaxis: {
        //tickAmount: 1000,
        labels: {
          style: {
            /*colors: '#2d9ca2',*/
            fontSize: '12px',
            fontFamily: 'Poppins !important',
            fontWeight: 'normal',
            cssClass: 'apexcharts-xaxis-label',
            // useSeriesColors: ['#efa65f','#efa65f']
          },
        }
      },
      legend: {
        position: 'bottom',
        offsetX: 0,
        labels: {
          colors: ['#efa65f', '#48d6a8'],
          useSeriesColors: ['#efa65f', '#48d6a8']
        },
      },
      fill: {
        opacity: 1
      }
    };

    var chart = new ApexCharts(document.querySelector("#realize_project_month_chart"), options);
    chart.render();
  });

  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>

<?php $this->renderPartial('/savings/create_saving'); ?>
<?php if (!empty($_GET['from']) && $_GET['from'] == 'information') { ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('.click-modal-poup').trigger('click');
    });
  </script>
<?php } ?>

<style type="text/css">
  .location .multiselect {width: 138%;  }
  .department .multiselect { width: 112%;  }
  /*.select2-container--default .select2-selection--single, .select2-container--default .select2-selection--multiple {
    border: none;  border-bottom: 1px solid #eee !important;
    }
.select2-container--default .select2-selection--multiple { border-radius: 0px !important;}*/
  .multiselect-container>li>a>label.checkbox,
  .multiselect-container>li>a>label.radio {  color: #777;}
  #quote_table_filter label {  font-weight: 500;}
  #quote_table_filter label input {font-weight: normal;  }
  body {  background: #ebecf6}
  .apexcharts-toolbar {  display: none !important;}
  .apexcharts-xaxis-label:nth-last-child(2) {  transform: translateX(-20px)}
  .th-center{  text-align: center !important;}
  .select2-selection__clear {display: none !important;  }
  .saving_filter_container {  position: relative;}
</style>