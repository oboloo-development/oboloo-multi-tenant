<div class="col-md-12"><br />
  <div class="pull-left" style="width: 71%">
    <h4 class="heading">Saving Milestones</h4>
  </div>
  <button type="button" class="btn btn-success pull-right hidden-xs" onclick="$('#add_milestone').modal('show');" style="background-color: #F79820 !important; border-color: #F79820 !important">Add Saving Milestone
  </button>
  <div class="clearfix"></div><br /><br />

  <?php $this->renderPartial('_view_milestone', array('saving_id' => $vendor_id, 'userID' => $saving['user_id'])); ?>

  <div id="" class="alert alert-success ajax_success" role="alert"></div>
  <div class="clearfix"></div>

</div>
<div class="clearfix"></div>
<div class="col-md-12 col-lg-12">
  <div>
    <div class="table-responsive table-container">
      <table id="milestone_table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" style="width: 100%;">
        <thead>
          <tr>
            <!-- <th style="width: 5%;">S No.</th> -->
            <th style="width: 10%;">Milestone Name</th>
            <th>Due Date</th>
            <th style="width: 9%; text-align: center;">Baseline Spend</th>
            <th style="text-align: center;">Projected Cost Reduction</th>
            <th style="width: 13%;text-align: center;">Projected Cost Avoidance</th>
            <th style="text-align: center;">Realised Cost Reduction</th>
            <th style="text-align: center;">Realised Cost Avoidance</th>
            <th>Created on (UTC)</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $total_milestone = 0;
          $getSaving = new Saving();
          $savingRecord = $getSaving->getOne(array('id' => $saving['id']));
          $sql = "select * from currency_rates where id=" . $savingRecord['currency_id'];
          $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
          //echo "<pre>"; print_r($milestones); echo "</pre>"; exit;
          foreach ($milestones as $milestone) {
            $total_milestone++;
            $dueDate = !in_array($milestone['due_date'], array('0000-00-00', '0000-00-00 00:00:00')) ? date(FunctionManager::dateFormat(), strtotime($milestone['due_date'])) : '';

            $spendLine = $milestone['base_line_spend'] > 0 ? number_format($milestone['base_line_spend'], 0, "", "") : 1;
            $reduction = number_format($milestone['cost_reduction'], 0, "", "");
            $avoidance = number_format($milestone['cost_avoidance'], 0, "", "");

            $baseLine = '';
            $costAvoidance = '';

            $disabled = FunctionManager::sandbox();
            $formID  = "savingedit" . $milestone['id'];
            $popUpID = "savingedit" . $milestone['id'];
            $milestoneID = $milestone['id'];
            $formIDComplete = "savingmilestonecomplete" . $milestoneID;
            $popUpIDComplete = "savingmilestonecomplete" . $milestoneID;

            $reduction = number_format($milestone['cost_reduction'], 0, "", "");
            $avoidance = number_format($milestone['cost_avoidance'], 0, "", "");
            $realised_avoidance = $milestone['realised_cost_avoidance'] >0 ? $milestone['realised_cost_avoidance'] : $avoidance;
            $realised_reduction = $milestone['realised_cost_reduction'] >0 ? $milestone['realised_cost_reduction'] : $reduction;
            $actionLink = '<a data-toggle="modal" data-target="#' . $popUpID . '" href="#"><i class="fa fa-edit" style="font-size:18px"></i></a>';

            $modelHeadingName = "";
            $modelFieldNotes = "Milestone Notes";
            $savingMilestoneDuration  = ''; ?>

            <!-- Start: Edit MileStone -->
            <div id="<?= $popUpID ?>" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:left;font-size: 20px">Edit Milestone</h4>
                  </div>
                    <form id="<?= $formID ?>" action="<?php echo AppUrl::bicesUrl('savings/editMilestone'); ?>" method="post" style="text-align: left;">
                  <div class="modal-body">
                      <div class="col-md-6  mt-15">
                        <label class="control-label">Due Date</label><br />
                        <input type="text" onchange="loadgeneraldate();" class="form-control notranslate generaldate " name="edit_due_date" autocomplete="off" id="edit_due_date" value="<?= $dueDate ?>" placeholder="Due Date" style="width:100%" />
                      </div>
                      <div class="col-md-6 col-xs-12  mt-15">
                        <label class="control-label">Baseline Spend</label><br />
                        <input type="number" class="form-control notranslate" name="edit_base_line_spend" id="edit_base_line_spend" value="<?= $milestone['base_line_spend'] ?>" placeholder="Projected Cost Reduction" style="width:100%" />
                      </div>
                      <div class="col-md-6 col-xs-12  mt-15">
                        <label class="control-label">Projected Cost Avoidance</label><br />
                        <input type="number" class="form-control notranslate" name="edit_cost_avoidance" id="edit_cost_avoidance" value="<?= $avoidance ?>" placeholder="Projected Cost Reduction" style="width:100%" />
                      </div>
                      <div class="col-md-6 col-xs-12  mt-15">
                        <label class="control-label">Projected Cost Reduction</label><br />
                        <input type="number" class="form-control notranslate" name="edit_cost_reduction" id="edit_cost_reduction" value="<?= $reduction ?>" placeholder="Projected Cost Reduction" style="width:100%" />
                      </div>
                      <!-- <div class="col-md-6 col-xs-12 mt-15">
                        <label class="control-label">Realised Cost Avoidance</label><br />
                        <input type="number" class="form-control notranslate" name="edit_realised_cost_avoidance" id="edit_realised_cost_avoidance" value="<?= $realised_avoidance ?>" placeholder="Realised Cost Avoidance" style="width:100%" />
                      </div>
                      <div class="col-md-6 col-xs-12 mt-15">
                        <label class="control-label">Realised Cost Reduction</label><br />
                        <input type="number" class="form-control notranslate" name="edit_realised_cost_reduction" id="edit_realised_cost_reduction" value="<?= $realised_reduction ?>" placeholder="Realised Cost Reduction" style="width:100%" />
                      </div> -->
                      <input type="hidden" class="notranslate" name="milestone_edit_id" value="<?= $milestone['id']; ?>" />
                      <input type="hidden" class="notranslate" name="edit_saving_id" value="<?= $saving['id']; ?>" />
                    
                      <div class="col-md-12">
                        <label class="control-label">Milestone Notes</label><br />
                        <textarea class="form-control notranslate" name="edit_milestone_notes" id="edit_milestone_notes" rows="4" placeholder="Savings Notes" style="width:100%" ><?= $milestone['notes']?></textarea><br /><br />
                      </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default  btn sm" data-dismiss="modal">Close</button> 
                      <button type="submit" class="btn btn sm green-btn" <?= $disabled ?>>Save</button>
                    </div>
                  </div>
                    </form>
                </div>
              </div>
            </div>
            <!-- END: Edit MileStone -->
            <?php // END:

            // Start: Milestone complete modal
            if (strtolower(trim($milestone['status'])) != "completed") { ?>

              <div id="<?= $popUpIDComplete ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title" style="text-align:left;font-size: 15px">Mark Milestone As Complete</h4>
                    </div>
                    <form id="<?= $popUpIDComplete ?>" action="<?php echo AppUrl::bicesUrl('savings/milestoneComplete'); ?>" method="post" style="text-align: left;">
                      <input type="hidden" class="notranslate" name="complete_milestone_edit_id"  value="<?= $milestone['id'] ?>"/>
        			        <input type="hidden" class="notranslate" name="complete_edit_saving_id"  value="<?= $saving['id'] ?>"/>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Realised Cost Reduction</label>
                              <input type="text" name="total_realised_cost_reduction" id="total_realised_cost_reduction" class="form-control notranslate total_realised_cost_reduction" value="<?= ($reduction != "0.00" ? round($reduction) : $reduction) ?>" data-milestone-id="<?= $milestoneID ?>"  />
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Realised Cost Avoidance</label>
                              <input type="text" name="total_realised_cost_avoidance" id="total_realised_cost_avoidance" class="form-control notranslate total_realised_cost_avoidance" value="<?= number_format($avoidance, 0, "", "") ?>" data-milestone-id="<?= $milestoneID ?>"  />
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Total Realised Savings</label>
                              <input type="text" name="total_realised_saving" id="total_realised_saving" class="form-control notranslate total_realised_saving" value="" data-milestone-id="<?= $milestoneID ?>" readonly />
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label">Baseline Spend</label>
                              <input type="text" name="base_line_spend"   class="base_line_spend form-control notranslate" value="<?= number_format($spendLine, 0, "", "") ?>" data-milestone-id="<?= $milestoneID ?>" readonly />
                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="control-label">Total Realised Savings %</label>
                              <input type="text" name="total_project_realised_perc" id="total_project_realised_perc" class="form-control notranslate total_project_realised_perc" value="" data-milestone-id="<?= $milestoneID ?>" readonly />
                            </div>
                          </div>
                        </div>
                      </div>

                      <input type="hidden" class="notranslate" name="complete_edit_realised_cost_reduction" value="<?= ($value['realised_cost_reduction']>0?round($value['realised_cost_reduction']):$reduction) ?>" />
                      <input type="hidden" class="notranslate" name="complete_edit_realised_cost_avoidance" value="<?= ($value['realised_cost_avoidance']>0?round($value['realised_cost_avoidance']):$avoidance) ?>" />
                    
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger sm" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn sm green-btn" <?= $disabled ?>>Confirm Milestone Completion</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

            <?php } ?>
            <tr>
              <td><?= $milestone['title']; ?></td>
              <td><?= $dueDate; ?></td>
              <td class="text-center"><?= $currencyReader['currency_symbol'] . $milestone['base_line_spend']; ?></td>
              <td class="text-center"><?= $currencyReader['currency_symbol'] . $reduction; ?></td>
              <td class="text-center"><?= $currencyReader['currency_symbol'] . $avoidance; ?></td>
              <td class="text-center"><?= $milestone['realised_cost_reduction'] > 0 ? $currencyReader['currency_symbol'] . number_format($milestone['realised_cost_reduction'], 0, "", "") : ''; ?></td>
              <td class="text-center"><?= $milestone['realised_cost_avoidance'] > 0 ? $currencyReader['currency_symbol'] . number_format($milestone['realised_cost_avoidance'], 0, "", "") : ''; ?></td>
              <td><?= date(FunctionManager::dateFormat(), strtotime($milestone['created_at'])); ?></td>
              <td>
                <a data-toggle="modal" data-target="#<?= $popUpID; ?>" href="#"><i class="fa fa-edit" style="font-size:18px"></i></a>
                <?php if (strtolower(trim($milestone['status'])) != "completed") { ?>
                  <a data-toggle="modal" data-target="#<?= $popUpIDComplete ?>" href="#" class="btn btn-primary submit-btn" onclick="updateTotalInput(<?= $milestoneID ?>)" <?= $disabled ?> style="text-decoration:none;font-size:10px;">Mark As Complete</a>
                <?php } else { ?>
                  <span class="btn green-btn">Completed</span>
                <?php } ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

  </div>
</div>
<div class="clearfix"></div> <br /><br />

<!-- Modal -->
<div class="clearfix"></div><br />

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
<script>
  $(".generaldate").datetimepicker({
    format: "<?= FunctionManager::dateFormatJS() ?>"
  });
  
  function updateTotalInput(milestoneID) {
    
    var milestoneElement = $("#savingmilestonecomplete"+milestoneID);
    var avoidance = parseFloat(milestoneElement.find(".total_realised_cost_avoidance").val()) || 0;
    var reduction = parseFloat(milestoneElement.find(".total_realised_cost_reduction").val()) || 0;
    var baseLineSpend = parseFloat(milestoneElement.find('.base_line_spend').val()) || 0;
    var total_realised_saving = avoidance + reduction;
    milestoneElement.find(".total_realised_saving").val(total_realised_saving);
    var total_realised_saving_per = ((total_realised_saving / baseLineSpend ) * 100).toFixed(2);
    milestoneElement.find(".total_project_realised_perc").val(total_realised_saving_per);
  }

  $("#total_realised_cost_avoidance, .total_realised_cost_reduction").on("input", function() {
    var milestoneID = $(this).data("milestone-id"); // Add a data attribute to store milestone ID
    updateTotalInput(milestoneID);
  });

  // Initial update
  updateTotalInput(milestoneID=0);
  
</script>