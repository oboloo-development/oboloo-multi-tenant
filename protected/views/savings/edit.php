<?php $dateFormate = FunctionManager::dateFormat();
$sql = "select * from currency_rates where id=" . $saving['currency_id'];
$currencyReader = Yii::app()->db->createCommand($sql)->queryRow();

?>
<div class="right_col" role="main">
  <div class="row-fluid tile_count">
    <div class="span6 pull-left">
      <h4>
        <?php $tool_currency = Yii::app()->session['user_currency'];
        if (!empty($saving['title'])) { ?>
          <br /><br /><strong class="notranslate">
            Savings Title
            :</strong> <span class="title-text notranslate"> <?php echo $saving['title']; ?></span>
        <?php }
        if (!empty($saving['vendor_id'])) { ?>
          <br /><br /><strong class="notranslate">Supplier Name: </strong> <span class="title-text notranslate"> <?php echo $vendor['vendor_name']; ?></span>
        <?php }
        if (!empty($currency_rate_detail['currency'])) { ?>
          <br /> <br /><strong class="notranslate">Currency: </strong>: <span class="title-text notranslate"> <?php echo $currency_rate_detail['currency']; ?></span>
        <?php }

        if (!empty($saving['approver_status'])) {
          $supStatus = $saving['approver_status'];
          if ($supStatus == "Approved") {
            $color = "#DDF1D5";
          } else if ($supStatus == "Sent For Approval") {
            $color = "#d5dbf1";
          } else if ($supStatus == "Rejected") {
            $color = "#F0DFDF";
          } else {
            $color = "#DCDCDC";
          } ?>

          <br /><br /><strong class="notranslate">Savings Status: </strong> <span class="title-text notranslate">
            <span class="btn btn-primary" style="background: <?php echo $color; ?> !important;border-color: <?php echo $color; ?>  !important;border-radius: 10px;color:#2d9ca2 !important"><?php echo $saving['approver_status']; ?></span></span>
        <?php }
        if (!empty($saving['validate_approver_status'])) {
          $supStatus = $saving['validate_approver_status'];
          if ($supStatus == "Approved") {
            $color = "#DDF1D5";
          } else if ($supStatus == "Sent For Approval") {
            $color = "#d5dbf1";
          } else if ($supStatus == "Rejected") {
            $color = "#F0DFDF";
          } else {
            $color = "#DCDCDC";
          } ?>
          
          <br /><br /><strong class="notranslate">Validate Savings Status: </strong> <span class="title-text notranslate"> <span class="btn btn-primary" style="background: <?php echo $color; ?> !important;border-color: <?php echo $color; ?>  !important;border-radius: 10px;color:#2d9ca2 !important"><?php echo $supStatus; ?></span></span>
        <?php  } 
        if (!empty($totalBaseLineSpend['total_base_line_spend'])) {?>
          <br /><br /><strong class="notranslate">Total Baseline Spend: </strong> <span class="title-text notranslate"> <?php echo $totalBaseLineSpend['total_base_line_spend']>0? number_format($totalBaseLineSpend['total_base_line_spend'],0,'.',''):''; ?></span>
        <?php } 
        if (!empty($saving['total_project_saving']) && $saving['total_project_saving'] > 0) { ?>
          <br /><br /><strong class="notranslate">Total Projected Savings: </strong> <span class="title-text notranslate"> <?php echo $saving['total_project_saving']>0?number_format($saving['total_project_saving'],0,'.',''):''; ?></span>
        <?php }
        if(!empty($saving['realised_saving']) && $saving['realised_saving'] >0) { ?>
          <br /><br /><strong class="notranslate">Total Realised Savings: </strong> <span class="title-text notranslate"> <?php echo $saving['realised_saving']>0? number_format($saving['realised_saving'],0,'.',''):''; ?></span>
        <?php }  ?>
      </h4>
    </div>



    <div class="span6 pull-right">
      <?php if (strtolower($saving['saving_archive']) == 'yes') { ?>
        
        <div class="clearfix"></div><br />
        <a href="<?php echo AppUrl::bicesUrl('savings/archiveList'); ?>">
          <button type="button" class="btn btn-info" >
            <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
            Return To Archived Savings
          </button>
        </a>
      <?php } else { ?>
        <a href="<?php echo AppUrl::bicesUrl('savings/list'); ?>">
          <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;border-color: #F79820;">
            <span class="glyphicon glyphicon-list-alt mr-2" aria-hidden="true"></span> Return To Savings List
          </button>
        </a>
        <div class="clearfix"></div><br />
        <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#mysupplierStatusModal" style="background-color: #F79820;color: #fff; border-color:#F79820">
          <span class="glyphicon-glyphicon-info-sign" aria-hidden="true"></span> Change Savings Status</button>
      <?php } ?>
    </div>

    <?php $savingCreated = Yii::app()->user->getFlash('saving_message');
    if (!empty($savingCreated)) { ?>
      <div class="clearfix"></div><br />
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $savingCreated; ?>
      </div>
    <?php } ?>
    <div class="clearfix"> </div><br />
    <div class="col-md-7 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2> Savings Amount By Milestone & Savings Type</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div id="saving_area_chart"></div>
        </div>
      </div>
    </div>
    <div class="col-md-5 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Savings Amount By Savings Type</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div id="saving_column_stacked_chart"></div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
    </div>
    <div class="clearfix"> </div>
    <?php if (!empty(Yii::app()->user->getFlash('success'))) : ?>
      <div class="alert alert-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
      </div>
    <?php endif; ?>
    <?php if (Yii::app()->user->hasFlash('error')) : ?>
      <div class="alert alert-danger">
        <?php echo Yii::app()->user->getFlash('error'); ?>
      </div>
    <?php endif; ?>
  </div>

  <div class="row tile_count supplier-mbl-vw-tab" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
      <li role="presentation" class="active">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="quote-tab" role="tab" data-toggle="tab" aria-expanded="true">
          Savings Summary
        </a>
      </li>
      <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] =='milestone'?'active':'';?>">
        <a href="#tab_content2" role="tab" id="vendor-tab" data-toggle="tab" aria-expanded="false">
          Savings Milestones
        </a>
      </li>
      <li role="presentation" class="<?php echo !empty($_GET['tab']) && $_GET['tab'] =='attachments'?'active':'';?>">
            <a href="#tab_content3" role="tab" id="vendor-tab" data-toggle="tab" aria-expanded="false">
                Attachments
            </a>
        </li>
    </ul>

    <div id="myTabContent" class="tab-content">
      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">
        <?php $this->renderPartial('_saving_summary', array('saving' => $saving, 'saving_status' => $saving_status, 'currency_rate' => $currency_rate, 'categories' => $categories, 'locations' => $locations, 'currencyReader' => $currencyReader, 'savingsArea' => $savingsArea,
         'savingsBussinessUnit' => $savingsBussinessUnit, 'totalBaseLineSpend' => $totalBaseLineSpend,
         'totalBaselineSpendRealizedSaving' => $totalBaselineSpendRealizedSaving)); ?>
      </div>
      <div role="tabpanel" class="tab-pane" id="tab_content2" aria-labelledby="vendor-tab">
        <?php $this->renderPartial('_milestone', array('saving' => $saving, 'currencyReader' => $currencyReader, 'milestones' => $milestones)); ?>
      </div>
      <div role="tabpanel" class="tab-pane <?php echo !empty($_GET['tab']) && $_GET['tab'] =='financial'?'fade active in':'';?>" id="tab_content3" aria-labelledby="vendor-tab">
       <?php  $this->renderPartial('_saving_documents',array('saving_id'=>$saving['id']));?>
      </div>
      <div class="clearfix"></div>
    </div>
    <?php $this->renderPartial('../shared_component/_log', array('logTitle' => 'Saving', 'logHistory' => $savingLogs)); ?>
    <?php $this->renderPartial('_savings_modals', array('saving' => $saving, 'saving_status' => $saving_status)); ?>
  </div>
  <style type="text/css">
    .h4,h4 {font-size: 14px;}
    td,th {word-break: break-all !important;  word-wrap: break-word !important;}
    #milestone_table td:last-child { text-align: center; }
    .apexcharts-toolbar {  display: none !important;}
    .form-control {  padding-left: 6px !important; }
    .select2-container .select2-selection--single {  height: 33px !important; }
    .fieldLeftRadius {  border-top-left-radius: 0px !important; border-bottom-left-radius: 0px !important;  }
    .fieldGroupReadOnly {
      background-color: #d8d4d4;  border-radius: 10px;
      border-top-right-radius: 0px;  border-bottom-right-radius: 0px;
    }
  </style>
  <?php
  $subcategoryUrl =  AppUrl::bicesUrl('products/getSubCategories');
  $departmentUrl =  AppUrl::bicesUrl('/locations/getDepartments');
  $milestoneUrl  =  AppUrl::bicesUrl('/savings/listMilestone');
  ?>
  <script type="text/javascript">
    var milestoneurl = "<?php echo AppUrl::bicesUrl('/savings/listMilestone'); ?>";
    var saving_id = "<?php echo $saving['id']; ?>";
    var addMilestoneUrl = "<?php echo AppUrl::bicesUrl('/savings/addSavingMilestone'); ?>";
    var dateFormat = "<?php echo FunctionManager::dateFormatJS(); ?>";
    var deleteMilestoneUrl = "<?php echo AppUrl::bicesUrl('savings/deleteSavingMilestone'); ?>";
    var deletePlaneUrl = "<?php echo AppUrl::bicesUrl('savings/deletePlan'); ?>";
    var savingCalculationFieldsUrl = "<?php echo AppUrl::bicesUrl('savings/savingCalculationFields'); ?>";
    var savingChartUrl = "<?php echo AppUrl::bicesUrl('savings/savingChart1'); ?>";
    var savingChartUrl2 = "<?php echo AppUrl::bicesUrl('savings/savingChart2'); ?>";
    var departmentUrl = "<?php echo $departmentUrl; ?>";
  </script>
  <script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/custom_savings.js'); ?>"></script>
  <script type="text/javascript">
    loadSubcategories("<?php echo $subcategoryUrl; ?>", "<?php echo $saving['category_id']; ?>", "<?php echo $saving['subcategory_id']; ?>");
    loadDepartments("<?php echo $departmentUrl; ?>", "<?php echo $saving['location_id']; ?>", "<?php echo $saving['department_id']; ?>")
    $(document).ready(function() {

      // dynamically create duration field
      $("#field_value,#milestone_due_date,#cost_reduction").on("change keyup blur", function() {
        var duration = $('#field_value').val();
        var start_date = $('#milestone_due_date').val();
        var cost_reduction = $('#cost_reduction').val();
        if (parseFloat(duration) > 0 && parseFloat(cost_reduction) > 0) {
          $.ajax({
            type: "POST",
            data: {
              duration: duration,
              start_date,
              start_date,
              cost_reduction: cost_reduction
            },
            dataType: "json",
            url: "<?php echo AppUrl::bicesUrl('savings/durationValue/'); ?>",
            success: function(results) {
              $('#amountFields').html(results.durationList);
              $('#due_date').val(results.endDate);
            }
          });
        }
      });


      // Start: saving milestone load
      $('#milestone_table').dataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false,
        "autoWidth": true,
        "responsive": true,
        "columnDefs": [{
          "className": "dt-center",
          "targets": [2, 3, 4, 5, 6]
        }],
        "createdRow": function(row, data, index) {
          if (data[0].indexOf('glyphicon glyphicon-ok') >= 0)
            for (var i = 1; i <= 6; i++)
              $('td', row).eq(i).css('text-decoration', 'line-through');
        },
        //"order": [[ 1,"asc" ]],
        // "pageLength": 50,        
        "processing": true,
        "serverSide": false,
        "oLanguage": {
          "sProcessing": "<h1>Please wait ... retrieving data</h1>"
        },
        "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          if (aData[1].search("<span class='deactivated_vendor'></span>") > 0) {
            $(nRow).addClass('deactivated_record');
          }
        }
      });
      // $('#milestone_table').DataTable().ajax.reload();
      // End: saving milestone load


    });


    function loadgeneraldate() {
      $('.generaldate').datetimepicker({
        format: '<?php echo FunctionManager::dateFormatJS(); ?> HH:mm:ss',
        useCurrent: false,
        debug: true
      });
    }

    
    $(document).ready(function() {
      var options = {

        series: [{
          name: 'Projected Savings',
          type: 'area',
          data: [<?php echo implode(",", $project_saving_chart1); ?>]
        }, {
          name: 'Realised Savings',
          type: 'line',
          data: [<?php echo implode(",", $realised_saving_chart1); ?>]
        }],
        chart: {
          fontFamily: 'Poppins !important',
          id: 'id_saving_area_chart',
          height: 270,
          type: 'line',
        },
        stroke: {
          curve: 'smooth'
        },
        fill: {
          type: 'solid',
          opacity: [0.35, 1],
        },
        labels: [<?php echo "'" . implode("','", $milestone_chart1) . "'"; ?>],
        markers: {
          size: 0
        },
        dataLabels: {
          enabled: false,
        },
        xaxis: {
          labels: {
            style: {
              /* colors: '#2d9ca2',*/
              fontSize: '12px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
            },
          }
        },
        yaxis: [

          {
            title: {
              text: '',
              style: {
                fontSize: '12px',
                fontFamily: 'Poppins !important',
                fontWeight: 'normal',
                /*color:'#2d9ca2'*/
              },


            },
            labels: {
              style: {
                /* colors: '#2d9ca2',*/
                fontSize: '12px',
                fontFamily: 'Poppins !important',
                fontWeight: 'normal',
                cssClass: 'apexcharts-xaxis-label',
              },
            }

          },
        ],
        tooltip: {
          shared: true,
          intersect: false,
        },
        legend: {
          position: 'bottom',
          offsetX: 0,
          labels: {
            colors: ['#2d9ca2', '#2d9ca2'],
            useSeriesColors: ['#2d9ca2', '#2d9ca2']
          },
        },
      };
      /* var saving_area_chart = new ApexCharts(document.querySelector("#saving_area_chart"), options);
       saving_area_chart.render();*/
      var options = {
        series: [{
          name: 'Projected Savings',

          data: [<?php echo implode(",", $project_saving_chart1); ?>]
        }, {
          name: 'Realised Savings',

          data: [<?php echo implode(",", $realised_saving_chart1); ?>]
        }],
        chart: {
          fontFamily: 'Poppins !important',
          id: 'id_saving_area_chart',
          type: 'bar',
          height: 273,
          //width:1350,
          toolbar: {
            show: false
          }
        },
        //colors: colorL,
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: [<?php echo "'" . implode("','", $milestone_chart1) . "'"; ?>],
          labels: {
            show: false,
          },
        },
        yaxis: {
          labels: {

            formatter: function(val) {
              return parseInt(val)
            },
          },
          /*title: {
            text: 'Count'
          },*/
          tickAmount: 5,
          //min: 1,
          //max: 20,
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function(val) {

              return val;


            }
          }
        }
      };

      var chart = new ApexCharts(document.querySelector("#saving_area_chart"), options);
      chart.render();
      // End: line and area chart

      // Start: column stacked chart
      colorstype: ['#F44336', '#E91E63', '#9C27B0'];
      var options = {
        series: [{
          name: 'Cost Reduction',
          data: [<?php echo implode(",", $reduction_chart2); ?>]
        }, {
          name: 'Cost Avoidance',
          data: [<?php echo implode(",", $avoidance_chart2); ?>]
        }],
        chart: {
          fontFamily: 'Poppins !important',
          id: 'id_saving_bar_chart',
          type: 'bar',
          height: 270,
          stacked: true,
          toolbar: {
            show: true
          },
          zoom: {
            enabled: true
          }
        },
        dataLabels: {
          enabled: false,
        },
        colors: ['#F79820', '#3b4fb4'],
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '35%',
            //colors: {colorstype},
          },
        },
        xaxis: {
          //type: 'datetime',
          categories: ['Projected Savings', 'Realised Savings'],
          labels: {
            show: false,
            style: {
              colors: ['#000', '#2d9ca2'],
              fontSize: '10px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
            },
          }
        },
        yaxis: {
          labels: {
            style: {
              colors: '#000',
              fontSize: '10px',
              fontFamily: 'Poppins !important',
              fontWeight: 'normal',
              cssClass: 'apexcharts-xaxis-label',
            },
          }
        },
        legend: {
          position: 'bottom',
          offsetX: 0,
          labels: {
            // colors: ['#000','#2d9ca2'],
            //useSeriesColors: ['#000','#2d9ca2']
          },
        },
        fill: {
          opacity: 1
        }
      };

      var chart = new ApexCharts(document.querySelector("#saving_column_stacked_chart"), options);
      chart.render();
      // End: column stacked chart
    });
  </script>
  <style type="text/css">
    th.dt-center,
    td.dt-center {
      text-align: center;
    }

    th {
      word-break: break-all !important;
      word-wrap: break-word !important;
    }

    .ui-datepicker {
      width: 17em !important;
      /*what ever width you want*/
    }

    .bootstrap-datetimepicker-widget.dropdown-menu {
      z-index: 99999999 !important;
      width: 50% !important;
    }
  </style>