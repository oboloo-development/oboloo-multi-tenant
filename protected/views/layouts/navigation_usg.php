<!-- only usg script -->
<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/usg.js'); ?>"></script> 


<div id="sidebar-menu"
	class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<ul class="nav side-menu">
			
			<?php if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>
			<?php $savingTip = MessageManager::tooltipsName('Savings Management'); ?>
			<li <?php if (in_array($this->current_option,array('savings_list','report_build'))) echo ' class="active" '?>>
				<a class="savingMenu-tour">
					<i class="fa fa-tags side_tool" <?php echo $savingTip; ?>></i>
					<span class="hidemenutext" >Savings<span class="hidden-xs"> Management
					</span> <span class="fa fa-chevron-down"></span></span>
				</a>
			<ul class="nav child_menu">
			 <li <?php if ($this->current_option == 'strategicsourcing') echo ' class="active" '; ?>>
			 	<a  href="<?php echo AppUrl::bicesUrl('home'); ?>">Strategic Sourcing</a>
			 </li>
			<!--  <li <?php if ($this->current_option == 'savings_list') echo ' class="active" '; ?>>
			 	<a href="<?php echo AppUrl::bicesUrl('savingslist'); ?>">Savings Management</a>
			 </li> -->
			 <li <?php if ($this->current_option == 'report_build') echo ' class="active" '; ?> class="hidden-xs"><a  href="<?php echo AppUrl::bicesUrl('reports/pivotSaving'); ?>">Report Builder </a>
			 </li>
			</ul>
		   </li>
			<?php } ?>
		<?php 
			$user_type = Yii::app()->session['user_type'];
					
			 if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>
				
   
         <?php if(in_array($user_type,array(4)) && FunctionManager::checkEnvironment(true)){?>
         <?php $settingTip = MessageManager::tooltipsName('Settings'); ?>
			<li <?php if ($this->current_option == 'settings') echo ' class="active" '; ?>>
				<a class="configurations-tour"><i class="fa fa-gears" <?php echo $settingTip; ?>></i><span class="hidemenutext"> Settings <span
					class="fa fa-chevron-down"></span></span></a>
				<ul class="nav child_menu">

					<?php if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>
						<li <?php if ($this->current_option == 'company_details') echo ' class="active" '; ?>>
						<a href="<?php echo AppUrl::bicesUrl('app/companyDetails'); ?>">Company Settings</a></li>
              		<?php } ?>
           	  
					<?php if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>

				 	  <li<?php if ($this->current_option == 'departments') echo ' class="active" '; ?>>
						<a href="<?php echo AppUrl::bicesUrl('departments/list'); ?>">Operating Unit</a>
					  </li> 
					  <li <?php if ($this->current_option == 'locations') echo ' class="active" '; ?>>
					 	<a href="<?php echo AppUrl::bicesUrl('locations/list'); ?>">Savings Oracle Location</a>
					  </li>
				

					<li <?php if ($this->current_option == 'users') echo ' class="active" '; ?>>
						<a href="<?php echo AppUrl::bicesUrl('usersUsg/list'); ?>">Users</a>
					</li>
				    <li <?php if ($this->current_option == 'users') echo ' class="active" '; ?>>
						<a href="<?php echo AppUrl::bicesUrl('goals'); ?>">Goals</a>
					</li> 
					<!-- <li><a
						<?php if ($this->current_option == 'approvals') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('approvals/list'); ?>">Approval
							Routing</a></li> -->
					<li <?php if ($this->current_option == 'currency_rates') echo ' class="active" '; ?>><a
						<?php //if ($this->current_option == 'currency_rates') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/currencyRates'); ?>">Currency</a></li>
					<li <?php if ($this->current_option == 'settings') echo ' class="active" '; ?> class="hidden-xs"><a
						<?php //if ($this->current_option == 'settings') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/settingsusg'); ?>">Configurations</a></li>
              <?php } ?>
            </ul></li>
	<?php  } } ?>

		<?php  $showModel = '';
		if(strpos(Yii::app()->getBaseUrl(true),"sandbox")){
			 $showModel = '#sandbox_help_sidebar_open_module_modal';
		}else{
			 $showModel ='#module_modal';
		} ?>
		</ul>
	</div>
</div>
<!-- /sidebar menu -->
			