<?php
header ( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
header ( 'Last-Modified: ' . gmdate ( 'D, d M Y H:i:s' ) . ' GMT' );
header ( 'Cache-Control: no-store, no-cache, must-revalidate' );
header ( 'Cache-Control: post-check=0, pre-check=0', false );
header ( 'Pragma: no-cache' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="language" content="en" />
			<meta http-equiv="Pragma" content="no-cache">

				<title>
        <?php echo strtoupper(Yii::app()->params['title']); ?>
        <?php if (!empty($this->pageTitle)) echo ' - ' . CHtml::encode($this->pageTitle); ?>
    </title>

				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('bootstrap/dist/css/bootstrap.min.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/nprogress/nprogress.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('animate.css/animate.min.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('build/css/custom.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/validator/fv.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('bootstrap-daterangepicker/daterangepicker.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('fileinput.css'); ?>" />

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('jquery/dist/jquery.min.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('fastclick/lib/fastclick.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('nprogress/nprogress.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/build/js/custom.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('Chart.js/dist/Chart.min.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('moment/min/moment.min.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('bices.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('jquery.validate.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('additional-methods.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('duplicateFields.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('fileinput.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('bootstrap-confirmation.min.js'); ?>"></script>

				<!-- Datatables -->
				<link type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>"
					rel="stylesheet">
					<link type="text/css"
						href="<?php echo AppUrl::cssUrl('../js/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>"
						rel="stylesheet">
						<link type="text/css"
							href="<?php echo AppUrl::cssUrl('../js/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>"
							rel="stylesheet">
							<link type="text/css"
								href="<?php echo AppUrl::cssUrl('../js/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>"
								rel="stylesheet">

								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net/js/jquery.dataTables.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
								<script type="text/javascript"
									src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
								<script type="text/javascript"
									src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-keytable/js/dataTables.keyTable.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>

								<link rel="stylesheet" type="text/css"
									href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.css">
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

									<link rel="stylesheet" type="text/css"
										href="<?php echo AppUrl::cssUrl('pivot.css'); ?>">
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('pivot.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('d3_renderers.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('c3_renderers.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('export_renderers.js'); ?>"></script>

										<style>
.dt-button {
	margin-left: 10px !important;
}
</style>

										<script type="text/javascript">
		var BICES = window.BICES ||
		{
			foo: function()
			{
			}
		};

		BICES.Options = { baseurl: '<?php echo Yii::app()->request->getBaseUrl(true); ?>' };
	</script>

</head>

	<?php
	$user = new User ();
	if (! $user->isLoggedIn ()) {
		?>
			<body class="login">
				<?php echo $content; ?>
			</body>
	<?php
	} else {
		$notification = new Notification ();
		$user_notifications = $notification->getMyNotifications ();
		
		$unread_user_notifications = 0;
		foreach ( $user_notifications as $a_notification )
			if ($a_notification ['read_flag'] == 0)
				$unread_user_notifications += 1;
		?>

	<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="<?php echo AppUrl::bicesUrl('dashboard'); ?>"> <img
							src="<?php echo AppUrl::bicesUrl('images/app_logo.png'); ?>"
							class="img-responsive" />
						</a>
					</div>

					<div class="clearfix">
						<br />
					</div>
					<div class="clearfix">
						<br />
					</div>
					<div class="clearfix">
						<br />
					</div>
					<div class="clearfix">
						<br />
					</div>

					<!-- menu profile quick info -->
					<div class="profile" style="min-height: 40px; text-align: center;">
						<h1><?php echo Yii::app()->session['full_name']; ?></h1>
					</div>
					<!-- /menu profile quick info -->

					<!-- sidebar menu -->
					<div id="sidebar-menu"
						class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<ul class="nav side-menu">
								<li><a
									<?php if ($this->current_option == 'dashboard') echo ' class="current_option" '; ?>
									href="<?php echo AppUrl::bicesUrl('dashboard'); ?>"><i
										class="fa fa-bar-chart"></i> Spend Analysis</a></li>

								<li><a><i class="fa fa-file-text-o"></i> Reporting <span
										class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a
											<?php if ($this->current_option == 'report_build') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('reports'); ?>">Report
												Builder</a></li>
										<li><a
											<?php if ($this->current_option == 'report_pivot') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('reports/pivot'); ?>">Ad-Hoc
												Reporting</a></li>
									</ul></li>
								<li><a><i class="fa fa-question"></i> Requests <span
										class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a
											<?php if ($this->current_option == 'orders_edit') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('orders/edit/0'); ?>">Order</a></li>
										<li><a
											<?php if ($this->current_option == 'expenses_edit') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('expenses/edit/0'); ?>">Travel
												&amp; Expenses</a></li>
									</ul></li>
								<li><a><i class="fa fa-shopping-cart"></i> Orders <span
										class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a
											<?php if ($this->current_option == 'orders_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('orders/list'); ?>">Orders</a></li>
										<li><a
											<?php if ($this->current_option == 'expenses_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('expenses/list'); ?>">Travel
												&amp; Expenses</a></li>
										<li><a
											<?php if ($this->current_option == 'routing') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('routing'); ?>">Approvals</a></li>
										<li><a
											<?php if ($this->current_option == 'purchase_orders') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('purchaseOrders/list'); ?>">Purchase
												Orders</a></li>
										<li><a
											<?php if ($this->current_option == 'invoices_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('invoices/list'); ?>">Invoices</a></li>
									</ul></li>
                  	    <?php if (isset(Yii::app()->session['user_type']) && Yii::app()->session['user_type'] == 1) { ?>
		                    <li><a><i class="fa fa-user"></i> Supplier
										Management <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a
											<?php if ($this->current_option == 'vendors_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('vendors/list'); ?>">All
												Suppliers</a></li>
										<li><a
											<?php if ($this->current_option == 'vendors_edit') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('vendors/edit/0'); ?>">Add
												New Supplier</a></li>
										<li><a
											<?php if ($this->current_option == 'vendors_analysis') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('vendors/analysis'); ?>">Supplier
												Analysis</a></li>
									</ul></li>
                  	    <?php } ?>
	                    <li><a href="#"> <i class="fa fa-list"></i>
										E-Catalogue (<small>Coming Early 2018</small>)
								</a></li>
                  	    <?php if (isset(Yii::app()->session['user_type']) && Yii::app()->session['user_type'] == 1) { ?>
		                    <li><a><i class="fa fa-shopping-basket"></i>
										Internal Catalogue <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a
											<?php if ($this->current_option == 'products_edit') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('products/edit/0'); ?>">Add
												New Product</a></li>
										<li><a
											<?php if ($this->current_option == 'products_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('products/list'); ?>">All
												Products</a></li>
										<li><a
											<?php if ($this->current_option == 'bundles_edit') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('bundles/edit/0'); ?>">Add
												New Bundle</a></li>
										<li><a
											<?php if ($this->current_option == 'bundles_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('bundles/list'); ?>">All
												Bundles</a></li>
									</ul></li>
                  	    <?php } ?>
	                    <li><a><i class="fa fa-handshake-o"></i> Contract
										Management <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a
											<?php if ($this->current_option == 'contracts_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('contracts/list'); ?>">All
												Contracts</a></li>
										<li><a
											<?php if ($this->current_option == 'contracts_edit') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('contracts/edit/0'); ?>">Add
												New Contract</a></li>
										<li><a
											<?php if ($this->current_option == 'contracts_analysis') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('contracts/analysis'); ?>">Contract
												Analysis</a></li>
									</ul></li>
                  	    <?php if (isset(Yii::app()->session['user_type']) && Yii::app()->session['user_type'] == 1) { ?>
		                    <li><a><i class="fa fa-money"></i> Budget
										Management <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a
											<?php if ($this->current_option == 'budget_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('budget/list'); ?>">Budgets</a></li>
										<li><a
											<?php if ($this->current_option == 'budget_analysis') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('budget/analysis'); ?>">Budget
												Analysis</a></li>
									</ul></li>
                  	    <?php } ?>
	                    <li><a><i class="fa fa-group"></i> RFP/RFQ <span
										class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a
											<?php if ($this->current_option == 'quotes_list') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('quotes/list'); ?>">Quotes</a></li>
										<li><a
											<?php if ($this->current_option == 'quotes_edit') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('quotes/edit'); ?>">Create
												Quote</a></li>
									</ul></li>
								<li><a href="#"> <i class="fa fa-gavel" style="float: left;"></i>
										<span class="col-md-10"> Reverse Auctions<br /> (<small>Coming
												Early 2018</small>)
									</span>
								</a>
									<div class="clearfix"></div></li>
								<div class="clearfix">
									<br />
								</div>

								<li><a><i class="fa fa-gears"></i> Settings <span
										class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
	                    	  <?php if (isset(Yii::app()->session['user_type']) && Yii::app()->session['user_type'] == 1) { ?>
	                          	  <li><a
											<?php if ($this->current_option == 'company_details') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('app/companyDetails'); ?>">Company
												Settings</a></li>
	                          <?php } ?>
                          	  <li><a
											<?php if ($this->current_option == 'import_templates') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('app/importTemplates'); ?>">Import
												Templates</a></li>
	                    	  <?php if (isset(Yii::app()->session['user_type']) && Yii::app()->session['user_type'] == 1) { ?>
		                          <li><a
											<?php if ($this->current_option == 'locations') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('locations/list'); ?>">Locations</a></li>
										<li><a
											<?php if ($this->current_option == 'departments') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('departments/list'); ?>">Departments</a></li>
										<li><a
											<?php if ($this->current_option == 'users') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('users/list'); ?>">Users</a></li>
										<li><a
											<?php if ($this->current_option == 'approvals') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('approvals/list'); ?>">Approval
												Routing</a></li>
										<li><a
											<?php if ($this->current_option == 'currency_rates') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('app/currencyRates'); ?>">Currency</a></li>
										<li><a href="<?php echo AppUrl::bicesUrl('budget/list'); ?>">Budgets</a></li>
										<li><a
											<?php if ($this->current_option == 'settings') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('app/settings'); ?>">Options</a></li>
		                  	      <?php if (isset(Yii::app()->session['admin_flag']) && Yii::app()->session['admin_flag'] == 1) { ?>
		                          	  <li><a
											<?php if ($this->current_option == 'import_admin') echo ' class="current_option" '; ?>
											href="<?php echo AppUrl::bicesUrl('app/import'); ?>">Upload
												Templates</a></li>
		                          <?php } ?>
	                          <?php } ?>
	                        </ul></li>
							</ul>
						</div>

						<div class="menu_section">
							<ul class="nav side-menu">
								<li><a href="<?php echo AppUrl::bicesUrl('logout'); ?>"><i
										class="fa fa-sign-out"></i> Log Out</a></li>
							</ul>
						</div>
					</div>
					<!-- /sidebar menu -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>
					<form action="<?php echo AppUrl::bicesUrl('dashboard'); ?>"
						method="post" name="currency_dashboard_form"
						id="currency_dashboard_form">
						<div class="col-md-7 col-xs-12 pull-right"
							style="text-align: right; padding-right: 5%; padding-top: 1%; padding-bottom: 1%;">
              		<?php if (isset($this->current_option) && ($this->current_option == 'dashboard' || $this->current_option == 'contracts_analysis' ||  $this->current_option == 'budget_list')) { ?>
	              		<div class="col-md-4" style="padding-top: 2%; font-weight: bold;">All Amounts In</div>
							<div class="col-md-3">
								<select name="user_currency" id="user_currency"
									class="form-control"
									onchange="$('#currency_dashboard_form').submit();">
									<option value="GBP">GBP</option>
									<option value="USD"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'USD') echo ' selected="SELECTED" '; ?>>USD</option>
									<option value="EUR"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'EUR') echo ' selected="SELECTED" '; ?>>EUR</option>
									<option value="JPY"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'JPY') echo ' selected="SELECTED" '; ?>>JPY</option>
									<option value="INR"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'INR') echo ' selected="SELECTED" '; ?>>INR</option>
									<option value="CHF"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'CHF') echo ' selected="SELECTED" '; ?>>CHF</option>
									<option value="CAD"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'CAD') echo ' selected="SELECTED" '; ?>>CAD</option>
								</select>
							</div>
							<div class="col-md-1" style="padding-top: 2%; font-weight: bold;">
								For</div>
							<div class="col-md-3">
								<select name="user_selected_year" id="user_selected_year"
									class="form-control"
									onchange="$('#currency_dashboard_form').submit();">
				        		<?php $year = date("Y"); for ($i=0; $i<=3; $i++) { ?>
				        			<option value="<?php echo $year - $i; ?>"
										<?php if (isset(Yii::app()->session['user_selected_year']) && Yii::app()->session['user_selected_year'] == ($year - $i)) echo ' selected="SELECTED" '; ?>>
				        				<?php echo $year - $i; ?>
				        			</option>
				        		<?php } ?>
				        	</select>
							</div>
              		<?php } else { ?>
	              		<div class="col-md-6"
								style="padding-top: 2%; font-weight: bold;">All Amounts In</div>
							<div class="col-md-5">
								<select name="user_currency" id="user_currency"
									class="form-control"
									onchange="$('#currency_dashboard_form').submit();">
									<option value="GBP">GBP</option>
									<option value="USD"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'USD') echo ' selected="SELECTED" '; ?>>USD</option>
									<option value="EUR"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'EUR') echo ' selected="SELECTED" '; ?>>EUR</option>
									<option value="JPY"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'JPY') echo ' selected="SELECTED" '; ?>>JPY</option>
									<option value="INR"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'INR') echo ' selected="SELECTED" '; ?>>INR</option>
									<option value="CHF"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'CHF') echo ' selected="SELECTED" '; ?>>CHF</option>
									<option value="CAD"
										<?php if (isset(Yii::app()->session['user_currency']) && Yii::app()->session['user_currency'] == 'CAD') echo ' selected="SELECTED" '; ?>>CAD</option>
								</select>
							</div>
	              	<?php } ?>
	              	<div class="col-md-1">
								<ul class="nav navbar-nav navbar-right">
									<li role="presentation" class="dropdown"><a href="javascript:;"
										class="dropdown-toggle info-number" data-toggle="dropdown"
										aria-expanded="false"> <i class="fa fa-envelope-o"></i>
			                    <?php if ($unread_user_notifications) { ?>
			                    	<span class="badge bg-green"><?php echo $unread_user_notifications; ?></span>
			                    <?php } ?>
			                  </a>
										<ul id="menu1" class="dropdown-menu list-unstyled msg_list"
											role="menu">
			                  	<?php
		foreach ( $user_notifications as $notification ) {
			?>
					                    <li><a><span class="time"><?php echo Notification::getTimeAgo($notification['notification_date']); ?></span></a>
												<br /> <span class="message"><?php echo $notification['notification_text']; ?></span>
											</li>
					            
					            <?php
		}
		?>

			                    <?php if (isset($user_notifications) && count($user_notifications)) { ?>
					                <li>
												<div class="text-center">
													<a href="<?php echo AppUrl::bicesUrl('notifications'); ?>">
														<strong>See All Notifications</strong> <i
														class="fa fa-angle-right"></i>
													</a>
												</div>
											</li>
								<?php } ?>
			                  </ul></li>
								</ul>

							</div>
						</div>
						<input name="current_page_url" id="current_page_url" type="hidden"
							value="<?php echo Yii::app()->request->requestUri; ?>" />
					</form>
					</nav>

				</div>
			</div>
			<!-- /top navigation -->

		  <?php echo $content; ?>

		</div>
	</div>
	</div>
	</div>
	<!-- /page content -->

	<!-- footer content -->
	<footer> <!-- multi select -->
	<link type="text/css"
		href="<?php echo AppUrl::cssUrl('../js/select2/dist/css/select2.min.css'); ?>"
		rel="stylesheet">
		<script type="text/javascript"
			src="<?php echo AppUrl::jsUrl('select2/dist/js/select2.full.min.js'); ?>"></script>

		<!-- nice looking checkboxes -->
		<link type="text/css"
			href="<?php echo AppUrl::cssUrl('../js/iCheck/skins/flat/green.css'); ?>"
			rel="stylesheet">
			<script type="text/javascript"
				src="<?php echo AppUrl::jsUrl('iCheck/icheck.min.js'); ?>"></script>
			<script
				src="<?php echo AppUrl::jsUrl('devbridge-autocomplete/dist/jquery.autocomplete.min.js'); ?>"></script>

			<!-- Multiple file upload -->
			<link type="text/css"
				href="<?php echo AppUrl::cssUrl('../js/dropzone/dist/min/dropzone.min.css'); ?>"
				rel="stylesheet">
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('dropzone/dist/min/dropzone.min.js'); ?>"></script>

				<!--
	  <script type="text/javascript" src="<?php echo AppUrl::jsUrl('jszip/dist/jszip.min.js'); ?>"></script>
	  <script type="text/javascript" src="<?php echo AppUrl::jsUrl('pdfmake/build/pdfmake.min.js'); ?>"></script>
	  <script type="text/javascript" src="<?php echo AppUrl::jsUrl('pdfmake/build/vfs_fonts.js'); ?>"></script>
	  -->

				<div class="clearfix"></div>
	
	</footer>
	<!-- /footer content -->
	</div>
	</div>

</body>

	<?php } ?>

</html>
<script>

<script>
	$("#supplier_type_area").outerHeight($(window).height()-$("#supplier_type_area").offset().top- Math.abs($("#supplier_type_area").outerHeight(true) - $("#supplier_type_area").outerHeight()));
$(window).on("resize", function(){         				$("#supplier_type_area").outerHeight($(window).height()-$("#supplier_type_area").offset().top- Math.abs($("#supplier_type_area").outerHeight(true) - $("#canvas").outerHeight()));
});
</script>

</script>

<script type="text/javascript">
	$(document).ajaxStop(function() {
		var content_height = $(document).height();
		content_height += 100;
		var menu_height = $('.left_col').height();
		if (menu_height < content_height) $('.left_col').height(content_height);
	});
	
	$(document).ready(function() {
		
		var content_height = $(document).height();
		content_height += 100;
		var menu_height = $('.left_col').height();
		if (menu_height < content_height) $('.left_col').height(content_height);

		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var content_height = $(document).height();
			content_height += 100;
			var menu_height = $('.left_col').height();
			if (menu_height < content_height) $('.left_col').height(content_height);
		});
		
		$('.current_option').each(function() {
			if ($(this).parent().parent().hasClass('child_menu'))
			{
				var menu_expanded = false;
				
				$(this).parent().parent().parent().find('a').each(function() {
					if (!menu_expanded) {
						menu_expanded = true;
						$(this).trigger('click');
					}
				});
				
				if (menu_expanded) 
				{
					$(this).parent().css('background', '#12A79C');
					$(this).css('color', '#fff');
					$(this).css('font-size', '110%');
				}
			}
			else 
			{
				$(this).parent().addClass('active');
				$(this).css('font-size', '120%');
			}
		});
	});
</script>
