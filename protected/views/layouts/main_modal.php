<?php
header ( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
header ( 'Last-Modified: ' . gmdate ( 'D, d M Y H:i:s' ) . ' GMT' );
header ( 'Cache-Control: no-store, no-cache, must-revalidate' );
header ( 'Cache-Control: post-check=0, pre-check=0', false );
header ( 'Pragma: no-cache' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="language" content="en" />
			<meta http-equiv="Pragma" content="no-cache">

				<title>
          <?php echo strtoupper(Yii::app()->params['title']); ?>
          <?php if (!empty($this->pageTitle)) echo ' - ' . CHtml::encode($this->pageTitle); ?>
    </title>
	            <link rel="shortcut icon" type="image/png" href="<?php echo AppUrl::bicesUrl('images/icons/favicon.png'); ?>"/>
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('bootstrap/dist/css/bootstrap.min.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/nprogress/nprogress.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('animate.css/animate.min.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('build/css/custom.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/validator/fv.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('bootstrap-daterangepicker/daterangepicker.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('fileinput.css'); ?>" />
	            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	            <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>

	            <script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('jquery/dist/jquery.min.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('fastclick/lib/fastclick.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('nprogress/nprogress.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/build/js/custom.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('Chart.js/dist/Chart.min.js'); ?>"></script>
				<script src="//cdn.jsdelivr.net/npm/apexcharts@latest"></script>
				<script src="<?php echo AppUrl::jsUrl('apexcharts/assets/stock-prices.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('moment/min/moment.min.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('bices.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('jquery.validate.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('additional-methods.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('duplicateFields.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('fileinput.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('bootstrap-confirmation.min.js'); ?>"></script>

				<!-- Datatables -->
				<link type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>"
					rel="stylesheet">
					<link type="text/css"
						href="<?php echo AppUrl::cssUrl('../js/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>"
						rel="stylesheet">
						<link type="text/css"
							href="<?php echo AppUrl::cssUrl('../js/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>"
							rel="stylesheet">
							<link type="text/css"
								href="<?php echo AppUrl::cssUrl('../js/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>"
								rel="stylesheet">

								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net/js/jquery.dataTables.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
								<script type="text/javascript"
									src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
								<script type="text/javascript"
									src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-keytable/js/dataTables.keyTable.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>

								<link rel="stylesheet" type="text/css"
									href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.css">
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

									<link rel="stylesheet" type="text/css"
										href="<?php echo AppUrl::cssUrl('pivot.css'); ?>">
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('pivot.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('d3_renderers.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('c3_renderers.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('export_renderers.js'); ?>"></script>

										<style>
.dt-button {
	margin-left: 10px !important;
}
body {
	background-color: #fff;
}
</style>




</head>
	<body class="nav-md">
		<?php echo $content; ?>

    <style type="text/css">
	body {
    font-family: 'Poppins';
    }
</style>

</body>

</html>



