<?php
$userID = Yii::app()->session['user_id'];
$languageCode = FunctionManager::userLanguage($userID);
setcookie('googtrans', '/auto/'.$languageCode);
header ( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
header ( 'Last-Modified: ' . gmdate ( 'D, d M Y H:i:s' ) . ' GMT' );
header ( 'Cache-Control: no-store, no-cache, must-revalidate' );
header ( 'Cache-Control: post-check=0, pre-check=0', false );
header ( 'Pragma: no-cache' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- <meta name="viewport" content="width=device-width,initial-scale=1" /> -->
			<meta name="language" content="en" />
			<meta http-equiv="Pragma" content="no-cache">
			<title>
			<?php  echo Yii::app()->params['title']; ?>
			<?php if (!empty($this->pageTitle)) echo ' - ' . CHtml::encode($this->pageTitle); ?>
			</title>
    <script type="text/javascript">

		function googleTranslateElementInit() {
			new google.translate.TranslateElement({includedLanguages: 'ar,zh-CN,zh-TW,nl,en,fr,de,hi,it,ja,pl,pt,ru,es,th,ko'}, 'google_translate_element');
		}
	    </script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

	           <link rel="shortcut icon" type="image/png" href="<?php echo AppUrl::bicesUrl('images/icons/favicon.png'); ?>"/>

	            
	            <link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('build/css/jquery-confirm.min.css'); ?>" />


				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('bootstrap/dist/css/bootstrap.min.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/nprogress/nprogress.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('animate.css/animate.min.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('build/css/custom.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/validator/fv.css'); ?>" />
				<!-- <link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('bootstrap-daterangepicker/daterangepicker.css'); ?>" /> -->
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('fileinput.css'); ?>" />
	            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	            <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	            

	             <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">
	
	        <script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('jquery/dist/jquery.min.js'); ?>"></script>

			 <script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('jquery-confirm.min.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('fastclick/lib/fastclick.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('nprogress/nprogress.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/build/js/custom.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/build/js/custom_common.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('Chart.js/dist/Chart.min.js'); ?>"></script>
				<script src="//cdn.jsdelivr.net/npm/apexcharts@latest"></script>
				<script src="<?php echo AppUrl::jsUrl('apexcharts/assets/stock-prices.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('moment/min/moment.min.js'); ?>"></script>
				
			<!-- 	<script type="text/javascript"
					src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script> -->
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('bices.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('jquery.validate.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('additional-methods.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('duplicateFields.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('fileinput.js'); ?>"></script>
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('bootstrap-confirmation.min.js'); ?>"></script>

				<!-- Datatables -->
				<link type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>"
					rel="stylesheet">
					<link type="text/css"
						href="<?php echo AppUrl::cssUrl('../js/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>"
						rel="stylesheet">
						<link type="text/css"
							href="<?php echo AppUrl::cssUrl('../js/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>"
							rel="stylesheet">
							<link type="text/css"
								href="<?php echo AppUrl::cssUrl('../js/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>"
								rel="stylesheet">

								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net/js/jquery.dataTables.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
								<script type="text/javascript"
									src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
								<script type="text/javascript"
									src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
								<script type="text/javascript"
									src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
								<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script> -->
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-keytable/js/dataTables.keyTable.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
								<script type="text/javascript"
									src="<?php echo AppUrl::jsUrl('datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>

								<script type="text/javascript"
									src="//cdn.datatables.net/plug-ins/1.10.20/sorting/date-uk.js"></script>

								<script type="text/javascript">
									$(document).ready(function(){

										$("#menu_toggle").on("click",function(){
											$(".hidemenutext").toggle();
										});

										$("#menu_toggle").trigger("click");
										
										
									});
								</script>

								<link rel="stylesheet" type="text/css"
									href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.css">
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
									<script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

									<link rel="stylesheet" type="text/css"
										href="<?php echo AppUrl::cssUrl('pivot.css'); ?>">
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('pivot.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('d3_renderers.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('c3_renderers.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('export_renderers.js'); ?>"></script>

										<style>
.dt-button {
	margin-left: 10px !important;
}

</style>

	<script type="text/javascript">

		moment.updateLocale('en', {
		  weekdaysMin : ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
		});


		var BICES = window.BICES ||
		{
			foo: function()
			{
			}
		};

		BICES.Options = { baseurl: '<?php echo Yii::app()->request->getBaseUrl(true); ?>' };
	</script>

	<?php 
	
	$user = new User ();

	if ($user->isLoggedIn ()) {?>
		
<?php }
 ?>

</head>

	<?php
	
	if (! $user->isLoggedIn () || (!isset(Yii::app()->session['2fa']))) {

		?>
			<body class="login" style="zoom:85%;">
				<?php echo $content; ?>
			</body>
	<?php
	} else {


		$userData = $user->getOne(array('user_id' => Yii::app()->session['user_id']));

		if(!empty($userData['profile_img'])){
			$profileImg = 'users/'.Yii::app()->session['user_id']."/".$userData['profile_img'];
		}else{
			$profileImg = "img.jpg";
		}

		$notification = new Notification ();
		$user_notifications = $notification->getMyNotifications ();
		$user_lastest_notifications = $notification->getMyHomeNotifications ();

		$unread_user_notifications = 0;
		foreach ( $user_notifications as $a_notification )
			if ($a_notification ['read_flag'] == 0)
				$unread_user_notifications += 1;
		?>

	<?php /*if($this->action->id =='home'){
      		$bgClass = "homebgbody";
	  }else{
	  		$bgClass = "";
	  }*/
	?>
	<body class="nav-md <?php //echo $bgClass;?>" style="zoom:85% !important;
	 background: url('<?php echo AppUrl::bicesUrl('images/screencap.png'); ?>') !important;
   		 background-repeat: no-repeat !important; background-size: cover !important; 
    height: 100%; width: 100%;">
	<div class="container body">
		<div class="main_container">

			<!-- top navigation -->
			<?php if(true /*$this->action->id !='home'*/){?>
			<div class="top_nav"></div>
		<?php } ?>
			<!-- /top navigation -->
		    <div class="clearfix"></div>
		     <?php echo $content; ?>
		    </div>
	</div>
	</div>
	</div>
	<!-- /page content -->

	<!-- footer content -->
	<footer> <!-- multi select -->
	<link type="text/css"
		href="<?php echo AppUrl::cssUrl('../js/select2/dist/css/select2.min.css'); ?>"
		rel="stylesheet">
		<script type="text/javascript"
			src="<?php echo AppUrl::jsUrl('select2/dist/js/select2.full.min.js'); ?>"></script>

		<!-- nice looking checkboxes -->
		<link type="text/css"
			href="<?php echo AppUrl::cssUrl('../js/iCheck/skins/flat/green.css'); ?>"
			rel="stylesheet">
			<script type="text/javascript"
				src="<?php echo AppUrl::jsUrl('iCheck/icheck.min.js'); ?>"></script>
			<script
				src="<?php echo AppUrl::jsUrl('devbridge-autocomplete/dist/jquery.autocomplete.min.js'); ?>"></script>

			<!-- Multiple file upload -->
			<link type="text/css"
				href="<?php echo AppUrl::cssUrl('../js/dropzone/dist/min/dropzone.min.css'); ?>"
				rel="stylesheet">
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('dropzone/dist/min/dropzone.min.js'); ?>"></script>

				<!--
	  <script type="text/javascript" src="<?php echo AppUrl::jsUrl('jszip/dist/jszip.min.js'); ?>"></script>
	  <script type="text/javascript" src="<?php echo AppUrl::jsUrl('pdfmake/build/pdfmake.min.js'); ?>"></script>
	  <script type="text/javascript" src="<?php echo AppUrl::jsUrl('pdfmake/build/vfs_fonts.js'); ?>"></script>
	  -->

				<div class="clearfix"></div>
	
	</footer>
	<!-- /footer content -->
	</div>
	</div>
    <style type="text/css">
	body {
    font-family: 'Poppins';
    }
</style>

</body>

	<?php } ?>

</html>



<?php

 if($this->action->id =="edit" && $this->id =="quotes"){ ?>
<script type="text/javascript">
	
		content_height = 2732;
	    $('.left_col').height(content_height);

</script>
<?php }else if($this->action->id !="dashboard"){?>

	<script type="text/javascript">


    $('#contract_table,#order_table,#expense_table,#expense_table,#routing_table,#invoice_table,#quote_table,#user_table').on( 'length.dt', function ( e, settings, len ) {
    	
 		content_height = 3584;
		content_height += 100;
		var menu_height = $('.left_col').height(3684);

		
	});

	$(document).ajaxStop(function() {
		var content_height = $(document).height();
		content_height += 100;
		var menu_height = $('.left_col').height();

		if (menu_height < content_height) $('.left_col').height(content_height);

	});
</script>
<?php } else { ?>
<script type="text/javascript">
	$(document).ajaxStop(function() {
		var content_height = $(document).height();
		content_height += 308;
	    $('.left_col').height(content_height);

	});
</script>
<?php } ?>
<script type="text/javascript">

	$(window).load(function(){
    	$(".goog-logo-link").empty();
    	$('.goog-te-gadget').html($('.goog-te-gadget').children());
    });
	
	$(document).ready(function() {


	/*$("#supplier_type_area").outerHeight($(window).height()-$("#supplier_type_area").offset().top- Math.abs($("#supplier_type_area").outerHeight(true) - $("#supplier_type_area").outerHeight()));
$(window).on("resize", function(){         				$("#supplier_type_area").outerHeight($(window).height()-$("#supplier_type_area").offset().top- Math.abs($("#supplier_type_area").outerHeight(true) - $("#canvas").outerHeight()));
});

*/
		
		var content_height = $(document).height();
		content_height += 100;
		var menu_height = $('.left_col').height();
		if (menu_height < content_height) $('.left_col').height(content_height);

		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var content_height = $(document).height();
			content_height += 100;
			var menu_height = $('.left_col').height();
			if (menu_height < content_height) $('.left_col').height(content_height);
		});
		
		$('.current_option').each(function() {
			if ($(this).parent().parent().hasClass('child_menu'))
			{
				var menu_expanded = false;
				
				$(this).parent().parent().parent().find('a').each(function() {
					if (!menu_expanded) {
						menu_expanded = true;
						$(this).trigger('click');
					}
				});
				
				if (menu_expanded) 
				{
					$(this).parent().css('background', '#12A79C');
					$(this).css('color', '#fff');
					$(this).css('font-size', '110%');
				}
			}
			else 
			{
				$(this).parent().addClass('active');
				//$(this).css('font-size', '120%');
			}
		});
	});

function changeCurrency()
{
	$('#currency_dashboard_form').submit();
}

/*function addOrderModal(e){
	e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('orders/createByModal'); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_order_cont').html(mesg);
            $('#create_order').modal('show');
            
             
        }
    });
}
 
function addExpenseModal(e){
	e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('expenses/createByModal'); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_expense_cont').html(mesg);
            $('#create_expense_modal').modal('show');
            
             
        }
    });
}*/

function changeLanguageLabel(){
	$('.goog-te-combo option:contains("Arabic")').text(" العربية ");
	$('.goog-te-combo option:contains("Chinese (Simplified)")').text('简体中文');
	$('.goog-te-combo option:contains("Chinese (Traditional)")').text('中國傳統的');
	$('.goog-te-combo option:contains("Dutch")').text('Nederlands');
	$('.goog-te-combo option:contains("French")').text('Français');
	$('.goog-te-combo option:contains("German")').text('Deutsche');
	$('.goog-te-combo option:contains("Hindi")').text('हिन्दी');
	$('.goog-te-combo option:contains("Italian")').text('Italiana');
	$('.goog-te-combo option:contains("Japanese")').text('Português');
	$('.goog-te-combo option:contains("Polish")').text('Polskie');
	$('.goog-te-combo option:contains("Portuguese")').text('Português');
	// $('.goog-te-combo option:contains("Russian")').text('русский');
	$('.goog-te-combo option:contains("Spanish")').text('Español');
}

$(document).ready(function(){
	
	$("body").on("change", "#google_translate_element select", function (e) {
		
		changeLanguageLabel();
        
		setTimeout(function(){ 
				changeLanguageLabel();
		}, 100);
		setTimeout(function(){ 
				changeLanguageLabel();
		}, 500);
		setTimeout(function(){ 
				changeLanguageLabel();
		}, 2000);
		setTimeout(function(){ 
			changeLanguageLabel();
	}, 2500);
	});
	

	setTimeout(function(){ 
			changeLanguageLabel();
	}, 100);
	setTimeout(function(){ 
			changeLanguageLabel();
	}, 500);
	setTimeout(function(){ 
			changeLanguageLabel();
	}, 2000);
	
	setTimeout(function(){ 
			changeLanguageLabel();
	}, 2500);
});
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
<!-- Start Google Translater -->
<style>
	.goog-logo-link {
   display:none !important;
}

.goog-te-gadget {
   color: transparent !important;
}

.goog-te-gadget .goog-te-combo {
   color: blue !important;
}
.goog-te-banner-frame.skiptranslate {
    display: none !important;
    } 
body {
    top: 0px !important; 
    }
 .presentation{display: block;}
    /*.goog-logo-link {
        display: none;
    }
    .goog-te-banner-frame.skiptranslate {
            display: none !important;
        }*/
    .goog-te-combo, .goog-te-banner *, .goog-te-ftab *, .goog-te-menu *, .goog-te-menu2 *, .goog-te-balloon * {
    	font-family:Poppins !important;
    	font-size: 12px;
		}

.goog-te-gadget .goog-te-combo { color: #000 !important;  border: none; padding: 5px 5px 9px 5px;
	    -webkit-box-shadow: 0 5px 15px 5px rgba(80, 102, 224, 0.08);
	    box-shadow: 0 5px 15px 5px rgba(80, 102, 224, 0.08);  border-radius: 30px;    
	}
.goog-tooltip { display: none !important;}
.goog-tooltip:hover { display: none !important;}
.goog-text-highlight {  background-color: transparent !important; border: none !important; box-shadow: none !important;}

.main_container .top_nav {  display: block; margin-left: 59px;}

/*ul.child_menu{display: block; max-height: 100px; overflow-y: auto;}*/
</style>

<!--  END Google Translater -->
<?php //setcookie('googtrans', '/en/'.$languageCode);//Yii::app()->user->setFlash('success','');?>

