<?php
$userID = Yii::app()->session['user_id'];
if (!empty($userID)) {
	$languageCode = FunctionManager::userLanguage($userID);
	$languageCode = !empty(Yii::app()->session['default_language_code']) ? Yii::app()->session['default_language_code'] : 'en';
	setcookie('googtrans', '/auto/' . $languageCode);
}

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
$domainUrl = Yii::app()->getBaseUrl(true);
$customisedSubDomain = FunctionManager::savingSpecific();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="language" content="en" />
	<meta http-equiv="Pragma" content="no-cache">
	<link rel="shortcut icon" type="image/png" href="<?php echo AppUrl::bicesUrl('images/icons/favicon.png'); ?>" />
	<title><?php echo Yii::app()->params['title'];
			if (!empty($this->pageTitle)) echo ' - ' . CHtml::encode($this->pageTitle); ?></title>
	<?php if (!strpos(Yii::app()->getBaseUrl(true), "local")) { ?>
		<script type="text/javascript">
			function googleTranslateElementInit() {
				new google.translate.TranslateElement({
					includedLanguages: 'ar,zh-CN,zh-TW,nl,en,fr,de,hi,it,ja,pl,pt,ru,es,th,ko'
				}, 'google_translate_element');
			}
		</script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	<?php } ?>
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/jquery-confirm.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('bootstrap/dist/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('../js/nprogress/nprogress.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('animate.css/animate.min.css'); ?>" />
	<!-- <link rel="stylesheet" type="text/css" href="<?php //echo AppUrl::cssUrl('welcome.css'); 
														?>" /> -->
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/home.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/quote.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/contract.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/vendor.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/saving.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/common.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/custom.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('../js/validator/fv.css'); ?>" />

	<?php if (!empty($customisedSubDomain) && in_array($customisedSubDomain, FunctionManager::subdomains()) && $customisedSubDomain == 'usg') { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/usg.css'); ?>" />
	<?php } else { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('build/css/changes_zoom.css'); ?>" />
	<?php } ?>
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::jsUrl('dist-tree/css/bootstrap-treefy.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo AppUrl::cssUrl('fileinput.css'); ?>" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">

	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('jquery/dist/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('jquery-confirm.min.js'); ?>"></script>

	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('fastclick/lib/fastclick.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('nprogress/nprogress.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/custom.js'); ?>"></script>
	<script src="//cdn.jsdelivr.net/npm/apexcharts@latest"></script>
	<?php if ($this->action->id != 'edit') { ?>
		<script type="text/javascript" src="<?php echo AppUrl::jsUrl('Chart.js/dist/Chart.min.js'); ?>"></script>


		<script src="<?php echo AppUrl::jsUrl('apexcharts/assets/stock-prices.js'); ?>"></script> <?php } ?>

	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('moment/min/moment.min.js'); ?>"></script>

	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('bices.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('jquery.validate.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('additional-methods.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('duplicateFields.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('fileinput.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('bootstrap-confirmation.min.js'); ?>"></script>
	<script src="https://cdn.paddle.com/paddle/paddle.js"></script>

	<!-- Datatables -->
	<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
	<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
	<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
	<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">

	<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net/js/jquery.dataTables.min.js'); ?>"></script> -->
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
	<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script> -->
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
	<!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script> -->
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-keytable/js/dataTables.keyTable.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>

	<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/date-uk.js"></script>



	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>
	<!-- <script type="text/javascript"
										src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script> -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
	<?php /*<script type="text/javascript" src="js/bootstrap/bootstrap-dropdown.js"></script>
									<link rel="stylesheet" type="text/css"
										href="<?php echo AppUrl::cssUrl('pivot.css'); ?>">
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('pivot.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('d3_renderers.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('c3_renderers.js'); ?>"></script>
										<script type="text/javascript"
											src="<?php echo AppUrl::jsUrl('export_renderers.js'); ?>"></script> */ ?>

	<script src="<?php echo AppUrl::jsUrl('dist-tree/bootstrap-treefy.js'); ?>"></script>

	<style>
		.dt-button {
			margin-left: 10px !important;
		}

		.dashboard-logo {
			max-height: 124px !important;
			width: 100% !important;
		}
	</style>

	<script type="text/javascript">
		moment.updateLocale('en', {
			weekdaysMin: ["S", "M", "T", "W", "T", "F", "S"],
		});
		var BICES = window.BICES || {
			foo: function() {}
		};
		BICES.Options = {
			baseurl: '<?php echo Yii::app()->request->getBaseUrl(true); ?>'
		};
	</script>
	<?php
	$user = new User();
	if ($user->isLoggedIn()) { ?>
	<?php } ?>
	<!-- Copy into the head of all HTML pages you wish to display the widget -->
	<!-- stonly-widget - copy into the head -->
	<script>
		window.STONLY_WID = "5a924814-25c9-11ed-871a-0a52ff1ec764";
	</script>
	<script>
		! function(s, t, o, n, l, y, w, g) {
			s.StonlyWidget || ((w = s.StonlyWidget = function() {
					w._api ? w._api.apply(w, arguments) : w.queue.push(arguments)
				}).scriptPath = n, w.queue = [], (y = t.createElement(o)).async = !0,
				(g = new XMLHttpRequest).open("GET", n + "version?v=" + Date.now(), !0), g.onreadystatechange = function() {
					4 === g.readyState && (y.src = n + "stonly-widget.js?v=" + (200 === g.status ? g.responseText : Date.now()),
						(l = t.getElementsByTagName(o)[0]).parentNode.insertBefore(y, l))
				}, g.send())
		}(window, document, "script", "https://stonly.com/js/widget/v2/");
	</script>

</head>
<?php
$subdomainByUrl = FunctionManager::subdomainByUrl();
if (!$user->isLoggedIn() && (!isset(Yii::app()->session['2fa']))) {
?>

	<body class="login" style="zoom:85%; ">
		<?php echo $content; ?>
	</body>
<?php
} else {
	$userData = $user->getOne(array('user_id' => Yii::app()->session['user_id']));
	if (!empty($userData['profile_img'])) {
		$profileImg = $subdomainByUrl . '/users/' . Yii::app()->session['user_id'] . "/" . $userData['profile_img'];
		$profileImg = AppUrl::bicesUrl('/images/' . $profileImg);
	} else {
		$profileImg = CommonFunction::userTextAvatar($userData['full_name']);
	}
	$companyLogo = FunctionManager::dishboardLogo();
	$notification = new Notification();
	$user_notifications = $notification->getMyNotifications();
	$user_lastest_notifications = $notification->getMyHomeNotifications();
	$unread_user_notifications = 0;
	foreach ($user_notifications as $a_notification)
		if ($a_notification['read_flag'] == 0)
			$unread_user_notifications += 1;
?>
	<?php
	if (strpos(Yii::app()->getBaseUrl(true), "local") || strpos(Yii::app()->getBaseUrl(true), "multi")) {
		$subdomain = 'oboloo1';
	} else {
		$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
	}
	$postRequest = array(
		'subdomain' => $subdomain,
	);

	if (strpos(Yii::app()->getBaseUrl(true), "devoboloo")) {
		$cURLConnection = curl_init('https://devoboloo.com/saas/clients/callForSubdomain');
	} else {
		$cURLConnection = curl_init('https://oboloo.software/saas/clients/callForSubdomain');
	}
	curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
	curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, false);

	$apiResponse = curl_exec($cURLConnection);
	curl_close($cURLConnection);
	// $apiResponse - available data from the API request
	$jsonArrayResponse  =  json_decode($apiResponse); ?>

	<body class="nav-sm">
		<div class="container body">
			<div class="main_container">
				<div class="col-md-3 left_col" style="height: auto !important;">
					<div class="scroll-view">
						<br />
						<div class="navbar nav_title" style="border: 0; width: 100%;">
							<a href="<?php echo AppUrl::bicesUrl('home'); ?>"> <img src="<?php echo AppUrl::bicesUrl($companyLogo); ?>" class="img-responsive dashboard-logo" style="width: 100%; " />
							</a>
						</div>
						<div class="clearfix"></div>
						<?php if (empty($customisedSubDomain)) { ?>
							<div class="clearfix"><br /></div>
							<div class="clearfix"><br /></div>
							<div class="clearfix"><br /></div>
							<div class="clearfix"><br /></div>
						<?php } ?>

						<!-- menu profile quick info -->
						<!-- <div class="profile" style="min-height: 40px; text-align: center;">
						<h1><?php echo Yii::app()->session['full_name']; ?></h1>
					</div> -->
						<!-- /menu profile quick info -->

						<!-- sidebar menu -->
						<?php
						if (!empty($customisedSubDomain) && in_array($customisedSubDomain, FunctionManager::subdomains()) && $customisedSubDomain !== "usg") {
							$this->renderPartial('/layouts/custom_domains/navigation_' . $customisedSubDomain);
						} else if (!empty($customisedSubDomain) && in_array($customisedSubDomain, FunctionManager::subdomains())) {
							$this->renderPartial('/layouts/navigation_' . $customisedSubDomain);
						} else {
							echo $this->renderPartial('/layouts/navigation');
						} ?>
						<!-- /sidebar menu -->
					</div>
				</div>

				<!-- top navigation -->
				<?php if (true /*$this->action->id !='home'*/) { ?>
					<div class="top_nav">
						<div class="nav_menu">
							<nav>
								<div class="nav toggle">
									<a id="menu_toggle"><i class="fa fa-bars"></i></a>
								</div>
								<!-- <div id="google_translate_element" style="float: left;margin-top: 7px;"></div> -->




								<div class="col-md-7 col-xs-12 pull-right user-dropdown" style="text-align: right; /*padding-right: 5%;*/ padding-top: 1%; padding-bottom: 1%;">

									<div class="col-md-8 pull-right">
										<?php //if (isset($this->current_option) && ($this->current_option == 'dashboard' || $this->current_option == 'contracts_analysis' ||  $this->current_option == 'budget_list' || $this->current_option == 'vendors_analysis' )) { 
										?>
										<?php $notificationClass = "col-md-12 col-xs-12 text-right"; ?>

										<div class="<?php echo $notificationClass; ?>">
											<ul class="nav navbar-nav navbar-right user-dropdown">
												<?php if (($this->action->id == 'list' && $this->id == 'savings') || ($this->action->id == 'list' && $this->id == 'vendors') || ($this->action->id == 'list' && $this->id == 'contracts') || ($this->action->id == 'list' && $this->id == 'quotes')
													|| ($this->action->id == 'list' && $this->id == 'vendorEvaluation') || ($this->action->id == 'home' && $this->id == 'app')
												) { ?>
												<?php }  ?>

												<?php if (FunctionManager::checkEnvironment(true)) { ?>
													<li class="nav-item dropdown user-dropdown2" style="padding-left: 15px;margin-top: -5px; ">
														<a href="javascript:;" id="user_prfile_img" class="user-profile  dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
															<div class="user-profile">
																<img src="<?php echo $profileImg; ?>" alt="" />
															</div>
														</a>
														<div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
															<a class="dropdown-item" href="<?php echo AppUrl::bicesUrl('app/profile'); ?>">Profile</a>
															<a class="dropdown-item" href="<?php echo AppUrl::bicesUrl('app/qrcodeRefresh') ?>">Two-Factor Authentication</a>
															<a class="dropdown-item" href="https://oboloo.com/support-hub" target="_blank"> User Guides</a>
															<!-- <a class="dropdown-item" href="<?php echo AppUrl::bicesUrl('app/raiseTicket') ?>" >Raise a Ticket</a> -->
															<a class="dropdown-item" href="https://oboloo.com/raise-a-ticket/" target="_blank">Raise a Ticket</a>
															<!-- <a class="dropdown-item" href="<?php echo AppUrl::bicesUrl('app/feedback') ?>">Feedback</a> -->
															<a class="dropdown-item" href="https://oboloo.com/feedback-form/" target="_blank">Feedback</a>
															<a class="dropdown-item">
																<?php $this->renderPartial('/app/environment'); ?>
															</a>
															<a class="dropdown-item" href="<?php echo AppUrl::bicesUrl('logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
														</div>
													</li>

												<?php } //if(!empty($user_lastest_notifications)) { 
												?>

												<li role="presentation" class="dropdown recent-notification hidden-xs"><a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-envelope-o" style="color: #3b4fb4"></i>
														<?php //if ($unread_user_notifications) { 
														?>
														<span class="badge bg-green"><?php echo $unread_user_notifications; ?></span>
														<?php //} 
														?>
													</a>
													<ul id="menu1" class="dropdown-menu list-unstyled msg_list d-flex" role="menu">
														<?php
														$notCtr = 1;
														foreach ($user_lastest_notifications as $notification) { ?>
															<div class="col-md-2 top-alert-btn-cont">
																<?php
																if (strtolower($notification['notification_type']) == 'contract') {
																	$btnStyle = "background-color: #fdaaaa;border-color: #fdaaaa;font-size:8px;margin-top: 27px;";
																	$btnText = "Contract";
																} elseif (strtolower($notification['notification_type']) ==  'supplier status' || strtolower($notification['notification_type']) == 'vendor') {
																	$btnStyle = "background-color: #fec98f;border-color: #fec98f;font-size:8px;margin-top: 27px;";
																	$btnText = "Supplier";
																} elseif (strtolower($notification['notification_type']) ==  'saving milestone') {
																	$btnStyle = "background-color: #98ddfc;border-color: #98ddfc;font-size:8px;margin-top: 27px;";
																	$btnText = "Savings";
																} elseif (strtolower($notification['notification_type']) ==  'savings status') {
																	$btnStyle = "background-color: #98ddfc;border-color: #98ddfc;font-size:8px;margin-top: 27px;";
																	$btnText = "Savings";
																} elseif (empty($notification['notification_type']) || strtolower($notification['notification_type']) ==  'quote' || strtolower($notification['notification_type']) ==  'quote and not home') {
																	$btnStyle = "background-color: #a0e7a0;border-color: #a0e7a0;font-size:8px;margin-top: 27px;";
																	$btnText = "eSourcing";
																} else {
																	$btnStyle = "";
																	$btnText = "Other";
																} ?>
																<button class="btn btn-success" style="<?php echo $btnStyle; ?>"><?php echo $btnText; ?></button>
															</div>
															<div class="col-md-9 top-alert-text-cont">
																<span class="message"><?php echo $notification['notification_text']; ?></span>
																<li><a><span class="time"><?php echo Notification::getTimeAgo($notification['notification_date']); ?></span></a><br />
															</div>
															<div class="col-md-12">
																<hr class="alert-hr">
															</div>
												</li>
											<?php } ?>
											<?php if (isset($user_lastest_notifications) && count($user_lastest_notifications)) { ?>
												<li>
													<div class="text-center">
														<a href="<?php echo AppUrl::bicesUrl('notifications'); ?>"><strong>See All Notifications</strong> <i class="fa fa-angle-right" style="margin-left: 10px;"></i>
														</a>
													</div>
												</li>
											<?php } ?>
											</ul>
											</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
						</div>
						<?php if (!FunctionManager::checkEnvironment(true)) { ?>
							<div class="clearfix"> <br /></div>
							<div class="col-md-12 col-xs-12 show-sandbox">
								<div class="alert alert-danger">
									<strong>You are currently in sandbox view. To add data please log back into your company's own oboloo subdomain</strong>
								</div>
							</div>
						<?php }  ?>
						</nav>
					</div>

				<?php } else { ?>
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>
					</nav>
				<?php } ?>
				<!-- /top navigation -->

				<?php echo $content; ?>

			</div>
		</div>
		</div>
		</div>
		<!-- /page content -->

		<!-- modules pages pop -->
		<?php
		/* if(strpos(Yii::app()->getBaseUrl(true),"sandbox")){
		echo  $this->renderPartial('/app/help_information_popup/sandbox_help_sidebar_open_pupop');
		}
		echo  $this->renderPartial('/app/information_poup');
	  	*/
		?>

		<script type="text/javascript">
			function googleTranslateElementInit() {
				new google.translate.TranslateElement({
					includedLanguages: 'ar,zh-CN,zh-TW,nl,en,fr,de,hi,it,ja,pl,pt,ru,es,th,ko'
				}, 'google_translate_element');
			}
		</script>
		<?php if (!strpos(Yii::app()->getBaseUrl(true), "multi-local")) { ?>
			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		<?php }   ?>
		</div>

		<!-- footer content -->
		<footer> <!-- multi select -->
			<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
			<script type="text/javascript" src="<?php echo AppUrl::jsUrl('select2/dist/js/select2.full.min.js'); ?>"></script>
			<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/custom_common.js'); ?>"></script>
			<!-- nice looking checkboxes -->
			<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">
			<script type="text/javascript" src="<?php echo AppUrl::jsUrl('iCheck/icheck.min.js'); ?>"></script>
			<script src="<?php echo AppUrl::jsUrl('devbridge-autocomplete/dist/jquery.autocomplete.min.js'); ?>"></script>
			<!-- Multiple file upload -->
			<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/dropzone/dist/min/dropzone.min.css'); ?>" rel="stylesheet">
			<script type="text/javascript" src="<?php echo AppUrl::jsUrl('dropzone/dist/min/dropzone.min.js'); ?>"></script>
			<div class="clearfix"></div>
		</footer>
		<script type="text/javascript" src="<?php echo AppUrl::jsUrl('select2/dist/js/select2.full.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/custom_common.js'); ?>"></script>

		<!-- nice looking checkboxes -->
		<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">
		<script type="text/javascript" src="<?php echo AppUrl::jsUrl('iCheck/icheck.min.js'); ?>"></script>
		<script src="<?php echo AppUrl::jsUrl('devbridge-autocomplete/dist/jquery.autocomplete.min.js'); ?>"></script>

		<!-- Multiple file upload -->
		<link type="text/css" href="<?php echo AppUrl::cssUrl('../js/dropzone/dist/min/dropzone.min.css'); ?>" rel="stylesheet">
		<script type="text/javascript" src="<?php echo AppUrl::jsUrl('dropzone/dist/min/dropzone.min.js'); ?>"></script>
		<!--
				<script type="text/javascript" src="<?php echo AppUrl::jsUrl('jszip/dist/jszip.min.js'); ?>"></script>
				<script type="text/javascript" src="<?php echo AppUrl::jsUrl('pdfmake/build/pdfmake.min.js'); ?>"></script>
				<script type="text/javascript" src="<?php echo AppUrl::jsUrl('pdfmake/build/vfs_fonts.js'); ?>"></script>
	 			-->
		<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
		</div>
		</div>

		<style type="text/css">
			.goog-te-banner-frame {display: none !important;}
			.skiptranslate { display: none !important;}
			body { font-family: 'Poppins'; }
			.icon-written-conversation-speech-bubble-with-letter-i-inside-of-information-for-interface-2:before {content: "\e900"; color: #fff;}
		</style>
	</body>
<?php } ?>
</html>
<?php
if ($this->action->id == "edit" && $this->id == "quotes") { ?>
	<script type="text/javascript">
		content_height = 2732;
		$('.left_col').height(content_height);
	</script>
<?php } else if ($this->action->id != "dashboard") { ?>

	<script type="text/javascript">
		$('#contract_table,#order_table,#expense_table,#expense_table,#routing_table,#invoice_table,#quote_table,#user_table').on('length.dt', function(e, settings, len) {
			content_height = 3584;
			content_height += 100;
			var menu_height = $('.left_col').height(3684);
		});

		$(document).ajaxStop(function() {
			var content_height = $(document).height();
			content_height += 100;
			var menu_height = $('.left_col').height();

			if (menu_height < content_height) $('.left_col').height(content_height);

		});
	</script>
<?php } else { ?>
	<script type="text/javascript">
		comopnentDidMount() {
			window.addEventListener("error", () => {
				this.setState({
					showGeneralError: true
				});
			});
		}
		$(document).ajaxStop(function() {
			var content_height = $(document).height();
			content_height += 308;
			$('.left_col').height(content_height);

		});
	</script>
<?php } ?>
<script type="text/javascript">
	window.onload = function() {
		var banner = document.getElementsByClassName('goog-te-banner-frame')[0];
		if (banner) {
			banner.style.display = 'none';
		}
	}

	$(window).load(function() {

		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
			$(".goog-logo-link").empty();
			$('.goog-te-gadget').html($('.goog-te-gadget').children());
		});
	});
	$(document).ready(function() {
		var content_height = $(document).height();
		content_height += 100;
		var menu_height = $('.left_col').height();
		if (menu_height < content_height) $('.left_col').height(content_height);

		$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
			var content_height = $(document).height();
			content_height += 100;
			var menu_height = $('.left_col').height();
			if (menu_height < content_height) $('.left_col').height(content_height);
		});

		$('.current_option').each(function() {
			if ($(this).parent().parent().hasClass('child_menu')) {
				var menu_expanded = false;

				$(this).parent().parent().parent().find('a').each(function() {
					if (!menu_expanded) {
						menu_expanded = true;
						$(this).trigger('click');
					}
				});

				if (menu_expanded) {
					$(this).parent().css('background', '#12A79C');
					$(this).css('color', '#fff');
					$(this).css('font-size', '110%');
				}
			} else {
				$(this).parent().addClass('active');
				//$(this).css('font-size', '120%');
			}
		});
	});

	function changeLanguageLabel() {
		$('.goog-te-combo option:contains("Arabic")').text(" العربية ");
		$('.goog-te-combo option:contains("Chinese (Simplified)")').text('简体中文');
		$('.goog-te-combo option:contains("Chinese (Traditional)")').text('中國傳統的');
		$('.goog-te-combo option:contains("Dutch")').text('Nederlands');
		$('.goog-te-combo option:contains("French")').text('Français');
		$('.goog-te-combo option:contains("German")').text('Deutsche');
		$('.goog-te-combo option:contains("Hindi")').text('हिन्दी');
		$('.goog-te-combo option:contains("Italian")').text('Italiana');
		$('.goog-te-combo option:contains("Japanese")').text('Português');
		$('.goog-te-combo option:contains("Polish")').text('Polskie');
		$('.goog-te-combo option:contains("Portuguese")').text('Português');
		// $('.goog-te-combo option:contains("Russian")').text('русский');
		$('.goog-te-combo option:contains("Spanish")').text('Español');
	}

	$(document).ready(function() {

		$("body").on("change", "#google_translate_element select", function(e) {

			changeLanguageLabel();

			setTimeout(function() {
				changeLanguageLabel();
			}, 100);
			setTimeout(function() {
				changeLanguageLabel();
			}, 500);
			setTimeout(function() {
				changeLanguageLabel();
			}, 2000);
			setTimeout(function() {
				changeLanguageLabel();
			}, 2500);
		});


		setTimeout(function() {
			changeLanguageLabel();
		}, 100);
		setTimeout(function() {
			changeLanguageLabel();
		}, 500);
		setTimeout(function() {
			changeLanguageLabel();
		}, 2000);

		setTimeout(function() {
			changeLanguageLabel();
		}, 2500);
	});


	$(document).ready(function() {
		$("#menu_toggle").on("click", function() {
			$(".hidemenutext").toggle();
			$("#module_modal").css("margin-left", "0px");
			$(".quick_module-img").css("width", "22px !important");
		});
		// $("#menu_toggle").trigger("click");
	});

	function changeCurrency() {
		$('#currency_dashboard_form').submit();
	}
</script>
<?php //$this->renderPartial('/orders/create');
?>
<?php //$this->renderPartial('/expenses/create_expense');
?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	function hideGoogleTranslateBanner() {
		var banner = document.querySelector('.goog-te-banner');
		if (banner) {
			banner.style.display = 'none';
		}
	}

	// Call the hideGoogleTranslateBanner function when the page is loaded
	window.addEventListener('load', hideGoogleTranslateBanner);

	$(function() {
		$('.ui-datepicker').addClass('notranslate');
		$('.goog-te-banner-frame').hide();
	});
</script>

<!-- Start Google Translater -->
<style>
	.VIpgJd-ZVi9od-aZ2wEe-wOHMyf {
		display: none !important;
	}

	body {
		top: 0px !important;
	}

	.nav-sm ul.nav.side-menu .quick_module_modal img {
		float: none;
		margin-left: 12px;
	}

	.goog-te-combo,
	.goog-te-banner *,
	.goog-te-ftab *,
	.goog-te-menu *,
	.goog-te-menu2 *,
	.goog-te-balloon * {
		font-family: Poppins !important;
		font-size: 10px;
	}

	.goog-te-gadget .goog-te-combo {
		color: #000 !important;
		border: none;
		/* border-bottom: 1px solid #eee;*/
		padding: 5px 5px 9px 5px;
		-webkit-box-shadow: 0 5px 15px 5px rgba(80, 102, 224, 0.08);
		box-shadow: 0 5px 15px 5px rgba(80, 102, 224, 0.08);
		border-radius: 30px;
	}

	.goog-tooltip {
		display: none !important;
	}

	.goog-tooltip:hover {
		display: none !important;
	}

	.goog-text-highlight {
		background-color: transparent !important;
		border: none !important;
		box-shadow: none !important;
	}

	.tooltip-inner {
		min-width: auto !important;
		color: #ffffff;
		background: black;
		padding: 15px 30px;
		font-weight: 100;
		box-shadow: 0px 8px 0px -8px #808080;
	}

	.tooltip .tooltip-arrow {
		border-top: 5px solid #eee !important;
	}

	.tooltip-inner {
		font-size: 10px !important;
	}

	#user_prfile_img {
		margin-top: 6px;
		clear: both;
		display: inline-block;
	}

	/*#user_prfile_img>span,i{color: #fff;}*/
	#user_prfile_img:focus,
	.user_prfile_img:hover {
		padding-left: 15px;
		clear: both;
		margin-top: 6px;
		display: inline-block;
	}

	body {
		background: #ebecf6 !important;
	}


	/*ul.child_menu{display: block; max-height: 100px; overflow-y: auto;}*/
</style>

<!--  END Google Translater -->
<?php //setcookie('googtrans', '/en/'.$languageCode);//Yii::app()->user->setFlash('success','');
?>
<!-- Custom domain css only  -->