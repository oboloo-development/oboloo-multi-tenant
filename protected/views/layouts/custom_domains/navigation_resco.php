<style type="text/css">
.hidemenutext{display: none !important;}
</style>
<script type="text/javascript" src="<?php echo AppUrl::jsUrl('../css/build/js/saving_document.js'); ?>"></script> 
<div id="sidebar-menu"
	class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<ul class="nav side-menu">

			<?php $homeTip = MessageManager::tooltipsName('Home Page'); ?>
			<li <?php if ($this->current_option == 'home') echo ' class="active" '; ?>><a href="<?php echo AppUrl::bicesUrl('home'); ?>">
				<i class="fa fa-home side_tool" <?php echo $homeTip; ?> ></i> <span class="hidemenutext">Home Page</span></a></li>

			<?php $eSourcingTip = MessageManager::tooltipsName('eSourcing'); ?>
			<li <?php if ($this->current_option == 'quotes_list' || $this->current_option =='quotes_quick_evaluation') echo ' class="active " '; ?>>
				<a class="sourcing-tour ">
					<i class="fa fa-group side_tool" <?php echo $eSourcingTip; ?>></i> 
				<span class="hidemenutext">Sourcing 
				<span class="fa fa-chevron-down"></span></span></a>
				<ul class="nav child_menu">
					<li <?php if ($this->current_option == 'quotes_list') echo ' class="active" '; ?>><a href="<?php echo AppUrl::bicesUrl('quotes/list'); ?>">eSourcing</a></li>
					<li <?php if ($this->current_option == 'mySourcingActivities') echo ' class="active" '; ?>>
						<a href="<?php echo AppUrl::bicesUrl('quotes/mySourcingActivities'); ?>">My Sourcing Activities</a></li>
					<li <?php if ($this->current_option == 'myScoring') echo ' class="active" '; ?>>
						<a href="<?php echo AppUrl::bicesUrl('quotes/myScoring'); ?>">My Scoring</a></li>
					<li <?php if ($this->current_option == 'customformbuilder') echo ' class="active" '; ?>>
					<a href="<?php echo AppUrl::bicesUrl('customformbuilder/list'); ?>">Sourcing Questionnaires</a></li>
					<!-- <li <?php //if ($this->current_option == 'quotes_quick_evaluation') echo ' class="active" '; ?>>
					<a href="<?php //echo AppUrl::bicesUrl('quick-sourcing-evaluation'); ?>">Quick Evaluation</a></li></li> -->
				</ul>
			</li> 
				<div class="clearfix"></div>
			<?php if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>

			<?php $contractTip = MessageManager::tooltipsName('Contract Management'); ?>
			<li <?php if ($this->current_option == 'contracts_list') echo ' class="active " '; ?>><a <?php /*if ($this->current_option == 'contracts_list') echo ' class="current_option" ';*/ ?> href="<?php echo AppUrl::bicesUrl('contracts/list'); ?>" class="contractMenu-tour"><i class="fa fa-handshake-o side_tool" <?php echo $contractTip; ?>></i><span class="hidemenutext">Contract <span class="hidden-xs">Management</span></span></a></li>
			

			<?php $vendorTip = MessageManager::tooltipsName('Supplier Management'); ?>
			<li>
				<a class="savingMenu-tour">
					<i class="fa fa-user side_tool" <?php echo $vendorTip; ?>></i>
					<span class="hidemenutext" >Supplier<span class="hidden-xs"> Management
					</span> <span class="fa fa-chevron-down"></span></span>
				</a>
			<ul class="nav child_menu">
			 <li <?php if ($this->current_option == 'vendors_list') echo ' class="active" '; ?>>
			 	<a <?php //if ($this->current_option == 'vendors_list') echo ' class="current_option" '; ?> href="<?php echo AppUrl::bicesUrl('vendors/list'); ?>">Supplier Management </a>
			 </li>
			 <li <?php if ($this->current_option == 'review_list') echo ' class="active" '; ?> class="hidden-xs"><a  href="<?php echo AppUrl::bicesUrl('vendors/review'); ?>">Supplier Review </a>
			 </li>
			 <li <?php if ($this->current_option == 'vendor_scorecard_list') echo ' class="active" '; ?> class="hidden-xs"><a  href="<?php echo AppUrl::bicesUrl('vendorScoreCard/list'); ?>">Supplier Scorecard</a></li>
			</ul>
		   </li>

			<?php $savingTip = MessageManager::tooltipsName('Savings Management'); ?>
			<li <?php if ($this->current_option == 'savings_list' || $this->current_option == 'archive_list') echo ' class="active" '; ?>>
				<a class="savingMenu-tour">
					<i class="fa fa-tags side_tool" <?php echo $savingTip; ?>></i>
					<span class="hidemenutext" >Savings<span class="hidden-xs"> Management
					</span> <span class="fa fa-chevron-down"></span></span>
				</a>
			<ul class="nav child_menu">
			 <li <?php if ($this->current_option == 'savings_list') echo ' class="active" '; ?>><a <?php //if ($this->current_option == 'vendors_list') echo ' class="current_option" '; ?> href="<?php echo AppUrl::bicesUrl('savings/list'); ?>">Savings Management</a>
			 </li>
			 <li <?php if ($this->current_option == 'report_build') echo ' class="active" '; ?> class="hidden-xs"><a  href="<?php echo AppUrl::bicesUrl('reports/pivotSaving'); ?>">Report Builder </a>
			 </li>
			</ul>
		   </li>

			<?php } ?>
			<?php // $defaultValueTip = MessageManager::tooltipsName('Default Values'); ?>
			<?php 
			/* if(Yii::app()->session['user_type']==4){?>
				<li <?php if ($this->current_option == 'default_value') echo ' class="active" '; ?> class="hidden-xs"><a
				<?php //if ($this->current_option == 'settings') echo ' class="current_option" '; ?>
				href="<?php echo AppUrl::bicesUrl('app/defaultValue'); ?>" class="defaultMenu-tour"><i class="fa fa-pie-chart" <?php echo $defaultValueTip ?>></i><span class="hidemenutext">Default Values</span></a></li>
				<?php }
			 */ ?>	

		<?php 
			$user_type = Yii::app()->session['user_type'];
				if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>
			<!-- <li <?php if ($this->current_option == 'dashboard') echo ' class="active" '; ?>><a
				<?php  //if ($this->current_option == 'dashboard') echo ' class="current_option" ';?>
				href="<?php echo AppUrl::bicesUrl('dashboard'); ?>"><i
					class="fa fa-bar-chart"></i> Spend Analysis</a></li> -->
<!-- 
			<li <?php if ($this->current_option == 'report_build') echo ' class="active" '; ?>><a <?php //if ($this->current_option == 'report_build') echo ' class="current_option" '; ?> href="<?php echo AppUrl::bicesUrl('reports'); ?>"><i class="fa fa-file-text-o"></i> Summary </a>
			</li>	 -->
			<?php } ?>
			<!-- <li><a><i class="fa fa-send"></i> Requests <span
					class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="<?php echo AppUrl::bicesUrl('orders/edit'); ?>"
						<?php if ($this->current_option == 'orders_edit') echo ' class="current_option" '; ?>
						>Goods and Services</a></li>

					<li><a href="<?php echo AppUrl::bicesUrl('expenses/edit'); ?>"
						<?php if ($this->current_option == 'expenses_edit') echo ' class="current_option" '; ?>
						>Travel
							&amp; Expenses</a></li>
				</ul></li> -->
			<!-- <li><a><i class="fa fa-shopping-cart"></i> Orders <span
					class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a
						<?php if ($this->current_option == 'orders_list') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('orders/list'); ?>">Goods and Services</a></li>
					<li><a
						<?php if ($this->current_option == 'expenses_list') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('expenses/list'); ?>">Travel
							&amp; Expenses</a></li>
					<li><a
						<?php if ($this->current_option == 'routing') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('routing'); ?>">Approvals</a></li>
					<li><a
						<?php if ($this->current_option == 'purchase_orders') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('purchaseOrders/list'); ?>">Purchase
							Orders</a></li>
					<li><a
						<?php if ($this->current_option == 'invoices_list') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('invoices/list'); ?>">Invoices</a></li>
				</ul></li> -->
			<?php if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>
				
           <!-- <li><a><i class="fa fa-shopping-basket"></i>
					Internal Catalogue <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">

					<li><a
						<?php if ($this->current_option == 'products_list') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('products/list'); ?>">All
							Products</a></li>
							
					<li><a
						<?php if ($this->current_option == 'products_edit') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('products/edit/0'); ?>">Add
							New Product</a></li>
	
				</ul></li>
-->
      		
		<!-- <li><a <?php if ($this->current_option == 'projects') echo ' class="current_option" '; ?> href="<?php echo AppUrl::bicesUrl('projects/list'); ?>"><i class="fa fa-tasks"></i> Project Management</a></li> -->

		  <?php if(in_array($user_type,array(3,4))){?>
           <!-- <li><a><i class="fa fa-money"></i> Budget
					Management <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a
						<?php if ($this->current_option == 'budget_list') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('budget/list'); ?>">Budgets</a></li>
				</ul>
		</li> -->
		<?php } ?>
			
			
                 <?php if(in_array($user_type,array(4)) && FunctionManager::checkEnvironment(true)){?>
         <?php $settingTip = MessageManager::tooltipsName('Settings'); ?>
			<li <?php if ($this->current_option == 'settings') echo ' class="active" '; ?>><a class="configurations-tour"><i class="fa fa-gears" <?php echo 
			$settingTip; ?>></i><span class="hidemenutext"> Settings <span
					class="fa fa-chevron-down"></span></span></a>
				<ul class="nav child_menu">

					<?php if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>
						<li <?php if ($this->current_option == 'company_details') echo ' class="active" '; ?>><a
						<?php //if ($this->current_option == 'company_details') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/companyDetails'); ?>">Company
							Settings</a></li>
              <?php } ?>
           	  
					<?php if (isset(Yii::app()->session['user_type']) && (Yii::app()->session['user_type'] == 1 || Yii::app()->session['user_type'] == 3 || Yii::app()->session['user_type'] == 4)) { ?>

			        <li <?php if ($this->current_option == 'locations') echo ' class="active" '; ?>><a
					  href="<?php echo AppUrl::bicesUrl('locations/list'); ?>">
						  Locations
						</a>
					</li>
					<li <?php if ($this->current_option == 'departments') echo ' class="active" '; ?>>
						<a href="<?php echo AppUrl::bicesUrl('departments/list'); ?>">
						Departments
					  	</a>
					 </li>
					<li <?php if ($this->current_option == 'users') echo ' class="active" '; ?>><a
						<?php //if ($this->current_option == 'users') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('users/list'); ?>">Users</a></li>
					<!-- <li><a
						<?php if ($this->current_option == 'approvals') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('approvals/list'); ?>">Approval
							Routing</a></li> -->
					<li <?php if ($this->current_option == 'currency_rates') echo ' class="active" '; ?>><a
						<?php //if ($this->current_option == 'currency_rates') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/currencyRates'); ?>">Currency</a></li>
					<!-- <li><a href="<?php echo AppUrl::bicesUrl('budget/list'); ?>">Budgets</a></li> -->
					<li <?php if ($this->current_option == 'settings') echo ' class="active" '; ?> class="hidden-xs"><a
						<?php //if ($this->current_option == 'settings') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/settings'); ?>">Configurations</a></li>

					<!-- <li <?php if ($this->current_option == 'settings') echo ' class="active" '; ?>><a
						<?php //if ($this->current_option == 'settings') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/defaultValue'); ?>">Default Values</a></li> -->
			       <!--  <li <?php if ($this->current_option == 'import_templates') echo ' class="active" '; ?>><a <?php if ($this->current_option == 'import_templates') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/importTemplates'); ?>">Data Templates</a></li> -->
         	          <?php if (true) { ?>
                 	   <li <?php if ($this->current_option == 'import_admin') echo ' class="active" '; ?> class="hidden-xs"><a
						<?php //if ($this->current_option == 'import_admin') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/import'); ?>">Import Data</a></li>
					<li <?php if ($this->current_option == 'settings') echo ' class="active" '; ?>><a
						<?php //if ($this->current_option == 'settings') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/accountManagement'); ?>">Account Management</a></li>
					<!-- <li <?php //if ($this->current_option == 'settings') echo ' class="active" '; ?>><a
						<?php //if ($this->current_option == 'settings') echo ' class="current_option" '; ?>
						href="<?php //echo AppUrl::bicesUrl('app/environment'); ?>">Environment</a></li> -->

                 <?php } ?> 
              <?php } ?>
            </ul></li>
	<?php  } } ?>

		<?php  $showModel = '';
		if(strpos(Yii::app()->getBaseUrl(true),"sandbox")){
			 $showModel = '#sandbox_help_sidebar_open_module_modal';
		}else{
			 $showModel ='#module_modal';
		} ?>
		
		<?php 
		/* <li <?php if ($this->current_option == 'profile') echo ' class="active" '; ?> class="hidden-xs">
		 <a href="#" class="quick_module_modal information-mainMenu"  data-toggle="modal" data-target="<?php echo $showModel; ?>" style="">
		<?php $infomationTip = MessageManager::tooltipsName('Help'); ?>
		<i class="fa fa-question-circle" aria-hidden="true"
		 <?php echo $infomationTip; ?> style="font-size: 25px !important;"></i><span class="hidemenutext" style="margin-left: 7px; ">Help </span></a></li> --> */ 
		?>
		
		<!-- 
			<li <?php if ($this->current_option == 'profile') echo ' class="active" '; ?>><br /><a
						<?php //if ($this->current_option == 'company_details') echo ' class="current_option" '; ?>
						href="<?php echo AppUrl::bicesUrl('app/profile'); ?>"><i class="fa fa-user"></i>Profile</a></li>
			<li><a href="https://oboloo.com/support-hub" target="_blank"><i
					class="fa fa-certificate"></i> Training & Support</a></li>
			<li><a href="https://oboloo.com/my-tickets"  target="_blank"><i
					class="fa fa-ticket"></i> Raise a Ticket</a></li>
			<li><a href="https://oboloo.com/feedback"  target="_blank"><i
					class="fa fa-support"></i> Feedback</a></li>
			<li><a href="<?php echo AppUrl::bicesUrl('logout'); ?>"><i
					class="fa fa-sign-out"></i> Log Out</a></li> -->
		</ul>
	</div>
</div>
<!-- /sidebar menu -->
			