<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="language" content="en" />
			<meta http-equiv="Pragma" content="no-cache">
			
				<title>
          <?php  echo Yii::app()->params['title']; ?>
          <?php if (!empty($this->pageTitle)) echo ' - ' . CHtml::encode($this->pageTitle); ?>
    </title>
    
	            <link rel="shortcut icon" type="image/png" href="<?php echo AppUrl::bicesUrl('images/icons/favicon.png'); ?>"/>

				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('bootstrap/dist/css/bootstrap.min.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
				
				
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('build/css/custom.css'); ?>" />
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('../js/validator/fv.css'); ?>" />
				
				<link rel="stylesheet" type="text/css"
					href="<?php echo AppUrl::cssUrl('fileinput.css'); ?>" />
	            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	            <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	  
	        <script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('jquery/dist/jquery.min.js'); ?>"></script>

				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
				
				<script type="text/javascript"
					src="<?php echo AppUrl::jsUrl('../css/build/js/custom.js'); ?>"></script>

	<style type="text/css">							
	.dt-button {margin-left: 10px !important;}
	body {background-color: #fff !important;}
	</style>

	<script type="text/javascript">
		var BICES = window.BICES ||
		{
			foo: function(){}
		};
		BICES.Options = { baseurl: '<?php echo Yii::app()->request->getBaseUrl(true); ?>' };
	</script>

	<?php 
	
	$user = new User ();
	if ($user->isLoggedIn ()) {?>
		
<?php }
 ?>

</head>

<body class="login" style="zoom: 85% !important; background-color: #fff !important;">
	<?php echo $content; ?>
</body>
	

</html>

