<!-- This is shared component for all modules 
@basheer alam 12/02/2023 -->
<div class="clearfix"> <br /> </div>
	<div class="form-group">
		<div class="col-md-4 col-sm-4 col-xs-8">
			<h4><?= $logTitle; ?> Log</h4>
		</div>
	</div>
	<div class="clearfix"> <br /> </div>
	<div class="col-md-7 col-sm-7 col-xs-12 table-responsive">
		<table class="table">
			<thead><tr><th class="col-lg-2 col-xs-12">User</th><th class="col-lg-2 col-xs-12">Date</th><th>Comments</th></tr></thead>
			<?php if(count($logHistory)) {
				foreach ($logHistory as $log) {?>
					<tr>
					 <td><?php echo $log['user']; ?></td>
					 <td><?php echo date(FunctionManager::dateFormat(), strtotime($log['created_datetime'])); ?></td>
					 <td><?php echo $log['comment']; ?></td>
					</tr>
					<?php }
			}else{ ?>
				<tr><td colspan="4">No Record Found</td></tr>
			<?php } ?>
		</table>
	</div>