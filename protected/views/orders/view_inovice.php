
<?php if(!empty($modal)){
  $classUpload = 'col-md-12 col-sm-8 col-xs-6';
  $classUploadButton = 'col-md-2 col-sm-2 col-xs-2';
  $classUploadSecond = 'col-md-10 col-sm-6 col-xs-4';
}else{
  $classUpload = 'col-md-6 col-sm-6 col-xs-6';
  $classUploadSecond = 'col-md-5 col-sm-5 col-xs-4';
  $classUploadButton = 'col-md-1 col-sm-1 col-xs-2';
}?>
<div class="modal fade" id="view_inovice" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content" style="width: 110%;">       
    <form id="order_invoice_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('orders/editInvoices'); ?>">
 

    <?php if (!empty($order_invoice)) { ?>

          <div class="form-group">
              <div class="col-md-12 col-sm-8 col-xs-12">
                  <h3><br /><br />Invoices</h3>
              </div>
          </div>
          
            <div class="col-md-12 col-sm-8 col-xs-12">
          <table class="table">
             <thead>
               <tr>
                <th class="text-left">Action</th>
                <th class="text-center">Attachment</th>
                <th class="text-center">Invoice NO.</th>
                <th class="text-center">Approve Invoice</th>
                <th class="text-center">Invoice type (external/internal)</th>
               </tr>

             </thead>
             <tbody>
           <?php  foreach ($order_invoice as $uploaded_file) {
            $invoice_idx = $uploaded_file['id'];
            $showFileName = substr($uploaded_file['file_name'], strpos($uploaded_file['file_name'], "_") + 1); ?>
            <tr id="existing_invoice_id_<?php echo $invoice_idx; ?>">
            <td class="text-left">
              <a href="<?php echo Yii::app()->createAbsoluteUrl('orders/invoiceDownload',array('invoice-id'=>$invoice_idx,'order-id'=>$order_id));  ?>" target="order_invoice"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
              </a> &nbsp;
              <a id="delete_order_invoice_link_<?php echo $invoice_idx; ?>" style="cursor: pointer;" 
                onclick="deleteOrderInvoice(<?php echo $invoice_idx; ?>);">
               <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
              </a> 
            </td>
            <td class="text-center"><?php echo $showFileName; ?></td>
            <td class="text-center"><input type="text" class="form-control" name="invoice[<?php echo $invoice_idx;?>]" value="<?php echo $uploaded_file['invoice_number'];?>" style="max-width: 105px;" /></td>
            <td class="text-center"><label>Approve</label>
              <input type="checkbox" name="approve_invoice_only[<?php echo $invoice_idx;?>]"  <?php if($uploaded_file['status']==2){ echo "checked='checked' disabled='disabled'";}?>>
              <?php if($uploaded_file['status']==2){?>
                <input type="hidden" name="approve_invoice_only[<?php echo $invoice_idx;?>]" checked="checked" value="on" />
              <?php } ?>
            </td>
            <td class="text-center"><?php echo ucwords($uploaded_file['type']); ?></td>
           <!--  <td>
                <?php 
                $status = StatusMgr::InvStatus($uploaded_file['status']);
                echo $status; ?>
            </td> -->
            </tr>
          <?php $invoice_idx += 1; if (empty($invoice_found)) $invoice_found = $uploaded_invoice; } ?>
          <tr>
            <td colspan="4" align="right">
            <!--   <input type="submit" name="approve_invoice_only" class="btn btn-sm btn-success" value="Approve" style="margin-top: 27px;" />
              <input type="submit" name="update_invoice_only" value="Update Only"  class="btn btn-sm btn-info" style="margin-top: 27px;" /> -->
              <input type="submit" name="approve_update_invoice" value="Save" class="btn btn-sm btn-success" style="margin-top: 27px;" /></td>
          </tr>
          </tbody>
          </table>

          </div>
               
          <div class="clearfix"> <br /> </div>
    
        <?php } ?>

    <div class="clearfix"> </div>
    <input type="hidden" name="order_id_for_invoices" id="order_id_for_invoices" value="<?php if (isset($order_id)) echo $order_id; ?>" />
     <input type="hidden" name="vendor_id_for_invoices" id="vendor_id_for_invoices" value="<?php if (isset($vendor_id)) echo $vendor_id; ?>" />
    <input type="hidden" name="form_submitted_for_invoices" id="form_submitted_for_invoices" value="1" />
  </form><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div></div></div></div>

<style type="text/css">
  input[type=checkbox] {
    bottom: -3px !important;
    margin: 2px 8px 0px 8px  !important;
  }
</style>
  