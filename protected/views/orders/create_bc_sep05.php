<div class="modal fade" id="create_order" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog"><div class="modal-content">
<?php
$loggedUserID = Yii::app()->session['user_id'];
$invoice_uploadable_statuses = array('Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier', 'Received', 'Invoice Received', 'Paid');



$userPer = new User;

$record_user_id = 0;
if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];
if (isset($order['user_id']) && !empty($order['user_id'])) $record_user_id = $order['user_id'];        
    $applicable_order_statuses = array('Pending','Closed', 'Declined', 'Ordered - PO Not Sent To Supplier', 'Received', 'Submitted', 'Paid', 'Cancelled', 'Invoice Received'/*, 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier'*/);
if(isset(Yii::app()->session['user_id']) && !empty($order['location_id']) && !empty($order['department_id'])){
  $appOrder = $userPer->checkPermission(Yii::app()->session['user_id'],$order['location_id'],$order['department_id'],"approve_order");
}
    if(isset($appOrder) && $appOrder["approve_order"]=="yes" && in_array(Yii::app()->session['user_type'],array(1,3,4))){
      if (isset($order['order_status']) && strtolower($order['order_status'])=="pending"){
        $applicable_order_statuses = array($order['order_status'], 'Submitted');
      }else if (isset($order['order_status']) && strtolower($order['order_status'])=="submitted"){
        $applicable_order_statuses = array($order['order_status'],'Approved');
      }else if (isset($order['order_status']) && strtolower($order['order_status'])=="submitted"){
        //$applicable_order_statuses = array($order['order_status'],'Approved');
        $applicable_order_statuses = array($order['order_status'], 'Pending', 'Approved', 'Closed', 'Declined', 'Ordered - PO Not Sent To Supplier', 'Received', 'Submitted', 'Paid', 'Cancelled', 'Invoice Received'/*, 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier'*/);
      }else if (isset($order['order_status']) && strtolower($order['order_status'])=="approved"){
         $applicable_order_statuses = array($order['order_status'],'Closed', 'Declined',/* 'Ordered - PO Not Sent To Supplier', */'Received', 'Submitted', 'Paid', 'Cancelled', 'Invoice Received'/*, 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier'*/);
      }else if ($order['order_status'] == 'Ordered - PO Sent To Supplier' || $order['order_status'] == 'Ordered - PO Not Sent To Supplier'){
          $applicable_order_statuses = array($order['order_status'], 'Received');
      }else if ($order['order_status'] == 'Received'){
          $applicable_order_statuses = array($order['order_status'], 'Invoice Received');
      }else if ($order['order_status'] == 'Invoice Received'){
          $applicable_order_statuses = array($order['order_status'], 'Paid');
      }else if ( strtolower($order['order_status']) == 'paid'){
          $applicable_order_statuses = array($order['order_status'], 'Closed', 'Declined','Cancelled');
      }
      else $applicable_order_statuses = array('Pending', 'Submitted','Approved');
    }else 
    if(!isset($order['order_status']) || $order['order_status'] == 'Pending' || $order['order_status'] == 'More Info Needed')
      {
      if (isset($order['order_status']))
          $applicable_order_statuses = array($order['order_status'], 'Submitted');
        else $applicable_order_statuses = array('Pending', 'Submitted');
      }else if (isset($order['order_status']) && strtolower($order['order_status'])=="approved"){
         $applicable_order_statuses = array($order['order_status'],'Closed', 'Declined', /*'Ordered - PO Not Sent To Supplier', */'Received', 'Submitted', 'Paid', 'Cancelled', 'Invoice Received'/*, 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier'*/);
      }else if ($order['order_status'] == 'Ordered - PO Sent To Supplier' || $order['order_status'] == 'Ordered - PO Not Sent To Supplier'){
          $applicable_order_statuses = array($order['order_status'], 'Received');
      }else if ($order['order_status'] == 'Received'){
          $applicable_order_statuses = array($order['order_status'], 'Invoice Received');
      }else if ($order['order_status'] == 'Invoice Received'){
          $applicable_order_statuses = array($order['order_status'], 'Paid');
      }else if ( strtolower($order['order_status']) == 'paid'){
          $applicable_order_statuses = array($order['order_status'], 'Closed', 'Declined','Cancelled');
      }
      else{
        $applicable_order_statuses = array($order['order_status']);
      }

      $applicable_order_statuses = array('Received', 'Invoice Received', 'Paid','Closed');
    
        if(!empty($order['order_status'])){
          $class="col-md-3 col-sm-3 col-xs-6";
        }else{
          $class="col-md-6 col-sm-6 col-xs-12";
        }

        // START : Invoices
        // integer starts at 0 before counting
        $vendor_id = $order['vendor_id'];
        $your_existing_files = array();
        $upload_dir = 'uploads/orders/';
        if ($order_id)
        {
            if (!is_dir('uploads/orders')) mkdir('uploads/orders');
            if (!is_dir('uploads/orders/' . $order_id))
                mkdir('uploads/orders/' . $order_id);
            if (!is_dir('uploads/orders/' . $order_id . '/' . $vendor_id))
                mkdir('uploads/orders/' . $order_id . '/' . $vendor_id);
            $upload_dir = 'uploads/orders/' . $order_id . '/' . $vendor_id;
            if ($handle = opendir($upload_dir)) {
                while (($uploaded_file = readdir($handle)) !== false){
                    if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file))
                        $your_existing_files[] = $uploaded_file;
                }
            }
        }
        // END : Invoices
?>


<div role="main">
 <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3><?php echo 'Add Order';?></h3></div>
 <div class="row-fluid tile_count">
 <div class="col-md-12">
  <?php if(Yii::app()->user->hasFlash('error')):?>
      <div class="alert alert-danger">
          <?php echo Yii::app()->user->getFlash('error'); ?>
      </div>
  <?php endif; ?></div>
	                             
<div class="tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php
      $orderObj = new Order();
      $location_id = 0;
      $department_id = 0;
      $approve = $orderObj->approveStatus($location_id,$department_id);?>

    </div>
    <form id="order_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('orders/edit'); ?>">
      <input type="hidden" name="user_id" value="<?php echo !empty($order['user_id'])?$order['user_id']:Yii::app()->session['user_id'];?>">
    
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Location <span style="color: #a94442;">*</span></label>
			  <select name="location_id" id="location_id" class="form-control" onchange="loadDepartmentsForSingleLocation(0);">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($order['location_id']) && $location['location_id'] == $order['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Department <span style="color: #a94442;">*</span></label>
              <select name="department_id" id="department_id" class="form-control"  onchange="checkLocationDepartmentPermission();">
                  <option value="">Select Department</option>
              </select>
          </div>
      </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 valid">
                <label class="control-label">Project</label>
                <select name="project_id" id="project_id" class="form-control">
                    <option value="">Select Project</option>
                    <?php foreach ($projects as $project) { ?>
                        <option value="<?php echo $project['project_id']; ?>"
                            <?php if (isset($order['project_id']) && $project['project_id'] == $order['project_id']) echo ' selected="SELECTED" '; ?>>
                            <?php echo $project['project_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 valid">
                <label class="control-label">Spend Type</label>
                <select name="spend_type" id="spend_type" class="form-control">
                    <option value="">Select Spend Type</option>
                    <option  <?php if (isset($order['spend_type']) && $order['spend_type'] == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                    <option  <?php if (isset($order['spend_type']) && $order['spend_type'] == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
                </select>
            </div>
        </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Currency Rate <span style="color: #a94442;">*</span></label>
              <select name="currency_id" id="currency_id" class="form-control" onchange="loadCurrencyRate();" required="required">
                <option value="">Select Currency</option>
                  <?php foreach ($currency_rate as $rate) { ?>
                      <option value="<?php echo $rate['id']; ?>"
                          <?php if (isset($order['currency_id']) && $rate['id'] == $order['currency_id']) echo ' selected="SELECTED" '; ?>>
                      <?php echo $rate['currency']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 valid">
              <label class="control-label">Currency Rate</label>
              <input readonly type="text" class="form-control" name="currency_rate" id="currency_rate"
                  <?php if (isset($order['currency_rate']) && !empty($order['currency_rate'])) { echo 'value="' . $order['currency_rate'] . '"'; } else { echo 'value="1"'; } ?> >

          </div>

      </div>

      <div class="form-group">

          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Order Date <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control has-feedback-left" name="order_date" id="order_date"
                    <?php if (isset($order['order_date']) && !empty($order['order_date'])) echo 'value="' . date("d/m/Y", strtotime($order['order_date'])) . '"'; else echo 'placeholder="Order Date"'; ?> >
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
	
       
        <div class="<?php echo $class;?>">
              <label class="control-label">User Name</label>
              <select class="form-control" disabled="disabled">
                  <?php foreach ($users as $user) { ?>
                      <option value="<?php echo $user['user_id']; ?>"
                                <?php if ($record_user_id == $user['user_id']) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $user['full_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
          <input type="hidden"  name="order_status" id="order_status" />
      </div>
      <div class="clearfix"> </div>

      <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <label class="control-label">Order Description/Notes</label>
              <textarea class="form-control" name="description" id="description" rows="4" <?php if (!isset($order['description']) || empty($order['description'])) echo 'placeholder="Order description/notes"'; ?>><?php if (isset($order['description']) && !empty($order['description'])) echo $order['description']; ?></textarea>
          </div>
      </div>
      <div class="clearfix"> <br /> </div>

      <?php
        $existing_items_found = false;
        if (isset($order_details) && is_array($order_details) && count($order_details))
            $existing_items_found = true;
      ?>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>
                  Order Items
        
              </h4>
          </div>
      </div>

      <?php
//        echo "<pre>";
//        print_r($order_details);
//        echo "</pre>";die;
           $total_price = 0;
       
            $tax_amount = 0;
            $tax_rate = 00;
           

      ?>

      <div id="here_is_where_you_add_new_items_to_this_order"
            <?php if ($existing_items_found) echo ' style="display: none;" '; ?>>
      <fieldset id="additional-field-model">
          <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;">
                  <label class="control-label">Product Type</label>
                  <select class="form-control product_type" name="product_type[]">
                      <option value="Product">Product</option>
                      <option value="Bundle">Bundle</option>
                  </select>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
                  <label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
                  <input required type="text" class="form-control has-feedback-left price_calc" value="" name="product_name[]" />
                  <input type="hidden" name="product_id[]" class="product_id" value="0" />
                  <input type="hidden" name="product_currency_id[]" class="product_currency_id" value="0" />
                  <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
                  <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                  <input required type="text" class="form-control has-feedback-left" name="vendor_name[]" id="vendor_name" />
                  <input type="hidden" name="vendor_id[]" class="vendor_id" value="0"/>
                  <span class="fa fa-user-circle-o form-control-feedback left" aria-hidden="true"></span>
                  <a style="text-decoration: underline; cursor: pointer;" onclick="$('#add_vendor_modal').modal('show');">
                      Click here</a> to add new vendor that is not the database.
              </div>

              <div class="clearfix"> <br /><br /> </div>

               <div class="col-md-4 col-sm-4 col-xs-6">
                  <label class="control-label"> Product Currency</label>
                  <input style="text-align: right;" type="text" class="unit_currency_symbol form-control" value="" name="unit_currency_symbol[]" placeholder="Currency" readonly="readonly" />
                  
              </div>
              <div class="col-md-4 col-sm-4 col-xs-6">
                  <label class="control-label">Original Product Price</label>
                  <input style="text-align: right;" type="text" class="unit_price form-control input_price_by_user" value="" name="unit_price[]" placeholder="Price" />
                  <input type="hidden" class="unit_price_hidden" name="unit_price_hidden[]" value="" />
                  <input type="hidden" class="calc_unit_price_hidden" name="calc_unit_price[]" value="" />
                  <input type="hidden" class="GBP_price_hidden" name="GBP_price_hidden[]" value="" />
              </div>

              <div class="col-md-4 col-sm-4 col-xs-6">
                  <label class="control-label">Total Product Price</label>
                  <input style="text-align: right;" type="text" class="calc_unit_price_display form-control" value="" name="calc_unit_price_display[]" placeholder="Price" readonly="readonly" />
              </div>
              <div class="clearfix"></div>
              <div class=" col-md-4 col-sm-4 col-xs-6 valid">
                  <label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
                  <input required style="text-align: right;" type="text" class="product_quantity form-control price_calc" name="quantity[]" placeholder="Quantity" />
              </div>

              <div class="col-md-2 col-sm-2 col-xs-6">
                  <label class="control-label">Tax %</label>
                  <input style="text-align: right;" type="text" class="product_tax form-control input_tax_by_user" value="" name="tax[]" placeholder="Tax" />
                   <input type="hidden" class="unit_price_hidden_tax" name="calc_tax[]" value="" />
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                  <label class="control-label">Ship
                  ping</label>
                  <input style="text-align: right;" type="text" class="product_shipping form-control input_shipping_by_user" value="" name="shipping_cost[]" placeholder="Shipping" />
              </div>
              <div class="col-md-2">
                  <a href="javascript:void(0);" class="btn btn-link remove-this-field">
                      <span class="fa fa-remove hidden-xs" style="margin-top:30px;"></span>
                  </a>
                  <a href="javascript:void(0);" class="btn btn-link create-new-field">
                      <span class="fa fa-plus hidden-xs" style="margin-top:30px;"></span>
                  </a>
              </div>
          </div>
      </fieldset>
      <div class="clearfix"> </div>
      </div>
    <?php 
    $uploadCondArr = array('pending','more info needed');
    if(empty($order_id) || (!empty($order_id) && in_array(strtolower(trim($order['order_status'])),$uploadCondArr))){?>
         <div class="form-group"><br />
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h3 style="font-size: 18px !important;">Upload Documents</h3>
                </div>
            </div>

            <div class="form-group" id="document_area_1">
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <label class="control-label">File/Document</label>
                    <input class="form-control" type="file" name="order_file_1" id="order_file_1" />
                </div>

               <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">File/Document Notes</label>
                     <textarea class="form-control" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" rows="3" /></textarea> 
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                  <br /><br />
                    <a onclick="deleteDocument(1);" title="Click here to delete this document" style="cursor: pointer;">
                        <span class="fa fa-minus fa-2x"></span>
                    </a>
                    &nbsp;
                    <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
                        <span class="fa fa-plus fa-2x"></span>
                    </a>
                </div>
                <div class="clearfix"> </div>

                <div class="col-md-9 col-sm-9 col-xs-12 text-right">
                <button id="btn_upload_1" class="btn btn-info upload-doc" onclick="uploadDocument(1,event);" style="margin-top:4%">Upload</button>
          </div>
            </div>
          <?php } ?>
            <input type="hidden" name="delete_document_flag_1" id="delete_document_flag_1" value="0" />
            <div class="clearfix" id="end_document_area_1"> <br /> </div>
        <input type="hidden" name="total_documents" id="total_documents" value="1" />
      <div class="clearfix"> <br /> </div>
      <div id="uploaded_file_list" class="col-md-6 col-sm-6 col-xs-12"></div>
        <div id="new_document_code" style="display: none;">
            <div class="form-group" id="document_area_DOCIDX">
                 <div class="col-md-3 col-sm-3 col-xs-12">
                  <label class="control-label">File/Document</label>
                    <input class="form-control" type="file" name="order_file_DOCIDX" id="order_file_DOCIDX" />
                </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">File/Document Notes</label>
                     <textarea class="form-control"  class="form-control" name="file_desc_DOCIDX" id="file_desc_DOCIDX" placeholder="File/Document Description" rows="3" ></textarea>
                  </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <a onclick="deleteDocument(DOCIDX);" title="Click here to delete this document" style="cursor: pointer;">
                        <span class="fa fa-minus fa-2x"></span>
                    </a>
                    <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
                        <span class="fa fa-plus fa-2x"></span>
                    </a>
                </div>
                <div class="clearfix"> </div>
              
                <div class="col-md-9 col-sm-9 col-xs-12 text-right">
                  <button id="btn_upload_DOCIDX" class="btn btn-info upload-doc" onclick="uploadDocument(DOCIDX,event);" style="margin-top:4%">Upload</button>
                </div>
            </div>
            <input type="hidden" name="delete_document_flag_DOCIDX" id="delete_document_flag_DOCIDX" value="0" />
            <div class="clearfix" id="end_document_area_DOCIDX"> <br /> </div>
        </div>
        <div class="clearfix"> </div>

    <div class="row">

            <input type="hidden" name="total_price" id="total_price" value="<?php echo number_format($total_price, 2); ?>" />
            <input type="hidden" name="order_id" id="order_id" value="<?php if (isset($order_id)) echo $order_id; ?>" />
            <input type="hidden" name="recurrent_number" id="recurrent_number" value="<?php if (isset($order['recurrent_number'])) echo $order['recurrent_number']; ?>" />
            <input type="hidden" name="created_by_user_id" id="created_by_user_id" value="<?php if (isset(Yii::app()->session['user_id'])) echo Yii::app()->session['user_id']; else echo '0'; ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
            <input type="hidden" name="files_only_upload" id="files_only_upload" value="0" />
            <input type="hidden" name="currency_symbol" id="currency_symbol" value="" />
    </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-8">
           <h3 style="font-size: 18px !important;">Tax Amount <span style="font-size: 70%;">(if applicable)</span></h3>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-8">
            <label class="control-label">Tax Amount</label>
            <input style="text-align: right;" type="text" class="form-control total_tax_amount"
                    id="tax_amount" name="tax_rate" placeholder="Tax Amount" readonly="readonly"
                        <?php if (isset($order_details[0]['tax']) && $order_details[0]['tax']) echo 'value="' . number_format($order_details[0]['tax'], 2) . '"';  ?> />
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-8">
        <h3 style="font-size: 18px !important;">Shipping Costs <span style="font-size: 70%;">(if applicable)</span></h3>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-8">
        <label class="control-label">Shipping Costs</label>
        <input readonly="readonly" style="text-align: right;" type="text" class="form-control total_shipping_amount price_calc" onkeyup="calculateTotalPrice();"
                id="ship_amount" name="ship_amount" placeholder="Shipping Amount"
                    <?php if (isset($order_details[0]['shipping_cost']) && $order_details[0]['shipping_cost']) echo 'value="' . number_format($order_details[0]['shipping_cost'], 2) . '"';  ?> />
      </div>
      <div class="clearfix"></div>
    </div>
</form>
</div>


<div class="form-group">
    <div class="col-md-12 col-sm-12 col-xs-12 text-center">

        <h3>Total Price  <span id="total_price_display" style="display: inline-block;"><?php
        $display_final_price = false; $final_grand_total_price = $total_price;
        $currencySymbol='';?></span></h3>
    </div>
   
    <div class="clearfix"> </div>
</div>
 
<div class="modal-footer">    
<div class="form-group">
  <div class="col-md-12 col-sm-12 col-xs-12 text-right">
    <?php
    if(empty($order_id) || $loggedUserID == $order['user_id']){?>
      <a href="javascript:void(0);" onclick="$('#order_status').val('Pending');$('#order_form').submit();"><input type="submit" class="btn btn-primary" value="Save Order and Not Submit" name="expense_status" /></a>

      <a href="javascript:void(0);" onclick="$('#order_status').val('Submitted');$('#order_form').submit();"><input type="submit" class="btn btn-success" value="Submit for Approval" name="expense_status"></button></a>
    <?php } ?>

    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
    <div class="clearfix"> <br /></div> 
</div>
</div>

  


<input type="hidden" name="user_currency_symbol" id="user_currency_symbol" value="<?php echo Yii::app()->session['user_currency_symbol']; ?>" />

</div></div>
<script type="text/javascript">
$(document).ready( function() {

	<?php if (isset($order['location_id']) && !empty($order['location_id'])) { ?>

		<?php if (isset($order['department_id']) && !empty($order['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $order['department_id']; ?>);
		<?php } else { ?>
    loadDepartmentsForSingleLocation(0);
		<?php } ?>

	<?php } ?>


	<?php 
		if (isset(Yii::app()->session['order_status_changed_id']) && Yii::app()->session['order_status_changed_id']) 
		{ 
	?>
	
			$('#alert-info').show();
			setTimeout(function() { $("#alert-info").hide(); }, 7000);
	
	<?php 
			Yii::app()->session['order_status_changed_id'] = 0;
			unset(Yii::app()->session['order_status_changed_id']);
		} 
	?>		

    $('#order_form').on('submit', function() {
        $('#order_status').removeAttr('disabled');
    });

    $('input').on('ifChanged', function(event){
        calculateTotalPrice();
    });


    $( "#order_form" ).validate( {
        rules: {
            location_id: "required",
            department_id: "required",
            vendor_name: "required",
            order_date: "required"

        },
        messages: {
            location_id: "Location is required",
            department_id: "Department is required",
            vendor_name: "Supplier Name is required",
            order_date: "Order Date is required"
        },
	    submitHandler: function(form) {
	    	var product_id = 0;
	    	$('.product_quantity').each(function() {
	    		if ($.trim($(this).val()) == "") {
			        product_id = $(this).closest('.form-group').find('.product_id').val();
			        if (product_id == '0') $(this).val('0');
	    			else $(this).val('1');
	    		}
	    	});
	    	calculateTotalPrice();
		    form.submit();
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

            //$("#department_id-error, #location_id-error, #vendor_name-error, #order_date-error").css({"color":"#a94442"});

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
			//$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error error" );
			//$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field",
        "onRemove": function() { calculateTotalPrice(); }
    });

	$('#order_date').daterangepicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});
   
//    $('#vendor_name').devbridgeAutocomplete({
//        serviceUrl: '<?php //echo AppUrl::bicesUrl('orders/getVendors'); ?>//',
//        onSelect: function(suggestion) { $('#vendor_id').val(suggestion.data); },
//        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#vendor_id').val(0); }
//    });

    createProductAutocompleteSearch();
    createVendorAutocompleteSearch();

    $('body').on('click', "input[name^='product_name']", function() {
        createProductAutocompleteSearch();
    });

    $('body').on('click', "input[name^='vendor_name']", function() {
        createVendorAutocompleteSearch();
    });

    $('.price_calc').keyup(function() { calculateTotalPrice(); });
    $('.product_tax').keyup(function() { calculateTotalTax(); });
    $('.product_shipping').keyup(function() { calculateTotalShipping(); });

    $(document).on("blur","input",function() {
     if($(this).hasClass('input_price_by_user')){
    	var my_quantity = $(this).closest('.form-group').find("input[name^='quantity']").val();
    	if (!$.isNumeric(my_quantity)) 
    	{
    		my_quantity = 1;
    		$(this).closest('.form-group').find("input[name^='quantity']").val(1);
    	}

      var my_price = $(this).val();
      var obj = $(this);
      if (!$.isNumeric(my_price)) my_price = 0;

      var product_currency_id=$(this).closest('.form-group').find("input[name^='product_currency_id']").val();
      var product_id=$(this).closest('.form-group').find("input[name^='product_id']").val();

        $.ajax({
        type: "POST", data: { product_currency_id: product_currency_id,price:my_price,product_id:product_id }, 
        dataType: "json",
        url: BICES.Options.baseurl + '/orders/changeProductPric',
        success: function(options) {
          obj.closest('.form-group').find(".calc_unit_price_hidden").val(options.GBP_price);
          obj.closest('.form-group').find(".GBP_price_hidden").val(options.GBP_price);
          obj.closest('.form-group').find(".calc_unit_price_display").val($.trim($('#currency_id option:selected').text())+" "+options.GBP_price);
          calculateTotalPrice();
        },
        error: function() { }
    });
    	
    	
    	var my_unit_price = parseFloat(my_price / my_quantity); 
    	$(this).closest('.form-group').find("input[name^='unit_price_hidden']").val(my_unit_price);

		calculateTotalPrice();
  }
    });

    $('.product_type').change(function() {
        $(this).closest('.form-group').find("input[name^='product_name']").devbridgeAutocomplete().setOptions({
            params: { product_type: $(this).val() }
        });

        $(this).closest('.form-group').find(".display_price").html("");
        $(this).closest('.form-group').find('.product_id').val(0);
        $(this).closest('.form-group').find("input[name^='product_name']").val("");
        $(this).closest('.form-group').find('.unit_price').val("");
        $(this).closest('.form-group').find('.unit_price_hidden').val(0);
        $(this).closest('.form-group').find('.calc_unit_price_hidden').val(0);
        $(this).closest('.form-group').find('.unit_currency_symbol').val("");
        $(this).closest('.form-group').find('.GBP_price_hidden').val(0);
        
        $(this).closest('.form-group').find('.product_quantity').val("");
        calculateTotalPrice();
    });
});

function loadCurrencyRate()
{
    var currency_id = $('#currency_id').val();

    $.ajax({
        type: "POST", data: { currency_id: currency_id }, 
        dataType: "json",
        url: BICES.Options.baseurl + '/orders/getCurrencyRates',
        success: function(options) {
            $('#currency_rate').val(options.rate);
            $('#currency_symbol').val(options.currency_symbol);
            calculateTotalPrice();
        },
        error: function() { $('#currency_rate').html(1); }
    });

     
}

function createProductAutocompleteSearch()
{
    var product_type = '';
    $("input[name^='product_name']").each(function () {
        product_type = $(this).closest('.form-group').find('.product_type').val();
        $(this).devbridgeAutocomplete({
            serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getProducts'); ?>',
            params: { product_type: product_type },
            onSelect: function(suggestion) {
                $(this).closest('.form-group').find('.product_id').val(suggestion.data.id);
                $(this).closest('.form-group').find('.unit_price').val(suggestion.data.price);
                $(this).closest('.form-group').find('.unit_price_hidden').val(suggestion.data.price);
                $(this).closest('.form-group').find('.display_price').html('@ ' + suggestion.data.price + ' / Each');
                $(this).closest('.form-group').find('.product_currency_id').val(suggestion.data.currency_id);
                $(this).closest('.form-group').find('.unit_currency_symbol').val(suggestion.data.currency_id_symbol);
                $(this).closest('.form-group').find('.GBP_price_hidden').val(suggestion.data.GBP_price);
                
                $(this).closest('.form-group').find('.product_quantity').val(1);

                $('.price_calc').keyup(function() { calculateTotalTax();calculateTotalPrice(); });
                $('.product_tax').keyup(function() { calculateTotalTax(); });
                $('.product_shipping').keyup(function() { calculateTotalShipping(); });
                calculateTotalPrice();
                
            }
        });
    });
}

function createVendorAutocompleteSearch()
{
    $("input[name^='vendor_name']").each(function () {
        $(this).devbridgeAutocomplete({
            serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
            onSelect: function(suggestion) {
                $(this).closest('.form-group').find('.vendor_id').val(suggestion.data);
            }
        });
    });
}

function calculateTotalTax()
{

    var price = 0;
    var product_tax = 0;
    var total_tax_price = 0;

    $('.product_tax').each(function(){

        product_tax = $(this).val();
        price = parseFloat($(this).closest('.form-group').find('.calc_unit_price_hidden').val());
        quantity = parseFloat($(this).closest('.form-group').find('.product_quantity').val());

        if ($.trim(quantity) == '' || !$.isNumeric(quantity)) quantity = 1;

        price = price*quantity;

        if ($.trim(product_tax) == '' || !$.isNumeric(product_tax)) product_tax = 0;
        total_tax_price += Number((product_tax*price)/100);

        //$(this).closest('.form-group').find('.unit_price_hidden_tax').val(Number((product_tax*price)/100));
        $(this).next('.unit_price_hidden_tax').val(Number((product_tax*price)/100).toFixed(2));
        

        $('.total_tax_amount').val(total_tax_price.toFixed(2));
    })

    calculateTotalPrice();

}


function calculateTotalShipping()
{

    var product_shipping = 0;
    var total_shipping_price = 0;

    $('.product_shipping').each(function(index, val) {

        product_shipping = $(this).val();
        if ($.trim(product_shipping) == '' || !$.isNumeric(product_shipping)) product_shipping = 0;

        total_shipping_price += Number(product_shipping);
        $('.total_shipping_amount').val(total_shipping_price);
    })

    calculateTotalPrice();

}

function calculateTotalPrice()
{
    var total_price = 0;
    var quantity = 0;
    var price = 0;
    var display_price = 0;
    var product_id = 0;

    $('.product_quantity').each(function() {
        quantity = $(this).val();

        // below commented because now each product currency is converted into the GBP
       // price = $(this).closest('.form-group').find('.unit_price_hidden').val();
       
       // Below commented on 4/23/2019 for which was done for product price field changes
       // price = $(this).closest('.form-group').find('.unit_price_hidden').val();
       price = $(this).closest('.form-group').find('.GBP_price_hidden').val();
       

        /*price = $(this).closest('.form-group').find('.calc_unit_price_display').val();
        priceExp = price.split(" ");
        price = price[1];*/

        product_id = $(this).closest('.form-group').find('.product_id').val();
        
        if ($.trim(price) == '' || !$.isNumeric(price)) price = 0;
        if ($.trim(quantity) == '' || !$.isNumeric(quantity)) quantity = 0;

        display_price = parseFloat(quantity) * parseFloat(price);
        total_price += display_price;
        unit_tax_calculated = $(this).closest('.form-group').find('.unit_price_hidden_tax').val();
        unit_shipp_calculated = $(this).closest('.form-group').find('.product_shipping').val();
        // START calculate individule product price
        var currency_rate = $.trim($('#currency_rate').val());
        if ($.isNumeric(currency_rate))
        {
          currency_rate = parseFloat(currency_rate);
        }else{
          currency_rate=1;
        }

        if ($.isNumeric(unit_tax_calculated))
        {
          unit_tax_calculated = parseFloat(unit_tax_calculated);
        }else{
          unit_tax_calculated=0;
        }

        if ($.isNumeric(unit_shipp_calculated))
        {
          unit_shipp_calculated = parseFloat(unit_shipp_calculated);
        }else{
          unit_shipp_calculated=0;
        }

        //calc_unit_price_hidden = (currency_rate*display_price)+parseFloat(unit_tax_calculated)+parseFloat(unit_shipp_calculated);
        calc_unit_price_hidden = (currency_rate*price);

        calc_unit_price_hidden = calc_unit_price_hidden.toFixed(2);
        $(this).closest('.form-group').find('.calc_unit_price_hidden').val(calc_unit_price_hidden);
        $(this).closest('.form-group').find('.calc_unit_price_display').val($.trim($('#currency_id option:selected').text())+" "+calc_unit_price_hidden);

        // END calculate individule product price

        //$(this).closest('.form-group').find('.unit_price').val(display_price.toFixed(2));
    })

    var currency_rate = $.trim($('#currency_rate').val());
        if ($.isNumeric(currency_rate))
        {
          currency_rate = parseFloat(currency_rate);
          var selected_currency_id = $("#currency_id option:selected").val();
          var multiply_currency=1;

          /* $('.product_currency_id').each(function() {
              if($(this).val()==selected_currency_id){
                multiply_currency=0;
              }
           });
           if(multiply_currency==1)*/
              total_price =  currency_rate*total_price;
        }
        else currency_rate = 0;

    //$('#tax_amount').val('');
    //if ($('#tax_flag').is(':checked'))
   // {
        //var tax_rate = $.trim($('#tax_rate').val());
        //if ($.isNumeric(tax_rate)) tax_rate = parseFloat(tax_rate);
        //else tax_rate = 0;

        //var tax_amount = total_price * (tax_rate / 100);
        //$('#tax_amount').val(tax_amount.toFixed(2));
        $('#tax_amount').val();
        var tax_amount = $.trim($('#tax_amount').val());
        if ($.isNumeric(tax_amount)) tax_amount = parseFloat(tax_amount);
        else tax_amount = 0;
        total_price += tax_amount;
    //}

        var ship_amount = $.trim($('#ship_amount').val());
        if ($.isNumeric(ship_amount)) ship_amount = parseFloat(ship_amount);
        else ship_amount = 0;
        total_price += ship_amount;

        



    $('#total_price').val(total_price.toFixed(2));
    //$('#total_price').val(total_price.toFixed(2))
    
    //$('#user_currency_symbol').val()
    $('#total_price_display').html( $('#currency_symbol').val()+ total_price.toFixed(2));

}

function deleteOrderFile(file_idx, file_name)
{
	$('#delete_order_file_link_' + file_idx).confirmation({
	  title: "Are you sure you want to delete the attached file?",
	  singleton: true,
	  placement: 'right',
	  popout: true,
	  onConfirm: function() {
	  	$('#existing_file_id_' + file_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('orders/deleteFile/'); ?>",
            data: { order_id: $('#order_id').val(), file_name: file_name }
        });
	  },
	  onCancel: function() {  }
	});
	
}

function deleteOrderInvoice(invoice_id)
{
	$('#delete_order_invoice_link_' + invoice_id).confirmation({
	  title: "Are you sure you want to delete the attached invoice?",
	  singleton: true,
	  placement: 'right',
	  popout: true,
	  onConfirm: function() {
	  	$('#existing_invoice_id_' + invoice_id).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('orders/deleteInvoice/'); ?>",
            data: { order_id: $('#order_id').val(), invoice_id: invoice_id }
        });
	  },
	  onCancel: function() {  }
	});
	
}


function uploadReceipts()
{
	$('#files_only_upload').val(1);
	$('#order_form').submit();	
}


function uploadInvoices()
{
	if ($.trim($('#invoice_number').val()) == "")
	{
		$('#invoice_number_invalid_modal').modal('show');
		return false;
	}
	else $('#order_invoice_form').submit();	
}

function loadDepartmentsForSingleLocation(department_id)
{
    var location_id = $('#location_id').val();
    var single = 1;

    if (location_id == 0)
        $('#department_id').html('<option value="">Select Department</option>');
    else
    {
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getDepartments',
            success: function(options) {
                var options_html = '<option value="">Select Department</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                $('#department_id').html(options_html);
                if (department_id != 0) $('#department_id').val(department_id);
            },
            error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
        });
    }
}


function checkLocationDepartmentPermission()
{
    var location_id = $('#location_id').val();
    var department_id = $('#department_id').val();
    var options_html = '';
    attr = $('#order_status').attr('disabled');
    

    if (attr == undefined) {

    $.ajax({
            type: "POST", data: { location_id: location_id,department_id:department_id,field_name:"approve_order" }, dataType: "json",
            url: BICES.Options.baseurl + '/users/checkPermission',
            success: function(options) {
                //var options_html = '<option value="">Select Department</option>';
             
                for (var i=0; i<options.length; i++)
                   
                  options_html += '<option value="' + options[i] + '">' + options[i] + '</option>';
                $('#order_status').html(options_html);
                //if (department_id != 0) $('#department_id').val(department_id);*/
            },
          //  error: function() { $('#department_id').html('<option value=""></option>'); }
        });
    }

}


function addDocument()
{
    var total_documents = $('#total_documents').val();
    total_documents = parseInt(total_documents);
    total_documents = total_documents + 1;
    $('#total_documents').val(total_documents);

    var new_document_html = $('#new_document_code').html();
    new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
    $('#total_documents').before(new_document_html);
}

function uploadDocument(uploadBtnIDX,e)
{  
  e.preventDefault();
  var uploadedObj = $('#order_file_'+uploadBtnIDX);
  var uploadedObjDesc = $('#file_desc_'+uploadBtnIDX); 
  var file_data = $('#order_file_'+uploadBtnIDX).prop('files')[0];
  var field_data = $('#file_desc_'+uploadBtnIDX).val();   
  var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('file_description', field_data);
    form_data.append('order_id',$('#order_id').val());                           
    $.ajax({
        url: BICES.Options.baseurl + '/orders/uploadDocument', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(uploaded_response){
          $("#uploaded_file_list").html(uploaded_response);
          uploadedObj.val(null);
          uploadedObjDesc.val(null);
            // display response from the PHP script, if any
        }
     });
}

function deleteSessionDocument(IDX)
{  
    var arrIDX = IDX;                 
    $.ajax({
        url: BICES.Options.baseurl + '/orders/deleteSessionDocument', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        data: {IDX:arrIDX},                         
        type: 'post',
        success: function(uploaded_response){
         $("#uploaded_file_list").html(uploaded_response);
            // display response from the PHP script, if any
        }
     });
}

function deleteDocument(docidx)
{
    var display_document_count = 0;
    $("div[id^='document_area']").each(function () {
        if ($(this).is(':visible')) display_document_count += 1;
    });

    if (display_document_count <= 1) alert("You must have at least one document field in the order form");
    else
    {
        if (confirm("Are you sure you want to delete this document?"))
        {
            $('#delete_document_flag_' + docidx).val(1);
            $('#document_area_' + docidx).hide();
            $('#end_document_area_' + docidx).hide();
        }
    }
}

</script>

<style type="text/css">
.btn-file {
    padding:6px !important;
}
@media (max-width: @screen-xs-min) {
  .modal-xs { width: @modal-sm; }
}
/*.modal-dialog {
     /* width: 100%;
      height: 100%;
      padding: 0;
      margin:0;/**/
   /* width: 90%;
    height: 100%;
    padding: 0;
    margin: 0 auto;
}
.modal-content {    
      height: 100%;
      border-radius: 0;
      color:white;
      overflow:auto;
}

label { color:#73879C; }*/


</style>


