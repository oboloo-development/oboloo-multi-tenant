<?php
$invoice_uploadable_statuses = array('Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier', 'Received', 'Invoice Received', 'Paid'); 

 $userPer = new User;

?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>
                <?php
                    if (isset($order_id) && $order_id)
                    {
                        echo 'Edit Order';
                        if (isset($order) && is_array($order) && isset($order['order_id']) && !empty($order['order_id']))
                            echo ' - #' . $order['order_id'];
                    }
                    else echo 'Add Order';
                ?>
            </h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('orders/list'); ?>">
                <button type="button" class="btn btn-default" >
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Order List
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

	<div id="alert-info" class="alert alert-info alert-dismissable" style="display: none;">
	    <a style="margin-top: 2px;" onclick="$('#alert-info').hide();" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
	    <strong>This order was not submitted since no route was found for approving the same.</strong>
	</div>


	<?php 
		if (isset($order) && is_array($order) && isset($order['order_status'])) 
		{
			$class_1_selected = $class_2_selected = $class_3_selected = $class_4_selected = "done";
			if ($order['order_status'] == 'Closed' || $order['order_status'] == 'Declined' || $order['order_status'] == 'Paid' || $order['order_status'] == 'Cancelled')
				$class_1_selected = $class_2_selected = $class_3_selected = $class_4_selected = "selected";
			
			if ($order['order_status'] == 'Pending' || $order['order_status'] == 'More Info Needed' || $order['order_status'] == 'Approved' || $order['order_status'] == 'Submitted')
				$class_1_selected = "selected";

			if ($order['order_status'] == 'Ordered - PO Not Sent To Supplier' || $order['order_status'] == 'Ordered - PO Sent To Supplier' || $order['order_status'] == 'Ordered - PO Not Sent To Supplier')
				$class_1_selected = $class_2_selected = "selected";

			if ($order['order_status'] == 'Received' || $order['order_status'] == 'Invoice Received')
				$class_1_selected = $class_2_selected = $class_3_selected = "selected";
	?>
		    <div id="wizard" class="form_wizard wizard_horizontal col-md-10 col-md-offset-1">
			
			      <div class="form-group">
			          <div class="col-md-6 col-sm-6 col-xs-12">
			              <h4>Order Movement</h4>
			          </div>
			      </div>
			      <div class="clearfix"> <br /><br /> </div>
	
			    <ul class="wizard_steps">
			    	<li>
			        	<a href="#step-1" class="<?php echo $class_1_selected; ?>">
			            	<span class="step_no">A</span>
			                <span class="step_descr">
			                	Approvals<br />
			                    <small>Internal Approvals</small>
			                </span>
			            </a>
			        </li>
			    	<li>
			        	<a href="#step-2"  class="<?php echo $class_2_selected; ?>">
			            	<span class="step_no">O</span>
			                <span class="step_descr">
			                	Ordered<br />
			                    <small>PO Created</small>
			                </span>
			            </a>
			        </li>
			    	<li>
			        	<a href="#step-3"  class="<?php echo $class_3_selected; ?>">
			            	<span class="step_no">R</span>
			                <span class="step_descr">
			                	Received<br />
			                    <small>Items Are Received</small>
			                </span>
			            </a>
			        </li>
			    	<li>
			        	<a href="#step-4" style="width: 130px;" class="<?php echo $class_4_selected; ?>">
			            	<span class="step_no">P</span>
			                <span class="step_descr">
			                	Invoice Paid<br />
			                    <small>Order Payment Done</small>
			                </span>
			            </a>
			        </li>
				</ul>
		    </div>
		    <div class="clearfix"> </div>
	
	<?php 
		} 
	?>
	                             
<div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>
    <?php $invoice_found = ""; if (isset($order['order_status']) && in_array($order['order_status'], $invoice_uploadable_statuses)) {
                // integer starts at 0 before counting
                $existing_invoices = array();
                $upload_dir = 'uploads/orders/invoices/';
                if ($order_id)
                {
                    if (!is_dir('uploads/orders/invoices')) mkdir('uploads/orders/invoices');
                    if (!is_dir('uploads/orders/invoices/' . $order_id))
                        mkdir('uploads/orders/invoices/' . $order_id);
                    $upload_dir = 'uploads/orders/invoices/' . $order_id . '/';
                    if ($handle = opendir($upload_dir)) {
                        while (($uploaded_invoice = readdir($handle)) !== false){
                            if (!in_array($uploaded_invoice, array('.', '..')) && !is_dir($upload_dir . $uploaded_invoice))
                                $existing_invoices[] = $uploaded_invoice;
                        }
                    }
                }
                ?>

                <?php if (is_array($existing_invoices) && count($existing_invoices)) {
                    $invoice_idx = 1; foreach ($existing_invoices as $uploaded_invoice) {
                        $invoice_idx += 1; if (empty($invoice_found)) $invoice_found = $uploaded_invoice; }
                } ?>

    <?php } ?>
<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php
        $orderObj = new Order();
        if (isset($order['location_id'])) { $location_id = $order['location_id']; } else {$location_id = 0;}
        if (isset($order['department_id'])) { $department_id = $order['department_id']; } else {$department_id = 0;}
        $approve = $orderObj->approveStatus($location_id,$department_id);
        if (isset($order['order_status']) && $order['order_status'] != 'Pending' && $order['order_status'] != 'More Info Needed') { ?>
        <?php if (isset($purchase_order) && $purchase_order && is_array($purchase_order) && isset($purchase_order['order_id']) && $purchase_order['order_id'] == $order_id) { ?>


        <a target="_procure_purchase_order" href="<?php echo AppUrl::bicesUrl('purchaseOrders/edit/' . $purchase_order['po_id']); ?>">
            <button type="button" class="btn btn-success">
                Purchase Order
            </button>
        </a>
        <?php if($approve['order']==1){ ?>
        <button type="button" class="btn btn-primary" onclick="$('#order_status_modal').modal('show');"
                style="border: 1px solid #ffc200 !important; background-color: #FFC200 !important; color: #fff !important;">
            Change Order Status
        </button>
           <?php } ?>
        <?php if (!empty($invoice_found)) { ?>
            <a href="<?php echo AppUrl::bicesUrl('uploads/orders/invoices/' . $order_id . '/' . $invoice_found); ?>" target="order_invoice">
                <button type="button" class="btn btn-success">
                    View Invoice
                </button>
            </a>
        <?php } ?>
        <?php }
        } ?>

    </div>
    <div class="clearfix"></div>
    <div>&nbsp;</div>
    <form id="order_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('orders/edit'); ?>">
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>Order Details</h4>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Location <span style="color: #a94442;">*</span></label>
			  <select name="location_id" id="location_id" class="form-control" onchange="loadDepartmentsForSingleLocation(0);">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($order['location_id']) && $location['location_id'] == $order['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Department <span style="color: #a94442;">*</span></label>
              <select name="department_id" id="department_id" class="form-control"  onchange="checkLocationDepartmentPermission();">
                  <option value="">Select Department</option>
              </select>
          </div>
      </div>

        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6 valid">
                <label class="control-label">Project</label>
                <select name="project_id" id="project_id" class="form-control">
                    <option value="">Select Project</option>
                    <?php foreach ($projects as $project) { ?>
                        <option value="<?php echo $project['project_id']; ?>"
                            <?php if (isset($order['project_id']) && $project['project_id'] == $order['project_id']) echo ' selected="SELECTED" '; ?>>
                            <?php echo $project['project_name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6 valid">
                <label class="control-label">Spend Type</label>
                <select name="spend_type" id="spend_type" class="form-control">
                    <option value="">Select Spend Type</option>
                    <option  <?php if (isset($order['spend_type']) && $order['spend_type'] == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                    <option  <?php if (isset($order['spend_type']) && $order['spend_type'] == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
                </select>
            </div>
        </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Currency Rate </label>
              <select name="currency_id" id="currency_id" class="form-control" onchange="loadCurrencyRate();">
                  <?php foreach ($currency_rate as $rate) { ?>
                      <option value="<?php echo $rate['id']; ?>"
                          <?php if (isset($order['currency_id']) && $rate['id'] == $order['currency_id']) echo ' selected="SELECTED" '; ?>>
                      <?php echo $rate['currency']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Currency Rate</label>
              <input readonly type="text" class="form-control" name="currency_rate" id="currency_rate"
                  <?php if (isset($order['currency_rate']) && !empty($order['currency_rate'])) { echo 'value="' . $order['currency_rate'] . '"'; } else { echo 'value="1"'; } ?> >

          </div>

      </div>

      <div class="form-group">
<!--          <div class="col-md-4 col-sm-4 col-xs-8 date-input valid">-->
<!--              <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>-->
<!--              <input required type="text" class="form-control has-feedback-left" name="vendor_name" id="vendor_name"-->
<!--                    --><?php //if (isset($order['vendor']) && !empty($order['vendor'])) echo 'value="' . $order['vendor'] . '"'; else echo 'placeholder="Supplier Name"'; ?><!-- >-->
<!--              <input type="hidden" name="vendor_id" id="vendor_id" value="--><?php //if (isset($order['vendor']) && !empty($order['vendor'])) echo $order['vendor_id']; else echo '0'; ?><!--" />-->
<!--              <span class="fa fa-user-circle-o form-control-feedback left" aria-hidden="true"></span>-->
<!--         	  <a style="text-decoration: underline; cursor: pointer;" onclick="$('#add_vendor_modal').modal('show');">-->
<!--         	  Click here</a> to add new vendor that is not the database.-->
<!--          </div>-->

          <div class="col-md-2 col-sm-2 col-xs-4 date-input">
              <label class="control-label">Order Date <span style="color: #a94442;">*</span></label>
              <input type="text" class="form-control has-feedback-left" name="order_date" id="order_date"
                    <?php if (isset($order['order_date']) && !empty($order['order_date'])) echo 'value="' . date("d/m/Y", strtotime($order['order_date'])) . '"'; else echo 'placeholder="Order Date"'; ?> >
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
	  </div>


      <div class="form-group">
          <?php
          	$record_user_id = 0;
			if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];
			if (isset($order['user_id']) && !empty($order['user_id'])) $record_user_id = $order['user_id'];  
          	
     $applicable_order_statuses = array('Pending', 'Approved', 'Closed', 'Declined', 'Ordered - PO Not Sent To Supplier', 'Received', 'Submitted', 'Paid', 'Cancelled', 'Invoice Received', 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier');

    if(isset(Yii::app()->session['user_id']) && !empty($order['location_id']) && !empty($order['department_id'])){
     
      $appOrder = $userPer->checkPermission(Yii::app()->session['user_id'],$order['location_id'],$order['department_id'],"approve_order");
    }
    if(isset($appOrder) && $appOrder["approve_order"]=="yes"){
     
      if (isset($order['order_status'])){
       // $applicable_order_statuses = array($order['order_status'], 'Submitted','Approved');
        $applicable_order_statuses = array($order['order_status'], 'Pending', 'Approved', 'Closed', 'Declined', 'Ordered - PO Not Sent To Supplier', 'Received', 'Submitted', 'Paid', 'Cancelled', 'Invoice Received', 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier');

      }else $applicable_order_statuses = array('Pending', 'Submitted','Approved');

    }else 
    if(!isset($order['order_status']) || $order['order_status'] == 'Pending' || $order['order_status'] == 'More Info Needed')
			{
				if (isset($order['order_status']))
					$applicable_order_statuses = array($order['order_status'], 'Submitted');
				else $applicable_order_statuses = array('Pending', 'Submitted');
			}

        if(!empty($order['order_status'])){
          $class="col-md-3 col-sm-3 col-xs-6";
        }else{
          $class="col-md-6 col-sm-6 col-xs-6";
        }

          ?>
          <div class="<?php echo $class;?>">
              <label class="control-label">Name</label>
              <select class="form-control" name="user_id" id="user_id">
                  <?php foreach ($users as $user) { ?>
                      <option value="<?php echo $user['user_id']; ?>"
                                <?php if ($record_user_id == $user['user_id']) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $user['full_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
          <?php if(!empty($order['order_status'])){?>
          <div class="<?php echo $class;?> valid">
              <label class="control-label">Order Status</label>
              <select class="form-control" name="order_status" id="order_status"
                        <?php 

                        if(isset($order['order_status']) && (!isset($appOrder) || $appOrder["approve_order"] !="yes") && ($order['order_status'] != 'Pending' && $order['order_status'] != 'More Info Needed')){
                          echo 'disabled="disabled" ';
                        }else if (isset(Yii::app()->session['user_id']) && isset($order['user_id']) && Yii::app()->session['user_id']==$order['user_id'] && $order['order_status'] != 'Pending' && $order['order_status'] != 'More Info Needed') {
                         echo ' disabled="disabled" ';
                          }
                           ?>>
                  <?php foreach ($applicable_order_statuses as $order_status) { ?>
                      <option value="<?php echo $order_status; ?>"
                                <?php if (isset($order['order_status']) && $order['order_status'] == $order_status) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $order_status; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
        <?php }else{?>
          <input type="hidden"  name="order_status" id="order_status" />
        <?php } ?>
      </div>
      <div class="clearfix"> </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="control-label">Order Description/Notes</label>
              <textarea class="form-control" name="description" id="description" rows="4" <?php if (!isset($order['description']) || empty($order['description'])) echo 'placeholder="Order description/notes"'; ?>><?php if (isset($order['description']) && !empty($order['description'])) echo $order['description']; ?></textarea>
          </div>
      </div>
      <div class="clearfix"> <br /> </div>

      <?php
        $existing_items_found = false;
        if (isset($order_details) && is_array($order_details) && count($order_details))
            $existing_items_found = true;
      ?>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>
                  Order Items
        		  <?php if (!isset($order['order_status']) || $order['order_status'] == 'Pending' || $order['order_status'] == 'More Info Needed') { ?>
	                  <?php if ($existing_items_found) { ?>                  	
	                      <div class="pull-right" style="font-size: 70%; padding-top: 5px;">
	                          <a style="cursor: pointer; text-decoration: underline;"
	                                onclick="$('#here_is_where_you_add_new_items_to_this_order').show();">
	                              Click here</a>
	                          To Add More Items To this Order
	                      </div>
	                  <?php } ?>
                  <?php } ?>
              </h4>
          </div>
      </div>

      <?php
//        echo "<pre>";
//        print_r($order_details);
//        echo "</pre>";die;
        $total_price = 0;
        if (isset($order_details) && is_array($order_details) && count($order_details))
        {
      ?>

          <?php
            foreach ($order_details as $order_detail)
            {
                if (!isset($order_detail['quantity']) || empty($order_detail['quantity'])) $order_detail['quantity'] = 1;
                if (!isset($order_detail['unit_price']) || empty($order_detail['unit_price'])) $order_detail['unit_price'] = 0;
          ?>

              <div class="form-group">
                  <div class="col-md-2 col-sm-2 col-xs-4" style="display: none;">
                      <label class="control-label">Product Type</label>
                      <select class="form-control product_type" name="product_type[]">
                          <option value="Bundle"
                                <?php if (isset($order_detail['product_type']) && $order_detail['product_type'] == 'Bundle') echo ' selected="SELECTED" '; ?>>
                              Bundle
                          </option>
                          <option value="Product"
                                <?php if (isset($order_detail['product_type']) && $order_detail['product_type'] == 'Product') echo ' selected="SELECTED" '; ?>>
                              Product
                          </option>
                      </select>
                  </div>
                  <?php //print_r($order_detail['product_name']);die; ?>
                  <div class="col-md-3 col-sm-3 col-xs-6 date-input valid">
                      <label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
                      <input required="true" type="text" class="form-control has-feedback-left price_calc" name="product_name[]"
                            <?php if (isset($order_detail['product_name']) && !empty($order_detail['product_name'])) echo 'value="' . str_replace('"','',$order_detail['product_name']). '"'; ?> >
                      <input class="product_id" type="hidden" name="product_id[]" value="<?php echo $order_detail['product_id']; ?>" />
                      <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-6 date-input valid">
                      <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                      <input required type="text" class="form-control has-feedback-left" name="vendor_name[]" id="vendor_name"
                          <?php if (isset($order_detail['vendor']) && !empty($order_detail['vendor'])) echo 'value="' . str_replace('"','',$order_detail['vendor']). '"'; else echo 'placeholder="Supplier Name"'; ?> >
                      <input type="hidden" name="vendor_id[]" class="vendor_id" value="<?php if (isset($order_detail['vendor']) && !empty($order_detail['vendor'])) echo $order_detail['vendor_id']; else echo '0'; ?>" />
                      <span class="fa fa-user-circle-o form-control-feedback left" aria-hidden="true"></span>
                      <a style="text-decoration: underline; cursor: pointer;" onclick="$('#add_vendor_modal').modal('show');">
                          Click here</a> to add new vendor that is not the database.
                  </div>
                  <div class="clearfix"> <br /><br /> </div>

<!--                  <div class="col-md-2 col-sm-2 col-xs-4 display_price informational_messages_area" style="padding: 8px; font-size: 120%;">-->
<!--                      <label class="control-label">&nbsp;</label>-->
<!--                      -->
<!--                  </div>-->
                  <div class="col-md-3 col-sm-3 col-xs-6">
                      <label class="control-label">Unit Price
                          <?php
                          if (isset($order_detail['unit_price']) && !empty($order_detail['unit_price'])
                              && isset($order_detail['quantity']) && !empty($order_detail['quantity']))
                          {
                              echo '@ ';
                              echo number_format($order_detail['unit_price'] / $order_detail['quantity'], 2);
                              echo ' / Each';
                          }
                          ?>
                      </label>
                      <input style="text-align: right;" type="text" class="unit_price form-control input_price_by_user" name="unit_price[]" readonly="readonly"
                          <?php if (isset($order_detail['unit_price']) && !empty($order_detail['unit_price'])) echo 'value="' . $order_detail['unit_price'] . '"'; ?> >
                      <input type="hidden" class="unit_price_hidden" name="unit_price_hidden[]"
                          <?php if (isset($order_detail['unit_price']) && !empty($order_detail['unit_price']) && isset($order_detail['quantity']) && !empty($order_detail['quantity'])) echo 'value="' . round($order_detail['unit_price'] / $order_detail['quantity'], 2) . '"'; ?> >
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-2 date-input valid">
                      <label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
                      <input required style="text-align: right;" type="text" class="product_quantity form-control price_calc" name="quantity[]"
                          <?php if (isset($order_detail['quantity']) && !empty($order_detail['quantity'])) echo 'value="' . $order_detail['quantity'] . '"'; ?> >
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-2 date-input valid">
                      <label class="control-label">Tax % </label>
                      <input style="text-align: right;" type="text" class="product_tax form-control input_tax_by_user" name="tax[]"
                          <?php if (isset($order_detail['tax']) && !empty($order_detail['tax'])) echo 'value="' . $order_detail['tax'] . '"'; ?> >
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-2 date-input valid">
                      <label class="control-label">Shipping</label>
                      <input  style="text-align: right;" type="text" class="product_shipping form-control input_shipping_by_user" name="shipping_cost[]"
                          <?php if (isset($order_detail['shipping_cost']) && !empty($order_detail['shipping_cost'])) echo 'value="' . $order_detail['shipping_cost'] . '"'; ?> >
                  </div>


                  <div class="col-md-2">
	        		  <?php if (!isset($order['order_status']) || $order['order_status'] == 'Pending' || $order['order_status'] == 'More Info Needed') { ?>
	                      <a onclick="$(this).parent().parent().remove(); calculateTotalPrice(); return false;" class="btn btn-link">
	                          <span class="fa fa-remove hidden-xs"></span>
	                      </a>
	                  <?php } ?>
                  </div>
              </div>

          <?php $total_price += $order_detail['unit_price']; } ?>

      <?php
            }

            $tax_amount = 0;
            $tax_rate = 20;
            if (isset($order_details[0]['tax']) && $order_details[0]['tax'] && $total_price != 0)
            {
//                $tax_rate = $order['tax_rate'];
//                $tax_amount = ($order['tax_rate'] * $total_price) / 100.0;
//                $total_price += $tax_amount;
                  $total_price += $order_details[0]['tax'];

            }

            if (isset($order_details[0]['shipping_cost']) && $order_details[0]['shipping_cost'])
                $total_price += $order_details[0]['shipping_cost'];
      ?>

      <div id="here_is_where_you_add_new_items_to_this_order"
            <?php if ($existing_items_found) echo ' style="display: none;" '; ?>>
      <fieldset id="additional-field-model">
          <div class="form-group">
              <div class="col-md-2 col-sm-2 col-xs-4" style="display: none;">
                  <label class="control-label">Product Type</label>
                  <select class="form-control product_type" name="product_type[]">
                      <option value="Product">Product</option>
                      <option value="Bundle">Bundle</option>
                  </select>
              </div>

              <div class="col-md-3 col-sm-3 col-xs-6 date-input valid">
                  <label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
                  <input required type="text" class="form-control has-feedback-left price_calc" value="" name="product_name[]" />
                  <input type="hidden" name="product_id[]" class="product_id" value="0" />
                  <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div class="col-md-3 col-sm-3 col-xs-6 date-input valid">
                  <label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
                  <input required type="text" class="form-control has-feedback-left" name="vendor_name[]" id="vendor_name" />
                  <input type="hidden" name="vendor_id[]" class="vendor_id" value="0"/>
                  <span class="fa fa-user-circle-o form-control-feedback left" aria-hidden="true"></span>
                  <a style="text-decoration: underline; cursor: pointer;" onclick="$('#add_vendor_modal').modal('show');">
                      Click here</a> to add new vendor that is not the database.
              </div>

              <div class="clearfix"> <br /><br /> </div>

              <div class="col-md-3 col-sm-3 col-xs-6">
                  <label class="control-label">Total Product Price</label>
                  <input style="text-align: right;" type="text" class="unit_price form-control input_price_by_user" value="" name="unit_price[]" placeholder="Price" />
                  <input type="hidden" class="unit_price_hidden" name="unit_price_hidden[]" value="" />
              </div>

              <div class="col-md-1 col-sm-1 col-xs-2 valid">
                  <label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
                  <input required style="text-align: right;" type="text" class="product_quantity form-control price_calc" name="quantity[]" placeholder="Quantity" />
              </div>

<!--              <div class="col-md-2 col-sm-2 col-xs-4 display_price informational_messages_area" style="padding: 8px; font-size: 120%;">-->
<!---->
<!--              </div>-->

              <div class="col-md-1 col-sm-1 col-xs-2">
                  <label class="control-label">Tax %</label>
                  <input style="text-align: right;" type="text" class="product_tax form-control input_tax_by_user" value="" name="tax[]" placeholder="Tax" />
              </div>
              <div class="col-md-1 col-sm-1 col-xs-2">
                  <label class="control-label">Shipping</label>
                  <input style="text-align: right;" type="text" class="product_shipping form-control input_shipping_by_user" value="" name="shipping_cost[]" placeholder="Shipping" />
              </div>
              <div class="col-md-2">
                  <a href="javascript:void(0);" class="btn btn-link remove-this-field">
                      <span class="fa fa-remove hidden-xs" style="margin-top:30px;"></span>
                  </a>
                  <a href="javascript:void(0);" class="btn btn-link create-new-field">
                      <span class="fa fa-plus hidden-xs" style="margin-top:30px;"></span>
                  </a>
              </div>
          </div>
      </fieldset>
      <div class="clearfix"> </div>
      </div>

         <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h5>Upload Documents</h5>
                </div>
            </div>

            <div class="form-group" id="document_area_1">
                <div class="col-md-5 col-sm-5 col-xs-10">
                    <input class="form-control" type="file" name="order_file_1" id="order_file_1" />
                </div>
                <div class="col-md-1 col-sm-1 col-xs-2">
                    <a onclick="deleteDocument(1);" title="Click here to delete this document" style="cursor: pointer;">
                        <span class="fa fa-minus fa-2x"></span>
                    </a>
                    &nbsp;
                    <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
                        <span class="fa fa-plus fa-2x"></span>
                    </a>
                </div>
                <div class="clearfix"> </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">File/Document Description</label>
                    <input class="form-control" type="text" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" />
                </div>
            </div>
            <input type="hidden" name="delete_document_flag_1" id="delete_document_flag_1" value="0" />
            <div class="clearfix" id="end_document_area_1"> <br /> </div>
        <input type="hidden" name="total_documents" id="total_documents" value="1" />
      <div class="clearfix"> <br /> </div>

        <div id="new_document_code" style="display: none;">
            <div class="form-group" id="document_area_DOCIDX">
                <div class="col-md-5 col-sm-5 col-xs-10">
                    <input class="form-control" type="file" name="order_file_DOCIDX" id="order_file_DOCIDX" />
                </div>
                <div class="col-md-1 col-sm-1 col-xs-2">
                    <a onclick="deleteDocument(DOCIDX);" title="Click here to delete this document" style="cursor: pointer;">
                        <span class="fa fa-minus fa-2x"></span>
                    </a>
                    <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
                        <span class="fa fa-plus fa-2x"></span>
                    </a>
                </div>
                <div class="clearfix"> </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">File/Document</label>
                    <input class="form-control" type="text" name="file_desc_DOCIDX" id="file_desc_DOCIDX" placeholder="File/Document Description" />
                </div>
            </div>
            <input type="hidden" name="delete_document_flag_DOCIDX" id="delete_document_flag_DOCIDX" value="0" />
            <div class="clearfix" id="end_document_area_DOCIDX"> <br /> </div>
        </div>
        <div class="clearfix"> </div>

<!--      <div class="form-group">-->
<!--          <div class="col-md-5 col-sm-5 col-xs-10">-->
<!--              <h4>Upload Receipts/Files</h4>-->
<!--          </div>-->
<!--          <div class="col-md-1 col-sm-1 col-xs-2">-->
<!--	          --><?php //if (isset($order_id) && $order_id) { ?>
<!--    	          <button class="btn btn-sm btn-primary" onclick="uploadReceipts(); return false;">Upload</button>-->
<!--	          --><?php //} ?>
<!--          </div>-->
<!--      </div>     -->
      <div class="clearfix"> <br /> </div>

		<?php 
		    // integer starts at 0 before counting
		    $existing_files = array(); 
		    $upload_dir = 'uploads/orders/';
			if ($order_id)
			{
				if (!is_dir('uploads/orders')) mkdir('uploads/orders');
				if (!is_dir('uploads/orders/' . $order_id)) 
					mkdir('uploads/orders/' . $order_id);
				$upload_dir = 'uploads/orders/' . $order_id . '/';
			    if ($handle = opendir($upload_dir)) {
			        while (($uploaded_file = readdir($handle)) !== false){
			            if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file)) 
			                $existing_files[] = $uploaded_file;
			        }
			    }
			}
		?>
		
		<?php if (is_array($existing_files) && count($existing_files)) { ?>

		      <div class="form-group">
		          <div class="col-md-6 col-sm-6 col-xs-12">
		              <h5>Existing Receipts/Files</h5>
		          </div>
		      </div>
		      
	          <div class="col-md-6 col-sm-6 col-xs-12">
		      <table class="table">
		      <?php $file_idx = 1; 

          $orderObject = new Order();
          foreach ($existing_files as $uploaded_file) {

            $sql = 'select * from order_files where order_id='.$order_id.' and file_name="'.$uploaded_file.'"';
            $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
           ?>
		      	<tr id="existing_file_id_<?php echo $file_idx; ?>">
		      		<td>
                        <a href="<?php echo AppUrl::bicesUrl('uploads/orders/' . $order_id . '/' . $uploaded_file); ?>" target="order_file" >
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
                        &nbsp;
						<a id="delete_order_file_link_<?php echo $file_idx; ?>" style="cursor: pointer;" 
								onclick="deleteOrderFile(<?php echo $file_idx; ?>, '<?php echo $uploaded_file; ?>');">
						   <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</a>                        
                       </a>
		      		</td>
		      		<td>
		      			<?php echo $uploaded_file; ?>
		      		</td>
              <td>
                <?php echo !empty($fileReader['description'])?$fileReader['description']:'';?>
              </td>
		      	</tr>
		      <?php $file_idx += 1; } ?>
		      </table>
		      </div>
		           
		      <div class="clearfix"> <br /> </div>
		
        <?php } ?>
        
<!--    <div class="form-group">-->
<!--        <div class="col-md-6 col-sm-6 col-xs-12">-->
<!--            <input multiple="multiple" class="file" name="file[]" id="file" type="file" />-->
<!--    	</div>-->
<!--    </div>-->
    <div class="clearfix"> <br /> </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" name="total_price" id="total_price" value="<?php echo number_format($total_price, 2); ?>" />
            <input type="hidden" name="order_id" id="order_id" value="<?php if (isset($order_id)) echo $order_id; ?>" />
            <input type="hidden" name="recurrent_number" id="recurrent_number" value="<?php if (isset($order['recurrent_number'])) echo $order['recurrent_number']; ?>" />
            <input type="hidden" name="created_by_user_id" id="created_by_user_id" value="<?php if (isset(Yii::app()->session['user_id'])) echo Yii::app()->session['user_id']; else echo '0'; ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
            <input type="hidden" name="files_only_upload" id="files_only_upload" value="0" />
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-4 col-sm-4 col-xs-8">
            <h4 style="padding-top: 10px;">Tax Amount <span style="font-size: 70%;">(if applicable)</span></h4>
<!--            <span style="font-size: 130%; margin-right: 10px;">TAX</span>-->
<!--            <input type="checkbox" class="flat" name="tax_flag" id="tax_flag"-->
<!--                --><?php //if (isset($order['tax_rate']) && $order['tax_rate']) echo ' checked="CHECKED" '; ?><!-- />-->
<!--            Check here to apply tax @-->
<!--            <input type="text" style="width: 60px; text-align: right; display: inline !important;" onkeyup="calculateTotalPrice();" class="form-control price_calc" name="tax_rate" id="tax_rate" value="--><?php //echo number_format($tax_rate, 2); ?><!--" /> %-->
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4">
            <label class="control-label">Tax Amount</label>
            <input style="text-align: right;" type="text" class="form-control total_tax_amount"
                    id="tax_amount" name="tax_rate" placeholder="Tax Amount" readonly="readonly"
                        <?php if (isset($order_details[0]['tax']) && $order_details[0]['tax']) echo 'value="' . number_format($order_details[0]['tax'], 2) . '"';  ?> />
        </div>
        <div class="clearfix"> </div>
    </div>

    <div class="form-group">
        <div class="col-md-4 col-sm-4 col-xs-8">

            <h4 style="padding-top: 10px;">Shipping Costs <span style="font-size: 70%;">(if applicable)</span></h4>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4">
            <label class="control-label">Shipping Costs</label>
            <input readonly="readonly" style="text-align: right;" type="text" class="form-control total_shipping_amount price_calc" onkeyup="calculateTotalPrice();"
                    id="ship_amount" name="ship_amount" placeholder="Shipping Amount"
                        <?php if (isset($order_details[0]['shipping_cost']) && $order_details[0]['shipping_cost']) echo 'value="' . number_format($order_details[0]['shipping_cost'], 2) . '"';  ?> />
        </div>
        <div class="clearfix"> </div>
    </div>

</form>
</div>



<div class="form-group">
    <div class="col-md-10 col-sm-10 col-xs-10 text-center">

        <h3>Total Price  <span id="total_price_display" style="display: inline-block;"><?php $display_final_price = false; $final_grand_total_price = $total_price; echo Yii::app()->session['user_currency_symbol'] . number_format($total_price, 2); ?></span></h3>
    </div>
   
    <div class="clearfix"> </div>
</div>

<?php if (isset($order['discount']) && !empty($order['discount'])) { ?>
	<div class="form-group">
	    <div class="col-md-4 col-sm-4 col-xs-8">
	        <h3>Discount (PO Level)</h3>
	    </div>
	    <div class="col-md-2 col-sm-2 col-xs-4">
	        <h3 style="text-align: right;">
	            <?php $display_final_price = true; $final_grand_total_price = $final_grand_total_price - $order['discount']; echo Yii::app()->session['user_currency_symbol'] . number_format($order['discount'], 2); ?>
	        </h3>
	    </div>
	    <div class="clearfix"> </div>
	</div>
<?php } ?>

<?php if (isset($order['other_charges']) && !empty($order['other_charges'])) { ?>
	<div class="form-group">
	    <div class="col-md-4 col-sm-4 col-xs-8">
	        <h3>Other Charges (PO Level)</h3>
	    </div>
	    <div class="col-md-2 col-sm-2 col-xs-4">
	        <h3 style="text-align: right;">
	            <?php $display_final_price = true; $final_grand_total_price = $final_grand_total_price + $order['other_charges']; echo Yii::app()->session['user_currency_symbol'] . number_format($order['other_charges'], 2); ?>
	        </h3>
	    </div>
	    <div class="clearfix"> </div>
	</div>
<?php } ?>

<?php if ($display_final_price) { ?>
	<div class="form-group">
	    <div class="col-md-4 col-sm-4 col-xs-8">
	        <h3>Final Price (PO Level)</h3>
	    </div>
	    <div class="col-md-2 col-sm-2 col-xs-4">
	        <h3 style="text-align: right;">
	            <?php echo Yii::app()->session['user_currency_symbol'] . number_format($final_grand_total_price, 2); ?>
	        </h3>
	    </div>
	    <div class="clearfix"> </div>
	</div>
<?php } ?>

    <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;" /></div>
    <div class="clearfix"> </div>
    <?php if (isset($order_vendor_detail) && $order_vendor_detail) { ?>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <h4>
                 Supplier Updates
            </h4>
        </div>
    </div>
        <div class="clearfix"> <br /> </div>
    <?php $supplier_order_statuses = array('Preparing','Cannot Fulfil','Sent', 'Delivered', 'Paid'); ?>
    <div class="clearfix"> </div>
    <div class="form-group">

        <div class="col-md-3 col-sm-3 col-xs-6">
            <label class="control-label">Supplier Status</label>
            <select class="form-control" name="status" id="status" readonly>
                <?php foreach ($supplier_order_statuses as $supplier_status) { ?>
                    <option value="<?php echo $supplier_status; ?>"
                        <?php if (isset($order_vendor_detail['status']) && $order_vendor_detail['status'] == $supplier_status) echo ' SELECTED="SELECTED" '; ?>>
                        <?php echo $supplier_status; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="clearfix"> </div>

    <div class="form-group">

        <div class="col-md-3 col-sm-3 col-xs-6">
            <label class="control-label">Tracking Number</label>
            <input type="text" class="form-control" readonly name="tracking_number" id="tracking_number"
                <?php if (isset($order_vendor_detail['tracking_number']) && !empty($order_vendor_detail['tracking_number'])) { echo 'value="' . $order_vendor_detail['tracking_number'] . '"'; } else { echo 'placeholder="Tracking Number"'; } ?> >

        </div>

        <div class="col-md-3 col-sm-3 col-xs-6">
            <label class="control-label">Courier Company</label>
            <input type="text" class="form-control" readonly name="courier_company" id="courier_company"
                <?php if (isset($order_vendor_detail['courier_company']) && !empty($order_vendor_detail['courier_company'])) { echo 'value="' . $order_vendor_detail['courier_company'] . '"'; } else { echo 'placeholder="Courier Company"'; } ?> >

        </div>

    </div>
    <div class="clearfix"> </div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <label class="control-label">Additional Notes</label>
            <textarea class="form-control" name="note" readonly id="note" rows="4" <?php if (!isset($order_vendor_detail['note']) || empty($order_vendor_detail['note'])) echo 'placeholder="Additional Notes for Supplier"'; ?>><?php if (isset($order_vendor_detail['note']) && !empty($order_vendor_detail['note'])) echo $order_vendor_detail['note']; ?></textarea>
        </div>
    </div>
        <div class="clearfix"> </div><br/>
        <?php
        // integer starts at 0 before counting
        $vendor_id = $order['vendor_id'];
        $your_existing_files = array();
        $upload_dir = 'uploads/orders/';
        if ($order_id)
        {
            if (!is_dir('uploads/orders')) mkdir('uploads/orders');
            if (!is_dir('uploads/orders/' . $order_id))
                mkdir('uploads/orders/' . $order_id);
            if (!is_dir('uploads/orders/' . $order_id . '/' . $vendor_id))
                mkdir('uploads/orders/' . $order_id . '/' . $vendor_id);
            $upload_dir = 'uploads/orders/' . $order_id . '/' . $vendor_id;
            if ($handle = opendir($upload_dir)) {
                while (($uploaded_file = readdir($handle)) !== false){
                    if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file))
                        $your_existing_files[] = $uploaded_file;
                }
            }
        }
        ?>
        <?php if (is_array($your_existing_files) && count($your_existing_files)) { ?>

            <div class="col-md-6 col-sm-6 col-xs-6">
                <table class="table">
                    <?php $file_idx = 1; foreach ($your_existing_files as $uploaded_file) { ?>
                        <tr id="existing_file_id_<?php echo $file_idx; ?>">
                            <td>
                                <a href="<?php echo AppUrl::bicesUrl('uploads/orders/' . $order_id . '/'.$vendor_id.'/' .$uploaded_file); ?>" target="quote_file" style="color: #000;">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                </a>

                            </td>
                            <td>
                                <?php echo $uploaded_file; ?>
                            </td>
                        </tr>
                        <?php $file_idx += 1; } ?>
                </table>
            </div>
            <div class="clearfix"> <br /> </div>
            <?php 


            if(isset($order['location_id'])){
             $appOrder = $userPer->checkPermission(Yii::app()->session['user_id'],$order['location_id'],$order['department_id'],"approve_invoice");

              if(isset($appOrder) && $appOrder["approve_invoice"]=="yes"){ ?>
            <button type="button" class="btn btn-primary" onclick="$('#order_approve_inovice').modal('show');"
                    style="border: 1px solid #ffc200 !important; background-color: #FFC200 !important; color: #fff !important;">
               Approve Invoice
            </button>
            <?php } } ?>

            <div class="clearfix"> <br /> </div>

        <?php } ?>

    <?php } ?>

<?php 


$invoice_found = ""; if (isset($order['order_status']) && in_array($order['order_status'], $invoice_uploadable_statuses)) {

$order_invoice_save="order_invoice_form";


 ?>
<div class="row">
  <form id="order_invoice_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('orders/editInvoices'); ?>">
      <div class="form-group">
          <div class="col-md-2 col-sm-2 col-xs-4">
              <h4>Upload Invoices</h4>
          </div>
        <div class="clearfix"></div> <br /><br />
        <div class="col-md-4 col-sm-4 col-xs-4">
            <input multiple="multiple" class="file" name="invoice_file[]" id="invoice_file" type="file" />
        </div>
          <div class="clearfix"></div>
          <div class="col-md-3 col-sm-3 col-xs-6">
        <label class="control-label">Invoice Number</label>
			  <input type="text" class="form-control has-feedback-left" name="invoice_number" id="invoice_number"
                	<?php if (isset($order['invoice_number']) && !empty($order['invoice_number'])) echo 'value="' . $order['invoice_number'] . '"'; ?> placeholder="Invoice Number" />
              <span class="fa fa-file-o form-control-feedback left" aria-hidden="true"></span>
          </div>

          <div class="col-md-1 col-sm-1 col-xs-2">
            <label class="control-label">&nbsp;</label>
            <button class="btn btn-sm btn-primary" onclick="uploadInvoices(); return false;" style="margin-top: 27px;">Upload</button>
          </div>
      </div>     
      <div class="clearfix"> <br /> </div>

		<?php 
		    // integer starts at 0 before counting
		    $existing_invoices = array(); 
		    $upload_dir = 'uploads/orders/invoices/';
			if ($order_id)
			{
				if (!is_dir('uploads/orders/invoices')) mkdir('uploads/orders/invoices');
				if (!is_dir('uploads/orders/invoices/' . $order_id)) 
					mkdir('uploads/orders/invoices/' . $order_id);
				$upload_dir = 'uploads/orders/invoices/' . $order_id . '/';
			    if ($handle = opendir($upload_dir)) {
			        while (($uploaded_invoice = readdir($handle)) !== false){
			            if (!in_array($uploaded_invoice, array('.', '..')) && !is_dir($upload_dir . $uploaded_invoice)) 
			                $existing_invoices[] = $uploaded_invoice;
			        }
			    }
			}
		?>
		
		<?php if (is_array($existing_invoices) && count($existing_invoices)) { ?>

		      <div class="form-group">
		          <div class="col-md-6 col-sm-6 col-xs-12">
		              <h5>Existing Invoices</h5>
		          </div>
		      </div>
		      
	          <div class="col-md-6 col-sm-6 col-xs-12">
		      <table class="table">
		      <?php $invoice_idx = 1; foreach ($existing_invoices as $uploaded_invoice) { ?>
		      	<tr id="existing_invoice_id_<?php echo $invoice_idx; ?>">
		      		<td> 
                        <a href="<?php echo AppUrl::bicesUrl('uploads/orders/invoices/' . $order_id . '/' . $uploaded_invoice); ?>" target="order_invoice">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
                        &nbsp;
						<a id="delete_order_invoice_link_<?php echo $invoice_idx; ?>" style="cursor: pointer;" 
								onclick="deleteOrderInvoice(<?php echo $invoice_idx; ?>, '<?php echo $uploaded_invoice; ?>');">
						   <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</a>                        
                       </a>
		      		</td>
		      		<td>
		      			<?php echo $uploaded_invoice; ?>
		      		</td>
		      	</tr>
		      <?php $invoice_idx += 1; if (empty($invoice_found)) $invoice_found = $uploaded_invoice; } ?>
		      </table>
		      </div>
		           
		      <div class="clearfix"> <br /> </div>
		
        <?php } ?>

    <div class="clearfix"> </div>
    <input type="hidden" name="order_id_for_invoices" id="order_id_for_invoices" value="<?php if (isset($order_id)) echo $order_id; ?>" />
    <input type="hidden" name="form_submitted_for_invoices" id="form_submitted_for_invoices" value="1" />
  </form>
</div>
<?php } ?>

<?php if (isset($approvals) && is_array($approvals) && count($approvals)) { ?>
      <div class="clearfix"> <br /> </div>
      <div class="form-group">
          <div class="col-md-4 col-sm-4 col-xs-8">
              <h4>Approval Routing</h4>
          </div>
          <?php if (isset(Yii::app()->session['user_type']) && Yii::app()->session['user_type'] == 1) { ?>
          	
          	  <?php 
          	  		$last_apporval_id = 0;
          	  		foreach ($approvals as $an_approval)
						if ($an_approval['user_id'] == Yii::app()->session['user_id']) 
          	  				$last_apporval_id = $an_approval['id'];  
          	  ?>
	          <div class="col-md-2 col-sm-2 col-xs-4" style="text-align: right;">
	          	<?php if ($last_apporval_id) { ?>
		          	<a href="<?php echo AppUrl::bicesUrl('routing/edit/' . $last_apporval_id); ?>">
		              	<button class="btn btn-sm btn-primary">
		              		Approve Order
		              	</button>
		            </a>
	           <?php } ?>
	          </div>
	      <?php } ?>
      </div>     
      <div class="clearfix"> <br /> </div>
      
      <?php
      		$order_is_already_approved = false;
			if (isset($order) && is_array($order) && isset($order['order_status']))
			{
				if ($order['order_status'] != "Pending" && $order['order_status'] != "More Info Needed" && 
						$order['order_status'] != "Submitted" && $order['order_status'] != "Cancelled" && $order['order_status'] != "Declined")
					$order_is_already_approved = true;
			}
      ?>
      
      <div class="col-md-6 col-sm-6 col-xs-12">
      <table class="table">
      		<thead><tr><th>User</th><th>Date</th><th>Status</th><th>Comments</th></tr></thead>
      		<?php 
      			foreach ($approvals as $an_approval) 
      			{
      				if ($order_is_already_approved && $an_approval['routing_status'] != 'Approved') continue; 
      		?>
	      			<tr>
	      				<td><?php echo $an_approval['user']; ?></td>
	      				<td><?php echo date("F j, Y", strtotime($an_approval['routing_date'])); ?></td>
	      				<td>
	      					<?php 
	      						if ($an_approval['routing_status'] != 'Information') echo $an_approval['routing_status'];
								else echo 'More Info Needed'; 
	      					?>
	      				</td>
	      				<td><?php echo $an_approval['routing_comments']; ?></td>
	      			</tr>
      		<?php 
				} 
			?>
      </table>
	  </div>      
      
      <div class="clearfix"> <br /> </div>
<?php } ?>


<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php if (isset($order['order_status']) && (!isset($appOrder) || $appOrder["approve_order"] !="yes")  && ($order['order_status'] != 'Pending' && $order['order_status'] != 'More Info Needed')) { ?>
	       	<?php if (isset($purchase_order) && $purchase_order && is_array($purchase_order) && isset($purchase_order['order_id']) && $purchase_order['order_id'] == $order_id) { ?>
<!--	       		<button type="button" class="btn btn-primary" onclick="$('#order_status_modal').modal('show');"-->
<!--	       				style="border: 1px solid #ffc200 !important; background-color: #FFC200 !important; color: #fff !important;">-->
<!--	       			Change Order Status-->
<!--	       		</button>-->
<!---->
<!--	       		<a target="_procure_purchase_order" href="--><?php //echo AppUrl::bicesUrl('purchaseOrders/edit/' . $purchase_order['po_id']); ?><!--">-->
<!--		       		<button type="button" class="btn btn-success">-->
<!--		       			Purchase Order-->
<!--		       		</button>-->
<!--		       	</a>-->
			<?php } else { ?>
	            <div class="alert alert-warning fade in" role="alert" style="margin-top: 20px;">
	                <span style="font-size: 120%;">This order has already been submitted and hence cannot be edited.</span>
	            </div>
	        <?php } ?>
	        
	        <?php if (!empty($invoice_found)) { ?>
	            <a href="<?php echo AppUrl::bicesUrl('uploads/orders/invoices/' . $order_id . '/' . $invoice_found); ?>" target="order_invoice">
		       		<button type="button" class="btn btn-success">
		       			View Invoice
		       		</button>
	        	</a>
	        <?php } ?>
        <?php } else { 
                
                if(!empty($order_invoice_save)){
          ?>
            <a href="javascript:void(0);" onclick="$('#order_form').submit();$('#order_invoice_form').submit();">
                <button type="button"  class="btn btn-success">
                    <?php if (isset($order_id) && $order_id) echo 'Save Order'; else echo 'Add Order'; ?>
                </button>
            </a>
        <?php }else { ?>
          
               
                    <?php if(!empty($order['order_status'])  &&  strtolower(trim($order['order_status'])) == strtolower(trim('pending'))) {?>
                      <a href="javascript:void(0);" onclick="$('#order_status').val('Submitted');$('#order_form').submit();"><input type="submit" class="btn btn-success" value="Submit for Approval" name="expense_status"></button></a>



                        <?php }else if (isset($order_id) && $order_id) { ?>
                        <a href="javascript:void(0);" onclick="$('#order_form').submit();">
                          <input type="submit" class="btn btn-primary">Save Order </button></a>
                        <?php } 
                        else {?>
                          <a href="javascript:void(0);" onclick="$('#order_status').val('Pending');$('#order_form').submit();"><input type="submit" class="btn btn-primary" value="Save Order and Not Submit" name="expense_status" /></a>

                       <a href="javascript:void(0);" onclick="$('#order_status').val('Submitted');$('#order_form').submit();"><input type="submit" class="btn btn-success" value="Submit for Approval" name="expense_status"></button></a>
                       
                          <?php } ?>
           
           
        <?php }} ?>
        
    </div>


    <div class="clearfix"> <br /><br /><br /></div>
    <div class="row tile_count"><hr style="border: 1px solid #c7c7c7;margin:0px;" ><hr style="border: 1px solid #c7c7c7;margin-top:0px;" /></div>
    <?php if(isset($order_id) && !empty($order_id)) { ?>

    <div id="related_orders">
        <table id="order_table" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th colspan="7"> <h3>Related Orders</h3></th>
            </tr>
            <tr>
                <th>Order ID</th>
                <th>Supplier</th>
                <th>Date</th>
                <th>Amount in Tool Currency</th>
                <th>Amount in Order Currency</th>
                <th>User</th>
                <th style="width: 100px;">Status</th>
            </tr>
            </thead>

            <tbody>

            <?php
            if(count($related_orders)>0){
                foreach ($related_orders as $order) { ?>
                    <tr>
                        <td style="text-align: right;"><a href="<?php echo AppUrl::bicesUrl('orders/edit/' . $order['order_id']); ?>"><?php echo $order['order_id']; ?></a></td>
                        <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a><br/></td>
                        <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                        <td style="text-align: right;">
                            <nobr>
                                <?php
                                switch ($order['currency']) {
                                    case 'GBP' : $currency = '&#163;';
                                        break;
                                    case 'USD' : $currency = '$';
                                        break;
                                    case 'CAD' : $currency = '$';
                                        break;
                                    case 'CHF' : $currency = '&#8355;';
                                        break;
                                    case 'EUR' : $currency = '&#8364;';
                                        break;
                                    case 'JPY' : $currency = '&#165;';
                                        break;
                                    case 'INR' : $currency = '&#x20B9;';
                                        break;
                                    default :  $currency = '&#163;';
                                        break;
                                }
                                echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($order['total_price'], 2); ?>
                            </nobr>
                        </td>
                        <td style="text-align: right;">
                            <nobr>
                                <?php
                                echo $currency . ' ' . number_format($order['calc_price'], 2);
                                ?>
                            </nobr>
                        </td>

                        <td><a href="<?php echo AppUrl::bicesUrl('users/edit/' . $order['user_id']); ?>"><?php echo $order['user']; ?></a></td>
                        <td>
                            <?php
                            if (empty($order['order_status'])) $order['order_status'] = 'Pending';
                            $status_style = "";

                            if ($order['order_status'] == 'Paid' || $order['order_status'] == 'Closed' || $order['order_status'] == 'Received')
                                $status_style = ' style="border: 1px solid green !important; background-color: green !important; color: #fff !important;" ';
                            if ($order['order_status'] == 'Declined')
                                $status_style = ' style="border: 1px solid #990000 !important; background-color: #990000 !important; color: #fff !important;" ';
                            if ($order['order_status'] == 'More Info Needed')
                                $status_style = ' style="border: 1px solid #CC3333 !important; background-color: #CC3333 !important; color: #fff !important;" ';
                            if ($order['order_status'] == 'Pending' || $order['order_status'] == 'Ordered - PO Not Sent To Supplier'
                                || $order['order_status'] == 'Submitted' || $order['order_status'] == 'Ordered - PO Sent To Supplier')
                                $status_style = ' style="border: 1px solid orange !important; background-color: orange !important; color: #fff !important;" ';
                            if(!empty($order['description'])){
                                $toolTip = $order['description'];
                            }  else{
                                $toolTip = 'N/A';
                            }

                            ?>
                            <button class="btn btn-sm btn-success status" title="<?php echo $toolTip; ?>" <?php echo $status_style; ?>>
                                <?php echo $order['order_status']; ?>
                            </button>
                        </td>
                    </tr>

                <?php }
            } else { ?>
                <tr>
                    <td colspan="7">No Related Order Found</td>
                </tr>
            <?php } ?>

            </tbody>

        </table>
    </div>
    <?php } ?>

	<?php if (false && isset($purchase_order) && $purchase_order && is_array($purchase_order) && isset($purchase_order['order_id']) && $purchase_order['order_id'] == $order_id) { ?>
	    <div class="alert alert-info fade in col-md-10 col-md-offset-1" role="alert" style="margin-top: 10px;">
	        <div style="text-align: center; font-size: 110%; color: #fff;">
	        	A Purchase order <a style="color: #333; text-decoration: underline;" href="<?php echo AppUrl::bicesUrl('purchaseOrders/edit/' . $purchase_order['po_id']); ?>" target="_procure_purchase_order">#<?php echo $purchase_order['po_number']; ?></a> has already been created.
	        	<a style="color: #333; text-decoration: underline; cursor: pointer;" onclick="$('#order_status_modal').modal('show');">Click here</a> to change the status of the order and linked purchase order.
	        </div>
	    </div>            	
	    <div class="clearfix"> </div>
	<?php } ?>		            
    
</div>

</div>

<?php if (isset($order) && $order && isset($order['order_status'])) {

    ?>
<div class="modal fade" id="order_status_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Change Order Status</h4>
        </div>
       	<form role="form" class="form-horizontal" name="order_status_modal_form" id="order_status_modal_form" method="post" action="<?php echo AppUrl::bicesUrl('orders/updateOnlyStatus'); ?>">
        <div class="modal-body">
          <p>
          		<div class="form-group">
          			<div class="col-md-6">
	          			Change Order Status To:
	          		</div>
	          		<div class="col-md-6"> 
   	      				<?php
   	      					$new_statuses = array();
   	      					if ($order['order_status'] == 'Cancelled') $new_statuses = array('Cancelled');
   	      					if ($order['order_status'] == 'Approved')
								$new_statuses = array('Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier', 'Cancelled');
   	      					if ($order['order_status'] == 'Ordered - PO Sent To Supplier' || $order['order_status'] == 'Ordered - PO Not Sent To Supplier')
								$new_statuses = array($order['order_status'], 'Received');
   	      					if ($order['order_status'] == 'Received')
								$new_statuses = array($order['order_status'], 'Invoice Received');
   	      					if ($order['order_status'] == 'Invoice Received')
								$new_statuses = array($order['order_status'], 'Paid');
   	      				?>
    	      			<select name="order_status_from_modal" id="order_status_from_modal" class="form-control">
    	      				<?php foreach ($new_statuses as $new_status) { ?>
    	      					<option value="<?php echo $new_status; ?>"><?php echo $new_status; ?></option>
    	      				<?php } ?>
    	      			</select>
    	      		</div>
    	       </div>
    	       <input type="hidden" name="order_id_from_modal" id="order_id_from_modal" value="<?php echo $order_id; ?>" />
    	       <input type="hidden" name="po_id_from_modal" id="po_id_from_modal" value="<?php if (isset($purchase_order['po_id'])) echo $purchase_order['po_id']; ?>" />
    	       <input type="hidden" name="modal_form_submitted" id="modal_form_submitted" value="1" />
          </p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-info btn btn-default" data-dismiss="modal">
          	  Cancel
          </button>
          <button id="yes_delete" type="button" class="alert-success btn btn-default" onclick="$('#order_status_modal_form').submit();">
          	  Change Status
          </button>
        </div>
   	   </form>
      </div>
	</div>
</div>
<?php } ?>

<div class="modal fade" id="order_approve_inovice" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header alert-info">
                <h4 class="modal-title">Approve Invoice</h4>
            </div>
            <form role="form" class="form-horizontal" name="order_approve_inovice_form" id="order_approve_inovice_form" method="post" action="<?php echo AppUrl::bicesUrl('orders/moveSupplierInvoices'); ?>">
                <div class="modal-body">
                    <p>
                    Are you sure ? you want to approve invoice and move supplier invoice to order
                    </p>
                    <input type="hidden" name="order_id_from_modal" id="order_id_from_modal" value="<?php echo $order_id; ?>" />
                     <input type="hidden" name="modal_form_submitted" id="modal_form_submitted" value="1" />
                </div>
                <div class="modal-footer">
                    <button id="no_delete" type="button" class="alert-info btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button id="yes_delete" type="button" class="alert-success btn btn-default" onclick="$('#order_approve_inovice_form').submit();">
                        Approve Inovice
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" name="user_currency_symbol" id="user_currency_symbol" value="<?php echo Yii::app()->session['user_currency_symbol']; ?>" />

<div class="modal fade" id="invoice_number_invalid_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Upload Invoices</h4>
        </div>
        <div class="modal-body">
          <p>Invoice number is a mandatory field.</p>
        </div>
        <div class="modal-footer">
          <button id="ok_invoice_message" type="button" class="alert-info btn btn-default" data-dismiss="modal">
          	  Ok
          </button>
        </div>
      </div>
	</div>
</div>


<script type="text/javascript">
$(document).ready( function() {

	<?php if (isset($order['location_id']) && !empty($order['location_id'])) { ?>

		<?php if (isset($order['department_id']) && !empty($order['department_id'])) { ?>
    loadDepartmentsForSingleLocation(<?php echo $order['department_id']; ?>);
		<?php } else { ?>
    loadDepartmentsForSingleLocation(0);
		<?php } ?>

	<?php } ?>


	<?php 
		if (isset(Yii::app()->session['order_status_changed_id']) && Yii::app()->session['order_status_changed_id']) 
		{ 
	?>
	
			$('#alert-info').show();
			setTimeout(function() { $("#alert-info").hide(); }, 7000);
	
	<?php 
			Yii::app()->session['order_status_changed_id'] = 0;
			unset(Yii::app()->session['order_status_changed_id']);
		} 
	?>		

    $('#order_form').on('submit', function() {
        $('#order_status').removeAttr('disabled');
    });

    $('input').on('ifChanged', function(event){
        calculateTotalPrice();
    });


    $( "#order_form" ).validate( {
        rules: {
            location_id: "required",
            department_id: "required",
            vendor_name: "required",
            order_date: "required"

        },
        messages: {
            location_id: "Location is required",
            department_id: "Department is required",
            vendor_name: "Supplier Name is required",
            order_date: "Order Date is required"
        },
	    submitHandler: function(form) {
	    	var product_id = 0;
	    	$('.product_quantity').each(function() {
	    		if ($.trim($(this).val()) == "") {
			        product_id = $(this).closest('.form-group').find('.product_id').val();
			        if (product_id == '0') $(this).val('0');
	    			else $(this).val('1');
	    		}
	    	});
	    	calculateTotalPrice();
		    form.submit();
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

            //$("#department_id-error, #location_id-error, #vendor_name-error, #order_date-error").css({"color":"#a94442"});

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
			//$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error error" );
			//$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field",
        "onRemove": function() { calculateTotalPrice(); }
    });

	$('#order_date').daterangepicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});
   
//    $('#vendor_name').devbridgeAutocomplete({
//        serviceUrl: '<?php //echo AppUrl::bicesUrl('orders/getVendors'); ?>//',
//        onSelect: function(suggestion) { $('#vendor_id').val(suggestion.data); },
//        onSearchComplete: function(query, suggestions) { if (!suggestions.length) $('#vendor_id').val(0); }
//    });

    createProductAutocompleteSearch();
    createVendorAutocompleteSearch();

    $('body').on('click', "input[name^='product_name']", function() {
        createProductAutocompleteSearch();
    });

    $('body').on('click', "input[name^='vendor_name']", function() {
        createVendorAutocompleteSearch();
    });

    $('.price_calc').keyup(function() { calculateTotalPrice(); });
    $('.product_tax').keyup(function() { calculateTotalTax(); });
    $('.product_shipping').keyup(function() { calculateTotalShipping(); });

    $(document).on("blur","input",function() {
     if($(this).hasClass('input_price_by_user')){
    	var my_quantity = $(this).closest('.form-group').find("input[name^='quantity']").val();
    	if (!$.isNumeric(my_quantity)) 
    	{
    		my_quantity = 1;
    		$(this).closest('.form-group').find("input[name^='quantity']").val(1);
    	}
    	
    	var my_price = $(this).val();
    	if (!$.isNumeric(my_price)) my_price = 0;
    	
    	var my_unit_price = parseFloat(my_price / my_quantity); 
    	$(this).closest('.form-group').find("input[name^='unit_price_hidden']").val(my_unit_price);

		calculateTotalPrice();
  }
    });

    $('.product_type').change(function() {
        $(this).closest('.form-group').find("input[name^='product_name']").devbridgeAutocomplete().setOptions({
            params: { product_type: $(this).val() }
        });

        $(this).closest('.form-group').find(".display_price").html("");
        $(this).closest('.form-group').find('.product_id').val(0);
        $(this).closest('.form-group').find("input[name^='product_name']").val("");
        $(this).closest('.form-group').find('.unit_price').val("");
        $(this).closest('.form-group').find('.unit_price_hidden').val(0);
        $(this).closest('.form-group').find('.product_quantity').val("");
        calculateTotalPrice();
    });
});

function loadCurrencyRate()
{
    var currency_id = $('#currency_id').val();

    $.ajax({
        type: "POST", data: { currency_id: currency_id }, dataType: "json",
        url: BICES.Options.baseurl + '/orders/getCurrencyRates',
        success: function(options) {
            $('#currency_rate').val(options);
        },
        error: function() { $('#currency_rate').html(1); }
    });
}

function createProductAutocompleteSearch()
{
    var product_type = '';
    $("input[name^='product_name']").each(function () {
        product_type = $(this).closest('.form-group').find('.product_type').val();
        $(this).devbridgeAutocomplete({
            serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getProducts'); ?>',
            params: { product_type: product_type },
            onSelect: function(suggestion) {
                $(this).closest('.form-group').find('.product_id').val(suggestion.data.id);
                $(this).closest('.form-group').find('.unit_price').val(suggestion.data.price);
                $(this).closest('.form-group').find('.unit_price_hidden').val(suggestion.data.price);
                $(this).closest('.form-group').find('.display_price').html('@ ' + suggestion.data.price + ' / Each');
                $(this).closest('.form-group').find('.product_quantity').val(1);

                $('.price_calc').keyup(function() { calculateTotalPrice(); });
                $('.product_tax').keyup(function() { calculateTotalTax(); });
                $('.product_shipping').keyup(function() { calculateTotalShipping(); });
                calculateTotalPrice();
            }
        });
    });
}

function createVendorAutocompleteSearch()
{
    $("input[name^='vendor_name']").each(function () {
        $(this).devbridgeAutocomplete({
            serviceUrl: '<?php echo AppUrl::bicesUrl('orders/getVendors'); ?>',
            onSelect: function(suggestion) {
                $(this).closest('.form-group').find('.vendor_id').val(suggestion.data);
            }
        });
    });
}

function calculateTotalTax()
{

    var price = 0;
    var product_tax = 0;
    var total_tax_price = 0;

    $('.product_tax').each(function() {

        product_tax = $(this).val();
        price = $(this).closest('.form-group').find('.unit_price').val();
        if ($.trim(product_tax) == '' || !$.isNumeric(product_tax)) product_tax = 0;
        total_tax_price += Number((product_tax*price)/100);

        $('.total_tax_amount').val(total_tax_price);
    })

    calculateTotalPrice();

}


function calculateTotalShipping()
{

    var product_shipping = 0;
    var total_shipping_price = 0;

    $('.product_shipping').each(function(index, val) {

        product_shipping = $(this).val();
        if ($.trim(product_shipping) == '' || !$.isNumeric(product_shipping)) product_shipping = 0;

        total_shipping_price += Number(product_shipping);
        $('.total_shipping_amount').val(total_shipping_price);
    })

    calculateTotalPrice();

}

function calculateTotalPrice()
{
    var total_price = 0;
    var quantity = 0;
    var price = 0;
    var display_price = 0;
    var product_id = 0;
    var currencay_rate = 0;

    $('.product_quantity').each(function() {
        quantity = $(this).val();
        price = $(this).closest('.form-group').find('.unit_price_hidden').val();
        product_id = $(this).closest('.form-group').find('.product_id').val();
        currencay_rate = document.getElementById('currency_rate').value;

        
        if ($.trim(price) == '' || !$.isNumeric(price)) price = 0;
        if ($.trim(quantity) == '' || !$.isNumeric(quantity)) quantity = 0;

        display_price = parseFloat(quantity) * parseFloat(price);
        total_price += currencay_rate*display_price;
        //$(this).closest('.form-group').find('.unit_price').val(display_price.toFixed(2));
    })

    //$('#tax_amount').val('');
    //if ($('#tax_flag').is(':checked'))
   // {
        //var tax_rate = $.trim($('#tax_rate').val());
        //if ($.isNumeric(tax_rate)) tax_rate = parseFloat(tax_rate);
        //else tax_rate = 0;

        //var tax_amount = total_price * (tax_rate / 100);
        //$('#tax_amount').val(tax_amount.toFixed(2));
        $('#tax_amount').val();
        var tax_amount = $.trim($('#tax_amount').val());
        if ($.isNumeric(tax_amount)) tax_amount = parseFloat(tax_amount);
        else tax_amount = 0;
        total_price += currencay_rate*tax_amount;
    //}

        var ship_amount = $.trim($('#ship_amount').val());
        if ($.isNumeric(ship_amount)) ship_amount = parseFloat(ship_amount);
        else ship_amount = 0;
        total_price += ship_amount;
        alert(total_price);
    $('#total_price').val(total_price.toFixed(2));
    $('#total_price_display').html($('#user_currency_symbol').val() + total_price.toFixed(2));
}

function deleteOrderFile(file_idx, file_name)
{
	$('#delete_order_file_link_' + file_idx).confirmation({
	  title: "Are you sure you want to delete the attached file?",
	  singleton: true,
	  placement: 'right',
	  popout: true,
	  onConfirm: function() {
	  	$('#existing_file_id_' + file_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('orders/deleteFile/'); ?>",
            data: { order_id: $('#order_id').val(), file_name: file_name }
        });
	  },
	  onCancel: function() {  }
	});
	
}

function deleteOrderInvoice(invoice_idx, file_name)
{
	$('#delete_order_invoice_link_' + invoice_idx).confirmation({
	  title: "Are you sure you want to delete the attached invoice?",
	  singleton: true,
	  placement: 'right',
	  popout: true,
	  onConfirm: function() {
	  	$('#existing_invoice_id_' + invoice_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('orders/deleteInvoice/'); ?>",
            data: { order_id: $('#order_id').val(), file_name: file_name }
        });
	  },
	  onCancel: function() {  }
	});
	
}


function uploadReceipts()
{
	$('#files_only_upload').val(1);
	$('#order_form').submit();	
}


function uploadInvoices()
{
	if ($.trim($('#invoice_number').val()) == "")
	{
		$('#invoice_number_invalid_modal').modal('show');
		return false;
	}
	else $('#order_invoice_form').submit();	
}

function loadDepartmentsForSingleLocation(department_id)
{
    var location_id = $('#location_id').val();
    var single = 1;

    if (location_id == 0)
        $('#department_id').html('<option value="">Select Department</option>');
    else
    {
        $.ajax({
            type: "POST", data: { location_id: location_id,single:single }, dataType: "json",
            url: BICES.Options.baseurl + '/locations/getDepartments',
            success: function(options) {
                var options_html = '<option value="">Select Department</option>';
                for (var i=0; i<options.length; i++)
                    options_html += '<option value="' + options[i].department_id + '">' + options[i].department_name + '</option>';
                $('#department_id').html(options_html);
                if (department_id != 0) $('#department_id').val(department_id);
            },
            error: function() { $('#department_id').html('<option value="">Select Department</option>'); }
        });
    }
}


function checkLocationDepartmentPermission()
{
    var location_id = $('#location_id').val();
    var department_id = $('#department_id').val();
    var options_html = '';
    attr = $('#order_status').attr('disabled');
    

    if (attr == undefined) {

    $.ajax({
            type: "POST", data: { location_id: location_id,department_id:department_id,field_name:"approve_order" }, dataType: "json",
            url: BICES.Options.baseurl + '/users/checkPermission',
            success: function(options) {
                //var options_html = '<option value="">Select Department</option>';
             
                for (var i=0; i<options.length; i++)
                   
                  options_html += '<option value="' + options[i] + '">' + options[i] + '</option>';
                $('#order_status').html(options_html);
                //if (department_id != 0) $('#department_id').val(department_id);*/
            },
          //  error: function() { $('#department_id').html('<option value=""></option>'); }
        });
    }

}


function addDocument()
{
    var total_documents = $('#total_documents').val();
    total_documents = parseInt(total_documents);
    total_documents = total_documents + 1;
    $('#total_documents').val(total_documents);

    var new_document_html = $('#new_document_code').html();
    new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
    $('#total_documents').before(new_document_html);
}

function deleteDocument(docidx)
{
    var display_document_count = 0;
    $("div[id^='document_area']").each(function () {
        if ($(this).is(':visible')) display_document_count += 1;
    });

    if (display_document_count <= 1) alert("You must have at least one document field in the order form");
    else
    {
        if (confirm("Are you sure you want to delete this document?"))
        {
            $('#delete_document_flag_' + docidx).val(1);
            $('#document_area_' + docidx).hide();
            $('#end_document_area_' + docidx).hide();
        }
    }
}

</script>

<style type="text/css">
  .btn-file {
    padding:6px !important;
}
</style>
