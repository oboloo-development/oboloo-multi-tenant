<?php
//print_r($location_id);die;
$order_statuses = array( 'Pending', 'Approved', 'Cancelled','Closed','Submitted', 'Received', 'Paid', 'Invoice Received', 'Declined', 'Ordered - PO Sent To Supplier','Ordered - PO Not Sent To Supplier'); ?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count col-md-12">
        <div class="span6 pull-left">
            <h3>Orders</h3>
        </div>

        <div class="span6 pull-right">
            <button type="button" class="btn btn-warning" onclick="$('#create_order').modal('show');">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Order ( New )
            </button>

            <a href="<?php echo AppUrl::bicesUrl('orders/edit/0'); ?>">
                <button type="button" class="btn btn-warning">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Order
                </button>
            </a>

            <a href="<?php echo AppUrl::bicesUrl('orders/export'); ?>">
                <button type="button" class="btn btn-success">
                    <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export CSV
                </button>
            </a>
        </div>

        <div class="clearfix"> </div><br />
    </div>
<div class="clearfix"> </div>
	<form class="form-horizontal" id="order_list_form" name="order_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('orders/list'); ?>">
		<div class="form-group">
          <div class="col-md-1" style="width:10.5%">
              <input type="text" class="form-control" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date("d/m/Y", strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> >
          </div>
			
          <div class="col-md-1" style="width:10.5%">
              <input type="text" class="form-control" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date("d/m/Y", strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> >
          </div>

			<div class="col-md-2 location">
				<select name="location_id[]" id="location_id"  class="form-control" multiple searchable="Search here.."  onchange="loadDepartments(0);">
					<?php
                    $i = 0 ;
                    foreach ($locations as $location) {
                        if(isset($location_id[$i])){
                        ?>
						<option value="<?php echo $location['location_id']; ?>"
								<?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
							<?php echo $location['location_name']; ?>
						</option>
                        <?php } else { ?>
                            <option value="<?php echo $location['location_id']; ?>">
                                <?php echo $location['location_name']; ?>
                            </option>
                        <?php } ?>
					<?php
                    $i++;
                    } ?>
				</select>
			</div>

			<div class="col-md-2 department">
				<select name="department_id[]" id="department_id" class="form-control" multiple searchable="Search here..">
					<option value="0">All Departments</option>
                    <?php if(!empty($department_info))
                        foreach($department_info as $dept_value){?>
                        <option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
                    <?php } ?>
                    
				</select>
			</div>
			<div class="col-md-2 status">
				<select name="order_status[]" id="order_status" multiple class="form-control">
					<?php
                    $i = 0 ;
                    foreach ($order_statuses as $order_status_display) {
                    if(isset($order_status[$i])){
                        ?>
						<option value="<?php echo $order_status_display; ?>"
								<?php if ($order_status[$i] == $order_status_display) echo ' selected="SELECTED" '; ?>>
							<?php echo $order_status_display; ?>
						</option>
                        <?php } else { ?>
                        <option value="<?php echo $order_status_display; ?>">
                            <?php echo $order_status_display; ?>
                        </option>
                        <?php }
					  $i++;
                    } ?>
				</select>
			</div>
            <div class="col-md-2">
                <select name="spend_type" id="spend_type" class="form-control">
                    <option value="">Select Spend Type</option>
                    <option  <?php if (isset($spend_type) && $spend_type == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                    <option  <?php if (isset($spend_type) && $spend_type == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
                </select>
            </div>
			<div class="col-md-1  text-right">
				<button class="btn btn-primary" onclick="$('#order_list_form').submit();">Search Orders</button>
			</div>
		</div>
	</form>
<div class="clearfix"> </div>
<div class="row tile_count"> </div>
<div class="col-md-12">
    <?php if(Yii::app()->user->hasFlash('success')):?>
        <div class="alert alert-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?></div>
    <table id="order_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th> </th>
          <th>Order ID</th>
          <th>Supplier</th>
          <th>Date</th>
          <th>Type</th>
          <th>Amount in Tool Currency</th>
          <th>Amount in Order Currency</th>
          <th>User</th>
          <th style="width: 100px;"> Supplier Status</th>
          <th style="width: 100px;">Status</th>
        </tr>
      </thead>

      <tbody>

          <?php foreach ($orders as $order) { ?>
              <tr>
                    <td>
                        <a href="<?php echo AppUrl::bicesUrl('orders/edit/' . $order['order_id']); ?>">
                            <button class="btn btn-sm btn-success">View/Edit</button>
                        </a>
                    </td>
                    <td style="text-align: right;"><?php echo $order['order_id']; ?></td>
                    <td><a href="<?php echo AppUrl::bicesUrl('vendors/edit/' . $order['vendor_id']); ?>"><?php echo $order['vendor']; ?></a><br/></td>
                    <td><?php echo date("F j, Y", strtotime($order['order_date'])); ?></td>
                    <td><?php echo $order['spend_type']; ?></td>
                    <td style="text-align: right;">
                    	<nobr>
                    		<?php
                            switch ($order['currency']) {
                                case 'GBP' : $currency = '&#163;';
                                    break;
                                case 'USD' : $currency = '$';
                                    break;
                                case 'CAD' : $currency = '$';
                                    break;
                                case 'CHF' : $currency = '&#8355;';
                                    break;
                                case 'EUR' : $currency = '&#8364;';
                                    break;
                                case 'JPY' : $currency = '&#165;';
                                    break;
                                case 'INR' : $currency = '&#x20B9;';
                                    break;
                                default :  $currency = '&#163;';
                                    break;
                            }

                            $tool_currency = Yii::app()->session['user_currency'];

                            $sql = "select sum(calc_unit_price) as total_price,sum(calc_tax) as total_calc_tax,sum(shipping_cost) as total_shipping_cost,quantity from order_details where order_id=".$order['order_id'];
                            $detail_order = Yii::app()->db->createCommand($sql)->queryRow();

                            $totla_order_price = ($detail_order['total_price']*$detail_order['quantity'])+$detail_order['total_calc_tax']+$detail_order['total_shipping_cost'];

                            if(!empty($order['currency_id'])){
                            $currency_rate = new CurrencyRate();
                            $currency_rate = $currency_rate->getOne(array('id' => $order['currency_id']));
                            if(isset($currency_rate) && !empty($currency_rate['currency'])){
                                $currency = $currency_rate['currency'];
                                
                                $currencySymbol=FunctionManager::showCurrencySymbol(strtolower($currency));
                            }
                        }else{
                            $currencySymbol='';
                        }
                          


                            echo Yii::app()->session['user_currency_symbol'] . ' '.number_format(FunctionManager::priceInGBP($totla_order_price,$currency)*FunctionManager::currencyRate($tool_currency) , 2); ?>
                    	</nobr>
                    </td>
                    <td style="text-align: right;">
                      <nobr>
                        <?php

                        
                          
                          echo $currencySymbol . '' . number_format(($detail_order['total_price']*$detail_order['quantity'])+$detail_order['total_calc_tax']+$detail_order['total_shipping_cost'], 2);
                          ?>
                          <?php
                          //echo $currency . ' ' . number_format($order['calc_price'], 2);
                          ?>
                      </nobr>
                  </td>

                    <td><a href="<?php echo AppUrl::bicesUrl('users/edit/' . $order['user_id']); ?>"><?php echo $order['user']; ?></a></td>
                    <td>
                        <?php $order_vendor_detail = new OrderVendorDetail();
                        $order_vendor_detail = $order_vendor_detail->getData(array('order_id' =>  $order['order_id']));
                        if(isset($order_vendor_detail[0]['status']) && !empty($order_vendor_detail[0]['status'])){

                            if ($order_vendor_detail[0]['status'] == 'Cannot Fulfil')
                                $vendor_status_style = 'danger';
                            else
                                $vendor_status_style = 'success';

                            ?>

                        <button class="btn btn-sm btn-<?php echo $vendor_status_style; ?> status">
                            <?php echo $order_vendor_detail[0]['status']; ?>
                        </button>
                        <?php } else { ?>
                            N/A
                        <?php } ?>
                    </td>
                    <td>
                    	<?php
                    		if (empty($order['order_status'])) $order['order_status'] = 'Pending';
                    		$status_style = "";
							
							if ($order['order_status'] == 'Paid' || $order['order_status'] == 'Closed' || $order['order_status'] == 'Received')
								$status_style = ' style="border: 1px solid green !important; background-color: green !important; color: #fff !important;" ';
							if ($order['order_status'] == 'Declined')
								$status_style = ' style="border: 1px solid #990000 !important; background-color: #990000 !important; color: #fff !important;" ';								
							if ($order['order_status'] == 'More Info Needed')
								$status_style = ' style="border: 1px solid #CC3333 !important; background-color: #CC3333 !important; color: #fff !important;" ';								
							if ($order['order_status'] == 'Pending' || $order['order_status'] == 'Ordered - PO Not Sent To Supplier' 
										|| $order['order_status'] == 'Submitted' || $order['order_status'] == 'Ordered - PO Sent To Supplier')
								$status_style = ' style="border: 1px solid orange !important; background-color: orange !important; color: #fff !important;" ';
                            if ($order['order_status'] == 'Cancelled')
                            $status_style = ' style="border: 1px solid grey !important; background-color: grey !important; color: #fff !important;" ';

                            if($order['order_status'] == 'Pending') {
                                $toolTip = "Order has been created by " . $order['user'] . " however not submitted for approval.";
                            }
                            elseif($order['order_status'] == 'Submitted'){
                                $toolTip = "Order has been submitted  by ".$order['user']." and is awaiting approval.";
                            }
                            elseif($order['order_status'] == 'More Info Needed'){
                                $toolTip = "Approver has requested more information from the order creator.";
                            }
                            elseif($order['order_status'] == 'Declined'){
                                $toolTip = "Approver has declined the order. Please read approver notes.";
                            }
                            elseif($order['order_status'] == 'Approved'){
                                $toolTip = "Order has been approved and awaiting Purchase Order creation.";
                            }
                            elseif($order['order_status'] == 'Ordered - PO Not Sent To Supplier'){
                                $toolTip = "Purchase Order created but not sent to supplier.";
                            }
                            elseif($order['order_status'] == 'Ordered - PO Sent To Supplier'){
                                $toolTip = "Purchase Order created and sent to supplier.";
                            }
                            elseif($order['order_status'] == 'Invoice Received'){
                                $toolTip = "Invoice has been received by supplier and awaiting payment.";
                            }
                            elseif($order['order_status'] == 'Paid'){
                                $toolTip = "Order has been paid .";
                            }
                            elseif($order['order_status'] == 'Received'){
                                $toolTip = "Order has been received.";
                            }
                            elseif($order['order_status'] == 'Cancelled'){
                                $toolTip = "Order has been cancelled.";
                            }
                            elseif($order['order_status'] == 'Closed'){
                                $toolTip = "Order has been closed.";
                            }
                            else{
                                $toolTip = 'N/A';
                            }

                    	?>
                    	<button class="btn btn-sm btn-success status" title="<?php echo $toolTip; ?>" <?php echo $status_style; ?>>
                    		<?php echo $order['order_status']; ?>
                    	</button>
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>
<style>
    .ui-tooltip {
        width: 200px;
        text-align: center;
        box-shadow: none;
        padding: 0;
    }
    .ui-tooltip-content {
        position: relative;
        padding: 0.5em;
    }
    .ui-tooltip-content::after, .ui-tooltip-content::before {
        content: "";
        position: absolute;
        border-style: solid;
        display: block;
        left: 90px;
    }
    .bottom .ui-tooltip-content::before {
        bottom: -10px;
        border-color: #AAA transparent;
        border-width: 10px 10px 0;
    }
    .bottom .ui-tooltip-content::after {
        bottom: -7px;
        border-color: white transparent;
        border-width: 10px 10px 0;
    }
    .top .ui-tooltip-content::before {
        top: -10px;
        border-color: #AAA transparent;
        border-width: 0 10px 10px;
    }
    .top .ui-tooltip-content::after {
        top: -7px;
        border-color: white transparent;
        border-width: 0 10px 10px;
    }
    .location  .multiselect {
        width: 138%;
    }
    .status  .multiselect {
        width: 100%;
    }
    .multiselect-selected-text{
        float: left;
        margin-left: 0px;
    }

    .btn .caret {
        float: right;
        margin-top: 10px;

    }
    .department .dropdown-toggle{
           width: 112%;
    }

</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

    function getOptions(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Locations',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }

    function getOptions2(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Departments',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }


    function getOptions1(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Status Values',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }
    $('#location_id').multiselect(getOptions(true));
    $('#department_id').multiselect(getOptions2(true));
    $('#order_status').multiselect(getOptions1(true));

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );


$.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
	locale: {
	    format: 'DD/MM/YYYY'
	}
  });
};

$(document).ready( function() {
    $('#order_table').dataTable({
        "columnDefs": [ 
        	{ "targets": 0, "width": "6%", "orderable": false },
			{ "type": "sort-month-year", targets: 3 }         
        ],
        "order": []
    });

	$('#from_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$('#to_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

    $( ".status" ).tooltip({
        position: {
            my: "center bottom",
            at: "center top-10",
            collision: "flip",
            using: function( position, feedback ) {
                $( this ).addClass( feedback.vertical )
                    .css( position );
            }
        }
    });

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			//loadDepartments(<?php //echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>
		
});
</script>

<?php $this->renderPartial('create');?>