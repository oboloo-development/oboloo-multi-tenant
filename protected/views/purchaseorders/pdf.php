<table border="0" cellpadding="2" cellspacing="2" style="width: 700px;">
	<tr>
		<td>
			<?php if(!empty($company['logo'])) { ?>
		    <img src="<?php echo 'uploads/companies/'.$company['logo'];?>" width="200" height="120">
			<?php } else { ?>
				<img src="images/no_logo.jpg" width="250" height="120">
			<?php } ?>
		</td>
		<td style="text-align: right; color: #334249; font-size: 250%; font-weight: bold;">
			PURCHASE ORDER
		</td>
	</tr>
</table>

<table border="0" cellpadding="2" cellspacing="2" style="width: 700px;">
	<tr>
		<td style="vertical-align: top;">

			Company Number: <?php echo $company['company_number']; ?>
			<br />
			Tax Number: <?php echo $company['tax_number']; ?>
			<br />
			Phone: <?php echo $company['phone_number']; ?>
			<br />
			Website: <?php echo $company['company_website']; ?>
		</td>
		
		<td style="text-align: right; vertical-align: top;">
			DATE: <?php echo date("M j, Y"); ?>
			<br />
			PO #: <?php echo $purchase_order['po_number']; ?>
			<br />
			Payment Terms: <?php echo !empty($payment_terms['value']) ? $payment_terms['value'] : '-'; ?>
			<br />
		</td>
	</tr>
</table>
<br /><br />

<table border="0" cellpadding="2" cellspacing="2" style="width: 700px;">
	<tr>
		<td style="width: 50%; vertical-align: top;">
			<div style="background-color: #334249; color: white; font-weight: bold;">
				&nbsp;&nbsp;&nbsp;&nbsp;
				VENDOR
			</div>
			<br />
			<?php echo $vendor['vendor_name']; ?>
			<br />
			<?php echo $vendor['contact_name']; ?>
			<br />
			<?php echo $vendor['address_1']; ?>
			<br />
			<?php echo $vendor['city'] . ' ' . $vendor['state'] . ' ' . $vendor['zip']; ?>
			<br />
			Phone: <?php echo $vendor['phone_1']; ?>
			<br />
			Fax: <?php echo $vendor['fax']; ?>
		</td>

		<td style="width: 50%; vertical-align: top;">
			<div style="background-color: #334249; color: white; font-weight: bold;">
				&nbsp;&nbsp;&nbsp;&nbsp;
				SHIP TO
			</div>
			<br />
			<?php echo $location['contact_person']; ?>
			<br />
			<?php echo $location['location_name']; ?>
			<br />
			<?php echo $location['ship_address']; ?>
			<br />
			<?php echo $location['ship_city'] . ' ' . $location['ship_state'] . ' ' . $location['ship_zip_code']; ?>
			<br />
			Phone: <?php echo $location['phone']; ?>
		</td>
	</tr>
</table>
<br /><br />

<table border="0" cellpadding="2" cellspacing="2" style="width: 700px;">
	<thead>
		<tr>
			<th style="background-color: #334249; color: white; width: 400px;"> ITEM </th>
			<th style="background-color: #334249; color: white; text-align: right; width: 100px;"> QUANTITY &nbsp;&nbsp;</th>
			<th style="background-color: #334249; color: white; text-align: right; width: 100px;"> UNIT PRICE &nbsp;&nbsp;</th>
			<th style="background-color: #334249; color: white; text-align: right; width: 100px;"> TOTAL &nbsp;&nbsp;</th>
		</tr>
	</thead>	
	<tbody>
		<?php

			if(!empty($order_info['currency_id'])){
                $currency_rate = new CurrencyRate();
                $currency_rate = $currency_rate->getOne(array('id' => $order_info['currency_id']));
                if(isset($currency_rate) && !empty($currency_rate['currency'])){
                    $currency = $currency_rate['currency'];
                    
                    $currencySymbol=FunctionManager::showCurrencySymbol(strtolower($currency));
                }
            }else{
                $currencySymbol='';
            }
			$total = 0; 
			foreach ($order_details as $order_item) 
			{
				//$sub_total =  round($order_item['calc_unit_price']+$order_item['calc_tax']+$order_item['shipping_cost'], 2);
				$sub_total =  round($order_item['calc_unit_price'],2);
				$total += $sub_total;
		?>
				<tr>
					<td style="width: 400px;">
						<?php 
							echo $order_item['product_name'];
							if ($order_item['product_type'] == 'Bundle') echo ' (Bundle)'; 
						?>
					</td>
					<td style="text-align: right; width: 100px;"><?php echo number_format($order_item['quantity'], 0); ?></td>
					<td style="text-align: right; width: 100px;"><?php 
					echo $currencySymbol. number_format(round($order_item['calc_unit_price'], 2), 2); ?></td>
					<td style="text-align: right; width: 100px; background-color: #34ff99;"><?php echo $currencySymbol . number_format($sub_total, 2) . '&nbsp;'; ?></td>
				</tr>
		<?php 
			} 
			
			$tax = $total * ($order_info['tax_rate'] / 100);
			
			$po_discount = $purchase_order['po_discount'];
			$po_other_charges = $purchase_order['po_other_charges'];
			$other = round($po_other_charges, 2);

			$sub_total = $total + $order_info['tax_rate'] + $order_info['ship_amount'] + $other;
			$grand_total = $sub_total - $po_discount;
		?>

		<tr><td colspan="4"><br /><hr /><br /></td></tr>
			
		<tr>
			<td style="vertical-align: top; width: 400px; border: 1px #777 solid;">
				<div style="background-color: #334249; color: white; font-weight: bold; font-size: 110%;">
					&nbsp;&nbsp;&nbsp;&nbsp;
					Comments
				</div>
				<br />
				<?php echo str_replace("\n", "<br />", $purchase_order['po_notes']); ?>
			</td>
			
			<td> </td>

			<td colspan="2" style="vertical-align: top;">
				<table border="0" cellpadding="2" cellspacing="2" style="width: 200px;">
					<tr>
						<td style="text-align: right; width: 95px;"> SUBTOTAL </td>
						<td style="text-align: right; width: 100px;"><?php echo $currencySymbol . number_format($total, 2); ?></td>
					</tr>
			
					<tr>
						<td style="text-align: right;"> TAX </td>
						<td style="text-align: right;"><?php echo $currencySymbol . number_format($order_info['tax_rate'], 2); ?></td>
					</tr>
					
					<tr>
						<td style="text-align: right;"> SHIPPING </td>
						<td style="text-align: right;"><?php echo $currencySymbol . number_format($order_info['ship_amount'], 2); ?></td>
					</tr>
			
					<tr>
						<td style="text-align: right;"> OTHER </td>
						<td style="text-align: right;">
							<?php
								if ($other >= 0) echo $currencySymbol . number_format($other, 2);
								else echo '(' . $currencySymbol . number_format(abs($other), 2) . ')';
							?>
						</td>
					</tr>

<!--					<tr>-->
<!--						<td style="border-top: 2px solid #c7c7c7; text-align: right; font-weight: normal;"> SUBTOTAL </td>-->
<!--						<td style="border-top: 2px solid #c7c7c7; text-align: right; font-weight: bold; font-size: 120%;">--><?php //echo Yii::app()->session['user_currency_symbol'] . number_format($sub_total, 2); ?><!--</td>-->
<!--					</tr>-->

					<tr>
						<td style="text-align: right;"> DISCOUNT </td>
						<td style="text-align: right;">
							<?php
							if ($po_discount >= 0) echo $currencySymbol . number_format($po_discount, 2);
							else echo '(' . $currencySymbol . number_format(abs($purchase_order['po_discount']), 2) . ')';
							?>
						</td>
					</tr>
			
					<tr>
						<td style="border-top: 2px solid #c7c7c7; text-align: right; font-weight: bold; font-size: 120%;"> TOTAL </td>
						<td style="border-top: 2px solid #c7c7c7; text-align: right; font-weight: bold; font-size: 120%;"><?php echo $currencySymbol . number_format($grand_total, 2); ?></td>
					</tr>
				</table>				
			</td>
		</tr>	
	</tbody>
</table>