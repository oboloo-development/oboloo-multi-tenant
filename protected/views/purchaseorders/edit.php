<div class="right_col" role="main">
<?php $shipAmount= $taxAmount=0;

  $orderID = $order_info['order_id'];
  $currencySymbol=FunctionManager::orderCurrSymbol($orderID);
?>
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>

                <?php
                    if (isset($po_id) && $po_id)
                    {
                        echo 'Purchase Order ';
                        if (isset($purchase_order) && is_array($purchase_order) && isset($purchase_order['po_id']) && !empty($purchase_order['po_id']))
                            echo ' - #' . $purchase_order['po_id'];

                        echo ' -> Order - #'.$order_info['order_id'];

                    }
                    else echo 'Add Purchase Order';


                ?>

            </h3>
        </div>

        <div class="span6 pull-right">

            <?php  if (isset($purchase_order['po_status']) && $purchase_order['po_status'] == 'PO Not Sent To Supplier') { ?>

                <a target="pdf_po_<?php echo $purchase_order['po_id']; ?>" href="<?php echo AppUrl::bicesUrl('purchaseOrders/pdf/?po_id=' . $purchase_order['po_id']); ?>">
                    <button type="button" class="btn btn-warning">
                        View Purchase Order
                    </button>
                </a>
                <a href="javascript:void(0);">
                    <button type="button" class="btn btn-primary" onclick="updatePOApproval();">
                        Send PO to Supplier
                    </button>
                </a>
            <?php } elseif (isset($purchase_order['po_status']) && $purchase_order['po_status'] == 'PO Sent To Supplier')  { ?>

                <a target="pdf_po_<?php echo $purchase_order['po_id']; ?>" href="<?php echo AppUrl::bicesUrl('purchaseOrders/pdf/?po_id=' . $purchase_order['po_id']); ?>">
                    <button type="button" class="btn btn-warning">
                        View Purchase Order
                    </button>
                </a>
                <a href="javascript:void(0);" onclick="$('#po_status').removeAttr('disabled'); $('#purchase_order_form').submit();">
                    <button type="button" class="btn btn-primary">
                        Save Purchase Order
                    </button>
                </a>
            <?php } elseif (isset($purchase_order['po_status']) && $purchase_order['po_status'] == 'Paid')  { ?>

                <a target="pdf_po_<?php echo $purchase_order['po_id']; ?>" href="<?php echo AppUrl::bicesUrl('purchaseOrders/pdf/?po_id=' . $purchase_order['po_id']); ?>">
                    <button type="button" class="btn btn-warning">
                        View Purchase Order
                    </button>
                </a>
            <?php } ?>

            <a href="<?php echo AppUrl::bicesUrl('orders/edit/' . $order_info['order_id']); ?>">
                <button type="button" class="btn btn-default" style="background-color: #12A79C;color: #fff;">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Go to Order
                </button>
            </a>

            <a href="<?php echo AppUrl::bicesUrl('purchaseOrders/list'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Purchase Order List
                </button>
            </a>


        </div>

        <div class="clearfix"> </div>
    </div>

<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs">
  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
    <li role="presentation" class="active">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="po-tab" role="tab" data-toggle="tab" aria-expanded="true">
            Purchase Order
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content2" role="tab" id="order-tab" data-toggle="tab" aria-expanded="false">
            Related Order
        </a>
    </li>
  </ul>

  <form id="purchase_order_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="<?php echo AppUrl::bicesUrl('purchaseOrders/edit'); ?>">

  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="po-tab">
      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">PO Number <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control has-feedback-left" name="po_number" id="po_number"
                    <?php if (isset($purchase_order['po_number']) && !empty($purchase_order['po_number'])) echo 'value="' . $purchase_order['po_number'] . '"'; else echo 'placeholder="PO Number"'; ?> >
              <span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
          </div>

		  <?php
		  		$editable_po_values = true;
				if (isset($purchase_order['po_status']) && $purchase_order['po_status'] != 'Pending')
					$editable_po_values = false;
				
		  		$po_status_values = array();
				if (isset($purchase_order['po_status']))
				{
                    //echo $purchase_order['po_status'].'uuuuu';die;
					switch($purchase_order['po_status'])
					{
						case 'Pending' : 
							$po_status_values = array( 'Pending', 'Purchased', 'Cancelled', 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier', 'Received', 'Invoice Received' );
							break;							
						case 'Purchased' :
							$po_status_values = array('Purchased', 'Received' );
							break;							
						case 'Received' : 
							$po_status_values = array( 'Received', 'Invoice Received' );
							break;							
						case 'Invoice Received' : 
							$po_status_values = array( 'Invoice Received', 'Paid' );
							break;							
						case 'Paid' : 
							$po_status_values = array( 'Paid', 'Closed' );
							break;
                        case 'PO Not Sent To supplier' :
                            $po_status_values = array( 'PO Not Sent To supplier');
                            break;
                        case 'PO Sent To supplier' :
                            $po_status_values = array( 'PO Sent To supplier');
                            break;
						default : 
							$po_status_values = array( $purchase_order['po_status'] );
							break;							
					}
				}
				else $po_status_values = array( 'Pending' );
		  ?>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">PO Status<span style="color: #a94442;"></span></label>
              <select name="po_status" id="po_status" class="form-control" disabled="disabled">
                  <?php foreach ($po_status_values as $a_po_status) { ?>
                      <option value="<?php echo $a_po_status; ?>"
                            <?php if (isset($purchase_order['po_status']) && $a_po_status == $purchase_order['po_status']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $a_po_status; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
      </div>

      <div class="form-group">
        <div class="col-md-2 col-sm-2 col-xs-4 date-input">
            <label class="control-label">Additional Charges<span style="color: #a94442;"></span></label>
            <input style="text-align: right;" type="text" class="form-control"
            	<?php if (!$editable_po_values) echo ' readonly="readonly" '; ?> id="po_other_charges" name="po_other_charges" placeholder="Additional Charges"
                    <?php if (isset($purchase_order['po_other_charges']) && $purchase_order['po_other_charges']) echo 'value="' . number_format($purchase_order['po_other_charges'], 2) . '"';  ?> />
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4 date-input">
            <label class="control-label">Discount<span style="color: #a94442;"></span></label>
            <input style="text-align: right;" type="text" class="form-control"
            	<?php if (!$editable_po_values) echo ' readonly="readonly" '; ?> id="po_discount" name="po_discount" placeholder="Discount"
                    <?php if (isset($purchase_order['po_discount']) && $purchase_order['po_discount']) echo 'value="' . number_format($purchase_order['po_discount'], 2) . '"';  ?> />
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4 date-input">
            <label class="control-label">Total Payment<span style="color: #a94442;"></span></label>
            <input style="text-align: right;" type="text" class="form-control" readonly="readonly" id="po_total" name="po_total" placeholder="Total Payment"
                    <?php if (isset($purchase_order['po_total']) && $purchase_order['po_total']) echo 'value="' . number_format($purchase_order['po_total'], 2) . '"';  ?> />
            <input type="hidden" name="order_payment" id="order_payment" value="<?php if (isset($purchase_order['order_payment'])) echo $purchase_order['order_payment']; else echo '0'; ?>" />
        </div>
      </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Purchase Order notes<span style="color: #a94442;"></span></label>
              <textarea class="form-control" name="po_notes" id="po_notes" rows="3" <?php if (!isset($purchase_order['po_notes']) || empty($purchase_order['po_notes'])) echo 'placeholder="Purchase Order notes"'; ?>><?php if (isset($purchase_order['po_notes']) && !empty($purchase_order['po_notes'])) echo $purchase_order['po_notes']; ?></textarea>
          </div>
      </div>

      <div class="clearfix"> <br /> </div>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>Upload Documents</h4>
          </div>
      </div>     
      <div class="clearfix"> </div>

		<?php 
		    // integer starts at 0 before counting
		    $existing_files = array(); 
		    $upload_dir = 'uploads/purchase_orders/';
			if ($po_id)
			{
				if (!is_dir('uploads/purchase_orders')) mkdir('uploads/purchase_orders');
				if (!is_dir('uploads/purchase_orders/' . $po_id)) 
					mkdir('uploads/purchase_orders/' . $po_id);
				$upload_dir = 'uploads/purchase_orders/' . $po_id . '/';
			    if ($handle = opendir($upload_dir)) {
			        while (($uploaded_file = readdir($handle)) !== false){
			            if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file)) 
			                $existing_files[] = $uploaded_file;
			        }
			    }
			}
		?>
		
		<?php if (is_array($existing_files) && count($existing_files)) { ?>

		      <div class="form-group">
		          <div class="col-md-6 col-sm-6 col-xs-12">
		              <h5>Existing Purchase Order Files</h5>
		          </div>
		      </div>
		      
	          <div class="col-md-6 col-sm-6 col-xs-12">
		      <table class="table">
		      <?php $file_idx = 1; foreach ($existing_files as $uploaded_file) { ?>
		      	<tr id="existing_file_id_<?php echo $file_idx; ?>">
		      		<td>
                        <a href="<?php echo AppUrl::bicesUrl('uploads/purchase_orders/' . $po_id . '/' . $uploaded_file); ?>" target="order_file">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
                        &nbsp;
						<a id="delete_purchase_order_file_link_<?php echo $file_idx; ?>" style="cursor: pointer;" 
								onclick="deletePurchaseOrderFile(<?php echo $file_idx; ?>, '<?php echo $uploaded_file; ?>');">
						   <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</a>                        
                       </a>
		      		</td>
		      		<td>
		      			<?php echo $uploaded_file; ?>
		      		</td>
		      	</tr>
		      <?php $file_idx += 1; } ?>
		      </table>
		      </div>
		           
		      <div class="clearfix"> <br /> </div>
		
        <?php } ?>
        
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input multiple="multiple" class="file" name="file[]" id="file" type="file" />
    	</div>
    </div>
    <div class="clearfix"> </div>

<?php if (isset($approvals) && is_array($approvals) && count($approvals)) { ?>
      <div class="clearfix"> <br /> </div>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>Approval Routing</h4>
          </div>
      </div>     
      <div class="clearfix"> </div>

      <?php
      		$order_is_already_approved = false;
			if (isset($order_info) && is_array($order_info) && isset($order_info['order_status']))
			{
				if ($order_info['order_status'] != "Pending" && $order_info['order_status'] != "More Info Needed" && 
						$order_info['order_status'] != "Submitted" && $order_info['order_status'] != "Cancelled" && $order_info['order_status'] != "Declined")
					$order_is_already_approved = true;
			}
      ?>
            
      <div class="col-md-6 col-sm-6 col-xs-12">
      <table class="table">
      		<thead><tr><th>User</th><th>Date</th><th>Status</th><th>Comments</th></tr></thead>
      		<?php 
      			foreach ($approvals as $an_approval) 
      			{ 
      				if ($order_is_already_approved && $an_approval['routing_status'] != 'Approved') continue; 
      		?>
	      			<tr>
	      				<td><?php echo $an_approval['user']; ?></td>
	      				<td><?php echo date("F j, Y", strtotime($an_approval['routing_date'])); ?></td>
	      				<td>
	      					<?php 
	      						if ($an_approval['routing_status'] != 'Information') echo $an_approval['routing_status'];
								else echo 'More Info Needed'; 
	      					?>
	      				</td>
	      				<td><?php echo $an_approval['routing_comments']; ?></td>
	      			</tr>
      		<?php 
				} 
			?>
      </table>
	  </div>      
      
      <div class="clearfix"> <br /> </div>
<?php } ?>


	<div class="form-group">
	    <div class="col-md-6 col-sm-6 col-xs-12">
	        <?php if (isset($purchase_order['po_status']) && $purchase_order['po_status'] == 'Cancelled') { ?>
	            <div class="alert alert-warning fade in" role="alert" style="margin-top: 20px;">
	                <span style="font-size: 120%;">This purchase order is already cancelled and hence cannot be edited.</span>
	            </div>
	        <?php
            } else if (isset($purchase_order['po_status']) && $purchase_order['po_status'] == 'Pending') {  ?>

                <a href="javascript:void(0);">
                    <button type="submit" name="create_po" value="1" class="btn btn-primary">
                        Create Purchase Order
                    </button>
                </a>
	        <?php } //elseif(isset($purchase_order['po_status']) && $purchase_order['po_status'] == 'PO Not Sent To Supplier') { ?>

<!--                <a target="pdf_po_--><?php //echo $purchase_order['po_id']; ?><!--" href="--><?php //echo AppUrl::bicesUrl('purchaseOrders/pdf/?po_id=' . $purchase_order['po_id']); ?><!--">-->
<!--                    <button type="button" class="btn btn-warning">-->
<!--                        View Purchase Order-->
<!--                    </button>-->
<!--                </a>-->
<!--	            <a href="javascript:void(0);">-->
<!--	                <button type="button" class="btn btn-primary" onclick="updatePOApproval();">-->
<!--                        Send PO to Supplier-->
<!--	                </button>-->
<!--	            </a>-->
	        <?php //} else { ?>

<!--                <a target="pdf_po_--><?php //echo $purchase_order['po_id']; ?><!--" href="--><?php //echo AppUrl::bicesUrl('purchaseOrders/pdf/?po_id=' . $purchase_order['po_id']); ?><!--">-->
<!--                    <button type="button" class="btn btn-warning">-->
<!--                        View Purchase Order-->
<!--                    </button>-->
<!--                </a>-->
<!--                <a href="javascript:void(0);" onclick="$('#po_status').removeAttr('disabled'); $('#purchase_order_form').submit();">-->
<!--                    <button type="button" class="btn btn-primary">-->
<!--                        Save Purchase Order-->
<!--                    </button>-->
<!--                </a>-->
            <?php //} ?>
	    </div>
	    <div class="clearfix"> <br /><br /> </div>
	</div>

	</div>
    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="order-tab">
    	
<!--    <div class="alert alert-info fade in col-md-6" role="alert" style="margin-top: 5px;">-->
<!--        <div style="text-align: center; font-size: 110%; color: #fff;">-->
<!--        	This PO is created from <a style="color: #333; text-decoration: underline;" href="--><?php //echo AppUrl::bicesUrl('orders/edit/' . $order_info['order_id']); ?><!--">Order #--><?php //echo $order_info['order_id']; ?><!--</a>. Click the order number link to view the order.-->
<!--        </div>-->
<!--    </div>            	-->
    <div class="clearfix"> </div>

      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Location <span style="color: #a94442;"></span></label>
              <select name="location_id" id="location_id" class="form-control" disabled="disabled">
                  <option value="">Select Location</option>
                  <?php foreach ($locations as $location) { ?>
                      <option value="<?php echo $location['location_id']; ?>"
                            <?php if (isset($order_info['location_id']) && $location['location_id'] == $order_info['location_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $location['location_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Department <span style="color: #a94442;"></span></label>
              <select name="department_id" id="department_id" class="form-control" disabled="disabled">
                  <option value="">Select Department</option>
                  <?php foreach ($departments as $department) { ?>
                      <option value="<?php echo $department['department_id']; ?>"
                            <?php if (isset($order_info['department_id']) && $department['department_id'] == $order_info['department_id']) echo ' selected="SELECTED" '; ?>>
                          <?php echo $department['department_name']; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
      </div>

      <div class="form-group">
          <div class="col-md-4 col-sm-4 col-xs-8 date-input">
              <label class="control-label">Supplier Name <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control has-feedback-left" name="vendor_name" id="vendor_name" readonly="readonly"
                    <?php if (isset($order_info['vendor']) && !empty($order_info['vendor'])) echo 'value="' . $order_info['vendor'] . '"'; else echo 'placeholder="Supplier Name"'; ?> >
              <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($order_info['vendor']) && !empty($order_info['vendor'])) echo $order_info['vendor_id']; else echo '0'; ?>" />
              <span class="fa fa-user-circle-o form-control-feedback left" aria-hidden="true"></span>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-4 date-input">
              <label class="control-label">Order Date<span style="color: #a94442;"></span></label>
              <input type="text" class="form-control has-feedback-left" name="order_date" id="order_date" readonly="readonly"
                    <?php if (isset($order_info['order_date']) && !empty($order_info['order_date'])) echo 'value="' . date("d/m/Y", strtotime($order_info['order_date'])) . '"'; else echo 'placeholder="Order Date"'; ?> >
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
          </div>
	  </div>

      <div class="form-group">
          <?php
          	$record_user_id = 0;
			if (isset(Yii::app()->session['user_id'])) $record_user_id = Yii::app()->session['user_id'];
			if (isset($order_info['user_id']) && !empty($order_info['user_id'])) $record_user_id = $order_info['user_id'];  
          	
            $applicable_order_statuses = array('Pending', 'Approved', 'Closed', 'Declined', 'Ordered - PO Not Sent To Supplier', 'Received', 'Submitted', 'Ordered - PO Sent To Supplier', 'Invoice Received', 'Ordered - PO Not Sent To Supplier');
            if (!isset($order_info['order_status']) || $order_info['order_status'] == 'Pending')
                $applicable_order_statuses = array('Pending', 'Submitted');
          ?>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">User Name<span style="color: #a94442;"></span></label>
              <select class="form-control" name="user_id" id="user_id" disabled="disabled">
                <option value="<?php echo $order_info['user_id']; ?>"
                <?php if ($record_user_id == $order_info['user_id']) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $order_info['user_name']; ?>
                </option>
              </select>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Order Status<span style="color: #a94442;"></span></label>
              <select class="form-control" name="order_status" id="order_status" disabled="disabled"
                        <?php if (isset($order_info['order_status']) && $order_info['order_status'] != 'Pending') echo ' disabled="disabled" '; ?>>
                  <?php foreach ($applicable_order_statuses as $order_info_status) { ?>
                      <option value="<?php echo $order_info_status; ?>"
                                <?php if (isset($order_info['order_status']) && $order_info['order_status'] == $order_info_status) echo ' SELECTED="SELECTED" '; ?>>
                          <?php echo $order_info_status; ?>
                      </option>
                  <?php } ?>
              </select>
          </div>
      </div>
      <div class="clearfix"> </div>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 date-input">
              <label class="control-label">Order description/notes<span style="color: #a94442;"></span></label>
              <textarea  readonly="readonly" class="form-control" name="description" id="description" rows="4" <?php if (!isset($order_info['description']) || empty($order_info['description'])) echo 'placeholder="Order description/notes"'; ?>><?php if (isset($order_info['description']) && !empty($order_info['description'])) echo $order_info['description']; ?></textarea>
          </div>
      </div>
      <div class="clearfix"> <br /> </div>

      <?php
        $existing_items_found = false;
        if (isset($order_details) && is_array($order_details) && count($order_details))
            $existing_items_found = true;
      ?>

      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12">
              <h4>Order Items</h4>
          </div>
      </div>

      <?php
        $total_price = 0;
        if (isset($order_details) && is_array($order_details) && count($order_details))
        {
      ?>

          <?php
            foreach ($order_details as $order_detail)
            {
              if (!isset($order_detail['quantity']) || empty($order_detail['quantity'])) $order_detail['quantity'] = 1;
              if (!isset($order_detail['calc_unit_price']) || empty($order_detail['calc_unit_price'])) $order_detail['calc_unit_price'] = 0;

              if(!empty($order_detail['calc_tax'])){
                $taxAmount += $order_detail['calc_tax'];
              }
              if(!empty($order_detail['shipping_cost'])){
                $shipAmount += $order_detail['shipping_cost'];
              }
          ?>

              <div class="form-group">
                  <div class="col-md-2 col-sm-2 col-xs-4 date-input">
                      <label class="control-label">Product Type<span style="color: #a94442;"></span></label>
                      <select class="form-control product_type" name="product_type[]" disabled="disabled">
                          <option value="Bundle"
                                <?php if (isset($order_detail['product_type']) && $order_detail['product_type'] == 'Bundle') echo ' selected="SELECTED" '; ?>>
                              Bundle
                          </option>
                          <option value="Product"
                                <?php if (isset($order_detail['product_type']) && $order_detail['product_type'] == 'Product') echo ' selected="SELECTED" '; ?>>
                              Product
                          </option>
                      </select>
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-8 date-input">
                      <label class="control-label">Product Name<span style="color: #a94442;"></span></label>
                      <input type="text" class="form-control has-feedback-left price_calc" name="product_name[]" readonly="readonly"
                            <?php if (isset($order_detail['product_name']) && !empty($order_detail['product_name'])) echo 'value="' . $order_detail['product_name'] . '"'; ?> >
                      <input class="product_id" type="hidden" name="product_id[]" value="<?php echo $order_detail['product_id']; ?>" />
                      <span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
                  </div>
                  <div class="clearfix"> <br /><br /> </div>

                  <div class="col-md-2 col-sm-2 col-xs-4 date-input">
                      <label class="control-label">Quantity<span style="color: #a94442;"></span></label>
                      <input style="text-align: right;" type="text" class="product_quantity form-control price_calc" name="quantity[]" readonly="readonly"
                            <?php if (isset($order_detail['quantity']) && !empty($order_detail['quantity'])) echo 'value="' . $order_detail['quantity'] . '"'; ?> >
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4 display_price informational_messages_area" style="padding: 8px; font-size: 120%;">
                      <?php
                            if (isset($order_detail['calc_unit_price']) && !empty($order_detail['calc_unit_price'])
                                    && isset($order_detail['quantity']) && !empty($order_detail['quantity']))
                            {
                                echo '@ ';
                                echo number_format($order_detail['calc_unit_price'] / $order_detail['quantity'], 2);
                                echo ' / Each';
                            }
                       ?>
                  </div>

                  <div class="col-md-2 col-sm-2 col-xs-4">
                      <input style="text-align: right;" type="text" class="unit_price form-control" name="unit_price[]" readonly="readonly"
                            <?php if (isset($order_detail['calc_unit_price']) && !empty($order_detail['calc_unit_price'])) echo 'value="' . $order_detail['calc_unit_price'] . '"'; ?> >
                      <input type="hidden" class="unit_price_hidden" name="unit_price_hidden[]"
                              <?php if (isset($order_detail['calc_unit_price']) && !empty($order_detail['calc_unit_price']) && isset($order_detail['quantity']) && !empty($order_detail['quantity'])) echo 'value="' . number_format($order_detail['calc_unit_price'] / $order_detail['quantity'], 2) . '"'; ?> >
                  </div>
              </div>

          <?php $total_price += $order_detail['calc_unit_price']; } ?>

      <?php
            }

            /*$tax_amount = $taxAmount;*/
            //$tax_rate = 20;
            /*if (isset($order_info['tax_rate']) && $order_info['tax_rate'] && $total_price != 0)
            {
                $tax_rate = $order_info['tax_rate'];
                $tax_amount = ($order_info['tax_rate'] * $total_price) / 100.0;
                $total_price += $tax_amount;
            }

            if (isset($order_info['ship_amount']) && $order_info['ship_amount'])
                $total_price += $order_info['ship_amount'];*/
            $total_price += $taxAmount;
            $total_price += $shipAmount;
      ?>

      <div class="clearfix"> <br /> </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" name="total_price" id="total_price" value="<?php echo number_format($total_price, 2); ?>" />
            <input type="hidden" name="po_id" id="po_id" value="<?php if (isset($po_id)) echo $po_id; ?>" />
            <input type="hidden" name="order_id" id="order_id" value="<?php if (isset($purchase_order['order_id'])) echo $purchase_order['order_id']; else echo '0'; ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-4 col-sm-4 col-xs-8 date-input">
            <label class="control-label">Tax Rate<span style="color: #a94442;"></span></label>
            <!-- <span style="font-size: 130%; margin-right: 10px;">TAX</span>
            <input type="checkbox" class="flat" name="tax_flag" id="tax_flag" readonly="readonly"
                <?php if (isset($order_info['tax_rate']) && $order_info['tax_rate']) echo ' checked="CHECKED" '; ?> />
            Check here to apply tax @ -->
            <input type="text" readonly="readonly" style="width: 60px; text-align: right; display: inline !important;" onkeyup="calculateTotalPrice();" class="form-control price_calc" name="tax_rate" id="tax_rate" value="<?php echo !empty($order_details[0]['tax'])?$order_details[0]['tax']:0; ?>" /> %
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4 date-input">
            <label class="control-label">Tax Amount<span style="color: #a94442;"></span></label>
            <input style="text-align: right;" type="text" class="form-control"
                    id="tax_amount" name="tax_amount" placeholder="Tax Amount" readonly="readonly"
                        <?php if (isset($taxAmount) && $taxAmount) echo 'value="' . $taxAmount . '"';  ?> />
        </div>
        <div class="clearfix"> </div>
    </div>

    <div class="form-group">
        <div class="col-md-4 col-sm-4 col-xs-8">
            <h4 style="padding-top: 10px;">Shipping Costs <span style="font-size: 70%;">(if applicable)</span></h4>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4 date-input">
            <label class="control-label">Shipping Amount<span style="color: #a94442;"></span></label>
            <input style="text-align: right;" type="text" class="form-control price_calc" onkeyup="calculateTotalPrice();"
                    id="ship_amount" name="ship_amount" placeholder="Shipping Amount" readonly="readonly"
                        <?php if (isset($shipAmount) && $shipAmount) echo 'value="' .$shipAmount. '"';  ?> />
        </div>
        <div class="clearfix"> </div>
    </div>

	<div class="form-group">
	    <div class="col-md-4 col-sm-4 col-xs-8">
	        <h4>Order Total</h4>
	    </div>
	    <div class="col-md-2 col-sm-2 col-xs-4">
	        <h4 style="text-align: right;" id="total_price_display">
	            <?php $display_final_price = false; $final_grand_total_price = $total_price; echo  $currencySymbol . number_format($total_price, 2); ?>
	        </h4>
	    </div>
	    <div class="clearfix"> </div>
	</div>

	<?php if (isset($order_info['discount']) && !empty($order_info['discount'])) { ?>
		<div class="form-group">
		    <div class="col-md-4 col-sm-4 col-xs-8">
		        <h4>Discount (PO Level)</h4>
		    </div>
		    <div class="col-md-2 col-sm-2 col-xs-4">
		        <h4 style="text-align: right;">
		            <?php $display_final_price = true; $final_grand_total_price = $final_grand_total_price - $order_info['discount']; echo  $currencySymbol . number_format($order_info['discount'], 2); ?>
		        </h4>
		    </div>
		    <div class="clearfix"> </div>
		</div>
	<?php } ?>
	
	<?php if (isset($order_info['other_charges']) && !empty($order_info['other_charges'])) { ?>
		<div class="form-group">
		    <div class="col-md-4 col-sm-4 col-xs-8">
		        <h4>Other Charges (PO Level)</h4>
		    </div>
		    <div class="col-md-2 col-sm-2 col-xs-4">
		        <h4 style="text-align: right;">
		            <?php $display_final_price = true; $final_grand_total_price = $final_grand_total_price + $order_info['other_charges']; echo  $currencySymbol . number_format($order_info['other_charges'], 2); ?>
		        </h4>
		    </div>
		    <div class="clearfix"> </div>
		</div>
	<?php } ?>
	
	<?php if ($display_final_price) { ?>
		<div class="form-group">
		    <div class="col-md-4 col-sm-4 col-xs-8">
		        <h4>Final Price (PO Level)</h4>
		    </div>
		    <div class="col-md-2 col-sm-2 col-xs-4">
		        <h4 style="text-align: right;">
		            <?php echo  $currencySymbol . number_format($final_grand_total_price, 2); ?>
		        </h4>
		    </div>
		    <div class="clearfix"> </div>
		</div>
<?php } ?>
</div>
</div>
</form>
</div>

<?php if (isset($purchase_order['po_status']) && $purchase_order['po_status'] == 'PO Not Sent To Supplier') { ?>
<div class="modal fade" id="po_edit_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Send Purchase Order To Supplier</h4>
        </div>
       	<form role="form" class="form-horizontal" name="po_status_modal_form" id="po_edit_modal_form" method="post" action="">
        <div class="modal-body">
          <p>
          		<div class="form-group">
          			<div class="col-md-8">
	          			<h4>Send the purchase order to:</h4>
	          		</div>
	          		<div class="clearfix"> <br /><br /> </div>
	          		<div class="col-md-10 col-md-offset-1">
	          			<div class="col-md-4"><strong>Supplier:</strong></div> 
	          			<div class="col-md-8"><?php echo $po_vendor['vendor_name']; ?>
                  <input type="hidden" value="<?php echo $po_vendor['vendor_id']; ?>" id="vendor_id" name="vendor_id">
                  </div>
                  <div class="clearfix"> &nbsp;</div>
                  <div class="col-md-4"><strong>Contact:</strong></div>
                  <div class="col-md-8"><input type="text" value="<?php echo $po_vendor['contact_name']; ?>" id="contact_name" name="contact_name"></div>
                  <div class="clearfix"> &nbsp;</div>
	          			<div class="col-md-4"><strong>Address:</strong></div> 
	          			<div class="col-md-8"><input type="text" value=" <?php echo $po_vendor['address_1']; ?>" id="address_1" name="address_1"></div>
	          			<div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>City:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_vendor['city']; ?>" id="city" name="city"></div>
                        <div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>State:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_vendor['state']; ?>" id="state" name="state"></div>
                        <div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>Zip:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_vendor['zip']; ?>" id="zip" name="zip"></div>
                        <div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>Phone:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_vendor['phone_1']; ?>" id="phone_1" name="phone_1"></div>
                        <div class="clearfix"> &nbsp;</div>


                        <div class="col-md-4"><strong>Email:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_vendor['emails']; ?>" id="emails" name="emails" readonly></div>
                        <div class="clearfix"> &nbsp;</div>


    	      		</div>
          			<div class="clearfix"> </div> 
    	      		
    	      		<div class="row tile_count">
			            <div class="col-md-10 col-md-offset-1"><hr style="border: 1px solid #c7c7c7;" /></div>
					</div>
          			<div class="clearfix"> </div> 
					
          			<div class="col-md-8">
	          			<h4>Ship to:</h4>
	          		</div>
	          		<div class="clearfix"> <br /><br /> </div>
	          		<div class="col-md-10 col-md-offset-1">

                        <div class="col-md-4"><strong>Location:</strong></div>
                        <div class="col-md-8">
                            <input type="hidden" value="<?php echo $po_location['location_id']; ?>" id="location_id" name="location_id">
                            <input type="text" value=" <?php echo $po_location['location_name']; ?>" id="location_name" name="location_name">
                        </div>
                        <div class="clearfix"> &nbsp;</div>

	          			<div class="col-md-4"><strong>Contact:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_location['contact_person']; ?>" id="contact_person" name="contact_person"></div>
                        <div class="clearfix"> &nbsp;</div>

	          			<div class="col-md-4"><strong>Address:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_location['ship_address']; ?>" id="ship_address" name="ship_address"></div>
                        <div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>City:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_location['ship_city']; ?>" id="ship_city" name="ship_city"></div>
                        <div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>State:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_location['ship_state']; ?>" id="ship_state" name="ship_state"></div>
                        <div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>Zip:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_location['ship_zip_code']; ?>" id="ship_zip_code" name="ship_zip_code"></div>
                        <div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>Phone:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_location['phone']; ?>" id="phone" name="phone"></div>
                        <div class="clearfix"> &nbsp;</div>

                        <div class="col-md-4"><strong>Email:</strong></div>
                        <div class="col-md-8"><input type="text" value=" <?php echo $po_location['email']; ?>" id="email" name="email"></div>
                        <div class="clearfix"> &nbsp;</div>
    	      		</div>
	          		<div class="clearfix"> </div>
    	       </div>
    	       <input type="hidden" name="po_id_from_modal" id="po_id_from_modal" value="<?php if (isset($purchase_order['po_id'])) echo $purchase_order['po_id']; ?>" />
    	       <input type="hidden" name="modal_form_confirm" id="modal_form_confirm" value="1" />
          </p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-info btn btn-default" data-dismiss="modal">
          	  Cancel
          </button>
            <button type="button" class="btn btn-primary" onclick="confirmPOApproval();">
               Confirm
            </button>
        </div>
   	   </form>
      </div>
	</div>
</div>
<?php } ?>

<?php if (isset($purchase_order['po_status']) && $purchase_order['po_status'] == 'PO Not Sent To Supplier') { ?>
        <div class="modal fade" id="po_send_modal" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header alert-info">
                        <h4 class="modal-title">Send Purchase Order To Supplier</h4>
                    </div>
                    <form role="form" class="form-horizontal" name="po_confirm_status_modal_form" id="po_confirm_status_modal_form" method="post" action="<?php echo AppUrl::bicesUrl('purchaseOrders/updateOnlyStatus'); ?>">
                        <div class="modal-body">
                            <p>
                            <div class="form-group">
                                <div class="col-md-8">
                                    <h4>Send the purchase order to:</h4>
                                </div>
                                <div class="clearfix"> <br /><br /> </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="col-md-4"><strong>Supplier:</strong></div>
                                    <div class="col-md-8"><?php echo $po_vendor['vendor_name']; ?></div>
                                    <div class="clearfix"> </div>

                                    <div class="col-md-4"><strong>Contact:</strong></div>
                                    <div class="col-md-8"><span id="show_contact_name"></span></div>
                                    <div class="clearfix"> </div>

                                    <div class="col-md-4"><strong>Address:</strong></div>
                                    <div class="col-md-8"><span id="show_address_1"></div>
                                    <div class="clearfix"> </div>

                                    <div class="col-md-4"><strong>&nbsp;</strong></div>
                                    <div class="col-md-8"> <span id="show_city"></span> <span id="show_state"></span> <span id="show_zip"></span> </div>
                                    <div class="clearfix"> </div>

                                    <div class="col-md-4"><strong>Phone:</strong></div>
                                    <div class="col-md-8"> <span id="show_phone_1"></div>
                                    <div class="clearfix"> </div>
                                     <div class="col-md-4"><strong>Email:</strong></div>
                                    <div class="col-md-8"> <span id="show_supplier_email"></div>
                                    <div class="clearfix"> </div>
                                </div>
                                <div class="clearfix"> </div>

                                <div class="row tile_count">
                                    <div class="col-md-10 col-md-offset-1"><hr style="border: 1px solid #c7c7c7;" /></div>
                                </div>
                                <div class="clearfix"> </div>

                                <div class="col-md-8">
                                    <h4>Ship to:</h4>
                                </div>
                                <div class="clearfix"> <br /><br /> </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="col-md-4"><strong>Location:</strong></div>
                                    <div class="col-md-8"><span id="show_location_name"></div>
                                    <div class="clearfix"> </div>

                                    <div class="col-md-4"><strong>Contact:</strong></div>
                                    <div class="col-md-8"><span id="show_contact_person"></div>
                                    <div class="clearfix"> </div>

                                    <div class="col-md-4"><strong>Address:</strong></div>
                                    <div class="col-md-8"><span id="show_ship_address"></div>
                                    <div class="clearfix"> </div>

                                    <div class="col-md-4"><strong>&nbsp;</strong></div>
                                    <div class="col-md-8"> <span id="show_ship_city"></span> <span id="show_ship_state"></span> <span id="show_ship_zip"></span> </div>
                                    <div class="clearfix"> </div>

                                    <div class="col-md-4"><strong>Phone:</strong></div>
                                    <div class="col-md-8"><span id="show_ship_phone"></div>
                                    <div class="clearfix"> </div>
                                    <div class="col-md-4"><strong>Email:</strong></div>
                                    <div class="col-md-8"><span id="show_ship_email"></div>
                                    <div class="clearfix"> </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <input type="hidden" name="po_confirm_id_from_modal" id="po_confirm_id_from_modal" value="<?php if (isset($purchase_order['po_id'])) echo $purchase_order['po_id']; ?>" />
                            <input type="hidden" name="modal_confirm_form_submitted" id="modal_confirm_form_submitted" value="1" />
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button id="no_delete" type="button" class="alert-info btn btn-default" data-dismiss="modal">
                                Cancel
                            </button>
                            <button id="yes_delete" type="button" class="alert-success btn btn-default" onclick="$('#po_confirm_status_modal_form').submit();">
                                Send To Supplier
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>


</div>


<script type="text/javascript">
$(document).ready( function() {

    $('#po_other_charges').keyup(function() { calculatePOTotal(); });
    $('#po_other_charges').blur(function() { calculatePOTotal(); });
    
    $('#po_discount').keyup(function() { calculatePOTotal(); });
    $('#po_discount').blur(function() { calculatePOTotal(); });
    
    calculatePOTotal();
});


function calculatePOTotal()
{
	var order_payment = $('#order_payment').val();
	var po_discount = $('#po_discount').val();
	var po_other_charges = $('#po_other_charges').val();

    if ($.trim(order_payment) == '' || !$.isNumeric(order_payment)) order_payment = 0;
    if ($.trim(po_discount) == '' || !$.isNumeric(po_discount)) po_discount = 0;
    if ($.trim(po_other_charges) == '' || !$.isNumeric(po_other_charges)) po_other_charges = 0;
	
	var po_total = parseFloat(order_payment) + parseFloat(po_other_charges) - parseFloat(po_discount);
    $('#po_total').val(po_total.toFixed(2));	
}

function deletePurchaseOrderFile(file_idx, file_name)
{
	$('#delete_purchase_order_file_link_' + file_idx).confirmation({
	  title: "Are you sure you want to delete the attached file?",
	  singleton: true,
	  placement: 'right',
	  popout: true,
	  onConfirm: function() {
	  	$('#existing_file_id_' + file_idx).remove();
        $.ajax({
            type: "POST",
            url: "<?php echo AppUrl::bicesUrl('purchaseOrders/deleteFile/'); ?>",
            data: { po_id: $('#po_id').val(), file_name: file_name }
        });
	  },
	  onCancel: function() {  }
	});
	
}


function confirmPOApproval()
{ 

    $.ajax({
        url: "<?php echo AppUrl::bicesUrl('purchaseOrders/updateVendorLocation/'); ?>",
        type: "POST",
        data: $('#po_edit_modal_form').serialize(),
        success: function(data) {
            $('#po_edit_modal').modal('hide');
            $.ajax({
                url: "<?php echo AppUrl::bicesUrl('purchaseOrders/vendorLocationData/'); ?>",
                type: "POST",
                data: $('#po_edit_modal_form').serialize(),
                dataType:'json',
                success: function(data) {
                    
                    $('#po_send_modal').modal('show');
                    $("#show_contact_name").text(data[0].contact_name);
                    $("#show_address_1").text(data[0].address_1);
                    $("#show_city").text(data[0].city);
                    $("#show_state").text(data[0].state);
                    $("#show_zip").text(data[0].zip);
                    $("#show_phone_1").text(data[0].phone_1);
                    $("#show_supplier_email").text(data[0].emails);


                    $("#show_location_name").text(data[1].location_name);
                    $("#show_contact_person").text(data[1].contact_person);
                    $("#show_ship_address").text(data[1].ship_address);
                    $("#show_ship_city").text(data[1].ship_city);
                    $("#show_ship_state").text(data[1].ship_state);
                    $("#show_ship_zip").text(data[1].ship_zip_code);
                    $("#show_ship_phone").text(data[1].phone);
                    $("#show_ship_email").text(data[1].email);

                }
            });

        }
    });

}

function updatePOApproval()
{
    $('#po_edit_modal').modal('show');
}


</script>
