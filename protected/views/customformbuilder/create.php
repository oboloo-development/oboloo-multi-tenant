
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
      <div class="span6 pull-left col-md-6 col-xs-12 p-0">
         <h3 class="mt-15">eSourcing Questionnaires</h3>
      </div>
      <div class="span6  col-md-6 col-xs-12">
        <a href="<?php echo Yii::app()->createUrl('customformbuilder/list') ?>" class="btn btn-success pull-right">Return To Questionnaires List</a>
      </div>
       <?php $quoteCreated=Yii::app()->user->getFlash('success');
          if(!empty($quoteCreated)) { ?>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $quoteCreated;?></div>
        <?php } ?>
      <div class="clearfix"><br/></div>
            
      <div class="row">
         <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-body">
             <form>
               <div class="col-md-9">
                  <div class="form-group" style="margin-right: 11px;">
                     <label for="name">Questionnaire Name <span style="color: red;">*</span></label>
                     <input type="text" name="name" id="name" class="form-control" placeholder="Enter Questionnaire Name"  />
                     <span id="required_form_name" style="display: none;">Questionnaire Name is required</span>
                  </div>
               </div>
               </form>
               <div class="build-wrap"></div>
               <button type="button" onclick="save()" class="btn btn-primary mt-32 questionaire_create">Save</button>
             
            </div>
         </div>
        
         </div>
      </div>
    </div>
</div>
<?php $this->renderPartial('_script'); ?>
<script>
$(document).click(function() {
   $('label[for^="label-frmb-"]:contains("Label")').text('Type Question here:');
   $('.option-value[name^="select-"]').hide();
   $('.option-value[name^="radio-group-"]').hide();
   // $('.option-value[name^="checkbox-group-"]').css({
   //    'background': '#ffff',
   //    'color': '#fff',
   //    'border': '1px #ffffff'
   // });
});   
var options = { 
   disableFields: ['autocomplete','button','file','hidden','paragraph','header','checkbox-group'],
   disabledAttrs: ['access','max','maxlength', 'min','rows','step','style','toggle','other','className','name','value','type'],
   disabledSubtypes: {
    textarea: ['tinymce', 'quill'],
    text: ['password', 'email', 'color', 'tel']
   },
   showActionButtons: false,
};
var formBuilder = $('.build-wrap').formBuilder(options);
function save(){
   var formName = $('#name').val();
   $('#required_form_name').css('display','none');
   if(formName != ''){
      var form = formBuilder.actions.getData();
	  for(var i = 0; i < form.length; i++){
	   form[i].label = form[i].label.replace(/(&nbsp;|<([^>]+)>)/ig, "");
	  }

      $.ajax({
	      type: "POST", data: { formName: formName, userId: 1 ,form: form }, dataType: "json",
	      url: "<?php echo AppUrl::bicesUrl('customformbuilder/save/'); ?>",
         beforeSend: function() {
          $('.questionaire_create').prop('disabled', true);
         },
	       success: function(status) {
           window.location = "<?php echo AppUrl::bicesUrl('customformbuilder/list'); ?>";
	       },
	       error: function() {  }
	    });
   }else{
      $('#required_form_name').css('display','block');
   }
}

</script>
<style>
     #required_form_name{ display: block; color: red; padding-top: 6px; }
</style>
   

