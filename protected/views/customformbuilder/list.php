<div class="right_col" role="main">
    <div class="row-fluid tile_count">
      <div class="col-md-6 col-xs-12 p-0">
         <h3 class="mt-15">eSourcing Questionnaires</h3>
      </div>
      <div class="col-md-6 col-xs-12">
        <a href="<?php echo Yii::app()->createUrl('customformbuilder/create/') ?>" class="btn green-btn pull-right">Create New Questionnaire</a>
        <!-- <a href="<?php echo Yii::app()->createUrl('customformbuilder/create/') ?>" class="btn btn-success pull-right">Clone Questionnaire</a> -->
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModalQuestion">Clone Questionnaire</button>
      </div>
      <div class="clearfix"><br/></div>
            
      <div class="row">
      <?php $quoteCreated=Yii::app()->user->getFlash('success');
      if(!empty($quoteCreated)) { ?>
      <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $quoteCreated;?></div>
    <?php } ?>
      <div class="col-md-12">
      <form class="form-horizontal mt-26" id="contract_list_form" name="contract_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('customformbuilder/list'); ?>">
			<div class="col-md-6 col-sm-6 col-xs-12 pl-0" >
				<div class="form-group">
					<select name="user_id" class="form-control select_users" id="user_id"
						class="form-control">
						<option value="">All User</option>
          	  	  <?php 
                  $query = "select * from users where user_id not in (2,3) order by full_name asc ";
                  $usersName = Yii::app()->db->createCommand($query)->queryAll();
          	  	  foreach ($usersName as $key=>$user) { ?>
          	  	  		<option value="<?= $user['user_id']; ?>"
							<?php if (isset($_POST['user_id']) && $_POST['user_id'] == $user['user_id']) echo ' selected="SELECTED" '; ?>>
          	  	  			<?= $user['full_name']; ?>
          	  	  		</option>
          	  	  <?php } ?>
          	  </select>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 search-contract text-left" style="padding: 0px;">
				<button class="btn btn-info" onclick="clearFilter(); return false;" style="border-color: #46b8da;">Clear Filters</button>
				<button class="btn btn-primary " onclick="loadQuestionnaires(); return false;" style="margin-right: -7px">Apply filters</button>
			</div>
			
		</form>
		<div class="clearfix"></div>
      <table id="questionnaires_table" class="table table-striped table-bordered suppliers-table" style="width: 100%">
       <thead>
        <tr>
          <th style="width: 6%;" tabindex="-1" class="nosort"> </th>
          <th style="width: 15%;">Questionnaire Name</th>
          <th style="width: 15%;">Creator</th>
          <th style="width: 15%;">Created Date</th>
          <th style="width: 15%;">Last Updated At</th>
          <th style="width: 15%;">Delete</th>
        </tr>
       </thead>
          <tbody>
          </tbody>
        </table>
        </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Clone Questionnaire</h4>
      </div>
      <div class="modal-body">
      <form action="<?php echo AppUrl::bicesUrl('customformbuilder/saveFormClone/'); ?>" method="POST">
        <div class="form-group">
          <label>Questionaire <span class="text-danger">*</span></label>
          <select class="form-control select_question" id="quote_questionaire" name="questionaire_id" required>
            <option value="">Select Questionnaire</option>
            <?php $questionReader = Yii::app()->db->createCommand('SELECT id, name FROM questionnaires_forms order by name asc ')->queryAll();
            foreach ($questionReader as $questions) { ?>
              <option value="<?= $questions['id']; ?>"><?= $questions['name']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>Questionaire New Name <span class="text-danger">*</span></label>
          <input type="text" class="form-control" placeholder="Enter Questionaire Name" name="new_form_name" required />
        </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
      </div>
    </div>
  </div>
</div>

<script>
  function deleteForm(formKey, status) {
   var formKey = formKey;
   var status = status;
   if(status === 'yes'){
    $.confirm({
      title: false,
      content: '<spam style="font-size:11px">Are you sure you want to delete the form?</span>',
      buttons: {
        Yes : {
          text: "Yes",
          btnClass: 'btn-blue',
          action: function() {
            $.ajax({
              dataType: 'json',
              type: "post",
              url: "<?php echo AppUrl::bicesUrl('customformbuilder/delete/'); ?>",
              data: {
                formId: formKey, status :status,
              },
              success: function(option) {
                if(option.uploaded_response == 1 && status === 'yes'){
                  $(".delete_form_row_"+formKey).remove();
                  // location.reload();
                  $('#questionnaires_table').DataTable().ajax.reload();
                }
              }
            });
          }
        },
        No: {
          text: "Close",
          btnClass: 'btn-red',
          action: function() {

          }
        },
      }
    });
  }else{
    $.confirm({
      title: false,
      content: '<spam style="font-size:11px">You do not have permission to delete this questionnaire. Only the person who created the questionnaire or a super user can delete questionnaires</span>',
      buttons: {
        No: {
          text: "Close",
          btnClass: 'btn-red',
          action: function() {
            //window.reload();
          }
        },
      }
    });
  }
  $("#file_upload_alert").delay(1000 * 5).fadeOut();
  }
  $('document').ready(() => {
      $(".select_question").select2({
        placeholder: "Select Questionnaire",
        allowClear: true,
        containerCssClass: "notranslate",
        dropdownCssClass: "notranslate"
      });
  });

  $(document).ready( function () {
      $('#questionnaires_table').DataTable({
        "columnDefs": [{
         "targets": [1, 2],
        }],
        "createdRow": function(row, data, index) {
          if (data[0].indexOf('glyphicon glyphicon-ok') >= 0)
            for (var i = 1; i <= 6; i++)
              $('td', row).eq(i).css('text-decoration', 'line-through');
        },
        "order": [
          [1, "asc"]
        ],
        "pageLength": 50,
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?php echo AppUrl::bicesUrl('customformbuilder/listAjax'); ?>",
          "type": "POST",
          data: function(input_data) {
            input_data.user_id = $('#user_id').val();
          }
        },
        "oLanguage": {
          "sProcessing": "<h1>Please wait ... retrieving data</h1>"
        },
        "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          if (aData[1].search("<span class='deactivated_vendor'></span>") > 0) {
            $(nRow).addClass('deactivated_record');
          }
        }
      });
  });

  function clearFilter() {
    $("option:selected").removeAttr("selected");
    $('.select_location_multiple').trigger("change")
    $('#questionnaires_table').DataTable().ajax.reload();
  }

  function loadQuestionnaires()
  {
    $('#questionnaires_table').DataTable().ajax.reload();
  }
</script>



      
   

