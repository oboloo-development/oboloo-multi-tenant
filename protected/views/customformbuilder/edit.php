
<div class="right_col" role="main">
 <?php $this->renderPartial('_script'); ?>
    <div class="row-fluid tile_count">
      <div class="span6 pull-left col-md-6 col-xs-12 p-0">
         <h3 class="mt-15">eSourcing Questionnaires</h3>
      </div>
      <div class="span6  col-md-6 col-xs-12">
        <a href="<?php echo Yii::app()->createUrl('customformbuilder/list') ?>" class="btn btn-success pull-right">Return To Questionnaires List</a>
      </div>
       <?php $quoteCreated=Yii::app()->user->getFlash('success');
          if(!empty($quoteCreated)) { ?>
          <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php echo $quoteCreated;?></div>
        <?php } ?>
      <div class="clearfix"><br/></div>
            
      <div class="row">
         <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-body">
               <form>
               <input type="hidden" name="form_id" id="form_id" value="<?= $editform['id']; ?>" />
               <div class="col-md-9">
                  <div class="form-group" style="margin-right: 11px;">
                     <label for="">Questionnaire Name</label>
                     <input type="text" name="name" id="name" value="<?= $editform['name'] ?>" class="form-control" placeholder="Enter Questionnaire Name" required />
                  </div>
               </div>
               </form>
            
               <div class="build-wrap"></div>
               <?php if((!empty($editform['user_id']) && $editform['user_id'] == $user_data['user_id']) || $user_data['admin_flag'] == 1){ ?> 
               <button type="button" onclick="update()" class="btn btn-primary questionaire_create">Save</button>
            <?php } ?>
            </div>
         </div>
        
         </div>
      </div>
    </div>
</div>
<?php
$sql = "select * from form_questions where form_id=".$editform['id'];
$form_questions = Yii::app()->db->createCommand($sql)->query()->readAll();
?>

<script>
$(document).click(function() {
   $('label[for^="label-frmb-"]:contains("Label")').text('Type Question here:');
   $('.option-value[name^="select-"]').hide();
   $('.option-value[name^="radio-group-"]').hide();
   // $('.option-value[name^="checkbox-group-"]').css({
   //    'background': '#ffff',
   //    'color': '#fff',
   //    'border': '1px #ffffff'
   // });
});
 var formQuestion = <?php echo json_encode($form_questions); ?>;
 var allQuestionsform = [];
 for(let index=0; index < formQuestion.length; index++){
   var formFieldRow = formQuestion[index];
   var formField = formFieldRow.question;
   try {
   var formField = $.parseJSON(formField);
   console.log('✅ JSON array parsed successfully');
   } catch (err) {
   console.log('⛔️ invalid JSON provided');
  }
   allQuestionsform.push(formField);
 }

 var formData = {
   disableFields: ['autocomplete','button','file','hidden','paragraph','header','checkbox-group'],
   disabledAttrs: ['access','max','maxlength', 'min','rows','step','style','toggle','other','className','name','value','type'],
   disabledSubtypes: {
    textarea: ['tinymce', 'quill'],
    text: ['password', 'email', 'color', 'tel']
   },
   showActionButtons: false,
   formData : allQuestionsform
 }


var formBuilder = $('.build-wrap').formBuilder(formData);
 function update(){
   var formName = $('#name').val();
   var form_id  = $('#form_id').val();
   if(formName != ''){
   var form = formBuilder.actions.getData();
	for(var i = 0; i < form.length; i++){
	 form[i].label = form[i].label.replace(/(&nbsp;|<([^>]+)>)/ig, "");
	}
      $.ajax({
	        type: "POST", data: { formName: formName, userId: 1, form_id:form_id ,form: form }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('customformbuilder/update/'); ?>",
            beforeSend: function() {
            $('.questionaire_create').prop('disabled', true);
           },
	        success: function(options) {
            window.location = "<?php echo AppUrl::bicesUrl('customformbuilder/list'); ?>";
	        },
	        error: function() {  }
	    });
   }
 }
</script>

      
   

