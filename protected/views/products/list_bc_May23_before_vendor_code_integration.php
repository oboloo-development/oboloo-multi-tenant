<?php $action=$this->action->id;
$categorSearch = $subCategorySearch=$productTypeSearch=$vendorIDSearch=$contractIDSearch='';
if(!empty($_POST['category_id'])){
  $categorSearch=$_POST['category_id'];
}
if(!empty($_POST['subcategory_id'])){
  $subCategorySearch=$_POST['subcategory_id'];
}
if(!empty($_POST['product_type'])){
  $productTypeSearch=$_POST['product_type'];
}
if(!empty($_POST['vendor_id'])){
  $vendorIDSearch=$_POST['vendor_id'];
}

if(!empty($_POST['contract_id'])){
  $contractIDSearch=$_POST['contract_id'];
}
?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Products</h3>
        </div>



        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('products/edit/0'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Product
                </button>
            </a>

          <?php if (isset(Yii::app()->session['admin_flag']) && Yii::app()->session['admin_flag'] == 1) { ?>
        
             <!-- <a href="<?php /*echo AppUrl::bicesUrl('products/import'); */?>">
                  <button type="button" class="btn btn-default">
                      <span class="glyphicon glyphicon-import" aria-hidden="true"></span> Upload CSV
                  </button>
              </a>-->
          
          <?php } ?>

            <a href="<?php echo AppUrl::bicesUrl('products/export'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #207244;color: #fff;">
                    <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export CSV
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

    <form class="form-horizontal" id="product_list_form" name="product_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('products/list'); ?>">
        <div class="form-group">
            <div class="col-md-6">
                <input  type="text" name="product_type" id="product_type" class="form-control" value="<?php if(Yii::app()->input->post('product_type')){ echo Yii::app()->input->post('product_type'); } ?>" placeholder="Search Product">
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" onclick="$('#order_list_form').submit();">Search Products</button>
            </div>
        </div>
    </form>
    <div class="clearfix"> </div>
    
     <img src="<?php echo AppUrl::bicesUrl('images/loading.gif'); ?>" class="img-responsive" id="big_table_processing" style="    max-width: 116px;
    margin: 0 auto;" />
    <table id="product_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th> </th>
          <th>Name</th>
          <th>Supplier</th>
          <th>Category</th>
          <th>Sub Category</th>
          <th>Brand</th>
          <th>Price</th>
        </tr>
      </thead>

  </table>

</div>


<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">De-Activate Product Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to de-activate this product: <span id="delete_name" style="font-weight: bold;"></span>?</p>
          <input type="hidden" name="delete_item_id" id="delete_item_id" value="0" />
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
              NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
              YES
          </button>
        </div>
      </div>
  </div>
</div>



<style type="text/css">
  .dataTables_filter
    {display:none !important;}
  .dataTables_length
    {display:none !important;}
   .dataTables_info
   {display:none !important;}
 </style>

<script type="text/javascript">

function make_datatable1(table_id,source_url,colm,sort_order,is_last_col_sort){
  
  $("#big_table_processing").show();
  var colmn   = '';
  var last_col_sort = false;
  
  if(typeof(colm) != 'undefined' &&  colm != ''){
    colmn = colm;
    if(typeof(is_last_col_sort) != 'undefined' && typeof(is_last_col_sort) != 'string'  &&  is_last_col_sort == false){
      colmn = -1;
    }
  }
  else{
    colmn = -1;
   }
  //alert(colm+' '+sort_order+' '+typeof(colm)+' '+is_last_col_sort);
  if(typeof(colm) != 'undefined' &&  colm != '' && typeof(colm) == 'number'){
    colm = colm;
    last_col_sort = true;
  }
  else{
    colm = 0;
  }
  //alert(colm+' '+sort_order);
  if(typeof(sort_order) != 'undefined'  &&  sort_order != ''){
    sort_order = sort_order;
  }
  else{
    sort_order = "desc";
  }
  
  if(typeof(is_last_col_sort) != 'undefined' && typeof(is_last_col_sort) != 'string'  &&  is_last_col_sort == false){
    last_col_sort = is_last_col_sort;
  }
  
  $.extend( true, $.fn.dataTable.defaults, {
    "dom": 'rt<"#bottom_option"i<"#pagination_bottom" p ><"clear">>', //https://datatables.net/examples/basic_init/dom.html
    'lengthMenu': [
      [ 10, 25, 50, 100,500,700,750,1000 ],
      [ '10 rows','25 rows', '50 rows', '100 rows','500 rows','700 rows','750 rows','1000 rows' ]
    ],
    'buttons': [
      'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
        
    ] 
  } );
  
  if(typeof(colmn) == 'string'){
    colmn = colmn.split(',');
    for(var i=0; i<colmn.length; i++) { colmn[i] = parseInt(colmn[i]); } 
  }
  
  //$main_url = site_admin_base_url+"/"+source_url;
  $main_url = source_url;
  //alert(colm+' '+sort_order);
  var oTable = $('#'+table_id).DataTable({
    "serverSide": true,
    "deferRender": true,
    "displayStart": 0,
    "pageLength": 25,
    "lengthChange": false,
    "searching": true,
    "ordering": true,
    "paging": true,
    "pagingType": "full_numbers",
    "order": [[ colm, sort_order ]],
    "autoWidth": false,
    "ajax": {
      "url":$main_url,
      "type": "POST",
       error: function (XMLHttpRequest, textStatus, errorThrown) {
        if(XMLHttpRequest.responseText=='SESSION_OUT'){
          alert(textStatus);
          alert(errorThrown);
          window.location = site_admin_base_url;
        }
        else {
          alert(textStatus);
          alert(errorThrown);
        }
        $("#big_table_processing").hide();
      }
    },

    "columnDefs": [
      { "targets": ['nosort',colmn], "orderable": false},
      //{ targets: [0, 1], visible: true},
      //{ targets: '_all', visible: false }
      //{ "targets": [1], visible: false }
    ],
    /*"language": {
      "processing": "DataTables is currently busy",
    },
    "processing" : true,*/
    "initComplete" : function(){
      $("#big_table_processing").hide()
      
    },
    
    "drawCallback":function(){
      if ( $('#'+table_id+'_paginate span a.paginate_button').size()>1) {
        $('#'+table_id+'_paginate')[0].style.display = "block";
        $('#pagination_bottom')[0].style.display = "block";
      } else {
       // $('#'+table_id+'_paginate')[0].style.display = "none";
        //$('#pagination_bottom')[0].style.display = "none";
      }
    }
  });     
      
  oTable.columns().every( function () {
    var that = this;
 
    $( 'input', this.footer() ).on( 'keydown', function (e) {
      if ( that.search() !== this.value && e.keyCode == 13) {
        that
          .search( this.value )
          .draw();
      }
    } );
  } );       

  if (! $("select[name='"+table_id+"_length']").hasClass("form-control")) {
    $("select[name='"+table_id+"_length']").addClass("form-control input-small big_table_length");
  }
  
  $("div.dataTables_filter input")
    .unbind() // Unbind previous default bindings
    .bind("input", function (e) { // Bind for field changes
      // Search if enough characters, or search cleared with backspace
      /*if (this.value.length >= 3 || this.value == "") {
        // Call the API search function
        oTable.api().search(this.value).draw();
      }*/
    })
    .bind("keydown", function (e) { // Bind for enter key press
      // Search when user presses Enter
      if (e.keyCode == 13)
        oTable.search(this.value).draw();
    });
    
  return oTable;
}

function filterGlobal () {
    $('#product_table').DataTable().search(
        $('#global_filter_field').val(),
        $('#global_regex').prop('checked'),
        $('#global_smart').prop('checked')
    ).draw();
}
 
$(document).ready(function() {
    oTable =  make_datatable1("product_table","<?php echo $this->createAbsoluteUrl('products/listAjax?url_action='.$action.'&category_id='.$categorSearch.'&subcategory_id='.$subCategorySearch.'&product_type='.$productTypeSearch.'&vendor_id='.$vendorIDSearch.'&contract_id='.$contractIDSearch); ?>",1,'desc');  
 
    $('#search_btn').on( 'click', function () {
        filterGlobal();
    } ); 
    $('input.column_filter').on( 'keyup click', function () {
        filterColumn( $(this).parents('tr').attr('data-column') );
    } );
} );
</script>

<script type="text/javascript">

$(document).ready( function() {
    <?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>
    //loadSubcategories(<?php echo $subcategory_id; ?>);
    <?php }

     ?>

    /*$('#product_table').dataTable({
        "columnDefs": [ {
            "targets": 0,
            "width": "6%",
            "orderable": false
        } ],
        "order": [],
        "pageLength": 50
    });*/
<?php //if (!empty($category_id) || !empty($subcategory_id) || !empty($product_type) || !empty($vendor_id)) { ?>
//    $('#product_table').dataTable({
//        "columnDefs": [ {
//            "targets": 0,
//            "width": "6%",
//            "orderable": false
//        } ],
//        "createdRow": function ( row, data, index ) {
//            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
//              for (var i=1; i<=6; i++)
//                  $('td', row).eq(i).css('text-decoration', 'line-through');
//        },
//        "order": [],
//    "pageLength": 50
//    });
//    <?php //} else { ?>
//    $('#product_table').dataTable({
//        "columnDefs": [ {
//            "targets": 0,
//            "width": "6%",
//            "orderable": false
//        } ],
//        "createdRow": function ( row, data, index ) {
//            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
//                for (var i=1; i<=7; i++)
//                    $('td', row).eq(i).css('text-decoration', 'line-through');
//        },
//        "order": [],
//        "pageLength": 50,
//        "processing": true,
//        "serverSide": true,
//        "ajax": "<?php //echo AppUrl::bicesUrl('products/list'); ?>//"
//    });
//    <?php //} ?>

  $('#delete_confirm_modal .modal-footer button').on('click', function(event) {
    var button = event.target; // The clicked button
  
    $(this).closest('.modal').one('hidden.bs.modal', function() {
        if (button.id == 'yes_delete') changeItemStatus();
      });
   });
    
});

function deleteProduct(item_id)
{
    $('#delete_item_id').val(0);
    $('#delete_name').text('');

  var item_name = "";
  var col_index = 0;
  $('#delete_icon_' + item_id).parent().parent().parent().find('td').each(function() {
    col_index += 1;
    if (col_index == 2) item_name = $(this).text();
  });

  $('#delete_item_id').val(item_id);
  if ($('#delete_icon_' + item_id).hasClass('glyphicon-ok')) changeItemStatus();
  else
  {
    $('#delete_item_id').val(item_id);
    $('#delete_name').text(item_name);
    $('#delete_confirm_modal').modal('show');
  } 
}

function changeItemStatus()
{
    var delete_item_id = $('#delete_item_id').val();
    var active_flag = 0;
        
    if ($('#delete_icon_' + delete_item_id).hasClass('glyphicon-remove'))
    {
      active_flag = 0;
      $('#delete_icon_' + delete_item_id).removeClass('glyphicon-remove');
      $('#delete_icon_' + delete_item_id).addClass('glyphicon-ok');
    }
    else
    {
      active_flag = 1;
      $('#delete_icon_' + delete_item_id).removeClass('glyphicon-ok');
      $('#delete_icon_' + delete_item_id).addClass('glyphicon-remove');
    }
        
    $.ajax({
        type: "POST", data: { delete_item_id: delete_item_id, active_flag: active_flag },
        url: "<?php echo AppUrl::bicesUrl('products/updateItemStatus/'); ?>"
    });
    
  var col_index = 0;
  $('#delete_icon_' + delete_item_id).parent().parent().parent().find('td').each(function() {
    col_index += 1;
    if (col_index != 1)
    {
      if (active_flag == 0) $(this).css("text-decoration", "line-through");
      else $(this).css("text-decoration", "none");
    }
  });
}

function loadSubcategories(input_subcategory_id)
{
    var category_id = $('#category_id').val();

    if (category_id == 0)
        $('#subcategory_id').html('<option value="0">Select Product Sub Category</option>');
    else
    {
        $.ajax({
            type: "POST", data: { category_id: category_id }, dataType: "json",
            url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
            success: function(options) {
                var options_html = '<option value="0">Select Product Sub Category</option>';
                for (var i=0; i<options.suggestions.length; i++)
                    options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
                $('#subcategory_id').html(options_html);
                if (input_subcategory_id != 0) $('#subcategory_id').val(input_subcategory_id);
            },
            error: function() { $('#subcategory_id').html('<option value="0">Select Product Sub Category</option>'); }
        });
    }
}
</script>
