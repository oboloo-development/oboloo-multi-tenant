<div class="right_col" role="main">

	<div class="row-fluid tile_count">
		<div class="span6 pull-left">
			<h3>
                <?php
                if (isset($product_id) && $product_id) {
                    echo 'Save Product';
                    if (isset($product) && is_array($product) && isset($product['product_name']) && ! empty($product['product_name']))
                        echo ' - ' . $product['product_name'];
                } else
                    echo 'Add Product';
                ?>
            </h3>
		</div>

		<div class="span6 pull-right">
			<a href="<?php echo AppUrl::bicesUrl('products/list'); ?>">
				<button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;">
					<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
					Product List
				</button>
			</a>
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="row tile_count">
		<form id="product_form"
			class="form-horizontal form-label-left input_mask" method="post"
			action="<?php echo AppUrl::bicesUrl('products/edit'); ?>">
			<div class="form-group">
		      <?php if (isset($product_id) && $product_id && $product['active'] == 0) { ?>
	              <div class="col-md-4 col-sm-4 col-xs-8 date-input">
					  <label class="control-label">Product Name</label>
					<input type="text" class="form-control has-feedback-left"
						name="product_name" id="product_name"
						<?php if (isset($product['product_name']) && !empty($product['product_name'])) echo 'value="' . htmlentities($product['product_name']) . '"'; else echo 'placeholder="Product Name"'; ?>>
					<span class="fa fa-qrcode form-control-feedback left"
						aria-hidden="true"></span>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-4">
					<label class="control-label">Status</label>
					<select name="active" id="active" class="form-control">
						<option value="0">Inactive</option>
						<option value="1">Active</option>
					</select>
				</div>
	          <?php } else { ?>
	              <div class="col-md-6 col-sm-6 col-xs-12 date-input valid">
					  <label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
					<input required type="text" class="form-control has-feedback-left"
						name="product_name" id="product_name"
						<?php if (isset($product['product_name']) && !empty($product['product_name'])) echo 'value="' . htmlentities($product['product_name']) . '"'; else echo 'placeholder="Product Name"'; ?>>
					<span class="fa fa-qrcode form-control-feedback left"
						aria-hidden="true"></span>
				</div>
	          <?php } ?>
          </div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<label class="control-label">Product Type</label>
					<select name="product_type" id="product_type" class="form-control">
						<option value="">Select Product Type</option>
						<option  <?php if (isset($product['product_type']) && $product['product_type'] == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
						<option  <?php if (isset($product['product_type']) && $product['product_type'] == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<label class="control-label">Product Category</label>
					<select name="category_id" id="category_id" class="form-control"
						onchange="loadSubcategories(0);">
						<option value="0">Select Product Category</option>
					<?php foreach ($categories as $category) { ?>
						<option value="<?php echo $category['id']; ?>"
							<?php if (isset($product['category_id']) && $product['category_id'] == $category['id']) echo ' selected="SELECTED" '; ?>>
							<?php echo $category['value']; ?>
						</option>
					<?php } ?>
				</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<label class="control-label">Product Sub Category</label>
					<select name="subcategory_id" id="subcategory_id"
						class="form-control">
						<option value="0">Select Product Sub Category</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<label class="control-label">Supplier</label>
					<select name="vendor_id" id="vendor_id" class="form-control">
						<option value="">Select Supplier</option>
						<?php

							foreach ($vendors as $vendor) {
								if($vendor['vendor_name']!='') {
								?>
								<option value="<?php echo $vendor['vendor_id']; ?>"
									<?php if (isset($product['vendor_id']) && $product['vendor_id'] == $vendor['vendor_id']) echo ' selected="SELECTED" '; ?>>
									<?php echo $vendor['vendor_name']; ?>
								</option>
							<?php }
						       } ?>
						</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<label class="control-label">Contract</label>
					<select name="contract_id" id="contract_id" class="form-control">
						<option value="">Select Contract</option>
						<?php

						foreach ($contracts as $contract) {
							if($contract['contract_title']!='') {
								?>
								<option value="<?php echo $contract['contract_id']; ?>"
									<?php if (isset($product['contract_id']) && $product['contract_id'] == $contract['contract_id']) echo ' selected="SELECTED" '; ?>>
									<?php echo $contract['contract_title']; ?>
								</option>
							<?php }
						} ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 date-input">
					<label class="control-label">Brand</label>
					<input type="text" class="form-control has-feedback-left"
						name="brand" id="brand"
						<?php if (isset($product['brand']) && !empty($product['brand'])) echo 'value="' . $product['brand'] . '"'; else echo 'placeholder="Brand"'; ?>>
					<span class="fa fa-trademark form-control-feedback left"
						aria-hidden="true"></span>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 date-input date-input">
					<label class="control-label">URL</label>
					<input type="text" class="form-control has-feedback-left"
						name="url" id="url"
						<?php if (isset($product['url']) && !empty($product['url'])) echo 'value="' . $product['url'] . '"'; else echo 'placeholder="URL"'; ?>>
					<span class="fa fa-globe form-control-feedback left"
						aria-hidden="true"></span>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label class="control-label">UPC</label>
					<input type="text" class="form-control" name="upc" id="upc"
						<?php if (isset($product['upc']) && !empty($product['upc'])) echo 'value="' . $product['upc'] . '"'; else echo 'placeholder="UPC"'; ?>>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label class="control-label">SKU Code</label>
					<input type="text" class="form-control" name="sku_code"
						id="sku_code"
						<?php if (isset($product['sku_code']) && !empty($product['sku_code'])) echo 'value="' . $product['sku_code'] . '"'; else echo 'placeholder="SKU Code"'; ?>>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label class="control-label">Length</label>
					<input type="text" class="form-control" name="length" id="length"
						<?php if (isset($product['length']) && !empty($product['length'])) echo 'value="' . $product['length'] . '"'; else echo 'placeholder="Length"'; ?>>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label class="control-label">Width</label>
					<input type="text" class="form-control" name="width" id="width"
						<?php if (isset($product['width']) && !empty($product['width'])) echo 'value="' . $product['width'] . '"'; else echo 'placeholder="Width"'; ?>>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label class="control-label">Height</label>
					<input type="text" class="form-control" name="height" id="height"
						<?php if (isset($product['height']) && !empty($product['height'])) echo 'value="' . $product['height'] . '"'; else echo 'placeholder="Height"'; ?>>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label class="control-label">Weight</label>
					<input type="text" class="form-control" name="weight" id="weight"
						<?php if (isset($product['weight']) && !empty($product['weight'])) echo 'value="' . $product['weight'] . '"'; else echo 'placeholder="Weight"'; ?>>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label class="control-label">Size</label>
					<input type="text" class="form-control" name="size" id="size"
						<?php if (isset($product['size']) && !empty($product['size'])) echo 'value="' . $product['size'] . '"'; else echo 'placeholder="Size"'; ?>>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 valid">
					<label class="control-label">Price <span style="color: #a94442;">*</span></label>
					<input required type="text" class="form-control" name="price" id="price"
						<?php if (isset($product['price']) && !empty($product['price'])) echo 'value="' . $product['price'] . '"'; else echo 'placeholder="Price"'; ?>>
				</div>
			</div>

			<div class="form-group">
	          <div class="col-md-3 col-sm-3 col-xs-6 valid">
	          	<?php
	          		
	          		if(!empty($product['currency_id'])){
            			$sql = "select * from order_details where product_id='".$product['product_id']."'";
            			$currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
            			$disabled = !empty(count($currencyReader))?"readonly='readonly'":"";
        			}else{
        				$disabled = "";
        			}

	          	 ?>
	              <label class="control-label">Currency <span style="color: #a94442;">*</span></label>
	              <select name="currency_id" required id="currency_id" class="form-control" <?php echo $disabled;?>>   <?php if(empty($disabled)){?>  
                      <option value="">Select Currency</option>
	                  <?php } foreach ($currency_rate as $rate) { 
	                  	if(isset($product['currency_id']) && $rate['id'] == $product['currency_id']){
	                  	?><option value="<?php echo $rate['id']; ?>"
	                          <?php echo ' selected="SELECTED" '; ?>>
	                          <?php echo $rate['currency']; ?>
	                      </option>
	                  <?php }  else if(empty($disabled)){?>
	                  			 <option value="<?php echo $rate['id']; ?>">
	                      		 <?php echo $rate['currency']; ?>
	                      		</option>
	                  		<?php } 
	                  	} ?>
	              </select>
	          </div>

	          <div class="clearfix">
					<br />
				</div>
			<div class="form-group">
				<div class="clearfix">
					<br />
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="hidden" name="product_id" id="product_id"
						value="<?php if (isset($product_id)) echo $product_id; ?>" /> <input
						type="hidden" name="form_submitted" id="form_submitted" value="1" />

					<a href="javascript:void(0);"
						onclick="$('#product_form').submit();">
						<button type="button" class="btn btn-primary">
            <?php if (isset($product_id) && $product_id) echo 'Save Product'; else echo 'Add Product'; ?>
        </button>
					</a>

    <?php if (isset($product_id) && $product_id && $product['active'] == 1) { ?>
					<button type="button" class="btn btn-warning"
						onclick="$('#delete_confirm_modal').modal('show');">Change Status
					</button>
					<?php } ?>
    <?php if (isset($product_id) && $product_id) { ?>
        <button type="button" class="btn btn-danger"
						onclick="$('#delete_modal').modal('show');">Delete Product
					</button>
    <?php } ?>

    </div>
			</div>

		</form>
	</div>
</div>


<div class="modal fade" id="delete_confirm_modal" role="dialog"
	data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header alert-info">
				<h4 class="modal-title">De-Activate Product Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to de-activate this product?</p>
			</div>
			<div class="modal-footer">
				<button id="no_delete" type="button"
					class="alert-success btn btn-default" data-dismiss="modal">NO</button>
				<button id="yes_delete" type="button"
					class="alert-danger btn btn-default" data-dismiss="modal">YES</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_modal" role="dialog"
	data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header alert-info">
				<h4 class="modal-title">Delete Product Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this product?</p>
			</div>
			<div class="modal-footer">
				<button id="no_delete" type="button"
					class="alert-success btn btn-default" data-dismiss="modal">NO</button>
				<button id="yes_delete_item" type="button"
					class="alert-danger btn btn-default" data-dismiss="modal">YES</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
$(document).ready( function() {
	<?php if (isset($product['subcategory_id']) && !empty($product['subcategory_id'])) { ?>
		loadSubcategories(<?php echo $product['subcategory_id']; ?>);
	<?php } ?>	
	
    $( "#product_form" ).validate( {
        rules: {
            product_name: "required",
			price: "required"
        },
        messages: {
            product_name: "Product Name is required",
			price: "Price is required"
        },
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-6" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" )
				error.insertAfter( element.parent( "label" ) );
			else error.insertAfter( element );

			if ( !element.next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-remove form-control-feedback' style='display: none;'></span>" ).insertAfter( element );
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] )
				$( "<span class='glyphicon glyphicon-ok form-control-feedback' style='display: none;'></span>" ).insertAfter( $( element ) );
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-error" ).removeClass( "has-success" );
			//$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".valid" ).addClass( "has-success" ).removeClass( "has-error" );
			//$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
    });

    $('#additional-field-model').duplicateElement({
        "class_remove": ".remove-this-field",
        "class_create": ".create-new-field"
    });

	$('#delete_confirm_modal .modal-footer button').on('click', function(event) {
	  var button = event.target; // The clicked button
	
	  $(this).closest('.modal').one('hidden.bs.modal', function() {
	      if (button.id == 'yes_delete') changeItemStatus();
      });
   });

	$('#delete_modal .modal-footer button').on('click', function(event) {
		  var button = event.target; // The clicked button
		
		  $(this).closest('.modal').one('hidden.bs.modal', function() {
		      if (button.id == 'yes_delete') deleteStatus();
	      });
	   });

	$('#yes_delete_item').on('click', function(event) {
		deleteStatus();
	});
    
});

function deleteStatus()
{
    $.ajax({
        type: "POST",
        data: { delete_item_id: $('#product_id').val()},
        url: "<?php echo AppUrl::bicesUrl('products/deleteItemStatus/'); ?>",
        success: function() {
			location = "<?php echo AppUrl::bicesUrl('products/list/'); ?>";
		},
    });
}

function changeItemStatus()
{
    $.ajax({
        type: "POST", data: { delete_item_id: $('#product_id').val(), active_flag: 0 },
        url: "<?php echo AppUrl::bicesUrl('products/updateItemStatus/'); ?>",
        success: function() { location = "<?php echo AppUrl::bicesUrl('products/edit/'); ?>" + $('#product_id').val(); }
    });
}

function loadSubcategories(input_subcategory_id)
{
	var category_id = $('#category_id').val();
	
	if (category_id == 0)
		$('#subcategory_id').html('<option value="0">Select Product Sub Category</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { category_id: category_id }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="0">Select Product Sub Category</option>';
	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	        	$('#subcategory_id').html(options_html);
	        	if (input_subcategory_id != 0) $('#subcategory_id').val(input_subcategory_id); 
	        },
	        error: function() { $('#subcategory_id').html('<option value="0">Select Product Sub Category</option>'); }
	    });
	}
}

</script>
