<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Products</h3>
        </div>



        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('products/edit/0'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #F79820;color: #fff;">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Product
                </button>
            </a>

	        <?php if (isset(Yii::app()->session['admin_flag']) && Yii::app()->session['admin_flag'] == 1) { ?>
				
	            <a href="<?php echo AppUrl::bicesUrl('products/import'); ?>">
	                <button type="button" class="btn btn-default">
	                    <span class="glyphicon glyphicon-import" aria-hidden="true"></span> Upload CSV
	                </button>
	            </a>
	        
	        <?php } ?>

            <a href="<?php echo AppUrl::bicesUrl('products/export'); ?>">
                <button type="button" class="btn btn-default" style="background-color: #207244;color: #fff;">
                    <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export CSV
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

    <form class="form-horizontal" id="product_list_form" name="product_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('products/list'); ?>">
        <div class="form-group">

            <div class="col-md-2">
                <select name="category_id" id="category_id" class="form-control"
                        onchange="loadSubcategories(0);">
                    <option value="0">Select Product Category</option>
                    <?php foreach ($categories as $category) { ?>
                        <option value="<?php echo $category['id']; ?>"
                            <?php if (isset($category_id) && $category_id == $category['id']) echo ' selected="SELECTED" '; ?>>
                            <?php echo $category['value']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-2">
                <select name="subcategory_id" id="subcategory_id"
                        class="form-control">
                    <option value="0">Select Product Sub Category</option>
                </select>
            </div>

            <div class="col-md-2">
                <select name="product_type" id="product_type" class="form-control">
                    <option value="">Select Product Type</option>
                    <option  <?php if (isset($product_type) && $product_type == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
                    <option  <?php if (isset($product_type) && $product_type == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
                </select>
            </div>

            <div class="col-md-2">
                    <select name="vendor_id" id="vendor_id" class="form-control">
                        <option value="">Select Supplier</option>
                        <?php

                        foreach ($vendors as $vendor) {
                            if($vendor['vendor_name']!='') {
                                ?>
                                <option value="<?php echo $vendor['vendor_id']; ?>"
                                    <?php if (isset($vendor_id) && $vendor_id == $vendor['vendor_id']) echo ' selected="SELECTED" '; ?>>
                                    <?php echo $vendor['vendor_name']; ?>
                                </option>
                            <?php }
                        } ?>
                    </select>
            </div>

            <div class="col-md-2">
                <button class="btn btn-primary" onclick="$('#order_list_form').submit();">Search Products</button>
            </div>
        </div>
    </form>
    <div class="clearfix"> </div>
    <table id="product_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th> </th>
          <th>Name</th>
          <th>Supplier</th>
          <th>Category</th>
          <th>Sub Category</th>
          <th>Brand</th>
          <th>UPC</th>
          <th>Price</th>
        </tr>
      </thead>

      <tbody>

          <?php
          	  foreach ($products as $product) 
          	  {
          	  	  $text_decoration = ' style="text-decoration: none;" ';
				  if ($product['active'] == 0) $text_decoration = ' style="text-decoration: line-through;" ';

          	  	  $price_text_decoration = ' style="text-decoration: none; text-align: right;" ';
				  if ($product['active'] == 0) $price_text_decoration = ' style="text-decoration: line-through; text-align: right;" ';
          ?>

              <tr>
                    <td>
                        <a href="<?php echo AppUrl::bicesUrl('products/edit/' . $product['product_id']); ?>">
                            <button class="btn btn-sm btn-success">View/Edit</button>
                        </a>
                        <!--
                        &nbsp;
                        <a onclick="deleteProduct(<?php echo $product['product_id']; ?>);">
                        	<?php if ($product['active'] == 1) { ?>
	                        	<span id="delete_icon_<?php echo $product['product_id']; ?>" class="glyphicon glyphicon-remove" aria-hidden="true"></span>
	                        <?php } else { ?>
	                        	<span id="delete_icon_<?php echo $product['product_id']; ?>" class="glyphicon glyphicon-ok" aria-hidden="true"></span>
	                        <?php } ?>
	                    </a>
	                    -->
                    </td>
                    <td <?php echo $text_decoration; ?>><?php echo $product['product_name']; ?></td>
                    <td <?php echo $text_decoration; ?>>  <?php echo !empty($product['vendor_name']) ? $product['vendor_name']:''; ?></td>
                    <td <?php echo $text_decoration; ?>><?php echo $product['category']; ?></td>
                    <td <?php echo $text_decoration; ?>><?php echo $product['subcategory']; ?></td>
                    <td <?php echo $text_decoration; ?>><?php echo $product['brand']; ?></td>
                    <td <?php echo $text_decoration; ?>><?php echo $product['upc']; ?></td>
                    <td <?php echo $price_text_decoration; ?>>
                    	<nobr>
                    		<?php echo Yii::app()->session['user_currency_symbol'] . ' ' . number_format($product['price'], 2); ?>
                    	</nobr>
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>


<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">De-Activate Product Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you want to de-activate this product: <span id="delete_name" style="font-weight: bold;"></span>?</p>
          <input type="hidden" name="delete_item_id" id="delete_item_id" value="0" />
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
          	  NO
          </button>
          <button id="yes_delete" type="button" class="alert-danger btn btn-default" data-dismiss="modal">
          	  YES
          </button>
        </div>
      </div>
	</div>
</div>


<script type="text/javascript">
$(document).ready( function() {
    <?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>
    loadSubcategories(<?php echo $subcategory_id; ?>);
    <?php }

     ?>

    $('#product_table').dataTable({
        "columnDefs": [ {
            "targets": 0,
            "width": "6%",
            "orderable": false
        } ],
        "order": [],
        "pageLength": 50
    });
<!--    --><?php //if (!empty($category_id) || !empty($subcategory_id) || !empty($product_type) || !empty($vendor_id)) { ?>
//    $('#product_table').dataTable({
//        "columnDefs": [ {
//            "targets": 0,
//            "width": "6%",
//            "orderable": false
//        } ],
//        "createdRow": function ( row, data, index ) {
//            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
//            	for (var i=1; i<=6; i++)
//                	$('td', row).eq(i).css('text-decoration', 'line-through');
//        },
//        "order": [],
//		"pageLength": 50
//    });
//    <?php //} else { ?>
//    $('#product_table').dataTable({
//        "columnDefs": [ {
//            "targets": 0,
//            "width": "6%",
//            "orderable": false
//        } ],
//        "createdRow": function ( row, data, index ) {
//            if ( data[0].indexOf('glyphicon glyphicon-ok') >= 0 )
//                for (var i=1; i<=7; i++)
//                    $('td', row).eq(i).css('text-decoration', 'line-through');
//        },
//        "order": [],
//        "pageLength": 50,
//        "processing": true,
//        "serverSide": true,
//        "ajax": "<?php //echo AppUrl::bicesUrl('products/list'); ?>//"
//    });
//    <?php //} ?>

	$('#delete_confirm_modal .modal-footer button').on('click', function(event) {
	  var button = event.target; // The clicked button
	
	  $(this).closest('.modal').one('hidden.bs.modal', function() {
	      if (button.id == 'yes_delete') changeItemStatus();
      });
   });
    
});

function deleteProduct(item_id)
{
   	$('#delete_item_id').val(0);
   	$('#delete_name').text('');

	var item_name = "";
	var col_index = 0;
	$('#delete_icon_' + item_id).parent().parent().parent().find('td').each(function() {
		col_index += 1;
		if (col_index == 2) item_name = $(this).text();
	});

	$('#delete_item_id').val(item_id);
	if ($('#delete_icon_' + item_id).hasClass('glyphicon-ok')) changeItemStatus();
	else
	{
		$('#delete_item_id').val(item_id);
		$('#delete_name').text(item_name);
		$('#delete_confirm_modal').modal('show');
	}	
}

function changeItemStatus()
{
   	var delete_item_id = $('#delete_item_id').val();
   	var active_flag = 0;
	    	
   	if ($('#delete_icon_' + delete_item_id).hasClass('glyphicon-remove'))
   	{
   		active_flag = 0;
   		$('#delete_icon_' + delete_item_id).removeClass('glyphicon-remove');
   		$('#delete_icon_' + delete_item_id).addClass('glyphicon-ok');
   	}
   	else
   	{
   		active_flag = 1;
   		$('#delete_icon_' + delete_item_id).removeClass('glyphicon-ok');
   		$('#delete_icon_' + delete_item_id).addClass('glyphicon-remove');
  	}
	    	
    $.ajax({
        type: "POST", data: { delete_item_id: delete_item_id, active_flag: active_flag },
        url: "<?php echo AppUrl::bicesUrl('products/updateItemStatus/'); ?>"
    });
    
	var col_index = 0;
	$('#delete_icon_' + delete_item_id).parent().parent().parent().find('td').each(function() {
		col_index += 1;
		if (col_index != 1)
		{
			if (active_flag == 0) $(this).css("text-decoration", "line-through");
			else $(this).css("text-decoration", "none");
		}
	});
}

function loadSubcategories(input_subcategory_id)
{
    var category_id = $('#category_id').val();

    if (category_id == 0)
        $('#subcategory_id').html('<option value="0">Select Product Sub Category</option>');
    else
    {
        $.ajax({
            type: "POST", data: { category_id: category_id }, dataType: "json",
            url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
            success: function(options) {
                var options_html = '<option value="0">Select Product Sub Category</option>';
                for (var i=0; i<options.suggestions.length; i++)
                    options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
                $('#subcategory_id').html(options_html);
                if (input_subcategory_id != 0) $('#subcategory_id').val(input_subcategory_id);
            },
            error: function() { $('#subcategory_id').html('<option value="0">Select Product Sub Category</option>'); }
        });
    }
}
</script>
