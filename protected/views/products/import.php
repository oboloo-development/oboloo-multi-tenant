<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Import Products</h3>
        </div>

        <div class="span6 pull-right">
            <a href="<?php echo AppUrl::bicesUrl('products/list'); ?>">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Product List
                </button>
            </a>
        </div>

        <div class="clearfix"> </div>
    </div>

    <div class="x_content">
      <p>Select one or more CSV files to import products. They will be automatically imported.</p>
      <p> </p>
      <p>The file should not have any header and must have fields in the following order: </p>
      <p> </p>
      <p>
          Product Name, Category, Sub-Category, UPC, SKU Code,
          Brand, Size, URL, Length, Width, Height, Weight, Price
      </p>

      <div class="row tile_count">
          <div class="clearfix"> </div>
      </div>

      <form name="import_form" id="import_form" action="<?php echo AppUrl::bicesUrl('products/importData'); ?> " method="post" enctype="multipart/form-data" class="dropzone">
          <div class="fallback">
              <input name="file[]" type="file" multiple />
          </div>
      </form>

    </div>

</div>
