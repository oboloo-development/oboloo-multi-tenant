<?php

Yii::import('application.components.Curl');
Yii::import("ext.mercury.MercuryHCClient");

class Mercury extends CApplicationComponent
{
    public function initPaymentExample($hcws)    
    {
        $hcws = new MercuryHCClient();
        $initPaymentRequest = array(
                "MerchantID"    =>  "013163015566916",
                "Password"  =>   "ypBj@f@zt3fJRX,k",
                "Invoice"   =>  "54321",
                "TotalAmount"   =>  9.9,
                "TaxAmount" =>  0.0,
                "TranType"  =>  "PreAuth",
                "Frequency" =>  "OneTime",
                "Memo"  =>  "InitializePaymentTest",
                "ProcessCompleteUrl"    =>  "http://www.mercurypay.com",
                "ReturnUrl" =>  "http://www.mercurypay.com"
                ); 
        
        $initPaymentResponse = $hcws->sendInitializePayment($initPaymentRequest);
        echo "<pre>";
        echo "<h1>InitializePayment Example</h1>";
        echo "Response Code: " . $initPaymentResponse->InitializePaymentResult->ResponseCode;
        echo "<br />Message: " . $initPaymentResponse->InitializePaymentResult->Message;
        echo "<br /><h3>Request:</h3>";
        
        // Hide Password from public echo
        $initPaymentRequest["Password"] = "********";
        
        print_r($initPaymentRequest);
        echo "<br /><h3>Response:</h3>";
        
        $ret = $initPaymentResponse->InitializePaymentResult->PaymentID;
        
        // Hide PaymentID from public echo
        $initPaymentResponse->InitializePaymentResult->PaymentID = "********";
        
        print_r($initPaymentResponse);
        echo "</pre>";
        
        return $ret;        
    }
}