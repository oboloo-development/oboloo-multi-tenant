<?php $quote_statuses = array('Pending', 'Submitted', 'Closed', 'Cancelled'); 
 $quteLocName = $quteCount = '';
  foreach ($location_quote_count as $value) { 
    $quteLocName .= '"'.$value['location_name'].'",';
    $quteCount .= $value['total_quotes'].',';
  }
 $quteLocName = rtrim($quteLocName,",");
 $quteCount = rtrim($quteCount,","); 
    

?>
<div class="right_col" role="main">
  <!-- <div class="quote-tutorial"></div> -->
    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>RFPs</h3>
        </div>

        <div class="span6 pull-right">

        	 <button type="button" class="btn btn-warning" onclick="addQuoteModal(event);">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create RFP
            </button> 
        </div>

        <div class="clearfix"> </div><br /><br /><br />

    <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel tile overflow_hidden">
      <div class="x_title">
        <h2>Current Default Values (%)</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content"><div id="scoring_pie_chart"></div><br /></div>
    </div></div>



  <div class="col-md-6 col-sm-6 col-xs-12">
  <div class="x_panel tile overflow_hidden">
    <div class="x_title">
      <h2>RFPs by Location</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content"><div id="full_scoring_by_location_count"></div></div>
  </div></div>

    </div>
    <div class="clearfix"></div>
  <br /><br /><br /><br /><br />
	<form class="form-horizontal" id="quote_list_form" name="quote_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('quotes/list'); ?>">
		<div class="form-group">
          <div class="col-md-1 col-sm-1 col-xs-6" style="min-width: 120px;">
              <input type="text" class="form-control border-select" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date(FunctionManager::dateFormat(), strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> autocomplete="off" >
          </div>
			
          <div class="col-md-1 col-sm-1 col-xs-6" style="min-width: 120px;">
              <input type="text" class="form-control border-select" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date(FunctionManager::dateFormat(), strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> autocomplete="off"  >
          </div>

			<div class="col-md-2 col-sm-2 col-xs-6  location">
				<select name="location_id[]" id="location_id"  class="form-control" multiple searchable="Search here.."  onchange="loadDepartments(0);">
					<?php
					$i = 0 ;
					foreach ($locations as $location) {
						if(isset($location_id[$i])){
							?>
							<option value="<?php echo $location['location_id']; ?>"
								<?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
								<?php echo $location['location_name']; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $location['location_id']; ?>">
								<?php echo $location['location_name']; ?>
							</option>
						<?php } ?>
						<?php
						$i++;
					} ?>
				</select>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-6 department">
				<select name="department_id[]" id="department_id" class="form-control" multiple searchable="Search here..">
					<option value="0">All Departments</option>
					<?php if(!empty($department_info))
						foreach($department_info as $dept_value){?>
						<option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
					<?php } ?>
					
				</select>
			</div>
      <div class="col-md-2 col-sm-2 col-xs-12">
      <div class="form-group">
      
        <select name="category_id[]" id="category_id" class="form-control border-select select_category_multiple"  multiple searchable="Search here.." onchange="loadSubcategories(0);">
          <option value="0">All Categories</option>
          <?php foreach ($categories as $category) { ?>
            <option value="<?php echo $category['id']; ?>"
                <?php if (!empty($category_id) && in_array($category['id'],$category_id)) echo ' selected="SELECTED" '; ?>>
              <?php echo $category['value']; ?>
            </option>
          <?php } ?>
        </select>
      </div></div>
      <div class="col-md-2 col-sm-2 col-xs-12">
        <div class="form-group">
        <select name="subcategory_id[]" id="subcategory_id" class="form-control border-select select_subcategory_multiple" multiple searchable="Search here.." >
          <option value="0">All Subcategories</option>
          <?php foreach ($subcategory as $sub) { ?>
            <option value="<?php echo $category['id']; ?>"
                <?php if (!empty($subcategory_id) && in_array($sub['id'],$subcategory_id)) echo ' selected="SELECTED" '; ?>>
              <?php echo $sub['value']; ?>
            </option>
          <?php } ?>
        </select></div>
      </div>
			<!-- <div class="col-md-2 status">
				<select name="quote_status[]" id="quote_status" multiple class="form-control">
					<?php
					$i = 0 ;
					foreach ($quote_statuses as $quote_status_display) {
						if(isset($quote_status[$i])){
							?>
							<option value="<?php echo $quote_status_display; ?>"
								<?php if ($quote_status[$i] == $quote_status_display) echo ' selected="SELECTED" '; ?>>
								<?php echo $quote_status_display; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $quote_status_display; ?>">
								<?php echo $quote_status_display; ?>
							</option>
						<?php }
						$i++;
					} ?>
				</select>
			</div> -->
			<div class="col-md-1 col-sm-1 col-xs-2 text-right-not-xs">
				<button class="btn btn-primary" onclick="$('#quote_list_form').submit();">Search RFPs</button>
			</div>
      <div class="clearfix"> </div>
		</div>
	</form>
<div class="clearfix"> </div><br /><br />
<div class="col-md-12">
<table id="quote_table" class="table table-striped table-bordered" style="width: 100%">
      <thead>
        <tr>
          <th> </th>
          <th>RFP ID</th>
          <th>RFP Name</th>
          <th>Opening Date</th>
          <th>Closing Date</th>
          <th>User</th>
          <th>Awarded Supplier</th>
          <th>Total Savings</th>
          <th>Currency</th>
          <th>Status</th>
        </tr>
      </thead>

      <tbody>

          <?php foreach ($quotes as $quote) { ?>
              <tr>
                    <td>
                        <a href="<?php echo AppUrl::bicesUrl('quotes/edit/' . $quote['quote_id']); ?>">
                            <button class="btn btn-sm btn-success">View/Edit</button>
                        </a>
                    </td>
                    <td style="text-align: right;"><?php echo $quote['quote_id']; ?></td>
                    <td><?php echo $quote['quote_name']; ?></td>
                    <td><p hidden="hidden" class="sample_date"> 
                      <?php echo strtotime($quote['opening_date']);?></P><?php echo date(FunctionManager::dateFormat()." H:iA", strtotime($quote['opening_date'])); ?></td>
                    <td><p hidden="hidden" class="sample_date"><?php echo strtotime($quote['closing_date']);?></p><?php echo date(FunctionManager::dateFormat()." H:iA", strtotime($quote['closing_date'])); ?></td>
                    <td><?php echo $quote['created_by_name']; ?></td>
                    <td class="notranslate"><?php echo !empty($quote['awarded_vendor_name'])?$quote['awarded_vendor_name']:''; ?></td>
                     <td><?php echo !empty($quote['awarded_vendor_name']) && $quote['estimated_value']>0?$quote['estimated_value']-$quote['vendor_submitted_price']:''; ?></td>


                      <td><?php echo $quote['quote_currency']; ?></td>

                    <td>
                    	<?php 
                    		if (strtotime($quote['closing_date']) <= time()) echo 'Complete<br />';

                    		if (strtolower($quote['quote_status'])=='cancelled') 
							{
								echo 'Cancelled';
							}else if ($quote['submit_count'] <= 0) 
							{
								if (strtotime($quote['closing_date']) > time()) echo 'Awaiting Quotes';
							}
							else 
							{
								if ($quote['invite_count'] == 1)
									echo $quote['submit_count'] . ' of ' . $quote['invite_count'] . ' Quote received';
								else
									echo $quote['submit_count'] . ' of ' . $quote['invite_count'] . ' Quotes received';
							}
              ?>
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>
</div>

</div>

<style>
	.location  .multiselect {
		width: 138%;
	}
	.status  .multiselect {
		width: 100%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}

	.btn .caret {
		float: right;
		margin-top: 10px;

	}
    .department .dropdown-toggle{
        min-width: 112%;
    }
</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
 colors = ['#2d9ca2','#66DA26', '#546E7A', '#E91E63', '#FF9800','#2E93fA','#2196F3', '#3a62ba', '#7b9333', '#344189','#','#','#','#','#'];
$('#location_id').multiselect(getOptions(true));
  //$('#department_id').multiselect(getOptions2(true));
  //$('#quote_status').multiselect(getOptions1(true));
$(document).ready(function(){
  select2function('select_category_multiple','All Categories');
  select2function('select_subcategory_multiple','All Sub Categories');
});

function select2function(className,lableTitle){
  var lableTitle = lableTitle;
  $("."+className).select2({
      placeholder: lableTitle,
      allowClear: true
    });
}

  <?php if (isset($category_id) && !empty($category_id)) { ?>

    <?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>
    
    <?php } else { ?>
    <?php } ?>

  <?php } ?>

function createLocationChart()
{   

    var quote_id = $("#quote_id").val();
    $.ajax({
        url: '<?php echo AppUrl::bicesUrl('quotes/listScoreDataChart'); ?>',
        type: 'POST', dataType: 'json',
        data: {},
        success: function(chart_data) {
       var score_values = chart_data.score_values;
       var score_labels = chart_data.score_labels;
       var pie_score_series = [];
       var pie_score_label  = chart_data.pie_score_label;
      for (i = 0; i < chart_data.pie_score_series.length; i++) {
          pie_score_series.push(parseFloat(chart_data.pie_score_series[i]));
      }
      
      /**/
         /* var options = {
            chart: {
                height: 275,
                type: 'bar',
               // stacked: true,
               // stackType: '100%',
                id:"apexChartVendDeptscoring",
                toolbar: {
                  tools: {
                      download: false
                  }
                },
            },
            plotOptions: {bar: {distributed: true}},
            responsive: [{
                breakpoint: 380,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            series: [{name:'Score',data: score_values}],
            xaxis: {
                categories: score_labels,

                },
            yaxis:{ 
            tickAmount: 10,
            min: 0,
            max: 100,
            labels: {
              formatter: function (value) {
                  valueL = value+"%";
                  return valueL;
                },
             }
      },

      dataLabels: {
        enabled: false,
        enabledOnSeries: false,
        formatter: function(value, { seriesIndex, dataPointIndex, w }) {
          return w.config.series[seriesIndex].data[dataPointIndex]+"%";
        },
      
      },
      tooltip: {
          y: {
           
          },
          marker: {
          show: true,
          },
      },
            fill: {
                opacity: 1
            },
            legend: {
                position: 'bottom',
                offsetX: 0,
                offsetY: 0
            },
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_chart"),
            options
        );
        chart.render();
        
         ApexCharts.exec('apexChartVendDeptscoring', 'updateOptions', {
            xaxis: {
              categories: score_labels,
            }
          }, false, true);
          ApexCharts.exec('apexChartVendDeptscoring', 'updateSeries', score_values,true);*/
    
    // START: Pie Chart
    var options = {
            chart: {
              fontFamily: 'Poppins !important',
              height: 235,
              width: 430,
              type: 'pie',
              id:"scoringPieChartlist"

            },
           legend: {show: true},
            dataLabels: {enabled: false},
            labels: pie_score_label,
            series: pie_score_series,
            responsive: [{
               breakpoint: 380,
                options: {
                    chart: {
                       width: 'auto',

                        //height: 'auto',

                    },
                    legend: {
                        show: false,
                        
                    }
                }
            }],
          tooltip: {
            y: {
                formatter: function(value) {
                 return value+"%";
               },
                 
            },}
            
        }
        var chart = new ApexCharts(
            document.querySelector("#scoring_pie_chart"),
            options
        );
         chart.render();

          ApexCharts.exec('scoringPieChartlist', 'updateOptions', {
           labels:  pie_score_label,
          }, false, true);
          ApexCharts.exec('scoringPieChartlist', 'updateSeries', pie_score_series,true);

    // END: Pie Chart
       }
      });

  }
  createLocationChart();


  function loadSubcategories(subcategory_id)
  {
    var category_id = $('#category_id').val();

        $.ajax({
            type: "POST", data: { category_id: category_id }, dataType: "json",
            url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
            success: function(options) { 
            
              var options_html = '<option value="0">All Subcategories</option>';
              for (var i=0; i<options.suggestions.length; i++)
                options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
              $('#subcategory_id').html(options_html);
              if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id); 

                $('#subcategory_id').trigger('change');
            },
            error: function() { $('#subcategory_id').html('<option value="0">All Subcategories</option>'); }
        });

  
  }

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions2(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Departments',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions1(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Status Values',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}
  function getOptionsCategory(isFilter) {
    return {
      enableFiltering: isFilter,
      enableCaseInsensitiveFiltering: isFilter,
      filterPlaceholder: 'Search ...',
      nonSelectedText: 'All Categories',
      numberDisplayed: 1,
      maxHeight: 400,
    }
  }
  function getOptionsSubCategory(isFilter) {
    return {
      enableFiltering: isFilter,
      enableCaseInsensitiveFiltering: isFilter,
      filterPlaceholder: 'Search ...',
      nonSelectedText: 'All Subcategories',
      numberDisplayed: 1,
      maxHeight: 400,
    }
  }
	$('#location_id').multiselect(getOptions(true));
	$('#department_id').multiselect(getOptions2(true));
	//$('#quote_status').multiselect(getOptions1(true));

  //$('#category_id').multiselect(getOptionsCategory(true));
  //$('#subcategory_id').multiselect(getOptionsSubCategory(true));


jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );


$.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
	locale: {
	    format: 'DD/MM/YYYY'
	}
  });
};

$(document).ready( function() {
    $('#quote_table').dataTable({
        "columnDefs": [ 
        	{ "targets": 0, "width": "6%", "orderable": false },
		/*	{ "type": "sort-month-year", targets: 3 },         
			{ "type": "sort-month-year", targets: 4 } */        
        ],
        "order": []
    });

	$('#from_date').daterangepicker({
	  singleDatePicker: true,
    autoUpdateInput: false,
	  singleClasses: "picker_3",
	  locale: {
		format: '<?php echo FunctionManager::dateFormatJS();?>'
	  }
	});

	$('#to_date').daterangepicker({
	  singleDatePicker: true,
    autoUpdateInput: false,
	  singleClasses: "picker_3",
	  locale: {
		format: '<?php echo FunctionManager::dateFormatJS();?>'
	  }
	});

  $('#to_date').on('apply.daterangepicker', function(ev, picker) {
    $('#to_date').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS();?>')); 
  });

  $('#from_date').on('apply.daterangepicker', function(ev, picker) {
    $('#from_date').val(picker.startDate.format('<?php echo FunctionManager::dateFormatJS();?>')); 
  });

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			  //loadDepartments(<?php //echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>
		
});

function addQuoteModal(e){
	e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('quotes/createByModal'); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_quote_cont').html(mesg);
            $('#create_quote').modal('show');   
        }
    });
}

var options = {
          series: [{ name: "Create RFP",
          data: [<?php echo  $quteCount;?>]
        }],
          chart: {
          download: false, 
          toolbar: {
            show: false
          },
          height: 239,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [<?php echo  $quteLocName;?>],
          labels: {
            style: {
              colors: colors,
              fontSize: '12px'
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#full_scoring_by_location_count"), options);
        chart.render();
 
</script>
<?php $this->renderPartial('/quotes/create_quote');?>

<style type="text/css">
.location .multiselect {width: 138%;}
.department .multiselect {width: 112%;}
.select2-container--default .select2-selection--single, .select2-container--default .select2-selection--multiple {
    border: none;
    border-bottom: 1px solid #eee !important;
}
.select2-container--default .select2-selection--multiple {
     border-radius: 0px !important;
}
.multiselect-container>li>a>label.checkbox, .multiselect-container>li>a>label.radio {color: #777;}
#quote_table_filter label{font-weight: 500;}
#quote_table_filter label input{font-weight: normal;}
</style>
