<?php

/**
 * This is the model class for table "quote_vendor_files".
 *
 * The followings are the available columns in table 'quote_vendor_files':
 * @property integer $id
 * @property integer $quote_id
 * @property integer $vendor_id
 * @property string $file_name
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class QuoteVendorFilesHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quote_vendor_files_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('quote_id, vendor_id, file_name,reopen_id, created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quote_id' => 'Quote',
			'vendor_id' => 'Vendor',
			'file_name' => 'File Name',
			
		);
	}

	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
