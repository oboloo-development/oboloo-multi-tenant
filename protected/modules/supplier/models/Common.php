<?php
/**
 * This is an abstract class (meaning it cannot be instanciated into an object)
 * and hence various model/database classes are derived from this one. Because
 * of this, various database access layer methods are abstracted in here. So any
 * of the derived classes then do not need to write separate queries for insert,
 * update and delete (at least for certain search by key values/columns cases).
 * Also a lot of select methods to retrieve one or multiple rows have also been
 * provided here which again eliminates writing separate SQL queries in each of
 * the derived classes for mundane purposes.
 */
class Common
{
    public $table_name = ''; /*!< table from which data is retrieved or updated to by this class */
	public $key_name = ''; /*!< primary key field/column name e.g. user_id or permit_id */
    public $alternate_key_name = '';
	public $rs = array(); /*!< current record/dataset values for various columns in the table in array format */
    public $fields = array(); /*!< current record/dataset values for various columns in the table in array format */
	public $timestamp = true; /*!< whether the table has fields for created and updated timestamps for each row or not */

	protected $db_conn = false; /*!< database connection object provided by YII framework - abstracted and hence protected */
	protected $log = false; /*!< whether to store log of various database activities or not - abstracted and hence protected */

	protected $currency_fields = array();
	
	/**
 	 * \param $key_name - primary key field/column name - optional
 	 * \param $table_name - database table name - optional
	 *
	 * Common model class constructor - initialize member variables
	 * The method just assigns input parameters to respective member
	 * variables and also instanciates a database connection using
	 * the YII framework provided database access layer methods. Finally
	 * it also creates an instance of the log class depending upon a
	 * global parameter to maintain an audit trail of changes that
	 * happen in the database due to various queries being executed.
	 */
	public function __construct($key_name = '', $table_name = '')
	{
		$this->table_name = $table_name;
		$this->key_name = $key_name;
		$this->db_conn = Yii::app()->db;
		if (Yii::app()->params['logChanges']) $this->log = new Log();
	}

	/**
 	 * \param $sql - SQL query to be executed - mandatory
 	 * \param $row - whether to return a single row for the query results - optional (default: false ie return everything as an array)
 	 * \return $results - query results in an array format
	 *
	 * This method provides an abstraction layer for executing ANY type of
	 * queries (be it SELECT, INSERT, UPDATE or DELETE). Depending upon the
	 * query being executed, this method then calls appropriate underlying
	 * YII framework provided database access layer function and returns
	 * the result in an array format. It also does rudimentary error checks
	 * such as e.g. when database connection information is incorrect, then
	 * it will just return blank result i.e. fail and exit in a graceful way
	 */
	public function executeQuery($sql, $row = false)
	{
		// print_r($this->db_conn); exit;
		// whether this is a database update (delete, insert included) query?
		if (substr(rtrim(ltrim(strtoupper($sql))), 0, 7) != "SELECT ")
		{
			// if ($this->log) $this->log->log($sql, $this->rs);
			return $this->db_conn ? $this->db_conn->createCommand($sql)->execute() : 0;
		}
		// for select queries, we need to return the query results in an appropriate format
		else 
		{
			$result = array();
			if (!empty($sql) && $this->db_conn)
			{
				if ($row) $result = $this->db_conn->createCommand($sql)->queryRow();
				else $result = $this->db_conn->createCommand($sql)->queryAll();
				if ($result && is_array($result) && count($this->currency_fields)) 
					$result = $this->convertCurrencyFields($result, $row);
			}
			return $result;
		}
	}
	
	private function convertCurrencyFields($result, $row)
	{
		
		if (!isset(Yii::app()->session['currency_rates']))
		{
			$currency_rates = array();
			foreach ($this->db_conn->createCommand("SELECT * FROM currency_rates")->queryAll() as $currency_rate)
				$currency_rates[$currency_rate['currency']] = $currency_rate['rate'];
			Yii::app()->session['currency_rates'] = $currency_rates;
		}
		
		
		$output = array();
		$excludeFunc = array("edit","getproducts");
		$excludeCont = array("contracts");
		$actionID = Yii::app()->controller->action->id;
		$contID = Yii::app()->controller->id;
		if (!in_array($actionID,$excludeFunc) && !in_array($contID,$excludeCont) && isset(Yii::app()->session['user_currency']) && !empty(Yii::app()->session['user_currency']))
		{  
			$currency_rate = 0;
			$currency_rates = Yii::app()->session['currency_rates'];
			$user_currency = Yii::app()->session['user_currency'];
			if (isset($currency_rates[$user_currency])) $currency_rate = $currency_rates[$user_currency];
			
			if (!empty($currency_rate))
			{
				if ($row)
				{
					foreach ($result as $column => $value)
					{
						if (in_array($column, $this->currency_fields))
							$output[$column] = number_format($value * $currency_rate, 2, '.', '');
						else $output[$column] = $value;
					}
				}
				else
				{
					foreach ($result as $row_index => $row_data)
					{
						$output_row = array();
						foreach ($row_data as $column => $value)
						{
							if (in_array($column, $this->currency_fields))
								$output_row[$column] =  number_format($value * $currency_rate, 2, '.', '');
							else $output_row[$column] = $value;
						}
						$output[$row_index] = $output_row;
					}
				}
			}  
			else $output = $result;
		}
		else $output = $result;
		//echo "<pre>";print_r($output);exit;
		return $output;
		
	}

	/**
 	 * \param $dataset - array containing total data to be displayed - optional
 	 * \return $total_pages - number of pages the above data will be split/displayed in
	 *
	 * This method is used to calculate total number of pages for pagination in various
	 * places such as user list, tax parcel list and fee transactions search results list
	 * etc. Depending upon the global page size variable/paramter value and of course the
	 * input dataset row count, the total pages are calculated and returned.
	 */
	public function calculateTotalPages($dataset = array())
	{
		return count($dataset) <= 0 ? 0 : (count($dataset) <= Yii::app()->params['pageSize'] ? 1 :
					intval(count($dataset) / Yii::app()->params['pageSize']) + (count($dataset) % Yii::app()->params['pageSize'] != 0 ? 1 : 0));
	}

	/**
 	 * \param $dataset - array containing total data to be displayed - optional
 	 * \param $page_number - which "page" or slice of the data is needed - optional (default: 1 ie first page)
 	 * \return $sliced_data - slice of the data for the needed page number
	 *
	 * This method is used to slice up the input data set by global page size value
	 * and then return the required slice using PHP provided array slice method. It
	 * is used in various places like tax parcels and users listing for pagination
	 */
	public function retrieveDataPage($dataset = array(), $page_number = 1)
	{
		return array_slice($dataset, ($page_number - 1) * Yii::app()->params['pageSize'], Yii::app()->params['pageSize']);
	}

	/**
 	 * \return $total_rows - total number of rows in the table that this class refers to
	 *
	 * This method is used to calculate total number of rows in the table that this class
	 * refers to. Most of the times, this method is used in conjunction with pagination
	 */
	public function getTotalRows()
	{
		$count_data = $this->executeQuery("SELECT COUNT(*) AS total_rows FROM {$this->table_name}", true);
		return $count_data && is_array($count_data) && isset($count_data['total_rows']) ? $count_data['total_rows'] : 0;
	}

	/**
 	 * \param $total_rows - row count for the data set to be split into pages - optional
 	 * \return $total_pages - total number of pages that ALL data in the table is split into
	 *
	 * This method is used to calculate total number of rows in the table that this class
	 * refers to. Most of the times, this method is used in conjunction with pagination
	 */
	public function getTotalPages($total_rows = 0)
	{
		if ($total_rows == 0) $total_rows = $this->getTotalRows();
		return $total_rows == 0 ? 0 : ($total_rows <= Yii::app()->params['pageSize'] ? 1 :
					intval($total_rows / Yii::app()->params['pageSize']) + ($total_rows % Yii::app()->params['pageSize'] != 0 ? 1 : 0));
	}

	/**
 	 * \param $conditions - retrieve data matching the conditions provided in this array - optional
	 * \param $searchable - return data in an array which is searchable in PHP by the primary key - optional
 	 * \return $total_data - data from the database matching the conditions provided above
	 *
	 * This method is used to retrieve multiple rows from the table matching provided conditions.
	 */
	public function getAll($conditions = array(), $searchable = false)
	{
		if ($searchable)
		{
			$data = array();
			foreach ($this->getData($conditions) as $row) $data[$row[$this->key_name]] = $row;
			return $data;
		}
		else
        {
            if (!in_array('order', array_map('strtolower', array_keys($conditions))))
                $conditions['order'] = '`value`';
            return $this->getData($conditions);
        }
	}

	/**
 	 * \param $conditions - retrieve single row matching the conditions provided in this array - optional
 	 * \return $data_row - single data row from the database matching the conditions provided above
	 *
	 * This method is used to retrieve a single data row from the table matching provided conditions.
	 */
	public function getOne($conditions = array())
	{
		return $this->getData($conditions, true);
	}

	/**
 	 * \param $conditions - retrieve data matching the conditions provided in this array - optional
	 * \param $row - whether to return data as a single row or then in an array format - optional
 	 * \return $data_records - data from the database matching the conditions provided above
	 *
	 * This method is used to retrieve a set of rows (single or multiple) matching provided conditions.
	 */
	public function getData($conditions = array(), $row = false)
	{
		return $this->executeQuery("SELECT * FROM {$this->table_name} " . $this->getClause($conditions) . ($row ? ' LIMIT 1 ' : ' '), $row);
	}

	/**
 	 * \param $conditions - delete data matching the conditions provided in this array - optional
	 *
	 * This method is used to delete a set of rows (single or multiple) matching provided conditions.
	 */
	public function delete($conditions = array())
	{
		return $this->executeQuery("DELETE FROM {$this->table_name} " . $this->getClause($conditions, true));
	}

	/**
 	 * \param $conditions - build actual SQL query using the conditions provided in this array - optional
	 *
	 * This method is used to build actual SQL query using the conditions provided.
	 */
	private function getClause($conditions = array(), $delete_flag = false)
	{
		$limit = $order = '';
		$clause = ' WHERE 1 = 1 ';

		// each condition has a column part and a value
		// whether equal to or IN or then not equal to
		foreach ($conditions as $column => $value)
		{
			// ORDER is a keyword in SQL and not a column
			if (strtoupper($column) == 'ORDER')
			{
				if (is_array($value))
				{
					$orderby_columns = '';
					foreach ($value as $orderby)
						if (empty($orderby_columns)) $orderby_columns = $orderby;
						else $orderby_columns = $orderby_columns . ', ' . $orderby;
					$order = " ORDER BY $orderby_columns ";
				}
				else $order = " ORDER BY $value ";
			}
			// same goes with LIMIT clause/keyword
			else if (strtoupper($column) == 'LIMIT')
			{
				if (is_array($value)) $limit = " LIMIT {$value[0]}, {$value[1]} ";
				else $limit = " LIMIT $value ";
			}
			// and then there are a few other keywords in SQL
			else if (strpos(strtoupper($column), ' NOT LIKE') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" NOT LIKE", "", $column))) . "` NOT LIKE '$value' ";
			else if (strpos(strtoupper($column), ' LIKE') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" LIKE", "", $column))) . "` LIKE '$value' ";
			else if (strpos(strtoupper($column), ' NOT IN') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" NOT IN", "", $column))) . "` NOT IN ( " . implode(",", $value) . " ) ";
			else if (strpos(strtoupper($column), ' IN') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" IN", "", $column))) . "` IN ( " . implode(",", $value) . " ) ";
			// for everything else, it's just a matching column and value combination
			else if (strpos(strtoupper($column), ' !=') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" !=", "", $column))) . "` != '$value' ";
			else if (strpos(strtoupper($column), ' >=') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" >=", "", $column))) . "` >= '$value' ";
			else if (strpos(strtoupper($column), ' <=') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" <=", "", $column))) . "` <= '$value' ";
			else if (strpos(strtoupper($column), ' >') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" >", "", $column))) . "` > '$value' ";
			else if (strpos(strtoupper($column), ' <') !== false)
				$clause .= " AND `" . rtrim(ltrim(str_ireplace(" <", "", $column))) . "` < '$value' ";
			else $clause .= " AND `$column` = '$value' ";
		}

        // order some values in alphabetical order by default
        // if (!$delete_flag && empty($order)) $order = " ORDER BY `value` ";

		// sql clause usually goes where blah order by blah and finally limit yadaa yadaa
		return $clause . ' ' . $order . ' ' . $limit;
	}

	/**
 	 * \param $page_number - which page/slice number of the data is needed for pagination - mandatory
 	 * \param $order_by - which column to sort returned data by - optional
 	 * \param $sort_direction - which direction to sort returned data by i.e. ASC or DESC - optional
 	 * \param $sql - execute this particular query instead of running a bland SELECT * - optional
 	 * \return $total_data - data from the database matching the conditions provided above
	 *
	 * This method is used to build additional condition parameters array (including order
	 * by and limit clauses) and then return data from the database table for that page.
	 */
	function getDataPage($page_number, $order = '', $direction = 'ASC', $sql = '')
	{
		if (empty($sql))
		{
			$conditions = array('LIMIT' => array(($page_number - 1) * Yii::app()->params['pageSize'], Yii::app()->params['pageSize']));
			if (!empty($order)) $conditions['ORDER'] = $order . ' ' . $direction;
			return $this->getData($conditions);
		}
		else return $this->executeQuery($sql . " ORDER BY $order $direction LIMIT " .
				(($page_number - 1) * Yii::app()->params['pageSize']) . ", " . Yii::app()->params['pageSize']);
	}

	/**
 	 * \return $key_value - Primary key value of the record set contained in this class
	 *
	 * This method is used to retrieve the primary key value of the record set contained in this class.
	 */
	public function getPrimaryKeyValue()
	{
		return isset($this->rs[$this->key_name]) ? $this->rs[$this->key_name] : 0;
	}

	/**
 	 * \param $modify_timestamp - whether to modify timestamp values in the database or not - optional (default: TRUE)
	 *
	 * This method is used to write the current record/dataset values to the database.
	 * Depending upon whether the primary key column value is already present in the
	 * record or not, the method calls "insert" or "update" operations appropriately.
	 */
	public function write($modify_timestamp = true)
	{
		// if primary key value is not set, then insert a new row
		if (!isset($this->rs[$this->key_name]) || !$this->rs[$this->key_name] || empty($this->rs[$this->key_name]))
		{
			if ($this->timestamp)
			{
				$this->rs['created_datetime'] = date("Y-m-d H:i:s");
				$this->rs['updated_datetime'] = $this->rs['created_datetime'];
			}
			$this->create();
			$this->rs[$this->key_name] = Yii::app()->db->getLastInsertID();
		}
		// if primary key value is set, then update the row referred to by it
		else
		{
			if ($modify_timestamp && $this->timestamp)
				$this->rs['updated_datetime'] = date("Y-m-d H:i:s");
			$this->update();
		}
	}

	/**
	 * This method is used to update the current record/dataset values to the database.
	 */
	public function update()
	{
		$clause = $updates = "";

		foreach ($this->rs as $column => $value)
		{
			if ($column != $this->key_name)
			{
                if ($value == "0000-00-00" && strlen($value) == 10)
                {
    				if ($updates == "") $updates = " `$column` = NULL ";
    				else $updates = $updates . ", `$column` = NULL ";
                }
                else
                {
    				if ($updates == "") $updates = " `$column` = '$value' ";
    				else $updates = $updates . ", `$column` = '$value' ";
                }
			}
			else $clause = " WHERE `$column` = '$value' ";
		}

		$this->executeQuery("UPDATE {$this->table_name} SET $updates $clause");
	}

	/**
	 * This method is used to create a new row in the database using the current record/dataset values.
	 */
	public function create()
	{
		$columns = $values = "";

		foreach ($this->rs as $column => $value)
		{
			if ($column == $this->key_name) continue;
			if ($columns == "") $columns = " `$column` "; else $columns .= " , `$column` ";
            if ($value == "0000-00-00" && strlen($value) == 10)
            {
			    if ($values == "") $values = " NULL "; else $values .= " , NULL ";
            }
            else
            {
			    if ($values == "") $values = " '$value' "; else $values .= " , '$value' ";
            }
		}

		$this->executeQuery("INSERT INTO {$this->table_name} ( $columns ) VALUES ( $values ) ");
	}

	/**
 	 * \param $id - primary key value for the record for which navigational information is needed - mandatory
	 * \return $info - array containing various navigational information such as first, next, previous, total and index of records
	 *
	 * This method is used to find out complete record navigation information for the
	 * input record id. This includes information such as whether this is the first or
	 * last record or not, what are the next and previous record numbers related to this
	 * record and finally also what is the "index" of this record within total records.
	 */
	public function getRecordInfo($id)
	{
		$count_data = $this->executeQuery("SELECT COUNT(*) AS total_rows FROM {$this->table_name}", true);
		$total_rows = $count_data && is_array($count_data) && isset($count_data['total_rows']) ? $count_data['total_rows'] : 0;

		$left_data = $this->executeQuery("SELECT COUNT(*) AS left_rows FROM {$this->table_name} WHERE {$this->key_name} < '$id'", true);
		$left_rows = $left_data && is_array($left_data) && isset($left_data['left_rows']) ? $left_data['left_rows'] : 0;

		$first_data = $this->executeQuery("SELECT {$this->key_name} FROM {$this->table_name} ORDER BY {$this->key_name} LIMIT 1", true);
		$first_id = $first_data && is_array($first_data) && isset($first_data[$this->key_name]) ? $first_data[$this->key_name] : 0;

		$prev_data = $this->executeQuery("SELECT {$this->key_name} FROM {$this->table_name}
										   WHERE {$this->key_name} < '$id' ORDER BY {$this->key_name} DESC LIMIT 1", true);
		$prev_id = $prev_data && is_array($prev_data) && isset($prev_data[$this->key_name]) ? $prev_data[$this->key_name] : 0;

		$next_data = $this->executeQuery("SELECT {$this->key_name} FROM {$this->table_name}
										   WHERE {$this->key_name} > '$id' ORDER BY {$this->key_name} LIMIT 1", true);
		$next_id = $next_data && is_array($next_data) && isset($next_data[$this->key_name]) ? $next_data[$this->key_name] : 0;

		$last_data = $this->executeQuery("SELECT {$this->key_name} FROM {$this->table_name} ORDER BY {$this->key_name} DESC LIMIT 1", true);
		$last_id = $last_data && is_array($last_data) && isset($last_data[$this->key_name]) ? $last_data[$this->key_name] : 0;

		return array('total' => $total_rows, 'left' => $left_rows, 'current' => $left_rows + 1, 'right' => $total_rows - $left_rows - 1,
						'first_id' => $first_id, 'last_id' => $last_id, 'prev_id' => $prev_id, 'next_id' => $next_id);
	}

	/**
 	 * \param $tables - additional tables if any that need to be locked for this transaction - optional (default: blank/none)
 	 * \param $mode - mode in which table is to be locked e.g. read or write - optional (default: write lock)
	 *
 	 * This method is used to lock table in a particular mode
	 * (read/write) for database transactions to be performed
	 */
	public function lockTable($tables = "", $mode = "WRITE")
	{
		if (empty($tables)) $this->executeQuery("LOCK TABLES {$this->table_name} $mode");
		else
		{
			$lock_tables = $this->table_name . " " . $mode;
			foreach (explode(",", $tables) as $table)
				$lock_tables .= ", " . rtrim(ltrim($table)) . " " . $mode . " ";
			$this->executeQuery("LOCK TABLES $lock_tables");
		}
	}

	/**
	 * This method is used to unlock tables after transactions have been performed
	 */
	public function unlockTable()
	{
		$this->executeQuery("UNLOCK TABLES");
	}

    public function translateValueToId($input_value = "")
    {
        $value = rtrim(ltrim(preg_replace('/\s\s+/', ' ', $input_value)));
        if (empty($value) || strtoupper($value) == '<SELECT>') return 0;
        else
        {
		    $value_data = $this->executeQuery("SELECT * FROM {$this->table_name} WHERE `value` = '$value' LIMIT 1", true);
		    if ($value_data && is_array($value_data) && isset($value_data['id']) && $value_data['id'])
                return $value_data['id'];
            else
            {
                $this->executeQuery("INSERT INTO {$this->table_name} ( `value` ) SELECT '$value' ");
                return Yii::app()->db->getLastInsertID();
            }
        }
    }

    public function getDisplay($column_names = false, $additional_filter = "")
    {
        $select_column_names = "value";
        if ($column_names)
        {
            if (is_array($column_names))
                $select_column_names = implode(",", $column_names);
            else $select_column_names = $column_names;
        }

        $output = "";
        if (is_array($column_names)) $output = "<table border='0'>";

        $values_data = $this->executeQuery
            ("SELECT DISTINCT $select_column_names FROM {$this->table_name} WHERE 1 = 1 $additional_filter ORDER BY $select_column_names");
        if ($values_data && is_array($values_data))
        {
            foreach ($values_data as $value_row)
            {
                $current_data_row = "";

                if (is_array($column_names))
                {
                    foreach ($column_names as $one_column)
                        $current_data_row .= "<td style='min-width: 30px; padding-left: 10px;'>" . $value_row[$one_column] . "</td>";
                    $current_data_row = "<tr>" . $current_data_row . "</tr>";
                    $output .= $current_data_row;
                }
                else $output .= $value_row[$select_column_names] . "<br />";
            }
        }

        if (is_array($column_names)) $output .= '</table>';
        return "<div style='overflow-y: auto; max-height: 350px;'>" . $output . '</div>';
    }

    public function getSchema()
	{
		return $fields;
	}
	

	public function getIdForName($name = "")
	{
		$id = 0;
		
		if (!empty($name))
		{
			$data = $this->getData(array($this->alternate_key_name => $name));
			if ($data && is_array($data) && isset($data[$this->key_name]) && $data[$this->key_name])
				$id = $data[$this->key_name];
			else
			{
				// $name = mysql_escape_string($name);
                $this->executeQuery("INSERT INTO {$this->table_name} ( {$this->alternate_key_name} ) SELECT '$name' ");
                $id = Yii::app()->db->getLastInsertID();
			}
		}
		
		return $id; 
	}

		

    public function saveData($data = array())
    { 	
		if (!isset(Yii::app()->session['currency_rates']))
		{
			$currency_rates = array();
			foreach ($this->db_conn->createCommand("SELECT * FROM currency_rates")->queryAll() as $currency_rate)
				$currency_rates[$currency_rate['currency']] = $currency_rate['rate'];
			Yii::app()->session['currency_rates'] = $currency_rates;
		}
		
		$currency_rate = 0;
		$currency_rates = Yii::app()->session['currency_rates'];
		$user_currency = Yii::app()->session['user_currency'];
		if (isset($currency_rates[$user_currency])) $currency_rate = $currency_rates[$user_currency];
			
        $this->rs = array();


        if ($this->table_name =="products"){
         	$this->fields['contract_id'] ='L';
        }
        if ($this->table_name =="products"){
         	$this->fields['currency_id'] ='N';
        }
        
        foreach ($this->fields as $field_name => $field_type)
        {
            $input_value = Yii::app()->input->post($field_name);
            if (is_array($input_value))
                $input_value = implode(",", $input_value);

            if (isset($data[$field_name]))
                $input_value = $this->rs[$field_name] = $data[$field_name];
            else $this->rs[$field_name] = $input_value;

            if ($this->table_name !="products" && ($field_type == 'N' || $field_type == 'L'))
			{
	            if (empty($this->rs[$field_name])) $this->rs[$field_name] = 0;
				else $this->rs[$field_name] = str_replace(",", "", $this->rs[$field_name]);
			}
			
            if ($field_type == 'D')
            {
                if (empty($input_value)) unset($this->rs[$field_name]);
                else 
                {
                	$input_value = rtrim(ltrim($input_value));
                	if (strpos($input_value, ' ') !== false)
                	{
						list($date_part, $time_part, $ampm) = explode(" ", $input_value);
    	            	list($input_date, $input_month, $input_year) = explode("/", $date_part);
	                	// Below commented on appril 18/2019	                	
    	            	//$this->rs[$field_name] = date("Y-m-d H:i:s", strtotime($input_year . '-' . $input_month . '-' . $input_date . ' ' . $time_part . $ampm));

    	            	$this->rs[$field_name] = date("Y-m-d H:i:s", strtotime(strtr($input_value, '/', '-')));

					}
                	else
                	{  
    	            	/*
						// Below commented on appril 18/2019	
    	            	list($input_date, $input_month, $input_year) = explode("/", $input_value);
	                	$this->rs[$field_name] = $input_year . '-' . $input_month . '-' . $input_date;*/
	                	$this->rs[$field_name] = date("Y-m-d", strtotime(strtr($input_value, '/', '-')));
					}
				}
            }

            if ($field_name == 'password' && isset($this->rs[$field_name]))
            {
                if (empty($input_value)) unset($this->rs[$field_name]);
                else $this->rs[$field_name] = md5($this->rs[$field_name]);
            }
			
			if (in_array($field_name, $this->currency_fields) && !empty($currency_rate))
				$this->rs[$field_name] = $this->rs[$field_name] / $currency_rate;

            /*
            if ($field_type != 'N')
                $this->rs[$field_name] = mysql_escape_string($this->rs[$field_name]);
            */
               
        }



        $this->write();

    }

    public function importData($filename)
    {
        // if file cannot be read ... some problem
		$fp = fopen($filename, 'r');
		if (!$fp) echo "Cannot read data file to be imported";

        // read every record from the file and process ...
        while (($record = fgetcsv($fp)) !== false)
		{
            // read all the fields in member variable
            $this->rs = array($this->key_name => 0);
            $field_index = 0;

            foreach ($this->fields as $field_name => $field_type)
            {
                if ($field_name == $this->key_name) continue;
                if (isset($record[$field_index]))
                {
                    if ($field_type == 'L')
                    {
                        $class_name = str_replace(' ', '', ucwords(str_replace('_', ' ', str_replace('_id', '', $field_name))));
                        $lookup = new $class_name();
                        $this->rs[$field_name] = $lookup->translateValueToId($record[$field_index]);
                    }
                    else $this->rs[$field_name] = $record[$field_index];
                }
                else
                    $this->rs[$field_name] = ($field_type == 'N' || $field_type == 'L') ? 0 : "";

                if ($field_name == 'password' && isset($this->rs[$field_name]))
                {
                    if (empty($input_value)) unset($this->rs[$field_name]);
                    else $this->rs[$field_name] = md5($this->rs[$field_name]);
                }
                $field_index += 1;
            }

            // check if this is an existing record
            if (!empty($this->alternate_key_name))
            {
                $existing_data = $this->getOne(array($this->alternate_key_name => $this->rs[$this->alternate_key_name]));
                if ($existing_data && is_array($existing_data) && isset($existing_data[$this->key_name]))
                    $this->rs[$this->key_name] = $existing_data[$this->key_name];
            }

            // and now update the db values
            $this->write();
        }
    }

	public function export($data = array(), $filename = 'export')
	{
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
		
		if (is_array($data) && count($data))
		{
			$header = $data[0];
			$total_columns = count($header);
			$column_index = 0;
			foreach (array_keys($header) as $column_name)
			{
				$column_index += 1;
				if (!is_array($header[$column_name]))
					echo '"' . ucwords(str_replace("_", " ", $column_name)) . '"';
				else
				{
					$column_header_index = 0;
					foreach (array_keys($header[$column_name][0]) as $column_header_name)
					{
						$column_header_index += 1;
						echo '"' . ucwords(str_replace("_", " ", $column_header_name)) . '"';
						if ($column_header_index < count($header[$column_name][0])) echo ",";
					}
				}
				if ($column_index < $total_columns) echo ",";
				else echo "\n";
			}
			
			foreach ($data as $row)
			{
				$column_index = 0;
				foreach ($row as $data_point)
				{
					$column_index += 1;
					if (is_array($data_point))
					{
						echo "\n";					
						foreach ($data_point as $data_element)
						{
							for ($i=1; $i<$column_index; $i++) echo '"",';
							$data_index = 0;
							foreach ($data_element as $data_node)
							{
								if (is_numeric($data_node) && intval($data_node) != $data_node)
									$data_node = round($data_node, 2);
								
								echo '"' . $data_node . '"';
								$data_index += 1;
								if ($data_index < count($data_element)) echo ",";
							}
							echo "\n";
						}						
					}
					else 
					{
						if (is_numeric($data_point) && intval($data_point) != $data_point)
							$data_point = round($data_point, 2);
						
						echo '"' . $data_point . '"';
						if ($column_index < $total_columns) echo ",";
						else echo "\n";
					}
				}
			}
		}
	}

}
