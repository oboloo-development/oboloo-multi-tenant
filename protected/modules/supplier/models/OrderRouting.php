<?php

/**
 * This is the model class for table "order_routing".
 *
 * The followings are the available columns in table 'order_routing':
 * @property string $id
 * @property string $approval_type
 * @property integer $order_id
 * @property integer $user_id
 * @property string $routing_status
 * @property string $routing_date
 * @property string $routing_comments
 * @property integer $status
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class OrderRouting extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_routing';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, user_id, status', 'numerical', 'integerOnly'=>true),
			array('approval_type', 'length', 'max'=>1),
			array('routing_status', 'length', 'max'=>11),
			array('routing_date, routing_comments, created_datetime, updated_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, approval_type, order_id, user_id, routing_status, routing_date, routing_comments, status, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'approval_type' => 'Approval Type',
			'order_id' => 'Order',
			'user_id' => 'User',
			'routing_status' => 'Routing Status',
			'routing_date' => 'Routing Date',
			'routing_comments' => 'Routing Comments',
			'status' => 'Status',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('approval_type',$this->approval_type,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('routing_status',$this->routing_status,true);
		$criteria->compare('routing_date',$this->routing_date,true);
		$criteria->compare('routing_comments',$this->routing_comments,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderRouting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
