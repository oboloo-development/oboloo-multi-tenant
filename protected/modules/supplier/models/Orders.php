<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property string $order_id
 * @property integer $location_id
 * @property integer $department_id
 * @property integer $project_id
 * @property integer $vendor_id
 * @property integer $user_id
 * @property string $user_name
 * @property string $user_email
 * @property integer $currency_id
 * @property double $currency_rate
 * @property string $order_date
 * @property string $description
 * @property double $total_price
 * @property double $calc_price
 * @property double $tax_rate
 * @property double $ship_amount
 * @property double $other_charges
 * @property double $discount
 * @property string $spend_type
 * @property string $order_status
 * @property string $invoice_number
 * @property string $recurrent_number
 * @property integer $created_by_user_id
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class Orders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('location_id, department_id, user_name, user_email, currency_id', 'required'),
			array('location_id, department_id, project_id, vendor_id, user_id, currency_id, created_by_user_id', 'numerical', 'integerOnly'=>true),
			array('currency_rate, total_price, calc_price, tax_rate, ship_amount, other_charges, discount', 'numerical'),
			array('user_name, user_email', 'length', 'max'=>120),
			array('spend_type', 'length', 'max'=>8),
			array('order_status', 'length', 'max'=>33),
			array('invoice_number, recurrent_number', 'length', 'max'=>100),
			array('order_date, description, created_datetime, updated_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('order_id, location_id, department_id, project_id, vendor_id, user_id, user_name, user_email, currency_id, currency_rate, order_date, description, total_price, calc_price, tax_rate, ship_amount, other_charges, discount, spend_type, order_status, invoice_number, recurrent_number, created_by_user_id, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'order_id' => 'Order',
			'location_id' => 'Location',
			'department_id' => 'Department',
			'project_id' => 'Project',
			'vendor_id' => 'Vendor',
			'user_id' => 'User',
			'user_name' => 'User Name',
			'user_email' => 'User Email',
			'currency_id' => 'Currency',
			'currency_rate' => 'Currency Rate',
			'order_date' => 'Order Date',
			'description' => 'Description',
			'total_price' => 'Total Price',
			'calc_price' => 'Calc Price',
			'tax_rate' => 'Tax Rate',
			'ship_amount' => 'Ship Amount',
			'other_charges' => 'Other Charges',
			'discount' => 'Discount',
			'spend_type' => 'Spend Type',
			'order_status' => 'Order Status',
			'invoice_number' => 'Invoice Number',
			'recurrent_number' => 'Recurrent Number',
			'created_by_user_id' => 'Created By User',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('order_id',$this->order_id,true);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('department_id',$this->department_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('currency_id',$this->currency_id);
		$criteria->compare('currency_rate',$this->currency_rate);
		$criteria->compare('order_date',$this->order_date,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('total_price',$this->total_price);
		$criteria->compare('calc_price',$this->calc_price);
		$criteria->compare('tax_rate',$this->tax_rate);
		$criteria->compare('ship_amount',$this->ship_amount);
		$criteria->compare('other_charges',$this->other_charges);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('spend_type',$this->spend_type,true);
		$criteria->compare('order_status',$this->order_status,true);
		$criteria->compare('invoice_number',$this->invoice_number,true);
		$criteria->compare('recurrent_number',$this->recurrent_number,true);
		$criteria->compare('created_by_user_id',$this->created_by_user_id);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
