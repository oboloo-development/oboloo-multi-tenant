<?php

/**
 * This is the model class for table "quote_vendor_details".
 *
 * The followings are the available columns in table 'quote_vendor_details':
 * @property string $id
 * @property integer $quote_id
 * @property integer $vendor_id
 * @property string $product_name
 * @property double $tax_rate
 * @property double $shipping
 * @property double $other_charges
 * @property integer $quantity
 * @property double $unit_price
 * @property string $vendor_product_notes
 */
class QuoteVendorSubmissionHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $product_tax_rate;
	public $product_shipping;
	public $product_other_charges;
	public function tableName()
	{
		return 'quote_vendor_submission_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quote_id, vendor_id, quantity', 'numerical', 'integerOnly'=>true),
			array('tax_rate, shipping, other_charges, unit_price', 'numerical'),
			array('product_name', 'length', 'max'=>255),
			array('product_code,quote_start_date,quote_end_date,created_at,vendor_repsond_date,reopen_id','safe'),
			array('vendor_product_notes', 'length', 'max'=>1024),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, quote_id, vendor_id, product_name, tax_rate, shipping, other_charges, quantity, unit_price, vendor_product_notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quote_id' => 'Quote',
			'vendor_id' => 'Vendor',
			'product_name' => 'Product Name',
			'tax_rate' => 'Tax Rate',
			'shipping' => 'Shipping',
			'other_charges' => 'Other Charges',
			'quantity' => 'Quantity',
			'unit_price' => 'Unit Price',
			'vendor_product_notes' => 'Vendor Product Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('quote_id',$this->quote_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('product_name',$this->product_name,true);
		$criteria->compare('tax_rate',$this->tax_rate);
		$criteria->compare('shipping',$this->shipping);
		$criteria->compare('other_charges',$this->other_charges);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('vendor_product_notes',$this->vendor_product_notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuoteVendorDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
