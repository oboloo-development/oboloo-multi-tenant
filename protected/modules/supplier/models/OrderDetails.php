<?php

/**
 * This is the model class for table "order_details".
 *
 * The followings are the available columns in table 'order_details':
 * @property string $id
 * @property integer $order_id
 * @property string $product_type
 * @property integer $product_id
 * @property integer $vendor_id
 * @property integer $quantity
 * @property double $unit_price
 * @property double $tax
 * @property string $calc_tax
 * @property double $shipping_cost
 * @property string $calc_unit_price
 */
class OrderDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vendor_id, calc_tax, calc_unit_price', 'required'),
			array('order_id, product_id, vendor_id, quantity', 'numerical', 'integerOnly'=>true),
			array('unit_price, tax, shipping_cost', 'numerical'),
			array('product_type', 'length', 'max'=>7),
			array('calc_tax, calc_unit_price', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, order_id, product_type, product_id, vendor_id, quantity, unit_price, tax, calc_tax, shipping_cost, calc_unit_price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Order',
			'product_type' => 'Product Type',
			'product_id' => 'Product',
			'vendor_id' => 'Vendor',
			'quantity' => 'Quantity',
			'unit_price' => 'Unit Price',
			'tax' => 'Tax',
			'calc_tax' => 'Calc Tax',
			'shipping_cost' => 'Shipping Cost',
			'calc_unit_price' => 'Calc Unit Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('product_type',$this->product_type,true);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('tax',$this->tax);
		$criteria->compare('calc_tax',$this->calc_tax,true);
		$criteria->compare('shipping_cost',$this->shipping_cost);
		$criteria->compare('calc_unit_price',$this->calc_unit_price,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
