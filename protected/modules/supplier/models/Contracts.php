<?php

/**
 * This is the model class for table "contracts".
 *
 * The followings are the available columns in table 'contracts':
 * @property string $contract_id
 * @property integer $location_id
 * @property integer $department_id
 * @property integer $project_id
 * @property integer $vendor_id
 * @property integer $currency_id
 * @property integer $user_id
 * @property double $currency_rate
 * @property string $contract_reference
 * @property string $po_number
 * @property string $cost_centre
 * @property string $break_clause
 * @property string $contract_title
 * @property string $contact_name
 * @property string $contact_info
 * @property integer $phone_number
 * @property string $website
 * @property string $contract_description
 * @property string $contract_comments
 * @property string $termination_terms
 * @property string $termination_comments
 * @property string $extension_options
 * @property string $extension_comments
 * @property string $contract_type
 * @property string $contract_start_date
 * @property string $contract_end_date
 * @property string $original_end_date
 * @property double $contract_value
 * @property double $calc_contract_value
 * @property double $contract_budget
 * @property double $original_budget
 * @property double $original_annual_budget
 * @property string $spend_type
 * @property integer $contract_status
 * @property string $contract_manager
 * @property integer $sbo_user_id
 * @property string $commercial_lead
 * @property integer $commercial_user_id
 * @property string $procurement_lead
 * @property integer $procurement_user_id
 * @property integer $contract_category_id
 * @property integer $contract_subcategory_id
 * @property string $contract_workstream
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class Contracts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $category;
	public $subcategory;
	public $currency;
	public function tableName()
	{
		return 'contracts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('currency_id, currency_rate, po_number, cost_centre, break_clause, contact_name, contact_info, phone_number, website, contract_comments, termination_terms, termination_comments, extension_options, extension_comments, commercial_lead, procurement_lead', 'required'),
			array('location_id, department_id, project_id, vendor_id, currency_id, user_id, phone_number, contract_status, sbo_user_id, commercial_user_id, procurement_user_id, contract_category_id, contract_subcategory_id', 'numerical', 'integerOnly'=>true),
			array('currency_rate, contract_value, calc_contract_value, contract_budget, original_budget, original_annual_budget', 'numerical'),
			array('contract_reference, po_number, cost_centre, break_clause, termination_terms, extension_options', 'length', 'max'=>50),
			array('contract_title, contact_name, website', 'length', 'max'=>200),
			array('contact_info, contract_description, contract_comments, termination_comments, extension_comments, contract_workstream', 'length', 'max'=>1024),
			array('contract_type', 'length', 'max'=>10),
			array('spend_type', 'length', 'max'=>8),
			array('contract_manager, commercial_lead, procurement_lead', 'length', 'max'=>255),
			array('contract_start_date, contract_end_date, original_end_date, created_datetime, updated_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('contract_id, location_id, department_id, project_id, vendor_id, currency_id, user_id, currency_rate, contract_reference, po_number, cost_centre, break_clause, contract_title, contact_name, contact_info, phone_number, website, contract_description, contract_comments, termination_terms, termination_comments, extension_options, extension_comments, contract_type, contract_start_date, contract_end_date, original_end_date, contract_value, calc_contract_value, contract_budget, original_budget, original_annual_budget, spend_type, contract_status, contract_manager, sbo_user_id, commercial_lead, commercial_user_id, procurement_lead, procurement_user_id, contract_category_id, contract_subcategory_id, contract_workstream, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'contract_id' => 'Contract',
			'location_id' => 'Location',
			'department_id' => 'Department',
			'project_id' => 'Project',
			'vendor_id' => 'Vendor',
			'currency_id' => 'Currency',
			'user_id' => 'User',
			'currency_rate' => 'Currency Rate',
			'contract_reference' => 'Contract Reference',
			'po_number' => 'Po Number',
			'cost_centre' => 'Cost Centre',
			'break_clause' => 'Break Clause',
			'contract_title' => 'Contract Title',
			'contact_name' => 'Contact Name',
			'contact_info' => 'Contact Info',
			'phone_number' => 'Phone Number',
			'website' => 'Website',
			'contract_description' => 'Contract Description',
			'contract_comments' => 'Contract Comments',
			'termination_terms' => 'Termination Terms',
			'termination_comments' => 'Termination Comments',
			'extension_options' => 'Extension Options',
			'extension_comments' => 'Extension Comments',
			'contract_type' => 'Contract Type',
			'contract_start_date' => 'Contract Start Date',
			'contract_end_date' => 'Contract End Date',
			'original_end_date' => 'Original End Date',
			'contract_value' => 'Contract Value',
			'calc_contract_value' => 'Calc Contract Value',
			'contract_budget' => 'Contract Budget',
			'original_budget' => 'Original Budget',
			'original_annual_budget' => 'Original Annual Budget',
			'spend_type' => 'Spend Type',
			'contract_status' => 'Contract Status',
			'contract_manager' => 'Contract Manager',
			'sbo_user_id' => 'Sbo User',
			'commercial_lead' => 'Commercial Lead',
			'commercial_user_id' => 'Commercial User',
			'procurement_lead' => 'Procurement Lead',
			'procurement_user_id' => 'Procurement User',
			'contract_category_id' => 'Contract Category',
			'contract_subcategory_id' => 'Contract Subcategory',
			'contract_workstream' => 'Contract Workstream',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('contract_id',$this->contract_id,true);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('department_id',$this->department_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('currency_id',$this->currency_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('currency_rate',$this->currency_rate);
		$criteria->compare('contract_reference',$this->contract_reference,true);
		$criteria->compare('po_number',$this->po_number,true);
		$criteria->compare('cost_centre',$this->cost_centre,true);
		$criteria->compare('break_clause',$this->break_clause,true);
		$criteria->compare('contract_title',$this->contract_title,true);
		$criteria->compare('contact_name',$this->contact_name,true);
		$criteria->compare('contact_info',$this->contact_info,true);
		$criteria->compare('phone_number',$this->phone_number);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('contract_description',$this->contract_description,true);
		$criteria->compare('contract_comments',$this->contract_comments,true);
		$criteria->compare('termination_terms',$this->termination_terms,true);
		$criteria->compare('termination_comments',$this->termination_comments,true);
		$criteria->compare('extension_options',$this->extension_options,true);
		$criteria->compare('extension_comments',$this->extension_comments,true);
		$criteria->compare('contract_type',$this->contract_type,true);
		$criteria->compare('contract_start_date',$this->contract_start_date,true);
		$criteria->compare('contract_end_date',$this->contract_end_date,true);
		$criteria->compare('original_end_date',$this->original_end_date,true);
		$criteria->compare('contract_value',$this->contract_value);
		$criteria->compare('calc_contract_value',$this->calc_contract_value);
		$criteria->compare('contract_budget',$this->contract_budget);
		$criteria->compare('original_budget',$this->original_budget);
		$criteria->compare('original_annual_budget',$this->original_annual_budget);
		$criteria->compare('spend_type',$this->spend_type,true);
		$criteria->compare('contract_status',$this->contract_status);
		$criteria->compare('contract_manager',$this->contract_manager,true);
		$criteria->compare('sbo_user_id',$this->sbo_user_id);
		$criteria->compare('commercial_lead',$this->commercial_lead,true);
		$criteria->compare('commercial_user_id',$this->commercial_user_id);
		$criteria->compare('procurement_lead',$this->procurement_lead,true);
		$criteria->compare('procurement_user_id',$this->procurement_user_id);
		$criteria->compare('contract_category_id',$this->contract_category_id);
		$criteria->compare('contract_subcategory_id',$this->contract_subcategory_id);
		$criteria->compare('contract_workstream',$this->contract_workstream,true);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contracts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
