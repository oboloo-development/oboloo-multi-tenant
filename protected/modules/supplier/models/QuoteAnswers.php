<?php

/**
 * This is the model class for table "quote_answers".
 *
 * The followings are the available columns in table 'quote_answers':
 * @property integer $id
 * @property integer $quote_id
 * @property integer $vendor_id
 * @property integer $question_id
 * @property string $answer
 * @property string $excellent
 * @property string $good
 * @property string $okay
 * @property string $bad
 * @property string $very_bad
 * @property string $yes
 * @property string $no
 * @property string $free_text
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class QuoteAnswers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quote_answers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quote_id, vendor_id, question_id', 'required'),
			array('quote_id, vendor_id, question_id', 'numerical', 'integerOnly'=>true),
			array('excellent, good, okay, bad, very_bad, yes, no', 'length', 'max'=>20),
			array('free_text', 'length', 'max'=>100),
			array('answer, created_datetime, updated_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, quote_id, vendor_id, question_id, answer, excellent, good, okay, bad, very_bad, yes, no, free_text, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quote_id' => 'Quote',
			'vendor_id' => 'Vendor',
			'question_id' => 'Question',
			'answer' => 'Answer',
			'excellent' => 'Excellent',
			'good' => 'Good',
			'okay' => 'Okay',
			'bad' => 'Bad',
			'very_bad' => 'Very Bad',
			'yes' => 'Yes',
			'no' => 'No',
			'free_text' => 'Free Text',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('quote_id',$this->quote_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('excellent',$this->excellent,true);
		$criteria->compare('good',$this->good,true);
		$criteria->compare('okay',$this->okay,true);
		$criteria->compare('bad',$this->bad,true);
		$criteria->compare('very_bad',$this->very_bad,true);
		$criteria->compare('yes',$this->yes,true);
		$criteria->compare('no',$this->no,true);
		$criteria->compare('free_text',$this->free_text,true);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuoteAnswers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
