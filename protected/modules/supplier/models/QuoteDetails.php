<?php

/**
 * This is the model class for table "quote_details".
 *
 * The followings are the available columns in table 'quote_details':
 * @property string $id
 * @property integer $quote_id
 * @property string $product_name
 * @property integer $quantity
 * @property string $uom
 * @property double $price
 */
class QuoteDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $unit_price;
	public $product_tax_rate;
	public $product_shipping;
	public $product_other_charges;
	public $vendor_product_notes;

	public function tableName()
	{
		return 'quote_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quote_id, quantity', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('unit_price,product_tax_rate,product_shipping,product_other_charges,vendor_product_notes','safe'),
			array('product_name', 'length', 'max'=>255),
			array('uom', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, quote_id, product_name, quantity, uom, price', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quote_id' => 'Quote',
			'product_name' => 'Product Name',
			'quantity' => 'Quantity',
			'uom' => 'Uom',
			'price' => 'Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('quote_id',$this->quote_id);
		$criteria->compare('product_name',$this->product_name,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('uom',$this->uom,true);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuoteDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
