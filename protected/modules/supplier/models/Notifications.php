<?php

/**
 * This is the model class for table "notifications".
 *
 * The followings are the available columns in table 'notifications':
 * @property string $id
 * @property integer $user_id
 * @property string $user_type
 * @property string $notification_flag
 * @property string $notification_date
 * @property string $notification_text
 * @property integer $read_flag
 */
class Notifications extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notifications';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_type, notification_flag, notification_date', 'required'),
			array('user_id, read_flag', 'numerical', 'integerOnly'=>true),
			array('user_type', 'length', 'max'=>6),
			array('notification_text', 'length', 'max'=>2048),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, user_type, notification_flag, notification_date, notification_text, read_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'user_type' => 'User Type',
			'notification_flag' => 'Notification Flag',
			'notification_date' => 'Notification Date',
			'notification_text' => 'Notification Text',
			'read_flag' => 'Read Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_type',$this->user_type,true);
		$criteria->compare('notification_flag',$this->notification_flag,true);
		$criteria->compare('notification_date',$this->notification_date,true);
		$criteria->compare('notification_text',$this->notification_text,true);
		$criteria->compare('read_flag',$this->read_flag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function getMyNotifications($mark_read = false, $from_date = "", $to_date = "")
	{
		$vendorID = UtilityManager::getVendorID();
		$date_filter_clause = "";
		if (!empty($from_date) && !empty($to_date)) $date_filter_clause = " AND notification_date BETWEEN '$from_date' AND '$to_date' ";
		else
		{
			if (!empty($from_date)) $date_filter_clause = " AND notification_date >= '$from_date' ";
			if (!empty($to_date)) $date_filter_clause = " AND notification_date <= '$to_date' ";
		}
		
		if ($mark_read) Yii::app()->db->createCommand("UPDATE notifications SET read_flag = 1 WHERE read_flag = 0 and user_type='Vendor' AND user_id = " . $vendorID)->execute();
		return Yii::app()->db->createCommand("SELECT * FROM notifications WHERE  user_type='Vendor' and user_id = " . $vendorID . " " . $date_filter_clause . " ORDER BY notification_date DESC, id DESC")->query()->readAll();
	}

	public function getMyHomeNotifications($mark_read = false, $from_date = "", $to_date = "")
	{
		$vendorID = UtilityManager::getVendorID();
		$date_filter_clause = "";
		// Start: Check login for quote, document etc
	   /* $quoteReference = Yii::app()->user->getState('quote_reference');
	    $quoteID = Yii::app()->user->getState('quote_id');
	    if(!empty($quoteReference) && !empty($quoteID)){
			$notificationType = " and notification_type='Quote'";
	    }else{
	    	$notificationType = "";
	    }*/
	    // End: Check login for quote, document etc

		if (!empty($from_date) && !empty($to_date)) $date_filter_clause = " AND notification_date BETWEEN '$from_date' AND '$to_date' ";
		else
		{
			if (!empty($from_date)) $date_filter_clause = " AND notification_date >= '$from_date' ";
			if (!empty($to_date)) $date_filter_clause = " AND notification_date <= '$to_date' ";
		}

		if ($mark_read) Yii::app()->db->createCommand("UPDATE notifications SET read_flag = 1 WHERE read_flag = 0  and user_type='Vendor' AND user_id = " . $vendorID)->execute();
		return Yii::app()->db->createCommand("SELECT * FROM notifications WHERE  user_type='Vendor' and user_id = " . $vendorID . " " . $date_filter_clause . " ORDER BY notification_date DESC, id DESC LIMIT 0,5")->query()->readAll();
	}

	static function getTimeAgo($time_ago)
	{
	    $time_ago 	= strtotime($time_ago);
	    $cur_time   = time();
	    $time_elapsed   = $cur_time - $time_ago;
	    $seconds    = $time_elapsed ;
	    $minutes    = round($time_elapsed / 60 );
	    $hours      = round($time_elapsed / 3600);
	    $days       = round($time_elapsed / 86400 );
	    $weeks      = round($time_elapsed / 604800);
	    $months     = round($time_elapsed / 2600640 );
	    $years      = round($time_elapsed / 31207680 );

	    if ($seconds <= 60) return "just now";
	    else if ($minutes <= 60)
	    {
	        if ($minutes == 1) return "one minute ago";
	        else return "$minutes minutes ago";
	    }
	    else if ($hours <= 24)
	    {
	        if ($hours == 1) return "an hour ago";
	        else return "$hours hrs ago";
	    }
	    else if ($days <= 7)
	    {
	        if ($days == 1) return "yesterday";
			else return "$days days ago";
	    }
	    else if ($weeks <= 4.3)
	    {
	        if($weeks == 1) return "a week ago";
	        else return "$weeks weeks ago";
	    }
	    else if ($months <= 12)
	    {
	        if ($months == 1) return "a month ago";
	        else return "$months months ago";
	    }
	    else
	    {
	        if ($years == 1) return "one year ago";
	        else return "$years years ago";
	    }
	}

	public function deleteNotifications($notifications_to_delete)
	{
		$vendorID = UtilityManager::getVendorID();
		if ($vendorID && $notifications_to_delete)
			foreach (explode(",", $notifications_to_delete) as $an_id_to_delete)
				Yii::app()->db->createCommand("DELETE FROM notifications WHERE user_type='Vendor' and id = $an_id_to_delete AND user_id = $vendorID")->execute();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Notifications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
