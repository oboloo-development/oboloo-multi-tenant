<?php

/**
 * This is the model class for table "quote_vendors".
 *
 * The followings are the available columns in table 'quote_vendors':
 * @property string $id
 * @property integer $quote_id
 * @property integer $vendor_id
 * @property string $reference
 * @property double $tax_rate
 * @property double $shipping
 * @property double $other_charges
 * @property double $discount
 * @property double $total_price
 * @property string $notes
 * @property integer $submit_status
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class QuoteVendors extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quote_vendors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quote_id, vendor_id, submit_status', 'numerical', 'integerOnly'=>true),
			array('tax_rate, shipping, other_charges, discount, total_price', 'numerical'),
			array('reference', 'length', 'max'=>255),
			array('notes', 'length', 'max'=>1024),
			array('created_datetime, updated_datetime,vendor_repsond_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, quote_id, vendor_id, reference, tax_rate, shipping, other_charges, discount, total_price, notes, submit_status, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quote_id' => 'Quote',
			'vendor_id' => 'Vendor',
			'reference' => 'Reference',
			'tax_rate' => 'Tax Rate',
			'shipping' => 'Shipping',
			'other_charges' => 'Other Charges',
			'discount' => 'Discount',
			'total_price' => 'Total Price',
			'notes' => 'Notes',
			'submit_status' => 'Submit Status',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('quote_id',$this->quote_id);
		$criteria->compare('vendor_id',$this->vendor_id);
		$criteria->compare('reference',$this->reference,true);
		$criteria->compare('tax_rate',$this->tax_rate);
		$criteria->compare('shipping',$this->shipping);
		$criteria->compare('other_charges',$this->other_charges);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('total_price',$this->total_price);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('submit_status',$this->submit_status);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuoteVendors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
