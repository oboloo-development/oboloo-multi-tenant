<?php

/**
 * This is the model class for table "locations".
 *
 * The followings are the available columns in table 'locations':
 * @property string $location_id
 * @property string $location_name
 * @property string $contact_person
 * @property string $email
 * @property string $phone
 * @property string $fax
 * @property string $website
 * @property string $loc_label
 * @property string $loc_address
 * @property string $loc_city
 * @property string $loc_state
 * @property string $loc_zip_code
 * @property string $loc_country
 * @property string $ship_label
 * @property string $ship_address
 * @property string $ship_city
 * @property string $ship_state
 * @property string $ship_zip_code
 * @property string $ship_country
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class Locations extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'locations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('location_name, email, loc_city, loc_state, ship_city, ship_state', 'length', 'max'=>100),
			array('contact_person, website, loc_country, ship_country', 'length', 'max'=>150),
			array('phone, fax, loc_label, loc_zip_code, ship_label, ship_zip_code', 'length', 'max'=>50),
			array('loc_address, ship_address', 'length', 'max'=>255),
			array('created_datetime, updated_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('location_id, location_name, contact_person, email, phone, fax, website, loc_label, loc_address, loc_city, loc_state, loc_zip_code, loc_country, ship_label, ship_address, ship_city, ship_state, ship_zip_code, ship_country, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'location_id' => 'Location',
			'location_name' => 'Location Name',
			'contact_person' => 'Contact Person',
			'email' => 'Email',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'website' => 'Website',
			'loc_label' => 'Loc Label',
			'loc_address' => 'Loc Address',
			'loc_city' => 'Loc City',
			'loc_state' => 'Loc State',
			'loc_zip_code' => 'Loc Zip Code',
			'loc_country' => 'Loc Country',
			'ship_label' => 'Ship Label',
			'ship_address' => 'Ship Address',
			'ship_city' => 'Ship City',
			'ship_state' => 'Ship State',
			'ship_zip_code' => 'Ship Zip Code',
			'ship_country' => 'Ship Country',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('location_id',$this->location_id,true);
		$criteria->compare('location_name',$this->location_name,true);
		$criteria->compare('contact_person',$this->contact_person,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('loc_label',$this->loc_label,true);
		$criteria->compare('loc_address',$this->loc_address,true);
		$criteria->compare('loc_city',$this->loc_city,true);
		$criteria->compare('loc_state',$this->loc_state,true);
		$criteria->compare('loc_zip_code',$this->loc_zip_code,true);
		$criteria->compare('loc_country',$this->loc_country,true);
		$criteria->compare('ship_label',$this->ship_label,true);
		$criteria->compare('ship_address',$this->ship_address,true);
		$criteria->compare('ship_city',$this->ship_city,true);
		$criteria->compare('ship_state',$this->ship_state,true);
		$criteria->compare('ship_zip_code',$this->ship_zip_code,true);
		$criteria->compare('ship_country',$this->ship_country,true);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Locations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
