<?php

/**
 * This is the model class for table "quotes".
 *
 * The followings are the available columns in table 'quotes':
 * @property string $quote_id
 * @property integer $created_by_user_id
 * @property string $created_by_name
 * @property integer $manager_user_id
 * @property string $manager_name
 * @property integer $commercial_lead_user_id
 * @property string $commercial_lead_name
 * @property integer $procurement_lead_user_id
 * @property string $procurement_lead_name
 * @property string $quote_name
 * @property string $quote_desc
 * @property string $quote_currency
 * @property integer $user_id
 * @property string $user_name
 * @property string $user_email
 * @property string $contact_phone
 * @property string $contact_email
 * @property integer $location_id
 * @property integer $department_id
 * @property integer $project_id
 * @property string $spend_type
 * @property string $quote_status
 * @property string $opening_date
 * @property string $closing_date
 * @property string $needed_date
 * @property string $cancelled_date
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class Quotes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $invite_count;
	public $submit_count;

	public function tableName()
	{
		return 'quotes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('created_by_user_id, created_by_name, manager_user_id, manager_name, commercial_lead_user_id, commercial_lead_name, procurement_lead_user_id, procurement_lead_name, user_name, user_email, cancelled_date', 'required'),
			array('invite_count,submit_count'),
			array('created_by_user_id, manager_user_id, commercial_lead_user_id, procurement_lead_user_id, user_id, location_id, department_id, project_id', 'numerical', 'integerOnly'=>true),
			array('created_by_name, manager_name, commercial_lead_name, procurement_lead_name, quote_name, contact_phone, contact_email', 'length', 'max'=>255),
			array('quote_currency', 'length', 'max'=>256),
			array('user_name, user_email', 'length', 'max'=>120),
			array('spend_type', 'length', 'max'=>8),
			array('quote_status', 'length', 'max'=>9),
			array('quote_desc, opening_date, closing_date, needed_date, created_datetime, updated_datetime,contact_email2,contact_phone2', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('quote_id, created_by_user_id, created_by_name, manager_user_id, manager_name, commercial_lead_user_id, commercial_lead_name, procurement_lead_user_id, procurement_lead_name, quote_name, quote_desc, quote_currency, user_id, user_name, user_email, contact_phone, contact_email, location_id, department_id, project_id, spend_type, quote_status, opening_date, closing_date, needed_date, cancelled_date, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'quote_id' => 'Quote',
			'created_by_user_id' => 'Created By User',
			'created_by_name' => 'Created By Name',
			'manager_user_id' => 'Manager User',
			'manager_name' => 'Manager Name',
			'commercial_lead_user_id' => 'Commercial Lead User',
			'commercial_lead_name' => 'Commercial Lead Name',
			'procurement_lead_user_id' => 'Procurement Lead User',
			'procurement_lead_name' => 'Procurement Lead Name',
			'quote_name' => 'Quote Name',
			'quote_desc' => 'Quote Desc',
			'quote_currency' => 'Quote Currency',
			'user_id' => 'User',
			'user_name' => 'User Name',
			'user_email' => 'User Email',
			'contact_phone' => 'Contact Phone',
			'contact_email' => 'Contact Email',
			'location_id' => 'Location',
			'department_id' => 'Department',
			'project_id' => 'Project',
			'spend_type' => 'Spend Type',
			'quote_status' => 'Quote Status',
			'opening_date' => 'Opening Date',
			'closing_date' => 'Closing Date',
			'needed_date' => 'Needed Date',
			'cancelled_date' => 'Cancelled Date',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('quote_id',$this->quote_id,true);
		$criteria->compare('created_by_user_id',$this->created_by_user_id);
		$criteria->compare('created_by_name',$this->created_by_name,true);
		$criteria->compare('manager_user_id',$this->manager_user_id);
		$criteria->compare('manager_name',$this->manager_name,true);
		$criteria->compare('commercial_lead_user_id',$this->commercial_lead_user_id);
		$criteria->compare('commercial_lead_name',$this->commercial_lead_name,true);
		$criteria->compare('procurement_lead_user_id',$this->procurement_lead_user_id);
		$criteria->compare('procurement_lead_name',$this->procurement_lead_name,true);
		$criteria->compare('quote_name',$this->quote_name,true);
		$criteria->compare('quote_desc',$this->quote_desc,true);
		$criteria->compare('quote_currency',$this->quote_currency,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('contact_phone',$this->contact_phone,true);
		$criteria->compare('contact_email',$this->contact_email,true);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('department_id',$this->department_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('spend_type',$this->spend_type,true);
		$criteria->compare('quote_status',$this->quote_status,true);
		$criteria->compare('opening_date',$this->opening_date,true);
		$criteria->compare('closing_date',$this->closing_date,true);
		$criteria->compare('needed_date',$this->needed_date,true);
		$criteria->compare('cancelled_date',$this->cancelled_date,true);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Quotes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
