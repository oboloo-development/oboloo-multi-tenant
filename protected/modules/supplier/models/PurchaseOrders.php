<?php

/**
 * This is the model class for table "purchase_orders".
 *
 * The followings are the available columns in table 'purchase_orders':
 * @property string $po_id
 * @property integer $order_id
 * @property string $po_number
 * @property integer $payment_term_id
 * @property integer $shipping_term_id
 * @property integer $shipping_method_id
 * @property double $po_other_charges
 * @property double $po_discount
 * @property double $po_total
 * @property string $po_status
 * @property string $po_notes
 * @property string $reference
 * @property string $po_date
 * @property string $created_datetime
 * @property string $updated_datetime
 */
class PurchaseOrders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'purchase_orders';
	}
	public $location_name;
	public $department_name;
	public $project_id;
	public $spend_type;
	public $currency_id;
	public $currency_rate;
	public $order_date;
	public $user_name;
	public $user_id;
    public $order_status;
    public $location_id;
    public $department_id;
    public $description;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, payment_term_id, shipping_term_id, shipping_method_id', 'numerical', 'integerOnly'=>true),
			array('po_other_charges, po_discount', 'numerical'),
			array('po_number', 'length', 'max'=>50),
			array('po_status', 'length', 'max'=>27),
			array('po_notes', 'length', 'max'=>1024),
			array('reference', 'length', 'max'=>255),
			array('po_date, created_datetime, updated_datetime,vendor_id,po_sent_date,location_name,po_total', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('po_id, order_id, po_number, payment_term_id, shipping_term_id, shipping_method_id, po_other_charges, po_discount, po_total, po_status, po_notes, reference, po_date, created_datetime, updated_datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'po_id' => 'Po',
			'order_id' => 'Order',
			'po_number' => 'Po Number',
			'payment_term_id' => 'Payment Term',
			'shipping_term_id' => 'Shipping Term',
			'shipping_method_id' => 'Shipping Method',
			'po_other_charges' => 'Po Other Charges',
			'po_discount' => 'Po Discount',
			'po_total' => 'Po Total',
			'po_status' => 'Po Status',
			'po_notes' => 'Po Notes',
			'reference' => 'Reference',
			'po_date' => 'Po Date',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('po_id',$this->po_id,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('po_number',$this->po_number,true);
		$criteria->compare('payment_term_id',$this->payment_term_id);
		$criteria->compare('shipping_term_id',$this->shipping_term_id);
		$criteria->compare('shipping_method_id',$this->shipping_method_id);
		$criteria->compare('po_other_charges',$this->po_other_charges);
		$criteria->compare('po_discount',$this->po_discount);
		$criteria->compare('po_total',$this->po_total);
		$criteria->compare('po_status',$this->po_status,true);
		$criteria->compare('po_notes',$this->po_notes,true);
		$criteria->compare('reference',$this->reference,true);
		$criteria->compare('po_date',$this->po_date,true);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('updated_datetime',$this->updated_datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PurchaseOrders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
