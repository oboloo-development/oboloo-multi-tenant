<?php

/**
 * This is the model class for table "contract_documents".
 *
 * The followings are the available columns in table 'contract_documents':
 * @property integer $id
 * @property integer $contract_id
 * @property integer $uploaded_by
 * @property string $document_title
 * @property string $document_file
 * @property integer $document_type
 * @property string $status
 * @property string $date_created
 */
class ContractDocuments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contract_documents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contract_id, uploaded_by, document_title, document_file, document_type, status, date_created', 'safe'),
			array('contract_id, uploaded_by, document_type', 'numerical', 'integerOnly'=>true),
			array('document_title', 'length', 'max'=>50),
			array('document_file', 'length', 'max'=>150),
			array('status', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, contract_id, uploaded_by, document_title, document_file, document_type, status, date_created', 'safe', 'on'=>'search'),
		);
	}
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contract_id' => 'Contract',
			'uploaded_by' => 'Uploaded By',
			'document_title' => 'Document Title',
			'document_file' => 'Document File',
			'document_type' => 'Document Type',
			'status' => 'Status',
			'date_created' => 'Date Created',
		);
	}
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contract_id',$this->contract_id);
		$criteria->compare('uploaded_by',$this->uploaded_by);
		$criteria->compare('document_title',$this->document_title,true);
		$criteria->compare('document_file',$this->document_file,true);
		$criteria->compare('document_type',$this->document_type);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContractDocuments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
