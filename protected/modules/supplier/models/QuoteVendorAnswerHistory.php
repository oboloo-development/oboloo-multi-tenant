<?php

/**
 * This is the model class for table "quote_vendor_details".
 *
 * The followings are the available columns in table 'quote_vendor_details':
 * @property string $id
 * @property integer $quote_id
 * @property integer $vendor_id
 * @property string $product_name
 * @property double $tax_rate
 * @property double $shipping
 * @property double $other_charges
 * @property integer $quantity
 * @property double $unit_price
 * @property string $vendor_product_notes
 */
class QuoteVendorAnswerHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quote_vendor_answers__history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(


			array('quote_id, vendor_id,question, question_id,answer,created_at','safe'),

			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quote_id' => 'Quote',
			'vendor_id' => 'Vendor',
			
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
