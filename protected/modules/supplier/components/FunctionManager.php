<?php
class FunctionManager {

    public static function showCurrencySymbol($currency='',$currency_id='') {
    	if(!empty($currency_id)){
    		$sql = "select currency from currency_rates where id=".$currency_id;
        	$currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
        	if(!empty($currencyReader['currency'])){
        		$currency = strtolower($currencyReader['currency']);
        	}
        }

        $currencies = array('aed'=>'AED','afn'=>'&#1547;','all'=>'&#76;&#101;&#107;','amd'=>'AMD','ang'=>'&#402;','aoa'=>'AOA','ars'=>'&#36;','aud'=>'&#36;','awg'=>'&#402;','azn'=>'&#1084;&#1072;&#1085;','bam'=>'&#75;&#77;','bbd'=>'&#36;','bdt'=>'BDT','bgn'=>'&#1083;&#1074;','bhd'=>'BHD','bif'=>'BIF','bmd'=>'&#36;','bnd'=>'&#36;','bob'=>'&#36;&#98;','brl'=>'&#82;&#36;','bsd'=>'&#36;','btn'=>'BTN','bwp'=>'&#80;','byr'=>'&#112;&#46;','bzd'=>'&#66;&#90;&#36;','cad'=>'&#36;','cdf'=>'CDF','chf'=>'&#67;&#72;&#70;','clp'=>'&#36;','cny'=>'&#165;','cop'=>'&#36;','crc'=>'&#8353;','cuc'=>'CUC','cup'=>'&#8369;','cve'=>'CVE','czk'=>'&#75;&#269;','djf'=>'DJF','dkk'=>'&#107;&#114;','dop'=>'&#82;&#68;&#36;','dzd'=>'DZD','egp'=>'&#163;','ern'=>'ERN','etb'=>'ETB','eur'=>'&#8364;','fjd'=>'&#36;','fkp'=>'&#163;','gbp'=>'&#163;','gel'=>'GEL','ggp'=>'&#163;','ghs'=>'&#162;','gip'=>'&#163;','gmd'=>'GMD','gnf'=>'GNF','gtq'=>'&#81;','gyd'=>'&#36;','hkd'=>'&#36;','hnl'=>'&#76;','hrk'=>'&#107;&#110;','htg'=>'HTG','huf'=>'&#70;&#116;','idr'=>'&#82;&#112;','ils'=>'&#8362;','imp'=>'&#163;','inr'=>'&#8377;','iqd'=>'IQD','irr'=>'&#65020;','isk'=>'&#107;&#114;','jep'=>'&#163;','jmd'=>'&#74;&#36;','jod'=>'JOD','jpy'=>'&#165;','kes'=>'KES','kgs'=>'&#1083;&#1074;','khr'=>'&#6107;','kmf'=>'KMF','kpw'=>'&#8361;','krw'=>'&#8361;','kwd'=>'KWD','kyd'=>'&#36;','kzt'=>'&#1083;&#1074;','lak'=>'&#8365;','lbp'=>'&#163;','lkr'=>'&#8360;','lrd'=>'&#36;','lsl'=>'LSL','lyd'=>'LYD','mad'=>'MAD','mdl'=>'MDL','mga'=>'MGA','mkd'=>'&#1076;&#1077;&#1085;','mmk'=>'MMK','mnt'=>'&#8366;','mop'=>'MOP','mro'=>'MRO','mur'=>'&#8360;','mvr'=>'MVR','mwk'=>'MWK','mxn'=>'&#36;','myr'=>'&#82;&#77;','mzn'=>'&#77;&#84;','nad'=>'&#36;','ngn'=>'&#8358;','nio'=>'&#67;&#36;','nok'=>'&#107;&#114;','npr'=>'&#8360;','nzd'=>'&#36;','omr'=>'&#65020;','pab'=>'&#66;&#47;&#46;','pen'=>'&#83;&#47;&#46;','pgk'=>'PGK','php'=>'&#8369;','pkr'=>'&#8360;','pln'=>'&#122;&#322;','prb'=>'PRB','pyg'=>'&#71;&#115;','qar'=>'&#65020;','ron'=>'&#108;&#101;&#105;','rsd'=>'&#1044;&#1080;&#1085;&#46;','rub'=>'&#1088;&#1091;&#1073;','rwf'=>'RWF','sar'=>'&#65020;','sbd'=>'&#36;','scr'=>'&#8360;','sdg'=>'SDG','sek'=>'&#107;&#114;','sgd'=>'&#36;','shp'=>'&#163;','sll'=>'SLL','sos'=>'&#83;','srd'=>'&#36;','ssp'=>'SSP','std'=>'STD','syp'=>'&#163;','szl'=>'SZL','thb'=>'&#3647;','tjs'=>'TJS','tmt'=>'TMT','tnd'=>'TND','top'=>'TOP','try'=>'&#8378;','ttd'=>'&#84;&#84;&#36;','twd'=>'&#78;&#84;&#36;','tzs'=>'TZS','uah'=>'&#8372;','ugx'=>'UGX','usd'=>'&#36;','uyu'=>'&#36;&#85;','uzs'=>'&#1083;&#1074;','vef'=>'&#66;&#115;','vnd'=>'&#8363;','vuv'=>'VUV','wst'=>'WST','xaf'=>'XAF','xcd'=>'&#36;','xof'=>'XOF','xpf'=>'XPF','yer'=>'&#65020;','zar'=>'&#82;','zmw'=>'ZMW');
        if( array_key_exists( $currency, $currencies) ){
            $symbol = $currencies[$currency];
        } else {
            $symbol = $currency;
        }
        return $symbol;
    }

    public static function orderCurrSymbol($orderID){
        $sql = "select currency_id from orders where order_id=".$orderID;
        $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
        return self::showCurrencySymbol('',$currencyReader['currency_id']);
    }

    public static function productPriceInGBP($productID) {

    	if(!empty($productID)){
    		$sql = "select currency_rates.rate,products.price from currency_rates,products where currency_rates.id=products.currency_id and products.product_id=".$productID;
        	$currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
        	if(!empty($currencyReader['rate'])){
        		$currencyRate = $currencyReader['rate'];
        	}else{
        		$currencyRate=1;
        	}

        	$productPriceInGBP = round($currencyReader['price']/$currencyRate,2);
        	return $productPriceInGBP;

        }
        return "0";
    }

    public static function orderTotalPrice($orderID){
        $sql = "select sum(calc_unit_price*quantity)+sum(calc_tax)+sum(shipping_cost) as total_price from order_details where order_id=".$orderID;
        $detail_order = Yii::app()->db->createCommand($sql)->queryRow();
        return $detail_order['total_price'];
    }

    public static function priceInGBP($price,$currency) {
      $sql = "select rate from currency_rates where LOWER(currency)='".strtolower($currency)."'";
      $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
      if(!empty($currencyReader['rate'])){
          $currencyRate = $currencyReader['rate'];
      }else{
          $currencyRate=1;
      }

      $priceInGBP = $price/$currencyRate;
      return number_format($priceInGBP, 2, '.', '');
    }

    public static function priceInGBPByID($price,$currencyID) {

      $sql = "select rate from currency_rates where id=".$currencyID;
      $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
      if(!empty($currencyReader['rate'])){
        $currencyRate = $currencyReader['rate'];
      }else{
        $currencyRate=1;
      }

      $priceInGBP = $price/$currencyRate;
      return number_format($priceInGBP, 2, '.', '');
    }

    public static function currencyRate($currency) {
      $sql = "select rate from currency_rates where LOWER(currency)='".strtolower($currency)."'";
      $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
      return !empty($currencyReader['rate'])?$currencyReader['rate']:0;  
    }

    public static function routingStatus($key=''){
      $status = array('Pending'=>'Pending', 'Approved'=>'Approved', 'Declined'=>'Declined', 'Information'=>'More Info Needed');
      
      if(!empty($key)){
        return $status[$key];
      }else
        return $status;
    }

    public static function currencySymbol($currencyID='',$currency=''){
         
      if(!empty($currencyID)){
        $sql = "select currency from currency_rates where id='".$currencyID."'";
        $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
        $currency = $currencyReader['currency'];
      }

      switch ($currency) {
          case 'GBP' : $symbol = '&#163;';
              break;
          case 'USD' : $symbol = '$';
              break;
          case 'CAD' : $symbol = '$';
              break;
          case 'CHF' : $symbol = '&#8355;';
              break;
          case 'EUR' : $symbol = '&#8364;';
              break;
          case 'JPY' : $symbol = '&#165;';
              break;
          case 'INR' : $symbol = '&#x20B9;';
              break;
          default :    $symbol = '&#163;';
              break;
      }
      return $symbol;
    }

    public static function vendorDocument($key='') {
      $sql = "select name,id from vendor_document_type where created_type='General' and soft_deleted=0 order by name asc";
        $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
        $document = array();
        foreach($reader as $value){
          $document[$value['id']] = $value['name'];
        }

        if(!empty($key)){
          $sql = "select name,id from vendor_document_type where id=".$key." and soft_deleted=0 "; 
           $reader = Yii::app()->db->createCommand($sql)->queryRow();
           return $reader['name'];
        }
        return $document;
    }

    public static function vendorDocumentOnboard($key='') {
      $sql = "select name,id from vendor_document_type_onboard where created_type ='General' and soft_deleted=0 order by name asc";
        $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
        $document = array();
        foreach($reader as $value){
          $document[$value['id']] = $value['name'];
        }

        if(!empty($key)){
          $sql = "select name,id from vendor_document_type_onboard where id=".$key." and soft_deleted=0 "; 
           $reader = Yii::app()->db->createCommand($sql)->queryRow();
           return $reader['name'];
        }
        return $document;
    }

    public static function vendorDocumentBgColor($key='') {
      $sql = "select btn_style,id from vendor_document_type";
      $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
      $document = array();
      foreach($reader as $value){
        $document[$value['id']] = $value['btn_style'];
      }    
      if(!empty($key)){
          return $document[$key];
      }
      return $document;
    }

    public static function vendorDocumentBgColorOnboard($key='') {
      $sql = "select btn_style,id from vendor_document_type_onboard";
      $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
      $document = array();
      foreach($reader as $value){
        $document[$value['id']] = $value['btn_style'];
      }
       
      if(!empty($key)){
          return $document[$key];
      }
      return $document;
    }

    public static function contractDocument($key='') {
        $sql = "select name,id from contract_document_type where created_type ='General' order by name asc";
        $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
        $document = array();
        foreach($reader as $value){
          $document[$value['id']] = $value['name'];
        }

        if(!empty($key)){
           $sql = "select name,id from contract_document_type where id=".$key;
           $reader = Yii::app()->db->createCommand($sql)->queryRow();
           return $reader['name'];
        }
        return $document;
    }

     public static function contractDocumentBgColor($key='') {
        $sql = "select btn_style,id from contract_document_type";
        $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
        $document = array();
        foreach($reader as $value){
          $document[$value['id']] = $value['btn_style'];
        }
         
        if(!empty($key)){
            return $document[$key];
        }
        return $document;
    }

    public static function contractStatuses($key='')
    {
        if($key ==''){
            $sql = "SELECT * FROM contract_status GROUP BY value";
        }else if($key !=''){
            $sql = "SELECT value FROM contract_status where code=".$key;
        }
        $statues = Yii::app()->db->createCommand($sql)->query()->readAll();
        return $statues;
    }	

     public static function contractTitle($key='') {
        $title = array(
                          "Craft"=>"Craft",
                          "Harrision"=>"Harrision",
                          "Healthcare"=>"Healthcare",
                          "L’Oreal"=>"L’Oreal",
                          "ME International"=>"ME International",
                          "ME Local"=>"ME Local",
                          "MRM"=>"MRM",
                    );
        if(!empty($key)){
            return $title[$key];
        }
        return $title;
    } 

    public static function quoteCheckStatus($quoteID){
      $vendor_id = UtilityManager::getVendorID();
      $sql = "select * from quote_vendors where quote_id=".$quoteID." and vendor_id=".$vendor_id;
      $vendorQuote = Yii::app()->db->createCommand($sql)->queryRow();

      $sql = "select * from quotes where quote_id=".$quoteID;
      $quote = Yii::app()->db->createCommand($sql)->queryRow();

      if($vendorQuote['submit_status']== 1) {
        $status = 1;
      }else if (strtotime(date("Y-m-d H:i:s"))>strtotime($quote['closing_date'])){
         $status = 2;
        }else if (strtotime(date("Y-m-d H:i:s"))<=strtotime($quote['opening_date'])) {
          $status = 3;
        }else{
           $status = 4;
        }
      return $status;
    }

    public static function getFormAnswer($question_form_id ='', $quote_id=''){
      // $sql = "select * from form_answers where quote_id=".$quote_id." and form_id=".$question_form_id;
      // $formAnswers =Yii::app()->db->createCommand($sql)->query()->readAll(); 
      // $answer = !empty($formAnswers) ? $formAnswers : null;
      // reutrn $answer;
    } 

    public static function getFormQuestions($question_form_id =''){
        $sql="SELECT  * FROM `form_questions` where form_id=".$question_form_id;
        $formQuestions = Yii::app()->db->createCommand($sql)->query()->readAll();
       // reutrn $formQuestions;
    } 

    public static function dateFormat(){
      $dateFormate = Yii::app()->session['default_date_format'];

      if($dateFormate=="MM/DD/YYYY" or $dateFormate=="mm/dd/yyyy"){
        $dateFormate="m/d/Y";
      }else if($dateFormate=="DD/MM/YYYY" or $dateFormate=="dd/mm/yyyy"){
        $dateFormate="d/m/Y";
      }else {
        $dateFormate="d/m/Y";
      }

      return $dateFormate;
    } 

    public static function dateFormatJS(){
     $dateFormate = Yii::app()->session['default_date_format'];
     if(empty($dateFormate)){
        $dateFormate = "DD/MM/YYYY";
     }
      return $dateFormate;
    }

    	
}