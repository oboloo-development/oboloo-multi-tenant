<?php
class VendorEmail
{
	public static function ForgotPassword($email){ 

       require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');

		$user_id = 0;
		$vendor=Vendor::model()->findByAttributes(array('emails'=>$email));
		if ($vendor !=null)
			$vendorID = $vendor->vendor_id;
		
		/*if (!$vendorID)
		{
			$vendor=Vendor::model()->findByAttributes(array('emails'=>$username));
			if ($vendor !=null)
			 $vendorID = $vendor->vendor_id;
		}*/
		
		if (!$vendorID) return 1;
		else
		{
			$newPassword = self::generateRandomString(10);
			$sql = "UPDATE vendors SET password = MD5('".$newPassword."') WHERE vendor_id = $vendorID";
			Yii::app()->db->createCommand($sql)->execute();				

			$mail = new \PHPMailer(true);					
			$mail->IsSMTP();
			$mail->Host = Yii::app()->params['host'];
			$mail->Port = Yii::app()->params['port'];
			$mail->SMTPAuth = true;
			$mail->Username = Yii::app()->params['username'];
			$mail->Password = Yii::app()->params['password'];
			$mail->SMTPSecure = Yii::app()->params['stmpSecure'];
            $mail->CharSet="UTF-8";

			$email_body = '';

			$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
			$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 60%;">
                     <span style="color:#10a798;"><strong>Forgot Password&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
            $email_body .= '<p>This is a notification from Spend 365. As per your request, your password has been changed as below:</p>';
			$email_body .= '<strong>Username: </strong>' . $vendor->emails . '<br/>';
			$email_body .= '<strong>Password: </strong>' . $newPassword . '<br/>';
			$email_body .= '<p><strong>NOTE: </strong>If you did not request this change, please contact your administrator immediately.<p/>';
			$email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';
			$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
			$mail->addAddress($vendor->emails, $vendor->vendor_name);
			$mail->Subject = 'Spend 365: Password Change';
			$mail->isHTML(true);
			$mail->Body = $email_body;			
			$mail->send();
			
			return 0;
		}
	
  	}

    public static function changePassword($email)
    {
         require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');

        $user_id = 0;
        $vendor=Vendor::model()->findByAttributes(array('emails'=>$email));
        if ($vendor !=null)
            $vendorID = $vendor->vendor_id;
        
        if (!$vendorID) return 1;
        else
        {
            $new_password =  self::generateRandomString(10);
           // $this->executeQuery("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id");                   

            $mail = new \PHPMailer(true);                   
            $mail->IsSMTP();
            $mail->Host = Yii::app()->params['host'];
            $mail->Port = Yii::app()->params['port'];
            $mail->SMTPAuth = true;
            $mail->Username = Yii::app()->params['username'];
            $mail->Password = Yii::app()->params['password'];
            $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
            $mail->CharSet="UTF-8";

            $email_body = '';

            $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
            $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 60%;">
                     <span style="color:#10a798;"><strong>Change Password&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
            $email_body .= '<p>This is a notification from Spend 365. Please follow the link below to change your password:</p>';
            $email_body .= '<strong>Username: </strong>' . $vendor->emails . '<br/>';
            $email_body .= '<strong>Change Password Link: </strong><a href="' .Yii::app()->createAbsoluteUrl('site/changePasswordSave',array("user-id"=>base64_encode($vendorID),'time'=>base64_encode(date("m-d"))))  . '" title="Change Password">'.Yii::app()->createAbsoluteUrl('app/changePasswordSave',array("user-id"=>base64_encode($vendorID),'time'=>base64_encode(date("m-d")))).'</a><br/>';
            $email_body .= '<p><strong>NOTE: </strong>If you did not request this change, please contact your administrator immediately.<p/>';
            $email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';
            $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
            $mail->addAddress($vendor->emails, $vendor->vendor_name);
            $mail->Subject = 'Spend 365: Password Change';
            $mail->isHTML(true);
            $mail->Body = $email_body;          
            $mail->send();
            
            return 0;
        }
    }

    public static function changePasswordSave($vendor)
    {   
        require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');                   
        $mail = new \PHPMailer(true);                   
        $mail->IsSMTP();
        $mail->Host = Yii::app()->params['host'];
        $mail->Port = Yii::app()->params['port'];
        $mail->SMTPAuth = true;
        $mail->Username = Yii::app()->params['username'];
        $mail->Password = Yii::app()->params['password'];
        $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
        $mail->CharSet="UTF-8";

        $email_body = '';

            $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
            $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 60%;">
                     <span style="color:#10a798;"><strong>Change Password&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
            $email_body .= '<p>This is a notification from Spend 365. Your password changed successfully:</p>';
            
            $email_body .= '<p><strong>NOTE: </strong>If you did not request this change, please contact your administrator immediately.<p/>';
            $email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table></div>';
    $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
    $mail->addAddress($vendor->emails, $vendor->vendor_name);
    $mail->Subject = 'Spend 365: Password Change';
    $mail->isHTML(true);
    $mail->Body = $email_body;          
    $mail->send();
    return 0;
    }

    public static function activateUser($email,$password)
    {
        require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');
        $user_id = 0;
        $vendor=Vendor::model()->findByAttributes(array('emails'=>$email));
        $mail = new \PHPMailer(true);
        $mail->IsSMTP();
        $mail->Host = Yii::app()->params['host'];
        $mail->Port = Yii::app()->params['port'];
        $mail->SMTPAuth = true;
        $mail->Username = Yii::app()->params['username'];
        $mail->Password = Yii::app()->params['password'];
        $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
        $mail->CharSet="UTF-8";

        $email_body = '';
        $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
            $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#10a798;"><strong>User Activation&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
            $email_body .= '<p>Dear ' . $vendor->vendor_name.', </p>';
            $email_body .= '<p>This is a notification from Spend 365. your account has been activated as below:</p>';
            $email_body .= '<strong>Email: </strong>' . $vendor->emails . '<br/>';
            $email_body .= '<strong>Password: </strong>' . $password . '<br/>';
            $email_body .= '<strong>URL: </strong><a target="_blank" href="http://live1.spend-365.online/supplier">live1.spend-365.online/supplier</a><br/>';
            $email_body .= '<p><strong>NOTE: </strong>Incase any problem, Please contact administrator immediately.<p/>';
            $email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table></div>';
    $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
    $mail->addAddress($vendor->emails, $vendor->vendor_name);
    $mail->Subject = 'Spend 365: User Activation';
    $mail->isHTML(true);
    $mail->Body = $email_body;
    $mail->send();
    return 0;
    }


  	public static function generateRandomString($length = 10) 
	{
	    $characters = '0123456789%@&*abcdefghijklmnop!~qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++)
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    return $randomString;
	}
 }
?>