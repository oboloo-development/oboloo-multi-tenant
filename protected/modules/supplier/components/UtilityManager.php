<?php
class UtilityManager {

	public static function getVendorsDocuments($vendorID,$documentType)
	{
	        $sql= "SELECT *, SUM(CASE WHEN status='Pending' THEN 1 ELSE 0 END) AS pendiing_documents,
	              SUM(CASE WHEN status='Approved' THEN 1 ELSE 0 END) AS approved_documents from vendor_documents where vendor_id=$vendorID and document_type='".$documentType."' group by document_type order by document_type asc";

	        return Yii::app()->db->createCommand($sql)->query()->readAll();
	        
	}

	public static function getVendorsDocumentsOnboard($vendorID,$documentType)
	{
	        $sql= "SELECT *, SUM(CASE WHEN status='Pending' THEN 1 ELSE 0 END) AS pendiing_documents,
	              SUM(CASE WHEN status='Approved' THEN 1 ELSE 0 END) AS approved_documents from vendor_documents_onboard where vendor_id=$vendorID and document_type='".$documentType."' group by document_type order by document_type asc";
	        return Yii::app()->db->createCommand($sql)->query()->readAll();        
	}


	public static function getContractsDocuments($contractID,$documentType)
	{
	        $sql= "SELECT *, SUM(CASE WHEN status='Pending' THEN 1 ELSE 0 END) AS pendiing_documents,
	              SUM(CASE WHEN status='Approved' THEN 1 ELSE 0 END) AS approved_documents from contract_documents where contract_id=$contractID and document_type='".$documentType."' group by document_type order by document_type asc";

	        return Yii::app()->db->createCommand($sql)->query()->readAll();
	        
	}

	public static function getVendorID(){
		$type = Yii::app()->user->getState('type');
		if($type == "1" || $type == "2" || $type == "3") {
			$parentVendor =  VendorTeammember::model()->findByAttributes(array('teammember_id'=>Yii::app()->user->id));
			$vendorID = $parentVendor->vendor_id;
		 }else{
		 	$vendorID = Yii::app()->user->id;
		 }
		 return $vendorID;
    }

    public function saveDocument($vendor_id, $file_description, $file_name,$document_type,$document_status,$expiry_date)
    {  
	    $user_id = Yii::app()->user->id;
	    $dateCreated = date("Y-m-d H:i:s");
	    $expiryDate  =  date("Y-m-d", strtotime(strtr($expiry_date, '/', '-')));
	    if(!empty($vendor_id) && !empty($file_name)){
	      return Yii::app()->db->createCommand("INSERT INTO vendor_documents (vendor_id, uploaded_by,uploaded_by_type, document_title,document_file,document_type,status,expiry_date,date_created) SELECT $vendor_id,$user_id,'Vendor', '$file_description', '$file_name',$document_type,'$document_status','$expiryDate','$dateCreated'")->execute();

	    }
  	}

  	public function saveDocumentOnboard($vendor_id, $file_description, $file_name,$document_type,$document_status,$expiry_date)
    {  
	    $user_id = Yii::app()->user->id;
	    $dateCreated = date("Y-m-d H:i:s");
	    $expiryDate  =  date("Y-m-d", strtotime(strtr($expiry_date, '/', '-')));
	    if(!empty($vendor_id) && !empty($file_name)){
	      return Yii::app()->db->createCommand("INSERT INTO vendor_documents_onboard (vendor_id, uploaded_by,uploaded_by_type, document_title,document_file,document_type,status,expiry_date,date_created) SELECT $vendor_id,$user_id,'Vendor', '$file_description', '$file_name',$document_type,'$document_status','$expiryDate','$dateCreated'")->execute();

	    }
  	}

  	public function updateDocumentTypeStatus($vendor_id, $recordID)
    {  
	    if(!empty($vendor_id) && !empty($recordID)){
	      return Yii::app()->db->createCommand("update  vendor_request_documents set status='Uploaded' where id=".$recordID)->execute();
	    }
  	}

  	public function updateDocumentTypeStatusOnboard($vendor_id, $recordID)
    {  
	    if(!empty($vendor_id) && !empty($recordID)){
	      return Yii::app()->db->createCommand("update  vendor_request_documents_onboard set status='Uploaded' where id=".$recordID)->execute();
	    }
  	}

  	public function updateContractDocumentTypeStatus($contractID, $recordID)
    {  
	    if(!empty($contractID) && !empty($recordID)){
	      return Yii::app()->db->createCommand("update contract_request_documents set status='Uploaded' where id=".$recordID)->execute();
	    }
  	}


  	public function saveContractDocument($contract_id, $file_description, $file_name,$document_type,$document_status,$expiry_date)
    {  
	    $user_id = Yii::app()->user->id;
	    $dateCreated = date("Y-m-d H:i:s"); 
	    $expiryDate  =  date("Y-m-d", strtotime(strtr($expiry_date, '/', '-')));
	    if(!empty($contract_id) && !empty($file_name)){
	      return Yii::app()->db->createCommand("INSERT INTO contract_documents (contract_id, uploaded_by,uploaded_by_type, document_title,document_file,document_type,status,expiry_date,date_created) SELECT $contract_id,$user_id,'Vendor','$file_description', '$file_name',$document_type,'$document_status','$expiryDate','$dateCreated'")->execute();
	    }
  	}


  	

  	public static function getOrderDetails($order_id = 0)
	{
		$sql = "SELECT d.*, b.bundle_name AS product_name, v.vendor_name as vendor
				  FROM order_details d
				  LEFT JOIN vendors v ON d.vendor_id = v.vendor_id
				  LEFT JOIN bundles b ON d.product_id = b.bundle_id
				  WHERE d.product_type = 'Bundle'
				  AND d.order_id = $order_id

				 UNION

				 SELECT d.*, p.product_name, v.vendor_name as vendor
				  FROM order_details d
				   LEFT JOIN vendors v ON d.vendor_id = v.vendor_id
				  LEFT JOIN products p ON d.product_id = p.product_id
				  WHERE d.product_type = 'Product'
				  AND d.order_id = $order_id";
		return Yii::app()->db->createCommand($sql)->query()->readAll();
	}

	public static function orderInvoice($order_id,$vendor_id=0)
	{
		$whereCond = '';
		if(!empty($order_id)){
			$whereCond = ' and order_id='.$order_id;
		}if(!empty($vendor_id)){
			$whereCond .= ' and vendor_id='.$vendor_id;
		}
		$sql = "
				SELECT * FROM (
				SELECT order_invoices.*,'vendor' as type from order_invoices where created_by_type=1
					UNION
				select order_invoices.*,'internal' as type from order_invoices where created_by_type=2
				) AS order_invoice where 1 ".$whereCond."  ORDER BY id DESC ";
			return Yii::app()->db->createCommand($sql)->query()->readAll();

	}


  	
}