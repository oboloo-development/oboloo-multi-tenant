<?php
class QuoteEmail
{
	public static function submitQuote($quote_id,$vendor_id)
    {
        require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');

        $quote_vendor = Yii::app()->db->createCommand("SELECT *
                  FROM quote_vendors qv
                  INNER JOIN quotes q ON q.quote_id=qv.quote_id
                  WHERE qv.quote_id = $quote_id AND qv.vendor_id = $vendor_id")->queryRow();

        $vendor = Yii::app()->db->createCommand("SELECT * FROM vendors WHERE vendor_id = $vendor_id")->queryRow();

        $user_id = $quote_vendor['user_id'];
        $vendor_name = $vendor['vendor_name'];
        //$user = $this->executeQuery("SELECT * FROM users WHERE user_id = $user_id",1);
        $user_ids  = array();
        if(!empty($quote_vendor['created_by_user_id'])) $user_ids[$quote_vendor['created_by_user_id']] = $quote_vendor['created_by_user_id'];
        /*if(!empty($quote_vendor['manager_user_id'])) $user_ids[$quote_vendor['manager_user_id']] = $quote_vendor['manager_user_id'];*/
        if(!empty($quote_vendor['commercial_lead_user_id'])) $user_ids[$quote_vendor['commercial_lead_user_id']] = $quote_vendor['commercial_lead_user_id'];
       /* if(!empty($quote_vendor['procurement_lead_user_id'])) $user_ids[$quote_vendor['procurement_lead_user_id']] = $quote_vendor['procurement_lead_user_id'];*/
        $user = new User;
        $users = Yii::app()->db->createCommand("select * from users where user_id in (".implode(",", $user_ids).")")->query()->readAll();
        foreach($users as $user){
            if (!empty($user['email']))
            {
                $mail = new \PHPMailer(true);
                $mail->IsSMTP();
                $mail->Host = Yii::app()->params['host'];
                $mail->Port = Yii::app()->params['port'];
                $mail->SMTPAuth = true;
                $mail->Username = Yii::app()->params['username'];
                $mail->Password = Yii::app()->params['password'];
                $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
                $mail->CharSet="UTF-8";
                $email_body = '';
                $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <div style="margin:0px auto;max-width:600px;">
                <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="vertical-align:top;width:600px;">
                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                 <tr>
                  <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                  <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                      <tr>
                          <td style="width:600px;">';
                $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
                $email_body .= '</td>
                      </tr>
                      </tbody>
                  </table>
                  </td>
                 </tr>
                 <tr>
                 <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;width:600px;">';
                $email_body .= '<p style="margin-left: 16px;">Dear ' . $user['full_name'] . ', </p>';
                $email_body .= '</td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center;">';
                $email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                         <h1 style="font-family: Cabin; line-height: 100%;">
                             <span style="color:#000000;"><strong> <span style="color:#10a798;">' . $vendor_name . '</span> has responded to your RFQ </strong></span>
                         </h1>
                     </div>
                 </td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;">
                 <div style="font-size:11px;line-height:22px;color:#000000;font-family:Roboto, Tahoma, sans-serif;marign-left:16px;">';
                $email_body .= '<p> You have had a response for  quote ' . $quote_id . '. Please see further details via the <span style="color:red;">Quote</span> button below.:</p>';
                $email_body .= '</div>
                 </td>
                </tr>
                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                     </td>
                 </tr>';
                $email_body .= '<tr>
                       <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                        <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                        <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                        <a href="' . AppUrl::bicesUrl('quotes/edit/'.$quote_id) . '" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Quote Response</a>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                         <tr>
                             <td style="word-wrap:break-word;font-size:0px;">
                             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                             </td>
                         </tr>
                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;" align="left">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;margin-left:16px;">
                             <p>Terms of Service &amp; User Agreement</p>
                             <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                             <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                         </div>
                     </td>
                 </tr>
                 </tbody>
                </table>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </div></td>
                </tr>
                </table>
                </div>';
                $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
                $mail->addAddress($user['email'], $vendor_name);
                $mail->Subject = 'Spend 365: Supplier Quote Response';
                $mail->isHTML(true);
                $mail->Body = $email_body;
                //$mail->SMTPDebug = 2;
                $mail->send();
        }
        }
    }
 }
?>