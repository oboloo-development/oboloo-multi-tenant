<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
    public function authenticate()
    {

       if(Yii::app()->controller->action->id=='login'){
    		//$record=Vendor::model()->findByAttributes(array('emails'=>$this->username,'active'=>1));
            $record=QuoteVendors::model()->findByAttributes(array('reference'=>$this->username));
        
    		//$password = md5($this->password);
            if($record===null)
                $this->errorCode=self::ERROR_USERNAME_INVALID;
            /*else if($record->password!==$password)
                $this->errorCode=self::ERROR_PASSWORD_INVALID;*/
            else
            {
               // $vendor=Vendor::model()->findByAttributes(array('id'=>$record->vendor_id));
                $quotes = Quotes::model()->findByAttributes(array('quote_id'=>$record->quote_id));
                $this->_id = $record->vendor_id;
                $this->setState('main_vendor_name', $record->contact_name);
                $this->setState('vendor_name', $record->contact_name);
                $this->setState('title', $quotes->quote_name);
                $this->setState('type', 0);
                $this->setState('quote_id', $record->quote_id);
                $this->setState('quote_reference', $record->reference);
                $this->setState('quote_vendor_id', $record->id);
                 $this->setState('page_type', 'Quote');
    			$this->setState('isAdmin', true);
                $this->errorCode = self::ERROR_NONE;
            }
        }else if(Yii::app()->controller->action->id=='contractDocumentLogin'){
            //$record=Vendor::model()->findByAttributes(array('emails'=>$this->username,'active'=>1));
            $record=RequestContractDocuments::model()->findByAttributes(array('reference'=>$this->username));
        
            //$password = md5($this->password);
            if($record===null)
                $this->errorCode=self::ERROR_USERNAME_INVALID;
            /*else if($record->password!==$password)
                $this->errorCode=self::ERROR_PASSWORD_INVALID;*/
            else
            {
                $contactName = unserialize($record->supplier_contact_name);
                $contactEmail= unserialize($record->supplier_contact_email);
               // $vendor=Vendor::model()->findByAttributes(array('id'=>$record->vendor_id));
                $contract = Contracts::model()->findByAttributes(array('contract_id'=>$record->contract_id));
                $this->_id = $record->vendor_id;
                $this->setState('main_vendor_name',$contactName[0]);
                $this->setState('vendor_name', $contactName[0]);
                $this->setState('title', $contract->contract_title);
                $this->setState('type', 0);
                $this->setState('contract_id', $record->contract_id);
                $this->setState('contract_document_reference', $record->reference);
                $this->setState('contract_document_vendor_id', $record->vendor_id);
                $this->setState('record_id', $record->id);
                $this->setState('contract_document_type', $record->contract_type);
                $this->setState('page_type', 'Contract Document');
                $this->setState('isAdmin', true);
                $this->errorCode = self::ERROR_NONE;
            }
        }else if(Yii::app()->controller->action->id=='vendorDocumentLogin'){
            //$record=Vendor::model()->findByAttributes(array('emails'=>$this->username,'active'=>1));
            $record=RequestVendorDocuments::model()->findByAttributes(array('reference'=>$this->username));
            //$password = md5($this->password);
            if($record===null)
                $this->errorCode=self::ERROR_USERNAME_INVALID;
            /*else if($record->password!==$password)
                $this->errorCode=self::ERROR_PASSWORD_INVALID;*/
            else
            {
                $contactName = unserialize($record->supplier_contact_name);
                $contactEmail= unserialize($record->supplier_contact_email);
                $vendor=Vendor::model()->findByAttributes(array('vendor_id'=>$record->vendor_id));
                $this->_id = $record->vendor_id;
                $this->setState('main_vendor_name',$vendor->vendor_name);
                $this->setState('vendor_name', $contactName[0]);
                $this->setState('title', $vendor->vendor_name);
                $this->setState('type', 0);
                $this->setState('vendor_document_reference', $record->reference);
                $this->setState('record_id', $record->id);
                $this->setState('vendor_document_type', $record->vendor_document_type);
                $this->setState('page_type', 'Supplier Document');
                $this->setState('isAdmin', true);
                $this->errorCode = self::ERROR_NONE;
            }
        }else if(Yii::app()->controller->action->id=='documentOnboardLogin'){
            //$record=Vendor::model()->findByAttributes(array('emails'=>$this->username,'active'=>1));
            $record=RequestVendorDocumentsOnboard::model()->findByAttributes(array('reference'=>$this->username));
            //$password = md5($this->password);
            if($record===null)
                $this->errorCode=self::ERROR_USERNAME_INVALID;
            /*else if($record->password!==$password)
                $this->errorCode=self::ERROR_PASSWORD_INVALID;*/
            else
            {
                $contactName = unserialize($record->supplier_contact_name);
                $contactEmail= unserialize($record->supplier_contact_email);
                $vendor=Vendor::model()->findByAttributes(array('vendor_id'=>$record->vendor_id));
                $this->_id = $record->vendor_id;
                $this->setState('main_vendor_name',$vendor->vendor_name);
                $this->setState('vendor_name', $contactName[0]);
                $this->setState('title', $vendor->vendor_name);
                $this->setState('type', 0);
                $this->setState('vendor_document_reference', $record->reference);
                $this->setState('record_id', $record->id);
                $this->setState('vendor_document_type', $record->vendor_document_type);
                $this->setState('page_type', 'Supplier Document');
                $this->setState('isAdmin', true);
                $this->errorCode = self::ERROR_NONE;
            }
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}