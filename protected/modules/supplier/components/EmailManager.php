<?php
class EmailManager {

    
    public static function vendorAcivation($vendor) {
      $passwordExp='';
      require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');
      $mainVendorID = UtilityManager::getVendorID();
      $mainVendor = Vendor::model()->findByPk($mainVendorID);

      $vendor_id = $vendor->vendor_id;

      $passwordNew = self::generateRandomString(10);
      $password = md5($passwordNew);
      $userName = $mainVendor->vendor_name;
      $passwordExp .= '<strong><span style="color:#16a085;">Password: </span></strong>' . $passwordNew . '<br/><br/>';
       
      Yii::app()->db->createCommand("UPDATE vendors SET password = '".$password."' WHERE vendor_id = ".$vendor_id)->execute();                  

        $mail = new \PHPMailer(true);                   
        $mail->IsSMTP();
        $mail->Host = Yii::app()->params['host'];
        $mail->Port = Yii::app()->params['port'];
        $mail->SMTPAuth = true;
        $mail->Username = Yii::app()->params['username'];
        $mail->Password = Yii::app()->params['password'];
        $mail->SMTPSecure= Yii::app()->params['stmpSecure'];
        $mail->CharSet="UTF-8";

        $email_body = '';
        $header1 = '<span style="font-size:22px;"><font color="#7f8c8d">Welcome! Supplier Activiation on</font>&nbsp;<strong><span style="color:#2d9ca2;">Oboloo!</span></strong></span>';
        $header2 = '<div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;">        <p>This is a notification from Oboloo on behalf of <span style="color:#16a085;"></span><strong><span style="color:#16a085;">'.$userName.'</span></strong>&nbsp;who have&nbsp;requested access for your</p><p></p><p>account on&nbsp;<span style="color:#16a085;"><strong>Oboloo\'s&nbsp;Supplier Portal</strong></span>.</p>Please find your log in details below:</p></div>';
        $solutation  = $vendor->vendor_name;
        $email_body  .= self::emailHeader($header1,$header2,$solutation);


       $email_body  .='<tr>
         <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
         <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
        $email_body .= '';
        $email_body .= '<strong><span style="color:#16a085;">Username: </span></strong>' .$vendor->emails . '<br/><br/>';
        $email_body .= $passwordExp;
        $email_body .= '<strong><span style="color:#16a085;">Email: </span></strong>' .$vendor->emails . '<br/><br/>';
        $email_body .= '<strong><span style="color:#16a085;">URL: </span></strong><a target="_blank" href="'.Yii::app()->createAbsoluteUrl('default/login').'">'.Yii::app()->createAbsoluteUrl('default/login').'</a><br /><br />';
        $email_body .= '<strong><span style="color:#2d9ca2;">Training and Support URL: </span></strong><a target="_blank" href="https://oboloo.com/supplier-support-hub">https://oboloo.com/supplier-support-hub</a></td>
     </tr>';
        $footerText = 'No Note';

        $email_body .= self::emailFooter($footerText);
        $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
        $mail->addAddress($vendor->emails, $vendor->vendor_name);
        $mail->Subject = 'Oboloo: Supplier Team Member Activation';
        $mail->isHTML(true);
        $mail->Body = $email_body;          
        $mail->send();
        $email_body;
    
    }
    public static function userNotification($email,$name,$subject,$notificationText,$url) {
      require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');
      $emailBody = '';
      $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">Oboloo!</span></strong></span>';
      $header2 = '';
      $emailBody  .= self::emailHeader($header1,$header2);
      $loginUrl = '<a style="display:inline-block;background:#2d9ca2;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:12px 36px 12px 36px;mso-padding-alt:0px;border-radius:14px;" href="' .$url. '"> Login here </a>';
      $emailBody  .='<tr>
       <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
       <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
      $emailBody .= '';
      $emailBody .= '<span style="font-size:18px;color:#2d9ca2;">'.$notificationText.'</span><br/><br/>';
      $emailBody .= $loginUrl;
      $emailBody .= '</td></tr>';
      $emailBody .= self::emailFooter('No Note');

      $mail = new \PHPMailer(true);          
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet="UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
      $mail->addAddress($email, $name);
      $mail->Subject = 'Oboloo: '.$subject;
      $mail->isHTML(true);
      $mail->Body = $emailBody;
      //$mail->SMTPDebug = 2;
      $mail->send();
    }
    public static function userQuote($quote) {
        $user_ids  = array();
        $quote_id  = $quote['quote_id'];
        if(!empty($quote['created_by_user_id'])) $user_ids[$quote['created_by_user_id']] = $quote['created_by_user_id'];
        if(!empty($quote['commercial_lead_user_id'])) $user_ids[$quote['commercial_lead_user_id']] = $quote['commercial_lead_user_id'];
        $sql = "SELECT * FROM quote_vendors q, vendors v WHERE q.quote_id = $quote_id AND q.vendor_id = v.vendor_id";
        $vendors = Yii::app()->db->createCommand($sql)->query()->readAll();
        $vendors = array_column($vendors, 'vendor_name');
        $vendors = implode(", ", $vendors);

        $sql = "select * from users where user_id in(".implode(",",$user_ids).")";
        $users = Yii::app()->db->createCommand($sql)->query()->readAll();
        foreach ($users as $value)
        {
          $user_name = $value['full_name'];
          $user_email = $value['email'];
          $email_body = '';
          $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">Oboloo!</span></strong></span>';
          $header2 = '';

          $email_body  .= self::emailHeader($header1,$header2);

          $notificationComments  = "A new quote <b>".$quote_id."</b> - <b>".$quote['quote_name']."</b> has been created";
          $loginUrl = '<a style="display:inline-block;background:#2d9ca2;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:12px 36px 12px 36px;mso-padding-alt:0px;border-radius:14px;" href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_id). '"> Login here </a>';

 
           $email_body  .='<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
            $email_body .= '';
            $email_body .= '<span style="font-size:18px;color:#2d9ca2;">'.$notificationComments.'</span><br/><br/>';
            $email_body .= $loginUrl;
            $email_body .= '</td></tr>';
            $email_body .= self::emailFooter('No Note');
      
              $mail = new \PHPMailer(true);          
              $mail->IsSMTP();
              $mail->Host = Yii::app()->params['host'];
              $mail->Port = Yii::app()->params['port'];
              $mail->SMTPAuth = true;
              $mail->Username = Yii::app()->params['username'];
              $mail->Password = Yii::app()->params['password'];
              $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
              $mail->CharSet="UTF-8";
              $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
              $mail->addAddress($user_email, $user_name);
              $mail->Subject = 'Oboloo: Quote Publication';
              $mail->isHTML(true);
              $mail->Body = $email_body;
              //$mail->SMTPDebug = 2;
              $mail->send();
            
        }
    }

    public static function userAcivation($vendor) {
      
        require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');
        if ($vendor !=null){
            $new_password = self::generateRandomString(10);
            $user_id = Yii::app()->db->createCommand("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id")->execute();                   

            $mail = new \PHPMailer(true);                   
            $mail->IsSMTP();
            $mail->Host = Yii::app()->params['host'];
            $mail->Port = Yii::app()->params['port'];
            $mail->SMTPAuth = true;
            $mail->Username = Yii::app()->params['username'];
            $mail->Password = Yii::app()->params['password'];
            $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
            $mail->CharSet="UTF-8";

            $email_body = '';
            $header1 = '<span style="font-size:22px;"><font color="#7f8c8d">Welcome! User Activiation on</font>&nbsp;<strong><span style="color:#2d9ca2;">Oboloo!</span></strong></span>';
            $header2 = '<span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">This is a notification from </span><strong><span style="color:#16a085;">Oboloo</span></strong><span style="color:black">. As per your request, your account has been activated and can be accessed via the below credentials:</span></span></span></span>';
            $solutation  = $user_data['full_name'];
            $email_body  .= self::emailHeader($header1,$header2,$solutation);
 

           $email_body  .='<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
            $email_body .= '';
            $email_body .= '<strong><span style="color:#16a085;">Username: </span></strong>' . $user_data['username'] . '<br/><br/>';
            $email_body .= '<strong><span style="color:#16a085;">Password: </span></strong>' . $new_password . '<br/><br/>';
            $email_body .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $user_data['email'] . '<br/><br/>';
            $email_body .= '<strong><span style="color:#16a085;">URL: </span></strong><a target="_blank" href="'.Yii::app()->createAbsoluteUrl('app/login').'">'.Yii::app()->createAbsoluteUrl('app/login').'</a></td>
         </tr>';
            $email_body .= self::emailFooter(); 
            $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
            $mail->addAddress($user_data['email'], $user_data['full_name']);
            $mail->Subject = 'Oboloo: User Activation';
            $mail->isHTML(true);
            $mail->Body = $email_body;          
            $mail->send();

        }
    
    }
    public static function userPasswordChangeReuest($email) {
      
        require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');

        $vendor=Vendor::model()->findByAttributes(array('emails'=>$email));
        
        if ($vendor !=null)
        {   $vendorID = $vendor->vendor_id;   
            $mail = new \PHPMailer(true);                   
            $mail->IsSMTP();
            $mail->Host = Yii::app()->params['host'];
            $mail->Port = Yii::app()->params['port'];
            $mail->SMTPAuth = true;
            $mail->Username = Yii::app()->params['username'];
            $mail->Password = Yii::app()->params['password'];
            $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
            $mail->CharSet="UTF-8";

            $email_body = '';
            $header1 = '<font color="#7f8c8d">Request for new password&nbsp;on</font>&nbsp;<strong><span style="color:#2d9ca2;">Oboloo</span></strong>';
            $header2 = '<span style="color:black">This is a notification from </span><span style="color:#16a085;"><strong>Oboloo</strong></span><span style="color:black">. Please follow the link below to change your password:</span></span>';
            $email_body  .= self::emailHeader($header1,$header2);
 

           $email_body  .='<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
            $email_body .= '';
            $email_body .= '<strong><span style="color:#16a085;">Username: </span></strong>' .  $vendor->emails . '<br/><br/>';
          
            $email_body .= '<strong><span style="color:#16a085;">Change Password Link: </span></strong><a href="' .Yii::app()->createAbsoluteUrl('default/changePasswordSave',array("user-id"=>base64_encode($vendorID),'time'=>base64_encode(date("m-d"))))  . '" title="Change Password">'.Yii::app()->createAbsoluteUrl('app/changePasswordSave',array("user-id"=>base64_encode($vendorID),'time'=>base64_encode(date("m-d")))).'</a></td>
         </tr>';
            $email_body .= self::emailFooter();  ;     
            $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
            $mail->addAddress($vendor->emails, $vendor->vendor_name);
            $mail->Subject = 'Oboloo: Password Change';
            $mail->isHTML(true);
            $mail->Body = $email_body;          
            $mail->send();
            return 1;
        }
        return 0;
    }
  public static function userPasswordChanged($vendor) {
    require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');
    if($vendor !=null)
    { 
      $vendorID = $vendor->vendor_id;
      $mail = new \PHPMailer(true);                   
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet="UTF-8";

      $email_body = '';
      $header1 = '<font color="#7f8c8d">Your password has been changed on </font>&nbsp;<strong><span style="color:#2d9ca2;">Oboloo</span></strong>';
      $header2 = '<span style="color:black">This is a notification from Oboloo to let you know that your password has changed.</span>';
      $email_body  .= self::emailHeader($header1,$header2);
      $email_body .= self::emailFooter();

      $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
      $mail->addAddress($vendor->emails, $vendor->vendor_name);
      $mail->Subject = 'Oboloo: Password Change';
      $mail->isHTML(true);
      $mail->Body = $email_body;          
      $mail->send();
    }
  }

  public static function generateRandomString($length = 10) 
  {
      $characters = '0123456789%@*abcdefghijklmnop!~qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++)
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      return $randomString;
  }

  public static function userPasswordForgotReuest($email) {
    require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');
    $vendor=Vendor::model()->findByAttributes(array('emails'=>$email));
      if($vendor !=null){ 
        $vendorID = $vendor->vendor_id;
        $newPassword = self::generateRandomString(10);
        $sql = "UPDATE vendors SET password = MD5('".$newPassword."') WHERE vendor_id = $vendorID";
        Yii::app()->db->createCommand($sql)->execute();                
        $mail = new \PHPMailer(true);                   
        $mail->IsSMTP();
        $mail->Host = Yii::app()->params['host'];
        $mail->Port = Yii::app()->params['port'];
        $mail->SMTPAuth = true;
        $mail->Username = Yii::app()->params['username'];
        $mail->Password = Yii::app()->params['password'];
        $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
        $mail->CharSet="UTF-8";
        $emailBody = '';
        $header1 = '<font color="#7f8c8d">Forgotten password &nbsp;on</font>&nbsp;<strong><span style="color:#2d9ca2;">Oboloo</span></strong>';
        $header2 = '<span style="color:black">This is a notification from </span><span style="color:#16a085;"><strong>Oboloo</strong></span><span style="color:black">. As per your request, your password has been reset to the below:</span>';
        $emailBody  .= self::emailHeader($header1,$header2);
        $emailBody  .='<tr>
         <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
         <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
        $emailBody .= '';
        $emailBody .= '<strong><span style="color:#16a085;">Username: </span></strong>' . $vendor->emails. '<br/><br/>';
        $emailBody .= '<strong><span style="color:#16a085;">Password: </span></strong>'.$newPassword.'</td>
     </tr>';
      $emailBody .=self::emailFooter();  
      $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
      $mail->addAddress($vendor->emails, $vendor->vendor_name);
      $mail->Subject = 'Oboloo: Password Change';
      $mail->isHTML(true);
      $mail->Body = $emailBody;          
      $mail->send();

      }
    
    }	

    public static function emailHeader($header1,$header2,$solutation=''){
      $bodyHtml='<div class="mj-container" style="background-color:#FFFFFF;">
     <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="">';
            $bodyHtml .= '<img src="' . AppUrl::bicesUrl('images/email_banner.jpg') . '" style=""border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="259" />';
            $bodyHtml .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <span style="font-size:22px;">'.$header1.'</span>
             </div>
         </td>
         </tr>';
        if(!empty($solutation)){
            $bodyHtml .='<tr><td align="left" style="font-size:0px;padding:15px 15px 0px 15px;word-break:break-word;"><div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"> <p><span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">Dear&nbsp;</span><span style="color:#16a085;"><strong>'.$solutation.'</strong></span></span></span></span></p></div></td></tr>';
        }

        $bodyHtml .='<tr><td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
          <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;"><br /><span style="font-family:" tahoma",sans-serif"="">'.$header2.'</span></span></span></div></td>
         </tr>';
      return $bodyHtml;
    }

    public static function emailFooter($footerText=''){
      $bodyHtml = '';
      if($footerText==''){
         $bodyHtml='<tr><td align="center" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"><div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><p><strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:#c0392b;">NOTE:</span><span style="color:black"> </span></span></span></strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">If you did not request this change, please contact your administrator immediately.</span></span></span></p></div></td>
         </tr>';
      }else if($footerText !='No Note'){
        $bodyHtml= $footerText;
      }
      $bodyHtml .=' 
          <tr>
           <td style="word-wrap:break-word;font-size:0px;">
           <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
           </td>
          </tr>
          <tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"> <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.2;text-align:left;color:#2D9CA2;">        <p lang="x-size-17"><span style="font-size:14px;">Thanks,<br><strong>The Oboloo Team</strong><br><em>Support -&nbsp;<strong>hello@oboloo.com</strong></em></span></p><p lang="x-size-17"></p><p lang="x-size-10"><a href="https://oboloo.com/privacy-policy"><span style="font-size:14px;">Privacy Policy</span></a></p><p lang="x-size-10"><a href="http://oboloo.com/Terms"><span style="font-size:14px;">Terms</span></a></p><table emb-web-links="" role="presentation"> <tbody>   <tr role="navigation"><td emb-web-links=""></td><td emb-web-links=""></td>      <td emb-web-links=""></td>    </tr> </tbody></table><p></p><p></p></div></td></tr>
          <tr><td align="center" style="font-size:0px;padding:10px 10px 10px 10px;word-break:break-word;"><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"      >        <tr><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;"><tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;"><a href="www.facebook.com/oboloosoftware" target="_blank"><img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/facebook.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td></tr></tbody></table></td></tr></tbody></table></td><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;"><tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;"><a href="https://twitter.com/oboloosoftware" target="_blank"><img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/twitter.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td></tr></tbody></table></td></tr></tbody></table></td><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;">            <tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;"><a href="https://www.linkedin.com/company/oboloo" target="_blank"><img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/linkedin.png" style="border-radius:3px;display:block;" width="35"></a>                </td></tr></tbody></table></td></tr></tbody></table></td></tr></table></td></tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
    </div>';
      return $bodyHtml;
    } 

      public static function emailQuoteContributorUser($email,$name,$subject,$notificationText,$url) {
      require_once(Yii::getPathOfAlias('webroot').'/protected/vendors/phpmailer/PHPMailerAutoload.php');
      $emailBody = '';
      $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">Oboloo!</span></strong></span>';
      $header2 = '';
      $emailBody  .= self::emailHeader($header1,$header2);
      $loginUrl = '<a style="display:inline-block;background:#2d9ca2;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:12px 36px 12px 36px;mso-padding-alt:0px;border-radius:14px;" href="' .$url. '"> Login here </a>';
      $emailBody  .='<tr>
       <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
       <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
      $emailBody .= '';
      $emailBody .= '<span style="font-size:18px;color:#2d9ca2;">'.$notificationText.'</span><br/><br/>';
      $emailBody .= $loginUrl;
      $emailBody .= '</td></tr>';
      $emailBody .= self::emailFooter('No Note');

      $mail = new \PHPMailer(true);          
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet="UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
      $mail->addAddress($email, $name);
      $mail->Subject = 'Oboloo: '.$subject;
      $mail->isHTML(true);
      $mail->Body = $emailBody;
      //$mail->SMTPDebug = 2;
      $mail->send();
    }						
}