<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Notifications</h3>
        </div>
        <div class="clearfix"> </div>
    </div>

	<form class="form-horizontal" id="notification_list_form" name="notification_list_form" role="form" method="post" action="<?php echo $this->createUrl('notificatons/index'); ?>" autocomplete="off">
		<div class="form-group">
          <div class="col-md-2">
              <input type="text" class="form-control" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date("d/m/Y", strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> >
          </div>
			
          <div class="col-md-2">
              <input type="text" class="form-control" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date("d/m/Y", strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> >
          </div>

			<div class="col-md-3">
				<button class="btn btn-primary" onclick="$('#notification_list_form').submit();">Search Notifications</button>
			</div>

			<div class="col-md-2">
			</div>

            <div class="col-md-3 text-right">
				<button class="btn btn-danger" onclick="deleteNotifications(); return false;">Delete Selected</button>
            </div>
		</div>
	</form>
<div class="clearfix"> <br /><br /> </div>


    <table id="notifications_table" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Date</th>
          <th>Notification</th>
          <th>Delete</th>
        </tr>
      </thead>

      <tbody>

          <?php /* $str = '<a href="http://dev.spend-365.com/orders/edit/73">Order ID #73</a> has been declined by Rubin Wilson';
                $start = strpos($str,"<a");
                $end   = strpos($str,"</a>");
                echo substr_replace($str,'',$start,$end);
                echo "<br />";

                $str = 'More information has been requested by Rubin Wilson for <a href="http://dev.spend-365.com/orders/edit/71">Order ID #71</a>';
                $start = strpos($str,"<a");
                $end   = strpos($str,"</a>");
                echo substr_replace($str,'',$start,$end);*/

                
                ?>
          <?php 

          $notificationLabelArr = array();
          foreach ($notifications as $notification) { 
               /* $str = $notification['notification_text'];
                $start = strpos($str,"<a");
                $end   = strpos($str,"</a>");
                $parsedStr = substr_replace($str,'',$start,$end);
                $notificationLabelArr[$parsedStr] = $parsedStr;*/
            ?>

              <tr>
                    <td><?php echo $notification['id']; ?></td>
                    <td><?php echo date("F j, Y h:iA", strtotime($notification['notification_date'])); ?></td>
                    <td><?php echo $notification['notification_text']; ?></td>
                    <td><input class="delete_notification_checkbox" type="checkbox" name="delete_notification_id[]" value="<?php echo $notification['id']; ?>" /></td>
              </tr>

          <?php } ?>

      </tbody>

  </table>
  <?php //echo "<pre>"; print_r($notificationLabelArr);exit;?>

</div>


<div class="modal fade" id="delete_notifications_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Delete Notifications</h4>
        </div>
        <div class="modal-body">
        	<p>Are you sure you want to delete the selected notifications? This action cannot be reversed.</p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-info btn btn-default" data-dismiss="modal">
          	  No
          </button>
          <button id="yes_delete" type="button" class="alert-success btn btn-default">
          	  Yes
          </button>
        </div>
      </div>
	</div>
</div>


<div class="modal fade" id="nothing_selected_modal" role="dialog" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Delete Notifications</h4>
        </div>
        <div class="modal-body">
          <p>You must select at least one notification to delete.</p>
        </div>
        <div class="modal-footer">
          <button id="ok_invoice_message" type="button" class="alert-info btn btn-default" data-dismiss="modal">
          	  Ok
          </button>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s.replace('AM', ':00').replace('PM', ':00'));
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );


$.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
	locale: {
	    format: 'DD/MM/YYYY'
	}
  });
};

$(document).ready( function() {
    $('#notifications_table').dataTable({
        "columnDefs": [ 
        	{ "targets": 0, "width": "6%", "orderable": false },
			{ "type": "sort-month-year", targets: 1 }         
        ],
        "order": []
    });

	$('#from_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$('#to_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$('#delete_notifications_modal .modal-footer button').on('click', function(event) {
	    var button = event.target; // The clicked button
	    if (button.id == 'yes_delete') {
		  	var selected_notifications_for_delete = "";
		  	$('.delete_notification_checkbox').each(function() {
				if ($(this).is(':checked'))
				{
					if (selected_notifications_for_delete == "")
						selected_notifications_for_delete = $(this).val();
					else
						selected_notifications_for_delete = selected_notifications_for_delete + ',' + $(this).val();
				}
		  	});
		  	window.location = '<?php echo AppUrl::bicesUrl('notificatons/deleteNotifications/?notification_ids_to_delete=') ?>' + selected_notifications_for_delete;
	    }
	    else $('.delete_notification_checkbox').each(function() { $(this).removeAttr('checked'); });
   });

});

function deleteNotifications()
{
	var selected_notifications_for_delete = 0;
	$('.delete_notification_checkbox').each(function() {
		if ($(this).is(':checked')) selected_notifications_for_delete = selected_notifications_for_delete + 1;
	});
	
	if (selected_notifications_for_delete == 0) $('#nothing_selected_modal').modal('show');
	else $('#delete_notifications_modal').modal('show');
}
</script>
