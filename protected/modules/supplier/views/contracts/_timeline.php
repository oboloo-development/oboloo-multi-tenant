<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<h4><strong>Timelines</strong><br /><br /></h4>
	</div>
</div>
<div class="clearfix"></div>

<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Contract Start Date</label>
		<input type="text" class="form-control contract_start_date notranslate"
			name="contract_start_date" id="contract_start_date"
			<?php if (!empty($contract['contract_start_date']) &&  $contract['contract_start_date'] !="0000-00-00") echo 'value="' . date("d/m/Y", strtotime($contract['contract_start_date'])) . '"'; else echo 'placeholder="Contract Start Date"'; ?> disabled="disabled" >
		
	</div>
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
	<label class="control-label">Contract End Date</label>
		<input type="text" class="form-control contract_end_date notranslate"
			name="contract_end_date" id="contract_end_date"
			<?php if (!empty($contract['contract_end_date']) &&  $contract['contract_end_date']!="0000-00-00") echo 'value="' . date("d/m/Y", strtotime($contract['contract_end_date'])) . '"'; else echo 'placeholder="Contract End Date"'; ?> disabled="disabled" >
		
	</div>
</div><div class="clearfix"></div><br />

<!-- <div class="form-group">

	<div class="col-md-3 col-sm-3 col-xs-6">
		<label class="control-label">Notice Period Date</label>
		<input type="text" class="form-control notranslate" name="break_clause"
			id="break_clause"
			<?php if (!empty($contract['break_clause']) && $contract['break_clause'] !="0000-00-00 00:00:00") echo 'value="' . $contract['break_clause'] . '"'; else echo 'placeholder="Notice Period Date"'; ?> disabled="disabled" >
	</div>

	<div class="col-md-3 col-sm-3 col-xs-6">
		<label class="control-label">Termination Terms</label>
		<input type="text" class="form-control notranslate" name="termination_terms"
			id="termination_terms"
			<?php if (isset($contract['termination_terms']) && !empty($contract['termination_terms'])) echo 'value="' . $contract['termination_terms'] . '"'; else echo 'placeholder="Termination Terms"'; ?> disabled="disabled" >
	</div>
</div> -->
<div class="clearfix"></div><br />
<!-- <div class="form-group">
	<div class="label-adjust"><label class="control-label">Extension Options (in Months)</label></div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control notranslate"
			name="extension_options" id="extension_options"
			<?php if (isset($contract['extension_options']) && !empty($contract['extension_options'])) echo 'value="' . $contract['extension_options'] . '"'; else echo 'placeholder="Extension Options (in Months)"'; ?> disabled="disabled" >
		
	</div>
</div> -->
