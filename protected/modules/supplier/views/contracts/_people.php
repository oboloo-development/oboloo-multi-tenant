<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<h4> <strong>People </strong><br /><br /></h4>
	</div>
</div>
<div class="clearfix"></div>

<div class="form-group">
      <div class="label-adjust"><label class="control-label">User Full Name</label></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input  type="text" class="form-control notranslate" value="<?php echo $user->full_name;?>" disabled="disabled" /></div>
    </div><div class="clearfix"></div><br />

    <div class="form-group">
      <div class="label-adjust"><label class="control-label">Senior Business Contract Owner</label></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input  type="text" class="form-control notranslate" value="<?php echo $contract->contract_manager;?>" disabled="disabled" /></div>
    </div><div class="clearfix"></div><br />

    <div class="form-group">
      <div class="label-adjust"><label class="control-label">Commercial Lead</label></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input  type="text" class="form-control notranslate" value="<?php echo $contract->commercial_lead;?>" disabled="disabled" /></div>
    </div><div class="clearfix"></div><br />

    <div class="form-group">
      <div class="label-adjust"><label class="control-label">Procurement Lead</label></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input  type="text" class="form-control notranslate" value="<?php echo $contract->procurement_lead;?>" disabled="disabled" /></div>
    </div><div class="clearfix"></div><br />

     <div class="form-group">
      <div class="label-adjust"><label class="control-label">Service Provider</label></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input  type="text" class="form-control notranslate" value="<?php echo $vendor->vendor_name;?>" disabled="disabled" /></div>
    </div><div class="clearfix"></div><br />

    <div class="form-group">
      <div class="label-adjust"><label class="control-label">Service Provider Contact Name</label></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input  type="text" class="form-control notranslate" value="<?php echo $contract->contact_name;?>" disabled="disabled" /></div>
    </div><div class="clearfix"></div><br />

     <div class="form-group">
      <div class="label-adjust"><label class="control-label">Service Provider Email</label></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input  type="text" class="form-control" value="<?php echo $contract->website;?>" disabled="disabled" /></div>
    </div><div class="clearfix"></div><br />

     <div class="form-group">
      <div class="label-adjust"><label class="control-label">Service Provider Telephone</label></div>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input  type="text" class="form-control notranslate" value="<?php echo $contract->contact_info;?>" disabled="disabled" /></div>
    </div><div class="clearfix"></div><br />
