<?php $contract_id = $contract['contract_id'];?>
<div class="clearfix"></div>
<div class="form-group">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<h4><strong>Financial Information</strong></h4>
	</div>
</div>
<div class="clearfix"></div><br />
<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-6 valid">
		<label class="control-label">Currency Rate </label>
		<select name="currency_id" id="currency_id" class="form-control notranslate" onchange="loadCurrencyRate();" disabled="disabled" >
			<?php foreach ($currency_rate as $rate) { ?>
				<option value="<?php echo $rate['id']; ?>"
					<?php if (isset($contract['currency_id']) && $rate['id'] == $contract['currency_id']) echo ' selected="SELECTED" '; ?>>
					<?php echo $rate['currency']; ?>
				</option>
			<?php } ?>
		</select>
	</div>

	<div class="col-md-3 col-sm-3 col-xs-6 valid">
		<label class="control-label">Currency Rate</label>
		<input readonly type="text" class="form-control notranslate" name="currency_rate" id="currency_rate"
			<?php if (isset($contract['currency_rate']) && !empty($contract['currency_rate'])) { echo 'value="' . $contract['currency_rate'] . '"'; } else { echo 'value="1"'; } ?> disabled="disabled" >

	</div>

</div><div class="clearfix"></div><br />

<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Estimated Total Contract Value</label>
		<input type="text" class="form-control notranslate"
			name="contract_value" id="contract_value"
			style="text-align: right;"
			<?php if (isset($contract['contract_value']) && !empty($contract['contract_value'])) echo 'value="' . $contract['contract_value'] . '"'; else echo 'placeholder="Estimated Total Contract Value"'; ?> disabled="disabled" >
		
	</div>
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Estimated Annual Contract Value</label>
		<input type="text" class="form-control notranslate"
			   name="contract_budget" id="contract_budget"
			   style="text-align: right;"
			<?php if (isset($contract['contract_budget']) && !empty($contract['contract_budget'])) echo 'value="' . $contract['contract_budget'] . '"'; else echo 'placeholder="Estimated Annual Contract Value"'; ?> disabled="disabled" >
		
	</div>
</div><div class="clearfix"></div><br />

<div class="form-group">
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Original Contract Value</label>
		<input type="text" class="form-control notranslate"
			name="original_budget" id="original_budget"
			style="text-align: right;"
			<?php if (isset($contract['original_budget']) && !empty($contract['original_budget'])) echo 'value="' . $contract['original_budget'] . '"'; else echo 'placeholder="Original Contract Value"'; ?> disabled="disabled" >
		
	</div>
	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Original Contract Annual Value</label>
		<input type="text" class="form-control notranslate"
			name="original_annual_budget" id="original_annual_budget"
			style="text-align: right;"
			<?php if (isset($contract['original_annual_budget']) && !empty($contract['original_annual_budget'])) echo 'value="' . $contract['original_annual_budget'] . '"'; else echo 'placeholder="Original Contract Annual Value"'; ?> disabled="disabled" >
		
	</div>
</div>
<div class="clearfix"></div><br />
<div class="form-group">

	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">PO Number</label>
		<input type="text" class="form-control notranslate"
			name="po_number" id="po_number" style="text-align: right;"
			<?php if (isset($contract['po_number']) && !empty($contract['po_number'])) echo 'value="' . $contract['po_number'] . '"'; else echo 'placeholder="PO Number"'; ?> disabled="disabled" >
		
	</div>

	<div class="col-md-3 col-sm-3 col-xs-6 date-input">
		<label class="control-label">Cost Centre</label>
		<input type="text" class="form-control notranslate"
			name="cost_centre" id="cost_centre" style="text-align: right;"
			<?php if (isset($contract['cost_centre']) && !empty($contract['cost_centre'])) echo 'value="' . $contract['cost_centre'] . '"'; else echo 'placeholder="Cost Centre"'; ?> disabled="disabled" >
		
	</div>
</div> 


<div class="clearfix"></div><br />
			