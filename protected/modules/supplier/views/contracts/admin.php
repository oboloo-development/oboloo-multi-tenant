<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left"><br />
            <h3>Contracts</h3>
        </div>
        <div class="clearfix"> </div>
    </div>
	<form class="form-horizontal" id="contract_list_form" name="contract_list_form" role="form" method="post" action="">
		<div class="col-md-2 col-sm-2 col-xs-12 location">
			<div class="form-group">
			<select name="location_id[]" id="location_id" class="form-control" multiple searchable="Search here.."  onchange="loadDepartments(0);">
				<?php
				$i = 0 ;
				foreach ($locations as $location) {
					if(isset($location_id[$i])){
						?>
						<option value="<?php echo $location->location_id; ?>"
							<?php if ($location_id[$i] == $location->location_id) echo ' selected="SELECTED" '; ?>>
							<?php echo $location->location_name; ?>
						</option>
					<?php } else { ?>
						<option value="<?php echo $location->location_id; ?>">
							<?php echo $location->location_name; ?>
						</option>
					<?php } ?>
					<?php
					$i++;
				} ?>
			</select>
		</div></div>
			<div class="col-md-2 col-sm-2 col-xs-12 department">
				<div class="form-group">
				<select name="department_id[]" id="department_id" class="form-control" multiple searchable="Search here..">
					<option value="0">All Departments</option>
					<?php if(!empty($department_info))
                        foreach($department_info as $dept_value){?>
                        <option value="<?php echo $dept_value->department_id;?>" <?php if (in_array($dept_value->department_id,array($department_id))) echo ' selected="SELECTED" '; ?>><?php echo $dept_value->department_name;?></option>
                    <?php } ?>
                    
				</select>
			</div></div>
			<!-- <div class="col-md-2">
				<div class="form-group">
				<select name="contract_status" id="contract_status"
						class="form-control">
					<option value="">All Contract Statuses</option>
          	  	  <?php 
          	  	  foreach (FunctionManager::contractStatuses() as $contract_status) { ?>
          	  	  		<option value="<?php echo $contract_status['code']; ?>"
							<?php if (isset($_POST['contract_status']) && $_POST['contract_status'] == $contract_status['code']) echo ' selected="SELECTED" '; ?>>
          	  	  			<?php echo $contract_status['value']; ?>
          	  	  		</option>
          	  	  <?php } ?>
          	  	</select>
				</div>
			</div> -->
			<div class="col-md-2 col-sm-2 col-xs-12  col-md-offset-2 col-sm-offset-2">
			<div class="form-group">
			
				<select name="category_id" id="category_id" class="form-control border-select" onchange="loadSubcategories(0);">
					<option value="0">All Categories</option>
					<?php foreach ($categories as $category) { ?>
						<option value="<?php echo $category['id']; ?>"
								<?php if (isset($category_id) && $category_id == $category->id) echo ' selected="SELECTED" '; ?>>
							<?php echo $category->value; ?>
						</option>
					<?php } ?>
				</select>
			</div></div>
			<div class="col-md-2 col-sm-2 col-xs-12 ">
				<div class="form-group">
				<select name="subcategory_id" id="subcategory_id" class="form-control border-select">
					<option value="0">All Subcategories</option>
				</select></div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-2 text-right-not-xs">
				<button class="btn btn-primary" onclick="$('#contract_list_form').submit();">Search Contracts</button>
			</div>
		</form>
<div class="clearfix"> </div>
<div class="row tile_count"> </div>

    <table id="contract_table" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
        <tr>
          <th> </th>
          <th>Reference</th>
          <th>Title</th>
          <th>Start Date</th>
          <th>End Date</th>
		  <!-- <th>Amount in Order Currency</th> -->
        </tr>
      </thead>

      <tbody>

          <?php 
          		foreach ($contracts as $contract) 
          		{
					$class = '';
          			$endDate = date("Y-m-d",strtotime($contract->contract_end_date));
          			$afterMonth = date("Y-m-d", strtotime("+1 month"));
          			$after3Month = date("Y-m-d", strtotime("+3 months"));

          			if ($endDate < date("Y-m-d")){
          			 $class = ' class="" style="background: #d1d1d1 !important;" ';
          			 $color="";
          			}
					else
					{
						if ($endDate >=  date("Y-m-d") && $endDate <=$afterMonth)
						{
							$class = ' class="" style="background: #F0DFDF !important;" ';
							 $color="";
						}else if ($endDate >=  date("Y-m-d") && $endDate <=$after3Month)
						{
							$class = ' class="" style="background: #FDEC90 !important;" ';
							 $color="";
						}
						else {
							$class = ' class="" style="background: #DDF1D5 !important;" ';
							$color="";
							}

					}
          ?>
		              <tr <?php if (!empty($class)) echo $class; ?>>
		                    <td>
		                        <a href="<?php echo Yii::app()->createUrl('contracts/view',array('id'=>$contract->contract_id)); ?>">
		                            <button class="btn btn-sm btn-success">View/Edit</button>
		                        </a>
		                    </td>
		                    <td><?php echo $contract->contract_reference; ?></td>
		                    <td><?php echo $contract->contract_title; ?></td>
		                    <td><?php echo date("F j, Y", strtotime($contract->contract_start_date)); ?></td>
		                    <td><?php echo date("F j, Y", strtotime($contract->contract_end_date)); ?></td>
						  
						    <!-- <td style="text-align: right;">
							  <nobr>

								  <?php
								   $price_total = $contract->contract_value;
			                    	$currency_id = $contract->currency_id;

                                  switch ($contract->currency) {
                                      case 'GBP' : $currency = '&#163;';
                                          break;
                                      case 'USD' : $currency = '$';
                                          break;
                                      case 'CAD' : $currency = '$';
                                          break;
                                      case 'CHF' : $currency = '&#8355;';
                                          break;
                                      case 'EUR' : $currency = '&#8364;';
                                          break;
                                      case 'JPY' : $currency = '&#165;';
                                          break;
                                      case 'INR' : $currency = '&#x20B9;';
                                          break;
                                      default :  $currency = '&#163;';
                                          break;
                                  }
			                   	echo $currency.number_format($price_total, 2);
								?>
							  </nobr>
						  </td> -->
		              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>
<style>

	.location  .multiselect {
		width: 138%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}
	.btn .caret {
		float: right;
		margin-top: 10px;

	}

</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions2(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Departments',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }


	$('#location_id').multiselect(getOptions(true));
	$('#department_id').multiselect(getOptions2(true));

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );


$(document).ready( function() {

	<?php if (isset($category_id) && !empty($category_id)) { ?>

		<?php if (isset($subcategory_id) && !empty($subcategory_id)) { ?>
			loadSubcategories(<?php echo $subcategory_id; ?>);
		<?php } else { ?>
			loadSubcategories(0);
		<?php } ?>

	<?php } ?>

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			//loadDepartments(<?php //echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>




    $('#contract_table').dataTable({
        "columnDefs": [ 
        	{ "targets": 0, "width": "6%", "orderable": false },
			{ "type": "sort-month-year", targets: 4 },         
			{ "type": "sort-month-year", targets: 5 },         
        ],
        "order": []
    });
});

function loadSubcategories(subcategory_id)
{
	var category_id = $('#category_id').val();
	
	if (category_id == 0)
		$('#subcategory_id').html('<option value="0">All Subcategories</option>');
	else
	{
	    $.ajax({
	        type: "POST", data: { category_id: category_id }, dataType: "json",
	        url: "<?php echo AppUrl::bicesUrl('products/getSubCategories/'); ?>",
	        success: function(options) {
	        	var options_html = '<option value="0">All Subcategories</option>';
	        	for (var i=0; i<options.suggestions.length; i++)
	        		options_html += '<option value="' + options.suggestions[i].data + '">' + options.suggestions[i].value + '</option>';
	        	$('#subcategory_id').html(options_html);
	        	if (subcategory_id != 0) $('#subcategory_id').val(subcategory_id); 
	        },
	        error: function() { $('#subcategory_id').html('<option value="0">All Subcategories</option>'); }
	    });
	}
}

function exportChart(canvas_id)
{
	location = '<?php echo AppUrl::bicesUrl('app/exportChart/?canvas_id='); ?>' + canvas_id;
}

function addContractModal(e){
	e.preventDefault();
    $.ajax({
        datatype: 'html',
        url: "<?php echo AppUrl::bicesUrl('contracts/createByModal'); ?>",
        type: "POST",
        data: {modalWind:1},
        success: function(mesg) {
            $('#create_contract_cont').html(mesg);
            $('#create_contract').modal('show');
            
             
        }
    });
}
</script>
<style type="text/css">
.location .multiselect {width: 138%%;}
.department .multiselect {width: 112%;}

</style>