<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->order_id,
);

$this->menu=array(
	array('label'=>'List Orders', 'url'=>array('index')),
	array('label'=>'Create Orders', 'url'=>array('create')),
	array('label'=>'Update Orders', 'url'=>array('update', 'id'=>$model->order_id)),
	array('label'=>'Delete Orders', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->order_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Orders', 'url'=>array('admin')),
);
?>

<h1>View Orders #<?php echo $model->order_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'order_id',
		'location_id',
		'department_id',
		'project_id',
		'vendor_id',
		'user_id',
		'user_name',
		'user_email',
		'currency_id',
		'currency_rate',
		'order_date',
		'description',
		'total_price',
		'calc_price',
		'tax_rate',
		'ship_amount',
		'other_charges',
		'discount',
		'spend_type',
		'order_status',
		'invoice_number',
		'recurrent_number',
		'created_by_user_id',
		'created_datetime',
		'updated_datetime',
	),
)); ?>
