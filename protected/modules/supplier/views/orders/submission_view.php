<?php $order_id = $purchaseModel->order_id;
	$path = Yii::getPathOfAlias('webroot').'/../';
	$vendor_id = $vendorID;
	$po_id = $poID;
?>
 <div class="right_col" role="main">
  <div class="row-fluid tile_count col-md-12">
    <div class="span6 pull-left">
        <h3>Submit Order Details </h3>
    </div>
    <div class="span6 pull-right">
        <a href="<?php echo $this->createAbsoluteUrl('orders/submission'); ?>" class="btn btn-info">Submissions List</a>
    </div>
    <div class="clearfix"> </div>
    <?php if(Yii::app()->user->hasFlash('error')): ?>
    <div class="alert alert-danger" role="alert">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php elseif (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
  <?php endif; ?>

<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs">
  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background: none !important;">
    <li role="presentation" class="active">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="quote-tab" role="tab" data-toggle="tab" aria-expanded="true">
            Order Details
        </a>
    </li>
    <li role="presentation" class="">
        <a href="#tab_content2" role="tab" id="document-tab" data-toggle="tab" aria-expanded="false">
            Uploads Invoices
        </a>
    </li>

  </ul>
  <div class="clearfix"> </div>

  <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="">

  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab"><br />
    	<h3 class="col-lg-12">Order Information </h3>
			<div class="form-group">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 valid">  
					<label class="control-label">Location <span style="color: #a94442;">*</span></label>
					<select name="location_id" id="location_id" class="form-control" disabled="disabled">
						<option value="<?php echo $purchaseModel->location_id; ?>" selected="SELECTED">
							<?php echo $purchaseModel->location_name; ?>
						</option>
					</select>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 valid">
					<label class="control-label">Department <span style="color: #a94442;">*</span></label>
					<select name="department_id" id="department_id" class="form-control" disabled="disabled">
						<option value="<?php echo $purchaseModel->department_id; ?>" selected="SELECTED">
							<?php echo $purchaseModel->department_name; ?>
						</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 valid">
					<label class="control-label">Project</label>
					<select name="project_id" id="project_id" class="form-control" disabled="disabled">
						<option value="">Select Project</option>
							<option value="<?php echo  $projects->project_id; ?>" selected="SELECTED">
								<?php echo $projects->project_name; ?>
							</option>
					</select>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 valid">
					<label class="control-label">Spend Type</label>
					<select name="spend_type" id="spend_type" class="form-control" disabled="disabled">
						<option value=""></option>
						<option  <?php if (isset($purchaseModel->spend_type) && $purchaseModel->spend_type == 'Goods') echo ' selected="SELECTED" '; ?> value="Goods">Goods</option>
						<option  <?php if (isset($purchaseModel->spend_type) && $purchaseModel->spend_type == 'Services') echo ' selected="SELECTED" '; ?> value="Services">Services</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 valid">
					<label class="control-label">Currency Rate </label>
					<select name="currency_id" id="currency_id" class="form-control" disabled="disabled">
					<option value="<?php echo $currencyRates['id']; ?>" selected="SELECTED">
					<?php echo $currencyRates['currency']; ?>
					</option>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 valid">
					<label class="control-label">Currency Rate</label>
					<input readonly type="text" class="form-control" name="currency_rate" id="currency_rate"
						<?php if (isset($purchaseModel->currency_rate) && !empty($purchaseModel->currency_rate)) { echo 'value="' . $purchaseModel->currency_rate . '"'; } else { echo 'value="1"'; } ?> >

				</div>
			</div>
			<div class="form-group">

				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 date-input">
					<label class="control-label">Order Date <span style="color: #a94442;">*</span></label>
					<input type="text" class="form-control has-feedback-left" name="order_date" id="order_date" disabled="disabled"
						<?php if (isset($purchaseModel->order_date) && !empty($purchaseModel->order_date)) echo 'value="' . date("d/m/Y", strtotime($purchaseModel->order_date)) . '"'; else echo 'placeholder="Order Date"'; ?> >
					<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
				</div>
			</div>

			<div class="form-group">
				<?php
				$record_user_id = 0;
				if (isset($purchaseModel->user_id) && !empty($purchaseModel->user_id)) $record_user_id = $purchaseModel->user_id;

				$supplier_order_statuses = array('Awaiting Payment','Preparing','Cannot Fulfil','Sent', 'Delivered', 'Paid');

				$applicable_order_statuses = array('Pending', 'Approved', 'Closed', 'Declined', 'Ordered - PO Not Sent To Supplier', 'Received', 'Submitted', 'Paid', 'Cancelled', 'Invoice Received', 'Ordered - PO Sent To Supplier', 'Ordered - PO Not Sent To Supplier');
				if (!isset($purchaseModel->order_status) || $purchaseModel->order_status == 'Pending' || $purchaseModel->order_status == 'More Info Needed')
				{
					if (isset($purchaseModel->order_status))
						$applicable_order_statuses = array($purchaseModel->order_status, 'Submitted');
					else $applicable_order_statuses = array('Pending', 'Submitted');
				}

				?>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label class="control-label">Name</label>
					<select class="form-control" name="user_id" id="user_id" disabled="disabled">
						<option value="<?php echo $purchaseModel->user_id; ?>" SELECTED="SELECTED">
							<?php echo $purchaseModel->user_name; ?>
						</option>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 valid">
					<label class="control-label">Order Status</label>
					<select class="form-control" name="order_status" id="order_status" disabled="disabled">
						<?php foreach ($applicable_order_statuses as $order_status) { ?>
							<option value="<?php echo $order_status; ?>"
								<?php if (isset($purchaseModel->order_status) && $purchaseModel->order_status == $order_status) echo ' SELECTED="SELECTED" '; ?>>
								<?php echo $order_status; ?>
							</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="clearfix"> </div>

			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<label class="control-label">Order Description/Notes</label>
					<textarea class="form-control" disabled="disabled" name="description" id="description" rows="4" <?php if (!isset($purchaseModel->description) || empty($purchaseModel->description)) echo 'placeholder="Order description/notes"'; ?>><?php if (isset($purchaseModel->description) && !empty($purchaseModel->description)) echo $purchaseModel->description; ?></textarea>
				</div>
			</div>
			<div class="clearfix"> <br /> </div>

			<?php
			$existing_items_found = false;
			if (isset($order_details) && is_array($order_details) && count($order_details))
				$existing_items_found = true;
			?>

			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<h3>
						Order Items
						<?php if (!isset($order['order_status']) || $order['order_status'] == 'Pending' || $order['order_status'] == 'More Info Needed') { ?>
							<?php if ($existing_items_found) { ?>
								<div class="pull-right" style="font-size: 70%; padding-top: 5px;">
									<a style="cursor: pointer; text-decoration: underline;"
									   onclick="$('#here_is_where_you_add_new_items_to_this_order').show();">
										Click here</a>
									To Add More Items To this Order
								</div>
							<?php } ?>
						<?php } ?>
					</h3>
				</div>
			</div>

			<?php
			//        echo "<pre>";
			//        print_r($order_details);
			//        echo "</pre>";die;
			$total_price = 0;
			$taxAmount = 0;
			$shippingAmount = 0;
			if (isset($orderDetails) && $orderDetails !=null)
			{
				?>

				<?php
				foreach ($orderDetails as $order_detail)
				{
					if (!isset($order_detail['quantity']) || empty($order_detail['quantity'])) $order_detail['quantity'] = 1;
					if (!isset($order_detail['unit_price']) || empty($order_detail['unit_price'])) $order_detail['unit_price'] = 0;
					?>

					<div class="form-group">
						<div class="col-md-2 col-sm-2 col-xs-4" style="display: none;">
							<label class="control-label">Product Type</label>
							<select class="form-control product_type" name="product_type[]">
								<option value="Bundle"
									<?php if (isset($order_detail['product_type']) && $order_detail['product_type'] == 'Bundle') echo ' selected="SELECTED" '; ?>>
									Bundle
								</option>
								<option value="Product"
									<?php if (isset($order_detail['product_type']) && $order_detail['product_type'] == 'Product') echo ' selected="SELECTED" '; ?>>
									Product
								</option>
							</select>
						</div>
						<?php //print_r($order_detail['product_name']);die; ?>
						<div class="col-md-3 col-sm-3 col-xs-6 date-input valid">
							<label class="control-label">Product Name <span style="color: #a94442;">*</span></label>
							<input required="true" type="text" class="form-control has-feedback-left price_calc" name="product_name[]" disabled="disabled"
								<?php if (isset($order_detail['product_name']) && !empty($order_detail['product_name'])) echo 'value="' . str_replace('"','',$order_detail['product_name']). '"'; ?> >
							<input class="product_id" type="hidden" name="product_id[]" value="<?php echo $order_detail['product_id']; ?>" />
							<span class="fa fa-shopping-basket form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6 date-input valid">
							<label class="control-label">Supplier Name <span style="color: #a94442;">*</span></label>
							<input required type="text" class="form-control has-feedback-left" name="vendor_name[]" id="vendor_name" disabled="disabled"
								<?php if (isset($order_detail['vendor']) && !empty($order_detail['vendor'])) echo 'value="' . str_replace('"','',$order_detail['vendor']). '"'; else echo 'placeholder="Supplier Name"'; ?> >
							<input type="hidden" name="vendor_id[]" class="vendor_id" value="<?php if (isset($order_detail['vendor']) && !empty($order_detail['vendor'])) echo $order_detail['vendor_id']; else echo '0'; ?>" />
							<span class="fa fa-user-circle-o form-control-feedback left" aria-hidden="true"></span>

						</div>
						<div class="clearfix"> <br /><br /> </div>

						<!--                  <div class="col-md-2 col-sm-2 col-xs-4 display_price informational_messages_area" style="padding: 8px; font-size: 120%;">-->
						<!--                      <label class="control-label">&nbsp;</label>-->
						<!--                      -->
						<!--                  </div>-->
						<div class="col-md-3 col-sm-3 col-xs-6">
							<label class="control-label">Unit Price
								<?php
								if (isset($order_detail['unit_price']) && !empty($order_detail['unit_price'])
									&& isset($order_detail['quantity']) && !empty($order_detail['quantity']))
								{
									echo '@ ';
									echo number_format($order_detail['unit_price'] / $order_detail['quantity'], 2);
									echo ' / Each';
								}
								?>
							</label>
							<input style="text-align: right;" type="text" class="unit_price form-control input_price_by_user" name="unit_price[]" readonly="readonly"
								<?php if (isset($order_detail['unit_price']) && !empty($order_detail['unit_price'])) echo 'value="' . $order_detail['unit_price'] . '"'; ?> >
							<input type="hidden" class="unit_price_hidden" name="unit_price_hidden[]"
								<?php if (isset($order_detail['unit_price']) && !empty($order_detail['unit_price']) && isset($order_detail['quantity']) && !empty($order_detail['quantity'])) echo 'value="' . round($order_detail['unit_price'] / $order_detail['quantity'], 2) . '"'; ?> >
						</div>
						<div class="col-md-1 col-sm-1 col-xs-2 date-input valid">
							<label class="control-label">Quantity <span style="color: #a94442;">*</span></label>
							<input required style="text-align: right;" type="text" class="product_quantity form-control price_calc" name="quantity[]" disabled="disabled"
								<?php if (isset($order_detail['quantity']) && !empty($order_detail['quantity'])) echo 'value="' . $order_detail['quantity'] . '"'; ?> >
						</div>
						<div class="col-md-1 col-sm-1 col-xs-2 date-input valid">
							<label class="control-label">Tax </label>
							<input style="text-align: right;" type="text" class="product_tax form-control input_tax_by_user" name="tax[]" disabled="disabled"
								<?php if (isset($order_detail['tax']) && !empty($order_detail['tax'])) echo 'value="' . $order_detail['tax'] . '"'; ?> >
						</div>
						<div class="col-md-1 col-sm-1 col-xs-2 date-input valid">
							<label class="control-label">Shipping</label>
							<input  style="text-align: right;" type="text" class="product_shipping form-control input_shipping_by_user" name="shipping_cost[]" disabled="disabled"
								<?php if (isset($order_detail['shipping_cost']) && !empty($order_detail['shipping_cost'])) echo 'value="' . $order_detail['shipping_cost'] . '"'; ?> >
						</div>


						<div class="col-md-2">
							<?php if (!isset($purchaseModel->order_status) || $purchaseModel->order_status == 'Pending' || $purchaseModel->order_status == 'More Info Needed') { ?>
								<a onclick="$(this).parent().parent().remove(); calculateTotalPrice(); return false;" class="btn btn-link">
									<span class="fa fa-remove hidden-xs"></span>
								</a>
							<?php } ?>
						</div>
					</div>

					<?php $total_price += $order_detail['calc_unit_price'];
					$shippingAmount += $order_detail['shipping_cost'];
					$taxAmount += $order_detail['calc_tax'];
					 } ?>

				<?php
				
			}

			
			$tax_rate = 20;
			if (isset($taxAmount))
			{
//                $tax_rate = $order['tax_rate'];
//                $tax_amount = ($order['tax_rate'] * $total_price) / 100.0;
//                $total_price += $tax_amount;
				$total_price += $taxAmount;

			}

			if (isset($shippingAmount))
				$total_price += $shippingAmount;
			?>


			<div class="clearfix"> <br /> </div>

			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-8">
					<h4 style="padding-top: 10px;">Tax Amount <span style="font-size: 70%;">(if applicable)</span></h4>
					</div>
				<div class="col-md-2 col-sm-2 col-xs-4">
					<label class="control-label">Tax Amount</label>
					<input style="text-align: right;" type="text" class="form-control total_tax_amount"
						   id="tax_amount" name="tax_rate" placeholder="Tax Amount" readonly="readonly"
						<?php if (isset($taxAmount) && $taxAmount) echo 'value="' . number_format($taxAmount, 2) . '"';  ?> />
				</div>
				<div class="clearfix"> </div>
			</div>

			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-8">

					<h4 style="padding-top: 10px;">Shipping Cost <span style="font-size: 70%;">(if applicable)</span></h4>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-4">
					<label class="control-label">Shipping Costs</label>
					<input readonly="readonly" style="text-align: right;" type="text" class="form-control total_shipping_amount price_calc" onkeyup="calculateTotalPrice();"
						   id="ship_amount" name="ship_amount" placeholder="Shipping Amount"
						<?php if (isset($shippingAmount) && $shippingAmount) echo 'value="' . number_format($shippingAmount, 2) . '"';  ?> />
				</div>
				<div class="clearfix"> </div>
			</div>

			<div class="form-group">
				<div class="col-md-4 col-sm-4 col-xs-8">

					<h4 style="padding-top: 10px;">Total Amount </h4>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-4">
					<label class="control-label">Total Amount</label>
					<input readonly="readonly" style="text-align: right;" type="text" class="form-control" 
						<?php if (isset($total_price) && $total_price) echo 'value="' . number_format($total_price, 2) . '"';  ?> />
				</div>
				<div class="clearfix"> </div>
			</div>



		<div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" name="order_id" id="order_id" value="<?php if (isset($order_id)) echo $order_id; ?>" />
            <input type="hidden" name="vendor_id" id="vendor_id" value="<?php if (isset($vendor_id)) echo $vendor_id; ?>" />
            <input type="hidden" name="po_id" id="po_id" value="<?php if (isset($po_id)) echo $po_id; ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
        </div>
        </div>

		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h3>
					For Supplier
				</h3>
			</div>
		</div>

		<div class="form-group">

			<div class="col-md-6 col-sm-6 col-xs-6 valid">
				<label class="control-label">Supplier Status</label>
				<select class="form-control" name="status" id="status">
					<?php foreach ($supplier_order_statuses as $supplier_status) { ?>
						<option value="<?php echo $supplier_status; ?>"
							<?php if (isset($orderVendorDetail->status) && $orderVendorDetail->status == $supplier_status) echo ' SELECTED="SELECTED" '; ?>>
							<?php echo $supplier_status; ?>
						</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="clearfix"> </div>

		<div class="form-group">

			<div class="col-md-6 col-sm-6 col-xs-6 valid">
				<label class="control-label">Tracking Number</label>
				<input type="text" class="form-control" name="tracking_number" id="tracking_number"
					<?php if (isset($orderVendorDetail->tracking_number) && !empty($orderVendorDetail->tracking_number)) { echo 'value="' . $orderVendorDetail->tracking_number . '"'; } else { echo 'placeholder="Tracking Number"'; } ?> >

			</div>

			<div class="col-md-6 col-sm-6 col-xs-6 valid">
				<label class="control-label">Courier Company</label>
				<input type="text" class="form-control" name="courier_company" id="courier_company"
					<?php if (isset($orderVendorDetail->courier_company) && !empty($orderVendorDetail->courier_company)) { echo 'value="' . $orderVendorDetail->courier_company . '"'; } else { echo 'placeholder="Courier Company"'; } ?> >

			</div>

		</div>

		<div class="form-group">
			<div class="col-md-12 col-sm-12 col-xs-12 col-xs-12">
				<label class="control-label">Additional Notes</label>
				<textarea class="form-control" name="note" id="note" rows="4" <?php if (!isset($orderVendorDetail->note) || empty($orderVendorDetail->note)) echo 'placeholder="Additional Notes for Supplier"'; ?>><?php if (isset($orderVendorDetail->note) && !empty($orderVendorDetail->note)) echo $orderVendorDetail->note; ?></textarea>
			</div>
		</div>

</div>
	  <div class="clearfix"> </div>
    
    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="document-tab">


		<?php
		// integer starts at 0 before counting
		$existing_invoices = array();
		$upload_dir = $path.'uploads/orders/invoices/';
		if ($order_id)
		{
			if (!is_dir($upload_dir)) mkdir($upload_dir);
			if (!is_dir($upload_dir . $order_id))
				mkdir($upload_dir . $order_id);
			$upload_dir = $upload_dir . $order_id . '/';
			if ($handle = opendir($upload_dir)) {
				while (($uploaded_invoice = readdir($handle)) !== false){
					if (!in_array($uploaded_invoice, array('.', '..')) && !is_dir($upload_dir . $uploaded_invoice))
						$existing_invoices[] = $uploaded_invoice;
				}
			}
		}
		?>

	
		

		      <div class="form-group">
		          <div class="col-md-6 col-sm-6 col-xs-6"><br />
		          	<h3>Invoice Documents </h3>
						<?php if (!isset($quote['submit_status']) || $quote['submit_status'] == 0) { ?>  <h4>              	
                          <div class="pull-right" style="font-size: 70%; padding-top: 5px;">
                              <a style="cursor: pointer; text-decoration: underline;"
                                    onclick="$('#here_is_where_you_add_documents_to_this_order').toggle();">
                                  Click here</a>
                              To Add Documents for this Order
                          </div></h4>
                        <?php } ?>
                    
		          </div>
		      </div>
		<?php if ($orderInvoice !=null) { ?>

	          <div class="col-md-6 col-sm-6 col-xs-6">
		      <table class="table">
		      <?php $file_idx = 1; foreach ($orderInvoice as $uploaded_file) {
		      	if($uploaded_file['type']=='vendor'){
		      		$showFileName = substr($uploaded_file['file_name'], strpos($uploaded_file['file_name'], "_") + 1);
		       ?>
		      	<tr id="existing_file_id_<?php echo $file_idx; ?>">
		      		<td>
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('orders/documentDownload',array('id'=>$uploaded_file['id'])); ?>" target="quote_file">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                        </a>
						<?php if (!isset($quote['submit_status']) || $quote['submit_status'] == 0) { ?>
	                        &nbsp;
							<a id="delete_order_file_link_<?php echo $file_idx; ?>" style="cursor: pointer; color: #fff;"
									onclick="deleteOrderFile(<?php echo $file_idx; ?>, '<?php echo $uploaded_file['file_name']; ?>');">
							   <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</a>
						<?php } ?>
		      		</td>
		      		<td>
		      			<?php echo $showFileName; ?>
		      		</td>
		      	</tr>
		      <?php $file_idx += 1;} } ?>
		      </table>
		      </div>

		      <div class="clearfix"> <br /> </div>

        <?php } ?>

        <div id="here_is_where_you_add_documents_to_this_order" style="display: none;">
		<?php if (!isset($purchaseModel->order_status) || $purchaseModel->order_status == 0) { ?>
		    <div class="form-group" id="document_area_1">
		    	<div class="col-md-5 col-sm-5 col-xs-5">
		    		<input class="form-control" type="file" name="order_file_1" id="quote_file_1" />
		    	</div>
		    	<div class="col-md-1 col-sm-1 col-xs-1">
		          	 <a onclick="deleteDocument(1);" title="Click here to delete this document" style="cursor: pointer;">
		          	 	<span class="fa fa-minus fa-2x"></span>
		          	 </a>
		          	 &nbsp;
		          	 <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
		          	 	<span class="fa fa-plus fa-2x"></span>
		          	 </a>
		    	</div>
		    </div>
			<input type="hidden" name="delete_document_flag_1" id="delete_document_flag_1" value="0" />
			<div class="clearfix" id="end_document_area_1"> <br /> </div>
        <?php } ?>
        <input type="hidden" name="total_documents" id="total_documents" value="1" />
	    </div>
    </div>
	<div class="clearfix"> <br /><hr /><br /> </div>
	
	<?php if (isset($quote['submit_status']) && $quote['submit_status'] == 1) { ?>
		<div class="checkbox">
			<div class="col-md-10 col-md-offset-1">
				<label for="submitted_already" style="font-weight: bold; font-size: 115%;">
					Since you have already submitted this order, you cannot make any changes to it.
				</label>
			</div>
		</div>
	    <div class="clearfix"> </div>
	<?php } else { ?>
	    <div class="clearfix"> </div>
	
		<div class="col-md-10">
			<a href="javascript:void(0);" onclick="$('#quote_form').submit();">
				<button type="button" class="btn btn-primary">
					Update Order
				</button>
			</a>
	    </div>
	<?php } ?>
    <div class="clearfix"> <br /> </div>
</div>
    
</form>
</div>
</div>
</div>

<div id="new_document_code" style="display: none;">
	<div class="form-group" id="document_area_DOCIDX">
		<div class="col-md-5 col-sm-5 col-xs-5">
		    <input class="form-control" type="file" name="order_file_DOCIDX" id="order_file_DOCIDX" />
		</div>
		<div class="col-md-1 col-sm-1 col-xs-1">
		    <a onclick="deleteDocument(DOCIDX);" title="Click here to delete this document" style="cursor: pointer;">
		        <span class="fa fa-minus fa-2x"></span>
		    </a>
		    <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer;">
		        <span class="fa fa-plus fa-2x"></span>
		    </a>
		</div>
	</div>
	<input type="hidden" name="delete_document_flag_DOCIDX" id="delete_document_flag_DOCIDX" value="0" />
	<div class="clearfix" id="end_document_area_DOCIDX"> <br /> </div>
</div>



<script type="text/javascript">


	function submitQuote()
	{
		if($("#submit_quote").prop('checked') == true){
			$('#quote_form').submit();
		} else {
			alert('Please check the check box above');
		}


	}

	function deleteOrderFile(file_idx, file_name)
	{
		$('#delete_order_file_link_' + file_idx).confirmation({
		  title: "Are you sure you want to delete the attached file?",
		  singleton: true,
		  placement: 'right',
		  popout: true,
		  onConfirm: function() {
		  	$('#existing_file_id_' + file_idx).remove();
	        $.ajax({
	            type: "POST",
	            url: "<?php echo AppUrl::bicesUrl('purchaseOrders/deleteVendorFile/'); ?>",
	            data: { quote_id: $('#quote_id').val(), vendor_id: $('#vendor_id').val(), file_name: file_name }
	        });
		  },
		  onCancel: function() {  }
		});
		
	}

	function addDocument()
	{
		var total_documents = $('#total_documents').val();
		total_documents = parseInt(total_documents);
		total_documents = total_documents + 1;
		$('#total_documents').val(total_documents);
		
		var new_document_html = $('#new_document_code').html();
		new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
		$('#total_documents').before(new_document_html);
	}

	function deleteDocument(docidx)
	{
		var display_document_count = 0;
	    $("div[id^='document_area']").each(function () {
	    	if ($(this).is(':visible')) display_document_count += 1;
		});

		if (display_document_count <= 1) alert("You must have at least one document field in the order form");
		else
		{
			if (confirm("Are you sure you want to delete this document?"))
			{
				$('#delete_document_flag_' + docidx).val(1);
				$('#document_area_' + docidx).hide();
				$('#end_document_area_' + docidx).hide();
			}
		}
	}

	
</script>
