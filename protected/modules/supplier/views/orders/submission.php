<?php $po_statuses = array('Pending'=> 'PO to be Created',
	'Approved'=> 'Approved',
	'Canceled'=> 'Cancelled',
	'Closed'=>  'Closed',
	'Submitted'=> 'Submitted',
	'Paid'=> 'Paid',
	'Invoice Received'=> 'Invoice Received',
	'Declined'=> 'Declined',
	'PO Sent To Supplier'=> 'Ordered - PO Sent To Supplier'
);
?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count">
        <div class="span6 pull-left">
            <h3>Purchase Orders</h3>
        </div>
        <div class="clearfix"> </div>
    </div>

	<form class="form-horizontal" id="po_list_form" name="po_list_form" role="form" method="post" action="<?php echo AppUrl::bicesUrl('purchaseOrders/list'); ?>">
		<div class="form-group">
          <div class="col-md-2">
              <input type="text" class="form-control" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date("d/m/Y", strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> >
          </div>
			
          <div class="col-md-2">
              <input type="text" class="form-control" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date("d/m/Y", strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> >
          </div>

			<div class="col-md-2 location">
				<select name="location_id[]" id="location_id"  class="form-control" multiple searchable="Search here.."  onchange="loadDepartments(0);">
					<?php
					$i = 0 ;
					foreach ($locations as $location) {
						if(isset($location_id[$i])){
							?>
							<option value="<?php echo $location->location_id; ?>"
								<?php if ($location_id[$i] == $location->location_id) echo ' selected="SELECTED" '; ?>>
								<?php echo $location->location_name; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $location->location_id; ?>">
								<?php echo $location->location_name; ?>
							</option>
						<?php } ?>
						<?php
						$i++;
					} ?>
				</select>
			</div>
			<div class="col-md-2 department">
				<select name="department_id[]" id="department_id" class="form-control" multiple searchable="Search here..">
					<option value="0">All Departments</option>
					 <?php if(!empty($departments))
                        foreach($departments as $dept_value){?>
                        <option value="<?php echo $dept_value->department_id;?>" <?php if (in_array($dept_value->department_id,$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value->department_name;?></option>
                    <?php } ?>
				</select>
			</div>
			<div class="col-md-2 status">
				<select name="po_status[]" id="po_status" multiple class="form-control">
					<?php
					$i = 0 ;
					foreach ($po_statuses as $index=>$po_status_display) {
						if(isset($po_status[$i])){
							?>
							<option value="<?php echo $index; ?>"
								<?php if ($po_status[$i] == $po_status_display) echo ' selected="SELECTED" '; ?>>
								<?php echo $po_status_display; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $index;?>">
								<?php echo $po_status_display; ?>
							</option>
						<?php }
						$i++;
					} ?>
				</select>
			</div>

			<div class="col-md-2">
				<button class="btn btn-primary" onclick="$('#po_list_form').submit();">Search Purchase Orders</button>
			</div>
		</div>
	</form>
<div class="clearfix"> </div>
<div class="row tile_count"> </div>

    <table id="order_table" class="table table-striped table-bordered" style="width:100%">
      <thead>
        <tr>
          <th> </th>
          <th>Date</th>
          <th>Order ID</th>
          <th>Total Amount</th>
          <th>Tax Amount</th>
          <th>Shipping Amount</th>
          <th>Supplier Status</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
          <?php foreach ($purchaseModel as $value) { 

          	$sql = "select sum(calc_tax) as total_tax,sum(shipping_cost) as total_shipping from order_details where order_id=".$value['order_id'];
            $detailOrder = Yii::app()->db->createCommand($sql)->queryRow();

           ?>
              <tr>
                    <td>
                        <a href="<?php echo Yii::app()->createUrl('orders/submissionView',array('po_id'=>$value->po_id)); ?>">
                            <button class="btn btn-sm btn-success">View</button>
                        </a>
                    </td>
                    <td><?php echo date("F j, Y", strtotime($value->po_sent_date)); ?></td>
                    <td style="text-align: right;"><?php echo '#' . $value->order_id; ?></td>
                    <td style="text-align: right;"><?php 
                    $po_total = $value->po_total;

                    echo FunctionManager::showCurrencySymbol('',$value->currency_id).$po_total;
                    ?></td>
                    <td style="text-align: right;"><?php 
                    echo FunctionManager::showCurrencySymbol('',$value->currency_id).$detailOrder['total_tax'];
                    ?></td>
                    <td style="text-align: right;"><?php 
                    echo FunctionManager::showCurrencySymbol('',$value->currency_id).$detailOrder['total_shipping'];
                    ?></td>
                    
                    <td>
					  <?php $order_vendor_detail = OrderVendorDetails::model()->findByAttributes(array('po_id' =>  $value->po_id),array('order'=>'id DESC'));
					  
					  if(!empty($order_vendor_detail->status)){

						  if ($order_vendor_detail->status == 'Cannot Fulfil')
							  $vendor_status_style = 'danger';
						  else
							  $vendor_status_style = 'success'

						  ?>
						  <button class="btn btn-sm btn-<?php echo $vendor_status_style; ?> status">
							  <?php echo $order_vendor_detail->status; ?>
						  </button>
					  <?php } else { ?>
						  N/A
					  <?php } ?>
				  </td>
                    <td>
                    	<?php

	            		if (empty($value->po_status)) $value->po_status = 'Pending';
	            		$status_style = "";

	            		$poStatus = $value->po_status;
						
						if ($poStatus == 'Paid' || $poStatus == 'Closed' || $poStatus == 'Received')
							$status_style = ' style="border: 1px solid green !important; background-color: green !important; color: #fff !important;" ';
						if ($poStatus == 'Declined')
							$status_style = ' style="border: 1px solid #990000 !important; background-color: #990000 !important; color: #fff !important;" ';								
						if ($poStatus == 'More Info Needed')
							$status_style = ' style="border: 1px solid #CC3333 !important; background-color: #CC3333 !important; color: #fff !important;" ';
						if ($poStatus == 'Pending' || $poStatus == 'Ordered - PO Not Sent To Supplier' 
									|| $poStatus== 'Submitted' || $poStatus == 'Ordered - PO Sent To Supplier')
							$status_style = ' style="border: 1px solid orange !important; background-color: orange !important; color: #fff !important;" ';
						if($poStatus == 'Pending') {
							$toolTip = "PO has been created however not submitted.";
						}
						elseif($poStatus == 'Purchased'){
							$toolTip = "PO has been Purchased.";
						}
						elseif($poStatus == 'More Info Needed'){
							$toolTip = "Approver has requested more information from the order creator.";
						}
						elseif($poStatus == 'Declined'){
							$toolTip = "Approver has declined the order. Please read approver notes.";
						}
						elseif($poStatus == 'Approved'){
							$toolTip = "Order has been approved and awaiting Purchase Order creation.";
						}
						elseif($poStatus == 'PO Not Sent To Supplier'){
							$toolTip = "PO created but not sent to supplier.";
						}
						elseif($poStatus == 'PO Sent To Supplier'){
							$toolTip = "PO created and sent to supplier.";
						}
						elseif($poStatus == 'Invoice Received'){
							$toolTip = "Invoice has been received by supplier and awaiting payment.";
						}
						elseif($poStatus == 'Paid'){
							$toolTip = "PO has been paid .";
						}
						elseif($poStatus == 'Submitted'){
							$toolTip = "PO has been submitted.";
						}
						elseif($poStatus == 'Received'){
							$toolTip = "PO has been received.";
						}
						elseif($poStatus == 'Cancelled'){
							$toolTip = "PO has been cancelled.";
						}
						elseif($poStatus == 'Closed'){
							$toolTip = "PO has been closed.";
						}
						else{
							$toolTip = 'N/A';
						}
						?>
                    	<button class="btn btn-sm btn-success status" title="<?php echo $toolTip; ?>" <?php echo $status_style; ?>>
                    		<?php
							if($poStatus=='Pending')
							echo 'PO to be Created';
							else
							echo $poStatus;
							?>
                    	</button>
                    </td>
              </tr>

          <?php } ?>

      </tbody>

  </table>

</div>
<style>
	.ui-tooltip {
		width: 200px;
		text-align: center;
		box-shadow: none;
		padding: 0;
	}
	.ui-tooltip-content {
		position: relative;
		padding: 0.5em;
	}
	.ui-tooltip-content::after, .ui-tooltip-content::before {
		content: "";
		position: absolute;
		border-style: solid;
		display: block;
		left: 90px;
	}
	.bottom .ui-tooltip-content::before {
		bottom: -10px;
		border-color: #AAA transparent;
		border-width: 10px 10px 0;
	}
	.bottom .ui-tooltip-content::after {
		bottom: -7px;
		border-color: white transparent;
		border-width: 10px 10px 0;
	}
	.top .ui-tooltip-content::before {
		top: -10px;
		border-color: #AAA transparent;
		border-width: 0 10px 10px;
	}
	.top .ui-tooltip-content::after {
		top: -7px;
		border-color: white transparent;
		border-width: 0 10px 10px;
	}

	.location  .multiselect {
		width: 138%;
	}
	.status  .multiselect {
		width: 100%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}

	.btn .caret {
		float: right;
		margin-top: 10px;

	}
    .department .dropdown-toggle{
        min-width: 112%;
    }
</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions2(isFilter) {
        return {
            enableFiltering: isFilter,
            enableCaseInsensitiveFiltering: isFilter,
            filterPlaceholder: 'Search ...',
            nonSelectedText: 'All Departments',
            numberDisplayed: 1,
            maxHeight: 400,
        }
    }

	function getOptions1(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Status Values',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	$('#location_id').multiselect(getOptions(true));
	$('#department_id').multiselect(getOptions2(true));
	$('#po_status').multiselect(getOptions1(true));

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );

$.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
	locale: {
	    format: 'DD/MM/YYYY'
	}
  });
};

$(document).ready( function() {
    $('#order_table').dataTable({
        "columnDefs": [ 
        	{ "targets": 0, "width": "6%", "orderable": false },
			{ "type": "sort-month-year", targets: 1 }         
        ],
        "order": []
    });

	$('#from_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$('#to_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$( ".status" ).tooltip({
		position: {
			my: "center bottom",
			at: "center top-10",
			collision: "flip",
			using: function( position, feedback ) {
				$( this ).addClass( feedback.vertical )
					.css( position );
			}
		}
	});

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			//loadDepartments(<?php //echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>
	
});
</script>
