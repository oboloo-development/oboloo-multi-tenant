
<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel tile overflow_hidden">
      <div class="x_title">
        <h2 style="color: #2d9ca2;">Sourcing Activity Selection Criteria (%)</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content"><div id="scoring_pie_chart"></div><br /></div>
    </div></div>
<script type="text/javascript">
   var options = {
    chart: {
      fontFamily: 'Poppins !important',
      height: 260,
      /*width: 210,*/
      type: 'pie',
      id:"scoringPieChart"

    },
   // legend: {show: false},
    dataLabels: {enabled: false},
    labels: [<?php echo '"' . implode('", "', $scoringCriteria['pie_score_label']). '"';?>],
    series: [<?php echo implode(",",$scoringCriteria['pie_score_series']);?>],
    responsive: [{
        breakpoint: 380,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }],
    
}
var chart = new ApexCharts(
    document.querySelector("#scoring_pie_chart"),
    options
);
 chart.render();
</script>
    