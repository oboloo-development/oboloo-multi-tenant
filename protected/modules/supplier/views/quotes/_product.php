
  <h3 style="padding: 10px;" class="pull-left">Products</h3>

  <div class="form-group pull-right">

    <div class="col-md-6 col-sm-6 col-xs-6  pull-right">
      <input type="text" class="form-control has-feedback-left" name="quote_currency" id="quote_currency" disabled="disabled"
      <?php if (isset($quote['quote_currency']) && !empty($quote['quote_currency'])) echo 'value="' . $quote['quote_currency'] . '"'; else echo 'placeholder="Quote Currency"'; ?> style="color:#2d9ca2">
       <span class="fa fa-money form-control-feedback left notranslate" aria-hidden="true" style="color:#2d9ca2"></span>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6  pull-right">
      <label class="control-label  pull-right" style="color:#2d9ca2">Quote Currency</label>
      
      </div>
    </div><div class="clearfix"></div>
     <h5 style="color: #000; padding: 10px;">Please fill in pricing for the below goods or services including shipping, tax and any other charges. This will automatically be calculated against the product quantity.</h5>
    <div class="clearfix"></div><br /><br />


      <?php
        $existing_items_found = false;
        if (isset($quote_details) && is_array($quote_details) && count($quote_details))
            $existing_items_found = true;
      ?>

      <?php
        $i=1;
        $countNumer = 1;
        $total_price = 0;
        if (isset($quote_details) && is_array($quote_details) && count($quote_details))
        {
      ?>

          <?php

            foreach ($quote_details as $quote_detail)
            {?>
            <table class="table table-striped table-bordered" style="width: 100%">
            <thead>
              <tr>
                <th style="width: 30px;"></th>
                <th style="width: 400px;">Product Name</th>
                <th style="width: 400px;">Product Code</th>
                <th>Unit of Measure</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>
            <?php
                if (!isset($quote_detail->quantity) || empty($quote_detail->quantity)) $quote_detail['quantity'] = 1;
                if (!isset($quote_detail->unit_price) || empty($quote_detail->unit_price)) $quote_detail->unit_price = 0;

              $criteria=new CDbCriteria;
              $criteria->select = "t.unit_price,t.tax_rate as product_tax_rate,t.shipping as product_shipping,t.other_charges as product_other_charges, t.vendor_product_notes";
              $criteria->condition="t.quote_id=".$quote_id." and t.vendor_id=".$vendor_id." and product_name like '%".addslashes($quote_detail->product_name)."%'";
              $criteria->group="t.id";
              $quoteVendorDetailModel = QuoteVendorDetails::model()->find($criteria);
          ?>
         <!--  <tr><td colspan="6"></td></tr><tr><td colspan="6"></td></tr> -->

           <tr>
            <td class="counter"><span><?php echo $countNumer++; ?> .</span></td>
            <td style="width: 400px;"><div class="date-input">
                  <textarea class="form-control notranslate" name="product_name[]" readonly="readonly"><?php if (isset($quote_detail->product_name) && !empty($quote_detail->product_name)) echo $quote_detail->product_name;?></textarea>
                 
                  </div>
              </td>
            <td style="width: 400px;"><div class="date-input">
                
                <input type="text" class="form-control notranslate" name="product_code[]" readonly="readonly" <?php if (isset($quote_detail->product_code) && !empty($quote_detail->product_name)) echo 'value="' . $quote_detail->product_code . '"'; ?>style="width: 400px;" >
               
                  </div></td>

            <td>
              <input style="text-align: right;" type="text" class="form-control notranslate" name="uom[]" readonly="readonly" <?php if (isset($quote_detail->uom) && !empty($quote_detail->uom)) echo 'value="' . $quote_detail->uom . '"'; else echo 'Units'; ?> >
            </td>
            <td>
              <input style="text-align: right;" type="text" class="qty form-control notranslate" name="quantity[]" readonly="readonly" <?php if (isset($quote_detail->quantity) && !empty($quote_detail->quantity)) echo 'value="' . $quote_detail->quantity . '"'; ?> ></td>
            
               
          
          </tr>
          <?php
          if ($quoteVendorDetailModel !=null && !empty($quoteVendorDetailModel->unit_price)){

              $total_unit_price = $quoteVendorDetailModel->unit_price*$quote_detail->quantity;
              $product_tax_rate_price = ($quoteVendorDetailModel->product_tax_rate/100)*$total_unit_price;

              $product_total_price = $total_unit_price+$product_tax_rate_price+$quoteVendorDetailModel->product_shipping+$quoteVendorDetailModel->product_other_charges;
          }
          else if (isset($quote_detail->unit_price) && !empty($quote_detail->unit_price)){

              $total_unit_price = $quote_detail->unit_price*$quote_detail->quantity;
              $product_tax_rate_price = ($quote_detail->product_tax_rate/100)*$total_unit_price;

              $product_total_price = $total_unit_price+$product_tax_rate_price+$quote_detail->product_shipping+$quote_detail->product_other_charges;
          }

          ?>
          </tbody>
          </table>
          <div class="clearfix"></div>
          <table class="table table-striped table-bordered" style="width: 100%; margin-bottom: 0px;">
           <thead>
            <tr>
            <th style="width: 500px;"><b>Product Notes</b></th>
            <th style="width: 200px;"><b>Unit Price</b></th>
            <th style="width: 200px;"><b>Tax Rate %</b></th>
            <th style="width: 200px;"><b>Shipping</b></th>
            <th style="width: 200px;"><b>Other Charges</b></th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <td style="width: 500px;"><input type="text" class="form-control notranslate" name="vendor_product_notes[]" placeholder="Product Notes" <?php if ($quoteVendorDetailModel !=null && !empty($quoteVendorDetailModel->vendor_product_notes)) echo 'value="' . $quoteVendorDetailModel->vendor_product_notes . '"'; ?> style="width: 500px;"></td>
            <td style="width:200px;"><input onblur="calculateProductPrice(<?php echo $i; ?>)" onkeyup="calculateProductPrice(<?php echo $i; ?>)" type="number"   step="0.01" class="price_calc product_unit_price form-control notranslate unit_<?php echo $i; ?>" id="unit_price_<?php echo $i; ?>" data-value="<?php echo $quote_detail['quantity']; ?>" name="unit_price[]" placeholder="Price Per Unit"
                            <?php if ($quoteVendorDetailModel !=null && !empty($quoteVendorDetailModel->unit_price)) echo 'value="' . $quoteVendorDetailModel->unit_price . '"'; ?>  style="width: 200px;" ></td>
            <td style="width: 200px;"><input onblur="calculateProductPrice(<?php echo $i; ?>)" onkeyup="calculateProductPrice(<?php echo $i; ?>)" type="number"   step="0.01"  class="tax_calc product_tax_rate form-control notranslate unit_<?php echo $i; ?>" id="tax_rate_<?php echo $i; ?>" data-value="<?php echo $quote_detail['quantity']; ?>" data-id="<?php echo $i; ?>" name="product_tax_rate[]"  placeholder="Tax Rate %"
                            <?php if ($quoteVendorDetailModel !=null && !empty($quoteVendorDetailModel->product_tax_rate)) echo 'value="' . $quoteVendorDetailModel->product_tax_rate . '"'; ?> style="width: 200px;"></td>
            <td style="width: 200px;"> <input onblur="calculateProductPrice(<?php echo $i; ?>)" onkeyup="calculateProductPrice(<?php echo $i; ?>)" type="number"  step="0.01" class="shipping_calc product_shipping_charges form-control notranslate unit_<?php echo $i; ?>" id="shipping_charges_<?php echo $i; ?>" name="product_shipping_charges[]" placeholder="Shipping Charges"
                            <?php if ($quoteVendorDetailModel !=null && !empty($quoteVendorDetailModel->product_shipping)) echo 'value="' . $quoteVendorDetailModel->product_shipping . '"'; ?> style="width: 200px;"></td>
            <td style="width: 200px;"> <input onblur="calculateProductPrice(<?php echo $i; ?>)" onkeyup="calculateProductPrice(<?php echo $i; ?>)"  type="number"  step="0.01" class="other_calc product_other_charges form-control notranslate unit_<?php echo $i; ?>" id="other_charges_<?php echo $i; ?>" name="product_other_charges[]" placeholder="Other Charges"
                            <?php if ($quoteVendorDetailModel !=null && !empty($quoteVendorDetailModel->product_other_charges)) echo 'value="' . $quoteVendorDetailModel->product_other_charges . '"'; ?> style="width: 200px;"></td>
            
          </tr>
          </tbody>
          </table>
          <div class="col-xs-8"></div>
          <div class="col-xs-4" style="padding-right: 0px;">
             <table class="table table-striped table-bordered" style="width: 100%; margin-bottom: 0px;">
           <thead>
          <tr>
             <th><b>Product Total Price</b></th> 
          </tr>
           </thead>
          <tbody>
            <tr>
            <td> <input style="text-align: right;" type="number"  step="0.01"  class="form-control notranslate product_total_price_<?php echo $i; ?>"  id="product_total_price_<?php echo $i; ?>" name="product_total_price[]" placeholder="Product Total Price" <?php if (isset($product_total_price) && !empty($product_total_price)) echo 'value="' . $product_total_price . '"'; ?> readonly></td> 
          </tr>
          
            <div class="clearfix"> <br /> </div>
          </tbody>
          </table><br /><br />
          </div>
         
          <?php $i++;
                $total_price += $quoteVendorDetailModel !=null?$quoteVendorDetailModel->unit_price:$quote_detail['unit_price'];
            } ?>


      <?php
            }
      ?>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="hidden" class="notranslate" name="quote_id" id="quote_id" value="<?php if (isset($quote_id)) echo $quote_id; ?>" />
            <input type="hidden" class="notranslate" name="vendor_id" id="vendor_id" value="<?php if (isset($vendor_id)) echo $vendor_id; ?>" />
            <input type="hidden" class="notranslate" name="quote_vendor_id" id="quote_vendor_id" value="<?php if (isset($quote_vendor_id)) echo $quote_vendor_id; ?>" />
            <input type="hidden" name="form_submitted" id="form_submitted" value="1" />
        </div>
    </div>

    <table class="table table-striped table-bordered pull-right" style="width: 40%">
          <tbody>
            <tr>
              <th>Tax Amount</th><td><input type="number"  step="0.01"  class="price_calc form-control notranslate text-right" name="tax_rate" id="total_tax_rate"
                <?php if (isset($quote['tax_rate']) && !empty($quote['tax_rate'])) echo 'value="' . $quote['tax_rate'] . '"'; else echo 'placeholder="Tax Amount"'; ?> readonly></td></tr>

                <tr>
              <th>Shipping Charges</th><td><input type="number"  step="0.01"  class="price_calc form-control notranslate text-right" name="shipping" id="total_shipping"
                <?php if (isset($quote['shipping']) && !empty($quote['shipping'])) echo 'value="' . $quote['shipping'] . '"'; else echo 'placeholder="Shipping"'; ?> readonly></td></tr>
              <tr>
              <th>Other Charges</th><td><input type="number"  step="0.01"  class="price_calc form-control notranslate text-right" name="other_charges" id="total_other_charges"
                <?php if (isset($quote['other_charges']) && !empty($quote['other_charges'])) echo 'value="' . $quote['other_charges'] . '"'; else echo 'placeholder="Other Charges"'; ?> readonly></td></tr>

              <tr>
              <th>Total Price</th><td><input type="number"  step="0.01"  class="form-control notranslate text-right" name="total_price" id="total_price" readonly="readonly"
                <?php if (isset($quote['total_price']) && !empty($quote['total_price'])) echo 'value="' . $quote['total_price'] . '"'; else echo 'placeholder="Total Price"'; ?> readonly></td></tr>

          </tbody>
        </table><div class="clearfix"></div>

 <style type="text/css">
   .counter{
    width: 41px;
    font-size: 14px;
    font-weight: 600;
    padding-top: 15px !important;}
 </style>