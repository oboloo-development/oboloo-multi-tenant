<?php $quote_statuses = array('Pending', 'Submitted', 'Closed', 'Cancelled'); ?>
<div class="right_col" role="main">

    <div class="row-fluid tile_count"><br /><br />
        <div class="span6 pull-left">
            <h3>Quotes</h3>
        </div>
        <div class="clearfix"> </div>
    </div>

	<form class="form-horizontal" id="quote_list_form" name="quote_list_form" role="form" method="post" action="">
		<div class="form-group">
          <div class="col-md-2 col-sm-2 col-xs-6 ">
              <input type="text" class="form-control border-select" name="from_date" id="from_date"
                    <?php if (isset($from_date) && !empty($from_date)) echo 'value="' . date("d/m/Y", strtotime($from_date)) . '"'; else echo 'placeholder="From Date"'; ?> autocomplete="off" >
          </div>
			
          <div class="col-md-2 col-sm-2 col-xs-6 ">
              <input type="text" class="form-control border-select" name="to_date" id="to_date"
                    <?php if (isset($to_date) && !empty($to_date)) echo 'value="' . date("d/m/Y", strtotime($to_date)) . '"'; else echo 'placeholder="To Date"'; ?> autocomplete="off" >
          </div>

			<div class="col-md-2 col-sm-2 col-xs-6  col-md-offset-2 col-sm-offset-2 location">
				<select name="location_id[]" id="location_id"  class="form-control" multiple searchable="Search here.."  onchange="loadDepartments(0);">
					<?php
					$i = 0 ;
					foreach ($locations as $location) {
						if(isset($location_id[$i])){
							?>
							<option value="<?php echo $location['location_id']; ?>"
								<?php if ($location_id[$i] == $location['location_id']) echo ' selected="SELECTED" '; ?>>
								<?php echo $location['location_name']; ?>
							</option>
						<?php } else { ?>
							<option value="<?php echo $location['location_id']; ?>">
								<?php echo $location['location_name']; ?>
							</option>
						<?php } ?>
						<?php
						$i++;
					} ?>
				</select>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-6 department">
				<select name="department_id[]" id="department_id" class="form-control" multiple searchable="Search here..">
					<option value="0">All Departments</option>
					<?php if(!empty($department_info))
						foreach($department_info as $dept_value){?>
						<option value="<?php echo $dept_value['department_id'];?>" <?php if (in_array($dept_value['department_id'],$department_id)) echo ' selected="SELECTED" '; ?>><?php echo $dept_value['department_name'];?></option>
					<?php } ?>
					
				</select>
			</div>
			
			<div class="col-md-2 text-right-not-xs">
				<button class="btn btn-primary" onclick="$('#quote_list_form').submit();">Search Quotes</button>
			</div>
		</div>
	</form>
<div class="clearfix"> </div>
<div class="row tile_count"> </div>

<table id="quote_table" class="table table-striped table-bordered" style="width:100%">
      <thead>
        <tr>
          <th> </th>
          <th>Quote ID</th>
          <th>Quote Name</th>
          <th>Opening Date</th>
          <th>Closing Date</th>
        </tr>
      </thead>

      <tbody>

          <?php foreach ($quoteModel as $quote) { ?>
              <tr>
                    <td>
                        <a href="<?php echo Yii::app()->createUrl('quotes/view',array('quote_id'=>$quote->quote_id)); ?>"><button class="btn btn-sm btn-success">View/Edit</button>
                        </a>
                    </td>
                    <td style="text-align: right;"><?php echo $quote->quote_id; ?></td>
                    <td><?php echo $quote->quote_name; ?></td>
                    <td><?php echo date("F j, Y H:iA", strtotime($quote->opening_date)); ?></td>
                    <td><?php echo date("F j, Y H:iA", strtotime($quote->closing_date)); ?></td>
                   
              </tr>

          <?php } ?>

      </tbody>

  </table>


</div>

<style>
	.location  .multiselect {
		width: 138%;
	}
	.status  .multiselect {
		width: 100%;
	}
	.multiselect-selected-text{
		float: left;
		margin-left: 0px;
	}

	.btn .caret {
		float: right;
		margin-top: 10px;

	}
    .department .dropdown-toggle{
        min-width: 112%;
    }
</style>
<link rel="stylesheet" href="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css">
<script src="https://rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">

	function getOptions(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Locations',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions2(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Departments',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}

	function getOptions1(isFilter) {
		return {
			enableFiltering: isFilter,
			enableCaseInsensitiveFiltering: isFilter,
			filterPlaceholder: 'Search ...',
			nonSelectedText: 'All Status Values',
			numberDisplayed: 1,
			maxHeight: 400,
		}
	}
	$('#location_id').multiselect(getOptions(true));
	$('#department_id').multiselect(getOptions2(true));
	$('#quote_status').multiselect(getOptions1(true));


jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "sort-month-year-pre": function ( s ) {
        return Date.parse(s);
    },
 
    "sort-month-year-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "sort-month-year-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ?  -1 : 0));
    }
} );


$.fn.singleDatePicker = function() {
  $(this).on("apply.daterangepicker", function(e, picker) {
    picker.element.val(picker.startDate.format('DD/MM/YYYY'));
  });
  return $(this).daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: false,
	locale: {
	    format: 'DD/MM/YYYY'
	}
  });
};

$(document).ready( function() {
    $('#quote_table').dataTable({
        "columnDefs": [ 
        	{ "targets": 0, "width": "6%", "orderable": false },
			{ "type": "sort-month-year", targets: 3 },         
			{ "type": "sort-month-year", targets: 4 }         
        ],
        "order": []
    });

	$('#from_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	$('#to_date').singleDatePicker({
	  singleDatePicker: true,
	  singleClasses: "picker_3",
	  locale: {
		format: 'DD/MM/YYYY'
	  }
	});

	<?php if (isset($location_id) && !empty($location_id)) { ?>

		<?php if (isset($department_id) && !empty($department_id)) { ?>
			  //loadDepartments(<?php //echo $department_id; ?>);
		<?php } else { ?>
			loadDepartments(0);
		<?php } ?>

	<?php } ?>
		
});

</script>
