<?php $path = Yii::getPathOfAlias('webroot').'/';
  $sql = "SELECT * FROM company order by id desc";
  $clientCompany = Yii::app()->db->createCommand($sql)->queryRow();
  $dateFormate = FunctionManager::dateFormat();
?>

<div class="" role="main" style="width: 94%;margin: auto;">
  <div class="row-fluid tile_count col-md-12">
    <div class="span6 pull-left">
      <h4 class="mb-20">
        <?php if(!empty($clientCompany['company_name'])){?>
            <strong class="notranslate">Client Name</strong>: <span class="title-text notranslate"> <?= $clientCompany['company_name']; ?></span>
        <?php } ?>
      </h4>
      <h4 class="mb-20">
        <?php if(!empty($quote['quote_name'])){?>
         <strong class="notranslate">Quote Name</strong>: <span class="title-text notranslate"> <?= $quote['quote_name'] ?></span>
        <?php } ?>
      </h4>
      <h4 class="mb-20">
        <?php if (isset($quote['opening_date']) && !empty($quote['opening_date'])){?>
         <strong class="notranslate">Opening Date & Time (UTC)</strong>: <span class="title-text notranslate"> <?= date($dateFormate . " H:iA", strtotime($quote['opening_date'])) ?></span>
        <?php } ?>
      </h4>
      <h4 class="mb-20">
        <?php if (isset($quote['closing_date']) && !empty($quote['closing_date'])){?>
         <strong class="notranslate">Closing Date & Time (UTC)</strong>: <span class="title-text notranslate"> <?= date($dateFormate . " H:iA", strtotime($quote['closing_date'])) ?></span>
        <?php } ?>
      </h4>
    </div>
     
    <div class="clearfix"> </div>
    <?php if(Yii::app()->user->hasFlash('error')): ?>
    <div class="alert alert-danger" role="alert">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php elseif (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
  <?php endif; ?>

<div class="row tile_count" role="tabpanel" data-example-id="togglable-tabs" >
  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" style="background: none !important;">
    <li role="presentation" class="active">
        <a class="has-feedback-left pull-right" href="#tab_content1" id="quote-tab" role="tab" data-toggle="tab" aria-expanded="true">
            Quote details
        </a>
    </li>
    <?php if(!empty($pie_score_label)){?>
     <li role="presentation" class="" >
            <a href="#tab_content4" role="tab" id="supplier-evaluation" data-toggle="tab" aria-expanded="false">
                Scoring Criteria
            </a>
    </li>
  <?php } ?>
  <?php if (!empty($quoteQuestionnaireRender) && count($quoteQuestionnaireRender) > 0){?>
    <li role="presentation" class="">
        <a href="#tab_content3" role="tab" id="question-tab" data-toggle="tab" aria-expanded="false">
            Questionnaire
        </a>
    </li>
  <?php } ?>
    <li role="presentation" class="">
        <a class="has-feedback-left pull-right" href="#tab_content5" id="product-tab" role="tab" data-toggle="tab" aria-expanded="true">
                Products to Quote
        </a>
    </li>

    <li role="presentation" class="">
        <a href="#tab_content2" role="tab" id="document-tab" data-toggle="tab" aria-expanded="false">
            Documents
        </a>
    </li>
    
   
  </ul>
  <div class="clearfix"> </div>

  <form id="quote_form" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" action="">

  <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="quote-tab">
      <h3 style="padding: 10px;">Quote Information<br /></h3>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">Quote Name</label>
              <input type="text" class="form-control has-feedback-left notranslate" name="quote_name" id="quote_name" disabled="disabled" <?php if (isset($quote['quote_name']) && !empty($quote['quote_name'])) echo 'value="' . $quote['quote_name'] . '"'; else echo 'placeholder="Quote Name"'; ?> >
              <span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
          </div>
      </div>
       <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6">
              <label class="control-label">Description/Notes</label>
              <textarea class="form-control notranslate" placeholder="Notes/comments for the quote" name="notes" id="notes" disabled="disabled"  style="height: 200px;"><?php if (isset($quote['quote_desc']) && !empty($quote['quote_desc'])) echo $quote['quote_desc']; ?></textarea>
          </div>
      </div>

      <div class="form-group">
          <!-- <div class="col-md-6 col-sm-6 col-xs-6 date-input">
              <label class="control-label">First Owner</label>
              <input type="text" class="form-control has-feedback-left notranslate" name="full_name" id="full_name" disabled="disabled"
                    <?php if (isset($quote['created_by_name']) && !empty($quote['created_by_name'])) echo 'value="' . $quote['created_by_name'] . '"'; else echo 'placeholder="Quote Created By"'; ?> >
              <span class="fa fa-user form-control-feedback left notranslate" aria-hidden="true"></span>
          </div><div class="clearfix"></div> -->

          <div class="col-md-3 col-sm-3 col-xs-3  date-input">
              <label class="control-label">Contact Phone</label>
              <input type="text" class="form-control has-feedback-left notranslate" name="contact_phone" id="contact_phone" disabled="disabled"
                    <?php if (isset($quote['contact_phone2']) && !empty($quote['contact_phone2'])) echo 'value="' . $quote['contact_phone2'] . '"'; else echo 'placeholder="Contact Phone"'; ?> >
              <span class="fa fa-phone form-control-feedback left notranslate" aria-hidden="true"></span>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-3  date-input">
              <label class="control-label">Contact Email</label>
              <input type="text" class="form-control has-feedback-left notranslate" name="contact_email2" id="contact_email2" disabled="disabled"
                    <?php if (isset($quote['contact_email2']) && !empty($quote['contact_email2'])) echo 'value="' . $quote['contact_email2'] . '"'; else echo 'placeholder="Contact Email"'; ?> >
              <span class="fa fa-envelope form-control-feedback left notranslate" aria-hidden="true"></span>
          </div>
      </div>
      <div class="form-group">
      <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Location </label>
        <input type="text" required name="location_id" id="location_id" class="form-control notranslate" disabled="disabled" value="<?php echo $location !=null?$location->location_name:'';?>" />
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 valid">
              <label class="control-label">Department</label>
              <input type="text" required name="department_id" id="department_id" class="form-control notranslate"  value="<?php echo $department !=null?$department->department_name:'';?>" disabled="disabled" />
          </div>
      </div>

       <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Address Line 1</label>
              <input type="text" class="form-control has-feedback-left notranslate" name="loc_label" id="loc_label"  disabled="disabled" value="<?php echo $location !=null?$location->loc_label:'';?>">
                
              <span class="fa fa-sticky-note-o form-control-feedback left notranslate" aria-hidden="true"></span>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Address Line 2 <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control has-feedback-left notranslate" name="loc_address" id="loc_address" value="<?php echo $location !=null?$location->loc_address:'';?>" disabled="disabled">
              <span class="fa fa-address-card-o form-control-feedback left notranslate" aria-hidden="true"></span>
          </div>
      </div>


      <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">City <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="loc_city" id="loc_city" value="<?php echo $location !=null?$location->loc_city:'';?>" disabled="disabled" >
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">State/County <span style="color: #a94442;"></span></label>
              <input type="text" class="form-control notranslate" name="loc_state" id="loc_state" value="<?php echo $location !=null?$location->loc_state:'';?>" disabled="disabled" >
          </div>
      </div>

       <div class="form-group">
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label">Country</label>
              <input type="text" class="form-control" name="loc_country" id="loc_country" value="<?php 
               echo $location !=null?$location->loc_country:'';?>" disabled="disabled">
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 date-input">
              <label class="control-label" style="margin-left: 3px;">Zip/Postal Code</label>
              <input type="text" class="form-control notranslate" name="loc_zip_code" id="loc_zip_code" value="
              <?php echo $location !=null?$location->loc_zip_code:'';?>" disabled="disabled">
          </div>
      </div>

      <div class="form-group">
       <!--  <div class="col-md-6 col-sm-6 col-xs-6  date-input"><label class="control-label">Secondary Owner</label>
          <input type="text" class="form-control has-feedback-left notranslate"
            name="commercial_lead_name" id="commercial_lead_name"
            <?php if (isset($quote['commercial_lead_name']) && !empty($quote['commercial_lead_name'])) echo 'value="' . $quote['commercial_lead_name'] . '"'; else echo 'placeholder="Commercial Lead"'; ?> disabled="disabled">
            <span class="fa fa-user form-control-feedback left notranslate" aria-hidden="true"></span>
          
        </div> -->

          <!-- <div class="col-md-3 col-sm-3 col-xs-3 date-input">
              <label class="control-label">Contact Name</label>
              <input type="text" class="form-control has-feedback-left" name="full_name" id="full_name" disabled="disabled"
                    <?php if (isset($quote['created_by_name']) && !empty($quote['created_by_name'])) echo 'value="' . $quote['created_by_name'] . '"'; else echo 'placeholder="Quote Created By"'; ?> >
              <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
          </div><div class="clearfix"></div> -->
          <div class="clearfix"></div> 
         <!--  <div class="col-md-3 col-sm-3 col-xs-3  date-input">
              <label class="control-label">Contact Phone</label>
              <input type="text" class="form-control has-feedback-left notranslate" name="contact_phone" id="contact_phone" disabled="disabled"
                    <?php if (isset($quote['contact_phone']) && !empty($quote['contact_phone'])) echo 'value="' . $quote['contact_phone'] . '"'; else echo 'placeholder="Contact Phone"'; ?> >
              <span class="fa fa-phone form-control-feedback left notranslate" aria-hidden="true"></span>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-3  date-input">
              <label class="control-label">Contact Email</label>
              <input type="text" class="form-control has-feedback-left notranslate" name="contact_email" id="contact_email" disabled="disabled"
                    <?php if (isset($quote['contact_email']) && !empty($quote['contact_email'])) echo 'value="' . $quote['contact_email'] . '"'; else echo 'placeholder="Contact Email"'; ?> >
              <span class="fa fa-envelope form-control-feedback left notranslate" aria-hidden="true"></span>
          </div> -->
      </div>

     <!--  <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-6">
              <label class="control-label">Vendor Name</label>
              <input type="text" class="form-control notranslate" name="vendor_name" id="vendor_name" disabled="disabled"
                    <?php if (isset($vendor['vendor_name']) && !empty($vendor['vendor_name'])) echo 'value="' . $vendor['vendor_name'] . '"'; else echo 'placeholder="Vendor Name"'; ?> >
          </div>
      </div> -->

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-6 date-input">
                <label class="control-label">Quote Currency</label>
                <input type="text" class="form-control has-feedback-left notranslate" name="quote_currency" id="quote_currency" disabled="disabled"
                    <?php if (isset($quote['quote_currency']) && !empty($quote['quote_currency'])) echo 'value="' . $quote['quote_currency'] . '"'; else echo 'placeholder="Quote Currency"'; ?> >
                <span class="fa fa-money form-control-feedback left notranslate" aria-hidden="true"></span>
            </div>
        </div>

     

    
    </div>

      <div class="clearfix"> </div>
    
    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="document-tab">
        <?php 
            // integer starts at 0 before counting
            $existing_files = array(); 
            $upload_dir = $path.'uploads/quotes/';
            if ($quote_id)
            {
                if (!is_dir($path.'uploads/quotes')) mkdir($path.'uploads/quotes');
                if (!is_dir($path.'uploads/quotes/' . $quote_id)) 
                    mkdir($path.'uploads/quotes/' . $quote_id);
                $upload_dir = $path.'uploads/quotes/' . $quote_id . '/';
                if ($handle = opendir($upload_dir)) {
                    while (($uploaded_file = readdir($handle)) !== false){
                        if (!in_array($uploaded_file, array('.', '..')) && !is_dir($upload_dir . $uploaded_file)) 
                            $existing_files[] = $uploaded_file;
                    }
                }
            }
        ?>

    
       <div class="col-md-6 col-sm-6 col-xs-6">
        <h3 style="padding-top: 10px;font-size: 15px !important;
    padding-left: 8px;">Clients Documents<br /><br /></h3>
      </div><div class="clearfix"></div>

        <div class="col-md-6 col-sm-6 col-xs-6">
           <div class="clearfix"></div><br />
           <div id="document_list_cont_2"></div>
         
        </div>
        
         

  <div class="clearfix"> <br /><br /> </div><br /><br />

   <?php if(FunctionManager::quoteCheckStatus($quote_id)==4){
    $documentTitle = "Upload Your Documents Here";
   }else{
    $documentTitle = "Your Uploaded Documents";
   }?>

   <div class="col-md-12">
    <h3 style="padding-top: 10px;font-size: 15px !important;
    padding-left: 17px;"><?php echo  $documentTitle;?><br /><br /></h3>
  </div><div class="clearfix"></div><br />
 
 
  <div class="col-md-6 col-sm-6 col-xs-12">
     <div id="file_upload_alert" class="alert alert-success" role="alert"></div>
     <img class="processing_oboloo" src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/../images/icons/processin_oboloo.gif'; ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">
  <?php if(FunctionManager::quoteCheckStatus($quote_id)==4){?>
  <div class="col-md-4 col-sm-4 col-xs-12">
        <label class="control-label">Document</label>
        <input class="form-control notranslate" type="file" name="order_file_1" id="order_file_1" />
        <span class="text-danger" style="font-size: 10px;">Maximum File Size of 25MB</span>
    </div>
   <div class="col-md-6 col-sm-6 col-xs-12">
        <label class="control-label">File/Document Description</label>
        <input type="text" class="form-control notranslate" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" /> 
    </div>

    <div class="col-md-1 col-sm-1 col-xs-12 text-right"><br />
    <button id="btn_upload_1" class="btn btn-info" onclick="uploadDocument(1,event);" style="margin-top: 8px;">Upload</button>
     </div>
      <div class="clearfix"></div><br /><br />
   <?php } ?>
    
     <div id="document_list_cont"></div>
  </div>
 
</div>
    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="question-tab">
        <div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 style="padding: 10px 0px;">Quote Questionnaires<br /></h3>
                    <h5 style="color: #000;">Please answer each of the questionnaires below</h5>
                </div>
            </div>
           <?php $this->renderPartial('_quote_questionnaire_answer_form', 
                array('quoteQuestionnaireRender' => $quoteQuestionnaireRender, 'quote_id' => $quote_id ,'vendor_id'=>$vendor_id, 'submit_status' => $quote['submit_status'])); ?>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="tab_content4" aria-labelledby="supplier-evaluation">
      <p style="color: #2d9ca2; font-size: 14px; margin-top: 5px; margin-bottom: 10px;">This is the scoring criteria that has been created for this Sourcing Activity. This is an indication of how this quote will be scored based on your overall submission.</p>
      <div class="col-md-12 col-sm-12 col-xs-12">
        
        <?php $this->renderPartial('_scoring_criteria',array('quote'=>$quote,'scoringCriteria'=>$scoringCriteria));?>
      </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="tab_content5" aria-labelledby="product-evaluation">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <?php $this->renderPartial('_product',array('quote'=>$quote,'vendor_id'=>$vendor_id,'submit_quote_id'=>isset($submit_quote_id)?$submit_quote_id:0,'quote_vendor_id'=>$quote_vendor_id,'quote_vendor_id'=>$quote_vendor_id,'quote_id'=>$quote_id,'quote_details'=>$quote_details));?>
      </div>
    </div>
     


    <div class="clearfix"> <br /><br /> </div>
    
    <?php
     if (isset($quote['submit_status']) && $quote['submit_status']== 1) { ?>
        <div class="checkbox">
            <label for="submitted_already" style="font-weight: bold; font-size: 115%;">
                Since you have already submitted this quote, you cannot make any changes to it.
            </label>
        </div>
        <div class="clearfix"> <br /> </div>
    <?php } else if ( strtotime(date("Y-m-d H:i:s"))>strtotime($quote['closing_date']) ) { ?>
        <div class="checkbox">
            <label for="submitted_already" style="font-weight: bold; font-size: 115%;">
                This quote is closed, you cannot make any changes to it.
            </label>
        </div>
        <div class="clearfix"> <br /> </div>
    <?php } else if (strtotime(date("Y-m-d H:i:s"))<=strtotime($quote['opening_date'])) { ?>
        <div class="checkbox">
            <label for="submitted_already" style="font-weight: bold; font-size: 115%;">
                This quote wil be opened at <?php echo date("d/m/Y h:i A", strtotime($quote['opening_date']));?> <b>UTC</b>
            </label>
        </div>
        <div class="clearfix"> <br /> </div>
    <?php } else { ?>
        <div class="submit_btn_cont">
        <div class="checkbox">
            <div class="col-md-12">
                <input type="checkbox" name="submit_quote" id="submit_quote" value="1" style="margin-bottom: 6px !important;margin-left: -4px;" />
                <label for="submit_quote">Check this box if you want to submit the quote. You will not be able to change it later.

If you want to just save it and come back later to finish it, please click the <b>'Save Quote'</b> button.</label>
            </div>
        </div>
        <div class="clearfix"> <br /><br /> </div>
    
        <div class="col-md-12 submit_btn">
            <a href="javascript:void(0);" onclick="$('#quote_form').submit();">
                <button type="button" class="btn btn-primary">
                    Save Quote
                </button>
            </a>
        <a href="javascript:void(0);" onclick="submitQuote();">
            <button type="button" class="btn green-btn" >
                 Submit Quote
            </button>
        </a>
        </div>
      </div>
    <?php } ?>
    <div class="clearfix"> <br /> </div>
</div>
    <input type="hidden" name="submit_quote" value="" class="submit_quote">
</form>
</div>
</div>
</div>

<div id="new_document_code" style="display: none;">
    <div class="form-group" id="document_area_DOCIDX">
        <div class="col-md-5 col-sm-5 col-xs-5">
            <input class="form-control notranslate" type="file" name="quote_file_DOCIDX" id="quote_file_DOCIDX" />
        </div>
        <div class="col-md-1 col-sm-1 col-xs-1">
            <a onclick="deleteDocument(DOCIDX);" title="Click here to delete this document" style="cursor: pointer; color: #fff;">
                <span class="fa fa-minus fa-2x"></span>
            </a>
            <a onclick="addDocument();" title="Click here to add another document" style="cursor: pointer; color: #fff;">
                <span class="fa fa-plus fa-2x"></span>
            </a>
        </div>
    </div>
    <input type="hidden" name="delete_document_flag_DOCIDX" id="delete_document_flag_DOCIDX" value="0" />
    <div class="clearfix" id="end_document_area_DOCIDX"> <br /> </div>
</div>
<?php echo $this->questionnairFormModels;?>


<script type="text/javascript">
    $(document).ready(function() {

        $("#file_upload_alert").hide();
        $(".processing_oboloo").hide();

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          var target = $(e.target).attr("href") // activated tab
          if( target=="#tab_content5"){
            $(".submit_btn_cont").addClass('submit_btn_product');
          }else{
            $(".submit_btn_cont").removeClass('submit_btn_product');
          }
        });

        $('.price_calc').on('keyup', function() { calculateTotalPrice(); });
        $('.price_calc').on('blur', function() { calculateTotalPrice(); });

        $('.tax_calc').on('keyup', function() { calculateTotalPrice(); });
        $('.tax_calc').on('blur', function() { calculateTotalPrice(); });

        $('.shipping_calc').on('keyup', function() { calculateTotalPrice(); });
        $('.shipping_calc').on('blur', function() { calculateTotalPrice(); });

        $('.other_calc').on('keyup', function() { calculateTotalPrice(); });
        $('.other_calc').on('blur', function() { calculateTotalPrice(); });
    });

    function calculateProductPrice(id){
        //alert(id);
        var total_single_product_price = 0;
        $('.unit_'+id).each(function() {
            var single_product_price = $.trim($(this).val());
            var eid = this.id;
            if(eid == 'unit_price_'+id){
                var qty = $(this).data("value");
                if (single_product_price != "") total_single_product_price += parseFloat(single_product_price*qty);
            }else{
                if(eid == 'tax_rate_'+id){
                    var qty = $(this).data("value");
                    if (single_product_price != ""){
                        var product_unit_price = $("#unit_price_"+id).val();
                        var single_tax_product_price = parseFloat((single_product_price / 100.0)*qty) * parseFloat(product_unit_price);
                        total_single_product_price += parseFloat(single_tax_product_price);
                    }
                }else{
                    if (single_product_price != "") total_single_product_price += parseFloat(single_product_price);
                }

            }

        });
        $('.product_total_price_'+id).val(total_single_product_price.toFixed(2));
    }
    
    function calculateTotalPrice()
    {
        var total_product_price = 0;
        var product_price = 0;
        var total_product_rate = 0;
        var product_tax = 0;
        var total_product_shipping_charges = 0;
        var product_shipping_charges = 0;
        var total_product_other_charges = 0;
        var product_other_charges = 0;


        $('.product_unit_price').each(function() {
            product_price = $.trim($(this).val());
            var qty = $(this).data("value");
            if (product_price != "") total_product_price += parseFloat(product_price*qty);
        });

        $('.product_tax_rate').each(function() {
            product_tax = $.trim($(this).val());
            var idx = $(this).data("id");
            var qty = $(this).data("value");
            if (product_tax != "")
            {
                var product_unit_price = $("#unit_price_"+idx).val();
                product_tax = parseFloat((product_tax / 100.0)*qty) * parseFloat(product_unit_price);
                total_product_rate += product_tax;
            }

        });

        $('.product_shipping_charges').each(function() {
            product_shipping_charges = $.trim($(this).val());
            if (product_shipping_charges != "") total_product_shipping_charges += parseFloat(product_shipping_charges);
        });

        $('.product_other_charges').each(function() {
            product_other_charges = $.trim($(this).val());
            if (product_other_charges != "") total_product_other_charges += parseFloat(product_other_charges);
        });

        //console.log(total_single_product_price);
        
        var tax_rate = $.trim($('#tolal_tax_rate').val());
        if (tax_rate == "") tax_rate = 0; else tax_rate = parseFloat(tax_rate);
        
        var shipping = $.trim($('#tolal_shipping').val());
        if (shipping == "") shipping = 0; else shipping = parseFloat(shipping);
        
        var other_charges = $.trim($('#tolal_other_charges').val());
        if (other_charges == "") other_charges = 0; else other_charges = parseFloat(other_charges);
        
        var discount = $.trim($('#discount').val());
        if (discount == "") discount = 0; else discount = parseFloat(discount);
                
        var total_price = 0;
        total_price = parseFloat(total_product_price);

        total_price = total_price + total_product_rate;
        total_price = total_price + total_product_shipping_charges;
        total_price = total_price + total_product_other_charges;
        total_price = total_price - discount;

        $('#total_tax_rate').val(total_product_rate.toFixed(2));
        $('#total_shipping').val(total_product_shipping_charges.toFixed(2));
        $('#total_other_charges').val(total_product_other_charges.toFixed(2));
        $('#total_price').val(total_price.toFixed(2));
    }

    function submitQuote()
    {

        var unitPrice = parseFloat($("#unit_price_1").val());

         if(isNaN(unitPrice)){
            alert('Product Unit Price is Manditory');
            return false;
          } 

        if($("#submit_quote").prop('checked') == true){
            
            $(".submit_quote").val('1');

            $('#quote_form').submit();
        } else {
            alert('Please check the check box above');
        }


    }

    function deleteQuoteFile(file_idx, file_name)
    {
        $('#delete_quote_file_link_' + file_idx).confirmation({
          title: "Are you sure you want to delete the attached file?",
          singleton: true,
          placement: 'right',
          popout: true,
          onConfirm: function() {
            $('#existing_file_id_' + file_idx).remove();
            $.ajax({
                type: "POST",
                url: "<?php echo AppUrl::bicesUrl('quotes/deleteVendorFile/'); ?>",
                data: { quote_id: $('#quote_id').val(), vendor_id: $('#vendor_id').val(), file_name: file_name }
            });
          },
          onCancel: function() {  }
        });
        
    }

    function addDocument()
    {
        var total_documents = $('#total_documents').val();
        total_documents = parseInt(total_documents);
        total_documents = total_documents + 1;
        $('#total_documents').val(total_documents);
        
        var new_document_html = $('#new_document_code').html();
        new_document_html = new_document_html.replace(/DOCIDX/g, total_documents);
        $('#total_documents').before(new_document_html);
    }

    function deleteDocument(docidx)
    {
        var display_document_count = 0;
        $("div[id^='document_area']").each(function () {
            if ($(this).is(':visible')) display_document_count += 1;
        });

        if (display_document_count <= 1) alert("You must have at least one document field in the quote form");
        else
        {
            if (confirm("Are you sure you want to delete this document?"))
            {
                $('#delete_document_flag_' + docidx).val(1);
                $('#document_area_' + docidx).hide();
                $('#end_document_area_' + docidx).hide();
            }
        }
    }


function loadQuoteFile(){
  $.ajax({
        url: "<?php echo  $this->createUrl('quotes/quoteFile');?>", // point to server-side PHP script 
        dataType: 'json',  // what to expect back from the PHP script, if anything
        data: {quote_id:"<?php echo $quote['quote_id']; ?>"},                         
        type: 'post',
        success: function(uploaded_response){
          $("#document_list_cont").html(uploaded_response.uploaded_list);
          $("#document_list_cont_2").html(uploaded_response.uploaded_list_2);
        }
     });

}
function uploadDocument(uploadBtnIDX,e)
{  
 
  e.preventDefault();
  var uploadedObj = $('#order_file_'+uploadBtnIDX);
  var uploadedObjDesc = $('#file_desc_'+uploadBtnIDX); 
 

  var file_data = $('#order_file_'+uploadBtnIDX).prop('files')[0];
  var field_data = $('#file_desc_'+uploadBtnIDX).val();

  if (typeof file_data !== 'undefined'){
  
  var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('file_description', field_data); 
    form_data.append('quote_id', "<?php echo $quote['quote_id']; ?>");       
    $.ajax({
        url:"<?php echo  $this->createUrl('quotes/uploadDocumentModal');?>", // point to server-side PHP script 
        dataType: 'json',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
         beforeSend: function() {
          $(".processing_oboloo").show();
          $("#file_upload_alert").html('');
          $("#file_upload_alert").hide();
          $(".submit_btn a").addClass('a_disabled');
          $("#btn_upload_1").prop('disabled',true);
         },
        success: function(uploaded_response){

          if(uploaded_response.uploaded==1){
            $("#file_upload_alert").show();
            $("#file_upload_alert").html('File uploaded successfully');
          }else{

            $("#file_upload_alert").html('Problem occured while uploading file, try again');
             $("#file_upload_alert").show();
          }

          $(".processing_oboloo").hide();
          $(".submit_btn a").removeClass('a_disabled');
          $("#btn_upload_1").prop('disabled',false);


          loadQuoteFile();

          uploadedObj.val(null);
          uploadedObjDesc.val(null);
        }
     });
    }else{
      $("#file_upload_alert").show();
      $("#file_upload_alert").html('Document is required');  
    }

     $("#file_upload_alert").delay(1000*5).fadeOut();
}

 loadQuoteFile();

function deleteDocumentModal(documentKey)
{
  var documentKey = documentKey;
  $("#file_upload_alert").hide();
  var r = confirm("Are you sure want to delete this file/document");
  if (r == true) {
    
        $.ajax({
            dataType: 'json',  
            type: "post",
            url: "<?php echo AppUrl::bicesUrl('quotes/deleteDocumentModal'); ?>",
            data: { documentKey: documentKey,quote_id:"<?php echo $quote['quote_id']; ?>" },

            success: function(uploaded_response){
              if(uploaded_response.uploaded==1){
                $("#file_upload_alert").show();
                $("#file_upload_alert").html('File deleted successfully');
              }else{
                $("#file_upload_alert").show();
                $("#file_upload_alert").html('Problem occured while deleting file, try again');
              }

              loadQuoteFile();
              
            }
        });
      }

      $("#file_upload_alert").delay(1000*5).fadeOut();
}

</script>
<style type="text/css">
  label { color: #73879C; }
  table tr {color:#2d9ca2 !important;}
  .submit_btn_product {float:right;}
  .submit_btn_product .submit_btn{text-align: right;}

  .a_disabled { pointer-events: none; cursor: default; box-shadow: none; opacity: .65; }
  .count{ color: #000; font-size: 14px; font-weight: 600; width: 5%; float: left; margin-top: 10px; }
</style>
