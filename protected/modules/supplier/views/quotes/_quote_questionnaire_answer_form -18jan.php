<?php $this->renderPartial('_script');
  $formquestions ='';
 ?>
<div class="container">
     <div class="panel-group" id="accordion">
      <?php foreach($quoteQuestionnaireRender as $form){
          getForms($form);  
      } ?>
    </div>
</div>
<?php 
function getForms($form){ ?>     
  
  <div class="panel panel-default ">
    <div class="panel-heading bg-primary">
      <h4 class="panel-title">
       <a class="accordion-toggle" data-toggle="collapse" 
        data-parent="#accordion" href="#collapse_<?= $form['question_form_id'] ?>">
         <span class="glyphicon glyphicon-plus"></span>
         <?= $form['question_form_name']; ?>
        </a>
      </h4>
     </div>
    <?php 
    $sql = "select * from form_answers where quote_id=".$_GET['quote_id']." and form_id=".$form['question_form_id'];
    $quoteAnswerRender = Yii::app()->db->createCommand($sql)->queryAll();
    if(!empty($quoteAnswerRender) && count($quoteAnswerRender) > 0){ ?>
      <div class="formcontquestans_<?= $form['question_form_id'] ?>">
      <div id="collapse_<?= $form['question_form_id'] ?>" class="panel-collapse collapse">
        <div class="panel-body">
         <div class="build-wrap-<?= $form['question_form_id'] ?>"></div>
         <button type="button" class="btn btn-primary" onclick="saveUpdateAnswer(<?= $form['question_form_id'] ?>)">save</button>
        </div>
      </div> 
      <script type="text/javascript">
       $(document).ready(function(){  
        var formQuestion = <?php echo json_encode($quoteAnswerRender); ?>;
        getAnswersForm(formQuestion , <?= $form['question_form_id'] ?>); 
       });
      </script>
      </div>
    <?php } else {
    
    $sql="SELECT  id, form_id, question FROM `form_questions` where form_id=".$form['question_form_id'];
    $formquestions = Yii::app()->db->createCommand($sql)->queryAll();
    ?>
    
    <script type="text/javascript">
     $(document).ready(function(){  
      var formQuestion = <?php echo json_encode($formquestions); ?>;
      getQuestionsForm(formQuestion , <?= $form['question_form_id'] ?>); 
     });
    </script>
    <div class="formcontquestans_<?= $form['question_form_id'] ?>">
     <div id="collapse_<?= $form['question_form_id'] ?>" class="panel-collapse collapse">
       <div class="panel-body">
         <div class="build-wrap-<?= $form['question_form_id'] ?>"></div>
         <button type="button" class="btn btn-primary" onclick="saveUpdateAnswer(<?= $form['question_form_id'] ?>)">save</button>
       </div>
     </div>
    </div>
    <?php } ?>
    </div>
<?php } ?>

<script>
$(document).ready(function(){
   $('.collapse').on('shown.bs.collapse', function(){
     $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
     }).on('hidden.bs.collapse', function(){
      $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
   });  
  });

// If quote against No answer submitted ,Case show questionnire form only against this formID and Quote ID
function getQuestionsForm(options, formId){
    var formQuestion = options;
    var allQuestionsform = [];

    for(let index = 0; index < formQuestion.length; index++){
     var formFieldRow = formQuestion[index];
     var formField = formFieldRow.question;
     formField = JSON.parse(formField);
     allQuestionsform.push(formField)
    }

    var formData = { formData : allQuestionsform }
    var formBuilder = $('.build-wrap-'+formId).formRender(formData);
  }
// If quote against answer form submitted , in this case form update value show againtest this formID and Quote ID
function getAnswersForm(options, formId){
    var formAnswers = options;
    var allanswerOfForm = [];
     for(let index=0; index < formAnswers.length; index++){
       var formFieldRow = formAnswers[index];
       var formField = formFieldRow.answer;
       formField = JSON.parse(formField);
       allanswerOfForm.push(formField)
     }
 var formData = {
   formData : allanswerOfForm
 }
 var formBuilder = $('.build-wrap-'+formId).formRender(formData);
}

function saveUpdateAnswer(formId){
    var formId = formId;
    $('#success_alert_'+formId).hide();
    var formanswer = $('.build-wrap-'+formId);
    const formRender = formanswer.formRender('userData');

    $.ajax({
       type: "POST", 
       data: {quoteID: <?= $quote_id; ?>,  formId: formId, formRender: formRender }, dataType: "json",
       url: "<?php echo $this->createUrl('quotes/answerSave'); ?>",
       success: function(options) {
          $('#success_alert_'+formId).show();
          $('#success_alert_'+formId).html('Answer submitted!');
         setTimeout(function() {
           $('#success_alert_'+formId).hide();
         }, 5000);
         
       },
       error: function() {  }
   });

  }
</script>

<style type="text/css">
  .accordion-toggle:hover { text-decoration: none;}  
  .container{ margin-top:100px; } 
</style>