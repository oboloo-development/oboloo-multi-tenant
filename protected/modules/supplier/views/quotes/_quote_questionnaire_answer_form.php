<?php $this->renderPartial('_script'); ?>
<div class="container">
      <table class="table table table-striped table-bordered questionnaire-table" style="width:100%; color: #000000 !important;">
        <thead>
          <tr class="tr">
            <th>Action</th>
            <th class="text-center">Section</th>
            <th class="text-center">Status</th>
          </tr>
        </thead>
        <tbody>
         <?php 
         $modelForm = '';
         foreach($quoteQuestionnaireRender as $form){ ?>
          <tr class="tr">
            <td class="td">
            <?php if (isset($submit_status) && $submit_status != 1) { ?>
              <button type="button" class="btn btn-primary btn-sm" onclick="openQuestionAnswerModal(<?= $form['question_form_id']; ?>,'')" data-toggle="modal" data-target="#form_<?= $form['question_form_id']; ?>">Open Section</button>
            <?php } else{?>
              <button type="button" class="btn btn-primary btn-sm" onclick="openQuestionAnswerModal(<?= $form['question_form_id']; ?>, 'view')" data-toggle="modal" data-target="#form_<?= $form['question_form_id']; ?>">Open Section</button>
            <?php } ?>
            </td>
            <td class="td text-center"><?= $form['question_form_name']; ?></td>
            <td class="td text-center status_<?= $form['id']; ?>"><button type="button" class="btn btn-success"><?= $form['status']; ?></button></td>
          </tr> 
         <?php 
         $formquestionID = $form['question_form_id'];
         $formquestionName = $form['question_form_name'];
         $quoteformID = $form['id'];
         $modelForm .= '
        <div class="modal fade modal_'.$formquestionID.'" id="form_'.$formquestionID.'" tabindex="-1" role="dialog" 
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="margin:20px !important; margin-left: 0px !important;" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <form id="quote_form_qutestion" class="quote_form_qutestion" action="" method="POST" onsubmit="return false">
          <div class="modal-content">
            <div class="modal-header">
              <div class="col-md-11" style="margin-top: 10px;">
                 <h3 class="modal-title" id="exampleModalLongTitle">'.$formquestionName.'</h3>
              </div>
              <div class="col-md-1">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
            </div>
            <div class="modal-body">
             <div id="success_alert_'.$formquestionID.'" class="alert alert-success" style="display: none;"></div><br />
             <div class="build-wrap-'.$formquestionID.'"></div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
               <button type="submit" onclick="saveAndUpdateAnswer('.$formquestionID.','.$quoteformID.');"
                class="btn btn-primary viewCase_'.$formquestionID.'  before_disabled_'.$formquestionID.'" >Save Questionnaire</button>
            </div>
           </div>
          </form>
        </div>
      </div>';
        } ?> 
       </tbody>
     </table>
</div>
  <?php $this->questionnairFormModels=$modelForm; ?>
<script>
  
function openQuestionAnswerModal(formId, status){
  if(status === 'view'){
    $('.viewCase_'+formId).hide();
  }

  if(formId){
    var formId = formId;
    $.ajax({
      type: "POST", data: { formId: formId, quoteID: <?= $quote_id ?>,vendor_id: <?= $vendor_id ?> }, dataType: "json",
      url: "<?php echo $this->createUrl('quotes/vendorFormQuestions'); ?>",
      success: function(options) {
      if(options.status == 'ans'){
        getAnswersForm(options, formId);
      }else if(options.status == 'question'){
        getQuestionsForm(options, formId);
      }
      },
      error: function() {  }
    });
  }
}
// If quote against answer form submitted , in this case form update value show againtest this formID and Quote ID
function getAnswersForm(options, formId){
  var formAnswers = options.formquestions;
  var allanswerOfForm = [];
    
  for(let index=0; index < formAnswers.length; index++){
    var formFieldRow = formAnswers[index];
    var formField = formFieldRow.answer;
    formField = JSON.parse(formField);
    allanswerOfForm.push(formField)
  }
  
 var formData = {formData : allanswerOfForm, dataType: 'json'}
 var formBuilder = $('.build-wrap-'+formId).formRender(formData);
}
// If quote against No answer submitted ,Case show questionnire questions form only against this formID and Quote ID
function getQuestionsForm(options, formId){
  var formQuestion = options.formquestions;
  var allQuestionsform = [];
  for(let index = 0; index < formQuestion.length; index++){
    var formFieldRow = formQuestion[index];
    var formField = formFieldRow.question;
    formField = JSON.parse(formField);
    allQuestionsform.push(formField)
  }

  var formData = {formData : allQuestionsform, dataType: 'json'}
  var formBuilder = $('.build-wrap-'+formId).formRender(formData);
}

function saveAndUpdateAnswer(formId, qouteFormId){
  var formId = formId;
  var qouteFormId = qouteFormId;
  var checkRequired= true;
  $('#success_alert_'+formId).hide();
  
  $('.quote_form_qutestion').find('select, textarea, input[type=text], input[type=number], input[type=redio]').each(function(){
    if($(this).prop('required') && ( !$(this).val() || $(this).val()[0].trim().length === 0 )){
      checkRequired = false;
    }
  });

  $('.quote_form_qutestion').find('input[type=checkbox]').each(function(){
    if($(this).prop('required') && ($(this).prop("checked")[0] != true)){
      checkRequired = false;
    }
  });

  $('.quote_form_qutestion').find('input[type=date]').each(function(){
    if($(this).prop('required') && ( !$(this).val() || $(this).val()[0].trim().length === 0 )){
      checkRequired = false;
    }
  });
  
  if(checkRequired){ 
   var formRender = $('.build-wrap-'+formId).formRender('userData');
     $.ajax({
     type: "POST",
     data: {
      quoteID: <?= $quote_id; ?>, 
      vendor_id: <?= $vendor_id; ?>, 
      formId: formId,
      formRender: formRender,
      qouteFormId : qouteFormId 
     },
     dataType: "json",
     url: "<?php echo $this->createUrl('quotes/answerSave'); ?>",
     beforeSend: function() {    
      $('.before_disabled_'+formId).prop('disabled', true);
     },
     success: function(options) {
      $('#success_alert_'+formId).show();
      $('#success_alert_'+formId).html('Section Completed');
      $('#success_alert_'+formId).html('Section Completed');
      setTimeout(function() {
        $('.modal').modal('hide');
        $('#success_alert_'+formId).hide();
        $('.status_'+qouteFormId).closest("td").find('button').html('Completed');
        $('.before_disabled_'+formId).prop('disabled', false);
      }, 1000);
      }
     });
   }
  }

  $(document).ready(function(){
    $('.questionnaire-table').dataTable({});
  })
</script>
<style>
  @media (min-width: 768px){ .modal-dialog { width: 50% !important; margin: 50px auto !important; } }
  .tr, .td {color: #000000 !important; font-weight: 600;}
</style>



