<?php $this->pageTitle = 'Extensions';?>

<div class="main-content">

<?php if(Yii::app()->user->hasFlash('success')):?>
	<?php echo Yii::app()->user->getFlash('success'); ?>
<?php endif; ?>

  <div class="row"> 
    <!--Condensed Table--> 
    
    <!--Hover table-->
    <div class="col-sm-12">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-border-color panel-border-color-primary">
            <div class="panel-body">
              <div class="white-box">
                <div class="text-left"> </div>
                <br>
                <div class="row ">
                  <div class="col-md-12">
                    <div class="well well-sm">
                      <p class="m-b-10">Google Calendar Sync</p>
                    
                      <a href="<?php echo Yii::app()->createAbsoluteUrl('site/oAuth2Callback'); ?>" class="btn btn-default">Google Calendar Sync Permission </a>
                    
                    </div>
                    <div class="well well-sm">
                      <p class="m-b-10">Microsoft Sync</p>
                      <button type="button" class="btn btn-default">Microsoft Sync Permission</button>
                    </div>
                    <div class="well well-sm">
                      <p class="m-b-10">Office 365 Calendar Sync</p>
                      <button type="button" class="btn btn-default">Office 365 Calendar Permission</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
