<script type="text/javascript" src="<?php echo AppUrl::jsUrl('app.js'); ?>"></script>
<div>
<div class="col-md-3" style="width: 24% !important; margin-left: 38% !important;">
      <br /><br />
          <img class="img-responsive" src="<?php echo AppUrl::bicesUrl('images/app_logo.png'); ?>" />
          <div class="clearfix"> </div>
  </div>
    <div class="clearfix"> </div>
  
  <a class="hiddenanchor" id="signup"></a>
  <a class="hiddenanchor" id="signin"></a>

  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
      <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'login-form',
          'enableAjaxValidation'=>true,
          //'htmlOptions'=>array('class'=>'form-horizontal form-material','id'=>'loginform'),
      )); ?>
         
         <div class="form-group">
          <div style="text-align: center;
    margin-bottom: 10px;">Please Enter Your Reference Code <br />That can be found in the oboloo Email</div>
        <div class="item form-group">
         
            <?php echo $form->textField($model,'username',array('class'=>'form-control','placeholder'=>'Reference','id'=>'username','autocomplete'=>"off","class"=>"form-control")); ?>
          
        </div>
       <!--  <div class="item form-group">
            <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Password','id'=>'password')); ?>
        </div> -->
        <div class="clearfix"></div>
        <div class="login_btns">
          <button class="btn btn-default submit pull-right logocolorbtn" onclick="return checkLogin();">Log in</button><div class="clearfix"></div><br />
        <!--    <a class="to_forgot pull-right" href="<?php echo AppUrl::bicesUrl('site/changePassword'); ?>">Change Password?</a> -->
          <!-- <a class="to_forgot pull-right" href="<?php echo AppUrl::bicesUrl('site/forgot'); ?>">Forgot Reference?</a> -->
        </div>
      <?php $this->endWidget(); ?>
    </section>
    </div>
  

    <?php if (isset($register_success) && $register_success == 1) { ?>
        <div id="message_area">
            You have successfully registered. Please contact your administrator to activate your account.
        </div>
    <?php } ?>

    <?php if (isset($error) && $error == 1) { ?>
        <div id="message_area" style="color: red;">
            Invalid username/password combination specified.<br />Please try again.
            <br /><br />
        </div>
    <?php } ?>

    <?php if (isset($error) && $error == 2) { ?>
        <div id="message_area" style="color: red;">
            Your account has not been activated as yet.<br />Please contact your administrator.
            <br /><br />
        </div>
    <?php } ?>

    <div id="status_area">
    </div>
    <div class="clearfix"></div>

    <div class="login_helpfularea">
          <div class="social-icons-wrapper">
            <a class="social-icon-login" href="https://www.linkedin.com/company/18585513" target="_blank"> <img src="<?php echo AppUrl::bicesUrl('images/icons/linkedin.png'); ?>" alt="" width="50px;" height="50px;" /></a>
            <a class="social-icon-login" href="https://twitter.com/oboloosoftware" target="_blank"><img src="<?php echo AppUrl::bicesUrl('images/icons/Twitter.PNG'); ?>" alt="" width="50px;" height="50px;"/></a>
            <!-- <a class="social-icon-login" href="https://www.facebook.com/oboloosoftware/" target="_blank"><img src="<?php //echo AppUrl::bicesUrl('images/icons/face.PNG'); ?>" alt="" width="50px;" height="49px;"/></a> -->
          </div>
          <div class="login_helpfularea_text">
          By logging into the oboloo system you agree to oboloo's <a href="https://oboloo.com/privacy-cookies-policy"  class="login-helpul-link" target="_blank">Privacy Policy</a>, <a href="https://oboloo.com/Terms-of-Service/" class="login-helpul-link" target="_blank">Terms of Use</a> & <a href="https://oboloo.com/acceptable-use-policy" class="login-helpul-link" target="_blank">Acceptable Use Policy</a>
          © <?php echo date('Y'); ?> oboloo Limited. All rights reserved. Republication or redistribution of oboloo content, including by framing or similar means, is prohibited without the prior written consent of oboloo Limited. oboloo & Oboloo and the oboloo logo are registered trademarks of oboloo Limited and its affiliated companies. Trademark number: UK00003466421  Company Number 12420854. ICO Reference Number: ZA764971'
        </div></div><div class="clearfix"></div>

  </div>


</div>

<div class="modal fade" id="delete_confirm_modal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header alert-info">
          <h4 class="modal-title">Fix Registration Errors</h4>
        </div>
        <div class="modal-body">
          <p id="registration_errors"></p>
        </div>
        <div class="modal-footer">
          <button id="no_delete" type="button" class="alert-success btn btn-default" data-dismiss="modal">
              OK
          </button>
        </div>
      </div>
  </div>
</div>