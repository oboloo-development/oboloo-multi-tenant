<?php $this->pageTitle =  'Update Profile';?>
<?php $this->renderPartial('settingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
    <div class="cleafix " style="padding: 30px 20px; ">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h4 class="m-b-10">Update Profile</h4>
            <!--<p class="m-b-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>-->

            <?php if(Yii::app()->user->hasFlash('success')):?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
            <?php if(Yii::app()->user->hasFlash('error')):?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo Yii::app()->user->getFlash('error'); ?>
                </div>
            <?php endif; ?>

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'vendor-form',
                'enableAjaxValidation'=>false,
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
            )); ?>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for=""> First Name </label>
                        <?php echo $form->textField($model,'first_name',array('class'=>'form-control')); ?> <?php echo $form->error($model,'first_name'); ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="">Last Name</label>
                        <?php echo $form->textField($model,'last_name',array('class'=>'form-control')); ?> <?php echo $form->error($model,'last_name'); ?>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="">Vendor Profile Image</label>
                        <?php echo $form->fileField($model,'profile_image',array('class'=>'form-control')); ?> <?php echo $form->error($model,'profile_image'); ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="">Vendor Phone Number</label>
                        <?php echo $form->textField($model,'phone',array('class'=>'form-control')); ?> <?php echo $form->error($model,'phone'); ?>
                    </div>
                </div>
            </div>
            <!-- row -->
            <button type="submit" class="btn btn-success">Update</button>
            <?php $this->endWidget(); ?>

        </div> <!-- col -->

        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <!-- <h4 class="m-b-10">Excepteur sint</h4>

           --<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                 tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                 consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                 cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                 proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>-->

        </div> <!-- col -->

    </div>
    <div class="seprater-bottom-100"></div>
    <div class="seprater-bottom-100"></div>
    </div>



