<div>
  <div style="right: 0px;margin: 0px auto;max-width: 350px;position: relative;"><br /><br />
  <img class="img-responsive" src="<?php echo AppUrl::bicesUrl('images/app_logo.png'); ?>" />
  <div class="clearfix"> </div>
</div><div class="clearfix"> </div>
  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
          <form novalidate id="forgot_form" name="forgot_form" method="post" action="">
          <h1>Change Password</h1>
          <div class="item form-group">
            <input id="email" name="email" type="text" class="form-control" placeholder="Email" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="login_btns">
            <a class="btn btn-default submit logocolorbtn pull-right" onclick="$('#forgot_form').submit();" style="color: #333">Change Password</a>
            <div class="clearfix"></div>
            <div style="margin-top: 15px; display: inline; float: right;">
            <a  href="<?php echo AppUrl::bicesUrl('site/login'); ?>">Login Here</a>
           </div>
          </div>
			<input type="hidden" name="form_submitted" id="form_submitted" value="1" />
          <div class="clearfix"></div>

            <br />

          </div>
        </form>
      </section>
    </div>


    <?php if (isset($success) && $success == 1) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            Please, check your email, an email has been sent to your email address.
        </div>
    <?php } ?>

    <?php if (isset($error) && $error == 1) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            No such account was not found. Please enter correct username or email address.
            <br /><br />
        </div>
    <?php } ?>


    <div id="status_area">
    </div>
    <div class="clearfix"></div>


  </div>
</div>

<style type="text/css">
  #message_area {
    font-size: 17px;
    padding-top: 14px;
    text-shadow: none;
  }
</style>
