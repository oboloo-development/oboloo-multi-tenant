<?php $this->pageTitle =  'Company Information';?>
<?php $this->renderPartial('settingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                        <div class="cleafix " style="padding: 30px 20px; ">
                          <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <h4 class="m-b-10">Company Profile</h4>
                            <!--<p class="m-b-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>-->

                           <?php if(Yii::app()->user->hasFlash('success')):?>
                            <div class="alert alert-success">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <?php echo Yii::app()->user->getFlash('success'); ?>
                            </div>
                            <?php endif; ?>
                            <?php if(Yii::app()->user->hasFlash('error')):?>
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <?php echo Yii::app()->user->getFlash('error'); ?>
                            </div>
                            <?php endif; ?>
                            
                            <?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'vendor-form',
								'enableAjaxValidation'=>false,
								)); ?>
                               <div class="row">
                                  <br />
                                  <div class="col-xs-12 col-sm-6">
                                     <div class="form-group">
                                        <label for="">Vendor Organization Name</label>
                                        <?php echo $form->textField($model,'organization',array('class'=>'form-control')); ?> <?php echo $form->error($model,'organization'); ?>  
                                     </div>
                                  </div>
                                  
                               </div>
                               <!-- row -->
                               <div class="row">
                                  <div class="col-xs-12 col-sm-6">
                                     <div class="form-group">
                                        <label for="">Vendor Email Address</label>
                                        <?php echo $form->textField($model,'email',array('class'=>'form-control')); ?> <?php echo $form->error($model,'email'); ?>  
                                     </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6">
                                     <div class="form-group">
                                        <label for="">Vendor Phone Number</label>
                                        <?php echo $form->textField($model,'phone',array('class'=>'form-control')); ?> <?php echo $form->error($model,'phone'); ?>  
                                     </div>
                                  </div>
                               </div>
                               <!-- row -->
                               <hr>
                               <div class="row">
                                  <div class="col-xs-12 col-sm-6">
                                     <div class="form-group">
                                        <label for=""> First Name </label>
                                        <?php echo $form->textField($model,'first_name',array('class'=>'form-control')); ?> <?php echo $form->error($model,'first_name'); ?>  
                                     </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-6">
                                     <div class="form-group">
                                        <label for="">Last Name</label>
                                        <?php echo $form->textField($model,'last_name',array('class'=>'form-control')); ?> <?php echo $form->error($model,'last_name'); ?>  
                                     </div>
                                  </div>
                               </div>
                              
                               
                              
                             
                               <hr>
                             
                              
                               <br><br>
                               <button type="submit" class="btn btn-success">Update</button>
                            <?php $this->endWidget(); ?>
                             
                          </div> <!-- col -->

                          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                             <!--<h4 class="m-b-10">Excepteur sint</h4>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>-->
                          </div> <!-- col -->
                        </div> <!-- row -->
                    </div>
              

<script>
      var placeSearch, autocomplete;
      var componentForm = {
        //street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          console.log(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;

          }
        }
      }

      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

 $('#autocomplete').live('change',function(){
 	$('#route').val($(this).value);
 });
    </script> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeg8MgxBJnzgTHYvbwMtHr7rUyhxsUbUA&libraries=places&callback=initAutocomplete"
        async defer></script>
