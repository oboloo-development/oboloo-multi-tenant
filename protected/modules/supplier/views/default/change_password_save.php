<div>
   <div style="right: 0px;margin: 0px auto;max-width: 350px;position: relative;"><br /><br />
  <img class="img-responsive" src="<?php echo AppUrl::bicesUrl('images/app_logo.png'); ?>" />
  <div class="clearfix"> </div>
</div><div class="clearfix"> </div>
  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
          <form novalidate id="forgot_form" name="forgot_form" method="post" action="">
          <h1>Change Password</h1>
          <div class="item form-group">
            <input id="password" name="password" type="password" class="form-control" placeholder="Current Password" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="item form-group">
            <input id="password_new" name="password_new" type="password" class="form-control" placeholder="New Password" required="required" />
          </div>
          <div class="clearfix"></div>
          <div class="item form-group">
            <input  name="password_new_confirm" type="password" class="form-control" placeholder="Confirm New Password" required="required" />
          </div>
          <div class="clearfix"></div>
          <div style="width: 350px;">
            <a class="btn btn-default submit" onclick="$('#forgot_form').submit();" style="color: #333">Change Password</a>
            <div style="margin-top: 15px; display: inline; float: right;">
            OR <a style="text-decoration: underline;" href="<?php echo AppUrl::bicesUrl('site/login'); ?>">Login Here</a>
           </div>
          </div>
			<input type="hidden" name="form_submitted" id="form_submitted" value="1" />
          <div class="clearfix"></div>

            <br />

          </div>
        </form>
      </section>
    </div>


    <?php if (isset($success) && $success == 1) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            Your password changed successfully.
        </div>
    <?php } ?>

    <?php if (isset($error) && $error == 1) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            No such account was not found. Please contact your administrator.
            <br /><br />
        </div>
    <?php }else if (isset($error) && $error == 2) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            Your current password does not match, please, try again.
            <br /><br />
        </div>
    <?php }else if (isset($error) && $error == 3) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            Your new password and confirm new password do not match, please, try again.
            <br /><br />
        </div>
    <?php }else if (isset($error) && $error ==4) { ?>
        <div id="message_area" style="text-align: center; color: red;">
            Your new password is less then 6 characters, your new password should be at least 6 characters.
            <br /><br />
        </div>
    <?php } ?>


    <div id="status_area">
    </div>
    <div class="clearfix"></div>


  </div>
</div>
<style type="text/css">
  #message_area {
    font-size: 17px;
    padding-top: 14px;
    text-shadow: none;
  }
</style>
