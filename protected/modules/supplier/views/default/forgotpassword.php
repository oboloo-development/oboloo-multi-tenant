<div class="login-inner">
<div class="text-center">
<img src="<?php echo Yii::app()->request->baseUrl; ?>/themeassets/assets/images/logo-large.png"></div>

    <?php $form=$this->beginWidget('CActiveForm', array(

      'id'=>'login-form',

      'enableAjaxValidation'=>true,

      //'htmlOptions'=>array('class'=>'form-horizontal','id'=>''),

      // 'action'=>Yii::app()->createUrl('Admin/default/forgotpassword'),

    )); ?>

       

           <?php if(Yii::app()->user->hasFlash('error')):

                    echo Yii::app()->user->getFlash('error');

                 endif; ?>

        <h4 class="m-b-30">Forgot Password</h4>
        <div class="form-group ">
          <label for="">Your Email Address</label>
            <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'Email','id'=>'email','required'=>'required')); ?>
        </div>

        <div class="form-group login-submit">
        <button  type="submit" class="btn btn-success">Reset</button>
      </div>
      <?php $this->endWidget(); ?>

  </div>