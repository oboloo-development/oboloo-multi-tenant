<div class="col-lg-3 p-a-0 hidden-md-down messages-sidebar scroll-y flexbox-xs full-height">
                       <div class="p-a-1">
                          <nav>
                              
                           <ul class="nav nav-pills nav-stacked m-b-1">

                           <?php
                             $vendorID =  Yii::app()->user->id;
                              $vendor = Vendor::model()->findByPk($vendorID);
                             if(in_array($vendor->member_type,array(0,3))) {?>
                                                           
                               <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('vendor/teammemberList'); ?>">Staff Members</a>
                               </li>
                           
                               <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('vendTeamCat/index'); ?>">Job Alerts</a> 
                               </li>

                                <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('vendTeamCat/vendNotification'); ?>">General Notifications</a> 
                               </li>
                           
                               <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('site/updateAccount'); ?>">Company Information</a>
                               </li>
                               <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('site/updateProfile'); ?>">Update Profile</a>
                               </li>
                               <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('site/changepassword'); ?>">Change Password</a>
                               </li>

                               <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('site/markUp'); ?>">Mark Up</a>
                               </li>
                               <?php }else{ ?>
                              <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('site/updateProfile'); ?>">Update Profile</a>
                               </li>
                               <li class="nav-item"><a class="nav-link" href="<?php echo $this->createAbsoluteUrl('site/changepassword'); ?>">Change Password</a>
                               </li>
                               <?php } ?>
                           
                            </ul>
                           
                          </nav>
                       </div>
                    </div>