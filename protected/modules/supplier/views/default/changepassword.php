<?php $this->pageTitle =  'Change Password';?>
<?php $this->renderPartial('settingsidebar'); ?>
                    <div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                        <div class="cleafix " style="padding: 30px 20px; ">
                          <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                            <h4 class="m-b-10">Change Password</h4>
                            <!--<p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>-->
							<?php if(Yii::app()->user->hasFlash('success')):?>
                            <div class="alert alert-success">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <?php echo Yii::app()->user->getFlash('success'); ?>
                            </div>
                            <?php endif; ?>
                            <?php if(Yii::app()->user->hasFlash('reject')):?>
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <?php echo Yii::app()->user->getFlash('reject'); ?>
                            </div>
                            <?php endif; ?>

                            <form id="client-form" action="" method="post">
                               <div class="row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                     <div class="form-group">
                                        <label for="">Current password</label>
                                        <input type="password" name="current_password" class="form-control" id="" placeholder="">
                                     </div>

                                      <div class="form-group">
                                        <label for="">New password</label>
                                        <input type="password" name="new_password" class="form-control" id="" placeholder="">
                                     </div>

                                      <div class="form-group">
                                        <label for="">Confirm password</label>
                                        <input type="password" name="repeate_password" class="form-control" id="" placeholder="">
                                     </div>
                                  </div>
                                  <!-- col -->
                                 
                               </div>
                               <!-- row -->
                               <br>
                               <button type="submit" name="UpdatePassword" class="btn btn-success">Submit</button>
                            </form>
                             
                            
                          </div> <!-- col -->

                          
                        </div> <!-- row -->
                        
                        <div class="seprater-bottom-100"></div>
                    </div>