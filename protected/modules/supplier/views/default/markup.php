<?php $this->pageTitle =  'Company Information';?>
<?php $this->renderPartial('settingsidebar'); ?>
<div class="col-lg-9 p-a-0 messages-list bg-white flexbox-xs layout-column-xs full-height">
                        <div class="cleafix " style="padding: 30px 20px; ">
                          <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                           <h4 class="m-b-30 table-heading">List of Mark Up</h4>
                            <!--<p class="m-b-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, at.</p>-->

                           <?php if(Yii::app()->user->hasFlash('success')):?>
                            <div class="alert alert-success">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <?php echo Yii::app()->user->getFlash('success'); ?>
                            </div>
                            <?php endif; ?>
                            <?php if(Yii::app()->user->hasFlash('error')):?>
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <?php echo Yii::app()->user->getFlash('error'); ?>
                            </div>
                            <?php endif; ?>
                            
                           <table class="table m-b-40 without-border">
                              <thead class="thead-default">
                                <tr>
                                   <th>S.No</th>
                                   <th>Category</th>
                                   <th>Markup</th>
                                </tr>
                             </thead>
                             <tbody>
                             <?php $i=1;foreach($vendorMarkup as $value) { 
                                $cateSetting = Setting::model()->findByPk($value['category_id']);
                                $cateTitle = $cateSetting->title;?>
                                  <tr> 
                                    <td><?php echo $i;$i++;?></td>
                                     <td><?php echo $cateTitle;?></td>                                  
                                     <td><?php echo $value['mark_up'];?>%</td>
                                  </tr>
                                  <?php } ?>
                            </tbody>
                          </table>  <div class="clearfix"></div><br /><br /><br /><br /><br /><br /><br /><br />
                          </div></div></div>