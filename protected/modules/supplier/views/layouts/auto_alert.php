<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="http://www.simplifyvms.net/clientportal/msp/assets/img/logo-fav.png">
<title><?php echo $this->pageTitle; ?></title>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themeassets/template-assets/vendor/bower-jvectormap/jquery-jvectormap-1.2.2.css"/>
</head>
<body>
<?php $content;?>
</body>
</html>
