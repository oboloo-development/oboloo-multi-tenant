<?php
header ( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
header ( 'Last-Modified: ' . gmdate ( 'D, d M Y H:i:s' ) . ' GMT' );
header ( 'Cache-Control: no-store, no-cache, must-revalidate' );
header ( 'Cache-Control: post-check=0, pre-check=0', false );
header ( 'Pragma: no-cache' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="language" content="en" />
      <meta http-equiv="Pragma" content="no-cache">

        <title>
          <?php echo Yii::app()->params['title']; ?>
          <?php if (!empty($this->pageTitle)) echo ' - ' . CHtml::encode($this->pageTitle); ?>
            
          </title>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
              <link rel="shortcut icon" type="image/png" href="<?php echo AppUrl::bicesUrl('images/favicon.png'); ?>"/>

         <link rel="stylesheet" type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/build/css/jquery-confirm.min.css'; ?>" />

        <link rel="stylesheet" type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/bootstrap/dist/css/bootstrap.min.css'; ?>" />
        <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/nprogress/nprogress.css'; ?>" />
        <link rel="stylesheet" type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/animate.css/animate.min.css'; ?>" />
        <link rel="stylesheet" type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/build/css/custom.css'; ?>" />
        <link rel="stylesheet" type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/../js/validator/fv.css'; ?>" />
        <link rel="stylesheet" type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/bootstrap-daterangepicker/daterangepicker.css'; ?>" />
        <link rel="stylesheet" type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/fileinput.css'; ?>" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/jquery/dist/jquery.min.js'; ?>"></script>
      <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/jquery-confirm.min.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/bootstrap/dist/js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/fastclick/lib/fastclick.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/nprogress/nprogress.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/build/js/custom.js'; ?>"></script>

        
        <script src="//cdn.jsdelivr.net/npm/apexcharts@latest"></script>
        <script src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/apexcharts/assets/stock-prices.js'; ?>"></script>

        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/moment/min/moment.min.js' ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/bootstrap-daterangepicker/daterangepicker.js'; ?>"></script>

        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/bices.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/jquery.validate.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/additional-methods.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/duplicateFields.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/fileinput.js'; ?>"></script>
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/bootstrap-confirmation.min.js'; ?>"></script>

        <!-- Datatables -->
        <link type="text/css"
          href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-bs/css/dataTables.bootstrap.min.css'; ?>"
          rel="stylesheet">
          <link type="text/css"
            href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'; ?>"
            rel="stylesheet">
            <link type="text/css"
              href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'; ?>"
              rel="stylesheet">
              <link type="text/css"
                href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'; ?>"
                rel="stylesheet">

                <!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net/js/jquery.dataTables.min.js'); ?>"></script> -->
                <script type="text/javascript"
                  src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript"
                  src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
                <script type="text/javascript"
                  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                <script type="text/javascript"
                  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
                <script type="text/javascript"
                  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
                <script type="text/javascript"
                  src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

                <script type="text/javascript"
                  src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-bs/js/dataTables.bootstrap.min.js'; ?>"></script>
                <!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script> -->
                <script type="text/javascript"
                  src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'; ?>"></script>
                <script type="text/javascript"
                  src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-buttons/js/buttons.flash.min.js'; ?>"></script>
                <!-- <script type="text/javascript" src="<?php echo AppUrl::jsUrl('datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script> -->
                <script type="text/javascript"
                  src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-buttons/js/buttons.print.min.js'; ?>"></script>
                <script type="text/javascript"
                  src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'; ?>"></script>
                <script type="text/javascript"
                  src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-keytable/js/dataTables.keyTable.min.js'; ?>"></script>
                <script type="text/javascript"
                  src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-responsive/js/dataTables.responsive.min.js'; ?>"></script>
                <script type="text/javascript"
                  src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/datatables.net-responsive-bs/js/responsive.bootstrap.js'; ?>"></script>

                <link rel="stylesheet" type="text/css"
                  href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.css">
                  <script type="text/javascript"
                    src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
                  <script type="text/javascript"
                    src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>
                  <script type="text/javascript"
                    src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                  <script type="text/javascript"
                    src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

                  <link rel="stylesheet" type="text/css"
                    href="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/css/pivot.css'; ?>">
                    <script type="text/javascript"
                      src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/pivot.js'; ?>"></script>
                    <script type="text/javascript"
                      src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/d3_renderers.js'?>"></script>
                    <script type="text/javascript"
                      src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/c3_renderers.js' ?>"></script>
                    <script type="text/javascript"
                      src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/js/export_renderers.js' ?>"></script>

<style>
.dt-button {
  margin-left: 10px !important;
}
body {
  background-color: #fff;
}
</style>
<script type="text/javascript">
  moment.updateLocale('en', {
      weekdaysMin : ["S", "M", "T", "W", "T", "F", "S"]
    });
  var BICES = window.BICES ||
  {
    foo: function()
    {
    }
  };
BICES.Options = { baseurl: '<?php echo Yii::app()->request->getBaseUrl(true); ?>' };
</script>
<script type="text/javascript">
var g_gTranslateIsAdded = false;
function googleTranslateElementInit() {
    if (!g_gTranslateIsAdded ) {
        g_gTranslateIsAdded = true;
        new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'ar,zh-CN,zh-TW,nl,en,fr,de,hi,it,ja,pl,pt,es,th,ko'}, 'google_translate_element');
    }
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 
</head>
  <?php
  $user = new User ();
  if (true){
   if($this->action->id =='home'){
          $bgClass = "homebgbody";
    }else{
        $bgClass = "";
    }
    // Start: Check login for quote, document etc
      $quoteReference = Yii::app()->user->getState('quote_reference');
      $quoteID = Yii::app()->user->getState('quote_id'); 
      if(!empty($quoteReference) && !empty($quoteID)){
      }
    // End: Check login for quote, document etc
  ?>
  <body class="nav-md <?php echo $bgClass;?>" style="zoom:85% !important">
  <div class="container body">
    <div class="" style="width: 94%;margin: auto;">
      
      <!-- top navigation -->
      <div class="top_nav">
        <div  class="pull-left">
          <div class="navbar nav_title" style="border: 0;">
            <!-- <a href="<?php echo AppUrl::bicesUrl('index.php/default/index'); ?>">  -->
             <img src="<?php echo $this->createUrl('/images/app_supplier_logo.png'); ?>"
              class="img-responsive" />
            <!-- </a> -->
          </div>
          <div class="clearfix"><br /></div>
          <div class="profile" style="min-height: 40px;width: 230px;text-align: center;">
            <h1></h1>
          </div>
          <!-- /menu profile quick info -->
        </div>
        <div  class="pull-right" id="google_translate_element" style="float: left;margin-top: 7px;margin-left: 41px;"></div>
        <div class="link-site pull-right">
            <a class="btn btn-success" style="margin-top: 10px;" href="https://oboloo.com/supplier-support-hub" target="_blank"> Training & Support</a>
          </div>
        </div>
      </div>
      <!-- /top navigation -->
      <?php echo $content; ?>
     </div>
    </div>
   </div>
  </div>
  <!-- /page content -->

  <!-- footer content -->
  <footer> <!-- multi select -->
  <link type="text/css"
    href="<?php echo AppUrl::cssUrl('../js/select2/dist/css/select2.min.css'); ?>"
    rel="stylesheet">
    <script type="text/javascript"
      src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/../js/select2/dist/js/select2.full.min.js'; ?>"></script>

    <!-- nice looking checkboxes -->
    <link type="text/css"
      href="<?php echo AppUrl::cssUrl('../js/iCheck/skins/flat/green.css'); ?>"
      rel="stylesheet">
      <script type="text/javascript"
        src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/../js/iCheck/icheck.min.js'; ?>"></script>
      <script
        src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/../js/devbridge-autocomplete/dist/jquery.autocomplete.min.js' ?>"></script>

      <!-- Multiple file upload -->
      <link type="text/css"
        href="<?php echo AppUrl::cssUrl('../js/dropzone/dist/min/dropzone.min.css'); ?>"
        rel="stylesheet">
        <script type="text/javascript"
          src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/../js/dropzone/dist/min/dropzone.min.js'; ?>"></script>

        <!--
    <script type="text/javascript" src="<?php echo AppUrl::jsUrl('jszip/dist/jszip.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo AppUrl::jsUrl('pdfmake/build/pdfmake.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo AppUrl::jsUrl('pdfmake/build/vfs_fonts.js'); ?>"></script>
    -->

      <div class="clearfix"></div>
  
  </footer>
  <!-- /footer content -->
  </div>
  </div>
  <style type="text/css">
   body {
    font-family: 'Poppins';
    }
  </style>

</body>

  <?php } ?>

</html>



<?php

 if($this->action->id =="edit" && $this->id =="quotes"){ ?>
<script type="text/javascript">
  
    content_height = 2732;
      $('.left_col').height(content_height);

</script>
<?php }else if($this->action->id !="dashboard"){?>

  <script type="text/javascript">


    $('#contract_table,#order_table,#expense_table,#expense_table,#routing_table,#invoice_table,#quote_table,#user_table').on( 'length.dt', function ( e, settings, len ) {
      
    content_height = 3584;
    content_height += 100;
    var menu_height = $('.left_col').height(3684);

    
  });

  $(document).ajaxStop(function() {
    var content_height = $(document).height();
    content_height += 100;
    var menu_height = $('.left_col').height();

    if (menu_height < content_height) $('.left_col').height(content_height);

  });
</script>
<?php } else { ?>
<script type="text/javascript">
  $(document).ajaxStop(function() {
    var content_height = $(document).height();
    content_height += 308;
      $('.left_col').height(content_height);

  });
</script>
<?php } ?>
<script type="text/javascript">

  $(window).load(function(){
      $(".goog-logo-link").empty();
      $('.goog-te-gadget').html($('.goog-te-gadget').children());
    });
  
  $(document).ready(function() {


  /*$("#supplier_type_area").outerHeight($(window).height()-$("#supplier_type_area").offset().top- Math.abs($("#supplier_type_area").outerHeight(true) - $("#supplier_type_area").outerHeight()));
$(window).on("resize", function(){                $("#supplier_type_area").outerHeight($(window).height()-$("#supplier_type_area").offset().top- Math.abs($("#supplier_type_area").outerHeight(true) - $("#canvas").outerHeight()));
});

*/
    
    var content_height = $(document).height();
    content_height += 100;
    var menu_height = $('.left_col').height();
    if (menu_height < content_height) $('.left_col').height(content_height);

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var content_height = $(document).height();
      content_height += 100;
      var menu_height = $('.left_col').height();
      if (menu_height < content_height) $('.left_col').height(content_height);
    });
    
    $('.current_option').each(function() {
      if ($(this).parent().parent().hasClass('child_menu'))
      {
        var menu_expanded = false;
        
        $(this).parent().parent().parent().find('a').each(function() {
          if (!menu_expanded) {
            menu_expanded = true;
            $(this).trigger('click');
          }
        });
        
        if (menu_expanded) 
        {
          //$(this).parent().css('background', '#12A79C');
          $(this).css('color', '#fff');
          $(this).css('font-size', '110%');
        }
      }
      else 
      {
        $(this).parent().addClass('active');
        $(this).css('font-size', '120%');
      }
    });
  });

function changeCurrency()
{
  $('#currency_dashboard_form').submit();
}

function changeLanguageLabel(){
	$('.goog-te-combo option:contains("Arabic")').text(" العربية ");
	$('.goog-te-combo option:contains("Chinese (Simplified)")').text('简体中文');
	$('.goog-te-combo option:contains("Chinese (Traditional)")').text('中國傳統的');
	$('.goog-te-combo option:contains("Dutch")').text('Nederlands');
	$('.goog-te-combo option:contains("French")').text('Français');
	$('.goog-te-combo option:contains("German")').text('Deutsche');
	$('.goog-te-combo option:contains("Hindi")').text('हिन्दी');
	$('.goog-te-combo option:contains("Italian")').text('Italiana');
	$('.goog-te-combo option:contains("Japanese")').text('Português');
	$('.goog-te-combo option:contains("Polish")').text('Polskie');
	$('.goog-te-combo option:contains("Portuguese")').text('Português');
	$('.goog-te-combo option:contains("Spanish")').text('Español');
}

$(document).ready(function(){
	
	$("body").on("change", "#google_translate_element select", function (e) {
		
		changeLanguageLabel();
        
		setTimeout(function(){ 
				changeLanguageLabel();
		}, 100);
		setTimeout(function(){ 
				changeLanguageLabel();
		}, 500);
		setTimeout(function(){ 
				changeLanguageLabel();
		}, 2000);
		setTimeout(function(){ 
			changeLanguageLabel();
	}, 2500);
	});
	

	setTimeout(function(){ 
			changeLanguageLabel();
	}, 100);
	setTimeout(function(){ 
			changeLanguageLabel();
	}, 500);
	setTimeout(function(){ 
			changeLanguageLabel();
	}, 2000);
	
	setTimeout(function(){ 
			changeLanguageLabel();
	}, 2500);
});


</script>
<style>
  .goog-logo-link {
   display:none !important;
}

.goog-te-gadget {
   color: transparent !important;
}

.goog-te-gadget .goog-te-combo {
   color: blue !important;
}
.goog-te-banner-frame.skiptranslate {
    display: none !important;
    } 
body {
    top: 0px !important; 
    }
    /*.goog-logo-link {
        display: none;
    }
    .goog-te-banner-frame.skiptranslate {
            display: none !important;
        }*/
    .goog-te-combo, .goog-te-banner *, .goog-te-ftab *, .goog-te-menu *, .goog-te-menu2 *, .goog-te-balloon * {
      font-family:Poppins !important;
      font-size: 12px;
    }

  .goog-te-gadget .goog-te-combo {
      color: #555 !important;
      border: none;
      border-bottom: 1px solid #eee;
      -webkit-box-shadow: none;
      padding: 5px 5px 9px 5px;
      box-shadow: none;
  }
  .goog-tooltip {
    display: none !important;
}
.goog-tooltip:hover {
    display: none !important;
}
.goog-text-highlight {
    background-color: transparent !important;
    border: none !important; 
    box-shadow: none !important;
}
.left_col {
  border-right: none;
}
</style>
