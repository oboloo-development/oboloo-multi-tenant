<?php $this->renderPartial('_header',array('model'=>$model));?>
<!-- Tab panes -->
<div class="tab-content">

<div role="tabpanel" class="tab-pane active" id="profile">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<a data-toggle="modal" href='#submision-job-select' class="btn btn-sm btn-success">New Submission</a>
</div> <!-- col -->
</div> <!-- row -->

<hr>

<h4>Submissions</h4>

<table class="table table-striped dataTable no-footer v-middle-table">
<thead>
  <tr>
     <th>S.No</th> 
     <th>Name</th>
     <th>Email & Phone</th>
     <th>Rates</th>
     <th>Status</th>
     <th>Vendor Manager</th>
     <th style="text-align: center">Action</th>
  </tr>
</thead>
<tbody>
<?php error_reporting(0);$i=1; foreach($vendorSubmission as $value){

      $candidateModel = Candidates::model()->findByPk($value->candidate_Id);
      if($vendorSubmission) {
      $vendorSubmission = VendorJobSubmission::model()->findByAttributes(array('candidate_Id'=>$candidateModel->id));

       $vendorSubmission = VendorJobSubmission::model()->findByAttributes(array('candidate_Id'=>$candidateModel->id));

       $vendor = Vendor::model()->findByAttributes(array('member_type'=>'Account Manager','id'=>$candidateModel->default_contact_id));
     }

     // $vendorSubmission->pay_rate
  ?>
  
  <tr>
    <td><?php echo $i++;?></td>
    <td>
      <div class="avatar-small-text"><span><?php echo $candidateModel->first_name[0].$candidateModel->last_name[0];?></span></div>
       <span class="name"><?php echo $candidateModel->first_name.' '.$candidateModel->last_name;?></span> 
     </td>
    <td><?php echo $candidateModel->email;?>, <?php echo $candidateModel->phone;?></td>
    <td><?php echo "$".$vendorSubmission->pay_rate;?></td>
    <td><span class="label label-<?php echo $value->resume_status=='Rejected'?'default':'success'?>"><?php $value->resume_status;?></span></td>
    <td><?php if($vendor){
                  echo $member_type->first_name.' '.$member_type->last_name;
                }else{
                  echo 'NULL';
                }?></td>
     <td style="text-align: center">
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Admin/candidates/candidateInfo',array('id'=>$value->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Workspace"><i class="fa fa-bars"></i></a>
        <a href="<?php echo Yii::app()->createAbsoluteUrl('Admin/candidates/deletecandidate',array('id'=>$value->id)); ?>" data-placement="top" data-toggle="tooltip" class=" tooltips" data-original-title="Delete"><i class=" ti-trash"></i></a>
     </td>
  </tr>
  <?php } ?>



  

</tbody>
</table>

</div> <!-- profile -->

</div>
</div>

</div> <!-- row -->


</div>
<!-- add-job -->
</div>
</div>
<!-- row -->

<div class="seprater-bottom-100"></div>
</div>
</div>
</div>


<div class="modal fade" id="submision-job-select">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Select Job</h4>
      </div>
         <form action="<?php echo $this->createUrl('/Admin/vendor/createsubmission');?>" method="post" role="form">
      <div class="modal-body">

      <?php $vendorJobs =  VendorJobs::model()->findAll(array('condition'=>'vendor_id='.$model->id));

      if($vendorJobs){


      ?>
     
          
        <input type="hidden" name="id" value="<?php echo $model->id;?>">
          <div class="form-group">
            <label for=""> Select Job</label>
             
             <div class="single">
                <select name="job-id" class="ui fluid search dropdown">
                 <option value=""></option>
                <?php foreach($vendorJobs as $value){
                    $job = Job::model()->findByPk($value->job_id);
                  ?>  
                  <option value="<?php echo $job->id;?>"><?php echo $job->title;?></option>
                  <?php } ?>
                     
                }
                </select>
              </div>
          </div>
        
          <?php } ?>
       
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Continue</button>
        <a type="button" class="btn btn-default" data-dismiss="modal">Close</a>
        
      </div>
      </form>
    </div>
  </div>
</div>

