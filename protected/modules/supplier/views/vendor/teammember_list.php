<?php $this->pageTitle =  'Team Members';?>
<div class="right_col" role="main">
  <div class="row-fluid tile_count col-md-12">
    <div class="span6 pull-left">
        <h3>Team Members</h3>
    </div>
    <div class="span6 pull-right">
        <a href="<?php echo $this->createAbsoluteUrl('vendor/create'); ?>" class="btn btn-success">Add New Team Member</a>
    </div>
    <div class="clearfix"> </div><br />
  </div>
 <div class="clearfix"> </div><br />
    <?php if(Yii::app()->user->hasFlash('error')): ?>
    <div class="alert alert-danger" role="alert">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php elseif (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
  <?php endif; ?>


<div class="clearfix">
  <div>
    
    <table class="table m-b-40 without-border">
      <thead class="thead-default">
        <tr>
           <th>S.No</th>
           <th class="notranslate">Name</th>
           <th class="notranslate">Email</th>
           <!-- <th>Staff Type</th> -->
           <th>Status</th>
           <th style="text-align: center">Action</th>
        </tr>
     </thead>
     <tbody>
     	<?php $i = 1;
      foreach ($model as $key => $value) {
       $memberTyoe = Yii::app()->params['teamember_type'];
      ?>
      <tr>
      <td><?php echo $i; ?></td>
      <td class="notranslate"><?php echo $value['vendor_name']; ?></td>
      <td class="notranslate"><?php echo $value['emails'] ?></td>
      <!-- <td><?php echo $memberTyoe[$value['member_type']]; ?></td> -->
      <td>
      <?php if ($value['active'] == '1') { ?>
      <span class="label label-info">Active</span>
      <?php } if ($value['active'] == '0') { ?>
      <span class="label label-danger">Inactive</span>
      <?php }?></td>

      <td style="text-align: center"><a data-original-title="Edit" class=" tooltips"data-toggle="tooltip" data-placement="top"href="<?php echo $this->createAbsoluteUrl('vendor/updatestaff',array('id'=>$value['vendor_id'])); ?>"><i class="fa fa-pencil"></i></a>
      <a href="<?php echo $this->createAbsoluteUrl('vendor/remove', array('id' => $value['vendor_id'])) ?>" data-toggle="tooltip" data-placement="top" title="Delete" class="icon" onclick="return confirm('Are you sure you want to delete this member?');"><i class="fa fa-remove"></i></a>
      </td>
      </tr><?php $i++;} ?>
  </tbody>
  </table>
  </div> <!-- col -->
</div> <!-- row -->
<div class="seprater-bottom-100"></div>
<div class="seprater-bottom-100"></div>
</div>
