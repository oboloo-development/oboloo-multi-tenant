<div class="" role="main" style="width: 94%;margin: auto;">
  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12 col-xs-12 tile_count"> <br />
    <div class="span6" style=" margin-left: 19px;">
       <h4>  <strong>Supplier Documents</strong> <br /><br />
            <?php
             if(isset($documentType)){?>
              <br /><br /><strong>Document Type </strong>: <span class="title-text"><?php echo FunctionManager::vendorDocument($documentType);?></span>
            <?php 
             }
           
            ?>
           </h4><br /></div>
    

    <div class="clearfix"> </div>
  </div>


<div class="col-md-12 col-sm-12 col-xs-12">
  
<div class="form-group" id="document_area_1">
  <div class="clearfix"></div>
  <div id="file_upload_alert" class="alert alert-success" role="alert"></div>
  <img class="processing_oboloo" src="<?php echo '//'.$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.'/../images/icons/processin_oboloo.gif'; ?>" style="display: block;text-align: center;position: absolute;z-index: 9;left: 40%;">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <label class="control-label">Document  <span style="color: #a94442;">*</span></label>
        <input class="form-control notranslate" type="file" name="order_file_1" id="order_file_1" />
        <span class="text-danger" style="font-size: 10px;">Maximum File Size of 100MB</span>
    </div>
   <div class="col-md-4 col-sm-4 col-xs-12">
        <label class="control-label">Document Title  <span style="color: #a94442;">*</span></label>
        <input type="text" class="form-control notranslate" name="file_desc_1" id="file_desc_1" placeholder="File/Document Description" /> 
    </div>
    
    <div class="col-md-3 col-sm-3 col-xs-12">
    <label class="control-label">Expiry Date  <span style="color: #a94442;">*</span></label>
    <input type="text" class="form-control expiry_date_1 notranslate" name="expiry_date_1" id="expiry_date_1" placeholder="Expiry Date">
    </div>    
    <div class="col-md-1 col-sm-1 col-xs-12 text-left"> <br />
    <button id="btn_upload_1" class="btn btn-info notranslate submit-btn" onclick="uploadDocument(1,event);">Upload</button>
</div>
</div><div class="clearfix"></div><br /><br />
<div id="document_list_cont">
  <?php $this->renderPartial('_documents',array('documentList'=>$documentList,'vendorID'=>$vendorID));?>
</div>


<script type="text/javascript">
function uploadDocument(uploadBtnIDX,e)
{  
  e.preventDefault();
  var uploadedObj = $('#order_file_'+uploadBtnIDX);
  var uploadedObjDesc = $('#file_desc_'+uploadBtnIDX); 
  var uploadedDocStatus = $('#document_status_'+uploadBtnIDX);
  var uploadedDocExpiryDate = $('#expiry_date_'+uploadBtnIDX);


  var file_data = $('#order_file_'+uploadBtnIDX).prop('files')[0];
  var field_data = $('#file_desc_'+uploadBtnIDX).val();
  var field_type = '<?php echo $documentType;?>';
  if($('#document_status_'+uploadBtnIDX).is(':checked')){
    var field_status = 'Approved';
  }else{
    var field_status = 'Pending';
  }
  var expiry_date = $('#expiry_date_'+uploadBtnIDX).val();

   if(typeof file_data === "undefined" || file_data==""){
    $.alert({title: 'Required!',content: 'Document is required',});
   }else if(field_data==""){
    $.alert({title: 'Required!',content: 'Document Title is required',});
   }else if(field_type==""){
    $.alert({title: 'Required!',content: 'Document Type is required',});
  }else if(expiry_date==""){
    $.alert({title: 'Required!',content: 'Expiry Date is required',});
   }else{
  var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('file_description', field_data);
    form_data.append('document_type',field_type);
    form_data.append('vendor_id',$('#vendor_id').val());
    form_data.append('document_status',field_status);
    form_data.append('expiry_date',expiry_date);                           
    $.ajax({
        url: '<?php echo $this->createUrl('vendor/uploadDocument')?>', // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        beforeSend: function() {
          $("#file_upload_alert").html('');
          $("#file_upload_alert").hide();
          $(".processing_oboloo").show();
          $("#btn_upload_1").prop('disabled',true);
        }, 
        success: function(uploaded_response){
          $("#document_list_cont").html(uploaded_response);
          uploadedObj.val(null);
          uploadedObjDesc.val(null);
          
          uploadedDocExpiryDate.val(null);

          $("#file_upload_alert").show();
          $("#file_upload_alert").html('File uploaded successfully');
          $(".processing_oboloo").hide();
          $("#btn_upload_1").prop('disabled',false);
          
            // display response from the PHP script, if any
        }
     });
    }
}



function deleteDocument(documentID)
{
  var docID = documentID;
  $('#delete_document_' + documentID).confirmation({
    title: "Are you sure you want to delete the document?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->createUrl('vendor/deleteDocument/'); ?>",
            data: { documentID: docID },
            success: function(uploaded_response){
              $("#document_list_cont").html(uploaded_response);
            }
        });
    },
    onCancel: function() {  }
  });
  
}

function deleteDocumentOther(documentID)
{
  var docID = documentID;
  $('#delete_document_' + documentID).confirmation({
    title: "Are you sure you want to delete the document?",
    singleton: true,
    placement: 'right',
    popout: true,
    onConfirm: function() {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->createUrl('vendor/deleteDocumentOther/'); ?>",
            data: { documentID: docID },
            success: function(uploaded_response){
              $("#document_list_cont").html(uploaded_response);
            }
        });
    },
    onCancel: function() {  }
  });
}
   var today = new Date(); 
$('.expiry_date_1').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    minDate:today,
    locale: {
    format: 'DD/MM/YYYY'
    }
  });

  $('.expiry_date_1').on('apply.daterangepicker', function(ev, picker) {
    $('#expiry_date_1').val(picker.startDate.format('DD/MM/YYYY')); 
  });

  $(document).ready( function() {

    $("#file_upload_alert").hide();
    $(".processing_oboloo").hide();

    $('#order_file_1').bind('change', function() {
     var MAX_FILE_SIZE = 101 * 1024 * 1024;
     if(this.files[0].size > MAX_FILE_SIZE){
        alert("Maximum File Size of 100MB");
        this.value = "";
       }    
    });
  });
</script>