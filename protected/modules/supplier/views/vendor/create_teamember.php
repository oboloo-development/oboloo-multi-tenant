<!--Hover table-->
 <?php $this->pageTitle =  'Add Team Member';?>
  <div class="right_col" role="main">
  <div class="row-fluid tile_count col-md-11">
    <div class="span6 pull-left">
        <h3>Add Team Member</h3>
    </div>
    <div class="span6 pull-right">
        <a href="<?php echo $this->createAbsoluteUrl('vendor/list'); ?>" class="btn btn-success">Member list</a>
    </div>
    <div class="clearfix"> </div><br />
    <?php if(Yii::app()->user->hasFlash('error')): ?>
    <div class="alert alert-danger" role="alert">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php elseif (Yii::app()->user->hasFlash('success')): ?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
  <?php endif; ?>
      <div class="cleafix">
          <?php $form = $this->beginWidget('CActiveForm',array('id' => 'staff-form', 'enableAjaxValidation' => true, 'htmlOptions' => array('role' => 'form', 'id' => 'test', 'enctype' => 'multipart/form-data'),)); ?>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="two-flieds-2">
            <div class="form-group">
              <label for="">Name <span style="color: #a94442;">*</span></label> 
              <input type="text" name="vendor_name"class="form-control notranslate" placeholder="" required="required">
            </div>
          </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           <div class="form-group"><label for="">Email <span style="color: #a94442;">*</span></label> 
            <input text="email" name="emails" class="form-control notranslate" id="" placeholder="" required="required">
          </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group"><label for="">Phone Number</label><input type="text" name="phone1" class="form-control notranslate" id="" placeholder="" ></div>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
         <div class="form-group">
            <label for="">Status</label>
            <div class="single">
              <?php  $status = Yii::app()->params['teamember_status'];?>
          <select class="form-control notranslate" name="active">
            <option value="1">Active</option>
            <option value="0">Inactive</option>
        </select></div></div></div><div class="clearfix"></div><br /><br />
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-right text-right">
               <button type="submit" name="Staff" class="btn btn-success">Add New Staff</button>
               <button type="reset" class="btn btn-default">Cancel</button>
        </div><div class="clearfix"></div>
        <?php $this->endWidget(); ?>
        </div>
        <div class="seprater-bottom-100"></div>
    </div>