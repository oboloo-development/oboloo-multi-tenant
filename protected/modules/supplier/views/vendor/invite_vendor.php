<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12 m-b-20">
    <h4 class="page-title" style="display: inline-block">Sr UX Design</h4>
    <a href="" class="pull-right">
    <h4 class="page-title"  style="display: inline-block;font-size: 15px;">Back to Job Liting</h4>
    </a> 
    <!-- <button type="button" class="btn btn-default waves-effect w-md waves-light m-b-5 pull-right">Back to Job Liting</button> --> 
  </div>
</div>
<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="card-box team-member">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
          <ul class="navigation-menu list-inline">
            <li><a href="">JOB REPORTS</a></li>
            <li><a href="">JOB DETAIL</a></li>
            <li><a href="">SUBMISSION</a></li>
            <li><a href="">CANDIDATES</a></li>
            <li><a href="">SUPPLIER / VENDOR</a></li>
            <li><a href="">NOTES & COMMENTS</a></li>
            <li><a href="">RATES & CONTRACT</a></li>
            <li><a href="">TEAM MEMBERS</a></li>
          </ul>
        </div>
      </div>
      <div class="center-block" style="margin-left: -20px;  margin-right: -20px; " >
        <div class="border-bottom-pink"></div>
      </div>
      <div class="sep-10"></div>
      <div class="row">
        <div class="col-md-12">
          <div class="card-box">
            <div class="form-group row m-0">
              <form method="post">
              
              <div class="col-md-3">
                <label>Select Country</label>
                <select id="supplier_country" name="country" class="form-control" required="required">
                	<option value=""></option>
                	<?php 
					$countries = Countries::model()->findAll(); 
					foreach($countries as $key=>$value){ ?>
                  		<option value="<?php echo $value->id; ?>" 
						<?php if(isset($_POST['country'])){ echo $_POST['country']== $value->id?'selected':''; } ?> >
						<?php echo $value->name; ?>
                        </option>
                    <?php } ?>
                </select>
              </div>
              
              <div class="col-md-3">
                <label>Select State</label>
                <select id="supplier_states" name="state" class="form-control" required="required">
                	<option value=""></option>
                </select>
              </div>
              <div class="col-md-3">
                <label>Select Group</label>
                <select name="group" id="supplier_group"  class="form-control" required="required">
                  <option value=""></option>
                </select>
              </div>
              <div class="col-md-2">
                <label>&nbsp;</label>
                <input type="submit" name="find_supplier" class="btn btn-primary waves-light m-t-25" value="Show Supplier">
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        
        <?php if(Yii::app()->user->hasFlash('success')):?>
                    <div class="info" style="color:green;">
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
        <?php endif; ?>
        
          <?php if($modelVendor){ ?>
          
                
          <table class="table table-bordered m-0 vendor-invite">
            <thead>
              <tr>
                <th style="width: 6%;">Invite</th>
                <th>Suppiler Name</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($modelVendor as $vendorKey=>$vendorVal){ ?>
              <tr>
                <td>
                <form method="post">
                	<div class="checkbox checkbox-primary">
                        <input id="checkbox1" name="vendor_id[]" type="checkbox" value="<?php echo $vendorVal->id ?>">
                        <label for="checkbox1"> </label>
                        
                        <input id="job_id" name="job_id" type="hidden" value="<?php echo $_GET['job_id']; ?>">
               		</div>
               </td>
                <td><?php echo $vendorVal->organization ?><!--, 3458,5498-->, <?php echo $vendorVal->department ?> & <?php echo $vendorVal->state ?></td>
              </tr>
           <?php }
		   		$jobData = Job::model()->findByPk($_GET['job_id']);
		    	?>   
            </tbody>
          </table>
               <input type="submit" onclick="return submission(<?php echo $jobData->number_submission; ?>)" name="invite_supplier" class="btn btn-primary waves-light m-t-25" value="Invite">
               </form>
          <?php } ?>
         
        </div>
      </div>
      <!-- row -->
      
      <div class="seprater-bottom-100"></div>
      <div class="center-block" style="margin-left: -20px;  margin-right: -20px; " >
        <div class="border-bottom-gray"></div>
      </div>
    </div>
    <!-- end card-box --> 
  </div>
  <!-- end col --> 
</div>

<style type="text/css">
    .team-member hr { margin-top: 0; }
    .team-member .card-box.widget-user {     border: 0;   }
    .team-member .navigation-menu { margin-bottom: 4px; }
    .team-member .navigation-menu > li > a {
        display: block;
        color: #797979;
        font-weight: 500;
        -webkit-transition: all .3s ease;
        transition: all .3s ease;
        line-height: 20px;
        padding-left: 10px;
        padding-right: 10px;
        font-weight: bold; 
    }

     .team-member .navigation-menu > li > a:hover, .team-member .navigation-menu > li > a:focus  {
        color: #ef5350;
    }       
    .team-member.card-box {     border-top: 3px solid #7266BA;  }
    .border-bottom-pink { display: inline-block; width: 100%; height: 1px;    border-bottom: 1px solid #7266BA; margin-left: auto; margin-right: auto; }
    .border-bottom-gray { display: inline-block; width: 100%; height: 1px;    border-bottom: 1px solid #ddd; margin-left: auto; margin-right: auto; }
    .sep-10 { display: inline-block; width: 100%; height: 1px; margin-bottom: 10px;    margin-left: auto; margin-right: auto; }

    ul.users img {  width: 40px; height: auto; margin-bottom: 14px;  }

    .skills .label { 

            display: inline-block;
            padding: 5px;
            font-size: 12px;
            margin-right: 2px;
            margin-bottom: 5px; 

     }

    table.table-description td {     border-bottom: 1px solid #f3f3f3; }
    table.table-description span {   color: #98a6b0;  font-size: 14px;  }
    

    div.address { padding-left: 30px; padding-right: 30px; padding-bottom: 40px; padding-top: 10px;}

    .m-t-25 {
      margin-top: 25px; 
    }
    table.vendor-invite tr  td {
        vertical-align: middle;
    }
       table.vendor-invite td .checkbox { 
          padding-left: 39px;
       }
    
}
    
</style>

<script>
	function submission(e){
		alert(e + " Number of submission!");
		}
</script>


