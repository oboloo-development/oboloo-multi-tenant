<?php $this->pageTitle = 'Edit ' . $model->vendor_name; ?>
     <div class="right_col" role="main">
  <div class="row-fluid tile_count col-md-11">
    <h3 class="pull-left">Edit <?php echo $model->vendor_name;?></h3>

     <div class="span6 pull-right">
        <a href="<?php echo $this->createAbsoluteUrl('vendor/list'); ?>" class="btn btn-success">Member list</a>
    </div>
    <div class="clearfix"></div> <br />
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php endif; ?>

    <?php $form = $this->beginWidget('CActiveForm',
			 array('id' => 'staff-form', 'enableAjaxValidation' => true, 'htmlOptions' => array('role' => 'form', 'id' => 'test', 'enctype' => 'multipart/form-data'),)); ?>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="two-flieds-2">
          <div class="form-group">
          <label for="">Supplier Name </label> 
          <input type="text" name="Vendor[vendor_name]" readonly class="form-control notranslate" placeholder="" value="<?php echo $model->vendor_name;?>" required="required">
          </div>

       </div>
     </div> <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <div class="form-group"><label for="">Email <span style="color: #a94442;">*</span></label> 
       <input type="email" name="Vendor[emails]" readonly class="form-control notranslate" id="" placeholder="" value="<?php echo $model->emails;?>" required="required">
       </div>
     </div>
     </div>
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Phone Number</label> 
       <input type="text" name="Vendor[phone_1]" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->phone_1;?>" required="required">
       </div></div>
      <!-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Company Assigned Supplier ID</label> 
       <input type="text" name="external_id" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->external_id;?>" required="required">
       </div></div>-->
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Address Line 1</label> 
       <input type="text" name="Vendor[address_1]" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->address_1;?>" required="required">
       </div></div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Address Line 2</label> 
       <input type="text" name="Vendor[address_2]" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->address_2;?>" required="required">
       </div></div>

       <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Company Number</label> 
       <input type="text" name="Vendor[number]" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->number;?>" required="required">
       </div></div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Tax Number</label> 
       <input type="text" name="Vendor[tax_number]" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->tax_number;?>" required="required">
      </div></div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">City</label> 
       <input type="text" name="Vendor[city]" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->city;?>" required="required">
      </div></div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">County</label> 
       <input type="text" name="Vendor[state]" class="form-control" id="" placeholder="" value="<?php echo $model->state;?>" required="required">
      </div></div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Country</label> 
       <input type="text" name="Vendor[country]" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->country;?>" required="required">
      </div></div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Zip/Postal Code</label> 
       <input type="text" name="Vendor[zip]" class="form-control notranslate" id="" placeholder="" value="<?php echo $model->zip;?>" required="required">
      </div></div>


       <div class="clearfix"></div><br />

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-right text-right">
       <button type="submit" name="Staff" class="btn btn-success">Edit</button>
       </div> 
    <?php $this->endWidget(); ?>
     
    
  </div> <!-- col -->

   <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
     <!--<h4 class="m-b-10">Excepteur sint</h4>

    <p>Lorem ipsum dolor sit amet, consectetur adipisici</p>-->
  </div> <!-- col -->



<div class="seprater-bottom-100"></div>
</div>