    
      <?php foreach($documentList as $key=>$value) {
         $vendor_id = UtilityManager::getVendorID();
          $sql = " SELECT * from vendor_documents where vendor_id=$vendor_id and  document_type=".$value['document_type']." order by id DESC";
          $documentDetailReader = Yii::app()->db->createCommand($sql)->query()->readAll();
          $documentType = $value['document_type'];
        ?>
      <table id="" class="table table-striped table-bordered" style="width: 100%;">
      <thead>
      <tr><th colspan="6"><?php echo FunctionManager::vendorDocument($documentType);?></th></tr>
      <tr>
          <th style="min-width: 15%">Document Type</th>
          <th style="width: 25%">Document Title</th>
          <th style="width: 15%">Document</th>
          <th style="width: 10%">Approved</th>
          <th style="width: 12%">Expiry Date</th>
          <th style="width: 10%">Uploaded Date</th>
          <!-- <th style="width: 10%">Action</th> -->
      </tr>
      </thead>
      <tbody>
        <?php foreach($documentDetailReader as $detailKey=>$detailValue){?>
        <tr>
          <td><span class="btn btn-success doc-btn-round" style="<?php echo FunctionManager::vendorDocumentBgColor($documentType);?>"><?php echo FunctionManager::vendorDocument($documentType);?></span></td>
          <td style='word-break:break-all'><?php echo $detailValue['document_title'];?></td>
          <td style='word-break:break-all'><a href="<?php echo Yii::app()->createAbsoluteUrl('supplier/vendor/documentDownload',array('id'=>$detailValue['id']));  ?>"><?php echo $detailValue['document_file'];?></a></td>
          <td><input type="checkbox"  <?php echo strtolower($detailValue['status'])=='approved'?'checked="checked"':'';?> value="<?php echo $detailValue['id'];?>" disabled="disabled" /></td>
          <td><?php echo  $detailValue['expiry_date'] !='0000-00-00'?date("d/m/Y", strtotime($detailValue['expiry_date'])):'';?></td>
          <td><?php echo date("d/m/Y",strtotime($detailValue['date_created']));?></td>
         <!--  <td>
            <?php if(strtolower($detailValue['status'])=='pending' && !empty($detailValue['id'])){?>
            <a href="<?php echo Yii::app()->createAbsoluteUrl('vendor/documentDownload',array('id'=>$detailValue['id']));  ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
            <a style="cursor: pointer; padding: 5px;" id="delete_document_<?php echo $detailValue['id'];?>" onclick="deleteDocument(<?php echo $detailValue['id']; ?>)">
               <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a><?php 
            }else {?>
              <a style="cursor: pointer; padding: 5px;" id="delete_document_<?php echo $key.'_'.$detailKey;?>" onclick="deleteDocumentOther('<?php echo $key.'_'.$detailKey; ?>')">
               <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a>
            <?php } ?>
          </td> -->
        </tr>
      <?php }?>
       </tbody>
      </table>
    <?php } ?>
  <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />