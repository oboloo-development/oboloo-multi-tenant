<?php $this->pageTitle = 'Edit ' . $model->vendor_name; ?>
     <div class="right_col" role="main">
  <div class="row-fluid tile_count col-md-11">
    <h3 class="pull-left">Edit <?php echo $model->vendor_name;?></h3>

     <div class="span6 pull-right">
        <a href="<?php echo $this->createAbsoluteUrl('vendor/list'); ?>" class="btn btn-success">Member list</a>
    </div>
    <div class="clearfix"></div> <br />
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php endif; ?>

    <?php $form = $this->beginWidget('CActiveForm',
			 array('id' => 'staff-form', 'enableAjaxValidation' => true, 'htmlOptions' => array('role' => 'form', 'id' => 'test', 'enctype' => 'multipart/form-data'),)); ?>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="two-flieds-2">
          <div class="form-group">
          <label for="">Name <span style="color: #a94442;">*</span></label> 
          <input type="text" name="vendor_name"class="form-control" placeholder="" value="<?php echo $model->vendor_name;?>" required="required">
          </div>

       </div>
     </div> <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Email <span style="color: #a94442;">*</span></label> 
       <input type="email" name="emails" class="form-control" id="" placeholder="" value="<?php echo $model->emails;?>" required="required">
       </div>
     </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
       <label for="">Phone Number</label> 
       <input type="text" name="phone_1" class="form-control" id="" placeholder="" value="<?php echo $model->phone_1;?>" >
       </div></div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
       <div class="form-group">
          <label for="">Status</label>
          <div class="single">
            <?php  $status = Yii::app()->params['teamember_status'];?>
            <select class="form-control" name="active">
              <option value="1" <?php if($model->active==1) { echo "selected";}?>>Active</option>
              <option value="0" <?php if($model->active==0) { echo "selected";}?>>Inactive</option>

            
    </select>
    </div>
       </div></div>
       <div class="clearfix"></div><br />
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-right text-right">
       <button type="submit" name="Staff" class="btn btn-success">Edit</button>
       </div> 
    <?php $this->endWidget(); ?>
     
    
  </div> <!-- col -->

  

<div class="seprater-bottom-100"></div>
</div>