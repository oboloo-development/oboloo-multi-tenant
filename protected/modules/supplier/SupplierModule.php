<?php

class SupplierModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'supplier.models.*',
			'supplier.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{   date_default_timezone_set(Yii::app()->params['timeZone']);
		if(parent::beforeControllerAction($controller, $action))
		{
			$quoteReference = Yii::app()->user->getState('page_type');

			if(Yii::app()->user->isGuest || ( $quoteReference == '' || $quoteReference == false))

			{

				$controller->layout = 'login';

			}

			else

			{

				$controller->layout = 'main';

			}
			return true;
		}
		else
			return false;
	}
	/*public function getModuleConfig()
	{

		require dirname(__FILE__).'/config/main.php';

		return $adminConfig;

	}*/
}
