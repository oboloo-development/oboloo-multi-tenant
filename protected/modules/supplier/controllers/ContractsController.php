<?php

class ContractsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */

	public $current_option = 'contracts_list';
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','view','uploadDocument','deleteDocument','deleteDocumentOther','documentDownload','viewDocument'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$vendorID = UtilityManager::getVendorID();
		$criteria=new CDbCriteria;
		$criteria->select = "t.*, a.value AS category, s.value as subcategory,cr.currency";
		$join = "		  
		        LEFT JOIN categories a ON t.contract_category_id = a.id
		        LEFT JOIN currency_rates cr ON t.currency_id = cr.id
				LEFT JOIN sub_categories s ON t.contract_subcategory_id = s.id
				";

		$criteria->join=$join;
		$criteria->condition='t.vendor_id='.$vendorID.' and contract_id='.$id;
		$contract = Contracts::model()->find($criteria);
		$view_data = array();
		$view_data['contract'] = $contract;
		$documentType = Yii::app()->user->getState('contract_document_type');
		$view_data['documentType'] = $documentType;
		$documentList = UtilityManager::getContractsDocuments($id,$documentType);
		$view_data['documentList'] = $documentList;
		$view_data['contractID'] = $id;

		$contractStatus = ContractStatus::model()->findByAttributes(array('code'=>$contract->contract_status));
        $view_data['contractStatus'] = $contractStatus;

        $currencyRate = CurrencyRates::model()->findByPk($contract->currency_id);
        $view_data['currencyRate'] = $currencyRate;

        $criteria=new CDbCriteria;
		$criteria->select = "full_name";
		$criteria->condition='user_id='.$contract->user_id;
        $user = Users::model()->find($criteria);      
        $view_data['user'] = $user;

        $criteria->select = "emails,contact_name,vendor_name,phone_1";
		$criteria->condition='vendor_id='.$contract->vendor_id;
        $vendor = Vendor::model()->find($criteria);      
        $view_data['vendor'] = $vendor;

        $criteria->select = "location_name";
		$criteria->condition='location_id='.$contract->location_id;
		$location = Locations::model()->find($criteria);
		$view_data['location'] = $location;

		$criteria->select = "department_name";
		$criteria->condition='department_id='.$contract->department_id;
		$department = Departments::model()->find($criteria);
		$view_data['department'] = $department;

		$criteria->select = "project_name";
		$criteria->condition='project_id='.$contract->project_id;
		$project = Projects::model()->find($criteria);
		$view_data['project'] = $project;

		$criteria->select = "value";
		$criteria->condition='id='.$contract->contract_category_id;
		$category = Categories::model()->find($criteria);
        $view_data['category'] = $category;

        $criteria->select = "value";
		$criteria->condition='id='.$contract->contract_subcategory_id;
		$subCategory = SubCategories::model()->find($criteria);
        $view_data['subCategory'] = $subCategory;

        $currencyRate = CurrencyRates::model()->findAll();
        $view_data['currency_rate'] = $currencyRate;

        $vendorList = Vendor::model()->findAll();
        $view_data['vendor_list'] = $vendorList;

        
		$this->render('view',$view_data);
	}

	public function actionViewDocument($id)
	{
		$contract_id = Yii::app()->user->getState('contract_id');
		if($contract_id != $id){
			//$this->redirect(array('site/contractDocumentLogin'));
		}
		$vendorID = UtilityManager::getVendorID();
		$criteria=new CDbCriteria;
		$criteria->select = "t.*, a.value AS category, s.value as subcategory,cr.currency";
		$join = "		  
		        LEFT JOIN categories a ON t.contract_category_id = a.id
		        LEFT JOIN currency_rates cr ON t.currency_id = cr.id
				LEFT JOIN sub_categories s ON t.contract_subcategory_id = s.id
				";

		$criteria->join=$join;
		$criteria->condition='t.vendor_id='.$vendorID.' and contract_id='.$id;
		$contract = Contracts::model()->find($criteria);
		$view_data = array();
		$view_data['contract'] = $contract;

		$documentType = Yii::app()->user->getState('contract_document_type');
		$view_data['documentType'] = $documentType;
		
		$documentList = UtilityManager::getContractsDocuments($id,$documentType);
		$view_data['documentList'] = $documentList;
		$view_data['contractID'] = $id;

		$contractStatus = ContractStatus::model()->findByAttributes(array('code'=>$contract->contract_status));
        $view_data['contractStatus'] = $contractStatus;

        $currencyRate = CurrencyRates::model()->findByPk($contract->currency_id);
        $view_data['currencyRate'] = $currencyRate;

        $criteria=new CDbCriteria;
		$criteria->select = "full_name";
		$criteria->condition='user_id='.$contract->user_id;
        $user = Users::model()->find($criteria);      
        $view_data['user'] = $user;

        $criteria->select = "emails,contact_name,vendor_name,phone_1";
		$criteria->condition='vendor_id='.$contract->vendor_id;
        $vendor = Vendor::model()->find($criteria);      
        $view_data['vendor'] = $vendor;

        $criteria->select = "location_name";
		$criteria->condition='location_id='.$contract->location_id;
		$location = Locations::model()->find($criteria);
		$view_data['location'] = $location;

		$criteria->select = "department_name";
		$criteria->condition='department_id='.$contract->department_id;
		$department = Departments::model()->find($criteria);
		$view_data['department'] = $department;

		$criteria->select = "project_name";
		$criteria->condition='project_id='.$contract->project_id;
		$project = Projects::model()->find($criteria);
		$view_data['project'] = $project;

		$criteria->select = "value";
		$criteria->condition='id='.$contract->contract_category_id;
		$category = Categories::model()->find($criteria);
        $view_data['category'] = $category;

        $criteria->select = "value";
		$criteria->condition='id='.$contract->contract_subcategory_id;
		$subCategory = SubCategories::model()->find($criteria);
        $view_data['subCategory'] = $subCategory;

        $currencyRate = CurrencyRates::model()->findAll();
        $view_data['currency_rate'] = $currencyRate;

        $vendorList = Vendor::model()->findAll();
        $view_data['vendor_list'] = $vendorList;

        
		$this->render('view',$view_data);
	}


	public function actionUploadDocument(){  
		error_reporting(0);
		if (!empty($_FILES)){
			$recordID = Yii::app()->user->getState('record_id');
			$documentType = Yii::app()->user->getState('contract_document_type');

			$vendor = new Vendor;
			if (isset($_FILES['file']) && is_array($_FILES['file'])){
				$contractID = $_POST['contract_id'];
				
				$path = Yii::getPathOfAlias('webroot').'/uploads/contract_documents';
				if (!is_dir($path)) mkdir($path);
				if (!is_dir($path.'/' . $contractID))
					mkdir($path.'/' . $contractID);
				if (!is_dir($path.'/'))
					mkdir($path.'/');

				if (!empty($_FILES['file']['name']))
				{
					$file_name = time().'_'.$_FILES['file']['name'];
                    $file_name = str_replace("'","",$file_name);
                    $file_name = str_replace('"','',$file_name);
                    $file_name = str_replace(' ','_',$file_name);
                    $file_description = addslashes($_POST['file_description']);

					$document_type = $_POST['document_type'];
					$document_status = $_POST['document_status'];
					$expiry_date = $_POST['expiry_date'];
					
					$uploadedFiles = Yii::app()->session['uploaded_files'];

					$uploadedFiles[] = array('file_name'=>$file_name,'file_description'=>$file_description,'contract_id'=>$contractID,'file_type'=>$document_type,'document_status'=>$document_status,'expiry_date'=>$expiry_date);

					
					//move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/orders/' . $file_name);
					if(move_uploaded_file($_FILES['file']['tmp_name'], $path.'/' . (!empty($contractID)?$contractID. '/'. $file_name: $file_name))){
						if(!empty($contractID)){
							Yii::app()->session['uploaded_files'] = $uploadedFiles;
						UtilityManager::saveContractDocument($contractID,$file_description, $file_name,$document_type,$document_status,$expiry_date);

							UtilityManager::updateContractDocumentTypeStatus($contractID,$recordID);
							$documentList = UtilityManager::getContractsDocuments($contractID,$documentType);
						
						}else {
							$uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
							$uploadedFilesDisplay[$document_type][] =  array(
																		'id'=>0,
																		'document_file'=>$file_name,
																		'document_title'=>$file_description,
																		'contract_id'=>0,
																		'status'=>$document_status,
																		'expiry_date'=>$expiry_date,
																		'date_created'=>date("Y-m-d")
																	);
							Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
							$documentList = Yii::app()->session['uploaded_files_display'];
						}
					}
					
					$this->renderPartial('_documents',array('documentList'=>$documentList,'contractID'=>$contractID));

					
				}
			}
		}

	}

	
	public function actionDocumentDownload()
	{
		$id = $_GET['id'];

        $sql = "select document_file,contract_id from contract_documents WHERE id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();

        $contractID = $fileReader['contract_id'];
	    $file =$path = Yii::getPathOfAlias('webroot').'/uploads/contract_documents/'.$contractID."/".$fileReader['document_file'];
       
		if ($fileReader) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}

	}

	public function actionDeleteDocument()
	{
		$id = $_POST['documentID'];
        $sql = "select document_file,contract_id from contract_documents WHERE status='Pending' and id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
        $contractID='0';
        if(!empty($fileReader)){
        	$vendor_id = UtilityManager::getVendorID();
        	$fileName = $fileReader['document_file'];
        	$contractID = $fileReader['contract_id'];
        	$file = Yii::getPathOfAlias('webroot').'/uploads/contract_documents/'.$contractID.'/'.$fileName;
        	if(unlink($file)){
        		$sql = "delete from contract_documents WHERE status='Pending' and id =".$id;
        		$fileReader = Yii::app()->db->createCommand($sql)->execute();
        	}
        }
        $documentType = Yii::app()->user->getState('contract_document_type');
        $vendor = new Vendor;
		$this->renderPartial('_documents',array('documentList'=> UtilityManager::getContractsDocuments($contractID,$documentType),'contractID'=>$contractID));
	}

	public function actionDeleteDocumentOther()
	{
		$ids = explode("_",$_POST['documentID']);

		$uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
		$documentFileDislplay = $uploadedFilesDisplay[$ids[0]][$ids[1]]['document_file'];
		unset($uploadedFilesDisplay[$ids[0]][$ids[1]]);
		if(empty($uploadedFilesDisplay[$ids[0]])){
			unset($uploadedFilesDisplay[$ids[0]]);
		}
		Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
		$documentList = Yii::app()->session['uploaded_files_display'];

		$uploadedFiles = Yii::app()->session['uploaded_files'];
		$uploadedFilesRowKey = '';
		foreach($uploadedFiles as $key=>$fileVal){
			if($fileVal['file_name']==$documentFileDislplay){
				$uploadedFilesRowKey =$key;
				break;
			}
		}
		unset($uploadedFiles[$uploadedFilesRowKey]);

	
		Yii::app()->session['uploaded_files'] = $uploadedFiles;
        if(!empty($documentFileDislplay)){
        	$file = Yii::app()->basePath."/../uploads/contract_documents/".$documentFileDislplay;
        	if(unlink($file)){
        	}
        }
        $vendor = new Vendor;
		$this->renderPartial('_documents',array('documentList'=>$documentList,'contractID'=>0));
	}

	public function actionDelete($id)
	{
		/*$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));*/
	}

	

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$vendorID = UtilityManager::getVendorID();
		$view_data = array();
        // get contracts as per filters applied
        $location_id = !empty($_POST['location_id'])?$_POST['location_id']:0;
        $department_id = !empty($_POST['department_id'])?$_POST['department_id']:0;
        $category_id = !empty($_POST['category_id'])?$_POST['category_id']:0;
        $subcategory_id = !empty($_POST['subcategory_id'])?$_POST['subcategory_id']:0;
        $status_code = !empty($_POST['contract_status'])?$_POST['contract_status']:0;
		$criteria=new CDbCriteria;
		$criteria->select = "t.*, a.value AS category, s.value as subcategory,cr.currency";
		$join = "		  
		        LEFT JOIN categories a ON t.contract_category_id = a.id
		        LEFT JOIN currency_rates cr ON t.currency_id = cr.id
				LEFT JOIN sub_categories s ON t.contract_subcategory_id = s.id
				";
		$criteria->join=$join;
		$criteria->condition='t.vendor_id='.$vendorID;
		$contracts = Contracts::model()->findAll($criteria);
		$criteria=new CDbCriteria;
		$criteria->select = "location_name,location_id";
		$criteria->order='location_name asc';
		$locations = Locations::model()->findAll($criteria);
		$criteria=new CDbCriteria;
		$criteria->select = "department_name,department_id";
		$criteria->order='department_name asc';
		$departments = Departments::model()->findAll($criteria);
        $categories = Categories::model()->findAll(array('order'=>'value asc'));
        $view_data['categories'] = $categories;
		$view_data['contracts'] = $contracts;
		$view_data['location_id'] = $location_id;
        $view_data['locations'] = $locations;
        $view_data['department_info'] = $departments;
        $view_data['department_id'] = $department_id;
        $view_data['category_id'] = $category_id;
        $view_data['subcategory_id'] = $subcategory_id;
		$this->render('admin',$view_data);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ContractDocuments the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Contract::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ContractDocuments $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='contract-documents-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
