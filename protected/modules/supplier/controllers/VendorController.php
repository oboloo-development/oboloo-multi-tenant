<?php 
class VendorController extends Controller
{ 
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	/**
	 * @return array action filters
	 */
    public $current_option = 'supplier_list';
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('documents','documentsOnboard','uploadDocument','deleteDocument','deleteDocumentOther','documentDownload','teammemberList','create','list','remove','updatestaff','update','documentDownloadOnboard','uploadDocumentOnboard'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','captcha'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
 

	
	public function actionDocuments()
	{  $this->layout="main";
		$vendorID = UtilityManager::getVendorID();
		$vendorName = Yii::app()->user->getState('main_vendor_name');
		$documentType = Yii::app()->user->getState('vendor_document_type');
		$documentList = UtilityManager::getVendorsDocuments($vendorID,$documentType);

		$this->render('documents',array(
			'documentList'=>$documentList,
			'vendorID'=>$vendorID,
			'documentType'=>$documentType,
			'vendorName'=>$vendorName,
		));
	}

	public function actionDocumentsOnboard()
	{  $this->layout="main";
		$vendorID = UtilityManager::getVendorID();
		$vendorName = Yii::app()->user->getState('main_vendor_name');
		$documentType = Yii::app()->user->getState('vendor_document_type');
		$documentList = UtilityManager::getVendorsDocumentsOnboard($vendorID,$documentType);

		$this->render('documents_onboard',array(
			'documentList'=>$documentList,
			'vendorID'=>$vendorID,
			'documentType'=>$documentType,
			'vendorName'=>$vendorName,
		));
	}

	public function actionUploadDocument(){  
		error_reporting(0);
		if (!empty($_FILES)){
			$recordID = Yii::app()->user->getState('record_id');
			$documentType = Yii::app()->user->getState('vendor_document_type');
			$documentType = $documentType;
			$vendor = new Vendor;
			if (isset($_FILES['file']) && is_array($_FILES['file'])){
				$vendor_id = UtilityManager::getVendorID();
				if(empty($vendor_id)){
					$vendor_id ='';
				}
				$path = Yii::getPathOfAlias('webroot').'/uploads/vendor_documents';
				if (!is_dir($path)) mkdir($path);
				if (!is_dir($path.'/' . $vendor_id))
					mkdir($path.'/' . $vendor_id);
				if (!is_dir($path.'/'))
					mkdir($path.'/');

				if (!empty($_FILES['file']['name']))
				{
					$file_name = time().'_'.$_FILES['file']['name'];
                    $file_name = str_replace("'","",$file_name);
                    $file_name = str_replace('"','',$file_name);
                    $file_name = str_replace(' ','_',$file_name);
                    $file_description = addslashes($_POST['file_description']);
                    
					$document_type = $_POST['document_type'];
					$document_status = $_POST['document_status'];
					$expiry_date = $_POST['expiry_date'];
					
					$uploadedFiles = Yii::app()->session['uploaded_files'];

					$uploadedFiles[] = array('file_name'=>$file_name,'file_description'=>$file_description,'vendor_id'=>$vendor_id,'file_type'=>$document_type,'document_status'=>$document_status,'expiry_date'=>$expiry_date);

					
					//move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/orders/' . $file_name);
					if(move_uploaded_file($_FILES['file']['tmp_name'], $path.'/' . (!empty($vendor_id)?$vendor_id. '/'. $file_name: $file_name))){
						if(!empty($vendor_id)){
							Yii::app()->session['uploaded_files'] = $uploadedFiles;
							UtilityManager::saveDocument($vendor_id,$file_description, $file_name,$document_type,$document_status,$expiry_date);
							UtilityManager::updateDocumentTypeStatus($vendor_id,$recordID);
							$documentList = UtilityManager::getVendorsDocuments($vendor_id,$documentType);

							if(!empty($vendor_id)){
		                    $msg = 1;
		                    $cronEmail = new CronEmail;
		                    $cronEmail->rs = array();
		                    $cronEmail->rs['record_id'] = $recordID;
		                    $cronEmail->rs['send_to'] = 'User'; 
		                    $cronEmail->rs['user_id'] = $vendor_id;
		                    $cronEmail->rs['user_type'] = 'Supplier Uploaded Vendor Document';
		                    $cronEmail->rs['status'] = 'Pending';
		                    $cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
		                    $cronEmail->write();
               			 }
						}else {
							$uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
							$uploadedFilesDisplay[$document_type][] =  array(
																		'id'=>0,
																		'document_file'=>$file_name,
																		'document_title'=>$file_description,
																		'vendor_id'=>0,
																		'status'=>$document_status,
																		'expiry_date'=>$expiry_date,
																		'date_created'=>date("Y-m-d")
																	);
							Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
							$documentList = Yii::app()->session['uploaded_files_display'];
						}
					}
					
					$this->renderPartial('_documents',array('documentList'=>$documentList,'vendor_id'=>$vendor_id));

					
				}
			}
		}

	}

	public function actionUploadDocumentOnboard(){  
		error_reporting(0);
		if (!empty($_FILES)){
			$recordID = Yii::app()->user->getState('record_id');
			$documentType = Yii::app()->user->getState('vendor_document_type');
			$documentType = $documentType;
			$vendor = new Vendor;
			if (isset($_FILES['file']) && is_array($_FILES['file'])){
				$vendor_id = UtilityManager::getVendorID();
				if(empty($vendor_id)){
					$vendor_id ='';
				}
				$path = Yii::getPathOfAlias('webroot').'/uploads/vendor_documents_onboard';
				if (!is_dir($path)) mkdir($path);
				if (!is_dir($path.'/' . $vendor_id))
					mkdir($path.'/' . $vendor_id);
				if (!is_dir($path.'/'))
					mkdir($path.'/');

				if (!empty($_FILES['file']['name']))
				{
					$file_name = time().'_'.$_FILES['file']['name'];
                    $file_name = str_replace("'","",$file_name);
                    $file_name = str_replace('"','',$file_name);
                    $file_name = str_replace(' ','_',$file_name);
                    $file_description = addslashes($_POST['file_description']);
                    
					$document_type = $_POST['document_type'];
					$document_status = $_POST['document_status'];
					$expiry_date = $_POST['expiry_date'];
					
					$uploadedFiles = Yii::app()->session['uploaded_files'];

					$uploadedFiles[] = array('file_name'=>$file_name,'file_description'=>$file_description,'vendor_id'=>$vendor_id,'file_type'=>$document_type,'document_status'=>$document_status,'expiry_date'=>$expiry_date);

					
					//move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/orders/' . $file_name);
					if(move_uploaded_file($_FILES['file']['tmp_name'], $path.'/' . (!empty($vendor_id)?$vendor_id. '/'. $file_name: $file_name))){
						if(!empty($vendor_id)){
						 Yii::app()->session['uploaded_files'] = $uploadedFiles;
						 UtilityManager::saveDocumentOnboard($vendor_id,$file_description, $file_name,$document_type,$document_status,$expiry_date);
						 UtilityManager::updateDocumentTypeStatusOnboard($vendor_id,$recordID);
						$documentList = UtilityManager::getVendorsDocumentsOnboard($vendor_id,$documentType);
						}else {
							$uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
							$uploadedFilesDisplay[$document_type][] =  array(
																		'id'=>0,
																		'document_file'=>$file_name,
																		'document_title'=>$file_description,
																		'vendor_id'=>0,
																		'status'=>$document_status,
																		'expiry_date'=>$expiry_date,
																		'date_created'=>date("Y-m-d")
																	);
							Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
							$documentList = Yii::app()->session['uploaded_files_display'];
						}
					}				
					$this->renderPartial('_documents_onboard',array('documentList'=>$documentList,'vendor_id'=>$vendor_id));	
				}
			}
		}

	}
	
	public function actionDocumentDownload()
	{
		$id = $_GET['id'];
        $sql = "select document_file,vendor_id from vendor_documents WHERE id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();

        $vendor_id = UtilityManager::getVendorID();
	    $file =$path = Yii::getPathOfAlias('webroot').'/uploads/vendor_documents/'.$vendor_id."/".$fileReader['document_file'];
       
		if ($fileReader) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}
	}

	public function actionDocumentDownloadOnboard()
	{
		$id = $_GET['id'];

        $sql = "select document_file,vendor_id from vendor_documents_onboard WHERE id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();

        $vendor_id = UtilityManager::getVendorID();
	    $file =$path = Yii::getPathOfAlias('webroot').'/uploads/vendor_documents_onboard/'.$vendor_id."/".$fileReader['document_file'];
       
		if ($fileReader) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}

	}

	public function actionDeleteDocument()
	{
		$id = $_POST['documentID'];
        $sql = "select document_file,vendor_id from vendor_documents WHERE status='Pending' and id =".$id;
        $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
        if(!empty($fileReader)){
        	$vendor_id = UtilityManager::getVendorID();
        	$fileName = $fileReader['document_file'];
        	$file = Yii::getPathOfAlias('webroot').'/uploads/vendor_documents/'.$vendor_id.'/'.$fileName;
        	if(unlink($file)){
        		$sql = "delete from vendor_documents WHERE status='Pending' and id =".$id;
        		$fileReader = Yii::app()->db->createCommand($sql)->execute();
        	}
        }
        $documentType = Yii::app()->user->getState('vendor_document_type');
        $vendor = new Vendor;
		$this->renderPartial('_documents',array('documentList'=>UtilityManager::getVendorsDocuments($vendor_id,$documentType),'vendor_id'=>$vendor_id));
	}

	public function actionDeleteDocumentOther()
	{
		$ids = explode("_",$_POST['documentID']);

		$uploadedFilesDisplay = Yii::app()->session['uploaded_files_display'];
		$documentFileDislplay = $uploadedFilesDisplay[$ids[0]][$ids[1]]['document_file'];
		unset($uploadedFilesDisplay[$ids[0]][$ids[1]]);
		if(empty($uploadedFilesDisplay[$ids[0]])){
			unset($uploadedFilesDisplay[$ids[0]]);
		}
		Yii::app()->session['uploaded_files_display'] = $uploadedFilesDisplay;
		$documentList = Yii::app()->session['uploaded_files_display'];

		$uploadedFiles = Yii::app()->session['uploaded_files'];
		$uploadedFilesRowKey = '';
		foreach($uploadedFiles as $key=>$fileVal){
			if($fileVal['file_name']==$documentFileDislplay){
				$uploadedFilesRowKey =$key;
				break;
			}
		}
		unset($uploadedFiles[$uploadedFilesRowKey]);

	
		Yii::app()->session['uploaded_files'] = $uploadedFiles;
        if(!empty($documentFileDislplay)){
        	$file = Yii::app()->basePath."/../uploads/vendor_documents/".$documentFileDislplay;
        	if(unlink($file)){
        	}
        }
        $vendor = new Vendor;
		$this->renderPartial('_documents',array('documentList'=>$documentList,'vendor_id'=>0));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{ 	$model=Vendor::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{ if(isset($_POST['ajax']) && $_POST['ajax']==='admin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
