<?php

class OrdersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public $current_option = '';
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','submission','delete','submissionView','documentDownload'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Orders;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Orders']))
		{
			$model->attributes=$_POST['Orders'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->order_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Orders']))
		{
			$model->attributes=$_POST['Orders'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->order_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Orders');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionSubmission()
	{

		$this->current_option ='order_submission'; 
		$vendorID = UtilityManager::getVendorID();
		$criteria=new CDbCriteria;
		$criteria->select = "t.po_id,t.po_total,t.po_status,po_sent_date,ord.order_id,ord.location_name,ord.department_name,ord.currency_id";
		$join = 'INNER JOIN  
						(SELECT inord.order_id,inord.currency_id,inloc.location_name,indep.department_name FROM orders inord, locations inloc,departments indep where inord.location_id=inloc.location_id and inord.department_id = indep.department_id) ord 
						ON t.order_id=ord.order_id';
        $criteria->join=$join;
        $criteria->condition="t.vendor_id=".$vendorID;
		$purchaseModel = PurchaseOrders::model()->findAll($criteria);
		$criteria=new CDbCriteria;
		$criteria->select = "location_name,location_id";
		$locations = Locations::model()->findAll($criteria);
		$criteria=new CDbCriteria;
		$criteria->select = "department_name,department_id";
		$departments = Departments::model()->findAll($criteria);

		$view_data = array();
		$view_data['purchaseModel'] = $purchaseModel;
		$view_data['locations'] = $locations;
		$view_data['departments'] = $departments;
		$view_data['department_id'] = array();
		$view_data['location_id'] = array();

		//echo "<pre>";print_r($departments);exit;
		$this->render('submission',$view_data);
	}

	public function actionSubmissionView($po_id)
	{
		$this->current_option ='order_submission'; 
		$vendorID = UtilityManager::getVendorID();
		
		$criteria=new CDbCriteria;
		$criteria->select = "t.po_id,t.po_total,t.po_status,po_sent_date,ord.*";
		$join = 'INNER JOIN  
						(SELECT inord.order_id,inloc.location_id,inloc.location_name,indep.department_id,indep.department_name,
						inord.project_id,inord.spend_type,inord.currency_id,inord.currency_rate,
						inord.order_date,inord.user_name,inord.user_id,order_status,inord.description
						FROM orders inord, locations inloc,departments indep where inord.location_id=inloc.location_id and inord.department_id = indep.department_id) ord 
						ON t.order_id=ord.order_id';
        $criteria->join=$join;
        $criteria->condition="t.vendor_id=".$vendorID." and po_id=".$po_id;
		$purchaseModel = PurchaseOrders::model()->find($criteria);
		$poID = $purchaseModel->po_id;
		$orderID = $purchaseModel->order_id;

		$criteria=new CDbCriteria;
		$criteria->select = "location_name,location_id";
		$locations = Locations::model()->findAll($criteria);
		$criteria=new CDbCriteria;
		$criteria->select = "department_name,department_id";
		$departments = Departments::model()->findAll($criteria);

		$orderDetails = UtilityManager::getOrderDetails($purchaseModel->order_id);
		$orderInvoice = UtilityManager::orderInvoice($purchaseModel->order_id);
	
		$orderVendorDetail = OrderVendorDetails::model()->findByAttributes(array('po_id' =>$poID,'order_id'=>$orderID,'vendor_id'=>$vendorID),array('order'=>'id DESC'));

		$currencyRates = CurrencyRates::model()->findByPk($purchaseModel->currency_id);

		$criteria=new CDbCriteria;
		$criteria->select = "project_name";
		$criteria->condition="t.project_id=".$purchaseModel->project_id;
		$projects = Projects::model()->find($criteria);

		$view_data = array();
		$view_data['vendorID'] = $vendorID;
		$view_data['poID'] = $poID;
		
		$view_data['purchaseModel'] = $purchaseModel;
		$view_data['locations'] = $locations;
		$view_data['departments'] = $departments;
		$view_data['department_id'] = array();
		$view_data['location_id'] = array();
		$view_data['orderDetails'] = $orderDetails;
		$view_data['orderVendorDetail'] = $orderVendorDetail;
		$view_data['projects'] = $projects;
		$view_data['currencyRates'] = $currencyRates;
		$view_data['orderInvoice'] = $orderInvoice;

		if(!empty($_POST['status'])){
			//echo "<pre>"; print_r($_POST);exit;
			$submissionData = array();
			$submissionData['status'] = $_POST['status'];
			$submissionData['tracking_number'] = $_POST['tracking_number'];
			$submissionData['courier_company'] = $_POST['courier_company'];
			$submissionData['note'] = $_POST['note'];
			if($orderVendorDetail == null){
				$orderVendorDetail  = new OrderVendorDetails;
				$submissionData['order_id'] = $orderID;
				$submissionData['po_id'] = $poID;
				$submissionData['vendor_id'] = $vendorID;
				$submissionData['created_at'] = date("Y-m-d H:i:s");
			}
			$orderVendorDetail->attributes = $submissionData;
			if($orderVendorDetail->save(false)){
				Yii::app()->user->setFlash('success', "Info submitted successfully");
			}else {
				Yii::app()->user->setFlash('error', "Ops! &nbsp;   there is error in submitting. Try again");
			}

			// files uploaded if any
				$total_documents = $_POST['total_documents'];
				$path = Yii::getPathOfAlias('webroot').'/../';
				for ($idx=1; $idx<=$total_documents; $idx++)
				{
					$delete_document_flag = $_POST['delete_document_flag_' . $idx];
					if ($delete_document_flag) continue;

					if (isset($_FILES['order_file_' . $idx]) && is_array($_FILES['order_file_' . $idx]))
					{
						if (!is_dir($path.'uploads/orders')) mkdir($path.'uploads/orders');
						if (!is_dir($path.'uploads/orders/' . $orderID))
							mkdir($path.'uploads/orders/' . $orderID);
						/*if (!is_dir('uploads/orders/' . $orderID . '/' . $vendor_id))
							mkdir('uploads/orders/' . $orderID . '/' . $vendor_id);*/
						if (!is_dir($path.'uploads/orders/invoices/' . $orderID)) 
							mkdir($path.'uploads/orders/invoices/' . $orderID);

						if (!isset($_FILES['order_file_' . $idx]['error']) || empty($_FILES['order_file_' . $idx]['error']))
						{
							$file_name = time().'_'.$_FILES['order_file_' . $idx]['name'];
							if(move_uploaded_file($_FILES['order_file_' . $idx]['tmp_name'], $path.'uploads/orders/invoices/' . $orderID . '/'. $file_name)){
							//Save documents
								$orderInvoice = new OrderInvoices();
								$orderInvoice->order_id = $orderID;
								$orderInvoice->vendor_id = $vendorID;
								$orderInvoice->created_by_type = 1;
								$orderInvoice->file_name = $file_name;
								$orderInvoice->status = 1;
								$orderInvoice->created_at = date('Y-m-d H:i:s');
								$orderInvoice->save(false);

								$vendorFile = new OrderVendorFiles();
								$vendorFile->order_id = $orderID;
								$vendorFile->vendor_id = $vendorID;
								$vendorFile->file_name = $file_name;
								$vendorFile->created_datetime = date('Y-m-d H:i:s'); 
								$vendorFile->save(false);
							}
						}

					}
				}
				//$purchase_order->submitOrder($order_id,$vendor_id);

			$this->redirect(array('submissionView','po_id'=>$poID));
		}
		$this->render('submission_view',$view_data);
	}

	public function actionDocumentDownload()
	{
		$id = $_GET['id'];
		$vendorID = UtilityManager::getVendorID();
        $invoiceFile = OrderInvoices::model()->findByAttributes(array('id'=>$id,'vendor_id'=>$vendorID));
		if ($invoiceFile !=null) {
		    $path = Yii::getPathOfAlias('webroot').'/../';
		    $file_name = $invoiceFile->file_name;
		    $orderID = $invoiceFile->order_id;
		    $file =  $path.'uploads/orders/invoices/' . $orderID . '/'. $file_name;
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		}

	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Orders the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Orders::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Orders $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='orders-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
