<?php

class QuotesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */

	public $current_option = 'quote_submission';
	public $questionnairFormModels = '';
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
	
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','view','qouteDocument','uploadDocumentModal','quoteFile',
					'deleteDocumentModal','vendorFormQuestions','answerSave','formAnswers','vendorFormAnswers'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($quote_id)
	{
		$vendorID = UtilityManager::getVendorID();
		$quote_vendor_id =  $vendorID;

		$quoteReference = Yii::app()->user->getState('quote_reference');
		

		$view_data = array();	

		$view_data['quote_vendor_id'] = $quote_vendor_id;
		$view_data['quote_id'] = $quote_id;
		$view_data['vendor_id'] = $vendorID;
		$criteriaQuote=new CDbCriteria;
		$criteriaQuote->select = "t.*,COUNT(DISTINCT q_v.id) AS invite_count,SUM(q_v.submit_status) AS submit_count";
		$join = 'INNER JOIN quote_vendors as q_v ON t.quote_id=q_v.quote_id';
        $criteriaQuote->join=$join;
        $criteriaQuote->condition="q_v.vendor_id=".$vendorID." and t.quote_id=".$quote_id." and q_v.reference='".$quoteReference."'";


        $criteriaQuoteModel=new CDbCriteria;
		$criteriaQuoteModel->select = "t.*";
		$join = 'INNER JOIN quote_vendors as q_v ON t.quote_id=q_v.quote_id';
        $criteriaQuoteModel->join=$join;
        $criteriaQuoteModel->condition="q_v.vendor_id=".$vendorID." and t.quote_id=".$quote_id." and q_v.reference='".$quoteReference."'";


		$quoteModel = Quotes::model()->find($criteriaQuoteModel);

		$view_data['quote'] = $quoteModel;
		// $view_data['quote_questions'] = QuoteQuestions::model()->findAll(array('condition'=>'quote_id='.$quote_id));

		$sql = 'select * from quote_vendor_questionnaire where quote_id ="'.$quote_id.'" and vendor_id ="'.$vendorID.'"  group by question_form_id order by  question_order asc ';
		$view_data['quoteQuestionnaireRender'] = Yii::app()->db->createCommand($sql)->queryAll();
	
		if ($quoteModel !=null)
		{
			$quote_found = true;
			
			$criteria=new CDbCriteria;
			$criteria->select = "t.*,q_v.unit_price,q_v.tax_rate as product_tax_rate,q_v.shipping as product_shipping,q_v.other_charges as product_other_charges, q_v.vendor_product_notes";
			$join = 'LEFT JOIN quote_vendor_details as q_v ON t.quote_id=q_v.quote_id and q_v.vendor_id='.$vendorID;
	        $criteria->join=$join;
	        $criteria->condition="t.quote_id=".$quote_id;
	        $criteria->group="t.id";
			$quoteDetailModel = QuoteDetails::model()->findAll($criteria);

			$criteriaQvendor=new CDbCriteria;
			$criteriaQvendor->condition="t.quote_id=".$quote_id.' AND vendor_id ='.$vendorID;
			$quoteVendors = QuoteVendors::model()->find();
			/*print_r($view_data['quote']);
			print_r($quoteVendors);*/

			$view_data['quote_details'] = $quoteDetailModel;
			

			// START: to conver of object into array()
			$view_data['quote'] = $quoteModel->getCommandBuilder()
               ->createFindCommand($quoteModel->tableSchema, $criteriaQuote)
               ->queryAll();

            $view_data['quoteVendors'] = $quoteVendors->getCommandBuilder()
               ->createFindCommand($quoteVendors->tableSchema, $criteriaQvendor)
               ->queryAll();

            $view_data['quote'] = array_merge($view_data['quote'], $view_data['quoteVendors']);

            $view_data['quote'] = array_merge($view_data['quote'][0], $view_data['quote'][1]);

            // End: to conver of object into array()
			$vendor =  Vendor::model()->findByPk($vendorID);
			$view_data['vendor'] = $vendor;
			
		}else{
			$this->redirect(array('default/logout'));
		}
		$quote = $view_data['quote'];

		if (!empty($_POST['form_submitted'])){
			

			$quote_submission_data = array();
			$quote_submission_data['quote_vendor_id'] = $_POST['quote_vendor_id'];
			$quote_id = $quote_submission_data['quote_id'] = $_POST['quote_id'];
			$vendor_id = $quote_submission_data['vendor_id'] = $_POST['vendor_id'];
			$quote_submission_data['submit_quote'] = isset($_REQUEST['submit_quote']) ? $_REQUEST['submit_quote'] : 0;
			$quote_submission_data['product_names'] = $_POST['product_name'];
			$quote_submission_data['product_codes'] = $_POST['product_code'];
			
			$quote_submission_data['quantities'] = $_POST['quantity'];
			$quote_submission_data['prices'] = !empty($_POST['price'])?$_POST['price']:0;
			$quote_submission_data['unit_price'] = $_POST['unit_price'];
			$quote_submission_data['product_notes'] = $_POST['vendor_product_notes'];
			$quote_submission_data['product_tax_rate'] = $_POST['product_tax_rate'];
			$quote_submission_data['product_shipping_charges'] = $_POST['product_shipping_charges'];
			$quote_submission_data['product_other_charges'] = $_POST['product_other_charges'];

			$quote_submission_data['notes'] = !empty($_POST['notes'])?$_POST['notes']:'';
			$quote_submission_data['tax_rate'] = !empty($_POST['tax_rate'])?$_POST['tax_rate']:0.00;
			$quote_submission_data['shipping'] = !empty($_POST['shipping'])?$_POST['shipping']:0.00;
			$quote_submission_data['other_charges'] = !empty($_POST['other_charges'])?$_POST['other_charges']:0.00;
			$quote_submission_data['discount'] = !empty($_POST['discount'])?$_POST['discount']:0;
			$quote_submission_data['total_price'] = !empty($_POST['total_price'])?$_POST['total_price']:0.00;
			$this->saveQuoteSubmissionData($quote_submission_data);
			$this->deleteQuoteDocumentandAnswers($quote_id,$vendor_id);
			// files uploaded if any
			
			 $path = Yii::getPathOfAlias('webroot').'/';
		
			 $this->redirect(array('view','quote_id'=>$quote_id));
		}

		$sql = "select score as s_valu,scoring_title from quote_scoring where quote_id=".$quote_id." order by s_valu DESC";

        $scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();

        $pie_score_series = array();
		$pie_score_label = array();
		$zeroValueCtr = 0;

		foreach($scoreReader as $value){
			/*$scoreID = $value['score_id'];
			$sql = "SELECT * FROM quote_scoring where vendor_scoring_id=".$scoreID." and quote_id=".$quote_id;
        	$scoringForQuoteReader = Yii::app()->db->createCommand($sql)->queryRow();*/
        	/*if($value['s_valu']<=0){
        	  $zeroValueCtr++;

        	}*/
        	if($value['s_valu']>0){
			$pie_score_series[] = $value['s_valu'];
			$pie_score_label[] = $value['scoring_title'];
			}
		}

		/*if($zeroValueCtr==count($scoreReader)){
			$pie_score_series = array();
			$pie_score_label = array();
		}*/
       
        $view_data['scoringCriteria'] = array('pie_score_series'=>$pie_score_series,'pie_score_label'=>$pie_score_label);
        $view_data['pie_score_label'] = $pie_score_label;

        $sql = "SELECT * FROM quote_files WHERE quote_id = ".$quote_id." ORDER BY id";
        $quoteFiles = Yii::app()->db->createCommand($sql)->query()->readAll();
		$view_data['quote_files'] = $quoteFiles;

		$location = Locations::model()->findByPk($view_data['quote']['location_id']);
		$departments = Departments::model()->findByPk($view_data['quote']['department_id']);
		$view_data['location'] = $location;
		$view_data['department'] = $departments;

		$this->render('view',$view_data);
	}

	public function saveQuoteSubmissionData($quote_submission_data)
	{
		
		// which vendor is submitting this quote?
		$vendor_id = isset($quote_submission_data['vendor_id']) ? $quote_submission_data['vendor_id'] : 0;
		$quote_id = isset($quote_submission_data['quote_id']) ? $quote_submission_data['quote_id'] : 0;
		$notes = isset($quote_submission_data['notes']) ? $quote_submission_data['notes'] : "";
		$submit_quote = !empty($quote_submission_data['submit_quote']) ? $quote_submission_data['submit_quote'] : 0;
		
		if ($quote_id && $vendor_id)
		{
		
            // Start:  Keep History
            $checkReopen = Yii::app()->db->createCommand("SELECT count(*) as total_reopen,max(id) as reopen_id FROM quote_reopen_history where quote_id=".$quote_id." and vendor_id=".$vendor_id)->queryRow();


			if($submit_quote !=0  && $checkReopen['total_reopen']>0){
					
				$reopenID = $checkReopen['reopen_id'];
				$sql = "Select * FROM quote_vendor_details WHERE quote_id = $quote_id AND vendor_id = $vendor_id";
				$qVenDetailReader = Yii::app()->db->createCommand($sql)->query()->readAll();

				$sql = "select * from  quotes WHERE quote_id = $quote_id";
			    $quoteReader  = Yii::app()->db->createCommand($sql)->queryRow();

				foreach($qVenDetailReader as $value){

					$sql = "select * from  quote_vendors WHERE quote_id = $quote_id AND vendor_id = $vendor_id";
					$vendorReader  = Yii::app()->db->createCommand($sql)->queryRow();

					$submissionHistory = new QuoteVendorSubmissionHistory;
					$submissionHistory->quote_id  =  $quote_id;
					$submissionHistory->vendor_id =  $vendor_id;
					$submissionHistory->reopen_id =  $reopenID;
					$submissionHistory->unit_price = $value['unit_price'];
					$submissionHistory->product_name =  $value['product_name'];
					$submissionHistory->product_code =  $value['product_code'];
					$submissionHistory->tax_rate =  $value['tax_rate'];
					$submissionHistory->shipping =  $value['shipping'];
					$submissionHistory->other_charges =  $value['other_charges'];
					$submissionHistory->quantity =  $value['quantity'];
					$submissionHistory->vendor_product_notes =  $value['vendor_product_notes'];
					$submissionHistory->vendor_repsond_date =  $vendorReader['vendor_repsond_date'];
					$submissionHistory->quote_start_date =  $quoteReader['opening_date'];
					$submissionHistory->quote_end_date =  $quoteReader['closing_date'];
					$submissionHistory->created_at =  date("Y-m-d H:i:s");
					$submissionHistory->save(false);
				}

				$qVenAnsReader = Yii::app()->db->createCommand("select * from quote_vendor_answers WHERE quote_id = $quote_id AND vendor_id = $vendor_id")->query()->readAll();
				if(!empty($qVenAnsReader)){
				foreach($qVenAnsReader as $value){
					if($value['question_type']=='free_text'){
						$anwer = $value['free_text'];
					}else if($value['question_type']=='yes_or_no'){
						$anwer = $value['yes_no'];
					}else{
						$sql = "select * from  quote_answers WHERE quote_id = $quote_id and  id=".$value['quote_answer'];
						$multipleChoiceAns  = Yii::app()->db->createCommand($sql)->queryRow();
						if(!empty($multipleChoiceAns['answer'])){
							$anwer = $multipleChoiceAns['answer'];
						}else{
							$anwer = '';
						}
					}
					$sql = "select * from  quote_questions WHERE quote_id = $quote_id and  id=".$value['question_id'];
					$qReader  = Yii::app()->db->createCommand($sql)->queryRow();

					$ansHistory = new QuoteVendorAnswerHistory;
					$ansHistory->quote_id  =  $quote_id;
					$ansHistory->vendor_id =  $vendor_id;
					$ansHistory->reopen_id =  $reopenID;
					$ansHistory->question_id =  $value['question_id'];
					$ansHistory->question =  $qReader['question'];
					$ansHistory->vendor_scoring_id = $qReader['vendor_scoring_id'];
					$ansHistory->answer =  $anwer;
					$ansHistory->created_at =  date("Y-m-d H:i:s");
					$ansHistory->save(false);
				}}

				$filerReader = Yii::app()->db->createCommand("select * from quote_vendor_files WHERE quote_id = $quote_id AND vendor_id = $vendor_id")->query()->readAll();
				if(!empty($filerReader)){
				foreach($filerReader as $value){
					$fileHistory = new QuoteVendorFilesHistory;
					$fileHistory->quote_id = $quote_id;
					$fileHistory->vendor_id = $vendor_id;
					$fileHistory->reopen_id =  $reopenID;
					$fileHistory->file_name =$value['file_name'];
					$fileHistory->created_at =  date("Y-m-d H:i:s");
					$fileHistory->save(false);
				}}
			}
			// End: Keep History

			// insert product prices first
			$sql = "DELETE FROM quote_vendor_details WHERE quote_id = $quote_id AND vendor_id = $vendor_id";
			Yii::app()->db->createCommand($sql)->execute();

			$product_names = isset($quote_submission_data['product_names']) ? $quote_submission_data['product_names'] : array();
			$product_codes = isset($quote_submission_data['product_codes']) ? $quote_submission_data['product_codes'] : array();
			$quantities = isset($quote_submission_data['quantities']) ? $quote_submission_data['quantities'] : array();
			$prices = isset($quote_submission_data['prices']) ? $quote_submission_data['prices'] : array();
			$unit_prices = isset($quote_submission_data['unit_price']) ? $quote_submission_data['unit_price'] : array();
			$product_notes = isset($quote_submission_data['product_notes']) ? $quote_submission_data['product_notes'] : array();
			$product_tax_rates = isset($quote_submission_data['product_tax_rate']) ? $quote_submission_data['product_tax_rate'] : array();
			$product_shipping_charges = isset($quote_submission_data['product_shipping_charges']) ? $quote_submission_data['product_shipping_charges'] : array();
			$product_other_charges = isset($quote_submission_data['product_other_charges']) ? $quote_submission_data['product_other_charges'] : array();

			$total_product_price = 0;
			for ($idx=0; $idx<count($product_names); $idx++)
			{
				$product_name = isset($product_names[$idx]) ? $product_names[$idx] : "";
				$product_code = isset($product_codes[$idx]) ? $product_codes[$idx] : "";
				$quantity = !empty($quantities[$idx]) ? $quantities[$idx] : 0;
				$price = !empty($prices[$idx]) ? $prices[$idx] : 0;
				$unit_price = !empty($unit_prices[$idx]) ? $unit_prices[$idx] : 0;
				$product_note = !empty($product_notes[$idx]) ? $product_notes[$idx] : "";


				$product_tax_rate = isset($product_tax_rates[$idx]) ? $product_tax_rates[$idx] : 0;
				$product_shipping_charge = isset($product_shipping_charges[$idx]) ? $product_shipping_charges[$idx] : 0;
				$product_other_charge = isset($product_other_charges[$idx]) ? $product_other_charges[$idx] : 0;

				if (!is_numeric($quantity) || empty($quantity)) $quantity = 0;
				if (!is_numeric($price) || empty($price)) $price = 0;
				
				if (!empty($product_name))
				{  
					$total_product_price += $unit_price;

					//Save Qoute product
					$qouteVendorDetail = new QuoteVendorDetails;
					$qouteVendorDetail->quote_id = $quote_id;
					$qouteVendorDetail->vendor_id = $vendor_id;
					$qouteVendorDetail->product_name = $product_name;
					$qouteVendorDetail->product_code = $product_code;
					$qouteVendorDetail->quantity = $quantity;
					$qouteVendorDetail->unit_price = $unit_price;
					$qouteVendorDetail->tax_rate = !empty($product_tax_rate)?$product_tax_rate:0;
					$qouteVendorDetail->shipping = !empty($product_shipping_charge)?$product_shipping_charge:0;
					$qouteVendorDetail->other_charges = !empty($product_other_charge)?$product_other_charge:0;
					$qouteVendorDetail->vendor_product_notes = $product_note;
					$qouteVendorDetail->save(false);
				}
			}
			
			// update the values at the quote level next
			$tax_rate = isset($quote_submission_data['tax_rate']) ? $quote_submission_data['tax_rate'] : 0;
			if (!is_numeric($tax_rate) || empty($tax_rate)) $tax_rate = 0;
			
			$shipping = isset($quote_submission_data['shipping']) ? $quote_submission_data['shipping'] : 0;
			if (!is_numeric($shipping) || empty($shipping)) $shipping = 0;

			$other_charges = isset($quote_submission_data['other_charges']) ? $quote_submission_data['other_charges'] : 0;
			if (!is_numeric($other_charges) || empty($other_charges)) $other_charges = 0;

			$discount = isset($quote_submission_data['discount']) ? $quote_submission_data['discount'] : 0;
			if (!is_numeric($discount) || empty($discount)) $discount = 0;

			$total_price = isset($quote_submission_data['total_price']) ? $quote_submission_data['total_price'] : 0;
			if (!is_numeric($total_price) || empty($total_price)) $total_price = 0;
			
			$update_sql = "UPDATE quote_vendors
							  SET tax_rate = $tax_rate,
							  	  discount = $discount,
							  	  total_price = $total_price,
							  	  other_charges = $other_charges,
							  	  shipping = $shipping,
							  	  submit_status = $submit_quote,
							  	  notes = '$notes',
							  	  vendor_repsond_date = '".date("Y-m-d H:i:s")."',
							  	  updated_datetime = '".date("Y-m-d H:i:s")."'
							WHERE quote_id = $quote_id AND vendor_id = $vendor_id";
			Yii::app()->db->createCommand($update_sql)->execute();
			
			// finally create a notification for the user of the quote that supplier has sent a quote
			$vendor_data = Yii::app()->db->createCommand("SELECT * FROM vendors WHERE vendor_id = $vendor_id")->queryRow();
			$quote_data = Yii::app()->db->createCommand("SELECT * FROM quotes WHERE quote_id = $quote_id")->queryRow();
			
			if (!empty($vendor_data) && !empty($quote_data))
			{
				$user_ids  = array();
				if(!empty($quote_data['created_by_user_id'])) $user_ids[$quote_data['created_by_user_id']] = $quote_data['created_by_user_id'];
				
				if(!empty($quote_data['commercial_lead_user_id'])) $user_ids[$quote_data['commercial_lead_user_id']] = $quote_data['commercial_lead_user_id'];
				
				if(!empty($submit_quote)){
					$notificationComments  = "<b>".$vendor_data['vendor_name']."</b> has responded to <b>".$quote_id."</b> - <b>".$quote_data['quote_name']."</b>";
				foreach($user_ids as $user_value){
					$notification = new Notifications;
					$notification->id = 0;
					$notification->user_id = $user_value;
					$notification->user_type = 'Client';
					$notification->notification_text ='<a href="' .  str_replace("supplier/quotes","quotes",AppUrl::bicesUrl('quotes/edit/' . $quote_id)) .'">'.addslashes($notificationComments).'</a>';
					$notification->notification_date = date("Y-m-d H:i");
					$notification->read_flag = 0;
					$notification->notification_type = 'Quote';
					$notification->save(false);

					$sql  = 'select full_name,email from users where user_id='.$user_value; 
            		$userReader = Yii::app()->db->createCommand($sql)->queryRow();

            	$sql  = 'select quote_name from quotes where quote_id='.$quote_id; 
            	$quoteReader = Yii::app()->db->createCommand($sql)->queryRow();
					
				$email = $userReader['email'];
		         $name  = $userReader['full_name'];
		         $subject = $quoteReader['quote_name'].' Notification';
		         $notificationText = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_id) . '" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$notificationComments.'</a>';
		         $url = AppUrl::bicesUrl('quotes/edit/' . $quote_id);
		         EmailManager::userNotification($email,$name,$subject,$notificationText,$url);

				}}
			}

			$this->notifyContributors($quote_id,$vendor_id);
		}

		if($submit_quote==1){
			$submitStatus = "Quote Submitted successfully";
		}else {
			$submitStatus = "Quote saved successfully";
		}

		Yii::app()->user->setFlash('success',$submitStatus);	
	}

	public function deleteQuoteDocumentandAnswers($quote_id, $vendor_id)
	{
//		$results = $this->executeQuery("SELECT * FROM quote_vendor_files WHERE quote_id = $quote_id AND vendor_id = $vendor_id");
//		foreach($results as $result){
//			unlink('uploads/quotes/' . $quote_id . '/' . $vendor_id . '/' . $result['file_name']);
//		}
		


		// Yii::app()->db->createCommand("DELETE FROM quote_vendor_files WHERE quote_id = $quote_id AND vendor_id = $vendor_id")->execute();

		Yii::app()->db->createCommand("DELETE FROM quote_vendor_answers WHERE quote_id = $quote_id AND vendor_id = $vendor_id")->execute();
	}

	/**
	 * Manages all models.
	 */
	 

	public function actionQouteDocument()
	{   $quoteID  = $_GET['quote_id'];
		$vendorID = UtilityManager::getVendorID();
		$quote_vendor_id =  $vendorID;
		$docName = $_GET['doc_name'];

		if(!empty($_GET['vendor_id'])){
			 $file = Yii::getPathOfAlias('webroot')."/uploads/quotes/".$quoteID."/".$vendorID."/".$docName;
		}else{
			 $file = Yii::getPathOfAlias('webroot')."/uploads/quotes/".$quoteID."/".$docName;
		}
       
		if ($docName) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
			//header("Content-Type: application/pdf");
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		   readfile($file);
      	Yii::app()->end();
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Quotes the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Quotes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Quotes $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='quotes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionUploadDocumentModal(){  
       //date_default_timezone_set(Yii::app()->params['timeZone']);
       $uploaded = 0;
        if (!empty($_FILES)){
        	$vendor_id = UtilityManager::getVendorID();
            $quote_id = $_POST['quote_id'];
            if (isset($_FILES['file']) && is_array($_FILES['file'])){
                $path = Yii::getPathOfAlias('webroot').'/uploads/quotes';
                if (!is_dir($path)) mkdir($path);
                if (!is_dir($path.'/'.$quote_id))
                    mkdir($path.'/'.$quote_id);
                 $path .= '/'.$quote_id;
                if (!is_dir($path. '/' . $vendor_id))
			            mkdir($path.'/' . $vendor_id);
			      $path .= '/'.$vendor_id;
			                

                if (!empty($_FILES['file']['name']))
                {
                    $file_name = time().'_'.$_FILES['file']['name'];
                    $file_description = $_POST['file_description'];
                    
                    if(move_uploaded_file($_FILES['file']['tmp_name'], $path.'/'. $file_name)){

					//Save documents
						$qouteVendorFile = new QuoteVendorFiles;
						$qouteVendorFile->quote_id = $quote_id;
						$qouteVendorFile->vendor_id = $vendor_id;
						$qouteVendorFile->file_name = $file_name;
						$qouteVendorFile->file_description = $file_description;
						$qouteVendorFile->save(false);

					$uploaded = 1;
                       
                    }  
                }
            }
        }
        $data = array();
        $data['uploaded'] =$uploaded;
        echo json_encode($data);exit;

    }


    public function actionQuoteFile(){
    	
    	$vendor_id = UtilityManager::getVendorID();
        $quote_id = $_POST['quote_id'];

    	$quoteFile = QuoteVendorFiles::model()->findAll(array('condition'=>'quote_id='.$quote_id.' and vendor_id='.$vendor_id));
        $exp = "<table class='table table-striped table-bordered' style='width: 100%;'><tr><th>File</th><th>Description</th><th>Action</th></tr>";
        foreach($quoteFile as $key=>$value){
        	$deleteFunc = 'deleteDocumentModal("'.$value->id.'")';
        	$downloadUrl = $this->createUrl('quotes/qouteDocument',array('quote_id'=>$quote_id,'vendor_id'=>$vendor_id,'doc_name'=>$value->file_name));

        	$fileName =substr($value->file_name, strpos($value->file_name, "_") + 1);
        	$fileDescription = $value->file_description;

        	$exp .="<tr><td>".$fileName."</td>";
        	$exp .="<td>".$fileDescription."</td>";
        	$exp .="<td><a href='".$downloadUrl."' target='quote_file'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span></a>";

            if(FunctionManager::quoteCheckStatus($quote_id)==4){
            	$exp .="<a style='cursor: pointer; padding: 5px;' onclick='".$deleteFunc."'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a>";
        	}
             $exp .="</td></tr>";
        }
 

        $exp .= "</table>";

        $sql = "SELECT * FROM quote_files WHERE quote_id = ".$quote_id." ORDER BY id";

        $qouteDocumentDownload = Yii::app()->db->createCommand($sql)->query()->readAll();

		$exp2 = "<table class='table table-striped table-bordered' style='width: 100%;'><tr><th>File</th><th>Description</th><th>Action</th></tr>";

		foreach($qouteDocumentDownload as $key=>$value) {


		$uploadedFileUrl = $this->createUrl('quotes/qouteDocument',array('quote_id'=>$quote_id,'doc_name'=>$value['file_name']));

		$fileName =substr($value['file_name'], strpos($value['file_name'], "_") + 1);

		$exp2 .= "<tr><td>".$fileName."</td>"; 	
		   //$exp2 .= "";
		   $exp2 .= "<td>".$value['description']."</td>";
		   $exp2 .= "<td><a href=".$uploadedFileUrl." target='quote_file'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span></a></td>";
		   $exp2 .= "</tr>";
		  }
		$exp2 .= "</table>";
		 
        $data = array();
        $data['uploaded_list'] = $exp;
        $data['uploaded_list_2'] = $exp2;
        echo json_encode($data);exit;

    }

     public function actionDeleteDocumentModal()
	{
		$vendor_id = UtilityManager::getVendorID();
		 $quote_id = $_POST['quote_id'];
		$key = $_POST['documentKey'];
		$uploaded='';
		$quoteFile = QuoteVendorFiles::model()->findByPk($key);

    	$file = Yii::getPathOfAlias('webroot')."/uploads/quotes/$quote_id/$vendor_id/".$quoteFile->file_name;
    	if(unlink($file)){
    		$uploaded=1;
    		$quoteFile->delete();
    	}
        
        $data['uploaded'] =$uploaded;
        echo json_encode($data);exit;
	}

	public function actionVendorFormQuestions(){
		if(!empty($_POST['formId'])) {
		 $sql = "select id from form_answers where form_id=".$_POST['formId']." and quote_id=".$_POST['quoteID']." and user_id=".$_POST['vendor_id']." group by form_id ";
		 $fileReader = Yii::app()->db->createCommand($sql)->queryRow();
		 
		 $formquestions = $status = '';
			if(!empty($fileReader['id'])){
				$sql = "SELECT * FROM `form_answers` where form_id=".$_POST['formId']." and user_id=".$_POST['vendor_id']." and quote_id=".$_POST['quoteID'];
		  		$formquestions = Yii::app()->db->createCommand($sql)->query()->readAll();
		  		$status = 'ans';
			}else{
				$sql = "SELECT  id, form_id, question FROM `form_questions` where form_id=".$_POST['formId'];
		  		$formquestions = Yii::app()->db->createCommand($sql)->query()->readAll();
		  		$status = 'question';
			}

		  echo json_encode(array('formquestions' => $formquestions, 'status' => $status)); exit;
		}
	}

	public function actionAnswerSave(){
		
		if(!empty($_POST['formRender'])){
			$questionForm = new QuoteVendorsQuestionnaire();
			$questionForm->rs = array();
			$questionForm->rs['id'] 	  = $_POST['qouteFormId'];
			$questionForm->rs['quote_id']= $_POST['quoteID'];
			$questionForm->rs['status']  = 'Completed';
			$questionForm->write();
			
			
		 $sql = "delete from form_answers where quote_id=".$_POST['quoteID']." and user_id=".$_POST['vendor_id']." and form_id=".$_POST['formId'];
		 Yii::app()->db->createCommand($sql)->execute();

		 $sql = "delete from vendor_question_answer where quote_id=".$_POST['quoteID']." and vendor_id=".$_POST['vendor_id']." and form_id=".$_POST['formId'];
		   Yii::app()->db->createCommand($sql)->execute();
		
		 $formRender = $this->formRenderUpdate($_POST['formRender']);
		
		 foreach($formRender as $formanswerObj) {
		
		   $question = new FormAnswer();
		   $question->rs = array();
		   $question->rs['id'] = 0;
		   $question->rs['form_id'] = $_POST['formId'];
		   $question->rs['quote_id']= $_POST['quoteID'];
		   $question->rs['user_id'] = $_POST['vendor_id'];
		   $question->rs['answer']  = addslashes(json_encode($formanswerObj, JSON_UNESCAPED_UNICODE));
		   $question->rs['created_datetime'] = date("Y-m-d");
		   $question->write();

		   $questionQuestionAnswer = new VendorQuestionAnswer();
		   $questionQuestionAnswer->rs = array();
		   $questionQuestionAnswer->rs['id'] = 0;
		   $questionQuestionAnswer->rs['form_id']  = $_POST['formId'];
		   $questionQuestionAnswer->rs['quote_id'] = $_POST['quoteID'];
		   $questionQuestionAnswer->rs['vendor_id']= $_POST['vendor_id'];
		   $questionQuestionAnswer->rs['questions']= addslashes($formanswerObj['label']);
			//  select and radio the answer is comming so first we search the value in array and take the key of value save into answer
		    if($formanswerObj['type'] == 'select' || $formanswerObj['type'] == 'radio-group'){
			 $ans = $this->checkLabelExists($formanswerObj['values'], $formanswerObj['userData']);
			 $questionQuestionAnswer->rs['answer'] = !empty($ans) ? addslashes(implode(", ", $ans)) : '';
		    }else{
		   	 $questionQuestionAnswer->rs['answer'] = !empty($formanswerObj['userData'][0]) ? addslashes($formanswerObj['userData'][0]) : '';
		    }
		    $questionQuestionAnswer->rs['type'] = $formanswerObj['type'];
		    $questionQuestionAnswer->rs['created_datetime'] = date("Y-m-d");
		    $questionQuestionAnswer->write();
		 } 

		}
		echo json_encode(["status" => 1]); exit;  
	}

	private function formRenderUpdate($formData){
		// $checkedboxArr = [];
		// $formanswerObjUpdate = [];
		// foreach($formData as $formanswerObj) {
		//  if($formanswerObj['type'] === 'checkbox-group' && !empty($formanswerObj['userData'])){
			
		// 	foreach($formanswerObj['values'] as $value){	
		// 		if(!empty($value['value']) && (in_array($value['value'], $formanswerObj['userData']))){
		// 		  $checkedboxArr[] = array_merge($value, ['selected'=>"true"]);
		// 		}elseif (!in_array($value['value'], $formanswerObj['userData'])){ 
		// 		  $checkedboxArr[] = array_merge($value, ['selected'=> ""]);
		// 		}
		// 	}
		// 	$formanswerObj['values'] = $checkedboxArr;	 
		//  } 
		// array_push($formanswerObjUpdate, $formanswerObj);
		// }
		return $formData;
	}


	private function notifyContributors($quote_id, $vendor_id){
			
		$sql  ='select user_id from quote_user where quote_id='.$quote_id; 
		$userArr = Yii::app()->db->createCommand($sql)->queryAll();
		
		$vendor_data = Yii::app()->db->createCommand("SELECT vendor_name FROM vendors WHERE vendor_id = $vendor_id")->queryRow();
		$quote_data = Yii::app()->db->createCommand("SELECT quote_id,quote_name FROM quotes WHERE quote_id = $quote_id")->queryRow();
		
		$notificationComments  = "<b>".$vendor_data['vendor_name']."</b> has responded to <b>".$quote_id."</b> - <b>".$quote_data['quote_name']."</b>";
		foreach($userArr as $user_value){
			$notification = new Notifications;
			$notification->id = 0;
			$notification->user_id = $user_value['user_id'];
			$notification->user_type = 'Client';
			$notification->notification_text ='<a href="' .  str_replace("supplier/quotes","quotes",AppUrl::bicesUrl('quotes/edit/' . $quote_id)) .'">'.addslashes($notificationComments).'</a>';
			$notification->notification_date = date("Y-m-d H:i");
			$notification->read_flag = 0;
			$notification->notification_type = 'Quote';
			$notification->save(false);
			
			$sql  = 'select full_name,email from users where user_id='.$user_value['user_id']; 
			$userReader = Yii::app()->db->createCommand($sql)->queryRow();
			
			$email = $userReader['email'];
			$name  = $userReader['full_name'];
			$subject = $quote_data['quote_name'].' Notification';
			$notificationText = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_id) . '" style="font-size:18px;color:#2d9ca2;text-decoration: none;">'.$notificationComments.'</a>';
			$url = AppUrl::bicesUrl('quotes/edit/' . $quote_id);
			EmailManager::emailQuoteContributorUser($email,$name,$subject,$notificationText,$url);
		}
			
	}

	function checkLabelExists($options, $searchLabels) {
		// Convert search labels to keys for faster look-up
		$searchSet = array_flip($searchLabels);
		$matchingLabels = [];
		foreach ($options as $index => $option) {
			if (isset($option['label']) && isset($searchSet[$option['label']])) {
				$matchingLabels[] = $option['label'];
			}
			if (isset($option['value']) && isset($searchSet[$option['value']])) {
				$matchingLabels[] = $option['label'];
			}
		}
		return !empty($matchingLabels) ? array_unique($matchingLabels) : false;
	}

}
