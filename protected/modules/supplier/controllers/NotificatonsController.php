<?php
class NotificatonsController extends Controller
{
	public $current_option = '';
	public function actionIndex()
	{  
		// may be we need only notifications for a certain period
		
		$this->current_option = 'notifications';
		$db_from_date = "";
		if (!empty( $_POST['from_date']))
		{
			$from_date = $_POST['from_date'];
	       	list($input_date, $input_month, $input_year) = explode("/", $from_date);
	       	$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['from_date'] = $db_from_date;
		}

		$db_to_date = "";
		if (!empty($_POST['to_date']))
		{   $to_date = $_POST['to_date'];
	       	list($input_date, $input_month, $input_year) = explode("/", $to_date);
	       	$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
			$view_data['to_date'] = $db_to_date;
		}
		// and display various approvals to the user
		$notification = new Notifications();
		$view_data['notifications'] = $notification->getMyNotifications(true, $db_from_date, $db_to_date);
		$this->render('index', $view_data);
	}


	public function actionDeleteNotifications()
	{
		$notifications_to_delete = isset($_REQUEST['notification_ids_to_delete']) ? $_REQUEST['notification_ids_to_delete'] : "0";
		$notification = new Notifications();		
		$notification->deleteNotifications($notifications_to_delete);
		$this->redirect(AppUrl::bicesUrl('notificatons/index'));	
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}