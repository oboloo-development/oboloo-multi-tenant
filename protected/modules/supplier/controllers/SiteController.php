<?php

class SiteController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public $current_option = 'dashboard';


	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('login','contractDocumentLogin','vendorDocumentLogin','captcha','recovery','logout','oAuth2Callback','extension','cronejobEamil','forgot','changePassword','changePasswordSave'),
				'users'=>array('*'),
			),
		
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('manage','updateAccount','index','updateProfile','markUp','home'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionUpdateProfile(){
		$model = Vendor::model()->findByPk(Yii::app()->user->id);

		if(isset($_POST['Vendor'])){ //echo "<pre>"; print_r($_POST);exit;

				$model->attributes=$_POST['Vendor'];
				$imageUploadFile = CUploadedFile::getInstance($model, 'profile_image');
				$rnd = rand(0,9999);
				if($imageUploadFile != ""){
					$fileName = "{$rnd}-{$imageUploadFile}";  // random number + file name
					$model->profile_image = $fileName;
				}
				if($model->save(false)){
					if ($imageUploadFile != "") { // validate to save file
						$imageUploadFile->saveAs(Yii::app()->basePath . '/../files/profile-images/' . $fileName);
					}
				Yii::app()->user->setFlash('success', "your Profile is updated");
				$this->redirect(array('site/updateProfile'));
			}else{

				Yii::app()->user->setFlash('error', "Some thing Went wrong");
				$this->redirect(array('site/updateProfile'));

			}

		}
		$this->render('updateProfile',array('model'=>$model));
	}


	public function getVendorID(){
	$type = Yii::app()->user->getState('type');

	if($type == "1" || $type == "2" || $type == "3") {

		$parentVendor =  VendorTeammember::model()->findByAttributes(array('teammember_id'=>Yii::app()->user->id));
		$vendorID = $parentVendor->vendor_id;
	 }else{
	 	$vendorID = Yii::app()->user->id;
	 }
	 return $vendorID;
    }



	
	public function actionIndex()
	{
       $this->layout = "main";
       error_reporting(0);
	   $this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */

	/**
	 * Displays the login page
	 */
	/*
	* ADMIN LOGIN
	*/
	public function actionLogin()
	{
		$this->layout = 'login';
		$model = new LoginForm;
		
		if(!empty($_POST['LoginForm']['username']))
		{
			$model->attributes=$_POST['LoginForm'];
			
			$identity = new UserIdentity($_POST['LoginForm']['username'],'');

			if($model->validate()) {
			if($identity->authenticate()){
				Yii::app()->user->login($identity);
				Yii::app()->session->remove('error');
				$quoteID = Yii::app()->user->getState('quote_id');
				$this->redirect(array('quotes/view','quote_id'=>$quoteID));
				//$this->redirect(Yii::app()->user->returnUrl);
				Yii::app()->end();
			}else
			{
				Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error!</strong> Invalid login information.</div>');
				//Yii::app()->session->add('error', 'Invalid login information');
				$this->redirect(array('site/login'));
				Yii::app()->end();
			}
		}else{
				Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error!</strong> Invalid login information.</div>');
				$this->redirect(array('site/login'));
				Yii::app()->end();
			}
		}
		
		$this->render('login', array('model' => $model));
	}

	public function actionContractDocumentLogin()
	{
		$this->layout = 'login';
		$model = new LoginForm;
		
		if(!empty($_POST['LoginForm']['username']))
		{
			$model->attributes=$_POST['LoginForm'];
			
			$identity = new UserIdentity($_POST['LoginForm']['username'],'');

			if($model->validate()) {
			if($identity->authenticate()){
				Yii::app()->user->login($identity);
				Yii::app()->session->remove('error');
				$contractID = Yii::app()->user->getState('contract_id');
				$this->redirect(array('contracts/viewDocument','id'=>$contractID));
				//$this->redirect(Yii::app()->user->returnUrl);
				Yii::app()->end();
			}else
			{
				Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error!</strong> Invalid login information.</div>');
				//Yii::app()->session->add('error', 'Invalid login information');
				$this->redirect(array('site/login'));
				Yii::app()->end();
			}
		}else{
				Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error!</strong> Invalid login information.</div>');
				$this->redirect(array('site/login'));
				Yii::app()->end();
			}
		}
		
		$this->render('login', array('model' => $model));
	}

	public function actionVendorDocumentLogin()
	{
		$this->layout = 'login';
		$model = new LoginForm;
		
		if(!empty($_POST['LoginForm']['username']))
		{
			$model->attributes=$_POST['LoginForm'];
			
			$identity = new UserIdentity($_POST['LoginForm']['username'],'');

			if($model->validate()) {
			if($identity->authenticate()){
				Yii::app()->user->login($identity);
				Yii::app()->session->remove('error');
				$this->redirect(array('vendor/documents'));
				//$this->redirect(Yii::app()->user->returnUrl);
				Yii::app()->end();
			}else
			{
				Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error!</strong> Invalid login information.</div>');
				//Yii::app()->session->add('error', 'Invalid login information');
				$this->redirect(array('site/login'));
				Yii::app()->end();
			}
		}else{
				Yii::app()->user->setFlash('error', '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error!</strong> Invalid login information.</div>');
				$this->redirect(array('site/login'));
				Yii::app()->end();
			}
		}
		
		$this->render('login', array('model' => $model));
	}


	 public function actionHome() {
	 	$this->current_option = 'home';
        $this->render('home');
    }


	public function actionForgot() {
        // make sure some one is not logged in
        $this->pageTitle = "Forgot Password";
        $this->layout = 'login';

        // may be the form was submitted
        $view_data['success'] = $view_data['error'] = 0;
        if (isset($_POST['form_submitted'])) {
            $email = $_POST['email'];
            $user = new User();
            $return = EmailManager::userPasswordForgotReuest($email);
            
            if ($return)
                $view_data['error'] = 1;
            else
                $view_data['success'] = 1;
        }

        // display forgot password form
        $this->render('forgot', $view_data);
    }

    public function actionChangePassword() {
        // make sure some one is not logged in
        $this->pageTitle = "Change Password";
        $this->layout = 'login';
        // may be the form was submitted
        $view_data['success'] = $view_data['error'] = 0;
      
        if (!empty($_POST['form_submitted'])) {
        	$form_submitted = $_POST['form_submitted'];
            //$username = $_POST['username'];
            $email = $_POST['email'];
            $return = EmailManager::userPasswordChangeReuest($email);
            if ($return)
                $view_data['success'] = 1;
            else
                $view_data['error'] = 1;
        }

        // display forgot password form
        $this->render('change_password', $view_data);
    }

    public function actionChangePasswordSave() {
        // make sure some one is not logged in

        
        $this->pageTitle = "Change Password";
        $this->layout = 'login';

        $checkDate = date("m-d");
        $emailDate = base64_decode($_GET['time']);
        $vendorID=base64_decode($_GET['user-id']);

        $vendor=Vendor::model()->findByAttributes(array('vendor_id'=>$vendorID));
        if($checkDate==$emailDate){
        $view_name = 'change_password_save';
        // may be the form was submitted
        $view_data['success'] = $view_data['error'] = 0;
        
        if (!empty($_POST['form_submitted'])) {
        	$form_submitted = $_POST['form_submitted'];
            $current_pass = $_POST['password'];
            $new_pass = $_POST['password_new'];
            $new_pass_confirm = $_POST['password_new_confirm'];

            if($vendor->password !=md5($current_pass)){
                $view_data['error'] = 2;
            }else if($new_pass !=$new_pass_confirm){
                $view_data['error'] = 3;
            }else if (strlen($new_pass) < 6 ) {
                $view_data['error'] = 4 ;
            }

            if($view_data['error']==0){
            	$vendor->password = MD5($new_pass);
            	$vendor->password = $new_pass;
            	$vendor->password;
            	
                $vendor->save(false);
                $return = EmailManager::userPasswordChanged($vendor);
                if (!$return)
                    $view_data['success'] = 1;
                    $view_name = 'change_password_alert';
                }
            }
        }else{
            $view_data['error'] = 1;
            $view_name = 'change_password_alert';
        }

        $this->render($view_name, $view_data);
    }


	
	public function actionUpdateAccount()
	{
		  $model = Vendor::model()->findByPk(Yii::app()->user->id);
			
			if(isset($_POST['Vendor'])){ //echo "<pre>"; print_r($_POST);exit;
			
			//$model = Admin::model()->findByAttributes(array('email'=>$_POST['email']));
			
			if($model){
				$model->organization = $_POST['Vendor']['organization'];
				$model->email = $_POST['Vendor']['email'];
				$model->phone = $_POST['Vendor']['phone'];
				$model->first_name = $_POST['Vendor']['first_name'];
				$model->last_name = $_POST['Vendor']['last_name'];
				$model->save(false);
				Yii::app()->user->setFlash('success', "your Profile is updated");
				$this->redirect(array('site/updateAccount'));
				}else{
					
					Yii::app()->user->setFlash('error', "Some thing Went wrong");
				$this->redirect(array('site/updateAccount'));
					
					}
				
				}
		$this->render('updateAccount',array(
			'model'=>$model,
		));
	}
	
	public function actionLogout()
	{
		$pageType = Yii::app()->user->getState('page_type');
		if($pageType=='Contract Document'){
			$url = $this->createUrl('site/contractDocumentLogin');
		}if($pageType=='Supplier Document'){
			$url = $this->createUrl('site/vendorDocumentLogin');
		}else{
			$url = $this->createUrl('site/login');
		}
		$this->redirect($url);
	}
}