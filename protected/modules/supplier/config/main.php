<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
 
 $adminConfig =
    array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'timeZone' => 'Asia/Macau',
	'name'=>'Commercial Software',
	'defaultController'=>'site',
);

