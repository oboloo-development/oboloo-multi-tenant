<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

/*
$services_json = json_decode(getenv("VCAP_SERVICES"),true);
$mysql_config = $services_json["mysql-5.1"][0]["credentials"];
$username = $mysql_config["username"];
$password = $mysql_config["password"];
$hostname = $mysql_config["hostname"];
$port = $mysql_config["port"];
$db = $mysql_config["name"];
*/
$folderToRemove = '\custom_doamin_config';
$updatedPath = str_replace($folderToRemove, '', dirname(__FILE__));
return array(
	'basePath'=> $updatedPath.DIRECTORY_SEPARATOR.'..',
	'name'=>'Procurement Software',
    // 'language' => 'fmo',
    'timeZone' => 'UTC',

	// preloading 'log' component
	'preload'=>array('log', 'input'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.controllers.CommonController',
		'application.controllers.customDomain.resco.*',
		'application.models.customDomain.resco.*',
	),
	'modules'=>array(
         'gii'=>array(
                'class'=>'system.gii.GiiModule',
                'password'=>'malik',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters'=>array('127.0.0.1','::1'),
            )
		,
        'supplier'=>array(

        'defaultController' => 'default',

    ),
    ),

	'defaultController'=>'app',
        // application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		'db'=>require($updatedPath.'/database.php'),

            'urlManager'=>array(
			'urlFormat'=>'path',
 				'showScriptName'=>false,
                        //'caseSensitive'=>false,
                'rules'=>array(
				'register'=>'app/register',
				'forgot'=>'app/forgot',
				'logout'=>'app/logout',
				'login'=>'app/login',
				'dashboard'=>'app/dashboard',
				'home'=>'app/home',
				'quick-sourcing-evaluation'=>'vendorEvaluation/list',
				'quick-sourcing-evaluation/edit/<score_record:\d+>'=>'vendorEvaluation/edit',
				'vendors/list'=>'customDomain/resco/vendors/list',
				'vendors/edit/<vendor_id:\d+>'=>'customDomain/resco/vendors/edit',
				'vendors/archiveList'=>'customDomain/resco/vendors/archiveList',
				'vendors/uploadDocument'=>'customDomain/resco/vendors/uploadDocument',
				'vendors/requestVendorDocument'=>'customDomain/resco/vendors/requestVendorDocument',
				'vendors/createSupplierContract'=>'customDomain/resco/vendors/createSupplierContract',
				'contracts/list'=>'customDomain/resco/contracts/list',
				'contracts/archiveList'=>'customDomain/resco/contracts/archiveList',
				'contracts/edit/<contract_id:\d+>'=>'customDomain/resco/contracts/edit',
				'contracts/saveArhive/<contract_id:\d+>'=>'customDomain/resco/contracts/saveArhive',
				'contracts/uploadDocument'=>'customDomain/resco/contracts/uploadDocument',
				'contracts/requestVendorDocument'=>'customDomain/resco/contracts/requestVendorDocument',
				'contracts/approveDocument'=>'customDomain/resco/contracts/approveDocument',
				'contracts/documentArchive'=>'customDomain/resco/contracts/documentArchive',
				'contracts/documentUnArchive'=>'customDomain/resco/contracts/documentUnArchive',
				'contracts/deleteDocument'=>'customDomain/resco/contracts/deleteDocument',
				'contracts/deleteDocumentPending'=>'customDomain/resco/contracts/deleteDocumentPending',
				'contracts/loadingRequestDocument'=>'customDomain/resco/contracts/loadingRequestDocument',
				'contracts/deleteDocumentOther'=>'customDomain/resco/contracts/deleteDocumentOther',
				'contracts/editDocument'=>'customDomain/resco/contracts/editDocument',
				'contracts/deleteFile'=>'customDomain/resco/contracts/deleteFile',
				'contracts/documentDownload'=>'customDomain/resco/contracts/documentDownload',
				'app/settings'=>'customDomain/resco/app/settings',
				'quotes/openForScoring/<id:\d+>'=>'quotes/openForScoring',
				'locations/edit/<location_id:\d+>/*'=>'locations/edit',
				'users/edit/<user_id:\d+>'=>'users/edit',
				'users/delete/<user_id:\d+>'=>'users/delete',
				'products/edit/<product_id:\d+>'=>'products/edit',
				'bundles/edit/<bundle_id:\d+>'=>'bundles/edit',
				'quotes/edit/<quote_id:\d+>'=>'quotes/edit',
				'quotes/questionnaires/<quote_id:\d+>'=>'quotes/questionnaires',
				'quotes/clone/<quote_id:\d+>'=>'quotes/clone',
				'savings/edit/<saving_id:\d+>'=>'savings/edit',
				'orders/edit/<order_id:\d+>'=>'orders/edit',
				'orders/clone/<order_id:\d+>'=>'orders/clone',
				'expenses/edit/<expense_id:\d+>'=>'expenses/edit',
				'expenses/clone/<expense_id:\d+>'=>'expenses/clone',
				'routing/edit/<approval_id:\d+>'=>'routing/edit',
				'purchaseOrders/edit/<po_id:\d+>'=>'purchaseOrders/edit',
				'customformbuilder/edit/<id:\d+>'=>'customformbuilder/edit',
				'customformbuilder/view/<id:\d+>'=>'customformbuilder/view',
				'customformbuilder/answer/<id:\d+>'=>'customformbuilder/answer',
				'customformbuilder/viewanswer/<id:\d+>'=>'customformbuilder/viewanswer',
				'quotes/myscoringdetail/<quote_id:\d+>'=>'quotes/myscoringdetail',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>'
				),
		),
		 

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
    	'input'=>array(
            'class'         => 'CmsInput',
            'cleanPost'     => false,
            'cleanGet'      => false,
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
        'params'=> require($updatedPath.'/params.php'),
);
