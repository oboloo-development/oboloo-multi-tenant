<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

/*
$services_json = json_decode(getenv("VCAP_SERVICES"),true);
$mysql_config = $services_json["mysql-5.1"][0]["credentials"];
$username = $mysql_config["username"];
$password = $mysql_config["password"];
$hostname = $mysql_config["hostname"];
$port = $mysql_config["port"];
$db = $mysql_config["name"];
*/

$homeController = "USG/strategicSourcing";
$homeAction = "list";

$userController = "usersUsg";
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Procurement Software',
    // 'language' => 'fmo',
    'timeZone' => 'UTC',

	// preloading 'log' component
	'preload'=>array('log', 'input'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.controllers.CommonController',
        'application.controllers.USG.*',
	),
	'modules'=>array(
         'gii'=>array(
                'class'=>'system.gii.GiiModule',
                'password'=>'malik',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters'=>array('127.0.0.1','::1'),
            )
		,
        'supplier'=>array(

        'defaultController' => 'default',

    ),
    ),

	'defaultController'=>$homeController,
        // application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		'db'=>require(dirname(__FILE__).'/database.php'),

		/*'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=live2_db_2',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			
			'connectionString' => "mysql:host=$hostname;dbname=$db",
			'emulatePrepare' => true,
			'username' => "$username",
			'password' => "$password",
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
			

			
		),*/

                'urlManager'=>array(
			'urlFormat'=>'path',
 				'showScriptName'=>false,
                        //'caseSensitive'=>false,
                        'rules'=>array(
				'register'=>'app/register',
				'forgot'=>'app/forgot',
				'logout'=>'app/logout',
				'login'=>'app/login',
				'dashboard'=>'app/dashboard',
				'home'=>$homeController.'/'.$homeAction,
				'savingslist'=>'savingsUsg/list',
				'savingsUsg/edit/<saving_id:\d+>'=>'savingsUsg/edit',
				'goals' => $homeController.'/goals',
				'quick-sourcing-evaluation'=>'vendorEvaluation/list',
				'quick-sourcing-evaluation/edit/<score_record:\d+>'=>'vendorEvaluation/edit',
				'vendors/edit/<vendor_id:\d+>'=>'vendors/edit',
				'locations/edit/<location_id:\d+>/*'=>'locations/edit',
				$userController.'/edit/<user_id:\d+>'=>$userController.'/edit',
				$userController.'/delete/<user_id:\d+>'=>$userController.'/delete',
				'products/edit/<product_id:\d+>'=>'products/edit',
				'bundles/edit/<bundle_id:\d+>'=>'bundles/edit',
				'quotes/edit/<quote_id:\d+>'=>'quotes/edit',
				'quotes/clone/<quote_id:\d+>'=>'quotes/clone',
				'strategicSourcing/edit/<saving_id:\d+>'=> $homeController.'/edit',
				'strategicSourcing/savingApprove'=> $homeController.'/savingApprove',
				'orders/edit/<order_id:\d+>'=>'orders/edit',
				'orders/clone/<order_id:\d+>'=>'orders/clone',
				'contracts/edit/<contract_id:\d+>'=>'contracts/edit',
				'expenses/edit/<expense_id:\d+>'=>'expenses/edit',
				'expenses/clone/<expense_id:\d+>'=>'expenses/clone',
				'routing/edit/<approval_id:\d+>'=>'routing/edit',
				'purchaseOrders/edit/<po_id:\d+>'=>'purchaseOrders/edit',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>'
			   ),
		),
		 

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
    	'input'=>array(
            'class'         => 'CmsInput',
            'cleanPost'     => false,
            'cleanGet'      => false,
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
        'params'=> require(dirname(__FILE__).'/params.php'),
);
