<?php
/*********** Custom functions used only for SAAS app-config file ***********/
function getSubdomain($domain)
{
    $segments = explode('.',$domain);
    // If no subdomain exist
    if( count( $segments ) <= 2){
        return false;
    }
    // else sanitize and return
    $subdomain = str_replace( "http://", "", $segments[0] );
    $subdomain = str_replace( "https://", "", $subdomain );

    return $subdomain;
}

function getDatabaseForSubdomain( $subdomain )
{
    
    $conn = mysqli_connect('localhost', APP_FOREMAN_USER, APP_FOREMAN_PASS, APP_FOREMAN_DB);
    
    if (mysqli_connect_errno()) {
        error_log( "Failed to connect to MySQL: " . mysqli_connect_error() );
    }

    $subdomain = mysqli_real_escape_string($conn, $subdomain);
    $result = mysqli_query( $conn, "SELECT database_name, database_host, database_user, database_pass, expiry_date, active, checkout_type, is_trial FROM foreman_clients WHERE subdomain = '$subdomain'" );
 
    if ( $result->num_rows > 0 ) {
        $row = $row=mysqli_fetch_assoc($result);
        mysqli_free_result($result);
        mysqli_close($conn);
        
        $dbUser = encryptor( $row['database_user'], 'decrypt' );
        $dbPass = encryptor( $row['database_pass'], 'decrypt' );
   
        // Check if the database actually exists according to records
        $conn = mysqli_connect($row['database_host'], $dbUser, $dbPass );
        
        if ( !mysqli_select_db($conn, $row['database_name']) ) {
            return FALSE;
        }
        mysqli_close($conn);

        return $row;
    } else {
        return FALSE;
    }
}

function getCurrentDataSite($dbUser='SWRVWDB4UVMyVjVycWhNY25VVk9HNlFrL1JSamtYZFhBSWkzUGlVa09oUT0=',$dbPassword='N3VCdnVkaGxPM0pqRDc3b0t0dDROZz09',$dbName='oboloo_ridakhan_2')
{
   /* 
     $siteuserName = "sandbox";
    $sitepassword = "$2a$08$G1Ka/0m/4O2ZWfIBT7UPXOu/a1CJhRaYIlfvDN4ExqRVS7WO14YAC";
    $sitedatabase = "oboloo_ridakhan_2";*/
    
     $dbUser = encryptor( $dbUser, 'decrypt' );
     $dbPassword = encryptor( $dbPassword, 'decrypt' );
    
    $conn = mysqli_connect('localhost', $dbUser, $dbPassword, $dbName);
    if (mysqli_connect_errno()) {
        error_log( "Failed to connect to MySQL: " . mysqli_connect_error() );
    }
    $result = mysqli_query($conn, "SELECT * FROM users WHERE user_id=1");

    if ( $result->num_rows > 0 ) {
        $row = $row=mysqli_fetch_assoc($result);
        mysqli_free_result($result);
        mysqli_close($conn);
        return $row;
    } else {
        return FALSE;
    }
}


function encryptor( $string, $action = 'encrypt' )

{

    $secret_key = FOREMAN_ENCRYPTIONSECRET;

    $secret_iv = FOREMAN_ENCRYPTIONKEY;

    $output = false;

    $encrypt_method = "AES-256-CBC";

    $key = hash( 'sha256', $secret_key );

    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );



    if( $action == 'encrypt' ) {

        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );

    } else if( $action == 'decrypt' ) {

        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );

    }



    return $output;

}