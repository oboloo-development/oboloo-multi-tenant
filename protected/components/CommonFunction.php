<?php
class CommonFunction{

    function getAllUser(){
        $sql = " SELECT user_id,full_name FROM `users` ";
        return Yii::app()->db->createCommand($sql)->queryAll(); 
    }

   
   // Check archive list of contracts or vendors
    function disabledArchive($tbl='', $id=''){
      $ID = $column = '';
      if($tbl == 'vendors'){
        $ID = 'vendor_id';
        $column = 'vendor_archive';
      }else{
       $ID = 'contract_id';
       $column = 'contract_archive';
      }

      $sql = " SELECT ".$column." FROM ".$tbl." where ".$ID." =".$id;
      $result =  Yii::app()->db->createCommand($sql)->queryRow();
      if(!empty($result['contract_archive']) && $result['contract_archive'] == 'yes'){
         $result = 'yes';
      }else if(!empty($result['vendor_archive']) && $result['vendor_archive'] == 'yes'){
         $result = 'yes';
      }else{
         $result = '';
      }
      
      return $result;
    }


  public static function getCustomQuestion($vendorScoringId,$quoteID){
    $sql= "SELECT * FROM quote_questions q where q.quote_id=".$quoteID." and q.vendor_scoring_id=".$vendorScoringId;
    return Yii::app()->db->createCommand($sql)->queryAll();
  }

  public static function getCustomAnswer($questionId,$quoteID){
    $sql= "SELECT * FROM quote_answers qa where qa.quote_id=".$quoteID." and qa.question_id=".$questionId;
    return Yii::app()->db->createCommand($sql)->queryAll();
  }

  public static function getQuoteVendor($quote_id)
  {
    $sql="SELECT quote_id, vendor_id, vendor_name, emails, contact_name FROM quote_vendors q
          WHERE q.quote_id =".$quote_id;
    return Yii::app()->db->createCommand($sql)->queryAll();
  }

  public static function getDefaultQuestion($vendorScoringId, $quoteID){ 
    $sql= "SELECT * FROM quote_questions q where q.quote_id=".$quoteID." and q.vendor_scoring_id=".$vendorScoringId;
    return Yii::app()->db->createCommand($sql)->queryAll();
  }

  public static function getDefaultAnswer($questionId, $quoteID){
    $sql= "SELECT * FROM quote_answers qa where answer is not null and qa.quote_id=".$quoteID." and qa.question_id=".$questionId;
    return Yii::app()->db->createCommand($sql)->queryAll();
  }


  function deleteRecordTable($ID='', $tblName=''){
      $sql='';
      if(!empty($ID))
       $sql = "delete from ".$tblName." where id=".$ID;
      else
        $sql = "delete from ".$tblName;
      if(!empty($sql))
        Yii::app()->db->createCommand($sql)->execute();
      return true;
   }

   function GetGoals($deptId,$filter_by_year){
      if(!empty($deptId) && empty($filter_by_year)){
         $sql = "select * from goal where dept_id=".$deptId;
         return Yii::app()->db->createCommand($sql)->queryAll(); 
      }else{
         $sql = "select * from goal where dept_id=".$deptId." and year =".$filter_by_year." group by dept_id ";
         return Yii::app()->db->createCommand($sql)->queryAll(); 
      }
   }

   // function insertGoalToGoalHistory($currentDate){
   //     $sql="insert into goal_history (user_id,dept_id,username,cost_avoidance,cost_reduction,cost_containment,updated_date) 
   //        select user_id,dept_id,username,cost_avoidance,cost_reduction,cost_containment, updated_date from goal ";
   // }

   public static function getQuoteParticipateUser($key=''){
    $quoteParticipateUser = 
    [
     1 => "Contributor",
     2 =>"Scorer",
     3 =>"Contributor & Scorer"
    ];  
        
    if(!empty($key)){
     return $quoteParticipateUser[$key];
    }
    return $quoteParticipateUser;   
    } 

   function tableByDateTable($date, $tbl){
    $result = '';
    if(!empty($date)){
      $sql = "delete from ".$tbl." where year ='".$date."' ";
      $result = Yii::app()->db->createCommand($sql)->execute(); 
    }
    return $result;
   }

   public static function userTextAvatar($username){
      return "https://ui-avatars.com/api/?name=" . urlencode($username)."&background=random";
   }

  public static function getBaseLineSpend($savingID){
    $sql = "SELECT sum(base_line_spend) as total_base_line_spend FROM saving_milestone m where m.saving_id=".$savingID;
    return  Yii::app()->db->createCommand($sql)->queryRow(); 
  }
  
  public static function totalBaselineSpendRealizedSaving($savingID){
    $sql = "SELECT sum(base_line_spend) as total_base_line_spend FROM saving_milestone m where m.status='Completed' and  m.saving_id=".$savingID;
    return  Yii::app()->db->createCommand($sql)->queryRow(); 
  }

  public static function quoteVendorNameShow($quiteID=0){
    if(!empty($quiteID)){
        $sql="select id,vendor_name from quote_vendors where quote_id=".$quiteID;
        return Yii::app()->db->createCommand($sql)->queryRow();
    }
  }


  public static function quoteVendQuesAndAnswer($quote_id,$quoteQuestionVendID, $questionFormId){
    if(!empty($quote_id) && !empty($quoteQuestionVendID)){
		  $sql= "select fvq.*, fa.answer,fa.id answer_id from quote_vendor_questionnaire as fvq
		  left join form_answers as fa on fa.user_id = fvq.vendor_id where fvq.quote_id=".$quote_id." and fa.quote_id=".$quote_id."
      and fa.form_id=".$questionFormId." and fvq.vendor_id=".$quoteQuestionVendID." group by fa.id, fa.user_id "; 
		  $resQuestAndAnswer = Yii::app()->db->createCommand($sql)->queryAll(); 
		  return $resQuestAndAnswer;
		} 
  }

  public static function quoteVendQuestions($quote_id,$quoteQuestionVendID, $questionFormId){
    if(!empty($quote_id) && !empty($quoteQuestionVendID)){
		 $sql= "select fvq.*,fq.question  from form_questions as fq
		  inner join quote_vendor_questionnaire as fvq on fq.form_id = fvq.question_form_id where fvq.quote_id=".$quote_id."
      and fq.form_id=".$questionFormId." and fvq.vendor_id=".$quoteQuestionVendID." "; 
		  $resQuestAndAnswer = Yii::app()->db->createCommand($sql)->queryAll(); 
		  return $resQuestAndAnswer;
		} 
  }

  public static function ScorerStatus($quoteID = ''){
    $sql = " select status from quote_scorer_questions_form where user_id=".Yii::app()->session['user_id']." and quote_id=".$quoteID." group by scoring_row_id ";
    return Yii::app()->db->createCommand($sql)->queryRow();
  }

  public static function getVendorQuestion($quoteID = '', $questionsFormID=''){
      $sql = " SELECT fa.*, group_concat(fa.vendor_id) as vend_id
          FROM vendor_question_answer fa
          INNER JOIN quote_vendors qv ON qv.vendor_id = fa.vendor_id
          WHERE fa.quote_id = " . $quoteID . " AND fa.form_id = " . $questionsFormID . " AND qv.submit_status = 1
          GROUP BY fa.questions order by fa.id ";
      return Yii::app()->db->createCommand($sql)->queryAll();
  }

  public static function getVendorNameAndAnswers($data){
    $question = self::limitStringQuestionFilter($data['questions']);
    $sql = ' SELECT sum(uqas.calculated_score) as total_score_answer, count(uqas.user_id) as all_scorers, v.vendor_id, v.vendor_name as vendor_name,vqa.answer as vend_answer, vqa.type as que_ans_type, vqa.id as vqa_id FROM vendors v
      inner join vendor_question_answer vqa on vqa.vendor_id = v.vendor_id
      left join user_quote_ans_score as uqas on uqas.vendor_question_answer_id = vqa.id
      WHERE  vqa.questions LIKE "%' . $question  . '%" and vqa.quote_id = ' . $data['quoteID'] . ' AND vqa.form_id = ' . $data['questionFormId'] . ' and v.vendor_id in(' . $data['vendorID'] . ') group by v.vendor_id ';
    
    return  Yii::app()->db->createCommand($sql)->queryAll();
  } 

  public static function getAnswerScores($data){
    $sql = ' SELECT sum(uqas.calculated_score) as total_score_answer, count(uqas.user_id) as all_scorers, v.vendor_id,
     v.vendor_name as vendor_name,vqa.answer as vend_answer, vqa.id as vqa_id 
     FROM vendors v
      left join vendor_question_answer vqa on vqa.vendor_id = v.vendor_id
      left join user_quote_ans_score as uqas on uqas.vendor_question_answer_id = vqa.id
      left join quote_questionnaire  qq on qq.quote_id = uqas.quote_id
      WHERE vqa.quote_id = ' . $data['quoteID'];
    return  Yii::app()->db->createCommand($sql)->queryAll();
  } 

  public static function  supplierScore($vendorID, $quoteID, $questionFormId){
      $sql ='SELECT sum(uqas.calculated_score) AS total_score_answer, count(vqa.questions) AS all_question
      FROM vendor_question_answer vqa
      LEFT JOIN user_quote_ans_score uqas ON uqas.vendor_question_answer_id = vqa.id 
      where vqa.quote_id='.$quoteID.' AND vqa.form_id ='.$questionFormId.' AND vqa.vendor_id='.$vendorID.' AND uqas.status IN(2)  ';
      $vendorRender = Yii::app()->db->createCommand($sql)->queryRow(); 
      $totalVendorScore = !empty($vendorRender['total_score_answer']) ? $vendorRender['total_score_answer']: 0;
      $allQuestion = !empty($vendorRender['all_question']) ? $vendorRender['all_question']: 0;
      if(!empty($totalVendorScore)){
        return $totalVendorScore / $allQuestion;
      }else{
        return 0;
      }
  }

  public static function supplierTotalScore($vendorID, $quoteID){
    $sql =' SELECT vqa.*, sum(uqas.calculated_score) as total_supplier_score, count(vqa.questions) AS all_question from vendor_question_answer vqa
      LEFT JOIN user_quote_ans_score uqas ON vqa.id = uqas.vendor_question_answer_id 
      WHERE vqa.quote_id='.$quoteID.' AND vqa.vendor_id='.$vendorID.' AND uqas.status IN(2) ';
      $vendorRender = Yii::app()->db->createCommand($sql)->queryRow(); 
      $totalScoreAnswer = !empty($vendorRender['total_supplier_score']) ? $vendorRender['total_supplier_score']: 0;
      $allQuestion = !empty($vendorRender['all_question']) ? $vendorRender['all_question']: 0;
      if(!empty($totalScoreAnswer)){
        $totalVendorScore = $totalScoreAnswer / $allQuestion;
        return $totalVendorScore;
      }else{
        return 0;
      }
  }

  public static function scorerAllQustionnaires($userID, $quoteID, $status=""){
    if(!empty($status)){
      $questionnoareCount = Yii::app()->db->createCommand("SELECT count(distinct question_form_id) as scorer_questionnaire FROM user_quote_ans_score uqas
	      where user_id=".$userID." AND quote_id = " .$quoteID." and status=2 ")->queryScalar();
    }else{
      $questionnoareCount = Yii::app()->db->createCommand("SELECT count(qsqf.status) as scorer_questionnaire FROM quote_scorer_questions_form qsqf
	      where user_id=".$userID." AND quote_id = " .$quoteID)->queryScalar();
    }
    return $questionnoareCount;
  }

 public static function supplierTotalQuestionnaireByStatus($quoteID, $vendID, $status=""){
    if(!empty($status) && $status=='all'){
      $questionnoareCount = Yii::app()->db->createCommand("SELECT count(*) FROM quote_vendor_questionnaire WHERE vendor_id=".$vendID." and quote_id =".$quoteID)->queryScalar();
    }else{
      $questionnoareCount = Yii::app()->db->createCommand("SELECT count(*) FROM quote_vendor_questionnaire WHERE vendor_id=".$vendID." and status='".$status."' and quote_id =".$quoteID)->queryScalar();
    }
    return $questionnoareCount;
   }
   
  public static function getScorerQuestionnaire($userID,$quote_id){
   $sql =" select qsq.id, qsq.quote_id,qsq.user_id,qsq.question_id as question_form_id, qsq.creator_id,qq.question_form_name, qq.question_scorer from 
		quote_scorer_questions_form qsq
		inner join quote_questionnaire qq on qq.question_form_id = qsq.question_id and qsq.quote_id = qq.quote_id
		where qsq.quote_id=".$quote_id." and qsq.user_id=".$userID." group by question_form_name "; 
		return Yii::app()->db->createCommand($sql)->queryAll();
  }

  private function limitStringQuestionFilter($str, $length = 20) {
    if (strlen($str) > $length) {
        return substr($str, 0, $length);
    }
    return $str;
  }

  public static function getContractDocumentTable($fliter, $fliterSupplier){
    $query = " 
      select * from (
      SELECT 
      'main_document' as type,
      cd.document_type, cd.contract_id, cd.document_title, cd.id, cd.approved_by_name, cd.approved_datetime, cd.expiry_date, cd.date_created,
      cd.uploaded_by_type,cd.archive,
      CASE WHEN cd.status='Pending' THEN 1 ELSE 0 END AS pendiing_documents,
      CASE WHEN cd.status='Approved' THEN 1 ELSE 0 END AS approved_documents
      from contract_documents as cd
        where ".$fliter.") as cd
        Union (
          SELECT 
          'requested_document' as type,
          crd.contract_type as document_type,
          crd.contract_id,
          crd.note as document_title,
          crd.id,
          '' as approved_by_name,
          '' as approved_datetime,
          '' expiry_date, 
          crd.created_at as date_created, 
          'Upload By Supplier' as uploaded_by_type,
          '' as archive,
  
          CASE WHEN crd.status='Pending' THEN 1 ELSE 0 END as pending_documents,
          CASE WHEN crd.status='Uploaded' THEN 1 ELSE 0 END as Uploaded_documents

        FROM 
        contract_request_documents AS crd
        LEFT JOIN 
          contract_document_type AS cdt 
        ON
          crd.contract_type = cdt.id
        WHERE crd.status='Pending' and ".$fliterSupplier." ) order by document_type ";
        return $query;
  }

  public static function getVendorDocumentTable($fliter, $fliterSupplier){
    $query = " 
      select * from (
      SELECT 
      'main_document' as type,
      vd.document_type, vd.vendor_id, vd.document_title, vd.id, vd.approved_by_name, vd.approved_datetime, vd.expiry_date, vd.date_created,
      vd.uploaded_by_type,vd.archive,
      CASE WHEN vd.status='Pending' THEN 1 ELSE 0 END AS pendiing_documents,
      CASE WHEN vd.status='Approved' THEN 1 ELSE 0 END AS approved_documents
      from vendor_documents as vd
      where ".$fliter.") as vd
      
      Union (
        SELECT 
        'requested_document' as type,
        vrd.vendor_document_type as document_type,
        vrd.vendor_id,
        vrd.note as document_title,
        vrd.id,
        '' as approved_by_name,
        '' as approved_datetime,
        '' expiry_date, 
        vrd.created_at as date_created, 
        'Upload By Supplier' as uploaded_by_type,
        '' as archive,
        CASE WHEN vrd.status='Pending' THEN 1 ELSE 0 END as pending_documents,
        CASE WHEN vrd.status='Uploaded' THEN 1 ELSE 0 END as Uploaded_documents
       FROM 
        vendor_request_documents AS vrd
        LEFT JOIN 
          vendor_document_type AS vdt 
        ON
          vrd.vendor_document_type = vdt.id
        WHERE vrd.status='Pending' ".$fliterSupplier." ) order by document_type ";
        return Yii::app()->db->createCommand($query)->queryAll();
  }

}