<?php
class EmailManager
{

  public static function vendorQuoteInvitation($quote, $vendorName, $vendoremail, $reference)
  {
    $user_ids  = array();
    $quote_id  = $quote['quote_id'];
    if (!empty($vendoremail)) {
      $company_data = new Company();
      $company_details = $company_data->getOne();

      //$clientName = $quote['created_by_name'];
      $clientName = !empty($company_details['company_name']) ? $company_details['company_name'] : $quote['created_by_name'];
      $email_body = '';
      $subjectText =
        $subject = $clientName . ' has invited you to participate in a sourcing activity';
      $header1 = '<font  style="color:#54595F; font-size:24px;"><span style="color:#54595F">You’ve been invited by </span><span style="color:#2d9ca2;">' . $clientName . ' </span><span style="color:#54595F;">to submit a quote for the following sourcing activity</font>&nbsp;<strong><span style="color:#2d9ca2;"></span></strong>';
      $header2 = '<span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">This is a notification from oboloo on behalf of </span><span style="color:#e74c3c;">' . $clientName . '</span><span style="color:black">. You have been invited to submit&nbsp;a quote for the following:</span></span></span></span>';

      $email_body  .= self::emailHeader($header1, $header2 = '');
      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<strong><span style="color:#54595F;">Sourcing Activity Details:</span></strong><br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2;">Quote Name: </span></strong><span style="color:#54595F;">' . $quote['quote_name'] . '</span><br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2;">Reference: </span></strong><span style="color:#54595F;" >' . $reference . '</span><br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2;">Starts at (UTC): </span></strong><span style="color:#54595F;">' . date("F j, Y H:iA", strtotime($quote['opening_date'])) . '</span><br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2;">Deadline (UTC): </span></strong><span style="color:#54595F;">' . date("F j, Y H:iA", strtotime($quote['closing_date'])) . '</span></br></br>
            </td>
         </tr>';
      $footerText = '<tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"><div style="font-family:Lato, Tahoma, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#2d9ca2;"><p style="color:#54595F;"><strong><span style="color:#2d9ca2;">&nbsp;</span></strong>You can submit a quote for the above sourcing activity via oboloo’s secure network, click on the <strong style="color:#2d9ca2;">Submit Quote</strong> button below</p>      </div></td></tr>
              <tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;"><tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="border: 0px solid #2D9CA2;border-radius: 30px;cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2;" valign="middle"><a href="' . Yii::app()->createAbsoluteUrl('supplier/default/login') . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px;mso-padding-alt:0px;border-radius:30px; " target="_blank">Respond</a></td></tr></tbody></table></td></tr>             
              <tr>
              <td align="center" valign="middle">
              <br />
              <span style="display:inline-block;margin-bottom:10px;font-family:Lato, Tahoma, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#54595F; margin-top:20px;">Learn how to respond to a quote within our  <a href="https://oboloo.com/supplier-support-hub/" target="_blank" style="color: #0000ff; text-decoration: underline;">training and support hub  </a></span> </td></tr>
              </tbody></table></td></tr>';

      $email_body .= self::emailFooter($footerText);
    
      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($vendoremail, $vendorName);
      $mail->Subject = $subject;
      $mail->isHTML(true);
      $mail->Body = $email_body;
      //$mail->SMTPDebug = 2;
      if ($mail->send()) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  public static function vendorRequestContractDocument($vendorContactName, $vendorContactEmail, $reference, $contractType, $contractTitle, $note)
  {
    $user_ids  = array();
    if (!empty($vendorContactEmail)) {
      $company_data = new Company();
      $company_details = $company_data->getOne();

      //$clientName = $quote['created_by_name'];
      $clientName = !empty($company_details['company_name']) ? $company_details['company_name'] : $quote['created_by_name'];
      $email_body = '';
      //$subject =  $clientName.'  has asked you to upload a '.$vendorContactName.' document';
      $subject =  $clientName . '  has asked you to upload a Contract document';
      $header1 = '<font color="#54595F"><span style="color:54595F">You’ve been asked by </span><span style="color:#2d9ca2;">' . $clientName . ' </span><span style="color:54595F">to upload a new contract document into their contract management platform</font>&nbsp;<strong><span style="color:#2d9ca2;"></span></strong>';
      $header2 = '<span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:54595F">This is a notification from oboloo on behalf of </span><span style="color:#e74c3c;">' . $clientName . '</span><span style="color:54595F">. You have been requested to upload&nbsp;a document for the following:</span></span></span></span>';

      $email_body  .= self::emailHeader($header1, $header2 = '');
      $email_body  .= '<tr>
                     <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                     <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<strong><span style="color:#677294; font-weight: 400;">Requested Document Details:</span></strong><br/><br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2; font-weight: 400;">Document Name: </span></strong><span style="color:#677294; font-weight: 400;">' . $contractType . '</span><br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2; font-weight: 400;">Contract Title: </span></strong><span style="color:#677294; font-weight: 400;">' . $contractTitle . '</span><br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2; font-weight: 400;">Reference: </span></strong><span style="color:#677294; font-weight: 400;">' . $reference . '</span><br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2; font-weight: 400;">Description/Notes: </span></strong><span style="color:#677294; font-weight: 400;">' . $note . '</span><br/><br/>
            </td>
         </tr>';
      $footerText = '<tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"><div style="font-family:Lato, Tahoma, sans-serif;font-size:11px;line-height:1.5;text-align:left;font-weight: 400;color:#677294;"><p style="text-align: center;">You can upload the requested document to oboloo’s secure network by clicking on the&nbsp;<strong><span style="color:#2d9ca2;">Upload Contract Document </span></strong> button below</p>      </div></td></tr>
                     <tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellpadding="0" cellspacing="10" role="presentation" style="border-collapse:separate;line-height:100%;">
                     <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle"><a href="' . Yii::app()->createAbsoluteUrl('supplier/default/contractDocumentLogin') . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Upload Contract Document</a></td>
                     </tr></tbody></table></td></tr>';


      $email_body .= self::emailFooter($footerText);
      // echo "<pre>"; print_r($email_body); exit;
      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($vendorContactEmail, $vendorContactName);
      $mail->Subject =  $subject;
      $mail->isHTML(true);
      $mail->Body = $email_body;
      //$mail->SMTPDebug = 2;
      if ($mail->send()) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  public static function vendorRequestDocument($vendorContactName, $vendorContactEmail, $reference, $contractType, $note)
  {
    $user_ids  = array();


    if (!empty($vendorContactEmail)) {


      $company_data = new Company();
      $company_details = $company_data->getOne();

      //$clientName = $quote['created_by_name'];
      $clientName = !empty($company_details['company_name']) ? $company_details['company_name'] : $quote['created_by_name'];
      $email_body = '';
      //$subject =  $clientName.' has uploaded a new document to Supplier';
      $subject =  $clientName . '  has asked you to upload a Supplier document';
      $header1 = '<font color="#677294"><span style="color:#677294">You\'ve been asked by </span><span style="color:#2d9ca2;">' . $clientName . ' </span><span style="color:#677294">to upload a new supplier document into their supplier management platform</font>&nbsp;<strong><span style="color:#2d9ca2;"></span></strong>';
      $header2 = '<span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">This is a notification from oboloo on behalf of </span><span style="color:#e74c3c;">' . $clientName . '</span><span style="color:black">. You have been requested to upload&nbsp;a document for the following:</span></span></span></span>';

      $email_body  .= self::emailHeader($header1, $header2 = "");

      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<strong><span style="color:#677294;">Requested Document Details:</span></strong><br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Document Name: </span></strong>' . $contractType . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Reference: </span></strong>' . $reference . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Description/Notes: </span></strong>' . $note . '<br/><br/>
            
            
            </td>
         </tr>';
      $footerText = '<tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"><div style="font-family:Lato, Tahoma, sans-serif;font-size:11px;line-height:1.5;text-align:center;font-weight: 400;color:#677294;"><p>You can upload the requested document to oboloo’s secure network by clicking on the&nbsp;<strong><span style="color:#16a085;">Upload Supplier Document </span></strong> button below</p>      </div></td></tr>

              
              <tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellpadding="0" cellspacing="10" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle"><a href="' . Yii::app()->createAbsoluteUrl('supplier/default/vendorDocumentLogin') . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Upload Supplier Document</a></td>
              </tr></tbody></table></td></tr>
              ';

      $email_body .= self::emailFooter($footerText);

      //echo "<pre>";print_r($email_body);exit;

      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($vendorContactEmail, $vendorContactName);
      $mail->Subject = $subject;
      $mail->isHTML(true);
      $mail->Body = $email_body;
      //$mail->SMTPDebug = 2;
      if ($mail->send()) {
        return 1;
      } else {
        return 0;
      }
    }
  }


  public static function vendorRequestDocumentOnboard($vendorContactName, $vendorContactEmail, $reference, $contractType, $note)
  {
    $user_ids  = array();


    if (!empty($vendorContactEmail)) {


      $company_data = new Company();
      $company_details = $company_data->getOne();

      //$clientName = $quote['created_by_name'];
      $clientName = !empty($company_details['company_name']) ? $company_details['company_name'] : $quote['created_by_name'];
      $email_body = '';
      $subject =  $clientName . ' has asked you to upload a supplier document';
      $header1 = '<font color="#677294"><span style="color:#677294">You\'ve been asked by </span><span style="color:#2d9ca2;">' . $clientName . ' </span><span style="color:#677294">to upload a new supplier document into their supplier management platform</font>&nbsp;<strong><span style="color:#2d9ca2;"></span></strong>';
      $header2 = '<span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">This is a notification from oboloo on behalf of </span><span style="color:#e74c3c;">' . $clientName . '</span><span style="color:black">. You have been requested to upload&nbsp;a document for the following:</span></span></span></span>';

      $email_body  .= self::emailHeader($header1, $header2 = "");

      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<strong><span style="color:#677294;">Requested Document Details:</span></strong><br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Document Name: </span></strong>' . $contractType . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Reference: </span></strong>' . $reference . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Description/Notes: </span></strong>' . $note . '<br/><br/>
            
            
            </td>
         </tr>';
      $footerText = '<tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"><div style="font-family:Lato, Tahoma, sans-serif;font-size:11px;line-height:1.5;text-align:center;font-weight: 400;color:#677294;"><p>You can upload the requested document to oboloo’s secure network by clicking on the&nbsp;<strong><span style="color:#16a085;">Upload Supplier Document </span></strong> button below</p>      </div></td></tr>

              
              <tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellpadding="0" cellspacing="10" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle"><a href="' . Yii::app()->createAbsoluteUrl('supplier/default/documentOnboardLogin') . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Upload Onboard Supplier Document</a></td>
              </tr></tbody></table></td></tr>
              ';

      $email_body .= self::emailFooter($footerText);

      //echo "<pre>";print_r($email_body);exit;

      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($vendorContactEmail, $vendorContactName);
      $mail->Subject = $subject;
      $mail->isHTML(true);
      $mail->Body = $email_body;
      //$mail->SMTPDebug = 2;
      if ($mail->send()) {
        return 1;
      } else {
        return 0;
      }
    }
  }


  public static function vendorQuoteReopen($quote, $vendorName, $vendorEmail, $reference)
  {
    //$clientName = $quote['created_by_name'];

    $company_data = new Company();
    $company_details = $company_data->getOne();
    $companyName = $company_details['company_name'];
    $clientName = !empty($companyName) ? $companyName : $quote['user_name'];

    $emailBody = '';
    $subject = $clientName . ' has reopened the sourcing activity ' . $quote['quote_name'];
    $header1 = '<font color="#54595F;"><span style="color:#2d9ca2;">' . $clientName . '</span><span style="color:#54595F;"> has reopened the sourcing activity <span style="color:#2d9ca2;">' . $quote['quote_name'] . '</span>. You can now edit and resubmit your quote.</span></font>&nbsp;<strong><span style="color:#2d9ca2;"></span></strong>';
    $header2 = '<span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:2d9ca2">Why has this been reopened? Please see the comments below given by </span><span style="color:#2d9ca2;">' . $clientName . '</span><span style="color:black"></span></span></span></span>';

    $emailBody  .= self::emailHeader($header1, $header2);

    $emailBody  .= '<tr>
       <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
       <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
    $emailBody .= '<tr>
                 <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                 <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br /><br />';
    $emailBody .= '';
    $emailBody .= '<strong><span style="color:#54595F;">Sourcing Activity Details:</span></strong><br /><br />';
    $emailBody .= '<strong><span style="color:#2d9ca2;">Quote Name: </span></strong><span style="color:#54595F;">' . $quote['quote_name'] . '</span><br/><br/>';
    $emailBody .= '<strong><span style="color:#2d9ca2;">Reference: </span></strong><span style="color:#54595F;">' . $reference . '</span><br/><br/>';
    $emailBody .= '<strong><span style="color:#2d9ca2;">Starts at: </span></strong><span style="color:#54595F;">' . date("F j, Y H:iA", strtotime($quote['opening_date'])) . '</span><br/><br/>';
    $emailBody .= '<strong><span style="color:#2d9ca2;">Deadline: </span></strong><span style="color:#54595F;">' . date("F j, Y H:iA", strtotime($quote['closing_date'])) . '</span><br/><br/>';
    $emailBody .= '<strong><span style="color:#2d9ca2;">Comments: </span></strong><span style="color:#54595F;">' . $quote['notes'] . '</span></td>
       </tr>
      </td>
      </tr>';
    $footerText = '<tr><td align="center" style="font-size:0px;padding:15px 0px;word-break:break-word;"><div style="font-family:Lato, Tahoma, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#54595F;"><p><strong><span style="color:#16a085;">&nbsp;</span></strong>You can submit a quote for the above sourcing activity via oboloo’s secure network, click on the <strong style="color:#2d9ca2;">Respond</strong> button below</p>      </div></td></tr>

        <tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;"><tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="border: 1px solid #2D9CA2;border-radius: 30px;cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2;" valign="middle"><a href="' . Yii::app()->createAbsoluteUrl('supplier/default/login') . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px;mso-padding-alt:0px;border-radius:30px; border:1px;" target="_blank">Respond</a></td></tr></tbody></table></td></tr>
         <tr>
        <td align="center" valign="middle">
        <br />
        <span style="display:inline-block;margin-bottom:10px;font-family:Lato, Tahoma, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#54595F; margin-top:20px;">Learn how to respond to a quote within our  <a href="https://oboloo.com/supplier-support-hub/" target="_blank" style="color: #0000ff; text-decoration: underline;">training and support hub  </a></span> </td></tr>
        </tbody></table></td></tr>
        ';

    $emailBody .= self::emailFooter($footerText);
    // echo "<pre>"; print_r($emailBody); exit;
    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth = true;
    $mail->Username = Yii::app()->params['username'];
    $mail->Password = Yii::app()->params['password'];
    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
    $mail->addAddress($vendorEmail, $vendorName);
    $mail->Subject = 'oboloo: Re-Open Quote Submission Invite';
    $mail->isHTML(true);
    $mail->Body = $emailBody;
    //$mail->SMTPDebug = 2;

    if ($mail->send()) {
      return 1;
    }
    return 0;
  }
  public static function vendorAcivation($vendor_id, $setPassword = 1, $userName = '')
  {
    $passwordExp = '';
    $sql       = "select * from vendors where vendor_id=" . $vendor_id;
    $vendorData = Yii::app()->db->createCommand($sql)->queryRow($sql);
    $vendor_id = $vendorData['vendor_id'];
    $userName  = !empty(Yii::app()->session['company_name']) ? Yii::app()->session['company_name'] : $userName;

    if (true) {
      if (!empty($setPassword)) {
        $passwordNew = self::generateRandomString(10);
        $password = md5($passwordNew);
        $passwordExp .= '<strong><span style="color:#16a085;">Password: </span></strong>' . $passwordNew . '<br/><br/>';
      } else {
        $password = $vendorData['password'];
      }
      Yii::app()->db->createCommand("UPDATE vendors SET password = '" . $password . "' WHERE vendor_id = " . $vendorData['vendor_id'])->execute();

      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";

      $email_body = '';
      $header1 = '<span style="font-size:22px;"><font color="#7f8c8d">Welcome! Supplier Activation on</font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
      $header2 = '<div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;">        <p>This is a notification from oboloo on behalf of <span style="color:#16a085;"></span><strong><span style="color:#16a085;">' . $userName . '</span></strong>&nbsp;who have&nbsp;requested access for your</p><p></p><p>organisation on&nbsp;<span style="color:#16a085;"><strong>oboloo\'s&nbsp;Supplier Portal</strong></span>. This is a free service that&nbsp;allows you to respond to requests from</p><p></p><p><strong><span style="color:#16a085;">' . $userName . '</span></strong> such as tender activities and contract updates. Please find your log in details below:</p></div>';
      $solutation  = $vendorData['vendor_name'];
      $email_body .= self::emailHeader($header1, $header2, $solutation);
      $email_body .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<strong><span style="color:#16a085;">Username: </span></strong>' . $vendorData['emails'] . '<br/><br/>';
      $email_body .= $passwordExp;
      $email_body .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $vendorData['emails'] . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">URL: </span></strong><a target="_blank" href="' . Yii::app()->createAbsoluteUrl('supplier/default/login') . '">' . Yii::app()->createAbsoluteUrl('supplier/default/login') . '</a>
            <br/><br/>';
      $email_body .= '<strong><span style="color:#2d9ca2;">Training and Support URL: </span></strong><a target="_blank" href="https://oboloo.com/supplier-support-hub">https://oboloo.com/supplier-support-hub</a></td>
         </tr>';
      $footerText  = '<tr><td align="center" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"><div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><p><strong><span style="color:#16a085;">NOTE:&nbsp;</span></strong>Once logged in you can also invite other team members to gain access to the portal</p></div></td></tr>';

      $email_body .= self::emailFooter($footerText);
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($vendorData['emails'], $vendorData['vendor_name']);
      $mail->Subject = 'oboloo: Supplier Activation';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      $mail->send();
      $email_body;
    }
  }
  public static function userNotification($email, $name, $subject, $notificationText, $url)
  {

    $emailBody = '';
    $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $emailBody  .= self::emailHeader($header1 = '', $header2);

    //if(!empty($url)){
    /* $loginUrl = '<a style="display:block;background:#2d9ca2;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:12px 36px 12px 36px;mso-padding-alt:0px;border-radius:14px;" href="' .$url. '"> Login here </a>';*/
    $loginUrl = '<tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellspacing="10" cellpadding="0" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle">

              <a href="' . $url . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Login here</a></td>
              </tr></tbody></table></td></tr>';
    //}
    $emailBody  .= '<tr>
       <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
       <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
    $emailBody .= '';
    $emailBody .= '<span style="font-size:18px;color:#54595F;">' . $notificationText . '</span><br/><br/></td></tr>';
    $emailBody .= $loginUrl;
    $emailBody .= '';
    $emailBody .= self::emailFooter('No Note');
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $mail = new \PHPMailer(true);
      try {
        $mail->IsSMTP();
        $mail->Host = Yii::app()->params['host'];
        $mail->Port = Yii::app()->params['port'];
        $mail->SMTPAuth = true;
        $mail->Username = Yii::app()->params['username'];
        $mail->Password = Yii::app()->params['password'];
        $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
        $mail->CharSet = "UTF-8";
        $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
        $mail->addAddress($email, $name);
        $mail->Subject = 'oboloo: ' . $subject;
        $mail->isHTML(true);

        $mail->Body = $emailBody;
        $mail->send();
        return 1;
      } catch (phpmailerException $e) {
        return 0;
      } catch (Exception $e) {
        return 0;
      }
    }
  }
  public static function userQuote($userID, $quote)
  {
    $user_ids  = array();
    $quote_id  = $quote['quote_id'];

    $sql = "select * from users where user_id=" . $userID;
    $users = Yii::app()->db->createCommand($sql)->query()->readAll();
    foreach ($users as $value) {
      $user_name = $value['full_name'];
      $user_email = $value['email'];
      $email_body = '';
      $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
      $header2 = '';

      $email_body  .= self::emailHeader($header1 = '', $header2);

      $notificationComments  = "A new quote <b>" . $quote_id . "</b> - <b>" . $quote['quote_name'] . "</b> has been created";
      /* $loginUrl = '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_id). '" style="display:inline-block;background:#2d9ca2;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:12px 36px 12px 36px;mso-padding-alt:0px;border-radius: 30px;"> Login here </a>';*/


      $loginUrl = '<tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellspacing="10" cellpadding="10" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle">

              <a href="' .  AppUrl::bicesUrl('quotes/edit/' . $quote_id) . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Login here</a></td>
              </tr></tbody></table></td></tr>';


      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<span style="font-size:18px;color:#54595F;">' . $notificationComments . '</span><br/><br/></td></tr>';
      $email_body .= $loginUrl;
      $email_body .= '';
      $email_body .= self::emailFooter('No Note');
      // echo "<pre>"; print_r($email_body); exit;
      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($user_email, $user_name);
      $mail->Subject = 'oboloo: eSourcing Publication';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      //$mail->SMTPDebug = 2;
      if ($mail->send()) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  public static function independentUserQuote($quote, $userArr)
  {
    $user_ids  = array();
    $quote_id  = $quote['quote_id'];
    if (!empty($quote['created_by_user_id'])) $user_ids[$quote['created_by_user_id']] = $quote['created_by_user_id'];
    if (!empty($quote['commercial_lead_user_id'])) $user_ids[$quote['commercial_lead_user_id']] = $quote['commercial_lead_user_id'];
    $sql = "SELECT * FROM quote_vendors q, vendors v WHERE q.quote_id = $quote_id AND q.vendor_id = v.vendor_id";
    $vendors = Yii::app()->db->createCommand($sql)->query()->readAll();
    $vendors = array_column($vendors, 'vendor_name');
    $vendors = implode(", ", $vendors);

    foreach ($userArr as $key => $value) {
      $user_name = $value;
      $user_email = $value;
      $email_body = '';
      $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
      $header2 = '';

      $email_body  .= self::emailHeader($header1 = '', $header2);

      $notificationComments  = "A new quote <b>" . $quote_id . "</b> - <b>" . $quote['quote_name'] . "</b> has been created";
      /* $loginUrl = '<a  href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_id). '" style="display:inline-block;background:#2d9ca2;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:12px 36px 12px 36px;mso-padding-alt:0px;border-radius: 30px;"> Login here </a>';*/


      $loginUrl = '<tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellspacing="0" cellpadding="0" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle">

              <a href="' .  AppUrl::bicesUrl('quotes/edit/' . $quote_id) . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Login here</a></td>
              </tr></tbody></table></td></tr>';

      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<span style="font-size:18px;color:#54595F;">' . $notificationComments . '</span><br/><br/>';
      $email_body .= '</td></tr>';
      $email_body .= $loginUrl;

      $email_body .= self::emailFooter('No Note');

      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($user_email, $user_name);
      $mail->Subject = 'oboloo: Quote Publication';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      //$mail->SMTPDebug = 2;
      $mail->send();
    }
  }

  public static function independentConsultantQuote($userArr, $userLoggedinID)
  {
    $user_ids  = $userLoggedinID;
    $sql = "SELECT * FROM users  WHERE user_id = $user_ids";
    $userReader = Yii::app()->db->createCommand($sql)->queryRow();

    foreach ($userArr as $key => $value) {
      $user_name = $value;
      $user_email = $value;
      $email_body = '';
      $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">' . $userReader['full_name'] . ' has requested for help an</span> <span style="color:#2d9ca2;">independent procurement consultant </span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
      $header2 = '';

      $email_body  .= self::emailHeader($header1, $header2);

      $notificationComments  = "<b>" . $userReader['full_name'] . "</b>  request for help an <b>independent procurement consultant</b>";
      $loginUrl = '<a href="' . AppUrl::bicesUrl('app/login') . '" style="display:inline-block;background:#2d9ca2;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:12px 36px 12px 36px;mso-padding-alt:0px;border-radius: 30px;"> Login here </a>';

      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<span style="font-size:18px;color:#2d9ca2;">' . $notificationComments . '</span><br/><br/>';

      $email_body .= '<strong><span style="color:#16a085;">User Name: </span></strong>' . $userReader['full_name'] . '<br/><br/>';

      $email_body .= '<strong><span style="color:#16a085;">Contact Email: </span></strong>' . $userReader['email'] . '<br/><br/>';

      $email_body .= '<strong><span style="color:#16a085;">Contact Number: </span></strong>' . $userReader['contact_number'] . '<br/><br/>';

      $email_body .= $loginUrl;
      $email_body .= '</td></tr>';
      $email_body .= self::emailFooter('No Note');

      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($user_email, $user_name);
      $mail->Subject = 'oboloo: Quote Request';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      //$mail->SMTPDebug = 2;
      $mail->send();
    }
  }


  public static function userAcivation($email, $username)
  {

    $user_id = 0;
    $sql = "select * from users where email='" . $email . "'";
    $user_data = Yii::app()->db->createCommand($sql)->queryRow();

    if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
      $user_id = $user_data['user_id'];

    if (!$user_id) {
      $sql = "select * from users where username='" . $username . "'";
      $user_data = Yii::app()->db->createCommand($sql)->queryRow();
    }
    $user_id = $user_data['user_id'];

    if (true) {
      $new_password = self::generateRandomString(10);
      $user_id = Yii::app()->db->createCommand("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id")->execute();
      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";

      $email_body = '';
      $header1 = '<span style="font-size:22px;"><font color="#7f8c8d">Welcome! User Activation on</font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
      $header2 = '<span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">A member of your organisation has granted you a licence to the </span><strong><span style="color:#16a085;">oboloo</span></strong><span style="color:black"> platform. Your account has been activated and can be accessed with the below credentials:</span></span></span></span>';
      $solutation  = $user_data['full_name'];
      $email_body  .= self::emailHeader($header1, $header2, $solutation);


      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<strong><span style="color:#16a085;">Username: </span></strong>' . $user_data['username'] . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Password: </span></strong>' . $new_password . '<br/><br/>';
      // $email_body .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $user_data['email'] . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">URL: </span></strong><a target="_blank" href="' . Yii::app()->createAbsoluteUrl('app/login') . '">' . Yii::app()->createAbsoluteUrl('app/login') . '</a></td>
         </tr>';
      $email_body .= self::emailFooter();
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($user_data['email'], $user_data['full_name']);
      $mail->Subject = 'oboloo: User Activation';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      $mail->send();
    }
  }
  public static function userPasswordChangeReuest($email, $username)
  {

    $user_id = 0;
    $sql = "select * from users where email='" . $email . "'";
    $user_data = Yii::app()->db->createCommand($sql)->queryRow();

    if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
      $user_id = $user_data['user_id'];

    if (!$user_id) {
      $sql = "select * from users where username='" . $username . "'";
      $user_data = Yii::app()->db->createCommand($sql)->queryRow();

      if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
        $user_id = $user_data['user_id'];
    }

    if (true) {

      // $this->executeQuery("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id");                   

      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";

      $email_body = '';
      $header1 = '<font color="#7f8c8d">Request for new password&nbsp;on</font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong>';
      $header2 = '<span style="color:black">This is a notification from </span><span style="color:#16a085;"><strong>oboloo</strong></span><span style="color:black">. Please follow the link below to change your password:</span></span>';
      $email_body  .= self::emailHeader($header1, $header2);


      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<strong><span style="color:#16a085;">Username: </span></strong>' . $user_data['username'] . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $user_data['email'] . '<br/><br/>';
      $email_body .= '<strong><span style="color:#16a085;">Change Password Link: </span></strong><a href="' . Yii::app()->createAbsoluteUrl('app/changePasswordSave', array("user-id" => base64_encode($user_id), 'time' => base64_encode(date("m-d"))))  . '" title="Change Password">' . Yii::app()->createAbsoluteUrl('app/changePasswordSave', array("user-id" => base64_encode($user_id), 'time' => base64_encode(date("m-d")))) . '</a></td>
         </tr>';
      $email_body .= self::emailFooter('No Note');
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($user_data['email'], $user_data['full_name']);
      $mail->Subject = 'oboloo: Password Change';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      $mail->send();
    }
  }
  public static function userPasswordChanged($email, $username)
  {

    $user_id = 0;
    $sql = "select * from users where email='" . $email . "'";
    $user_data = Yii::app()->db->createCommand($sql)->queryRow();

    if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
      $user_id = $user_data['user_id'];

    if (!$user_id) {
      $sql = "select * from users where username='" . $username . "'";
      $user_data = Yii::app()->db->createCommand($sql)->queryRow();

      if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
        $user_id = $user_data['user_id'];
    }

    if (true) {


      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";

      $email_body = '';
      $header1 = '<font color="#7f8c8d">Your password has been changed on </font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong>';
      $header2 = '<span style="color:black">This is a notification from oboloo to let you know that your password has changed.</span>';
      $email_body  .= self::emailHeader($header1, $header2);
      $email_body .= self::emailFooter('No Note');

      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($user_data['email'], $user_data['full_name']);
      $mail->Subject = 'oboloo: Password Change';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      $mail->send();
    }
  }

  public static function userCode($email, $username, $code)
  {

    $user_id = 0;
    $sql = "select * from users where email='" . $email . "'";
    $user_data = Yii::app()->db->createCommand($sql)->queryRow();

    if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
      $user_id = $user_data['user_id'];

    if (!$user_id) {
      $sql = "select * from users where username='" . $username . "'";
      $user_data = Yii::app()->db->createCommand($sql)->queryRow();

      if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
        $user_id = $user_data['user_id'];
    }

    $mail = new \PHPMailer(true);
    try {
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth  = true;
      $mail->Username  = Yii::app()->params['username'];
      $mail->Password  = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $email_body = '';
      $header1 = '<font color="#7f8c8d">Your login verification code for oboloo can be found below</font>&nbsp;<strong><span style="color:#2d9ca2;"></span></strong>';
      $header2 = '<span style="color:black"><strong>Code: ' . $code . '</strong></span>';
      $email_body  .= self::emailHeader($header1, $header2);
      $email_body .= self::emailFooter('No Note');
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($user_data['email'], $user_data['full_name']);
      $mail->Subject = 'oboloo: Code Verification';
      $mail->isHTML(true);
      $mail->Body = $email_body;

      $mail->send();
      return true;
    } catch (phpmailerException $e) {
      return false;
    } catch (Exception $e) {
      return false;
    }
  }

  public static function generateRandomString($length = 10)
  {
    $characters = '0123456789%@*abcdefghijklmnop!~qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++)
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    return $randomString;
  }

  public static function userPasswordForgotReuest($email, $username)
  {
    $user_id = 0;
    $sql = "select * from users where email='" . $email . "'";
    $user_data = Yii::app()->db->createCommand($sql)->queryRow();
    if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
      $user_id = $user_data['user_id'];


    if (!$user_id) {
      $sql = "select * from users where username='" . $username . "'";
      $user_data = Yii::app()->db->createCommand($sql)->queryRow();
    }
    $user_id = !empty($user_data['user_id']) ? $user_data['user_id'] : 0;
    if (!empty($user_id)) {
      $new_password = self::generateRandomString(10);

      $testing = Yii::app()->db->createCommand("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id")->execute();
      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $emailBody = '';
      $header1 = '<font color="#7f8c8d">Forgotten password &nbsp;on</font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong>';
      $header2 = '<span style="color:black">This is a notification from </span><span style="color:#16a085;"><strong>oboloo</strong></span><span style="color:black">. As per your request, your password has been reset to the below:</span>';
      $emailBody  .= self::emailHeader($header1, $header2);
      $emailBody  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
      $emailBody .= '';
      $emailBody .= '<strong><span style="color:#16a085;">Username: </span></strong>' . $user_data['username'] . '<br/><br/>';
      $emailBody .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $user_data['email'] . '<br/><br/>';
      $emailBody .= '<strong><span style="color:#16a085;">Password: </span></strong>' . $new_password . '</td>
         </tr>';
      $emailBody .= self::emailFooter();
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($user_data['email'], $user_data['full_name']);
      $mail->Subject = 'oboloo: Password Change';
      $mail->isHTML(true);
      $mail->Body = $emailBody;
      $mail->send();
      return 1;
    } else {
      return 0;
    }
  }

  public static function emailHeader($header1, $header2, $solutation = '')
  {
    $bodyHtml = '<div class="mj-container" style="background-color:#FFFFFF;">
     <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="">';
    $bodyHtml .= '<img src="' . AppUrl::bicesUrl('images/Very-Small.png') . '" alt="oboloo logo" style="border:0;display:block;outline:none;text-decoration:none;height:167;font-size:13px; width:300px"/>';
    $bodyHtml .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <span style="font-size:22px;">' . $header1 . '</span>
             </div>
         </td>
         </tr>';
    if (!empty($solutation)) {
      $bodyHtml .= '<tr><td align="left" style="font-size:0px;padding:15px 15px 0px 15px;word-break:break-word;"><div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"> <p><span style="line-height:16.5pt"><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">Dear&nbsp;</span><span style="color:#16a085;"><strong>' . $solutation . '</strong></span></span></span></span></p></div></td></tr>';
    }

    $bodyHtml .= '<tr><td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;"><br /><span style="font-family:" tahoma",sans-serif"="">' . $header2 . '</span></span></span></div></td>
         </tr>';
    return $bodyHtml;
  }

  public static function emailFooter($footerText = '')
  {
    $bodyHtml = '';
    if ($footerText == '') {
      $bodyHtml = '<tr><td align="center" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"><div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><p><strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:#c0392b;">NOTE:</span><span style="color:black"> </span></span></span></strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">If you did not request this change, please contact your administrator immediately.</span></span></span></p></div></td>
         </tr>';
    } else if ($footerText != 'No Note') {
      $bodyHtml = $footerText;
    } else if ($footerText == 'No Note') {
      $bodyHtml .= '
             <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>
         <tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"> <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.2;text-align:left;color:#2D9CA2;">        <p lang="x-size-17"><span style="font-size:14px;"><strong>Thanks</strong><br><br><strong>The oboloo Team</strong></span></p>

       <p></p><p></p></div></td></tr>
         <tr><td align="center" style="font-size:0px;padding:10px 10px 10px 10px;word-break:break-word;"><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"      >        <tr><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;">          
         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;"><tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;">

         <a class="social-icon-login" href="https://www.linkedin.com/company/18585513" target="_blank"> <img src="' . AppUrl::bicesUrl('images/icons/linkedin.png') . '" width="55" height="57" alt="scoail icon"  style="height: 57px;
    width: 60px;" /></a></td></tr></tbody></table></td></tr></tbody></table></td><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;"><tbody><tr><td style="font-size:0;vertical-align:middle; width:55px; height: 57px;"><a class="social-icon-login" href="https://twitter.com/oboloosoftware" target="_blank"> <img src="' . AppUrl::bicesUrl('images/icons/Twitter.PNG') . '" alt="scoail icon" width="55" height="57" style="width:55px !important; height: 57px !important;"  /></a>                </td></tr></tbody></table></td></tr></tbody></table></td><td></td></tr></table></td></tr>
             </tbody></table>
            </div>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </tbody>
            </table>
            </div></td>
            </tr>
            </table>
            </div>';
      return $bodyHtml;
    }
    $bodyHtml .= ' 
         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>
         <tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"> <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.2;text-align:left;color:#2D9CA2;">        <p lang="x-size-17"><span style="font-size:14px;"><strong>Thanks</strong><br><br><strong>The oboloo Team</strong><br><br><em><strong>Support-</strong>&nbsp;<strong style="color:#0000ff">hello@oboloo.com</strong></em><span style="color:#677294; display:block; margin: 50px 0px 20px; font-weight:400"></br></br> If you would like to find out more about oboloo, please <a href="https://oboloo.com" style="color: #0000ff; text-decoration: underline;">click here</a></span></br></br></span></p>

         <p lang="x-size-10" style="margin-bottom:0px;"><a href="https://oboloo.com/privacy-cookies-policy/"><span style="font-size:14px;">Privacy Policy</span></a></p><p lang="x-size-10" style="margin-bottom:0px;"><a href="http://oboloo.com/Terms"><span style="font-size:14px; ">Terms</span></a></p><p lang="x-size-10" style=" margin-bottom:0px;"><span style="font-size:14px;text-decoration: underline;"><a href="https://oboloo.com/modern-slavery/"><span style="color: #0000ff; text-decoration: underline;">Modern Slavery Policy</span></a></span></p><table emb-web-links="" role="presentation"> <tbody>   <tr role="navigation"><td emb-web-links=""></td><td emb-web-links=""></td>      <td emb-web-links=""></td>    </tr> </tbody></table></div></td></tr>
         <tr><td align="center" style="font-size:0px;word-break:break-word;"><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"      >        <tr><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;"><tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;">

         <a class="social-icon-login" href="https://www.linkedin.com/company/18585513" target="_blank"> <img src="' . AppUrl::bicesUrl('images/icons/linkedin.png') . '" width="55" height="57" style="width:59px !important; height: 57px !important;" /></a></td></tr></tbody></table></td></tr></tbody></table></td><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;"><tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;"><a class="social-icon-login" href="https://twitter.com/oboloosoftware" target="_blank"> <img src="' . AppUrl::bicesUrl('images/icons/Twitter.PNG') . '" width="55" height="57" style="width:55px !important; height: 57px !important;" /></a>                </td></tr></tbody></table></td></tr></tbody></table></td><td></td></tr></table></td></tr>
         </tbody></table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';
    return $bodyHtml;
  }

  public static function feedback($name, $email, $company, $message)
  {
    $sendToEmail = "feedback@oboloo.software";

    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth  = true;
    $mail->Username  = Yii::app()->params['username'];
    $mail->Password  = Yii::app()->params['password'];
    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";

    $email_body = '';
    $header1 = '<span style="font-size:22px;"><font color="#7f8c8d">Feedback on</font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $email_body  .= self::emailHeader($header1, $header2);
    $email_body  .= '<tr>
       <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
       <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
    $email_body .= '<strong><span style="color:#16a085;">Full Name: </span></strong>' . $name . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">Company Name: </span></strong>' . $company . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $email . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">Feedback: </span></strong>' . $message . '<br/><br/>
      </tr>';
    $email_body .= self::emailFooter('No Note');
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Client');
    $mail->addAddress($sendToEmail, 'Feedback');
    $mail->Subject = 'oboloo: Feedback';
    $mail->isHTML(true);
    $mail->Body = $email_body;


    $mail->send();
  }

  public static function raiseTicket($name, $email, $company, $message, $filePath)
  {
    $sendToEmail = "Tickets@oboloo.software";

    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth = true;
    $mail->Username = Yii::app()->params['username'];
    $mail->Password = Yii::app()->params['password'];

    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";

    $email_body = '';
    $header1 = '<span style="font-size:22px;"><font color="#7f8c8d">Raise a Ticket on</font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $email_body  .= self::emailHeader($header1, $header2);
    $email_body  .= '<tr>
       <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
       <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
    $email_body .= '';
    $email_body .= '<strong><span style="color:#16a085;">Full Name: </span></strong>' . $name . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">Company Name: </span></strong>' . $company . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $email . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">Raise a Ticket: </span></strong>' . $message . '<br/><br/>
      </tr>';
    $email_body .= self::emailFooter('No Note');
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Client');
    if (!empty($filePath)) {
      $mail->addAttachment($filePath);
    }
    $mail->addAddress($sendToEmail, 'Raise a Ticket');
    $mail->Subject = 'oboloo: Raise a Ticket';
    $mail->isHTML(true);
    $mail->Body = $email_body;


    $mail->send();
  }

  public static function userCancelSubscription($name, $email, $company)
  {


    if (strpos(Yii::app()->getBaseUrl(true), "localhost")) {
      $subdomain = 'bondoporaja';
      $sendToEmail = "alikhust@gmail.com";
    } else {
      $sendToEmail = "cancel@oboloo.software";
      $subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
    }

    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth = true;
    $mail->Username = Yii::app()->params['username'];
    $mail->Password = Yii::app()->params['password'];

    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";

    $email_body = '';
    $header1 = '<span style="font-size:22px;"><font color="#7f8c8d">Cancel Subscription</font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $solutation  = '';
    $email_body  .= self::emailHeader($header1, $header2, $solutation);

    $email_body  .= '<tr>
       <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
       <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
    $email_body .= '';
    $email_body .= '<strong><span style="color:#16a085;">Company Name: </span></strong>' . $company . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">Sub Domain: </span></strong>' . $subdomain . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">User Email: </span></strong>' . $email . '<br/><br/>';
    // $email_body .= '<strong><span style="color:#16a085;">Raise a Ticket: </span></strong>'.$message.'<br/><br/>
    $email_body .= '</tr>';
    $email_body .= self::emailFooter('No Note');
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Client');
    $mail->addAddress($sendToEmail, 'Cancel Subscription');
    $mail->Subject = 'oboloo: Cancel Subscription';
    $mail->isHTML(true);
    $mail->Body = $email_body;
    $mail->send();
  }


  public function vendorApprovalBySentForApprovalEmail($userEmail, $venderName, $venderID, $venderStatus, $userName, $subject, $comments = '')
  {
    // echo "<pre>"; print_r($venderEmail); exit;


    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth = true;
    $mail->Username = Yii::app()->params['username'];
    $mail->Password = Yii::app()->params['password'];
    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";

    $email_body = '';
    $header1 = '<span style="font-size:22px;"><font color="#7f8c8d">Feedback on</font>&nbsp;<strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $email_body  .= self::emailHeader($header1, $header2);
    $email_body  .= '<tr>
       <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
       <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
    $email_body .= '<strong><span style="color:#16a085;">Supplier Name: </span></strong>' . $venderName . '<br/><br/>';
    $email_body .= '<strong><span style="color:#16a085;">Supplier Status: </span></strong>' . $venderStatus . '<br/><br/>';
    if (!empty($comments)) {
      $email_body .= '<strong><span style="color:#16a085;">Comments: </span></strong>' . $comments . '<br/><br/>';
    }

    $email_body .= '<strong><span style="color:#16a085;">Supplier Url: </span></strong><a href="' . Yii::app()->createAbsoluteUrl('vendors/edit', array('id' => $venderID)) . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px;mso-padding-alt:0px;border-radius:30px; " target="_blank">' . Yii::app()->createAbsoluteUrl('vendors/edit', array('id' => $venderID)) . '</a><br/><br/> 
      </tr>';
    $email_body .= self::emailFooter('No Note');
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Client');
    $mail->addAddress($userEmail, $subject);
    $mail->Subject = 'oboloo: ' . $subject;
    $mail->isHTML(true);
    $mail->Body = $email_body;
    $mail->send();
  }


  public static function contributeUserQuote($quoteID, $contributorID)
  {   
      $quote = new Quote();
      $quote = $quote->getOne(['quote_id' => $quoteID]);
      $contributeUser = new User();
      $contributeUser = $contributeUser->getOne(['user_id' => $contributorID]);
      $crtbutorDateLine = FunctionManager::getCrtburelineDate($quote['quote_id'],'');
      $email_body = '';
      $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
      $header2 = '';

      $email_body  .= self::emailHeader($header1 = '', $header2);
      $notificationComments ='';
      $notificationComments  .= "<b>" . $quote['full_name'] . "</b> has invited you to contribute to <b>" . $quote['quote_name'] . "</b><br />";
      $notificationComments  .= "Please make any contributions by the deadine of <b>" . $crtbutorDateLine . "</b>";
      $loginUrl = '<tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellspacing="0" cellpadding="0" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle">

              <a href="' .  AppUrl::bicesUrl('quotes/edit/' . $quote['quote_id']) . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Login here</a></td>
              </tr></tbody></table></td></tr>';

      $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
      $email_body .= '';
      $email_body .= '<span style="font-size:18px;color:#54595F;">' . $notificationComments . '</span><br/><br/>';
      $email_body .= '</td></tr>';
      $email_body .= $loginUrl;

      $email_body .= self::emailFooter('No Note');

      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
      $mail->CharSet = "UTF-8";
      $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
      $mail->addAddress($contributeUser['email'], $contributeUser['username']);
      $mail->Subject = 'oboloo: You Have Been Asked To Contribute To A Sourcing Activity';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      if ($mail->send()) {
        return 1;
      } else {
        return 0;
      }
  }

  public static function quoteOpenForScoring($quote, $userID)
  {
    $user = new User();
    $quoteScorer  = $user->getOne(['user_id' => $userID]);
    $email_body = '';
    $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $email_body  .= self::emailHeader($header1 = '', $header2);
    $notificationComments ='';
    $notificationComments  .= "<b>" . $quote['created_by_name'] . "</b> has invited you to score the supplier responses for the sourcing activity - <b>" . $quote['quote_name'] . "</b><br />";
    // $notificationComments  .= "Please make any contributions by the deadine of <b>" . FunctionManager::getCrtburelineDate($quoteuser['quote_id']) . "</b>";
    
    $loginUrl = '<tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellspacing="0" cellpadding="0" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle">

              <a href="' .  AppUrl::bicesUrl('quotes/myscoringdetail/'. $_POST['quote_id']) . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Score Responses</a></td>
              </tr></tbody></table></td></tr>';

    $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
    $email_body .= '';
    $email_body .= '<span style="font-size:18px;color:#54595F;">' . $notificationComments . '</span><br/><br/>';
    $email_body .= '</td></tr>';
    $email_body .= $loginUrl;

    $email_body .= self::emailFooter('No Note');

    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth = true;
    $mail->Username = Yii::app()->params['username'];
    $mail->Password = Yii::app()->params['password'];
    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
    $mail->addAddress($quoteScorer['email'], $quoteScorer['username']);
    $mail->Subject = 'oboloo: You Have Been Invited To Score To A Sourcing Activity';
    $mail->isHTML(true);
    $mail->Body = $email_body;
    //$mail->SMTPDebug = 2;
    $mail->send();
  }


  public static function reOpenForScoring($quote, $userID)
  {
    $user = new User();
    $quoteScorer  = $user->getOne(['user_id' => $userID]);
    $email_body = '';
    $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $email_body  .= self::emailHeader($header1 = '', $header2);
    $notificationComments ='';
    $notificationComments  .= "<b>" . $quote['created_by_name']."</b> has reopened the supplier scoring for sourcing activity <b> ".$quote['quote_name']."</b> You can now amend your scores for this sourcing activity<br />";
    
    $loginUrl = '<tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellspacing="0" cellpadding="0" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle">

              <a href="' .  AppUrl::bicesUrl('quotes/myscoringdetail/'. $quote['quote_id']) . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Score Responses</a></td>
              </tr></tbody></table></td></tr>';

    $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
    $email_body .= '';
    $email_body .= '<span style="font-size:18px;color:#54595F;">' . $notificationComments . '</span><br/><br/>';
    $email_body .= '</td></tr>';
    $email_body .= $loginUrl;

    $email_body .= self::emailFooter('No Note');

    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth = true;
    $mail->Username = Yii::app()->params['username'];
    $mail->Password = Yii::app()->params['password'];
    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
    $mail->addAddress($quoteScorer['email'], $quoteScorer['username']);
    $mail->Subject = 'oboloo: You Have Been Asked To Contribute To A Sourcing Activity';
    $mail->isHTML(true);
    $mail->Body = $email_body;
    //$mail->SMTPDebug = 2;
    $mail->send();
  }

  public static function closedQuoteScorer($quote, $userID)
  {
    $user = new User();
    $quoteScorer  = $user->getOne(['user_id' => $userID]);
    $email_body = '';
    $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $email_body  .= self::emailHeader($header1 = '', $header2);
    $notificationComments ='';
    $notificationComments  .= "<b>" . $quote['created_by_name']."</b> has closed the supplier scoring for sourcing activity <b> ".$quote['quote_name']."</b> You can no longer submit supplier scoring for this sourcing activity <br />";
    
    $loginUrl = '<tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellspacing="0" cellpadding="0" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle">

              <a href="' .  AppUrl::bicesUrl('quotes/myscoringdetail/'. $quote['quote_id']) . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Score Responses</a></td>
              </tr></tbody></table></td></tr>';

    $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
    $email_body .= '';
    $email_body .= '<span style="font-size:18px;color:#54595F;">' . $notificationComments . '</span><br/><br/>';
    $email_body .= '</td></tr>';
    $email_body .= $loginUrl;

    $email_body .= self::emailFooter('No Note');

    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth = true;
    $mail->Username = Yii::app()->params['username'];
    $mail->Password = Yii::app()->params['password'];
    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
    $mail->addAddress($quoteScorer['email'], $quoteScorer['username']);
    $mail->Subject = 'oboloo: You Have Been Asked To Contribute To A Sourcing Activity';
    $mail->isHTML(true);
    $mail->Body = $email_body;
    //$mail->SMTPDebug = 2;
    $mail->send();
  }

    public static function scorerHasCompletedScoring($quote, $userID)
  {
    $user = new User();
    $quoteScorer  = $user->getOne(['user_id' => $userID]);
    $quoteCreator = $user->getOne(['user_id' => $quote['created_by_user_id']]);
    $email_body = '';
    $header1 = '<span style="font-size:22px;"><span style="color:#7f8c8d;">You have a new</span> <span style="color:#2d9ca2;">notification</span> <span style="color:#7f8c8d;">on</span> <strong><span style="color:#2d9ca2;">oboloo</span></strong></span>';
    $header2 = '';
    $email_body  .= self::emailHeader($header1 = '', $header2);
    $notificationComments ='';
    $notificationComments  .= "<b>" . $quoteScorer['full_name']."</b> has completed the supplier scoring for sourcing activity <b> ".$quote['quote_name']."</b>.<br />";
    
    $loginUrl = '<tr><td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;"><table  cellspacing="0" cellpadding="0" role="presentation" style="border-collapse:separate;line-height:100%;">
              <tbody><tr><td align="center" bgcolor="#2D9CA2" role="presentation" style="cursor:auto;mso-padding-alt:11px 32px 11px 32px;background:#2D9CA2; border-radius:30px !important;" valign="middle">

              <a href="' .  AppUrl::bicesUrl('quotes/myscoringdetail/'. $quote['quote_id']) . '" style="display:inline-block;background:#2D9CA2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:11px 32px 11px 32px !important;mso-padding-alt:0px;border-radius: 30px !important; text-decoration:none !important;" target="_blank">Score Responses</a></td>
              </tr></tbody></table></td></tr>';

    $email_body  .= '<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><br />';
    $email_body .= '';
    $email_body .= '<span style="font-size:18px;color:#54595F;">' . $notificationComments . '</span><br/><br/>';
    $email_body .= '</td></tr>';
    $email_body .= $loginUrl;

    $email_body .= self::emailFooter('No Note');

    $mail = new \PHPMailer(true);
    $mail->IsSMTP();
    $mail->Host = Yii::app()->params['host'];
    $mail->Port = Yii::app()->params['port'];
    $mail->SMTPAuth = true;
    $mail->Username = Yii::app()->params['username'];
    $mail->Password = Yii::app()->params['password'];
    $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
    $mail->CharSet = "UTF-8";
    $mail->setFrom(Yii::app()->params['email'], 'oboloo Notifications');
    $mail->addAddress($quoteCreator['email'], $quoteCreator['username']);
    $mail->Subject = 'oboloo:'.$quoteScorer['full_name']." Has Completed Scoring For ".$quote['quote_name'];
    $mail->isHTML(true);
    $mail->Body = $email_body;
    //$mail->SMTPDebug = 2;
    $mail->send();
  }

}
