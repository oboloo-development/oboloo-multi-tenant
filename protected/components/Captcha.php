<?php  if ( ! defined('YII_PATH')) exit('No direct script access allowed');

/**
 * This class defines method to generate captcha images to verify human
 * interaction when filling web forms. It generates a random sequence of
 * characters using different fonts and also some other such different
 * attributes as tilt angle etc. and creates an image. The value of the
 * randomly generated string is then stored in the session so that it can
 * then be checked against user input. It is used in various places such
 * as new user registration and forgot password options.
 */

class Captcha
{
	/**
 	 * \return $captcha - array containing randomly generated string and image output file name containing the string
	 * 
	 * This method is used to generate a random string, store it in session,
	 * apply different font, tilt angle and other such attributes to each of
	 * the characters in the string and finally create an image with this
	 * generated string superimposed on it to be displayed to the user as
	 * kind of a challenge.
	 */
	public function get()
	{
		# create the random string to be used as captcha
   		$rand_str = substr(str_shuffle('abcefghijkmnpqrstuvwxyz#123456789!'), rand(0, 10), 6);
   		
		# Get each letter in one valiable, we will format all letters different
		$letter1 = substr($rand_str, 0, 1); $letter2 = substr($rand_str, 1, 1); $letter3 = substr($rand_str, 2, 1);
		$letter4 = substr($rand_str, 3, 1); $letter5 = substr($rand_str, 4, 1); $letter6 = substr($rand_str, 5, 1);
		
		# Select random background image
		$base_path = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->request->getBaseUrl();
		$bg_image = $base_path . '/images/captcha_background.png';
		
		# Creates an image from a png file. 
		# If you want to use gif or jpg images, just use the coresponding 
		# functions: imagecreatefromjpeg and imagecreatefromgif.
		$image = imagecreatefrompng($bg_image);
		
		# Get a random angle for each letter to be rotated with.
		$angle1 = rand(-20, 20); $angle2 = rand(-20, 20); $angle3 = rand(-20, 20); 
		$angle4 = rand(-20, 20); $angle5 = rand(-20, 20); $angle6 = rand(-20, 20);
		
		# Get a random font. (In this examples, the fonts are located in 
		# "fonts" directory and named from 1.ttf to 10.ttf)
		$font_dir = $base_path . '/images/captcha_fonts/';
		$font1 = $font_dir . "16.ttf"; $font2 = $font_dir . rand(11, 17) . ".ttf"; $font3 = $font_dir . rand(11, 17) . ".ttf"; 
		$font4 = $font_dir . rand(11, 17) . ".ttf"; $font5 = $font_dir . rand(11, 17) . ".ttf"; $font6 = $font_dir . rand(11, 17) . ".ttf";
		
		# Define a table with colors (the values are the RGB components for each color).
		$colors[0] = array(35, 113, 191);  $colors[1] = array(55, 85, 114); $colors[2] = array(55, 114, 85);
		$colors[3] = array(114, 21, 67); $colors[4] = array(0, 0, 0); $colors[5] = array(255, 120, 0);
		
		# Get a random color for each letter.
		$color1 = rand(0, 5); $color2 = rand(0, 5); $color3 = rand(0, 5); $color4 = rand(0, 5); $color5 = rand(0, 5); $color6 = rand(0, 5);
		
		# Allocate colors for letters.
		$textColor1 = imagecolorallocate ($image, $colors[$color1][0], $colors[$color1][1], $colors[$color1][2]);
		$textColor2 = imagecolorallocate ($image, $colors[$color2][0], $colors[$color2][1], $colors[$color2][2]);
		$textColor3 = imagecolorallocate ($image, $colors[$color3][0], $colors[$color3][1], $colors[$color3][2]);
		$textColor4 = imagecolorallocate ($image, $colors[$color4][0], $colors[$color4][1], $colors[$color4][2]);
		$textColor5 = imagecolorallocate ($image, $colors[$color5][0], $colors[$color5][1], $colors[$color5][2]);
		$textColor6 = imagecolorallocate ($image, $colors[$color5][0], $colors[$color6][1], $colors[$color6][2]);
		
		# Write text to the image using TrueType fonts.
		$size = 22; $chart_start = 5; $char_space = 25; $y = 30;
		
		imagettftext($image, $size, $angle1, $chart_start, $y, $textColor1, $font1, $letter1);
		imagettftext($image, $size, $angle2, $chart_start + ($char_space * 1), $y, $textColor2, $font2, $letter2);
		imagettftext($image, $size, $angle3, $chart_start + ($char_space * 2), $y, $textColor3, $font3, $letter3);
		imagettftext($image, $size, $angle4, $chart_start + ($char_space * 3), $y, $textColor4, $font4, $letter4);
		imagettftext($image, $size, $angle5, $chart_start + ($char_space * 4), $y, $textColor5, $font5, $letter5);
		imagettftext($image, $size, $angle6, $chart_start + ($char_space * 5), $y, $textColor6, $font6, $letter6);
		
		# create output file containing this string superimposed on that image
		$outputfilename = 'captcha_images/' . Yii::app()->session->sessionID . '.png';
		$outputfile = $base_path . '/images/' . $outputfilename;
		if (file_exists($outputfile)) unlink($outputfile);
		imagepng($image, $outputfile);
		imagedestroy($image);
		
		# return the output file name as well as random captcha string generated
		return array('code' => $rand_str, 'file' => $outputfilename);
	}
	
	/**
	 * This method is used to remove existing captcha file for the logged in user
	 * if any so that if necessary a new one can be generated in the same place
	 */
	public function removeFile()
	{
		$base_path = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->request->getBaseUrl();
		$outputfilename = 'captcha_images/' . Yii::app()->session->sessionID . '.png';
		$outputfile = $base_path . '/images/' . $outputfilename;
		if (file_exists($outputfile)) unlink($outputfile);		
	}

	/**
	 * This method is used to remove really old captcha image files 
	 * if there are any present so that disk space does not get full
	 */
	public function removeOldFiles()
	{
		$path = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->request->getBaseUrl() . '/images/captcha_images/';
		if ($handle = opendir($path))
			while (false !== ($file = readdir($handle)))
				if (strpos($file, ".png") !== false && 
						(time()- filectime($path . $file)) >= 86400)
					unlink($path . $file);
	}

}