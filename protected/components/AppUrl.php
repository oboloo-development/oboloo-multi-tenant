<?php  if ( ! defined('YII_PATH')) exit('No direct script access allowed');

/**
 * This class defines method to generate URLs in readable format
 * by wrapping the YII framework provided URL access methods
 */

class AppUrl
{
    /**
     * Makes the given URL relative to the base directory
     */
    public static function bicesUrl($url){
        return  Yii::app()->request->getBaseUrl(true) . '/' . $url;
    }
    
    /**
     * Makes the given URL relative to the /image directory
     */
    public static function imageUrl($url) {
        return  Yii::app()->request->getBaseUrl(true) . '/images/' .$url;
    }

    /**
    * Makes the given URL relative to the /css directory
    */
    public static function cssUrl($url) {
        return  Yii::app()->request->getBaseUrl(true) . '/css/' .$url;
    }
    /**
    * Makes the given URL relative to the /js directory
    */
    public static function jsUrl($url) {
        return  Yii::app()->request->getBaseUrl(true) . '/js/' . $url;
    }
	
	public static function fullPath($path) {
		return str_replace('\\', '/', dirname(__FILE__)) . '/../../' . $path;
	}
}