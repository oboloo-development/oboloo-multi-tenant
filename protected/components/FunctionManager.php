<?php
class FunctionManager
{
  public static $colorCache = [];
  public static function showCurrencySymbol($currency = '', $currency_id = '')
  {
    if (!empty($currency_id)) {
      $sql = "select currency from currency_rates where id=" . $currency_id;
      $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
      if (!empty($currencyReader['currency'])) {
        $currency = $currencyReader['currency'];
        return $currency;
      }
    }

    $currencies = array('aed' => 'AED', 'afn' => '&#1547;', 'all' => '&#76;&#101;&#107;', 'amd' => 'AMD', 'ang' => '&#402;', 'aoa' => 'AOA', 'ars' => '&#36;', 'aud' => '&#36;', 'awg' => '&#402;', 'azn' => '&#1084;&#1072;&#1085;', 'bam' => '&#75;&#77;', 'bbd' => '&#36;', 'bdt' => 'BDT', 'bgn' => '&#1083;&#1074;', 'bhd' => 'BHD', 'bif' => 'BIF', 'bmd' => '&#36;', 'bnd' => '&#36;', 'bob' => '&#36;&#98;', 'brl' => '&#82;&#36;', 'bsd' => '&#36;', 'btn' => 'BTN', 'bwp' => '&#80;', 'byr' => '&#112;&#46;', 'bzd' => '&#66;&#90;&#36;', 'cad' => '&#36;', 'cdf' => 'CDF', 'chf' => '&#67;&#72;&#70;', 'clp' => '&#36;', 'cny' => '&#165;', 'cop' => '&#36;', 'crc' => '&#8353;', 'cuc' => 'CUC', 'cup' => '&#8369;', 'cve' => 'CVE', 'czk' => '&#75;&#269;', 'djf' => 'DJF', 'dkk' => '&#107;&#114;', 'dop' => '&#82;&#68;&#36;', 'dzd' => 'DZD', 'egp' => '&#163;', 'ern' => 'ERN', 'etb' => 'ETB', 'eur' => '&#8364;', 'fjd' => '&#36;', 'fkp' => '&#163;', 'gbp' => '&#163;', 'gel' => 'GEL', 'ggp' => '&#163;', 'ghs' => '&#162;', 'gip' => '&#163;', 'gmd' => 'GMD', 'gnf' => 'GNF', 'gtq' => '&#81;', 'gyd' => '&#36;', 'hkd' => '&#36;', 'hnl' => '&#76;', 'hrk' => '&#107;&#110;', 'htg' => 'HTG', 'huf' => '&#70;&#116;', 'idr' => '&#82;&#112;', 'ils' => '&#8362;', 'imp' => '&#163;', 'inr' => '&#8377;', 'iqd' => 'IQD', 'irr' => '&#65020;', 'isk' => '&#107;&#114;', 'jep' => '&#163;', 'jmd' => '&#74;&#36;', 'jod' => 'JOD', 'jpy' => '&#165;', 'kes' => 'KES', 'kgs' => '&#1083;&#1074;', 'khr' => '&#6107;', 'kmf' => 'KMF', 'kpw' => '&#8361;', 'krw' => '&#8361;', 'kwd' => 'KWD', 'kyd' => '&#36;', 'kzt' => '&#1083;&#1074;', 'lak' => '&#8365;', 'lbp' => '&#163;', 'lkr' => '&#8360;', 'lrd' => '&#36;', 'lsl' => 'LSL', 'lyd' => 'LYD', 'mad' => 'MAD', 'mdl' => 'MDL', 'mga' => 'MGA', 'mkd' => '&#1076;&#1077;&#1085;', 'mmk' => 'MMK', 'mnt' => '&#8366;', 'mop' => 'MOP', 'mro' => 'MRO', 'mur' => '&#8360;', 'mvr' => 'MVR', 'mwk' => 'MWK', 'mxn' => '&#36;', 'myr' => '&#82;&#77;', 'mzn' => '&#77;&#84;', 'nad' => '&#36;', 'ngn' => '&#8358;', 'nio' => '&#67;&#36;', 'nok' => '&#107;&#114;', 'npr' => '&#8360;', 'nzd' => '&#36;', 'omr' => '&#65020;', 'pab' => '&#66;&#47;&#46;', 'pen' => '&#83;&#47;&#46;', 'pgk' => 'PGK', 'php' => '&#8369;', 'pkr' => '&#8360;', 'pln' => '&#122;&#322;', 'prb' => 'PRB', 'pyg' => '&#71;&#115;', 'qar' => '&#65020;', 'ron' => '&#108;&#101;&#105;', 'rsd' => '&#1044;&#1080;&#1085;&#46;', 'rub' => '&#1088;&#1091;&#1073;', 'rwf' => 'RWF', 'sar' => '&#65020;', 'sbd' => '&#36;', 'scr' => '&#8360;', 'sdg' => 'SDG', 'sek' => '&#107;&#114;', 'sgd' => '&#36;', 'shp' => '&#163;', 'sll' => 'SLL', 'sos' => '&#83;', 'srd' => '&#36;', 'ssp' => 'SSP', 'std' => 'STD', 'syp' => '&#163;', 'szl' => 'SZL', 'thb' => '&#3647;', 'tjs' => 'TJS', 'tmt' => 'TMT', 'tnd' => 'TND', 'top' => 'TOP', 'try' => '&#8378;', 'ttd' => '&#84;&#84;&#36;', 'twd' => '&#78;&#84;&#36;', 'tzs' => 'TZS', 'uah' => '&#8372;', 'ugx' => 'UGX', 'usd' => '&#36;', 'uyu' => '&#36;&#85;', 'uzs' => '&#1083;&#1074;', 'vef' => '&#66;&#115;', 'vnd' => '&#8363;', 'vuv' => 'VUV', 'wst' => 'WST', 'xaf' => 'XAF', 'xcd' => '&#36;', 'xof' => 'XOF', 'xpf' => 'XPF', 'yer' => '&#65020;', 'zar' => '&#82;', 'zmw' => 'ZMW');
    if (array_key_exists($currency, $currencies)) {
      $symbol = $currencies[$currency];
    } else {
      $symbol = $currency;
    }
    return $symbol;
  }

  public static function saveNotification($userID, $text, $flag = '', $type = 'Client', $notificationType = '')
  {
    $notification = new Notification();
    $notification->rs = array();
    $notification->rs['id'] = 0;
    $notification->rs['user_type'] = $type;
    $notification->rs['notification_flag'] = $flag;
    $notification->rs['user_id'] = $userID;
    $notification->rs['notification_text'] = $text;
    $notification->rs['notification_date'] = date("Y-m-d H:i");
    $notification->rs['read_flag'] = 0;
    $notification->rs['notification_type'] = $notificationType;
    $notification->write();
  }
  public static function checkNotification($flag = '', $user_id, $type = 'Client')
  {
    //if(!empty($user_id)){
    if ($user_id == '' || $user_id == 0) $user_id = 0;
    $sql  = 'select * from notifications where notification_flag="' . $flag . '" and user_id=' . $user_id . ' and user_type="' . $type . '"';
    $checkReader = Yii::app()->db->createCommand($sql)->queryRow();
    return $checkReader;
    if (!empty($checkReader)) {
      return $checkReader;
    }
    //}

    return '';
  }

  public static function orderCurrSymbol($orderID)
  {
    $sql = "select currency_id from orders where order_id=" . $orderID;
    $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
    return self::showCurrencySymbol('', $currencyReader['currency_id']);
  }

  public static function productPriceInGBP($productID)
  {

    if (!empty($productID)) {
      $sql = "select currency_rates.rate,products.price from currency_rates,products where currency_rates.id=products.currency_id and products.product_id=" . $productID;
      $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
      if (!empty($currencyReader['rate'])) {
        $currencyRate = $currencyReader['rate'];
      } else {
        $currencyRate = 1;
      }

      $productPriceInGBP = round($currencyReader['price'] / $currencyRate, 2);
      return $productPriceInGBP;
    }
    return "0";
  }

  public static function orderTotalPrice($orderID)
  {
    $sql = "select sum(calc_unit_price*quantity)+sum(calc_tax)+sum(shipping_cost) as total_price from order_details where order_id=" . $orderID;
    $detail_order = Yii::app()->db->createCommand($sql)->queryRow();
    return $detail_order['total_price'];
  }

  public static function priceInGBP($price, $currency)
  {

    $sql = "select rate from currency_rates where LOWER(currency)='" . strtolower($currency) . "'";
    $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
    if (!empty($currencyReader['rate'])) {
      $currencyRate = $currencyReader['rate'];
    } else {
      $currencyRate = 1;
    }

    $priceInGBP = $price / $currencyRate;
    return number_format($priceInGBP, 2, '.', '');
  }

  public static function priceInGBPByID($price, $currencyID)
  {

    $sql = "select rate from currency_rates where id=" . $currencyID;
    $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
    if (!empty($currencyReader['rate'])) {
      $currencyRate = $currencyReader['rate'];
    } else {
      $currencyRate = 1;
    }

    $priceInGBP = $price / $currencyRate;
    return number_format($priceInGBP, 2, '.', '');
  }


  public static function currencyRate($currency)
  {
    $sql = "select rate from currency_rates where LOWER(currency)='" . strtolower($currency) . "'";
    $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
    return !empty($currencyReader['rate']) ? $currencyReader['rate'] : 0;
  }

  public static function routingStatus($key = '')
  {
    $status = array('Pending' => 'Pending', 'Approved' => 'Approved', 'Declined' => 'Declined', 'Information' => 'More Info Needed');

    if (!empty($key)) {
      return $status[$key];
    } else
      return $status;
  }

  public static function currencySymbol($currencyID = '', $currency = '')
  {

    $sql = "select currency,currency_symbol from currency_rates where id='" . $currencyID . "' or LOWER(currency)='" . $currency . "'";
    $currencyReader = Yii::app()->db->createCommand($sql)->queryRow();
    $currency = $currencyReader['currency'];

    $symbol = $currencyReader['currency_symbol'];

    /*switch ($currency) {
            case 'GBP' : $symbol = '&#163;';
                break;
            case 'USD' : $symbol = '$';
                break;
            case 'CAD' : $symbol = '$';
                break;
            case 'CHF' : $symbol = '&#8355;';
                break;
            case 'EUR' : $symbol = '&#8364;';
                break;
            case 'JPY' : $symbol = '&#165;';
                break;
            case 'INR' : $symbol = '&#x20B9;';
                break;
            default :    $symbol = '&#163;';
                break;
        }*/
    return $symbol;
  }


  public static function vendorDocument($key = '')
  {
    $sql = "select name,id from vendor_document_type where created_type ='General' and soft_deleted=0 order by name";
    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $document = array();
    foreach ($reader as $value) {
      $document[$value['id']] = $value['name'];
    }

    if (!empty($key)) {
      $sql = "select name,id from vendor_document_type where id='" . $key . "'  and soft_deleted=0 order by name ";
      $reader = Yii::app()->db->createCommand($sql)->queryRow();
      return $reader['name'];
    }
    return $document;
  }


  public static function vendorDocumentBgColor($key='') {
    $sql = "select btn_style,id from vendor_document_type";
    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $document = array();

    foreach($reader as $value){
        if (!isset(self::$colorCache[$value['id']])) {
            self::$colorCache[$value['id']] = self::getRandomColor();
        }
        $document[$value['id']] = self::$colorCache[$value['id']];
    }    

    if(!empty($key)){
        $color = isset($document[$key]) ? $document[$key] : self::getRandomColor();
        return "background-color: $color; border-color: $color ";
    }

    $randomColors = array_map(function($color) {
        return "background-color: $color; border-color: $color ";
    }, $document);

    return $randomColors;
}

  public static function vendorDocumentOnboard($key = '')
  {
    $sql = "select name,id from vendor_document_type_onboard where created_type ='General' order by name";
    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $document = array();
    foreach ($reader as $value) {
      $document[$value['id']] = $value['name'];
    }

    if (!empty($key)) {
      $sql = "select name,id from vendor_document_type_onboard where id='" . $key . "' order by name ";
      $reader = Yii::app()->db->createCommand($sql)->queryRow();
      return $reader['name'];
    }
    return $document;
  }

  public static function vendorDocumentBgColorOnboard($key = '')
  {
    $sql = "select btn_style,id from vendor_document_type_onboard";
    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $document = array();
    foreach ($reader as $value) {
      $document[$value['id']] = $value['btn_style'];
    }

    if (!empty($key)) {
      return $document[$key];
    }
    return $document;
  }

  public static function contractDocument($key = '')
  {

    $sql = "select name,id from contract_document_type where created_type ='General' and soft_deleted=0 order by name ";
    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $document = array();
    foreach ($reader as $value) {
      $document[$value['id']] = $value['name'];
    }

    if (!empty($key)) {
      $sql = "select name,id from contract_document_type where id='" . $key . "' and soft_deleted=0 order by name ";
      $reader = Yii::app()->db->createCommand($sql)->queryRow();
      return $reader['name'];
    }
    return $document;
  }

  public static function contractDocumentBgColor($key = '')
  {
    $sql = "select btn_style,id from contract_document_type";
    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $document = array();
    foreach($reader as $value){
        if (!isset(self::$colorCache[$value['id']])) {
            self::$colorCache[$value['id']] = self::getRandomColor();
        }
        $document[$value['id']] = self::$colorCache[$value['id']];
    }    

    if(!empty($key)){
        $color = isset($document[$key]) ? $document[$key] : self::getRandomColor();
        return "background-color: $color; border-color: $color ";
    }

    $randomColors = array_map(function($color) {
        return "background-color: $color; border-color: $color ";
    }, $document);

    return $randomColors;
  }


  public static function quoteCommunicationDocument($key = '')
  {
    $document = array(
      1 => 'Statement of work',
      2 => 'Detail brief (upload PPT/ word/ PDF file)',
      3 => "Reference  (upload Mpeg/Mov/ jpeg file/photo as reference)",
      4 => 'Records (upload email / wechat screen shot, back and forth exchange with BT as evidence)',
      5 => 'Confirmation emails',
      6 => 'Purchase Orders',
      7 => 'Vendor’s Proof of Work',
      8 => 'Vendor’s KPI report',
      9 => 'Vendor’s invoice and Fapiao',
      10 => 'Misc',
      11 => 'Others',
    );
    if (!empty($key)) {
      return $document[$key];
    }
    return $document;
  }

  public static function quoteCommunicationDocumentBgColor($key = '')
  {
    $document = array(
      1 => 'background:#e88686;border:solid 1px #e88686',
      2 => 'background:#2d9ca2;border:solid 1px #2d9ca2',
      3 => 'background:#0cbfc9;border:solid 1px #0cbfc9',
      4 => 'background:#86e88d;border:solid 1px #86e88d',
      5 => 'background:#e88686;border:solid 1px #e88686',
      6 => 'background:#ff9c6b;border:solid 1px #ff9c6b',
      7 => 'background:#ff6b6b;border:solid 1px #ff6b6b',
      8 => 'background:#ffb01c;border:solid 1px #ffb01c',
      9 => 'background:#ffb01c;border:solid 1px #ffb01c',
      10 => 'background:#ffb01c;border:solid 1px #ffb01c',
      11 => 'background:#86e88d;border:solid 1px #86e88d',
    );
    if (!empty($key)) {
      return $document[$key];
    }
    return $document;
  }

  public static function projectStatus($key = '')
  {
    /*$document = array(1=>'Scoping',
                          2=>'In progress',
                          3=>'Complete',
                    );*/

    $document =  array();
    $sql = "select * from project_status";
    $statusReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    foreach ($statusReader as $value) {
      $document[$value['id']] =  $value['value'];
    }

    if (!empty($key)) {
      return $document[$key];
    }
    return $document;
  }
  public static function contractSearchStatus($key = '')
  {
    $arr = array(
      'Active' => 'Active',
      // 'Expires in 3 months'=>'Expires in 3 months',
      // 'Expires in 30 days'=>'Expires in 30 days',
      'Expired' => 'Expired',
    );

    if (!empty($key)) {
      return $arr[$key];
    }
    return $arr;
  }

  public static function dateFormat()
  {
    $dateFormate = Yii::app()->session['default_date_format'];

    if ($dateFormate == "MM/DD/YYYY" or $dateFormate == "mm/dd/yyyy") {
      $dateFormate = "m/d/Y";
    } else if ($dateFormate == "DD/MM/YYYY" or $dateFormate == "dd/mm/yyyy") {
      $dateFormate = "d/m/Y";
    } else {
      $dateFormate = "d/m/Y";
    }

    return $dateFormate;
  }

  public static function dateFormatJS()
  {
    $dateFormate = Yii::app()->session['default_date_format'];
    if (empty($dateFormate)) {
      $dateFormate = "DD/MM/YYYY";
    }
    return $dateFormate;
  }

  public static function countryList($key = '')
  {

    $countries = array(
      "AF" => "Afghanistan",
      "AX" => "Åland Islands",
      "AL" => "Albania",
      "DZ" => "Algeria",
      "AS" => "American Samoa",
      "AD" => "Andorra",
      "AO" => "Angola",
      "AI" => "Anguilla",
      "AQ" => "Antarctica",
      "AG" => "Antigua and Barbuda",
      "AR" => "Argentina",
      "AM" => "Armenia",
      "AW" => "Aruba",
      "AU" => "Australia",
      "AT" => "Austria",
      "AZ" => "Azerbaijan",
      "BS" => "Bahamas",
      "BH" => "Bahrain",
      "BD" => "Bangladesh",
      "BB" => "Barbados",
      "BY" => "Belarus",
      "BE" => "Belgium",
      "BZ" => "Belize",
      "BJ" => "Benin",
      "BM" => "Bermuda",
      "BT" => "Bhutan",
      "BO" => "Bolivia",
      "BA" => "Bosnia and Herzegovina",
      "BW" => "Botswana",
      "BV" => "Bouvet Island",
      "BR" => "Brazil",
      "IO" => "British Indian Ocean Territory",
      "BN" => "Brunei Darussalam",
      "BG" => "Bulgaria",
      "BF" => "Burkina Faso",
      "BI" => "Burundi",
      "KH" => "Cambodia",
      "CM" => "Cameroon",
      "CA" => "Canada",
      "CV" => "Cape Verde",
      "KY" => "Cayman Islands",
      "CF" => "Central African Republic",
      "TD" => "Chad",
      "CL" => "Chile",
      "CN" => "China",
      "CX" => "Christmas Island",
      "CC" => "Cocos (Keeling) Islands",
      "CO" => "Colombia",
      "KM" => "Comoros",
      "CG" => "Congo",
      "CD" => "Congo, The Democratic Republic of The",
      "CK" => "Cook Islands",
      "CR" => "Costa Rica",
      "CI" => "Cote D'ivoire",
      "HR" => "Croatia",
      "CU" => "Cuba",
      "CY" => "Cyprus",
      "CZ" => "Czech Republic",
      "DK" => "Denmark",
      "DJ" => "Djibouti",
      "DM" => "Dominica",
      "DO" => "Dominican Republic",
      "EC" => "Ecuador",
      "EG" => "Egypt",
      "SV" => "El Salvador",
      "GQ" => "Equatorial Guinea",
      "ER" => "Eritrea",
      "EE" => "Estonia",
      "ET" => "Ethiopia",
      "FK" => "Falkland Islands (Malvinas)",
      "FO" => "Faroe Islands",
      "FJ" => "Fiji",
      "FI" => "Finland",
      "FR" => "France",
      "GF" => "French Guiana",
      "PF" => "French Polynesia",
      "TF" => "French Southern Territories",
      "GA" => "Gabon",
      "GM" => "Gambia",
      "GE" => "Georgia",
      "DE" => "Germany",
      "GH" => "Ghana",
      "GI" => "Gibraltar",
      "GR" => "Greece",
      "GL" => "Greenland",
      "GD" => "Grenada",
      "GP" => "Guadeloupe",
      "GU" => "Guam",
      "GT" => "Guatemala",
      "GG" => "Guernsey",
      "GN" => "Guinea",
      "GW" => "Guinea-bissau",
      "GY" => "Guyana",
      "HT" => "Haiti",
      "HM" => "Heard Island and Mcdonald Islands",
      "VA" => "Holy See (Vatican City State)",
      "HN" => "Honduras",
      "HK" => "Hong Kong",
      "HU" => "Hungary",
      "IS" => "Iceland",
      "IN" => "India",
      "ID" => "Indonesia",
      "IR" => "Iran, Islamic Republic of",
      "IQ" => "Iraq",
      "IE" => "Ireland",
      "IM" => "Isle of Man",
      "IL" => "Israel",
      "IT" => "Italy",
      "JM" => "Jamaica",
      "JP" => "Japan",
      "JE" => "Jersey",
      "JO" => "Jordan",
      "KZ" => "Kazakhstan",
      "KE" => "Kenya",
      "KI" => "Kiribati",
      "KP" => "Korea, Democratic People's Republic of",
      "KR" => "Korea, Republic of",
      "KW" => "Kuwait",
      "KG" => "Kyrgyzstan",
      "LA" => "Lao People's Democratic Republic",
      "LV" => "Latvia",
      "LB" => "Lebanon",
      "LS" => "Lesotho",
      "LR" => "Liberia",
      "LY" => "Libyan Arab Jamahiriya",
      "LI" => "Liechtenstein",
      "LT" => "Lithuania",
      "LU" => "Luxembourg",
      "MO" => "Macao",
      "MK" => "Macedonia, The Former Yugoslav Republic of",
      "MG" => "Madagascar",
      "MW" => "Malawi",
      "MY" => "Malaysia",
      "MV" => "Maldives",
      "ML" => "Mali",
      "MT" => "Malta",
      "MH" => "Marshall Islands",
      "MQ" => "Martinique",
      "MR" => "Mauritania",
      "MU" => "Mauritius",
      "YT" => "Mayotte",
      "MX" => "Mexico",
      "FM" => "Micronesia, Federated States of",
      "MD" => "Moldova, Republic of",
      "MC" => "Monaco",
      "MN" => "Mongolia",
      "ME" => "Montenegro",
      "MS" => "Montserrat",
      "MA" => "Morocco",
      "MZ" => "Mozambique",
      "MM" => "Myanmar",
      "NA" => "Namibia",
      "NR" => "Nauru",
      "NP" => "Nepal",
      "NL" => "Netherlands",
      "AN" => "Netherlands Antilles",
      "NC" => "New Caledonia",
      "NZ" => "New Zealand",
      "NI" => "Nicaragua",
      "NE" => "Niger",
      "NG" => "Nigeria",
      "NU" => "Niue",
      "NF" => "Norfolk Island",
      "MP" => "Northern Mariana Islands",
      "NO" => "Norway",
      "OM" => "Oman",
      "PK" => "Pakistan",
      "PW" => "Palau",
      "PS" => "Palestinian Territory, Occupied",
      "PA" => "Panama",
      "PG" => "Papua New Guinea",
      "PY" => "Paraguay",
      "PE" => "Peru",
      "PH" => "Philippines",
      "PN" => "Pitcairn",
      "PL" => "Poland",
      "PT" => "Portugal",
      "PR" => "Puerto Rico",
      "QA" => "Qatar",
      "RE" => "Reunion",
      "RO" => "Romania",
      "RU" => "Russian Federation",
      "RW" => "Rwanda",
      "SH" => "Saint Helena",
      "KN" => "Saint Kitts and Nevis",
      "LC" => "Saint Lucia",
      "PM" => "Saint Pierre and Miquelon",
      "VC" => "Saint Vincent and The Grenadines",
      "WS" => "Samoa",
      "SM" => "San Marino",
      "ST" => "Sao Tome and Principe",
      "SA" => "Saudi Arabia",
      "SN" => "Senegal",
      "RS" => "Serbia",
      "SC" => "Seychelles",
      "SL" => "Sierra Leone",
      "SG" => "Singapore",
      "SK" => "Slovakia",
      "SI" => "Slovenia",
      "SB" => "Solomon Islands",
      "SO" => "Somalia",
      "ZA" => "South Africa",
      "GS" => "South Georgia and The South Sandwich Islands",
      "ES" => "Spain",
      "LK" => "Sri Lanka",
      "SD" => "Sudan",
      "SR" => "Suriname",
      "SJ" => "Svalbard and Jan Mayen",
      "SZ" => "Swaziland",
      "SE" => "Sweden",
      "CH" => "Switzerland",
      "SY" => "Syrian Arab Republic",
      "TW" => "Taiwan, Province of China",
      "TJ" => "Tajikistan",
      "TZ" => "Tanzania, United Republic of",
      "TH" => "Thailand",
      "TL" => "Timor-leste",
      "TG" => "Togo",
      "TK" => "Tokelau",
      "TO" => "Tonga",
      "TT" => "Trinidad and Tobago",
      "TN" => "Tunisia",
      "TR" => "Turkey",
      "TM" => "Turkmenistan",
      "TC" => "Turks and Caicos Islands",
      "TV" => "Tuvalu",
      "UG" => "Uganda",
      "UA" => "Ukraine",
      "AE" => "United Arab Emirates",
      "GB" => "United Kingdom",
      "US" => "United States",
      "UM" => "United States Minor Outlying Islands",
      "UY" => "Uruguay",
      "UZ" => "Uzbekistan",
      "VU" => "Vanuatu",
      "VE" => "Venezuela",
      "VN" => "Viet Nam",
      "VG" => "Virgin Islands, British",
      "VI" => "Virgin Islands, U.S.",
      "WF" => "Wallis and Futuna",
      "EH" => "Western Sahara",
      "YE" => "Yemen",
      "ZM" => "Zambia",
      "ZW" => "Zimbabwe"
    );
    return $countries;
  }

  public static function quoteCheckStatus($quoteID)
  {
    $vendor_id = UtilityManager::getVendorID();
    $sql = "select * from quote_vendors where quote_id=" . $quoteID . " and vendor_id=" . $vendor_id;
    $vendorQuote = Yii::app()->db->createCommand($sql)->queryRow();

    $sql = "select * from quotes where quote_id=" . $quoteID;
    $quote = Yii::app()->db->createCommand($sql)->queryRow();

    if ($vendorQuote['submit_status'] == 1) {
      $status = 1;
    } else if (strtotime(date("Y-m-d H:i:s")) > strtotime($quote['closing_date'])) {
      $status = 2;
    } else if (strtotime(date("Y-m-d H:i:s")) <= strtotime($quote['opening_date'])) {
      $status = 3;
    } else {
      $status = 4;
    }
    return $status;
  }

  public static function userSettings($userID, $columnName)
  {

    $sql = "select " . $columnName . " from users where user_id='" . $userID . "'";
    $userReader = Yii::app()->db->createCommand($sql)->queryRow();
    if (!empty($userReader)) {
      return $userReader[$columnName];
    } else {
      return '';
    }
  }

  public static function superUserCheck()
  {
    $sql = "select count(*) as total_users from users where status=1 and user_type_id=4 and user_id !=1";
    $userReader = Yii::app()->db->createCommand($sql)->queryRow();
    if (!empty($userReader)) {
      return $userReader['total_users'];
    } else {
      return '';
    }
  }

  public static function getUserType($id)
  {
    $sql = "select value from user_types where id=" . $id;
    $userType = Yii::app()->db->createCommand($sql)->queryRow();
    if (!empty($userType)) {
      return $userType['value'];
    } else {
      return '';
    }
  }

  public function getNWordsFromString($str, $wordsNumber = 14)
  {
    error_reporting(0);
    $dom = new DomDocument();
    $dom->loadHTML($str);
    $output = array();
    foreach ($dom->getElementsByTagName('a') as $item) {
      $output[] = array(
        'str' => $dom->saveHTML($item),
        'href' => $item->getAttribute('href'),
        'anchorText' => $item->nodeValue
      );
    }
    $textArr = explode(" ", $output[0]['str']);
    $textNofitication = '';
    for ($i = 0; $i < $wordsNumber; $i++) {
      if (isset($textArr[$i])) {
        $textNofitication .= ' ' . $textArr[$i];
      }
    }
    if (count($textArr) >= $wordsNumber) {
      $textNofitication .= ' ...';
    }
    if ($str) {
      return "<a href='" . $output[0]['href'] . "'>" . $textNofitication . "</a>";
    } else {
      return '';
    }
  }

  public static function savingCalculationSave($savingID)
  {

    $sql = "SELECT 
      sum(cost_avoidance)+sum(cost_reduction) as total_project_saving,
      (sum(cost_avoidance)+sum(cost_reduction))/sum(base_line_spend) as total_project_saving_perc,
      (select max(due_date) from saving_milestone where saving_milestone.saving_id=" . $savingID . " limit 1 ) as latest_due_date 

      FROM saving_milestone m where m.saving_id=" . $savingID;

    $reader = Yii::app()->db->createCommand($sql)->queryRow();

    $sql = "SELECT 
      sum(realised_cost_avoidance)+sum(realised_cost_reduction) as total_realised_saving,
      (sum(realised_cost_avoidance)+sum(realised_cost_reduction))/sum(base_line_spend) as total_realised_saving_perc
      FROM saving_milestone m where m.status='Completed' and m.saving_id=" . $savingID;

    $readerRealized = Yii::app()->db->createCommand($sql)->queryRow();
    if (!empty($reader)) {
      $projectSaving = isset($reader['total_project_saving']) ? $reader['total_project_saving'] : 0;
      $projectSavingPer = isset($reader['total_project_saving_perc']) ? $reader['total_project_saving_perc'] : 0;
      $realisedSaving = isset($readerRealized['total_realised_saving']) ? $readerRealized['total_realised_saving'] : 0;
      $realisedSavingPer = isset($readerRealized['total_realised_saving_perc']) ? $readerRealized['total_realised_saving_perc'] : 0;
      $dueDate = isset($reader['latest_due_date']) ? $reader['latest_due_date'] : "0000-00-00 00:00:00";

      $createdAt = date("Y-m-d H:i:s");
      $saving = new Saving;
      $saving->rs = array();
      $saving->rs['id'] = $savingID;
      $saving->rs['total_project_saving'] = $projectSaving;
      $saving->rs['total_project_saving_perc'] = $projectSavingPer;
      $saving->rs['realised_saving'] = $realisedSaving;
      $saving->rs['realised_saving_perc'] = $realisedSavingPer;
      $saving->rs['due_date'] = $dueDate;
      $saving->rs['updated_at'] = $createdAt;
      $saving->write();
    }
  }

  public function dishboardLogo()
  {
    $campany_name = Yii::app()->session['company_name'];
    $sql = "select logo from company";
    $companyLogo = Yii::app()->db->createCommand($sql)->queryRow();
    if (!empty($companyLogo['logo'])) {
      $companyLogo = 'uploads/companies/' . $companyLogo['logo'];
    } else {
      $companyLogo = 'images/dashboard_logo.png';
    }
    return $companyLogo;
  }

  public function companyLogo()
  {
    // $sql = "select logo from company";
    // $companyLogo = Yii::app()->db->createCommand($sql)->queryRow();
    // if (!empty($companyLogo['logo'])) {
    $companyLogo = 'images/app_logo.png';
    // }else{
    // $companyLogo ='images/app_logo.png';
    // }
    return $companyLogo;
  }

  public static  function validateDate($date, $format = 'Y-m-d')
  {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
  }

  // excludeSubdomainName

  public static function sandbox()
  {
    $disabled = '';
    if (strpos(Yii::app()->getBaseUrl(true), "sandbox") && Yii::app()->session['admin_flag']  == "0") {
      $disabled = 'disabled';
    } else {
      $disabled = '';
    }
    return $disabled;
  }

  public static function checkEnvironment($checkUser = 0)
  {
    if (strpos(Yii::app()->getBaseUrl(true), "sandbox") && $checkUser) {
      if (Yii::app()->session['admin_flag']  == "0") {
        return false;
      } else {
        return true;
      }
    } else if (strpos(Yii::app()->getBaseUrl(true), "sandbox")) {
      return false;
    } else {
      return true;
    }
  }

  public static function environmentUrl()
  {
    if (self::checkEnvironment()) {
      if (strpos(Yii::app()->getBaseUrl(true), "devoboloo")) {
        return "//sandbox.devoboloo.com/login";
      } else {
        return "//sandbox.oboloo.software/login";
      }
    } else {
      return Yii::app()->session['live_subdomain'] . "/home";
    }
  }

  public static function currentDomain()
  {
    return Yii::app()->request->hostinfo;
  }

  public static function userLanguage($userID)
  {
    if (!empty(Yii::app()->session['default_language_code'])) {
      $code = Yii::app()->session['default_language_code'];
    } else {
      $sql = "select language_code from users where user_id=" . $userID;
      $userReader = Yii::app()->db->createCommand($sql)->queryRow();
      if (!empty($userReader['language_code'])) {
        $code = $userReader['language_code'];
      } else {
        $code = 'en';
      }
    }
    return $code;
  }

  public static function savingStatusIgnore()
  {
    return 4;
  }

  public static function savingSpecific()
  {
    $domains = self::subdomains();
    $url = Yii::app()->getBaseUrl(true);
    $parsedUrl = parse_url($url);
    $host = explode('.', $parsedUrl['host']);
    $subdomain = $host[0];
  
    if (in_array($subdomain, $domains)) {
      if (!empty(self::subdomains(1)) && self::subdomains(1) == $subdomain) {
        $subdomain = self::subdomains(2);
      }
      return $subdomain;
    } else {
      return 0;
    }
    //$db = getDatabaseForSubdomain(APP_SUBDOMAIN_URL);
  }

  public static function subdomains($key = '')
  {
    $domains = array(
      //1=>"localhost",
      2 => "usg",
      3 => "resco"
    );

    if (!empty($key)) {
      return $domains[$key];
    } else {
      return $domains;
    }
  }

  public static function homeController($key = '')
  {
    $defaultArray = array('usg' => 'savings');
    if (!empty($key)) {
      return $defaultArray[$key];
    } else {
      return 'app';
    }
  }
  public static function savingType($key = '')
  {
    $status = array(1 => 'Cost Reduction', 2 => 'Cost Avoidance ', 3 => 'Cost Containment');
    if (!empty($key)) {
      return $status[$key];
    } else
      return $status;
  }

  public static function idForLocationPermission()
  {
    return array(1, 2);
  }

  // This function return saveing new to incremental Saving 
  public static function newOrIncrementalSavings($key = '')
  {
    $newOrIncreSavings = [
      'New' => 'New',
      'Incr' => 'Incr'
    ];

    if (!empty($key)) {
      return $newOrIncreSavings[$key];
    } else
      return $newOrIncreSavings;
  }

  public static function subdomainByUrl()
  {
    $domain = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
    //$domain = "https://usg.oboloo.software/home";
    $segments = explode('.', $domain);
    // If no subdomain exist
    if (count($segments) <= 2) {
      return 'general';
    }
    // else sanitize and return
    $subdomain = str_replace("http://", "", $segments[0]);
    $subdomain = str_replace("https://", "", $subdomain);
    return $subdomain;
  }

  public static function savingCalculationSaveWhenMilestoneEdit($savingID)
  {
    $sql = "SELECT 
      sum(cost_avoidance)+sum(cost_reduction) as total_project_saving,
      (sum(cost_avoidance)+sum(cost_reduction))/sum(base_line_spend) as total_project_saving_perc,
      (select max(due_date) from saving_milestone where saving_milestone.saving_id=" . $savingID . " limit 1 ) as latest_due_date 

      FROM saving_milestone m where m.saving_id=" . $savingID;

    $reader = Yii::app()->db->createCommand($sql)->queryRow();
    //echo "<pre>"; print_r($reader); echo "</pre>"; exit;
    $sql = "SELECT 
      sum(realised_cost_avoidance)+sum(realised_cost_reduction) as total_realised_saving,
      (sum(realised_cost_avoidance)+sum(realised_cost_reduction))/sum(base_line_spend) as total_realised_saving_perc
      FROM saving_milestone m where m.status='Completed' and m.saving_id=" . $savingID;

    $readerRealized = Yii::app()->db->createCommand($sql)->queryRow();
    if (!empty($reader)) {

      $projectSaving = isset($reader['total_project_saving']) ? $reader['total_project_saving'] : 0;
      $projectSavingPer = isset($reader['total_project_saving_perc']) ? $reader['total_project_saving_perc'] : 0;
      $realisedSaving = isset($readerRealized['total_realised_saving']) ? $readerRealized['total_realised_saving'] : 0;
      $realisedSavingPer = isset($readerRealized['total_realised_saving_perc']) ? $readerRealized['total_realised_saving_perc'] : 0;
      $dueDate = isset($reader['latest_due_date']) ? $reader['latest_due_date'] : "0000-00-00 00:00:00";
      $createdAt = date("Y-m-d H:i:s");

      $saving = new Saving;
      $saving->rs = array();
      $saving->rs['id'] = $savingID;
      $saving->rs['total_project_saving'] = $projectSaving;
      $saving->rs['total_project_saving_perc'] = $projectSavingPer;
      $saving->rs['realised_saving'] = $realisedSaving;
      $saving->rs['realised_saving_perc'] = $realisedSavingPer;
      $saving->rs['due_date'] = $dueDate;
      $saving->rs['updated_at'] = $createdAt;
      $saving->write();

      $oldRecord = $comment = '';
      if (!empty($savingID)) {
        $savingCommentExit = new Saving;
        $oldRecord = $savingCommentExit->getOne(array('id' => $savingID));
      }

      if (!empty($oldRecord['id'])) {

        if (strcasecmp($oldRecord['total_project_saving'], $projectSaving) != 0) {
          $comment .= '<strong>Total Projected Savings:</strong> <span class="title-text"><b>' . $oldRecord['total_project_saving'] .
            ' </b>changed to <b>' . $projectSaving . '</b></span><br/>';
        }

        if (strcasecmp($oldRecord['total_project_saving_perc'], $projectSavingPer) != 0) {
          $comment .= 'Total Projected Savings %:</strong> <span class="title-text"><b>' .
            $oldRecord['total_project_saving_perc'] . ' </b>changed to <b>' . $projectSavingPer . '</b></span><br/>';
        }

        if (strcasecmp($oldRecord['realised_saving'], $realisedSaving) != 0) {
          $comment .= '<strong>Total Realised Savings %:</strong> <span class="title-text"><b>' .
            $oldRecord['realised_saving'] . ' </b>changed to <b>' . $realisedSaving . '</b></span><br/>';
        }

        if (strcasecmp($oldRecord['realised_saving_perc'], $realisedSavingPer) != 0) {
          $comment .= '<strong>Total Realised Savings:</strong> <span class="title-text"><b>' .
            $oldRecord['realised_saving_perc'] . ' </b>changed to <b>' . $realisedSavingPer . '</b></span><br/>';
        }

        if (!empty($comment)) {
          $log = new SavingLog();
          $log->rs = array();
          $log->rs['saving_id'] = $savingID;
          $log->rs['user_id'] = Yii::app()->session['user_id'];
          $log->rs['comment'] = !empty($comment) ? $comment : "";
          $log->rs['created_datetime'] = date('Y-m-d H:i:s');
          $log->rs['updated_datetime'] = date('Y-m-d H:i:s');
          $log->write();
        }
      }
    }
  }
  public static function TotalForecasted($saving_id = 0)
  {
    $sql = "select 
      sum(currencyConversion(field_value,s.currency_rate,currency_id,'" . $currency . "'))
      as total_forecasted_saving 
      from savings as s inner join milestone_field f on  s.id=f.saving_id where s.status not in(4) and saving_id=" . $saving_id;
    $totalForecasted = Yii::app()->db->createCommand($sql)->queryRow();
    return $totalForecasted['total_forecasted_saving'];
  }
  public static function getCrtburelineDate($quoteID, $userID)
  {
    if (!empty($quoteID) && !empty($userID)) {
      $sql = "select contribute_deadline from quote_user where quote_id=" . $quoteID . " and user_id=" . $userID . " group by quote_id ";
    } else {
      $sql = "select contribute_deadline from quote_user where quote_id=" . $quoteID . " group by quote_id ";
    }
    $res = Yii::app()->db->createCommand($sql)->queryRow();
    return date(self::dateFormat() . " H:iA", strtotime($res['contribute_deadline']));
  }

  public static function CheckContriddlineExprie($quoteID, $userID)
  {
    if (!empty($quoteID)) {
      $sql = "select contribute_deadline from quote_user where quote_id=" . $quoteID . " and user_id=" . $userID . " group by quote_id ";
      $res = Yii::app()->db->createCommand($sql)->queryRow();
      return date("Y-m-d H:i:s", strtotime($res['contribute_deadline']));
    }
  }

  public static function scoreValueMaximum()
  {
    return 5;
  }
  
  public static function getRandomColor()
  {
    do {
        $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    } while (self::isColorNearWhite($color));
    
    return $color;
  }

  public static function isColorNearWhite($color) {
    $r = hexdec(substr($color, 1, 2));
    $g = hexdec(substr($color, 3, 2));
    $b = hexdec(substr($color, 5, 2));
    return $r > 200 && $g > 200 && $b > 200;
  }

  public static function contractDocumentTable($key = '')
  {

    $sql = "select name,id from contract_document_type where created_type ='General' order by name ";
    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $document = array();
    foreach ($reader as $value) {
      $document[$value['id']] = $value['name'];
    }

    if (!empty($key)) {
      $sql = "select name,id from contract_document_type where id='" . $key . "'  order by name ";
      $reader = Yii::app()->db->createCommand($sql)->queryRow();
      return $reader['name'];
    }
    return $document;
  }

  public static function vendorDocumentTable($key = '')
  {
    $sql = "select name,id from vendor_document_type where created_type ='General' order by name";
    $reader = Yii::app()->db->createCommand($sql)->query()->readAll();
    $document = array();
    foreach ($reader as $value) {
      $document[$value['id']] = $value['name'];
    }

    if (!empty($key)) {
      $sql = "select name,id from vendor_document_type where id='" . $key . "' order by name ";
      $reader = Yii::app()->db->createCommand($sql)->queryRow();
      return $reader['name'];
    }
    return $document;
  }
}
