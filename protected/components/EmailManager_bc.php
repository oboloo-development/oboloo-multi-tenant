<?php
class EmailManager {

    public static function userPasswordChangeReuest($email,$username) {
      
        $user_id = 0;
        $sql = "select * from users where email='".$email."'";
        $user_data = Yii::app()->db->createCommand($sql)->queryRow();

        if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
            $user_id = $user_data['user_id'];
        
        if (!$user_id)
        {
            $sql = "select * from users where username='".$username."'";
            $user_data = Yii::app()->db->createCommand($sql)->queryRow();

            if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
                $user_id = $user_data['user_id'];
        }
        
        if (!$user_id) return 1;
        else
        {
            $new_password = self::generateRandomString(10);
           // $this->executeQuery("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id");                   

            $mail = new \PHPMailer(true);                   
            $mail->IsSMTP();
            $mail->Host = Yii::app()->params['host'];
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->Username = Yii::app()->params['username'];
            $mail->Password = Yii::app()->params['password'];
            $mail->SMTPSecure = 'tls';

            $email_body = '';

            $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="">';
            $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.jpg') . '" style=""border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="259" />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <span style="font-size:22px;"><font color="#7f8c8d">Request for new password&nbsp;on</font>&nbsp;<strong><span style="color:#2d9ca2;">Oboloo</span></strong></span>
             </div>
         </td>
         </tr>
         <tr><td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;"><br /><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">This is a notification from </span><span style="color:#16a085;"><strong>Oboloo</strong></span><span style="color:black">. Please follow the link below to change your password:</span></span></span></span></div></td>
         </tr>

         <tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
            $email_body .= '';
            $email_body .= '<strong><span style="color:#16a085;">Username: </span></strong>' . $user_data['username'] . '<br/><br/>';
            $email_body .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $user_data['email'] . '<br/><br/>';
            $email_body .= '<strong><span style="color:#16a085;">Change Password Link: </span></strong><a href="' .Yii::app()->createAbsoluteUrl('app/changePasswordSave',array("user-id"=>base64_encode($user_id),'time'=>base64_encode(date("m-d"))))  . '" title="Change Password">'.Yii::app()->createAbsoluteUrl('app/changePasswordSave',array("user-id"=>base64_encode($user_id),'time'=>base64_encode(date("m-d")))).'</a></td>
         </tr>';
            $email_body .= ' <tr><td align="center" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;">        <p><strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:#c0392b;">NOTE:</span><span style="color:black"> </span></span></span></strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">If you did not request this change, please contact your administrator immediately.</span></span></span></p></div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.2;text-align:left;color:#2D9CA2;">        <p lang="x-size-17"><span style="font-size:14px;">Thanks,<br><strong>The Oboloo Team</strong><br><em>Support -&nbsp;<strong>hello@oboloo.com</strong></em></span></p><p lang="x-size-17"></p><p lang="x-size-10"><a href="https://oboloo.com/privacy-policy"><span style="font-size:14px;">Privacy Policy</span></a></p><p lang="x-size-10"><a href="http://oboloo.com/Terms"><span style="font-size:14px;">Terms</span></a></p><table emb-web-links="" role="presentation"> <tbody>   <tr role="navigation"><td emb-web-links=""></td><td emb-web-links=""></td>      <td emb-web-links=""></td>    </tr> </tbody></table><p></p><p></p></div></td></tr>
         <tr>              <td align="center" style="font-size:0px;padding:10px 10px 10px 10px;word-break:break-word;">                           <!--[if mso | IE]>      <table         align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"      >        <tr>                    <td>            <![endif]-->              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">                      <tbody><tr>        <td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;">            <tbody><tr>              <td style="font-size:0;height:35px;vertical-align:middle;width:35px;">                <a href="www.facebook.com/oboloosoftware" target="_blank">                    <img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/facebook.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td>              </tr>          </tbody></table>        </td>              </tr>                  </tbody></table>            <!--[if mso | IE]>              </td>                          <td>            <![endif]-->              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">                      <tbody><tr>        <td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;">            <tbody><tr>              <td style="font-size:0;height:35px;vertical-align:middle;width:35px;">                <a href="https://twitter.com/oboloosoftware" target="_blank">                    <img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/twitter.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td>              </tr>          </tbody></table>        </td>              </tr>                  </tbody></table>            <!--[if mso | IE]>              </td>                          <td>            <![endif]-->              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">                      <tbody><tr>        <td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;">            <tbody><tr>              <td style="font-size:0;height:35px;vertical-align:middle;width:35px;">                <a href="https://www.linkedin.com/company/oboloo" target="_blank">                    <img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/linkedin.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td>              </tr>          </tbody></table>        </td>              </tr>                  </tbody></table>            <!--[if mso | IE]>              </td>                      </tr>        </table>      <![endif]-->                      </td>            </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';   
            $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
            $mail->addAddress($user_data['email'], $user_data['full_name']);
            $mail->Subject = 'Oboloo: Password Change';
            $mail->isHTML(true);
            $mail->Body = $email_body;          
            $mail->send();
            
            return 0;
        }
    
    }
  public static function userPasswordChanged($email,$username) {
      
        $user_id = 0;
        $sql = "select * from users where email='".$email."'";
        $user_data = Yii::app()->db->createCommand($sql)->queryRow();

        if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
            $user_id = $user_data['user_id'];
        
        if (!$user_id)
        {
            $sql = "select * from users where username='".$username."'";
            $user_data = Yii::app()->db->createCommand($sql)->queryRow();

            if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
                $user_id = $user_data['user_id'];
        }
        
        if (!$user_id) return 1;
        else
        {
            $new_password = self::generateRandomString(10);
           // $this->executeQuery("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id");                   

            $mail = new \PHPMailer(true);                   
            $mail->IsSMTP();
            $mail->Host = Yii::app()->params['host'];
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->Username = Yii::app()->params['username'];
            $mail->Password = Yii::app()->params['password'];
            $mail->SMTPSecure = 'tls';

            $email_body = '';

            $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="">';
            $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.jpg') . '" style=""border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="259" />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <span style="font-size:22px;"><font color="#7f8c8d">Your password has been changed on </font>&nbsp;<strong><span style="color:#2d9ca2;">Oboloo</span></strong></span>
             </div>
         </td>
         </tr>
         <tr><td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;"><br /><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">This is a notification from Oboloo to let you know that your password has changed.</span></span></span></span></div></td>
         </tr>';

       
            $email_body .= ' <tr><td align="center" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;">        <p><strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:#c0392b;">NOTE:</span><span style="color:black"> </span></span></span></strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">If you did not request this change, please contact your administrator immediately.</span></span></span></p></div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.2;text-align:left;color:#2D9CA2;">        <p lang="x-size-17"><span style="font-size:14px;">Thanks,<br><strong>The Oboloo Team</strong><br><em>Support -&nbsp;<strong>hello@oboloo.com</strong></em></span></p><p lang="x-size-17"></p><p lang="x-size-10"><a href="https://oboloo.com/privacy-policy"><span style="font-size:14px;">Privacy Policy</span></a></p><p lang="x-size-10"><a href="http://oboloo.com/Terms"><span style="font-size:14px;">Terms</span></a></p><table emb-web-links="" role="presentation"> <tbody>   <tr role="navigation"><td emb-web-links=""></td><td emb-web-links=""></td>      <td emb-web-links=""></td>    </tr> </tbody></table><p></p><p></p></div></td></tr>
         <tr>              <td align="center" style="font-size:0px;padding:10px 10px 10px 10px;word-break:break-word;">                           <!--[if mso | IE]>      <table         align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"      >        <tr>                    <td>            <![endif]-->              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">                      <tbody><tr>        <td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;">            <tbody><tr>              <td style="font-size:0;height:35px;vertical-align:middle;width:35px;">                <a href="www.facebook.com/oboloosoftware" target="_blank">                    <img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/facebook.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td>              </tr>          </tbody></table>        </td>              </tr>                  </tbody></table>            <!--[if mso | IE]>              </td>                          <td>            <![endif]-->              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">                      <tbody><tr>        <td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;">            <tbody><tr>              <td style="font-size:0;height:35px;vertical-align:middle;width:35px;">                <a href="https://twitter.com/oboloosoftware" target="_blank">                    <img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/twitter.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td>              </tr>          </tbody></table>        </td>              </tr>                  </tbody></table>            <!--[if mso | IE]>              </td>                          <td>            <![endif]-->              <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">                      <tbody><tr>        <td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;">            <tbody><tr>              <td style="font-size:0;height:35px;vertical-align:middle;width:35px;">                <a href="https://www.linkedin.com/company/oboloo" target="_blank">                    <img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/linkedin.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td>              </tr>          </tbody></table>        </td>              </tr>                  </tbody></table>            <!--[if mso | IE]>              </td>                      </tr>        </table>      <![endif]-->                      </td>            </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';   
            $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
            $mail->addAddress($user_data['email'], $user_data['full_name']);
            $mail->Subject = 'Oboloo: Password Change';
            $mail->isHTML(true);
            $mail->Body = $email_body;          
            $mail->send();
            
            return 0;
        }
    
    }

  public static function generateRandomString($length = 10) 
  {
      $characters = '0123456789%@&*abcdefghijklmnop!~qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++)
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      return $randomString;
  }

  public static function userPasswordForgotReuest($email,$username) {
        $user_id = 0;
        $sql = "select * from users where email='".$email."'";
        $user_data = Yii::app()->db->createCommand($sql)->queryRow();
        if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
            $user_id = $user_data['user_id'];
        if (!$user_id)
        {
            $sql = "select * from users where username='".$username."'";
            $user_data = Yii::app()->db->createCommand($sql)->queryRow();
            if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
                $user_id = $user_data['user_id'];
        }
        if (!$user_id) return 1;
        else
        {
            $new_password = self::generateRandomString(10);
           // $this->executeQuery("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id");                   
            $mail = new \PHPMailer(true);                   
            $mail->IsSMTP();
            $mail->Host = Yii::app()->params['host'];
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->Username = Yii::app()->params['username'];
            $mail->Password = Yii::app()->params['password'];
            $mail->SMTPSecure = 'tls';
            $emailBody = '';
            $header1 = '<font color="#7f8c8d">Forgotten password &nbsp;on</font>&nbsp;<strong><span style="color:#2d9ca2;">Oboloo</span></strong>';
            $header2 = '<span style="color:black">This is a notification from </span><span style="color:#16a085;"><strong>Oboloo</strong></span><span style="color:black">. As per your request, your password has been reset to the below:</span>';
            $emailBody  .= self::emailHeader($header1,$header2);
            $emailBody  .='<tr>
             <td style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;"><br />';
            $emailBody .= '';
            $emailBody .= '<strong><span style="color:#16a085;">Username: </span></strong>' . $user_data['username'] . '<br/><br/>';
            $emailBody .= '<strong><span style="color:#16a085;">Email: </span></strong>' . $user_data['email'] . '<br/><br/>';
            $emailBody .= '<strong><span style="color:#16a085;">Password: </span></strong>'.$new_password.'</td>
         </tr>';
          $emailBody .=self::emailFooter();  
          $mail->setFrom(Yii::app()->params['email'], 'Oboloo Admin');
          $mail->addAddress($user_data['email'], $user_data['full_name']);
          $mail->Subject = 'Oboloo: Password Change';
          $mail->isHTML(true);
          $mail->Body = $emailBody;          
          $mail->send();
          
          return 0;
        }
    
    }	

    public static function emailHeader($header1,$header2){
      $bodyHtml='<div class="mj-container" style="background-color:#FFFFFF;">
     <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="">';
            $bodyHtml .= '<img src="' . AppUrl::bicesUrl('images/email_banner.jpg') . '" style=""border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="259" />';
            $bodyHtml .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <span style="font-size:22px;">'.$header1.'</span>
             </div>
         </td>
         </tr>
         <tr><td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;"><br /><span style="font-family:" tahoma",sans-serif"="">'.$header2.'</span></span></span></div></td>
         </tr>';
      return $bodyHtml;
    }

    public static function emailFooter($footerText=''){
      $bodyHtml=' <tr><td align="center" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"><div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;color:#000000;"><p><strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:#c0392b;">NOTE:</span><span style="color:black"> </span></span></span></strong><span style="font-size:8.5pt"><span style="font-family:" tahoma",sans-serif"=""><span style="color:black">If you did not request this change, please contact your administrator immediately.</span></span></span></p></div></td>
         </tr>
         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>
         <tr><td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;"> <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.2;text-align:left;color:#2D9CA2;">        <p lang="x-size-17"><span style="font-size:14px;">Thanks,<br><strong>The Oboloo Team</strong><br><em>Support -&nbsp;<strong>hello@oboloo.com</strong></em></span></p><p lang="x-size-17"></p><p lang="x-size-10"><a href="https://oboloo.com/privacy-policy"><span style="font-size:14px;">Privacy Policy</span></a></p><p lang="x-size-10"><a href="http://oboloo.com/Terms"><span style="font-size:14px;">Terms</span></a></p><table emb-web-links="" role="presentation"> <tbody>   <tr role="navigation"><td emb-web-links=""></td><td emb-web-links=""></td>      <td emb-web-links=""></td>    </tr> </tbody></table><p></p><p></p></div></td></tr>
         <tr><td align="center" style="font-size:0px;padding:10px 10px 10px 10px;word-break:break-word;"><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"      >        <tr><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;">          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;"><tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;"><a href="www.facebook.com/oboloosoftware" target="_blank"><img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/facebook.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td></tr></tbody></table></td></tr></tbody></table></td><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;"><tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;"><a href="https://twitter.com/oboloosoftware" target="_blank"><img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/twitter.png" style="border-radius:3px;display:block;" width="35">                  </a>                </td></tr></tbody></table></td></tr></tbody></table></td><td><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"><tbody><tr><td style="padding:4px;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:35px;">            <tbody><tr><td style="font-size:0;height:35px;vertical-align:middle;width:35px;"><a href="https://www.linkedin.com/company/oboloo" target="_blank"><img height="35" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/linkedin.png" style="border-radius:3px;display:block;" width="35"></a>                </td></tr></tbody></table></td></tr></tbody></table></td></tr></table></td></tr>
         </tbody></table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';
      return $bodyHtml;
    } 						
}