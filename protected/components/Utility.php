<?php  if ( ! defined('YII_PATH')) exit('No direct script access allowed');

/**
 * This class defines common/utility methods used throughout the CIVICgov application.
 */

class Utility 
{
	private static $salt = "b60f8797eb";
	
	const STAFF_USER = 1;
	const CONTRACTOR_USER = 2;
	const PUBLIC_USER = 3;
	const ADMIN_USER = 4;
	const CONTAINER_ADMIN_USER = 5;
	const INSPECTOR_USER = 6;
	const CLERK_USER = 7;
	const SUPERVISOR_USER = 8;
	const SUPERVISOR_INSPECTOR_USER = 9;
	const BOARD_MEMBER_USER = 10;
	
	const AUTO_GENERATE_FOR_NUMBER = "0.0.0.0";
	
	const PDF_PAGE_BREAK = '<div id="filler_page_break"> &nbsp; </div>';
    
    private static $_header_height = 0;
	
	/**
	 * \param $input - string to be encoded - mandatory
	 * \return $output - string in an encoded format
	 * 
 	 * This method is used to encrypt the input string using certain algorithm.
 	 */
    public static function encrypt($input)
	{
        return strrev(base64_encode(self::$salt . '~' . $input . '~' . self::$salt));
    }
    
	/**
	 * \param $input - string to be decoded - mandatory
	 * \return $output - string in an decoded format
	 * 
 	 * This method is used to decrypt the input string (which was encoded using the encrypt function above)
 	 */
    public static function decrypt($input)
	{
        return str_replace("~" . self::$salt, "", str_replace(self::$salt . "~", "", base64_decode(strrev($input))));
    }

	/**
	 * \return $output - array containing table names used for drop downs
	 * 
 	 * This method returns an array containing table names which are used for various drop down values in the application.
 	 */
	public static function getCodeTypes()
	{
		return array('building_categories', 'business_types', 'certificate_types', 'complaint_types', 'complaint_statuses', 
				'contact_types', 'current_zoning_types', 'departments', 'documentation_items', 'electrical_items', 'evacuation_items', 
				'exterior_items', 'fee_types', 'fire_alarm_systems', 'fire_sprinkler_systems', 'foundation_materials', 'foundation_types', 
				'heating_items', 'inspection_types', 'insurance_types', 'interior_items', 'legal_structures', 'other_categories', 
				'payment_methods', 'permit_types', 'permit_uses', 'suppression_extinguishers', 'structure_types', 'zoning_permit_types', 
				'zoning_proposed_uses', 'zoning_proposed_structures', 'zoning_structure_types', 'violation_action_types');		
	}

	public static function email($to, $subject, $message, $from_email = "", $from = "", $attachments = array(), $additional_arguments = array())
	{
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') return;

        // area/container from where the email is being sent
        $area = "";
        $matches = array();
        preg_match("/dbname=([^;]*)/", Yii::app()->db->connectionString, $matches);
		if (isset($matches[1])) $area = $matches[1];
		
		// tracking arguments?
		$civicgov_arguments = array();
		$civicgov_arguments['subject'] = $subject;
		if (!empty($area)) $civicgov_arguments['area'] = $area;
		$civicgov_arguments = array_merge($civicgov_arguments, $additional_arguments);
        
		require_once 'protected/vendors/sendgrid-php/sendgrid-php.php';
		if (empty($from_email)) $from_email = Yii::app()->params['adminEmail'];
		if (empty($from)) $from = Yii::app()->params['adminName'];
		$message = '<html><body>' . $message . '</body></html>';

		$sendgrid = new SendGrid("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");
		$email    = new SendGrid\Email();

		$email->addTo($to)
		      ->setFrom($from_email)
		      ->setReplyTo($from_email)
		      ->setFromName($from)
		      ->setSubject($subject)
		      ->setHtml($message)
		      ->setUniqueArguments($civicgov_arguments);

      	// Add attachments
      	if (!empty($attachments) && is_array($attachments)) {
      		foreach ($attachments as $attachment) {
      			if (file_exists($attachment)) {
      				$email->addAttachment($attachment);	
  				}
      		}
      	}

		try {
		    $sendgrid->send($email);
		    $result = "success";
		} catch(\SendGrid\Exception $e) {
		    // Uncomment following to print the exception
		    /*echo $e->getCode();
		    foreach($e->getErrors() as $er) {
		        echo $er;
		    }*/
		    $result = "fail";
		}
		return $result;
	}

	/**
	 * \param $invoice_no - invoice number
	 * \param $invoice_amt - invoice amount upto 2 decimal places eg. 20.00
	 * \param $lockbox_number - optional lockbox number defaults to 9999
	 * \param $customer_number - customer number - optional (default: blank i.e. CIVICgov Administrator)
	 * \param $check_digit - name of the sender - optional (default: blank i.e. CIVICgov Administrator)
	 *
	 * This method is used to generate OCR based on $invoice_no and $invoice_amt.
	 * Example usage is as follows:
	 * echo Utility::printocr(99236651,20.00);
	 */
	public static function printocr($invoice_no,$invoice_amt,$lockbox_number = "9999",$customer_number = "18760",$check_digit = "7")
 	{
 		$customer_number = sprintf("%'010s", $customer_number);
 		$invoice_no = sprintf("%'010s", $invoice_no);
 		$invoice_amt = sprintf("%'010s", $invoice_amt*100);

 		$html = "<style> @font-face{ font-family: 'OCRAExtendedRegular'; src: url(".AppUrl::cssUrl('ocraextended.ttf')."); } </style>";
 		$html.= "<style> .ocra{ font-family: 'OCRAExtendedRegular'; font-weight: bold;font-size:18px; } </style>";
 		$html.= "<p class='ocra'>".$lockbox_number.$customer_number.$invoice_no.$invoice_amt.$check_digit."</p>";
 		
 		return $html;
 	}


	/**
	 * \param $to - email address of the recipient - mandatory
	 * \param $subject - subject for the email message - mandatory
	 * \param $message - actual email message to be sent - mandatory
	 * \param $from_email - email address of the sender - optional (default: blank i.e. CIVICgov Administrator)
	 * \param $from - name of the sender - optional (default: blank i.e. CIVICgov Administrator)
	 * 
 	 * This method is used to send HTML email as per input parameters.
 	 */
	public static function oldemail($to, $subject, $message, $from_email = "", $from = "", $extra_headers = "")
	{
		if (empty($from_email)) $from_email = Yii::app()->params['adminEmail'];
		if (empty($from)) $from = Yii::app()->params['adminName'];

		$headers  = "From: $from <$from_email>" . "\r\n";
		$headers .= "Return-Path: $from_email" . "\r\n";
		$headers .= "Reply-To: $from_email" . "\r\n";
        if ($extra_headers) $headers .= $extra_headers . "\r\n";
    	$headers .= "Content-type: text/html\r\n"; 		
		mail($to, $subject, '<html><body>' . $message . '</body></html>', $headers); 				
	}

	/**
	 * \return $admin_logged_in - whether the logged in user is an administrator or not (true/false)
	 * 
 	 * This method checks if the logged in user is an administrator or not
 	 */
	public static function isAdminUserLoggedIn()
	{
		return Yii::app()->session['user_type'] == Utility::ADMIN_USER
			|| Yii::app()->session['user_type'] == Utility::CONTAINER_ADMIN_USER;
	}

	public static function isPublicUserLoggedIn()
	{
		return Yii::app()->session['user_type'] == Utility::PUBLIC_USER
			|| Yii::app()->session['user_type'] == Utility::CONTRACTOR_USER;
	}

    public static function isReadOnlyUserLoggedIn()
    {
        return isset(Yii::app()->session['read_only_flag']) ? Yii::app()->session['read_only_flag'] : 0;
    }

    public static function isGISEnabled()
    {
        return Yii::app()->session['gis_map_display'] == 1 &&
                is_dir(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'maps');
    }

	public static function getFormattedTime($datetime = false)
	{
		if (!$datetime)
		{
			$current_date = date("m/d/Y");
			$current_hour = date("H");
			$current_minutes = intval(date("i"));
            
            if (isset(Yii::app()->session['default_inspection_length']) && !empty(Yii::app()->session['default_inspection_length']))
            {
                $default_inspection_length = intval(Yii::app()->session['default_inspection_length']);
                if ($default_inspection_length <= 0) $default_inspection_length = 60;
                $next_timestamp = strtotime("+$default_inspection_length Minutes");
            }
            else $next_timestamp = strtotime("+1 Hour");
		}
		else
		{
			$current_date = date("m/d/Y", strtotime($datetime));
			$current_hour = date("H", strtotime($datetime));
			$current_minutes = intval(date("i", strtotime($datetime)));

            if (isset(Yii::app()->session['default_inspection_length']) && !empty(Yii::app()->session['default_inspection_length']))
            {
                $default_inspection_length = intval(Yii::app()->session['default_inspection_length']);
                if ($default_inspection_length <= 0) $default_inspection_length = 60;
                $next_timestamp = strtotime("+$default_inspection_length Minutes", strtotime($datetime));
            }
            else $next_timestamp = strtotime("+1 Hour", strtotime($datetime));
		}
        
		$ampm = "AM";
		if ($current_hour > 12)
		{
			$current_hour -= 12;
			$ampm = "PM";
		}
		else if ($current_hour == 12) $ampm = 'PM';
		
		if (intval($current_hour) == 0)
		{
			$current_hour = 12;
			$ampm = "AM";
		}
		
        if ($current_minutes <= 4) $current_minutes = 0;
        if ($current_minutes >= 5 && $current_minutes <= 9) $current_minutes = 5;
		if ($current_minutes > 55) $current_minutes = 55;
		if ($current_minutes < 55) 
		{
			if ($current_minutes >= 5 && $current_minutes % 5 != 0) $current_minutes += (5 - ($current_minutes % 5));
		}

        $next_date = date("m/d/Y", $next_timestamp);
        $next_hour = date("H", $next_timestamp);
        $next_minutes = intval(date("i", $next_timestamp));

		$next_ampm = "AM";
		if ($next_hour > 12)
		{
			$next_hour -= 12;
			$next_ampm = "PM";
		}
		else if ($next_hour == 12) $next_ampm = 'PM';
		
		if (intval($next_hour) == 0)
		{
			$next_hour = 12;
			$next_ampm = "AM";
		}
		
        if ($next_minutes <= 4) $next_minutes = 0;
        if ($next_minutes >= 5 && $next_minutes <= 9) $next_minutes = 5;
		if ($next_minutes > 55) $next_minutes = 55;
		if ($next_minutes < 55) 
		{
			if ($next_minutes >= 5 && $next_minutes % 5 != 0) $next_minutes += (5 - ($next_minutes % 5));
		}
        
		return array('date' => $current_date, 'hour' => $current_hour, 'minute' => $current_minutes, 'ampm' => $ampm,
                        'next_date' => $next_date, 'next_hour' => $next_hour, 'next_minute' => $next_minutes, 'next_ampm' => $next_ampm);
	}
	
	public static function getDatabaseFormattedDate($date, $hour, $minutes, $ampm)
	{
		if ($ampm == 'PM' && $hour < 12) $hour += 12;
		if ($ampm == 'AM' && $hour == 12) $hour = 0;
		return date("Y-m-d", strtotime($date)) . ' ' . sprintf("%02d", $hour) . ':' . sprintf("%02d", $minutes) . ':00';
	}
	
	/**
	 * \param $user_type - which number is to be "converted" to string user type display - optional
	 * \return $user_type_string - String showing type of user logged in as per input user type variable
	 * 
 	 * This method converts the input user type (or if the input user type is empty then
	 * session usertype for the logged in user) into a string as indicated by appropriate
	 * member variables. So e.g. 4 means system administrator while 2 means contractor etc
 	 */
	public static function getUserTypeDisplay($user_type = "")
	{
		if (empty($user_type)) 
			$user_type = isset(Yii::app()->session['user_type']) ? Yii::app()->session['user_type'] : '';
		
		switch ($user_type)
		{
			case self::STAFF_USER : return 'Staff';
			case self::CONTRACTOR_USER : return 'Contractor';
			case self::PUBLIC_USER : return 'Public';
			case self::ADMIN_USER : return 'Sys Admin';
			case self::CONTAINER_ADMIN_USER : return 'Container Admin';
			case self::INSPECTOR_USER : return 'Inspector';
			case self::CLERK_USER : return 'Clerk';
			case self::SUPERVISOR_USER : return 'Supervisor';
			case self::SUPERVISOR_INSPECTOR_USER : return 'Supervisor / Inspector';
			case self::BOARD_MEMBER_USER : return 'Board Member';
			default : return $user_type;
		}
	}
	
	/**
	 * \param $page_number - current page number that the user is on - mandatory
	 * \param $total_pages - total number of pages to be displayes for the data set - mandatory
	 * \param $function_name - function to call when loading different pages - mandatory
	 * \return $pagination_html - Pagination links code as html formatted string
	 * 
 	 * This method is used to generate pagination links in HTML format.
 	 */
	public static function pagination($page_number, $total_pages, $function_name)
	{
		// no pagination is needed of course when there is only one page
		if ($total_pages <= 1) return '';
		else
		{
			// create only 3 links at the most at a time
			$pages = array();
			if ($total_pages <= 3) for ($i=1; $i<=$total_pages; $i++) $pages[] = $i;
			else
			{
				// for page number 1, display links for page 2 and 3
				if ($page_number == 1) $pages = array(1, 2, 3);
				else 
				{
					// for last page, display links for previous 2 pages
					if ($page_number == $total_pages) 
						$pages = array($page_number - 2, $page_number - 1, $page_number);
					// otherwise display always links for previous and next page
					else $pages = array($page_number - 1, $page_number, $page_number + 1);
				} 
			}

			// using the links created above, create HTML formatted output			
			$output = '';
			
			$output .= $page_number == 1 ? '<span class="previous-off">&laquo; First</span>' 
					: '<a href="" onclick="' . $function_name . '(1); return false;" class="previous">&laquo; First</a>';
				
			for ($i=0; $i<count($pages); $i++)
			{
				if ($pages[$i] == $page_number) $output .= '<span class="active">' . $page_number . '</span>';
				else $output .= '<a href="" onclick="' . $function_name . '(' . $pages[$i] . '); return false;">' . $pages[$i] . '</a>';
			}
			
			$output .= $page_number == $total_pages ? '<span class="next-off">Last &raquo;</span>' 
					: '<a href="" onclick="' . $function_name . '(' . $total_pages . '); return false;">Last &raquo;</a>';
				
			// CSS rules have been defined for a class name pagination 
			return '<div class="pagination">' . $output . '</div>';
		}
	}

	/**
	 * \return $theme - theme selected by the user (default - blue)
	 * 
 	 * This method returns the theme which has been selected by the logged in user 
	 * and hence what is stored in the session. By default i.e. when no such theme
	 * has been selected and hence nothing is in the session, then it returns blue
 	 */
	public static function getTheme()
	{
		return isset(Yii::app()->session['user_theme']) && !empty(Yii::app()->session['user_theme']) 
						? Yii::app()->session['user_theme'] : 'blue';
	}
	
	/**
	 * \return $current_tab - check if the user is on a tab function that is to be shown as "active" or current
	 * 
 	 * This method checks if the tab function associated with the page that the logged in user is on matches the
	 * one that is stored in the session. If it does, it returns the tab class as "current", else returns blank
 	 */
	public function getCurrentTabClass($tab)
	{
		return isset(Yii::app()->session['tab']) && Yii::app()->session['tab'] == $tab ? ' current' : '';
	}

	/**
	 * \param $record_info - array containing record information such as what is next and prev record and if this is last record or not - mandatory
	 * \param $style - additional style (CSS rules) to be applied to navigational links if any - optional
	 * \return $navigation_html - Navigation links code as html formatted string
	 * 
 	 * This method is used to generate record navigation links (a la MS Access) in HTML format.
 	 */
	public static function recordNavigation($record_info = array(), $style = "")
	{
		$navigation = '';
		
		if (isset($record_info['total']) && !empty($record_info['total']))
		{
			// navigation links use aptly names css class
			$theme = self::getTheme();
			$navigation = '<div class="record_navigation" ' . $style . '>';
			
			// first and previous record links
			if ($record_info['current'] == 1)
			{
				$navigation .= '&nbsp; <img src="' . AppUrl::imageUrl('black_first.png') . '" />';
				$navigation .= '&nbsp; <img src="' . AppUrl::imageUrl('black_prev.png') . '" />';
			}
			else
			{
				$navigation .= '&nbsp; <a href="' . $record_info['url'] . '1' . '">' .
					'<img src="' . AppUrl::imageUrl($theme . '_first.png') . '" /></a>';
				$navigation .= '&nbsp; <a href="' . $record_info['url'] . $record_info['prev_id'] . '">' .
					'<img src="' . AppUrl::imageUrl($theme . '_prev.png') . '" /></a>';
			}
			
			// display of the current record number
			$navigation .= ' &nbsp; Record #' . $record_info['current'] . ' of ' . $record_info['total'];
			
			// next and last record links
			if ($record_info['current'] == $record_info['total'])
			{
				$navigation .= '&nbsp; <img src="' . AppUrl::imageUrl('black_next.png') . '" />';
				$navigation .= '&nbsp; <img src="' . AppUrl::imageUrl('black_last.png') . '" />';
			}
			else
			{
				$navigation .= '&nbsp; <a href="' . $record_info['url'] . $record_info['next_id'] . '">' .
					'<img src="' . AppUrl::imageUrl($theme . '_next.png') . '" /></a>';
				$navigation .= '&nbsp; <a href="' . $record_info['url'] . $record_info['last_id'] . '">' .
					'<img src="' . AppUrl::imageUrl($theme . '_last.png') . '" /></a>';
			}
			
			$navigation .= '</div>';
			$navigation .= '<div style="clear: both;"> </div>';
		}
		
		return $navigation;
	}

	/**
	 * \param $table_headers - array containing various table header parameters - mandatory
	 * \param $order_by - which column the data in the table is sorted by currently - mandatory 
	 * \param $sort_direction - in which direction (asc or desc), the data in the table is sorted at present - mandatory
	 * \param $function_name - which function to call when a particular column is clicked to sort table data - mandatory
	 * \return $sortable_headers - string in HTML format containing table header code with clickable column headers
	 * 
 	 * This method generates HTML table header with clickable headers for sorting table data.
 	 */
	public static function getSortableTableHeader($table_headers, $order_by = "", $sort_direction = "", $function_name = "")
	{
		$output = '<thead><tr>';
		
		foreach ($table_headers as $column => $header)
		{
			// create html table header for each column
			$output .= '<th ' . (isset($header['style']) ? ' style="' . $header['style'] . '"' : '') . ' scope="col">';
			
			// some of them may not be sortable
			if ((isset($header['nosort']) && $header['nosort']) || $function_name == "") $output .= $header['caption'];
			else
			{
				// sort by function to be called via javascript
				$output .= '<a href="" onclick="sortData(\'' . $column . '\', \'' . $function_name . '\'); ';
				$output .= 'return false;">' . $header['caption'] . '</a>';
				
				// and finally indicate sort direction for appropriate column using current theme icons
				if ($order_by == $column)
				{
					$output .= '<br /><span style="text-align: center; ';
					if (strpos($header['caption'], '<img ') !== false) $output .= 'padding-top: 10px;">';
					else $output .= 'padding-left: ' . ((strlen($header['caption']) - 1) * 2) . 'px; padding-top: 5px;">';
					$output .= '<img src="' . AppUrl::imageUrl(self::getTheme() . '_' . strtolower($sort_direction) . '.png') . '" /></span>';
				}
			}
			$output .= '</th>';
		}
		
		return $output . '</tr></thead>';
	}

	/**
	 * \return $pdf_header - content in HTML format to be rendered as header in PDF format
	 * 
 	 * This method returns a string in HTML format which is used as a common PDF header
 	 */
	public static function pdfheader($orientation = '')
	{
		// blank image to be used for header
		$base_path = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->request->getBaseUrl();
		$municipality = isset(Yii::app()->session['municipality']) ? Yii::app()->session['municipality'] : '';
		$font = './images/captcha_fonts/17.ttf';

        // what if header image file name has already been set
        $default_header_used = true;
        $header_image_file_name = 'header.jpg';
        $header_landscape_image_file_name = 'header_landscape.jpg';
        foreach (array('main_header', 'main_header_landscape') as $header_file)
        {
            $dir = dirname(__FILE__);
            $header_file_dir = str_replace("\\", "/", $dir) . '/../../images/uploads/';
            foreach (glob($header_file_dir . $header_file . '.*') as $header_file_name)
            {
                if ($header_file == 'main_header')
                {
                    $header_image_file_name = 'uploads/' . basename($header_file_name);
                    $default_header_used = false;
                }
                if ($header_file == 'main_header_landscape') 
                {
                    $header_landscape_image_file_name = 'uploads/' . basename($header_file_name);               
                    $default_header_used = false;
                }
            }
        }
		
		// add municipality name at the right hand side
        if ($orientation == 'L') $header_image = imagecreatefromjpeg(AppUrl::imageUrl($header_landscape_image_file_name));
		else $header_image = imagecreatefromjpeg(AppUrl::imageUrl($header_image_file_name));
        $header_image = imagecreatefromjpeg(AppUrl::imageUrl($header_image_file_name));
		$font_color = imagecolorallocate($header_image, 211, 211, 211);
		
		if ($orientation == 'L') $end_position = 1150;
        else $end_position = 780;
		foreach (str_split(strrev($municipality)) as $single_char)
			if ($single_char == strtolower($single_char)) $end_position -= 10; else $end_position -= 15;
        if ($default_header_used) imagettftext($header_image, 25, 0, $end_position, 37, $font_color, $font, $municipality);
		
        // resize if necessary
        $width = 672;
        $height = 114;
        $width_orig = 0;
        $height_orig = 0;
        if ($orientation == 'L') list($width_orig, $height_orig) = getimagesize(AppUrl::imageUrl($header_landscape_image_file_name));
        else list($width_orig, $height_orig) = getimagesize(AppUrl::imageUrl($header_image_file_name));
        
        $height = intval(( $width * $height_orig ) / $width_orig);
        if ($orientation == 'L') $image = imagecreatefromjpeg(AppUrl::imageUrl($header_landscape_image_file_name));
        else $image = imagecreatefromjpeg(AppUrl::imageUrl($header_image_file_name));

        if (isset(Yii::app()->session['use_uploaded_header_image']) && Yii::app()->session['use_uploaded_header_image'] == 1)
        {
            $image_p = imagecreate($width, $height_orig);
            imagecopyresampled($image_p, $image, (($width - $width_orig) / 2), 0, 0, 0, $width_orig, $height_orig, $width_orig, $height_orig);
        }
        else 
        {
            $image_p = imagecreatetruecolor($width, $height);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
        }
        
		// create image and return
		$pdfheaderimage = $base_path . '/images/uploads/' . Yii::app()->session['user_id'] . '_pdfheader.png';
		imagepng($image_p, $pdfheaderimage);
        list($final_width, $final_height) = getimagesize($pdfheaderimage);
        self::$_header_height = $final_height;
		imagedestroy($image_p);
        
		return str_replace($base_path, '', $pdfheaderimage);		
	}
	
	/**
	 * \param $content - content in HTML format to be rendered in PDF format - mandatory
	 * \param $filename - generate the PDF output with the provided file name - optional
	 * 
 	 * This method creates a PDF file to be displayed in the browser using the content provided in the input.
 	 */
	public static function pdf($content = '', $filename = 'some_pdf_output_from_civicgov.pdf', $orientation = '', $display_in_browser = true)
	{
		// load tcpdf library
		Yii::import('application.vendors.*');
		require_once('tcpdf/tcpdf.php');
		require_once('tcpdf/config/lang/eng.php');

		// create PDF class
        $header_image_file_name = self::pdfheader($orientation);
        $pdf = new MYPDF($orientation, 'mm', 'LETTER');
        $pdf->SetCreator("CIVICgov");
		$pdf->SetAuthor('CIVICgov');
		$pdf->SetTitle('CIVICgov Online Application');

		// set margins and fonts
		$pdf->SetAutoPageBreak(TRUE, 15);
		if ($orientation == "L" || $orientation == "P" || empty($orientation)) 
        {
            $header_height = 0;
            if (self::$_header_height) $header_height = intval(ceil(self::$_header_height / 6));
            else $header_height = 40;
            $pdf->_header_height = $header_height;
            $pdf->SetMargins(8, $header_height + 2, 8);
		    $pdf->SetFont('dejavusans', '', 8);
        }
        else $pdf->SetFontSize(8);
        
		// set header and footer
        if ($orientation == "L" || $orientation == "P" || empty($orientation)) 
        {
    		$pdf->setHeaderMargin(5);
            $pdf->setX(100);
    		$pdf->setHeaderData('../../../..' . $header_image_file_name, ($orientation == 'L' ? 180 : 95));
        }
        else $pdf->setPrintHeader(false);
        
		$pdf->setPrintFooter(true);
        $pdf->SetFooterMargin(15);

		// multiple page output may be needed
		if (!is_array($content)) $content = array($content);
		foreach ($content as $content_pages)
		{
			foreach (explode(Utility::PDF_PAGE_BREAK, $content_pages) as $content_page)
			{
				$pdf->AddPage();		
				$pdf->writeHTML($content_page, true, false, true, false, '');
			}
		}
		
		// output directly to the browser
        $output_type = 'I';
        if (!$display_in_browser) $output_type = 'D';
		$pdf->Output($filename, $output_type);		
	}

	/**
	 * \param $domain - for which "area"/table/module (e.g. users, permits, inspections etc.) the unique id is to be generated - mandatory
	 * \return $unique_id - unique identifier value (e.g. user reference number) for the requested area
	 * 
 	 * This method returns a unique id/key value for various "areas" or tables so e.g. user reference
	 * number. At present, this method is just returning a random string based off of time() which is
	 * number of seconds from "epoch" since it is called only for generating a reference number (that
	 * is again an unique key for the user record). But in future when it is called for other "areas"
	 * such as permits and inspections, we may want to utilize database and transactions for the same
 	 */
	public static function getUniqueID($domain = '')
	{
		return mt_rand(101, 999) . time();
	}

	public static function getRelatedLinksNote()
	{
		return '<br /><br />Related Links: <small>(<strong>NOTE: </strong>' .
				'You will need to login to CIVICgov Online Application first before clicking on this link</small>)<br />';
	}

    public static function sortActionItemsByDueDate($item1, $item2)
	{
    	return ($item1['due_date_timestamp'] == $item2['due_date_timestamp']) 
					? 0 : (($item1['due_date_timestamp'] > $item2['due_date_timestamp']) ? 1 : -1);
	}

    public static function sortByDateField($item1, $item2)
	{
    	return ($item1['sort_date'] == $item2['sort_date']) ? 0 : (($item1['sort_date'] > $item2['sort_date']) ? 1 : -1);
	}


	public static function createCSV($report_data, $report_area = "")
	{
		// default filename if not passed in
		extract($report_data);
		if (empty($report_area)) $filename = "civicgov_export_report.csv";
		else $filename = "civicgov_{$report_area}_report.csv";

        // is it a matrix report?
        $total_fields = isset($fields_to_display['total_fields']) ? $fields_to_display['total_fields'] : array();
        $group_fields = isset($fields_to_display['group_by']) ? $fields_to_display['group_by'] : array();
        if (isset($matrix_report) && !empty($matrix_report))
        {
            $fields_to_display = $group_fields;
            if (isset($matrix_columns) && is_array($matrix_columns))
                $fields_to_display = array_merge($fields_to_display, $matrix_columns);
        }
                
		// first write the headers
		$csv_output = "";
		foreach ($fields_to_display as $search_field)
		{
			// if (!in_array($search_field, $fields_to_display)) continue;
            if (is_array($search_field) || empty($search_field) || $search_field == 1) continue;
            if ($search_field == 'occupancy_1') $search_field_output = 'Occupancy - Empty Room';
            else if ($search_field == 'occupancy_2') $search_field_output = 'Occupancy - With Chairs';
            else if ($search_field == 'occupancy_3') $search_field_output = 'Occupancy - With Tables & Chairs';
            else if ($search_field == 'parcel_id') $search_field_output = 'Parcel ID';
			else $search_field_output = str_replace(" Id", "", ucwords(str_replace("_", " ", $search_field)));
			
            if (isset($report_data['report_type']) && $report_data['report_type'] == 'munis_file')
                $csv_output = $csv_output;
            else
            {
    			if (empty($csv_output)) $csv_output = $search_field_output;
    			else $csv_output = $csv_output . "," . $search_field_output;			
            }
		}
		
		// may be report name has been passed in
		if (isset($report_name) && !empty($report_name))
        {
            if (isset($report_data['report_type']) && 
                    ($report_data['report_type'] == 'vision_permits' || $report_data['report_type'] == 'munis_file'))
                $csv_output = $csv_output;
            else
    			$csv_output = $report_name . "\n\n" . $csv_output;            
        }
		
		// and then the data 
        $fee_total = 0;
        $project_cost_total = 0;
        $project_value_total = 0;
        $total_rows = 0;
		foreach ($display_records as $record_to_display)
		{
			$row_output = "";
            $total_rows += 1;
            
			foreach ($fields_to_display as $search_field)
			{
                if ($search_field == 'group_report' || $search_field == 'group_by' || $search_field == 1 ||
                        $search_field == 'total_field' || empty($search_field) || is_array($search_field)) 
                    continue;

				// if (!in_array($search_field, $fields_to_display)) continue;

                if ($search_field == 'fee_amount' && isset($record_to_display['fee_amount']))
                    $fee_total += floatval($record_to_display['fee_amount']);
                                            
                if ($search_field == 'project_cost' && isset($record_to_display['project_cost']))
                    $project_cost_total += floatval(preg_replace('/[^0-9\.]/s', '', $record_to_display['project_cost']));
                                            
                if ($search_field == 'project_value' && isset($record_to_display['project_value']))
                    $project_value_total += floatval(preg_replace('/[^0-9\.]/s', '', $record_to_display['project_value']));
                                            
				if ($search_field == 'fee_type_id') $field_value = $record_to_display['fee_category']; 
				else if ($search_field == 'fee_id') $field_value = $record_to_display['fee_type']; 
                else if ($search_field == 'department_id') $field_value = $record_to_display['department']; 
				else if ($search_field == 'permit_type_id') $field_value = $record_to_display['permit_type']; 
				else if ($search_field == 'permit_use_id') $field_value = $record_to_display['permit_use']; 
				else if ($search_field == 'project_type_id') $field_value = $record_to_display['project_type']; 
				else if ($search_field == 'project_use_id') $field_value = $record_to_display['project_use']; 
				else if ($search_field == 'zone_permit_type_id') $field_value = $record_to_display['zoning_permit_type']; 
				else if ($search_field == 'zone_proposed_use_id') $field_value = $record_to_display['zoning_proposed_use']; 
				else if ($search_field == 'zone_current_use_id') $field_value = $record_to_display['zoning_current_use']; 
				else if ($search_field == 'current_zone_type_id') $field_value = $record_to_display['current_zoning_type']; 
				else if ($search_field == 'proposed_zone_type_id') $field_value = $record_to_display['proposed_zoning_type']; 
				else if ($search_field == 'zone_structure_type_id') $field_value = $record_to_display['zoning_structure_type']; 
				else if ($search_field == 'structure_type_id') $field_value = $record_to_display['structure_type']; 
				else if ($search_field == 'foundation_material_id') $field_value = $record_to_display['foundation_material']; 
				else if ($search_field == 'foundation_type_id') $field_value = $record_to_display['foundation_type']; 
				else if ($search_field == 'payment_method_id') 
                {
                    if (isset($record_to_display['payment_number']) && !empty($record_to_display['payment_number']))
                        $field_value = $record_to_display['payment_method'] . ' #' . $record_to_display['payment_number'];
                    else $field_value = $record_to_display['payment_method']; 
                }
				else if ($search_field == 'status') 
                {
                    if ($record_to_display['status'] == "A") $field_value = "Approved"; 
                    else if ($record_to_display['status'] == "N") $field_value = "Approved With Conditions"; 
                    else if ($record_to_display['status'] == "M") $field_value = "Incomplete Submittal"; 
                    else if ($record_to_display['status'] == "R") $field_value = "Rejected"; 
                    else if ($record_to_display['status'] == "B") $field_value = "Abandoned"; 
                    else if ($record_to_display['status'] == "W") $field_value = "Withdrawn"; 
                    else if ($record_to_display['status'] == "C") $field_value = "Closed"; 
                    else if ($record_to_display['status'] == "W") $field_value = "In Appeal"; 
                    else if ($record_to_display['status'] == "X") $field_value = "Unpaid Application"; 
                    else if ($record_to_display['status'] == '0') $field_value = "Fail"; 
                    else if ($record_to_display['status'] == '1') $field_value = "Pass"; 
                    else if ($record_to_display['status'] == '2') $field_value = "Incomplete"; 
                    else if ($record_to_display['status'] == '10') $field_value = "Closed"; 
                    else if ($record_to_display['status'] == '11') $field_value = "Closed With Complaint"; 
                    else if ($record_to_display['status'] == '-1') $field_value = "Open"; 
                    else if ($record_to_display['status'] == '21') $field_value = "Declaration of Compliance"; 
                    else if ($record_to_display['status'] == '31') $field_value = "Declaration Received"; 
                    else if ($record_to_display['status'] == '41') $field_value = "Failed & Referred To Other"; 
                    else if ($record_to_display['status'] == '51') $field_value = "Referred To Other Agency"; 
                    else if ($record_to_display['status'] == '61') $field_value = "Corrected On Site"; 
                    else if ($record_to_display['status'] == '71') $field_value = "Deferred To Regular Inspection"; 
                    else if ($record_to_display['status'] == '101') $field_value = "Requested"; 
                    else if ($record_to_display['status'] == '110') $field_value = "Reschedule Request"; 
                    else if ($record_to_display['status'] == '102') $field_value = "To Be Cancelled"; 
                    else if ($record_to_display['status'] == '103') $field_value = "Cancelled"; 
                    else $field_value = $record_to_display['status']; 
                }
				else if ($search_field == 'violation_type_id') $field_value = $record_to_display['violation_type']; 
				else if ($search_field == 'violation_law_id') $field_value = $record_to_display['violation_law']; 
				else if ($search_field == 'violation_reason_id') $field_value = $record_to_display['violation_reason']; 
				else if ($search_field == 'court_id') $field_value = $record_to_display['court']; 
				else if ($search_field == 'judge_id') $field_value = $record_to_display['judge']; 
				else if ($search_field == 'complaint_type_id') $field_value = $record_to_display['complaint_type']; 
				else if ($search_field == 'complaint_status_id') $field_value = $record_to_display['complaint_status']; 
				else if ($search_field == 'violation_status_id') $field_value = $record_to_display['violation_status']; 
				else if ($search_field == 'inspector_id') $field_value = $record_to_display['inspector']; 
				else if ($search_field == 'business_type_id') $field_value = $record_to_display['business_type']; 
				else if ($search_field == 'business_subtype_id') $field_value = $record_to_display['business_subtype']; 
				else if ($search_field == 'license_status_id') $field_value = $record_to_display['license_status']; 
				else if ($search_field == 'legal_structure_id') $field_value = $record_to_display['legal_structure']; 
				else if ($search_field == 'waste_disposal_method_id') $field_value = $record_to_display['waste_disposal_method']; 
                else if ($search_field == 'inspection_type_id') $field_value = $record_to_display['inspection_type'];
                else if ($search_field == 'inspector_id') $field_value = $record_to_display['inspector'];
                else if ($search_field == 'occupancy_class') $field_value = $record_to_display['occupancy_class_value'];
                else if ($search_field == 'construction_class') $field_value = $record_to_display['construction_class_value'];
                else if ($search_field == 'inspection_category_code')
                {
                    if ($record_to_display[$search_field] == 'F') $field_value = 'Fire/Safety';
                    else if ($record_to_display[$search_field] == 'P') $field_value = 'Permits';
                    else if ($record_to_display[$search_field] == 'C') $field_value = 'Complaints';
                    else if ($record_to_display[$search_field] == 'V') $field_value = 'Violations';
                    else if ($record_to_display[$search_field] == 'L') $field_value = 'Licenses';
                    else $field_value = $record_to_display[$search_field];
                }
				else if ($search_field == 'violation_date' || $search_field == 'resolve_by_date' || $search_field == 'closed_date' ||
                            $search_field == 'next_inspection_date' || $search_field == 'date' || $search_field == 'entry_date' || 
                            $search_field == 'issue_date' || $search_field == 'expiration_date' || $search_field == 'end_date' || 
                            $search_field == 'inspection_end_date' || $search_field == 'file_date' || $search_field == 'permit_date' ||
                            $search_field == 'start_date' || $search_field == 'inspection_start_date' || $search_field == 'certificate_date' ||
                            $search_field == 'paid_date' || $search_field == 'hearing_date' || $search_field == 'action_date' ||
                            $search_field == 'effective_date' || $search_field == 'exemption_through_date' || $search_field == 'inspection_date') 
                {
                    $date_search_field = $search_field;
                    if ($date_search_field == "end_date") $date_search_field = "inspection_end_date";
                    if ($date_search_field == "start_date") $date_search_field = "inspection_start_date";

                    if (stripos($record_to_display[$date_search_field], "Subtotal") !== false
                            || stripos($record_to_display[$date_search_field], " Records") !== false
                                || stripos($record_to_display[$date_search_field], "T O T A L") !== false)
                        $field_value = $record_to_display[$date_search_field];
                    else
                    {
                        if (intval(date("Y", strtotime($record_to_display[$date_search_field]))) < 1971) $field_value = '';
    					else $field_value = date("F j, Y", strtotime($record_to_display[$search_field])); 
                    }
                }
                else if ($search_field == 'fee_amount' || $search_field == 'project_cost' || $search_field == 'project_value')
                {
                    $field_value = 0;
                    if (isset($record_to_display[$search_field]) && !empty($record_to_display[$search_field]))
                        $field_value = floatval(preg_replace('/[^0-9\.\-]/s', '', $record_to_display[$search_field]));
                }
				else $field_value = isset($record_to_display[$search_field]) ? $record_to_display[$search_field] : ''; 

                if (isset($report_data['report_type']) && $report_data['report_type'] == 'vision_permits' 
                        && ($search_field == 'parcel_number' || $search_field == 'parcel_id'))
                    if (strlen($field_value) < 10) $field_value = str_repeat('0', (10 - strlen($field_value))) . $field_value; 

                $field_value = str_replace('<br />', ', ', $field_value);
                $field_value = str_replace('<strong>', '', $field_value);
                $field_value = str_replace('</strong>', '', $field_value);
                $field_value = strip_tags($field_value);
                
                $field_value = str_replace('"', '""', $field_value);
                $field_value = preg_replace("/[\n]+/", " ", preg_replace("/[\r\n]+/", " ", $field_value));
                
				if (empty($row_output)) $row_output = '"' . $field_value . '"';
				else $row_output = $row_output . ',"' . $field_value . '"';
			}
			
            $row_output = str_replace("&nbsp;", "", $row_output);
			if (empty($csv_output)) $csv_output = $row_output;
			else $csv_output = $csv_output . "\n" . $row_output;
		}
        
        // write a total record in certain cases
        if ((!isset($matrix_report) || !$matrix_report) && count($group_fields) <= 0)
        {
            $total_label_displayed = false;
            $row_output = "";
            
            if (isset($report_area) && $report_area == 'parcels') $record_display_type = 'Parcels';
            else if (isset($report_area) && $report_area == 'complaints') $record_display_type = 'Complaints';
            else if (isset($report_area) && $report_area == 'permits') $record_display_type = 'Permits';
            else if (isset($report_area) && $report_area == 'violations') $record_display_type = 'Violations';
            else if (isset($report_area) && $report_area == 'certificates') $record_display_type = 'Certificates';
            else if (isset($report_area) && $report_area == 'licenses') $record_display_type = 'Licenses';
            else if (isset($report_area) && $report_area == 'inspections') $record_display_type = 'Inspections';
            else if (isset($report_area) && $report_area == 'fees') $record_display_type = 'Transactions';
            else $record_display_type = 'Records';
            
            if (isset($report_data['report_type']) && $report_data['report_type'] == 'monthly_permits')
                $record_display_type = 'Permits';

            foreach ($fields_to_display as $search_field)
            {
                $field_value = "";
                if (is_array($search_field) || empty($search_field)) continue;
                
                if ($search_field == 'fee_amount') $field_value = $fee_total;
                else if ($search_field == 'project_cost') $field_value = $project_cost_total;
                else if ($search_field == 'project_value') $field_value = $project_value_total;
                else
                {
                    if (!$total_label_displayed) $field_value = $total_rows . ' ' . $record_display_type;
                }
                $total_label_displayed = true;

    			if (empty($row_output)) $row_output = $field_value;
    			else $row_output = $row_output . "," . $field_value;			
            }

            if (isset($report_data['report_type']) && 
                    ($report_data['report_type'] == 'vision_permits' || $report_data['report_type'] == 'munis_file'))
                $csv_output = $csv_output;
            else
            {
    			if (empty($csv_output)) $csv_output = $row_output;
    			else $csv_output = $csv_output . "\n" . $row_output;
            }
        }

        if (isset($report_data['report_type']) && $report_data['report_type'] == 'monthly_permits' && isset($report_data['year_to_date']))
        {
            $row_output = "";
            $row_output .= 'Year To Date: ' . $report_data['year_to_date']['record_count'] . ' Permits,,,,';
            $row_output .= $report_data['year_to_date']['total_project_cost'] . ',';            
            $row_output .= $report_data['year_to_date']['total_fee_amount'];
            
			if (empty($csv_output)) $csv_output = $row_output;
			else $csv_output = $csv_output . "\n" . $row_output;
        }
		
		// export as csv ... well or may be text since munis needs a text file
        if (isset($report_data['report_type']) && $report_data['report_type'] == "munis_file")
        {
            header("Content-type: text/plain");
        	header('Content-Disposition:attachment;filename='. str_replace(".csv", ".txt", $filename));
        	header('Pragma: no-cache');		
    		echo str_replace("\n", "\r\n", str_replace('"', '', str_replace(",", "", $csv_output)));
        }
        else
        {
        	header('Content-Type: application/csv');
        	header('Content-Disposition:attachment;filename='. $filename);
        	header('Pragma: no-cache');		
    		echo $csv_output;
        }
	}

    public static function groupData($input_data, $input_group_fields, $total_fields, $sort_fields, $display_only_totals = 0) 
    {
        $return_data = array();
        $prev_key = "";
        $totals = array();
        $complete_totals = array();
        $total_label_assigned = false;
        $subtotal_label_assigned = false;
        $parcel_number_found = false;
        $sub_total_count = 0;
        $sub_totals = array();
        $fee_sub_totals = array();
        $debit_subtotal_count = 0;
        $credit_subtotal_count = 0;
        $balance_subtotal_count = 0;
        $total_subtotal_count = 0;
        $total_count = 0;
        
        // group the data - including for separate sub-key combinations
        $key = "";
        for ($gfc=1; $gfc<=count($input_group_fields); $gfc++)
        {
            // always first sort data using group fields so that the sub total calculations are correct
            $group_fields = array_slice($input_group_fields, 0, count($input_group_fields) - $gfc + 1);
            $data = Utility::msort($input_data, $group_fields);

            $prev_key = "";
            $total_label_assigned = false;
            $subtotal_label_assigned = false;
            
            foreach ($data as $data_key_index => $row)
            {
                // grand total row needs non total fields populated
                if (count($complete_totals) <= 0)
                {
                    foreach ($row as $field_name => $field_value) 
                    {
                        if (!in_array($field_name, $total_fields)) 
                        {
                            if ($field_name == $group_fields[0])
                            {
                                if (!$total_label_assigned)
                                {
                                    $complete_totals[$field_name] = "<strong>T O T A L</strong>";
                                    $total_label_assigned = true;
                                }
                                else $complete_totals[$field_name] = "";
                            }
                            else $complete_totals[$field_name] = "";
                        }
                        else $complete_totals[$field_name] = 0;
                        
                        if ($field_name == 'parcel_number') $parcel_number_found = true;
                    }
                }
                
                // build key first
                $key = "";
                foreach ($group_fields as $group_field)
                {
                    if (isset($row[$group_field]))
                    {
                        if ($key == "") $key = $row[$group_field];
                        else $key = $key . "~|~" . $row[$group_field];
                    }
                }
                
                $key = rtrim(ltrim($key));
                if (empty($key)) $key = "EMPTY_KEY_VALUE";
                
                // is it found in the return i.e. grouped data
                if (!isset($return_data[$key]))
                {
                    $grouped_data = array();
                    foreach ($row as $field_name => $field_value) 
                    {
                        if (in_array($field_name, $group_fields))
                            $grouped_data[$field_name] = $field_value;
                        else 
                        {
                            if (in_array($field_name, $total_fields))
                                $grouped_data[$field_name] = 0;
                            else $grouped_data[$field_name] = "";
                        }
                    }
                    $return_data[$key] = $grouped_data;
                }
                
                // is this same from previous record?
                if ($key != $prev_key)
                {
                    if ($prev_key != "")
                    {
                        $current_sub_total = $return_data[$prev_key];
                        foreach ($total_fields as $total_field)
                        {
                            if (isset($totals[$total_field]))
                                $current_sub_total[$total_field] = $totals[$total_field];
                            else $current_sub_total[$total_field] = 0;
                        }

                        foreach ($row as $field_name => $field_value) 
                        {
                            if (!in_array($field_name, $total_fields)) 
                            {
                                if (in_array($field_name, $group_fields))
                                {
                                    $group_fields_count = count($group_fields);
                                    if ($field_name == $group_fields[$group_fields_count - 1])
                                    {
                                        $current_sub_total[$field_name] .= "<strong>(SUB TOTAL)</strong>";
                                        $subtotal_label_assigned = true;
                                        $sub_totals[$prev_key] = $sub_total_count;
                                        $fee_sub_totals[$prev_key]['debit'] = $debit_subtotal_count;
                                        $fee_sub_totals[$prev_key]['credit'] = $credit_subtotal_count;
                                        $fee_sub_totals[$prev_key]['balance'] = $balance_subtotal_count;
                                        $fee_sub_totals[$prev_key]['total'] = $total_subtotal_count;
                                        // print $gfc . ' ' . $field_name . ' ' . $current_sub_total[$field_name] . '<br />';
                                        break;
                                    }
                                }
                            }
                        }
                        
                        if ($parcel_number_found && !in_array('parcel_number', $group_fields) && empty($current_sub_total['parcel_number']))
                            $current_sub_total['parcel_number'] = '~~~~';
                        
                        $return_data[$prev_key] = $current_sub_total;
                    }
                    
                    $totals = array();
                    $prev_key = $key;
                    $subtotal_label_assigned = false;
                    $key_index = 0;
                    $sub_total_count = 0;
                    $debit_subtotal_count = 0;
                    $credit_subtotal_count = 0;
                    $balance_subtotal_count = 0;
                    $total_subtotal_count = 0;
                }
                
                // total up various fields as necessary
                foreach ($total_fields as $total_field)
                {
                    if (!isset($totals[$total_field])) $totals[$total_field] = 0;
                    if (isset($row[$total_field]) && $gfc == 1) 
                        $totals[$total_field] += floatval(preg_replace('/[^0-9\.\-]/s', '', $row[$total_field]));

                    if (!isset($complete_totals[$total_field])) $complete_totals[$total_field] = 0;
                    if (isset($row[$total_field]) && $gfc == 1) 
                        $complete_totals[$total_field] += floatval(preg_replace('/[^0-9\.\-]/s', '', $row[$total_field]));
                }
                
                // and also add the actual report data too :)
                $sub_total_count += 1;
                $debit_subtotal_count = $debit_subtotal_count + (isset($row['debit']) ? floatval($row['debit']) : 0);
                $credit_subtotal_count = $credit_subtotal_count + (isset($row['credit']) ? floatval($row['credit']) : 0);
                $balance_subtotal_count = $balance_subtotal_count + (isset($row['balance']) ? floatval($row['balance']) : 0);
                $total_subtotal_count = $total_subtotal_count + (isset($row['total']) ? floatval($row['total']) : 0);
                if ($gfc == 1) $total_count += 1;
                $row['row_count'] = 1;
                if (!$display_only_totals) $return_data['non_key_' . $data_key_index] = $row;
            }
            
            // populate totals for the last group in the returned data
            if (count($data))
            {            
                $subtotal_label_assigned = false;
                $current_sub_total = $return_data[$key];
                                        
                foreach ($total_fields as $total_field)
                {
                    if (isset($totals[$total_field]))
                        $current_sub_total[$total_field] = $totals[$total_field];
                    else $current_sub_total[$total_field] = 0;
                }

                foreach ($row as $field_name => $field_value) 
                {
                    if (!in_array($field_name, $total_fields)) 
                    {
                        if (in_array($field_name, $group_fields))
                        {
                            $group_fields_count = count($group_fields);
                            if ($field_name == $group_fields[$group_fields_count - 1])
                            {
                                if (!isset($current_sub_total[$field_name])) $current_sub_total[$field_name] = "";
                                if (strpos($current_sub_total[$field_name], "<strong>(SUB TOTAL)</strong>") === false)
                                    $current_sub_total[$field_name] .= "<strong>(SUB TOTAL)</strong>";
                                $subtotal_label_assigned = true;
                                $sub_totals[$key] = $sub_total_count;
                                $fee_sub_totals[$prev_key]['debit'] = $debit_subtotal_count;
                                $fee_sub_totals[$prev_key]['credit'] = $credit_subtotal_count;
                                $fee_sub_totals[$prev_key]['balance'] = $balance_subtotal_count;
                                $fee_sub_totals[$prev_key]['total'] = $total_subtotal_count;
                                // print $gfc . ' ' . $field_name . ' ' . $current_sub_total[$field_name] . '<br />';
                                break;
                            }
                        }
                    }
                }

                if ($parcel_number_found && !in_array('parcel_number', $group_fields) && empty($current_sub_total['parcel_number']))
                    $current_sub_total['parcel_number'] = '~~~~';
         
                 $return_data[$key] = $current_sub_total;            
            }
        }
            
        // and also the grand total row but only after sorting again :)
        $complete_sort_fields = $sort_fields;
        $return_data = Utility::msort($return_data, $complete_sort_fields);
        // exit;
        /*
        if ($parcel_number_found && !in_array('parcel_number', $complete_sort_fields))
        {
            $complete_sort_fields[] = 'parcel_number';
            $return_data = Utility::specialSortWithParcelNumber($return_data, $complete_sort_fields);
        }
        else $return_data = Utility::msort($return_data, $complete_sort_fields);
        */
        
        // one more small change :( for some reason "sub totals" does not get sorted properly
        $final_sort_data = $return_data;
        $return_data = array();
        $sub_total_index = 0;
        $final_data_indexes = array();
        $sorted_sub_total_index = 0;
        for ($gfc=1; $gfc<=count($input_group_fields); $gfc++)
        {
            $previous_keys = array();
            $processed_keys = array();
            $group_fields = array_slice($input_group_fields, 0, count($input_group_fields) - $gfc + 1);
            foreach ($final_sort_data as $row)
            {
                // build key again to search sub-totals by
                $key = "";
                foreach ($group_fields as $group_field)
                {
                    if (isset($row[$group_field]))
                    {
                        if ($key == "") $key = $row[$group_field];
                        else $key = $key . "~|~" . $row[$group_field];
                    }
                }
                
                $key = rtrim(ltrim($key));
                if (empty($key)) $key = "EMPTY_KEY_VALUE";
                $sub_totals_array_key = str_replace("<strong>(SUB TOTAL)</strong>", "", $key);

                if (substr(strrev($sub_totals_array_key), 0, 3) == "~|~") continue;
                if (!isset($sub_totals[$sub_totals_array_key])) continue;
                if ($gfc != 1)
                {
                    if (in_array($sub_totals_array_key, $processed_keys)) continue;
                    $processed_keys[] = $sub_totals_array_key;
                }
                else
                {
                    if (count($input_group_fields) > 1)
                    {
                        $other_keys = array();
                        for ($other=1; $other<=count($input_group_fields); $other++)
                        {
                            $other_key = "";
                            $other_key_count = count($input_group_fields) - $other;
                            
                            foreach (array_slice($input_group_fields, 0, $other_key_count) as $other_key_field)
                            {
                                if (isset($row[$other_key_field]))
                                {
                                    if ($other_key == "") $other_key = $row[$other_key_field];
                                    else $other_key = $other_key . "~|~" . $row[$other_key_field];
                                }
                            }

                            // if (empty($other_key)) $other_key = "N/A";
                            $other_keys[] = $other_key;
                        }
                        
                        if (count($previous_keys) <= 0) $previous_keys = $other_keys;
                        else
                        {
                            foreach ($previous_keys as $other_key_index => $other_key_value)
                            {
                                if ($other_key_value != $other_keys[$other_key_index])
                                {
                                    $final_data_indexes[$other_key_value] = $sorted_sub_total_index++;
                                    $previous_keys = $other_keys;
                                }
                            }
                        }

                    }
                }

                /*
                print $gfc . ' ' . $sorted_sub_total_index++ . ' key = ' . $sub_totals_array_key;
                if (isset($sub_totals[$sub_totals_array_key])) print ' count = ' . $sub_totals[$sub_totals_array_key];
                print "<br />";
                */
                
                foreach ($row as $field_key => $field_value)
                {
                    if ($gfc != 1)
                    {
                        if (in_array($field_key, $group_fields))
                        {
                            $group_fields_count = count($group_fields);
                            if ($field_key == $group_fields[$group_fields_count - 1])
                            {
                                $key = str_replace("<strong>(SUB TOTAL)</strong>", "", $key);
                                $key = rtrim(ltrim($key));
                                
                                $display_key = $key;
                                $display_key = str_replace("~|~", " - ", $display_key);
                                
                                if (empty($key)) $key = "EMPTY_KEY_VALUE";

                                $sub_total_count = 0;
                                if (isset($sub_totals[$key]))
                                {
                                    $sub_total_count = $sub_totals[$key];
                                    $sub_total_index += 1;
                                }

                                if ($display_only_totals) $return_row[$field_key] = "Subtotal For $field_value ($sub_total_count Records)";
                                else $return_row[$field_key] = "<strong>Subtotal For $field_value ($sub_total_count Records)</strong>";
                            }
                            else 
                            {
                                if ($display_only_totals) $return_row[$field_key] = $field_value;
                                else $return_row[$field_key] = "<strong>" . $field_value . "</strong>";
                            }
                        }
                        else 
                        {
                            if (in_array($field_key, array('debit', 'credit', 'balance', 'total')))
                            {
                                $key = str_replace("<strong>(SUB TOTAL)</strong>", "", $key);
                                $key = rtrim(ltrim($key));
                                if (empty($key)) $key = "EMPTY_KEY_VALUE";

                                if (isset($fee_sub_totals[$key]))
                                    $return_row[$field_key] = $fee_sub_totals[$key][$field_key];
                                else $return_row[$field_key] = "";
                            }
                            else $return_row[$field_key] = "";
                        }
                    }
                    else
                    {
                        $sub_total_display_position = strpos($field_value, "<strong>(SUB TOTAL)</strong>");
                        if ($sub_total_display_position !== false)
                        {
                            $key = str_replace("<strong>(SUB TOTAL)</strong>", "", $key);
                            $key = rtrim(ltrim($key));
                            
                            $display_key = $key;
                            $display_key = str_replace("~|~", " - ", $display_key);
                            
                            if (empty($key)) $key = "EMPTY_KEY_VALUE";

                            $sub_total_count = 0;
                            if (isset($sub_totals[$key]))
                            {
                                $sub_total_count = $sub_totals[$key];
                                $sub_total_index += 1;
                            }
                            
                            if ($display_only_totals)
                            {
                                $field_display_value = substr($field_value, 0, $sub_total_display_position);
                                if ($sub_total_count)
                                {
                                    $return_row[$field_key] = "Subtotal For $field_display_value ($sub_total_count Records)";
                                    $return_row['row_count'] = $sub_total_count;
                                }
                                else 
                                {
                                    if ($field_key != 'row_count') 
                                    {
                                        if (in_array($field_key, $group_fields))
                                            $return_row[$field_key] = '<strong>' . $field_display_value . '</strong>';
                                        else $return_row[$field_key] = $field_display_value;
                                    }
                                }
                            }
                            else
                            {
                                if ($sub_total_count)
                                {
                                    if (empty($display_key))
                                        $return_row[$field_key] = "<strong>Subtotal ($sub_total_count Records)</strong>";
                                    else $return_row[$field_key] = "<strong>Subtotal For $display_key ($sub_total_count Records)</strong>";
                                    $return_row['row_count'] = $sub_total_count;
                                }
                                else 
                                {
                                    if ($field_key != 'row_count') 
                                    {
                                        if (empty($display_key)) $return_row[$field_key] = "<strong>Subtotal</strong>";
                                        else $return_row[$field_key] = "<strong>Subtotal For $display_key</strong>";
                                    }
                                    else $return_row['row_count'] = $sub_total_count;
                                }
                            }                    
                        }
                        else 
                        {
                            if ($field_key == 'parcel_number' && $field_value == '~~~~')
                                $return_row['parcel_number'] = '';
                            else 
                            {
                                if ($field_key != 'row_count') $return_row[$field_key] = $field_value;
                                else 
                                {
                                    if (!isset($row['row_count']) || empty($row['row_count']))
                                        $return_row['row_count'] = $sub_total_count;
                                    else
                                        $return_row['row_count'] = $row['row_count'];
                                }
                            }
                        }                        
                    }
                }

                if (isset($final_data_indexes[$sub_totals_array_key]))
                {
                    $sorted_return_data_index = $final_data_indexes[$sub_totals_array_key];
                    $return_data[$sorted_return_data_index] = $return_row;
                }
                else $return_data[$sorted_sub_total_index++] = $return_row;
            }
        }
        
        // append total count reocrds of course
        ksort($return_data);
        $group_fields = $input_group_fields;
        $grand_total_row = array();
        foreach ($complete_totals as $total_row_key => $total_row_value)
        {
            if ($total_row_value === '<strong>T O T A L</strong>')
            {
                $grand_total_row[$total_row_key] = "<strong>T O T A L - $total_count Records</strong>";
                $grand_total_row['row_count'] = $total_count;
            }
            else if ($total_row_key != 'row_count') $grand_total_row[$total_row_key] = $total_row_value;
        }
        if (count($data)) $return_data[] = $grand_total_row;
                
        // and now return the grouped data
        return $return_data;
    }

    public function specialSortWithParcelNumber($data, $keys)
    {
        $return_data = array();
        
        foreach ($data as $row)
        {
            // build complete key for sorting first
            $key = '';
            foreach ($keys as $key_field)
            {
                if (isset($row[$key_field]))
                {
                    $field_value = rtrim(ltrim($row[$key_field]));
                    if (strpos($field_value, "<strong>(SUB TOTAL)</strong>") !== false)
                        $field_value = str_replace("<strong>(SUB TOTAL)</strong>", "", $field_value);
                    
                    if ($key_field == 'parcel_number' && $field_value == '') $field_value = '000000000000000000000000';
                    if ($key == "") $key = $field_value;
                    else $key = $key . "~|~" . $field_value;
                }
            }
            
            // build a complete array first
            $key = rtrim(ltrim($key));
            if (empty($key)) $key = "EMPTY_KEY_VALUE";
            $return_data[] = array_merge(array('CIVICGOV-special-sort-key-field' => $key), $row);
        }

        // now sort using our own function
		uasort($return_data, array("Utility", "sortByConcatenatedKey"));
        return $return_data;
    }
    
    public static function sortByConcatenatedKey($col1, $col2)
	{
		$c1 = $col1['CIVICGOV-special-sort-key-field'];
    	$c2 = $col2['CIVICGOV-special-sort-key-field'];
        if ($c1 == $c2) return 0; else return ($c1 > $c2) ? 1 : -1;
	}
    
    public static function msort($array, $key, $sort_flags = SORT_REGULAR) 
    {
        if (is_array($array) && count($array) > 0) 
        {
            if (!empty($key)) 
            {
                $subtotals = array();
                $mapping = array();
                
                foreach ($array as $k => $v) 
                {
                    $sort_key = '';
                    if (!is_array($key)) 
                    {
                        $sort_key = $v[$key];
                    } 
                    else 
                    {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) 
                        {
                            $sort_key .= isset($v[$key_key]) ? $v[$key_key] : '';
                        }
                        $sort_flags = SORT_STRING;
                    }

                    $sort_key = rtrim(ltrim($sort_key));
                    if (empty($sort_key)) $sort_key = "EMPTY_KEY_VALUE"; 

                    if (strpos($sort_key, "<strong>(SUB TOTAL)</strong>") !== false)
                    {
                        $normal_sort_key = str_replace("<strong>(SUB TOTAL)</strong>", "", $sort_key);
                        if (empty($normal_sort_key)) $normal_sort_key = "EMPTY_KEY_VALUE"; 
                        $subtotals[$normal_sort_key] = $v;             
                    }
                    else
                        $mapping[$k] = $sort_key;
                }
                
                asort($mapping, $sort_flags);
                
                $sorted = array();
                $prev_sort_key = $sort_key = "";
                foreach ($mapping as $k => $v) 
                {
                    $array_value = $array[$k];
                    $sort_key = '';
                    if (!is_array($key)) $sort_key = $array_value[$key];
                    else foreach ($key as $key_key) $sort_key .= isset($array_value[$key_key]) ? $array_value[$key_key] : '';
                    $sort_key = rtrim(ltrim($sort_key));
                    if (empty($sort_key)) $sort_key = "EMPTY_KEY_VALUE"; 

                    if ($prev_sort_key != $sort_key)
                    {
                        $normal_sort_key = str_replace("<strong>(SUB TOTAL)</strong>", "", $prev_sort_key);
                        if (empty($normal_sort_key)) $normal_sort_key = "EMPTY_KEY_VALUE"; 

                        if ($prev_sort_key != "" && isset($subtotals[$prev_sort_key]))
                            $sorted[] = $subtotals[$prev_sort_key];

                        $prev_sort_key = $sort_key;
                    }

                    $sorted[] = $array[$k];
                }
                
                if ($prev_sort_key != "")
                {
                    $normal_sort_key = str_replace("<strong>(SUB TOTAL)</strong>", "", $prev_sort_key);
                    if (isset($subtotals[$prev_sort_key])) $sorted[] = $subtotals[$prev_sort_key];
                }
                
                if (count($mapping) <= 0 && count($array) > 0 && count($subtotals) > 0 && count($sorted) <= 0)
                    $sorted = $subtotals;

                return $sorted;
            }
        }

        return $array;
    }


    public static function generateMatrix($report_data, $group_fields, $matrix_field, $start_date, $end_date)
    {
        // first create "matrix columns" as per date range selection
        $matrix_columns = array();
        $matrix_start_date = $start_date;
        $loop_control = 1; 
        while ($loop_control <= 25 && strtotime($matrix_start_date) <= strtotime($end_date))
        {
            $matrix_column_value = date("M Y", strtotime($matrix_start_date));
            if (!in_array($matrix_column_value, $matrix_columns)) $matrix_columns[] = $matrix_column_value;
            
            $month_days = date("t", strtotime($matrix_start_date));
            $matrix_start_date = date("m/d/Y", strtotime("+$month_days Days", strtotime($matrix_start_date)));
            $loop_control += 1;
        }
        if (count($matrix_columns) <= 0) $matrix_columns[] = date("M Y", strtotime($matrix_start_date));
        $matrix_columns[] = "TOTAL";
        
        // we need column wise as well as row wise totals
        $loop_control = 0;
        $total_row = array();
        foreach ($group_fields as $group_field)
        {
            if (!$loop_control) $total_row[$group_field] = "<strong>T O T A L</strong>";
            else $total_row[$group_field] = "&nbsp;";
            $loop_control += 1;
        }
        foreach ($matrix_columns as $matrix_column) $total_row[$matrix_column] = 0;
        
        // now process the data
        $return_data = array();
        foreach ($report_data as $report_row)
        {
            // build a key using various group by fields
            $row_key = "";
            $row_key_fields = array();
            foreach ($group_fields as $group_field)
            {
                $field_value = isset($report_row[$group_field]) ? $report_row[$group_field] : "";
                if ($row_key == "") $row_key = $field_value; else $row_key .= "~|~" . $field_value;
                $row_key_fields[$group_field] = $field_value;
            }
            if ($row_key == "") continue;
            
            // is this already processed key?
            if (!isset($return_data[$row_key]))
            {
                $return_row = $row_key_fields;
                foreach ($matrix_columns as $matrix_column) $return_row[$matrix_column] = 0;
                $return_data[$row_key] = $return_row;
            }
            else $return_row = $return_data[$row_key];

            // one final check to make sure that date range is being followed properly            
            $matrix_field_value = isset($report_row[$matrix_field]) ? $report_row[$matrix_field] : date("Y-m-d");
            if (strtotime($matrix_field_value) < strtotime($start_date) || strtotime($matrix_field_value) > strtotime($end_date)) continue;

            // build the current data row's matrix key
            $matrix_field_value_key = date("M Y", strtotime($matrix_field_value));
            if (!isset($return_row[$matrix_field_value_key])) $return_row[$matrix_field_value_key] = 0;
            $return_row[$matrix_field_value_key] += 1;
            $return_row["TOTAL"] += 1;
            $return_data[$row_key] = $return_row;
            
            // also add these numbers to the total row
            if (!isset($total_row[$matrix_field_value_key])) $total_row[$matrix_field_value_key] = 0;
            $total_row[$matrix_field_value_key] += 1;
            $total_row["TOTAL"] += 1;
        }
        
        // and return the data built in this manner (after sorting as required)
        $return_data = Utility::msort($return_data, $group_fields);
        $return_data["TOTAL_ROW_KEY_SHOULD_ALWAYS_BE_SOMETHING_DIFFERENT_I.E._UNIQUE"] = $total_row;
        return array('report_data' => $return_data, 'matrix_columns' => $matrix_columns);
    }


	public static function pdfTable($content = array(), $filename = 'some_pdf_output_from_civicgov.pdf', $orientation = '')
	{
		// load tcpdf library
		Yii::import('application.vendors.*');
		require_once('tcpdf/tcpdf.php');
		require_once('tcpdf/config/lang/eng.php');

		// create PDF class
        $header_image_file_name = self::pdfheader($orientation);
        $pdf = new MYPDF($orientation, 'mm', 'LETTER');
        $pdf->SetCreator("CIVICgov");
		$pdf->SetAuthor('CIVICgov');
		$pdf->SetTitle('CIVICgov Online Application');

		// set margins and fonts
		$pdf->SetAutoPageBreak(true, 15);
		if ($orientation == "L" || $orientation == "P" || empty($orientation)) 
        {
            $header_height = 0;
            if (self::$_header_height) $header_height = intval(ceil(self::$_header_height / 6));
            else $header_height = 40;
            $pdf->_header_height = $header_height;
            $pdf->SetMargins(8, $header_height + 2, 8);
		    $pdf->SetFont('dejavusans', '', 8);
        }
        else $pdf->SetFontSize(8);
        
		// set header and footer
        if ($orientation == "L" || $orientation == "P" || empty($orientation)) 
        {
    		$pdf->setHeaderMargin(5);
            $pdf->setX(100);
    		$pdf->setHeaderData('../../../..' . $header_image_file_name, ($orientation == 'L' ? 180 : 95));
        }
        else $pdf->setPrintHeader(false);
        
		$pdf->setPrintFooter(true);
        $pdf->SetFooterMargin(15);

		// do not use HTML calls but direct data print calls
		$pdf->dataTable($content);
        
		// output directly to the browser
		$pdf->Output($filename, 'I');		
	}

    /*
     * calculate the area of a polygon, in whatever units it's in
     */
    public static function getAreaOfPolygon($geometry) {
        $area = 0;
        for ($ri=0, $rl=sizeof($geometry->rings); $ri<$rl; $ri++) {
            $ring = $geometry->rings[$ri];

            for ($vi=0, $vl=sizeof($ring); $vi<$vl; $vi++) {
                $thisx = $ring[ $vi ][0];
                $thisy = $ring[ $vi ][1];
                $nextx = $ring[ ($vi+1) % $vl ][0];
                $nexty = $ring[ ($vi+1) % $vl ][1];
                $area += ($thisx * $nexty) - ($thisy * $nextx);
            }
        }

        // done with the rings: "sign" the area and return it
        $area = abs(($area / 2));
        return $area;
    }

    /*
     * calculate the centroid of a polygon
     * return a 2-element list: array($x,$y)
     */
    public static function getCentroidOfPolygon($geometry) {
        $cx = 0;
        $cy = 0;

        for ($ri=0, $rl=sizeof($geometry->rings); $ri<$rl; $ri++) {
            $ring = $geometry->rings[$ri];

            for ($vi=0, $vl=sizeof($ring); $vi<$vl; $vi++) {
                $thisx = $ring[ $vi ][0];
                $thisy = $ring[ $vi ][1];
                $nextx = $ring[ ($vi+1) % $vl ][0];
                $nexty = $ring[ ($vi+1) % $vl ][1];

                $p = ($thisx * $nexty) - ($thisy * $nextx);
                $cx += ($thisx + $nextx) * $p;
                $cy += ($thisy + $nexty) * $p;
            }
        }

        // last step of centroid: divide by 6*A
        $area = Utility::getAreaOfPolygon($geometry);
        $cx = -$cx / ( 6 * $area);
        $cy = -$cy / ( 6 * $area);

        // done!
        return array($cx,$cy);
    }



}


// load tcpdf library
Yii::import('application.vendors.*');
require_once('tcpdf/tcpdf.php');
require_once('tcpdf/config/lang/eng.php');

class MYPDF extends TCPDF {

    public $_header_height = 0;

    private function addTableHeader($content) {
        $content_data = $content['data'];
        $content_headers = $content['header'];
        $column_alignment = $content['align'];

        $margins = $this->getMargins();
        $write_area = $this->getPageWidth() - $margins['left'] - $margins['right'];

        $column_count = count($content_data[0]);
        $cell_width = $write_area / $column_count;            
        $cellcount = 1;

        $startX = $this->GetX();
        $startY = $this->GetY();

        $this->SetFillColor(199, 199, 199);
        $this->setFont('', 'B');
        $this->SetFontSize(8);
        $i = 0;
        foreach ($content_headers as $column_header)
        {
            $align = 'L';
            if (isset($column_alignment[$i])) $align = $column_alignment[$i];
            $currentcellcount = $this->MultiCell($cell_width, 5, $column_header, 0, $align, true, 0);
            if ($currentcellcount > $cellcount) $cellcount = $currentcellcount;                
            $i += 1;
        }
        
        if ($cellcount <= 0) $cellcount = 1;
        else
        {
            if ($cellcount > 1)
            {
                $this->SetX($startX);
                $this->SetY($startY);
                
                $i = 0;
                foreach ($content_headers as $column_header)
                {
                    $align = 'L';
                    if (isset($column_alignment[$i])) $align = $column_alignment[$i];
                    $this->MultiCell($cell_width, (($cellcount - 1) * 5) + 3, $column_header, 0, $align, true, 0);
                    $i += 1;
                }
            }
        }
        
        for ($i=1; $i<=$cellcount; $i++) 
        {
            if ($i == 1) 
            {
                if ($cellcount == 1) $this->Ln(5);
                else $this->Ln((($cellcount - 1) * 5) + 3);
            }
            $this->checkPageBreak(5);
        }

        $this->setFont('', '');
        $this->SetFontSize(8);
    }

    public function dataTable($content) {

        $content_data = $content['data'];
        $content_headers = $content['header'];
        $column_alignment = $content['align'];

        $this->AddPage();
        $this->SetTextColor(0);
        $this->SetFont('');

        $margins = $this->getMargins();
        $write_area = $this->getPageWidth() - $margins['left'] - $margins['right'];

        if (count($content_data))
        {
            $column_count = count($content_data[0]);
            $cell_width = $write_area / $column_count;            
            $cellcount = 1;

            if (isset($content['report_name']) && !empty($content['report_name']))
            {
                $this->setFont('', 'B');
                $this->SetFontSize(9);
            
                $currentcellcount = $this->MultiCell(0, 5, $content['report_name'], 0, 'L', false, 0);
                if ($currentcellcount == 1) $this->Ln(5);
                else $this->Ln((($currentcellcount - 1) * 5) + 3);                

                $this->setFont('', '');
                $this->SetFontSize(8);
            }

            $this->addTableHeader($content);

            $this->setFont('', '');
            $this->SetFontSize(8);
            foreach ($content_data as $row)
            {
                $cellcount = 1;
                
                for ($i=0; $i<$column_count; $i++)
                {
                    $colspan = 1;
                    if (strpos($row[$i], 'colspan=') !== false)
                    {
                        list($colspan_params, $row_text) = explode("~", $row[$i]);
                        list($colspan_tag, $colspan) = explode("=", $colspan_params);
                        $row[$i] = $row_text;
                    }
                    if ($colspan == 0) continue;
                    
                    if (stripos($row[$i], '<strong>') !== false)
                    {
                        $this->setFont('', 'B');
                        $row[$i] = str_replace('<strong>', '', $row[$i]);
                        $row[$i] = str_replace('</strong>', '', $row[$i]);
                    }
                    else $this->setFont('', '');
                    
                    $align = 'L';
                    if (isset($column_alignment[$i])) $align = $column_alignment[$i];
                    if (strpos($row[$i], '<a href=') !== false)
                    {
                        $currentcellcount = 1;
                        $this->writeHTMLCell($colspan * $cell_width, 5, $this->GetX(), $this->GetY(), $row[$i]);
                    }
                    else $currentcellcount = $this->MultiCell($colspan * $cell_width, 5, $row[$i], 0, $align, false, 0);
                    if ($currentcellcount > $cellcount) $cellcount = $currentcellcount;
                }
                
                if ($cellcount <= 0) $cellcount = 1;
                for ($i=1; $i<=$cellcount; $i++) 
                {
                    if ($i == 1) 
                    {
                        if ($cellcount == 1) $this->Ln(5);
                        else $this->Ln((($cellcount - 1) * 5) + 3);
                    }
                    
                    if ($cellcount == 1)
                    {
                        if ($this->checkPageBreak(5)) $this->addTableHeader($content);
                    }
                    else
                    {
                        if ($this->checkPageBreak((($cellcount - 1) * 5) + 3)) $this->addTableHeader($content);
                    }
                }
            }
        }        
        
    }

    //Page header
    public function Header() {
        // Logo
		$base_path = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->request->getBaseUrl();
        $image_file = $base_path . '/images/uploads/' . Yii::app()->session['user_id'] . '_pdfheader.png';
        if ($this->CurOrientation == "P") $this->Image($image_file, 55, 3, 95);
        else $this->Image($image_file, 90, 3, 95);
        $margins = $this->getMargins();
        $y_cd = $margins['top'] - 1;
        if ($this->_header_height) $y_cd = $this->_header_height;
        $this->Line($margins['left'], $y_cd, $this->getPageWidth() - $margins['left'], $y_cd);
    }

    public function Footer() {
        $this->SetFont('helvetica', 'I', 8);

        $margins = $this->getMargins();
        $this->Line($margins['left'], $this->GetY(), $this->getPageWidth() - $margins['left'], $this->GetY());
        
        // $this->Cell($cell_width, 10, "Civicgov Portal", 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetY(-12);
	    $this->writeHTML("&copy; CIVICGov Online Portal", true, false, true, false, '');
        $this->SetY(-15);
        $this->Cell($this->getPageWidth(), 10, 
            'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 10, date("F j, Y h:iA"), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

}