<?php
class StatusMgr {

    public static function InvByType($key='') {
        $arr = array(1=>'Vendor',2=>'Internal');
        if(!empty($key) && !empty($arr[$key])) {
            return $arr[$key];
        }
        return $arr;
    }
    public static function InvStatus($key='') {
        $arr = array('1'=>'Waiting for Approval',2=>'Approved');
        if(!empty($key) && !empty($arr[$key])) {
            return $arr[$key];
        }
        return $arr;
    }
   
}