<?php
class MessageManager {
    // side Bar tooltip only.. //
    public static function tooltipsName($mgs= '') {
       $text = 'data-toggle="tooltip" data-placement="right" title="'. $mgs.'" ';
        return $text;
    }
    // All tooltip
    public function tooltipText(){

      if(FunctionManager::savingSpecific()){
        $savingMilestoneDueDate = 'When is this Finaincials due?';
      }else{
        $savingMilestoneDueDate = '';
      }
      $label = array();
      $label['sandboxView']        = 'Click on the Sandbox View button to see what your oboloo system will look like once you have created eSourcing activities, suppliers, contracts and savings records.  You are unable to create any records whilst in sandbox view';

      // Quote label
      $label['quoteItemlebal']       = 'Add items below that you would like your suppliers to quote on';
      $label['estimatedTotallebal']  = 'How much do you think this quote will cost? (This will not be seen by the suppliers and will only be used to calculate savings)';
      $label['openingDateLabel']     = 'Suppliers can begin to respond to the quote after this date & time';
      $label['closingDateLabel']     = 'Suppliers will not be able to respond to the quote after this date and time';
      $label['quoteDocumentLabel']   = 'Attach any additional documents for your suppliers';
      $label['toolTipCurrencyLabel'] = 'You can add more currencies under the configurations menu';
      $label['productQuote ']        = 'If you know this it could help your suppliers identify the product easier';
      $label['unitMeasure']          = 'How is this product measured? e.g - each, KG, hour, meter, day etc';


      // Contract Management label
      $label['referenceLabel']         = 'This helps you and your organisation easily identify this contract later';
      $label['contractStartDateLabel'] = 'When did this contract start?';
      $label['contractEndDateLabel']   = 'If this contract does not have an end date then please put it 12 or 24 months from the start date';
      $label['noticePeriodDateLabel']  = 'When do you need to give the supplier notice that this contract is coming to an end/wont be renewed?';
      $label['estimatedTotalLabel']    = 'How much will this contract be worth over its full active period?';
      $label['descriptionLabel']        = 'Let others in your organisation know what this contract is for';
      $label['contractCategoryLabel']    = 'You can add new categories under the configurations page in settings';
      $label['contractSubCategoryLabel'] = 'You can add new sub categories under the configurations page in settings';
      $label['savingsCurrencyLabel']     = 'You can add more currencies under the configurations menu';
      $label['supplierIndustryLabel']    = 'You can add new industries under the configurations page in settings';
      $label['supplierSubIndustryLabel'] = 'You can add new sub industries under the configurations page in settings';

      // Saveing Management Label
      $label['savingsTitle']             = 'Give your savings project a name';
      $label['savingsCategory']          = 'You can add new categories under the configurations page in settings';
      $label['savingsCurrency']          = 'You can add more currencies under the configurations menu';
      $label['savingsSubCategory']       = 'You can add new sub categories under the configurations page in settings';
      $label['associatedContract']       = 'Link a contract from the contract Management module to this savings project';
      $label['addMilestoneTitle']        = 'Here you can create individual savings records under your savings project';
      $label['dueDate']                  = 'When is this milestone due?';
      $label['milestonedueDate']         = 'When is this Financials due?';
      $label['spendMilestone']           = 'How much spend is this savings milestone against?';

      
      $label['savingMilestoneDueDate']   = $savingMilestoneDueDate;
      $label['baselineSpend']            = 'e.g. the total amount being spent through this supplier/category or sub category';
      $label['projectedCostReduction']   = 'How much will you save via cost reduction within this milestone?';
      $label['projectedCostAvoidance']   = 'How much will you save via cost avoidance within this milestone?';
      $label['totalProjectSavings']      = 'This is your total calculated savings from all of your milestones';
      $label['totalProjectSavingsPerce'] = 'This is the % you will save within this project (Total savings ÷ Total Baseline Spend)';
      $label['supplierIndustry']         = 'You can add new industries under the configurations page in settings';
      $label['supplierSubIndustry']      = 'You can add new sub industries under the configurations page in settings';

      // user label 
       $label['roleTool']                = 'This gives users different access rights';
       $label['roleLogTool']             = 'Only active suppliers can log into oboloo';

      // Company details
      $label['selectCurrency']  = 'What currency does your organisation mainly work in? WARNING - changing this could effect exchange rates in oboloo';
      $label['selectDate']      = 'What date format does your organisation mainly work in?';
        
      return $label;
    }


}