<?php

class ValidateSavingApprovalHistory extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'saving_validate_approval_history');
		$this->timestamp = false;
	}

	public function getHistroy($saving_id)
	{
			$sql = "SELECT * FROM saving_validate_approval_history his
				 WHERE his.saving_id = $saving_id ORDER BY  his.id DESC";
				 
		return $this->executeQuery($sql);
	}

	public function getApprovedDate($saving_id)
	{
			$sql = "SELECT approver_datetime FROM saving_validate_approval_history his
				 WHERE his.saving_id = $saving_id ORDER BY  his.id DESC";

				 
		return Yii::app()->db->createCommand($sql)->queryRow();
	}

	 
}
