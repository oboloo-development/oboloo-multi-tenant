<?php

/**
 * An object of this class represents a row in the "users"
 * table in the database. The class provides methods for
 * various user activities such as registration, activation,
 * forgot password, login and change status.
 */


class User extends Common
{
	/**
	 * Regular model class constructor (derived from Common)
	 * id is the primary key while users is the table name
	 */
	public function __construct()
	{
		$this->fields = array (
			'user_id' => 'N',
			'location_id' => 'N',
			'department_id' => 'N',
			'username' => 'C',
			'email' => 'C',
            'language_code'=>'C',
            'contact_number'=>'C',
			'password' => 'C',
			'full_name' => 'C',
            'position' =>'C',
			'user_type_id' => 'L',
			'status' => 'N',
            'status_payment' => 'N',
            'google_secret_code'=>'N'
		);
		$this->alternate_key_name = 'username';
		parent::__construct('user_id', 'users');
	}

	/**
 	 * \param $username - login username entered by the user on the login form - mandatory
 	 * \param $password - password entered by the user on the login form - mandatory
 	 * \return $user_data - user data matching the username and password combination above if one exists
	 *
	 * This method is used to select a row from the users table matching the input
	 * username and password (stored after encryption), if such a row does exist
	 */
	public function login($username, $password)
	{ 
		return $this->getOne(array('username' => $username, 'password' => md5($password)));
	}

    public function loginWithoutEncode($username, $password)
    { 
        
        return $this->getOne(array('username' => $username, 'password' => $password));

    }

	/**
 	 * \return $is_user_logged_in - whether someone has logged in to the system or not
	 *
	 * This method is used to check if someone has been logged in to the application or not.
	 * This is used to determine whether to redirect the user to the login page OR then if
	 * the user is logged in, then what kind of page layout is to be used for rendering etc
	 */
	public function isLoggedIn()
	{
		return isset(Yii::app()->session['user_id']) && !empty(Yii::app()->session['user_id']) && isset(Yii::app()->session['2fa']);
	}

	public function getUsers($user_id = 0,$softDeleted=0)
    {
        
        if ($softDeleted==0){
            $sDeleted = " soft_deleted=0 ";
        }else if($softDeleted==1){
             $sDeleted = " soft_deleted =1 ";
        }else {
             $sDeleted = " soft_deleted in(0,1) ";
        }
        $sDeleted .= " and ";
        

        $sql = "SELECT u.*, t.value AS user_type
                  FROM users u
                  LEFT JOIN user_types t ON u.user_type_id = t.id";

        if ($user_id)
            return $this->executeQuery($sql . " WHERE $sDeleted  u.user_id = $user_id  ", true);
        else return $this->executeQuery($sql . " WHERE $sDeleted u.admin_flag =0 ORDER BY full_name ");

        
    }

	public function isBadPassword($password)
	{
		if (strlen($password) < 8) return 'Password must be at least 8 character long';
		if (!preg_match('/[a-z]/', $password)) return 'Password must contain at least one lower case character';
		if (!preg_match('/[A-Z]/', $password)) return 'Password must contain at least one upper case character';
		if (!preg_match('/[0-9]/', $password)) return 'Password must contain at least one digit i.e. numeric character';
		return "";
	}

	public function forgot($username, $email)
	{
		$user_id = 0;
		$user_data = $this->getOne(array('email' => $email));
		if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
			$user_id = $user_data['user_id'];
		
		if (!$user_id)
		{
			$user_data = $this->getOne(array('username' => $username));
			if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
				$user_id = $user_data['user_id'];
		}
		
		if (!$user_id) return 1;
		else
		{
			$new_password = $this->generateRandomString(10);
			$this->executeQuery("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id");					

			$mail = new \PHPMailer(true);					
			$mail->IsSMTP();
			$mail->Host = Yii::app()->params['host'];
			$mail->Port = Yii::app()->params['port'];
			$mail->SMTPAuth = true;
			$mail->Username = Yii::app()->params['username'];
			$mail->Password = Yii::app()->params['password'];
			$mail->SMTPSecure = Yii::app()->params['stmpSecure'];
			$mail->CharSet="UTF-8";

			$email_body = '';

			$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
			$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 60%;">
                     <span style="color:#10a798;"><strong>Forgot Password&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
            $email_body .= '<p>This is a notification from Spend 365. As per your request, your password has been changed as below:</p>';
			$email_body .= '<strong>Username: </strong>' . $user_data['username'] . '<br/>';
			$email_body .= '<strong>Email: </strong>' . $user_data['email'] . '<br/>';
			$email_body .= '<strong>Password: </strong>' . $new_password . '<br/>';
			$email_body .= '<p><strong>NOTE: </strong>If you did not request this change, please contact your administrator immediately.<p/>';
			$email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';
			$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
			$mail->addAddress($user_data['email'], $user_data['full_name']);
			$mail->Subject = 'Spend 365: Password Change';
			$mail->isHTML(true);
			$mail->Body = $email_body;			
			$mail->send();
			
			return 0;
		}
	}

    public function changePassword($username, $email)
    {
        $user_id = 0;
        $user_data = $this->getOne(array('email' => $email));
        if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
            $user_id = $user_data['user_id'];
        
        if (!$user_id)
        {
            $user_data = $this->getOne(array('username' => $username));
            if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
                $user_id = $user_data['user_id'];
        }
        
        if (!$user_id) return 1;
        else
        {
            $new_password = $this->generateRandomString(10);
           // $this->executeQuery("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id");                   

            $mail = new \PHPMailer(true);                   
            $mail->IsSMTP();
            $mail->Host = Yii::app()->params['host'];
            $mail->Port = Yii::app()->params['port'];
            $mail->SMTPAuth = true;
            $mail->Username = Yii::app()->params['username'];
            $mail->Password = Yii::app()->params['password'];
            $mail->SMTPSecure = Yii::app()->params['port'];
			$mail->CharSet="UTF-8";

            $email_body = '';

            $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
            $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 60%;">
                     <span style="color:#10a798;"><strong>Change Password&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
            $email_body .= '<p>This is a notification from Spend 365. Please follow the link below to change your password:</p>';
            $email_body .= '<strong>Username: </strong>' . $user_data['username'] . '<br/>';
            $email_body .= '<strong>Email: </strong>' . $user_data['email'] . '<br/>';
            $email_body .= '<strong>Change Password Link: </strong><a href="' .Yii::app()->createAbsoluteUrl('app/changePasswordSave',array("user-id"=>base64_encode($user_id),'time'=>base64_encode(date("m-d"))))  . '" title="Change Password">'.Yii::app()->createAbsoluteUrl('app/changePasswordSave',array("user-id"=>base64_encode($user_id),'time'=>base64_encode(date("m-d")))).'</a><br/>';
            $email_body .= '<p><strong>NOTE: </strong>If you did not request this change, please contact your administrator immediately.<p/>';
            $email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';
            $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
            $mail->addAddress($user_data['email'], $user_data['full_name']);
            $mail->Subject = 'Spend 365: Password Change';
            $mail->isHTML(true);
            $mail->Body = $email_body;          
            $mail->send();
            
            return 0;
        }
    }

     public function changePasswordSave($user_data,$new_password )
    {        
    
    if (true) {                          

     $mail = new \PHPMailer(true);                   
     $mail->IsSMTP();
     $mail->Host = Yii::app()->params['host'];
     $mail->Port = Yii::app()->params['port'];
     $mail->SMTPAuth = true;
     $mail->Username = Yii::app()->params['username'];
     $mail->Password = Yii::app()->params['password'];
     $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
	 $mail->CharSet="UTF-8";

     $email_body = ''; 

     $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
     <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
            $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" />';
            $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 60%;">
                     <span style="color:#10a798;"><strong>Change Password&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
            $email_body .= '<p>This is a notification from Spend 365. Your password changed successfully:</p>';
            
            $email_body .= '<p><strong>NOTE: </strong>If you did not request this change, please contact your administrator immediately.<p/>';
            $email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
        
         $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
         $mail->addAddress($user_data['email'], $user_data['full_name']);
         $mail->Subject = 'Spend 365: Password Change';
         $mail->isHTML(true);
         $mail->Body = $email_body;          
         $mail->send();
         return 0;
        }
    }

	public function sendRegistrationEmail($password)
	{
		$mail = new \PHPMailer(true);					
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];


		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
       <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
		$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
		$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#10a798;"><strong>Account Creation&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		$email_body .= '<p>This is a notification from the procurement system. Your account is now created as per details below:</p>';
		$email_body .= '<strong>Username: </strong>' . $this->rs['username'] . '<br />';
		$email_body .= '<strong>Email: </strong>' . $this->rs['email'] . '<br />';
		$email_body .= '<strong>Password: </strong>' . $password . '<br />';
		$email_body .= '<br /><strong>NOTE: </strong>Please contact your administrator to activate your account.<br />';
		$email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';


		$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
		$mail->addAddress($this->rs['email'], $this->rs['full_name']);
		$mail->Subject = 'Spend 365: Account Created';
		$mail->isHTML(true);
		$mail->Body = $email_body;			
		$mail->send();
	}

	public function activateUser($username, $email,$password)
	{
		$user_id = 0;
		$user_data = $this->getOne(array('email' => $email));
		if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
			$user_id = $user_data['user_id'];

		if (!$user_id)
		{
			$user_data = $this->getOne(array('username' => $username));
			if ($user_data && is_array($user_data) && isset($user_data['user_id']) && $user_data['user_id'])
				$user_id = $user_data['user_id'];
		}

		if (!$user_id) return 1;
		else
		{
			if(!empty($password)){
                $new_password = $password;
            } else{
                $new_password = $this->generateRandomString(10);
                $this->executeQuery("UPDATE users SET password = MD5('$new_password') WHERE user_id = $user_id");
            }

			$mail = new \PHPMailer(true);
			$mail->IsSMTP();
			$mail->Host = Yii::app()->params['host'];
			$mail->Port = Yii::app()->params['port'];
			$mail->SMTPAuth = true;
			$mail->Username = Yii::app()->params['username'];
			$mail->Password = Yii::app()->params['password'];
			$mail->SMTPSecure = Yii::app()->params['stmpSecure'];
			$mail->CharSet="UTF-8";

			$email_body = '';
		    $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                    <tr>
                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                    <div style="margin:0px auto;max-width:600px;">
                    <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                    <tr>
                    <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                    <td style="vertical-align:top;width:600px;">
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                    <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                     <tbody>
                     <tr>
                      <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                      <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                          <tbody>
                          <tr>
                              <td style="width:600px;">';
            			$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
            			$email_body .= '</td>
                          </tr>
                          </tbody>
                      </table>
                      </td>
                     </tr>
                     <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                         <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
                     </td>
                     </tr>
                     <tr>
                     <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                             <h1 style="font-family: Cabin; line-height: 100%;">
                                 <span style="color:#10a798;"><strong>User Activation&nbsp;</strong></span>
                             </h1>
                         </div>
                     </td>
                     </tr>

                     <tr>
                         <td style="word-wrap:break-word;font-size:0px;">
                         <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
            			$email_body .= '<p>Dear ' . $user_data['full_name'].', </p>';
            			$email_body .= '<p>This is a notification from Spend 365. As per your request, your account has been activated as below:</p>';
            			$email_body .= '<strong>Username: </strong>' . $user_data['username'] . '<br/>';
            			$email_body .= '<strong>Email: </strong>' . $user_data['email'] . '<br/>';
            			$email_body .= '<strong>Password: </strong>' . $new_password . '<br/>';
            			$email_body .= '<strong>URL: </strong><a target="_blank" href="'.Yii::app()->createAbsoluteUrl('app/login').'">'.Yii::app()->createAbsoluteUrl('app/login').'</a><br/>';
            			$email_body .= '<p><strong>NOTE: </strong>If you did not request this change, please contact your administrator immediately.<p/>';
            			$email_body .= '</div>
                         </td>
                     </tr>

                     <tr>
                         <td style="word-wrap:break-word;font-size:0px;">
                         <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                         </td>
                     </tr>

                     <tr>
                         <td style="word-wrap:break-word;font-size:0px;" align="left">
                             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                                 <p>Terms of Service &amp; User Agreement</p>
                                 <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                                 <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                             </div>
                         </td>
                     </tr>
                     </tbody>
                    </table>
                    </div>
                    </td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </div></td>
                    </tr>
                </table>
            </div>';

			$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
			$mail->addAddress($user_data['email'], $user_data['full_name']);
			$mail->Subject = 'Spend 365: User Activation';
			$mail->isHTML(true);
			$mail->Body = $email_body;
			$mail->send();

			return 0;
		}
	}

	public function generateRandomString($length = 10) 
	{
	    $characters = '0123456789%@*abcdefghijklmnop!~qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++)
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    return $randomString;
	}

    public function saveUserPrivilege($privileg_data)
    {
        $user_id =  isset($privileg_data['user_id']) ? $privileg_data['user_id'] : 0;
        $user_data = $this->executeQuery("SELECT * FROM user_privilege WHERE user_id = $user_id");   
        if (count($user_data)>0){
            $this->executeQuery("DELETE FROM user_privilege WHERE user_id = $user_id");
        }

        $locArr = !empty($privileg_data['locationDepartment'])?$privileg_data['locationDepartment']:'';
        $user_type_id = isset($privileg_data['user_type_id']) ? $privileg_data['user_type_id'] : 0;
        
        if(!empty($locArr))
        foreach($locArr as $locKey=>$deptArr){
            foreach($deptArr as $deptKey=>$privilegValue){
              
                if(true){
                   $approve_order = !empty($privilegValue['approve_order'])?'yes':'no';
                   $approve_travel_expense = !empty($privilegValue['approve_travel_expense'])?'yes':'no';
                   $approve_invoice = !empty($privilegValue['approve_invoice'])?'yes':'no';
                   $amend_budget    = !empty($privilegValue['amend_budget'])?'yes':'no';
                   $vendor_document   = !empty($privilegValue['vendor_document'])?'yes':'no';
                   $contract_document = !empty($privilegValue['contract_document'])?'yes':'no';
                   $contract_view = !empty($privilegValue['contract_view'])? 'yes':'no';
                   $contract_edit = !empty($privilegValue['contract_edit'])? 'yes':'no';
                   $saving_view   = !empty($privilegValue['saving_view']) ? 'yes':'no';
                   $saving_edit   = !empty($privilegValue['saving_edit']) ? 'yes':'no';
                   $eSourcing_view= !empty($privilegValue['eSourcing_view']) ? 'yes':'no';
                   $createdBy     = $user_id;
                   $createdAt     = date("Y-m-d H:i:s");
                    
                    $sql = "INSERT INTO `user_privilege` 
                    (`user_id`,
                    `user_type_id`, 
                    `location_id`,
                    `department_id`,
                    `approve_order`,
                    `approve_travel_expense`,
                    `approve_invoice`,
                    `amend_budget`,
                    `vendor_document`,
                    `contract_document`,
                    `contract_view`,
                    `contract_edit`,
                    `saving_view`,
                    `saving_edit`,
                    `eSourcing_view`,
                    `created_by_id`, 
                    `created_datetime`, 
                    `updated_datetime`) VALUES 
                    ('".$user_id."', 
                    '".$user_type_id."',
                    '".$locKey."',
                    '". $deptKey."',
                    '".$approve_order."',
                    '".$approve_travel_expense."',
                    '".$approve_invoice."',
                    '".$amend_budget."',
                    '".$vendor_document."',
                    '".$contract_document."',
                    '".$contract_view."',
                    '".$contract_edit."',
                    '".$saving_view."',
                    '".$saving_edit."',
                    '".$eSourcing_view."',
                    '".$createdBy."',
                    '".$createdAt."',
                    '".$createdAt."')";
                     $this->executeQuery($sql);
                }
            }
        }
    }

   /* public function checkPermission($userID,$locationID,$departmentID,$fieldName=''){
     $sql="select $fieldName FROM user_privilege WHERE user_id = $userID and location_id=$locationID and department_id=$departmentID";
     $checkRecord = Yii::app()->db->createCommand($sql)->queryRow();

      return $checkRecord;
    }*/

    public function checkPermission($userID,$locationID,$departmentID,$fieldName=''){
        
      $sql="select $fieldName FROM user_privilege WHERE user_id = $userID ";

        if(!empty($locationID)){
            $sql .=" and location_id=$locationID";
        }
        if(!empty($departmentID)){
            $sql .=" and department_id=$departmentID";
        }
        if(!empty($fieldName)){
            $sql .=" and $fieldName='yes'";
        }
        
        $checkRecord = Yii::app()->db->createCommand($sql)->queryRow();

        return $checkRecord;
    }



    public function saveUserACL($acl_data)
    {



        // $lastId = $this->executeQuery("SELECT *  FROM users order by user_id DESC LIMIT 0,1 ", 1);
        //CVarDumper::dump($lastId['user_id'],10,1);die;
        $user_id =  isset($acl_data['user_id']) ? $acl_data['user_id'] : 0;
        $user_data = $this->executeQuery("SELECT * FROM user_acl WHERE user_id = $user_id");
        if (count($user_data)>0){
            $this->executeQuery("DELETE FROM user_acl WHERE user_id = $user_id");
        }
        $london_procurement_location =  isset($acl_data['london_procurement_location']) ? $acl_data['london_procurement_location'] : 0;
        $london_procurement_department =  isset($acl_data['london_procurement_department']) ? $acl_data['london_procurement_department'] : 0;
        $london_procurement_order =  isset($acl_data['london_procurement_order']) ? 1 : 0;
        $london_procurement_travel_expense =  isset($acl_data['london_procurement_travel_expense']) ? 1 : 0;
        $london_procurement_invoice =  isset($acl_data['london_procurement_invoice']) ? 1 : 0;
        $london_procurement_amend_budget =  isset($acl_data['london_procurement_amend_budget']) ? 1 : 0;

        $london_admin_location =  isset($acl_data['london_admin_location']) ? $acl_data['london_admin_location'] : 0;
        $london_admin_department =  isset($acl_data['london_admin_department']) ? $acl_data['london_admin_department'] : 0;
        $london_admin_order =  isset($acl_data['london_admin_order']) ? 1 : 0;
        $london_admin_travel_expense =  isset($acl_data['london_admin_travel_expense']) ? 1 : 0;
        $london_admin_invoice =  isset($acl_data['london_admin_invoice']) ? 1 : 0;
        $london_admin_amend_budget =  isset($acl_data['london_admin_amend_budget']) ? 1 : 0;

        $london_it_location =  isset($acl_data['london_it_location']) ? $acl_data['london_it_location'] : 0;
        $london_it_department =  isset($acl_data['london_it_department']) ? $acl_data['london_it_department'] : 0;
        $london_it_order =  isset($acl_data['london_it_order']) ? 1 : 0;
        $london_it_travel_expense =  isset($acl_data['london_it_travel_expense']) ? 1 : 0;
        $london_it_invoice =  isset($acl_data['london_it_invoice']) ? 1 : 0;
        $london_it_amend_budget =  isset($acl_data['london_it_amend_budget']) ? 1 : 0;

        $london_facilities_location =  isset($acl_data['london_facilities_location']) ? $acl_data['london_facilities_location'] : 0;
        $london_facilities_department =  isset($acl_data['london_facilities_department']) ? $acl_data['london_facilities_department'] : 0;
        $london_facilities_order =  isset($acl_data['london_facilities_order']) ? 1 : 0;
        $london_facilities_travel_expense =  isset($acl_data['london_facilities_travel_expense']) ? 1 : 0;
        $london_facilities_invoice =  isset($acl_data['london_facilities_invoice']) ? 1 : 0;
        $london_facilities_amend_budget =  isset($acl_data['london_facilities_amend_budget']) ? 1 : 0;

        $berlin_admin_location =  isset($acl_data['berlin_admin_location']) ? $acl_data['berlin_admin_location'] : 0;
        $berlin_admin_department =  isset($acl_data['berlin_admin_department']) ? $acl_data['berlin_admin_department'] : 0;
        $berlin_admin_order =  isset($acl_data['berlin_admin_order']) ? 1 : 0;
        $berlin_admin_travel_expense =  isset($acl_data['berlin_admin_travel_expense']) ? 1 : 0;
        $berlin_admin_invoice =  isset($acl_data['berlin_admin_invoice']) ? 1 : 0;
        $berlin_admin_amend_budget =  isset($acl_data['berlin_admin_amend_budget']) ? 1 : 0;

        $berlin_it_location =  isset($acl_data['berlin_it_location']) ? $acl_data['berlin_it_location'] : 0;
        $berlin_it_department =  isset($acl_data['berlin_it_department']) ? $acl_data['berlin_it_department'] : 0;
        $berlin_it_order =  isset($acl_data['berlin_it_order']) ? 1 : 0;
        $berlin_it_travel_expense =  isset($acl_data['berlin_it_travel_expense']) ? 1 : 0;
        $berlin_it_invoice =  isset($acl_data['berlin_it_invoice']) ? 1 : 0;
        $berlin_it_amend_budget =  isset($acl_data['berlin_it_amend_budget']) ? 1 : 0;

        $berlin_facilities_location =  isset($acl_data['berlin_facilities_location']) ? $acl_data['berlin_facilities_location'] : 0;
        $berlin_facilities_department =  isset($acl_data['berlin_facilities_department']) ? $acl_data['berlin_facilities_department'] : 0;
        $berlin_facilities_order =  isset($acl_data['berlin_facilities_order']) ? 1 : 0;
        $berlin_facilities_travel_expense =  isset($acl_data['berlin_facilities_travel_expense']) ? 1 : 0;
        $berlin_facilities_invoice =  isset($acl_data['berlin_facilities_invoice']) ? 1 : 0;
        $berlin_facilities_amend_budget =  isset($acl_data['berlin_facilities_amend_budget']) ? 1 : 0;

        $mumbai_admin_location =  isset($acl_data['mumbai_admin_location']) ? $acl_data['mumbai_admin_location'] : 0;
        $mumbai_admin_department =  isset($acl_data['mumbai_admin_department']) ? $acl_data['mumbai_admin_department'] : 0;
        $mumbai_admin_order =  isset($acl_data['mumbai_admin_order']) ? 1 : 0;
        $mumbai_admin_travel_expense =  isset($acl_data['mumbai_admin_travel_expense']) ? 1 : 0;
        $mumbai_admin_invoice =  isset($acl_data['mumbai_admin_invoice']) ? 1 : 0;
        $mumbai_admin_amend_budget =  isset($acl_data['mumbai_admin_amend_budget']) ? 1 : 0;

        $mumbai_procurement_location =  isset($acl_data['mumbai_procurement_location']) ? $acl_data['mumbai_procurement_location'] : 0;
        $mumbai_procurement_department =  isset($acl_data['mumbai_procurement_department']) ? $acl_data['mumbai_procurement_department'] : 0;
        $mumbai_procurement_order =  isset($acl_data['mumbai_procurement_order']) ? 1 : 0;
        $mumbai_procurement_travel_expense =  isset($acl_data['mumbai_procurement_travel_expense']) ? 1 : 0;
        $mumbai_procurement_invoice =  isset($acl_data['mumbai_procurement_invoice']) ? 1 : 0;
        $mumbai_procurement_amend_budget =  isset($acl_data['mumbai_procurement_amend_budget']) ? 1 : 0;

        $newyork_procurement_location =  isset($acl_data['newyork_procurement_location']) ? $acl_data['newyork_procurement_location'] : 0;
        $newyork_procurement_department =  isset($acl_data['newyork_procurement_department']) ? $acl_data['newyork_procurement_department'] : 0;
        $newyork_procurement_order =  isset($acl_data['newyork_procurement_order']) ? 1 : 0;
        $newyork_procurement_travel_expense =  isset($acl_data['newyork_procurement_travel_expense']) ? 1 : 0;
        $newyork_procurement_invoice =  isset($acl_data['newyork_procurement_invoice']) ? 1 : 0;
        $newyork_procurement_amend_budget =  isset($acl_data['newyork_procurement_amend_budget']) ? 1 : 0;

        $newyork_admin_location =  isset($acl_data['newyork_admin_location']) ? $acl_data['newyork_admin_location'] : 0;
        $newyork_admin_department =  isset($acl_data['newyork_admin_department']) ? $acl_data['newyork_admin_department'] : 0;
        $newyork_admin_order =  isset($acl_data['newyork_admin_order']) ? 1 : 0;
        $newyork_admin_travel_expense =  isset($acl_data['newyork_admin_travel_expense']) ? 1 : 0;
        $newyork_admin_invoice =  isset($acl_data['newyork_admin_invoice']) ? 1 : 0;
        $newyork_admin_amend_budget =  isset($acl_data['newyork_admin_amend_budget']) ? 1 : 0;

        $newyork_it_location =  isset($acl_data['newyork_it_location']) ? $acl_data['newyork_it_location'] : 0;
        $newyork_it_department =  isset($acl_data['newyork_it_department']) ? $acl_data['newyork_it_department'] : 0;
        $newyork_it_order =  isset($acl_data['newyork_it_order']) ? 1 : 0;
        $newyork_it_travel_expense =  isset($acl_data['newyork_it_travel_expense']) ? 1 : 0;
        $newyork_it_invoice =  isset($acl_data['newyork_it_invoice']) ? 1 : 0;
        $newyork_it_amend_budget =  isset($acl_data['newyork_it_amend_budget']) ? 1 : 0;

        $userACL = new UserACL();
        $userACL->rs = array();
        $userACL->rs['user_id'] = $user_id;
        $userACL->rs['london_procurement_location'] = $london_procurement_location;
        $userACL->rs['london_procurement_department'] = $london_procurement_department;
        $userACL->rs['london_procurement_order'] = $london_procurement_order;
        $userACL->rs['london_procurement_travel_expense'] = $london_procurement_travel_expense;
        $userACL->rs['london_procurement_invoice'] = $london_procurement_invoice;
        $userACL->rs['london_procurement_amend_budget'] = $london_procurement_amend_budget;

        $userACL->rs['london_admin_location'] = $london_admin_location;
        $userACL->rs['london_admin_department'] = $london_admin_department;
        $userACL->rs['london_admin_order'] = $london_admin_order;
        $userACL->rs['london_admin_travel_expense'] = $london_admin_travel_expense;
        $userACL->rs['london_admin_invoice'] = $london_admin_invoice;
        $userACL->rs['london_admin_amend_budget'] = $london_admin_amend_budget;

        $userACL->rs['london_it_location'] = $london_it_location;
        $userACL->rs['london_it_department'] = $london_it_department;
        $userACL->rs['london_it_order'] = $london_it_order;
        $userACL->rs['london_it_travel_expense'] = $london_it_travel_expense;
        $userACL->rs['london_it_invoice'] = $london_it_invoice;
        $userACL->rs['london_it_amend_budget'] = $london_it_amend_budget;

        $userACL->rs['london_facilities_location'] = $london_facilities_location;
        $userACL->rs['london_facilities_department'] = $london_facilities_department;
        $userACL->rs['london_facilities_order'] = $london_facilities_order;
        $userACL->rs['london_facilities_travel_expense'] = $london_facilities_travel_expense;
        $userACL->rs['london_facilities_invoice'] = $london_facilities_invoice;
        $userACL->rs['london_facilities_amend_budget'] = $london_facilities_amend_budget;

        $userACL->rs['berlin_admin_location'] = $berlin_admin_location;
        $userACL->rs['berlin_admin_department'] = $berlin_admin_department;
        $userACL->rs['berlin_admin_order'] = $berlin_admin_order;
        $userACL->rs['berlin_admin_travel_expense'] = $berlin_admin_travel_expense;
        $userACL->rs['berlin_admin_invoice'] = $berlin_admin_invoice;
        $userACL->rs['berlin_admin_amend_budget'] = $berlin_admin_amend_budget;

        $userACL->rs['berlin_it_location'] = $berlin_it_location;
        $userACL->rs['berlin_it_department'] = $berlin_it_department;
        $userACL->rs['berlin_it_order'] = $berlin_it_order;
        $userACL->rs['berlin_it_travel_expense'] = $berlin_it_travel_expense;
        $userACL->rs['berlin_it_invoice'] = $berlin_it_invoice;
        $userACL->rs['berlin_it_amend_budget'] = $berlin_it_amend_budget;

        $userACL->rs['berlin_facilities_location'] = $berlin_facilities_location;
        $userACL->rs['berlin_facilities_department'] = $berlin_facilities_department;
        $userACL->rs['berlin_facilities_order'] = $berlin_facilities_order;
        $userACL->rs['berlin_facilities_travel_expense'] = $berlin_facilities_travel_expense;
        $userACL->rs['berlin_facilities_invoice'] = $berlin_facilities_invoice;
        $userACL->rs['berlin_facilities_amend_budget'] = $berlin_facilities_amend_budget;

        $userACL->rs['mumbai_admin_location'] = $mumbai_admin_location;
        $userACL->rs['mumbai_admin_department'] = $mumbai_admin_department;
        $userACL->rs['mumbai_admin_order'] = $mumbai_admin_order;
        $userACL->rs['mumbai_admin_travel_expense'] = $mumbai_admin_travel_expense;
        $userACL->rs['mumbai_admin_invoice'] = $mumbai_admin_invoice;
        $userACL->rs['mumbai_admin_amend_budget'] = $mumbai_admin_amend_budget;

        $userACL->rs['mumbai_procurement_location'] = $mumbai_procurement_location;
        $userACL->rs['mumbai_procurement_department'] = $mumbai_procurement_department;
        $userACL->rs['mumbai_procurement_order'] = $mumbai_procurement_order;
        $userACL->rs['mumbai_procurement_travel_expense'] = $mumbai_procurement_travel_expense;
        $userACL->rs['mumbai_procurement_invoice'] = $mumbai_procurement_invoice;
        $userACL->rs['mumbai_procurement_amend_budget'] = $mumbai_procurement_amend_budget;

        $userACL->rs['newyork_procurement_location'] = $newyork_procurement_location;
        $userACL->rs['newyork_procurement_department'] = $newyork_procurement_department;
        $userACL->rs['newyork_procurement_order'] = $newyork_procurement_order;
        $userACL->rs['newyork_procurement_travel_expense'] = $newyork_procurement_travel_expense;
        $userACL->rs['newyork_procurement_invoice'] = $newyork_procurement_invoice;
        $userACL->rs['newyork_procurement_amend_budget'] = $newyork_procurement_amend_budget;

        $userACL->rs['newyork_admin_location'] = $newyork_admin_location;
        $userACL->rs['newyork_admin_department'] = $newyork_admin_department;
        $userACL->rs['newyork_admin_order'] = $newyork_admin_order;
        $userACL->rs['newyork_admin_travel_expense'] = $newyork_admin_travel_expense;
        $userACL->rs['newyork_admin_invoice'] = $newyork_admin_invoice;
        $userACL->rs['newyork_admin_amend_budget'] = $newyork_admin_amend_budget;

        $userACL->rs['newyork_it_location'] = $newyork_it_location;
        $userACL->rs['newyork_it_department'] = $newyork_it_department;
        $userACL->rs['newyork_it_order'] = $newyork_it_order;
        $userACL->rs['newyork_it_travel_expense'] = $newyork_it_travel_expense;
        $userACL->rs['newyork_it_invoice'] = $newyork_it_invoice;
        $userACL->rs['newyork_it_amend_budget'] = $newyork_it_amend_budget;

        $userACL->write();
    }

    public function updateUserPrivilege($user_id,$user_type_id,$locationID,$DepartmentID){

        $approve_order = 'yes';
        $approve_travel_expense = 'no';
        $approve_invoice =  'no';
        $amend_budget ='no';
        $vendor_document   = 'no';
        $contract_document = 'yes';
        $contract_view = 'yes';
        $contract_edit = 'yes';
        $createdBy     = Yii::app()->session['user_id'];
        $createdAt     = date("Y-m-d H:i:s");

        $sql = "INSERT INTO `user_privilege` 
                    (`user_id`,
                    `user_type_id`, 
                    `location_id`,
                    `department_id`,
                    `approve_order`,
                    `approve_travel_expense`,
                    `approve_invoice`,
                    `amend_budget`,
                    `vendor_document`,
                    `contract_document`,
                    `contract_view`,
                    `contract_edit`,
                    `created_by_id`, 
                    `created_datetime`, 
                    `updated_datetime`) VALUES 
                    ('".$user_id."', 
                    '".$user_type_id."',
                    '".$locationID."',
                    '".$DepartmentID."',
                    '".$approve_order."',
                    '".$approve_travel_expense."',
                    '".$approve_invoice."',
                    '".$amend_budget."',
                    '".$vendor_document."',
                    '".$contract_document."',
                    '".$contract_view."',
                    '".$contract_edit."',
                    '".$createdBy."',
                    '".$createdAt."',
                    '".$createdAt."')";
                     $this->executeQuery($sql);
    }


}
