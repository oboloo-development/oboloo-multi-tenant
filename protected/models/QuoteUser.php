<?php

class QuoteUser extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'quote_user');
		$this->timestamp = false;
	}


	public function getQuoteUser($quoteID, $userID){
	  if(!empty($userID)){
	  	$sql =" select qu.*, u.full_name,email,username from quote_user qu left join users u on u.user_id = qu.user_id where qu.quote_id =".$quoteID."
	  	and qu.user_id=". $userID;
	  	return Yii::app()->db->createCommand($sql)->queryRow();
	  } 
	}

	public function getQuoteUserAll($quote_id){
	 if(!empty($quote_id)){
	  $sql = "select qu.*, u.full_name,email,username from quote_user qu
			left join users u on u.user_id = qu.user_id where qu.quote_id=".$quote_id;
	  return $this->executeQuery($sql);
	 }else{
	   $sql = "select qu.*, u.full_name,email,username from quote_user qu
			left join users u on u.user_id = qu.user_id where qu.open_for_scoring=1 and qu.create_user_id=". 
			Yii::app()->session['user_id'];
	  return $this->executeQuery($sql);
	 }
	}
	

}
