<?php

class OrderDetail extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'order_id' => 'N',
			'product_type' => 'C',
			'product_id' => 'N',
			'vendor_id' => 'N',
			'quantity' => 'N',
			'unit_price' => 'N',
			'calc_tax' => 'N',
			'calc_unit_price' => 'N',
			'tax' => 'N',
			'shipping_cost' => 'N'
		);

		$this->currency_fields = array ( 'unit_price' );
		parent::__construct('id', 'order_details');
		$this->timestamp = false;
	}

	public function getOrderDetails($order_id = 0)
	{
		$sql = "SELECT d.*, b.bundle_name AS product_name, v.vendor_name as vendor
				  FROM order_details d
				  LEFT JOIN vendors v ON d.vendor_id = v.vendor_id
				  LEFT JOIN bundles b ON d.product_id = b.bundle_id
				  WHERE d.product_type = 'Bundle'
				  AND d.order_id = $order_id

				 UNION

				 SELECT d.*, p.product_name, v.vendor_name as vendor
				  FROM order_details d
				   LEFT JOIN vendors v ON d.vendor_id = v.vendor_id
				  LEFT JOIN products p ON d.product_id = p.product_id
				  WHERE d.product_type = 'Product'
				  AND d.order_id = $order_id";
		//CVarDumper::dump($this->executeQuery($sql),10,1);die;
		return $this->executeQuery($sql);
	}
}
