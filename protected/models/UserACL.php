<?php

class UserACL extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'user_acl');
		$this->timestamp = false;
	}

	public function getUserACL($user_id = 0)
	{
		$sql = "SELECT * FROM user_acl ";
		return $this->executeQuery($sql . " WHERE user_id = $user_id ", true);
	}

}
