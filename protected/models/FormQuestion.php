<?php

class FormQuestion extends Common
{
	
		public function __construct()
		{
			$this->fields = array (
				'id' => 'N',
				'question' => 'C',
				'created_datetime' => 'D',
				'updated_datetime' => 'D',
			);
			parent::__construct('id', 'form_questions');
		}
}
