<?php

class SavingLog extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'saving_logs');
		$this->timestamp = false;
	}

	public function getSavingLogs($saving_id = 0)
	{
		$sql = "SELECT s.*, u.full_name AS user FROM saving_logs s, users u WHERE  s.saving_id = $saving_id
                 AND s.user_id = u.user_id
				 ORDER BY s.id DESC";
		return $this->executeQuery($sql);
	}
}
