<?php

class Industry extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'industries');
		$this->timestamp = false;
	}

	public function export($data = array(), $file = '')
	{
		parent::export($this->executeQuery("SELECT `value` AS industry_name FROM industries ORDER BY `value`"), 'industries');
	}

}
