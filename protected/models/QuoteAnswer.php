<?php

class QuoteAnswer extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'quote_answers');
		$this->timestamp = false;
	}

	public function getQuoteAnswers($question_id)
	{
		return $this->executeQuery("SELECT * FROM quote_answers WHERE question_id = $question_id ORDER BY id");
	}

}
