<?php

class VendorRisk extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'vendor_id' => 'N',
			'created_by_id' => 'N',
			'created_by_name' => 'C',
			'title' => 'C',
			'risk_description' => 'C',
			'risk_score_value' => 'N',
			'created_atafree' => 'D'
		);
		parent::__construct('id', ' vendor_risk');
		$this->timestamp = false;
	}

	
}
