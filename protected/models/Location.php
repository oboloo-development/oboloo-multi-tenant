<?php

class Location extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'location_id' => 'N',
			'location_name' => 'C',
			'contact_person' => 'C',
			'email' => 'C',
			'phone' => 'C',
			'fax' => 'C',
			'website' => 'C',
			'loc_label' => 'C',
			'loc_address' => 'C',
			'loc_city' => 'C',
			'loc_state' => 'C',
			'loc_zip_code' => 'C',
			'loc_country' => 'C',
			'ship_label' => 'C',
			'ship_address' => 'C',
			'ship_city' => 'C',
			'ship_state' => 'C',
			'ship_zip_code' => 'C',
			'ship_country' => 'C'
		);
		$this->alternate_key_name = 'location_name';
		parent::__construct('location_id', 'locations');
	}
	
	public function getDepartments($location_id = 0, $single = 0)
	{
		
		if (!empty($location_id)) {
            if(!empty($single)){
				$location_id = $location_id;
			 if(is_array($location_id)){
				$where  = 'location_id in('.implode(',',$location_id).')';
			 }else{ $where  = 'location_id ='. $location_id; }
			}else {
			 $location_id = implode(',',$location_id);
			 $where  = 'location_id IN ('.$location_id.')';
			}

		}else{
			$location_id = 0;
			$where  = 'location_id ='.$location_id;
		}
		 $sql = "SELECT * FROM departments
				 WHERE department_id IN ( SELECT department_id FROM locations_departments
				 WHERE $where)
				group by department_name ORDER BY department_name";
		return $this->executeQuery($sql);
	}
	
	public function getDepartmentsByPerms($user_id,$location_id,$field_name){
	    /* 
	     $sql = "SELECT departments.* FROM departments
		 INNER JOIN user_privilege on departments.department_id=user_privilege.department_id
		 WHERE 1 
		 group by department_id ORDER BY department_name";
		*/

		$sql = "SELECT departments.* FROM departments
		      INNER JOIN locations_departments loc_dept on departments.department_id=loc_dept.department_id
			  WHERE loc_dept.location_id=".$location_id."
			  group by department_id ORDER BY department_name";
		return $this->executeQuery($sql);
	}

	public function getLocationContactData($location_id){
	 $location_sql = "SELECT location_name,contact_person,ship_address,
	 ship_city,ship_state,ship_zip_code,phone,email FROM locations WHERE location_id = $location_id";
	 return $this->executeQuery($location_sql);
	}

	public function getLocationAndDepartment($info=''){
	 $location_sql = "SELECT locations.location_id,locations.location_name,dept.department_id,dept.department_name FROM `locations` 

	 INNER join (select inn_dept.*,inn_dept_loc.location_id from departments as inn_dept 
	 inner join locations_departments as inn_dept_loc on inn_dept.department_id=inn_dept_loc.department_id) as dept
	 on locations.location_id=dept.location_id ";
	 
	 if(!empty($info)){
	  $location_sql .=" order by ".$info['order']." asc ";
	 }
	 
	 return $this->executeQuery($location_sql);
	}

	public function getDepartmentForMultiLocations($department_id = 0)
	{
	 if(!empty($department_id)) {
		$where  = 'department_id ='.$department_id;
		$sql = "SELECT * FROM locations
		 WHERE location_id IN ( SELECT location_id FROM locations_departments
		 WHERE $where)
		 ORDER BY location_name";
		 return $this->executeQuery($sql);
		}else {
			$sql = " SELECT * FROM locations ORDER BY location_name";
			return $this->executeQuery($sql);
		}
	}

	public function getAccesstDepartments($location_id = 0, $single = 0)
	{
		if (!empty($location_id)) {
            if(!empty($single)){
				$location_id = $location_id;
			 if(is_array($location_id)){
				$where  = 'location_id in('.implode(',',$location_id).')';
			 }else{ $where  = 'location_id ='. $location_id; }
			}else {
			 $location_id = implode(',',$location_id);
			 $where  = 'location_id IN ('.$location_id.')';
			}
		}else{
			$location_id = 0;
			$where  = 'location_id ='.$location_id;
		}
		
    if(Yii::app()->session['user_type'] !=4){
      $sql = "SELECT * FROM departments as d 
			INNER JOIN user_privilege up On d.department_id = up.department_id 
			WHERE d.department_id IN ( SELECT department_id FROM locations_departments WHERE $where)
			and (up.saving_view='yes' or up.saving_edit='yes') and up.user_id=".Yii::app()->session['user_id']." group by department_name ORDER BY department_name asc ";
        }else{
          $sql = "SELECT * FROM departments as d 
			WHERE d.department_id IN ( SELECT department_id FROM locations_departments WHERE $where)
			group by department_name ORDER BY department_name asc ";
        }

		return $this->executeQuery($sql);
	}
}
