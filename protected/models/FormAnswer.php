<?php

class FormAnswer extends Common
	{
	
		public function __construct()
		{
			$this->fields = array (
				'id' => 'N',
				'answer' => 'C',
				'question_id'  => 'N',
				'user_id'  => 'N',
				'form_id'  => 'N',
				'username' => 'C',
				'created_datetime' => 'D',
				'updated_datetime' => 'D',
			);
			parent::__construct('id', 'form_answers');
		}
}
