<?php

class MainQuickScoring extends Common
{
	public function __construct()
	{

		$this->fields = array (
			'id' => 'N',
			'best_supplier_id' => 'N',
			'created_by_id' => 'N',
			'quote_name' => 'N',
			'quote_desc' => 'N',
			'detail_description'=> 'N',
			'created_by_name' => 'N',
			'category_id'=>'N',
			'subcategory_id'=>'N',
			'created_at' => 'D',
		);
		parent::__construct('id', 'main_quick_scoring');
		$this->timestamp = false;
	}
	
}
