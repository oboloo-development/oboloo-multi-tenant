<?php
require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');
class PurchaseOrder extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'po_id' => 'N',
			'order_id' => 'N',
			'vendor_id' => 'N',
			'po_number' => 'C',
			'reference' => 'C',
			'payment_term_id' => 'N',
			'shipping_term_id' => 'N',
			'shipping_method_id' => 'N',
			'po_other_charges' => 'N',
			'po_discount' => 'N',
			'po_total' => 'N',
			'po_status' => 'C',
			'po_notes' => 'C',
			'po_date' => 'D',
			'po_sent_date' =>'D',
		);

		$this->currency_fields = array( 'po_total', 'po_other_charges', 'po_discount' );
		parent::__construct('po_id', 'purchase_orders');
	}
	
	public function add($order_id = 0, $total_price = 0)
	{
		if ($order_id)
		{
			// first make sure nothing has been created already
			$po_already_created = false;
			$existing_po_data = $this->executeQuery("SELECT * FROM purchase_orders WHERE order_id = $order_id", true);
			if ($existing_po_data && is_array($existing_po_data) 
					&& isset($existing_po_data['order_id']) && $existing_po_data['order_id'] == $order_id)
				$po_already_created = true;
			
			// create new po if not found
			if (!$po_already_created)
			{
				$this->rs = array();
				$this->rs['po_id'] = 0;
				$this->rs['po_number'] = date("Y") . '-' . sprintf("%04d", $order_id);
				$this->rs['order_id'] = $order_id;
				$this->rs['po_status'] = 'Pending';
				$this->rs['po_date'] = date("Y-m-d");
				$this->rs['po_total'] = $total_price;
				$this->write(); 
			}
		}
	}

	
	public function getOrders($po_id = 0, $location_id = 0, $department_id = 0, $po_status = "", $from_date = "", $to_date = "")
	{
	/*	$sql = "SELECT p.*,v.vendor_id, v.vendor_name AS vendor,o.currency_id
				  FROM (SELECT p_order.*,ord_inv.file_name,ord_inv.status as inv_status FROM `order_invoices` ord_inv right join purchase_orders p_order on ord_inv.order_id=p_order.order_id ) p

				  LEFT JOIN orders o ON p.order_id = o.order_id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id";*/

	    $sql = "SELECT p.*,v.vendor_id, v.vendor_name AS vendor,o.currency_id,o.routing_users_ids,o.user_id
				  FROM purchase_orders p
				  LEFT JOIN (SELECT in_o.*,GROUP_CONCAT(in_r.user_id SEPARATOR ',') as routing_users_ids FROM `orders` in_o inner join order_routing in_r on in_o.order_id =in_r.order_id  group by in_r.order_id) o ON p.order_id = o.order_id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id where 1 ";

		$userType = Yii::app()->session['user_type'];
		if (!isset($userType) || !in_array(Yii::app()->session['user_type'],array(1,4)))
		{   $user_id = Yii::app()->session['user_id'];
			$sql .= " and ( FIND_IN_SET(".$user_id.", o.routing_users_ids) or o.user_id=".$user_id.")";
		}
		
		
		if ($po_id)
			return $this->executeQuery($sql . " and p.po_id = $po_id ", true);
		else 
		{
			$sql .= " and p.po_id > 0 ";
			if (!empty($location_id)) {
				$location_id = implode(',',$location_id);
				$sql .= " AND o.location_id IN ($location_id)";
			}
			//if (!empty($department_id)) $sql .= " AND o.department_id = $department_id ";
			if(!empty($department_id)) {
				$department_id = implode(',',$department_id);
				$sql .= " AND o.department_id IN ($department_id)";
			}

			if (!empty($po_status)){
				$status = "'" . implode("','", $po_status) . "'";
				$sql .= " AND p.po_status IN ($status)";
			}
			if (!empty($from_date)) $sql .= " AND p.po_date >= '$from_date' ";
			if (!empty($to_date)) $sql .= " AND p.po_date <= '$to_date' ";
			
			return $this->executeQuery($sql . ' ORDER BY p.po_date DESC ');
		}
	}

	
	public function checkPermission($po_id)
	{
		$sql = "SELECT o.routing_users_ids,o.user_id FROM purchase_orders p
				       LEFT JOIN (SELECT in_o.*,GROUP_CONCAT(in_r.user_id SEPARATOR ',') as routing_users_ids FROM `orders` in_o inner join order_routing in_r on in_o.order_id =in_r.order_id  group by in_r.order_id) o ON p.order_id = o.order_id where po_id = ".$po_id;

		$userType = Yii::app()->session['user_type'];
		if (!isset($userType) || !in_array(Yii::app()->session['user_type'],array(1,4)))
		{   $user_id = Yii::app()->session['user_id'];
			$sql .= " and  FIND_IN_SET(".$user_id.", o.routing_users_ids)";
		}
		
		return !empty($this->executeQuery($sql, 1))?1:0;
	}
	public function saveOrderLevelData($po_id)
	{
		$sql = "UPDATE orders o, purchase_orders p
				   SET o.discount = p.po_discount,
				   	   o.other_charges = p.po_other_charges  
				 WHERE o.order_id = p.order_id
				   AND p.po_id = $po_id";
		$this->executeQuery($sql);
	}

	public function getOrderByReference($input_reference)
	{
		$reference = $input_reference;
		if (empty($reference)) $reference = 'hopefully this is not any random string generated in this exact format :)';

		$sql = "SELECT o.*,p.po_id,v.vendor_id, v.vendor_name AS vendor
				  FROM orders o
				  INNER JOIN purchase_orders p ON p.order_id = o.order_id
				  INNER JOIN vendors v ON o.vendor_id = v.vendor_id
				  WHERE p.reference = '$reference'";
		return $this->executeQuery($sql, true);
	}

	public function saveOrderSubmissionData($order_submission_data)
	{
		// which vendor is submitting this order?
		$order_id = isset($order_submission_data['order_id']) ? $order_submission_data['order_id'] : 0;
		$po_id = isset($order_submission_data['po_id']) ? $order_submission_data['po_id'] : 0;
		$vendor_id = isset($order_submission_data['vendor_id']) ? $order_submission_data['vendor_id'] : 0;
		$status = isset($order_submission_data['status']) ? $order_submission_data['status'] : 0;
		$tracking_number = isset($order_submission_data['tracking_number']) ? $order_submission_data['tracking_number'] : 0;
		$courier_company = isset($order_submission_data['courier_company']) ? $order_submission_data['courier_company'] : 0;
		$notes = isset($order_submission_data['note']) ? $order_submission_data['note'] : "";

		if ($order_id && $vendor_id)
		{

			$this->executeQuery("DELETE FROM order_vendor_details WHERE order_id = $order_id AND po_id = $po_id  AND vendor_id = $vendor_id");

			//Save Qoute product
			$orderVendorDetail = new OrderVendorDetail();
			$orderVendorDetail->rs = array();
			$orderVendorDetail->rs['order_id'] = $order_id;
			$orderVendorDetail->rs['po_id'] = $po_id;
			$orderVendorDetail->rs['vendor_id'] = $vendor_id;
			$orderVendorDetail->rs['status'] = $status;
			$orderVendorDetail->rs['tracking_number'] = $tracking_number;
			$orderVendorDetail->rs['courier_company'] = $courier_company;
			$orderVendorDetail->rs['note'] = $notes;
			$orderVendorDetail->write();

			//$this->executeQuery("UPDATE orders SET status = $status WHERE id = $order_id");

			// finally create a notification for the user of the quote that supplier has sent a quote
			$vendor_data = $this->executeQuery("SELECT * FROM vendors WHERE vendor_id = $vendor_id", true);
			$order_data = $this->executeQuery("SELECT * FROM orders WHERE order_id = $order_id", true);

			if ($vendor_data && $order_data && is_array($vendor_data) && is_array($order_data))
			{
				$notification = new Notification();
				$notification->rs = array();
				$notification->rs['id'] = 0;
				$notification->rs['user_id'] = $order_data['user_id'];
				$notification->rs['notification_text'] =
					'Supplier ' . $vendor_data['vendor_name'] . ' has submitted a order for '
					. '<a href="' . AppUrl::bicesUrl('orders/edit/' . $order_id) . '">Order ID #'
					. $order_id . '</a> - ' . $order_data['invoice_number'];
				$notification->rs['notification_date'] = date("Y-m-d H:i");
				$notification->rs['read_flag'] = 0;
				$notification->write();
			}
		}
	}

	public function sendInvites($po_id,$order_id)
	{
		$order = new Order();
		$order = $order->getOne(array('order_id' => $order_id));
		$vendor_id =  $order['vendor_id'];
		$order_vendor = $this->executeQuery("SELECT * FROM vendors WHERE vendor_id = $vendor_id",1);
		$company = $this->executeQuery("SELECT * FROM company",1);

		if (!empty($order_vendor['emails']))
			{

				$vendor_name = $order_vendor['vendor_name'];
				$reference = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)))), 1, 24);
				$this->executeQuery("UPDATE purchase_orders SET reference = '$reference' WHERE po_id = $po_id");

				$email_body = '';
				$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <div style="margin:0px auto;max-width:600px;">
                <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="vertical-align:top;width:600px;">
                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                 <tr>
                  <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                  <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                      <tr>
                          <td style="width:600px;">';
				$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
				$email_body .= '</td>
                      </tr>
                      </tbody>
                  </table>
                  </td>
                 </tr>
                 <tr>
                 <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;width:600px;">';
				$email_body .= '<p style="margin-left: 16px;">Dear ' . $vendor_name . ', </p>';
				$email_body .= '</td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center;">';
				$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                         <h1 style="font-family: Cabin; line-height: 100%;">
                             <span style="color:#000000;"><strong>New Order - <span style="color:#10a798;">' . $order_id . '</span> </strong></span>
                         </h1>
                     </div>
                 </td>
                 </tr>

                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;">
                 <div style="font-size:11px;line-height:22px;color:#000000;font-family:Roboto, Tahoma, sans-serif;marign-left:16px;">';
				$email_body .= '<p>An order has been raised by '.$company['company_name'].' via Spend 365. Please follow the below link to view the order details:</p>';
				$email_body .= '<strong>Order For: </strong>' . $company['company_name'] . '<br />';
				$email_body .= '<strong>Order Type: </strong>NEW<br />';
				$email_body .= '<br /><strong>NOTE:</strong> To submit a order, please click on the <span style="color: red;">Order Submission Page</span> link below and use the reference number which is unique for you.<br />';
				$email_body .= '</div>
                 </td>
                </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                     </td>
                 </tr>';

				$email_body .= '<tr>
                       <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                        <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                        <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                        <a href="' . Yii::app()->createAbsoluteUrl('supplier/orders/submissionView?po_id='.$po_id). '" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Order Submission Page</a>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>

                         <tr>
                             <td style="word-wrap:break-word;font-size:0px;">
                             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                             </td>
                         </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;" align="left">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;margin-left:16px;">
                             <p>Terms of Service &amp; User Agreement</p>
                             <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                             <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                         </div>
                     </td>
                 </tr>
                 </tbody>
                </table>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </div></td>
                </tr>
                </table>
                </div>';

               
                	$attachmentFile =  Yii::getPathOfAlias('webroot')."/uploads/purchase_orders/attachment/".$po_id;
                	$attachmentFile =  $attachmentFile."/".$po_id.".pdf";  
					$mail = new \PHPMailer(true);
					$mail->IsSMTP();
					$mail->Host = Yii::app()->params['host'];
					$mail->Port = Yii::app()->params['port'];
					$mail->SMTPAuth = true;
					$mail->Username = Yii::app()->params['username'];
					$mail->Password = Yii::app()->params['password'];
					$mail->SMTPSecure = Yii::app()->params['stmpSecure'];
					$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
					$mail->addAddress($order_vendor['emails'], $vendor_name);
					$mail->Subject = Yii::app()->session['company_name'] . 'Spend 365: Order Submission Invite';
					$mail->isHTML(true);
					$mail->Body = $email_body;
					$mail->AddAttachment($attachmentFile, $name = 'spend_'.$order_id,  $encoding = 'base64', $type = 'application/pdf');
					//$mail->SMTPDebug = 2;
					$mail->send();


			}
	}

	public function submitOrder($order_id,$vendor_id)
	{
		$order_vendor = $this->executeQuery("SELECT *
				  FROM orders
				  WHERE order_id = $order_id AND vendor_id = $vendor_id",1);

		$vendor = $this->executeQuery("SELECT * FROM vendors WHERE vendor_id = $vendor_id",1);

		$user_id = $order_vendor['user_id'];
		$vendor_name = $vendor['vendor_name'];
		$user = $this->executeQuery("SELECT * FROM users WHERE user_id = $user_id",1);


		//CVarDumper::dump($quote_vendor['emails'],10,1);die;

		if (!empty($user['email']))
		{
			$mail = new \PHPMailer(true);
			$mail->IsSMTP();
			$mail->Host = Yii::app()->params['host'];
			$mail->Port = Yii::app()->params['port'];
			$mail->SMTPAuth = true;
			$mail->Username = Yii::app()->params['username'];
			$mail->Password = Yii::app()->params['password'];
			$mail->SMTPSecure = Yii::app()->params['stmpSecure'];

			$email_body = '';
			$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <div style="margin:0px auto;max-width:600px;">
                <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="vertical-align:top;width:600px;">
                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                 <tr>
                  <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                  <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                      <tr>
                          <td style="width:600px;">';
			$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
			$email_body .= '</td>
                      </tr>
                      </tbody>
                  </table>
                  </td>
                 </tr>
                 <tr>
                 <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;width:600px;">';
			$email_body .= '<p style="margin-left: 16px;">Dear ' . $user['full_name'] . ', </p>';
			$email_body .= '</td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center;">';
			$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                         <h1 style="font-family: Cabin; line-height: 100%;">
                             <span style="color:#000000;"><strong> <span style="color:#10a798;">' . $vendor_name . '</span> has responded to Order </strong></span>
                         </h1>
                     </div>
                 </td>
                 </tr>

                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;">
                 <div style="font-size:11px;line-height:22px;color:#000000;font-family:Roboto, Tahoma, sans-serif;marign-left:16px;">';
			$email_body .= '<p> You have had a response for  order ' . $order_id . '. Please see further details via the <span style="color:red;">Order</span> button below.:</p>';
			$email_body .= '</div>
                 </td>
                </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                     </td>
                 </tr>';

			$email_body .= '<tr>
                       <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                        <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                        <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                        <a href="' . AppUrl::bicesUrl('orders/edit/'.$order_id) . '" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Order Response</a>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>

                         <tr>
                             <td style="word-wrap:break-word;font-size:0px;">
                             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                             </td>
                         </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;" align="left">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;margin-left:16px;">
                             <p>Terms of Service &amp; User Agreement</p>
                             <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                             <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                         </div>
                     </td>
                 </tr>
                 </tbody>
                </table>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </div></td>
                </tr>
                </table>
                </div>';

			$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
			$mail->addAddress($user['email'], $vendor_name);
			$mail->Subject = Yii::app()->session['company_name'] . 'Spend 365: Supplier Response';
			$mail->isHTML(true);
			$mail->Body = $email_body;
			//$mail->SMTPDebug = 2;
			$mail->send();
		}
	}

}
