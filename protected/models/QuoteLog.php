<?php

class QuoteLog extends Common
{
	public function __construct()
	{
	 parent::__construct('id', 'quote_logs');
	 $this->timestamp = false;
	}

	public function getScoringLogs($quote_id = 0)
	{
		$sql = " SELECT q.*, u.full_name AS user FROM quote_logs q, users u WHERE  q.quote_id = $quote_id
                 AND q.user_id = u.user_id ORDER BY q.id DESC ";
		return $this->executeQuery($sql);
	}
}
