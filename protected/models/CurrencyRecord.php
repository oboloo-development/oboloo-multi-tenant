<?php

class CurrencyRecord extends Common
{
	public function __construct()
	{

		$this->fields = array (
			'id' => 'N',
			'record_id' => 'N',
			'record_type' => 'C',
			'currency_id' => 'N',
			'currency' => 'C',
			'currency_symbol' => 'C',
			'currency_rate' => 'C',
		);

		parent::__construct('id', 'record_currency');
		$this->timestamp = false;
	}

}
