<?php
class VendorScoreCard extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'vendor_score_card');
		$this->timestamp = false;
	}

	public function getVendorScoreCardList($id = 0, $start = 0, $length = 10, $search_for = "", $order_by = array(), $user_id="")
    {
        $sql = "select vsc.*, 
                c.value as category_name, 
                sc.value as subcategory_name,
				vsn.name as score_card_name
                FROM vendor_score_card as vsc
                LEFT JOIN vendor_scoring_name as vsn ON vsn.id = vsc.scorecard_id
                LEFT JOIN categories as c ON c.id = vsc.category_id
                LEFT JOIN sub_categories as sc ON sc.id = vsc.subcategory_id ";
        if (!empty($id))
            return $this->executeQuery($sql . " WHERE id = $id ", true);
        else {
            if (! empty($search_for)) {
              $sql = "SELECT vsc.*, 
                c.value as category_name, 
                sc.value as subcategory_name
                FROM vendor_score_card as vsc
                 LEFT JOIN vendor_scoring_name as vsn ON vsn.id = vsc.scorecard_id
                 LEFT JOIN categories as c ON c.id = vsc.category_id
                 LEFT JOIN sub_categories as sc ON sc.id = vsc.subcategory_id
                 WHERE vsc.username LIKE '%$search_for%' 
                  OR c.value LIKE '%$search_for%' 
                  OR sc.value LIKE '%$search_for%'
                  OR vsn.name LIKE '%$search_for%'
                  OR vsc.quote_name LIKE '%$search_for%' 
                  OR vsc.notes LIKE '%$search_for%' 
                  OR vsc.username LIKE '%$search_for%' 
                  OR vsc.created_datetime LIKE '%$search_for%' 
                  OR vsc.updated_datetime LIKE '%$search_for%'
                ";
            }
        
            $order_by_clause = "";
            if (is_array($order_by) && count($order_by)) {
                foreach ($order_by as $column) {
                    $column_index = $column['column'];
                    $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                    
                    $column_name = "";
                    switch ($column_index) {
                        case 0:
                            $column_name = "vsc.id";
                            break;
                        case 1:
                            $column_name = "vsc.quote_name";
                            break;
						case 2:
                            $column_name = "vsc.notes";
                            break;
                        case 3:
                            $column_name = "vsn.name";
                            break;
                        case 4:
                            $column_name = "c.value";
                            break;
                        case 5:
                            $column_name = "sc.value";
                            break;
                        case 6:
                            $column_name = "vsc.created_datetime";
                            break;
                        case 7:
                            $column_name = "vsc.updated_datetime";
                            break;
                        default:
                            $column_name = "vsc.quote_name";
                            break;
                    }
                    
                    if (! empty($column_name)) {
                        if (empty($order_by_clause))
                            $order_by_clause = $column_name . ' ' . $column_direction;
                        else
                            $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                    }
                }
            }
             
            if (empty($order_by_clause))
                $order_by_clause = " value ";
            if (strpos($sql, "WHERE") === false)
                $sql .= " WHERE 1 = 1 ";
            if(!empty($user_id))
                $sql .= " AND vsc.user_id = $user_id ";

            if($start != 'none'){
              $sql .= "  ORDER BY $order_by_clause LIMIT $start, $length ";
            }else{
              $sql .= " ORDER BY $order_by_clause ";
            }
    
            return $this->executeQuery($sql);
        }
    }
}
