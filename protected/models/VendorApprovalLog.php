<?php

class VendorApprovalLog extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'vendor_approval_log');
		$this->timestamp = false;
	}

	public function getApprovalLogs($vendor_id = 0)
	{
		 $sql = "SELECT r.*,u.full_name AS user FROM vendor_approval_log r, users u
		 WHERE  r.user_id = u.user_id and r.vendor_id = $vendor_id ORDER BY r.id desc";
		return $this->executeQuery($sql);
	}

	 
}
