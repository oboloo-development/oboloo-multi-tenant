<?php

class ContractLog extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'contract_logs');
		$this->timestamp = false;
	}

	public function getContractLogs($contract_id = 0)
	{
		$sql = "SELECT r.*, u.full_name AS user, s.value FROM contract_logs r, users u, contract_status s WHERE  r.contract_id = $contract_id
                 AND r.user_id = u.user_id
                 AND r.contract_status = s.code
				 ORDER BY r.id DESC";
		return $this->executeQuery($sql);
	}
}
