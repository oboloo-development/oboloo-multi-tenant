<?php

class Notification extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'user_id' => 'N',
			'user_type' => 'C',
			'notification_flag' => 'C',
			'notification_text' => 'C',
			'notification_type' => 'C',
			'notification_date' => 'D',
			'read_flag' => 'N'
		);
		parent::__construct('id', 'notifications');
		$this->timestamp = false;
	}

	public function getMyNotifications($mark_read = false, $from_date = "", $to_date = "", $notification_filter = "")
	{
		$date_filter_clause = "";
		if (!empty($from_date) && !empty($to_date)) $date_filter_clause = " AND notification_date BETWEEN '$from_date' AND '$to_date' ";
		else
		{
			if (!empty($from_date)) $date_filter_clause = " AND notification_date >= '$from_date' ";
			if (!empty($to_date)) $date_filter_clause = " AND notification_date <= '$to_date' ";
		}
		
		$notification_type = "";
		if(!empty($notification_filter) && $notification_filter == "sourcing") $notification_type = ['Quote','Quote and not home',];
		if(!empty($notification_filter) && $notification_filter == "contract") $notification_type = ['Contract'];
		if(!empty($notification_filter) && $notification_filter == "supplier") $notification_type = ['Vendor','Supplier Status'];
		if(!empty($notification_filter) && $notification_filter == "savings")  $notification_type = ['Savings','Saving Milestone'];
	
		if ($mark_read) $this->executeQuery("UPDATE notifications SET read_flag = 1 WHERE read_flag = 0 AND user_type='Client' and user_id = " . Yii::app()->session['user_id']);
		$return  = '';
		if(!empty($notification_type)){
			$comma_separated = implode("','", $notification_type);
			$notification_type = "'".$comma_separated."'";
			$return = $this->executeQuery("SELECT * FROM notifications WHERE notification_type IN(".$notification_type.") and user_type='Client' and user_id = " . Yii::app()->session['user_id'] . " " . $date_filter_clause . " ORDER BY notification_date DESC, id DESC");
		}else{
			$return = $this->executeQuery("SELECT * FROM notifications WHERE  user_type='Client' and user_id = " . Yii::app()->session['user_id'] . " " . $date_filter_clause . " ORDER BY notification_date DESC, id DESC");
		}
		return $return;
	}

	public function getLastest($limit=10)
	{
		return $this->executeQuery("SELECT * FROM notifications WHERE  user_type='Client' AND notification_type not in('Quote and not home') and user_id = " . Yii::app()->session['user_id'] . " ORDER BY id DESC limit $limit");
	}

	public function getLastestForContract($limit=10)
	{
		return $this->executeQuery("SELECT * FROM notifications WHERE  user_type='Client' and user_id = " . Yii::app()->session['user_id'] . " and notification_type='Contract' ORDER BY id DESC limit $limit");
	}

	public function getLastestForSupplier($limit=10)
	{
		return $this->executeQuery("SELECT * FROM notifications WHERE  user_type='Client' and user_id = " . Yii::app()->session['user_id'] . " and notification_type='Vendor' ORDER BY id DESC limit $limit");
	}
	public function getLastestForQuotes($limit=10)
	{
		return $this->executeQuery("SELECT * FROM notifications WHERE  user_type='Client' and user_id = " . Yii::app()->session['user_id'] . " and notification_type in('Quote','Quote and not home') ORDER BY id DESC limit $limit");
	}

	

	public function getMyHomeNotifications($mark_read = false, $from_date = "", $to_date = "")
	{
		$date_filter_clause = "";
		if (!empty($from_date) && !empty($to_date)) $date_filter_clause = " AND notification_date BETWEEN '$from_date' AND '$to_date' ";
		else
		{
			if (!empty($from_date)) $date_filter_clause = " AND notification_date >= '$from_date' ";
			if (!empty($to_date)) $date_filter_clause = " AND notification_date <= '$to_date' ";
		}

		if ($mark_read) $this->executeQuery("UPDATE notifications SET read_flag = 1 WHERE user_type='Client' and read_flag = 0 AND user_id = " . Yii::app()->session['user_id']);
		return $this->executeQuery("SELECT * FROM notifications WHERE  user_type='Client' and user_id = " . Yii::app()->session['user_id'] . " " . $date_filter_clause . " ORDER BY notification_date DESC, id DESC LIMIT 0,5");
	}

	static function getTimeAgo($time_ago)
	{
	    $time_ago 	= strtotime($time_ago);
	    $cur_time   = time();
	    $time_elapsed   = $cur_time - $time_ago;
	    $seconds    = $time_elapsed ;
	    $minutes    = round($time_elapsed / 60 );
	    $hours      = round($time_elapsed / 3600);
	    $days       = round($time_elapsed / 86400 );
	    $weeks      = round($time_elapsed / 604800);
	    $months     = round($time_elapsed / 2600640 );
	    $years      = round($time_elapsed / 31207680 );

	    if ($seconds <= 60) return "just now";
	    else if ($minutes <= 60)
	    {
	        if ($minutes == 1) return "one minute ago";
	        else return "$minutes minutes ago";
	    }
	    else if ($hours <= 24)
	    {
	        if ($hours == 1) return "an hour ago";
	        else return "$hours hrs ago";
	    }
	    else if ($days <= 7)
	    {
	        if ($days == 1) return "yesterday";
			else return "$days days ago";
	    }
	    else if ($weeks <= 4.3)
	    {
	        if($weeks == 1) return "a week ago";
	        else return "$weeks weeks ago";
	    }
	    else if ($months <= 12)
	    {
	        if ($months == 1) return "a month ago";
	        else return "$months months ago";
	    }
	    else
	    {
	        if ($years == 1) return "one year ago";
	        else return "$years years ago";
	    }
	}

	public function deleteNotifications($notifications_to_delete)
	{
		$my_user_id = Yii::app()->session['user_id'];
		if ($my_user_id && $notifications_to_delete)
			foreach (explode(",", $notifications_to_delete) as $an_id_to_delete)
				$this->executeQuery("DELETE FROM notifications WHERE id = $an_id_to_delete AND user_id = $my_user_id");
	}
}
