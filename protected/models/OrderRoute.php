<?php

class OrderRoute extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'order_id' => 'N',
			'user_id' => 'N',
			'approval_type' => 'C',
			'routing_status' => 'C',
			'routing_date' => 'D',
			'routing_comments' => 'C'
		);
		parent::__construct('id', 'order_routing');
	}

	public function getApprovals($from_date,$to_date,$location_id,$department_id, $order_status,$spend_type)
	{
		$this->currency_fields = array('total_price');
		$my_user_id = Yii::app()->session['user_id'];

		$tool_currency = Yii::app()->session['user_currency'];
        $sql = "select cr_r.id from currency_rates cr_r where LOWER(cr_r.currency)='".strtolower($tool_currency)."'";
        $toolRateReader = Yii::app()->db->createCommand($sql)->queryRow();

		$sql = "select * from ( 
				 SELECT  r.id,r.user_id,r.routing_date,r.routing_comments,r.status,r.created_datetime,r.updated_datetime,o.location_id,o.department_id,o.spend_type,(SELECT routing_status from order_routing in_r where in_r.user_id = $my_user_id and in_r.order_id =r.order_id order by in_r.updated_datetime DESC limit 1) as routing_status, o.order_id, o.order_date, getTotalInToolCurrency(GetOrderTotalAmount(o.order_id),o.currency_id,".$toolRateReader['id'].") as func_total_price, l.location_name, d.department_name,u.full_name,r.approval_type,o.order_status as other_status
				  FROM order_routing r, orders o, locations l, departments d,users u
				 WHERE r.order_id = o.order_id AND o.location_id = l.location_id AND r.user_id = u.user_id
				   AND o.department_id = d.department_id AND r.approval_type = 'O'
				   AND (r.user_id = $my_user_id ) AND r.status = 1

				   GROUP by o.order_id
				   
				 UNION

				SELECT r.id,r.user_id,r.routing_date,r.routing_comments,r.status,r.created_datetime,r.updated_datetime,e.location_id,e.department_id,(SELECT (select etype.value from expense_types etype where etype.id=ed.expense_type_id) FROM expense_details ed where ed.expense_id=e.expense_id limit 1) as spend_type ,(SELECT routing_status from order_routing in_r where in_r.user_id = $my_user_id and  in_r.order_id =r.order_id order by in_r.updated_datetime DESC limit 1) as routing_status, e.expense_id AS order_id, e.created_datetime AS order_date,
				 		getTotalInToolCurrency(getExpenseTotalAmount(e.expense_id),e.currency_id,".$toolRateReader['id'].") AS func_total_price, l.location_name, d.department_name,u.full_name,r.approval_type,e.expense_status as other_status
				  FROM order_routing r, expenses e, locations l, departments d,users u
				 WHERE r.order_id = e.expense_id AND e.location_id = l.location_id AND r.user_id = u.user_id
				   AND e.department_id = d.department_id AND r.approval_type = 'E'				 
				   AND (r.user_id = $my_user_id ) AND r.status = 1 GROUP by e.expense_id ) as tbl_appr where 1
				";
		if (!empty($location_id)) {
			$location_id = implode(',',$location_id);
			$sql .= " AND location_id IN ($location_id)";
		}


		/*if (!empty($department_id)) {
			$sql .= " AND o.department_id = $department_id";
		}*/

		if(!empty($department_id)) {
			$department_id = implode(',',$department_id);
			$sql .= " AND department_id IN ($department_id)";
		}

		if (!empty($order_status)){
			$status = "'" . implode("','", $order_status) . "'";
			$sql .= " AND routing_status IN ($status)";
		}
		if (!empty($from_date)) {
			$from_date = date("Y-m-d",strtotime($from_date));
			$sql .= " AND order_date >= '$from_date' ";
		}
		if (!empty($to_date)){
			$to_date = date("Y-m-d",strtotime($to_date));
			$sql .= " AND order_date <= '$to_date' ";
		}

		if (!empty($spend_type)){
			$sql .= " AND spend_type='".$spend_type."'";
		}
		
	
		$approvals = $this->executeQuery($sql);

		$this->currency_fields = array();
		return $approvals;
	}
	
	public function getRoute($route_type = '', $location_id = 0, $department_id = 0)
	{
		// anount based routing trumps step based one
		$route = array();
		for ($i=1; $i<=2; $i++)
		{
			$loop_data_type = 'A';
			if ($i == 2) $loop_data_type = 'S';
			
			// first for specific department
			$sql = "SELECT * FROM approval_levels 
					 WHERE approval_type = '$route_type'
					   AND location_id = $location_id
					   AND department_id = $department_id
					   AND data_type = '$loop_data_type'
					 ORDER BY amount";
			$route = $this->executeQuery($sql);
			
			if (!$route || !is_array($route) || count($route) <= 0)
			{
				// next specific location
				$sql = "SELECT * FROM approval_levels 
						 WHERE approval_type = '$route_type'
						   AND location_id = $location_id
						   AND department_id = 0
						   AND data_type = '$loop_data_type'
						 ORDER BY amount";
				$route = $this->executeQuery($sql);
				
				if (!$route || !is_array($route) || count($route) <= 0)
				{
					// finally default route for everything
					$sql = "SELECT * FROM approval_levels 
							 WHERE approval_type = '$route_type'
							   AND location_id = 0
							   AND department_id = 0
					   		   AND data_type = '$loop_data_type'
							 ORDER BY amount";
					$route = $this->executeQuery($sql);
	
					// is this needed?
					if (!$route || !is_array($route) || count($route) <= 0)
					{
						$sql = "SELECT * FROM approval_levels 
								 WHERE approval_type = ''
								   AND location_id = 0
								   AND department_id = 0
					   			   AND data_type = '$loop_data_type'
								 ORDER BY amount";
						$route = $this->executeQuery($sql);
					}
				}
			}
			
			if ($route && is_array($route) && count($route)) return $route;
		}
		
		return $route;
	}

	public function getUsersApprover($locationID = 0, $departmentID = 0,$fieldName='')
	{
		
		 $sql="select pri.* FROM user_privilege pri inner join users on pri.user_id=users.user_id WHERE users.user_type_id in (1,3,4) and users.status=1";

        if(!empty($locationID)){
            $sql .=" and pri.location_id=$locationID";
        }
        if(!empty($departmentID)){
            $sql .=" and pri.department_id=$departmentID";
        }
        if(!empty($fieldName)){
            $sql .=" and $fieldName='yes'";
        }
        
        $permReader = Yii::app()->db->createCommand($sql)->query()->readAll();

			
		return $permReader;
	}

	public function getUsersApproved($orderID,$approvalType)
	{
		$sql="SELECT GROUP_CONCAT(`user_id`) as user_ids FROM `order_routing` WHERE approval_type='".$approvalType."' and order_id=".$orderID;
        $approvedReader = Yii::app()->db->createCommand($sql)->queryRow();
		return !empty($approvedReader['user_ids'])?$approvedReader['user_ids']:'';
	}

	public function getOrderRouteData($approval_id = 0)
	{
		$this->currency_fields = array('total_price', 'unit_price', 'expense_price');
		$complete_data = array( 'route_data' => array(), 'order_data' => array(), 'order_details' => array() );

		// first route data
		$route_sql = "SELECT * FROM order_routing WHERE id = $approval_id";
		$complete_data['route_data'] = $this->executeQuery($route_sql, true);
		
		// make sure that the data belongs to this user only
		$my_user_id = Yii::app()->session['user_id'];
		if (!isset($complete_data['route_data']['user_id']) || $complete_data['route_data']['user_id'] != $my_user_id)
		{
			$complete_data['route_data']['routing_status'] = 'Pending';
			$complete_data['route_data']['routing_comments'] = ""; 
		}
		//$complete_data['route_data']['user_id'] = $complete_data['route_data']['user_id'];

		// next order related for this approval
		if ($complete_data['route_data']
				&& is_array($complete_data['route_data'])
					&& isset($complete_data['route_data']['order_id']))
		{
			$order_id = $complete_data['route_data']['order_id'];
			$approval_type = $complete_data['route_data']['approval_type'];
			
			if ($approval_type == 'O')
			{
				$order_sql = "SELECT o.*, l.location_name, d.department_name, v.vendor_name
								FROM orders o, locations l, departments d, vendors v
							   WHERE o.order_id = $order_id AND o.location_id = l.location_id
			  				     AND o.department_id = d.department_id AND o.vendor_id = v.vendor_id";
				$complete_data['order_data'] = $this->executeQuery($order_sql, true);
	
				// and finally products in the order too
				$detail_sql = "SELECT d.*, p.product_name FROM order_details d, products p
								WHERE d.order_id = $order_id AND d.product_id = p.product_id
								  AND d.product_type = 'Product'
	
								UNION
	
								SELECT d.*, b.bundle_name AS product_name
								  FROM order_details d, bundles b
								 WHERE d.order_id = $order_id AND d.product_id = b.bundle_id
								   AND d.product_type = 'Bundle'";
			    $complete_data['order_details'] = $this->executeQuery($detail_sql);
			}

			if ($approval_type == 'E')
			{
				$order_sql = "SELECT e.*, e.total_expenses AS total_price,
									 l.location_name, d.department_name
								FROM expenses e, locations l, departments d
							   WHERE e.expense_id = $order_id AND e.location_id = l.location_id
			  				     AND e.department_id = d.department_id";
				$complete_data['order_data'] = $this->executeQuery($order_sql, true);
	
				// and also expense details
				$detail_sql = "SELECT d.*, v.vendor_name FROM expense_details d, vendors v
				 				WHERE d.vendor_id = v.vendor_id AND d.expense_id = $order_id
				 				ORDER BY d.id";
			    $complete_data['order_details'] = $this->executeQuery($detail_sql);
			}
		}

		$this->currency_fields = array();
		return $complete_data;
	}


	public function getApprovalRouteO($orderData)
	{
		$sql = "SELECT r.*, users.full_name AS user
				FROM order_routing r inner join users on r.user_id=users.user_id
				WHERE r.status = 1  AND approval_type='O' and r.order_id = ".$orderData['order_id']." 
				ORDER BY r.id";
		return $this->executeQuery($sql);
	}
	public function getApprovalRouteE($expenseData)
	{
		$sql = "SELECT r.*, users.full_name AS user 
				FROM order_routing r inner join users on r.user_id=users.user_id
				WHERE r.status = 1  AND approval_type='E' and r.order_id = ".$expenseData['expense_id']."  
				ORDER BY r.id";
		return $this->executeQuery($sql);
	}

	
	
	public function getApprovalRoute($approval_type = '', $order_id = 0)
	{
		$sql = "SELECT r.*, u.full_name AS user 
				  FROM order_routing r, users u 
				 WHERE r.approval_type = '$approval_type' 
				   AND r.order_id = $order_id
				   AND r.user_id = u.user_id 
				   AND r.status = 1
				 ORDER BY r.id";
		return $this->executeQuery($sql);
	}
	
	public function saveOrderStatus()
	{
		$route_id = $this->rs['id'];
		$approval_type = $this->rs['approval_type'];
		$order_id = $this->rs['order_id'];
		$routing_status = $this->rs['routing_status'];
		
		$order_status_sql = "";
		$order_approved = false;
		$total_price = 0;
		if ($route_id && $order_id && $routing_status != 'Pending')
		{
			if ($routing_status == 'Declined')
			{
				if ($approval_type == 'O')
					$order_status_sql = "UPDATE orders SET order_status = 'Declined' WHERE order_id = $order_id";
				else if ($approval_type == 'E')
					$order_status_sql = "UPDATE expenses SET expense_status = 'Declined' WHERE expense_id = $order_id";

				// Send approved Email
				$this->orderRejected($order_id,$approval_type);
			}

			if ($routing_status == 'Information')
			{
				if ($approval_type == 'O')
					$order_status_sql = "UPDATE orders SET order_status = 'More Info Needed' WHERE order_id = $order_id";
				else if ($approval_type == 'E')
					$order_status_sql = "UPDATE expenses SET expense_status = 'More Info Needed' WHERE expense_id = $order_id";

				$this->orderOnHold($order_id,$approval_type);
			}

			// only approved needs check for everything approved or not
			if ($routing_status == 'Approved')
			{
				// get the order data first since we need to know the complete route
				$order_data_sql = "";
				if ($approval_type == 'O')
					$order_data_sql = "SELECT * FROM orders WHERE order_id = $order_id"; 
				else if ($approval_type == 'E')
					$order_data_sql = "SELECT * FROM expenses WHERE expense_id = $order_id";
				
				if (!empty($order_data_sql))
				{
					$order_data = $this->executeQuery($order_data_sql, true);
					if ($order_data && is_array($order_data) 
								&& (isset($order_data['order_id']) || isset($order_data['expense_id'])))
					{
						if ($approval_type == 'O') {
							$total_price =  FunctionManager::orderTotalPrice($order_id);/*$order_data['total_price']*/;
						}
						$location_id = $order_data['location_id'];
						$department_id = $order_data['department_id'];
						
						// get approval route for this order
						$routing_users = array();
						$approval_condition_type = 'A';
						foreach ($this->getRoute($approval_type, $location_id, $department_id) as $route_data)
						{
							$routing_users[] = $route_data['user_id'];
							$approval_condition_type = $route_data['condition_type'];							
						}
						$routing_users = array_unique($routing_users);
						sort($routing_users);

						// and also whatever current approvals have been made
						$approvals_sql = "SELECT * FROM order_routing 
										   WHERE order_id = $order_id 
										     AND approval_type = '$approval_type'";						
						$approvals = $this->executeQuery($approvals_sql);
						
						// check if everyone has approved
						$approved_users = array();
						$manual_approval_granted = false;
						foreach ($approvals as $approval_record)
						{
							if ($approval_record['routing_status'] == 'Approved')
								$approved_users[] = $approval_record['user_id'];
							if ($approval_record['status'] == 1)
								$manual_approval_granted = true;
						}
						$approved_users = array_unique($approved_users);
						sort($approved_users);
						
						// for "OR" condition, even single user approval is fine
						$completely_approved = true;
						/*if ($approval_condition_type == 'O') 
						{
							if ($manual_approval_granted) $completely_approved = true;
						}
						else
						{
							if ($approved_users == $routing_users) $completely_approved = true;
						}*/
							
						if ($completely_approved)
						{
							if ($approval_type == 'O')
							{
								$order_approved = true;
								$order_status_sql = "UPDATE orders SET order_status = 'Approved' WHERE order_id = $order_id";								
							}
							else if ($approval_type == 'E')
								$order_status_sql = "UPDATE expenses SET expense_status = 'Approved' WHERE expense_id = $order_id";
						}
					}

					// Send approved Email
                   $this->orderApproved($order_id,$approval_type);
				}
			}
		}

		if (!empty($order_status_sql)) 
		{
			$this->executeQuery($order_status_sql);
			
			// and if an order was approved, then create purchase order
			if ($order_approved)
			{
				$purchase_order = new PurchaseOrder();
				$purchase_order->add($order_id, $total_price);
			}

			$user_id = 0;
			$order_data_sql = "";
			if ($approval_type == 'O') $order_data_sql = "SELECT * FROM orders WHERE order_id = $order_id";
			else $order_data_sql = "SELECT * FROM expenses WHERE expense_id = $order_id";
			
			if (!empty($order_data_sql))
			{
				$order_data = $this->executeQuery($order_data_sql, true);
				if ($order_data && is_array($order_data) && isset($order_data['user_id'])) 
					$user_id = $order_data['user_id'];

				$final_approval_obtained = false;
				if ($approval_type == 'O')
				{
					if ($order_data && is_array($order_data) 
							&& isset($order_data['order_status']) && $order_data['order_status'] == 'Approved')
						$final_approval_obtained = true; 
				}
				else
				{
					if ($order_data && is_array($order_data) 
							&& isset($order_data['expense_status']) && $order_data['expense_status'] == 'Approved')
						$final_approval_obtained = true; 
				}

				if ($user_id)
				{
					$notification_text = "";
					$notification_url = "";

					if ($approval_type == 'O') 
						$notification_url = '<a href="' . AppUrl::bicesUrl('orders/edit/' . $order_id) . '">Order ID #' . $order_id . '</a>';
					else 
						$notification_url = '<a href="' . AppUrl::bicesUrl('expenses/edit/' . $order_id) . '">Expense ID #' . $order_id . '</a>';
					
					if ($routing_status == 'Declined')
						$notification_text = $notification_url . ' has been declined by ' . Yii::app()->session['full_name']; 

					if ($routing_status == 'Information')
						$notification_text = 'More information has been requested by ' . Yii::app()->session['full_name'] . ' for ' . $notification_url; 

					if ($routing_status == 'Approved')
					{
						if ($final_approval_obtained)
							$notification_text = $notification_url . ' has been completely approved by ' . Yii::app()->session['full_name'];
						else  
							$notification_text = $notification_url . ' has been approved and forwarded over to the next step by ' . Yii::app()->session['full_name'];
					}
					
					$notification = new Notification();
					$notification->rs = array();
					$notification->rs['id'] = 0;
					$notification->rs['user_id'] = $user_id;
					$notification->rs['notification_text'] = $notification_text;
					$notification->rs['notification_date'] = date("Y-m-d H:i");
					$notification->rs['read_flag'] = 0;
					$notification->write();
				}
			}
		}
	}

	public function orderApproved($order_id,$approval_type)
	{

		$userIDs = $this->getUsersApproved($order_id,$approval_type);
		if(!empty($userIDs)){
		$user_data_reader = $this->executeQuery("SELECT * from users  WHERE user_id in (".$userIDs.")");

		
		if(strtolower($approval_type) !='e'){
			$subjectLine='Order Approved';

			$user_data = $this->executeQuery("SELECT * FROM orders INNER JOIN users ON orders.user_id=users.user_id WHERE orders.order_id = $order_id",1);

			$order_data = $this->executeQuery("SELECT orders.currency_id,orders.spend_type,products.product_name,det.order_id,det.calc_unit_price,det.quantity,GetOrderTotalAmount(det.order_id) as converted_total_amount  FROM order_details det
											   INNER JOIN orders ON det.order_id = orders.order_id
	                                           INNER JOIN products ON det.product_id=products.product_id WHERE det.order_id = $order_id");
			$rowHeading = '<tr>
                            <td style="width: 49%"><b>Product Name</b></td>
                            <td style="width: 15%"><b>Product Type</b></td>
                            <td style="width: 12%"><b>Unit Price</b></td>
                            <td style="width: 11%"><b>Quantity</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                           </tr>';
            foreach($order_data as $data){
			$currency = FunctionManager::currencySymbol($data['currency_id']);
			$rowBody  = '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $data['spend_type'].'</td>
                            <td>' . $currency.$data['calc_unit_price'].'</td>
                            <td style="text-align: center">' . $data['quantity'].'</td>
                            <td>' . $currency.$data['converted_total_amount'].'</td>
                            </tr>';
			}

		}else if(strtolower($approval_type) =='e'){
			$subjectLine='Expense Approved';

			$user_data = $this->executeQuery("SELECT * FROM expenses
	        INNER JOIN users ON expenses.user_id=users.user_id WHERE expenses.expense_id = $order_id",1);

			$order_data = $this->executeQuery("SELECT currency_id,expense_types.value as spend_type,exp_det.expense_name as product_name,exp_det.expense_id as order_id,expense_price as calc_unit_price,getExpenseTotalAmount(exp_det.expense_id) as converted_total_amount,tax_rate,getExpenseTotalAmount(exp_det.expense_id)*tax_rate/100 as tax_price  FROM (select exp_det_in.*,exp.currency_id,exp.expense_name from expenses exp inner join expense_details exp_det_in on exp.expense_id=exp_det_in.expense_id) as  exp_det
	            
	            INNER JOIN expense_types ON exp_det.expense_type_id=expense_types.id WHERE exp_det.expense_id = $order_id");
			$rowHeading = '<tr>
                            <td style="width: 49%"><b>Expense title</b></td>
                            <td style="width: 15%"><b>Expense Type</b></td>
                            <td style="width: 15%"><b>Expense Price</b></td>
                            <td style="width: 11%"><b>Tax Rate</b></td>
                            <td style="width: 11%"><b>Tax Amount</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                           </tr>';
            foreach($order_data as $data){
			$currency = FunctionManager::currencySymbol($data['currency_id']);
			$rowBody  = '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $data['spend_type'].'</td>
                            <td>' . $currency.$data['calc_unit_price'].'</td>
                            <td>' . $data['tax_rate'].'%</td>
                            <td>' . $currency.number_format($data['tax_price'],2).'</td>
                            <td>' . $currency.$data['converted_total_amount'].'</td>
                        </tr>';
			}
		}

		



		$mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];
		//foreach($user_data_reader as $user_data){
		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
		$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
		$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;">';
		$email_body .= '<p>Dear ' . (isset($user_data['full_name'])? $user_data['full_name']:'').', </p>';
		$email_body .= '</td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">';
		$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#000000;"><strong>'.$subjectLine.'!! <span style="color:#10a798;">-' .$order_id.'</span> </strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		$email_body .= '<p>Your '.(strtolower($approval_type) !='e'?'order':'expense').' for the below request has been approved. Please check '.(strtolower($approval_type) !='e'?'order':'expense').' status for any additional comments from the approver(s)</p>';
		$email_body .= '<table  border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                           '.$rowHeading.'
            <tr><td colspan="5">&nbsp;</td></tr>';

		$email_body .=$rowBody;
		$email_body .= '</table></div>
             </td>
         </tr>
         <tr>
					 <td style="word-wrap:break-word;font-size:0px;">
					 <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
					 </td>
				 </tr>';

		$email_body .= '<tr>
               <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                <a href="' .AppUrl::bicesUrl((strtolower($approval_type) !='e'?'orders':'expenses').'/edit/'.$order_id).'" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">'.(strtolower($approval_type) !='e'?'Order':'Expense').' Status</a>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;">
					 <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
					 </td>
				 </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;" align="left">
						 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
							 <p>Terms of Service &amp; User Agreement</p>
							 <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
							 <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
						 </div>
					 </td>
				 </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
		$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
		$mail->addAddress($user_data['email'], $user_data['full_name']);
		$mail->Subject = 'Spend 365: '.$subjectLine;
		$mail->isHTML(true);
		$mail->Body = $email_body;
		$mail->send();
		} //}
		return 0;
	}

	public function orderRejected($order_id,$approval_type)
	{

		$userIDs = $this->getUsersApproved($order_id,$approval_type);
		if(!empty($userIDs)){
		$user_data_reader = $this->executeQuery("SELECT * from users  WHERE user_id in (".$userIDs.")");

		if(strtolower($approval_type) !='e'){
			$subjectLine='Order rejected';

			$user_data = $this->executeQuery("SELECT * FROM orders INNER JOIN users ON orders.user_id=users.user_id WHERE orders.order_id = $order_id",1);

			$order_data = $this->executeQuery("SELECT orders.currency_id,orders.spend_type,products.product_name,det.order_id,det.calc_unit_price,det.quantity,GetOrderTotalAmount(det.order_id) as converted_total_amount  FROM order_details det
											   INNER JOIN orders ON det.order_id = orders.order_id
	                                           INNER JOIN products ON det.product_id=products.product_id WHERE det.order_id = $order_id");
			$rowHeading = '<tr>
                            <td style="width: 49%"><b>Product Name</b></td>
                            <td style="width: 15%"><b>Product Type</b></td>
                            <td style="width: 12%"><b>Unit Price</b></td>
                            <td style="width: 11%"><b>Quantity</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                           </tr>';
            foreach($order_data as $data){
			$currency = FunctionManager::currencySymbol($data['currency_id']);
			$rowBody  = '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $data['spend_type'].'</td>
                            <td>' . $currency.$data['calc_unit_price'].'</td>
                            <td style="text-align: center">' . $data['quantity'].'</td>
                            <td>' . $currency.$data['converted_total_amount'].'</td>
                            </tr>';
			}

		}else if(strtolower($approval_type) =='e'){
			$subjectLine='Expense rejected';

			$user_data = $this->executeQuery("SELECT * FROM expenses
	        INNER JOIN users ON expenses.user_id=users.user_id WHERE expenses.expense_id = $order_id",1);

			$order_data = $this->executeQuery("SELECT currency_id,expense_types.value as spend_type,exp_det.expense_name as product_name,exp_det.expense_id as order_id,expense_price as calc_unit_price,getExpenseTotalAmount(exp_det.expense_id) as converted_total_amount,tax_rate,getExpenseTotalAmount(exp_det.expense_id)*tax_rate/100 as tax_price  FROM (select exp_det_in.*,exp.currency_id,exp.expense_name from expenses exp inner join expense_details exp_det_in on exp.expense_id=exp_det_in.expense_id) as  exp_det
	            
	            INNER JOIN expense_types ON exp_det.expense_type_id=expense_types.id WHERE exp_det.expense_id = $order_id");
			$rowHeading = '<tr>
                            <td style="width: 49%"><b>Expense title</b></td>
                            <td style="width: 15%"><b>Expense Type</b></td>
                            <td style="width: 15%"><b>Expense Price</b></td>
                            <td style="width: 11%"><b>Tax Rate</b></td>
                            <td style="width: 11%"><b>Tax Amount</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                           </tr>';
            foreach($order_data as $data){
			$currency = FunctionManager::currencySymbol($data['currency_id']);
			$rowBody  = '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $data['spend_type'].'</td>
                            <td>' . $currency.$data['calc_unit_price'].'</td>
                            <td>' . $data['tax_rate'].'%</td>
                            <td>' . $currency.number_format($data['tax_price'],2).'</td>
                            <td>' . $currency.$data['converted_total_amount'].'</td>
                        </tr>';
			}
		}


		$mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];

		//foreach($user_data_reader as $user_data){
		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
		$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
		$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;">';
		$email_body .= '<p>Dear ' . $user_data['full_name'].', </p>';
		$email_body .= '</td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">';
		$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#000000;"><strong>'.$subjectLine.' <span style="color:#10a798;">-' .$order_id.'</span> </strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		$email_body .= '<p>Your request for the below '.(strtolower($approval_type) !='e'?'order':'expense').' has been rejected. Please check '.(strtolower($approval_type) !='e'?'order':'expense').' status for any additional comments from the approver(s)</p>';
		$email_body .= '<table  border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                           '.$rowHeading.'
                            <tr><td colspan="5">&nbsp;</td></tr>';
        $email_body .= $rowBody;
		
		$email_body .= '</table></div>
             </td>
         </tr><tr>
					 <td style="word-wrap:break-word;font-size:0px;">
					 <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
					 </td>
				 </tr>';

		$email_body .= '<tr>
               <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                <a href="' .AppUrl::bicesUrl((strtolower($approval_type) !='e'?'orders':'expenses').'/edit/'.$order_id).'" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">'.(strtolower($approval_type) !='e'?'Order':'Expense').' Status</a>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;">
					 <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
					 </td>
				 </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;" align="left">
						 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
							 <p>Terms of Service &amp; User Agreement</p>
							 <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
							 <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
						 </div>
					 </td>
				 </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
		$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
		$mail->addAddress($user_data['email'], $user_data['full_name']);
		$mail->Subject = 'Spend 365: '.$subjectLine;
		$mail->isHTML(true);
		$mail->Body = $email_body;
		$mail->send();
		} //}

		return 0;
	}

	public function orderOnHold($order_id,$approval_type)
	{


		$userIDs = $this->getUsersApproved($order_id,$approval_type);
		if(!empty($userIDs)){
		$user_data_reader = $this->executeQuery("SELECT * from users  WHERE user_id in (".$userIDs.")");

		if(strtolower($approval_type) !='e'){
			$subjectLine='Order on Hold';

			$user_data = $this->executeQuery("SELECT * FROM orders INNER JOIN users ON orders.user_id=users.user_id WHERE orders.order_id = $order_id",1);

			$order_data = $this->executeQuery("SELECT orders.currency_id,orders.spend_type,products.product_name,det.order_id,det.calc_unit_price,det.quantity,GetOrderTotalAmount(det.order_id) as converted_total_amount  FROM order_details det
											   INNER JOIN orders ON det.order_id = orders.order_id
	                                           INNER JOIN products ON det.product_id=products.product_id WHERE det.order_id = $order_id");
			$rowHeading = '<tr>
                            <td style="width: 49%"><b>Product Name</b></td>
                            <td style="width: 15%"><b>Product Type</b></td>
                            <td style="width: 12%"><b>Unit Price</b></td>
                            <td style="width: 11%"><b>Quantity</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                           </tr>';
            foreach($order_data as $data){
			$currency = FunctionManager::currencySymbol($data['currency_id']);
			$rowBody  = '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $data['spend_type'].'</td>
                            <td>' . $currency.$data['calc_unit_price'].'</td>
                            <td style="text-align: center">' . $data['quantity'].'</td>
                            <td>' . $currency.$data['converted_total_amount'].'</td>
                            </tr>';
			}

		}else if(strtolower($approval_type) =='e'){
			$subjectLine='Expense on Hold';

			$user_data = $this->executeQuery("SELECT * FROM expenses
	        INNER JOIN users ON expenses.user_id=users.user_id WHERE expenses.expense_id = $order_id",1);

			$order_data = $this->executeQuery("SELECT currency_id,expense_types.value as spend_type,exp_det.expense_name as product_name,exp_det.expense_id as order_id,expense_price as calc_unit_price,getExpenseTotalAmount(exp_det.expense_id) as converted_total_amount,tax_rate,getExpenseTotalAmount(exp_det.expense_id)*tax_rate/100 as tax_price  FROM (select exp_det_in.*,exp.currency_id,exp.expense_name from expenses exp inner join expense_details exp_det_in on exp.expense_id=exp_det_in.expense_id) as  exp_det
	            
	            INNER JOIN expense_types ON exp_det.expense_type_id=expense_types.id WHERE exp_det.expense_id = $order_id");
			$rowHeading = '<tr>
                            <td style="width: 49%"><b>Expense title</b></td>
                            <td style="width: 15%"><b>Expense Type</b></td>
                            <td style="width: 15%"><b>Expense Price</b></td>
                            <td style="width: 11%"><b>Tax Rate</b></td>
                            <td style="width: 11%"><b>Tax Amount</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                           </tr>';
            foreach($order_data as $data){
			$currency = FunctionManager::currencySymbol($data['currency_id']);
			$rowBody  = '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $data['spend_type'].'</td>
                            <td>' . $currency.$data['calc_unit_price'].'</td>
                            <td>' . $data['tax_rate'].'%</td>
                            <td>' . $currency.number_format($data['tax_price'],2).'</td>
                            <td>' . $currency.$data['converted_total_amount'].'</td>
                        </tr>';
			}
		}



		$mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];

		//foreach($user_data_reader as $user_data){
		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
		$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
		$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;">';
		$email_body .= '<p>Dear ' . $user_data['full_name'].', </p>';
		$email_body .= '</td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">';
		$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#000000;"><strong>'.$subjectLine.' <span style="color:#10a798;">-' .$order_id.'</span> </strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		$email_body .= '<p>Your '.(strtolower($approval_type) !='e'?'order':'expense').' for the below request has been on hold. Please check '.(strtolower($approval_type) !='e'?'order':'expense').' status for any additional comments from the approver(s)</p>';
		$email_body .= '<table  border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                           '.$rowHeading.'
                            <tr><td colspan="5">&nbsp;</td></tr>';

		$email_body .= $rowBody;
		$email_body .= '</table></div>
             </td>
         </tr><tr>
					 <td style="word-wrap:break-word;font-size:0px;">
					 <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
					 </td>
				 </tr>';

		$email_body .= '<tr>
               <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                <a href="' .AppUrl::bicesUrl((strtolower($approval_type) !='e'?'orders':'expenses').'/edit/'.$order_id).'" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">'.(strtolower($approval_type) !='e'?'Order':'Expense').' Status</a>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;">
					 <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
					 </td>
				 </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;" align="left">
						 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
							 <p>Terms of Service &amp; User Agreement</p>
							 <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
							 <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
						 </div>
					 </td>
				 </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
		$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
		$mail->addAddress($user_data['email'], $user_data['full_name']);
		$mail->Subject = 'Spend 365: '.$subjectLine;
		$mail->isHTML(true);
		$mail->Body = $email_body;
		$mail->send();
	} //}
		return 0;
	}

	public function saveRoute($routing_data)
	{
		extract($routing_data);
		
		// always delete and insert a new row
		//$this->executeQuery("DELETE FROM order_routing WHERE order_id = $order_id AND approval_type = '$approval_type' AND user_id = $user_id");
		
		$this->rs = array();
		foreach ($this->fields as $field_name => $field_type)
			$this->rs[$field_name] = $routing_data[$field_name];

		//$this->rs['id'] = ;
		$this->rs['status'] = 1;
		$this->rs['routing_date'] = date("Y-m-d H:i:s");
		
		$this->update();
	} 

}
