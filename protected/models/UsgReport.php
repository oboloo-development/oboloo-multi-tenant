<?php
require_once('protected/vendors/phpexcel/PHPExcelReader/excel_reader2.php');
require_once('protected/vendors/phpexcel/PHPExcelReader/SpreadsheetReader.php');
class UsgReport extends Common
{
	public function savingCountSatus(){
		$from_date = date("Y-m-d", strtotime($_POST['report_from_date']));
		$to_date   = date("Y-m-d" ,strtotime($_POST['report_to_date']));
		
		$where 	   = ' 1 ';
	  if($from_date != '1970-01-01' && $to_date != '1970-01-01') {
		 $where = " DATE(f.field_name) >='".$from_date."' and  DATE(f.field_name) <='".$to_date."' ";
	  }

	   $sql= " SELECT 
		SUM(CASE When s.status = 1  Then 1 Else 0 End ) as 'Completed',
		SUM(CASE When s.status = 2  Then 1 Else 0 End ) as 'Planned',
		SUM(CASE When s.status = 3  Then 1 Else 0 End ) as 'Active',
		SUM(CASE When s.status = 4  Then 1 Else 0 End ) as 'Cancelled',
		SUM(CASE When s.status !=0  Then 1 Else 0 End ) as 'All'
		FROM savings s 
		 LEFT join milestone_field f on s.id=f.saving_id 
		 where ". $where . " GROUP BY s.id "; 
		$savingRecord = Yii::app()->db->createCommand($sql)->queryAll(); 
		return $savingRecord;
	}
	public function savingCountArea(){
		$savingStatus = FunctionManager::savingStatusIgnore();
		$from_date = date("Y-m-d", strtotime($_POST['report_from_date']));
		$to_date   = date("Y-m-d" ,strtotime($_POST['report_to_date']));
		
		$where 	   = ' 1 ';
		if($from_date != '1970-01-01' && $to_date != '1970-01-01') {
			 $where = " DATE(f.field_name) >='".$from_date."' and  DATE(f.field_name) <='".$to_date."' ";
		}


		$sql= "SELECT 
		 SUM(CASE When s.saving_area = 4  Then 1 Else 0 End ) as 'Direct_Materials',
		 SUM(CASE When s.saving_area = 6  Then 1 Else 0 End ) as 'Energy',
		 SUM(CASE When s.saving_area = 3  Then 1 Else 0 End ) as 'Indirect_Materials',
		 SUM(CASE When s.saving_area = 5  Then 1 Else 0 End ) as 'Capital_Mobile'
		 FROM savings s 
		 left join milestone_field f on s.id=f.saving_id 
		 where ". $where . " and s.status not in(".$savingStatus.")  GROUP BY s.id "; 
		$savingRecord = Yii::app()->db->createCommand($sql)->queryAll(); 
		return $savingRecord;
	}
	public function savingAmountByArea($from_date, $to_date){
		$currency       = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();
		$sql= "SELECT 
		SUM(CASE When s.saving_area = 4  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Direct_Materials',
		SUM(CASE When s.saving_area = 4  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Direct_Materials',
		SUM(CASE When s.saving_area = 6  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Energy',
		SUM(CASE When s.saving_area = 6  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Energy',
		SUM(CASE When s.saving_area = 3  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Indirect_Materials',
		SUM(CASE When s.saving_area = 3  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Indirect_Materials',
		SUM(CASE When s.saving_area = 5  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Capital_Mobile',
		SUM(CASE When s.saving_area = 5  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Capital_Mobile',
		SUM(currencyConversion(field_value,currency_rate,currency_id,'".$currency."')) as grand_total
		FROM savings s  inner join milestone_field f on  s.id=f.saving_id where s.status not in(".$savingStatus.") and field_name>='".$from_date."' and field_name <='".$to_date."'";
		//+sum(currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."'))
		$savingRecord = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingRecord;
	}
	public function savingAreaAndType($from_date, $to_date){
		/*$sql= "SELECT 
		CASE 
			When s.saving_area = 4 and saving_type=2   
				Then (SELECT sum(mf.field_value) as total_value  FROM milestone_field mf where mf.saving_id=s.id )
			Else 0 
		End  as 'DM_avoidance'
		CASE
		    When s.saving_area = 4 and saving_type=3   
				Then (SELECT sum(mf.field_value) as total_value  FROM milestone_field mf where mf.saving_id=s.id )
			Else 0 
		End  as 'DM_containment'
		
		FROM savings s where 1 order by s.id desc ";*/
		$currency       = Yii::app()->session['user_currency'];
		$sql= "SELECT sa.value as area_title,s.saving_area,s.saving_type,currencyConversion(mf.field_value,s.currency_rate,s.currency_id,'".$currency."') as  field_value
	   FROM savings s 
	   INNER JOIN  milestone_field mf on s.id=mf.saving_id
	   INNER JOIN saving_area sa on s.saving_area=sa.id where field_name>='".$from_date."' and field_name <='".$to_date."'  
		ORDER BY s.saving_area DESC ";
		$savingRecord = Yii::app()->db->createCommand($sql)->query()->readAll();
		$arr = array('Direct Materials'=>array(1=>0,2=>0,3=>0),'Energy'=>array(1=>0,2=>0,3=>0),'Indirect Materials'=>array(1=>0,2=>0,3=>0),'Capital/Mobile'=>array(1=>0,2=>0,3=>0));
		foreach($savingRecord as $value){
			$arr[$value['area_title']][$value['saving_type']] = $arr[$value['area_title']][$value['saving_type']]+number_format($value['field_value']/1000000, 2, '.', '');

		}
		return $arr;
	}

	public function plannedRealisedGraph($saving_id){
	 	$sql = "
		 	SELECT 
			SUM(total_project_saving) as planned_total,
			SUM(realised_saving) as realized_total
			FROM savings  where id=".$saving_id;
	    $savingsReader = Yii::app()->db->createCommand($sql)->queryRow();
	    return $savingsReader;
	}
	public function areaPlannedRealizedGraph($start, $end){
		
		// bellow planned_savings = total_project_saving-realised_saving
		// currencyConversion(field_value,currency_rate,currency_id,'".$currency."') -  
		// currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
	 	$currency = Yii::app()->session['user_currency'];
	 	$savingStatus = FunctionManager::savingStatusIgnore();
	 	$monthArr = array();
	 	
		/*$start = date("Y-m-01");
        $end = date("Y-m-t", strtotime("+11 months"));*/
		$sql= "SELECT 
		DATE_FORMAT(field_name, '%b-%y') as duration,
		SUM(CASE When s.saving_area = 4  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Direct_Materials',
		SUM(CASE When s.saving_area = 4  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Direct_Materials',

		SUM(CASE When s.saving_area = 6  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Energy',
		SUM(CASE When s.saving_area = 6  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Energy',
		SUM(CASE When s.saving_area = 3  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Indirect_Materials',
		SUM(CASE When s.saving_area = 3  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Indirect_Materials',
		SUM(CASE When s.saving_area = 5  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."') 
		Else 0 End ) as 'P_Capital_Mobile',
		SUM(CASE When s.saving_area = 5  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Capital_Mobile'
		
		FROM savings s inner join milestone_field f on  s.id=f.saving_id where s.status not in(".$savingStatus.") and field_name between '".$start."' and '".$end."' group by duration ";
		//+sum(currencyConversion(realised_saving,currency_rate,currency_id,'".$currency."'))
		$savingRecord = Yii::app()->db->createCommand($sql)->query()->readAll();

        $month      = date("M-y",strtotime($start));

        //Start: get number of months between start and end date
        $durationStart = new DateTime($start);
		$durationEnd = new DateTime($end);
		$diff = $durationStart->diff($durationEnd);

		$yearsInMonths = $diff->format('%r%y') * 12;
		$months = $diff->format('%r%m');
		$totalMonths = $yearsInMonths + $months;

		//End: get number of months between start and end date

        //$monthArr[][]['planned'] = 0;
        //$monthArr[][]['realized'] = 0;
        for($j=3;$j<=6;$j++){
        for($i=0;$i<=$totalMonths;$i++){
        	//if($i !=12){
            	$month      = date("M-y",strtotime($start." + $i months"));
            	$monthArr[$j][$month]['planned'] = 0;
            	$monthArr[$j][$month]['realized'] = 0;
            //}
        	}
        }
        foreach($savingRecord as $value){

        	$monthArr[3][$value['duration']]['planned']  = $value['P_Indirect_Materials'];
        	$monthArr[3][$value['duration']]['realized'] = $value['R_Indirect_Materials'];

        	$monthArr[4][$value['duration']]['planned']  = $value['P_Direct_Materials'];
        	$monthArr[4][$value['duration']]['realized'] = $value['R_Direct_Materials'];

        	$monthArr[5][$value['duration']]['planned']  = $value['P_Capital_Mobile'];
        	$monthArr[5][$value['duration']]['realized'] = $value['R_Capital_Mobile'];

        	$monthArr[6][$value['duration']]['planned']  = $value['P_Energy'];
        	$monthArr[6][$value['duration']]['realized'] = $value['R_Energy'];
        }
		
		return $monthArr;
	}

	public function savingAmountBySavingType($from_date,$to_date){
		$currency       = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();
		$sql= "SELECT 
		SUM(CASE When s.saving_type = 1 Then 
		currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'cost_reduction_realized',
		SUM(CASE When s.saving_type = 1 Then 
		abs(currencyConversion(field_value,currency_rate,currency_id,'".$currency."')-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."'))
		Else 0 End ) as 'cost_reduction_forecasted',
		SUM(CASE When s.saving_type = 2  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
		 else 0 End ) as 'cost_avoidance_realized',
		 SUM(CASE When s.saving_type = 2  Then abs(currencyConversion(field_value,currency_rate,currency_id,'".$currency."')-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."'))
		 Else 0 End ) as 'cost_avoidance_forecasted',
		SUM(CASE When s.saving_type = 3  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
		 Else 0 End ) as 'cost_containment_realized',
		 SUM(CASE When s.saving_type = 3  Then abs(currencyConversion(field_value,currency_rate,currency_id,'".$currency."')-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."'))
		 Else 0 End ) as 'cost_containment_forecasted'
		
		FROM savings s  inner join milestone_field f on  s.id=f.saving_id where s.status not in(".$savingStatus.") and field_name>='".$from_date."' and field_name <='".$to_date."'";
		//+sum(currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."'))
		$savingRecord = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingRecord;
	}


	// Dashboard Phase 2
	public function savingAmountBySavingTypePhase2($filterByYear,$dataArrFilter){
	  $currency = Yii::app()->session['user_currency'];
	  $savingStatus = FunctionManager::savingStatusIgnore();
		
	  $where=  '';
	  if(!empty($filterByYear)) $where = " and YEAR(field_name) ='".$filterByYear."' ";
	  else $where = " and YEAR(field_name) ='".date("Y")."' ";
	  $filterColumn = $this->filterTableByColumns($dataArrFilter);
		
	  $sql= "SELECT 
		SUM(CASE When s.saving_type = 1 Then 
		currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'cost_reduction_realized',
		SUM(CASE When s.saving_type = 1 Then 
		abs(currencyConversion(field_value,currency_rate,currency_id,'".$currency."')-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."'))
		Else 0 End ) as 'cost_reduction_forecasted',
		SUM(CASE When s.saving_type = 2  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
		 else 0 End ) as 'cost_avoidance_realized',
		 SUM(CASE When s.saving_type = 2  Then abs(currencyConversion(field_value,currency_rate,currency_id,'".$currency."')-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."'))
		 Else 0 End ) as 'cost_avoidance_forecasted',
		SUM(CASE When s.saving_type = 3  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
		 Else 0 End ) as 'cost_containment_realized',
		 SUM(CASE When s.saving_type = 3  Then abs(currencyConversion(field_value,currency_rate,currency_id,'".$currency."')-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."'))
		 Else 0 End ) as 'cost_containment_forecasted'
		
		FROM savings s  inner join milestone_field f on  s.id=f.saving_id where s.status not in(".$savingStatus.") 
		$where $filterColumn ";
		$savingRecord = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingRecord;
	}
	public function savingMetrixAmountBySavingType(&$filterArr){
		$currency     = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();

		$departmentID = $status = $initiativeOwner = $savingsArea = $savingsType = $businsUnit = '';
	 	if(!empty($filterArr['department_id'])) $departmentID = " and s.department_id = '".$filterArr['department_id']."' ";
		if(!empty($filterArr['status'])) $status = " and s.department_id = '".$filterArr['status']."' ";
	 	if(!empty($filterArr['initiative_owner'])) $initiativeOwner = " and s.user_id = '".$filterArr['initiative_owner']."' ";
	 	if(!empty($filterArr['savings_area'])) $savingsArea = " and s.saving_area = '".$filterArr['savings_area']."' ";
	 	if(!empty($filterArr['savings_type'])) $savingsType = " and s.saving_type = '".$filterArr['savings_type']."' ";
	 	if(!empty($filterArr['business_unit'])) $businsUnit = " and s.business_unit = '".$filterArr['business_unit']."' ";

		$filterDate = '';
		if(!empty($filterArr['filter_by_year'])) $filterDate = " and YEAR(field_name) ='".$filterArr['filter_by_year']."' ";

		$sql= " SELECT SUM(CASE When s.saving_type = 2 Then 
			currencyConversion(field_value,s.currency_rate,currency_id,'".$currency."') Else 0 End ) as cost_avoidance_total,
		SUM(CASE When s.saving_type = 3 Then 
			currencyConversion(field_value,s.currency_rate,currency_id,'".$currency."') Else 0 End ) as cost_containment_total,
		SUM(CASE When s.saving_type = 1 Then 
			currencyConversion(field_value,s.currency_rate,currency_id,'".$currency."') Else 0 End ) as cost_reduction_total

		FROM savings s  inner join milestone_field f on  s.id=f.saving_id where s.status not in(".$savingStatus.") ".$filterDate."
		$departmentID $status $initiativeOwner $savingsArea $savingsType $businsUnit ";
		$savingRecord = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingRecord;
	}


	public function areaPlannedRealizedGraphPhase2($yearDate, $dataArrFilter){
	 	$currency = Yii::app()->session['user_currency'];
	 	$savingStatus = FunctionManager::savingStatusIgnore();
	 	$monthArr = array();

	 	$filterDate = '';
		if(!empty($yearDate)) {
			$filterDate = " and YEAR(field_name) ='".$yearDate."' ";
		}else{
			$yearDate = date("Y");
			$filterDate = " and YEAR(field_name) ='".$yearDate."' ";
		}

		$filterColumn = $this->filterTableByColumns($dataArrFilter);
	 	
		/*$start = date("Y-m-01");
        $end = date("Y-m-t", strtotime("+11 months"));*/
		$sql= "SELECT 
		DATE_FORMAT(field_name, '%b-%y') as duration,
		SUM(CASE When s.saving_area = 4  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Direct_Materials',
		SUM(CASE When s.saving_area = 4  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Direct_Materials',

		SUM(CASE When s.saving_area = 6  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Energy',
		SUM(CASE When s.saving_area = 6  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Energy',
		SUM(CASE When s.saving_area = 3  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as 'P_Indirect_Materials',
		SUM(CASE When s.saving_area = 3  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Indirect_Materials',
		SUM(CASE When s.saving_area = 5  Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."') 
		Else 0 End ) as 'P_Capital_Mobile',
		SUM(CASE When s.saving_area = 5  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Capital_Mobile'
		
		FROM savings s inner join milestone_field f on  s.id=f.saving_id where s.status not in(".$savingStatus.") ".$filterDate." 
		$filterColumn group by duration ";
		//+sum(currencyConversion(realised_saving,currency_rate,currency_id,'".$currency."'))
		$savingRecord = Yii::app()->db->createCommand($sql)->query()->readAll();
		$start =$yearDate."-01-01";
		$end =$yearDate."-12-31";
        $month = date("M-y",strtotime($start));
 
        //Start: get number of months between start and end date
        $durationStart = new DateTime($start);
		$durationEnd = new DateTime($end);
		$diff = $durationStart->diff($durationEnd);


		$yearsInMonths = $diff->format('%r%y') * 12;
		$months = $diff->format('%r%m');
		$totalMonths = $yearsInMonths + $months;
		 
		//End: get number of months between start and end date

        for($j=3;$j<=6;$j++){
        for($i=0;$i<=$totalMonths;$i++){
        	//if($i !=12){
            	$month      = date("M-y",strtotime($start." + $i months"));
            	$monthArr[$j][$month]['planned'] = 0;
            	$monthArr[$j][$month]['realized'] = 0;
            //}
        	}
        }
        foreach($savingRecord as $value){

        	$monthArr[3][$value['duration']]['planned']  = $value['P_Indirect_Materials'];
        	$monthArr[3][$value['duration']]['realized'] = $value['R_Indirect_Materials'];

        	$monthArr[4][$value['duration']]['planned']  = $value['P_Direct_Materials'];
        	$monthArr[4][$value['duration']]['realized'] = $value['R_Direct_Materials'];

        	$monthArr[5][$value['duration']]['planned']  = $value['P_Capital_Mobile'];
        	$monthArr[5][$value['duration']]['realized'] = $value['R_Capital_Mobile'];

        	$monthArr[6][$value['duration']]['planned']  = $value['P_Energy'];
        	$monthArr[6][$value['duration']]['realized'] = $value['R_Energy'];
        }
		
		return $monthArr;
	}


	public function savingByTpyeAndAreaPhase2($filterDataArr, $sav_area, $sav_type, $type){
		$currency = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();

		$where = '';
		if(!empty($filterDataArr['filter_by_year'])) $where = " YEAR(field_name) ='".$filterDataArr['filter_by_year']."' and ";
		else $where = " YEAR(field_name) ='".Date("Y")."' and ";

		$filterColumns = $this->filterTableByColumnsAjax($filterDataArr);
		$sql= " SELECT 
		SUM(CASE When s.saving_area = ".$sav_area."  and s.saving_type = ".$sav_type." Then 
		currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
		Else 0 End ) as '".$type."'
		FROM savings s  inner join milestone_field f on  s.id=f.saving_id where ".$where." s.status not in(".$savingStatus.") 
		$filterColumns "; 
		$savingRecord = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingRecord;
	}

	public function savingAreaAndTypePhase2($filterByYear, $dataArrFilter){
	
		$where = ' 1 ';
		$filterColumn = $this->filterTableByColumns($dataArrFilter);
		if(!empty($filterByYear)) $where = " YEAR(field_name) ='".$filterByYear."' ";
		else $where = " YEAR(field_name) ='".Date("Y")."' ";
		$currency = Yii::app()->session['user_currency'];

		$sql= "SELECT sa.value as area_title,s.saving_area,s.saving_type,currencyConversion(mf.total_realised_savings,s.currency_rate,s.currency_id,'".$currency."') as  field_value
	   FROM savings s 
	   INNER JOIN  milestone_field mf on s.id=mf.saving_id
	   INNER JOIN saving_area sa on s.saving_area=sa.id where ".$where." $filterColumn ";
	   $sql .= "ORDER BY s.saving_area DESC ";
	   $savingRecord = Yii::app()->db->createCommand($sql)->query()->readAll();

	   $arr = array('Direct Materials'=>array(1=>0,2=>0,3=>0),'Energy'=>array(1=>0,2=>0,3=>0),'Indirect Materials'=>array(1=>0,2=>0,3=>0),'Capital/Mobile'=>array(1=>0,2=>0,3=>0));
		foreach($savingRecord as $value){
			$arr[$value['area_title']][$value['saving_type']] = 
			$arr[$value['area_title']][$value['saving_type']]+number_format($value['field_value'], 2, '.', '');

		}
		return $arr;
	}

	public function savingYTDForecastbyStatusPhase2($filterTableColumns){
	  $savingStatus  = FunctionManager::savingStatusIgnore();
	  $currency = Yii::app()->session['user_currency'];
	  
	  $where = ' 1 ';
	  if(!empty($filterTableColumns['filter_by_year'])) $where = "  YEAR(f.field_name) ='".$filterTableColumns['filter_by_year']."' ";
	  else $where = "  YEAR(f.field_name) ='".Date("Y")."' ";

	  $filterColumn = $this->filterTableByColumnsAjax($filterTableColumns);
	  $sql= " SELECT 
	   @plannedSaving := (SUM(CASE When s.status = 2  Then currencyConversion(field_value, currency_rate,currency_id,'".$currency."')
		-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End )) as Planned,
	   
	   @completed := SUM(CASE When s.status = 1  Then currencyConversion(total_realised_savings, currency_rate,currency_id,'".$currency."') Else 0 End )  Completed,
	   
	   @active :=  SUM(CASE When s.status = 3  Then currencyConversion(total_realised_savings, currency_rate,currency_id,'".$currency."') Else 0 End ) Active,
		
		SUM(CASE When s.status = 4  Then currencyConversion(field_value, currency_rate,currency_id,'".$currency."')
		-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'Cancelled',
		
		SUM(CASE When s.status = 6  Then currencyConversion(field_value, currency_rate,currency_id,'".$currency."') 
		-currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'Pipeline'
		FROM savings s
		LEFT join milestone_field f on s.id=f.saving_id 
		 where ". $where . " $filterColumn GROUP BY s.id ";
		$savingStatusRecord = Yii::app()->db->createCommand($sql)->queryAll();
		return $savingStatusRecord;
	}

	public function savingCountSatusPhase2($filterTableColumns){
	
	  if(!empty($filterTableColumns['filter_by_year'])) $where = "  YEAR(f.field_name) ='".$filterTableColumns['filter_by_year']."' ";
	  else $where = "  YEAR(f.field_name) ='".Date("Y")."' ";

	  $filterColumn = $this->filterTableByColumnsAjax($filterTableColumns);

	  $sql= " SELECT 
	  SUM(CASE When s.status = 1  Then 1 Else 0 End ) as 'Completed',
	  SUM(CASE When s.status = 2  Then 1 Else 0 End ) as 'Planned',
	  SUM(CASE When s.status = 3  Then 1 Else 0 End ) as 'Active',
	  SUM(CASE When s.status = 4  Then 1 Else 0 End ) as 'Cancelled',
	  SUM(CASE When s.status = 6  Then 1 Else 0 End ) as 'Pipeline',
	  SUM(CASE When s.status !=0  Then 1 Else 0 End ) as 'All'
	  FROM savings s 
	  inner join milestone_field f on s.id=f.saving_id 
	  where ". $where . " $filterColumn GROUP BY s.id ";
	  $savingRecord = Yii::app()->db->createCommand($sql)->queryAll(); 
	  return $savingRecord;
	}

	public function savingAmountByAreaPhase2($filterTableColumnsArr){
		$currency       = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();

		$where = ' 1 ';
		if(!empty($filterTableColumnsArr['filter_by_year']))
		  $where = " YEAR(f.field_name) ='".$filterTableColumnsArr['filter_by_year']."' ";
	  	else 
		 $where = " YEAR(f.field_name) ='".Date("Y")."' ";

		$filterColumn = $this->filterTableByColumnsAjax($filterTableColumnsArr);
		$sql= "SELECT 
			SUM(CASE When s.saving_area = 4  Then 
			currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
			Else 0 End ) as 'P_Direct_Materials',
			SUM(CASE When s.saving_area = 4  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Direct_Materials',
			SUM(CASE When s.saving_area = 6  Then 
			currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
			Else 0 End ) as 'P_Energy',
			SUM(CASE When s.saving_area = 6  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Energy',
			SUM(CASE When s.saving_area = 3  Then 
			currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
			Else 0 End ) as 'P_Indirect_Materials',
			SUM(CASE When s.saving_area = 3  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Indirect_Materials',
			SUM(CASE When s.saving_area = 5  Then 
			currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
			Else 0 End ) as 'P_Capital_Mobile',
			SUM(CASE When s.saving_area = 5  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."') Else 0 End ) as 'R_Capital_Mobile',
			SUM(currencyConversion(field_value,currency_rate,currency_id,'".$currency."')) as grand_total,
			SUM(currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')) as total_realised
			FROM savings s  inner join milestone_field f on  s.id=f.saving_id where ". $where ." 
			and s.status not in(".$savingStatus.") $filterColumn ";
		$savingRecord = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingRecord;
	}

	public function goalAmountBySavingAreaPhase2($deptID, $filterByYear){
		$currency       = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();

		!empty($filterByYear) ? $year = $filterByYear : $year = date("Y");
		$sql= "SELECT sum(cost_avoidance + cost_reduction + cost_containment) as total FROM goal where year=".$year." and dept_id=".$deptID." group by dept_id "; 
		$savingRecord = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingRecord;
	}

	public function plannedAndRealisedSavingsBySavingTypePhase2($filterByYear, $dataArrFilter){
	  $currency     = Yii::app()->session['user_currency'];
	  $savingStatus = FunctionManager::savingStatusIgnore();

	  $where=  '';
	  if(!empty($filterByYear)) $where = " and YEAR(field_name) ='".$filterByYear."' ";
	  else $where = " and YEAR(field_name) ='".date("Y")."' ";
	  $filterColumn = $this->filterTableByColumns($dataArrFilter);

	  $sql= "SELECT 
	  IFNULL(SUM(currencyConversion(field_value,s.currency_rate,currency_id,'".$currency."')),0)
      as planned_total,
	  IFNULL(SUM(currencyConversion(f.total_realised_savings,currency_rate,currency_id,'".$currency."')),0) as realized_total
	  FROM savings s  inner join milestone_field f on  s.id=f.saving_id where s.status not in(".$savingStatus.") 
	  $where $filterColumn ";
	  $savingRecord = Yii::app()->db->createCommand($sql)->queryRow();
	  return $savingRecord;
	}


	public function getSavingDeptarmentType($filterByYear){
	
		$sql= " SELECT 
		-- Direct Materials ID == 4 
		IFNULL(SUM(CASE When dept_id = 4  Then cost_avoidance Else 0 End ),0) as 'dm_cost_avoidance', 
		IFNULL(SUM(CASE When dept_id = 4  Then cost_containment Else 0 End ),0) as 'dm_cost_containment', 
		IFNULL(SUM(CASE When dept_id = 4  Then cost_reduction Else 0 End ),0) as 'dm_cost_reduction',
		-- Indirect Materials ID == 3 
		IFNULL(SUM(CASE When dept_id = 3  Then cost_avoidance Else 0 End ),0) as 'im_cost_avoidance', 
		IFNULL(SUM(CASE When dept_id = 3  Then cost_containment Else 0 End ),0) as 'im_cost_containment', 
		IFNULL(SUM(CASE When dept_id = 3  Then cost_reduction Else 0 End ),0) as 'im_cost_reduction',
		-- Capital/Mobile == 5
		IFNULL(SUM(CASE When dept_id = 5  Then cost_avoidance Else 0 End ),0) as 'cm_cost_avoidance', 
		IFNULL(SUM(CASE When dept_id = 5  Then cost_containment Else 0 End ),0) as 'cm_cost_containment', 
		IFNULL(SUM(CASE When dept_id = 5  Then cost_reduction Else 0 End ),0) as 'cm_cost_reduction',
		-- Energy Planned == 6
		IFNULL(SUM(CASE When dept_id = 6  Then cost_avoidance Else 0 End ),0) as 'e_cost_avoidance', 
		IFNULL(SUM(CASE When dept_id = 6  Then cost_containment Else 0 End ),0) as 'e_cost_containment', 
		IFNULL(SUM(CASE When dept_id = 6  Then cost_reduction Else 0 End ),0) as 'e_cost_reduction'

		FROM goal where year=".$filterByYear;
		$data = Yii::app()->db->createCommand($sql)->queryRow();
		return $data; 
	}

	public function savingDeptPlannedRelizedYealyGoal($filterByYear){
		$sql= " SELECT 
		IFNULL(SUM(cost_avoidance + cost_reduction + cost_containment),0) as 'total_goal_yealy' 
		FROM goal where year=".$filterByYear;
		$data = Yii::app()->db->createCommand($sql)->queryRow();

		return $data;
	} 

	private function filterTableByColumns($dataArrFilter){
		$filterByYear = $deptIDFilter = $savingsArea= $savingsType= $businessUnit= $initiativeOwner = '';
	    if(!empty($dataArrFilter['departmentIdFilter'])) $deptIDFilter =" AND s.department_id =".$dataArrFilter['departmentIdFilter'];
	  	if(!empty($dataArrFilter['savingsAreaFilter']))  $savingsArea  =" AND s.saving_area=".$dataArrFilter['savingsAreaFilter'];
	    if(!empty($dataArrFilter['savingsTypeFilter']))  $savingsType  =" AND s.saving_type=".$dataArrFilter['savingsTypeFilter'];
	  	if(!empty($dataArrFilter['businessUnitFilter'])) $businessUnit =" AND s.business_unit=".$dataArrFilter['businessUnitFilter'];
	  	if(!empty($dataArrFilter['initiativeOwnerFilter'])) $initiativeOwner=" AND s.user_id=".$dataArrFilter['initiativeOwnerFilter'];
	  	return $deptIDFilter.''.$savingsArea.''.$savingsType.''.$businessUnit.''.$initiativeOwner;
	}

	private function filterTableByColumnsAjax($dataArrFilter){
		$deptIDFilter = $savingsArea= $savingsType= $businessUnit= $initiativeOwner= $savingsStatus = '';
	    if(!empty($dataArrFilter['department_id'])) $deptIDFilter =" AND s.department_id =".$dataArrFilter['department_id'];
	  	if(!empty($dataArrFilter['savings_area']))  $savingsArea  =" AND s.saving_area=".$dataArrFilter['savings_area'];
	    if(!empty($dataArrFilter['savings_type']))  $savingsType  =" AND s.saving_type=".$dataArrFilter['savings_type'];
	  	if(!empty($dataArrFilter['business_unit'])) $businessUnit =" AND s.business_unit=".$dataArrFilter['business_unit'];
	  	if(!empty($dataArrFilter['initiative_owner'])) $initiativeOwner=" AND s.user_id=".$dataArrFilter['initiative_owner'];
	  	if(!empty($dataArrFilter['saving_status'])) $savingsStatus=" AND s.status=".$dataArrFilter['saving_status'];
	  	return $deptIDFilter.''.$savingsArea.''.$savingsType.''.$businessUnit.''.$initiativeOwner.''.$savingsStatus;
	}
}