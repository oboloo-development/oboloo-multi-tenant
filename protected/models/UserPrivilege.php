<?php

class UserPrivilege extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'user_privilege');
		$this->timestamp = false;
	}

	public function getUserPrivilege($user_id = 0)
	{
		$sql = "SELECT * FROM user_privilege";
		return $this->executeQuery($sql . " WHERE user_id = $user_id ", true);
	}

}