<?php

class Approval extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'approval_type' => 'C',
			'data_type' => 'C',
			'condition_type' => 'C',
			'location_id' => 'L',
			'department_id' => 'L',
			'user_id' => 'L',
			'amount' => 'N'
		);
		parent::__construct('id', 'approval_levels');
		$this->timestamp = false;
	}

}
