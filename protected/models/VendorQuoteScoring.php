<?php

class VendorQuoteScoring extends Common
{
	public function __construct()
	{

		$this->fields = array (
			'id' => 'N',
			'vendor_id' => 'N',
			'quote_id' => 'N',
			'quote_scoring_id'=>'N',
			'created_by_id' => 'N',
			'created_by_name' => 'N',
			'score_id' => 'N',
			'score_percentage' => 'N',
			'score_value' => 'N',
			'total_vendors' => 'N',
			'calculated_value' => 'N',
			'created_at' => 'D',
		);

		parent::__construct('id', 'vendor_quote_scoring');
		$this->timestamp = false;
	}

	
}
