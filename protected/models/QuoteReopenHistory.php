<?php

class QuoteReopenHistory extends Common
{
	public function __construct()
	{

		$this->fields = array (
			'id' => 'N',
			'quote_id'=>'N',
			'vendor_id' => 'N',
			'created_by_id' => 'N',
			'created_by_name' => 'C',
			'quote_start_date' => 'D',
			'qoute_end_date' => 'D',
			'quote_old_start_date' => 'C',
			'quote_old_end_date' => 'C',
			'notes'=>'C',
			'created_at' => 'C',
		);

		parent::__construct('id', 'quote_reopen_history');
		$this->timestamp = false;
	}
	
}
