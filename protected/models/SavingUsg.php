<?php
require_once('protected/vendors/phpexcel/PHPExcelReader/excel_reader2.php');
require_once('protected/vendors/phpexcel/PHPExcelReader/SpreadsheetReader.php');
class SavingUsg extends Common
{
	public function __construct()
	{
		$this->fields = array(
			'id' => 'N',
			'user_id' => 'N',
			'user_name' => 'C',
			'title' => 'C',
			'saving_desc' => 'C',
			'status' => 'N',
			'approver_id' => 'N',
			'approver_name' => 'C',
			'approver_status' => 'C',
			'currency_id' => 'N',
			'currency_rate' => 'C',
			'description' => 'C',
			'category_id' => 'N',
			'subcategory_id' => 'N',
			'location_id' => 'N',
			'department_id' => 'N',
			'vendor_id' => 'N',
			'country' => 'C',
			'saving_type' => 'N',
			'vendor_contact_name' => 'C',
			'vendor_contact_email' => 'C',
			'oracle_supplier_name' => 'C',
			'oracle_supplier_number' => 'C',
			'notes' => 'C',
			'total_project_saving' => 'N',
			'total_project_saving_perc' => 'N',
			'realised_saving' => 'N',
			'realised_saving_perc' => 'N',
			'new_or_incremental_savings' => 'C',
			'new_archived' => 'N',
			'current_budget' => 'N',
			'initially_verified' => 'N',
			'verified' => 'N',
			'supported_by_px' => 'N',
			'start_date' => 'C',
			'due_date' => 'C',
			'contract_id' => 'N',
			'created_at' => 'D',
			'updated_at' => 'D'
		);
		parent::__construct('id', 'savings');
		$this->timestamp = false;
	}

	public function getSavings($location_id, $department_id, $category_id, $subcategory_id, $saving_status, $db_from_date, $vendor_ids)
	{
		$sql = "SELECT s.*,cr.currency,cr.currency_symbol,ss.value as saving_status, u.profile_img,s.user_id,sum(sm.base_line_spend) as total_base_spend,realised_saving/sum(sm.base_line_spend)*100 as realised_saving_perc ,total_project_saving/sum(sm.base_line_spend)*100 as total_project_saving_perc FROM savings s 
				left join currency_rates as cr on s.currency_id = cr.id
				left join users as u on s.user_id = u.user_id
				left join saving_status as ss on s.status = ss.id
				left join saving_milestone as sm on s.id = sm.saving_id
				";

		$where  = "";

		if (!empty($location_id)) {
			$location_id = implode(',', $location_id);
			if (empty($where)) {
				$where = " where ";
			}
			$sql .= $where . "  s.location_id IN ($location_id)";
		}
		if (!empty($department_id)) {
			$department_id = implode(',', $department_id);
			if (empty($where)) {
				$where = " where ";
			} else {
				$where = " AND ";
			}
			$sql .= $where . " s.department_id IN ($department_id)";
		}
		if (!empty($category_id)) {
			$category_id = implode(',', $category_id);
			if (empty($where)) {
				$where = " where ";
			} else {
				$where = " AND ";
			}
			$sql .= $where . " s.category_id IN ($category_id)";
		}
		if (!empty($subcategory_id)) {
			$subcategory_id = implode(',', $subcategory_id);
			if (empty($where)) {
				$where = " where ";
			} else {
				$where = " AND ";
			}
			$sql .= $where . " s.subcategory_id IN ($subcategory_id)";
		}
		//if (!empty($department_id)) $sql .= " AND q.department_id = $department_id ";

		if (!empty($saving_status)) {

			$status = "'" . implode("','", $saving_status) . "'";
			if (empty($where)) {
				$where = " where  ";
			} else {
				$where = " AND ";
			}
			$sql .= $where . " s.status IN ($status)";
		}



		if (!empty($vendor_ids)) {
			$vendor = "'" . implode("','", $vendor_ids) . "'";
			if (empty($where)) {
				$where = " where  ";
			} else {
				$where = " AND ";
			}
			$sql .= $where . " s.vendor_id IN ($vendor)";
		}

		if (!empty($db_from_date)) {

			if (empty($where)) {
				$where = " where  ";
			} else {
				$where = " AND ";
			}

			$sql .= $where . "  DATE_FORMAT(s.due_date,'%Y-%m-%d')= '$db_from_date'";
		}
		if (!empty($db_to_date)) {
			if (empty($where)) {
				$where = " where ";
			} else {
				$where = " AND ";
			}
			$sql .= $where . " DATE_FORMAT(s.due_date,'%Y-%m-%d')<='$db_to_date'";
		}


		return $this->executeQuery($sql . ' GROUP BY s.id ORDER BY s.id DESC ');
	}


	public function getSavingsAjax(
		$saving_id = 0,
		$start = 0,
		$length = 10,
		$search_for = "",
		$order_by = array(),
		$location_id = 0,
		$department_id = 0,
		$category_id = 0,
		$subcategory_id = 0,
		$saving_status = 0,
		$due_date = 0,
		$due_date_to = 0,
		$initiative_owner = 0,
		$savings_area = 0,
		$savings_type = 0,
		$business_unit = 0,
		$filter_by_year,
		$savings_location_country = 0,
		$new_or_incremental_savings = '',
		$new_archived = ''
	) {
		$this->currency_fields = array('contract_value');
		$user_id 	= Yii::app()->session['user_id'];
		$memberType = Yii::app()->session['member_type'];
		$currency   = Yii::app()->session['user_currency'];


		$sql = "SELECT s.*, u.full_name, cr.currency,cr.currency_symbol,ss.value as saving_status, u.profile_img,s.user_id,
        sm.savings_due_date as saving_start_date,

		currencyConversion(s.total_project_saving,s.currency_rate,s.currency_id,'" . $currency . "') AS total_project_saving,
		currencyConversion(s.realised_saving,s.currency_rate,s.currency_id,'" . $currency . "') AS realised_saving

        FROM savings s 
        left join currency_rates as cr on s.currency_id = cr.id
        left join users as u on s.user_id = u.user_id
        left join saving_status as ss on s.status = ss.id
        left join saving_milestone as sm on s.id = sm.saving_id
        left join vendors as v on s.vendor_id =  v.vendor_id
        left join saving_area as s_area on s.saving_area = s_area.id
        left join saving_business_unit as s_business_unit on s.business_unit = s_business_unit.id
     	LEFT join milestone_field f on s.id=f.saving_id";
		$where  = "";
		if (empty($saving_id)) {
			$sql .= " WHERE 1=1 ";
		}

		if (!empty($search_for)) {
			$search_for = trim($search_for);
			$search_for_date = date("Y-m-d", strtotime(strtr($search_for, '/', '-')));

			$sql .= " and ( s.title LIKE '%" . $search_for . "%'";
			$sql .= " or s.id LIKE '%" . $search_for . "%'";
			$sql .= " or s.user_name LIKE '%" . $search_for . "%'";
			$sql .= " or cr.currency LIKE '%" . $search_for . "%'";

			if (!empty(filter_var($search_for, FILTER_SANITIZE_NUMBER_INT))) {
				// $sql .= " or sum(sm.base_line_spend) LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
				$sql .= " or total_project_saving LIKE '%" . filter_var($search_for, FILTER_SANITIZE_NUMBER_INT) . "%'";
				//$sql .= " or total_project_saving/sum(sm.base_line_spend)*100 LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
				$sql .= " or realised_saving LIKE '%" . filter_var($search_for, FILTER_SANITIZE_NUMBER_INT) . "%'";
				$sql .= " or realised_saving LIKE '%" . filter_var($search_for, FILTER_SANITIZE_NUMBER_INT) . "%'";
				//$sql .= " or realised_saving/sum(sm.base_line_spend)*100 LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
			}
			$sql .= " or ss.value LIKE '%" . $search_for . "%'";
			$sql .= " or v.vendor_name LIKE '%" . $search_for . "%'";
			if ($search_for_date != "1970-01-01") {
				$sql .= " or s.due_date='" . $search_for_date . "'";
				$sql .= " or sm.savings_due_date='" . $search_for_date . "'";
			}
			$sql .= " ) ";
		}

		$order_by_clause = "";
		if (is_array($order_by) && count($order_by)) {
			foreach ($order_by as $column) {
				$column_index = $column['column'];
				$column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
				$column_name = "";
				switch ($column_index) {
					case 1:
						$column_name = "id";
						break;
					case 2:
						$column_name = "s.title";
						break;
					case 3:
						$column_name = "sm.savings_due_date";
						break;
					case 4:
						$column_name = "s.due_date";
						break;
					case 5:
						$column_name = "s.user_name";
						break;
					case 6:
						$column_name = "cr.currency";
						break;
					case 7:
						$column_name = "total_base_spend";
						break;
					case 8:
						$column_name = "total_project_saving";
						break;
					case 9:
						$column_name = "total_project_saving_perc";
						break;
					case 10:
						$column_name = "realised_saving";
						break;

					case 11:
						$column_name = "realised_saving_perc";
						break;

					case 12:
						$column_name = "saving_status";
						break;
				}
				if (!empty($column_name)) {
					if (empty($order_by_clause))
						$order_by_clause = $column_name . ' ' . $column_direction;
					else
						$order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
				}
			}
		}

		if (!empty($location_id)) $sql .= " AND s.location_id IN ($location_id)";
		if (!empty($department_id)) $sql .= " AND s.department_id IN ($department_id)";
		if ($category_id)	$sql .= " AND s.category_id = $category_id ";
		if ($subcategory_id) $sql .= " AND s.subcategory_id = $subcategory_id ";
		if (!empty($saving_status)) $sql .= " and s.status IN ($saving_status)";

		if (!empty($due_date)) {
			$datetime = DateTime::createFromFormat(FunctionManager::dateFormat(), $due_date);
			$due_date_search = $datetime->format('Y-m-d');
		}
		if (!empty($due_date_to)) {
			$datetime = DateTime::createFromFormat(FunctionManager::dateFormat(), $due_date_to);
			$due_date_to_search = $datetime->format('Y-m-d');
		}


		if (!empty($due_date_search) && $due_date_search != "1970-01-01")
			$sql .= " and sm.savings_due_date='" . $due_date_search . "'";
		if (!empty($due_date_to_search) && $due_date_to_search != "1970-01-01") {
			$sql .= " and date_format(s.due_date,'%Y-%m-%d')='" . $due_date_to_search . "'";
		}
		if ($initiative_owner) $sql .= " AND s.user_id = $initiative_owner ";
		if ($savings_area) $sql .= " AND s.saving_area = $savings_area ";
		if ($business_unit) $sql .= " AND s.business_unit = $business_unit ";
		if ($savings_type) $sql .= " AND s.saving_type = $savings_type ";
		if ($savings_location_country) $sql .= " AND s.country ='" . $savings_location_country . "' ";
		if ($new_or_incremental_savings) $sql .= " AND s.new_or_incremental_savings ='" . $new_or_incremental_savings . "' ";
		if (!empty($new_archived) || $new_archived == "0")
			$sql .= " AND s.new_archived ='" . $new_archived . "' ";

		if (!empty($filter_by_year)) $sql .= " AND ((YEAR(f.field_name) ='" . $filter_by_year . "') or f.id is null ) ";
		else $sql .= " AND ((YEAR(f.field_name) ='" . Date("Y") . "') or f.id is null ) ";

		if (empty($order_by_clause))
			$order_by_clause = "s.id";
		return $this->executeQuery($sql . " GROUP BY s.id ORDER BY $order_by_clause LIMIT $start, $length ");
	}

	public function getSavingsAjaxCount(
		$saving_id = 0,
		$start = 0,
		$length = 10,
		$search_for = "",
		$order_by = array(),
		$location_id = 0,
		$department_id = 0,
		$category_id = 0,
		$subcategory_id = 0,
		$saving_status = 0,
		$due_date = 0,
		$due_date_to = 0,
		$initiative_owner = 0,
		$savings_area = 0,
		$savings_type = 0,
		$business_unit = 0,
		$filter_by_year,
		$savings_location_country = 0,
		$new_or_incremental_savings = '',
		$new_archived = ''
	) {
		$this->currency_fields = array('contract_value');
		$user_id 	= Yii::app()->session['user_id'];
		$memberType = Yii::app()->session['member_type'];
		$currency   = Yii::app()->session['user_currency'];


		$sql = "SELECT s.* 

        FROM savings s 
        left join currency_rates as cr on s.currency_id = cr.id
        left join users as u on s.user_id = u.user_id
        left join saving_status as ss on s.status = ss.id
        left join saving_milestone as sm on s.id = sm.saving_id
        left join vendors as v on s.vendor_id =  v.vendor_id
        left join saving_area as s_area on s.saving_area = s_area.id
        left join saving_business_unit as s_business_unit on s.business_unit = s_business_unit.id
     	LEFT join milestone_field f on s.id=f.saving_id";
		$where  = "";
		if (empty($saving_id)) {
			$sql .= " WHERE 1=1 ";
		}

		if (!empty($search_for)) {
			$search_for = trim($search_for);
			$search_for_date = date("Y-m-d", strtotime(strtr($search_for, '/', '-')));

			$sql .= " and ( s.title LIKE '%" . $search_for . "%'";
			$sql .= " or s.id LIKE '%" . $search_for . "%'";
			$sql .= " or s.user_name LIKE '%" . $search_for . "%'";
			$sql .= " or cr.currency LIKE '%" . $search_for . "%'";

			if (!empty(filter_var($search_for, FILTER_SANITIZE_NUMBER_INT))) {
				// $sql .= " or sum(sm.base_line_spend) LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
				$sql .= " or total_project_saving LIKE '%" . filter_var($search_for, FILTER_SANITIZE_NUMBER_INT) . "%'";
				//$sql .= " or total_project_saving/sum(sm.base_line_spend)*100 LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
				$sql .= " or realised_saving LIKE '%" . filter_var($search_for, FILTER_SANITIZE_NUMBER_INT) . "%'";
				$sql .= " or realised_saving LIKE '%" . filter_var($search_for, FILTER_SANITIZE_NUMBER_INT) . "%'";
				//$sql .= " or realised_saving/sum(sm.base_line_spend)*100 LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
			}
			$sql .= " or ss.value LIKE '%" . $search_for . "%'";
			$sql .= " or v.vendor_name LIKE '%" . $search_for . "%'";
			if ($search_for_date != "1970-01-01") {
				$sql .= " or s.due_date='" . $search_for_date . "'";
				$sql .= " or sm.savings_due_date='" . $search_for_date . "'";
			}
			$sql .= " ) ";
		}

		$order_by_clause = "";
		if (is_array($order_by) && count($order_by)) {
			foreach ($order_by as $column) {
				$column_index = $column['column'];
				$column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
				$column_name = "";
				switch ($column_index) {
					case 1:
						$column_name = "id";
						break;
					case 2:
						$column_name = "s.title";
						break;
					case 3:
						$column_name = "sm.savings_due_date";
						break;
					case 4:
						$column_name = "s.due_date";
						break;
					case 5:
						$column_name = "s.user_name";
						break;
					case 6:
						$column_name = "cr.currency";
						break;
					case 7:
						$column_name = "total_base_spend";
						break;
					case 8:
						$column_name = "total_project_saving";
						break;
					case 9:
						$column_name = "total_project_saving_perc";
						break;
					case 10:
						$column_name = "realised_saving";
						break;

					case 11:
						$column_name = "realised_saving_perc";
						break;

					case 12:
						$column_name = "saving_status";
						break;
				}
				if (!empty($column_name)) {
					if (empty($order_by_clause))
						$order_by_clause = $column_name . ' ' . $column_direction;
					else
						$order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
				}
			}
		}

		if (!empty($location_id)) $sql .= " AND s.location_id IN ($location_id)";
		if (!empty($department_id)) $sql .= " AND s.department_id IN ($department_id)";
		if ($category_id)	$sql .= " AND s.category_id = $category_id ";
		if ($subcategory_id) $sql .= " AND s.subcategory_id = $subcategory_id ";
		if (!empty($saving_status)) $sql .= " and s.status IN ($saving_status)";

		if (!empty($due_date)) {
			$datetime = DateTime::createFromFormat(FunctionManager::dateFormat(), $due_date);
			$due_date_search = $datetime->format('Y-m-d');
		}
		if (!empty($due_date_to)) {
			$datetime = DateTime::createFromFormat(FunctionManager::dateFormat(), $due_date_to);
			$due_date_to_search = $datetime->format('Y-m-d');
		}


		if (!empty($due_date_search) && $due_date_search != "1970-01-01")
			$sql .= " and sm.savings_due_date='" . $due_date_search . "'";
		if (!empty($due_date_to_search) && $due_date_to_search != "1970-01-01") {
			$sql .= " and date_format(s.due_date,'%Y-%m-%d')='" . $due_date_to_search . "'";
		}
		if ($initiative_owner) $sql .= " AND s.user_id = $initiative_owner ";
		if ($savings_area) $sql .= " AND s.saving_area = $savings_area ";
		if ($business_unit) $sql .= " AND s.business_unit = $business_unit ";
		if ($savings_type) $sql .= " AND s.saving_type = $savings_type ";
		if ($savings_location_country) $sql .= " AND s.country ='" . $savings_location_country . "' ";
		if ($new_or_incremental_savings) $sql .= " AND s.new_or_incremental_savings ='" . $new_or_incremental_savings . "' ";
		if (!empty($new_archived) || $new_archived == "0")
			$sql .= " AND s.new_archived ='" . $new_archived . "' ";

		if (!empty($filter_by_year)) $sql .= " AND ( YEAR(f.field_name) ='" . $filter_by_year . "' or f.id is null) ";
		else $sql .= " AND (YEAR(f.field_name) ='" . Date("Y") . "' or f.id is null)  ";

		if (empty($order_by_clause))
			$order_by_clause = "s.id";

		return Yii::app()->db->createCommand($sql . " group by s.id ")->query()->readAll();
	}

	public function getMilestones($saving_id)
	{
		$sql = "SELECT m.*,s.realised_saving_perc,s.realised_saving,

		(m.cost_avoidance+m.cost_reduction)/m.base_line_spend*100 as total_project_saving_perc,
		(m.cost_avoidance+m.cost_reduction)/m.base_line_spend*100 as total_project_realised_perc,
		(m.realised_cost_avoidance+m.realised_cost_reduction)/m.base_line_spend*100 as total_realised_saving_perc

		 FROM saving_milestone m 

			inner join savings s on m.saving_id = s.id

		 where m.saving_id=" . $saving_id . " ORDER BY m.id DESC";


		return $this->executeQuery($sql);
	}


	public static function getStatus($status = '')
	{

		if (!empty($status)) {
			$sql = "select * from saving_status where id='" . $status . "'";
			$reader = Yii::app()->db->createCommand($sql)->queryRow();
			return $reader['value'];
		} else {
			$sql = "select * from saving_status ORDER BY value ASC";
			$reader = Yii::app()->db->createCommand($sql)->query()->readAll();
			return $reader;
		}
	}

	public function projectSavings($saving_id = 0)
	{
		$sql = " SELECT sum(field_value) as total_amount FROM milestone_field where saving_id=" . $saving_id;
		$totalReader = Yii::app()->db->createCommand($sql)->queryRow();
		return !empty($totalReader) ? $totalReader['total_amount'] : 0;
	}

	public function realizedSavings($saving_id = 0)
	{
		$sql = " SELECT sum(total_realised_savings) as total_amount FROM milestone_field where saving_id=" . $saving_id;
		$totalReader = Yii::app()->db->createCommand($sql)->queryRow();
		return !empty($totalReader) ? $totalReader['total_amount'] : 0;
	}



	public function checkApprovel($saving_id = 0)
	{
		$sql = " select s.user_id,s.approver_id,s.approver_name,s.status from savings s where id=" . $saving_id;
		$savingCheck = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingCheck;
	}

	public function checkValidateApprovel($saving_id = 0)
	{
		$sql = " select s.user_id,s.validate_approver_id,s.validate_approver_name,s.status from savings s where id=" . $saving_id;
		$savingCheck = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingCheck;
	}

	public function validateApprovelDate($saving_id = 0)
	{
		$sql = " select approver_datetime from saving_validate_approval_history  where saving_id=" . $saving_id . " order by id desc";
		$history = Yii::app()->db->createCommand($sql)->queryRow();
		return $savingCheck;
	}

	public function getMetrix($from_date, $to_date)
	{


		$currencySymbol = Yii::app()->session['user_currency_symbol'];
		$currency       = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();

		// sum(currencyConversionOldCurrency(realised_saving,s.id,s.currency_rate,currency_id,'".$currency."')) as realised_total 

		$sql = "select 
		 sum(currencyConversion(total_realised_savings,s.currency_rate,currency_id,'" . $currency . "')) as realised_total 
		 FROM savings as s  inner join milestone_field f on  s.id=f.saving_id
		 where  s.status not in(" . $savingStatus . ") and ( field_name>='" . $from_date . "') and field_name <='" . $to_date . "'";
		$realised = Yii::app()->db->createCommand($sql)->queryRow();

		$sql = "select
	 	 sum(coalesce(currencyConversion(field_value,s.currency_rate,currency_id,'" . $currency . "'),0)) as total_project
		 from savings s  inner join milestone_field f on  s.id=f.saving_id WHERE  s.status not in(" . $savingStatus . ")  and (field_name>='" . $from_date . "')  and field_name <='" . $to_date . "' ";
		$project = Yii::app()->db->createCommand($sql)->queryRow();

		$sql = "select 
     	sum(currencyConversion(f.field_value,s.currency_rate,currency_id,'" . $currency . "')) as planned_savings 
		from savings as s inner join milestone_field f on  s.id=f.saving_id where s.status not in(" . $savingStatus . ") 
		and field_name>='" . $from_date . "' and field_name <='" . $to_date . "' ";
		$forecastedSaving = Yii::app()->db->createCommand($sql)->queryRow();


		$sql = "select 
     	(sum(currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+
     	sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'" . $currency . "')))
     	/
     	sum(currencyConversion(sm.base_line_spend,s.currency_rate,currency_id,'" . $currency . "'))
     	as total_project_baseline
		from savings as s inner join saving_milestone sm on s.id=sm.saving_id where s.status not in(" . $savingStatus . ") 
		and sm.status is null and  s.due_date>='" . $from_date . "'   and s.due_date <='" . $to_date . "' ";
		$projectSaving = Yii::app()->db->createCommand($sql)->queryRow();

		// Start: Currency Conversion
		$metrix1 = $realised['realised_total'];
		$metrix3 = $project['total_project'];
		// End: Currency Conversion
		return array(
			'metrix_1' => $metrix1, 'metrix_3' => $metrix3,
			'metrix_2' => $forecastedSaving['planned_savings'], 'metrix_4' => "0"
		);
	}

	public function getChartList($from_date, $to_date)
	{
		// Start: Project, Realised and Department


		if (!empty($_POST['report_of_location'])) {
			$locationFilter = 's.location_id=' . $_POST['report_of_location'] . ' and ';
		} else {
			$locationFilter = '';
		}

		$savingStatus = FunctionManager::savingStatusIgnore();

		$currency = Yii::app()->session['user_currency'];

		$sql = "select d.department_name,
       	sum(currencyConversion(sm.realised_cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+sum(currencyConversion(sm.realised_cost_reduction,s.currency_rate,currency_id,'" . $currency . "')) as realised_total,

		sum(currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'" . $currency . "')) as project_total

		from savings as s 
		inner join departments d on s.department_id = d.department_id 
		inner join locations l on s.location_id = l.location_id 
		inner join saving_milestone sm on sm.saving_id=s.id  
		where s.status not in(" . $savingStatus . ") and " . $locationFilter . " s.due_date>='" . $from_date . "' and s.due_date <='" . $to_date . "'  group by s.department_id ORDER BY d.department_name asc";
		//ORDER BY realised_total,project_total asc

		$proRealisedDept = Yii::app()->db->createCommand($sql)->queryAll();
		$projectRealisedArr = array();

		foreach ($proRealisedDept as $value) {

			$projectRealisedArr['project'][] = $value['project_total'] > 0 ? number_format($value['project_total'], 0, '.', '') : 0;
			$projectRealisedArr['realised'][] = $value['realised_total'] > 0 ? number_format($value['realised_total'], 0, '.', '') : 0;
			$projectRealisedArr['department'][] = $value['department_name'];
		}

		// End: Project, Realised and Department




		$sql = "select c.value,
	 		sum(currencyConversion(sm.realised_cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+sum(currencyConversion(sm.realised_cost_reduction,s.currency_rate,currency_id,'" . $currency . "')) as realised_total,
	 		sum(currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'" . $currency . "')) as total_project
			 from savings as s 

			 inner join categories c on s.category_id=c.id 
			 inner join saving_milestone sm on sm.saving_id=s.id 
			 where s.status not in(" . $savingStatus . ") and s.due_date>='" . $from_date . "' and s.due_date <='" . $to_date . "'
			 group by s.category_id order by c.value ASC";
		$proRealisedCat = Yii::app()->db->createCommand($sql)->queryAll();
		$projectRealisedCatArr = array();
		foreach ($proRealisedCat as $value) {
			$projectRealisedCatArr['project'][] = $value['total_project'] > 0 ? number_format($value['total_project'], 0, '.', '') : 0;
			$projectRealisedCatArr['realised'][] = $value['realised_total'] > 0 ? number_format($value['realised_total'], 0, '.', '') : 0;
			$projectRealisedCatArr['category'][] = $value['value'];
		}
		/*if(isset($_POST['report_from_date']) && isset($_POST['report_to_date'])){*/
		$orderByMonth = 'num_month_order';
		/*}else{
			$orderByMonth = 'num_month';
		}*/

		$sql = " select 
	  sum(currencyConversion(sm.cost_avoidance,s.currency_rate,s.currency_id,'" . $currency . "'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,s.currency_id,'" . $currency . "')) as total_project_saving,
	  	sum(currencyConversion(sm.realised_cost_avoidance,s.currency_rate,s.currency_id,'" . $currency . "'))+sum(currencyConversion(sm.realised_cost_reduction,s.currency_rate,s.currency_id,'" . $currency . "')) as total_realised_saving,
	  	date_format(sm.due_date,'%b-%y') as month, date_format(sm.due_date,'%m') as num_month,date_format(sm.due_date,'%Y%m') as num_month_order
	  	from saving_milestone sm inner join savings s on sm.saving_id=s.id  where s.status not in(" . $savingStatus . ") and s.due_date !='0000-00-00 00:00:00' and s.due_date>='" . $from_date . "' and s.due_date <='" . $to_date . "' group by month ORDER BY " . $orderByMonth . " ASC ";
		$proRealisedMonth = Yii::app()->db->createCommand($sql)->queryAll();
		$projectRealisedMonthArr = array();

		$i = 1;

		$month = strtotime($from_date);
		while ($i <= 12) {
			$month_name = date('M-y', $month);
			$projectRealisedMonthArr['project'][$month_name] = 0;
			$projectRealisedMonthArr['realised'][$month_name] = 0;
			$projectRealisedMonthArr['month'][$month_name] = $month_name;
			$month = strtotime('+1 month', $month);
			$i++;
		}
		// echo "<pre>";print_r($projectRealisedMonthArr);
		foreach ($proRealisedMonth as $value) {
			$projectRealisedMonthArr['project'][$value['month']] = $value['total_project_saving'] > 0 ? number_format($value['total_project_saving'], 0, '.', '') : 0;
			$projectRealisedMonthArr['realised'][$value['month']] = $value['total_realised_saving'] > 0 ? number_format($value['total_realised_saving'], 0, '.', '') : 0;
			$projectRealisedMonthArr['month'][$value['month']] = $value['month'];
		}

		return array(
			'chart_1' => $projectRealisedArr, 'chart_2' => $projectRealisedCatArr,
			'chart_3' => $projectRealisedMonthArr
		);
	}

	public function saveRecordCurrency($saving_id)
	{

		$currencyObj  = new CurrencyRate();
		$currencyList = $currencyObj->getAllCurrencyRate();
		foreach ($currencyList as $value) {
			$currencyID = $value['id'];
			$currency = $value['currency'];
			$currencySymbol = $value['currency_symbol'];
			$currencyRate = $value['rate'];

			$record = new CurrencyRecord;
			$record->rs = array();
			$record->rs['record_id'] = $saving_id;
			$record->rs['record_type'] = 'Saving';
			$record->rs['currency_id'] = $currencyID;
			$record->rs['currency'] = $currency;
			$record->rs['currency_symbol'] = $currencySymbol;
			$record->rs['currency_rate'] = $currencyRate;
			$record->write();
		}
	}

	public function getVendorSavings($vendor_id)
	{
		$sql = "SELECT s.*,cr.currency,cr.currency_symbol,ss.value as saving_status, u.full_name as user_name, u.profile_img,s.user_id,sum(sm.base_line_spend) as total_base_spend FROM savings s 
				  LEFT JOIN users u ON s.user_id = u.user_id
				  left join currency_rates as cr on s.currency_id = cr.id
				  INNER JOIN vendors v ON s.vendor_id = v.vendor_id
				  left join saving_status as ss on s.status = ss.id
				  left join saving_milestone as sm on s.id = sm.saving_id
				  WHERE v.vendor_id = $vendor_id";

		return $this->executeQuery($sql . ' GROUP BY s.id ORDER BY s.id DESC ');
	}

	public function getContractSavings($contract_id)
	{
		$sql = "SELECT s.*,cr.currency,cr.currency_symbol,ss.value as saving_status, u.full_name as user_name, u.profile_img,s.user_id,sum(sm.base_line_spend) as total_base_spend FROM savings s 
				  LEFT JOIN users u ON s.user_id = u.user_id
				   left join currency_rates as cr on s.currency_id = cr.id
				  INNER JOIN contracts c ON s.contract_id = c.contract_id
				  left join saving_status as ss on s.status = ss.id
				  left join saving_milestone as sm on s.id = sm.saving_id
				  WHERE c.contract_id = $contract_id";

		return $this->executeQuery($sql . ' GROUP BY s.id ORDER BY s.id DESC ');
	}

	public function export($data = array(), $file = '')
	{
		// bellow planned_savings = total_project_saving-realised_saving
		// -- (s.total_project_saving - s.realised_saving) as Total_Project_Savings,
		$sql = "SELECT 
		 s.id as Saving_ID, 
		 s.title as Savings_Initiative_Name ,
		 s.notes as Savings_Description, 
		 ss.value as Saving_Status,
		 cr.currency as Saving_Currency,
		 c.value as Saving_Category,
		 sc.value as Saving_Sub_Category, 
		 l.location_name as Savings_Oracle_Location,
		 d.department_name as Operating_Unit,
		 s.oracle_supplier_name as Oracle_supplier_name,
		 s.oracle_supplier_number as Oracle_supplier_number,
		 s.country as Savings_Location_Country,
			CASE 
		        WHEN s.saving_type = '1' THEN  'Cost Reduction'
		        WHEN s.saving_type = '2' THEN  'Cost Avoidance'
		        WHEN s.saving_type = '3' THEN  'Cost Containment' 
		        ELSE '' 
		    END AS Savings_Type,
			s_area.value as Savings_Area,
			s_business_unit.value as Business_Unit,
			s.special_event as Savings_Resulted_From_A_Special_Event,
			s.event_name as Event_Name_And_Details,
			s.total_project_saving as Total_Project_Savings,
			s.realised_saving as Realized_Savings,
			TRIM(DATE_FORMAT(s.due_date,'%d/%m/%Y')) as Savings_Due_Date,
			TRIM(DATE_FORMAT(sm.savings_due_date,'%d/%m/%Y')) as Savings_Start_Date,
			s.user_name as Created_By

		    FROM savings as s
		    left join saving_status as ss on s.status = ss.id
		    left join saving_milestone as sm on s.id = sm.saving_id
		    left join currency_rates as cr on s.currency_id = cr.id
		    left join categories as c on s.category_id = c.id
		    left join sub_categories as sc on s.subcategory_id = sc.id
		    left join locations as l on s.location_id = l.location_id
		    left join departments as d on s.department_id = d.department_id
		    left join saving_area as s_area on s.saving_area = s_area.id
		    left join saving_business_unit as s_business_unit on s.business_unit = s_business_unit.id
		    group by s.id ORDER BY s.id desc";
		parent::export($this->executeQuery($sql), 'savings');
	}


	public function getMetrixPhase2($filterArr)
	{

		$currencySymbol = Yii::app()->session['user_currency_symbol'];
		$currency     = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();
		$filterDate   = '';
		$filterColumn = $this->filterTableByColumns($filterArr);

		if (!empty($filterArr['filter_by_year'])) $filterDate = " and YEAR(f.field_name) = '" . $filterArr['filter_by_year'] . "' ";
		else $filterDate = " and YEAR(f.field_name) = '" . date("Y") . "' ";

		if (!empty($filterArr['filter_by_year'])) {
			$lastyear = $filterArr['filter_by_year'] - 1;
			$filterDateCarry = " and YEAR(f.field_name) ='" . $lastyear . "' ";
		} else {
			$lastyear = date("Y") - 1;
			$filterDateCarry = " and YEAR(f.field_name) ='" . $lastyear . "' ";
		}

		if (!empty($filterArr['filter_by_year'])) {
			$nextyear = $filterArr['filter_by_year'] + 1;
			$filterDateIncremental  = " and YEAR(f.field_name) ='" . $nextyear . "' ";
		} else {
			$nextyear = date("Y") + 1;
			$filterDateIncremental = " and YEAR(f.field_name) ='" . $nextyear . "' ";
		}

		if (!empty($filterArr['filter_by_year'])) $filterByYear = " year ='" . $filterArr['filter_by_year'] . "' ";
		else $filterByYear = " year ='" . date("Y") . "' ";

		$sql = "select 
	  sum(currencyConversion(total_realised_savings,s.currency_rate,currency_id,'" . $currency . "')) as realised_total 
	  FROM savings as s  inner join milestone_field f on  s.id=f.saving_id
	  where s.status not in(" . $savingStatus . ") 
	  $filterDate $filterColumn ";
		$realised = Yii::app()->db->createCommand($sql)->queryRow();

		$sql = "select
	  sum(coalesce(currencyConversion(field_value,s.currency_rate,currency_id,'" . $currency . "'),0)) as total_project
	  from savings s  inner join milestone_field f on  s.id=f.saving_id WHERE  s.status not in(" . $savingStatus . ") 
	  $filterDate $filterColumn ";
		$project = Yii::app()->db->createCommand($sql)->queryRow();

		// Total Forecasted Savings Saving
		$sql = "select 
	  sum(currencyConversion(field_value,s.currency_rate,currency_id,'" . $currency . "'))
	  as total_forecasted_sav 
	  from savings as s inner join milestone_field f on  s.id=f.saving_id where s.status not in(" . $savingStatus . ") 
	  $filterDate $filterColumn ";
		$totalForecasted = Yii::app()->db->createCommand($sql)->queryRow();

		// Total Carry Over Savings
		$sql = "select 
      sum(currencyConversion(field_value,s.currency_rate,currency_id,'" . $currency . "'))
      as planned_savings 
	  from savings as s inner join milestone_field f on  s.id=f.saving_id 
	  where s.status not in(" . $savingStatus . ") and f.status !='Completed' 
	  $filterDateCarry $filterColumn ";
		$totalCarryoverSavings = Yii::app()->db->createCommand($sql)->queryRow();

		// Total Remain Planned Saving
		$sql = "select  
 	  SUM(currencyConversion(field_value,s.currency_rate,currency_id,'" . $currency . "')) - 
 	  SUM(currencyConversion(total_realised_savings,s.currency_rate,currency_id,'" . $currency . "')) as planned_savings 
	  from savings as s inner join milestone_field f on  s.id=f.saving_id 
	  where s.status not in(" . $savingStatus . ") and f.status !='Completed' 
	  $filterDate $filterColumn ";
		$totalRemainPlanned = Yii::app()->db->createCommand($sql)->queryRow();

		// Total Incremental Savings
		$sql = "select 
      sum(currencyConversion(field_value,s.currency_rate,currency_id,'" . $currency . "')) as planned_savings 
	  from savings as s inner join milestone_field f on  s.id=f.saving_id 
	  where s.status not in(" . $savingStatus . ") AND (f.status != 'Completed' OR f.status IS NULL)
	  $filterDateIncremental $filterColumn ";
		$totalIncrementalSavings = Yii::app()->db->createCommand($sql)->queryRow();

		// Total In Current Budget Savings
		$sql = "select 
      sum(currencyConversion(field_value,s.currency_rate,currency_id,'" . $currency . "'))
      as planned_savings 
	  from savings as s inner join milestone_field f on  s.id=f.saving_id 
	  where s.status not in(" . $savingStatus . ") and s.current_budget=1 $filterDate $filterColumn ";
		$totalInCurrentBudgetSavings = Yii::app()->db->createCommand($sql)->queryRow();

		// Total Goal
		$sql = "select sum(cost_avoidance + cost_reduction + cost_containment) as total
		 FROM goal where " . $filterByYear;
		$totalGoalCurrentYear  = Yii::app()->db->createCommand($sql)->queryRow();

		// Start: Currency Conversion
		$metrix1 = $realised['realised_total'];
		$metrix3 = $project['total_project'];
		// End: Currency Conversion
		return array(
			'metrix_1' => $metrix1, 'metrix_3' => $metrix3, 'metrix_4' => "0",
			'metrix_2' =>				    $totalForecasted['total_forecasted_sav'],
			'totalCarryoverSavings'	  	 => $totalCarryoverSavings['planned_savings'],
			'totalRemainPlanned' 	  	 => $totalRemainPlanned['planned_savings'],
			'totalIncrementalSavings' 	 => $totalIncrementalSavings['planned_savings'],
			'totalInCurrentBudgetSavings' => $totalInCurrentBudgetSavings['planned_savings'],
			'totalGoalCurrentYear'		 => $totalGoalCurrentYear['total']
		);
	}
	public function getPhase2ChartList($yearDate)
	{
		// Start: Project, Realised and Department

		if (!empty($_POST['report_of_location'])) {
			$locationFilter = ' and s.location_id=' . $_POST['report_of_location'] . ' ';
		} else {
			$locationFilter = '';
		}

		$filterDate = '';
		if (!empty($yearDate)) {
			$filterDate = " and YEAR(f.field_name) ='" . $yearDate . "' ";
		}

		$smfilterDate = '';
		if (!empty($yearDate)) {
			$smfilterDate = " and YEAR(sm.due_date) ='" . $yearDate . "' ";
		}

		$savingStatus = FunctionManager::savingStatusIgnore();
		$currency = Yii::app()->session['user_currency'];

		$sql = "select d.department_name,
       	sum(currencyConversion(sm.realised_cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+sum(currencyConversion(sm.realised_cost_reduction,s.currency_rate,currency_id,'" . $currency . "')) as realised_total,

		sum(currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'" . $currency . "')) as project_total

		from savings as s 
		inner join departments d on s.department_id = d.department_id 
		inner join locations l on s.location_id = l.location_id 
		inner join saving_milestone sm on sm.saving_id=s.id  
		where s.status not in(" . $savingStatus . ")  " . $locationFilter . " " . $smfilterDate . "  group by s.department_id ORDER BY d.department_name asc";
		//ORDER BY realised_total,project_total asc

		$proRealisedDept = Yii::app()->db->createCommand($sql)->queryAll();
		$projectRealisedArr = array();

		foreach ($proRealisedDept as $value) {

			$projectRealisedArr['project'][] = $value['project_total'] > 0 ? number_format($value['project_total'], 0, '.', '') : 0;
			$projectRealisedArr['realised'][] = $value['realised_total'] > 0 ? number_format($value['realised_total'], 0, '.', '') : 0;
			$projectRealisedArr['department'][] = $value['department_name'];
		}

		// End: Project, Realised and Department

		$sql = "select c.value,
	 		sum(currencyConversion(sm.realised_cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+sum(currencyConversion(sm.realised_cost_reduction,s.currency_rate,currency_id,'" . $currency . "')) as realised_total,
	 		sum(currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'" . $currency . "'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'" . $currency . "')) as total_project
			 from savings as s 

			 inner join categories c on s.category_id=c.id 
			 inner join saving_milestone sm on sm.saving_id=s.id 
			 where s.status not in(" . $savingStatus . ") " . $smfilterDate . "
			 group by s.category_id order by c.value ASC";
		$proRealisedCat = Yii::app()->db->createCommand($sql)->queryAll();
		$projectRealisedCatArr = array();
		foreach ($proRealisedCat as $value) {
			$projectRealisedCatArr['project'][] = $value['total_project'] > 0 ? number_format($value['total_project'], 0, '.', '') : 0;
			$projectRealisedCatArr['realised'][] = $value['realised_total'] > 0 ? number_format($value['realised_total'], 0, '.', '') : 0;
			$projectRealisedCatArr['category'][] = $value['value'];
		}

		$orderByMonth = 'num_month_order';

		$sql = " select 
	  	sum(currencyConversion(sm.cost_avoidance,s.currency_rate,s.currency_id,'" . $currency . "'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,s.currency_id,'" . $currency . "')) as total_project_saving,
	  	sum(currencyConversion(sm.realised_cost_avoidance,s.currency_rate,s.currency_id,'" . $currency . "'))+sum(currencyConversion(sm.realised_cost_reduction,s.currency_rate,s.currency_id,'" . $currency . "')) as total_realised_saving,
	  	date_format(sm.due_date,'%b-%y') as month, date_format(sm.due_date,'%m') as num_month,date_format(sm.due_date,'%Y%m') as num_month_order
	  	from saving_milestone sm inner join savings s on sm.saving_id=s.id  where s.status not in(" . $savingStatus . ") and s.due_date !='0000-00-00 00:00:00' " . $smfilterDate . " group by month ORDER BY " . $orderByMonth . " ASC ";
		$proRealisedMonth = Yii::app()->db->createCommand($sql)->queryAll();
		$projectRealisedMonthArr = array();

		$i = 1;

		$month = strtotime($from_date);
		while ($i <= 12) {
			$month_name = date('M-y', $month);
			$projectRealisedMonthArr['project'][$month_name] = 0;
			$projectRealisedMonthArr['realised'][$month_name] = 0;
			$projectRealisedMonthArr['month'][$month_name] = $month_name;
			$month = strtotime('+1 month', $month);
			$i++;
		}

		foreach ($proRealisedMonth as $value) {
			$projectRealisedMonthArr['project'][$value['month']] = $value['total_project_saving'] > 0 ? number_format($value['total_project_saving'], 0, '.', '') : 0;
			$projectRealisedMonthArr['realised'][$value['month']] = $value['total_realised_saving'] > 0 ? number_format($value['total_realised_saving'], 0, '.', '') : 0;
			$projectRealisedMonthArr['month'][$value['month']] = $value['month'];
		}

		return array(
			'chart_1' => $projectRealisedArr, 'chart_2' => $projectRealisedCatArr,
			'chart_3' => $projectRealisedMonthArr
		);
	}

	private function filterTableByColumns($dataArrFilter)
	{
		$deptIDFilter = $savingsArea = $savingsType = $businessUnit = $initiativeOwner = '';
		if (!empty($dataArrFilter['department_id'])) $deptIDFilter = " AND s.department_id =" . $dataArrFilter['department_id'];
		if (!empty($dataArrFilter['savings_area']))  $savingsArea  = " AND s.saving_area=" . $dataArrFilter['savings_area'];
		if (!empty($dataArrFilter['savings_type']))  $savingsType  = " AND s.saving_type=" . $dataArrFilter['savings_type'];
		if (!empty($dataArrFilter['business_unit'])) $businessUnit = " AND s.business_unit=" . $dataArrFilter['business_unit'];
		if (!empty($dataArrFilter['initiative_owner'])) $initiativeOwner = " AND s.user_id=" . $dataArrFilter['initiative_owner'];
		return $deptIDFilter . '' . $savingsArea . '' . $savingsType . '' . $businessUnit . '' . $initiativeOwner;
	}
}
