<?php

class VendorQuickScoring extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'main_quick_scoring_id' => 'N',
			'vendor_id' => 'N',
			'created_by_id' => 'N',
			'created_by_name' => 'N',
			'score_id' => 'N',
			'score_title'=>'N',
			'socre_type'=>'N',
			'score_percentage' => 'N',
			'score_value' => 'N',
			'total_vendors' => 'N',
			'calculated_value' => 'N',
			'created_at' => 'D',
		);
		parent::__construct('id', 'vendor_quick_scoring');
		$this->timestamp = false;
	}

	public function getQuickEveluationList($start = 0, $length = 10, $search_for = "", $order_by = array(), $vendor_id = 0,
	 $category_id=0, $subcategory_id=0){
       
		$sql = "select mqs.*, u.profile_img,u.full_name,c.value as category_name,sub_c.value as sub_category_name,
             
            (select group_concat(distinct vendor_name SEPARATOR ', ') from vendors inner join vendor_quick_scoring qs on vendors.vendor_id=qs.vendor_id where qs.main_quick_scoring_id=mqs.id order by vendor_name asc) as vendor_name,

            (select group_concat(distinct vendor_name SEPARATOR ', ')  from vendors where vendors.vendor_id in(mqs.best_supplier_id) ) as best_vendor_name
            
            from main_quick_scoring mqs 
            inner join categories as c on mqs.category_id = c.id
            inner join sub_categories as sub_c on mqs.subcategory_id = sub_c.id
            inner join  vendor_quick_scoring qs on mqs.id=qs.main_quick_scoring_id 
            inner join  vendors v on qs.vendor_id = v.vendor_id 
            LEFT JOIN users u ON mqs.created_by_id = u.user_id
             where 1 ";

        if(!empty($vendor_id)){
            $sql .= " and qs.vendor_id like '%".$vendor_id."%'";
        } 

        if(!empty($category_id)){
            $sql .= " and mqs.category_id like '%".$category_id."%'";
        } 

        if(!empty($subcategory_id)){
            $sql .= " and mqs.subcategory_id like '%".$subcategory_id."%'";
        }

      
        if (!empty($search_for)) {

            $search_for = trim($search_for);
            $search_for_date = date("Y-m-d",strtotime(strtr($search_for,'/','-')));

            $sql .= " and (mqs.quote_name LIKE '%".$search_for."%'";
            $sql .= " or mqs.quote_desc LIKE '%".$search_for."%'";
            $sql .= " or c.value LIKE '%".$search_for."%'";
            $sql .= " or vendor_name LIKE '%".$search_for."%'";
            $sql .= " or vendor_name LIKE '%".$search_for."%'";
            $sql .= " or mqs.created_by_name LIKE '%".$search_for."%'";
            if($search_for_date !="1970-01-01"){
              $sql .= " or DATE_FORMAT(mqs.created_at,'%Y-%m-%d')='".$search_for_date."'";
            }
            $sql .= " ) ";
        }
         $order_by_clause = "";
         if (is_array($order_by) && count($order_by)) {
            foreach ($order_by as $column) {
                $column_index = $column['column'];
                $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                $column_name = "";
                switch ($column_index) {
                 case 0:
                    $column_name = "mqs.id";
                    break;
                 case 1:
                    $column_name = "quote_name";
                    break;
                 case 2:
                    $column_name = "quote_desc";
                    break;
                 case 3:
                    $column_name = "category_name";
                    break;
                 case 4:
                    $column_name = "vendor_name";
                    break;
                 case 5:
                    $column_name = "best_vendor_name";
                    break;
                 case 6:
                    $column_name = "created_by_name";
                    break;
                 case 7:
                    $column_name = "created_at";
                    break;
                }
                if (! empty($column_name)) {
                    if (empty($order_by_clause))
                        $order_by_clause = $column_name . ' ' . $column_direction;
                    else
                        $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                }
            }
         

        if(!empty($vendor_id))
         $sql .= " and qs.vendor_id like '%".$vendor_id."%'";
        if(!empty($category_id))
         $sql .= " and mqs.category_id like '%".$category_id."%'";
        if(!empty($subcategory_id))
         $sql .= " and mqs.subcategory_id like '%".$subcategory_id."%'";

         if($start != 'none' && $length != 'none'){
            $sql = $sql .= "GROUP BY mqs.id ORDER BY $order_by_clause LIMIT $start, $length";
         }else{
            $sql = $sql .= "GROUP BY mqs.id ORDER BY $order_by_clause ";
         }
    
       return $this->executeQuery($sql);
      }
    }	
}
