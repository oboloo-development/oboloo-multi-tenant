<?php
class VendorRequestDocumentOnboard extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'vendor_id' => 'N',
			'user_id' => 'N',
			'user_name' => 'C',
			'vendor_document_type' => 'N',
			'vendor_document_type_name'=>'C',
			'supplier_contact_name' => 'C',
			'supplier_contact_email' => 'C',
			'note'=>'C',
			'status' => 'C',
			'reference' =>'C',
			'created_at' => 'D'
		);
		parent::__construct('id', 'vendor_request_documents_onboard');
		$this->timestamp = false;
	}
}
