<?php

class Account extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'account_id' => 'N',
			'account_code' => 'C',
			'account_name' => 'C',
			'departments' => 'C',
			'account_type_id' => 'L',
			'parent_account_id' => 'L'
		);
		$this->alternate_key_name = 'account_code';
		parent::__construct('account_id', 'accounts');
		$this->timestamp = false;
	}

	public function getAccounts($account_id = 0)
	{
		$sql = "SELECT a.*, t.value AS account_type, p.account_name AS parent_account_name
		          FROM accounts a
				  LEFT JOIN accounts p ON a.parent_account_id = p.account_id
				  LEFT JOIN account_types t ON a.account_type_id = t.id";

		if ($account_id)
			return $this->executeQuery($sql . " WHERE a.account_id = $account_id ", true);
		else return $this->executeQuery($sql . ' ORDER BY account_name ');
	}

	public function sortForDisplay($accounts = array())
	{
		$sorted = $parents = array();

		// get all the parents first
		foreach ($accounts as $account)
			if ($account['parent_account_id'] == 0)
				$parents[$account['account_id']] = $account;

		// then respective children
		foreach ($accounts as $account)
		{
			if ($account['parent_account_id'] != 0)
			{
				if (isset($parents[$account['parent_account_id']]))
				{
					if (!isset($parents[$account['parent_account_id']]['children']))
						$parents[$account['parent_account_id']]['children'] = array();
					$parents[$account['parent_account_id']]['children'][] = $account;
				}
			}
		}

		// and now send back homogeneous data
		foreach ($parents as $account)
		{
			$children = array();
			if (isset($account['children']))
			{
				$children = $account['children'];
				unset($account['children']);
			}

			$sorted[] = $account;
			if (is_array($children))
				foreach ($children as $child)
					$sorted[] = $child;
		}

		return $sorted;
	}

}
