<?php

class SavingMilestone extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'saving_id' => 'N',
			'title' => 'C',
			'due_date' => 'D',
			'savings_due_date' => 'D',
			'start_date' => 'D',
			'saving_duration' => 'N',
			'base_line_spend' => 'N',
			'cost_reduction' => 'N',
			'cost_avoidance'=>'N',
			'realised_cost_reduction' => 'N',
			'realised_cost_avoidance'=>'N',
			'total_realised_saving' => 'N',
			'total_realised_saving_perc'=>'N',
			'completed_by_id' => 'N',
			'completed_by_name' => 'C',
			'completed_date' => 'D',
			'status' => 'C',
			'notes'=>'C',
			'created_at' => 'D',
		);
		parent::__construct('id', 'saving_milestone');
		$this->timestamp = false;
	}
}
