<?php

class ExpenseDetail extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'expense_id' => 'N',
			'vendor_id' => 'N',
			'expense_type_id' => 'N',
			'expense_date' => 'D',
			'expense_notes' => 'C',
			'expense_price' => 'N',
			'tax_rate' => 'N'
		);
		
		$this->currency_fields = array ( 'expense_price' );
		parent::__construct('id', 'expense_details');
		$this->timestamp = false;
	}

	public function getExpenseDetails($expense_id = 0)
	{
		$sql = "SELECT d.*, v.vendor_name
				  FROM expense_details d, vendors v
				 WHERE d.vendor_id = v.vendor_id
				   AND d.expense_id = $expense_id
				 ORDER BY d.id";
		return $this->executeQuery($sql);
	}

}
