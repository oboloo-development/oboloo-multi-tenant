<?php

class VendorPayment extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'vendor_id' => 'N',
			'payment_type_id' => 'N',
			'payment_type_name' => 'C',
			'currency_code' => 'C'
		);
		parent::__construct('id', 'vendor_payments');
		$this->timestamp = false;
	}
}
