<?php

class Bundle extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'bundle_id' => 'N',
			'bundle_name' => 'C',
			'description' => 'C',
			'price' => 'N'
		);

		$this->currency_fields = array ( 'price' );
		$this->alternate_key_name = 'bundle_name';
		parent::__construct('bundle_id', 'bundles');
	}

	public function getBundles($bundle_id = 0)
	{
		$sql = "SELECT b.*, p.product_id, p.product_name, bp.quantity
				  FROM bundles b, products p, bundle_products bp
				 WHERE b.bundle_id = bp.bundle_id
                   AND b.status = 1
				   AND p.product_id = bp.product_id";
		if ($bundle_id) $sql .= " AND b.bundle_id = $bundle_id ";
		$sql .= " ORDER BY b.bundle_id, p.product_name ";

		$bundles = array();
		foreach ($this->executeQuery($sql) as $bundle)
		{
			if (!isset($bundles[$bundle['bundle_id']]))
			{
				$bundles[$bundle['bundle_id']] = $bundle;
				$bundles[$bundle['bundle_id']]['product_details'] = array();
			}
			$bundles[$bundle['bundle_id']]['product_details'][] = $bundle;
		}

		return $bundles;
	}

	public function export($data = array(), $file = '')
	{
		$bundle_data_with_products = $bundle_data = array();
		$sql = "SELECT bundle_id, bundle_name, description, price FROM bundles";
		foreach ($this->executeQuery($sql) as $bundle) $bundle_data[$bundle['bundle_id']] = $bundle;
		
		if (count($bundle_data))
		{
			$bundle_products = array();
			$sql = "SELECT bp.*, p.product_name FROM bundle_products bp, products p
					 WHERE bp.product_id = p.product_id AND bundle_id IN ( " . implode(",", array_keys($bundle_data)) . " ) ";
			foreach ($this->executeQuery($sql) as $bundle_product)
			{
				if (!isset($bundle_products[$bundle_product['bundle_id']]))
					$bundle_products[$bundle_product['bundle_id']] = array();
				$bundle_products[$bundle_product['bundle_id']][] = $bundle_product;
			}
			
			/*
			foreach ($bundle_data as $bundle_id => $bundle)
			{
				$products = "";
				if (isset($bundle_products[$bundle_id]))
				{
					foreach ($bundle_products[$bundle_id] as $bundle_product)
					{
						$product_description = $bundle_product['product_name'] . ' (' . $bundle_product['quantity'] . ')';
						if ($products == "") $products = $product_description;
						else $products = $products . "\n" . $product_description;
					}
				}

				$bundle['products'] = $products;
				$bundle_data_with_products[] = $bundle;
			}
			*/

			foreach ($bundle_data as $bundle_id => $bundle)
			{
				$products = array();
				if (isset($bundle_products[$bundle_id]))
					foreach ($bundle_products[$bundle_id] as $bundle_product)
						$products[] = array('product_name' => $bundle_product['product_name'], 
							'product_quantity' => $bundle_product['quantity']);

				$bundle['products'] = $products;
				$bundle_data_with_products[] = $bundle;
			}
		}
		
		parent::export($bundle_data_with_products, 'bundles');
	}

}
