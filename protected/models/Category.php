<?php

class Category extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'categories');
		$this->timestamp = false;
	}

	public function export($data = array(), $file = '')
	{
		parent::export($this->executeQuery("SELECT `value` AS category_name FROM categories ORDER BY `value`"), 'categories');
	}

}
