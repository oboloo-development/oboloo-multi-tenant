<?php

class SavingStatus extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'saving_status');
		$this->timestamp = false;
	}

}
