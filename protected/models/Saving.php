<?php
require_once('protected/vendors/phpexcel/PHPExcelReader/excel_reader2.php');
require_once('protected/vendors/phpexcel/PHPExcelReader/SpreadsheetReader.php');
class Saving extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'user_id' => 'N',
			'user_name' => 'C',
			'title' => 'C',
			'status' => 'N',
			'currency_id' => 'N',
			'currency_rate' => 'C',
			'description' => 'C',
			'category_id'=>'N',
			'subcategory_id'=>'N',
			'location_id'=>'N',
			'department_id'=>'N',
			'vendor_id'=>'N',
			'country'=>'C',
			'saving_type'=>'N',
			'vendor_contact_name'=>'C',
			'vendor_contact_email'=>'C',
			'oracle_supplier_name'=>'C',
			'oracle_supplier_number'=>'C',
			'notes'=>'C',
			'total_project_saving'=>'N',
			'total_project_saving_perc'=>'N',
			'realised_saving'=>'N',
			'realised_saving_perc'=>'N',
			'new_or_incremental_savings' => 'C',
			'new_archived' => 'N',
			'start_date'=>'C',
			'due_date'=>'C',
			'contract_id'=>'N',
			'created_at' => 'D',
			'updated_at' => 'D'
		);
		parent::__construct('id', 'savings');
		$this->timestamp = false;
	}

	public function getSavings($location_id, $department_id,$category_id, $subcategory_id, $saving_status, $db_from_date, $vendor_ids)
	{
		$sql = "SELECT s.*,cr.currency,cr.currency_symbol,ss.value as saving_status, u.profile_img,s.user_id,sum(sm.base_line_spend) as total_base_spend,realised_saving/sum(sm.base_line_spend)*100 as realised_saving_perc ,total_project_saving/sum(sm.base_line_spend)*100 as total_project_saving_perc FROM savings s 
				left join currency_rates as cr on s.currency_id = cr.id
				left join users as u on s.user_id = u.user_id
				left join saving_status as ss on s.status = ss.id
				left join saving_milestone as sm on s.id = sm.saving_id
				";
		 
		$where  = "";

		if (!empty($location_id)) {
			$location_id = implode(',',$location_id);
			if(empty($where)){
				$where = " where " ;
			}
			$sql .= $where."  s.location_id IN ($location_id)";
		}
		if (!empty($department_id)) {
			$department_id = implode(',',$department_id);
			if(empty($where)){
				$where = " where " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." s.department_id IN ($department_id)";
		}
		if (!empty($category_id)) {
			$category_id = implode(',',$category_id);
			if(empty($where)){
				$where = " where " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." s.category_id IN ($category_id)";
		}
		if (!empty($subcategory_id)) {
			$subcategory_id = implode(',',$subcategory_id);
			if(empty($where)){
				$where = " where " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." s.subcategory_id IN ($subcategory_id)";
		}
		//if (!empty($department_id)) $sql .= " AND q.department_id = $department_id ";

		if (!empty($saving_status)){

			$status = "'" . implode("','", $saving_status) . "'";
			if(empty($where)){
				$where = " where  " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." s.status IN ($status)";
		}



		if (!empty($vendor_ids)){
			$vendor = "'" . implode("','", $vendor_ids) . "'";
			if(empty($where)){
				$where = " where  " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." s.vendor_id IN ($vendor)";
		}
		
		if (!empty($db_from_date)) {

			if(empty($where)){
				$where = " where  " ;
			}else{
				$where = " AND ";
			}

			$sql .= $where."  DATE_FORMAT(s.due_date,'%Y-%m-%d')= '$db_from_date'";
		}
		if (!empty($db_to_date)) {
			if(empty($where)){
				$where = " where " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." DATE_FORMAT(s.due_date,'%Y-%m-%d')<='$db_to_date'";
		}

		 
		return $this->executeQuery($sql . ' GROUP BY s.id ORDER BY s.id DESC ');
	}


	public function getSavingsAjax($saving_id = 0, $start = 0, $length = 10, $search_for = "", $order_by = array(), $location_id = 0, $department_id = 0, $category_id = 0, $subcategory_id = 0,$saving_status=0,$due_date=0,$due_date_to=0, $initiative_owner=0,$savings_area=0,$savings_type=0,$business_unit=0, $savings_location_country=0, $new_or_incremental_savings='')
    { 
        $this->currency_fields = array('contract_value'); 
        $user_id = Yii::app()->session['user_id'];
        $memberType = Yii::app()->session['member_type'];
		$userTypeID = Yii::app()->session['user_type'];
        if($userTypeID !="4" && ($memberType !=1 || $memberType =="0")){
         $up = "INNER JOIN user_privilege up On s.location_id = up.location_id and s.department_id= up.department_id and saving_view='yes' and up.user_id=".$user_id;
        }else {$up = "";}
       
        $sql = "SELECT s.*,u.full_name, 
		cr.currency,cr.currency_symbol,
		ss.value as saving_status, 
		u.profile_img,
		s.user_id,
		sum(sm.base_line_spend) as total_base_spend,
		s.realised_saving_perc*100 as realised_saving_perc,
		total_project_saving/sum(sm.base_line_spend)*100 as total_project_saving_perc
        FROM savings s 
        left join currency_rates as cr on s.currency_id = cr.id
        left join users as u on s.user_id = u.user_id
        left join saving_status as ss on s.status = ss.id
        left join saving_milestone as sm on s.id = sm.saving_id
        left join vendors as v on s.vendor_id =  v.vendor_id 
        ".$up;
        $currentAction = Yii::app()->controller->action->id;
        $where  = "";
        $sql .=" WHERE 1=1 ";
        
        if($currentAction =="listArchiveAjax"){
         $sql .=" and s.saving_archive='yes' "; 
        }else if(empty($saving_id)){
         $sql .=" and s.saving_archive='no' ";
        }
        
        if (! empty($search_for)) {
            $search_for = trim($search_for);
            $search_for_date = date("Y-m-d",strtotime(strtr($search_for,'/','-')));

            $sql .= " and ( s.title LIKE '%".$search_for."%'";
            $sql .= " or s.id LIKE '%".$search_for."%'";
            $sql .= " or s.user_name LIKE '%".$search_for."%'";
            $sql .= " or cr.currency LIKE '%".$search_for."%'";
            
            if(!empty(filter_var($search_for, FILTER_SANITIZE_NUMBER_INT))){
             // $sql .= " or sum(sm.base_line_spend) LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
             $sql .= " or total_project_saving LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
             //$sql .= " or total_project_saving/sum(sm.base_line_spend)*100 LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
             $sql .= " or realised_saving LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
             $sql .= " or realised_saving LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
             //$sql .= " or realised_saving/sum(sm.base_line_spend)*100 LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
            }

            $sql .= " or ss.value LIKE '%".$search_for."%'";
            $sql .= " or v.vendor_name LIKE '%".$search_for."%'";
            if($search_for_date !="1970-01-01"){
              $sql .= " or s.due_date='".$search_for_date."'";
              $sql .= " or s.created_at='".$search_for_date."'";   
             }
            $sql .= " ) ";
        }

        $order_by_clause = "";
        if (is_array($order_by) && count($order_by)) {
            foreach ($order_by as $column) {
                $column_index = $column['column'];
                $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                $column_name = "";
                switch ($column_index) {
                    case 1:
                        $column_name = "id";
                        break;
                    case 2:
                        $column_name = "s.title";
                        break;
                    case 3:
                        $column_name = "s.created_at";
                        break;
                    case 4:
                        $column_name = "s.due_date";
                        break;
                    case 5:
                        $column_name = "s.user_name";
                        break;
                    case 6:
                        $column_name = "cr.currency";
                        break;
                    case 7:
                        $column_name = "total_base_spend";
                        break;
                     case 8:
                        $column_name = "total_project_saving";
                        break;
                     case 9:
                        $column_name = "total_project_saving_perc";
                        break;
                     case 10:
                        $column_name = "realised_saving";
                        break;

                     case 11:
                        $column_name = "realised_saving_perc";
                        break;

                     case 12:
                        $column_name = "saving_status";
                        break;

                  
                }
                if (! empty($column_name)) {
                    if (empty($order_by_clause))
                        $order_by_clause = $column_name . ' ' . $column_direction;
                    else
                        $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                }
            }
            }
 		 
        if (!empty($location_id)) {
            //$location_id = implode(',',$location_id);
            $sql .= " AND s.location_id IN ($location_id)";
        }

        /*if ($department_id)
            $sql .= " AND c.department_id = $department_id ";*/
        if(!empty($department_id)) {
            //$department_id = implode(',',$department_id);
            $sql .= " AND s.department_id IN ($department_id)";
        }
        if ($category_id)
            $sql .= " AND s.category_id = $category_id ";
        if ($subcategory_id)
            $sql .= " AND s.subcategory_id = $subcategory_id ";
        if (!empty($saving_status)){

	      //$status = "'" . implode("','", $saving_status) . "'";
	      
	      $sql .= " and s.status IN ($saving_status)";
	    }
	    $due_date = date("Y-m-d",strtotime(strtr($due_date,'/','-')));
	    $due_date_to = date("Y-m-d",strtotime(strtr($due_date_to,'/','-')));

	    if (!empty($due_date) && $due_date!="1970-01-01"){

	      //$status = "'" . implode("','", $saving_status) . "'";
	      
	      $sql .= " and s.due_date>='".$due_date."'";
	    }

	    if (!empty($due_date_to) && $due_date_to!="1970-01-01"){
	      
	      $sql .= " and s.due_date<='".$due_date_to."'";
	    }

	    if ($initiative_owner)
            $sql .= " AND s.user_id = $initiative_owner ";

        
        if ($savings_type)
            $sql .= " AND s.saving_type = $savings_type ";
        
        if ($savings_location_country)
            $sql .= " AND s.country ='". $savings_location_country."' ";

        if ($new_or_incremental_savings)
        
            $sql .= " AND s.new_or_incremental_savings ='".$new_or_incremental_savings."' ";
        
        
        if (empty($order_by_clause))
           $order_by_clause = "s.id";
		if($start != 'none'){
			$sql .= " GROUP BY s.id ORDER BY $order_by_clause LIMIT $start, $length ";
		}else{
			$sql .= " GROUP BY s.id ORDER BY $order_by_clause ";
		}
        return $this->executeQuery($sql);
    }

	public function getMilestones($saving_id)
	{
		$sql = "SELECT m.*, s.realised_saving_perc,s.realised_saving,
		(m.cost_avoidance+m.cost_reduction)/m.base_line_spend*100 as total_project_saving_perc,
		(m.cost_avoidance+m.cost_reduction)/m.base_line_spend*100 as total_project_realised_perc,
		(m.realised_cost_avoidance+m.realised_cost_reduction)/m.base_line_spend*100 as total_realised_saving_perc
		FROM saving_milestone m 
		inner join savings s on m.saving_id = s.id
		where m.saving_id=".$saving_id." ORDER BY m.id DESC";
		return $this->executeQuery($sql);
	}


	public static function getStatus($status=''){
		
		if(!empty($status)){
			$sql = "select * from saving_status where id='".$status."'";
			$reader = Yii::app()->db->createCommand($sql)->queryRow();
			return $reader['value'];
		}else {
			$sql = "select * from saving_status ORDER BY value ASC";
			$reader = Yii::app()->db->createCommand($sql)->query()->readAll();
			return $reader;
		}


	}

	public function getMetrix($from_date, $to_date){
		
		$currencySymbol = Yii::app()->session['user_currency_symbol'];
		$currency       = Yii::app()->session['user_currency'];
		$savingStatus = FunctionManager::savingStatusIgnore();

		 // sum(currencyConversionOldCurrency(realised_saving,s.id,s.currency_rate,currency_id,'".$currency."')) as realised_total 
		$where = ''; 
		if($from_date != '1970-01-01' || $to_date != '1970-01-01'){
		 $where = " and ( DATE(s.due_date) >='".$from_date."') and DATE(s.due_date) <='".$to_date."' ";
		}else{
			$where = " or ( DATE(s.due_date) >='".$from_date."') and DATE(s.due_date) <='".$to_date."' ";
		}

		$sql="SELECT 
           SUM(currencyConversion(s.realised_saving, s.currency_rate, currency_id, '".$currency."')) as realised_total
        FROM savings AS s 
        WHERE s.status NOT IN (".$savingStatus.") AND s.saving_archive = 'no'
          ".$where; 
		$realised = Yii::app()->db->createCommand($sql)->queryRow();

	 	 $sql = "select sum(
	 	 currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'".$currency."')
	 	 )+sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'".$currency."')) as total_project
		 from  saving_milestone as sm inner join savings s on sm.saving_id=s.id  WHERE  sm.STATUS is null and s.status not in(".$savingStatus.") and s.saving_archive='no' ". $where; 
		$project = Yii::app()->db->createCommand($sql)->queryRow();

		 /*$sql = "select (sum(s.realised_saving))/(select sum(insm.base_line_spend) from saving_milestone as insm,savings s where insm.status='Completed' and s.status not in(".$savingStatus.")  ) as released_against_baseline
		 from savings as s inner join saving_milestone sm on s.id=sm.saving_id where s.status not in(".$savingStatus.")  and (sm.due_date>='".$from_date."' or sm.due_date>='0000-00-00 00:00:00'  or sm.due_date>='0000-00-00')  and sm.due_date <='".$to_date."' "; 
     	$realisedSaving = Yii::app()->db->createCommand($sql)->queryRow();*/

     	$sql = "select 
     	(sum(currencyConversion(sm.realised_cost_avoidance,s.currency_rate,currency_id,'".$currency."'))+
     	sum(currencyConversion(sm.realised_cost_reduction,s.currency_rate,currency_id,'".$currency."'))) / 
     	sum(currencyConversion(sm.base_line_spend,s.currency_rate,currency_id,'".$currency."'))
     	as released_against_baseline
		 from savings as s inner join saving_milestone sm on s.id=sm.saving_id and s.saving_archive = 'no' where s.status not in(".$savingStatus.") and sm.status='Completed' and s.saving_archive='no' ".$where;
     	$realisedSaving = Yii::app()->db->createCommand($sql)->queryRow();
 

     	$sql = "select 
     	(sum(currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'".$currency."')) +
     	sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'".$currency."'))) /
     	sum(currencyConversion(sm.base_line_spend,s.currency_rate,currency_id,'".$currency."'))
     	as total_project_baseline
		from savings as s inner join saving_milestone sm on s.id=sm.saving_id where sm.STATUS is null and s.status not in(".$savingStatus.") and s.saving_archive='no' ". $where;
     	$projectSaving = Yii::app()->db->createCommand($sql)->queryRow();

     	// Start: Currency Conversion
     	$metrix1 = $realised['realised_total'];
     	$metrix3 = $project['total_project'];


     	// End: Currency Conversion
		return array('metrix_1'=>$metrix1,'metrix_3'=>$metrix3,
			'metrix_2'=>number_format($realisedSaving['released_against_baseline']*100,1),'metrix_4'=>number_format($projectSaving['total_project_baseline']*100,1));

	
	}

	public function getChartList($from_date, $to_date){

		// Start: Project, Realised and Department
		if(!empty($_POST['report_of_location'])){
    	 	$locationFilter = ' s.location_id='.$_POST['report_of_location'].' and ';
    	}else{
    	 	$locationFilter = '';
    	}

    	$savingStatus = FunctionManager::savingStatusIgnore();
		$currency = Yii::app()->session['user_currency'];

      $sql=" select d.department_name,
       	sum(currencyConversion(realised_saving,s.currency_rate,currency_id,'".$currency."')) as realised_total,

		sum(currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'".$currency."'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'".$currency."')) as project_total

		from savings as s 
		inner join departments d on s.department_id = d.department_id 
		inner join locations l on s.location_id = l.location_id 
		inner join saving_milestone sm on sm.saving_id=s.id  
		where s.status not in(".$savingStatus.") and ".$locationFilter." s.due_date>='".$from_date."' and s.due_date <='".$to_date."'  and s.saving_archive='no' group by s.department_id ORDER BY d.department_name asc";
		//ORDER BY realised_total,project_total asc
		 
		$proRealisedDept = Yii::app()->db->createCommand($sql)->queryAll();
		$projectRealisedArr = array();

		foreach($proRealisedDept as $value){
			
			$projectRealisedArr['project'][] = $value['project_total']>0?number_format($value['project_total'], 0, '.', ''):0;
			$projectRealisedArr['realised'][] = $value['realised_total']>0?number_format($value['realised_total'], 0, '.', ''):0;
			$projectRealisedArr['department'][] = $value['department_name'];
		}

		// End: Project, Realised and Department




	 $sql ="select c.value,
	 		sum(currencyConversion(realised_saving,s.currency_rate,currency_id,'".$currency."')) as realised_total,
	 		sum(currencyConversion(sm.cost_avoidance,s.currency_rate,currency_id,'".$currency."'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,currency_id,'".$currency."')) as total_project
			 from savings as s 

			 inner join categories c on s.category_id=c.id 
			 inner join saving_milestone sm on sm.saving_id=s.id 
			 where s.status not in(".$savingStatus.") and s.due_date>='".$from_date."' and s.due_date <='".$to_date."'
			 and s.saving_archive='no' group by s.category_id order by c.value ASC";
		$proRealisedCat = Yii::app()->db->createCommand($sql)->queryAll();
		$projectRealisedCatArr = array();
		foreach ($proRealisedCat as $value) {
			$projectRealisedCatArr['project'][]= $value['total_project']>0?number_format($value['total_project'], 0, '.', ''):0;
			$projectRealisedCatArr['realised'][]= $value['realised_total']>0?number_format($value['realised_total'], 0, '.', ''):0;
			$projectRealisedCatArr['category'][]=$value['value'];
		}
		 /*if(isset($_POST['report_from_date']) && isset($_POST['report_to_date'])){*/
		 	$orderByMonth = 'num_month_order';
		 /*}else{
			$orderByMonth = 'num_month';
		}*/
		$sql ="select 
	  sum(currencyConversion(sm.cost_avoidance,s.currency_rate,s.currency_id,'".$currency."'))+sum(currencyConversion(sm.cost_reduction,s.currency_rate,s.currency_id,'".$currency."')) as total_project_saving,
	  	 sum(currencyConversion(sm.total_realised_saving,s.currency_rate,currency_id,'".$currency."')) as total_realised_saving,
	  	date_format(sm.due_date,'%b-%y') as month, date_format(sm.due_date,'%m') as num_month,date_format(sm.due_date,'%Y%m') as num_month_order
	  	from saving_milestone sm inner join savings s on sm.saving_id=s.id  where s.status not in(".$savingStatus.") and sm.due_date !='0000-00-00 00:00:00' and sm.due_date>='".$from_date."' and sm.due_date <='".$to_date."' and s.saving_archive='no' group by month ORDER BY ".$orderByMonth." ASC ";
 
	  $proRealisedMonth = Yii::app()->db->createCommand($sql)->queryAll();

	  $projectRealisedMonthArr = array();

	    $i = 0;
		$month = strtotime($from_date);

		//Start: get number of months between start and end date
        $durationStart = new DateTime($from_date);
		$durationEnd = new DateTime($to_date);
		$diff = $durationStart->diff($durationEnd);

		$yearsInMonths = $diff->format('%r%y') * 12;
		$months = $diff->format('%r%m');
		$totalMonths = $yearsInMonths + $months;
		$year = date("y",strtotime($from_date));
		//End: get number of months between start and end date
		
		while($i <= $totalMonths)
		{
			$month_name      = date("M-y",strtotime($from_date." + $i months"));
		    $projectRealisedMonthArr['project'][$month_name] = 0;
		    $projectRealisedMonthArr['realised'][$month_name] = 0;
		    $projectRealisedMonthArr['month'][$month_name] = $month_name;
		    $i++;
		}
	  foreach ($proRealisedMonth as $value) {
	  	$projectRealisedMonthArr['project'][$value['month']] = isset($value['total_project_saving']) ? number_format($value['total_project_saving'], 0, '.', ''):0;
	  	$projectRealisedMonthArr['realised'][$value['month']] = isset($value['total_realised_saving']) ? number_format($value['total_realised_saving'], 0, '.', ''):0;
	  	$projectRealisedMonthArr['month'][$value['month']] = $value['month'];
	  }
		
		return array('chart_1'=>$projectRealisedArr, 'chart_2'=>$projectRealisedCatArr,
					'chart_3'=>$projectRealisedMonthArr);
	} 

	public function saveRecordCurrency($saving_id){

		    $currencyObj  = new CurrencyRate();
            $currencyList = $currencyObj->getAllCurrencyRate();
            foreach ($currencyList as $value) {
            	$currencyID = $value['id'];
            	$currency = $value['currency'];
            	$currencySymbol = $value['currency_symbol'];
            	$currencyRate = $value['rate'];

			    $record = new CurrencyRecord;
				$record->rs = array();
				$record->rs['record_id'] = $saving_id;
				$record->rs['record_type'] = 'Saving';
				$record->rs['currency_id'] = $currencyID;
				$record->rs['currency'] = $currency;
				$record->rs['currency_symbol'] = $currencySymbol;
				$record->rs['currency_rate'] = $currencyRate;
				$record->write();
			}
	}

	public function getVendorSavings($vendor_id)
	{
		$sql = "SELECT s.*,cr.currency,cr.currency_symbol,ss.value as saving_status, 
		u.full_name as user_name, u.profile_img,s.user_id, 
		sum(sm.base_line_spend) as total_base_spend,
		s.realised_saving_perc*100 as realised_saving_perc,
		total_project_saving/sum(sm.base_line_spend)*100 as total_project_saving_perc
		FROM savings s 
				  LEFT JOIN users u ON s.user_id = u.user_id
				  left join currency_rates as cr on s.currency_id = cr.id
				  INNER JOIN vendors v ON s.vendor_id = v.vendor_id
				  left join saving_status as ss on s.status = ss.id
				  left join saving_milestone as sm on s.id = sm.saving_id
				  WHERE v.vendor_id = $vendor_id";

		return $this->executeQuery($sql . ' GROUP BY s.id ORDER BY s.id DESC ');

	}

	public function getContractSavings($contract_id)
	{
		$sql = "SELECT s.*,cr.currency,cr.currency_symbol,ss.value as saving_status, u.full_name as user_name, u.profile_img,s.user_id,sum(sm.base_line_spend) as total_base_spend FROM savings s 
				  LEFT JOIN users u ON s.user_id = u.user_id
				   left join currency_rates as cr on s.currency_id = cr.id
				  INNER JOIN contracts c ON s.contract_id = c.contract_id
				  left join saving_status as ss on s.status = ss.id
				  left join saving_milestone as sm on s.id = sm.saving_id
				  WHERE c.contract_id = $contract_id";

		return $this->executeQuery($sql . ' GROUP BY s.id ORDER BY s.id DESC ');

	}

	public function export($data = array(), $file = '')
	{	 

		$sql = "SELECT 
			s.id as Saving_Id ,
			s.title as Saving_Title,
			c.value as Category,
			sub_cat.value as Sub_Category,
			l.location_name as Location,
			d.department_name as Department,
			v.vendor_name as Supplier_Name,
			cont.contract_title as Associated_Contract,
			CASE 
             WHEN s.created_at IS NULL OR DATE_FORMAT(s.created_at, '%d/%m/%Y') = '00/00/0000' THEN ''
            ELSE DATE_FORMAT(s.created_at, '%d/%m/%Y') 
            END AS Created_On,
			CASE 
             WHEN s.due_date IS NULL OR DATE_FORMAT(s.due_date, '%d/%m/%Y') = '00/00/0000' THEN ''
            ELSE DATE_FORMAT(s.due_date, '%d/%m/%Y') 
            END AS Savings_Due_Date,
			s.user_name as Created_By,
			cr.currency as Saving_Currency,
			
			COALESCE(CONCAT(cr.currency_symbol, sum(sm.base_line_spend)),0) as Baseline_Spend,
			COALESCE(CONCAT(cr.currency_symbol, sum(sm.cost_avoidance))) as Projected_Cost_Avoidance,
			COALESCE(CONCAT(cr.currency_symbol, sum(sm.cost_reduction))) as Projected_Cost_Reduction,
			CONCAT(cr.currency_symbol, s.total_project_saving) as Projected_Savings,
			CONCAT(s.total_project_saving / sum(sm.base_line_spend)*100, ' %') as 'Projected Savings %',
			COALESCE(CONCAT(cr.currency_symbol, (SELECT sum(base_line_spend) as total_base_line_spend FROM saving_milestone m where m.status='Completed' and m.saving_id = s.id)),0) as 'Total Baseline Spend Against Realised Savings',
			COALESCE(CONCAT(cr.currency_symbol, sum(sm.realised_cost_avoidance)), 0) as Realised_Cost_Avoidance,
			COALESCE(CONCAT(cr.currency_symbol, sum(sm.realised_cost_reduction)), 0) as Realised_Cost_Reduction,
			CONCAT(cr.currency_symbol, s.realised_saving) as Realised_Savings,
			CONCAT(s.realised_saving_perc*100, ' %') as 'Realised Saving %',
			ss.value as Saving_Status

		    FROM savings as s
		    left join saving_status as ss on s.status = ss.id
		    left join currency_rates as cr on s.currency_id = cr.id
			left join saving_milestone as sm on s.id = sm.saving_id
			left join categories as c on s.category_id = c.id
			left join sub_categories as sub_cat on s.subcategory_id = s.id
			left join locations as l on s.location_id = l.location_id
			left join departments as d on s.department_id = d.department_id
			left join vendors as v on s.vendor_id = v.vendor_id
			left join contracts as cont on s.contract_id = cont.contract_id
			where s.saving_archive='no'
			group by s.id
		    ORDER BY s.id desc";
		parent::exportCommon($this->executeQuery($sql), 'savings');
	}

	public function checkApprovel($saving_id = 0)
    {
      $sql =" select s.user_id,s.approver_id,s.approver_name,s.approver_status from savings s where id=".$saving_id;
        $savingCheck = Yii::app()->db->createCommand($sql)->queryRow();
        return $savingCheck;
    }

	public function getTotalBaselineMilestone($saving_id)
	{
		$sql = "SELECT m.base_line_spend, sum(m.base_line_spend) as total_base_line_spend
		FROM saving_milestone m 
		inner join savings s on m.saving_id = s.id
		where m.saving_id=".$saving_id;
		return $this->executeQuery($sql);
	}

  
}
