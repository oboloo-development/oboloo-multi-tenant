<?php

class QuoteScorerForm extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'quote_scorer_questions_form');
		$this->timestamp = false;
	}

	public function getSelectedQuestionForm($quoteID, $userID,$formID){
	$scorerQuesID = 0;
	$sql =" select * from quote_scorer_questions_form where quote_id=".$quoteID." and user_id=".$userID." and question_id=".$formID;
	$result =  Yii::app()->db->createCommand($sql)->queryRow();
	if(!empty($result['scoring_row_id'])) $scorerQuesID = $result['scoring_row_id'];
	return  $scorerQuesID;
	
	}

}
