<?php
require_once('protected/vendors/phpexcel/PHPExcelReader/excel_reader2.php');
require_once('protected/vendors/phpexcel/PHPExcelReader/SpreadsheetReader.php');
class Product extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'product_id' => 'N',
			'product_name' => 'C',
			'product_type' => 'C',
			'category_id' => 'L',
			'subcategory_id' => 'L',
			'vendor_id' => 'L',
			'contract_id' => 'L',
			'upc' => 'C',
			'sku_code' => 'C',
			'brand' => 'C',
			'size' => 'C',
			'url' => 'C',
			'length' => 'C',
			'width' => 'C',
			'height' => 'C',
			'weight' => 'C',
			'price' => 'C',
			'currency_id' => 'N',
		);

		$this->currency_fields = array ( 'price' );
		$this->alternate_key_name = 'product_name';
		parent::__construct('product_id', 'products');
	}

	public function getProducts($product_id = 0, $start = 0, $length = 10, $search_for = "", $order_by = array())
	{
		$sql = "SELECT p.*, c.value AS category, s.value as subcategory,v.vendor_name
		          FROM products p
		          LEFT JOIN vendors v ON p.vendor_id = v.vendor_id
				  LEFT JOIN categories c ON p.category_id = c.id
				  LEFT JOIN sub_categories s ON p.subcategory_id = s.id WHERE p.status = 1";


		if ($product_id)
			return $this->executeQuery($sql . " AND p.product_id = $product_id ", true);
		else
		{
			if (!empty($search_for))
			{
				$sql = "SELECT p.*, c.value AS category, s.value as subcategory,v.vendor_name
				          FROM products p
						  LEFT JOIN vendors v ON p.vendor_id = v.vendor_id AND v.vendor_name LIKE '$search_for%'
						  LEFT JOIN categories c ON p.category_id = c.id AND c.value LIKE '$search_for%'
						  LEFT JOIN sub_categories s ON p.subcategory_id = s.id AND s.value LIKE '$search_for%'
						 WHERE status = 1 AND ( p.product_name LIKE '%$search_for%' OR p.brand LIKE '$search_for%' )";
			}

			$order_by_clause = "";
			if (is_array($order_by) && count($order_by))
			{
				foreach ($order_by as $column)
				{
					$column_index = $column['column'];
					$column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';

					$column_name = "";
					switch ($column_index)
					{
						case 1  : $column_name = "product_name"; break;
						case 2  : $column_name = "vendor_name"; break;
						case 3  : $column_name = "category"; break;
						case 4  : $column_name = "subcategory"; break;
						case 5  : $column_name = "brand"; break;
						case 6  : $column_name = "upc"; break;
						case 7  : $column_name = "sku_code"; break;
						default : break;
					}

					if (!empty($column_name))
					{
						if (empty($order_by_clause)) $order_by_clause = $column_name . ' ' . $column_direction;
						else $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
					}
				}
			}

			if (empty($order_by_clause)) $order_by_clause = "product_name";
 			return $this->executeQuery($sql . " ORDER BY $order_by_clause LIMIT $start, $length ");
 		}
	}

	public function getAllProducts($category_id = 0, $subcategory_id = 0, $product_type = 0, $vendor_id = 0 )
	{
		$sql = "SELECT p.*, c.value AS category, s.value as subcategory,v.vendor_name
		          FROM products p
		          LEFT JOIN vendors v ON p.vendor_id = v.vendor_id
				  LEFT JOIN categories c ON p.category_id = c.id
				  LEFT JOIN sub_categories s ON p.subcategory_id = s.id WHERE p.status = 1";



		if ($category_id)
		    $sql .= " AND p.category_id = $category_id";
		if ($subcategory_id)
		$sql .= " AND p.subcategory_id = $subcategory_id";
		if ($product_type)
		$sql .= " AND p.product_type = '$product_type'";
		if ($vendor_id)
		$sql .= " AND p.vendor_id = $vendor_id";

		$sql .= " ORDER BY product_id DESC LIMIT 2000";

		//CVarDumper::dump($this->executeQuery($sql),10,1);die;

		return $this->executeQuery($sql);
	}

	public function getSearchedRows($search_for)
	{
		$sql = "SELECT COUNT(*) AS total_rows
		          FROM products p
		          LEFT JOIN vendors v ON p.vendor_id = v.vendor_id AND v.vendor_name LIKE '$search_for%'
				  LEFT JOIN categories c ON p.category_id = c.id AND c.value LIKE '$search_for%'
				  LEFT JOIN sub_categories s ON p.subcategory_id = s.id AND s.value LIKE '$search_for%'
				 WHERE ( p.product_name LIKE '%$search_for%' OR p.brand LIKE '$search_for%' )";
		$count_data = $this->executeQuery($sql, true);
		return $count_data && is_array($count_data) && isset($count_data['total_rows']) ? $count_data['total_rows'] : 0;
	}

	public function export($data = array(), $file = '')
	{
		$sql = "SELECT product_name,product_type, c.value AS category_name, s.value AS subcategory_name,v.vendor_name as supplier,ct.contract_title as contract, upc, sku_code, brand,url, size, price, length, width, height, weight ,cr.currency
				  FROM products p LEFT JOIN categories c ON p.category_id = c.id
				  LEFT JOIN sub_categories s ON p.subcategory_id = s.id
				  LEFT JOIN vendors v ON p.vendor_id = v.vendor_id
				  LEFT JOIN contracts ct ON p.contract_id = ct.contract_id
				  LEFT JOIN currency_rates cr ON p.currency_id = cr.id
				 ORDER BY product_name";
		parent::export($this->executeQuery($sql), 'products');
	}

	public function importData($filename){

		$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

		if(in_array($filename["type"],$allowedFileType)){

			$targetPath = 'uploads/'.$filename['name'];
			move_uploaded_file($filename['tmp_name'], $targetPath);

			$reader = new SpreadsheetReader($targetPath);

			$sheetCount = count($reader->sheets());
			for($i=0;$i<$sheetCount;$i++)
			{
				$reader->ChangeSheet($i);

				foreach ($reader as $row)
				{
					$product_name = "";
					if(isset($row[0])) {
						$product_name = $row[0];
					}
					$category_id = "";
					if(isset($row[1])) {
						$category_value  = $row[1];
						$result = $this->executeQuery("SELECT * FROM categories WHERE value = '$category_value'", true);
						if(!empty($result)>0){
							$category_id = $result['id'];
						} else {

							$this->executeQuery("INSERT INTO categories (`value`)
                                                           values('".$category_value."')");
							$max = $this->executeQuery("SELECT MAX(id) as id FROM categories", true);
							$category_id = $max['id'];
						}

					}
					$subcategory_id = "";
					if(isset($row[2])) {
						$subcategory_value  = $row[2];
						$result = $this->executeQuery("SELECT * FROM sub_categories WHERE value = '$subcategory_value'", true);
						if(!empty($result)>0){
							$subcategory_id = $result['id'];
						} else {

							$this->executeQuery("INSERT INTO sub_categories (`value`)
                                                           values('".$subcategory_value."')");
							$max = $this->executeQuery("SELECT MAX(id) as id FROM sub_categories", true);
							$subcategory_id = $max['id'];
						}
					}
					$upc = "";
					if(isset($row[3])) {
						$upc = $row[3];
					}
					$sku_code = "";
					if(isset($row[4])) {
						$sku_code = $row[4];
					}
					$brand = "";
					if(isset($row[5])) {
						$brand = $row[5];
					}
					$size = "";
					if(isset($row[6])) {
						$size = $row[6];
					}
					$url = "";
					if(isset($row[7])) {
						$url = $row[7];
					}
					$length = "";
					if(isset($row[8])) {
						$length = $row[8];
					}
					$width = "";
					if(isset($row[9])) {
						$width = $row[9];
					}
					$height = "";
					if(isset($row[10])) {
						$height = $row[10];
					}
					$weight = "";
					if(isset($row[11])) {
						$weight = $row[11];
					}
					$price = "";
					if(isset($row[12])) {
						$price = $row[12];
					}


					if (!empty($product_name)) {
						$result = $this->executeQuery("SELECT * FROM products WHERE product_name = '$product_name'", true);
						if(!$result){
							$this->executeQuery("INSERT INTO products (product_name,category_id,subcategory_id,upc,sku_code,brand,size,url,length,width,height,weight,price)
                                                           values('".$product_name."','".$category_id."','".$subcategory_id."','".$upc."','".$sku_code."','".$brand."','".$size."','".$url."','".$length."','".$width."','".$height."','".$weight."','".$price."')");
						}
					}
				}

			}

			echo $type = "success";

		}
		else
		{
			$type = "error";
			echo $message = "Invalid File Type. Upload Excel File.";
		}

	}

	public function getProductCount($industry_id, $subindustry_id, $preferred_flag)
    {
        $sql = "SELECT COUNT(*) AS total_rows FROM vendors WHERE 1 = 1";
        if ($industry_id)
            $sql .= " AND industry_id = $industry_id ";
        if ($subindustry_id)
            $sql .= " AND subindustry_id = $subindustry_id ";
        if ($preferred_flag != - 1)
            $sql .= " AND preferred_flag = $preferred_flag ";
        
        $count_data = $this->executeQuery($sql, true);
        return $count_data && is_array($count_data) && isset($count_data['total_rows']) ? $count_data['total_rows'] : 0;
    }

}
