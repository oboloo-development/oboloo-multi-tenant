<?php

class QuoteVendorAnswer extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'quote_vendor_answers');
		$this->timestamp = false;
	}

	public function getQuoteVendorAnswers($quote_id,$question_id,$vendor_id)
	{
		$quote_answers =  $this->executeQuery("SELECT * FROM quote_vendor_answers WHERE quote_id = $quote_id AND question_id = $question_id and vendor_id=$vendor_id  ORDER BY id",1);
		//CVarDumper::dump($quote_answers,10,1);die;

		return $quote_answers;
	}
}
