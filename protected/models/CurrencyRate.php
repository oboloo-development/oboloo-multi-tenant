<?php

class CurrencyRate extends Common
{
	public function __construct()
	{

		$this->fields = array (
			'id' => 'N',
			'currency' => 'C',
			'currency_symbol' => 'C',
			'rate' => 'C',
			'country' => 'C',
			'currency_name' => 'C',
			'user_id' => 'N',
			'user_name' => 'C',
			'status'=>'N',
			'created_at' => 'C',
			'updated_at' => 'C',
		);

		parent::__construct('id', 'currency_rates');
		$this->timestamp = false;
	}

	public function getAllCurrencyRate()
	{   
		if(strtolower(Yii::app()->controller->action->id)=="indexsetup"){
			return $this->executeQuery("SELECT * FROM currency_rates   ORDER BY id ASC");
		}else {
			return $this->executeQuery("SELECT * FROM currency_rates where rate>0 and status='0' ORDER BY id ASC");
		}
	}

	public function getCurrencyRates($currency_id)
	{
		return $this->executeQuery("SELECT * FROM currency_rates WHERE id=$currency_id",1);
	}
}
