<?php

class VendorScoringName extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'vendor_scoring_name');
		$this->timestamp = false;
	}

	public function getVendorScoreCardTableSetting($id = 0, $start = 0, $length = 10, $search_for = "", $order_by = array(), $user_id="",$archive_status="")
    {
        $sql = " select vsn.*, sum(vs.score) as total_score from vendor_scoring_name as vsn
            inner join vendor_scoring as vs on vs.scorecard_id = vsn.id  ";
        
        if (!empty($id))
            return $this->executeQuery($sql . " WHERE id = $id ", true);
        else {
            if (! empty($search_for)) {
              $sql = "SELECT vsn.*, SUM(vs.score) as total_score 
                        FROM vendor_scoring_name as vsn
                        INNER JOIN vendor_scoring as vs ON vs.scorecard_id = vsn.id
                        WHERE vsn.status = ".$archive_status."
                        AND (vsn.username LIKE '%$search_for%' 
                        OR vsn.name LIKE '%$search_for%' 
                        OR vsn.created_datetime LIKE '%$search_for%' 
                        OR vsn.updated_datetime LIKE '%$search_for%' )
                     ";
            }
        
            $order_by_clause = "";
            if (is_array($order_by) && count($order_by)) {
                foreach ($order_by as $column) {
                    $column_index = $column['column'];
                    $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                    
                    $column_name = "";
                    switch ($column_index) {
                        case 0:
                            $column_name = "vsn.id";
                            break;
                        case 1:
                            $column_name = "vsn.name";
                            break;
						case 2:
                            $column_name = "vs.score";
                            break;
                        case 3:
                            $column_name = "vsn.username";
                            break;
                        case 4:
                            $column_name = "vsn.created_datetime";
                            break;
                        case 5:
                            $column_name = "vsn.updated_datetime";
                            break;
                        default:
                            $column_name = "vsn.name";
                            break;
                    }
                    
                    if (! empty($column_name)) {
                        if (empty($order_by_clause))
                            $order_by_clause = $column_name . ' ' . $column_direction;
                        else
                            $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                    }
                }
            }
             
            if (empty($order_by_clause))
                $order_by_clause = " value ";
            if (strpos($sql, "WHERE") === false)
               $sql .= " WHERE 1 = 1 and vsn.status=".$archive_status;
            if(!empty($user_id))
                $sql .= " AND vsn.status=".$archive_status." AND vsn.user_id = $user_id ";
            if($start != 'none'){
              $sql .= " group by vsn.id ORDER BY $order_by_clause LIMIT $start, $length ";
            }else{
              $sql .= " group by vsn.id  ORDER BY $order_by_clause ";
            }
    
            return $this->executeQuery($sql);
        }
    }
}
