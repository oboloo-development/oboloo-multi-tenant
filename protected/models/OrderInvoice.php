<?php

class OrderInvoice extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'order_id' => 'C',
			'vendor_id' => 'C',
			'created_by_type' => 'C',
			'invoice_number' => 'C',
			'file_name' => 'C',
			'status' => 'C',
			'created_at' => 'D'
		);
		parent::__construct('id', 'order_invoices');
		$this->timestamp = false;
	}

}
