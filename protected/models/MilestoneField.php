<?php

class MilestoneField extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' 						=> 'N',
			'saving_id'					=> 'N',
			'saving_id'					=> 'N',
			'field_name'				=> 'D',
			'field_value'				=> 'C',
			'total_realised_savings'	=> 'C',
			'status'	    			=> 'C',
			'completed_by_id' 			=> 'N',
			'completed_by_name' 		=> 'C',
			'completed_date' 			=> 'D',
			'comments_by_id' 			=> 'N',
			'comments' 					=> 'C',
			'comments_by_name' 		    => 'C',
			'comments_date' 			=> 'D',
		);
		parent::__construct('id', 'milestone_field');
		$this->timestamp = false;
	}
}
