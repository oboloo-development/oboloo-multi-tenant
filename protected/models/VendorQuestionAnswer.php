<?php

class VendorQuestionAnswer extends Common
	{
	
		public function __construct()
		{
			$this->fields = array (
				'id' => 'N',
				'questions' => 'C',
				'answer'  => 'C',
				'type'  => 'C',
				'vendor_id'  => 'N',
				'quote_id'  => 'N',
				'form_id'  => 'N',
				'created_datetime' => 'D',
				'updated_datetime' => 'D',
			);
			parent::__construct('id', 'vendor_question_answer');
		}
}
