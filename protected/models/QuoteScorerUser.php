<?php

class QuoteScorerUser extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'quote_scorer_user');
		$this->timestamp = false;
	}
	
	public function getQuoteScorerQuestions($quote_id){
		return Yii::app()->db->createCommand(" SELECT * FROM quote_scorer_user qsu WHERE qsu.quote_id=".$quote_id." group by qsu.id ")->queryAll();
	}
}
