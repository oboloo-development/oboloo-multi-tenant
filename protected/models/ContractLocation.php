<?php

class ContractLocation extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'contract_id' => 'N',
			'location_id' => 'N',
		);
		parent::__construct('id', 'contract_location');
		$this->timestamp = false;
	}
	
}
