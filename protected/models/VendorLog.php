<?php

class VendorLog extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'vendor_logs');
		$this->timestamp = false;
	}

	public function getVendorLogs($vendor_id = 0)
	{
		$sql = "SELECT v.*, u.full_name AS user FROM vendor_logs v, users u WHERE  v.vendor_id = $vendor_id
                 AND v.user_id = u.user_id
				 ORDER BY v.id DESC";
		return $this->executeQuery($sql);
	}
}
