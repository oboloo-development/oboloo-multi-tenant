<?php

class Budget extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'location_id' => 'L',
			'department_id' => 'L',
			'year' => 'N',
			'budget' => 'N'
		);

		$this->currency_fields = array ( 'budget' );
		parent::__construct('id', 'budgets');
		$this->timestamp = false;
	}

	public function getBudgets($budget_id = 0, $location_id = 0)
	{
		$sql = "SELECT b.*, l.location_name, d.department_name
		          FROM budgets b
				  LEFT JOIN locations l ON b.location_id = l.location_id
				  LEFT JOIN departments d ON b.department_id = d.department_id WHERE b.year = ".Yii::app()->session['user_selected_year']."";

		$order_by_clause = ' ORDER BY b.year DESC, l.location_name, d.department_name ';
		if ($budget_id)
			return $this->executeQuery($sql . " AND WHERE b.id = $budget_id ", true);
		else if ($location_id)
				return $this->executeQuery($sql . " AND WHERE b.location_id = $location_id " . $order_by_clause);
		else return $this->executeQuery($sql . $order_by_clause);
	}
	
	public function getDepartmentBudgets($departments = array())
	{
		$current_year = date('Y');
		if (!is_array($departments) || count($departments) <= 0) $departments = array(0);
		
		$sql = "SELECT department_id, SUM(budget) AS budget
		          FROM budgets WHERE year = $current_year
				   AND department_id IN ( " . implode(",", $departments) . " ) ";
		
		$budgets = array();
		foreach ($this->executeQuery($sql) as $budget)
			$budgets[$budget['department_id']] = $budget['budget'];
		return $budgets;
	}
	
	
	public function getDepartmentSpendAndBudgets($year = 0, $location_id = 0, $department_id = 0)
	{
		// may be time period is not specified and hence default
		$date_from = $date_to = "";
		if (empty($year)) $year = date("Y");
		$date_from = "$year-01-01"; 
		$date_to = "$year-12-31"; 

		if (!empty($location_id)) {
			$location_id = implode(',',$location_id);
		}else{
			$location_id = 0;
		}
		if (empty($department_id)) $department_id = 0;

		//CVarDumper::dump($location_id,10,1);
		
		// get budgets for the year selected
		$budgets = array();
		$budget_sql = "SELECT * FROM budgets WHERE year = $year AND location_id IN ($location_id) AND department_id = $department_id";
		foreach ($this->executeQuery($budget_sql) as $budget)
			$budgets[$department_id] = $budget['budget'];

		// now department spendings - expenses first
		$totals = array();
		$sql = "SELECT SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, expenses x
 				 WHERE x.expense_id = d.expense_id 
   				   AND d.expense_date >= '$date_from'
   				   AND d.expense_date <= '$date_to'
   				   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				   AND x.location_id IN ($location_id)
 				   AND x.department_id = $department_id";

 		foreach ($this->executeQuery($sql) as $row)
 			$totals[$department_id] = $row['total'];
		
		// and then orders
		$sql = "SELECT o.department_id, SUM(o.total_price + o.other_charges - o.discount) AS `total`
  				  FROM orders o
 				 WHERE o.order_date >= '$date_from'
   				   AND o.order_date <= '$date_to'
   				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND o.location_id IN ($location_id)
 				   AND o.department_id = $department_id";


 		foreach ($this->executeQuery($sql) as $row)
			if (isset($totals[$department_id]))
 				$totals[$department_id] += $row['total'];
			else $totals[$department_id] = $row['total'];



		// department names hopefully are not too many
		$departments = array();
		foreach ($this->executeQuery("SELECT * FROM departments WHERE department_id = $department_id") as $department)
			$departments[$department['department_id']] = $department['department_name'];
	
		// now combine the data
		$department_data = array();
		foreach (array_merge(array_keys($budgets), array_keys($totals)) as $department_id)
		{
			$department_data[$department_id] = 
				array('budget' => 0, 'spent' => 0, 'department_id' => $department_id, 'department_name' => 'Unknown');
			if (isset($budgets[$department_id]))
				$department_data[$department_id]['budget'] = round($budgets[$department_id], 2);
			if (isset($totals[$department_id]))
				$department_data[$department_id]['spent'] = round($totals[$department_id], 2);
			if (isset($departments[$department_id]))
				$department_data[$department_id]['department_name'] = $departments[$department_id];
		}			
		
		// and return without any specific index
		return array_values($department_data);
	}

	public function getDepartmentStats($year = 0, $location_id = 0, $department_id = 0)
	{
		// may be time period is not specified and hence default
		$date_from = $date_to = "";
		if (empty($year)) $year = date("Y");
		$date_from = "$year-01-01";
		$date_to = "$year-12-31";

		if (empty($location_id)) $location_id = 0;
		if (empty($department_id)) $department_id = 0;

		// get budgets for the year selected
		$budgets = array();
		$budget_sql = "SELECT * FROM budgets WHERE year = $year AND location_id = $location_id AND department_id = $department_id";
		foreach ($this->executeQuery($budget_sql) as $budget)
			$budgets[$department_id] = $budget['budget'];

		// now department spendings - expenses first
		$totals = array();
		$sql = "SELECT SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, expenses x
 				 WHERE x.expense_id = d.expense_id
   				   AND d.expense_date >= '$date_from'
   				   AND d.expense_date <= '$date_to'
   				   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				   AND x.location_id = $location_id
 				   AND x.department_id = $department_id";

		foreach ($this->executeQuery($sql) as $row)
			$totals[$department_id] = $row['total'];

		// and then orders
		$sql = "SELECT o.department_id, SUM(o.total_price + o.other_charges - o.discount) AS `total`
  				  FROM orders o
 				 WHERE o.order_date >= '$date_from'
   				   AND o.order_date <= '$date_to'
   				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND o.location_id = $location_id
 				   AND o.department_id = $department_id";


		foreach ($this->executeQuery($sql) as $row)
			if (isset($totals[$department_id]))
				$totals[$department_id] += $row['total'];
			else $totals[$department_id] = $row['total'];



		// department names hopefully are not too many
		$departments = array();
		foreach ($this->executeQuery("SELECT * FROM departments WHERE department_id = $department_id") as $department)
			$departments[$department['department_id']] = $department['department_name'];

		// now combine the data
		$department_data = array();
		foreach (array_merge(array_keys($budgets), array_keys($totals)) as $department_id)
		{
			$department_data[$department_id] =
				array('budget' => 0, 'spent' => 0, 'department_id' => $department_id, 'department_name' => 'Unknown');
			if (isset($budgets[$department_id]))
				$department_data[$department_id]['budget'] = round($budgets[$department_id], 2);
			if (isset($totals[$department_id]))
				$department_data[$department_id]['spent'] = round($totals[$department_id], 2);
			if (isset($departments[$department_id]))
				$department_data[$department_id]['department_name'] = $departments[$department_id];
		}

		// and return without any specific index
		return array_values($department_data);

	}

	public function getTotalBudget($date){
		$year = explode('-',$date);

		if (empty($year[0])) {
			$year = date("Y");
		} else{
			$year = $year[0];
		}
		$sql = "SELECT b.budget,d.department_name as department,l.location_name as location
                FROM budgets b
                INNER JOIN departments d ON b.department_id = d.department_id
                INNER JOIN locations l ON b.location_id = l.location_id
                WHERE year = $year
                ORDER BY budget DESC";
		return $this->executeQuery($sql);

	}

	public function getTotalBudgetLeft($start_date,$end_date){

		 $spend_total  =0;
		$year = explode('-',$start_date);

		if (empty($year[0])) {
			$year = date("Y");
		} else{
			$year = $year[0];
		}

		$data = [];
		$sql = "SELECT b.budget,d.department_id,d.department_name as department,l.location_id,l.location_name as location
                FROM budgets b
                INNER JOIN departments d ON b.department_id = d.department_id
                INNER JOIN locations l ON b.location_id = l.location_id
                WHERE year = $year";
		foreach ($this->executeQuery($sql) as $budget)
		{

			$department_id = $budget['department_id'];
			$location_id = $budget['location_id'];
			// total year spend and count - for orders
			$sql = "SELECT *
                    FROM (
                    SELECT SUM(total_price + other_charges - discount) AS `total`,department_id
				    FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				    AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				    AND department_id = $department_id
				    AND location_id = $location_id
				    GROUP BY department_id

                    UNION

		            SELECT SUM(d.expense_price + (d.expense_price * (d.tax_rate / 100.0))) AS `total`,department_id
				    FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				    AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				    AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				    AND department_id = $department_id
				    AND location_id = $location_id
				    GROUP BY department_id
				    )  a  ORDER BY total DESC";
			$spend_data = $this->executeQuery($sql);
			if(!empty($spend_data[0]['total'])){
				$data[] = array('location'=>$budget['location'],'department'=>$budget['department'],'budget'=>$budget['budget'],'left'=>$budget['budget'] - $spend_data[0]['total']);
			} else {
				$data[] = array('location'=>$budget['location'],'department'=>$budget['department'],'budget'=>$budget['budget'],'left'=>$budget['budget']);

			}

		}

		//CVarDumper::dump($data,10,1);die;
		return $data;


	}

	public function getAverageBudgetLeft($start_date,$end_date){

		$year = explode('-',$start_date);
		if (empty($year[0])) {
			$year = date("Y");
		} else{
			$year = $year[0];
		}
		$data = [];



		$sql = "SELECT b.budget,d.department_id,d.department_name as department,l.location_id,l.location_name as location
                FROM budgets b
                INNER JOIN departments d ON b.department_id = d.department_id
                INNER JOIN locations l ON b.location_id = l.location_id
                WHERE year = $year";
		foreach ($this->executeQuery($sql) as $budget)
		{

			$department_id = $budget['department_id'];
			$location_id = $budget['location_id'];
			// total year spend and count - for orders
			$sql = "SELECT *
                    FROM (
                    SELECT SUM(total_price + other_charges - discount) AS `total`,department_id
				    FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				    AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				    AND department_id = $department_id
				    AND location_id = $location_id
				    GROUP BY department_id

                    UNION

		            SELECT SUM(d.expense_price + (d.expense_price * (d.tax_rate / 100.0))) AS `total`,department_id
				    FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				    AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				    AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				    AND department_id = $department_id
				    AND location_id = $location_id
				    GROUP BY department_id
				    )  a  ORDER BY total DESC";
			$spend_data = $this->executeQuery($sql);
			if(!empty($spend_data[0]['total'])){
				$data[] = array('location'=>$budget['location'],'department'=>$budget['department'],'budget'=>$budget['budget'],'left'=>$budget['budget'] - $spend_data[0]['total'],'percentage'=>(($budget['budget'] - $spend_data[0]['total'])*100)/$budget['budget']);
			} else {
				$data[] = array('location'=>$budget['location'],'department'=>$budget['department'],'budget'=>$budget['budget'],'left'=>$budget['budget'],'percentage'=>(($budget['budget'])*100)/$budget['budget']);

			}

		}

		return $data;


	}

	public function getOverBudget($start_date,$end_date){

		$year = explode('-',$start_date);
		if (empty($year[0])) {
			$year = date("Y");
		} else{
			$year = $year[0];
		}
		$data = [];
		$sql = "SELECT b.budget,d.department_id,d.department_name as department,l.location_id,l.location_name as location
                FROM budgets b
                INNER JOIN departments d ON b.department_id = d.department_id
                INNER JOIN locations l ON b.location_id = l.location_id
                WHERE year = $year";
		foreach ($this->executeQuery($sql) as $budget)
		{

			$department_id = $budget['department_id'];
			$location_id = $budget['location_id'];
			// total year spend and count - for orders
			$sql = "SELECT *
                    FROM (
                    SELECT getOrderTotalInGB(GetOrderTotalAmount(orders.order_id),orders.currency_id) AS `total`,department_id
				    FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				    AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				    AND department_id = $department_id
				    AND location_id = $location_id
				    GROUP BY department_id

                    UNION

		            SELECT getOrderTotalInGB(getExpenseTotalAmount(e.expense_id),e.currency_id) AS `total`,department_id
				    FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				    AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				    AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				    AND department_id = $department_id
				    AND location_id = $location_id
				    GROUP BY department_id
				    )  a  ORDER BY total DESC";
			$spend_data = $this->executeQuery($sql);
			if(!empty($spend_data[0]['total'])){
				$left_budget = $budget['budget'] - $spend_data[0]['total'];
				if($left_budget<0){
					$data[] = array('location'=>$budget['location'],'department'=>$budget['department'],'budget'=>$budget['budget'],'left'=>$left_budget,'percentage'=>($left_budget*100)/$budget['budget']);
				 }
			}
		}

		return $data;

	}

	public function getCurrentMonthBudgetLeft(){

		// current month dates
		$start_date = date("Y-m-01");
		$end_date = date("Y-m-" . date("t"));
		$year = date("Y");

		$data = [];
		$sql = "SELECT b.budget,d.department_id,d.department_name as department,l.location_id,l.location_name as location
                FROM budgets b
                INNER JOIN departments d ON b.department_id = d.department_id
                INNER JOIN locations l ON b.location_id = l.location_id
                WHERE year = $year";
		foreach ($this->executeQuery($sql) as $budget)
		{

			$department_id = $budget['department_id'];
			$location_id = $budget['location_id'];
			// total year spend and count - for orders
			$sql = "SELECT *
                    FROM (
                    SELECT SUM(total_price + other_charges - discount) AS `total`,department_id
				    FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				    AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				    AND department_id = $department_id
				    AND location_id = $location_id
				    GROUP BY department_id

                    UNION

		            SELECT SUM(d.expense_price + (d.expense_price * (d.tax_rate / 100.0))) AS `total`,department_id
				    FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				    AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				    AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				    AND department_id = $department_id
				    AND location_id = $location_id
				    GROUP BY department_id
				    )  a  ORDER BY total DESC";
			$spend_data = $this->executeQuery($sql);
			if(!empty($spend_data[0]['total'])){
				$data[] = array('location'=>$budget['location'],'department'=>$budget['department'],'budget'=>$budget['budget'],'left'=>$budget['budget'] - $spend_data[0]['total'],'percentage'=>(($budget['budget'] - $spend_data[0]['total'])*100)/$budget['budget']);
			} else {
				$data[] = array('location'=>$budget['location'],'department'=>$budget['department'],'budget'=>$budget['budget'],'left'=>$budget['budget'],'percentage'=>(($budget['budget'])*100)/$budget['budget']);

			}

		}

		return $data;
	}



	public function getDashboardMetrics($start_date, $end_date)
	{   error_reporting(0);
		$metrics = array( 'total_budget' => 0, 'departments_over_budget' => 0,'record_location_department'=>0 );
		
		$budgetRecord = array();
		$locations = array();
		$departments = array();
		// for the current year
		$department_budgets = array();
		$current_year = date("Y");
		$sql = "SELECT * FROM budgets WHERE `year` = $current_year";
		foreach ($this->executeQuery($sql) as $budget_data)
		{
			$metrics['total_budget'] += $budget_data['budget'];
			$department_budgets[$budget_data['department_id']] = $budget_data['budget'];
		}

		// now get department wise spending - expenses first
		$totals = array();
		$sql = "SELECT loc.location_name,dept.department_name,x.department_id,x.location_id,
					   getOrderTotalInGB(getExpenseTotalAmount(x.expense_id),x.currency_id) AS `total`
  				   FROM  expenses x
  				   INNER JOIN expense_details d  ON x.expense_id = d.expense_id
  				   INNER JOIN budgets b ON x.department_id = b.department_id
  				   INNER JOIN departments dept on x.department_id = dept.department_id
  				   INNER JOIN locations loc  on x.location_id = loc.location_id
   				   WHERE d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date'
   				   AND b.year = $current_year
   				   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   GROUP BY x.department_id";
 		foreach ($this->executeQuery($sql) as $row) {
 			$totals['department'][$row['department_id']] = $row['total'];
 			$totals['location'][$row['location_id']] =0;

 			$locations[$row['location_id']] =  $row['location_name'];
 			$departments[$row['department_id']] =  $row['department_name'];
 		}


		// and then orders
		$sql = "SELECT loc.location_name,dept.department_name,o.department_id,o.location_id, getOrderTotalInGB(GetOrderTotalAmount(o.order_id),o.currency_id) AS `total` FROM orders o
                  INNER JOIN budgets b ON o.department_id = b.department_id
                  INNER JOIN departments dept on  o.department_id = dept.department_id
  				  INNER JOIN locations loc  on o.location_id = loc.location_id
 				  WHERE o.order_date >= '$start_date' AND order_date <= '$end_date'
 				  AND b.year = $current_year
 				  AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				  GROUP BY o.department_id";
 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']])) $totals['department'][$row['department_id']] = 0;
 			$totals['department'][$row['department_id']] += $row['total'];
 			$totals['location'][$row['location_id']] =0;
 			$locations[$row['location_id']] =  $row['location_name'];
 			$departments[$row['department_id']] =  $row['department_name'];
 		}
		// which ones are over budget?
		foreach ($totals['department'] as $department_id => $department_spend) {
			if (!isset($department_budgets[$department_id]) || $department_spend > $department_budgets[$department_id]){
				$metrics['departments_over_budget'] += 1;
				foreach ($totals['location'] as $location_id => $location_value) {

					$budgetRecord[$locations[$location_id]][$departments[$department_id]] =array('budget'=> $department_budgets[$department_id],'spend'=>$department_spend);
					unset($totals['location'][$location_id]);
					break;

				}
			}
		}
		$metrics['record_location_department'] = $budgetRecord;
		// current month dates
		$month_start_date = date("Y-m-01");
		$month_end_date = date("Y-m-" . date("t"));
		// current month spend - for orders
		$monthly_spend = 0;
		$sql = "SELECT  getOrderTotalInGB(GetOrderTotalAmount(order_id),currency_id) AS `order_total` FROM orders 
		 		 WHERE order_date >= '$month_start_date' AND order_date <= '$month_end_date'
		 		   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";
		foreach ($this->executeQuery($sql) as $spend_data)
			$monthly_spend += $spend_data['order_total'];
		
		// and then for travel and expenses
		$sql = "SELECT getOrderTotalInGB(getExpenseTotalAmount(e.expense_id),e.currency_id) AS `expense_total`
				  FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				   AND d.expense_date >= '$month_start_date' AND d.expense_date <= '$month_end_date'
				   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";
		foreach ($this->executeQuery($sql) as $spend_data)
			$monthly_spend += $spend_data['expense_total'];
				

		// how much is used this month?
		$metrics['budget_pct_current_month'] = 0;
		if (isset($metrics['total_budget']) && !empty($metrics['total_budget']))
			$metrics['budget_pct_current_month'] = ($monthly_spend * 100) / $metrics['total_budget'];
						
		return $metrics;
	}

}
