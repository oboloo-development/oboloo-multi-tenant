<?php

class CronEmail extends Common
{
	public function __construct()
	{

		$this->fields = array (
			'id' => 'N',
			'record_id'=>'N',
			'send_to' =>'C',
			'user_id' => 'C',
			'user_type' => 'C',
			'status' => 'C',
			'created_at' => 'C',
			'sent_at' => 'C',
		);

		parent::__construct('id', 'cron_email');
		$this->timestamp = false;
	}
	
}
