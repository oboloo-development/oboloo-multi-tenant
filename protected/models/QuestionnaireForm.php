<?php

class QuestionnaireForm extends Common
{
	
	public function __construct()
	 {
		$this->fields = array (
		 'id' => 'N',
		 'user_id' => 'N',
		 'name' => 'C',
		 'created_datetime' => 'D',
		 'updated_datetime' => 'D',
		);
		
		parent::__construct('id', 'questionnaires_forms');
	 }

		public function getQuoteQuestions($quote_id){
		  return Yii::app()->db->createCommand("SELECT qvq.*
		   FROM quote_questionnaire qvq WHERE qvq.quote_id=".$quote_id." group by question_form_name ")->queryAll();
		} 

		public function getQuoteFormQuestions($quote_id){
		  return Yii::app()->db->createCommand("SELECT * FROM quote_questionnaire qq WHERE qq.quote_id=".$quote_id." group by qq.id ")->queryAll();
		} 

		public function getQuoteVendorQuestionnaire($quote_id){
			return $quote_id;
		} 

		public function getQuestionnaireTableSetting($id = 0, $start = 0, $length = 10, $search_for = "", $order_by = array(), $user_id="")
    {
        
        $sql = " select * from questionnaires_forms ";
        
        if (!empty($id))
            return $this->executeQuery($sql . " WHERE id = $id ", true);
        else {
            if (! empty($search_for)) {
              $sql = "select * from questionnaires_forms  
			  WHERE ( username LIKE '%$search_for%' OR name LIKE '%$search_for%' 
				OR created_datetime LIKE '%$search_for%' OR updated_datetime LIKE '%$search_for%'
				) ";
            }
        
            $order_by_clause = "";
            if (is_array($order_by) && count($order_by)) {
                foreach ($order_by as $column) {
                    $column_index = $column['column'];
                    $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                    
                    $column_name = "";
                    switch ($column_index) {
                        case 0:
                            $column_name = "id";
                            break;
                        case 1:
                            $column_name = "name";
                            break;
                        case 2:
                            $column_name = "username";
                            break;
                        case 3:
                            $column_name = "created_datetime";
                            break;
                        case 4:
                            $column_name = "updated_datetime";
                            break;
                        default:
                            $column_name = "name";
                            break;
                    }
                    
                    if (! empty($column_name)) {
                        if (empty($order_by_clause))
                            $order_by_clause = $column_name . ' ' . $column_direction;
                        else
                            $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                    }
                }
            }
             
            if (empty($order_by_clause))
                $order_by_clause = " name ";
            if (strpos($sql, "WHERE") === false)
                $sql .= " WHERE 1 = 1 ";
            if(!empty($user_id))
                $sql .= " AND user_id = $user_id ";
        
            if($start != 'none'){
              $sql .= " ORDER BY $order_by_clause LIMIT $start, $length ";
            }else{
              $sql .= " ORDER BY $order_by_clause ";
            }
    
            return $this->executeQuery($sql);
        }
    }

}
