<?php

class Project extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'project_id' => 'N',
			'project_name' => 'C',
			'location_id' => 'N',
			'department_id' => 'N',
			'created_by' => 'N',
			'owner' => 'N',
			'status'=>'N',
			'created_at' => 'D',
		);
		$this->alternate_key_name = 'project_name';
		parent::__construct('project_id', 'projects');
		$this->timestamp = false;
	}

	public function saveLocations($project_id, $locations,$departments)
	{
		$this->executeQuery("DELETE FROM locations_projects WHERE project_id = $project_id");
		foreach (explode(",", $locations) as $location_id)
			if (!empty($location_id))
				$this->executeQuery("INSERT INTO locations_projects(location_id, project_id) SELECT $location_id, $project_id");

		$this->executeQuery("DELETE FROM departments_projects WHERE project_id = $project_id");
		foreach (explode(",", $departments) as $department_id)
			if (!empty($department_id))
				$this->executeQuery("INSERT INTO departments_projects(department_id, project_id) SELECT $department_id, $project_id");
	}

	public function getProjectsWithLocations()
	{
		$projects = array();

		$sql = "SELECT * FROM projects ORDER BY project_name";
		foreach ($this->executeQuery($sql) as $project_data)
			$projects[$project_data['project_id']] = $project_data;

		$sql = "SELECT * FROM locations_projects";
		foreach ($this->executeQuery($sql) as $location_data)
		{
			if (isset($projects[$location_data['project_id']]))
			{
				if (!isset($projects[$location_data['project_id']]['locations']))
					$projects[$location_data['project_id']]['locations'] = array();
				$projects[$location_data['project_id']]['locations'][] = $location_data['location_id'];
			}
		}

		$sql = "SELECT * FROM departments_projects";
		foreach ($this->executeQuery($sql) as $department_data)
		{
			if (isset($projects[$department_data['project_id']]))
			{
				if (!isset($projects[$department_data['project_id']]['departments']))
					$projects[$department_data['project_id']]['departments'] = array();
				$projects[$department_data['project_id']]['departments'][] = $department_data['department_id'];
			}
		}

		$locations = array();
		$sql = "SELECT * FROM locations";
		foreach ($this->executeQuery($sql) as $location_data)
			$locations[$location_data['location_id']] = $location_data['location_name'];
		$departments = array();
		$sql = "SELECT * FROM departments";
		foreach ($this->executeQuery($sql) as $department_data)
			$departments[$department_data['department_id']] = $department_data['department_name'];
		
		foreach ($projects as $project_id => $project_data)
		{
			$project_locations = "";
			if(isset($project_data['locations'])) {
				foreach ($project_data['locations'] as $location_id) {
					if (isset($locations[$location_id])) {
						if ($project_locations == "") $project_locations = $locations[$location_id];
						else $project_locations = $project_locations . ", " . $locations[$location_id];
					}
				}
			}

			$project_departments = "";
			if(isset($project_data['departments'])){

				foreach ($project_data['departments'] as $department_id)
				{
					if (isset($departments[$department_id]))
					{
						if ($project_departments == "") $project_departments = $departments[$department_id];
						else $project_departments = $project_departments . ", " . $departments[$department_id];
					}
				}

			}

			$project_data['project_locations'] = $project_locations;
			$project_data['project_departments'] = $project_departments;
			$projects[$project_id] = $project_data;
		}

		//CVarDumper::dump($projects,10,1);die;

		return $projects;

	}

	public function getDepartmentsByCategory($start_date, $end_date)
	{
		// expenses first
		$this->currency_fields = array( 'budget', 'total' );
		$totals = array();
		$sql = "SELECT x.department_id, d.expense_type_id AS category_id, e.value AS category, v.department_name, 
					   SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, departments v, expense_types e, expenses x
 				 WHERE x.department_id = v.department_id
 				   AND x.expense_id = d.expense_id 
   				   AND d.expense_type_id = e.id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
   				   AND x.expense_status != 'Declined' 
   				   AND x.expense_status != 'Cancelled'
 				 GROUP BY x.department_id, d.expense_type_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT x.department_id, d.expense_type_id AS category_id, e.value AS category, v.department_name, 
						   SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, departments v, expense_types e, expenses x
	 				 WHERE x.department_id = v.department_id
	 				   AND x.expense_id = d.expense_id 
	   				   AND d.expense_type_id = e.id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date' 
   					   AND x.expense_status != 'Declined' 
	   				   AND x.expense_status != 'Cancelled'
	 				 GROUP BY x.department_id, d.expense_type_id";

 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']]))
 				$totals[$row['department_id']] = array();
 			$totals[$row['department_id']][] = $row;
 		}
		
		// and then orders
		$sql = "SELECT o.department_id, v.department_name,  
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price * ((100 + o.tax_rate) / 100) + o.other_charges - o.discount) AS `total`
  				  FROM order_details d, orders o, departments v, products p, categories c
 				 WHERE d.order_id = o.order_id
   				   AND o.department_id = v.department_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
   				   AND d.product_type = 'Product'
   				   AND d.product_id = p.product_id
   				   AND p.category_id = c.id 
   				   AND o.order_status != 'Declined' 
   				   AND o.order_status != 'Cancelled'
 				 GROUP BY o.department_id, c.id
 
 				 UNION

				SELECT o.department_id, v.department_name, 
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price * ((100 + o.tax_rate) / 100) + o.other_charges - o.discount) AS `total`
  				  FROM order_details d, orders o, departments v, products p, categories c, bundle_products b
 				 WHERE d.order_id = o.order_id
   				   AND o.department_id = v.department_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
   				   AND d.product_type = 'Bundle'
   				   AND b.bundle_id = d.product_id 
   				   AND b.product_id = p.product_id 
   				   AND p.category_id = c.id 
   				   AND o.order_status != 'Declined' 
   				   AND o.order_status != 'Cancelled'
 				 GROUP BY b.bundle_id, o.department_id, c.id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT o.department_id, v.department_name,  
						   c.id AS category_id, c.value AS category,
						   SUM(d.unit_price) AS `total`
	  				  FROM order_details d, orders o, departments v, products p, categories c
	 				 WHERE d.order_id = o.order_id
	   				   AND o.department_id = v.department_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND d.product_type = 'Product'
	   				   AND d.product_id = p.product_id
	   				   AND p.category_id = c.id 
   					   AND o.order_status != 'Declined' 
	   				   AND o.order_status != 'Cancelled'
	 				 GROUP BY o.department_id, c.id
	 
	 				 UNION
	
					SELECT o.department_id, v.department_name, 
						   c.id AS category_id, c.value AS category,
						   SUM(d.unit_price) AS `total`
	  				  FROM order_details d, orders o, departments v, products p, categories c, bundle_products b
	 				 WHERE d.order_id = o.order_id
	   				   AND o.department_id = v.department_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND d.product_type = 'Bundle'
	   				   AND b.bundle_id = d.product_id 
	   				   AND b.product_id = p.product_id 
	   				   AND p.category_id = c.id 
   					   AND o.order_status != 'Declined' 
	   				   AND o.order_status != 'Cancelled'
	 				 GROUP BY b.bundle_id, o.department_id, c.id";
 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']]))
 				$totals[$row['department_id']] = array();
 			$totals[$row['department_id']][] = $row;
 		}

		$this->currency_fields = array();
 		return $totals;				
	}

	
	public function getSpendByCategory($location_id, $start_date, $end_date)
	{
		// expenses first
		$this->currency_fields = array( 'budget', 'total' );
		$totals = array();
		$sql = "SELECT x.department_id, d.expense_type_id AS category_id, e.value AS category, v.department_name, 
					   SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, departments v, expense_types e, expenses x
 				 WHERE x.department_id = v.department_id
 				   AND x.expense_id = d.expense_id 
   				   AND d.expense_type_id = e.id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
   				   AND x.location_id = $location_id
				   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY x.department_id, d.expense_type_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT x.department_id, d.expense_type_id AS category_id, e.value AS category, v.department_name, 
						   SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, departments v, expense_types e, expenses x
	 				 WHERE x.department_id = v.department_id
	 				   AND x.expense_id = d.expense_id 
	   				   AND d.expense_type_id = e.id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date' 
	   				   AND x.location_id = $location_id
   					   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY x.department_id, d.expense_type_id";
 				 
 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']]))
 				$totals[$row['department_id']] = array();
 			$totals[$row['department_id']][] = $row;
 		}
		
		// and then orders
		$sql = "SELECT o.department_id, v.department_name,  
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price + ((d.unit_price * o.tax_rate) / 100.0) + o.other_charges - o.discount) AS `total`
				  FROM order_details d, orders o, departments v, products p, categories c
	 			 WHERE d.order_id = o.order_id
	   			   AND o.department_id = v.department_id
	   			   AND o.order_date >= '$start_date'
	   			   AND o.order_date <= '$end_date' 
	   			   AND d.product_type = 'Product'
	   			   AND d.product_id = p.product_id
	   			   AND p.category_id = c.id 
	   			   AND o.location_id = $location_id
   				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 			 GROUP BY o.department_id, c.id
	 
	 			 UNION
	
				SELECT o.department_id, v.department_name, 
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price + ((d.unit_price * o.tax_rate) / 100.0) + o.other_charges - o.discount) AS `total`
	  			  FROM order_details d, orders o, departments v, products p, categories c, bundle_products b
	 			 WHERE d.order_id = o.order_id
	   			   AND o.department_id = v.department_id
	   			   AND o.order_date >= '$start_date'
	   			   AND o.order_date <= '$end_date' 
	   			   AND d.product_type = 'Bundle'
	   			   AND b.bundle_id = d.product_id 
	   			   AND b.product_id = p.product_id 
	   			   AND p.category_id = c.id 
	   			   AND o.location_id = $location_id
   				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 		 	 GROUP BY b.bundle_id, o.department_id, c.id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT o.department_id, v.department_name, c.id AS category_id, c.value AS category, SUM(d.unit_price) AS `total`
					  FROM order_details d, orders o, departments v, products p, categories c
	 				 WHERE d.order_id = o.order_id
	   				   AND o.department_id = v.department_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND d.product_type = 'Product'
	   				   AND d.product_id = p.product_id
	   				   AND p.category_id = c.id 
	   				   AND o.location_id = $location_id
   					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY o.department_id, c.id
	 
	 				 UNION
	
					SELECT o.department_id, v.department_name, c.id AS category_id, c.value AS category, SUM(d.unit_price) AS `total`
	  				  FROM order_details d, orders o, departments v, products p, categories c, bundle_products b
	 				 WHERE d.order_id = o.order_id
	   				   AND o.department_id = v.department_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND d.product_type = 'Bundle'
	   				   AND b.bundle_id = d.product_id 
	   				   AND b.product_id = p.product_id 
	   				   AND p.category_id = c.id 
	   				   AND o.location_id = $location_id
   					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY b.bundle_id, o.department_id, c.id";

	 	foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']]))
 				$totals[$row['department_id']] = array();
 			$totals[$row['department_id']][] = $row;
 		}

 		$category_spend_data = array();
 		$department_spend_totals = array();
 		foreach ($totals as $department_id => $department_data)
 		{
 			foreach ($department_data as $department_row)
 			{
 				$department_row['total'] = round($department_row['total'], 2);
 				$category_spend_data[] = $department_row;
 				
 				if (!isset($department_spend_totals[$department_id]))
					$department_spend_totals[$department_id] = 0;
				$department_spend_totals[$department_id] += $department_row['total'];
 			}
 		}

 		arsort($department_spend_totals);
 		$sorted_category_spend_data = array();
 		foreach ($department_spend_totals as $department_id => $department_totals)
 			foreach ($category_spend_data as $department_row)
 				if ($department_row['department_id'] == $department_id)
 					$sorted_category_spend_data[] = $department_row;
		
		$this->currency_fields = array();
		return $sorted_category_spend_data;			
	}
		
	/*	
	public function getDepartmentsBySpendType($start_date, $end_date)
	{
		// expenses first
		$totals = array();
		$sql = "SELECT x.department_id, d.expense_type_id AS category_id, e.value AS category, v.department_name, 
					   SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`, 'E' AS `type`
  				  FROM expense_details d, departments v, expense_types e, expenses x
 				 WHERE x.department_id = v.department_id
 				   AND x.expense_id = d.expense_id 
   				   AND d.expense_type_id = e.id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
 				 GROUP BY x.department_id, d.expense_type_id";
 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']]))
 				$totals[$row['department_id']] = array();
 			$totals[$row['department_id']][] = $row;
 		}
		
		// and then orders
		$sql = "SELECT o.department_id, v.department_name,  
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price + ((d.unit_price * o.tax_rate) / 100.0)) AS `total`,
					   'O' AS `type`
  				  FROM order_details d, orders o, departments v, products p, categories c
 				 WHERE d.order_id = o.order_id
   				   AND o.department_id = v.department_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
   				   AND d.product_type = 'Product'
   				   AND d.product_id = p.product_id
   				   AND p.category_id = c.id 
 				 GROUP BY o.department_id, c.id
 
 				 UNION

				SELECT o.department_id, v.department_name, 
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price + ((d.unit_price * o.tax_rate) / 100.0)) AS `total`,
					   'O' AS `type`
				  FROM order_details d, orders o, departments v, products p, categories c, bundle_products b
 				 WHERE d.order_id = o.order_id
   				   AND o.department_id = v.department_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
   				   AND d.product_type = 'Bundle'
   				   AND b.bundle_id = d.product_id 
   				   AND b.product_id = p.product_id 
   				   AND p.category_id = c.id 
 				 GROUP BY b.bundle_id, o.department_id, c.id";
 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']]))
 				$totals[$row['department_id']] = array();
 			$totals[$row['department_id']][] = $row;
 		}
 		
 		return $totals;				
	}
	*/

	public function getLocationsBySpendType($start_date, $end_date)
	{
		// expenses first
		$this->currency_fields = array( 'budget', 'total' );
		$totals = array();

		$sql = "SELECT x.location_id, d.expense_type_id AS category_id, e.value AS category, v.location_name, 
					   SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`, 'E' AS `type`
  				  FROM expense_details d, locations v, expense_types e, expenses x
 				 WHERE x.location_id = v.location_id
 				   AND x.expense_id = d.expense_id 
   				   AND d.expense_type_id = e.id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
				   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY x.location_id, d.expense_type_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT x.location_id, d.expense_type_id AS category_id, e.value AS category, v.location_name, 
						   SUM(d.expense_price) AS `total`, 'E' AS `type`
	  				  FROM expense_details d, locations v, expense_types e, expenses x
	 				 WHERE x.location_id = v.location_id
	 				   AND x.expense_id = d.expense_id 
	   				   AND d.expense_type_id = e.id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date' 
   					   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY x.location_id, d.expense_type_id";
		 		
 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['location_id']]))
 				$totals[$row['location_id']] = array();
 			$totals[$row['location_id']][] = $row;
 		}
		
		// and then orders
		$sql = "SELECT o.location_id, v.location_name,  
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price + ((d.unit_price * o.tax_rate) / 100.0) + o.other_charges - o.discount) AS `total`,
					   'O' AS `type`
  				  FROM order_details d, orders o, locations v, products p, categories c
 				 WHERE d.order_id = o.order_id
   				   AND o.location_id = v.location_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
   				   AND d.product_type = 'Product'
   				   AND d.product_id = p.product_id
   				   AND p.category_id = c.id 
				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY o.location_id, c.id
 
 				 UNION

				SELECT o.location_id, v.location_name, 
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price + ((d.unit_price * o.tax_rate) / 100.0) + o.other_charges - o.discount) AS `total`,
					   'O' AS `type`
				  FROM order_details d, orders o, locations v, products p, categories c, bundle_products b
 				 WHERE d.order_id = o.order_id
   				   AND o.location_id = v.location_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
   				   AND d.product_type = 'Bundle'
   				   AND b.bundle_id = d.product_id 
   				   AND b.product_id = p.product_id 
   				   AND p.category_id = c.id 
				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY b.bundle_id, o.location_id, c.id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT o.location_id, v.location_name,  
						   c.id AS category_id, c.value AS category,
						   SUM(d.unit_price) AS `total`, 'O' AS `type`
	  				  FROM order_details d, orders o, locations v, products p, categories c
	 				 WHERE d.order_id = o.order_id
	   				   AND o.location_id = v.location_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND d.product_type = 'Product'
	   				   AND d.product_id = p.product_id
	   				   AND p.category_id = c.id 
   					   AND o.order_status != 'Declined' 
	   				   AND o.order_status != 'Cancelled'
	 				 GROUP BY o.location_id, c.id
	 
	 				 UNION
	
					SELECT o.location_id, v.location_name, 
						   c.id AS category_id, c.value AS category,
						   SUM(d.unit_price) AS `total`, 'O' AS `type`
					  FROM order_details d, orders o, locations v, products p, categories c, bundle_products b
	 				 WHERE d.order_id = o.order_id
	   				   AND o.location_id = v.location_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND d.product_type = 'Bundle'
	   				   AND b.bundle_id = d.product_id 
	   				   AND b.product_id = p.product_id 
	   				   AND p.category_id = c.id 
   					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY b.bundle_id, o.location_id, c.id";

 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['location_id']]))
 				$totals[$row['location_id']] = array();
 			$totals[$row['location_id']][] = $row;
 		}
		
 		$location_totals = array();
 		foreach ($totals as $location_id => $location_data)
 		{
 			$location_totals[$location_id] = 0;
 			foreach ($location_data as $location_row)
 				$location_totals[$location_id] += $location_row['total'];
 		}
		array_multisort($location_totals, SORT_DESC, $totals);
 		 		 		
 		// print_r($totals); exit;
		$this->currency_fields = array();
 		return $totals;				
	}
	
	
	private function getSpendData($location_id, $start_date, $end_date)
	{
		// spend data - expenses first
		$this->currency_fields = array( 'budget', 'total' );
		$totals = array();
		$sql = "SELECT x.department_id,  
					   SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, expenses x
 				 WHERE x.expense_id = d.expense_id 
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
   				   AND x.location_id = $location_id
   				   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY x.department_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT x.department_id, SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, expenses x
	 				 WHERE x.expense_id = d.expense_id 
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date' 
	   				   AND x.location_id = $location_id
   				   	   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	   				 GROUP BY x.department_id";
		 				 
 		foreach ($this->executeQuery($sql) as $row)
 			$totals[$row['department_id']] = $row['total'];
		
		// orders next
		$sql = "SELECT department_id, SUM(total_price + other_charges - discount) AS `total` FROM orders
 				 WHERE order_date >= '$start_date' AND order_date <= '$end_date'
 				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				   AND location_id = $location_id GROUP BY department_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT department_id, SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `total` FROM orders
	 				 WHERE order_date >= '$start_date' AND order_date <= '$end_date' 
	 				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				   AND location_id = $location_id GROUP BY department_id";
		 		
 		foreach ($this->executeQuery($sql) as $row)
		{
			if (!isset($totals[$row['department_id']]))
				$totals[$row['department_id']] = 0;
			$totals[$row['department_id']] += $row['total'];	
		}
		
		$this->currency_fields = array();
		return $totals;
	}
	
	public function getBudgetSpendData($location_id, $start_date, $end_date)
	{
		// get budgets by departments for the current year
		$this->currency_fields = array( 'budget', 'total' );
		$budgets = array();
		$sql = "SELECT * FROM budgets WHERE `year` = YEAR('$start_date') AND location_id = $location_id";
		foreach ($this->executeQuery($sql) as $budget_data)
			$budgets[$budget_data['department_id']] = $budget_data['budget'];
		
		// and spend data too
		$totals = $this->getSpendData($location_id, $start_date, $end_date);
		
		// we also need various department names
		$departments = array();
		foreach ($this->executeQuery("SELECT * FROM departments") as $department)
			$departments[$department['department_id']] = $department['department_name'];

		// now return the data for budgets and spend
		$budget_and_spend = array();
		foreach (array_unique(array_merge(array_keys($budgets), array_keys($totals))) as $department_id)
		{
			$department_name = 'Unknown';
			$department_budget = 0;
			$department_spend = 0;
			
			if (isset($departments[$department_id])) $department_name = $departments[$department_id];
			if (isset($totals[$department_id])) $department_spend = $totals[$department_id];
			if (isset($budgets[$department_id])) $department_budget = $budgets[$department_id];
			
			$budget_and_spend[] = 
				array( 'department_name' => $department_name, 'budget' => $department_budget, 'spend' => $department_spend );
		}
		
		$this->currency_fields = array();
		usort($budget_and_spend, function($a, $b) { return $b['spend'] - $a['spend']; });
		return $budget_and_spend;				
	}


	public function getDepartmentAvgSuppliers($location_id,$start_date,$end_date)
	{
		$sql = "SELECT v.vendor_name AS vendor,v.vendor_id,dpt.department_name AS department,COUNT(v.vendor_id) AS vendor_count
		          FROM orders o
				  INNER JOIN vendors v ON o.vendor_id = v.vendor_id
				  INNER JOIN order_details d ON o.order_id = d.order_id
				  INNER JOIN departments dpt ON o.department_id = dpt.department_id
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				  AND location_id = $location_id GROUP BY v.vendor_id";

		  return $this->executeQuery($sql);

	}

	public function getSupplierAvgCategories($location_id,$start_date,$end_date)
	{
		$sql = "SELECT v.vendor_name AS vendor,v.vendor_id,dpt.department_name AS department,c.value as category,COUNT(v.vendor_id) AS vendor_count
		          FROM orders o
				  INNER JOIN vendors v ON o.vendor_id = v.vendor_id
				  INNER JOIN order_details d ON o.order_id = d.order_id
				  INNER JOIN products p ON d.product_id = p.product_id
				  INNER JOIN categories c ON p.category_id = c.id
				  INNER JOIN departments dpt ON o.department_id = dpt.department_id
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				  AND location_id = $location_id GROUP BY v.vendor_id";

		return $this->executeQuery($sql);

	}


	public function getYearlySortedData($location_id, $start_date, $end_date)
	{
		// department wise spend data
		$this->currency_fields = array( 'budget', 'total' );
		$totals = $this->getSpendData($location_id, $start_date, $end_date);
		arsort($totals);
				
		// we also need various department names
		$departments = array();
		foreach ($this->executeQuery("SELECT * FROM departments") as $department)
			$departments[$department['department_id']] = $department['department_name'];

		// get total to calculate percentage
		$total = 0;
		foreach ($totals as $department_id => $department_total)
			$total += $department_total;
		
		// now assign name percentage etc. and return
		$yearly_data = array();
		foreach ($totals as $department_id => $department_total)
		{
			$department_name = 'Unknown';
			if (isset($departments[$department_id])) $department_name = $departments[$department_id];
			$percent = round(($department_total * 100) / $total, 2);
			$yearly_data[] = array('department_name' => $department_name, 'total' => number_format($department_total, 0), 'percent' => $percent);
		}
		
		$this->currency_fields = array();
		return $yearly_data;
	}


	public function getSpendByVendor($location_id, $start_date, $end_date)
	{
		// expenses first
		$this->currency_fields = array( 'budget', 'total' );
		$totals = array();
		$sql = "SELECT x.department_id, d.vendor_id, v.vendor_name, 
					   SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, vendors v, expenses x
 				 WHERE x.expense_id = d.expense_id 
   				   AND d.vendor_id = v.vendor_id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
   				   AND x.location_id = $location_id
   				   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				 GROUP BY x.department_id, v.vendor_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT x.department_id, d.vendor_id, v.vendor_name, SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, vendors v, expenses x
	 				 WHERE x.expense_id = d.expense_id 
	   				   AND d.vendor_id = v.vendor_id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date' 
	   				   AND x.location_id = $location_id
   				   	   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	   				 GROUP BY x.department_id, v.vendor_id";

 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']]))
 				$totals[$row['department_id']] = array();
 			$totals[$row['department_id']][] = $row;
 		}
		
		// and then orders
		$sql = "SELECT o.department_id, v.vendor_name, o.vendor_id, 
						SUM(o.total_price + o.other_charges - o.discount) AS `total`
				  FROM orders o, vendors v
 				 WHERE o.vendor_id = v.vendor_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
   				   AND o.location_id = $location_id
   				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY o.department_id, o.vendor_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT o.department_id, v.vendor_name, o.vendor_id, 
							SUM(o.total_price * ((100 - o.tax_rate) / 100) + o.other_charges - o.discount) AS `total`
					  FROM orders o, vendors v
	 				 WHERE o.vendor_id = v.vendor_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND o.location_id = $location_id
   				   	   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	   				 GROUP BY o.department_id, o.vendor_id";
		
 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']]))
 				$totals[$row['department_id']] = array();
 			$totals[$row['department_id']][] = $row;
 		}

		// we also need various department names
		$departments = array();
		foreach ($this->executeQuery("SELECT * FROM departments") as $department)
			$departments[$department['department_id']] = $department['department_name'];

		// now build the data in needed format
		$department_spend_totals = array();
 		$vendor_spend_data = array();
 		foreach ($totals as $department_id => $department_data)
 		{
 			$department_vendors = array();
			$department_total = 0;
 			foreach ($department_data as $department_row)
 			{
 				$department_total += $department_row['total'];
 				if (!in_array($department_row['vendor_id'], $department_vendors))
 					$department_vendors[] = $department_row['vendor_id']; 
 			}
 			
			$department_vendor_spend = array();
			$department_vendor_spend['department_id'] = $department_id;
			$department_vendor_spend['department_name'] = 'Unknown';
			if (isset($departments[$department_id]))
				$department_vendor_spend['department_name'] = $departments[$department_id];
			$department_vendor_spend['vendor_count'] = count($department_vendors);
			$department_vendor_spend['vendor_total'] = $department_total;

			if (!isset($department_spend_totals[$department_id]))
				$department_spend_totals[$department_id] = 0;
			$department_spend_totals[$department_id] += $department_total;
			
			$vendor_spend_data[] = $department_vendor_spend; 
 		}
 		
		arsort($department_spend_totals);
 		$sorted_vendor_spend_data = array();
 		foreach ($department_spend_totals as $department_id => $department_totals)
 			foreach ($vendor_spend_data as $department_row)
 				if ($department_row['department_id'] == $department_id)
 					$sorted_vendor_spend_data[] = $department_row;
				
		$this->currency_fields = array();
		return $sorted_vendor_spend_data;			
	}

		
	public function getDashboardMetrics($location_id, $start_date, $end_date)
	{
		$metrics = array( 'spend_total' => 0, 'spend_count' => 0, 'departments_over_budget' => 0, 
						'total_budget' => 0, 'avg_per_category' => 0, 'avg_per_department' => 0 );
		
		// total year spend and count - for orders
		$this->currency_fields = array( 'order_total', 'expense_total', 'budget', 'total' );
		$sql = "SELECT SUM(total_price + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count` FROM orders 
				 WHERE order_date >= '$start_date' AND order_date <= '$end_date' AND location_id = $location_id
				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count` FROM orders 
					 WHERE order_date >= '$start_date' AND order_date <= '$end_date' AND location_id = $location_id
					   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";

		foreach ($this->executeQuery($sql) as $spend_data)
		{
			$metrics['spend_total'] += $spend_data['order_total'];
			$metrics['spend_count'] += $spend_data['order_count'];
		}
		
		// and then for travel and expenses
		$sql = "SELECT COUNT(DISTINCT d.expense_id) AS `expense_count`,
						SUM(d.expense_price + (d.expense_price * (d.tax_rate / 100.0))) AS `expense_total`
				  FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				   AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date' 
				   AND e.location_id = $location_id
				   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT COUNT(DISTINCT d.expense_id) AS `expense_count`, SUM(d.expense_price) AS `expense_total`
					  FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
					   AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date' 
					   AND e.location_id = $location_id
					   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";
				
		foreach ($this->executeQuery($sql) as $spend_data)
		{
			$metrics['spend_total'] += $spend_data['expense_total'];
			$metrics['spend_count'] += $spend_data['expense_count'];
		}
				
		// for the current year
		$department_budgets = array();
		$sql = "SELECT * FROM budgets WHERE `year` = YEAR('$start_date') AND location_id = $location_id";
				foreach ($this->executeQuery($sql) as $budget_data)
		{
			$metrics['total_budget'] += $budget_data['budget'];
			$department_budgets[$budget_data['department_id']] = $budget_data['budget'];
		}
		
		$metrics['pct_budget_used'] = 0;
		if (isset($metrics['total_budget']) && !empty($metrics['total_budget']))
			$metrics['pct_budget_used'] = round((($metrics['total_budget'] - $metrics['spend_total']) * 100) / $metrics['total_budget'], 2); 

		$metrics['total_budget'] = number_format($metrics['total_budget'], 0);			
		$metrics['spend_total'] = number_format($metrics['spend_total'], 0);			
			
		// now get department wise spending - expenses first
		$totals = array();
		$sql = "SELECT x.department_id, 
					   SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, expenses x
 				 WHERE x.expense_id = d.expense_id 
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date'
   				   AND x.location_id = $location_id 
   				   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY x.department_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT x.department_id, SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, expenses x
	 				 WHERE x.expense_id = d.expense_id 
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date'
	   				   AND x.location_id = $location_id 
   					   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY x.department_id";

 		foreach ($this->executeQuery($sql) as $row)
 			$totals[$row['department_id']] = $row['total'];
		
		// and then orders
		$sql = "SELECT department_id, SUM(total_price + other_charges - discount) AS `total` FROM orders
 				 WHERE order_date >= '$start_date' AND order_date <= '$end_date'
 				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				   AND location_id = $location_id GROUP BY department_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT department_id, SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `total` FROM orders
	 				 WHERE order_date >= '$start_date' AND order_date <= '$end_date' 
	 				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				   AND location_id = $location_id GROUP BY department_id";
		 				   
 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['department_id']])) $totals[$row['department_id']] = 0;
 			$totals[$row['department_id']] += $row['total'];
 		}

		// which ones are over budget?
		foreach ($totals as $department_id => $department_spend)
			if (!isset($department_budgets[$department_id]) ||
					$department_spend > $department_budgets[$department_id]) 
				$metrics['departments_over_budget'] += 1;

		// number of departments who have spent
		$department_count = count($totals);
				
		// next number of vendors used
		$totals = array();
		$vendors_used = array();
		$sql = "SELECT d.vendor_id, SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, expenses e
 				 WHERE e.expense_id = d.expense_id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date'
   				   AND e.location_id = $location_id
   				   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY d.vendor_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT d.vendor_id, SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, expenses e
	 				 WHERE e.expense_id = d.expense_id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date'
	   				   AND e.location_id = $location_id 
   					   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY d.vendor_id";

 		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['vendor_id']])) $totals[$row['vendor_id']] = 0;
 			$totals[$row['vendor_id']] += $row['total'];
			$vendors_used[] = $row['vendor_id'];
 		}
		
		// and then orders too
		$sql = "SELECT vendor_id, SUM(total_price + other_charges - discount) AS `total` FROM orders o 
				 WHERE order_date >= '$start_date' AND order_date <= '$end_date' 
 				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND location_id = $location_id GROUP BY vendor_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT vendor_id, SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `total` FROM orders o 
					 WHERE order_date >= '$start_date' AND order_date <= '$end_date' 
	 				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	   				   AND location_id = $location_id GROUP BY vendor_id";
		   				   
   		foreach ($this->executeQuery($sql) as $row)
 		{
 			if (!isset($totals[$row['vendor_id']])) $totals[$row['vendor_id']] = 0;
 			$totals[$row['vendor_id']] += $row['total'];
			if (!in_array($row['vendor_id'], $vendors_used))
				$vendors_used[] = $row['vendor_id'];
 		}
 		
		// next count total categories used 
		$category_count = 0;
		$sql = "SELECT COUNT(DISTINCT expense_type_id) AS category_count FROM expense_details 
				 WHERE expense_date >= '$start_date' AND expense_date <= '$end_date' 
 				   AND expense_id IN ( SELECT expense_id FROM expenses WHERE location_id = $location_id
 				   					      AND expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') ) ";
		foreach ($this->executeQuery($sql) as $category_data)
			$category_count += $category_data['category_count'];

		$sql = "SELECT COUNT(DISTINCT p.category_id) AS category_count 
				  FROM order_details d, orders o, products p 
				 WHERE d.order_id = o.order_id AND d.product_id = p.product_id
				   AND d.product_type = 'Product' AND o.location_id = $location_id
				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				   AND o.order_date >= '$start_date' AND o.order_date <= '$end_date'";
		foreach ($this->executeQuery($sql) as $category_data)
			$category_count += $category_data['category_count'];

		$sql = "SELECT COUNT(DISTINCT p.category_id) AS category_count 
				  FROM order_details d, orders o, products p, bundles b, bundle_products bp 
				 WHERE d.order_id = o.order_id AND d.product_id = b.bundle_id 
				   AND d.product_type = 'Bundle' AND o.location_id = $location_id
				   AND b.bundle_id = bp.bundle_id AND bp.product_id = p.product_id  
				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				   AND o.order_date >= '$start_date' AND o.order_date <= '$end_date'";
		foreach ($this->executeQuery($sql) as $category_data)
			$category_count += $category_data['category_count'];

		if (count($vendors_used) && $category_count)
			$metrics['avg_per_category'] = round(count($vendors_used) / $category_count, 2);

		if (count($vendors_used) && $department_count)
			$metrics['avg_per_department'] = round(count($vendors_used) / $department_count, 2);
			 																				
		$this->currency_fields = array();
		return $metrics;
	}

	public function getVendorTypeCount($location_id, $start_date, $end_date)
	{
		$vendors_used = array();
		$sql = "SELECT DISTINCT o.department_id, o.vendor_id, v.preferred_flag
		 		  FROM orders o, vendors v
		 		 WHERE o.vendor_id = v.vendor_id AND o.location_id = $location_id
		 		   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				   AND o.order_date >= '$start_date' AND o.order_date <= '$end_date'";
		foreach ($this->executeQuery($sql) as $vendor_data)
		{
			if (!isset($vendors_used[$vendor_data['department_id']]))
				$vendors_used[$vendor_data['department_id']] 
					= array('preferred' => array(), 'non_preferred' => array());
			if ($vendor_data['preferred_flag'] == 1)
				$vendors_used[$vendor_data['department_id']]['preferred'][] = $vendor_data['vendor_id'];
			else
				$vendors_used[$vendor_data['department_id']]['non_preferred'][] = $vendor_data['vendor_id'];
		}

		$sql = "SELECT DISTINCT x.department_id, d.vendor_id, v.preferred_flag
		 		  FROM expenses x, vendors v, expense_details d
		 		 WHERE d.vendor_id = v.vendor_id AND x.location_id = $location_id
		 		   AND d.expense_id = x.expense_id
		 		   AND x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				   AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'";
		foreach ($this->executeQuery($sql) as $vendor_data)
		{
			if (!isset($vendors_used[$vendor_data['department_id']]))
				$vendors_used[$vendor_data['department_id']] 
					= array('preferred' => array(), 'non_preferred' => array());
			if ($vendor_data['preferred_flag'] == 1)
			{
				if (!in_array($vendor_data['vendor_id'], $vendors_used[$vendor_data['department_id']]['preferred']))
					$vendors_used[$vendor_data['department_id']]['preferred'][] = $vendor_data['vendor_id'];
			}
			else
			{
				if (!in_array($vendor_data['vendor_id'], $vendors_used[$vendor_data['department_id']]['non_preferred']))
					$vendors_used[$vendor_data['department_id']]['non_preferred'][] = $vendor_data['vendor_id'];
			}
		}
		
		$departments = array();
		foreach ($this->executeQuery("SELECT * FROM departments") as $department_data)
			$departments[$department_data['department_id']] = $department_data['department_name'];
			
		$vendor_statistics = array();
		foreach ($vendors_used as $department_id => $department_data)
		{
			$row = array( 'department_name' => 'N/A' );
			if (isset($departments[$department_id])) $row['department_name'] = $departments[$department_id];
			$preferred = count($department_data['preferred']);
			$non_preferred = count($department_data['non_preferred']);
			$total = $preferred + $non_preferred;
			if ($total != 0)
			{
				$row['preferred'] = round(($preferred * 100) / $total, 2);
				$row['non_preferred'] = round(($non_preferred * 100) / $total, 2);

				$row['preferred'] = $preferred;
				$row['non_preferred'] = $non_preferred;
			}
			else $row['preferred'] = $row['non_preferred'] = 0;
			$vendor_statistics[] = $row;
		}

		usort($vendor_statistics, function($a, $b) 
		{
			 return ($b['preferred'] + $b['non_preferred']) - ($a['preferred'] + $a['non_preferred']); 
		});		
		return $vendor_statistics;
	}

	public function getProjectOrderAndAmounts($projectID,$from_date, $to_date)
	{

		$totals = array();
		// and then orders
		$sql = "SELECT o.*, v.vendor_name AS vendor,p.project_name,p.project_id, u.full_name AS user,u.user_id,v.vendor_id, cr.currency
		          FROM orders o
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN currency_rates cr ON o.currency_id = cr.id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  LEFT JOIN projects p ON o.project_id = p.project_id
				   WHERE o.project_id = $projectID";

		/*if (!empty($from_date)) $sql .= " AND o.order_date >= '$from_date' ";
		if (!empty($to_date)) $sql .= " AND o.order_date <= '$to_date' ";*/

		return $this->executeQuery($sql);
	}

	

}
