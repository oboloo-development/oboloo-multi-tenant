<?php

class Expense extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'expense_id' => 'N',
			'location_id' => 'N',
			'department_id' => 'N',
			'project_id' => 'N',
			'spend_type' => 'C',
			'user_id' => 'N',
			'user_name' => 'N',
			'user_email' => 'N',
			'currency_id' => 'N',
			'currency_rate' => 'N',
			'created_by_user_id' => 'N',
			'expense_name' => 'C',
			'description' => 'C',
			'expense_status' => 'C',
			'total_expenses' => 'N',
			'calc_expenses' => 'N'
		);

		$this->currency_fields = array ( 'total_expenses' );
		parent::__construct('expense_id', 'expenses');
	}

	public function getExpenses($expense_id = 0, $location_id = 0, $department_id = 0, $expense_status = "", $from_date = "", $to_date = "")
	{
		$tool_currency = Yii::app()->session['user_currency'];
		
		$sql = "SELECT e.*,getTotalInGBandToolCurrency(getExpenseTotalAmount(e.expense_id),e.currency_id,'".$tool_currency."') AS calc_func_expenses_gb,getExpenseTotalAmount(e.expense_id) AS calc_func_expenses,u.full_name AS user,u.user_id, cr.currency,cr.currency_symbol
		          FROM (select expenses.*,det.expense_date from expenses inner join expense_details as det on expenses.expense_id=det.expense_id group by expenses.expense_id) e
		          LEFT JOIN currency_rates cr ON e.currency_id = cr.id
				  LEFT JOIN users u ON e.user_id = u.user_id";


		if (!isset(Yii::app()->session['user_type']) || in_array(Yii::app()->session['user_type'],array(1,2,3)))
		{
			//$sql .= " WHERE ( e.user_id = $my_user_id OR e.created_by_user_id = $my_user_id )";
		}else
		if (!isset(Yii::app()->session['user_type']))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if (empty($expense_id))
				$sql .= " WHERE ( e.user_id = $my_user_id OR e.created_by_user_id = $my_user_id )";
		}
		else
		{
			if (empty($expense_id)) $sql .= " WHERE e.expense_id > 0 ";
		}

		if ($expense_id)
			return $this->executeQuery($sql . " WHERE e.expense_id = $expense_id ", true);
		else 
		{
			if (!empty($location_id)) {
				$location_id = implode(',',$location_id);
				$sql .= " AND e.location_id IN ($location_id)";
			}
			/*if (!empty($department_id)) $sql .= " AND e.department_id = $department_id ";*/
			if(!empty($department_id)) {
				$department_id = implode(',',$department_id);
				$sql .= " AND e.department_id IN ($department_id)";
			}

			if (!empty($expense_status)){
				$status = "'" . implode("','", $expense_status) . "'";
				$sql .= " AND e.expense_status IN ($status)";
			}
			
			if (!empty($from_date))
			{
				if (!empty($to_date))
					$sql .= " AND e.expense_id IN ( SELECT expense_id FROM expense_details 
													 WHERE expense_date >= '$from_date' AND expense_date <= '$to_date' ) ";
				else
					$sql .= " AND e.expense_id IN ( SELECT expense_id FROM expense_details 
													 WHERE expense_date >= '$from_date' ) ";
			}
			else
			{
				if (!empty($to_date))
					$sql .= " AND e.expense_id IN ( SELECT expense_id FROM expense_details 
													 WHERE expense_date <= '$to_date' ) ";
			}
			
			return $this->executeQuery($sql);
		}
	}

	public function createRouteForApproval($expense_id = 0)
	{
		// do we already have a route created? then don't bother
		$route_created = false;
		$expense_route = $this->executeQuery("SELECT * FROM order_routing WHERE approval_type = 'E' AND order_id = $expense_id");
		if ($expense_route && is_array($expense_route) && count($expense_route))
			foreach ($expense_route as $approval_node) $route_created = true;

		// create the first entry only
		if ($route_created)
		{
			// get the expense data
			$expense_data = $this->executeQuery("SELECT * FROM expenses WHERE expense_id = $expense_id", true);
			$expense_status = is_array($expense_data) && isset($expense_data['expense_status']) ? $expense_data['expense_status'] : "Pending";
			if($expense_status == 'Submitted')
			{   $approvalUsers = array();
				foreach ($expense_route as $approval_node)
				{
					$approval_status = $approval_node['routing_status'];
					$approval_routing_id = $approval_node['id'];
					if ($approval_status == 'Information') {
						$approval_user_id = $approval_node['user_id'];
						$approvalUsers[]=$approval_user_id;
						$this->executeQuery("UPDATE order_routing SET routing_status = 'Pending' WHERE id = $approval_routing_id");

						
							   $approval_id = $approval_node['id'];
								$notification = new Notification();
								$notification->rs = array();
								$notification->rs['id'] = 0;
								$notification->rs['user_id'] = $approval_user_id;
								$notification->rs['notification_text'] =
									'<a href="' . AppUrl::bicesUrl('routing/edit/' . $approval_id) . '">Expense ID #' 
									. $expense_id . '</a>' . " has been submitted for your approval.";
								$notification->rs['notification_date'] = date("Y-m-d H:i");
								$notification->rs['read_flag'] = 0;
								$notification->write();
						
					}


				}
				// send mail to the user that some approval is necessary
				if(!empty($approvalUsers))
					foreach($approvalUsers as $approver){
						$this->expenseApproval($approver,$expense_id);
						//$this->expenseSubmitted($approver,$expense_id);
					}
					// and may be also to the order creator that the order has been submitted
				
			}
		}
		else
		{
			// get the expense data
			$expense_data = $this->executeQuery("SELECT * FROM expenses WHERE expense_id = $expense_id", true);

			// routing is based on location and department
			$location_id = is_array($expense_data) && isset($expense_data['location_id']) ? $expense_data['location_id'] : 0;
			$department_id = is_array($expense_data) && isset($expense_data['department_id']) ? $expense_data['department_id'] : 0;
			$expense_price = is_array($expense_data) && isset($expense_data['total_expenses']) ? $expense_data['total_expenses'] : 0;
			$expense_status = is_array($expense_data) && isset($expense_data['expense_status']) ? $expense_data['expense_status'] : "Pending";

			if ($expense_status != "Pending")
			{
				$route = new OrderRoute();
				//$route_data = $route->getRoute('E', $location_id, $department_id);
				$route_data = $route->getUsersApprover($location_id, $department_id,"approve_travel_expense");

				if ($route_data && is_array($route_data) && count($route_data))
				{
					// insert into order routing the first user as per level
					$route_created = true;
					$approvalUsers = array();
					foreach ($route_data as $route_node)
					{
						$approval_user_id = isset($route_node['user_id']) ? $route_node['user_id'] : 0;
						if ($approval_user_id)
						{   $approvalUsers[]=$approval_user_id;
							$order_route = new OrderRoute();
							$order_route->rs['id'] = 0;
							$order_route->rs['approval_type'] = 'E';
							$order_route->rs['order_id'] = $expense_id;
							$order_route->rs['user_id'] = $approval_user_id;
							$order_route->rs['routing_status'] = 'Pending';
							$order_route->rs['status'] = 1;
							/*if ($route_node['data_type'] == 'A' && $expense_price > $route_node['amount'])
							{
								$order_route->rs['routing_status'] = 'Approved';
								$order_route->rs['status'] = 0;
							}*/
							$order_route->rs['routing_date'] = date("Y-m-d");
							$order_route->write();
							$approval_id = $order_route->rs['id'];
							// create notifications
							if ($order_route->rs['status'] == 1)
							{
								$notification = new Notification();
								$notification->rs = array();
								$notification->rs['id'] = 0;
								$notification->rs['user_id'] = $approval_user_id;
								$notification->rs['notification_text'] =
									'<a href="' . AppUrl::bicesUrl('routing/edit/' . $approval_id) . '">Expense ID #' 
									. $expense_id . '</a>' . " has been submitted for your approval.";
								$notification->rs['notification_date'] = date("Y-m-d H:i");
								$notification->rs['read_flag'] = 0;
								$notification->write();
							}
						}
					}

					// send mail to the user that some approval is necessary
					if(!empty($approvalUsers))
						foreach($approvalUsers as $approver){
							$this->expenseApproval($approver,$expense_id);
							//$this->expenseSubmitted($approver,$expense_id);
						}
					// and may be also to the order creator that the order has been submitted
					
				}
			}
		}

		return $route_created;
	}

	public function saveExpenseFile($expense_id, $file_name, $file_description)
	{
		return $this->executeQuery("INSERT INTO expense_files (expense_id, file_name, description) SELECT $expense_id, '$file_name', '$file_description'");
	}

	public function deleteFile($expense_id, $file_name)
	{
		$this->executeQuery("DELETE FROM expense_files WHERE expense_id = $expense_id AND file_name = '$file_name'");
	}

	public function expenseApproval($user_id,$expense_id)
	{


		$user_data = $this->executeQuery("SELECT * FROM users WHERE user_id = $user_id",1);
		$expense_owner = $this->executeQuery("SELECT *
          FROM expenses
        INNER JOIN users ON expenses.user_id=users.user_id WHERE expenses.expense_id = $expense_id",1);

		$expense_data = $this->executeQuery("SELECT * FROM (select exp_det_in.*,exp.currency_id from expenses exp inner join expense_details exp_det_in on exp.expense_id=exp_det_in.expense_id) as expense_details
                                            INNER JOIN expenses ON expense_details.expense_id=expenses.expense_id
                                            INNER JOIN expense_types ON expense_details.expense_type_id=expense_types.id
                                            WHERE expense_details.expense_id = $expense_id");


		$mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];

		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
		$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
		$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;">';
		$email_body .= '<p>Dear ' . $user_data['full_name'].', </p>';
		$email_body .= '</td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">';
		$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#000000;"><strong>You have received an travel & expense from <span style="color:#10a798;">' . $expense_owner['full_name'].'</span> </strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		$email_body .= '<p>Please find details of the travel & expense in the table below as well as the purchase travel & expense attached:</p>';
		$email_body .= '<table  border="0" cellpadding="2" cellspacing="1" width="600" align="center" style="width:600px;">
                            <tr>
                            <td style="width: 15%"><b>Name</b></td>
                            <td style="width: 11%"><b>Type</b></td>
                            <td style="width: 11%"><b>Price</b></td>
                            <td style="width: 11%"><b>Tax Rate</b></td>
                            <td style="width: 15%"><b>Total Price</b></td>
                            <td style="width: 39%"><b>Expense Notes</b></td>
                            </tr>
                            <tr><td colspan="5">&nbsp;</td></tr>';

		foreach($expense_data as $data){
			$tax = ($data['expense_price']* $data['tax_rate'])/100;
			$total_price = $tax+$data['expense_price'];
			$currency = FunctionManager::currencySymbol($data['currency_id']);
			$email_body .= '<tr>
                            <td>' . $data['expense_name'].'</td>
                            <td>' . $data['value'].'</td>
                            <td>' . $currency.$data['expense_price'].'</td>
                            <td>' . $data['tax_rate'].'%('.$currency.$tax.')</td>
                            <td>' . $currency.$total_price.'</td>
                            <td>' . $data['expense_notes'].'</td>
                            </tr>';
		}
		$email_body .= '</table></div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:11px;text-align: center;">';
		$email_body .= '<p><span style="color:#10a798;">This travel & expense is powered by Spend 365  LTD</span></p>';
		$email_body .= '</td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
		$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
		$mail->addAddress($user_data['email'], $user_data['full_name']);
		$mail->Subject = 'Spend 365: Travel & Expense Approval';
		$mail->isHTML(true);
		$mail->Body = $email_body;
		$mail->send();

		return 0;
	}


	public function expenseSubmitted($user_id,$expense_id)
	{

		$user_data = $this->executeQuery("SELECT *
          FROM users WHERE user_id = $user_id",1);

		$expense_data = $this->executeQuery("SELECT * FROM expense_details
                                            INNER JOIN expenses ON expense_details.expense_id=expenses.expense_id
                                            INNER JOIN expense_types ON expense_details.expense_type_id=expense_types.id
                                            WHERE expense_details.expense_id = $expense_id");

		$mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];

		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
		$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
		$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;">';
		$email_body .= '<p>Dear ' . $user_data['full_name'].', </p>';
		$email_body .= '</td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">';
		$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#000000;"><strong>New travel & expense confirmation <span style="color:#10a798;">-' .$expense_id.'</span> </strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		$email_body .= '<p>You have requested a new travel & expense. Please see details below.</p>';
		$email_body .= '<table  border="0" cellpadding="2" cellspacing="1" width="600" align="center" style="width:600px;">
                            <tr>
                            <td style="width: 15%"><b>Name</b></td>
                            <td style="width: 11%"><b>Type</b></td>
                            <td style="width: 11%"><b>Price</b></td>
                            <td style="width: 11%"><b>Tax Rate</b></td>
                            <td style="width: 15%"><b>Total Price</b></td>
                            <td style="width: 39%"><b>Expense Notes</b></td>
                            </tr>
                            <tr><td colspan="5">&nbsp;</td></tr>';

		foreach($expense_data as $data){
			$tax = ($data['expense_price']* $data['tax_rate'])/100;
			$total_price = $tax+$data['expense_price'];
			$email_body .= '<tr>
                            <td>' . $data['expense_name'].'</td>
                            <td>' . $data['value'].'</td>
                            <td>' . $data['expense_price'].'</td>
                            <td>' . $data['tax_rate'].'</td>
                            <td>' . $total_price.'</td>
                            <td>' . $data['expense_notes'].'</td>
                            </tr>';
		}
		$email_body .= '</table></div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:11px;">';
		$email_body .= '<p>You will receive email confirmation once the travel & expense status has been updated by the approver(s).</p>';
		$email_body .= '</td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
               <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                <a href="' .AppUrl::bicesUrl('expenses/edit/'.$expense_id).'" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Travel & Expense Status</a>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;">
					 <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
					 </td>
				 </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;" align="left">
						 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
							 <p>Terms of Service &amp; User Agreement</p>
							 <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
							 <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
						 </div>
					 </td>
				 </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
		$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
		$mail->addAddress($user_data['email'], $user_data['full_name']);
		$mail->Subject = 'Spend 365: Travel & Expense Submitted';
		$mail->isHTML(true);
		$mail->Body = $email_body;
		$mail->send();

		return 0;
	}

	public function export($data = array(), $file = '')
	{
		$expense_data_with_details = $expense_data = array();		
		$sql = "SELECT expense_id, expense_name, location_name, department_name,project_name,cr.currency,cr.rate as currency_rate,full_name,
						DATE_FORMAT(e.created_datetime, '%d-%b-%Y') AS expense_date, description, total_expenses, expense_status
				  FROM expenses e
				  LEFT JOIN locations l ON e.location_id = l.location_id
				  LEFT JOIN departments d ON e.department_id = d.department_id
				  LEFT JOIN projects pr ON e.project_id = pr.project_id
				  LEFT JOIN users u ON e.user_id = u.user_id
				  LEFT JOIN currency_rates cr ON e.currency_id = cr.id
				 ORDER BY e.expense_id";
		foreach ($this->executeQuery($sql) as $expense) $expense_data[$expense['expense_id']] = $expense;

		if (count($expense_data))
		{
			$expense_details = array();
			$sql = "SELECT d.*, e.value AS expense_type, vendor_name,
							DATE_FORMAT(expense_date, '%d-%b-%Y') AS expense_date
					  FROM expense_details d, expense_types e, vendors v
					 WHERE d.expense_type_id = e.id AND d.vendor_id = v.vendor_id 
					   AND expense_id IN ( " . implode(",", array_keys($expense_data)) . " ) ";
			foreach ($this->executeQuery($sql) as $expense_item)
			{
				if (!isset($expense_details[$expense_item['expense_id']]))
					$expense_details[$expense_item['expense_id']] = array();
				$expense_details[$expense_item['expense_id']][] = $expense_item;
			}
			
			foreach ($expense_data as $expense_id => $expense)
			{
				$details = array();
				if (isset($expense_details[$expense_id]))
					foreach ($expense_details[$expense_id] as $expense_item)
						$details[] = array('expense_type' => $expense_item['expense_type'], 
											'supplier' => $expense_item['vendor_name'], 
											'expense_date' => $expense_item['expense_date'], 
											'expense_amount' => $expense_item['expense_price'], 
											'tax_rate' => $expense_item['tax_rate'], 
											'comments' => $expense_item['expense_notes']);
				$expense['details'] = $details;
				$expense_data_with_details[] = $expense;
			}
		}
		parent::export($expense_data_with_details, 'expenses');			
	}

}
