<?php

class Company extends Common
{
	public function __construct()
	{
	    $this->fields = array (
	        'id' => 'N',
	        'company_name' => 'C',
	        'company_number' => 'C',
	        'tax_number' => 'C'
	    );
	    parent::__construct('id', 'company');
	    $this->timestamp = false;
	}

	public function getCompany()
	{
		return $this->executeQuery("SELECT * FROM company",1);

	}
}
