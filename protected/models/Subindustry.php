<?php

class Subindustry extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'sub_industries');
		$this->timestamp = false;
	}

	public function export($data = array(), $file = '')
	{
		$sql = "SELECT i.value AS industry_name, s.value AS sub_industry_name
		          FROM industries i, sub_industries s
		         WHERE i.id = s.code 
		         ORDER BY i.value, s.value";
		parent::export($this->executeQuery($sql), 'subindustries');
	}

}
