<?php

class ContractNotification extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'contract_id' => 'N',
			'note' => 'C',
			'date' => 'D'
		);
		parent::__construct('id', 'contract_notifications');
		$this->timestamp = false;
	}

	public function getNotifications($contract_id = 0)
	{
		return $this->executeQuery("SELECT * FROM contract_notifications WHERE contract_id = $contract_id ORDER BY `date` ASC");
	}

}
