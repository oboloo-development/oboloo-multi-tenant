<?php
//require_once('protected/vendors/phpexcel/PHPExcel.php');
//require_once('protected/vendors/phpexcel/PHPExcel/IOFactory.php');
Yii::import('application.vendors.phpexcel.*');
spl_autoload_unregister(array('YiiBase','autoload'));
require_once(Yii::app()->basePath.'/vendors/phpexcel/PHPExcel.php');
require_once(Yii::app()->basePath.'/vendors/phpexcel/PHPExcel/IOFactory.php');
spl_autoload_register(array('YiiBase','autoload'));

class Report extends Common
{
	public $location_id = 0;
	public $vendor_id = 0;
	public $department_id = 0;
	public $order_status = "";
	public $currency_id = 0;
	public $user_id = 0;
	public function __construct()
	{
		$this->currency_fields = array( 'total', 'order_total', 'expense_total', 'total_amount' );
		parent::__construct();
	}

	public function getReportTotals($report_type, $report_from_date, $report_to_date, $dim_1, $dim_2, $dim_3, $dim_4, $limit = 0)
	{
		$report_data = $this->getReportData($report_type, $report_from_date, $report_to_date, $dim_1, $dim_2, $dim_3, $dim_4, $limit);
		if (is_array($report_data) && count($report_data))
		{
			$row_totals = array();
			$total = 0;
			foreach ($report_data as $report_row) 
			{
				$total += $report_row['total'];
				$row_totals[] = $report_row['total'];
			}
			
			foreach ($report_data as $report_index => $report_row)
			{
				if ($report_index !== 'total')
				{
					$row_percent = 0;
					if ($total != 0) $row_percent = (($report_row['total'] * 100.0) / $total); 
					$report_data[$report_index]['percent'] = $row_percent;
				}
			}
			
			// order by total descending and then only apply limit :)
			if ($limit) 
			{
				array_multisort($row_totals, SORT_DESC, $report_data);
				$report_data = array_slice($report_data, 0, $limit);
			} 

			$report_data['total'] = array('dim_1' => 'Total', 'total' => $total, 'percent' => 100, 'dim_2' => '', 'dim_3' => '', 'dim_4' => '');
		}

		$dim_1_totals = array();
		$total_row = array();
		foreach ($report_data as $row_idx => $report_row)
		{
			if ($row_idx === 'total') $total_row = $report_row;
			else
			{
				if (!isset($report_row['dim_1_id'])) 
				{
					$report_row['dim_1_id'] = 0;
					if (!isset($report_row['dim_1']) || empty($report_row['dim_1'])) $report_row['dim_1'] = 'Unknown';
					$report_data[$row_idx] = $report_row;
				}
				
				$dim_1_id = $report_row['dim_1_id'];
				if (!isset($dim_1_totals[$dim_1_id])) $dim_1_totals[$dim_1_id] = 0;
				$dim_1_totals[$dim_1_id] += $report_row['total'];
			}
		}
		arsort($dim_1_totals);

		$sorted_report_data = array();
		foreach ($dim_1_totals as $dim_1_id => $dim_1_data)
		{
			foreach ($report_data as $row_idx => $report_row)
			{
				if ($row_idx === 'total') $total_row = $report_row;
				else
				{
				    if (empty($dim_1_id) && empty($report_row['dim_1_id'])) {
				        $sorted_report_data[] = $report_row;
				    } elseif ((string)$dim_1_id === $report_row['dim_1_id']) {
				        $sorted_report_data[] = $report_row;
				    }
				}
			}
		}
		if (is_array($total_row) && count($total_row)) 
			$sorted_report_data['total'] = $total_row;
			
		return $sorted_report_data;
	}

	public function getReportData($report_type, $report_from_date, $report_to_date, $dim_1, $dim_2, $dim_3, $dim_4, $limit = 0)
	{

		$report_data = array();
		if ($report_type != "O" && $report_type != "E") $report_type = 'A';
		
		for ($i=1; $i<=2; $i++)
		{
			if ($i == 1 && $report_type == 'E') continue;
			if ($i == 2 && $report_type == 'O') continue;

			$report_data[$i] = array();

			$dimensions = $dim_1 . $dim_2 . $dim_3 . $dim_4;
			if ($i == 2) $dimensions = str_replace("C", "E", $dimensions);
			
			$select_clause = $this->getQueryClause($i, $dimensions, 'SELECT');
			$group_by_clause = $this->getQueryClause($i, $dimensions, 'GROUP');
			
			if (!empty($group_by_clause))
			{
				$sql = "";
				$location_filter = $department_filter = $status_filter = $vendor_filter = "";
				
				$location_id = $this->location_id;
				$department_id = $this->department_id;
				$order_status = $this->order_status;
				$vendor_id = $this->vendor_id;
				
				if (Yii::app()->session['exclude_tax_from_dashboard'])
				{
					if ($i == 1)
					{

						if ($this->location_id) {
							$location_id = implode(',',$location_id);
							$location_filter = " AND o.location_id IN ($location_id)";
						}
						if ($this->department_id) $department_filter = " AND o.department_id = $department_id ";

						if ($this->order_status) {
							$status = "'" . implode("','", $order_status) . "'";
							$status_filter = "AND o.order_status IN ($status)";
						}
						if ($this->vendor_id) $location_filter = " AND o.vendor_id = $vendor_id ";
						
						$sql = "SELECT $select_clause, 'O' AS `type`, 
										sum(getOrderTotalInGB(GetOrderTotalAmount(o.order_id),o.currency_id)) AS `total`, u.full_name as user_name
								   FROM orders o, order_details d, products p, vendors v,users u
								   WHERE o.order_id = d.order_id
								   AND u.user_id = o.user_id
								   AND p.product_id = d.product_id
								   AND o.vendor_id = v.vendor_id
								   AND o.order_date >= '$report_from_date'
								   AND o.order_date <= '$report_to_date'
								   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
								   		$location_filter $department_filter 
								   		$status_filter $vendor_filter
								 GROUP BY $group_by_clause";
					}
					else
					{
						if ($this->location_id) {
							$location_id = implode(',',$location_id);
							$location_filter = " AND e.location_id IN ($location_id)";
						}
						if ($this->department_id) $department_filter = " AND e.department_id = $department_id ";

						if ($this->order_status) {
							$status = "'" . implode("','", $order_status) . "'";
							$status_filter = "AND e.expense_status IN ($status)";
						}
						if ($this->vendor_id) $location_filter = " AND d.vendor_id = $vendor_id ";
												
						$sql = "SELECT $select_clause, 'E' AS `type`,  sum(getOrderTotalInGB(getExpenseTotalAmount(e.expense_id),e.currency_id))  AS `total`, u.full_name as user_name
								  FROM expenses e, expense_details d, vendors v,users u
								 WHERE e.expense_id = d.expense_id
								   AND u.user_id = e.user_id
								   AND d.vendor_id = v.vendor_id
								   AND d.expense_date >= '$report_from_date'
								   AND d.expense_date <= '$report_to_date'
								   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
								   		$location_filter $department_filter 
								   		$status_filter $vendor_filter
								 GROUP BY $group_by_clause";
					}
				}
				else
				{
					if ($i == 1)
					{

						if ($this->location_id) {
							$location_id = implode(',',$location_id);
							$location_filter = " AND o.location_id IN ($location_id)";
						}
						if ($this->department_id) $department_filter = " AND o.department_id = $department_id ";

						if ($this->order_status) {
							$status = "'" . implode("','", $order_status) . "'";
							$status_filter = "AND o.order_status IN ($status)";
						}

						if ($this->vendor_id) $location_filter = " AND o.vendor_id = $vendor_id ";
						
						$sql = "SELECT $select_clause, 'O' AS `type`, sum(getOrderTotalInGB(GetOrderTotalAmount(o.order_id),o.currency_id)) AS `total`,
                                  u.full_name as user_name,v.vendor_name as vendor,l.location_name as location, dpt.department_name as department
								  FROM orders o, order_details d, products p, vendors v,users u, locations l, departments dpt
								 WHERE o.order_id = d.order_id
								   AND u.user_id = o.user_id
								   AND p.product_id = d.product_id
								   AND o.vendor_id = v.vendor_id
								   AND o.location_id = l.location_id
								   AND o.department_id = dpt.department_id
								   AND o.order_date >= '$report_from_date'
								   AND o.order_date <= '$report_to_date'
								   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
								   		$location_filter $department_filter 
								   		$status_filter $vendor_filter
								 GROUP BY $group_by_clause";
					}
					else
					{
						if ($this->location_id) {
							$location_id = implode(',',$location_id);
							$location_filter = " AND e.location_id IN ($location_id)";
						}
						if ($this->department_id) $department_filter = " AND e.department_id = $department_id ";

						if ($this->order_status) {
							$status = "'" . implode("','", $order_status) . "'";
							$status_filter = "AND e.expense_status IN ($status)";
						}
						if ($this->vendor_id) $location_filter = " AND d.vendor_id = $vendor_id ";
						
						$sql = "SELECT $select_clause, 'E' AS `type`, 
										sum(getOrderTotalInGB(getExpenseTotalAmount(e.expense_id),e.currency_id)) AS `total`,
										u.full_name as user_name,v.vendor_name as vendor,location_name as location, dpt.department_name as department
								  FROM expenses e, expense_details d, vendors v,users u,locations l,departments dpt
								 WHERE e.expense_id = d.expense_id
								   AND u.user_id = e.user_id
								   AND d.vendor_id = v.vendor_id
								   AND e.location_id = l.location_id
								   AND e.department_id = dpt.department_id
								   AND d.expense_date >= '$report_from_date'
								   AND d.expense_date <= '$report_to_date'
								   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
								   		$location_filter $department_filter 
								   		$status_filter $vendor_filter
								 GROUP BY $group_by_clause";
					}
				}
				
				if (!empty($sql))
				{
					if (!empty($limit)) $sql .= " ORDER BY total DESC LIMIT $limit ";
					foreach ($this->executeQuery($sql) as $report_row)
						$report_data[$i][$this->getKey($report_row, $group_by_clause)] = $report_row;
				}
			}
		}

		//CVarDumper::dump($report_data,10,1);die;
		return $this->populateAttributes($this->mergeData($report_data), $dim_1, $dim_2, $dim_3, $dim_4);
	}

	private function getQueryClause($index, $dimensions, $clause_type)
	{
		$query_clause = "";
		
		for ($i=0; $i<strlen($dimensions); $i++)
		{
			$current_column_name = "";
			$current_dimension = substr($dimensions, $i, 1);
			
			switch ($current_dimension)
			{
				case 'L' : 
					if ($index == 1) $current_column_name = 'o.location_id'; 
					else $current_column_name = 'e.location_id';
					break;

				case 'D' : 
					if ($index == 1) $current_column_name = 'o.department_id'; 
					else $current_column_name = 'e.department_id';
					break;

				case 'V' : 
					if ($index == 1) $current_column_name = 'o.vendor_id'; 
					else $current_column_name = 'd.vendor_id';
					break;

				case 'C' : 
				case 'E' : 
					if ($index == 1) $current_column_name = 'p.category_id'; 
					else $current_column_name = 'd.expense_type_id';
					break;

				case 'R' : 
					if ($index == 1) $current_column_name = 'p.subcategory_id'; 
					else $current_column_name = 'd.expense_type_id';
					break;

				case 'I' : 
					$current_column_name = 'v.industry_id';
					break;

				case 'S' : 
					$current_column_name = 'v.subindustry_id';
					break;

				case 'Y' : 
					if ($clause_type == 'SELECT')
					{
						if ($index == 1) $current_column_name = 'YEAR(o.order_date) AS report_year'; 
						else $current_column_name = 'YEAR(d.expense_date) AS report_year';
					}
					else $current_column_name = 'report_year'; 
					break;

				case 'M' : 
					if ($clause_type == 'SELECT')
					{
						if ($index == 1) $current_column_name = 'YEAR(o.order_date) AS report_year, MONTH(o.order_date) AS report_month'; 
						else $current_column_name = 'YEAR(d.expense_date) AS report_year, MONTH(d.expense_date) AS report_month';
					}
					else $current_column_name = 'report_year, report_month'; 
					break;

				default  : break;
			}

			if (!empty($current_column_name))
			{
				if ($query_clause == "") $query_clause = $current_column_name;
				else $query_clause = $query_clause . ', ' . $current_column_name;
			}
		}

		return $query_clause;
	}

	private function populateAttributes($report_data, $dim_1, $dim_2, $dim_3, $dim_4)
	{
		for ($i=1; $i<=4; $i++)
		{
			$var_name = 'dim_' . $i;
			$report_data = $this->populateNameAttributes($report_data, $i, $$var_name);
		}
       
		return $report_data;
	}
	
	private function populateNameAttributes($input_report_data, $dim_index, $dim_type)
	{
		$attributes = array();
		$attribute_sql = "";
		$attribute_name = "";
		$table_key_name = "";
		
		switch ($dim_type)
		{
			case 'L' : 
				$attribute_sql = "SELECT location_id AS id, location_name AS value FROM locations"; 
				$attribute_name = 'location_id'; 
				break;
				
			case 'D' : 
				$attribute_sql = "SELECT department_id AS id, department_name AS value FROM departments"; 
				$attribute_name = 'department_id'; 
				break;
			
			case 'V' : 
				$attribute_sql = "SELECT vendor_id AS id, vendor_name AS value FROM vendors"; 
				$attribute_name = 'vendor_id'; 
				break;
			
			case 'C' : 
				$attribute_sql = "SELECT * FROM categories"; 
				$attribute_name = 'category_id';
				$table_key_name = "id";
				break;

			case 'R' : 
				$attribute_sql = "SELECT * FROM sub_categories"; 
				$attribute_name = 'subcategory_id';
				$table_key_name = "id";
				break;

			case 'I' : 
				$attribute_sql = "SELECT * FROM industries"; 
				$attribute_name = 'industry_id'; 
				$table_key_name = "id";
				break;

			case 'S' : 
				$attribute_sql = "SELECT * FROM sub_industries"; 
				$attribute_name = 'subindustry_id'; 
				$table_key_name = "id";
				break;

			default  : break;
		}

		if (!empty($attribute_sql))
		{
			$selected_attributes = array(0);
			foreach ($input_report_data as $input_report_row)
				if (isset($input_report_row[$attribute_name]))
					$selected_attributes[] = $input_report_row[$attribute_name];				
			
			if (empty($table_key_name))
				$attribute_sql .= " WHERE $attribute_name IN ( " . implode(",", $selected_attributes) . " ) ";
			else $attribute_sql .= " WHERE $table_key_name IN ( " . implode(",", $selected_attributes) . " ) ";

			foreach ($this->executeQuery($attribute_sql) as $attribute_data)
				$attributes[$attribute_data['id']] = $attribute_data['value'];
		}
		
		// special case for expense types :(
		$expense_types = array();
		foreach ($this->executeQuery("SELECT * FROM expense_types") as $attribute_data)
			$expense_types[$attribute_data['id']] = $attribute_data['value'];
		
		$report_data = array();
		$searchable_values = false;
		$idx = 1;
		foreach ($input_report_data as $report_row)
		{
			$searchable_values = &$attributes;
			if ($report_row['type'] == 'E' && ($dim_type == 'C' || $dim_type == 'R'))
			{
				$searchable_values = &$expense_types;
				if ($attribute_name == 'category_id' || $attribute_name == 'subcategory_id') 
					$attribute_name = 'expense_type_id';
			}

			$report_row['dim_' . $dim_index] = '';
			if ($dim_type == 'M')
			{
				if (isset($report_row['report_month']))
				{
					$report_row['dim_' . $dim_index] = date("M Y", strtotime($report_row['report_year'] . '-' . $report_row['report_month'] . '-01'));
					$report_row['dim_' . $dim_index . '_id'] = $report_row['dim_' . $dim_index]; 				
				}
			}
			else if ($dim_type == 'Y')
			{
				if (isset($report_row['report_year']))
				{
					$report_row['dim_' . $dim_index] = $report_row['report_year'];				
					$report_row['dim_' . $dim_index . '_id'] = $report_row['report_year'];				
				}
			}
			else
			{
				if (isset($report_row[$attribute_name]) && isset($searchable_values[$report_row[$attribute_name]]))
				{
					if ($report_row['type'] == 'E' && ($dim_type == 'C' || $dim_type == 'R'))
					{
						if ($attribute_name == 'expense_type_id') 
							$report_row['dim_' . $dim_index . '_id'] = 'E_' . $report_row[$attribute_name];
					}
					else $report_row['dim_' . $dim_index . '_id'] = $report_row[$attribute_name];
					$report_row['dim_' . $dim_index] = $searchable_values[$report_row[$attribute_name]];
				}
			}
			
			$report_data[] = $report_row;
		}

		return $report_data;
	}

	private function mergeData($input_report_data)
	{
		$report_data = $input_report_data;	
		$merged_data = array();
		
		if (isset($report_data[1]))
			foreach ($report_data[1] as $report_row_key => $report_row)
			{
				if (isset($report_data[2][$report_row_key]))
				{
					$report_row['total'] += $report_data[2][$report_row_key]['total'];
					unset($report_data[2][$report_row_key]);
				}			
				$merged_data[$report_row_key] = $report_row;
			}

		if (isset($report_data[2]))
			foreach ($report_data[2] as $report_row_key => $report_row)
				$merged_data[$report_row_key] = $report_row;

		return $merged_data;
	}

	
	public function getKey($report_row, $group_by_clause)
	{
		$row_key = "";
		$group_by_columns = array();
		foreach (explode(",", $group_by_clause) as $group_by_column)
		{
			if (strpos($group_by_column, ".") === false) 
				$group_by_columns[] = ltrim(rtrim($group_by_column));
			else
			{
				list($table_alias, $column_name) = explode(".", $group_by_column);
				$group_by_columns[] = ltrim(rtrim($column_name));
			}
		}
		
		foreach ($report_row as $report_row_key => $report_row_value)
		{
			if (in_array($report_row_key, $group_by_columns))
			{
				if ($row_key == "") $row_key = $report_row_value;
				else $row_key = $row_key . '|' . $report_row_value; 
			}
		}
		return $row_key;
	}

	public function getOrderStatusTotals($start_date, $end_date)
	{
		$totals = array();
		
		// procurement orders
		$sql = "SELECT IF(order_status = '', 'Pending', order_status) AS `status`, 
						COUNT(*) AS `total_count` FROM orders
				 WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				 GROUP BY `status`";
		foreach ($this->executeQuery($sql) as $row)
		{
			if (!isset($totals[$row['status']])) $totals[$row['status']] = 0;
			$totals[$row['status']] += $row['total_count']; 
		}

		// travel and expenses
		$sql = "SELECT IF(e.expense_status = '', 'Pending', e.expense_status) AS `status`, 
						COUNT(DISTINCT e.expense_id) AS `total_count` 
				  FROM expenses e, expense_details d
				 WHERE d.expense_id = e.expense_id
				   AND d.expense_date >= '$start_date' 
				   AND d.expense_date <= '$end_date'
				 GROUP BY `status`";
		foreach ($this->executeQuery($sql) as $row)
		{
			if (!isset($totals[$row['status']])) $totals[$row['status']] = 0;
			$totals[$row['status']] += $row['total_count']; 
		}
		
		return $totals;
	}

	public function getDashboardMetrics($start_date, $end_date)
	{
		$metrics = array( 'spend_total' => 0, 'spend_count' => 0, 'spend_total_pending' => 0, 'spend_count_pending' => 0, 
						'to_be_paid_count' => 0, 'to_be_paid_amount' => 0, 'to_be_received_count' => 0, 'to_be_received_amount' => 0 );
		
		// total year spend and count - for orders
		$sql = "SELECT SUM(total_price + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count`
				  FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count`
					  FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
					  AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";
		foreach ($this->executeQuery($sql) as $spend_data)
		{
			$metrics['spend_total'] += $spend_data['order_total'];
			$metrics['spend_count'] += $spend_data['order_count'];
		}
		
		// and then for travel and expenses
		$sql = "SELECT COUNT(DISTINCT d.expense_id) AS `expense_count`,
						SUM(d.expense_price + (d.expense_price * (d.tax_rate / 100.0))) AS `expense_total`
				  FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				   AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
		$sql = "SELECT COUNT(DISTINCT d.expense_id) AS `expense_count`, SUM(d.expense_price) AS `expense_total`
				  FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				   AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";

		foreach ($this->executeQuery($sql) as $spend_data)
		{
			$metrics['spend_total'] += $spend_data['expense_total'];
			$metrics['spend_count'] += $spend_data['expense_count'];
		}
			
		// total year spend and count submitted - for orders
		$sql = "SELECT SUM(total_price + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count`
				  FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				   AND order_status = 'Submitted'";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count`
					  FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
					   AND order_status = 'Submitted'";
				
		foreach ($this->executeQuery($sql) as $spend_data)
		{
			$metrics['spend_total_pending'] += $spend_data['order_total'];
			$metrics['spend_count_pending'] += $spend_data['order_count'];
		}
		
		// and then submitted for travel and expenses
		$sql = "SELECT COUNT(DISTINCT d.expense_id) AS `expense_count`,
						SUM(d.expense_price + (d.expense_price * (d.tax_rate / 100.0))) AS `expense_total`
				  FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
				   AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				   AND e.expense_status = 'Submitted'";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT COUNT(DISTINCT d.expense_id) AS `expense_count`, SUM(d.expense_price) AS `expense_total`
					  FROM expenses e, expense_details d WHERE e.expense_id = d.expense_id
					   AND d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
					   AND e.expense_status = 'Submitted'";
		
		foreach ($this->executeQuery($sql) as $spend_data)
		{
			$metrics['spend_total_pending'] += $spend_data['expense_total'];
			$metrics['spend_count_pending'] += $spend_data['expense_count'];
		}
						
		// orders approved but not yet paid
		$sql = "SELECT SUM(total_price + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count`
				  FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				   AND order_status = 'Paid'";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count`
					  FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
					   AND order_status = 'Paid'";
		
		foreach ($this->executeQuery($sql) as $spend_data)
		{
			$metrics['to_be_paid_amount'] += $spend_data['order_total'];			
			$metrics['to_be_paid_count'] += $spend_data['order_count'];			
		}

		// orders not yet received
		$sql = "SELECT SUM(total_price + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count`
				  FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				   AND order_status IN ( 'Ordered - PO Not Sent To Supplier', 'Ordered - PO Sent To Supplier' )";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `order_total`, COUNT(*) AS `order_count`
					  FROM orders WHERE order_date >= '$start_date' AND order_date <= '$end_date'
					   AND order_status IN ( 'Ordered - PO Not Sent To Supplier', 'Ordered - PO Sent To Supplier' )";

		foreach ($this->executeQuery($sql) as $spend_data)
		{
			$metrics['to_be_received_amount'] += $spend_data['order_total'];			
			$metrics['to_be_received_count'] += $spend_data['order_count'];			
		}

		return $metrics;
	}

	public function getOrderSpendStats($start_date,$end_date)
	{
	    
		 $sql = "SELECT type,sum(total_amount) as total_amount,month_year,month from  view_spend
				 
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				 AND status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')  GROUP by month_year,type
		   ";

		return $this->executeQuery($sql . ' ORDER BY month ASC ');

	}

	public function getOrderSpendCount($start_date,$end_date)
	{
	    $sql = "SELECT count(order_id) as total_orders,DATE_FORMAT(order_date, '%M, %Y') as month_year,DATE_FORMAT(order_date, '%m') as month FROM orders o
		         
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') GROUP by month_year";

		return $this->executeQuery($sql . ' ORDER BY month asc ');

	}

	public function getOrderSpendPendingCount($start_date,$end_date)
	{
	    $sql = "SELECT count(order_id) as total_orders,DATE_FORMAT(order_date, '%M, %Y') as month_year,DATE_FORMAT(order_date, '%m') as month FROM orders o
		         
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND order_status IN ('Submitted') GROUP by month_year";

		return $this->executeQuery($sql . ' ORDER BY month asc ');

	}

	public function getExpenseSpendStats($start_date,$end_date)
	{
		$sql = "SELECT e.*, u.full_name AS user, l.location_name AS location,l.location_id,u.user_id
		          FROM expenses e
				  LEFT JOIN expense_details d ON e.expense_id = d.expense_id
				  LEFT JOIN users u ON e.user_id = u.user_id
				  LEFT JOIN locations l ON e.location_id = l.location_id
				  WHERE d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				  AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')";

		return $this->executeQuery($sql . ' ORDER BY d.expense_date DESC ');
	}

	public function getOrderToApproveStats($start_date,$end_date)
	{
		$sql = "SELECT sum(getOrderTotalInGB(GetOrderTotalAmount(o.order_id),o.currency_id)) as total_amount,DATE_FORMAT(o.order_date, '%M, %Y') as month_year,DATE_FORMAT(o.order_date, '%m') as month FROM orders o
				 
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND order_status = 'Submitted'  GROUP by month_year";

		return $this->executeQuery($sql . ' ORDER BY month ASC ');

	}

	public function getOrderToReceive($start_date,$end_date)
	{
		$sql = "SELECT count(order_id) as total_orders,DATE_FORMAT(order_date, '%M, %Y') as month_year,DATE_FORMAT(order_date, '%m') as month FROM orders o
		         
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND order_status IN ( 'Ordered - PO Not Sent To Supplier', 'Ordered - PO Sent To Supplier' ) GROUP by month_year";

		return $this->executeQuery($sql . ' ORDER BY month asc ');

	}

	public function getOrderSpendToApproveStats($start_date,$end_date)
	{
		$sql = "SELECT o.*, v.vendor_name AS vendor, l.location_name AS location,l.location_id,v.vendor_id
		          FROM orders o
				  LEFT JOIN locations l ON o.location_id = l.location_id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND order_status = 'Submitted'";

		return $this->executeQuery($sql . ' ORDER BY o.order_date DESC ');

	}

	public function getExpenseSpendToApproveStats($start_date,$end_date)
	{
		$sql = "SELECT e.*, u.full_name AS user, l.location_name AS location,l.location_id,u.user_id
		          FROM expenses e
				  LEFT JOIN expense_details d ON e.expense_id = d.expense_id
				  LEFT JOIN users u ON e.user_id = u.user_id
				  LEFT JOIN locations l ON e.location_id = l.location_id
				  WHERE d.expense_date >= '$start_date' AND d.expense_date <= '$end_date'
				  AND e.expense_status = 'Submitted'";

		return $this->executeQuery($sql . ' ORDER BY d.expense_date DESC ');

	}


	public function getOrderSpendToPaidStats($start_date,$end_date)
	{
		$sql = "SELECT o.order_id,sum(getOrderTotalInGB(GetOrderTotalAmount(o.order_id),o.currency_id)) as total_amount,o.order_date, v.vendor_name AS vendor, l.location_name AS location,l.location_id,v.vendor_id
		          FROM orders o
				  LEFT JOIN locations l ON o.location_id = l.location_id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND o.order_status = 'Invoice Received' group by o.order_id";

		//CVarDumper::dump($this->executeQuery($sql . ' ORDER BY o.order_date DESC '),10,1);die;

		return $this->executeQuery($sql . ' ORDER BY o.order_date DESC ');

	}

	public function getOrderSpendToReceivedStats($start_date,$end_date)
	{
		$sql = "SELECT o.*, v.vendor_name AS vendor, l.location_name AS location,l.location_id,v.vendor_id
		          FROM orders o
				  LEFT JOIN locations l ON o.location_id = l.location_id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND o.order_status IN ('Ordered - PO Not Sent To Supplier', 'Ordered - PO Sent To Supplier')";
		return $this->executeQuery($sql . ' ORDER BY o.order_date DESC ');

	}

	public function getOrderAvgSuppliersCategory($start_date,$end_date)
	{
		$sql = "
				SELECT * FROM (
				SELECT COUNT(ed.vendor_id) AS total_vendors,e_type.value as category FROM expense_details ed
				inner join expense_types e_type on ed.expense_type_id=e_type.id
				 WHERE ed.expense_date >= '$start_date' AND ed.expense_date <= '$end_date'
			   	   AND ed.expense_id IN (SELECT expense_id FROM expenses 
			   					 	   WHERE expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed' ) )
			   		GROUP BY e_type.id

				union

				SELECT COUNT(o.vendor_id) AS total_vendors,p.cat_title as category
				  FROM order_details d, orders o, (SELECT inp.*,cat.value as cat_title FROM `products` inp inner join categories cat on inp.category_id= cat.id) p 
				 WHERE d.order_id = o.order_id AND d.product_id = p.product_id
				   AND d.product_type = 'Product'
				   AND o.order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed' )
				   AND o.order_date >= '$start_date' AND o.order_date <= '$end_date'

				GROUP BY p.category_id

				union

				SELECT COUNT(o.vendor_id) AS total_vendors,p.cat_title as category
				  FROM order_details d, orders o, (SELECT inp.*,cat.value as cat_title FROM `products` inp inner join categories cat on inp.category_id= cat.id) p, bundles b, bundle_products bp 
				 WHERE d.order_id = o.order_id AND d.product_id = b.bundle_id 
				   AND d.product_type = 'Bundle' 
				   AND o.order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed' )
				   AND b.bundle_id = bp.bundle_id AND bp.product_id = p.product_id  
				   AND o.order_date >= '$start_date' AND o.order_date <= '$end_date'
				GROUP BY p.category_id
				) AS vendor_category";
		return $this->executeQuery($sql . ' ORDER BY category ASC ');

	}

	/* START: No need for future once all metrices are completed */
	public function getOrderAvgSuppliersCategoryStats($start_date,$end_date)
	{
		$sql = "SELECT o.*, v.vendor_name AS vendor,v.vendor_id,p.product_name,c.id as category_id,c.value as category,COUNT(v.vendor_id) AS vendor_count
		          FROM orders o
				  INNER JOIN vendors v ON o.vendor_id = v.vendor_id
				  INNER JOIN order_details d ON o.order_id = d.order_id
				  INNER JOIN products p ON d.product_id = p.product_id
				  INNER JOIN categories c ON p.category_id = c.id
				  WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				  AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				  GROUP BY v.vendor_id";
		//CVarDumper::dump($this->executeQuery($sql . ' ORDER BY o.order_date DESC '),10,1);die;
		return $this->executeQuery($sql . ' ORDER BY o.order_date DESC ');

	}

	/* END: No need for future once all metrices are completed */

	
	public function getPivotData($from_date, $to_date)
	{
		$location_filter = $department_filter = $vendor_filter = $status_filter = "";
		$tool_currency = Yii::app()->session['user_currency'];

		$return_data = array();
		$sorted_timeframes = array();		
		
		$location_id = $this->location_id;
		$department_id = $this->department_id;
		$order_status = $this->order_status;
		$vendor_id = $this->vendor_id;

		if ($this->location_id) $location_filter = " AND o.location_id = $location_id ";
		if ($this->department_id) $department_filter = " AND o.department_id = $department_id ";
		if ($this->order_status) $status_filter = " AND o.order_status = '$order_status' ";
		if ($this->vendor_id) $vendor_filter = " AND o.vendor_id = $vendor_id ";
		
	    $sql = "
			SELECT 	location_name AS location, 
					department_name AS department, 
					vendor_name AS vendor, 
					c.value AS category,
					CASE WHEN p.subcategory_id !=0 
					THEN (SELECT sub_categories.value FROM sub_categories where sub_categories.id=p.subcategory_id)
					ELSE null
					END AS subcategory,

					CASE WHEN v.industry_id !=0 
					THEN (SELECT industries.value FROM industries where industries.id=v.industry_id)
					ELSE null
					END AS Industry,

					CASE WHEN v.subindustry_id !=0
					THEN (SELECT sub_industries.value FROM sub_industries where sub_industries.id=v.subindustry_id)
					ELSE null
					END AS SubIndustry,

					DATE_FORMAT(order_date, '%b %Y') AS timeframe,
					ROUND(SUM(getTotalInGBandToolCurrency(GetOrderTotalAmount(o.order_id),o.currency_id,'".$tool_currency."')) , 2) AS total
  			FROM 	orders o, locations l, departments d, vendors v, order_details s, products p, categories c
 			WHERE	o.location_id = l.location_id 
   			  AND	o.vendor_id = v.vendor_id 
   			  AND   o.department_id = d.department_id  
   			  AND	o.order_id = s.order_id
   			  AND	s.product_id = p.product_id
   			  AND	p.category_id = c.id
   			  AND	o.order_date >= '$from_date'
   			  AND	o.order_date <= '$to_date'
   			  AND 	o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
			   		$location_filter $department_filter 
			   		$status_filter $vendor_filter
 			GROUP 	BY o.order_id,o.location_id, o.vendor_id, o.department_id, c.id, MONTH(order_date), YEAR(order_date)
		
		";

		foreach ($this->executeQuery($sql) as $row)
		{
			if (!in_array($row['timeframe'], $sorted_timeframes)) 
				$sorted_timeframes[] = $row['timeframe'];
			$return_data[] = $row;
		}

		if ($this->location_id) $location_filter = " AND x.location_id = $location_id ";
		if ($this->department_id) $department_filter = " AND x.department_id = $department_id ";
		if ($this->order_status) $status_filter = " AND x.expense_status = '$order_status' ";
		if ($this->vendor_id) $vendor_filter = " AND e.vendor_id = $vendor_id ";

		$sql = "
			SELECT 	location_name AS location, 
					department_name AS department, 
					vendor_name AS vendor, 
					t.value AS category,
					DATE_FORMAT(expense_date, '%b %Y') AS timeframe,
					ROUND(SUM(getTotalInGBandToolCurrency(getExpenseTotalAmount(x.expense_id),x.currency_id,'".$tool_currency."') ), 2) AS total
  			FROM 	expenses x, locations l, departments d, vendors v, expense_details e, expense_types t
 			WHERE	x.location_id = l.location_id 
   			  AND	e.vendor_id = v.vendor_id 
   			  AND   x.department_id = d.department_id  
   			  AND	e.expense_type_id = t.id
   			  AND	e.expense_date >= '$from_date'
   			  AND	e.expense_date <= '$to_date'
   			  AND 	x.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
			   		$location_filter $department_filter 
			   		$status_filter $vendor_filter
 			GROUP 	BY x.expense_id,x.location_id, e.vendor_id, x.department_id, e.expense_type_id, MONTH(expense_date), YEAR(expense_date)
		
		";

		foreach ($this->executeQuery($sql) as $row)
		{
			if (!in_array($row['timeframe'], $sorted_timeframes)) 
				$sorted_timeframes[] = $row['timeframe'];
			$return_data[] = $row;
		}
		
		uasort($sorted_timeframes, array("Report", "date_compare"));
		return array('data' => $return_data, 'sorted_timeframes' => array_values($sorted_timeframes));
	}

	public function getPivotSavingData($from_date, $to_date)
	{
	   $location_filter = $department_filter = $vendor_filter = $user_filter = $status_filter = $currency_id_filter = $currency = "";
		
		$return_data = array();
		$sorted_timeframes = array();		
		$location_id = $this->location_id;
		$department_id= $this->department_id;
		$order_status = $this->order_status;
		$vendor_id = $this->vendor_id;
		$currency_id  = $this->currency_id;
		$user_id = $this->user_id;
		
		if (!empty($location_id) && !($location_id === [0])) $location_filter = " AND sav.location_id IN (".implode(',',$location_id).") ";
		if (!empty($department_id) && !($department_id === [0])) $department_filter = " AND sav.department_id IN (".implode(',',$department_id).") ";
		if ($this->order_status) $status_filter = " AND sav.status = '$order_status' ";
		if (!empty($vendor_id)) $vendor_filter = " AND sav.vendor_id = $vendor_id ";
		if ($this->user_id) $user_filter = " AND sav.user_id = $user_id ";
		if (!empty($currency_id)){
		 	$currency = FunctionManager::showCurrencySymbol('',$currency_id);
		 	$currencyFilter = " AND sav.currency_id=$currency_id ";
		}
	
		$from_date = date("Y-m-d",strtotime("-1 years"));
        $to_date = date("Y-m-d",strtotime("+1 years"));

        /* Start: currency conversion commented */
	    $sql=" SELECT location_name AS location, 
			department_name AS department, 
			vendor_name AS vendor, 
			c.value AS category,
			sc.value AS subcategory,
			DATE_FORMAT(sav.due_date, '%b %Y') AS timeframe,
			sum(currencyConversion(sm.realised_cost_avoidance,sav.currency_rate,sav.currency_id,'".$currency."'))+
			sum(currencyConversion(sm.realised_cost_reduction,sav.currency_rate,sav.currency_id,'".$currency."')) as 'Realised Savings',
			sum(currencyConversion(sm.cost_avoidance,sav.currency_rate,sav.currency_id,'".$currency."'))+sum(currencyConversion(sm.cost_reduction,sav.currency_rate,sav.currency_id,'".$currency."')) as 'Projected Savings',
			sum(currencyConversion(sm.base_line_spend,sav.currency_rate,sav.currency_id,'".$currency."')) as 'Baseline Spend',
			sum(currencyConversion(sm.realised_cost_avoidance,sav.currency_rate,sav.currency_id,'".$currency."'))+
			sum(currencyConversion(sm.realised_cost_reduction,sav.currency_rate,sav.currency_id,'".$currency."'))+
			sum(currencyConversion(sm.cost_avoidance,sav.currency_rate,sav.currency_id,'".$currency."'))+
			sum(currencyConversion(sm.cost_reduction,sav.currency_rate,sav.currency_id,'".$currency."'))+
			sum(currencyConversion(sm.base_line_spend,sav.currency_rate,sav.currency_id,'".$currency."'))+
			sum(currencyConversion(sm.cost_reduction,sav.currency_rate,sav.currency_id,'".$currency."'))+
			sum(currencyConversion(sm.cost_avoidance,sav.currency_rate,sav.currency_id,'".$currency."')) AS total
  			FROM savings sav 
  			INNER JOIN saving_milestone sm on  sm.saving_id=sav.id 
  			LEFT JOIN  departments d on  sav.department_id = d.department_id
  			LEFT JOIN  saving_status s_status on  sav.status = s_status.id  
  			LEFT JOIN  categories c on  sav.category_id = c.id
  			LEFT JOIN  locations l on sav.location_id = l.location_id
  			LEFT JOIN  sub_categories sc on sav.subcategory_id = sc.id
  			LEFT JOIN  vendors v on sav.vendor_id = v.vendor_id 
  			LEFT JOIN currency_rates cr ON sav.currency_id = cr.id
  			   
 			WHERE sm.due_date >= '$from_date'
   			  AND sm.due_date <= '$to_date' AND sav.saving_archive = 'no' 
   			  $location_filter $department_filter 
			  $status_filter $vendor_filter $user_filter
 			GROUP BY sav.department_id,sav.category_id, timeframe,sav.location_id,sav.subcategory_id,sav.vendor_id
		";

		//End: currency conversion commented 

	/*	 $sql = "
			SELECT 	location_name AS location, 
			department_name AS department, 
			vendor_name AS vendor, 
			c.value AS category,
			sc.value AS subcategory,
			
			DATE_FORMAT(sav.due_date, '%b %Y') AS timeframe,
			sum(sm.realised_cost_avoidance)+sum(sm.realised_cost_reduction) as 'Realised Savings',
			sum(sm.cost_avoidance)+sum(sm.cost_reduction) as 'Projected Savings',
			sum(sm.base_line_spend) as 'Baseline Spend',
			sum(sm.cost_reduction) as 'Projected Cost Reduction',
			sum(sm.cost_avoidance) as 'Projected Cost Avoidance',
			sum(sm.realised_cost_avoidance)+sum(sm.realised_cost_reduction)
			+
			sum(sm.cost_avoidance)+sum(sm.cost_reduction)
			+
			sum(sm.base_line_spend)
			+
			sum(sm.cost_reduction)
			+
			sum(sm.cost_avoidance)
				AS total

  			FROM 	savings sav 
  			INNER JOIN saving_milestone sm on  sm.saving_id=sav.id 
  			LEFT JOIN  departments d on  sav.department_id = d.department_id  
  			LEFT JOIN  categories c on  sav.category_id = c.id
  			LEFT JOIN  locations l on sav.location_id = l.location_id
  			LEFT JOIN  sub_categories sc on sav.subcategory_id = sc.id
  			LEFT JOIN  vendors v on sav.vendor_id = v.vendor_id 
  			   
 			WHERE sm.due_date >= '$from_date'
   			  AND	sm.due_date <= '$to_date'
   			   
  		 
 			GROUP 	BY sav.department_id,sav.category_id, timeframe,sav.location_id,sav.subcategory_id,sav.vendor_id
		
		";
*/
		foreach ($this->executeQuery($sql) as $row)
		{
			if (!in_array($row['timeframe'], $sorted_timeframes)) 
				$sorted_timeframes[] = $row['timeframe'];
			$return_data[] = $row;
		}

		uasort($sorted_timeframes, array("Report", "date_compare"));
		return array('data' => $return_data, 'sorted_timeframes' => array_values($sorted_timeframes));
	}

	//
	public function getPivotSavingDatausg($from_date, $to_date)
	{
		
		$location_filter = $department_filter = $vendor_filter = $status_filter = $currency_id_filter = $currency = "";
		//$currency = Yii::app()->session['user_currency'];
		$return_data = array();
		$sorted_timeframes = array();		
		$location_id = $this->location_id;
		$department_id = $this->department_id;
		$order_status = $this->order_status;
		//$vendor_id = $this->vendor_id;
		$currency_id = $this->currency_id;
		$user_id 	 = $this->user_id;
		$savingStatus = FunctionManager::savingStatusIgnore();

		if ($this->location_id) $location_filter = " AND sav.location_id = $location_id ";
		if ($this->department_id) $department_filter = " AND sav.department_id = $department_id ";
		if ($this->order_status) $status_filter = " AND sav.status = '$order_status' ";
		//if ($this->vendor_id) $vendor_filter = " AND sav.vendor_id = $vendor_id ";
		if ($this->user_id) $user_filter = " AND sav.user_id = $user_id ";
		if (!empty($currency_id)){
		 	$currency = FunctionManager::showCurrencySymbol('',$currency_id);
		 	$currencyFilter = " AND sav.currency_id=$currency_id ";
		}
		
        /* Start: currency conversion commented */
	    $sql = "
			SELECT 	location_name AS `Location`, 
			department_name AS `Department`,
			c.value AS `Category`,
			sc.value AS `Sub Category`,
			sav.user_name AS `Username`,
			DATE_FORMAT(f.field_name, '%Y/%m/%d') AS `month`,
			DATE_FORMAT(f.field_name, '%Y') AS `Year`,
			sav.id as `Savings Initiative ID`,
			sav.title as `Saving Initiative Name`,

			sum(currencyConversion(total_realised_savings,sav.currency_rate,sav.currency_id,'".$currency."')) as 'Realised Savings',
			sum(currencyConversion(f.field_value,sav.currency_rate,sav.currency_id,'".$currency."')) as 'Forecasted Savings',
			sum(currencyConversion(f.field_value,sav.currency_rate,sav.currency_id,'".$currency."')) - sum(currencyConversion(total_realised_savings,sav.currency_rate,sav.currency_id,'".$currency."')) 
			as 'Planned Savings',

				SUM(CASE When sav.saving_type = 1 Then 
				currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Cost Reduction - Realized',
				SUM(CASE When sav.saving_type = 1 Then 
				currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Cost Reduction - Forecasted',
				SUM(CASE When sav.saving_type = 2  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
				else 0 End ) as 'Cost Avoidance - Realized',
				SUM(CASE When sav.saving_type = 2  Then currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Cost Avoidance - Forecasted',
				SUM(CASE When sav.saving_type = 3  Then currencyConversion(total_realised_savings,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Cost Containment - Realized',
				SUM(CASE When sav.saving_type = 3  Then currencyConversion(field_value,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Cost Containment - Forecasted',

				SUM(CASE When sav.saving_area = 3  Then
				currencyConversion(total_project_saving,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Indirect Materials',
				SUM(CASE When sav.saving_area = 4  Then
				currencyConversion(total_project_saving,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Direct Materials',
				SUM(CASE When sav.saving_area = 5  Then
				currencyConversion(total_project_saving,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Capital/Mobile',
				SUM(CASE When sav.saving_area = 6  Then
				currencyConversion(total_project_saving,currency_rate,currency_id,'".$currency."')
				Else 0 End ) as 'Energy',
				
				sum(currencyConversion(f.field_value,sav.currency_rate,sav.currency_id,'".$currency."'))
				AS `Total`
				
  			FROM savings sav 
  			LEFT JOIN milestone_field f on  sav.id=f.saving_id
  			LEFT JOIN  departments d on  sav.department_id = d.department_id
  			LEFT JOIN  saving_status s_status on  sav.status = s_status.id  
  			LEFT JOIN  categories c on  sav.category_id = c.id
  			LEFT JOIN  locations l on sav.location_id = l.location_id
  			LEFT JOIN  sub_categories sc on sav.subcategory_id = sc.id
  			LEFT JOIN currency_rates cr ON sav.currency_id = cr.id
  			
 			WHERE sav.status not in(".$savingStatus.")  AND ((f.field_name >= '$from_date' 
   			  AND f.field_name <= '$to_date' ) OR f.id is null ) 
   			  $location_filter $department_filter 
			  $status_filter $vendor_filter $user_filter 

 			GROUP BY sav.id,sav.department_id,sav.category_id, 
			CASE 
				WHEN f.id IS NOT NULL THEN f.field_name 
				ELSE '' 
			END,sav.location_id,sav.subcategory_id,sav.vendor_id";
		//End: currency conversion commented 
		foreach ($this->executeQuery($sql) as $row)
		{
			if (!in_array($row['month'], $sorted_timeframes)) 
				$sorted_timeframes[] = $row['month'];
			$return_data[] = $row;
		}
		
		uasort($sorted_timeframes, array("Report", "date_compare"));
		return array('data' => $return_data, 'sorted_timeframes' => array_values($sorted_timeframes));
	}
	//
	

	static function date_compare($a, $b)
	{
	    $c1 = strtotime("1 " . $a);
	    $c2 = strtotime("1 " . $b);
    	if ($c1 == $c2) return 0;
    	else return ($c1 > $c2) ? 1 : -1;
	}    
	
	public function getInvoicePaymentStatistics($start_date, $end_date)
	{
		$invoice_statistics = array('paid' => 0, 'unpaid' => 0);
		
		$sql = "SELECT COUNT(*) AS invoice_count FROM orders 
				 WHERE order_date >= '$start_date' 
				   AND order_date <= '$end_date'
				   AND order_status IN ( 'Invoice Received', 'Received', 'Ordered - PO Not Sent To Supplier' ) ";
		$sql = "SELECT COUNT(*) AS invoice_count FROM orders 
				 WHERE order_date >= '$start_date' 
				   AND order_date <= '$end_date'
				   AND order_status = 'Invoice Received' ";
		foreach ($this->executeQuery($sql) as $invoice_data)
			$invoice_statistics['unpaid'] = $invoice_data['invoice_count'];

		$sql = "SELECT COUNT(*) AS invoice_count FROM orders 
				 WHERE order_date >= '$start_date' 
				   AND order_date <= '$end_date'
				   AND order_status IN ( 'Paid') ";
		$sql = "SELECT COUNT(*) AS invoice_count FROM orders 
				 WHERE order_date >= '$start_date' 
				   AND order_date <= '$end_date'
				   AND order_status = 'Paid' ";
		foreach ($this->executeQuery($sql) as $invoice_data)
			$invoice_statistics['paid'] = $invoice_data['invoice_count'];

		//CVarDumper::dump($invoice_statistics,10,1);die;
		
		return $invoice_statistics;
	}

	public function getLocationSpendStatistics($start_date, $end_date)
	{
		$location_statistics = array();
		
		$sql = "SELECT location_id, SUM(total_price + other_charges - discount) AS total_amount FROM orders 
				 WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				   AND order_status IN ('Pending','Submitted')
				 GROUP BY location_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT location_id, SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS total_amount FROM orders 
					 WHERE order_date >= '$start_date' AND order_date <= '$end_date'
					   AND order_status IN ('Pending','Submitted')
					 GROUP BY location_id";
					
		foreach ($this->executeQuery($sql) as $spend_data)
		{
			if (!isset($location_statistics[$spend_data['location_id']]))
			{
				$location_statistics[$spend_data['location_id']] = array();
				$location_statistics[$spend_data['location_id']]['spend_total'] = 0;
				$location_statistics[$spend_data['location_id']]['spend_future'] = 0;
			}
			$location_statistics[$spend_data['location_id']]['spend_future'] = $spend_data['total_amount'];
		}

		$sql = "SELECT location_id, SUM(total_price + other_charges - discount) AS total_amount FROM orders 
				 WHERE order_date >= '$start_date' AND order_date <= '$end_date'
				   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
				 GROUP BY location_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT location_id, SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS total_amount FROM orders 
					 WHERE order_date >= '$start_date' AND order_date <= '$end_date'
					   AND order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					 GROUP BY location_id";
				 
		foreach ($this->executeQuery($sql) as $spend_data)
		{
			if (!isset($location_statistics[$spend_data['location_id']]))
			{
				$location_statistics[$spend_data['location_id']] = array();
				$location_statistics[$spend_data['location_id']]['spend_total'] = 0;
				$location_statistics[$spend_data['location_id']]['spend_future'] = 0;
			}
			$location_statistics[$spend_data['location_id']]['spend_total'] = $spend_data['total_amount'];
		}
		
		// need location names too
		$locations = array();
		foreach ($this->executeQuery("SELECT * FROM locations") as $location_data)
			$locations[$location_data['location_id']] = $location_data['location_name'];
		
		foreach ($location_statistics as $location_id => $location_data)
		{
			if (!isset($location_data['spend_total'])) $location_data['spend_total'] = 0;
			if (!isset($location_data['spend_future'])) $location_data['spend_future'] = 0;

			$location_data['location_name'] = 'N/A';
			if (isset($locations[$location_id])) $location_data['location_name'] = $locations[$location_id];
			
			$location_statistics[$location_id] = $location_data;
		}

		usort($location_statistics, function($a, $b) 
		{
			 return ($b['spend_total'] + $b['spend_future']) - ($a['spend_total'] + $a['spend_future']); 
		});		
		return $location_statistics;
	}

	public function exportMonthlySpend()
	{
		$order_data = array();
		$sql = "SELECT o.order_id, o.order_date,location_name, department_name, vendor_name, full_name,
						DATE_FORMAT(order_date, '%d-%b-%Y') AS order_date, description,cr.currency, order_status,
						od.quantity,od.unit_price,tax,shipping_cost, (od.unit_price*quantity+tax+shipping_cost) as total_price,od.product_id,p.product_name
				  FROM orders o
				  LEFT JOIN locations l ON o.location_id = l.location_id
				  LEFT JOIN departments d ON o.department_id = d.department_id
				  LEFT JOIN currency_rates cr ON o.currency_id = cr.id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN order_details od ON o.order_id = od.order_id
				  LEFT JOIN products p ON od.product_id = p.product_id
				  ORDER BY year(o.order_date)";
		foreach ($this->executeQuery($sql) as $order) $order_data[$order['order_id']] = $order;


		$expense_data_with_details = $expense_data = array();
		$sql1 = "SELECT expense_id, expense_name, location_name, department_name, full_name,
						DATE_FORMAT(e.created_datetime, '%d-%b-%Y') AS expense_date, description, total_expenses, expense_status
				  FROM expenses e
				  LEFT JOIN locations l ON e.location_id = l.location_id
				  LEFT JOIN departments d ON e.department_id = d.department_id
				  LEFT JOIN users u ON e.user_id = u.user_id
				 ORDER BY year(e.created_datetime)";
		foreach ($this->executeQuery($sql1) as $expense) $expense_data[$expense['expense_id']] = $expense;

		if (count($expense_data))
		{
			$expense_details = array();
			$sql = "SELECT d.*, e.value AS expense_type, vendor_name,
							DATE_FORMAT(expense_date, '%d-%b-%Y') AS expense_date
					  FROM expense_details d, expense_types e, vendors v
					 WHERE d.expense_type_id = e.id AND d.vendor_id = v.vendor_id
					   AND expense_id IN ( " . implode(",", array_keys($expense_data)) . " ) ";
			foreach ($this->executeQuery($sql) as $expense_item)
			{
				if (!isset($expense_details[$expense_item['expense_id']]))
					$expense_details[$expense_item['expense_id']] = array();
				$expense_details[$expense_item['expense_id']][] = $expense_item;
			}

			foreach ($expense_data as $expense_id => $expense)
			{
				$details = array();
				if (isset($expense_details[$expense_id]))
					foreach ($expense_details[$expense_id] as $expense_item)
						$details[] = array('expense_type' => $expense_item['expense_type'],
							'supplier' => $expense_item['vendor_name'],
							'expense_date' => $expense_item['expense_date'],
							'expense_amount' => $expense_item['expense_price'],
							'tax_rate' => $expense_item['tax_rate'],
							'comments' => $expense_item['expense_notes']);
				$expense['details'] = $details;
				$expense_data_with_details[] = $expense;
			}
		}

		//CVarDumper::dump($expense_data_with_details,10,1);die;

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Create a first sheet, representing sales data
		$objPHPExcel->setActiveSheetIndex(0);
		//$arrHeader = array('Name', 'Mobile');
		//$objPHPExcel->getActiveSheet()->fromArray($arrHeader,'','A1');
		$objPHPExcel->getActiveSheet()->fromArray($order_data);
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('orders');
		// Create a new worksheet, after the default sheet
		$objPHPExcel->createSheet();
		// Add some data to the second sheet, resembling some different data types
		$objPHPExcel->setActiveSheetIndex(1);
		//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'More data');
		$objPHPExcel->getActiveSheet()->fromArray($expense_data);
		// Rename 2nd sheet
		$objPHPExcel->getActiveSheet()->setTitle('Expense');
		// Redirect output to a client’s web browser (Excel5)

		// START: Fixed Excel download  3/15/2019
		error_reporting(0);
		ob_start();
		// END: Fixed Excel download  3/15/2019
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Monthly-Spend.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		ob_clean();
		$objWriter->save('php://output');die;
	}


	public function exportTopSuppliers()
	{
		$order_data = array();
		$sql = "SELECT v.vendor_id as id,case
											when preferred_flag = 1
											then 'Preferred'
											when preferred_flag = 0
											then 'Non-Preferred'
											else null
										  end AS vendor_type
                        ,vendor_name, i.value AS industry_name, s.value AS subindustry_name,
						address_1, address_2, city, state, zip, country,tax_number,contact_name,
						external_id AS company_number, phone_1 phone_2,fax,website,emails,sm.value as shipping_method,st.value as shipping_term,pt.value as payment_term,comments, COUNT(DISTINCT o.order_id) AS `count`,
					   SUM(o.total_price + o.other_charges - o.discount) AS `total`
					   FROM orders o
					   LEFT JOIN order_details d ON o.order_id = d.order_id
					   LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
					   LEFT JOIN industries i ON v.industry_id = i.id
					   LEFT JOIN sub_industries s ON v.subindustry_id = s.id
					   LEFT JOIN payment_terms pt ON v.payment_term_id = pt.id
					   LEFT JOIN shipping_methods sm ON v.shipping_method_id = sm.id
					   LEFT JOIN shipping_terms st ON v.shipping_method_id = st.id
					   WHERE  o.order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY v.vendor_id ORDER BY total DESC
					   LIMIT 10";
		foreach ($this->executeQuery($sql) as $order) $order_data[$order['id']] = $order;

		$expense_data = array();
		$sql1 = "SELECT v.vendor_id as id,case
											when preferred_flag = 1
											then 'Preferred'
											when preferred_flag = 0
											then 'Non-Preferred'
											else null
										  end AS vendor_type
                        ,vendor_name, i.value AS industry_name, s.value AS subindustry_name,
						address_1, address_2, city, state, zip, country,tax_number,contact_name,
						external_id AS company_number, phone_1 phone_2,fax,website,emails,sm.value as shipping_method,st.value as shipping_term,pt.value as payment_term,comments, COUNT(DISTINCT e.expense_id) AS `count`,
					   SUM(e.total_expenses) AS `total`
					   FROM expenses e
					   LEFT JOIN expense_details d ON e.expense_id = d.expense_id
					   LEFT JOIN vendors v ON d.vendor_id = v.vendor_id
					   LEFT JOIN industries i ON v.industry_id = i.id
				       LEFT JOIN sub_industries s ON v.subindustry_id = s.id
				       LEFT JOIN payment_terms pt ON v.payment_term_id = pt.id
				       LEFT JOIN shipping_methods sm ON v.shipping_method_id = sm.id
				       LEFT JOIN shipping_terms st ON v.shipping_method_id = st.id
					   WHERE e.expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY v.vendor_id ORDER BY total DESC
					   LIMIT 10";
		foreach ($this->executeQuery($sql1) as $expense) $expense_data[$expense['id']] = $expense;

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Create a first sheet, representing sales data
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->fromArray($order_data);
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('orders');
		// Create a new worksheet, after the default sheet
		$objPHPExcel->createSheet();
		// Add some data to the second sheet, resembling some different data types
		$objPHPExcel->setActiveSheetIndex(1);
		//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'More data');
		$objPHPExcel->getActiveSheet()->fromArray($expense_data);
		// Rename 2nd sheet
		$objPHPExcel->getActiveSheet()->setTitle('Expense');
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Suppliers.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

		//parent::export($this->executeQuery($sql), 'Years-Monthly');die;
	}


	public function exportTopLocations($start_date, $end_date)
	{
		$order_data = array();
		$sql = "SELECT o.order_id,o.order_date, location_name, department_name, vendor_name, full_name,
						DATE_FORMAT(order_date, '%d-%b-%Y') AS order_date, description,cr.currency, order_status,
						od.quantity,od.unit_price,tax,shipping_cost, (od.unit_price*quantity+tax+shipping_cost) as total_price,od.product_id,p.product_name, COUNT(DISTINCT o.order_id) AS `count`,
					   SUM(o.total_price + o.other_charges - o.discount) AS `total`
					   FROM orders o
					   LEFT JOIN locations l ON o.location_id = l.location_id
					   LEFT JOIN departments d ON o.department_id = d.department_id
					   LEFT JOIN currency_rates cr ON o.currency_id = cr.id
					   LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
					   LEFT JOIN users u ON o.user_id = u.user_id
					   LEFT JOIN order_details od ON o.order_id = od.order_id
					   LEFT JOIN products p ON od.product_id = p.product_id
					   WHERE order_date >= '$start_date'
				       AND order_date <= '$end_date'
					   AND o.order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY l.location_id ORDER BY total DESC
					   LIMIT 10";
		foreach ($this->executeQuery($sql) as $order) $order_data[$order['order_id']] = $order;

		$expense_data = array();
		$sql1 = "SELECT  e.expense_id, expense_name, location_name, department_name, full_name,
						DATE_FORMAT(e.created_datetime, '%d-%b-%Y') AS expense_date, description, total_expenses, expense_status, COUNT(DISTINCT e.expense_id) AS `count`,
					   SUM(e.total_expenses) AS `total`
					   FROM expenses e
					   LEFT JOIN expense_details ed ON e.expense_id = ed.expense_id
				       LEFT JOIN locations l ON e.location_id = l.location_id
				       LEFT JOIN departments d ON e.department_id = d.department_id
				       LEFT JOIN users u ON e.user_id = u.user_id
					   WHERE ed.expense_date >= '$start_date'
					   AND ed.expense_date <= '$end_date'
					   AND e.expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY l.location_id ORDER BY total DESC
					   LIMIT 10";
		foreach ($this->executeQuery($sql1) as $expense) $expense_data[$expense['expense_id']] = $expense;

		//CVarDumper::dump($expense_data,10,1);die;
		//ob_​start();
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Create a first sheet, representing sales data
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->fromArray($order_data);
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('orders');
		// Create a new worksheet, after the default sheet
		$objPHPExcel->createSheet();
		// Add some data to the second sheet, resembling some different data types
		$objPHPExcel->setActiveSheetIndex(1);
		//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'More data');
		$objPHPExcel->getActiveSheet()->fromArray($expense_data);
		// Rename 2nd sheet
		$objPHPExcel->getActiveSheet()->setTitle('Expense');
		// Redirect output to a client’s web browser (Excel5)
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Locations.xls"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
		exit;
	}

	public function contractByStatus(){

		$days30         = strtotime('+ 30 days');
	    $months3        = strtotime('+ 3 months');
	    $dateDays30     = date('Y-m-d', $days30);
	    $months3        = date('Y-m-d', $months3);
	    $currentDate    = date("Y-m-d");

		$sql = "select 
            SUM(CASE 
             WHEN contract_end_date>'".$months3."' THEN 1
             ELSE 0
            END) AS 'End Date Over 3 Months',
            SUM(CASE 
             WHEN contract_end_date>'".$dateDays30."' and contract_end_date<='".$months3."' THEN 1
             ELSE 0
            END) AS 'End Date Within 3 Months',
            SUM(CASE 
             WHEN contract_end_date>='".$currentDate."' and contract_end_date<='".$dateDays30."' THEN 1
             ELSE 0
            END) AS 'End Date Within 30 Days',
            SUM(CASE 
			 WHEN contract_end_date<'".$currentDate."' THEN 1
             ELSE 0
            END) AS 'Passed Contract End Date'
            from contracts  where contract_archive is null order by 'Active'";

		$contractCount = Yii::app()->db->createCommand($sql)->queryRow();
		$data =array();
		$data['contract_count']   = array($contractCount['End Date Over 3 Months'],$contractCount['End Date Within 3 Months'],$contractCount['End Date Within 30 Days'],$contractCount['Passed Contract End Date']);
    	$data['contract_label']   = array_keys($contractCount);
    	$data['contract_color']   = array('#48d6a8','#efa65f','#f7778c','#aaa');

		return $data;
	}

	public function getVendorAvgPerformance(){

		$sql = "select
		SUM( CASE WHEN  avg_score*10<=10                      THEN 1 ELSE 0 END) '0-10%',
		SUM( CASE WHEN  avg_score*10>10 and  avg_score*10<=20 THEN 1 ELSE 0 END) as '11%-20%',
		SUM( CASE WHEN  avg_score*10>20 and  avg_score*10<=30 THEN 1 ELSE 0 END) as '21%-30%',
		SUM( CASE WHEN  avg_score*10>30 and  avg_score*10<=40 THEN 1 ELSE 0 END) as '31%-40%',
		SUM( CASE WHEN  avg_score*10>40 and  avg_score*10<=50 THEN 1 ELSE 0 END) as '41%-50%',
		SUM( CASE WHEN  avg_score*10>50 and  avg_score*10<=60 THEN 1 ELSE 0 END) as '51%-60%',
		SUM( CASE WHEN  avg_score*10>60 and  avg_score*10<=70 THEN 1 ELSE 0 END) as '61%-70%',
		SUM( CASE WHEN  avg_score*10>70 and  avg_score*10<=80 THEN 1 ELSE 0 END) as '71%-80%',
		SUM( CASE WHEN  avg_score*10>80 and  avg_score*10<=90 THEN 1 ELSE 0 END) as '81%-90%',
		SUM( CASE WHEN  avg_score*10>90 THEN 1 ELSE 0 END) as '91%-100%'
		from vendors ven  
		left join (select sum(score_value)/count(id) as avg_score,vendor_id from vendor_performance group by vendor_id ) as per on ven.vendor_id=per.vendor_id where (ven.vendor_archive is null or ven.vendor_archive = '') ";
		$avgPerformanceReader = Yii::app()->db->createCommand($sql)->queryRow();
		return $avgPerformanceReader;
	 //   echo "<pre>";print_r($avgPerformanceReader);
		// exit;
		
	}

	public function getEvaluationTopTenCategory(){
		$sql ="select count(mqs.id) as quick_evaluations, c.value as category_name from main_quick_scoring mqs 
         inner join categories as c on mqs.category_id = c.id group BY category_name order by quick_evaluations DESC LIMIT 10 ";
        $evaluationCateReader = Yii::app()->db->createCommand($sql)->queryAll();
         return $evaluationCateReader;
     }
		
}
?>
