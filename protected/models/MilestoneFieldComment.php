<?php

class MilestoneFieldComment extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' 						=> 'N',
			'milestone_field_id'		=> 'N',
			'user_id'					=> 'N',
			'user_name'				    => 'C',
			'comment'				    => 'C',
			'datetime'	                => 'D',
		);
		parent::__construct('id', ' milestone_field_comments');
		$this->timestamp = false;
	}

}
