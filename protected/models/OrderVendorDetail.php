<?php

class OrderVendorDetail extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'order_vendor_details');
		$this->timestamp = false;
	}

	public function getOrderVendorDetails($id)
	{
		return $this->executeQuery("SELECT * FROM order_vendor_details WHERE order_id = $id",1);

	}


}
