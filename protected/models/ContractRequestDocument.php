<?php

class ContractRequestDocument extends Common
{
	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'vendor_id' => 'N',
			'contract_id' =>'N',
			'user_id' => 'N',
			'user_name' => 'C',
			'contract_type' => 'N',
			'document_type_name'=>'C',
			'supplier_contact_name' => 'C',
			'supplier_contact_email' => 'C',
			'note'=>'C',
			'status' => 'C',
			'reference' =>'C',
			'created_at' => 'D'
		);
		parent::__construct('id', 'contract_request_documents');
		$this->timestamp = false;
	}
}
