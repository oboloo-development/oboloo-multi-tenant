<?php

class BundleProduct extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'bundle_id' => 'N',
			'product_id' => 'N',
			'quantity' => 'N'
		);
		parent::__construct('id', 'bundle_products');
		$this->timestamp = false;
	}
}
