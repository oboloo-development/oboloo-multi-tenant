<?php
require_once('protected/vendors/phpexcel/PHPExcelReader/excel_reader2.php');
require_once('protected/vendors/phpexcel/PHPExcelReader/SpreadsheetReader.php');
class Vendor extends Common
{

    public function __construct()
    {
        $this->fields = array(
            'vendor_id' => 'N',
            'user_id' => 'N',
            'active'=>'C',
            'vendor_name' => 'C',
            'vendor_desc' => 'C',
            'preferred_flag' => 'N',
            'approver_id' => 'N',
            'industry_id' => 'L',
            'subindustry_id' => 'L',
            'address_1' => 'C',
            'address_2' => 'C',
            'city' => 'C',
            'state' => 'C',
            'zip' => 'C',
            'country' => 'C',
            'number' => 'C',
            'tax_number' => 'C',
            'contact_name' => 'C',
            'quick_created'=>'C',
            'phone_1' => 'C',
            'phone_2' => 'C',
            'fax' => 'C',
            'website' => 'C',
            'emails' => 'C',
            //'profile_status'=>'C',
            'approver_status'=>'C',
            'vendor_archive' => 'C',
            'comments' => 'C',
            'external_id' => 'C',
            'payment_term_id' => 'N',
            'shipping_method_id' => 'N',
            'shipping_term_id' => 'N'
        );
        $this->alternate_key_name = 'vendor_name';
        $this->currency_fields = array(
            'total','total_price'
        );
        parent::__construct('vendor_id', 'vendors');
    }

    public function getVendors($vendor_id = 0, $start = 0, $length = 10, $search_for = "", $order_by = array(), $industry_id = 0, $subindustry_id = 0, $preferred_flag = -1,$supplier_status="",$tableName='vendors',$archive='', $location_id, $department_id)
    {
        $currentDate = date('Y-m-d');
        $sql = "SELECT v.*, i.value AS industry, s.value as subindustry,con.contract_id,((nt*100/(nt+zs+se))-(zs*100/(nt+zs+se))) as net_score,avg_score, vdoc.total_pending,vdoc.expired_document,v_risk.risk_score_value,v_sustainability.score_value
		          FROM $tableName v
				  LEFT JOIN industries i ON v.industry_id = i.id
				  LEFT JOIN sub_industries s ON v.subindustry_id = s.id
                  LEFT JOIN vendor_location vl ON v.vendor_id = vl.vendor_id
				  LEFT JOIN vendor_department vd ON v.vendor_id = vd.vendor_id
                  LEFT JOIN ( select vendor_id,
                 
                sum(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END) as total_pending,
                sum(CASE WHEN expiry_date <'".$currentDate."' THEN 1 ELSE 0 END) as expired_document

                from vendor_documents where vendor_documents.archive !=1 group by vendor_id) vdoc ON v.vendor_id = vdoc.vendor_id

                LEFT JOIN (select count(inn_con.contract_id) as contract_id ,inn_con.vendor_id from contracts as inn_con where inn_con.contract_archive ='' or inn_con.contract_archive is null group by inn_con.vendor_id) con ON v.vendor_id = con.vendor_id

                LEFT JOIN (
                    SELECT
                    SUM(CASE WHEN score_value>=9 and score_value<=10 THEN 1 ELSE 0 END) AS nt,
                    SUM(CASE WHEN score_value>=0 and score_value<=6 THEN 1 ELSE 0 END) as zs,
                    SUM(CASE WHEN score_value>=7 and score_value<=8 THEN 1 ELSE 0 END) se,vendor_id,
                    sum(score_value)/count(id) as avg_score from vendor_performance where 1 group by vendor_id) as v_per on v.vendor_id=v_per.vendor_id

                LEFT JOIN (select * from vendor_risk where id=(select max(in_vr.id) from vendor_risk in_vr where in_vr.vendor_id=vendor_risk.vendor_id) group by vendor_id) as v_risk on v.vendor_id=v_risk.vendor_id
            LEFT JOIN (select * from vendor_sustainability where id=(select max(in_vr.id) from vendor_sustainability in_vr where in_vr.vendor_id=vendor_sustainability.vendor_id) group by vendor_id) as v_sustainability on v.vendor_id=v_sustainability.vendor_id
          ";
         
        if (!empty($vendor_id))
            return $this->executeQuery($sql . " WHERE v.vendor_id = $vendor_id", true);
        else {
            if (! empty($search_for)) {
              $sql = "SELECT v.*, i.value AS industry, s.value as subindustry,con.contract_id,((nt*100/(nt+zs+se))-(zs*100/(nt+zs+se))) as net_score,avg_score,vdoc.total_pending,vdoc.expired_document,v_risk.risk_score_value,v_sustainability.score_value,vl.location_id as location_id, vd.department_id as department_id
				          FROM $tableName v
						  LEFT JOIN industries i ON v.industry_id = i.id
						  LEFT JOIN sub_industries s ON v.subindustry_id = s.id
						  LEFT JOIN vendor_location vl ON v.vendor_id = vl.vendor_id
						  LEFT JOIN vendor_department vd ON v.vendor_id = vd.vendor_id
                          LEFT JOIN ( select vendor_id,
                 
                sum(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END) as total_pending,
                sum(CASE WHEN expiry_date <'".$currentDate."' THEN 1 ELSE 0 END) as expired_document

                from vendor_documents group by vendor_id) vdoc ON v.vendor_id = vdoc.vendor_id
              
              LEFT JOIN (select * from vendor_risk where id=(select max(in_vr.id) from vendor_risk in_vr where in_vr.vendor_id=vendor_risk.vendor_id) group by vendor_id) as v_risk on v.vendor_id=v_risk.vendor_id

              LEFT JOIN (select * from vendor_sustainability where id=(select max(in_vr.id) from vendor_sustainability in_vr where in_vr.vendor_id=vendor_sustainability.vendor_id) group by vendor_id) as v_sustainability on v.vendor_id=v_sustainability.vendor_id

              LEFT JOIN (select count(inn_con.contract_id) as contract_id,inn_con.vendor_id from contracts as inn_con group by inn_con.vendor_id) con ON v.vendor_id = con.vendor_id

              LEFT JOIN (
            SELECT
            SUM(CASE WHEN score_value>=9 and score_value<=10 THEN 1 ELSE 0 END) AS nt,SUM(CASE WHEN score_value>=0 and score_value<=6 THEN 1 ELSE 0 END) as zs,SUM(CASE WHEN score_value>=7 and score_value<=8 THEN 1 ELSE 0 END) se,vendor_id,sum(score_value)/count(id) as avg_score from vendor_performance where 1 group by vendor_id) as v_per on v.vendor_id=v_per.vendor_id
                    WHERE ( v.vendor_name LIKE '%$search_for%' OR v.contact_name LIKE '$search_for%' 
                    OR v.phone_1 LIKE '%$search_for%' OR v.phone_2 LIKE '%$search_for%'
                    OR i.value LIKE '%$search_for%' OR s.value LIKE '%$search_for%'
                )
            ";

            }
        
            $order_by_clause = "";
            if (is_array($order_by) && count($order_by)) {
                foreach ($order_by as $column) {
                    $column_index = $column['column'];
                    $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                    
                    $column_name = "";
                    switch ($column_index) {
                        case 0:
                            $column_name = "vendor_id";
                            break;
                        case 1:
                            $column_name = "vendor_name";
                            break;
                        case 2:
                            $column_name = "industry";
                            break;
                        case 3:
                            $column_name = "subindustry";
                            break;
                        case 4:
                            $column_name = "expired_document";
                            break;
                        case 5:
                            $column_name = "total_pending";
                            break;
                        case 6:
                            $column_name = "avg_score";
                            break;
                         case 7:
                            $column_name = "preferred_flag";
                            break;
                        case 8:
                            $column_name = "contract_id";
                            break;
                        case 9:
                            $column_name = "score_value";
                            break;
                        case 10:
                            $column_name = "risk_score_value";
                            break;
                        case 11:
                            $column_name = "l.location_id";
                            break;
                        case 12:
                            $column_name = "department_id";
                            break;
                        default:
                            $column_name = "vendor_name";
                            break;
                    }
                    
                    if (! empty($column_name)) {
                        if (empty($order_by_clause))
                            $order_by_clause = $column_name . ' ' . $column_direction;
                        else
                            $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                    }
                }
            }

            
            if (empty($order_by_clause))
                $order_by_clause = "vendor_id";
            if (strpos($sql, "WHERE") === false)
                $sql .= " WHERE 1 = 1 and member_type=0";
            if (!empty($industry_id))
                $sql .= " AND v.industry_id = $industry_id ";
            if (!empty($subindustry_id))
                $sql .= " AND v.subindustry_id = $subindustry_id ";
            if($preferred_flag != - 1)
                $sql .= " AND v.preferred_flag = $preferred_flag ";
            if($supplier_status !='')
                $sql .= " AND v.active = $supplier_status ";
            if($location_id !='')
                $sql .= " AND vl.location_id = $location_id ";
            if($department_id !='')
                $sql .= " AND vd.department_id = $department_id ";
            // $order_by_clause;
            if(Yii::app()->controller->id=="vendors" && Yii::app()->controller->action->id=='list')
                $sql .= " AND quick_created != 'Yes and Quick Sourcing Evaluation' ";
            // if(Yii::app()->controller->id=="vendors" && Yii::app()->controller->action->id=='list')
            //     $sql .= " AND ( v.vendor_archive is null  or v.vendor_archive='') ";
            if(Yii::app()->controller->id=="vendors" && Yii::app()->controller->action->id=='archiveList')
                $sql .= " AND (v.vendor_archive ='yes')";
            if(Yii::app()->controller->id=="vendors" && (Yii::app()->controller->action->id=='list' || Yii::app()->controller->action->id=='review'))
               $sql .= " AND (v.vendor_archive is null or v.vendor_archive='')"; 
            
            if($start != 'none'){
              $sql .= " group by vendor_id ORDER BY $order_by_clause LIMIT $start, $length ";
            }else{
              $sql .= " group by vendor_id ORDER BY $order_by_clause ";
            }
            return $this->executeQuery($sql);
        }
    }

     public function getVendorsReview($vendor_id = 0, $industry_id = 0, $subindustry_id = 0, $preferred_flag = -1,$supplier_status="",$tableName='vendors',$archive='')
    {   

        $currentDate = date('Y-m-d');
         $sql = "SELECT v.*, i.value AS industry, s.value as subindustry,con.contract_id,((nt*100/(nt+zs+se))-(zs*100/(nt+zs+se))) as net_score,avg_score, vdoc.total_pending,vdoc.expired_document,v_risk.risk_score_value,v_sustainability.score_value
                  FROM $tableName v
                  LEFT JOIN industries i ON v.industry_id = i.id
                  LEFT JOIN sub_industries s ON v.subindustry_id = s.id
                  LEFT JOIN ( select vendor_id, 
                sum(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END) as total_pending,
                sum(CASE WHEN expiry_date <'".$currentDate."' THEN 1 ELSE 0 END) as expired_document

                from vendor_documents where vendor_documents.archive !=1 group by vendor_id) vdoc ON v.vendor_id = vdoc.vendor_id

          LEFT JOIN (select count(inn_con.contract_id) as contract_id ,inn_con.vendor_id from contracts as inn_con where inn_con.contract_archive ='' or inn_con.contract_archive is null group by inn_con.vendor_id) con ON v.vendor_id = con.vendor_id

           LEFT JOIN (
            SELECT
            SUM(CASE WHEN score_value>=9 and score_value<=10 THEN 1 ELSE 0 END) AS nt,SUM(CASE WHEN score_value>=0 and score_value<=6 THEN 1 ELSE 0 END) as zs,SUM(CASE WHEN score_value>=7 and score_value<=8 THEN 1 ELSE 0 END) se,vendor_id,


            sum(score_value)/count(id) as avg_score from vendor_performance where 1 group by vendor_id) as v_per on v.vendor_id=v_per.vendor_id

           LEFT JOIN (select * from vendor_risk where id=(select max(in_vr.id) from vendor_risk in_vr where in_vr.vendor_id=vendor_risk.vendor_id) group by vendor_id) as v_risk on v.vendor_id=v_risk.vendor_id

            LEFT JOIN (select * from vendor_sustainability where id=(select max(in_vr.id) from vendor_sustainability in_vr where in_vr.vendor_id=vendor_sustainability.vendor_id) group by vendor_id) as v_sustainability on v.vendor_id=v_sustainability.vendor_id
          ";
          
        if (!empty($vendor_id))
            return $this->executeQuery($sql . " WHERE v.vendor_id = $vendor_id", true);
        else {
          
           
            if (strpos($sql, "WHERE") === false)
                $sql .= " WHERE 1 = 1 and member_type=0";
            if (!empty($industry_id))
                $sql .= " AND v.industry_id = $industry_id ";
            if (!empty($subindustry_id))
                $sql .= " AND v.subindustry_id = $subindustry_id ";
            if($preferred_flag != - 1)
                $sql .= " AND v.preferred_flag = $preferred_flag ";
            if($supplier_status !='')
                $sql .= " AND v.active = $supplier_status ";
            if(Yii::app()->controller->id=="vendors" && (Yii::app()->controller->action->id=='list' || Yii::app()->controller->action->id=='review'))
                $sql .= " AND (v.vendor_archive is null or v.vendor_archive='')"; 
             
            
            return $this->executeQuery($sql);
        }
    }
    public function getVendorsList()
    {
        $sql = "SELECT v.*,i.value AS industry,s.value as subindustry
                  FROM vendors v
                  LEFT JOIN industries i ON v.industry_id = i.id
                  LEFT JOIN sub_industries s ON v.subindustry_id = s.id";
            echo $sql . " ORDER BY vendor_id DESC ";exit;
        
    }

      public function madeVendorLogComment($vendor_id = 0)
    {
        $sql = "SELECT * FROM vendors WHERE  vendor_id = $vendor_id";
        return $this->executeQuery($sql);
    }

    public function getVendorsDocuments($vendorID,$type="active")
    {
        if($type=="active"){
            $archive = "archive !=1";
          }else{
            $archive = "archive =1";
          }
        $sql= "SELECT vendor_documents.*, SUM(CASE WHEN status='Pending' THEN 1 ELSE 0 END) AS pendiing_documents,
              SUM(CASE WHEN status='Approved' THEN 1 ELSE 0 END) AS approved_documents from vendor_documents,vendor_document_type vdt where $archive and vendor_id=$vendorID and vendor_documents.document_type=vdt.id group by document_type order by vdt.name asc ";
        return $this->executeQuery($sql);
        
    }

    public function getVendorsDocumentsOnboard($vendorID,$type="active")
    {
        if($type=="active"){
            $archive = "archive !=1";
          }else{
            $archive = "archive =1";
          }
        $sql= "SELECT vendor_documents_onboard.*, SUM(CASE WHEN status='Pending' THEN 1 ELSE 0 END) AS pendiing_documents,
              SUM(CASE WHEN status='Approved' THEN 1 ELSE 0 END) AS approved_documents from vendor_documents_onboard,vendor_document_type_onboard vdto where $archive and vendor_id=$vendorID and vendor_documents_onboard.document_type =vdto.id group by document_type order by vdto.name asc ";
        return $this->executeQuery($sql);
        
    }
    
    
    public function getVendorDocumentsPendingRequest($vendorID)
    {
        $sql= "SELECT vrd.*,vdt.name as type_name,vdt.id as type_id,vrd.id request_id FROM `vendor_request_documents` as vrd 
          left join vendor_document_type as vdt on vrd.vendor_document_type = vdt.id
          where vrd.vendor_id=".$vendorID." and vrd.status='Pending' order by vdt.name asc";
        return $this->executeQuery($sql);
        
    }
    public function getVendorDocumentsPendingRequestOnboard($vendorID)
    {
        $sql= "SELECT vrd.*,vdt.name as type_name,vdt.id as type_id,vrd.id request_id FROM `vendor_request_documents_onboard` as vrd      
          left join vendor_document_type_onboard as vdt on vrd.vendor_document_type = vdt.id
          where vrd.vendor_id=".$vendorID." and vrd.status='Pending' order by vdt.name asc";
        return $this->executeQuery($sql);
        
    }
    
    public function getVendorStatistics($vendorID=0)
    {
        $data = array('totalVendors'=>0,'preferredVendors'=>0,'preferredVendorPercentage'=>0,'vendorUnderContract'=>0,'vendorUnderContractPercentage'=>0,'vendorMissingDocuments'=>0,'vendorMissingDocumentsPercentage'=>0,'vendorCount'=>0,'contractCount'=>0,'documentApprovedPerc'=>0,'documentPendingdPerc'=>0,'totalDocument'=>0,'totalDocumentPerc'=>0,'documentValidPerc'=>0,'documentExpiredPerc'=>0);

        $sql = "select sum(CASE WHEN preferred_flag=1 THEN 1 ELSE 0 END) as preferred,sum(CASE WHEN preferred_flag !=1 THEN 1 ELSE 0 END) as non_preferred from vendors where member_type=0 and active=1 and (vendor_archive is null or vendor_archive ='')";
        $preNonVerndor = Yii::app()->db->createCommand($sql)->queryRow();
        if(!empty($preNonVerndor) && $preNonVerndor['preferred']+$preNonVerndor['non_preferred']>0){
          $preferred= $preNonVerndor['preferred'];
          $data['preferredVendors']=$preferred;
          $nonPreferred= $preNonVerndor['non_preferred'];
          $totalPrefAndNon = $preNonVerndor['preferred']+$preNonVerndor['non_preferred'];
          $data['preferredVendorPercentage'] =  number_format($preferred/ $totalPrefAndNon*100,0);
          $data['nonPreferredVendorPercentage'] =  number_format($nonPreferred/ $totalPrefAndNon*100,0);
        }

        $sql = "select count(*) as total_vendors from vendors where  active=1 and member_type=0 and vendor_archive is null ";
        $vendorCount = Yii::app()->db->createCommand($sql)->queryRow();
        if(!empty($vendorCount)){
          $data['vendorCount'] = $vendorCount['total_vendors']; 
        }

        $currentDate = date('Y-m-d');
        $sql = "select count(*) as total_contracts from contracts where contract_end_date>='".$currentDate."'";
        $contractCount = Yii::app()->db->createCommand($sql)->queryRow();
        if(!empty($contractCount)){
          $data['contractCount'] = $contractCount['total_contracts']; 
        }
        // Start: % of documents uploaded and approved and awaiting approval
            $sql = "select
                sum(CASE WHEN vnd.status = 'Approved' THEN 1 ELSE 0 END) as total_approved,
                sum(CASE WHEN vnd.status = 'Pending' and vnd.expiry_date >='".$currentDate."' THEN 1 ELSE 0 END) as total_pending,
                sum(CASE WHEN vnd.expiry_date >='".$currentDate."' THEN 1 ELSE 0 END) as valid_document,
                sum(CASE WHEN vnd.expiry_date <'".$currentDate."' THEN 1 ELSE 0 END) as expired_document
                from vendor_documents vnd 
                inner join  vendors ven on ven.vendor_id = vnd.vendor_id
                 where  (ven.vendor_archive='' or ven.vendor_archive is null) and (vnd.archive='0')";
        $document = Yii::app()->db->createCommand($sql)->queryRow();

        //Start :  model query show document need approvel
        $sql = "select
                ven.vendor_name,
                ven.vendor_id,
                vnd.document_title,vnd.expiry_date
                from vendor_documents vnd 
                inner join  vendors ven on ven.vendor_id = vnd.vendor_id
                 where vnd.status = 'Pending' and vnd.expiry_date >='".$currentDate."' AND (ven.vendor_archive='' or ven.vendor_archive is null) and (vnd.archive='0') ORDER by ven.vendor_name ASC";
        $documentNeedApproval = Yii::app()->db->createCommand($sql)->query()->readAll();
         $data['documentNeedApproval'] = $documentNeedApproval;
         //End : model query show document need approvel

        //Start :  model query show document that have expired 
        $sql = "select
                ven.vendor_name,
                ven.vendor_id,
                vnd.document_title,vnd.expiry_date
                from vendor_documents vnd 
                inner join  vendors ven on ven.vendor_id = vnd.vendor_id
                 where vnd.expiry_date <'".$currentDate."' AND (ven.vendor_archive='' or ven.vendor_archive is null) and (vnd.archive='0') ORDER by vnd.expiry_date DESC";
        $documentExpiredList = Yii::app()->db->createCommand($sql)->query()->readAll();
         $data['documentExpiredList'] = $documentExpiredList;
         //End : model query show document that have expired

        if(!empty($document)){
            $documentApproved = $document['total_approved'];
            $documentPending  = $document['total_pending'];
            $data['totalDocument']  = $documentApproved+$documentPending;

            $totalExpValidDocument = $document['valid_document']+$document['expired_document'];

            $data['totalDocumentPerc']  =  !empty($data['totalDocument'])?number_format($data['totalDocument']/ $data['totalDocument']*100,0):0;
            $data['documentApprovedPerc'] =  !empty($data['totalDocument'])?number_format($documentApproved/ $data['totalDocument']*100,0):0;
            $data['documentPendingdPerc'] =  !empty($data['totalDocument'])?number_format($documentPending/ $data['totalDocument']*100,0):0;

            $data['documentValidPerc'] =   !empty($totalExpValidDocument)?number_format($document['valid_document'] / $totalExpValidDocument*100,0):0;
            $data['documentPending'] =  number_format($documentPending,0);
            $data['documentExpired'] =  number_format($document['expired_document'],0);

            $data['documentExpiredPerc'] =  !empty($totalExpValidDocument)?number_format($document['expired_document']/ $totalExpValidDocument*100,0):0;

        }
        // End: % of documents uploaded and approved and awaiting approval


        $sql = "select count(*) as vendor_under_contract from vendors inner join (select * from contracts group by contracts.vendor_id) as contract on vendors.vendor_id=contract.vendor_id and vendors.active=1 and vendor_archive is null ";
        $vendorUnderContract = Yii::app()->db->createCommand($sql)->queryRow();

        if(!empty($vendorUnderContract) && !empty($totalPrefAndNon)){
          $underContract= $vendorUnderContract['vendor_under_contract'];
          $data['vendorUnderContractPercentage'] =  number_format($underContract/ $totalPrefAndNon*100,0);
        }

         $sql = "select count(vendors.vendor_id) as vendor_missing_document from vendors left join vendor_documents on vendors.vendor_id=vendor_documents.vendor_id where vendor_documents.vendor_id  is null and vendors.member_type=0 and vendors.active=1 and vendor_documents.archive !=1";
        $vendorDocument = Yii::app()->db->createCommand($sql)->queryRow(); 

        if(!empty($vendorDocument) && !empty($totalPrefAndNon)){
          $vendorMissingDocuments= $vendorDocument['vendor_missing_document'];
          $data['vendorMissingDocumentsPercentage'] =  number_format($vendorMissingDocuments/ $totalPrefAndNon*100,0);
        }

        
      return $data;
    }
    public function getVendorDetailStatistics($vendorID=0)
    {
        $data = array('totalQuotes'=>0,'respondedQuotes'=>0,'respondedQuotesPercentage'=>0,'totalDocuments'=>0,'documentApproved'=>0,'documentApprovedPercentage'=>0,'totalFields'=>0,'fieldsCompleted'=>0,'fieldsCompletedPercentage'=>0,'documentExpiredPercentage'=>0);

        $currentDate = date('Y-m-d');

       /* $sql = "select ((vendor_name !='')+(contact_name !='')+(phone_1 !='')+(phone_2 !='')++(external_id !='')+(profile_status !='')+(industry_id !=0)+(subindustry_id !=0)+(number !='')+(tax_number !='')+(preferred_flag !=0)+(address_1 !='')+(address_2 !='')+(city !='')+(state !='')+(country !='')+(zip !='')+(website !='' or website is not NULL)+(comments !='')+(payment_term_id !=0)+(shipping_term_id !=0)+(shipping_method_id !=0))  as total_completed_fields from vendors where vendor_id=".$vendorID;
         $vendorFields = Yii::app()->db->createCommand($sql)->queryRow();*/


        /* $sql = "select ((vendor_name !='')+(contact_name !='' or contact_name is not NULL)+(phone_1 !='' or phone_1 is not NULL)+(phone_2 !=''  or phone_2 is not NULL)+(external_id !='')+(profile_status !='' or profile_status is not NULL)+(industry_id !=0)+(subindustry_id !=0)+(number !='' or number is not NULL)+(tax_number !='' or tax_number is not NULL)+(preferred_flag !=0)+(website !='' or website is not NULL)+(address_1 !='' or address_1 is not NULL)+(address_2 !='' or address_2 is not NULL)+(city !='' or city is not NULL)+(state !='' or state is not NULL)+(country !='' or country is not NULL)+(zip !='' or zip is not NULL)+(payment_term_id !=0)+(shipping_term_id !=0)+(shipping_method_id !=0))  as total_completed_fields from vendors where vendor_id=".$vendorID;*/

 
        $sql = "select (
        (trim(coalesce(vendor_name, '')) !='')
        +(trim(coalesce(contact_name, ''))  !='' )
        +(trim(coalesce(emails !='', ''))  !='')
        +(trim(coalesce(external_id, '')) !='')
        +(industry_id !=0)
        +(subindustry_id !=0)
        +(trim(coalesce(number, '')) !='')
        +(trim(coalesce(tax_number, '')) !='')
        +(payment_term_id !=0)
        +(shipping_term_id !=0)
        +(shipping_method_id !=0)
        +(trim(coalesce(website , '')) !='')
        +(trim(coalesce(address_1 , '')) !='')
        +(trim(coalesce(address_2, ''))  !='')
        +(trim(coalesce(city , '')) !='')
        +(trim(coalesce(state , '')) !='' )
        +(trim(coalesce(country , '')) !='' )
        +(trim(coalesce(zip , '')) !='' )
        +(trim(coalesce(phone_1 , '')) !='' )
        +(trim(coalesce(phone_2 , '')) !='' )
    )  as total_completed_fields from vendors where vendor_id=".$vendorID;
        
        $vendorFields = Yii::app()->db->createCommand($sql)->queryRow();


        $sql = "select sum(CASE WHEN status = 'Approved' THEN 1 ELSE 0 END) as total_approved,sum(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END) as total_pending,sum(CASE WHEN expiry_date <='".$currentDate."' THEN 1 ELSE 0 END) as total_expired from vendor_documents where vendor_documents.archive !=1 and vendor_id=".$vendorID;
        $document = Yii::app()->db->createCommand($sql)->queryRow();


        $sql = "select sum(CASE WHEN submit_status = 0 THEN 1 ELSE 0 END) as total_pending,sum(CASE WHEN submit_status != 0 THEN 1 ELSE 0 END) as total_responded from  quote_vendors where vendor_id=".$vendorID;
        $quotes = Yii::app()->db->createCommand($sql)->queryRow();

        if(!empty($quotes['total_pending']) || !empty($quotes['total_responded'])){
            $data['respondedQuotes'] = $quotes['total_responded'];
            $data['totalQuotes'] = $quotes['total_responded']+$quotes['total_pending'];
            $data['respondedQuotesPercentage'] =  number_format($data['respondedQuotes']/ $data['totalQuotes']*100,0);
        }

       
        if(!empty($document['total_approved']) || !empty($document['total_pending'])){
            $data['documentApproved'] = $document['total_approved'];
            $data['totalDocuments'] = $document['total_approved']+$document['total_pending'];    
            $data['documentApprovedPercentage'] =  number_format($data['documentApproved']/ $data['totalDocuments']*100,0);

            $data['documentExpiredPercentage'] =  number_format($document['total_expired']/ $data['totalDocuments']*100,0);

        }

        if(!empty($vendorFields['total_completed_fields'])){
            $data['fieldsCompleted'] = $vendorFields['total_completed_fields'];
            $data['totalFields']     = 20;
            $data['fieldsCompletedPercentage'] = number_format($data['fieldsCompleted']/$data['totalFields']*100,0);
        }
        return $data;
    }
	public function getProductVendors()
	{
		$sql = "SELECT v.*, i.value AS industry, s.value as subindustry
		          FROM vendors v
				  LEFT JOIN industries i ON v.industry_id = i.id
				  LEFT JOIN sub_industries s ON v.subindustry_id = s.id";

		return $this->executeQuery($sql . " ORDER BY vendor_name ASC");
	}
  public function getProdVendors()
  {
    $sql = "SELECT v.vendor_id,v.vendor_name
              FROM vendors v";

    return $this->executeQuery($sql . " ORDER BY vendor_name ASC");
  }
	public function getSearchedRows($search_for, $industry_id = 0, $subindustry_id = 0, $preferred_flag = -1, $location_id= 0, $department_id= 0)
  {
        $sql = "SELECT COUNT(*) AS total_rows
		          FROM vendors v
				  LEFT JOIN industries i ON v.industry_id = i.id AND i.value LIKE '$search_for%'
				  LEFT JOIN sub_industries s ON v.subindustry_id = s.id AND s.value LIKE '$search_for%'
				  LEFT JOIN vendor_location vl ON v.vendor_id = vl.vendor_id 
				  LEFT JOIN vendor_department vd ON v.vendor_id = vd.vendor_id
				 WHERE ( v.vendor_name LIKE '$search_for%' OR v.contact_name LIKE '$search_for%' 
				 				OR v.phone_1 LIKE '$search_for%' OR v.phone_2 LIKE '$search_for%'
						)";
        
        if ($industry_id)
            $sql .= " AND v.industry_id = $industry_id ";
        if ($subindustry_id)
            $sql .= " AND v.subindustry_id = $subindustry_id ";
        if ($preferred_flag != - 1)
            $sql .= " AND v.preferred_flag = $preferred_flag ";
        if ($location_id)
            $sql .= " AND vl.location_id = $location_id ";
        if ($department_id)
            $sql .= " AND vl.department_id = $department_id ";
        
        $count_data = $this->executeQuery($sql, true);
        return $count_data && is_array($count_data) && isset($count_data['total_rows']) ? $count_data['total_rows'] : 0;
    }

    public function getVendorsAndContractsCategory($start_date, $end_date)
    {
      $sqlContractCate = "SELECT 'contract' as type, contract_title as title, cat.id as category_id , cat.value AS category
            FROM contracts cont, categories cat
            WHERE cont.contract_category_id = cat.id
             AND cont.updated_datetime >= '$start_date'
             AND cont.updated_datetime <= '$end_date'
            ";
      $sqlVendorContract = "SELECT 'vendor' as type, vend.vendor_name as title, cat.id as category_id, cat.value AS category
            FROM contracts cont, categories cat,vendors vend
            WHERE cont.contract_category_id = cat.id and cont.vendor_id=vend.vendor_id
             AND cont.updated_datetime >= '$start_date'
             AND cont.updated_datetime <= '$end_date'
            ";

      $sql = "(".$sqlContractCate.") union (".$sqlVendorContract.")";
      $recordReader = Yii::app()->db->createCommand($sql)->query()->readAll();
      $vendorCount = $contractCount = 0;
      $category = array();
      foreach($recordReader as $value){
        if($value['type']=='contract'  && $value['title'])
         $category[$value['category']]['contract'] = !empty($category[$value['category']]['contract'])?$category[$value['category']]['contract']+1:1;
        else if($value['type']=='vendor' && $value['title'])
         $category[$value['category']]['vendor'] = !empty($category[$value['category']]['vendor'])?$category[$value['category']]['vendor']+1:1;
      }
      return array('count_by_category'=>$category,'list_by_category'=>$recordReader);
    }

    public function getVendorsAndContractsIndustry(){

        if(Yii::app()->controller->id=="vendors"){
            $orderBy = 'total_vendors';
        }else{
            $orderBy = 'total_contracts';
        }
       $sql = "SELECT count(v.vendor_id) as total_vendors,(select count(c.contract_id) from vendors v,contracts c where c.vendor_id=v.vendor_id and v.industry_id = i.id and v.member_type=0) as total_contracts,i.value as industry
             FROM vendors v
             inner JOIN industries i ON v.industry_id = i.id
             
             where  v.member_type=0
             GROUP BY industry
             ORDER BY  ".$orderBy."  DESC limit 10";

      $recordReader = Yii::app()->db->createCommand($sql)->query()->readAll();

      return array('count_by_category'=>$recordReader,'list_by_category'=>$recordReader);
    }

    public function getVendorsByCategory($start_date, $end_date)
    {
        // expenses first
        $category_totals = array();
        $totals = array();
        $sql = "SELECT d.vendor_id, d.expense_type_id AS category_id, e.value AS category, v.vendor_name, 
					   v.preferred_flag, SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, vendors v, expense_types e
 				 WHERE d.vendor_id = v.vendor_id
   				   AND d.expense_type_id = e.id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date'
   				   AND d.expense_id IN (SELECT expense_id FROM expenses
   				   						 WHERE expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') )
 				 GROUP BY d.vendor_id, d.expense_type_id";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT d.vendor_id, d.expense_type_id AS category_id, e.value AS category, 
						   v.vendor_name, v.preferred_flag, SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, vendors v, expense_types e
	 				 WHERE d.vendor_id = v.vendor_id 
	   				   AND d.expense_type_id = e.id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date'
   					   AND d.expense_id IN (SELECT expense_id FROM expenses 
   					   						 WHERE expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') )
	 				 GROUP BY d.vendor_id, d.expense_type_id";
        
        foreach ($this->executeQuery($sql) as $row) {
            if (! isset($totals['E_' . $row['category_id']]))
                $totals['E_' . $row['category_id']] = array();
            $totals['E_' . $row['category_id']][] = $row;
            
            if (! isset($category_totals['E_' . $row['category_id']]))
                $category_totals['E_' . $row['category_id']] = 0;
            $category_totals['E_' . $row['category_id']] += $row['total'];
        }
        
        // and then orders
        $sql = "SELECT o.vendor_id, v.vendor_name, v.preferred_flag, 
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price * ((100 + o.tax_rate) / 100)) AS `total`
  				  FROM order_details d, orders o, vendors v, products p, categories c
 				 WHERE d.order_id = o.order_id
   				   AND o.vendor_id = v.vendor_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
   				   AND d.product_type = 'Product'
   				   AND d.product_id = p.product_id
   				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND p.category_id = c.id 
 				 GROUP BY v.vendor_id, c.id
 
 				 UNION

				SELECT o.vendor_id, v.vendor_name, v.preferred_flag, 
					   c.id AS category_id, c.value AS category,
					   SUM(d.unit_price * ((100 + o.tax_rate) / 100)) / COUNT(DISTINCT b.product_id) AS `total`
  				  FROM order_details d, orders o, vendors v, products p, categories c, bundle_products b
 				 WHERE d.order_id = o.order_id
   				   AND o.vendor_id = v.vendor_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date'
   				   AND d.product_type = 'Bundle'
   				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND b.bundle_id = d.product_id 
   				   AND b.product_id = p.product_id 
   				   AND p.category_id = c.id 
 				 GROUP BY b.bundle_id, v.vendor_id, c.id";
        
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT o.vendor_id, v.vendor_name, v.preferred_flag, 
						   c.id AS category_id, c.value AS category,
						   SUM(d.unit_price) AS `total`
	  				  FROM order_details d, orders o, vendors v, products p, categories c
	 				 WHERE d.order_id = o.order_id
	   				   AND o.vendor_id = v.vendor_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date'
   					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	   				   AND d.product_type = 'Product'
	   				   AND d.product_id = p.product_id
	   				   AND p.category_id = c.id 
	 				 GROUP BY v.vendor_id, c.id
	 
	 				 UNION
	
					SELECT o.vendor_id, v.vendor_name, v.preferred_flag, 
						   c.id AS category_id, c.value AS category,
						   SUM(d.unit_price) / COUNT(DISTINCT b.product_id) AS `total`
	  				  FROM order_details d, orders o, vendors v, products p, categories c, bundle_products b
	 				 WHERE d.order_id = o.order_id
	   				   AND o.vendor_id = v.vendor_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date'
	   	   			   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   AND d.product_type = 'Bundle'
	   				   AND b.bundle_id = d.product_id 
	   				   AND b.product_id = p.product_id 
	   				   AND p.category_id = c.id 
	 				 GROUP BY b.bundle_id, v.vendor_id, c.id";
        
        foreach ($this->executeQuery($sql) as $row) {
            if (! isset($totals['O_' . $row['category_id']]))
                $totals['O_' . $row['category_id']] = array();
            $totals['O_' . $row['category_id']][] = $row;
            
            if (! isset($category_totals['O_' . $row['category_id']]))
                $category_totals['O_' . $row['category_id']] = 0;
            $category_totals['O_' . $row['category_id']] += $row['total'];
        }
        
        arsort($category_totals);
        $sorted_totals = array();
        foreach ($category_totals as $category_type => $category_value)
            foreach ($totals as $category_id => $category_data)
                if ($category_id == $category_type)
                    $sorted_totals[$category_id] = $category_data;
        
        return $sorted_totals;
    }

    public function getVendorsByDepartment($start_date, $end_date)
    {
        // expenses first
        $totals = array();
        $sql = "SELECT d.vendor_id, p.department_name AS department, p.department_id,
					   SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, departments p, expenses e
 				 WHERE e.expense_id = d.expense_id
 				   AND e.department_id = p.department_id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
   				   AND d.expense_id IN (SELECT expense_id FROM expenses 
   				   						 WHERE expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') )
 				 GROUP BY d.vendor_id, p.department_id";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT d.vendor_id, p.department_name AS department, p.department_id, SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, departments p, expenses e
	 				 WHERE e.expense_id = d.expense_id
	 				   AND e.department_id = p.department_id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date' 
   					   AND d.expense_id IN (SELECT expense_id FROM expenses 
	   				   						 WHERE expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') )
	 				 GROUP BY d.vendor_id, p.department_id";
        
        foreach ($this->executeQuery($sql) as $row) {
            if (! isset($totals[$row['department_id']]))
                $totals[$row['department_id']] = array();
            $totals[$row['department_id']][] = $row;
        }
        
        // and then orders
        $sql = "SELECT o.vendor_id, p.department_name as department, p.department_id, 
						SUM(o.total_price + o.other_charges - o.discount) AS `total`
  				  FROM orders o, departments p
 				 WHERE o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND o.department_id = p.department_id
 				 GROUP BY o.vendor_id, p.department_id";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT o.vendor_id, p.department_name as department, p.department_id, 
						   SUM(o.total_price * ((100 - o.tax_rate) / 100) + o.other_charges - o.discount) AS `total`
	  				  FROM orders o, departments p
	 				 WHERE o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
   					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	   				   AND o.department_id = p.department_id
	 				 GROUP BY o.vendor_id, p.department_id";
        
        foreach ($this->executeQuery($sql) as $row) {
            if (! isset($totals[$row['department_id']]))
                $totals[$row['department_id']] = array();
            $totals[$row['department_id']][] = $row;
        }
        
        return $totals;
    }

    public function getVendorsCountsAndAmounts($start_date, $end_date)
    {
        // expenses first
        $totals = array();
        $sql = "SELECT d.vendor_id, v.vendor_name, COUNT(DISTINCT d.vendor_id) AS `count`,
					   sum(getOrderTotalInGB(getExpenseTotalAmount(e.expense_id),e.currency_id)) AS `total`
  				  FROM expenses e,expense_details d, vendors v
 				 WHERE e.expense_id = d.expense_id
             AND d.vendor_id = v.vendor_id 
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
   				   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY d.vendor_id
 				 ORDER BY total DESC
 				 LIMIT 10";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT d.vendor_id, v.vendor_name, COUNT(DISTINCT d.id) AS `count`, sum(getOrderTotalInGB(getExpenseTotalAmount(e.expense_id),e.currency_id)) AS `total`
	  				  FROM expenses e,expense_details d, vendors v
	 				 WHERE e.expense_id = d.expense_id
                AND d.vendor_id = v.vendor_id 
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date' 
   				   	   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	   				 GROUP BY d.vendor_id
	   				 ORDER BY total DESC
	   				 LIMIT 10";
        
        foreach ($this->executeQuery($sql) as $row) {
            if (! isset($totals[$row['vendor_id']]))
                $totals[$row['vendor_id']] = $row;
        }
        
        // and then orders
        $sql = "SELECT o.vendor_id, v.vendor_name, COUNT(DISTINCT o.order_id) AS `count`,
          SUM(getOrderTotalInGB(GetOrderTotalAmount(o.order_id),o.currency_id)) AS `total`
  				  FROM order_details d, orders o, vendors v
 				 WHERE d.order_id = o.order_id
   				   AND o.vendor_id = v.vendor_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
 				 GROUP BY v.vendor_id
 				 ORDER BY total DESC
 				 LIMIT 10";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT o.vendor_id, v.vendor_name, COUNT(DISTINCT o.order_id) AS `count`,
						    SUM(getOrderTotalInGB(GetOrderTotalAmount(o.order_id),o.currency_id)) AS `total`
	  				  FROM order_details d, orders o, vendors v
	 				 WHERE d.order_id = o.order_id
	   				   AND o.vendor_id = v.vendor_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
   					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY v.vendor_id
	 				 ORDER BY total DESC
	 				 LIMIT 10";
        foreach ($this->executeQuery($sql) as $row) {
            if (! isset($totals[$row['vendor_id']]))
                $totals[$row['vendor_id']] = $row;
            else {
                $totals[$row['vendor_id']]['count'] += $row['count'];
                $totals[$row['vendor_id']]['total'] += $row['total'];
            }
        }
        
        usort($totals, function ($a, $b) {
            return $b['total'] - $a['total'];
        });
        return $totals;
    }

  public function getSupplierUsed($start_date,$end_date)
  {
    $sql = "SELECT type,count(vendor_id) as total_vendors,month_year,month from  view_spend
        WHERE order_date >= '$start_date' AND order_date <= '$end_date' AND status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')  GROUP by month_year,type";
    return $this->executeQuery($sql . ' ORDER BY month ASC ');
  }

  public function getOwners($vendor_id=0)
  {
     $sql = "SELECT users.user_id,users.full_name from users  LEFT JOIN vendor_owner own on users.user_id=own.user_id and own.vendor_id=".$vendor_id."  where  users.admin_flag=0 and own.user_id IS NULL";
    return $this->executeQuery($sql . ' ORDER BY users.full_name ASC ');
  }

  public function getOwnersVendor($vendor_id=0)
  {
     $sql = "SELECT users.user_id,users.full_name,own.created_at,own.id as owner_id from users  LEFT JOIN vendor_owner own on users.user_id=own.user_id where own.vendor_id=".$vendor_id;
    return $this->executeQuery($sql . ' ORDER BY users.full_name ASC ');
  }

  public function getQuickComparision($vendor_id=0)
  {
    $condition=$scoreReader='';
    if(!empty($vendor_id)){
        $condition = "where qs.vendor_id=".$vendor_id;
    
          $sql = "select mqs.*,qs.vendor_id from main_quick_scoring mqs 

            inner join  vendor_quick_scoring qs on mqs.id=qs.main_quick_scoring_id 

              ".$condition." group by mqs.id order by mqs.id DESC";
        $scoreReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    }
    return $scoreReader;
  }

  public function getVendorPerformance($vendor_id=0)
  {
    $condition=$performanceReader='';
    if(!empty($vendor_id)){
        $condition = "where vendor_id=".$vendor_id;
    
        $sql = "select * from vendor_performance mqs ".$condition." order by id DESC";
        $performanceReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    }
    return $performanceReader;
  }

  public function getVendorRisk($vendor_id=0)
  {
    $condition=$riskReader='';
    if(!empty($vendor_id)){
        $condition = "where vendor_id=".$vendor_id;
    
        $sql = "select * from vendor_risk ".$condition."  order by id DESC";
        $riskReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    }
    return $riskReader;
  }

  public function getVendorSustainability($vendor_id=0)
  {
    $condition=$riskReader='';
    if(!empty($vendor_id)){
        $condition = "where vendor_id=".$vendor_id;
    
        $sql = "select * from vendor_sustainability ".$condition."  order by id DESC";
        $riskReader = Yii::app()->db->createCommand($sql)->query()->readAll();
    }
    return $riskReader;
  }


  public function saveDocument($vendor_id, $file_description, $file_name,$document_type,$document_status,$expiry_date)
  {  
    $user_id = Yii::app()->session['user_id'];
    $dateCreated = date("Y-m-d H:i:s"); 

    if(FunctionManager::dateFormat()=="d/m/Y"){
        $expiryDate  =  date("Y-m-d", strtotime(strtr($expiry_date, '/', '-')));
    }else{
        $expiryDate  =  date("Y-m-d", strtotime($expiry_date));
    }

    
    if(!empty($vendor_id) && !empty($file_name)){
      return $this->executeQuery("INSERT INTO vendor_documents (vendor_id, uploaded_by, document_title,document_file,document_type,status,date_created,expiry_date) SELECT $vendor_id,$user_id, '$file_description', '$file_name',$document_type,'$document_status','$dateCreated','$expiryDate'");
    }
  }

   public function saveDocumentOnboard($vendor_id, $file_description, $file_name,$document_type,$document_status,$expiry_date)
  {  
    $user_id = Yii::app()->session['user_id'];
    $dateCreated = date("Y-m-d H:i:s"); 

    if(FunctionManager::dateFormat()=="d/m/Y"){
        $expiryDate  =  date("Y-m-d", strtotime(strtr($expiry_date, '/', '-')));
    }else{
        $expiryDate  =  date("Y-m-d", strtotime($expiry_date));
    }
 
    if(!empty($vendor_id) && !empty($file_name)){
      return $this->executeQuery("INSERT INTO vendor_documents_onboard (vendor_id, uploaded_by, document_title,document_file,document_type,status,date_created,expiry_date) SELECT $vendor_id,$user_id, '$file_description', '$file_name',$document_type,'$document_status','$dateCreated','$expiryDate'");
    }
  }

 /* public function uploadDocumentOnboard($vendor_id, $file_description, $file_name,$document_type,$document_status,$expiry_date)
  {  
    $user_id = Yii::app()->session['user_id'];
    $dateCreated = date("Y-m-d H:i:s"); 

    if(FunctionManager::dateFormat()=="d/m/Y"){
        $expiryDate  =  date("Y-m-d", strtotime(strtr($expiry_date, '/', '-')));
    }else{
        $expiryDate  =  date("Y-m-d", strtotime($expiry_date));
    }

    
    if(!empty($vendor_id) && !empty($file_name)){
      return $this->executeQuery("INSERT INTO vendor_documents (vendor_id, uploaded_by, document_title,document_file,document_type,status,date_created,expiry_date) SELECT $vendor_id,$user_id, '$file_description', '$file_name',$document_type,'$document_status','$dateCreated','$expiryDate'");
    }
  }*/

  public function approveDocument($documentID)
  { 
    $user_id = Yii::app()->session['user_id'];
    $user = new User;
    $userDetail = $user->getOne(array('user_id'=>$user_id));
    $approvedbyName = $userDetail['full_name'];
    $approvedTime = date("Y-m-d H:i:s");
    if(is_array($documentID)){
      $sql = "update vendor_documents set status='Approved',approved_by='".$user_id."',approved_datetime='".$approvedTime."',approved_by_name='".$approvedbyName."' where id in(".implode(",", $documentID).")";
    }else{
      $sql = "update vendor_documents set status='Approved',approved_by='".$user_id."',approved_datetime='".$approvedTime."',approved_by_name='".$approvedbyName."' where id=".$documentID;
    }
    return $this->executeQuery($sql);
  }

  public function approveDocumentOnboard($documentID)
  { 
    $user_id = Yii::app()->session['user_id'];
    $user = new User;
    $userDetail = $user->getOne(array('user_id'=>$user_id));
    $approvedbyName = $userDetail['full_name'];
    $approvedTime = date("Y-m-d H:i:s");
    if(is_array($documentID)){
      $sql = "update vendor_documents_onboard set status='Approved',approved_by='".$user_id."',approved_datetime='".$approvedTime."',approved_by_name='".$approvedbyName."' where id in(".implode(",", $documentID).")";
    }else{
      $sql = "update vendor_documents_onboard set status='Approved',approved_by='".$user_id."',approved_datetime='".$approvedTime."',approved_by_name='".$approvedbyName."' where id=".$documentID;
    }
    return $this->executeQuery($sql);
  }

  public function archiveDocument($documentID){
        $user_id = Yii::app()->session['user_id'];
        $user = new User;
        $userDetail = $user->getOne(array('user_id'=>$user_id));
        $approvedbyName = $userDetail['full_name'];
        $archiveTime = date("Y-m-d H:i:s");  
        if(is_array($documentID)){
         $sql = " update vendor_documents set archive=1, archive_datetime='".$archiveTime."' where id in(".implode(",", $documentID).")";
        }else{
          $sql = "update vendor_documents set  archive=1, archive_datetime='".$archiveTime."' where id=".$documentID;
        }
        return $this->executeQuery($sql);
    }

    public function unArchiveDocument($documentID){
     
        $archiveTime = date("Y-m-d H:i:s");  
        if(is_array($documentID)){
          $sql = "update vendor_documents set archive=0, archive_datetime='".$archiveTime."' where id in(".implode(",", $documentID).")";
        }else{
          $sql = "update vendor_documents set archive=0, archive_datetime='".$archiveTime."' where id=".$documentID;
        }
        return $this->executeQuery($sql);
    }

    public function archiveDocumentOnboard($documentID){
        $user_id = Yii::app()->session['user_id'];
        $user = new User;
        $userDetail = $user->getOne(array('user_id'=>$user_id));
        $approvedbyName = $userDetail['full_name'];
        $archiveTime = date("Y-m-d H:i:s");  
        if(is_array($documentID)){
         $sql = " update vendor_documents_onboard set archive=1, archive_datetime='".$archiveTime."' where id in(".implode(",", $documentID).")";
        }else{
          $sql = "update vendor_documents_onboard set  archive=1, archive_datetime='".$archiveTime."' where id=".$documentID;
        }
        return $this->executeQuery($sql);
    }

    public function unArchiveDocumentOnboard($documentID){
     
        $archiveTime = date("Y-m-d H:i:s");  
        if(is_array($documentID)){
          $sql = "update vendor_documents_onboard set archive=0, archive_datetime='".$archiveTime."' where id in(".implode(",", $documentID).")";
        }else{
          $sql = "update vendor_documents_onboard set archive=0, archive_datetime='".$archiveTime."' where id=".$documentID;
        }
        return $this->executeQuery($sql);
    }

  public function editDocument($postedArr)
  { 
    /*$user_id = Yii::app()->session['user_id'];
    $user = new User;
    $userDetail = $user->getOne(array('user_id'=>$user_id));
    $approvedbyName = $userDetail['full_name'];*/
    $documentID = $postedArr['document_id'];
    $expiryDate = $postedArr['expiry_date'];

    if(FunctionManager::dateFormat()=="d/m/Y"){
        $expiryDate  =  date("Y-m-d", strtotime(strtr($expiryDate, '/', '-')));
    }else{
        $expiryDate  =  date("Y-m-d", strtotime($expiryDate));
    }
    $approvedTime = date("Y-m-d H:i:s");
    if(!empty($documentID)){
      $sql = "update vendor_documents set expiry_date='".$expiryDate."' where id in(".$documentID.")";
    }
    return Yii::app()->db->createCommand($sql)->execute();
  }

  public function editDocumentOnboard($postedArr)
  { 
    
    $documentID = $postedArr['document_id'];
    $expiryDate = $postedArr['expiry_date'];

    if(FunctionManager::dateFormat()=="d/m/Y"){
        $expiryDate  =  date("Y-m-d", strtotime(strtr($expiryDate, '/', '-')));
    }else{
        $expiryDate  =  date("Y-m-d", strtotime($expiryDate));
    }
    $approvedTime = date("Y-m-d H:i:s");
    if(!empty($documentID)){
      $sql = "update vendor_documents_onboard set expiry_date='".$expiryDate."' where id in(".$documentID.")";
    }
    return Yii::app()->db->createCommand($sql)->execute();
  }


  public function getSuppPreferNonPreferSpend($start_date,$end_date,$prefer_status=0,$vendor_count='')
  {
    if(!empty($vendor_count)){
      $vendor_count = ", count(snd.vendor_id) as total_vendors";
    }
    $sql = "SELECT type,sum(total_amount) as total_amount,month_year,month".$vendor_count." from  view_spend snd

            inner join  vendors ven on ven.vendor_id = snd.vendor_id

        WHERE ven.preferred_flag=".$prefer_status." and snd.order_date >= '$start_date' AND snd.order_date <= '$end_date' AND snd.status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')  GROUP by month_year,type";

    return $this->executeQuery($sql . ' ORDER BY snd.month ASC ');
  }

	public function getTop10Vendors($start_date, $end_date)
	{
		// expenses first
		$totals = array();

		$sql = "
                    SELECT *
                    FROM (
                    (SELECT o.vendor_id, v.vendor_name,v.contact_name,v.phone_1,v.phone_2,si.value as subindustry,i.value as industry, COUNT(DISTINCT o.order_id) AS `count`,
                    sum(getOrderTotalInGB(GetOrderTotalAmount(o.order_id),o.currency_id)) AS `total`
					   FROM orders o
					   LEFT JOIN order_details d ON o.order_id = d.order_id
					   LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
					   LEFT JOIN industries i ON v.industry_id = i.id
					   LEFT JOIN sub_industries si ON v.subindustry_id = si.id
					   WHERE o.order_date >= '$start_date'
					   AND o.order_date <= '$end_date'
					   AND o.order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY v.vendor_id)
					   UNION
					   (SELECT d.vendor_id, v.vendor_name,v.contact_name,v.phone_1,v.phone_2,si.value as subindustry,i.value as industry, COUNT(DISTINCT e.expense_id) AS `count`,
					   sum(getOrderTotalInGB(getExpenseTotalAmount(e.expense_id),e.currency_id)) AS `total` 
					   FROM expenses e
					   LEFT JOIN expense_details d ON e.expense_id = d.expense_id
					   LEFT JOIN vendors v ON d.vendor_id = v.vendor_id
					   LEFT JOIN industries i ON v.industry_id = i.id
					   LEFT JOIN sub_industries si ON v.subindustry_id = si.id
					   WHERE d.expense_date >= '$start_date'
					   AND d.expense_date <= '$end_date'
					   AND e.expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY v.vendor_id )
					   ) a ORDER BY total DESC
					   LIMIT 10";
        
		//CVarDumper::dump($this->executeQuery($sql),10,1);die;
		return $this->executeQuery($sql);

	}

	public function getVendorsForOrder($start_date, $end_date)
	{
		// expenses first
		$totals = array();

		$sql = "SELECT o.vendor_id, v.vendor_name,v.contact_name,v.phone_1,v.phone_2,si.value as subindustry,i.value as industry, COUNT(DISTINCT o.order_id) AS `count`,
					   SUM(o.total_price + o.other_charges - o.discount) AS `total`
					   FROM orders o
					   LEFT JOIN order_details d ON o.order_id = d.order_id
					   LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
					   LEFT JOIN industries i ON v.industry_id = i.id
					   LEFT JOIN sub_industries si ON v.subindustry_id = si.id
					   WHERE o.order_date >= '$start_date'
					   AND o.order_date <= '$end_date'
					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY v.vendor_id
					   ORDER BY total DESC";
		foreach ($this->executeQuery($sql) as $row) {
			if (! isset($totals[$row['vendor_id']]))
				$totals[$row['vendor_id']] = $row;
			else {
				$totals[$row['vendor_id']]['count'] += $row['count'];
				$totals[$row['vendor_id']]['total'] += $row['total'];
			}
		}
		usort($totals, function ($a, $b) {
			return $b['total'] - $a['total'];
		});
		//CVarDumper::dump($totals,10,1);die;
		return $totals;
	}

	public function getVendorsForExpense($start_date, $end_date)
	{
		// expenses first
		$totals = array();

		$sql = "SELECT d.vendor_id, v.vendor_name,v.contact_name,v.phone_1,v.phone_2,si.value as subindustry,i.value as industry, COUNT(DISTINCT e.expense_id) AS `count`,
					   SUM(e.total_expenses) AS `total`
					   FROM expenses e
					   LEFT JOIN expense_details d ON e.expense_id = d.expense_id
					   LEFT JOIN vendors v ON d.vendor_id = v.vendor_id
					   LEFT JOIN industries i ON v.industry_id = i.id
					   LEFT JOIN sub_industries si ON v.subindustry_id = si.id
					   WHERE d.expense_date >= '$start_date'
					   AND d.expense_date <= '$end_date'
					   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY v.vendor_id
					   ORDER BY total DESC";



		foreach ($this->executeQuery($sql) as $row) {
			if (! isset($totals[$row['vendor_id']]))
				$totals[$row['vendor_id']] = $row;
			else {
				$totals[$row['vendor_id']]['count'] += $row['count'];
				$totals[$row['vendor_id']]['total'] += $row['total'];
			}
		}

		usort($totals, function ($a, $b) {
			return $b['total'] - $a['total'];
		});
		//CVarDumper::dump($totals,10,1);die;
		return $totals;

	}


  public function getNonAndPreferredVendorsByIndustry()
  {

    $sql = "SELECT sum(CASE WHEN preferred_flag=1 THEN 1 ELSE 0 END) as preferred,sum(CASE WHEN preferred_flag !=1 THEN 1 ELSE 0 END) as non_preferred,i.value as industry
             FROM vendors v
             INNER JOIN industries i ON v.industry_id = i.id
             where  v.member_type=0
             GROUP BY industry
             ORDER BY industry DESC";
    $recordReader = $this->executeQuery($sql);
    return $recordReader;

  }

	public function getNonPreferredVendorsForOrder($start_date, $end_date)
	{
		// expenses first
		$totals = array();

		$sql = "SELECT o.vendor_id, v.vendor_name,v.contact_name,v.phone_1,v.phone_2,si.value as subindustry,i.value as industry, COUNT(DISTINCT o.order_id) AS `count`,
					   SUM(o.total_price + o.other_charges - o.discount) AS `total`
					   FROM orders o
					   LEFT JOIN order_details d ON o.order_id = d.order_id
					   LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
					   LEFT JOIN industries i ON v.industry_id = i.id
					   LEFT JOIN sub_industries si ON v.subindustry_id = si.id
					   WHERE o.order_date >= '$start_date'
					   AND o.order_date <= '$end_date'
					   AND v.preferred_flag = 0
					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY v.vendor_id
					   ORDER BY total DESC";



		foreach ($this->executeQuery($sql) as $row) {
			if (! isset($totals[$row['vendor_id']]))
				$totals[$row['vendor_id']] = $row;
			else {
				$totals[$row['vendor_id']]['count'] += $row['count'];
				$totals[$row['vendor_id']]['total'] += $row['total'];
			}
		}

		usort($totals, function ($a, $b) {
			return $b['total'] - $a['total'];
		});
		//CVarDumper::dump($totals,10,1);die;
		return $totals;

	}

	public function getNonPreferredVendorsForExpense($start_date, $end_date)
	{
		// expenses first
		$totals = array();

		$sql = "SELECT d.vendor_id, v.vendor_name,v.contact_name,v.phone_1,v.phone_2,si.value as subindustry,i.value as industry, COUNT(DISTINCT e.expense_id) AS `count`,
					   SUM(e.total_expenses) AS `total`
					   FROM expenses e
					   LEFT JOIN expense_details d ON e.expense_id = d.expense_id
					   LEFT JOIN vendors v ON d.vendor_id = v.vendor_id
					   LEFT JOIN industries i ON v.industry_id = i.id
					   LEFT JOIN sub_industries si ON v.subindustry_id = si.id
					   WHERE d.expense_date >= '$start_date'
					   AND d.expense_date <= '$end_date'
					   AND v.preferred_flag = 0
					   AND e.expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
					   GROUP BY v.vendor_id
					   ORDER BY total DESC";



		foreach ($this->executeQuery($sql) as $row) {
			if (! isset($totals[$row['vendor_id']]))
				$totals[$row['vendor_id']] = $row;
			else {
				$totals[$row['vendor_id']]['count'] += $row['count'];
				$totals[$row['vendor_id']]['total'] += $row['total'];
			}
		}

		usort($totals, function ($a, $b) {
			return $b['total'] - $a['total'];
		});
		//CVarDumper::dump($totals,10,1);die;
		return $totals;

	}

	public function getVendorOrderAndAmounts($vendor_id,$from_date, $to_date)
	{

		$totals = array();
		// and then orders
		$sql = "SELECT o.*, v.vendor_name AS vendor, u.full_name AS user,u.user_id,v.vendor_id, cr.currency
		          FROM orders o
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN currency_rates cr ON o.currency_id = cr.id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  WHERE o.vendor_id = $vendor_id";

		if (!empty($from_date)) $sql .= " AND o.order_date >= '$from_date' ";
		if (!empty($to_date)) $sql .= " AND o.order_date <= '$to_date' ";

		return $this->executeQuery($sql);
	}


	public function getVendorsByMonth($start_date, $end_date)
    {
        // expenses first
        $totals = array();
        $sql = "SELECT d.vendor_id, d.expense_type_id AS category_id, e.value AS category, v.vendor_name, 
					   MONTH(d.expense_date) AS `month`, YEAR(d.expense_date) AS `year`, 'E' AS `type`, 
					   v.preferred_flag, SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, vendors v, expense_types e
 				 WHERE d.vendor_id = v.vendor_id 
   				   AND d.expense_type_id = e.id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date' 
			   	   AND d.expense_id IN (SELECT expense_id FROM expenses 
			   					 		 WHERE expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') )
 				 GROUP BY d.vendor_id, d.expense_type_id, YEAR(d.expense_date), MONTH(d.expense_date)";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT d.vendor_id, d.expense_type_id AS category_id, e.value AS category, v.vendor_name, 
						   MONTH(d.expense_date) AS `month`, YEAR(d.expense_date) AS `year`, 'E' AS `type`, 
						   v.preferred_flag, SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, vendors v, expense_types e
	 				 WHERE d.vendor_id = v.vendor_id 
	   				   AND d.expense_type_id = e.id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date' 
   				   	   AND d.expense_id IN (SELECT expense_id FROM expenses 
   				   					 		 WHERE expense_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') )
	 				 GROUP BY d.vendor_id, d.expense_type_id, YEAR(d.expense_date), MONTH(d.expense_date)";
        foreach ($this->executeQuery($sql) as $row)
            $totals[] = $row;
        
        // and then orders
        $sql = "SELECT o.vendor_id, v.vendor_name, v.preferred_flag, 
					   'O' AS `type`, c.id AS category_id, c.value AS category,
					   MONTH(o.order_date) AS `month`, YEAR(o.order_date) AS `year`, 
					   SUM(d.unit_price * ((100 + o.tax_rate) / 100)) AS `total`
  				  FROM order_details d, orders o, vendors v, products p, categories c
 				 WHERE d.order_id = o.order_id
   				   AND o.vendor_id = v.vendor_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
				   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND d.product_type = 'Product'
   				   AND d.product_id = p.product_id
   				   AND p.category_id = c.id 
 				 GROUP BY v.vendor_id, c.id, YEAR(o.order_date), MONTH(o.order_date)
	 
 				 UNION

				SELECT o.vendor_id, v.vendor_name, v.preferred_flag, 
					   'O' AS `type`, c.id AS category_id, c.value AS category, 
					   MONTH(o.order_date) AS `month`, YEAR(o.order_date) AS `year`, 
					   SUM(d.unit_price * ((100 + o.tax_rate) / 100)) / COUNT(DISTINCT b.product_id) AS `total`
  				  FROM order_details d, orders o, vendors v, products p, categories c, bundle_products b
 				 WHERE d.order_id = o.order_id
   				   AND o.vendor_id = v.vendor_id
   				   AND o.order_date >= '$start_date'
   				   AND o.order_date <= '$end_date' 
  					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND d.product_type = 'Bundle'
   				   AND b.bundle_id = d.product_id 
   				   AND b.product_id = p.product_id 
   				   AND p.category_id = c.id 
 				 GROUP BY b.bundle_id, v.vendor_id, c.id, YEAR(o.order_date), MONTH(o.order_date)";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT o.vendor_id, v.vendor_name, v.preferred_flag, 
						   'O' AS `type`, c.id AS category_id, c.value AS category,
						   MONTH(o.order_date) AS `month`, YEAR(o.order_date) AS `year`, 
						   SUM(d.unit_price) AS `total`
	  				  FROM order_details d, orders o, vendors v, products p, categories c
	 				 WHERE d.order_id = o.order_id
	   				   AND o.vendor_id = v.vendor_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND d.product_type = 'Product'
	   				   AND d.product_id = p.product_id
	   				   AND p.category_id = c.id 
   					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY v.vendor_id, c.id, YEAR(o.order_date), MONTH(o.order_date)
	 
	 				 UNION
	
					SELECT o.vendor_id, v.vendor_name, v.preferred_flag, 
						   'O' AS `type`, c.id AS category_id, c.value AS category, 
						   MONTH(o.order_date) AS `month`, YEAR(o.order_date) AS `year`, 
						   SUM(d.unit_price) / COUNT(DISTINCT b.product_id) AS `total`
	  				  FROM order_details d, orders o, vendors v, products p, categories c, bundle_products b
	 				 WHERE d.order_id = o.order_id
	   				   AND o.vendor_id = v.vendor_id
	   				   AND o.order_date >= '$start_date'
	   				   AND o.order_date <= '$end_date' 
	   				   AND d.product_type = 'Bundle'
	   				   AND b.bundle_id = d.product_id 
	   				   AND b.product_id = p.product_id 
	   				   AND p.category_id = c.id 
   					   AND o.order_status NOT IN ('Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	 				 GROUP BY b.bundle_id, v.vendor_id, c.id, YEAR(o.order_date), MONTH(o.order_date)";
        foreach ($this->executeQuery($sql) as $row)
            $totals[] = $row;
        
        $counts = array();
        foreach ($totals as $row) {
            $category_type = $row['type'];
            $category_id = $row['category_id'];
            $month = $row['month'];
            $year = $row['year'];
            
            $row_key = $year . sprintf("%02d", $month);
            $category_key = $category_type . "_" . $category_id;
            
            if (! isset($counts[$row_key]))
                $counts[$row_key] = array();
            if (! isset($counts[$row_key][$category_key])) {
                $counts[$row_key][$category_key] = array();
                $counts[$row_key][$category_key]['vendor_count'] = 0;
                $counts[$row_key][$category_key]['category_id'] = $category_key;
                $counts[$row_key][$category_key]['category_name'] = $row['category'];
            }
            
            $counts[$row_key][$category_key]['vendor_count'] += 1;
        }
        
        return $counts;
    }

    public function getDashboardMetrics($start_date, $end_date)
    {
        $metrics = array(
            'top_vendors_total' => 0,
            'vendors_count' => 0,
            'non_preferred_count' => 0,
            'non_preferred_total' => 0,
            'avg_per_category' => 0
        );
        // expenses first for every vendor I guess :(
        $totals = array();
        $vendors_used = array();
        $sql = "SELECT d.vendor_id, SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				  FROM expense_details d, expenses e
 				 WHERE e.expense_id = d.expense_id
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date'
			   	   AND d.expense_id IN (SELECT expense_id FROM expenses
			   					 		 WHERE expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') )
   				 GROUP BY d.vendor_id";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT d.vendor_id, SUM(d.expense_price) AS `total`
	  				  FROM expense_details d, expenses e
	 				 WHERE e.expense_id = d.expense_id
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date'
				   	   AND d.expense_id IN (SELECT expense_id FROM expenses
				   					 		 WHERE expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed'))
	 				 GROUP BY d.vendor_id";

        foreach ($this->executeQuery($sql) as $row) {
            if (! isset($totals[$row['vendor_id']]))
                $totals[$row['vendor_id']] = 0;
            $totals[$row['vendor_id']] += $row['total'];
            $vendors_used[] = $row['vendor_id'];
        }

        // and then orders too
        $sql = "SELECT vendor_id, SUM(total_price + other_charges - discount) AS `total`
  				  FROM orders o WHERE order_date >= '$start_date'
  				   AND order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				   AND order_date <= '$end_date' GROUP BY vendor_id";
        if (Yii::app()->session['exclude_tax_from_dashboard'])
            $sql = "SELECT vendor_id, SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `total`
	  				  FROM orders o WHERE order_date >= '$start_date'
	  				   AND order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
	   				   AND order_date <= '$end_date' GROUP BY vendor_id";
        
        foreach ($this->executeQuery($sql) as $row) {
            if (! isset($totals[$row['vendor_id']]))
                $totals[$row['vendor_id']] = 0;
            $totals[$row['vendor_id']] += $row['total'];
            if (! in_array($row['vendor_id'], $vendors_used))
                $vendors_used[] = $row['vendor_id'];
        }

		// order the totals array for top 10 and then get the sum
        rsort($totals);
        foreach (array_slice($totals, 0, 10) as $top_vendor_spend)
            $metrics['top_vendors_total'] += $top_vendor_spend;



        // various counts
        $metrics['vendors_count'] = count($vendors_used);


		$non_preffered_totals = array();
		$non_preffered_vendors_used = array();
		$sql = "SELECT d.vendor_id, SUM(d.expense_price + ((d.expense_price * d.tax_rate) / 100.0)) AS `total`
  				   FROM expense_details d, expenses e, vendors v
 				   WHERE e.expense_id = d.expense_id
 				   AND d.vendor_id = v.vendor_id 
   				   AND v.preferred_flag = 0
   				   AND d.expense_date >= '$start_date'
   				   AND d.expense_date <= '$end_date'
			   	   AND d.expense_id IN (SELECT expense_id FROM expenses
			   					 		 WHERE expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed') )
   				 GROUP BY d.vendor_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT d.vendor_id, SUM(d.expense_price) AS `total`
	  				   FROM expense_details d, expenses e ,vendors v
	 				   WHERE e.expense_id = d.expense_id
	 				   AND d.vendor_id = v.vendor_id
   				       AND v.preferred_flag = 0
	   				   AND d.expense_date >= '$start_date'
	   				   AND d.expense_date <= '$end_date'
				   	   AND d.expense_id IN (SELECT expense_id FROM expenses
				   					 		 WHERE expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed' ) )
	 				 GROUP BY d.vendor_id";

		foreach ($this->executeQuery($sql) as $row) {
			if (! isset($non_preffered_totals[$row['vendor_id']]))
				$non_preffered_totals[$row['vendor_id']] = 0;
			$non_preffered_totals[$row['vendor_id']] += $row['total'];
			$non_preffered_vendors_used[] = $row['vendor_id'];
		}

		// and then orders too
		$sql = "SELECT o.vendor_id, SUM(total_price + other_charges - discount) AS `total`
  				  FROM orders o,vendors v
  				  WHERE o.vendor_id = v.vendor_id
   				  AND v.preferred_flag = 0
  				  AND order_date >= '$start_date'
  				  AND order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed')
   				  AND order_date <= '$end_date' GROUP BY vendor_id";
		if (Yii::app()->session['exclude_tax_from_dashboard'])
			$sql = "SELECT o.vendor_id, SUM(total_price * ((100 - tax_rate) / 100) + other_charges - discount) AS `total`
	  				  FROM orders o,vendors v
	  				  WHERE o.vendor_id = v.vendor_id
   				      AND v.preferred_flag = 0
	  				  AND order_date >= '$start_date'
	  				  AND order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed' )
	   				  AND order_date <= '$end_date' GROUP BY vendor_id";

		foreach ($this->executeQuery($sql) as $row) {
			if (! isset($non_preffered_totals[$row['vendor_id']]))
				$non_preffered_totals[$row['vendor_id']] = 0;
			$non_preffered_totals[$row['vendor_id']] += $row['total'];
			if (! in_array($row['vendor_id'], $non_preffered_vendors_used))
				$non_preffered_vendors_used[] = $row['vendor_id'];
		}

		// preferred flag
        if (count($non_preffered_vendors_used)) {
            $vendor_preferences = array();
            $sql = "SELECT * FROM vendors WHERE preferred_flag = 0  AND  vendor_id IN ( " . implode(",", $non_preffered_vendors_used) . " ) ";



            foreach ($this->executeQuery($sql) as $vendor_data)
                $vendor_preferences[$vendor_data['vendor_id']] = $vendor_data['preferred_flag'];

			foreach ($non_preffered_totals as $vendor_id => $vendor_total) {
                if (! isset($vendor_preferences[$vendor_id]) || ! $vendor_preferences[$vendor_id]) {
                    $metrics['non_preferred_count'] += 1;
                    $metrics['non_preferred_total'] += $vendor_total;
                }
            }
        }

        // finally count total categories used
        $category_count = 0;
        $sql = "SELECT COUNT(DISTINCT expense_type_id) AS category_count FROM expense_details 
				 WHERE expense_date >= '$start_date' AND expense_date <= '$end_date'
			   	   AND expense_id IN (SELECT expense_id FROM expenses 
			   					 	   WHERE expense_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed' ) )";
        foreach ($this->executeQuery($sql) as $category_data)
            $category_count += $category_data['category_count'];
        $sql = "SELECT COUNT(DISTINCT p.category_id) AS category_count 
				  FROM order_details d, orders o, products p 
				 WHERE d.order_id = o.order_id AND d.product_id = p.product_id
				   AND d.product_type = 'Product'
				   AND o.order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed' )
				   AND o.order_date >= '$start_date' AND o.order_date <= '$end_date'";
        foreach ($this->executeQuery($sql) as $category_data)
            $category_count += $category_data['category_count'];
        $sql = "SELECT COUNT(DISTINCT p.category_id) AS category_count 
				  FROM order_details d, orders o, products p, bundles b, bundle_products bp 
				 WHERE d.order_id = o.order_id AND d.product_id = b.bundle_id 
				   AND d.product_type = 'Bundle' 
				   AND o.order_status NOT IN ( 'Declined', 'Cancelled','Pending','Submitted','Closed','More Info Needed' )
				   AND b.bundle_id = bp.bundle_id AND bp.product_id = p.product_id  
				   AND o.order_date >= '$start_date' AND o.order_date <= '$end_date'";
        foreach ($this->executeQuery($sql) as $category_data)
            $category_count += $category_data['category_count'];
        
        if (count($vendors_used) && $category_count)
            $metrics['avg_per_category'] = count($vendors_used) / $category_count;
        
        return $metrics;
    }

    public function getVendorCount($industry_id, $subindustry_id, $preferred_flag)
    {
        $sql = "SELECT COUNT(*) AS total_rows FROM vendors WHERE 1 = 1 and member_type=0";
        if ($industry_id)
            $sql .= " AND industry_id = $industry_id ";
        if ($subindustry_id)
            $sql .= " AND subindustry_id = $subindustry_id ";
        if ($preferred_flag != - 1)
            $sql .= " AND preferred_flag = $preferred_flag ";
        
        $count_data = $this->executeQuery($sql, true);
        return $count_data && is_array($count_data) && isset($count_data['total_rows']) ? $count_data['total_rows'] : 0;
    }

    public function exportVendor()
    {
        $currentDate = date('Y-m-d');
        $sql = "SELECT
                v.external_id as Company_Assigned_Supplier_ID,
                v.vendor_name as Name,
                i.value AS Industry,
                s.value AS Sub_Industry,
                GROUP_CONCAT(DISTINCT l.location_name SEPARATOR ', ') as Linked_Locations,
                GROUP_CONCAT(DISTINCT d.department_name SEPARATOR ', ') as Linked_Departments,
                pt.value as Payment_Term,
                COALESCE(vdoc.expired_document, 0) as Documents_Expired,
                COALESCE(vdoc.total_pending, 0) as Documents_To_Approve,
                CASE WHEN avg_score IS NOT NULL THEN CONCAT(FORMAT(10 * avg_score , 0), '%') ELSE '0%' END as Performance_Score,
                CASE
                 WHEN preferred_flag = 1
                  THEN 'Preferred'
                 WHEN preferred_flag = 0
                  THEN 'Non-Preferred'
                ELSE null
                END AS Preferred,
                CASE 
                 WHEN contract_id IS NOT NULL THEN 'Yes'
                 ELSE 'No'
                END AS Contract,
				v_sustainability.score_value as Sustainability,
                v_risk.risk_score_value as Risk
				  FROM vendors v
				  LEFT JOIN industries i ON v.industry_id = i.id
				  LEFT JOIN sub_industries s ON v.subindustry_id = s.id
                  LEFT JOIN ( select vendor_id,
                    sum(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END) as total_pending,
                    sum(CASE WHEN expiry_date <'".$currentDate."' THEN 1 ELSE 0 END) as expired_document
                    from vendor_documents where vendor_documents.archive !=1 group by vendor_id) vdoc ON v.vendor_id = vdoc.vendor_id
                  LEFT JOIN (
                        SELECT vendor_id, sum(score_value)/count(id) as avg_score
                        FROM vendor_performance
                        WHERE 1 GROUP BY vendor_id
                    ) as v_per ON v.vendor_id = v_per.vendor_id

                  LEFT JOIN (select count(inn_con.contract_id) as contract_id ,inn_con.vendor_id from contracts as inn_con where inn_con.contract_archive ='' or inn_con.contract_archive is null group by inn_con.vendor_id) con ON v.vendor_id = con.vendor_id
                  LEFT JOIN (select * from vendor_sustainability where id=(select max(in_vr.id) from vendor_sustainability in_vr where in_vr.vendor_id=vendor_sustainability.vendor_id) group by vendor_id) as v_sustainability on v.vendor_id=v_sustainability.vendor_id
                  LEFT JOIN (select * from vendor_risk where id=(select max(in_vr.id) from vendor_risk in_vr where in_vr.vendor_id=vendor_risk.vendor_id) group by vendor_id) as v_risk on v.vendor_id=v_risk.vendor_id
                  LEFT JOIN vendor_location vl ON v.vendor_id = vl.vendor_id            
                  LEFT JOIN locations l ON vl.location_id = l.location_id
                  LEFT JOIN vendor_department vd ON v.vendor_id = vd.vendor_id
                  LEFT JOIN departments d ON vd.department_id = d.department_id
                  LEFT JOIN payment_terms pt ON v.payment_term_id = pt.id

                 WHERE v.vendor_archive is null or v.vendor_archive=''
                 GROUP BY v.vendor_id
                 ORDER BY v.vendor_name asc ";
        parent::exportCommon(Yii::app()->db->createCommand($sql)->query()->readAll(), 'vendors');
    }

	public function getVendorContactData($vendor_id){

		 $vendor_sql = "SELECT contact_name,address_1,city,state,zip,phone_1,emails FROM vendors WHERE vendor_id = $vendor_id";
		return $this->executeQuery($vendor_sql);
	}

	public function updateVendor($vendor_submission_date){

		$contact_name = isset($vendor_submission_date['contact_name']) ? $vendor_submission_date['contact_name'] : '';
		$address_1 = isset($vendor_submission_date['address_1']) ? $vendor_submission_date['address_1'] : '';
		$city = isset($vendor_submission_date['city']) ? $vendor_submission_date['city'] : '';
		$state = isset($vendor_submission_date['state']) ? $vendor_submission_date['state'] : '';
		$zip = isset($vendor_submission_date['zip']) ? $vendor_submission_date['zip']: '';
		$phone_1 = isset($vendor_submission_date['phone_1']) ? $vendor_submission_date['phone_1'] : '';
		$vendor_id = isset($vendor_submission_date['vendor_id']) ? $vendor_submission_date['vendor_id'] : 0;
    //$email = isset($vendor_submission_date['emails']) ? $vendor_submission_date['emails'] : '';
        
		$update_sql = "UPDATE vendors
							  SET contact_name = '$contact_name',
							  	  address_1 = '$address_1',
							  	  city ='$city',
							  	  state = '$state',
							  	  zip = '$zip',
							  	  phone_1 = '$phone_1',
							  	  updated_datetime = NOW()
							WHERE vendor_id = $vendor_id";
		$this->executeQuery($update_sql);
	}

	public function importData($filename){

		$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

		if(in_array($filename["type"],$allowedFileType)){

			$targetPath = 'uploads/'.$filename['name'];
			move_uploaded_file($filename['tmp_name'], $targetPath);

			$reader = new SpreadsheetReader($targetPath);

			$sheetCount = count($reader->sheets());
			for($i=0;$i<$sheetCount;$i++)
			{
				$reader->ChangeSheet($i);

				foreach ($reader as $row)
				{
					//CVarDumper::dump($row,10,1);die;
					$vendor_name = "";
					if(isset($row[0])) {
						$vendor_name = $row[0];
					}
					$industry_id = "";
					if(isset($row[1])) {
						$industry_value  = $row[1];
						$result = $this->executeQuery("SELECT * FROM industries WHERE value = '$industry_value'", true);
						if(!empty($result)>0){
							$industry_id = $result['id'];
						} else {

							$this->executeQuery("INSERT INTO industries (`value`)
                                                           values('".$industry_value."')");
							$max = $this->executeQuery("SELECT MAX(id) as id FROM industries", true);
							$industry_id = $max['id'];
						}

					}
					$subindustry_id = "";
					if(isset($row[2])) {
						$subindustry_value  = $row[2];
						$result = $this->executeQuery("SELECT * FROM sub_industries WHERE value = '$subindustry_value'", true);
						if(!empty($result)>0){
							$subindustry_id = $result['id'];
						} else {

							$this->executeQuery("INSERT INTO sub_industries (`value`)
                                                           values('".$subindustry_value."')");
							$max = $this->executeQuery("SELECT MAX(id) as id FROM sub_industries", true);
							$subindustry_id = $max['id'];
						}
					}
					$preferred_flag = "";
					if(isset($row[3])) {
						$preferred_flag = $row[3];
					}
					$address_1 = "";
					if(isset($row[4])) {
						$address_1 = $row[4];
					}
					$address_2 = "";
					if(isset($row[5])) {
						$address_2 = $row[5];
					}
					$city = "";
					if(isset($row[6])) {
						$city = $row[6];
					}
					$state = "";
					if(isset($row[7])) {
						$state = $row[7];
					}
					$zip = "";
					if(isset($row[8])) {
						$zip = $row[8];
					}
					$country = "";
					if(isset($row[9])) {
						$country = $row[9];
					}
					$contact_name = "";
					if(isset($row[10])) {
						$contact_name = $row[10];
					}
					$phone_1 = "";
					if(isset($row[11])) {
						$phone_1 = $row[11];
					}
					$phone_2 = "";
					if(isset($row[12])) {
						$phone_2 = $row[12];
					}
					$fax = "";
					if(isset($row[13])) {
						$fax = $row[13];
					}
					$website = "";
					if(isset($row[14])) {
						$website = $row[14];
					}
					$emails = "";
					if(isset($row[15])) {
						$emails = $row[15];
					}
					$comments = "";
					if(isset($row[16])) {
						$comments = $row[16];
					}


					if (!empty($vendor_name)) {
						$result = $this->executeQuery("SELECT * FROM vendors WHERE vendor_name = '$vendor_name'", true);
						if(!$result){
							$this->executeQuery("INSERT INTO vendors (vendor_name,industry_id,subindustry_id,preferred_flag,address_1,address_2,city,state,zip,country,contact_name,phone_1,phone_2,fax,website,emails,comments)
                                                           values('".$vendor_name."','".$industry_id."','".$subindustry_id."','".$preferred_flag."','".$address_1."','".$address_2."','".$city."','".$state."','".$zip."','".$country."','".$contact_name."','".$phone_1."','".$phone_2."','".$fax."','".$website."','".$emails."','".$comments."')");
						}
					}
				}

			}

			echo $type = "success";

		}
		else
		{
			$type = "error";
			echo $message = "Invalid File Type. Upload Excel File.";
		}

	}

  public function generateRandomString($length = 10) 
  {
      $characters = '0123456789%@*abcdefghijklmnop!~qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++)
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      return $randomString;
  }

  public function welcomeEmail($vendor_id,$setPassword=1)
  {
    $passwordExp='';
    $vendor = new Vendor();
    //$vendorData = $vendor->getOne(array('vendor_id' => $vendor_id));

    $sql = "select * from vendors where vendor_id=".$vendor_id;
    $vendorData = Yii::app()->db->createCommand($sql)->queryRow($sql);
  
    if (empty($vendorData['emails'])) return 1;
    else
    {
      if(!empty($setPassword)){
        $passwordNew = $this->generateRandomString(10);
        $password = md5($passwordNew);
        $passwordExp .= '<strong>Password: </strong>' . $passwordNew . '<br/>';
      }else{
        $password = $vendorData['password'];
      }

      $this->executeQuery("UPDATE vendors SET password = '".$password."' WHERE vendor_id = ".$vendorData['vendor_id']);


      $mail = new \PHPMailer(true);
      $mail->IsSMTP();
      $mail->Host = Yii::app()->params['host'];
      $mail->Port = Yii::app()->params['port'];
      $mail->SMTPAuth = true;
      $mail->Username = Yii::app()->params['username'];
      $mail->Password = Yii::app()->params['password'];
      $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
	  $mail->CharSet="UTF-8";

      $email_body = '';

      $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
      $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
      $email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:1px;line-height:20px;white-space:nowrap;">&nbsp;</div>
         </td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
             <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#10a798;"><strong>Supplier Activation&nbsp;</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
      $email_body .= '<p>Dear ' . $vendorData['vendor_name'].', </p>';
      $email_body .= '<p>This is a notification from Spend 365. As per your request, your account has been activated as below:</p>';
      $email_body .= '<strong>Username: </strong>' . $vendorData['emails'] . '<br/>';
      $email_body .= $passwordExp;
      $email_body .= '<strong>URL: </strong><a target="_blank" href="'.Yii::app()->createAbsoluteUrl('supplier').'">'.Yii::app()->createAbsoluteUrl('supplier').'</a><br/>';
      $email_body .= '<p><strong>NOTE: </strong>If you did not request this change, please contact your administrator immediately.<p/>';
      $email_body .= '</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
    </table>
</div>';
      $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
      $mail->addAddress($vendorData['emails'], $vendorData['vendor_name']);
      $mail->Subject = 'Spend 365: Supplier Activation';
      $mail->isHTML(true);
      $mail->Body = $email_body;
      $mail->send();

      return 0;
    }
  }

  public function getVendorMetric(){
    $currentDate = date('Y-m-d');
     $data = array('totalVendors'=>0,'preferredVendors'=>0,'preferredVendorPercentage'=>0,'vendorUnderContract'=>0,'documentExpired'=>0,'vendorMissingDocuments'=>0, 'documentApproved'=>0);

     // Start: Query for Number of Preferred Suppliers
        $sql = "select sum(CASE WHEN preferred_flag=1 THEN 1 ELSE 0 END) as preferred 
            from vendors where profile_status='Active' AND (vendor_archive is null or vendor_archive = '' )";
        $preVerndor = Yii::app()->db->createCommand($sql)->queryRow();
        
        // Start: Preferred Supplier list
        $sql = "select v.*,i.value AS industry, s.value as subindustry from vendors v

            LEFT JOIN industries i ON v.industry_id = i.id
            LEFT JOIN sub_industries s ON v.subindustry_id = s.id

         where  preferred_flag=1 and profile_status='Active' AND (vendor_archive is null or vendor_archive = '' ) ORDER BY v.vendor_name ASC";
        $preVerndorList = Yii::app()->db->createCommand($sql)->query()->readAll();
        // End: Preferred Supplier list

        $preferred  = $preVerndor['preferred'];
        $data['preferredVendors'] = $preferred;
     // End: Query for Number of Preferred Suppliers

      // Start : Number Of Suppliers With A Contract List
        $sql = "select  v.*,i.value AS industry, s.value as subindustry  from vendors v
        INNER JOIN contracts c on v.vendor_id=c.vendor_id
        LEFT JOIN industries i ON v.industry_id = i.id
        LEFT JOIN sub_industries s ON v.subindustry_id = s.id
         where (c.contract_archive ='' or c.contract_archive is null ) AND v.profile_status='Active' and (v.vendor_archive='' or v.vendor_archive is null) ORDER BY v.vendor_name ASC";
        $vendorContractList = Yii::app()->db->createCommand($sql)->query()->readAll();
     // End : Number Of Suppliers With A Contract List

     // Start : Number Of Suppliers With A Contract 
        $sql = "select count(DISTINCT c.vendor_id) as vendor_under_contract from vendors v inner  join contracts c on v.vendor_id=c.vendor_id where (c.contract_archive ='' or c.contract_archive is null ) AND v.profile_status='Active' and (v.vendor_archive='' or v.vendor_archive is null)";
        $vendorContract = Yii::app()->db->createCommand($sql)->queryRow();
        $vendorContract = $vendorContract['vendor_under_contract'];
        $data['vendorUnderContract'] = $vendorContract;
     // End : Number Of Suppliers With A Contract


     // Start : Number Of Documents That Need Approval And Document Expired
        $sql =" select status = 'Approved' as total_approved,
        expiry_date <'".$currentDate."' as expired_document
         from vendor_documents where archive !=1 ";
        $document    = Yii::app()->db->createCommand($sql)->queryRow(); 
        $documentApp = $document['total_approved'];
        $documentExp = $document['expired_document'];

        // // Start : Number Of Documents That Need Approval And Document Expired LIST
        // // Start : Number Of Documents That Need Approval And Document Expired LIST
         
        $data['documentApproved'] = $documentApp;
        $data['documentExpired'] = $documentExp;
        $data['preVerndorList'] = $preVerndorList;

        $data['vendorUnderContractList'] = $vendorContractList;
     // End   : Number Of Documents That Need Approval And Document Expired

    // echo "<pre>"; print_r($documentApp); exit;

    return $data;
  }

  public function checkVendorApprovel($vendor_id = 0)
    {
        $sql =" select v.approver_id, v.approver_status, v.approver_comment, v.approver_datetime,u.full_name from vendors v, users u where v.approver_id = u.user_id and vendor_id=".$vendor_id;
        $vendorCheck = Yii::app()->db->createCommand($sql)->queryRow();
        return $vendorCheck;
    }
}
