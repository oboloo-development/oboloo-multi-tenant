<?php

class Subcategory extends Common
{
	public function __construct()
	{
		parent::__construct('id', 'sub_categories');
		$this->timestamp = false;
	}

	public function export($data = array(), $file = '')
	{
		$sql = "SELECT c.value AS category_name, s.value AS sub_category_name
		          FROM categories c, sub_categories s
		         WHERE c.id = s.code 
		         ORDER BY c.value, s.value";
		parent::export($this->executeQuery($sql), 'subcategories');
	}

}
