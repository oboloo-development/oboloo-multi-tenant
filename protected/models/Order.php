<?php

class Order extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'order_id' => 'N',
			'location_id' => 'N',
			'department_id' => 'N',
			'project_id' => 'N',
			'spend_type' => 'C',
			'vendor_id' => 'N',
			'user_id' => 'N',
			'user_name' => 'N',
			'user_email' => 'N',
			'currency_id' => 'N',
			'currency_rate' => 'N',
			'created_by_user_id' => 'N',
			'order_date' => 'D',
			'description' => 'C',
			'order_status' => 'C',
			'total_price' => 'N',
			'calc_price' => 'N',
			'tax_rate' => 'N',
			'ship_amount' => 'N',
			'invoice_number' => 'C',
			'recurrent_number' => 'C'
		);

		$this->currency_fields = array ( 'total_price', 'ship_amount', 'other_charges', 'discount' );
		parent::__construct('order_id', 'orders');
	}

	public function getOrders($order_id = 0, $spend_type = 0, $location_id = 0, $department_id = 0, $order_status = "", $from_date = "", $to_date = "")
	{
		$sql = "SELECT o.*, v.vendor_name AS vendor, u.full_name AS user,u.user_id,v.vendor_id, cr.currency
		          FROM orders o
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN currency_rates cr ON o.currency_id = cr.id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id";

		if (!isset(Yii::app()->session['user_type']) || Yii::app()->session['user_type']=="2")
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			if (empty($order_id))
				$sql .= " WHERE ( o.user_id = $my_user_id OR o.created_by_user_id = $my_user_id )";
		}
		else
		{
			if (empty($order_id)) $sql .= " WHERE o.order_id > 0 ";
		}

		if ($order_id)
			return $this->executeQuery($sql . " WHERE o.order_id = $order_id ", true);
		else 
		{
			if (!empty($location_id)) {
				$location_id = implode(',',$location_id);
				$sql .= " AND o.location_id IN ($location_id)";
			}

			/*if (!empty($department_id)) {
				$sql .= " AND o.department_id = $department_id";
			}*/

			if(!empty($department_id)) {
				$department_id = implode(',',$department_id);
				$sql .= " AND o.department_id IN ($department_id)";
			}
			
			if (!empty($order_status)){
				$status = "'" . implode("','", $order_status) . "'";
				$sql .= " AND o.order_status IN ($status)";
			}
			if (!empty($spend_type))
				$sql .= " AND o.spend_type = '$spend_type'";

			if (!empty($from_date)) $sql .= " AND o.order_date >= '$from_date' ";
			if (!empty($to_date)) $sql .= " AND o.order_date <= '$to_date' ";

			return $this->executeQuery($sql . ' ORDER BY o.order_date DESC ');
		}


	}

	public function orderInvoice($order_id,$vendor_id=0)
	{
		$whereCond = '';
		if(!empty($order_id)){
			$whereCond = ' and order_id='.$order_id;
		}if(!empty($vendor_id)){
			$whereCond .= ' and vendor_id='.$vendor_id;
		}
		$sql = "
				SELECT * FROM (
				SELECT order_invoices.*,'vendor' as type from order_invoices where created_by_type=1
					UNION
				select order_invoices.*,'internal' as type from order_invoices where created_by_type=2
				) AS order_invoice where 1 ".$whereCond;
			return $this->executeQuery($sql . ' ORDER BY id DESC ');

	}

	public function orderConsolidateInvoice($invoice_num,$order_id)
	{
		$sql = " SELECT * from order_invoices where status=2 and invoice_number='".$invoice_num."' and order_id=".$order_id;
		return $this->executeQuery($sql . ' ORDER BY id DESC ');
	}


	public function getRelatedOrders($order_id)
	{
		if(empty($order_id)){
			$order_id = 0;
			$recurrent_number = '-1456';
		}else{
			$currentOrder = $this->getOne(array('order_id' => $order_id));

			if(!empty($currentOrder['recurrent_number']))
			    $recurrent_number = $currentOrder['recurrent_number'];
			    else
					$recurrent_number = '-1456';

		}

		$sql1 = "SELECT o.*, v.vendor_name AS vendor, u.full_name AS user,u.user_id,v.vendor_id, cr.currency
		          FROM orders o
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN currency_rates cr ON o.currency_id = cr.id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  WHERE recurrent_number = $recurrent_number AND  o.order_id != $order_id ";
		//CVarDumper::dump($this->executeQuery($sql1 . ' ORDER BY o.order_date DESC '),10,1);die;
		return $this->executeQuery($sql1 . ' ORDER BY o.order_id DESC ');

	}

	public function getSupplierOrders($vendor_id,$from_date,$to_date)
	{
		$tool_currency = Yii::app()->session['user_currency'];
		$sql = "SELECT o.*,getTotalInGBandToolCurrency(GetOrderTotalAmount(o.order_id),o.currency_id,'".$tool_currency."') as total_price,GetOrderTotalAmount(o.order_id) as calc_price,v.vendor_name AS vendor, u.full_name AS user,u.user_id,v.vendor_id, cr.currency
		          FROM orders o
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN currency_rates cr ON o.currency_id = cr.id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  WHERE o.vendor_id = $vendor_id";

		          if (!empty($from_date)) $sql .= " AND o.order_date >= '$from_date' ";
		          if (!empty($to_date)) $sql .= " AND o.order_date <= '$to_date' ";

			      return $this->executeQuery($sql . ' ORDER BY o.order_date DESC ');

	}

	public function getProjectOrders($projectID,$from_date,$to_date)
	{
		$tool_currency = Yii::app()->session['user_currency'];
		$sql = "SELECT o.*,getTotalInGBandToolCurrency(GetOrderTotalAmount(o.order_id),o.currency_id,'".$tool_currency."') as total_price,GetOrderTotalAmount(o.order_id) as calc_price,v.vendor_name AS vendor, u.full_name AS user,u.user_id,v.vendor_id,p.project_name,p.project_id, cr.currency
		          FROM orders o
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN currency_rates cr ON o.currency_id = cr.id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  LEFT JOIN projects p ON o.project_id = p.project_id
				  WHERE o.project_id = $projectID";

		          //if (!empty($from_date)) $sql .= " AND o.order_date >= '$from_date' ";
		          //if (!empty($to_date)) $sql .= " AND o.order_date <= '$to_date' ";

			      return $this->executeQuery($sql . ' ORDER BY o.order_date DESC ');

	}

	

	public function getSupplierDocs($order_id)
	{
		return $this->executeQuery("SELECT * FROM order_vendor_files WHERE order_id = $order_id");

	}

	public function getInvoices($location_id = 0, $department_id = 0, $order_status = "", $from_date = "", $to_date = "")
	{
		$sql = "SELECT o.*, v.vendor_name AS vendor, u.full_name AS user
		          FROM (SELECT inn_ord.*,ord_inv.file_name,ord_inv.status as inv_status,ord_inv.invoice_number as inv_number,ord_inv.id as inv_id,ord_inv.created_at as inv_created_at  FROM `order_invoices` ord_inv right join orders inn_ord on ord_inv.order_id=inn_ord.order_id where ord_inv.status = 2 group by inv_number) o
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				 WHERE 1";

		if (!isset(Yii::app()->session['user_type']))
		{
			$my_user_id = Yii::app()->session['user_id'];
			if (empty($my_user_id)) $my_user_id = '0';
			$sql .= " WHERE ( o.user_id = $my_user_id OR o.created_by_user_id = $my_user_id )";
		}

		if (!empty($location_id)) {
			$location_id = implode(',',$location_id);
			$sql .= " AND o.location_id IN ($location_id)";
		}

		/*if (!empty($department_id)) {
			$sql .= " AND o.department_id = $department_id";
		}*/

		if(!empty($department_id)) {
			$department_id = implode(',',$department_id);
			$sql .= " AND o.department_id IN ($department_id)";
		}

		if (!empty($order_status)){
			$status = "'" . implode("','", $order_status) . "'";
			$sql .= " AND o.order_status IN ($status)";
		}
		if (!empty($from_date)) $sql .= " AND o.order_date >= '$from_date' ";
		if (!empty($to_date)) $sql .= " AND o.order_date <= '$to_date' ";
						
		return $this->executeQuery($sql);
	}

	public function createRouteForApproval($order_id = 0)
	{
		// do we already have a route created? then don't bother
		$route_created = false;
		$order_route = $this->executeQuery("SELECT * FROM order_routing WHERE approval_type = 'O' AND order_id = $order_id");
		if ($order_route && is_array($order_route) && count($order_route))
			foreach ($order_route as $approval_node) $route_created = true;

		// create the first entry only
		if ($route_created)
		{
			// get the order data and change approvals from more info to submitted again
			$order_data = $this->executeQuery("SELECT * FROM orders WHERE order_id = $order_id", true);
			$order_status = is_array($order_data) && isset($order_data['order_status']) ? $order_data['order_status'] : "Pending";
			if ($order_status == 'Submitted')
			{   $approvalUsers = array();
				foreach ($order_route as $approval_node)
				{
					$approval_status = $approval_node['routing_status'];
					$approval_routing_id = $approval_node['id'];
					if ($approval_status == 'Information'){
						$this->executeQuery("UPDATE order_routing SET routing_status = 'Pending' WHERE id = $approval_routing_id");
					
					if ($order_route[0]['status'] == 1)
					{   $approvalUsers[]=$approval_node['user_id'];
						$notification = new Notification();
						$notification->rs = array();
						$notification->rs['id'] = 0;
						$notification->rs['user_id'] = $approval_node['user_id'];
						$notification->rs['notification_text'] =
							'<a href="' . AppUrl::bicesUrl('routing/edit/' . $order_route[0]['id']) . '">Order ID #' 
							. $order_id . '</a>' . " has been submitted for your approval.";
						$notification->rs['notification_date'] = date("Y-m-d H:i");
						$notification->rs['read_flag'] = 0;
						$notification->write();
					}

					//$this->orderSubmitted($approval_node['user_id'],$approval_node['order_id']);
					// send mail to the user that some approval is necessary
					}
				}
				if(!empty($approvalUsers))
					foreach($approvalUsers as $approver){
						$this->orderApproval($approver,$order_id);
						//$this->orderSubmitted($approver,$order_id);
					}
					// and may be also to the order creator that the order has been submitted
			}
		}
		else
		{
			// get the order data
			$order_data = $this->executeQuery("SELECT * FROM orders WHERE order_id = $order_id", true);

			// routing is based on location and department
			$location_id = is_array($order_data) && isset($order_data['location_id']) ? $order_data['location_id'] : 0;
			$department_id = is_array($order_data) && isset($order_data['department_id']) ? $order_data['department_id'] : 0;
			$order_price = is_array($order_data) && isset($order_data['total_price']) ? $order_data['total_price'] : 0;
			$order_status = is_array($order_data) && isset($order_data['order_status']) ? $order_data['order_status'] : "Pending";

			if ($order_status != "Pending")
			{
				$route = new OrderRoute();
				$route_data = $route->getUsersApprover($location_id, $department_id,"approve_order");


				if ($route_data && is_array($route_data) && count($route_data))
				{
					// insert into order routing the first user as per level
					$route_created = true;
					$approvalUsers = array();
					foreach ($route_data as $route_node)
					{
						$approval_user_id = isset($route_node['user_id']) ? $route_node['user_id'] : 0;
						if ($approval_user_id)
						{	$approvalUsers[]=$approval_user_id;
							$order_route = new OrderRoute();
							$order_route->rs['id'] = 0;
							$order_route->rs['approval_type'] = 'O';
							$order_route->rs['order_id'] = $order_id;
							$order_route->rs['user_id'] = $approval_user_id;
							$order_route->rs['routing_status'] = 'Pending';
							$order_route->rs['status'] = 1;
							$order_route->rs['routing_date'] = date("Y-m-d");
							$order_route->write();
							$approval_id = $order_route->rs['id'];
							// create notifications
							if ($order_route->rs['status'] == 1)
							{
								$notification = new Notification();
								$notification->rs = array();
								$notification->rs['id'] = 0;
								$notification->rs['user_id'] = $approval_user_id;
								$notification->rs['notification_text'] =
									'<a href="' . AppUrl::bicesUrl('routing/edit/' . $approval_id) . '">Order ID #' 
									. $order_id . '</a>' . " has been submitted for your approval.";
								$notification->rs['notification_date'] = date("Y-m-d H:i");
								$notification->rs['read_flag'] = 0;
								$notification->write();
							}
						}
					}

					// send mail to the user that some approval is necessary
					if(!empty($approvalUsers))
						foreach($approvalUsers as $approver){
							$this->orderApproval($approver,$order_id);
							//$this->orderSubmitted($approver,$order_id);
							// and may be also to the order creator that the order has been submitted
						}
				    

				}
			}
		}

		return $route_created;
	}

	public function saveOrderFile($order_id, $file_name, $file_description)
	{
		//$this->deleteFile($order_id, $file_name);
		return $this->executeQuery("INSERT INTO order_files (order_id, file_name, description) SELECT $order_id, '$file_name', '$file_description'");
	}

	public function deleteFile($order_id, $file_name)
	{
		$this->executeQuery("DELETE FROM order_files WHERE order_id = $order_id AND file_name = '$file_name'");
	}

	public function orderApproval($user_id,$order_id)
	{


		$user_data = $this->executeQuery("SELECT * FROM users WHERE user_id = $user_id",1);
		$order_owner = $this->executeQuery("SELECT *
          FROM orders
        INNER JOIN users ON orders.user_id=users.user_id WHERE orders.order_id = $order_id",1);

		$order_data = $this->executeQuery("SELECT products.product_name,det.calc_unit_price,det.quantity,GetOrderTotalAmount(det.order_id) as converted_total_amount FROM order_details det
                                           INNER JOIN products ON det.product_id=products.product_id WHERE det.order_id = $order_id");

		$mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];

		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
			$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
			$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;">';
        $email_body .= '<p>Dear ' . $user_data['full_name'].', </p>';
		$email_body .= '</td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">';
		$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#000000;"><strong>You have received an order from <span style="color:#10a798;">' . $order_owner['full_name'].'</span> </strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		    $email_body .= '<p>Please find details of the order in the table below:</p>';
		    $email_body .= '<table  border="0" cellpadding="2" cellspacing="1" width="600" align="center" style="width:600px;">
                            <tr>
                            <td style="width: 49%"><b>Product Name</b></td>
                            <td style="width: 15%"><b>Product Type</b></td>
                            <td style="width: 12%"><b>Unit Price</b></td>
                            <td style="width: 11%"><b>Quantity</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                            </tr>
                            <tr><td colspan="5">&nbsp;</td></tr>';

		    foreach($order_data as $data){
		    	$currency = FunctionManager::orderCurrSymbol($order_owner['order_id']);
				$email_body .= '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $order_owner['spend_type'].'</td>
                            <td>' . $currency.$data['calc_unit_price'].'</td>
                            <td style="text-align: center">' . $data['quantity'].'</td>
                            <td>' . $currency.$data['converted_total_amount'].'</td>
                            </tr>';
			}
		    $email_body .= '</table></div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:11px;text-align: center;">';
             $email_body .= '<p><span style="color:#10a798;">This order is powered by Spend 365  LTD</span></p>';
             $email_body .= '</td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
			$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
			$mail->addAddress($user_data['email'], $user_data['full_name']);
			$mail->Subject = 'Spend 365: Order Approval';
			$mail->isHTML(true);
			$mail->Body = $email_body;
			$mail->send();
			return 0;
	}

	public function orderChangeEmail($order_id)
	{


		
		$order_owner = $this->executeQuery("SELECT *
          FROM orders
        INNER JOIN users ON orders.user_id=users.user_id WHERE orders.order_id = $order_id",1);

		$order_data = $this->executeQuery("SELECT products.product_name,det.calc_unit_price,det.quantity,GetOrderTotalAmount(det.order_id) as converted_total_amount FROM order_details det
                                           INNER JOIN products ON det.product_id=products.product_id WHERE det.order_id = $order_id");
		$status = ucwords($order_owner['order_status']);
		$subject = 
		$mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];

		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
			$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
			$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;">';
        $email_body .= '<p>Dear ' . $order_owner['full_name'].', </p>';
		$email_body .= '</td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">';
		$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#000000;"><strong>You order has been '.$status.'</strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		    $email_body .= '<p>Please find details of the order in the table below:</p>';
		    $email_body .= '<table  border="0" cellpadding="2" cellspacing="1" width="600" align="center" style="width:600px;">
                            <tr>
                            <td style="width: 49%"><b>Product Name</b></td>
                            <td style="width: 15%"><b>Product Type</b></td>
                            <td style="width: 12%"><b>Unit Price</b></td>
                            <td style="width: 11%"><b>Quantity</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                            </tr>
                            <tr><td colspan="5">&nbsp;</td></tr>';

		    foreach($order_data as $data){
		    	$currency = FunctionManager::orderCurrSymbol($order_owner['order_id']);
				$email_body .= '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $order_owner['spend_type'].'</td>
                            <td>' . $currency.$data['calc_unit_price'].'</td>
                            <td style="text-align: center">' . $data['quantity'].'</td>
                            <td>' . $currency.$data['converted_total_amount'].'</td>
                            </tr>';
			}
		    $email_body .= '</table></div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:11px;text-align: center;">';
             $email_body .= '<p><span style="color:#10a798;">This order is powered by Spend 365  LTD</span></p>';
             $email_body .= '</td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;" align="left">
                 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                     <p>Terms of Service &amp; User Agreement</p>
                     <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                     <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                 </div>
             </td>
         </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
			$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
			$mail->addAddress($order_owner['email'], $order_owner['full_name']);
			$mail->Subject = 'Spend 365: Order '.$status;
			$mail->isHTML(true);
			$mail->Body = $email_body;
			$mail->send();
			return 0;
	}


	public function orderSubmitted($user_id,$order_id)
	{


		$user_data = $this->executeQuery("SELECT *
          FROM users WHERE user_id = $user_id",1);

		$order_data = $this->executeQuery("SELECT * FROM order_details
                                           INNER JOIN products ON order_details.product_id=products.product_id WHERE order_details.order_id = $order_id");

		$mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];

		$email_body = '';

		$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td style="vertical-align:top;width:600px;">
        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
        <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
         <tbody>
         <tr>
          <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
          <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
              <tbody>
              <tr>
                  <td style="width:600px;">';
		$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
		$email_body .= '</td>
              </tr>
              </tbody>
          </table>
          </td>
         </tr>
         <tr>
         <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;">';
		$email_body .= '<p>Dear ' . $user_data['full_name'].', </p>';
		$email_body .= '</td>
         </tr>
         <tr>
         <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">';
		$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                 <h1 style="font-family: Cabin; line-height: 100%;">
                     <span style="color:#000000;"><strong>New order confirmation <span style="color:#10a798;">-' .$order_id.'</span> </strong></span>
                 </h1>
             </div>
         </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">';
		$email_body .= '<p>You have requested a new order for the below product(s). Please see details below.</p>';
		$email_body .= '<table  border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                            <tr>
                            <td style="width: 49%"><b>Product Name</b></td>
                            <td style="width: 15%"><b>Product Type</b></td>
                            <td style="width: 12%"><b>Unit Price</b></td>
                            <td style="width: 11%"><b>Quantity</b></td>
                            <td style="width: 13%"><b>Total Price</b></td>
                            </tr>
                            <tr><td colspan="5">&nbsp;</td></tr>';

		foreach($order_data as $data){
			$email_body .= '<tr>
                            <td>' . $data['product_name'].'</td>
                            <td>' . $data['product_type'].'</td>
                            <td>' . $data['unit_price'].'</td>
                            <td style="text-align: center">' . $data['quantity'].'</td>
                            <td>' . $data['quantity']*$data['unit_price'].'</td>
                            </tr>';
		}
		$email_body .= '</table></div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:11px;">';
		$email_body .= '<p>You will receive email confirmation once the order status has been updated by the approver(s).</p>';
		$email_body .= '</td>
         </tr>

         <tr>
             <td style="word-wrap:break-word;font-size:0px;">
             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
             </td>
         </tr>

         <tr>
               <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                <a href="' .AppUrl::bicesUrl('orders/edit/'.$order_id).'" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Order Status</a>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;">
					 <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
					 </td>
				 </tr>

				 <tr>
					 <td style="word-wrap:break-word;font-size:0px;" align="left">
						 <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
							 <p>Terms of Service &amp; User Agreement</p>
							 <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
							 <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
						 </div>
					 </td>
				 </tr>
         </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div></td>
        </tr>
        </table>
        </div>';
		$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
		$mail->addAddress($user_data['email'], $user_data['full_name']);
		$mail->Subject = 'Spend 365: Order Submitted';
		$mail->isHTML(true);
		$mail->Body = $email_body;
		$mail->send();

		return 0;
	}

	public function export($data = array(), $file = '')
	{

		$order_data_with_details = $order_data = array();		
		$sql = "SELECT o.order_id,o.order_date, location_name, department_name,project_name,spend_type,vendor_name, full_name,
						DATE_FORMAT(order_date, '%d-%b-%Y') AS order_date, description,cr.currency,cr.rate,order_status,
						od.quantity,od.calc_unit_price,calc_tax,shipping_cost, (od.calc_unit_price*quantity+calc_tax+shipping_cost) as total_price,
						od.product_id,p.product_name,pcr.currency as product_currency,od.unit_price as original_product_price
				  FROM orders o
				  LEFT JOIN locations l ON o.location_id = l.location_id
				  LEFT JOIN departments d ON o.department_id = d.department_id
				  LEFT JOIN projects pr ON o.project_id = pr.project_id
				  LEFT JOIN currency_rates cr ON o.currency_id = cr.id
				  LEFT JOIN vendors v ON o.vendor_id = v.vendor_id
				  LEFT JOIN users u ON o.user_id = u.user_id
				  LEFT JOIN order_details od ON o.order_id = od.order_id
				  LEFT JOIN products p ON od.product_id = p.product_id
				  LEFT JOIN currency_rates pcr ON p.currency_id = pcr.id
				  ORDER BY o.order_id";
		foreach ($this->executeQuery($sql) as $order) $order_data[$order['order_id']] = $order;

//		if (count($order_data))
//		{
//			$order_details = array();
//			$sql = "SELECT d.*, p.product_name FROM order_details d, products p
//					 WHERE d.product_id = p.product_id AND order_id IN ( " . implode(",", array_keys($order_data)) . " ) ";
//			foreach ($this->executeQuery($sql) as $order_item)
//			{
//				if (!isset($order_details[$order_item['order_id']]))
//					$order_details[$order_item['order_id']] = array();
//				$order_details[$order_item['order_id']][] = $order_item;
//			}
//
//			foreach ($order_data as $order_id => $order)
//			{
////				$products = array();
////				if (isset($order_details[$order_id]))
////					foreach ($order_details[$order_id] as $order_item)
////						$products[] = array('product_name' => $order_item['product_name'],
////							'product_quantity' => $order_item['quantity'], 'total_price' => $order_item['unit_price']);
////				$order['products'] = $products;
//				$order_data_with_details[] = $order;
//			}
//		}

		//CVarDumper::dump($this->executeQuery($sql),10,1);die;

		parent::export($this->executeQuery($sql), 'orders');
	}

	public function aclRules(){

		$acl = $this->executeQuery("SELECT * FROM user_acl WHERE user_id = '".Yii::app()->session['user_id']."' ", true);
		return $acl_data = array(
			'0'=> array(
				'location'=>$acl['london_procurement_location'],
				'department'=>$acl['london_procurement_department'],
				'order'=>$acl['london_procurement_order'],
				'travel_expense'=>$acl['london_procurement_travel_expense'],
				'invoice'=>$acl['london_procurement_invoice'],
				'amend_budget'=>$acl['london_procurement_amend_budget'],
			),
			'1'=> array(
				'location'=>$acl['london_admin_location'],
				'department'=>$acl['london_admin_department'],
				'order'=>$acl['london_admin_order'],
				'travel_expense'=>$acl['london_admin_travel_expense'],
				'invoice'=>$acl['london_admin_invoice'],
				'amend_budget'=>$acl['london_admin_amend_budget'],
			),
			'2'=> array(
				'location'=>$acl['london_it_location'],
				'department'=>$acl['london_it_department'],
				'order'=>$acl['london_it_order'],
				'travel_expense'=>$acl['london_it_travel_expense'],
				'invoice'=>$acl['london_it_invoice'],
				'amend_budget'=>$acl['london_it_amend_budget'],
			),
			'3'=> array(
				'location'=>$acl['london_facilities_location'],
				'department'=>$acl['london_facilities_department'],
				'order'=>$acl['london_facilities_order'],
				'travel_expense'=>$acl['london_facilities_travel_expense'],
				'invoice'=>$acl['london_facilities_invoice'],
				'amend_budget'=>$acl['london_facilities_amend_budget'],
			),
			'4'=> array(
				'location'=>$acl['berlin_admin_location'],
				'department'=>$acl['berlin_admin_department'],
				'order'=>$acl['berlin_admin_order'],
				'travel_expense'=>$acl['berlin_admin_travel_expense'],
				'invoice'=>$acl['berlin_admin_invoice'],
				'amend_budget'=>$acl['berlin_admin_amend_budget'],
			),
			'5'=> array(
				'location'=>$acl['berlin_it_location'],
				'department'=>$acl['berlin_it_department'],
				'order'=>$acl['berlin_it_order'],
				'travel_expense'=>$acl['berlin_it_travel_expense'],
				'invoice'=>$acl['berlin_it_invoice'],
				'amend_budget'=>$acl['berlin_it_amend_budget'],
			),
			'6'=> array(
				'location'=>$acl['berlin_facilities_location'],
				'department'=>$acl['berlin_facilities_department'],
				'order'=>$acl['berlin_facilities_order'],
				'travel_expense'=>$acl['berlin_facilities_travel_expense'],
				'invoice'=>$acl['berlin_facilities_invoice'],
				'amend_budget'=>$acl['berlin_facilities_amend_budget'],
			),
			'7'=> array(
				'location'=>$acl['mumbai_admin_location'],
				'department'=>$acl['mumbai_admin_department'],
				'order'=>$acl['mumbai_admin_order'],
				'travel_expense'=>$acl['mumbai_admin_travel_expense'],
				'invoice'=>$acl['mumbai_admin_invoice'],
				'amend_budget'=>$acl['mumbai_admin_amend_budget'],
			),
			'8'=> array(
				'location'=>$acl['mumbai_procurement_location'],
				'department'=>$acl['mumbai_procurement_department'],
				'order'=>$acl['mumbai_procurement_order'],
				'travel_expense'=>$acl['mumbai_procurement_travel_expense'],
				'invoice'=>$acl['mumbai_procurement_invoice'],
				'amend_budget'=>$acl['mumbai_procurement_amend_budget'],
			),
			'9'=> array(
				'location'=>$acl['newyork_procurement_location'],
				'department'=>$acl['newyork_procurement_department'],
				'order'=>$acl['newyork_procurement_order'],
				'travel_expense'=>$acl['newyork_procurement_travel_expense'],
				'invoice'=>$acl['newyork_procurement_invoice'],
				'amend_budget'=>$acl['newyork_procurement_amend_budget'],
			),
			'10'=> array(
				'location'=>$acl['newyork_admin_location'],
				'department'=>$acl['newyork_admin_department'],
				'order'=>$acl['newyork_admin_order'],
				'travel_expense'=>$acl['newyork_admin_travel_expense'],
				'invoice'=>$acl['newyork_admin_invoice'],
				'amend_budget'=>$acl['newyork_admin_amend_budget'],
			),
			'11'=> array(
				'location'=>$acl['newyork_it_location'],
				'department'=>$acl['newyork_it_department'],
				'order'=>$acl['newyork_it_order'],
				'travel_expense'=>$acl['newyork_it_travel_expense'],
				'invoice'=>$acl['newyork_it_invoice'],
				'amend_budget'=>$acl['newyork_it_amend_budget'],
			)
		);
	}

	public function approveStatus($location,$department){

            $acl = $this->aclRules();

			foreach($acl as $ac):
				if($ac['location']==$location && $ac['department']==$department){
					return array(
						'order'=>$ac['order'],
						'travel_expense'=>$ac['travel_expense'],
						'invoice'=>$ac['invoice'],
						'amend_budget'=>$ac['amend_budget']
					);
				}
			endforeach;
	}


	public function approveLocationStatus($location){

		$acl = $this->aclRules();
		foreach($acl as $ac):
				if($ac['location']==$location){
					return array(
						'order'=>$ac['order'],
						'travel_expense'=>$ac['travel_expense'],
						'invoice'=>$ac['invoice'],
						'amend_budget'=>$ac['amend_budget']
					);
				}
			endforeach;

	}


}
