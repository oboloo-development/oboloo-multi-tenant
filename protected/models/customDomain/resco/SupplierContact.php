<?php

class SupplierContact extends Common
{
	public function __construct()
	{

		$this->fields = array (
			'id' => 'N',
			'vendor_id' => 'N',
			'contact_name' => 'C',
			'contact_email'=>'C',
			'created_by_id' => 'N',
			'created_by_name' => 'N',
			'contact_position' => 'C',
			'contact_phone_no' => 'C',
			'contact_location' => 'C',
			'created_at' => 'D',
		);

		parent::__construct('id', 'supplier_contacts');
		$this->timestamp = false;
	}

	
}
