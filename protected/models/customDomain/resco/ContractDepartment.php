<?php

class ContractDepartment extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'contract_id' => 'N',
			'location_id' => 'N',
			'department_id' => 'N'
		);
		parent::__construct('id', 'contract_department');
		$this->timestamp = false;
	}
	
}
