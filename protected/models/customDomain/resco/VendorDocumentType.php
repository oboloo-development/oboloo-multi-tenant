<?php
class VendorDocumentType extends Common
{
	public function __construct()
	{	
		$this->fields = array (
			'id' => 'N',
			'name' => 'C',
			'soft_deleted' => 'N',
		);
		parent::__construct('id', 'vendor_document_type');
		$this->timestamp = false;
	}
}