<?php
class ContractDocumentType extends Common
{
	public function __construct()
	{	
		$this->fields = array (
			'id' => 'N',
			'name' => 'C'
		);
		parent::__construct('id', 'contract_document_type');
		$this->timestamp = false;
	}
}