<?php

class VendorDepartment extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'id' => 'N',
			'vendor_id' => 'N',
			'location_id' => 'N',
			'department_id' => 'N'
		);
		parent::__construct('id', 'vendor_department');
		$this->timestamp = false;
	}
	
}
