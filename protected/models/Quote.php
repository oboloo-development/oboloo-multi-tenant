<?php

require_once('protected/vendors/phpmailer/PHPMailerAutoload.php');

class Quote extends Common
{

	public function __construct()
	{
		$this->fields = array (
			'quote_id' => 'N',
			'location_id' => 'N',
			'department_id' => 'N',
			'category_id' => 'N',
			'subcategory_id' => 'N',
			'project_id' => 'N',
			'awarded_contract_id'=>'N',
			'spend_type' => 'C',
			'awarded_vendor_id'=>'N',
			'awarded_by_user_id'=>'N',
			'awarded_by_username'=>'C',
			'awarded_vendor_name'=>'C',
			'awarded_datetime'=>'D',
			'awarded_saving_id'=>'N',
			'awarded_saving_by'=>'N',
			'awarded_saving_datetime'=>'D',
			'user_id' => 'N',
			'user_name' => 'N',
			'user_email' => 'N',
			'quote_name' => 'C',
			'estimated_value'=>'C',
			'cost_savings'=>'C',
			'send_to'=>'C',
			'quote_desc' => 'C',
			'quote_status' => 'C',
			'contact_email' => 'C',
			'contact_phone' => 'C',
			'contact_email2' => 'C',
			'contact_phone2' => 'C',
			'opening_date' => 'D',
			'closing_date' => 'D',
			'needed_date' => 'D',
			'quote_currency' => 'C',
			'created_by_user_id' => 'C',
			'created_by_name' => 'C',
			//'manager_user_id' => 'C',
			'manager_name' => 'C',
			'commercial_lead_user_id' => 'C',
			'commercial_lead_name' => 'C',
			//'procurement_lead_user_id' => 'C',
			'procurement_lead_name' => 'C',
		);
		parent::__construct('quote_id', 'quotes');
	}

	// public function getQuote($quote_id)
	// {
	// 	return $this->executeQuery("SELECT * FROM quotes q LEFT JOIN users u ON q.user_id = u.user_id WHERE q.quote_id = $quote_id", true);
	// }

	public function getQuotes($location_id, $department_id,$category_id, $subcategory_id, $quote_status, $db_from_date, $db_to_date)
	{
		$sql = "SELECT q.*,(select count(*) from quote_reopen_history where quote_id=q.quote_id) as total_reopen,cat.value as quotes_by_category,u.profile_img,q.user_id, u.full_name, COUNT(DISTINCT v.id) AS invite_count,
			SUM(CASE 
    				WHEN v.submit_status = 2
    					THEN 1 
    				ELSE v.submit_status 
				END) AS submit_count,
			(select qvw.total_price from quote_vendors qvw where  qvw.quote_id = q.quote_id and qvw.vendor_id=q.awarded_vendor_id group by quote_id) as vendor_submitted_price,
			(select GROUP_CONCAT(qvw.submit_status) from quote_vendors qvw where  qvw.quote_id = q.quote_id  group by quote_id) as quote_sub_status

		FROM quotes q LEFT JOIN users u ON q.user_id = u.user_id 
		LEFT JOIN quote_vendors v ON q.quote_id = v.quote_id
		LEFT JOIN categories cat ON q.category_id = cat.id
		";
		 
		$where  = "";

		if (!empty($location_id)) {
			$location_id = implode(',',$location_id);
			if(empty($where)){
				$where = " where " ;
			}
			$sql .= $where."  q.location_id IN ($location_id)";
		}
		if (!empty($department_id)) {
			$department_id = implode(',',$department_id);
			if(empty($where)){
				$where = " where " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." q.department_id IN ($department_id)";
		}
		if (!empty($category_id)) {
			$category_id = implode(',',$category_id);
			if(empty($where)){
				$where = " where " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." q.category_id IN ($category_id)";
		}
		if (!empty($subcategory_id)) {
			$subcategory_id = implode(',',$subcategory_id);
			if(empty($where)){
				$where = " where " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." q.subcategory_id IN ($subcategory_id)";
		}
		//if (!empty($department_id)) $sql .= " AND q.department_id = $department_id ";

		if (!empty($quote_status)){
			$status = "'" . implode("','", $quote_status) . "'";
			if(empty($where)){
				$where = " where  " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." q.quote_status IN ($status)";
		}
		
		if (!empty($db_from_date)) {

			if(empty($where)){
				$where = " where  " ;
			}else{
				$where = " AND ";
			}

			$sql .= $where."  DATE_FORMAT(q.opening_date,'%Y-%m-%d')>= '$db_from_date'";
		}
		if (!empty($db_to_date)) {
			if(empty($where)){
				$where = " where " ;
			}else{
				$where = " AND ";
			}
			$sql .= $where." DATE_FORMAT(q.closing_date,'%Y-%m-%d')<='$db_to_date'";
		}
		
		return $this->executeQuery($sql . ' GROUP BY q.quote_id ORDER BY q.quote_id DESC ');
	}

	public function getQuotesAjax($start = 0, $length = 10, $search_for = "", $order_by = array(),$location_id, $department_id,$category_id, $subcategory_id, $quote_status, $db_from_date, $db_to_date)
	{
	    $now  = date("Y-m-d H:i:s");
		if (!empty($db_from_date)){
		 
		 if(FunctionManager::dateFormat()=="d/m/Y"){
	      list($input_date, $input_month, $input_year) = explode("/", $db_from_date);
	      $db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
	     }else{
	      $db_from_date = date("Y-m-d",strtotime($db_from_date));
	     }
		}
		
		if(!empty($db_to_date)){
		 if(FunctionManager::dateFormat()=="d/m/Y"){
	      list($input_date, $input_month, $input_year) = explode("/", $db_to_date);
	      $db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
	     }else{
	      $db_to_date = date("Y-m-d",strtotime($db_to_date));
	     }
		}

        $user_id = Yii::app()->session['user_id'];
        $memberType = Yii::app()->session['member_type'];
		$userTypeID = Yii::app()->session['user_type'];
        if($userTypeID !="4" && ($memberType !=1 || $memberType =="0")){
         $up = "INNER JOIN user_privilege up On q.location_id = up.location_id and q.department_id= up.department_id and eSourcing_view='yes' and up.user_id=".$user_id;
        }else {$up = "";}

		$sql = "SELECT q.*,(select count(*) from quote_reopen_history where quote_id=q.quote_id) as total_reopen,cat.value as quotes_by_category,u.profile_img,q.user_id, u.full_name, COUNT(DISTINCT v.id) AS invite_count,
			SUM(CASE 
				WHEN v.submit_status = 2
    					THEN 1 
    				ELSE v.submit_status 
				END) AS submit_count,

			CASE 
				WHEN LOWER(q.quote_status) != 'cancelled' 
				and q.quote_status != 'Draft' and q.closing_date >'".$now."' 
				and awarded_vendor_id =0
    					THEN 'In Progress'

    			WHEN q.quote_status != 'Draft' and awarded_vendor_id !=0
    					THEN 'Awarded'

    			WHEN q.quote_status = 'Draft'  THEN 'Draft'		
    			WHEN q.quote_status != 'Draft' and q.closing_date <'".$now."'
    					THEN 'Complete'
    			
    				ELSE q.quote_status
				END AS quote_status,

			(select qvw.total_price from quote_vendors qvw where  qvw.quote_id = q.quote_id and qvw.vendor_id=q.awarded_vendor_id group by quote_id) as vendor_submitted_price,
			(select GROUP_CONCAT(qvw.submit_status) from quote_vendors qvw where  qvw.quote_id = q.quote_id  group by quote_id) as quote_sub_status

		FROM quotes q LEFT JOIN users u ON q.user_id = u.user_id 
		LEFT JOIN quote_vendors v ON q.quote_id = v.quote_id
		LEFT JOIN categories cat ON q.category_id = cat.id 
		".$up;
		
		$sql  .= " where 1 ";
		if (! empty($search_for)) {
            $search_for = trim($search_for);
            $search_for_date = date("Y-m-d",strtotime(strtr($search_for,'/','-')));

            $sql .= " and ( q.quote_id LIKE '%".$search_for."%'";
            $sql .= " or q.quote_name LIKE '%".$search_for."%'";
            $sql .= " or cat.value LIKE '%".$search_for."%'";
            $sql .= " or q.created_by_name LIKE '%".$search_for."%'";
            $sql .= " or q.quote_currency LIKE '%".$search_for."%'";
            $sql .= " or q.estimated_value LIKE '%".$search_for."%'";
            $sql .= " or q.awarded_vendor_name LIKE '%".$search_for."%'";
            if($search_for_date !="1970-01-01"){
              $sql .= " or DATE_FORMAT(q.opening_date,'%Y-%m-%d')='".$search_for_date."'";
              $sql .= " or DATE_FORMAT(q.closing_date,'%Y-%m-%d')='".$search_for_date."'";
            }
            $sql .= " ) ";
        }
        $order_by_clause = "";
        if (is_array($order_by) && count($order_by)) {
            foreach ($order_by as $column) {
                $column_index = $column['column'];
                $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                $column_name = "";
                switch ($column_index) {
                    case 0:
                        $column_name = "quote_id";
                        break;
                    case 1:
                        $column_name = "quote_id";
                        break;
                    case 2:
                        $column_name = "quote_name";
                        break;
                    case 3:
                        $column_name = "quotes_by_category";
                        break;
                    case 4:
                        $column_name = "opening_date";
                        break;
                    case 5:
                        $column_name = "closing_date";
                        break;
                    case 6:
                        $column_name = "created_by_name";
                        break;
                     case 7:
                        $column_name = "quote_currency";
                        break;
                    case 8:
                        $column_name = "awarded_vendor_name";
                        break;
                    case 9:
                        $column_name = "cost_savings";
                        break;
                    case 10:
                        $column_name = "quote_status";
                        break;
 					case 11:
                        $column_name = "quote_status";
                        break;

                }
                if (! empty($column_name)) {
                    if (empty($order_by_clause))
                        $order_by_clause = $column_name . ' ' . $column_direction;
                    else
                        $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                }
            }
        }

		if (!empty($location_id)) {
			$sql .= " AND q.location_id IN ($location_id)";
		}
		if (!empty($department_id)) {
			$sql .= " AND q.department_id IN ($department_id)";
		}
		if (!empty($category_id)) {
			$sql .= " AND q.category_id IN ($category_id)";
		}
		if (!empty($subcategory_id)) {
			$sql .= " AND q.subcategory_id IN ($subcategory_id)";
		}
		if (!empty($quote_status)){
			$sql .= " AND q.quote_status IN ($quote_status)";
		}
		if (!empty($db_from_date)) {
			$sql .= " AND q.opening_date>= '$db_from_date 00:00:00'";
		}
		if (!empty($db_to_date)) {
			$sql .= " AND q.closing_date <='$db_to_date 23:00:00'";
		}

		if($start != 'none'){
			$sql = $sql . " GROUP BY q.quote_id ORDER BY $order_by_clause LIMIT $start, $length ";
		}else{
			$sql = $sql . " GROUP BY q.quote_id ORDER BY $order_by_clause  ";
		}
	
		return $this->executeQuery($sql);
	}
 

	public function getVendorQuotes($vendor_id)
	{
		$sql = "SELECT q.*, u.full_name, COUNT(DISTINCT v.id) AS invite_count, SUM(v.submit_status) AS submit_count
				  FROM quotes q
				  LEFT JOIN users u ON q.user_id = u.user_id
				  INNER JOIN quote_vendors v ON q.quote_id = v.quote_id
				  WHERE v.vendor_id = $vendor_id";

		//CVarDumper::dump($this->executeQuery($sql),10,1);die;
		return $this->executeQuery($sql . ' GROUP BY q.quote_id ORDER BY q.quote_id DESC ');
	}

	public function getLocationQuote()
	{
		$sql = "SELECT location_name,count(q.quote_id) as total_quotes FROM `quotes` as q inner join locations as l on q.location_id= l.location_id group by l.location_id order by total_quotes DESC";

		return $this->executeQuery($sql);
	}

	public function getProjectQuotes($projectID)
	{
	
		$sql = "SELECT q.*, u.full_name, COUNT(DISTINCT v.id) AS invite_count, SUM(v.submit_status) AS submit_count
				  FROM quotes q
				  INNER JOIN users u ON q.user_id = u.user_id
				  LEFT JOIN quote_vendors v ON q.quote_id = v.quote_id
				  WHERE q.project_id = $projectID";

		//CVarDumper::dump($this->executeQuery($sql),10,1);die;
		return $this->executeQuery($sql . ' GROUP BY q.quote_id ORDER BY q.quote_id DESC ');
	}
	
	public function getQuoteDetails($quote_id, $vendor_id = 0)
	{
		$sql = "SELECT * FROM quote_details WHERE quote_id = $quote_id ORDER BY id";		
		if (!empty($vendor_id)){
			$vendorExp = ' and v.vendor_id ='.$vendor_id;  
		}else{
			$vendorExp = '';
		}
			$sql = "SELECT q.*,vend.*, v.unit_price,v.tax_rate as product_tax_rate,v.shipping as product_shipping,v.other_charges as product_other_charges, v.vendor_product_notes
					  FROM quote_details q 
					  LEFT JOIN quote_vendor_details v 
					  	ON q.quote_id = v.quote_id 
					   AND q.product_name = v.product_name
					   AND q.product_code = v.product_code
					    ".$vendorExp."
					    LEFT JOIN (SELECT q_vend.submit_status,q_vend.vendor_repsond_date,inn_vend.vendor_name,inn_vend.vendor_id,q_vend.tax_rate as tax_amount,q_vend.shipping as shipping_amount,q_vend.other_charges as other_charges_amount,q_vend.total_price as total_price FROM quote_vendors q_vend, vendors inn_vend WHERE q_vend.quote_id = $quote_id  AND q_vend.vendor_id = inn_vend.vendor_id and q_vend.submit_status in(1,2)) vend
					  	ON v.vendor_id = vend.vendor_id 
					 WHERE q.quote_id = $quote_id and vend.submit_status in(1,2) 
					 ORDER BY vendor_name,q.product_name";
		return $this->executeQuery($sql);
	}

	public function getQuoteDetailsHistory($quote_id, $vendor_id = 0)
	{
		$sql = "SELECT * FROM quote_details WHERE quote_id = $quote_id ORDER BY id";		
		if (!empty($vendor_id)){
			$vendorExp = ' and v.vendor_id ='.$vendor_id;  
		}else{
			$vendorExp = '';
		}
			$sql = "SELECT q.*,vend.*,reopen_id,vendor_repsond_date,sh.unit_price,sh.tax_rate as product_tax_rate,sh.shipping as product_shipping,sh.other_charges as product_other_charges, sh.vendor_product_notes,sh.created_at
					  FROM quote_details q 
					  LEFT JOIN quote_vendor_submission_history sh 
					  	ON q.quote_id = sh.quote_id 
					   AND q.product_name = sh.product_name
					   AND q.product_code = sh.product_code
					    ".$vendorExp."
					    LEFT JOIN (SELECT inn_vend.vendor_name,inn_vend.vendor_id,q_vend.tax_rate as tax_amount,q_vend.shipping as shipping_amount,q_vend.other_charges as other_charges_amount,q_vend.total_price as total_price FROM quote_vendors q_vend, vendors inn_vend WHERE q_vend.quote_id = $quote_id  AND q_vend.vendor_id = inn_vend.vendor_id) vend
					  	ON sh.vendor_id = vend.vendor_id 
					 WHERE sh.quote_id = $quote_id 
					 ORDER BY sh.id DESC";
		return $this->executeQuery($sql);
	}

	public function getQuoteDetailsHistoryNew($quote_id, $vendor_id = 0)
	{
		$sql = "SELECT * FROM quote_details WHERE quote_id = $quote_id ORDER BY id";		
		if (!empty($vendor_id)){
			$vendorExp = ' and v.vendor_id ='.$vendor_id;  
		}else{
			$vendorExp = '';
		}
			$sql = "SELECT q.*,vend.*,reopen_id,vendor_repsond_date,sh.unit_price,sh.tax_rate as product_tax_rate,sh.shipping as product_shipping,sh.other_charges as product_other_charges, sh.vendor_product_notes,sh.created_at
					  FROM quote_details q 
					  LEFT JOIN quote_vendor_submission_history sh 
					  	ON q.quote_id = sh.quote_id 
					   AND q.product_name = sh.product_name
					   AND q.product_code = sh.product_code
					    ".$vendorExp."
					    LEFT JOIN (SELECT inn_vend.vendor_name,inn_vend.vendor_id,q_vend.tax_rate as tax_amount,q_vend.shipping as shipping_amount,q_vend.other_charges as other_charges_amount,q_vend.total_price as total_price FROM quote_vendors q_vend, vendors inn_vend WHERE q_vend.quote_id = $quote_id  AND q_vend.vendor_id = inn_vend.vendor_id) vend
					  	ON sh.vendor_id = vend.vendor_id 
					 WHERE sh.quote_id = $quote_id 
					 ORDER BY sh.id ASC";
		return $this->executeQuery($sql);
	}


	public function getAnwserVendors($quote_id)
	{
		$sql = 'SELECT qs.quote_id,scoring_title,question,vendor_name,qva.vendor_id,quote_answer,
		(CASE
		  WHEN qva.question_type="free_text" THEN qva.free_text
		  WHEN qva.question_type="yes_or_no" THEN qva.yes_no
		  WHEN qva.question_type="multiple_choice" THEN  (select answer from quote_answers where id=qva.quote_answer)
		 ELSE ""
		END) as qans
		 FROM quote_scoring qs	
		LEFT JOIN quote_questions qq on qs.quote_id=qq.quote_id and qs.vendor_scoring_id=qq.vendor_scoring_id 
		LEFT JOIN (SELECT inn_qva.*,v.vendor_name FROM `quote_vendor_answers` as inn_qva inner join vendors as v on inn_qva.vendor_id=v.vendor_id) qva on qva.quote_id=qs.quote_id and qq.id=qva.question_id					
		where qs.quote_id='.$quote_id.' and qs.score>0 order by vendor_name DESC';
		return $this->executeQuery($sql);
	}
		
	public function getQuoteVendors($quote_id)
	{
		$sql = "SELECT * FROM quote_vendors q, vendors v 
				 WHERE q.quote_id = $quote_id AND q.vendor_id = v.vendor_id";
		return $this->executeQuery($sql);
	}

	public function getQuoteVendorInfo($vendor_id,$quote_id)
	{
		$sql = "SELECT q.vendor_id,q.vendor_name,q.contact_name,q.address_1 ,q.address_2,q.city,q.state,q.zip ,q.emails,q.phone_1  FROM quote_vendors q, vendors v 
				 WHERE q.quote_id = $quote_id AND q.vendor_id =$vendor_id AND q.vendor_id = v.vendor_id AND (q.vendor_name !='' OR q.contact_name !='' OR q.address_1 !='' OR q.address_2 !='' OR q.city !='' OR q.state !='' OR q.zip !='' OR q.emails !='' OR q.phone_1 !=''  )";
		return $this->executeQuery($sql);
	}

	public function saveVendors($quote_id, $selectedVendors){
	    $invited_count = 0;
		$vendor_name_1 = !empty($_POST['vendor_name_1']) ? $_POST['vendor_name_1'] : '';
	    $invited_data = $this->executeQuery("SELECT * FROM quote_vendors WHERE quote_id = $quote_id ");
		
		foreach ($selectedVendors as $key=>$vendor_id){
			if(!empty($vendor_name_1)){
			if(!empty($vendor_id) && in_array($vendor_id,$vendor_name_1)){
				$vendorReader = Yii::app()->db->createCommand("select * from vendors where vendor_id=".$vendor_id)->queryRow();
				$vendor_name  = $vendorReader['vendor_name'];
				$contact_name = $_POST['vendor_contact_new'][$key];
				$emails       = $_POST['vendor_email_new'][$key];
			}}else if(!empty($vendor_id)){
				$vendorReader = Yii::app()->db->createCommand("select * from vendors where vendor_id=".$vendor_id)->queryRow();
				$vendor_name  = $vendorReader['vendor_name'];
				$contact_name = !isset($_POST['vendor_contact_new']) ? $_POST['vendor_contact_new'] : $vendorReader['contact_name'];
				$emails       = isset($_POST['vendor_email_new']) ? $_POST['vendor_email_new'][$key] : $vendorReader['emails'];
			}
			
			if(!empty($vendor_id)){
			 
			$contactInfo = explode(" - ",$_POST['vendor_contact_new'][$key][0]);
			$cName = $contactInfo[0];
			$cEmail = $contactInfo[1];
			
			$reference = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)))), 1, 24);
			$this->executeQuery("INSERT INTO quote_vendors (quote_id, vendor_id, created_datetime, updated_datetime,reference,vendor_name,contact_name,emails) 
								 SELECT $quote_id, $vendor_id, NOW(), NOW(),'".$reference."','".$vendor_name."','".$cName."','".$cEmail."'");
			
			$quoteVendorID = Yii::app()->db->lastInsertID;
			$contactInfo = $_POST['vendor_contact_new'][$key];
			foreach($contactInfo  as $cValue){
				$contactInfo = explode(" - ",$cValue);
				$cName = $contactInfo[0];
				$cEmail = $contactInfo[1];
				$quoteVendContact = new QuoteVendorContact();
				$quoteVendContact->rs = array();
				$quoteVendContact->rs['quote_id'] = $quote_id;
				$quoteVendContact->rs['vendor_id'] = $vendor_id;
				$quoteVendContact->rs['quote_vendor_id'] = $quoteVendorID;
				$quoteVendContact->rs['contact_name'] = $cName;
				$quoteVendContact->rs['contact_email']= $cEmail;
				$quoteVendContact->rs['status'] = 0;
				$quoteVendContact->rs['created_at'] = date("Y-m-d H:i:s");
				$quoteVendContact->write();
				
			 }
			}
		}
		
		foreach ($invited_data as $invited_vendor)
		{
			$reference = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)))), 1, 24);
		    $update_sql = "UPDATE quote_vendors
							  SET tax_rate = {$invited_vendor['tax_rate']},
							  	  discount = {$invited_vendor['discount']},
							  	  total_price = {$invited_vendor['total_price']},
							  	  other_charges = {$invited_vendor['other_charges']},
							  	  shipping = {$invited_vendor['shipping']},
							  	  submit_status = {$invited_vendor['submit_status']},
							  	  notes = '{$invited_vendor['notes']}',
							  	  updated_datetime = NOW(),
							  	  reference ='".$reference."'
							WHERE quote_id = $quote_id AND vendor_id = {$invited_vendor['vendor_id']}";
		    $this->executeQuery($update_sql);
		}
	
		return $invited_count;
	}

	public function saveQuickVendors($quote_id, $quickvendorInfo)
	{
	   
		foreach ($quickvendorInfo as $vendor_id=>$value){
			//$vendor_id = $value['vendor_id'];
			$vendor = new Vendor();
			$vendorInfo = $vendor->getOne(array('vendor_id'=>$vendor_id));
			$vendor_name = $vendorInfo['vendor_name'];
			/*$contact_name = "";
			$address_1 = "";
			$address_2 = "";
			$city = "";
			$state = "";
			$zip = "";
			$emails = "";
			$phone_1 = "";*/
			$contact_name = $value['vendor_contact'];
			$address_1 = $value['vendor_address'];
			$address_2 = $value['vendor_address2'];
			$city = $value['vendor_city'];
			$state = $value['vendor_state'];
			$zip = $value['vendor_zip'];
			$emails = $vendorInfo['emails'];
			$phone_1 = '';
				
			$this->executeQuery("INSERT INTO quote_vendors (quote_id, vendor_id, created_datetime, updated_datetime,vendor_name,contact_name,address_1,address_2,city,state,zip,emails,phone_1) 
								 SELECT $quote_id, $vendor_id, NOW(), NOW(),'".$vendor_name."','".$contact_name."','".$address_1."','".$address_2."','".$city."','".$state."','".$zip."','".$emails."','".$phone_1."'");
		}
		$invited_count = 0;
	    $invited_data = $this->executeQuery("SELECT * FROM quote_vendors WHERE quote_id = $quote_id");
		foreach ($invited_data as $invited_vendor)
		{
		    $update_sql = "UPDATE quote_vendors
							  SET tax_rate = {$invited_vendor['tax_rate']},
							  	  discount = {$invited_vendor['discount']},
							  	  total_price = {$invited_vendor['total_price']},
							  	  other_charges = {$invited_vendor['other_charges']},
							  	  shipping = {$invited_vendor['shipping']},
							  	  submit_status = {$invited_vendor['submit_status']},
							  	  notes = '{$invited_vendor['notes']}',
							  	  updated_datetime = NOW()
							WHERE quote_id = $quote_id AND vendor_id = {$invited_vendor['vendor_id']}";
		    $this->executeQuery($update_sql);
		}
		return $invited_count;
	}

	public function setVendorNotification($quote, $already_invited,$comments='',$vendor_ids=''){
        $quote_id = $quote['quote_id'];
        if(empty($comments)){
        	$notificationText = '<a href="' .Yii::app()->createAbsoluteUrl('supplier/quotes/view?quote_id='.$quote_id). '">
        	New Quote Submission Invite</a>';
    	}else{
    		$notificationText = '<a href="' .Yii::app()->createAbsoluteUrl('supplier/quotes/view?quote_id='.$quote_id). '">'.$comments.'</a>';
    	}

    	if(empty($vendor_ids)){
		
			$sql = "SELECT q.* FROM quote_vendors q WHERE q.quote_id = $quote_id";
	        $vendors = Yii::app()->db->createCommand($sql)->query()->readAll();
	        foreach ($vendors as $quote_vendor)
	        {
				$valueVendorID = !empty($quote_vendor['vendor_id'])?$quote_vendor['vendor_id']:0;
				
				$notification = new Notification();
				$notification->rs = array();
				$notification->rs['id'] = 0;
				$notification->rs['user_type'] = 'Vendor';
				$notification->rs['notification_flag'] = '';
				$notification->rs['user_id'] = $valueVendorID;
				$notification->rs['notification_text'] = $notificationText;
				$notification->rs['notification_date'] = date("Y-m-d H:i");
				$notification->rs['read_flag'] = 0;
				$notification->rs['notification_type'] = 'Quote';
				$notification->write();

				$cronEmail = new CronEmail;
				$cronEmail->rs = array();
				$cronEmail->rs['record_id'] = $quote_id;
				$cronEmail->rs['send_to'] = 'Vendor'; 
				$cronEmail->rs['user_id'] = $valueVendorID;
				$cronEmail->rs['user_type'] = 'Quote Invite';
				$cronEmail->rs['status'] = 'Pending';
				$cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
				$cronEmail->write();
			}
		}else if(!empty($vendor_ids)){
			foreach ($vendor_ids as $valueVendorID)
	        {				
				$notification = new Notification();
				$notification->rs = array();
				$notification->rs['id'] = 0;
				$notification->rs['user_type'] = 'Vendor';
				$notification->rs['notification_flag'] = '';
				$notification->rs['user_id'] = $valueVendorID;
				$notification->rs['notification_text'] = $notificationText;
				$notification->rs['notification_date'] = date("Y-m-d H:i");
				$notification->rs['read_flag'] = 0;
				$notification->rs['notification_type'] = 'Quote';
				$notification->write();

				$cronEmail = new CronEmail;
				$cronEmail->rs = array();
				$cronEmail->rs['record_id'] = $quote_id;
				$cronEmail->rs['send_to'] = 'Vendor'; 
				$cronEmail->rs['user_id'] = $valueVendorID;
				$cronEmail->rs['user_type'] = 'Quote Invite';
				$cronEmail->rs['status'] = 'Pending';
				$cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
				$cronEmail->write();
			}
		}
	}
	public function setInternalUsersNotification($quote_data,$vendor_ids,$comments=''){ 
		$vendorObj = new Vendor;
		$user_ids  = array();
		if(!empty($quote_data['created_by_user_id'])) $user_ids[$quote_data['created_by_user_id']] = $quote_data['created_by_user_id'];
		//if(!empty($quote_data['manager_user_id'])) $user_ids[$quote_data['manager_user_id']] = $quote_data['manager_user_id'];
		if(!empty($quote_data['commercial_lead_user_id'])) $user_ids[$quote_data['commercial_lead_user_id']] = $quote_data['commercial_lead_user_id'];
		//if(!empty($quote_data['procurement_lead_user_id'])) $user_ids[$quote_data['procurement_lead_user_id']] = $quote_data['procurement_lead_user_id'];
		/*foreach ($vendor_ids as $vendor_id){
			$vendorData = $vendorObj->getOne(array('vendor_id'=>$vendor_id));*/
			
			if(empty($comments)){
				$notificationComments  = "A new quote <b>".$quote_data['quote_id']."</b> - <b>".$quote_data['quote_name']."</b> has been created";
			}else{
				$notificationComments  = $comments;
			}
			foreach($user_ids as $user_id){
				$notification = new Notification();
				$notification->rs = array();
				$notification->rs['id'] = 0;
				$notification->rs['user_id'] = $user_id;
				$notification->rs['user_type'] = 'Client';
				/*$notification->rs['notification_text'] =
					'Supplier ' . $vendorData['vendor_name'] . ' has submitted a quote for '
					. '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_data['quote_id']) . '">Quote ID #' 
					. $quote_data['quote_id'] . '</a> - ' . $quote_data['quote_name'];*/
				$notification->rs['notification_text'] = addslashes('<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_data['quote_id']). '">'.$notificationComments.'</a>');

				$notification->rs['notification_date'] = date("Y-m-d H:i");
				$notification->rs['read_flag'] = 0;
				$notification->rs['notification_type'] = 'Quote';
				$notification->write();

				$cronEmail = new CronEmail;
				$cronEmail->rs = array();
				$cronEmail->rs['record_id'] = $quote_data['quote_id'];
				$cronEmail->rs['send_to'] = 'User'; 
				$cronEmail->rs['user_id'] = $user_id;
				$cronEmail->rs['user_type'] = 'Quote Invite';
				$cronEmail->rs['status'] = 'Pending';
				$cronEmail->rs['created_at'] = date("Y-m-d H:i:s");
				$cronEmail->write();

			//}
		}
	}

	public function sendUserNotification($quote)
	{
		$user_ids  = array();
		$quote_id  = $quote['quote_id'];
		if(!empty($quote['created_by_user_id'])) $user_ids[$quote['created_by_user_id']] = $quote['created_by_user_id'];
		//if(!empty($quote['manager_user_id'])) $user_ids[$quote['manager_user_id']] = $quote['manager_user_id'];
		if(!empty($quote['commercial_lead_user_id'])) $user_ids[$quote['commercial_lead_user_id']] = $quote['commercial_lead_user_id'];
		//if(!empty($quote['procurement_lead_user_id'])) $user_ids[$quote['procurement_lead_user_id']] = $quote['procurement_lead_user_id'];
		$vendors = $this->getQuoteVendors($quote_id);
		$vendors = array_column($vendors, 'vendor_name');
		$vendors = implode(", ", $vendors);
		$user = new User;
		$users = $user->getData(array('user_id in '=>$user_ids));
		foreach ($users as $value)
		{
			//CVarDumper::dump($quote_vendor['emails'],10,1);die;
			if (!empty($value['email']))
			{
				$user_name = $value['full_name'];
				$user_email = $value['email'];
				$email_body = '';
				$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <div style="margin:0px auto;max-width:600px;">
                <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="vertical-align:top;width:600px;">
                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                 <tr>
                  <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                  <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                      <tr>
                          <td style="width:600px;">';
				$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
				$email_body .= '</td>
                      </tr>
                      </tbody>
                  </table>
                  </td>
                 </tr>
                 <tr>
                 <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;width:600px;">';
				$email_body .= '<p style="margin-left: 16px;">Dear ' . $user_name . ', </p>';
				$email_body .= '</td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center;">';
				$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                         <h1 style="font-family: Cabin; line-height: 100%;">
                             <span style="color:#000000;"><strong>New Quote - <span style="color:#10a798;">' . $quote_id . '</span> </strong></span>
                         </h1>
                     </div>
                 </td>
                 </tr>

                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;">
                 <div style="font-size:11px;line-height:22px;color:#000000;font-family:Roboto, Tahoma, sans-serif;marign-left:16px;">';
				$email_body .= '<p>This is a notification from the procurement system. The following quote has been published:</p>';
				$email_body .= '<strong>Quote For: </strong>' . $quote['quote_name'] . '<br />';
				$email_body .= '<strong>Quote Type: </strong>NEW<br />';
				$email_body .= '<strong>Starts at: </strong>' . date("F j, Y H:iA", strtotime($quote['opening_date'])) . '<br />';
				$email_body .= '<strong>Deadline: </strong>' . date("F j, Y H:iA", strtotime($quote['closing_date'])) . '<br />';
				$email_body .= '<br /><strong>NOTE:</strong> <span style="color: red;">Quote Submission Page</span> Quote has been published to '.$vendors.'<br />';
				$email_body .= '</div>
                 </td>
                </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                     </td>
                 </tr>';

				$email_body .= '<tr>
                       <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                        <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                        <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                        <a href="' . AppUrl::bicesUrl('quotes/submitQuote') . '" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Quote Submission Page</a>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>

                         <tr>
                             <td style="word-wrap:break-word;font-size:0px;">
                             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                             </td>
                         </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;" align="left">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;margin-left:16px;">
                             <p>Terms of Service &amp; User Agreement</p>
                             <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                             <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                         </div>
                     </td>
                 </tr>
                 </tbody>
                </table>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </div></td>
                </tr>
                </table>
                </div>';

			$mail = new \PHPMailer(true);					
			$mail->IsSMTP();
			$mail->Host = Yii::app()->params['host'];
			$mail->Port = Yii::app()->params['port'];
			$mail->SMTPAuth = true;
			$mail->Username = Yii::app()->params['username'];
			$mail->Password = Yii::app()->params['password'];
			$mail->SMTPSecure = Yii::app()->params['stmpSecure'];
			$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
			$mail->addAddress($user_email, $user_name);
			$mail->Subject = Yii::app()->session['company_name'] . 'Spend 365: Quote Publication';
			$mail->isHTML(true);
			$mail->Body = $email_body;
			//$mail->SMTPDebug = 2;
			$mail->send();
	
			}
		}
	}

	public function saveProducts($quote_id, $product_names, $quantities, $uoms, $current_prices='',$product_code='')
	{	
		$this->executeQuery("DELETE FROM quote_details WHERE quote_id = $quote_id ");
		$total_products = count($product_names);		
		for ($idx=0; $idx<$total_products; $idx++)
		{
			$product_name = isset($product_names[$idx]) ? $product_names[$idx] : "";
			$quantity = isset($quantities[$idx]) ? $quantities[$idx] : 0;
			$uom = isset($uoms[$idx]) ? $uoms[$idx] : "";
			$price = isset($current_prices[$idx]) ? $current_prices[$idx] : 0;
			$pcode =  isset($product_code[$idx]) ? $product_code[$idx] : 0;
			if (!empty($product_name) && $quantity)
				$this->executeQuery("INSERT INTO quote_details (quote_id, product_name, quantity, uom, price,product_code) SELECT $quote_id, '".$product_name."', '$quantity', '$uom', $price,'$pcode'");
		}
	}		

	public function sendInvites($quote_id, $already_invited)
	{
		$quote = $this->getOne(array('quote_id' => $quote_id));
		foreach ($this->getQuoteVendors($quote_id) as $quote_vendor)
		{

			//CVarDumper::dump($quote_vendor['emails'],10,1);die;

			if (!empty($quote_vendor['emails']))
			{
				$vendor_id = $quote_vendor['vendor_id'];
				$vendor_name = $quote_vendor['vendor_name'];
				$reference = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)))), 1, 24);
				$this->executeQuery("UPDATE quote_vendors SET reference = '$reference' WHERE quote_id = $quote_id AND vendor_id = $vendor_id");
				
				$email_body = '';
				$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <div style="margin:0px auto;max-width:600px;">
                <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="vertical-align:top;width:600px;">
                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                 <tr>
                  <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                  <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                      <tr>
                          <td style="width:600px;">';
				$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
				$email_body .= '</td>
                      </tr>
                      </tbody>
                  </table>
                  </td>
                 </tr>
                 <tr>
                 <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;width:600px;">';
				$email_body .= '<p style="margin-left: 16px;">Dear ' . $vendor_name . ', </p>';
				$email_body .= '</td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center;">';
				$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                         <h1 style="font-family: Cabin; line-height: 100%;">
                             <span style="color:#000000;"><strong>New Quote - <span style="color:#10a798;">' . $quote_id . '</span> </strong></span>
                         </h1>
                     </div>
                 </td>
                 </tr>

                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;">
                 <div style="font-size:11px;line-height:22px;color:#000000;font-family:Roboto, Tahoma, sans-serif;marign-left:16px;">';
				$email_body .= '<p>This is a notification from the procurement system. You have been invited to supply a quote for the following:</p>';
				$email_body .= '<strong>Quote For: </strong>' . $quote['quote_name'] . '<br />';
				if (!$already_invited) $email_body .= '<strong>Quote Type: </strong>NEW<br />';
				else $email_body .= '<strong>Quote Type: </strong>Modified<br />';
				//$email_body .= '<strong>Reference: </strong>' . $reference . '<br />';
				$email_body .= '<strong>Starts at: </strong>' . date("F j, Y H:iA", strtotime($quote['opening_date'])) . '<br />';
				$email_body .= '<strong>Deadline: </strong>' . date("F j, Y H:iA", strtotime($quote['closing_date'])) . '<br />';
				$email_body .= '<br /><strong>NOTE:</strong> To submit a quote, please click on the <span style="color: red;">Quote Submission Page</span> link below.<br />';
				$email_body .= '</div>
                 </td>
                </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                     </td>
                 </tr>';

				$email_body .= '<tr>
                       <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                        <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                        <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                        <a href="' . Yii::app()->createAbsoluteUrl('supplier/quotes/view?quote_id='.$quote_id) . '" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Quote Submission Page</a>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>

                         <tr>
                             <td style="word-wrap:break-word;font-size:0px;">
                             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                             </td>
                         </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;" align="left">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;margin-left:16px;">
                             <p>Terms of Service &amp; User Agreement</p>
                             <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                             <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                         </div>
                     </td>
                 </tr>
                 </tbody>
                </table>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </div></td>
                </tr>
                </table>
                </div>';

		
				foreach (explode(",", $quote_vendor['emails']) as $vendor_email)
				{
					$mail = new \PHPMailer(true);					
					$mail->IsSMTP();
					$mail->Host = Yii::app()->params['host'];
					$mail->Port = Yii::app()->params['port'];
					$mail->SMTPAuth = true;
					$mail->Username = Yii::app()->params['username'];
					$mail->Password = Yii::app()->params['password'];
					$mail->SMTPSecure= Yii::app()->params['stmpSecure'];
					$mail->CharSet="UTF-8";
					$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
					$mail->addAddress($vendor_email, $vendor_name);
					$mail->Subject = Yii::app()->session['company_name'] . 'Spend 365: Quote Submission Invite';
					$mail->isHTML(true);
					$mail->Body = $email_body;
					//$mail->SMTPDebug = 2;
					$mail->send();
				}
			}
		}
	}


	public function submitQuote($quote_id,$vendor_id)
	{
		$quote_vendor = $this->executeQuery("SELECT *
				  FROM quote_vendors qv
				  INNER JOIN quotes q ON q.quote_id=qv.quote_id
				  WHERE qv.quote_id = $quote_id AND qv.vendor_id = $vendor_id",1);

		$vendor = $this->executeQuery("SELECT * FROM vendors WHERE vendor_id = $vendor_id",1);

		$user_id = $quote_vendor['user_id'];
		$vendor_name = $vendor['vendor_name'];
		//$user = $this->executeQuery("SELECT * FROM users WHERE user_id = $user_id",1);

		$user_ids  = array();
		if(!empty($quote_vendor['created_by_user_id'])) $user_ids[$quote_vendor['created_by_user_id']] = $quote_vendor['created_by_user_id'];
		if(!empty($quote_vendor['manager_user_id'])) $user_ids[$quote_vendor['manager_user_id']] = $quote_vendor['manager_user_id'];
		if(!empty($quote_vendor['commercial_lead_user_id'])) $user_ids[$quote_vendor['commercial_lead_user_id']] = $quote_vendor['commercial_lead_user_id'];
		if(!empty($quote_vendor['procurement_lead_user_id'])) $user_ids[$quote_vendor['procurement_lead_user_id']] = $quote_vendor['procurement_lead_user_id'];
		$user = new User;
		$users = $user->getData(array('user_id in '=>$user_ids));


		//CVarDumper::dump($quote_vendor['emails'],10,1);die;
		foreach($users as $user){
			if (!empty($user['email']))
			{
				$mail = new \PHPMailer(true);
				$mail->IsSMTP();
				$mail->Host = Yii::app()->params['host'];
				$mail->Port = Yii::app()->params['port'];
				$mail->SMTPAuth = true;
				$mail->Username = Yii::app()->params['username'];
				$mail->Password = Yii::app()->params['password'];
				$mail->SMTPSecure = Yii::app()->params['stmpSecure'];
				$mail->CharSet="UTF-8";

				$email_body = '';
				$email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <div style="margin:0px auto;max-width:600px;">
                <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="vertical-align:top;width:600px;">
                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                 <tr>
                  <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                  <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                      <tr>
                          <td style="width:600px;">';
				$email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
				$email_body .= '</td>
                      </tr>
                      </tbody>
                  </table>
                  </td>
                 </tr>
                 <tr>
                 <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;width:600px;">';
				$email_body .= '<p style="margin-left: 16px;">Dear ' . $user['full_name'] . ', </p>';
				$email_body .= '</td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center;">';
				$email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                         <h1 style="font-family: Cabin; line-height: 100%;">
                             <span style="color:#000000;"><strong> <span style="color:#10a798;">' . $vendor_name . '</span> has responded to your RFQ </strong></span>
                         </h1>
                     </div>
                 </td>
                 </tr>

                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;">
                 <div style="font-size:11px;line-height:22px;color:#000000;font-family:Roboto, Tahoma, sans-serif;marign-left:16px;">';
				$email_body .= '<p> You have had a response for  quote ' . $quote_id . '. Please see further details via the <span style="color:red;">Quote</span> button below.:</p>';
				$email_body .= '</div>
                 </td>
                </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                     </td>
                 </tr>';

				$email_body .= '<tr>
                       <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                        <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                        <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                        <a href="' . AppUrl::bicesUrl('quotes/edit/'.$quote_id) . '" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Quote Response</a>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>

                         <tr>
                             <td style="word-wrap:break-word;font-size:0px;">
                             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                             </td>
                         </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;" align="left">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;margin-left:16px;">
                             <p>Terms of Service &amp; User Agreement</p>
                             <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                             <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                         </div>
                     </td>
                 </tr>
                 </tbody>
                </table>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </div></td>
                </tr>
                </table>
                </div>';

				$mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
				$mail->addAddress($user['email'], $vendor_name);
				$mail->Subject = Yii::app()->session['company_name'] . 'Spend 365: Supplier Quote Response';
				$mail->isHTML(true);
				$mail->Body = $email_body;
				//$mail->SMTPDebug = 2;
				$mail->send();
		}
		}
	}

	public function getQuoteByReference($input_reference)
	{
		$reference = $input_reference;
		if (empty($reference)) $reference = 'hopefully this is not any random string generated in this exact format :)';
		
		$current_datetime = date("Y-m-d H:i:s");
		$sql = "SELECT * FROM quote_vendors WHERE reference = '$reference'
				   AND quote_id IN ( SELECT quote_id FROM quotes
				   					  WHERE opening_date <= '$current_datetime'
				   					    AND closing_date >= '$current_datetime' )";
		return $this->executeQuery($sql, true);
	}
	
	public function saveQuoteSubmissionData($quote_submission_data)
	{
		// which vendor is submitting this quote?
		$vendor_id = isset($quote_submission_data['vendor_id']) ? $quote_submission_data['vendor_id'] : 0;
		$quote_id = isset($quote_submission_data['quote_id']) ? $quote_submission_data['quote_id'] : 0;
		$notes = isset($quote_submission_data['notes']) ? $quote_submission_data['notes'] : "";
		$submit_quote = isset($quote_submission_data['submit_quote']) ? $quote_submission_data['submit_quote'] : 0;
		
		if ($quote_id && $vendor_id)
		{
			// insert product prices first
			$this->executeQuery("DELETE FROM quote_vendor_details WHERE quote_id = $quote_id AND vendor_id = $vendor_id");
			$product_names = isset($quote_submission_data['product_names']) ? $quote_submission_data['product_names'] : array();
			$quantities = isset($quote_submission_data['quantities']) ? $quote_submission_data['quantities'] : array();
			$prices = isset($quote_submission_data['prices']) ? $quote_submission_data['prices'] : array();
			$unit_prices = isset($quote_submission_data['unit_price']) ? $quote_submission_data['unit_price'] : array();
			$product_notes = isset($quote_submission_data['product_notes']) ? $quote_submission_data['product_notes'] : array();
			$product_tax_rates = isset($quote_submission_data['product_tax_rate']) ? $quote_submission_data['product_tax_rate'] : array();
			$product_shipping_charges = isset($quote_submission_data['product_shipping_charges']) ? $quote_submission_data['product_shipping_charges'] : array();
			$product_other_charges = isset($quote_submission_data['product_other_charges']) ? $quote_submission_data['product_other_charges'] : array();

			$total_product_price = 0;
			for ($idx=0; $idx<count($product_names); $idx++)
			{
				$product_name = isset($product_names[$idx]) ? $product_names[$idx] : "";
				$quantity = isset($quantities[$idx]) ? $quantities[$idx] : 0;
				$price = isset($prices[$idx]) ? $prices[$idx] : 0;
				$unit_price = isset($unit_prices[$idx]) ? $unit_prices[$idx] : 0;
				$product_note = isset($product_notes[$idx]) ? $product_notes[$idx] : "";


				$product_tax_rate = isset($product_tax_rates[$idx]) ? $product_tax_rates[$idx] : 0;
				$product_shipping_charge = isset($product_shipping_charges[$idx]) ? $product_shipping_charges[$idx] : 0;
				$product_other_charge = isset($product_other_charges[$idx]) ? $product_other_charges[$idx] : 0;

				if (!is_numeric($quantity) || empty($quantity)) $quantity = 0;
				if (!is_numeric($price) || empty($price)) $price = 0;
				
				if (!empty($product_name) && $quantity && $unit_price)
				{  
					$total_product_price += $unit_price;

					//Save Qoute product
					$qouteVendorDetail = new QuoteVendorDetail();
					$qouteVendorDetail->rs = array();
					$qouteVendorDetail->rs['quote_id'] = $quote_id;
					$qouteVendorDetail->rs['vendor_id'] = $vendor_id;
					$qouteVendorDetail->rs['product_name'] = $product_name;
					$qouteVendorDetail->rs['quantity'] = $quantity;
					$qouteVendorDetail->rs['unit_price'] = $unit_price;
					$qouteVendorDetail->rs['tax_rate'] = $product_tax_rate;
					$qouteVendorDetail->rs['shipping'] = $product_shipping_charge;
					$qouteVendorDetail->rs['other_charges'] = $product_other_charge;
					$qouteVendorDetail->rs['vendor_product_notes'] = $product_note;
					$qouteVendorDetail->write();
				}
			}
			
			// update the values at the quote level next
			$tax_rate = isset($quote_submission_data['tax_rate']) ? $quote_submission_data['tax_rate'] : 0;
			if (!is_numeric($tax_rate) || empty($tax_rate)) $tax_rate = 0;
			
			$shipping = isset($quote_submission_data['shipping']) ? $quote_submission_data['shipping'] : 0;
			if (!is_numeric($shipping) || empty($shipping)) $shipping = 0;

			$other_charges = isset($quote_submission_data['other_charges']) ? $quote_submission_data['other_charges'] : 0;
			if (!is_numeric($other_charges) || empty($other_charges)) $other_charges = 0;

			$discount = isset($quote_submission_data['discount']) ? $quote_submission_data['discount'] : 0;
			if (!is_numeric($discount) || empty($discount)) $discount = 0;

			$total_price = isset($quote_submission_data['total_price']) ? $quote_submission_data['total_price'] : 0;
			if (!is_numeric($total_price) || empty($total_price)) $total_price = 0;

			$update_sql = "UPDATE quote_vendors
							  SET tax_rate = $tax_rate,
							  	  discount = $discount,
							  	  total_price = $total_price,
							  	  other_charges = $other_charges,
							  	  shipping = $shipping,
							  	  submit_status = $submit_quote,
							  	  notes = '$notes',
							  	  updated_datetime = NOW()
							WHERE quote_id = $quote_id AND vendor_id = $vendor_id";
			$this->executeQuery($update_sql);
			
			// finally create a notification for the user of the quote that supplier has sent a quote
			$vendor_data = $this->executeQuery("SELECT * FROM vendors WHERE vendor_id = $vendor_id", true);
			$quote_data = $this->executeQuery("SELECT * FROM quotes WHERE quote_id = $quote_id", true);
			
			if ($vendor_data && $quote_data && is_array($vendor_data) && is_array($quote_data))
			{
				$user_ids  = array();
				if(!empty($quote_data['created_by_user_id'])) $user_ids[$quote_data['created_by_user_id']] = $quote_data['created_by_user_id'];
				if(!empty($quote_data['manager_user_id'])) $user_ids[$quote_data['manager_user_id']] = $quote_data['manager_user_id'];
				if(!empty($quote_data['commercial_lead_user_id'])) $user_ids[$quote_data['commercial_lead_user_id']] = $quote_data['commercial_lead_user_id'];
				if(!empty($quote_data['procurement_lead_user_id'])) $user_ids[$quote_data['procurement_lead_user_id']] = $quote_data['procurement_lead_user_id'];
				if(!empty($submit_quote)){
				foreach($user_ids as $user_value){
					$notification = new Notification();
					$notification->rs = array();
					$notification->rs['id'] = 0;
					$notification->rs['user_id'] = $user_value;
					$notification->rs['notification_text'] =
						'Supplier ' . $vendor_data['vendor_name'] . ' has submitted a quote for '
						. '<a href="' . AppUrl::bicesUrl('quotes/edit/' . $quote_id) . '">Quote ID #' 
						. $quote_id . '</a> - ' . $quote_data['quote_name'];
					$notification->rs['notification_date'] = date("Y-m-d H:i");
					$notification->rs['read_flag'] = 0;
					$notification->write();
				}}
			}
		}		
	}

	public function getVendorQuote($quote_id, $vendor_id)
	{
		if(!empty($vendor_id)){
			$vendor_quote_details = $this->executeQuery("SELECT * FROM quote_vendors WHERE quote_id = $quote_id AND vendor_id = $vendor_id", true);
		}else{
			$vendor_quote_details = $this->executeQuery("SELECT * FROM quote_vendors WHERE quote_id = $quote_id", true);
		}
		if ($vendor_quote_details && is_array($vendor_quote_details)) return $vendor_quote_details; else return array();
	}		

	public function saveQuoteFile($quote_id, $file_name, $file_description)
	{	
	 $this->deleteFile($quote_id, $file_name);
	 $this->executeQuery("INSERT INTO quote_files (quote_id, file_name, description) SELECT $quote_id, '$file_name', '$file_description'");
	}

	public function deleteFile($quote_id, $file_name)
	{   
		$this->executeQuery("DELETE FROM quote_files WHERE quote_id = $quote_id AND file_name = '".$file_name."' ");
	}

	public function getQuoteFiles($quote_id)
	{
		return $this->executeQuery("SELECT * FROM quote_files WHERE quote_id = $quote_id ORDER BY id");
	}

	public function deleteQuestions($quote_id)
	{
		$this->executeQuery("DELETE FROM quote_questions WHERE quote_id = $quote_id");
	}
	
	public function saveQuoteQuestion($quote_id, $quote_question)
	{
		$this->executeQuery("INSERT INTO quote_questions (quote_id, question) SELECT $quote_id, '$quote_question'");
	}

	public function getQuoteQuestions($quote_id)
	{
		return $this->executeQuery("SELECT * FROM quote_questions WHERE quote_id = $quote_id ORDER BY id");
	}

	// stacked bar graph
	public function getQuoteGraph($quote_id,$vendor_id)
	{   error_reporting(0);
		if(is_array($vendor_id)){
			$vendor = " and vendors.vendor_id in(".implode(",", $vendor_id).")";
		}else if(!empty($vendor_id)){
			$vendor = " and vendors.vendor_id=".$vendor_id;
		}else{
			$vendor = "";
		}
		$stacked_bar_graph_data = $this->executeQuery("SELECT vendors.vendor_id, vendors.vendor_name, quote_vendor_details.*
          FROM vendors
          INNER JOIN quote_vendor_details ON vendors.vendor_id=quote_vendor_details.vendor_id WHERE quote_vendor_details.quote_id = $quote_id ".$vendor .' order by vendors.vendor_id desc');
		$vendor_name = [];
		$products = [];
		$product_prices = '';
		$idx = 0;
		$product_total = 0;
		$product_total_without_tax = 0;
		$product_prices_data ='';
		$vendorNameAndPrice = array();
		$productPriceInfo = array();
		$vendorProduct = array();
		$tool_currency = Yii::app()->session['user_currency'];
		foreach ($stacked_bar_graph_data as $key => $quote_graph) {

			$vendor_name[] =   $quote_graph['vendor_name'];

			$total_unit_price = $quote_graph['unit_price']*$quote_graph['quantity'];
			$product_tax_rate_price = ($quote_graph['tax_rate']/100)*$total_unit_price;
			$product_total_price = $total_unit_price+$product_tax_rate_price+$quote_graph['shipping']+$quote_graph['other_charges'];
			$product_total_without_tax_price = $total_unit_price+$quote_graph['shipping']+$quote_graph['other_charges'];
			$product_total += $product_total_price;
			$product_total_without_tax += $product_total_without_tax_price;

			if(!empty($quote_graph['product_code'])){
				$product_name = $quote_graph['product_name'].' ('.$quote_graph['product_code'].')';
			}else{
				$product_name = $quote_graph['product_name'];
			}
			$products[] = array('product_name'=>$product_name,'product_price'=>$product_total_price);

			if(array_key_exists($product_name,$productPriceInfo)){
				$productPriceInfo[$product_name] = $productPriceInfo[$product_name].','.($product_total_without_tax_price*FunctionManager::currencyRate($tool_currency));
			}else{
				$productPriceInfo[$product_name] = $product_total_without_tax_price*FunctionManager::currencyRate($tool_currency);
			}
			
			$vendorNameAndPrice[$quote_graph['vendor_name']][$product_name] = $vendorNameAndPrice[$quote_graph['vendor_name']][$product_name]+$product_total_price;

			$vendorProduct[$product_name][$quote_graph['vendor_name']] = $vendorProduct[$product_name][$quote_graph['vendor_name']]+$product_total_without_tax_price;

			$idx += 1;
			switch ($idx)
			{
				case 1  : $color = 'salmon'; break;
				case 2  : $color = 'purple'; break;
				case 3  : $color = 'green'; break;
				case 4  : $color = 'blue'; break;
				case 5  : $color = 'red'; break;
				case 6  : $color = 'antique-white'; break;
				case 7  : $color = 'aqua-marine'; break;
				case 8  : $color = 'bisque'; break;
				case 9  : $color = 'khaki'; break;
				default : $color = 'turquoise'; break;
			}
			/*$product_prices .= "{
               name: '$product_name',
               data: [$product_total_price],
               backgroundColor: '$color'
             },";*/

            $product_prices_data .= $product_total_price.",";

		}

		if(count($stacked_bar_graph_data)>0){
			$avgPrice = $product_total_without_tax/count($stacked_bar_graph_data);
		}else{
			$avgPrice = 0;
		}

		foreach($productPriceInfo as $prodName=>$prodPrice){
			$product_prices .= "{
               name: '$prodName',
               data: [$prodPrice],
             },";

		}
		$vendors =  implode(",", array_unique(str_replace('"Yes"', '',$vendor_name)));
		$result = array('total'=>$product_total,
			            'vendorCount'=>count(array_unique($vendor_name)),
			            'productCount'=>count($products),
			            'products'=>$products,
			            'totalPrice'=>$product_total_without_tax,
			            'avgPrice'=>$avgPrice,
			            'vendors'=>$vendors,
			            'datasets'=>substr_replace($product_prices,"", -1),
			            'quotePrice'=>$product_prices_data,
			        	'vendor_name'=>$vendor_name,
			        	'vendorProduct'=>$vendorProduct,);
		//CVarDumper::dump($productPriceInfo,10,1);die;
		return $result;
	}
	// line graph
	public function getQuoteAnswer($quote_id,$vendor_id)
	{

		if(is_array($vendor_id)){
			$vendor = " and vendor_id in(".implode(",", $vendor_id).")";
		}else if(!empty($vendor_id)){
			$vendor = " and vendor_id=".$vendor_id;
		}else {
			$vendor = "";
		}

		$results = $this->executeQuery("SELECT *
          FROM quote_questions
        
        INNER JOIN (select inn_ans.*,vend.vendor_name from quote_vendor_answers inn_ans  INNER JOIN vendors vend ON inn_ans.vendor_id=vend.vendor_id) quote_vendor_answers ON quote_questions.id=quote_vendor_answers.question_id WHERE quote_vendor_answers.quote_id = $quote_id ".$vendor."  order by vendor_id DESC");

		$quest = [];
		$score = [];
		$totalScore = 0;
		$avgScore = 0;

		$bestAnswered = '';
        $i=1;
		foreach($results as $result){

			if($result['question_type']=='free_text'){
				$question = 'Q'.$i;
				if($result['free_text']=='yes' || $result['free_text']=='YES' || $result['free_text']=='Yes'){
					$score[$result['vendor_name']] = $score['"'.$result['vendor_name'].'"'].",100";
					$totalScore+=100;
					if(empty($bestAnswered) || $bestAnswered<100){
						$bestAnswered = 'Q'.$i.' Scroe 100%';
					}
				}else{
					$score['"'.$result['vendor_name'].'"'] = $score['"'.$result['vendor_name'].'"'].",100";
					$totalScore+=100;
					if(empty($bestAnswered) || $bestAnswered<100){
						$bestAnswered = 'Q'.$i.' Scroe 100%';
					}
				}
				$quest[$question] = "'$question'";
			}elseif($result['question_type']=='yes_or_no'){
				$question = 'Q'.$i;
				if($result['yes_no']=='yes'){
					$score['"'.$result['vendor_name'].'"'] = $score['"'.$result['vendor_name'].'"'].",100";
					$totalScore+=100;
					if(empty($bestAnswered) || $bestAnswered<100){
						$bestAnswered = 'Q'.$i.' Scroe 100%';
					}

				}else{
					$score['"'.$result['vendor_name'].'"'] = $score['"'.$result['vendor_name'].'"'].",0";
					$totalScore+=0;
				}

				$quest[$question] = "'$question'";
			}else{

				$answer = $this->getAnswer($result['quote_answer']);
				$question = 'Q'.$i;
				if($answer['excellent']=='Excellent'){
					$score['"'.$result['vendor_name'].'"'] = $score['"'.$result['vendor_name'].'"'].",100";
					$totalScore+=100;
					if(empty($bestAnswered)){
						$bestAnswered = 'Q'.$i.' Scroe 100%';
					}
				}elseif($answer['good']=='Good'){
					$score['"'.$result['vendor_name'].'"'] = $score['"'.$result['vendor_name'].'"'].",80";
					$totalScore+=80;
					if(empty($bestAnswered)){
						$bestAnswered = 'Q'.$i.' Scroe 80%';
					}
				}elseif($answer['okay']=='Okay'){
					$score['"'.$result['vendor_name'].'"'] = $score['"'.$result['vendor_name'].'"'].",60";
					$totalScore+=60;
					if(empty($bestAnswered)){
						$bestAnswered = 'Q'.$i.' Scroe 60%';
					}
				}elseif($answer['bad']=='Bad'){
					$score['"'.$result['vendor_name'].'"'] = $score['"'.$result['vendor_name'].'"'].",40";
					$totalScore+=40;
					if(empty($bestAnswered)){
						$bestAnswered = 'Q'.$i.' Scroe 40%';
					}
				}else{
					$score['"'.$result['vendor_name'].'"'] = $score['"'.$result['vendor_name'].'"'].",20";
					$totalScore+=20;
					if(empty($bestAnswered)){
						$bestAnswered = 'Q'.$i.' Scroe 20%';
					}
				}
				$quest[$question] = "'$question'";
			}
			$i++;
			$score['"'.$result['vendor_name'].'"'] = ltrim($score['"'.$result['vendor_name'].'"'],",");
			
		}
		
		$questions =  implode(",", $quest);
		$scores =  implode(",", $score);
		if(count($results)>0){
			$avgScore = $totalScore/count($results);
		}else{
			$avgScore = 0;
		}
		
		foreach($score as $vendName=>$vendScore){
			$vendor_score .= "{
               name: $vendName,
               data: [$vendScore],
             },";

		}

		$result = array(
			'data'=>$results,
			'answers'=>$questions,
			'scores'=>$scores,
			'vendor_score'=>$vendor_score,
			'totalScore'=>$totalScore,
			'avgScore'=>$avgScore,
			'bestAnswered'=>$bestAnswered,
			);

		//CVarDumper::dump($result,10,1);die;
		return $result;
	}

	public function getQuoteOverAllStats($quote_id)
	{

		// calculate Stats for products
		  $products = $this->executeQuery("SELECT vendors.vendor_id, vendors.vendor_name, quote_vendor_details.*
          FROM vendors
          INNER JOIN quote_vendor_details ON vendors.vendor_id=quote_vendor_details.vendor_id WHERE quote_vendor_details.quote_id = $quote_id");

         $vendorProducts = $this->group_by("vendor_id", $products);


         $total = [];
		 $product_total = 0;
		 $overall_product_total = 0;
		 foreach($vendorProducts as $vendorProduct){
	      foreach($vendorProduct as $data) {
			$total_unit_price = $data['unit_price']*$data['quantity'];
			$product_total_price = $total_unit_price+$data['shipping']+$data['other_charges'];
			$product_total+= $product_total_price;
			$overall_product_total+= $product_total_price;
		  }

			$total[$data['vendor_id']] = $product_total;
			$product_total = 0;
		 }
		if(isset($total) && !empty($total)){
			$value = min($total);
			$key = array_search(min($total), $total);
			$vendor_name = $this->getVendorName($key);
			$bestPriceGiven = array('vendor_name'=>$vendor_name['vendor_name'],'price'=>$value);
			$supplier_count = $this->getCount($quote_id);
			if(count($supplier_count)>0){
				$avgPrice = $overall_product_total/count($supplier_count);
			}else{
				$avgPrice = 0;
			}
		}else{
			$bestPriceGiven = array('vendor_name'=>'None','price'=>0);
			$avgPrice = 0;
		}

		// calculate Stats for answers
		$answers = $this->executeQuery("SELECT *
          FROM quote_questions
        INNER JOIN quote_vendor_answers ON quote_questions.id=quote_vendor_answers.question_id WHERE quote_vendor_answers.quote_id = $quote_id" );
		$vendorScores = $this->group_by("vendor_id", $answers);
		$score = [];
		$totalScore = 0;
		$overAlltotalScore = 0;
		foreach($vendorScores as $vendorScore){
			foreach($vendorScore as $result) {
				if($result['question_type']=='yes_or_no'){
					if($result['yes_no']=='yes'){
						$totalScore+=100;
						$overAlltotalScore+=100;

					}else{
						$totalScore+=0;
						$overAlltotalScore+=0;
					}

				}else if($result['question_type']=='multiple_choice'){

					$answer = $this->getAnswer($result['quote_answer']);
					if($answer['excellent']=='Excellent'){
						$totalScore+=100;
						$overAlltotalScore+=100;
					}elseif($answer['good']=='Good'){
						$totalScore+=80;
						$overAlltotalScore+=80;
					}elseif($answer['okay']=='Okay'){
						$totalScore+=60;
						$overAlltotalScore+=60;
					}elseif($answer['bad']=='Bad'){
						$totalScore+=40;
						$overAlltotalScore+=40;

					}else{
						$totalScore+=20;
						$overAlltotalScore+=20;
					}
				}

			}

			$score[$result['vendor_id']] = $totalScore;
			$totalScore = 0;
		}

		if(isset($score) && !empty($score)){
			$value1 = max($score);
			$key1 = array_search(max($score), $score);
			$vendor_name1 = $this->getVendorName($key1);
			$supplier_count = $this->getCountVendor($quote_id);
			$bestScore = array('vendor_name'=>$vendor_name1['vendor_name'],'score'=>$value1);
			if(count($supplier_count)>0){
				$avgScore = $overAlltotalScore/count($supplier_count);
			}else{
				$avgScore = 0;
			}
		}else{
			$bestScore = array('vendor_name'=>'None','score'=>0);
			$avgScore = 0;
		}

		$result = array(
			'bestPrice'=>$bestPriceGiven,
			'avgPrice'=>$avgPrice,
			'bestScore'=>$bestScore,
			'avgScore'=>$avgScore
		);

		return $result;


	}

	function group_by($key, $data) {
		$result = array();

		foreach($data as $val) {
			if(array_key_exists($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}

		return $result;
	}

	public function getVendorName($vendor_id){
		$result = $this->executeQuery("SELECT * FROM vendors WHERE vendor_id = $vendor_id",1);
		return $result;
	}

	public function getCount($quote_id){
		$results = $this->executeQuery("SELECT * FROM quote_vendors WHERE quote_id = $quote_id");
		return $results;
	}

	public function getCountVendor($quote_id){
		$results = $this->executeQuery("SELECT count(vendor_id) as total_vendor FROM quote_vendor_answers WHERE quote_id = $quote_id group by vendor_id");
		return $results;
	}

	

	public function deleteQuoteDocumentandAnswers($quote_id, $vendor_id)
	{
//		$results = $this->executeQuery("SELECT * FROM quote_vendor_files WHERE quote_id = $quote_id AND vendor_id = $vendor_id");
//		foreach($results as $result){
//			unlink('uploads/quotes/' . $quote_id . '/' . $vendor_id . '/' . $result['file_name']);
//		}
		//$this->executeQuery("DELETE FROM quote_vendor_files WHERE quote_id = $quote_id AND vendor_id = $vendor_id");
		$this->executeQuery("DELETE FROM quote_vendor_answers WHERE quote_id = $quote_id AND vendor_id = $vendor_id");
	}

	public function getVendorProducts($quote_id,$vendor_id){
		$result = $this->executeQuery("SELECT * FROM quote_vendor_details WHERE quote_id = $quote_id AND vendor_id = $vendor_id");
		return $result;
	}

	public function getAnswer($answer_id){
		if($answer_id>0){
		$result = $this->executeQuery("SELECT * FROM quote_answers WHERE id = $answer_id",1);
		return $result;
		}
		return '';

	}

	public function scoringCriteria($quote_id=''){
		if(!empty($quote_id)){
			$result = $this->executeQuery("SELECT qs.score,qs.scoring_title as value,qs.id as quote_scoring_id,qs.vendor_scoring_id as id FROM  quote_scoring qs where qs.quote_id=".$quote_id);

		}else{
			$result = $this->executeQuery("SELECT * FROM vendor_scoring ");
		}
		
		return $result;
	}

	public function getVendorFiles($quote_id)
	{
		return $this->executeQuery("SELECT * FROM quote_vendor_files WHERE quote_id = $quote_id ORDER BY id");
	}


	public function getQuoteDocuments($quoteID)
    {
       $sql= "SELECT *, SUM(CASE WHEN status='Pending' THEN 1 ELSE 0 END) AS pendiing_documents,
              SUM(CASE WHEN status='Approved' THEN 1 ELSE 0 END) AS approved_documents from quote_communication where quote_id=$quoteID group by document_type order by document_type asc";
        return $this->executeQuery($sql);
        
    }


     public function approveDocument($documentID){
        $user_id = Yii::app()->session['user_id'];
        $user = new User;
        $userDetail = $user->getOne(array('user_id'=>$user_id));
        $approvedbyName = $userDetail['full_name'];
        $approvedTime = date("Y-m-d H:i:s");  
        if(is_array($documentID)){
          $sql = "update quote_communication set status='Approved',approved_by='".$user_id."',approved_datetime='".$approvedTime."',approved_by_name='".$approvedbyName."'  where id in(".implode(",", $documentID).")";
        }else{
          $sql = "update quote_communication set status='Approved',approved_by='".$user_id."',approved_datetime='".$approvedTime."',approved_by_name='".$approvedbyName."'  where id=".$documentID;
        }
        return $this->executeQuery($sql);
    }


    public function saveQuoteDocument($quote_id, $file_description, $file_name,$document_type,$document_status)
    {  
        $user_id = Yii::app()->session['user_id'];
        $dateCreated = date("Y-m-d H:i:s");
       
        if(!empty($quote_id) && !empty($file_name)){
          return Yii::app()->db->createCommand("INSERT INTO quote_communication (quote_id, uploaded_by, document_title,document_file,document_type,status,date_created) SELECT $quote_id,$user_id, '$file_description', '$file_name',$document_type,'$document_status','$dateCreated'")->execute();
        }
    }



     public function getCostSaving($quote_id,$vendor_id){

    	// query for quote_vendor_submission_history
        $sql = "SELECT sum(((tax_rate/100*unit_price*quantity) + shipping + other_charges +(unit_price*quantity))) AS total_cost FROM  quote_vendor_submission_history WHERE vendor_id = $vendor_id and quote_id=".$quote_id." group by reopen_id order by id ASC";
       
        $quoteHistory = Yii::app()->db->createCommand($sql)->query()->readAll();
         // query for Quote Vendor Details
        $sql = "SELECT sum(((tax_rate/100*unit_price*quantity) + shipping + other_charges + (unit_price*quantity))) AS total_cost FROM quote_vendor_details WHERE vendor_id = $vendor_id and quote_id=".$quote_id." group by vendor_id order by id ASC";
        $quoteDetails = Yii::app()->db->createCommand($sql)->query()->readAll();

        if(!empty($quoteHistory[0]) && !empty($quoteDetails[0])){
        	return number_format($quoteHistory[0]['total_cost']-$quoteDetails[0]['total_cost'],0,'','');
        }else{
        	return "0";
        }
 

    }

    public function defaultScoringCriteria($quote_id=''){
		
		$result = $this->executeQuery("SELECT qs.score,qs.scoring_title as value,qs.id as quote_scoring_id,qs.vendor_scoring_id as id, qs.socre_type FROM  quote_scoring qs where socre_type='Default' and score>0 and qs.quote_id=".$quote_id);
		if(empty($result)){
			$result = $this->executeQuery("SELECT * FROM vendor_scoring ");
		}
		
		
		return $result;
	}


	 public function defaultScoring($quote_id=''){
		
		$result = $this->executeQuery("SELECT qs.score,qs.scoring_title as value,qs.id as quote_scoring_id,qs.vendor_scoring_id as id, qs.socre_type FROM  quote_scoring qs where socre_type='Default' and score>0 and qs.quote_id=".$quote_id);
		
		
		return $result;
	}


    public function getQuoteQuestionires($quote_id=0){
    	$sql = "SELECT * FROM quote_scoring where socre_type='Custom' and quote_id=".$quote_id;
    	return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function getQuoteProducts($quote_id=0){
    	$sql = " SELECT * FROM quote_details where quote_id=".$quote_id;
    	return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function saveQuoteProducts($quote_id ,$product_names){	
	
		$comment = "";
		$quoteDetailId = [];
		foreach ($product_names as $key => $product_name)
		{	
		  if(!empty($_POST['quote_detail_id'][$key])){	
			$quoteDetailId[] = $_POST['quote_detail_id'][$key];
			
			$sql = "select * from quote_details WHERE id=".$_POST['quote_detail_id'][$key]." and quote_id =".$quote_id;
			$oldRecord = Yii::app()->db->createCommand($sql)->queryRow();
			if (strcmp(trim($oldRecord['product_name']), trim($product_name)) !== 0)
				$comment .= '<b>Product Name: </b> <span class="title-text"><b>' . $oldRecord['product_name'] . '</b> changed to <b>' . $product_name . '</b></span><br/>';
			if (strcmp(trim($oldRecord['product_code']), trim($_POST['product_code'][$key])) !== 0)
				$comment .= '<b>Product Code: </b> <span class="title-text"><b>' . $oldRecord['product_code'] . '</b> changed to <b>' . $_POST['product_code'][$key] . '</b></span><br/>';
			if(strcmp(trim($oldRecord['uom']), trim($_POST['uom'][$key])) !== 0)
				$comment .= '<b>Unit of Measure: </b> <span class="title-text"><b>' . $oldRecord['uom'] . '</b> changed to <b>' . $_POST['uom'][$key] . '</b></span><br/>';
			if(strcmp(trim($oldRecord['quantity']), trim($_POST['quantity'][$key])) !== 0){
				$comment .= '<b>Quantity: </b> <span class="title-text"><b>' . $oldRecord['quantity'] . '</b> changed to <b>' . $_POST['quantity'][$key] . '</b></span><br/>';
			}
		  }else{
			if (!empty($product_name))
				$comment .= '<b>Product Name: </b> <span class="title-text"></b> changed to <b>' . $product_name . '</b></span><br/>';
			if(!empty($_POST['product_code'][$key]))
				$comment .= '<b>Product Code: </b> <span class="title-text"><b>changed to <b>' . $_POST['product_code'][$key] . '</b></span><br/>';
			if(!empty($_POST['uom'][$key]))
				$comment .= '<b>Unit of Measure: </b> <span class="title-text"></b> changed to <b>' . $_POST['uom'][$key] . '</b></span><br/>';
			if(!empty($_POST['quantity'][$key]))
				$comment .= '<b>Quantity: </b> <span class="title-text">changed to <b>' . $_POST['quantity'][$key] . '</b></span><br/>';
		  }
			
		}	

		if (!empty($comment)) {
				$comment .= "<br />";
				$log = new QuoteLog();
				$log->rs = array();
				$log->rs['quote_id'] = $quote_id;
				$log->rs['user_id'] = Yii::app()->session['user_id'];
				$log->rs['comment'] = !empty($comment) ? $comment : "";
				$log->rs['created_datetime'] = date('Y-m-d H:i:s');
				$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
				$log->write();
		}
		
		if(!empty($quoteDetailId)){
					$sql = " select * from quote_details where id not in(". implode(',', $quoteDetailId).") and  quote_id=" . $quote_id;
					$deletedRecord = Yii::app()->db->createCommand($sql)->queryAll();
					$deleteComment = "";
					foreach($deletedRecord as $vValue){
						if (trim($vValue['product_name']))
							$deleteComment .= '<b style="color:red">Removed Product Name</b> <span class="title-text"><b>: ' . $vValue['product_name'] . '</b></span><br/>';
						if (trim($vValue['product_code']))
							$deleteComment .= '<b style="color:red">Removed Product Code</b> <span class="title-text"><b>: ' . $vValue['product_code'] . '</b></span><br/>';
						if(trim($vValue['uom']))
							$deleteComment .= '<b style="color:red">Removed Unit of Measure</b> <span class="title-text"><b>: ' . $vValue['uom'] . '</b></span><br/>';
						if(trim($vValue['quantity'])){
							$deleteComment .= '<b style="color:red">Removed Quantity</b> <span class="title-text"><b>: ' . $vValue['quantity'] . '</b></span><br/>';
						}
					}

					if (!empty($deleteComment)) {
						$log = new QuoteLog();
						$log->rs = array();
						$log->rs['quote_id'] = $quote_id;
						$log->rs['user_id'] = Yii::app()->session['user_id'];
						$log->rs['comment'] = !empty($deleteComment) ? $deleteComment : "";
						$log->rs['created_datetime'] = date('Y-m-d H:i:s');
						$log->rs['updated_datetime'] = date('Y-m-d H:i:s');
						$log->write();
					}
				}
		$this->executeQuery("DELETE FROM quote_details WHERE quote_id = $quote_id ");
		$quantity = $pcode = 0;
		$uom = "";
		foreach ($product_names as $key => $product_name)
		{
			$quantity = $pcode = 0;
			$uom = "";
			if (!empty($product_name))
			if(!empty($_POST['product_code'][$key]))
				$pcode = $_POST['product_code'][$key];
			if(!empty($_POST['uom'][$key]))
				$uom   = $_POST['uom'][$key];
			if(!empty($_POST['quantity'][$key]))
				$quantity = $_POST['quantity'][$key];
			if (!empty($product_name) && $quantity){
				$this->executeQuery("INSERT INTO quote_details (quote_id, product_name, quantity, uom,product_code) SELECT $quote_id, '".$product_name."', '$quantity', '$uom', '$pcode'");
			}
		}
	}	


    public function saveQuoteVendors($quote_id, $newVendors,$updatedVendors){

	   $this->executeQuery("delete FROM quote_vendors WHERE quote_id = $quote_id");
	   if(!empty($updatedVendors)){
	   foreach ($updatedVendors as $key=>$vendor_id){
	   		if(!empty($vendor_id)){
			$quoteVendorID =   $_POST['quote_vendors_id'][$key];
			$vendorReader = Yii::app()->db->createCommand("select * from vendors where vendor_id=".$vendor_id)->queryRow();
			$vendor_name  = $vendorReader['vendor_name'];
		    $contact_name = $emails = '';
			if(!empty($_POST['vendor_contact_update'][$key])){
				$contact_name = $_POST['vendor_contact_update'][$key];
			}
			if(!empty($_POST['vendor_email_update'][$key])){
				$emails = $_POST['vendor_email_update'][$key];
			}

		    $reference = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)))), 1, 24);
			$this->executeQuery("INSERT INTO quote_vendors (quote_id, vendor_id, created_datetime, updated_datetime,reference,vendor_name,contact_name,emails) 
								 SELECT $quote_id, $vendor_id, NOW(), NOW(),'".$reference."','".$vendor_name."','".$contact_name."','".$emails."'");
			
		}}}
		if(!empty($newVendors)){

		 foreach ($newVendors as $key=>$vendor_id){
			if(!empty($vendor_id)){
			
		    $vendorReader = Yii::app()->db->createCommand("select * from vendors where vendor_id=".$vendor_id)->queryRow();
			$vendor_name  = $vendorReader['vendor_name'];
			$contact_name = $emails = '';
			if(!empty($_POST['vendor_contact_new'][$key])){
				$contact_name = $_POST['vendor_contact_new'][$key];
			}
			if(!empty($_POST['vendor_email_new'][$key])){
				$emails = $_POST['vendor_email_new'][$key];
			}

			$reference = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)))), 1, 24);
			$this->executeQuery("INSERT INTO quote_vendors (quote_id, vendor_id, created_datetime, updated_datetime,reference,vendor_name,contact_name,emails) 
								 SELECT $quote_id, $vendor_id, NOW(), NOW(),'".$reference."','".$vendor_name."','".$contact_name."','".$emails."'");
			}
		  }
		}
	}


	public function getMySourcingActivities($start = 0, $length = 10, $search_for = "", $order_by = array(),$location_id, $department_id,$category_id, $subcategory_id, $quote_status, $db_from_date, $db_to_date, $userID)
	{	
		$now  = date("Y-m-d H:i:s");
		if (!empty($db_from_date))
		{
			if(FunctionManager::dateFormat()=="d/m/Y"){
	       		list($input_date, $input_month, $input_year) = explode("/", $db_from_date);
	       		$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
	       	}else{
	       		$db_from_date = date("Y-m-d",strtotime($db_from_date));
	       	}
		}
		if (!empty($db_to_date))
		{
			if(FunctionManager::dateFormat()=="d/m/Y"){
	       		list($input_date, $input_month, $input_year) = explode("/", $db_to_date);
	       		$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
	       	}else{
	       		$db_to_date = date("Y-m-d",strtotime($db_to_date));
	       	}
		}

        $user_id = Yii::app()->session['user_id'];
        $memberType = Yii::app()->session['member_type'];
		$userTypeID = Yii::app()->session['user_type'];
        if($userTypeID !="4" && ($memberType !=1 || $memberType =="0")){
         $up = "INNER JOIN user_privilege up On q.location_id = up.location_id and q.department_id= up.department_id and eSourcing_view='yes' and up.user_id=".$user_id;
        }else {$up = "";}

		$sql = "SELECT q.*,qu.contribute_deadline as contribute_deadline, qu.contribute_status,
		 qu.user_id as qu_userID, 
		 (select count(*) from quote_reopen_history where quote_id=q.quote_id) as total_reopen,
		 cat.value as quotes_by_category,u.profile_img,q.user_id, u.full_name, COUNT(DISTINCT v.id) AS invite_count,
			SUM(CASE 
    				WHEN v.submit_status = 2
    					THEN 1 
    				ELSE v.submit_status 
				END) AS submit_count,

			CASE 
				WHEN LOWER(q.quote_status) != 'cancelled' 
				and q.quote_status != 'Draft' and q.closing_date >'".$now."' 
				and awarded_vendor_id =0
    					THEN 'In Progress'

    			WHEN q.quote_status != 'Draft' and awarded_vendor_id !=0
    					THEN 'Awarded'

    			WHEN q.quote_status = 'Draft'  THEN 'Draft'		
    			WHEN q.quote_status != 'Draft' and q.closing_date <'".$now."'
    					THEN 'Complete'
    			WHEN qu.contribute_status = '1'  THEN 'Completed'	
    				ELSE q.quote_status
				END AS quote_status,

			(select qvw.total_price from quote_vendors qvw where  qvw.quote_id = q.quote_id and qvw.vendor_id=q.awarded_vendor_id group by quote_id) as vendor_submitted_price,
			(select GROUP_CONCAT(qvw.submit_status) from quote_vendors qvw where  qvw.quote_id = q.quote_id  group by quote_id) as quote_sub_status

		FROM quotes q LEFT JOIN users u ON q.user_id = u.user_id 
		LEFT JOIN quote_vendors v ON q.quote_id = v.quote_id
		LEFT JOIN categories cat ON q.category_id = cat.id
		LEFT JOIN (SELECT * FROM quote_user qu WHERE user_id = ".$userID." or create_user_id=".$userID.") qu ON qu.quote_id = q.quote_id 
		";
		

		$sqlRes = 'select quote_id,user_id,create_user_id from quote_user where user_id='.$userID;
        $quoteUsers  = Yii::app()->db->createCommand($sqlRes)->queryAll();
        $contribute = [$userID];
        $contributeQuoteID = [];
        $createUserID = [];
        foreach($quoteUsers as $quoteUserData){
         $contribute[] 		 = $quoteUserData['user_id'];
         $contributeQuoteID[]= $quoteUserData['quote_id'];
         $createUserID[]	 = $quoteUserData['create_user_id'];
    	}  
    	
    	// there are showing two type of data one if you have access as a contribute, second your own quote data    
	    if(in_array($userID, $contribute)){
	     $whereRow  = $in_quoteId_clause = $in_userId_clause= '';
	     if(!empty($contributeQuoteID)) { 
	     	$in_quoteId_clause= implode(',', $contributeQuoteID);
	     	$in_userId_clause = implode(',', $createUserID);
	     	$whereRow = " or q.user_id IN (".$in_userId_clause.") and q.quote_id IN (".$in_quoteId_clause.") "; 
	     }else{
			$whereRow = " or (qu.create_user_id=".$userID." or qu.user_id=".$userID.") ";
		}

	    $sql .= " where (q.user_id=". $userID ." ".$whereRow. " ) ";
	    }
	
		if (! empty($search_for)) {
            $search_for = trim($search_for);
            $search_for_date = date("Y-m-d",strtotime(strtr($search_for,'/','-')));

            $sql .= " and ( q.quote_id LIKE '%".$search_for."%'";
            $sql .= " or q.quote_name LIKE '%".$search_for."%'";
            $sql .= " or cat.value LIKE '%".$search_for."%'";
            $sql .= " or q.created_by_name LIKE '%".$search_for."%'";
            $sql .= " or q.quote_currency LIKE '%".$search_for."%'";
            $sql .= " or q.estimated_value LIKE '%".$search_for."%'";
            $sql .= " or q.awarded_vendor_name LIKE '%".$search_for."%'";
            if($search_for_date !="1970-01-01"){
              $sql .= " or DATE_FORMAT(q.opening_date,'%Y-%m-%d')='".$search_for_date."'";
              $sql .= " or DATE_FORMAT(q.closing_date,'%Y-%m-%d')='".$search_for_date."'";
              $sql .= " or DATE_FORMAT(qu.contribute_deadline,'%Y-%m-%d')='".$search_for_date."'";
            }
            $sql .= " ) ";
        }
        $order_by_clause = "";
        if (is_array($order_by) && count($order_by)) {
            foreach ($order_by as $column) {
                $column_index = $column['column'];
                $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                $column_name = "";
                switch ($column_index) {
                    case 0:
                        $column_name = "quote_id";
                        break;
                    case 1:
                        $column_name = "quote_id";
                        break;
                    case 2:
                        $column_name = "quote_name";
                        break;
                    case 3:
                        $column_name = "quotes_by_category";
                        break;
                    case 4:
                        $column_name = "opening_date";
                        break;
                    case 5:
                        $column_name = "closing_date";
                        break;
                    case 6:
                        $column_name = "created_by_name";
                        break;
                     case 7:
                        $column_name = "quote_currency";
                        break;
                    case 8:
                        $column_name = "awarded_vendor_name";
                        break;
                    case 9:
                        $column_name = "cost_savings";
                        break;
                    case 10:
                        $column_name = "quote_status";
                        break;
 					case 11:
                        $column_name = "quote_status";
                        break;

                }
                if (! empty($column_name)) {
                    if (empty($order_by_clause))
                        $order_by_clause = $column_name . ' ' . $column_direction;
                    else
                        $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                }
            }
        }

		if (!empty($location_id)) {
			$sql .= " AND q.location_id IN ($location_id)";
		}
		if (!empty($department_id)) {
			$sql .= " AND q.department_id IN ($department_id)";
		}
		if (!empty($category_id)) {
			$sql .= " AND q.category_id IN ($category_id)";
		}
		if (!empty($subcategory_id)) {
			$sql .= " AND q.subcategory_id IN ($subcategory_id)";
		}
		if (!empty($quote_status)){
			$sql .= " AND q.quote_status IN ($quote_status)";
		}
		if (!empty($db_from_date)) {
			$sql .= " AND q.opening_date>= '$db_from_date 00:00:00'";
		}
		if (!empty($db_to_date)) {
			$sql .= " AND q.closing_date <='$db_to_date 23:00:00'";
		}

		if($start != 'none'){
			$sql = $sql . " GROUP BY q.quote_id ORDER BY $order_by_clause LIMIT $start, $length ";
		}else{
			$sql = $sql . " GROUP BY q.quote_id ORDER BY $order_by_clause  ";
		}

		return $this->executeQuery($sql);
	}


	public function getMyScoring($start = 0, $length = 10, $search_for = "", $order_by = array(),$location_id, $department_id,$category_id, $subcategory_id, $quote_status, $db_from_date, $db_to_date, $userID='')
	{
		$now  = date("Y-m-d H:i:s");
		if (!empty($db_from_date))
		{
			if(FunctionManager::dateFormat()=="d/m/Y"){
	       		list($input_date, $input_month, $input_year) = explode("/", $db_from_date);
	       		$db_from_date = $input_year . '-' . $input_month . '-' . $input_date;
	       	}else{
	       		$db_from_date = date("Y-m-d",strtotime($db_from_date));
	       	}
		}
		if (!empty($db_to_date))
		{
			if(FunctionManager::dateFormat()=="d/m/Y"){
	       		list($input_date, $input_month, $input_year) = explode("/", $db_to_date);
	       		$db_to_date = $input_year . '-' . $input_month . '-' . $input_date;
	       	}else{
	       		$db_to_date = date("Y-m-d",strtotime($db_to_date));
	       	}
		}
		
		$sql = "SELECT q.*,(select count(*) from quote_reopen_history where quote_id=q.quote_id) as total_reopen,cat.value as quotes_by_category,u.profile_img,q.user_id, u.full_name, COUNT(DISTINCT v.id) AS invite_count,
			SUM(CASE 
    				WHEN v.submit_status = 2
    					THEN 1 
    				ELSE v.submit_status 
				END) AS submit_count,

			CASE 
				WHEN LOWER(q.quote_status) != 'cancelled' 
				and q.quote_status != 'Draft' and q.closing_date >'".$now."' 
				and awarded_vendor_id =0
    					THEN 'In Progress'

    			WHEN q.quote_status != 'Draft' and awarded_vendor_id !=0
    					THEN 'Awarded'

    			WHEN q.quote_status = 'Draft'  THEN 'Draft'		
    			WHEN q.quote_status != 'Draft' and q.closing_date <'".$now."'
    					THEN 'Complete'
    			
    				ELSE q.quote_status
				END AS quote_status,

			(select qvw.total_price from quote_vendors qvw where  qvw.quote_id = q.quote_id and qvw.vendor_id=q.awarded_vendor_id group by quote_id) as vendor_submitted_price,
			(select GROUP_CONCAT(qvw.submit_status) from quote_vendors qvw where  qvw.quote_id = q.quote_id  group by quote_id) as quote_sub_status

		FROM quotes q LEFT JOIN users u ON q.user_id = u.user_id 
		LEFT JOIN quote_vendors v ON q.quote_id = v.quote_id
		LEFT JOIN categories cat ON q.category_id = cat.id
		LEFT JOIN quote_scorer_user qsu ON qsu.quote_id = q.quote_id where qsu.user_id=".$userID;
		
		if (! empty($search_for)) {
            $search_for = trim($search_for);
            $search_for_date = date("Y-m-d",strtotime(strtr($search_for,'/','-')));

            $sql .= " and ( q.quote_id LIKE '%".$search_for."%'";
            $sql .= " or q.quote_name LIKE '%".$search_for."%'";
            $sql .= " or cat.value LIKE '%".$search_for."%'";
            $sql .= " or q.created_by_name LIKE '%".$search_for."%'";
            $sql .= " or q.quote_currency LIKE '%".$search_for."%'";
            $sql .= " or q.estimated_value LIKE '%".$search_for."%'";
            $sql .= " or q.awarded_vendor_name LIKE '%".$search_for."%'";
            if($search_for_date !="1970-01-01"){
              $sql .= " or DATE_FORMAT(q.opening_date,'%Y-%m-%d')='".$search_for_date."'";
              $sql .= " or DATE_FORMAT(q.closing_date,'%Y-%m-%d')='".$search_for_date."'";
            }
            $sql .= " ) ";
        }
        $order_by_clause = "";
        if (is_array($order_by) && count($order_by)) {
            foreach ($order_by as $column) {
                $column_index = $column['column'];
                $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                $column_name = "";
                switch ($column_index) {
                    case 0:
                        $column_name = "quote_id";
                        break;
                    case 1:
                        $column_name = "quote_id";
                        break;
                    case 2:
                        $column_name = "quote_name";
                        break;
                    case 3:
                        $column_name = "quotes_by_category";
                        break;
                    case 4:
                        $column_name = "opening_date";
                        break;
                    case 5:
                        $column_name = "closing_date";
                        break;
                    case 6:
                        $column_name = "created_by_name";
                        break;
                     case 7:
                        $column_name = "quote_currency";
                        break;
                    case 8:
                        $column_name = "awarded_vendor_name";
                        break;
                    case 9:
                        $column_name = "cost_savings";
                        break;
                    case 10:
                        $column_name = "quote_status";
                        break;
 					case 11:
                        $column_name = "quote_status";
                        break;

                }
                if (! empty($column_name)) {
                    if (empty($order_by_clause))
                        $order_by_clause = $column_name . ' ' . $column_direction;
                    else
                        $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                }
            }
        }

		if (!empty($location_id)) {
			$sql .= " AND q.location_id IN ($location_id)";
		}
		if (!empty($department_id)) {
			$sql .= " AND q.department_id IN ($department_id)";
		}
		if (!empty($category_id)) {
			$sql .= " AND q.category_id IN ($category_id)";
		}
		if (!empty($subcategory_id)) {
			$sql .= " AND q.subcategory_id IN ($subcategory_id)";
		}
		if (!empty($quote_status)){
			$sql .= " AND q.quote_status IN ($quote_status)";
		}
		if (!empty($db_from_date)) {
			$sql .= " AND q.opening_date>= '$db_from_date 00:00:00'";
		}
		if (!empty($db_to_date)) {
			$sql .= " AND q.closing_date <='$db_to_date 23:00:00'";
		}

		if($start != 'none'){
			$sql = $sql . " GROUP BY q.quote_id ORDER BY $order_by_clause LIMIT $start, $length ";
		}else{
			$sql = $sql . " GROUP BY q.quote_id ORDER BY $order_by_clause  ";
		}

		return $this->executeQuery($sql);
	}

	public function getQuoteMetric($type){
		switch($type) {
		 case "in_draft":
		  return Yii::app()->db->createCommand("SELECT count(*) FROM quotes WHERE quote_status ='Draft' ")->queryScalar();
		 break;
		 case "completed":
		   return Yii::app()->db->createCommand("SELECT count(*) FROM quotes WHERE quote_status not in('Draft','cancelled') and closing_date < CURDATE() ")->queryScalar();
		 break;
		 case "in_progress":
		   return Yii::app()->db->createCommand("SELECT count(*) FROM quotes WHERE quote_status != 'Draft' and awarded_vendor_id=0 and closing_date >= CURDATE() ")->queryScalar();
		 break;
		 case "awarded":
		   return Yii::app()->db->createCommand("SELECT count(*) FROM quotes WHERE quote_status != 'Draft' and awarded_vendor_id>0 ")->queryScalar();
		 break;
		 default:
			// Code to be executed if $type doesn't match any of the cases
			break;
		}

	}

	public function baseLinetotalByYear(){
		$sql = "select YEAR(awarded_datetime) as year, sum(cost_savings) as total_cost 
				from quotes where awarded_vendor_id>0 and cost_savings > 0 group by YEAR(awarded_datetime) ";
		$recordReader = Yii::app()->db->createCommand($sql)->queryAll();
		return $recordReader;
	}

	public function export($data = array(), $file = '')
	{	 
		$sql = "SELECT 
			q.quote_id as ID,
			q.quote_name as Title,
			cat.value as Category,
			sub_cat.value as Sub_Category,
			DATE_FORMAT(q.opening_date,'%d/%m/%Y') as 'Opening Date (UTC)',
			DATE_FORMAT(q.closing_date,'%d/%m/%Y') as 'Closing Date (UTC)',
			u.full_name as Created_By,
			GROUP_CONCAT(qv.vendor_name SEPARATOR ', ') as Suppliers_Invited,
			q.awarded_vendor_name as Awarded_Supplier,
			q.quote_currency as Currency,
			q.cost_savings as Cost_Savings,
			 CASE 
				WHEN LOWER(q.quote_status) != 'cancelled' 
					AND q.quote_status != 'Draft' 
					AND q.closing_date >= NOW() 
					AND q.awarded_vendor_id = 0 THEN 'In Progress'
					
				WHEN q.quote_status != 'Draft' 
					AND ( q.awarded_vendor_id IS NOT NULL
					AND q.awarded_vendor_id !=0) THEN 'Awarded'
					
				WHEN q.quote_status != 'Draft' 
					AND q.closing_date < NOW() THEN 'Complete'
					
				WHEN LOWER(q.quote_status) = 'cancelled' THEN 'Cancelled'
				WHEN q.quote_status = 'Draft' THEN 'Draft'
				ELSE ''
			END AS Status 
		    FROM quotes as q

		    LEFT JOIN categories cat ON q.category_id = cat.id 
		    LEFT JOIN sub_categories sub_cat ON q.subcategory_id = sub_cat.id
			LEFT JOIN quote_vendors qv ON q.quote_id = qv.quote_id
		    LEFT JOIN users u ON q.user_id = u.user_id
			group by q.quote_id
		    ORDER BY q.quote_id desc";
		parent::exportCommon($this->executeQuery($sql), 'quotes');
	}

}
