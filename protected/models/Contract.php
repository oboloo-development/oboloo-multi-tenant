<?php

class Contract extends Common
{

    public function __construct()
    {
        $this->fields = array(
            'contract_id' => 'N',
            'location_id' => 'N',
            'department_id' => 'N',
            'project_id' => 'N',
            //'spend_type' => 'C',
            'vendor_id' => 'N',
            'currency_id' => 'N',
            'currency_rate' => 'N',
            'user_id' => 'N',
            'user_name'=>'C',
            'contract_reference' => 'C',
            'po_number' => 'C',
            'cost_centre' => 'C',
            'break_clause' => 'D',
            'contract_title' => 'C',
            'contact_name' => 'C',
            'contact_info' => 'C',
            'phone_number' => 'N',
            'website' => 'C',
            'contract_description' => 'C',
            'contract_comments' => 'C',
            'termination_terms' => 'C',
            'termination_comments' => 'C',
            'extension_options' => 'C',
            'extension_comments' => 'C',
            'contract_type' => 'C',
            'contract_start_date' => 'D',
            'contract_end_date' => 'D',
            'original_end_date' => 'D',
            'contract_value' => 'N',
            'calc_contract_value' => 'N',
            'contract_budget' => 'N',
            'original_budget' => 'N',
            'original_annual_budget' => 'N',
            'contract_status' => 'N',
            'contract_manager' => 'C',
            'sbo_user_id' => 'N',
            'commercial_lead' => 'C',
            'commercial_user_id' => 'N',
            'procurement_lead' => 'C',
            'procurement_user_id' => 'N',
            'additional_user_lead_1' => 'C',
            'additional_user_1' => 'N',
            'additional_user_lead_2' => 'C',
            'additional_user_2' => 'N',
            'contract_category_id' => 'N',
            'contract_subcategory_id' => 'N',
            'contract_workstream' => 'C'
        );
        parent::__construct('contract_id', 'contracts');
    }
    

    public function getContracts($contract_id = 0, $location_id = 0, $department_id = 0, $category_id = 0, $subcategory_id = 0,$status_code=0)
    {    
        $this->currency_fields = array(
            'contract_value'
        );
         $sql = "SELECT c.*,(SELECT sum(score_value)/count(id) as avg_score from vendor_performance where vendor_id = c.vendor_id) as avg_score,u.full_name,c.contract_value as estimated_contract_value, v.vendor_name AS vendor,v.contact_name as vendor_contact_name,v.emails as vendor_email,v.phone_1 as vendor_phone, a.value AS category, s.value as subcategory,v.vendor_id,cr.currency,cr.currency_symbol,l.location_name 
				  FROM contracts c 
                  LEFT JOIN locations l ON c.location_id = l.location_id
                  LEFT JOIN users u ON c.user_id = u.user_id
		          LEFT JOIN vendors v ON c.vendor_id = v.vendor_id		  
		          LEFT JOIN categories a ON c.contract_category_id = a.id
		          LEFT JOIN currency_rates cr ON c.currency_id = cr.id
				  LEFT JOIN sub_categories s ON c.contract_subcategory_id = s.id ";
        if(!empty($contract_id)){
			$sql .= " WHERE 1=1 ";
        }else{
            $sql .= " WHERE c.contract_archive is null ";
        }
 
        if (!empty($location_id)) {
            $location_id = implode(',',$location_id);
            $sql .= " AND c.location_id IN ($location_id)";
        }

        /*if ($department_id)
            $sql .= " AND c.department_id = $department_id ";*/
        if(!empty($department_id)) {
            $department_id = implode(',',$department_id);
            $sql .= " AND c.department_id IN ($department_id)";
        }
        if ($category_id)
            $sql .= " AND c.contract_category_id = $category_id ";
        if ($subcategory_id)
            $sql .= " AND c.contract_subcategory_id = $subcategory_id ";
        if (!empty($_POST['contract_status']['0'])){

            $contractStatus = $_POST['contract_status'];
            $currentDate    = date("Y-m-d");
            $days30         = strtotime('+ 30 days');
            $months3         = strtotime('+ 3 months');
            $dateDays30     = date('Y-m-d', $days30);
            $dateMonths3     = date('Y-m-d', $months3);
             $sql .= " AND ( ";
             $sqlStatus = "";
            foreach($contractStatus as $status){
                if($status=='Active'){
                   $sqlStatus .= "  c.contract_end_date>='".$currentDate."'";
                }else if($status=='Expires in 30 days'){
                     if(!empty($sqlStatus))
                     $sqlStatus .= " OR ";
                     $sqlStatus .= " (contract_end_date>='".$currentDate."' and  c.contract_end_date<='".$dateDays30."')";
                }else if($status=='Expires in 3 months'){
                     if(!empty($sqlStatus))
                     $sqlStatus .= " OR ";
                     $sqlStatus .= " (contract_end_date>='".$currentDate."' and c.contract_end_date<='".$dateMonths3."')";
                }else if($status=='Expired'){
                     if(!empty($sqlStatus))
                     $sqlStatus .= " OR ";
                     $sqlStatus .= " c.contract_end_date<'".$currentDate."'";
                }
               
            }
             $sql .= $sqlStatus." ) ";
        }
        if ($contract_id)
            return $this->executeQuery($sql . " AND c.contract_id = $contract_id ", true);
        else
            return $this->executeQuery($sql . ' ORDER BY c.break_clause asc, 
                c.contract_reference ASC ');
    }

    public function getContractsAjax($contract_id = 0, $start = 0, $length = 10, $search_for = "", $order_by = array(), $location_id = 0, $department_id = 0, $category_id = 0, $subcategory_id = 0,$status_code=0)
    {    
        $this->currency_fields = array(
            'contract_value'
        ); 
        $user_id = Yii::app()->session['user_id'];
        $memberType = Yii::app()->session['member_type'];
        $userTypeID = Yii::app()->session['user_type'];
        if($userTypeID !="4" && ($memberType !=1 || $memberType =="0")){
            $up = "INNER JOIN user_privilege up On c.location_id = up.location_id and c.department_id= up.department_id and contract_view='yes' and up.user_id=".$user_id;
        }else {$up = "";}

        $sql = "SELECT c.*,(SELECT sum(score_value)/count(id) as avg_score from vendor_performance where vendor_id = c.vendor_id) as avg_score,u.full_name,u.profile_img,c.contract_value as estimated_contract_value, v.vendor_name AS vendor,v.contact_name as vendor_contact_name,v.emails as vendor_email,v.phone_1 as vendor_phone, a.value AS category, s.value as subcategory,v.vendor_id,cr.currency,cr.currency_symbol 
                  FROM contracts c 
                  LEFT JOIN users u ON c.sbo_user_id = u.user_id
                  LEFT JOIN vendors v ON c.vendor_id = v.vendor_id        
                  LEFT JOIN categories a ON c.contract_category_id = a.id
                  LEFT JOIN currency_rates cr ON c.currency_id = cr.id
                  LEFT JOIN sub_categories s ON c.contract_subcategory_id = s.id 
                  LEFT JOIN contract_location cl ON c.contract_id = cl.contract_id
                  LEFT JOIN contract_department dl ON c.contract_id = dl.contract_id
                  ".$up;
        if(!empty($contract_id)){
            $sql .= " WHERE 1=1 ";
        }else if(Yii::app()->controller->action->id=="listArchiveAjax"){
            $sql .= " WHERE c.contract_archive='yes'";
        }else{
            $sql .= " WHERE c.contract_archive='' or c.contract_archive is null ";
        }
        if (! empty($search_for)) {
            $search_for = trim($search_for);
            $search_for_date = date("Y-m-d",strtotime(strtr($search_for,'/','-')));

            $sql .= " and ( c.contract_title LIKE '%".$search_for."%'";
            //$sql .= " or CONCAT(cr.currency_symbol,c.contract_value) LIKE '%".number_format($search_for,0,"","")."%'";
            if(!empty(filter_var($search_for, FILTER_SANITIZE_NUMBER_INT))){
                $sql .= " or c.contract_value LIKE '%".filter_var($search_for, FILTER_SANITIZE_NUMBER_INT)."%'";
            }
            $sql .= " or a.value LIKE '%".$search_for."%'";
            $sql .= " or c.contract_manager LIKE '%".$search_for."%'";
            $sql .= " or v.vendor_name LIKE '%".$search_for."%'";
            $sql .= " or cr.currency LIKE '%".$search_for."%'";
            //$sql .= " or c.contract_value LIKE '%".$search_for."%'";
            if($search_for_date !="1970-01-01"){
              $sql .= " or c.contract_start_date='".$search_for_date."'";
              $sql .= " or c.break_clause='".$search_for_date."'";
              $sql .= " or c.contract_end_date='".$search_for_date."'";
             }
            $sql .= " ) ";
        }

        $order_by_clause = "";
        if (is_array($order_by) && count($order_by)) {
            foreach ($order_by as $column) {
                $column_index = $column['column'];
                $column_direction = strtoupper(substr($column['dir'], 0, 1)) == 'D' ? 'DESC' : 'ASC';
                $column_name = "";
                switch ($column_index) {
                    case 1:
                        $column_name = "contract_title";
                        break;
                    case 2:
                        $column_name = "category";
                        break;
                    case 3:
                        $column_name = "contract_manager";
                        break;
                    case 4:
                        $column_name = "vendor";
                        break;
                    case 5:
                        $column_name = "currency";
                        break;
                    case 6:
                        $column_name = "estimated_contract_value";
                        break;
                    case 7:
                        $column_name = "contract_start_date";
                        break;
                     case 8:
                        $column_name = "break_clause";
                        break;
                   
                    case 9:
                        $column_name = "contract_end_date";
                        break;  
                }
                if (! empty($column_name)) {
                    if (empty($order_by_clause))
                        $order_by_clause = $column_name . ' ' . $column_direction;
                    else
                        $order_by_clause = $order_by_clause . ', ' . $column_name . ' ' . $column_direction;
                }
            }
            }
 
        if (!empty($location_id)) {
            $sql .= " AND cl.location_id IN ($location_id)";
        }

        if(!empty($department_id)) {
            $sql .= " AND dl.department_id IN ($department_id)";
        }
        if ($category_id)
            $sql .= " AND c.contract_category_id = $category_id ";
        if ($subcategory_id)
            $sql .= " AND c.contract_subcategory_id = $subcategory_id ";
        if (!empty($_POST['contract_status'])){

            $contractStatus = $_POST['contract_status'];
            $currentDate    = date("Y-m-d");
            $days30         = strtotime('+ 30 days');
            $months3        = strtotime('+ 3 months');
            $dateDays30     = date('Y-m-d', $days30);
            $dateMonths3    = date('Y-m-d', $months3);
            $sql .= " AND ( ";
            $sqlStatus = "";

            if(!empty($contractStatus) && $contractStatus=='Active'){
               $sqlStatus .= "  c.contract_end_date>='".$currentDate."'";
            }else if(!empty($contractStatus) && $contractStatus=='Expires in 30 days'){
                 if(!empty($sqlStatus))
                 $sqlStatus .= " OR ";
                 $sqlStatus .= " (contract_end_date>='".$currentDate."' and  c.contract_end_date<='".$dateDays30."')";
            }else if(!empty($contractStatus) && $contractStatus=='Expires in 3 months'){
                 if(!empty($sqlStatus))
                 $sqlStatus .= " OR ";
                 $sqlStatus .= " (contract_end_date>='".$currentDate."' and c.contract_end_date<='".$dateMonths3."')";
            }else if(!empty($contractStatus) && $contractStatus=='Expired'){
                 if(!empty($sqlStatus))
                 $sqlStatus .= " OR ";
                 $sqlStatus .= " c.contract_end_date<'".$currentDate."'";
            }
               
            $sql .= $sqlStatus." ) ";
        }
        if (empty($order_by_clause))
           $order_by_clause = "contract_id";
        
        if($start != 'none'){
			$sql = $sql . " group by c.contract_id  ORDER BY $order_by_clause LIMIT $start, $length ";
		}else{
			$sql = $sql . " group by c.contract_id ORDER BY $order_by_clause ";
		}
       
         return $this->executeQuery($sql);
    }


    public function getArchiveContracts($contract_id = 0, $location_id = 0, $department_id = 0, $category_id = 0, $subcategory_id = 0,$status_code=0)
    {
        $this->currency_fields = array(
            'contract_value'
        );

        $user_id = Yii::app()->session['user_id'];
        $memberType = Yii::app()->session['member_type'];
        if($memberType !=1 || $memberType =="0"){
            $up = "INNER JOIN user_privilege up On c.location_id = up.location_id and c.department_id= up.department_id and contract_view='yes' and up.user_id=".$user_id;
        }else {$up = "";}

         $sql = "SELECT c.*,(SELECT sum(score_value)/count(id) as avg_score from vendor_performance where vendor_id = c.vendor_id) as avg_score,u.full_name,c.contract_value as estimated_contract_value, v.vendor_name AS vendor,v.contact_name as vendor_contact_name,v.emails as vendor_email,v.phone_1 as vendor_phone, a.value AS category, s.value as subcategory,v.vendor_id,cr.currency,cr.currency_symbol 
                  FROM contracts c 
                  LEFT JOIN users u ON c.user_id = u.user_id
                  LEFT JOIN vendors v ON c.vendor_id = v.vendor_id        
                  LEFT JOIN categories a ON c.contract_category_id = a.id
                  LEFT JOIN currency_rates cr ON c.currency_id = cr.id
                  LEFT JOIN sub_categories s ON c.contract_subcategory_id = s.id
                  LEFT JOIN contract_location cl ON c.contract_id = cl.contract_id
                  LEFT JOIN contract_department dl ON c.contract_id = dl.contract_id
                  ".$up."
                  ";
         
            $sql .= " WHERE c.contract_archive is not null ";
        
 
        if (!empty($location_id)) {
            $sql .= " AND cl.location_id IN ($location_id)";
        }

        if(!empty($department_id)) {
            $sql .= " AND dl.department_id IN ($department_id)";
        }
        if (!empty($category_id))
            $sql .= " AND c.contract_category_id = $category_id ";
        if (!empty($subcategory_id))
            $sql .= " AND c.contract_subcategory_id = $subcategory_id ";
        if (!empty($_POST['contract_status'])){
            $contractStatus = $_POST['contract_status'];
            $currentDate    = date("Y-m-d");
            $days30         = strtotime('+ 30 days');
            $months3         = strtotime('+ 3 months');
            $dateDays30     = date('Y-m-d', $days30);
            $dateMonths3     = date('Y-m-d', $months3);
             $sql .= " AND ( ";
             $sqlStatus = "";
            foreach($contractStatus as $status){
                if($status=='Active'){
                   $sqlStatus .= "  c.contract_end_date>='".$currentDate."'";
                }else if($status=='Expires in 30 days'){
                     if(!empty($sqlStatus))
                     $sqlStatus .= " OR ";
                     $sqlStatus .= " (contract_end_date>='".$currentDate."' and  c.contract_end_date<='".$dateDays30."')";
                }else if($status=='Expires in 3 months'){
                     if(!empty($sqlStatus))
                     $sqlStatus .= " OR ";
                     $sqlStatus .= " (contract_end_date>='".$currentDate."' and c.contract_end_date<='".$dateMonths3."')";
                }else if($status=='Expired'){
                     if(!empty($sqlStatus))
                     $sqlStatus .= " OR ";
                     $sqlStatus .= " c.contract_end_date<'".$currentDate."'";
                }
               
            }
             $sql .= $sqlStatus." ) ";
        }
        if ($contract_id)
            return $this->executeQuery($sql . " AND c.contract_id = $contract_id ", true);
        else
        
            return $this->executeQuery($sql . ' ORDER BY c.break_clause asc, c.contract_reference ASC ');
    }

    public function saveContractDocument($contract_id, $file_description, $file_name,$document_type,$document_status,$expiry_date)
    {  
        $user_id = Yii::app()->session['user_id'];
        $dateCreated = date("Y-m-d H:i:s");
        if(FunctionManager::dateFormat()=="d/m/Y"){
            $expiryDate  =  date("Y-m-d", strtotime(strtr($expiry_date, '/', '-')));
        }else{
            $expiryDate  =  date("Y-m-d", strtotime($expiry_date));
        }
        if(!empty($contract_id) && !empty($file_name)){
          return Yii::app()->db->createCommand("INSERT INTO contract_documents (contract_id, uploaded_by, document_title,document_file,document_type,status,date_created,expiry_date) SELECT $contract_id,$user_id, '$file_description', '$file_name',$document_type,'$document_status','$dateCreated','$expiryDate'")->execute();
        }
    }


    public function getContractsDocuments($contractID,$type="active")
    {
          if($type=="active"){
            $archive = "archive !=1";
          }else{
            $archive = "archive =1";
          }
            $sql= "SELECT contract_documents.*, SUM(CASE WHEN status='Pending' THEN 1 ELSE 0 END) AS pendiing_documents,
              SUM(CASE WHEN status='Approved' THEN 1 ELSE 0 END) AS approved_documents from contract_documents,contract_document_type doc_type where $archive and contract_id=$contractID and contract_documents.document_type = doc_type.id group by document_type order by doc_type.name asc ";

        return $this->executeQuery($sql);
        
    }

    public function getContractsDocumentsPendingRequest($contractID)
    {
        $sql= "SELECT crd.*,cdt.name as type_name,cdt.id as type_id,crd.id request_id FROM `contract_request_documents` as crd 
          left join contract_document_type as cdt on crd.contract_type = cdt.id
          where crd.contract_id=".$contractID." and crd.status='Pending' order by cdt.name asc";
        return $this->executeQuery($sql);
        
    }

    public function approveDocument($documentID){
        $user_id = Yii::app()->session['user_id'];
        $user = new User;
        $userDetail = $user->getOne(array('user_id'=>$user_id));
        $approvedbyName = $userDetail['full_name'];
        $approvedTime = date("Y-m-d H:i:s");  
        if(is_array($documentID)){
          $sql = "update contract_documents set status='Approved',approved_by='".$user_id."',approved_datetime='".$approvedTime."',approved_by_name='".$approvedbyName."'  where id in(".implode(",", $documentID).")";
        }else{
          $sql = "update contract_documents set status='Approved',approved_by='".$user_id."',approved_datetime='".$approvedTime."',approved_by_name='".$approvedbyName."'  where id=".$documentID;
        }
        return $this->executeQuery($sql);
    }

    public function archiveDocument($documentID){
        $user_id = Yii::app()->session['user_id'];
        $user = new User;
        $userDetail = $user->getOne(array('user_id'=>$user_id));
        $approvedbyName = $userDetail['full_name'];
        $archiveTime = date("Y-m-d H:i:s");  
        if(is_array($documentID)){
          $sql = "update contract_documents set archive=1,archive_datetime='".$archiveTime."' where id in(".implode(",", $documentID).")";
        }else{
          $sql = "update contract_documents set  archive=1,archive_datetime='".$archiveTime."' where id=".$documentID;
        }
        return $this->executeQuery($sql);
    }

     public function unArchiveDocument($documentID){
     
        $archiveTime = date("Y-m-d H:i:s");  
        if(is_array($documentID)){
          $sql = "update contract_documents set archive=0 where id in(".implode(",", $documentID).")";
        }else{
          $sql = "update contract_documents set  archive=0 where id=".$documentID;
        }
       
        return $this->executeQuery($sql);
    }

    public function editDocument($postedArr)
    { 
        $documentID = $postedArr['document_id'];
        $expiryDate = $postedArr['expiry_date'];

         if(FunctionManager::dateFormat()=="d/m/Y"){
            $expiryDate  =  date("Y-m-d", strtotime(strtr($expiryDate, '/', '-')));
        }else{
            $expiryDate  =  date("Y-m-d", strtotime($expiryDate));
        }

       
        $approvedTime = date("Y-m-d H:i:s");
        if(!empty($documentID)){
          $sql = "update contract_documents set expiry_date='".$expiryDate."' where id in(".$documentID.")";
        }
        return Yii::app()->db->createCommand($sql)->execute();
    }


    public function getVendorContracts($vendor_id,$start_date,$end_date)
    {
        $contDurationCond = '';
        if(Yii::app()->controller->id !="vendors" && Yii::app()->controller->action->id!="edit"){
            $contDurationCond = " AND (contract_start_date >= '".$start_date."' AND contract_end_date <= '".$end_date."')";
        }
        $sql = "SELECT c.*,getOrderTotalInGB(c.contract_value,c.currency_id) as contract_value_GB, v.vendor_name AS vendor, a.value AS category, s.value as subcategory,v.vendor_id,cr.currency
				  FROM contracts c
		          LEFT JOIN vendors v ON c.vendor_id = v.vendor_id
		          LEFT JOIN categories a ON c.contract_category_id = a.id
		          LEFT JOIN currency_rates cr ON c.currency_id = cr.id
				  LEFT JOIN sub_categories s ON c.contract_subcategory_id = s.id
				  WHERE c.vendor_id = $vendor_id ".$contDurationCond;
                return $this->executeQuery($sql);
    }

    public function getProjectContracts($project_id,$start_date,$end_date)
    {
        $contDurationCond = '';
        if(Yii::app()->controller->id !="vendors" && Yii::app()->controller->action->id!="edit"){
            $contDurationCond = " AND (contract_start_date >= '".$start_date."' AND contract_end_date <= '".$end_date."')";
        }
        $sql = "SELECT c.*,getOrderTotalInGB(c.contract_value,c.currency_id) as contract_value_GB, v.vendor_name AS vendor, a.value AS category, s.value as subcategory,v.vendor_id,cr.currency
                  FROM contracts c
                  LEFT JOIN vendors v ON c.vendor_id = v.vendor_id
                  LEFT JOIN categories a ON c.contract_category_id = a.id
                  LEFT JOIN currency_rates cr ON c.currency_id = cr.id
                  LEFT JOIN sub_categories s ON c.contract_subcategory_id = s.id
                  WHERE c.project_id = $project_id ".$contDurationCond;
                return $this->executeQuery($sql);
    }


    public function getContractsTitle($contract_id = 0)
    {
        $sql = "SELECT * FROM contracts";
        
        // if ($contract_id)
        return $this->executeQuery($sql, true);
    }

    public function getTopContracts($start_date, $end_date)
    {   
        $this->currency_fields = array(
            //'total'
        );

        $tool_currency = Yii::app()->session['user_currency'];
       
        

        if((Yii::app()->controller->id =='contracts' && Yii::app()->controller->action->id =='list') || (Yii::app()->controller->id =='vendors' && Yii::app()->controller->action->id =='list')){
        $sql = "SELECT currency_id,contract_id, IFNULL(contract_title, vendor_name) AS dim_1,getTotalInGBandToolCurrency(contract_value,currency_id,'".$tool_currency."') AS total, '' AS dim_2, '' AS dim_3, '' AS dim_4
				  FROM contracts c

				LEFT JOIN vendors v ON c.vendor_id = v.vendor_id 

                WHERE c.contract_end_date>='".$end_date."'

				ORDER BY contract_value DESC LIMIT 10";
                
        }else {
            $sql = "SELECT currency_id,contract_id, IFNULL(contract_title, vendor_name) AS dim_1,
                       getTotalInGBandToolCurrency(contract_value,currency_id,'".$tool_currency."') AS total, '' AS dim_2, '' AS dim_3, '' AS dim_4
                  FROM contracts c
                  LEFT JOIN vendors v ON c.vendor_id = v.vendor_id
                 WHERE c.contract_end_date>='".$end_date."'
                 ORDER BY contract_value DESC LIMIT 10";
        }
        $contracts = array();
        $total_value = 0;
        foreach ($this->executeQuery($sql) as $contract) {
            $currency_id = $contract['currency_id'];
            $total_value += $contract['total'];
            $contracts[] = $contract;
        }
        
        foreach ($contracts as $contract_id => $contract)
            if ($total_value)
                $contracts[$contract_id]['percent'] = (($contract['total'] * 100.0) / $total_value);
            else
                $contracts[$contract_id]['percent'] = 0;
        
        $this->currency_fields = array();
        return $contracts;
    }

    public function madeContractLogComment($contract_id = 0)
    {
        $sql = "SELECT * FROM contracts WHERE  contract_id = $contract_id";
        return $this->executeQuery($sql);
    }

    public function getContractStatusTotals($start_date, $end_date)
    {
        $sql = "SELECT contract_status AS `status`, COUNT(*) AS `total` FROM contracts 
				 WHERE  contract_archive is null and ( '$start_date' BETWEEN contract_start_date AND contract_end_date
				   		 OR '$end_date' BETWEEN contract_start_date AND contract_end_date)
				 GROUP BY contract_status";
        
        $status_totals = array();
        foreach ($this->executeQuery($sql) as $status_row) {
            if ($status_row['status'] == 1)
                $status_row['status'] = 'Active';
            else if ($status_row['status'] == 2)
                $status_row['status'] = 'Renew';
            else if ($status_row['status'] == 0)
                $status_row['status'] = 'Expired';
            else if ($status_row['status'] == 3)
                $status_row['status'] = 'Notice of Termination';
            else if ($status_row['status'] == 4)
                $status_row['status'] = 'Expired Still Active';
            $status_totals[$status_row['status']] = $status_row;
        }
        
        return $status_totals;
    }

    public  function contractStatuses($key='')
    {
        if($key ==''){
            $sql = "SELECT * FROM contract_status GROUP BY value";
        }else if($key !=''){
            $sql = "SELECT value FROM contract_status where code=".$key;
        }
        $statues = $this->executeQuery($sql);
        return $statues;
    }

    public function getContractLocationTotals($start_date, $end_date)
    { 
        /*$sql = "SELECT location_name, COUNT(*) AS `total` 
				  FROM contracts c, locations l
				 WHERE  l.location_id = c.location_id
		           AND c.contract_end_date>='".$end_date."'
				 GROUP BY location_name
				 ORDER BY `total` DESC";*/

        $days30         = strtotime('+ 30 days');
        $months3        = strtotime('+ 3 months');
        $dateDays30     = date('Y-m-d', $days30);
        $dateMonths3    = date('Y-m-d', $months3);
        $currentDate    = date("Y-m-d");

         /*$sql = 'select location_name,
            CASE
            WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays30.'" THEN count(*) as "Expires in 30 Days"
            WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays30.'" and "'.$dateMonths3.'" THEN count(*) as "Expires in 3 Months"

            WHEN date_format(contract_end_date,"%Y-%m-%d") >= "'.$currentDate.'" THEN count(*) as "Active"
            WHEN date_format(contract_end_date,"%Y-%m-%d") < "'.$currentDate.'" THEN count(*) as "Expired"
            
            ELSE "NULL"

            END as duration
            from contracts c INNER JOIN locations l on l.location_id = c.location_id group by location_name order by duration desc';*/

        $sql = 'select location_name,
            SUM(
                CASE
                   WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$currentDate.'" and "'.$dateDays30.'" THEN 1
                   ELSE 0
                END) AS "End Date Within 30 Days",

            SUM(
                CASE
                    WHEN date_format(contract_end_date,"%Y-%m-%d") between "'.$dateDays30.'" and "'.$dateMonths3.'" THEN 1
                    ELSE 0
                END) AS "End Date Within 3 Months",
            SUM(
                CASE
                    WHEN date_format(contract_end_date,"%Y-%m-%d") >= "'.$dateMonths3.'" THEN  1
                    ELSE 0
                END) AS "End Date Over 3 Months",
            SUM(
                CASE 
                    WHEN date_format(contract_end_date,"%Y-%m-%d") < "'.$currentDate.'" THEN  1
                    ELSE 0
                END) AS "Passed Contract End Date"
            
            from contracts c INNER JOIN locations l on l.location_id = c.location_id 

            where contract_archive is null 
            group by location_name';
          

        return $this->executeQuery($sql);
    }

    public function getVendorContractLocationTotals($vendor_id,$start_date, $end_date)
    {
        $sql = "SELECT location_name, COUNT(*) AS `total`
				   FROM contracts c
				   INNER JOIN locations l ON c.location_id =  l.location_id
				   INNER JOIN vendors v ON c.vendor_id =  v.vendor_id
				   WHERE c.vendor_id = $vendor_id
				   GROUP BY location_name
				   ORDER BY `total` DESC";

       // CVarDumper::dump($this->executeQuery($sql),10,1);die;
      /* AND (contract_start_date >= '$start_date' AND contract_end_date <= '$end_date'
       OR  contract_start_date >= '$start_date' AND contract_end_date <= '$end_date') */
        return $this->executeQuery($sql);
    }

    public function getProjectContractLocationTotals($project_id,$start_date, $end_date)
    {
        $sql = "SELECT location_name, COUNT(*) AS `total`
                   FROM contracts c
                   INNER JOIN locations l ON c.location_id =  l.location_id
                   
                   WHERE c.project_id = $project_id
                   GROUP BY location_name
                   ORDER BY `total` DESC";

       // CVarDumper::dump($this->executeQuery($sql),10,1);die;
      /* AND (contract_start_date >= '$start_date' AND contract_end_date <= '$end_date'
       OR  contract_start_date >= '$start_date' AND contract_end_date <= '$end_date') */
        return $this->executeQuery($sql);
    }

    public function getContractCategoryTotals($start_date, $end_date)
    {
        $sql = "SELECT g.value AS category_name, COUNT(*) AS `total` 
				  FROM contracts c, categories g
				 WHERE   c.contract_archive is null and g.id = c.contract_category_id
		           AND c.contract_end_date>='".$end_date."'
				 GROUP BY g.value
				 ORDER BY `total` DESC";
        return $this->executeQuery($sql);
    }
    public function getContractActiveValuesStats()
    {
        $sql = "SELECT c.vendor_id,c.contract_title,c.contract_reference,c.contract_start_date,c.contract_end_date,c.contract_value,v.vendor_name FROM contracts c
                LEFT JOIN vendors v ON v.vendor_id = c.vendor_id
                WHERE contract_status = 1 AND contract_end_date >= NOW() AND contract_start_date <= NOW()";
        return $this->executeQuery($sql);
    }

    public function getContractExpire30DaysStats()
    {
        $expiry_date = date("Y-m-d", strtotime("+30 Days"));

        $sql = "SELECT c.vendor_id,c.contract_title,c.contract_reference,c.contract_start_date,c.contract_end_date,c.contract_value,v.vendor_name FROM contracts c
                LEFT JOIN vendors v ON v.vendor_id = c.vendor_id
                WHERE contract_status = 1 AND contract_end_date >= NOW()
				AND contract_end_date <= '$expiry_date' AND contract_start_date <= NOW()";
        return $this->executeQuery($sql);
    }

    public function getContractExpire91DaysStats()
    {
        $expiry_date = date("Y-m-d", strtotime("+91 Days"));
        $sql = "SELECT c.vendor_id,c.contract_title,c.contract_reference,c.contract_start_date,c.contract_end_date,c.contract_value,v.vendor_name FROM contracts c
                LEFT JOIN vendors v ON v.vendor_id = c.vendor_id
                WHERE contract_status = 1 AND contract_end_date >= NOW()
				AND contract_end_date <= '$expiry_date' AND contract_start_date <= NOW()";
        return $this->executeQuery($sql);
    }

    public function getExpiringContracts()
    {  
        $this->currency_fields = array(
            'value'
        );
        $current_month_start = date("Y-m-01");
        $next_year_month_end = date("Y-m-d", strtotime("+1 Year", strtotime(date("Y-m-d", strtotime("-1 Day", strtotime(date("Y-m-01")))))));

        $over12Months = date("Y-m", strtotime("+13 months", strtotime(date("Y-m-d", strtotime("-1 Day", strtotime(date("Y-m-01")))))));

        $sql = "SELECT YEAR(contract_end_date) AS 'contract_end_year',  
					   MONTH(contract_end_date) AS 'contract_end_month',
					   COUNT(*) AS `total`, SUM(contract_value) AS `value` 
				  FROM contracts 
				 WHERE  contract_end_date >= '$current_month_start'
				
				 GROUP BY YEAR(contract_end_date), MONTH(contract_end_date)";
        //AND contract_end_date <= '$next_year_month_end'
        $expiring_contracts = array();
        foreach ($this->executeQuery($sql) as $expiring){
            if(strtotime($expiring['contract_end_year']."-".$expiring['contract_end_month'])<strtotime($over12Months)){
                 $expiring_contracts[$expiring['contract_end_year'] . sprintf("%02d", $expiring['contract_end_month'])] = $expiring;
            }else{
                if(!empty($expiring_contracts['over12Months']['total'])){
                    $expiring['total'] = $expiring_contracts['over12Months']['total']+$expiring['total'];
                }
                $expiring_contracts['over12Months'] =$expiring;
            }
          /* echo "<br /><pre>".$expiring['contract_end_year']." = ".date('Y',strtotime($current_month_start));
            print_r($expiring_contracts);*/
        }
        
        while (strtotime($current_month_start) <= strtotime($next_year_month_end)) {
            $current_month_process = date("m", strtotime($current_month_start));
            $current_year_process = date("Y", strtotime($current_month_start));
            $row_key = $current_year_process . sprintf("%02d", $current_month_process);
            
            if (! isset($expiring_contracts[$row_key]))
                $expiring_contracts[$row_key] = array(
                    'total' => 0,
                    'value' => 0,
                    'contract_end_year' => $current_year_process,
                    'contract_end_month' => $current_month_process
                );
            $expiring_contracts[$row_key]['month_name'] = date("M", strtotime($current_month_start)) . ' ' . $current_year_process;
           // echo "jjjjjjjjjjjjjj<pre>";print_r($expiring_contracts[$row_key]['month_name']);
            $current_month_days = date("t", strtotime($current_month_start));
            $current_month_start = date("Y-m-d", strtotime("+$current_month_days Days", strtotime($current_month_start)));
        }
        $expiring_contracts['over12Months']['contract_end_month'] = "02";
        
        $this->currency_fields = array();
        ksort($expiring_contracts);
        return $expiring_contracts;
    }

    public function getExpiringAndNoticePeriodContracts()
    {  
        $this->currency_fields = array(
            'value'
        );
        $current_month_start = date("Y-m-01");

        //$next_year_month_end = date("Y-m-d", strtotime("+1 Year", strtotime(date("Y-m-d", strtotime("-1 Day", strtotime(date("Y-m-01")))))));

        $next_year_month_end = date("Y-m-t", strtotime("+1 Year"));

        $monthArr = array();
        $month      = date("M/Y",strtotime($current_month_start));
        $monthArr[$month]['expiry_count'] = 0;
        $monthArr[$month]['noticeperiod_count'] = 0;
        for($i=1;$i<=12;$i++){
            $month      = date("M/Y",strtotime($current_month_start." + $i months"));
            $monthArr[$month]['expiry_count'] = 0;
            $monthArr[$month]['noticeperiod_count'] = 0;
        }

     
        $sql = "

        SELECT 'Count By Expiring' as type,
                date_format(contract_end_date,'%b/%Y') as month_year,
                YEAR(contract_end_date) AS 'contract_end_year',  
                MONTH(contract_end_date) AS 'contract_end_month',
                group_concat(contract_title separator ',') as title,
                group_concat(contract_end_date separator ',') as end_date,
                '' notice_date,
               SUM(
                    CASE
                        WHEN contract_end_date >= '$current_month_start' THEN 1
                        ELSE 0
                    END
               ) AS 'expiry_count',
               0 as 'notice_period_count'
               FROM contracts 
               WHERE  ( contract_archive='' or contract_archive is null) and contract_end_date between '$current_month_start' and '$next_year_month_end' GROUP by month_year
                 
                union 

                SELECT  'Count By Notice Period' as type,
                        date_format(break_clause,'%b/%Y') as month_year,
                        YEAR(break_clause) AS 'contract_end_year',  
                        MONTH(break_clause) AS 'contract_end_month',
                        group_concat(contract_title separator ',') as title,
                        '' as end_date,
                        group_concat(break_clause separator ',') notice_date,
                        0  as 'expiry_count',
                       SUM(
                            CASE
                                WHEN break_clause >= '$current_month_start' THEN 1
                                ELSE 0
                            END
                       ) AS 'notice_period_count'


                  FROM contracts 
                 WHERE ( contract_archive='' or contract_archive is null) and break_clause between '$current_month_start' and '$next_year_month_end' GROUP by month_year
                 ";
        //echo "<pre>";print_r($sql);exit;
        foreach ($this->executeQuery($sql) as $expiring){
            $month      = $expiring['month_year'];
           if($expiring['type']=='Count By Expiring'){
                $monthArr[$month]['expiry_count'] = $expiring['expiry_count'];
           }else{
                $monthArr[$month]['noticeperiod_count'] = $expiring['notice_period_count'];
            }
        }
        
        return $monthArr;
    }
 
    public function getExpiringAndNoticePeriodContractsList()
    {  
        $this->currency_fields = array(
            'value'
        );

        $user_id = Yii::app()->session['user_id'];
        $memberType = Yii::app()->session['member_type'];
        if($memberType !=1 || $memberType =="0"){
            $up = "INNER JOIN user_privilege up On c.location_id = up.location_id and c.department_id= up.department_id and contract_view='yes' and up.user_id=".$user_id;
        }else {$up = "";}

        $current_month_start = date("Y-m-01");

        //$next_year_month_end = date("Y-m-d", strtotime("+1 Year", strtotime(date("Y-m-d", strtotime("-1 Day", strtotime(date("Y-m-01")))))));

        $next_year_month_end = date("Y-m-t", strtotime("+1 Year"));

        $sql = "  
               ( SELECT 
               c.contract_id,
               c.contract_title as title, c.contract_manager,
               c.contract_end_date as end_date, c.break_clause as notice_period_date, v.vendor_name as vendor_name,v.vendor_id
               FROM contracts c
               LEFT JOIN vendors v ON c.vendor_id = v.vendor_id
               ".$up."
               WHERE  ( contract_archive='' or contract_archive is null) and (break_clause between '$current_month_start' and '$next_year_month_end')  order by 
                 end_date,
                notice_period_date desc )
              

                union 

                 (SELECT 
               c.contract_id,
               c.contract_title as title, c.contract_manager,
               c.contract_end_date as end_date, c.break_clause as notice_period_date, v.vendor_name as vendor_name,v.vendor_id
               FROM contracts c
               LEFT JOIN vendors v ON c.vendor_id = v.vendor_id
               ".$up."
               WHERE  ( contract_archive='' or contract_archive is null) and (contract_end_date between '$current_month_start' and '$next_year_month_end') order by 
                 end_date,
                notice_period_date desc  )   
                 

                ";
       
        
        return Yii::app()->db->createCommand($sql)->query()->readAll();
    }

    public function getContractStatistics($contractID,$tableName='contracts'){
        $data = array('totalDays'=>0,'expiryDays'=>0,'expiryDaysPercentage'=>0,'totalDocuments'=>0,'documentApproved'=>0,'documentApprovedPercentage'=>0,'totalFields'=>0,'fieldsCompleted'=>0,'fieldsCompletedPercentage'=>0);
        
        $sql = "select contract_end_date,created_datetime from $tableName where contract_id=".$contractID."  order by contract_end_date asc";
        $contract = Yii::app()->db->createCommand($sql)->queryRow();

        // $sql = "select ((contract_title !='')+(contract_reference !='')+(contract_type !='')+(location_id !=0)+(department_id !=0)+(contract_category_id !=0)+(contract_subcategory_id !=0)+(vendor_id !=0)+(contract_start_date !='0000-00-00')+(contract_end_date !='0000-00-00')+(contract_description !='')+(currency_id !=0)+(currency_rate !=0)+(contract_value !=0)+(contract_budget !=0)+(original_budget !=0)+(original_annual_budget !=0)+(po_number !='')+(cost_centre !='')+(break_clause !='')+(termination_terms !='')+(termination_comments !='')+(extension_options !='')+(extension_comments !='')+(user_id !=0)+(contract_manager !='')+(contact_name !='')+(website !='')+(contact_info !='')+(contract_comments !='')) as total_completed_fields from $tableName where contract_id=".$contractID;
          $sql = "select ((contract_title !='')+(contract_reference !='' or contract_reference is not NULL)+(contract_type !='')+(location_id !=0)+(department_id !=0)+(contract_category_id !=0 )+(contract_subcategory_id !=0)+(vendor_id !=0 )+(contract_start_date !='0000-00-00')+(contract_end_date !='0000-00-00')+(contract_description !='')+(currency_id !=0)+(currency_rate !=0 or currency_rate is not NULL)+(contract_value !=0)+(contract_budget !=0)+(original_budget !=0)+(original_annual_budget !=0)+(po_number !='')+(cost_centre !='')+(break_clause not in('0000-00-00 00:00:00','0000-00-00'))+(termination_terms !='')+(termination_comments !='')+(extension_options !='')+(extension_comments !='')+(user_id !=0)+(contract_manager !='' or contract_manager is not NULL)+(contact_name !=''  or contact_name is not NULL)+(contact_info !='' or contact_info is not NULL)+(contract_comments !='' or contract_comments is not NULL)) as total_completed_fields from $tableName where contract_id=".$contractID;
        $contractFields = Yii::app()->db->createCommand($sql)->queryRow();


        $sql = "select sum(CASE WHEN status = 'Approved' THEN 1 ELSE 0 END) as total_approved,sum(CASE WHEN status = 'Pending' THEN 1 ELSE 0 END) as total_pending from contract_documents where contract_documents.archive !=1 and contract_id=".$contractID;
        $document = Yii::app()->db->createCommand($sql)->queryRow();
       
        $now = strtotime(date("Y-m-d")); // or your date as well
        $endDate = strtotime($contract['contract_end_date']);
        $diff=date_diff($contract['contract_end_date'],date("Y-m-d"));

        $currentDate = new DateTime(date("Y-m-d"));
        $endDate = new DateTime($contract['contract_end_date']);
        if($contract['contract_end_date'] !="0000-00-00"){
            $expiryDays  = ($contract['contract_end_date']<date("Y-m-d")?"-":"").$endDate->diff($currentDate)->format('%a');
        }else{
             $expiryDays  ='0';
        }


        $createdDatetime =  new DateTime($contract['created_datetime']);
        $totalDays = $endDate->diff($createdDatetime)->format('%a');

        $data['expiryDays'] = number_format($expiryDays,0,",","");
        $data['totalDays'] = $totalDays;
        $data['expiryDaysPercentage'] = number_format($data['expiryDays'],0,",","");
        if(!empty($document['total_approved']) || !empty($document['total_pending'])){
            $data['documentApproved'] = $document['total_approved'];
            $data['totalDocuments'] = $document['total_approved']+$document['total_pending'];
            $data['documentApprovedPercentage'] =  number_format($data['documentApproved']/ $data['totalDocuments']*100,0);
        }

        if(!empty($contractFields['total_completed_fields'])){
            $data['fieldsCompleted'] = $contractFields['total_completed_fields'];
            $data['totalFields'] = 29;
            $data['fieldsCompletedPercentage'] = number_format($data['fieldsCompleted']/$data['totalFields']*100,0);
        }
        return $data;
    }

    public function getMetrics($start_date, $end_date,$checkPermission='')
    {
        $tool_currency = Yii::app()->session['user_currency'];

        $currentDate = date("Y-m-d");
        $this->currency_fields = array(
            'contract_total'
        );
        $metrics = array(
            'active_count' => 0,
            'active_total' => 0,
            'expire_current_count' => 0,
            'expire_current_total' => 0,
            'expire_later_count' => 0,
            'expire_later_total' => 0,
            'active_spark19Exp' => '',
            'active_total' => 0,
            'active_all' => 0,
            'active_total' => 0,
            'active_spark20Exp'=>'',


            'expired_only_total' => 0,
            'expired_only_count' => 0,
            'expired_only_all' => 0,
            'expired_only_sparkExp'=>'',

            'active_count'=>0,
            'expiries_all'=>0,
            'expiry_spark21Exp'=>'',
            'expiry_spark22Exp'=>'',
            'expiry_spark23Exp'=>'',
            'expiry_spark24Exp'=>'',
        );

        $user_id = Yii::app()->session['user_id'];
        $memberType = Yii::app()->session['member_type'];
        if($memberType !=1 || $memberType =="0"){
            $user_id = $user_id;
        }
        $expirydate3months = date("Y-m-d", strtotime("+ 3 months"));
        // active ones
        $joinPermission="";
 

        if(!empty($checkPermission) && ($memberType !=1 || $memberType =="0")){
            $joinPermission = "INNER JOIN user_privilege up On contracts.location_id = up.location_id and contracts.department_id= up.department_id and up.contract_view='yes' and up.user_id='".$user_id."'";
        }
        $sql = "SELECT sum(getOrderTotalInGB(contracts.contract_value,contracts.currency_id)) AS contract_total,contract_manager,contract_title,contract_end_date,break_clause,contract_id,count(contract_id) as count_total, v.vendor_name as vendor_name,v.vendor_id
          FROM contracts 
          LEFT JOIN vendors v ON contracts.vendor_id = v.vendor_id
        ".$joinPermission."
          WHERE ( contract_archive='' or contract_archive is null) and contract_end_date >= '".$expirydate3months."'";
             $metrics['active_all'] = $this->executeQuery($sql.' GROUP by contracts.contract_id ORDER BY contract_end_date ASC');
        foreach ($metrics['active_all'] as $contract_data) {
            $metrics['active_spark19Exp'] .= '{ x: "'.$contract_data['contract_title'].'",y: '.$contract_data['contract_total'].'},';
            $metrics['active_total'] += $contract_data['contract_total'];

            $metrics['active_spark20Exp'] .= '{ x: "'.$contract_data['contract_title'].'",y: '.$contract_data['count_total'].'},';
            $metrics['active_count'] += $contract_data['count_total'];
        }

        // active ones
         $sql = "SELECT sum(getOrderTotalInGB(contracts.contract_value,contracts.currency_id)) AS contract_total,contract_manager,contract_title,contract_end_date,break_clause,contract_id,count(contract_title) as count_total, v.vendor_name as vendor_name,v.vendor_id
          FROM contracts 
          LEFT JOIN vendors v ON contracts.vendor_id = v.vendor_id
          ".$joinPermission."
                  WHERE   ( contract_archive='' or contract_archive is null) and contract_end_date < '".$currentDate."'";
             $metrics['expired_only_all'] = $this->executeQuery($sql.' GROUP by contracts.contract_id ORDER BY contract_end_date DESC');

        foreach ($metrics['expired_only_all'] as $contract_data) {
            $metrics['expired_only_sparkExp'] .= '{ x: "'.$contract_data['contract_title'].'",y: '.$contract_data['contract_total'].'},';
            $metrics['expired_only_total'] += $contract_data['contract_total'];
            $metrics['expired_only_count'] += $contract_data['count_total'];
        }

        //CVarDumper::dump($metrics['active_all'],10,1);die;
        
        // expiring within 30 days
        $expirydate30Days = date("Y-m-d", strtotime("+30 Days"));
        $expiry_start_date = date("Y-m-d");
         $sql = "SELECT sum(getOrderTotalInGB(contracts.contract_value,contracts.currency_id)) AS contract_total,contract_manager,contract_title,count(contract_title) as count_total,contract_end_date,break_clause,contract_id ,  v.vendor_name as vendor_name,v.vendor_id
           FROM contracts 
           LEFT JOIN vendors v ON contracts.vendor_id = v.vendor_id
          ".$joinPermission."
           WHERE  ( contract_archive='' or contract_archive is null) and   
           contract_end_date <= '".$expirydate30Days."' AND contract_end_date >='".$expiry_start_date."'";
             $metrics['expiries_all'] = $this->executeQuery($sql.' GROUP by contracts.contract_id ORDER BY contract_end_date ASC');
        foreach ($metrics['expiries_all'] as $contract_data) {
            $metrics['expiry_spark21Exp'] .= '{ x: "'.$contract_data['contract_title'].'",y: '.$contract_data['contract_total'].'},';
            $metrics['expire_current_total'] += $contract_data['contract_total'];

            $metrics['expiry_spark22Exp'] .= '{ x: "'.$contract_data['contract_title'].'",y: '.$contract_data['count_total'].'},';
            $metrics['expire_current_count'] += $contract_data['count_total'];
        }
       /* $sql = "SELECT sum(getTotalInGBandToolCurrency(contracts.contract_value,contracts.currency_id,'".$tool_currency."')) AS contract_total,count(contract_title) as count_total,DATE_FORMAT(contract_end_date,'%m') as month,
            
            case 
                when day(contract_end_date) <= 10 then concat(DATE_FORMAT(contract_end_date,'%01/%m/%Y'),' to ',DATE_FORMAT(contract_end_date,'%10/%m/%Y'))
                when day(contract_end_date) <= 20 then concat(DATE_FORMAT(contract_end_date,'11/%m/%Y'),' to ',DATE_FORMAT(contract_end_date,'20/%m/%Y'))
                else concat(DATE_FORMAT(contract_end_date,'21/%m/%Y'),' to ',DATE_FORMAT(contract_end_date,'31/%m/%Y'))
            end as date_period

                  FROM contracts WHERE 
                    contract_end_date <= '".$expiry_end_date."' AND contract_end_date >='".$expiry_start_date."'";
                $metrics['expiries_all'] =  $this->executeQuery($sql." GROUP BY date_period ORDER BY month ASC");

               // contract_status = 1 
        foreach($metrics['expiries_all'] as $contract_data) {
            //$metrics['expire_current_count'] += $contract_data['contract_count'];
            $metrics['expire_current_total'] += $contract_data['contract_total'];

            $metrics['expiry_spark21Exp'] .= '{ x: "'.$contract_data['date_period'].'",y: '.$contract_data['contract_total'].'},';
            
        }*/

       // CVarDumper::dump($metrics['expiry_spark21Exp'],10,1);die;


        
        // expiring within 3 months
       
        $sql = "SELECT sum(getOrderTotalInGB(contracts.contract_value,contracts.currency_id)) AS contract_total,contract_title,count(contract_title) as count_total,contract_end_date,break_clause,contract_id,contract_manager,v.vendor_name as vendor_name,v.vendor_id
          FROM contracts 
          LEFT JOIN vendors v ON contracts.vendor_id = v.vendor_id
         ".$joinPermission."
          WHERE  ( contract_archive='' or contract_archive is null) and contract_end_date > '".$expirydate30Days."' AND contract_end_date <= '".$expirydate3months."'";
        $metrics['expiries_later_all'] = $this->executeQuery($sql.' GROUP by contracts.contract_id ORDER BY contract_end_date ASC');
        foreach ($metrics['expiries_later_all'] as $contract_data) {
            $metrics['expire_later_count'] += $contract_data['count_total'];
            $metrics['expire_later_total'] += $contract_data['contract_total'];
            $metrics['expiry_spark23Exp'] .= '{ x: "'.$contract_data['contract_title'].'",y: '.$contract_data['contract_total'].'},';
            $metrics['expiry_spark24Exp'] .= '{ x: "'.$contract_data['contract_title'].'",y: '.$contract_data['count_total'].'},';
        }
    
        $this->currency_fields = array();
        return $metrics;
    }

    public function getDashboardMetrics($start_date, $end_date)
    {
        $this->currency_fields = array(
            'contract_total'
        );
        $metrics = array(
            'active_count' => 0,
            'active_total' => 0,
            'expire_current_count' => 0,
            'expire_current_total' => 0,
            'expire_later_count' => 0,
            'expire_later_total' => 0
        );
        
        // active ones
        $sql = "SELECT SUM(contract_value) AS `contract_total`, COUNT(*) AS `contract_count`
				  FROM contracts WHERE contract_status = 1
				   AND contract_end_date >= NOW() AND contract_start_date <= NOW()";
        foreach ($this->executeQuery($sql) as $contract_data) {
            $metrics['active_count'] += $contract_data['contract_count'];
            $metrics['active_total'] += $contract_data['contract_total'];
        }

        //CVarDumper::dump($metrics,10,1);die;
        
        // expiring within 30 days
        $expiry_date = date("Y-m-d", strtotime("+30 Days"));
        $sql = "SELECT SUM(contract_value) AS `contract_total`, COUNT(*) AS `contract_count`
				  FROM contracts WHERE contract_status = 1 AND contract_end_date >= NOW()
				   AND contract_end_date <= '$expiry_date' AND contract_start_date <= NOW()";
        foreach ($this->executeQuery($sql) as $contract_data) {
            $metrics['expire_current_count'] += $contract_data['contract_count'];
            $metrics['expire_current_total'] += $contract_data['contract_total'];
        }
        
        // expiring within 3 months
        $expiry_date = date("Y-m-d", strtotime("+91 Days"));
        $sql = "SELECT SUM(contract_value) AS `contract_total`, COUNT(*) AS `contract_count`
				  FROM contracts WHERE contract_status = 1 AND contract_end_date >= NOW()
				   AND contract_end_date <= '$expiry_date' AND contract_start_date <= NOW()";
        foreach ($this->executeQuery($sql) as $contract_data) {
            $metrics['expire_later_count'] += $contract_data['contract_count'];
            $metrics['expire_later_total'] += $contract_data['contract_total'];
        }
        
        $this->currency_fields = array();
        return $metrics;
    }

    public function export($data = array(), $file = '')
    {
        $sql = "SELECT 
            r.contract_reference as Contract_Reference,
            r.contract_title as Title,
            c.value AS Category, 
            sub_c.value AS Sub_Category, 
            GROUP_CONCAT(DISTINCT l.location_name SEPARATOR ', ') as Locations,
            GROUP_CONCAT(DISTINCT d.department_name SEPARATOR ', ') as Departments,
            u.full_name as Created_By,
            u_sbo.full_name as Managed_By,
            vendor_name as Supplier,
            cr.currency as Currency,
            CONCAT(cr.currency_symbol,contract_value) as Estimated_Value,
            CASE 
             WHEN contract_start_date IS NULL OR DATE_FORMAT(contract_start_date, '%d/%m/%Y') = '00/00/0000' THEN ''
            ELSE DATE_FORMAT(contract_start_date, '%d/%m/%Y') 
            END AS Start_Date,
            CASE 
             WHEN break_clause IS NULL OR DATE_FORMAT(break_clause, '%d/%m/%Y') = '00/00/0000' THEN ''
            ELSE DATE_FORMAT(break_clause, '%d/%m/%Y') 
            END AS Notice_Period_Date,
            CASE 
             WHEN contract_end_date IS NULL OR DATE_FORMAT(contract_end_date, '%d/%m/%Y') = '00/00/0000' THEN ''
            ELSE DATE_FORMAT(contract_end_date, '%d/%m/%Y') 
            END AS End_Date
                
         FROM contracts r
         LEFT JOIN vendors v ON r.vendor_id = v.vendor_id
         LEFT JOIN users u ON r.user_id = u.user_id
         LEFT JOIN users u_sbo ON r.sbo_user_id = u_sbo.user_id
         LEFT JOIN categories c ON r.contract_category_id = c.id
         LEFT JOIN sub_categories sub_c ON r.contract_subcategory_id = sub_c.id 
         LEFT JOIN currency_rates cr ON r.currency_id = cr.id
         LEFT JOIN contract_location cl ON r.contract_id = cl.contract_id
         LEFT JOIN locations l ON cl.location_id = l.location_id
         LEFT JOIN contract_department dl ON r.contract_id = dl.contract_id
         LEFT JOIN departments d ON dl.department_id = d.department_id
         WHERE r.contract_archive='' or r.contract_archive is null
         group by r.contract_id 
        ORDER BY r.contract_id DESC ";
        parent::exportCommon($this->executeQuery($sql), 'contracts');
        $this->currency_fields = array();
    }

    public function refreshNotifications($contract_id = 0, $date = "")
    {
        $sql = "UPDATE contract_notifications SET processed = 0 WHERE 1 = 1";
        if ($contract_id)
            $sql .= " AND contract_id = $contract_id ";
        if (! empty($date))
            $sql .= " AND date = '$date' ";
        $this->executeQuery($sql);
    }

    public function saveContractFile($contract_id, $file_name, $file_description)
    {
        return $this->executeQuery("INSERT INTO contract_files (contract_id, file_name, description) SELECT $contract_id, '$file_name', '$file_description'");
    }

    public function deleteFile($contract_id, $file_name)
    {
        $this->executeQuery("DELETE FROM contract_files WHERE contract_id = $contract_id AND file_name = '$file_name'");
    }

    public function contractExpiryDays()
    {
        $sql = "SELECT *
				  FROM contracts c
				 WHERE c.contract_status = 1 AND c.contract_end_date > now()";
        $contracts = $this->executeQuery($sql);
        $arr = [];
        foreach($contracts as $contract){

            $current_date = strtotime(date('Y-m-d'));
            $contract_end_date = strtotime($contract['contract_end_date']);
            $datediff = $contract_end_date - $current_date;

            $arr[$contract['contract_id']] = $total_days = round($datediff / (60 * 60 * 24));
            if($total_days==90){
                $this->contractExpiry($contract['contract_id'],90);
            }elseif($total_days==30){
                $this->contractExpiry($contract['contract_id'],30);
            }elseif($total_days==14){
                $this->contractExpiry($contract['contract_id'],14);
            }elseif($total_days==5){
                $this->contractExpiry($contract['contract_id'],5);
            }

        }
        //CVarDumper::dump($arr,10,1);die;

    }

    public function contractExpiry ($contract_id,$days)
    {
        $contract = $this->executeQuery("SELECT *
				  FROM contracts c
				  INNER JOIN vendors v ON c.vendor_id=v.vendor_id
				  WHERE c.contract_status = 1 AND c.contract_id = $contract_id",1);

        $location_id = $contract['location_id'];
        $department_id = $contract['department_id'];
        $contract_reference = !empty($contract['contract_reference'])?$contract['contract_reference']:'N/A';
        $users = $this->executeQuery("SELECT * FROM users WHERE location_id = $location_id AND department_id = $department_id");
        if(count($users)>0) {

            foreach ($users as $user_data) {

                $mail = new \PHPMailer(true);
                $mail->IsSMTP();
                $mail->Host = Yii::app()->params['host'];
                $mail->Port = Yii::app()->params['port'];
                $mail->SMTPAuth = true;
                $mail->Username = Yii::app()->params['username'];
                $mail->Password = Yii::app()->params['password'];
                $mail->SMTPSecure = Yii::app()->params['port'];
				$mail->CharSet="UTF-8";

                $email_body = '';

                $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <div style="margin:0px auto;max-width:600px;">
                <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="vertical-align:top;width:600px;">
                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                 <tr>
                  <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                  <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                      <tr>
                          <td style="width:600px;">';
                        $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
                        $email_body .= '</td>
                      </tr>
                      </tbody>
                  </table>
                  </td>
                 </tr>
                 <tr>
                 <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;width:600px;">';
                        $email_body .= '<p>Dear ' . $user_data['full_name'] . ', </p>';
                        $email_body .= '</td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center;">';
                        $email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                         <h1 style="font-family: Cabin; line-height: 100%;">
                             <span style="color:#000000;"><strong>Contract due to expire in ' . $days . ' days - <span style="color:#10a798;">' . $contract_reference . '</span> </strong></span>
                         </h1>
                     </div>
                 </td>
                 </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;width:600px;">';
                        $email_body .= '<p>The contract for <span style="color:red;">' . $contract['vendor_name'] . '</span> is due to expire in ' . $days . ' days time. Please update the status of the contract following the below link.</p>';
                        $email_body .= '</div>
                     </td>
                 </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                     </td>
                 </tr>';

                        $email_body .= '<tr>
                       <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                        <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                        <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                        <a href="' . AppUrl::bicesUrl('contracts/edit/' . $contract_id) . '" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Contract Management</a>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>

                         <tr>
                             <td style="word-wrap:break-word;font-size:0px;">
                             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                             </td>
                         </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;" align="left">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                             <p>Terms of Service &amp; User Agreement</p>
                             <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                             <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                         </div>
                     </td>
                 </tr>
                 </tbody>
                </table>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </div></td>
                </tr>
                </table>
                </div>';
                $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
                $mail->addAddress($user_data['email'], $user_data['full_name']);
                $mail->Subject = 'Spend 365: Contract Expiry ('.$days.' days) ';
                $mail->isHTML(true);
                $mail->Body = $email_body;
                $mail->send();
            }
        }
        return 0;
    }

    public function changeToContract ($contract_id)
    {
        $contract = $this->executeQuery("SELECT *, concat(user_id, ',',sbo_user_id,',',commercial_user_id,',',procurement_user_id) as notification_users
				  FROM contracts c
				  LEFT JOIN vendors v ON c.vendor_id=v.vendor_id
				  WHERE c.contract_id = $contract_id",1);

        $location_id = $contract['location_id'];
        $department_id = $contract['department_id'];
        $contract_reference = !empty($contract['contract_reference'])?$contract['contract_reference']:'N/A';
        $vendor_name = !empty($contract['vendor_name'])?$contract['vendor_name']:'N/A';
        //$users = $this->executeQuery("SELECT * FROM users WHERE location_id = $location_id AND department_id = $department_id");
        $usersIDs = "";
        

        $users = $this->executeQuery("SELECT * FROM users WHERE user_id in(".$contract['notification_users'].")");



        if(count($users)>0) {

            foreach ($users as $user_data) {

               
                $mail = new \PHPMailer(true);
                $mail->IsSMTP();
                $mail->Host = Yii::app()->params['host'];
                $mail->Port =Yii::app()->params['port'];
                $mail->SMTPAuth = true;
                $mail->Username = Yii::app()->params['username'];
                $mail->Password = Yii::app()->params['password'];
                $mail->SMTPSecure = Yii::app()->params['stmpSecure'];
				$mail->CharSet="UTF-8";

                $email_body = '';

                $email_body .= '<div class="mj-container" style="background-color:#FFFFFF;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
                <tr>
                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <div style="margin:0px auto;max-width:600px;">
                <table role="presentation" style="font-size:0px;width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td style="vertical-align:top;width:600px;">
                <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                <table role="presentation" width="600" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                 <tr>
                  <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                  <table role="presentation" style="border-collapse:collapse;border-spacing:0px;" cellspacing="0" cellpadding="0" border="0" align="center">
                      <tbody>
                      <tr>
                          <td style="width:600px;">';
                $email_body .= '<img src="' . AppUrl::bicesUrl('images/email_banner.png') . '" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="600" height="auto" /><br /><br />';
                $email_body .= '</td>
                      </tr>
                      </tbody>
                  </table>
                  </td>
                 </tr>
                 <tr>
                 <td style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;width:600px;">';
                $email_body .= '<p>Dear ' . $user_data['full_name'] . ', </p>';
                $email_body .= '</td>
                 </tr>
                 <tr>
                 <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center;">';
                $email_body .= '<div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                         <h1 style="font-family: Cabin; line-height: 100%;">
                             <span style="color:#000000;"><strong>Change to contract - <span style="color:#10a798;">' . $contract_reference . '</span> </strong></span>
                         </h1>
                     </div>
                 </td>
                 </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;width:600px;">';
                $email_body .= '<p>The contract for <span style="color:red;">' . $vendor_name . '</span>  has been updated/changed. Please find the details via the Contract Management link below.</p>';
                $email_body .= '</div>
                     </td>
                 </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;">
                     <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                     </td>
                 </tr>';

                $email_body .= '<tr>
                       <td style="word-wrap:break-word;font-size:0px;padding:10px 25px 10px 25px;padding-top:10px;padding-left:25px;" align="center">
                        <table role="presentation" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr>
                        <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" valign="middle" bgcolor="#334249" align="center">
                        <a href="' . AppUrl::bicesUrl('contracts/edit/' . $contract_id) . '" style="text-decoration:none;background:#334249;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:25px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Contract Management</a>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>

                         <tr>
                             <td style="word-wrap:break-word;font-size:0px;">
                             <div style="font-size:11px;line-height:22px;white-space:nowrap;color:#000000;font-family:Roboto, Tahoma, sans-serif;">&nbsp;</div>
                             </td>
                         </tr>

                 <tr>
                     <td style="word-wrap:break-word;font-size:0px;" align="left">
                         <div style="cursor:auto;color:#000000;font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:22px;text-align:left;">
                             <p>Terms of Service &amp; User Agreement</p>
                             <p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Website-Terms-and-Conditions.pdf">Spend 365 Website Terms and Conditions</a></p>
                             <p>Privacy Policy&nbsp;</p><p><a href="https://spend-365.com/wp-content/uploads/2018/06/Spend-365-Privacy-policy.pdf">Spend 365 Privacy policy</a></p>
                         </div>
                     </td>
                 </tr>
                 </tbody>
                </table>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </div></td>
                </tr>
                </table>
                </div>';
                $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
                $mail->addAddress($user_data['email'], $user_data['full_name']);
                $mail->Subject = 'Spend 365: Change To Contract';
                $mail->isHTML(true);
                $mail->Body = $email_body;
                $mail->send();
            }
        }

        return 0;
    }

    public function processNotifications($contract_id = 0, $date = "")
    {   
        $admin_emails = array();
        $sql = "SELECT * FROM users WHERE status = 1 AND admin_flag = 1";
        foreach ($this->executeQuery($sql) as $admin_user)
            $admin_emails[$admin_user['username']] = $admin_user['email'];
        
        $sql = "SELECT *, n.id AS notification_id
				  FROM contract_notifications n, contracts c 
				 WHERE c.contract_id = n.contract_id AND n.processed = 0";
        if ($contract_id)
            $sql .= " AND n.contract_id = $contract_id ";
        if (! empty($date))
            $sql .= " AND n.date = '$date' ";
        
        $mail = new \PHPMailer(true);
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['host'];
		$mail->Port = Yii::app()->params['port'];
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['username'];
		$mail->Password = Yii::app()->params['password'];
		$mail->SMTPSecure = Yii::app()->params['stmpSecure'];
        
        foreach ($this->executeQuery($sql) as $a_notification) {
            $email_body = "This is a notification from the contracts system.<br /><br />";
            $email_body .= '<strong>' . $a_notification['note'] . '</strong><br />';
            $email_body .= '<strong>Contract ID: </strong>' . $a_notification['contract_id'] . '<br />';
            $email_body .= '<strong>Contract Number: </strong>' . $a_notification['contract_reference'] . '<br />';
            $email_body .= '<strong>Contract Title: </strong>' . $a_notification['contract_title'] . '<br />';
            $email_body .= '<strong>Start Date: </strong>' . date("F j, Y", strtotime($a_notification['contract_start_date'])) . '<br />';
            $email_body .= '<strong>End Date: </strong>' . date("F j, Y", strtotime($a_notification['contract_end_date'])) . '<br />';
            if (! empty($a_notification['contract_value']))
                $email_body .= '<strong>Contract Value: </strong>' . number_format($a_notification['contract_value'], 2) . '<br />';
            $email_body .= '<strong>Contract Type: </strong>' . $a_notification['contract_type'] . '<br />';
            $email_body .= '<strong>Contract Manager: </strong>' . $a_notification['contract_manager'] . '<br />';
            $email_body .= '<strong>Comments: </strong>' . $a_notification['contract_workstream'] . '<br /><br />';
            
            foreach ($admin_emails as $admin_user_name => $admin_user_email) {
                $mail->setFrom(Yii::app()->params['email'], 'Spend 365 Admin');
                $mail->addAddress($admin_user_email, $admin_user_name);
                $mail->Subject = 'Contract #' . $a_notification['contract_reference'] . ' - ' . $a_notification['note'];
                $mail->isHTML(true);
                $mail->Body = $email_body;
                $mail->send();
            }
            
            $this->executeQuery("UPDATE contract_notifications SET processed = 1 WHERE id = " . $a_notification['notification_id']);
        }
    }

    public function getProductContracts()
    {
        $sql = "SELECT c.*
                  FROM contracts c
                  LEFT JOIN products p ON c.contract_id = p.contract_id
                ";

        return $this->executeQuery($sql . " ORDER BY contract_title ASC");
    }

    public function getProdContracts()
    {
        $sql = "SELECT contract_id,contract_title FROM contracts ";

        return $this->executeQuery($sql . " ORDER BY contract_title ASC");
    }

}
