<?php

date_default_timezone_set("Europe/London");

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'];
$parsedUrl = parse_url($url);
$customSubDomain = explode('.', $parsedUrl['host']);
$customSubDomain = $customSubDomain[0];
$localSubDomain = 	"localhost";

$subDomainArray = array(
					 //1=> $localSubDomain,
					 2=>"usg",
					 3=>"resco"
					);

if(in_array($customSubDomain,$subDomainArray)){
	if(!empty($subDomainArray[1]) && $subDomainArray[1] == $customSubDomain){
	    $customSubDomain = $subDomainArray[2];
	}
	
	if($customSubDomain == 'usg'){
	 $configFile = 'main_'.$customSubDomain.'.php';
	}else{
		
	  $configFile = 'main_'.$customSubDomain.'.php';
	}
}else{
	$configFile = 'main.php';
}

$config=dirname(__FILE__).'/protected/config/'.$configFile;
//$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following line when in production mode
//defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once($yii);
Yii::createWebApplication($config)->run();
