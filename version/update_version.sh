#!/bin/sh

echo "" > update_version.sql
for directory in `cat container_names.txt`
do
        echo $directory
        echo "USE $directory ; " >> update_version.sql
        echo " " >> update_version.sql
        for tablename in `cat update_version.txt`
        do
                echo "ALTER TABLE $directory.$tablename ENGINE='InnoDB' ; " >> update_version.sql
        done
        echo " " >> update_version.sql
        echo " " >> update_version.sql
done
